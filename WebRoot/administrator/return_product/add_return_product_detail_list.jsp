<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemMatchKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductStatusKey"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductPictureRecognitionkey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%> 
  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加退货单</title>

<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<!-- table -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script type="text/javascript" src="../js/setImgWeightHeight.js"></script>
<style type="text/css">
    *{font-size:12px;}
    .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
    span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
	span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
	span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
	span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
	div.checkDiv{font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:90%;margin-top:3px;margin-bottom:5px;border-bottom:1px dashed  silver;border-top:1px dashed  silver;}
	a.normal{font-weight:normal;text-decoration:none;}
	a.normal:hover{text-decoration:underline ;}
</style>
<%
	long rp_id =  StringUtil.getLong(request,"rp_id");
	 DBRow[] returnProductDetails =  returnProductItemsFixMgrZr.getReturnProductItmesFix(rp_id) ;
	 String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	 ReturnProductItemMatchKey returnProductItemMatchKey = new ReturnProductItemMatchKey();
	 DBRow returnProductRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
	 int isReturnOver = ReturnProductKey.FINISHALL==returnProductRow.get("status",0)?1:2;
	 //mergeArray放无退货明细的退货文件明细
	 //map放有退货明细的退货文件明细
	 List<DBRow> mergeArray = new ArrayList<DBRow>();
	 Map<Long , List<DBRow>> map = new HashMap<Long,List<DBRow>>();
	 for(DBRow rows : returnProductDetails ){
		 long tempRpiId = rows.get("rpi_id",0l);
		 if(tempRpiId != 0l){
	 List<DBRow> tempList = map.get(tempRpiId);
	 if(tempList == null){
		 tempList = new ArrayList<DBRow>();
		 map.put(tempRpiId,tempList);
	 }
	 tempList.add(rows);
	  
		 }else{
	 mergeArray.add(rows);
		 }
	 } 
	 TDate date = new TDate();
	 ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
	 ArrayList<String> returnRequestTypelist = ( ArrayList<String> )handleResultRequestTypeKey.getStatus();
	 ReturnProductItemHandleResultTypeKey returnProductResultTypekey = new ReturnProductItemHandleResultTypeKey();
	 ArrayList<String> returnResultTypelist = ( ArrayList<String> )returnProductResultTypekey.getStatus();
	 AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
	 int isCanRegister = adminLoggerBean.getPs_id() == returnProductRow.get("ps_id",0L)?1:2;
	 int gather_picture_status = returnProductRow.get("gather_picture_status",0);//图片采集
	 int picture_recognition = returnProductRow.get("picture_recognition",0);//图片识别
%>
<script type="text/javascript">
//文件上传
//做成文件上传后,然后页面刷新提交数据如果是有添加文件
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
    $("input[name='file_names']").val(fileNames);
    if(fileNames.length > 0 && fileNames.split(",").length > 0 ){
        $("input[name='file_names']").val(fileNames);
        var myform = $("#uploadImageForm");
        var file_with_class = $("#file_with_class").val();
			 
		  	myform.submit();
	}
}
function addPictureDetail(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_picture_detail.html?rp_id="+ <%= rp_id%>; 
	 $.artDialog.open(uri , {title: '新增图片',width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
}
function addReturnPictureDetail(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_detail.html?rp_id="+ <%= rp_id%>; 
	 $.artDialog.open(uri , {title: '新增明细',width:'800px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
//显示在线图片
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function refreshWindow(){window.location.reload();}
function addReturnProductDettail(items_fix_rpi_id,_this)
{
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/relation_one_return_product.html?rp_id="+ <%= rp_id%> + "&items_fix_rpi_id="+items_fix_rpi_id; 
	 $.artDialog.open(uri , {title: '新增退货商品',width:'550px',height:'430px', lock: true,opacity: 0.3,fixed: true});
}
function pictrueNotMatch(fix_rpi_id){
    if (confirm("确认标记该图片内容?")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/SetReturnProductFixItemNotMatchAction.action' +"?fix_rpi_id="+fix_rpi_id,
			dataType:'json',
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}
			},
			error:function(){alert("更新失败,请稍后再试.");return ;}
		})
	}
}
var isEdit = false;
var nodeHtml = "" ;
function updateItem(vvv,_this){
    //var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/modify_return_product_item.html?rp_id="+ <%= rp_id%>; 
	// $.artDialog.open(uri , {title: '修改商品明细',width:'300px',height:'130px', lock: true,opacity: 0.3,fixed: true});
	if(isEdit){
		alert("你当前还有未保存的明细");
		return ;
	}
	isEdit = true ;
   var input = '<input type="text" name="p_name" class="p_name" style="width:100px;background:#FFB5B5;"/>&nbsp;<input type="text" class="quantity_row" style="color:#FF3300;font-weight:bold;width:35px;background:#FFFFF;" value="1"/>'+$("#returnProductReturnResultDiv").html()+' <a href="javascript:void(0)" class="entity_row_confire_a" onclick="updateReturnProductItem('+vvv+',this)">确定</a>&nbsp;' 
       		 + '<a href="javascript:void(0)" onclick="cancenlUpdate(this);">取消</a>';
 
	var node = $(_this).parent();
	nodeHtml=node.html();
	node.html(input);
	initPname();
}
function cancenlUpdate(_this){
    $(_this).parent().html(nodeHtml);
    isEdit = false ;
}
function updateReturnProductItem(rpi_id,_this){
	var parent = $(_this).parent();
	var p_name = ($(".p_name",parent).val());
	if(p_name.length < 1){
		alert("请先输入商品.");
		return ;
	}
	var node = $(_this);
	var quantityValue = ($(".quantity_row",parent)).val();
	if($.trim(quantityValue).length < 1){
		alert("请先输入数量");
		return ;
	}
	var handle_result_request = $("select[name='handle_result_request']",parent).val();
	if(handle_result_request * 1  < 0){
		alert("请先选择要求处理结果");
		return ;
	}
	 
	 
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ModifyReturnProductItemAction.action' + "?p_name="+p_name+"&rpi_id="+rpi_id+"&quantity=" +quantityValue+"&handle_result_request="+handle_result_request ,
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}
		},
		error:function(){alert("系统错误.");return ;}
	})
}
function deleteItem(rpi_id,fix_rpid_id){
	var str = "";
	if('<%=returnProductRow.get("gather_picture_status",0)%>' == 1)
	{
		str = "图片采集已完成  ";
	}
	if('<%=returnProductRow.get("picture_recognition",0)%>' == '<%=ReturnProductPictureRecognitionkey.RecognitionFinish %>')
	{
		str = "图片识别已完成  ";
	} 
	$.artDialog.confirm(str+'你确认删除该商品操作？', function(){
		ajaxDelete(rpi_id ,fix_rpid_id, '<%= rp_id%>');
	}, function(){
		
	});
}
function ajaxDelete(rpi_id,fix_rpid_id,rp_id){
	 var obj = {rpi_id:rpi_id,rp_id:rp_id,fix_rpid_id:fix_rpid_id};
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/DeleteReturnProductItemAction.action';
	 commonAjax(uri,obj);
}
// json 放回结果为success的适用
function commonAjax(uri,dataJson){
    $.ajax({
	    url:uri,
	    data:dataJson,
	    dataType:'json',
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag == "success"){
				window.location.reload();
		   	}else{
		   		 alert("系统错误.");
			}
    	},
    	error:function(){$.unblockUI();alert("系统错误.");}
	})
}
function selectAll(_this){
    var node = $(_this);
    var flag = node.attr("checked") == "checked";
   	$(".selectSingle").attr("checked",flag);
}
function selectSingle(_this){
    checkAllCheckBoxSelect();
   
}
function checkAllCheckBoxSelect(){
    var array = $(".selectSingle");
    var flag = true ;
    for(var index = 0 , count = array.length ; index < count ; index++ ){
		var nodeFlag = $(array.get(index)).attr("checked") == "checked";
		if(!nodeFlag){
			flag = false ;
			break;
		}
	}
	$("#checkall").attr("checked",flag);
}

 function getSelectIds(){
	var str = "" ;
	var array = $(".selectSingle");
	for(var index = 0 , count = array.length ; index < count ; index++ ){
	     var node = $(array.get(index));
		 if(node.attr("checked") == "checked"){
		     str += ","+node.attr("target");
		}
	}
	return str ;
 }
 function relationProduct(){
	var str = getSelectIds();
	if(str.length < 1){
		alert("请先勾选图片.");
		return ;
	}
	str = str.substr(1);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/relation_return_product.html?fix_rpi_ids="+ str + "&rp_id=" + '<%= rp_id%>'; 
	$.artDialog.open(uri , {title: '图片关联商品',width:'800px',height:'530px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	
 }
 function deleteReturnProductItemFix(fix_rpi_id) {

	 $.artDialog.confirm('你确认删除此明细？', function(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/RemoveReturnProductItemFixAction.action';
		 var obj = {fix_rpi_id:fix_rpi_id};
	     commonAjax(uri,obj);
			
		 }, function(){
			
		});

	 
    
 }
 function printLableOver(){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/UpdateReturnProductPrintLabelOverAction.action';
	 var obj = {rp_id:'<%= rp_id%>'};
  	commonAjax(uri,obj);
 }
 function printLabel(p_name,fix_rpi_id,pc_id){
    var obj = {rp_id:'<%= rp_id%>',
	    		p_name:p_name,
	    		fix_rpi_id:fix_rpi_id,
	    		pc_id:pc_id
	    	  }
     var uri = "../lable_template/returned_print_one_product.html?"+jQuery.param(obj); 
     $.artDialog.open(uri , {title: '退货标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
 }
 jQuery(function($){
	$("input[name='p_name']").live("keyup",function(e){
		var node = $(this);
		if(e.which == 13){
		 	node.next("input[type='text']").select();
		}
		return false ;
	})
	$("input.quantity_row").live("keyup",function(e){
	    var node = $(this);
	    if(e.which == 13){
	 		$(".entity_row_confire_a",node.parent()).click();
		}
		return false;
	})
 })
 function imageLoad(_this){
		var node = $(_this);
		var image = new Image();
		image.src = node.attr("src");
		image.onload = function(){
			var imageHeight =  image.height ;
			var imageWidth = image.width;
			 
			var changeHeight = imageHeight * (100 /imageWidth );
			node.attr("height",changeHeight+"px");
			var liNode = (node.parent().parent().parent());
			var spanSize = $(".size_info",liNode).html(imageWidth+"x"+imageHeight);
		};
	}
 function relationProduct2(){
     var str = getSelectIds();
	if(str.length < 1){
		alert("请先勾选明细.");
		return ;
	}
	str = str.substr(1);
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/merge_return_product.html?fix_rpi_ids="+ str + "&rp_id=" + '<%= rp_id%>'; 
	$.artDialog.open(uri , {title: '合并图片商品',width:'800px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 }
 function checkItem(rpi_id , rp_id) {
    
     $.artDialog.confirm('你确认该商品需要测试？', function(){
	    
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/check_return_product_item.html?rp_id=" + '<%= rp_id%>' + "&rpi_id="+ rpi_id; 
		$.artDialog.open(uri , {title: '商品测试',width:'400px',height:'230px', lock: true,opacity: 0.3,fixed: true});
		
	 }, function(){
		
	});
}
 function refreshWindow() {window.location.reload();}
 function replyCheck(check_return_product_id){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/check_return_product_reply.html?check_return_product_id=" + check_return_product_id; 
	 $.artDialog.open(uri , {title: '商品测试结果',width:'400px',height:'360px', lock: true,opacity: 0.3,fixed: true});
 }
 function updateCheck(check_return_product_id){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/check_return_product_update.html?check_return_product_id=" + check_return_product_id; 
	 $.artDialog.open(uri , {title: '修改测试要求',width:'400px',height:'230px', lock: true,opacity: 0.3,fixed: true});
 }
 function submitHandleResult(_this,rpi_id){
     var parent = $(_this).parent();
	 var html = ($("select option:selected",parent).html() );
	 var value = $("select option:selected",parent).val();
	 if(value * 1 < 0){
		alert("请先选择处理结果!");
		return ;
	 }
	 $.artDialog.confirm("你确认该商品处理结果为"+html+"?", function(){
	     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/AddReturnProductHandleResultAction.action';
		 var obj = {rpi_id:rpi_id,handle_result:value};
	     commonAjax(uri,obj);
	 }, function(){
		
	});
 }
 //识别完成前,退件采集完成
 function pictrueRecognitionFinish(rp_id){
	var tuijia_caiji = '<%= returnProductRow.get("gather_picture_status",0)%>';
	if(tuijia_caiji * 1  < 1){
		showMessage("请先确定,退件图片采集完成.","alert");
		return ;
	}
	if('<%=ReturnProductPictureRecognitionkey.RecognitionFinish%>'*1 != '<%=picture_recognition%>'*1)
	{
	     $.artDialog.confirm('确定所有的图片关联商品完成?', function(){
		 	pictrueRecognitionFinishAjax(rp_id);
		 }, function(){
			
		});
	}
	else{alert("图片识别已完成");} 
 }
 function pictrueRecognitionFinishAjax(rp_id){
     $.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductPictureRecognitionFinishAction.action',
	    dataType:'json',
	    data:{rp_id:rp_id},
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data.flag == "noproduct"){
				alert("请先添加退件");
				return ;
		    }else if(data.flag == "notfinish" ){
				alert("图片关联商品未完成,请先关联商品或者该商品无法识别");
				return ;
			}else if(data.flag == "success"){
			  
				window.location.reload();
			}
	},
	error:function(){$.unblockUI();alert("系统错误.");}
	})
  }
 function cancel(){$.artDialog && $.artDialog.close();}
 function showProductPictrue(pid){ 
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_file_upload.html?pc_id='+pid+"&open_type=read_only"
    $.artDialog.open(uri , {title: '查看商品图片',width:'700px',height:'430px', lock: true,opacity: 0.3,fixed: true});
 }
 function returnHandle(rp_id ,rpi_id){
	 if(1 == <%=isCanRegister%>*1)
	{
	     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/handle_return_product_item.html?rp_id='+rp_id+"&rpi_id="+rpi_id;
	     $.artDialog.open(uri , {title: '退货处理',width:'700px',height:'430px', lock: true,opacity: 0.3,fixed: true,
		     ok:function(){ 
		     	this.iframe.contentWindow.updateForm && this.iframe.contentWindow.updateForm(); 
		     	return false;
		     },
		     cancel:function(){}
		    });
	}
	else
	{
		alert("不在同一个仓库，禁止登记");
	}
 }
 function pictrueGatherFinish(rp_id){
	 if('<%=gather_picture_status%>'*1 == 1)
	 {
		 alert("图片采集已完成");
	 }
	 else
	 {
		if(0 != '<%=returnProductDetails.length%>'*1)
		{	
			 $.artDialog.confirm("确定所有退件图片采集完成", function(){
			  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/GatherPictureStatusAction.action';
				 var obj = {rp_id:rp_id};
			     commonAjax(uri,obj);
		     }, function(){
			     
			});
		}
		else
		{
			alert("请添加商品图片");
		}
	 }
 }
 function updateReturnProduct(fix_rpi_id){
     var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_picture_detail.html?rp_id="+ <%= rp_id%>+"&fix_rpi_id="+fix_rpi_id; 
	 $.artDialog.open(uri , {title: '添加明细图片',width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
 }
 function returnProductItemHandleFinish(rp_id){
	 if(1 == <%=isCanRegister%>*1)
	 {
 		//首先要识别完成。
		 var reco = '<%= returnProductRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish)%>';
		 if(reco * 1 == '<%= ReturnProductPictureRecognitionkey.RecognitionNoneFinish%>' * 1){
				showMessage("请先识别完成.","alert");
				return ;
			}	
		 $.artDialog.confirm('确定所有退件处理完成?', function(){
			 	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductItemHandleFinishAction.action';
				  var data = {rp_id:'<%= rp_id%>'};
				  returnProductHandleOver(uri,data);
			}, function(){
				
			});
	 }
	 else
	 {
		alert("不在同一个仓库，禁止登记");
	 }
}
function returnProductHandleOver(uri,_data){
 $.ajax({
	    url:uri,
	    dataType:'json',
	    data:_data,
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
		    
		    if(data && data.flag === "success"){
			    window.location.reload();
			}else if(data.flag === "not_finish"){
				showMessage("请先完成每一个退件处理","alert");
			}else{
			    showMessage("系统错误.","error");
			}	 
	},
	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function modReturnItemCount(rpi_id)
{
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/mod_return_item_count.html?rp_id=<%=rp_id%>&rpi_id='+rpi_id; 
	  $.artDialog.open(uri , {title: '修改明细及子明细商品数量',width:'300px',height:'120px', lock: true,opacity: 0.3,fixed: true});
}
function modReturnSubItemCount(rpsi_id)
{
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/mod_return_sub_item_count.html?rp_id=<%=rp_id%>&rpsi_id='+rpsi_id; 
	  $.artDialog.open(uri , {title: '修改子明细商品数量',width:'300px',height:'120px', lock: true,opacity: 0.3,fixed: true});
}
function returnProductRegister(rp_id,oid,isCanRegister){
	if(1 == isCanRegister*1)
	{
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_order_handle_product.html?rp_id='+rp_id;
		 $.artDialog.open(uri , {title: '['+rp_id+']退货登记',width:'800px',height:'430px', lock: true,opacity: 0.3,fixed: true,
			    ok:function(){ 
			    	this.iframe.contentWindow.updateForm && this.iframe.contentWindow.updateForm(); 
			    	return false;
			    },
			    cancel:function(){}
			   });
	}
	else
	{
		alert("不在同一个仓库，禁止登记");
	}
}
function viewReturnProductRegister(rp_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_items_register.html?rp_id="+rp_id;
	$.artDialog.open(uri , {title: '['+rp_id+']退货登记完成',width:'800px',height:'420px', lock: true,opacity: 0.3,fixed: true});
}

 </script>
</head>
<body onload="onLoadInitZebraTable()">
	 <div  style="display:none;" id="returnProductReturnResultDiv">
		 <select name="handle_result_request">
	 	 	<option value="-1">处理要求</option>
	 	 	<%for(String str : returnRequestTypelist){ %>
	 	 		<option value="<%=str %>"><%=handleResultRequestTypeKey.getStatusById(Integer.parseInt(str)) %></option>
	 	 	<%} %>
	 	 </select>
	 </div>
	 <!-- 仓储的人上传图片。一个商品不分套装还是单个商品都是算一个商品。然后可能有多个图片 -->
 	 <!-- 首先是一个列表，列出已经上传的商品。或者是图片 -->
 	 <!-- 上面有一个添加的按钮 -->
 	 <!-- 识别完成前,退件肯定是采集完成 -->
 	 <!-- 客服识别完成过后,就不应该有添加退件图片,退件采集完成,合并图片商品 -->
 	 <!-- 识别完成才可能退货处理 -->
 	 <%boolean isRecognition = returnProductRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish) == ReturnProductPictureRecognitionkey.RecognitionFinish ;%>
 	 <div style="padding:10px;">
 	 <%
		//退货单已经完成，不能再操作
		if(ReturnProductKey.FINISHALL!=returnProductRow.get("status",0))
		{
		 	 if(!isRecognition)
		 	 { 
	%>
		 	 	<input type="button" value="添加退件图片" onclick="addPictureDetail();" class="long-button"/>
		 	 	<input type="button" value="合并图片商品" onclick="relationProduct2()" class="long-button" />
		 	 	<% String pg = (1 == gather_picture_status)?"退件采集已完成":"退件采集完成"; %>
		 	 	<input type="button" value='<%=pg %>' class="long-button" onclick="pictrueGatherFinish('<%=rp_id %>')"/>
		 	 	<% String pr = (ReturnProductPictureRecognitionkey.RecognitionFinish == picture_recognition)?"识别已完成":"识别完成";%>
		 	 	<input type="button" value='<%=pr %>' class="long-button" onclick="pictrueRecognitionFinish('<%= rp_id %>');"/>
 	 <%
 	 		}
		 	else
 	 		{%>	
<%-- 	 			<input type="button" value="退货处理完成" class="long-button" style="cursor:pointer;" onclick="returnProductItemHandleFinish('<%=rp_id %>');"/>--%>
 	 <%		} 
 	 	}
 	 %>	
 	 <!-- 如果图片识别完成 应该有按钮 这个时候弹出窗口去确定完成。可以有和服务单关联上-->
	 <!-- 图片识别完成 && 退货没有完成。出现这个按钮 -->
	<%if(returnProductRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish) == ReturnProductPictureRecognitionkey.RecognitionFinish)
	{
		int isCanRegisterRet = adminLoggerBean.getPs_id() == returnProductRow.get("ps_id",0L)?1:2;
		if(ReturnProductKey.FINISHALL == returnProductRow.get("status",0))
		{
	%>
			<input type="button" value="退货已完成" class="long-button" style="cursor:pointer;" onclick="viewReturnProductRegister('<%=returnProductRow.get("rp_id",0l) %>');">
	<%		
		}
		else
		{
	%>
			<input type="button" value="登记退货" class="long-button" style="cursor:pointer;" onclick="returnProductRegister('<%=returnProductRow.get("rp_id",0l) %>','<%=returnProductRow.get("oid",0l) %>','<%=isCanRegisterRet %>');">
	<%		
		}
	
	}
	%>
 	 
 	 
 	 
 	 </div>	
 	 <table width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0"    class="zebraTable" id="stat_table" >
		<thead>
		<tr>
			<th width="3%"  class="right-title" style="text-align:center;"><input type="checkbox"  id="checkall" onclick="selectAll(this);"/></th>
			<th width="40%" class="right-title" style="text-align:center;">实物图片</th>
	 	 
		    <th width="30%" class="right-title" style="text-align:center;">商品明细</th>
	 
		    <th width="15%"  class="right-title" style="text-align: center;">其他信息</th>
		</tr>
		</thead>
 	 	<tbody>
 	 		
 	 		
 	 		 <%if(returnProductDetails != null && returnProductDetails.length > 0 ) {%>
 	 		 	 <%for(DBRow tempRow : mergeArray) {
 	 		 		 
 	 				int is_relation_product = tempRow.get("is_relation_product",ReturnProductItemMatchKey.NOT_RELATION); 
 	 				boolean isNotMatch = (is_relation_product == ReturnProductItemMatchKey.NOT_MATCH);
 	 			
 	 				String borderColor  = isNotMatch?"silver":"#993300";
 	 		 	 %>
 	 		 	 	<tr>
 	 		 	 		<td>
 	 		 	  
 	 		 	 			<%if( is_relation_product != ReturnProductItemMatchKey.RELATION ){ %>
 	 		 	 				<input type="checkbox" class="selectSingle" onclick="selectSingle(this);" target='<%=tempRow.get("fix_rpi_id",0l) %>'/>
 	 		 	 			<%} %>
 	 		 	 			&nbsp;
 	 		 	 		</td>
 	 		 	 		<td>
 	 		 	 			<fieldset class="set" style="padding-bottom:4px;border:2px solid <%=borderColor %>;width:360px;">
	 	 		 	  			<legend>
									<span>
										<span style="font-weight:bold;color:green;"><%= returnProductItemMatchKey.getStatusById(is_relation_product) %><font color="#f60"> 数量 : <%=tempRow.get("quantity",0.0d) %></font></span>
									</span> 
								</legend>
								<!-- 显示图片在页面上让后 -->
 	 		 	  			<%
 	 		 	  				String basePath = ConfigBean.getStringValue("systenFolder") + "/upload/" +systemConfig.getStringConfigValue("file_path_return_product_detail") +"/";
 	 		 	  				DBRow[] fileRows = fileMgrZr.getFilesByFileWithIdAndFileWithType(tempRow.get("fix_rpi_id",0l), FileWithTypeKey.RETURN_PRODUCT_DETAIL);
 	 		 	  				if(fileRows != null && fileRows.length > 0 ){
 	 		 	  				 
 	 		 	  					for(DBRow fileTempRow : fileRows){
 	 		 	  							String filePath = basePath + fileTempRow.getString("file_name");
 		 	  							   %> 
 		 	  							   <span style="border:1px solid silver ;border:1px solid silver;float:left;margin-left:2px;margin-top:2px;">
 		 	  							   		<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_PRODUCT_DETAIL %>','<%=tempRow.get("fix_rpi_id",0l) %>','<%=fileTempRow.getString("file_name") %>');">
 		 	  							   			<img src="<%=filePath %>" onload="setImgWeightHeight(this,100,100)" title='<%=fileTempRow.getString("file_name") %>'/>
 		 	  							   	 	</a>
 		 	  							   	 
 		 	  							   </span>
 		 	  					<% 		 
 	 		 	  					}%>
 	 		 	  		 
 	 		 	  				<%
 	 		 	  				}else{
 	 		 	  				 %>
 	 		 	  				 	&nbsp;
 	 		 	  				 <%
 	 		 	  				}
 	 		 	  			%>
 	 		 	  			<p style="clear:both;padding-left:10px;">
			 	 		 	  				<a href="javascript:void(0)" class="normal" onclick="updateReturnProduct('<%=tempRow.get("fix_rpi_id",0l) %>')">添加图片</a>
			 	 		 	</p>
 	 		 	  			</fieldset>
 	 		 	 		</td>
 	 		 	 	 
 	 		 	 	 	<td>
 	 		 	 	 		<%
 	 		 	 	 		long pc_id = 0l;
 	 		 	 	 		String p_name = "" ;
 	 		 	 	 		if( is_relation_product == ReturnProductItemMatchKey.NOT_RELATION){ %>
 	 		 	 	 			<input type="button" class="short-button" style="cursor:pointer;" value="添加" onclick="addReturnProductDettail('<%=tempRow.get("fix_rpi_id",0l) %>',this);"/>
 	 		 	 	 		 <%}else if(is_relation_product == ReturnProductItemMatchKey.NOT_MATCH){ %>
 	 		 	 	 		 	上传图片和商品不吻合.
 	 		 	 	 		 <%}if(is_relation_product == ReturnProductItemMatchKey.RELATION ){  		 	 	 		 
 	 		 	 	 	 	}%>
 	 		 	 	 		 <%if(tempRow.get("is_relation_product",1) == ReturnProductItemMatchKey.NOT_RELATION){  %>
 	 		 	 				<input type="button" value="该图片无法识别" onclick="pictrueNotMatch('<%= tempRow.get("fix_rpi_id",0l)%>');" class="long-button"/><br /><br />
 	 		 	 			<%} %>
 	 		 	 	 	</td>
 	 	 
 	 		 	 		<td style="padding-top:5px;padding-bottom:5px;text-align:left;">
 	 		 	 			<input type="button" class="short-button" style="cursor:pointer;" value="打签" onclick="printLabel('<%=p_name %>','<%=tempRow.get("fix_rpi_id",0l) %>','<%=pc_id %>');"/><br /><br />
 	 		 	 			 
 	 		 	 			
 	 		 	 			<%if(tempRow.get("is_relation_product",1) != ReturnProductItemMatchKey.RELATION){ %>
 	 		 	 			 	<input type="button" value="删除该明细" onclick="deleteReturnProductItemFix('<%= tempRow.get("fix_rpi_id",0l)%>');" class="short-button"/><br /><br />
 	 		 	 			<%} %>
 	 		 	 		</td>
 	 		 	 	</tr>
 	 		 	 	
 	 		<%
 	 		 	 }
 	 		 	 %>
 	 	<% 
 	 			Set<Long>	set = map.keySet();
 	 			Iterator<Long> it = set.iterator();
 	 			while(it.hasNext()){
 	 				long rpiId = it.next();
 	 				DBRow item = returnProductItemsFixMgrZr.getReturnProductItemByRpiId(rpiId);
 	 				boolean isReturnHandleFinish = item.get("is_return_handle",0) == 1; 
 	 				List<DBRow> arrays = map.get(rpiId);
 	 				if(arrays != null && arrays.size() > 0){
 	 						int runIndex = 0 ;
 	 						StringBuffer fix_ids = new StringBuffer();
 	 					%>
 	 			 		<tr>
 	 			 			<td>&nbsp;</td>
 	 			 			<td>
 	 			 				<%for(DBRow tempRow : arrays) {
 	 			 					fix_ids.append(",").append(tempRow.get("fix_rpi_id",0l));
 	 			 					int is_relation_product = tempRow.get("is_relation_product",ReturnProductItemMatchKey.NOT_RELATION); 
 	 			 	 				boolean isNotMatch = (is_relation_product == ReturnProductItemMatchKey.NOT_MATCH);
 	 			 	 				String borderColor  = isNotMatch?"silver":"#993300";
 	 			 				%>
 	 			 					<fieldset class="set" style="padding-bottom:4px;border:2px solid <%=borderColor %>;width:360px;">
			 	 		 	  			<legend>
											<span>
												<span style="font-weight:bold;color:green;"><%= returnProductItemMatchKey.getStatusById(is_relation_product) %> <font color="#f60">数量: <%=tempRow.get("quantity",0.0d) %></font></span>
												&nbsp;&nbsp;<span><%=(isReturnHandleFinish?"退件处理完成":"退件处理未完成") %></span>
											</span> 
										</legend>
										<%
			 	 		 	  				String basePath = ConfigBean.getStringValue("systenFolder") + "/upload/" +systemConfig.getStringConfigValue("file_path_return_product_detail") +"/";
			 	 		 	  				DBRow[] fileRows = fileMgrZr.getFilesByFileWithIdAndFileWithType(tempRow.get("fix_rpi_id",0l), FileWithTypeKey.RETURN_PRODUCT_DETAIL);
			 	 		 	  				if(fileRows != null && fileRows.length > 0 ){
			 	 		 	  				 
			 	 		 	  					for(DBRow fileTempRow : fileRows){
			 	 		 	  							String filePath = basePath + fileTempRow.getString("file_name");
			 		 	  							   %> 
			 		 	  							   <span style="border:1px solid silver ;border:1px solid silver;float:left;margin-left:2px;margin-top:2px;">
			 		 	  							   		<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_PRODUCT_DETAIL %>','<%=tempRow.get("fix_rpi_id",0l) %>','<%=fileTempRow.getString("file_name") %>');">
			 		 	  							   			<img src="<%=filePath %>" onload="setImgWeightHeight(this,100,100)" title='<%=fileTempRow.getString("file_name") %>'/>
			 		 	  							   	 	</a>
			 		 	  							   </span>		
			 		 	  						<% 		 
			 	 		 	  					}%>
			 	 		 	  				<%
			 	 		 	  				}else{
			 	 		 	  				 %>
			 	 		 	  				 	&nbsp;
			 	 		 	  				 <%
			 	 		 	  				}
			 	 		 	  			%>
			 	 		 	  			<p style="clear:both;padding-left:10px;">
			 	 		 	  				<a href="javascript:void(0)" class="normal" onclick="updateReturnProduct('<%=tempRow.get("fix_rpi_id",0l) %>')">添加图片</a>
			 	 		 	  			</p>
									</fieldset>
 	 			 				<%} %>
 	 			 			</td>
 	 			 			 
 	 			 			<td style="padding-top:10px;">
 	 			 				<%
	 		 	 	 				
 	 			 					DBRow[] checkItems = null ;
 	 			 					if(item.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){
 	 			 						 checkItems = checkReturnProductItemMgrZr.getCheckReturnProductItemByRpIId(item.get("rpi_id",0l));
 	 			 					}
 	 			 				%>
 	 			 				<p >
 	 		 	 	 		 		 	<span  style="font-weight:bold;"><a href="javascript:void(0)" onclick="showProductPictrue('<%= item.get("pid",0l)%>')"><%= item.getString("p_name") %></a></span> <span style="color:blue">x  <%= item.get("quantity",0.0)%>&nbsp;&nbsp;<%=item.getString("unit_name") %></span>
 	 		 	 	 		 		 	<%
 	 		 	 	 		 		 		if(ReturnProductKey.FINISHALL != returnProductRow.get("status",0))
 	 		 	 	 		 		 		{
 	 		 	 	 		 		 	%>
 	 		 	 	 		 		 	&nbsp;<a href="javascript:void(0);" onclick="modReturnItemCount('<%=item.get("rpi_id",0l)%>')"><img src="../imgs/application_edit.png" title="修改明细及子明细数量" width="14" height="14" border="0"></a>
 	 		 	 	 		 		 	<%
 	 		 	 	 		 		 		}
 	 		 	 	 		 		 	%>
  	 		 	 	 		 		<!-- 如果改商品已经退货处理完成,那么没有删除和添加商品测试了 -->
  	 		 	 	 		 			 <%if(item.get("is_return_handle",0) != 1){ %>
											<a href="javascript:void(0)" title="删除商品明细" onclick="deleteItem('<%=item.get("rpi_id",0l) %>','<%=fix_ids.substring(1) %>')">删除</a>
	 	 		 	 	 		  			<%if(checkItems == null || checkItems.length < 1){ %>
	 	 		 	 	 		  			&nbsp;&nbsp;<a href="javascript:void(0)" title="商品测试" onclick="checkItem('<%=item.get("rpi_id",0l) %>','<%= rp_id %>')">商品测试</a>
	 	 		 	 	 		 			<%} %>
	 	 		 	 	 		 		<%} %>
 	 		 	 	 		 		 	<!-- 查询是否是套装-->
 	 		 	 	 		 		 	<%if(item.get("product_type",0) == 2){
 	 		 	 	 		 		 		DBRow[] subItems = returnProductSubItemMgrZr.getReturnProductSubItemsByRpiId(rpiId);
 	 		 	 	 		 		 	 	for(DBRow subItem : subItems){
 	 		 	 	 		 		 	 %>
 	 		 	 	 		 		 			<p>
 	 		 	 	 		 		 				├<%=subItem.getString("p_name") %> x &nbsp;<%=subItem.getString("quantity") %> &nbsp;<%=subItem.getString("unit_name") %>
 	 		 	 	 		 		 				<%
			 	 		 	 	 		 		 		if(ReturnProductKey.FINISHALL != returnProductRow.get("status",0))
			 	 		 	 	 		 		 		{
			 	 		 	 	 		 		 	%>
			 	 		 	 	 		 		 	&nbsp;<a href="javascript:void(0);" onclick="modReturnSubItemCount('<%=subItem.get("rpsi_id",0l)%>')"><img src="../imgs/application_edit.png" title="修改子明细数量" width="14" height="14" border="0"></a>
			 	 		 	 	 		 		 	<%
			 	 		 	 	 		 		 		}
			 	 		 	 	 		 		 	%>
 	 		 	 	 		 		 				
 	 		 	 	 		 		 			</p>	
 	 		 	 	 		 		 	<%}}%>
 	 		 	 	 		 		 	
 	 		 	 	 		 		 	
 	 		 	 	 		  </p>
 	 		 	 	 		  <!-- 显示需要测试的东西 -->
 	 		 	 	 		  <% 
  	 		 	 	 		   		if(checkItems != null && checkItems.length > 0 ){
 	 		 	 	 		  			for(DBRow checkRow : checkItems){
 	 		 	 	 		  %>	
 	 		 	 	 		  			<div style="" class="checkDiv">
 	 		 	 	 		  				<span style="color:#f60;">Q:<%=checkRow.getString("create_employee_name") %></span> <span>[<%= tDate.getFormateTime(checkRow.getString("create_date") )%>]</span>
 	 		 	 	 		  				<p style="margin-top:2px;margin-bottom:3px;">
 	 		 	 	 		  					<%= checkRow.getString("check_product_context") %>
 	 		 	 	 		  					<a href="javascript:void(0)" title="编辑" onclick="updateCheck('<%=checkRow.get("check_return_product_id",0l) %>');"><img width="12" height="12" border="0" alt="编辑测试" src="../imgs/modify.gif"></a>
 	 		 	 	 		  					<a href="javascript:void(0)" title="回答" onclick="replyCheck('<%=checkRow.get("check_return_product_id",0l) %>');"><img width="12" height="12" border="0" alt="回答测试" src="../imgs/help.gif"></a>
 	 		 	 	 		  					
 	 		 	 	 		  				</p>
 	 		 	 	 		  				<%if(checkRow.getString("result_context").length() > 0){ %>
 	 		 	 	 		  				<span style="color:#f60;">A:<%=checkRow.getString("handle_employ_name") %></span> <span>[<%= tDate.getFormateTime(checkRow.getString("handle_date") )%>]</span>
 	 		 	 	 		  				<p style="margin-top:2px;">
 	 		 	 	 		  				  	<%=checkRow.getString("result_context") %>
 	 		 	 	 		  				</p>
 	 		 	 	 		  				<%} %>
 	 		 	 	 		  			</div>
 	 			 						
 	 			 			  <%		} 
 	 		 	 	 		   		}
 	 			 			 %>
 	 			 			 <!-- 处理结果 -->
 	 			 			 <p>
 	 			 			 	处理要求: <font style="font-weight:bold;"><%=handleResultRequestTypeKey.getStatusById(item.get("handle_result_request",0)) %></font><br />
  	 			 			 </p>
  	 			 			<p>
  	 			 			 	退件处理:<font style="font-weight:bold;"><%= item.get("is_return_handle",0)== 0? "未完成":"完成"%></font><br /> 
  	 			 			 </p>
 	 			 			  
 	 			 			</td>
 	 			 			<td>
 	 			 				<input type="button" value="打签"  class="short-button" style="cursor:pointer;" onclick="printLabel('<%=item.getString("p_name") %>','<%= item.get("rpi_id",0l)%>','<%=item.get("pid",0l) %>')"/> <br /> 	<br />
 	 			 				<%if(isRecognition && ReturnProductKey.FINISHALL!=returnProductRow.get("status",0)){ %>
<%-- 	 			 					<input type="button" value="退货处理" class="short-button" onclick="returnHandle('<%=rp_id %>','<%=item.get("rpi_id",0l) %>')"/>--%>
 	 			 				<%} %>
 	 			 			</td>
 	 			 		</tr>
 	 						
 	 					<% 	 
 	 				}
 	 			}
 	 		%>
 	 		 	 <% 
 	 		 }else{ %>
 	 			<td colspan="5" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 	 		<%} %>
 	 	</tbody>
 	 	 
 	 </table>
 	 <script type="text/javascript">
 	 function initPname(){
 	 	var pName = $(".p_name");
 	 	pName.each(function(){
 		 	addAutoComplete($(this),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");

 	 	})
 	 }
 	initPname();
 	 </script>
 	 <script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
</body>
</html>