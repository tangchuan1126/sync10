<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.StorageReturnProductStatusKey"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	long rp_id = StringUtil.getLong(request,"rp_id");
	DBRow returnRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
	int status = returnRow.get("status",0);
	//DBRow[] returnProductItems = returnProductItemsFixMgrZr.getReturnItemsByRpId(rp_id);
	DBRow[] returnProductItems = returnProductOrderMgrZyj.getReturnProductItemByReceiveHandleInfo(-1, rp_id);
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退货登记</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<!-- statebox -->
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css">
	td.left{width:69%;font-family:Arial;font-weight:bold};
	td.right_{width:31%;font-family:Arial;font-weight:bold};
	 th.th_head{background-color:#eeeeee;color:red;}
	  div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .setDisplay{border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;margin-top:10px;}
</style>
<script type="text/javascript">


function cancel(){$.artDialog && $.artDialog.close();}
function updateForm(){
 //StorageReturnProductStatusKey.RETURN_PRODUCT_HANDLE_FINISH
 if('<%=ReturnProductKey.FINISH%>'*1 == '<%=status%>'*1)
 {
   var _data = {rp_id:'<%= rp_id%>'}
   $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductRegisterAction.action',
		dataType:'json',
		data:_data,
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
		    if(data && data.flag == "success"){
				cancel();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();	
		   	}else{
		   		 showMessage("系统错误.","error");
			}
		},
		error:function(){
		    $.unblockUI();
		    showMessage("系统错误.","error");
		}
   })
 }
 else
 {
	alert("请先将退货处理完成");
 }
}
 
</script>
</head>

<body>
<form id="myform">
<div style="margin-left:20px;width:100%;border:0px solid silver;width:550px;margin-bottom: 20px;">
	<h4 style="text-align:center;">退货单号:<%=rp_id %></h4>	
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">主商品信息</legend>
			<table width="95%" cellspacing="0" cellpadding="2" border="0" align="center">
				<tbody>
					<%if(returnProductItems != null && returnProductItems.length > 0){ 
						for(DBRow row :returnProductItems ){
					%>
						<tr>
							<td width="69%" class="left"><%=row.getString("p_name") %></td>
							<td width="31%" style="font-weight:bold;"><%=row.get("quantity",0.0d) %>&nbsp;<%=row.getString("unit_name") %></td>
						</tr>
					<%}}else{%>
						<tr>
							<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
						</tr>
					<%} %>
				</tbody>
			</table>
	</fieldset>	
	
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;margin-top:10px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">散件商品信息</legend>
			  <%if(returnProductItems != null && returnProductItems.length > 0){ 
					for(DBRow row :returnProductItems ){
				%>
					 <table style="width:98%;">
					 	<thead>
					 		<th style="background:#eeeeee;line-height:22px;height:22px;width:41%;">商品名</th>
					 		<th style="background:#eeeeee;width:10%;">数量</th>
					 		<th style="background:#eeeeee;width:10%;">单位</th>
					 		<th style="background:#eeeeee;width:13%;">完好</th>
					 		<th style="background:#eeeeee;width:13%;">功能残损</th>
					 		<th style="background:#eeeeee;width:13%;">外观残损</th>
					 	</thead>
					 	<tbody>
					 		<%DBRow[] subItems = returnProductSubItemMgrZr.getReturnProductSubItemsByRpiId(row.get("rpi_id",0l)); %>
					 		<%if(subItems != null && subItems.length > 0 ){ %>
					 		<%	for(DBRow subItem : subItems){ %>
					 				<tr>
					 					<td width="200px;"><%= subItem.getString("p_name") %>
					 					<input type="hidden" name='<%=subItem.get("rpsi_id",0l) %>_pid' value='<%=subItem.get("pid",0l) %>'/>	
					 					<input class="ids" type="hidden" name="rpsi_id" value='<%=subItem.get("rpsi_id",0l)  %>'></td>
					 					<td style="text-align:center; "><input type="hidden" value='<%=subItem.get("quantity",0.0d) %>' name='total_quantity_<%=subItem.get("rpsi_id",0l)  %>'><%= subItem.get("quantity",0.0d) %></td>
					 					<td style="text-align:center; "><%= subItem.getString("unit_name") %></td>
					 					<td style="text-align:center; "><%=subItem.get("quality_nor",0.0d) %></td>
					 					<td style='text-align:center;<%=subItem.get("quality_damage",0.0d)>0?"color:#FF0000;":"" %> '><%=subItem.get("quality_damage",0.0d) %></td>
					 					<td style='text-align:center;<%=subItem.get("quality_package_damage",0.0d)>0?"color:#FF0000;":"" %>  '><%=subItem.get("quality_package_damage",0.0d) %></td>
					 				</tr>
					 			
					 		<%	} %>
					 		<%} %>
					 	</tbody>
					 </table>
					
				<%}}else{%>
						  
				<%} %>
	</fieldset>	
	<%
		if(!"".equals(returnRow.getString("return_difference_note")))
		{
	%>
		<fieldset class="setDisplay">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">退货异常数据备注</legend>
	        <%=returnRow.getString("return_difference_note") %>
		</fieldset>
	<%
		}
	%>
</div>

</form>
<div class="buttonDiv" style="width:100%;margin-bottom:5px;position:fixed;left:0px;bottom:-5px;">
		 <%
		 	if(ReturnProductKey.FINISHALL!=returnRow.get("status",0))
		 	{
		 %>
		 <input id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil" type="button" value="提交" onclick="updateForm();">
		 <button id="jqi_state0_button取消" class="jqidefaultbutton specl" onclick="cancel();" name="jqi_state0_button取消" value="n">取消</button>
		 <%		
		 	}
		 %>
		
	</div>
	<script type="text/javascript">
	//遮罩
	$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
	</script>
</body>
</html>