<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="returnProductLogTypeKey" class="com.cwc.app.key.ReturnProductLogTypeKey"></jsp:useBean>
<%@ include file="../../include.jsp"%> 
<%
	long rp_id = StringUtil.getLong(request,"rp_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>退货单号：<%=rp_id %></title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../common.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
</head>

<body >
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:10px;">
  	  <table width="98%" height="100%"  border="0" cellpadding="0" cellspacing="0">
  	    <tr>
  	      <td  align="left" valign="top">
  	        <div class="demo">
  	          <div id="tabs">
  	            <ul>
	  	              <li><a href="return_order_logs_tright.html?rp_id=<%=rp_id %>">全部日志<span> </span></a></li>
	  	              <%
	  	              	ArrayList<String> returnProductLogTypeKeys = returnProductLogTypeKey.getReturnProductLogKeys();
	  	              	for(int i = 0; i < returnProductLogTypeKeys.size(); i ++)
	  	              	{
	  	              %>
	  	               <li><a href='return_order_logs_tright.html?rp_id=<%=rp_id %>&follow_type=<%=returnProductLogTypeKeys.get(i) %>'><%=returnProductLogTypeKey.getReturnProductLogTypeName(returnProductLogTypeKeys.get(i)) %><span> </span></a></li>
	  	              <%		
	  	              	}
	  	              %>
					 
			    </ul>
			  </div>
		    </div>
		 </td>
	    </tr>
      </table>
  </td></tr>
 <tr>
 	<td align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeWindow()">	</td>
 </tr>
</table>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: { expires: 5000 } ,
	load: function(event, ui) {onLoadInitZebraTable();}	
});
</script>
<script type="text/javascript">
function closeWindow(){
	$.artDialog && $.artDialog.close();
}
</script>
</body>
</html>

