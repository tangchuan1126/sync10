<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ReturnProductLogTypeKey"%>
<jsp:useBean id="returnProductLogTypeKey" class="com.cwc.app.key.ReturnProductLogTypeKey"></jsp:useBean>
<%@ include file="../../include.jsp"%> 
<%
	long rp_id = StringUtil.getLong(request,"rp_id");
	int follow_type = StringUtil.getInt(request,"follow_type");
	TDate tdate = new TDate();
	DBRow [] rows = returnProductOrderMgrZyj.getReturnProductLogsByIdTypeStatus(rp_id, follow_type, 0);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>退货单日志</title>
</head>

<body >
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">操作员</th>
        <th width="35%"  class="left-title " style="vertical-align: center;text-align: center;">记录备注</th>
        <th width="13%"  class="right-title " style="vertical-align: center;text-align: center;">记录时间</th>
        <th width="10%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
  	</tr>
  	<% 
  		for(int i=0;i<rows.length;i++)
  		{
  	%>
  		<tr align="center" valign="middle">
		    <td height="30"><%=adminMgrLL.getAdminById(rows[i].getString("operator")).getString("employe_name") %></td>
		    <td align="left">
		    	<%
		    		out.print(rows[i].getString("content"));
		    	%>	      
		    </td>
		    <td><%=tdate.getFormateTime(rows[i].getString("operate_time")) %></td>
		    <td><%=returnProductLogTypeKey.getReturnProductLogTypeName(rows[i].get("follow_type",0)) %></td>
	  	</tr>
  	<%
  		}
  	%>
</table>
</body>
</html>



