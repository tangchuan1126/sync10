<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemMatchKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemMatchKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductPictureRecognitionkey"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>

<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 
<title>退货部件详细</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<!-- table -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script type="text/javascript" src="../js/setImgWeightHeight.js"></script>
<style type="text/css">
	.right-title{height:22px;}
	div.checkDiv{font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:90%;margin-top:3px;margin-bottom:5px;border-bottom:1px dashed  silver;border-top:1px dashed  silver;}
	a.normal{font-weight:normal;text-decoration:none;}
	a.normal:hover{text-decoration:underline ;}
</style>
<%
	long rp_id = StringUtil.getLong(request,"rp_id");
	DBRow returnProductRow = returnProductMgrZr.getReturnProductByRpId(rp_id);
	DBRow[] rows = returnProductItemsFixMgrZr.getReturnItemsByRpId(rp_id);
	ReturnProductItemMatchKey returnProductItemMatchKey = new ReturnProductItemMatchKey();
    ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
	DBRow[] fixItemRows = returnProductItemsFixMgrZr.getReturnProductItmesFix(rp_id);
 	 
	List<DBRow> mergeArray = new ArrayList<DBRow>();
	Map<Long , List<DBRow>> map = new HashMap<Long,List<DBRow>>();
	for(DBRow fixRows : fixItemRows ){
		 long tempRpiId = fixRows.get("rpi_id",0l);
		 if(tempRpiId != 0l){
	 List<DBRow> tempList = map.get(tempRpiId);
	 if(tempList == null){
		 tempList = new ArrayList<DBRow>();
		 map.put(tempRpiId,tempList);
	 }
	 tempList.add(fixRows);
	  
		 }else{
	 mergeArray.add(fixRows);
		 }
	 } 
	AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
	int isCanRegister = adminLoggerBean.getPs_id() == returnProductRow.get("ps_id",0L)?1:2;
	 int gather_picture_status = returnProductRow.get("gather_picture_status",0);//图片采集
	 int picture_recognition = returnProductRow.get("picture_recognition",0);//图片识别
%>
<script type="text/javascript">
function addPictureDetail(rpi_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_picture_detail.html?rp_id="+ <%= rp_id%>+"&return_product_item_is_exits=yes&rpi_id="+rpi_id; 
	 $.artDialog.open(uri , {title: '新增图片',width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
}
function updatePictureDetail(rpi_id , fix_rpi_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_picture_detail.html?rp_id="+ <%= rp_id%>+"&return_product_item_is_exits=yes&rpi_id="+rpi_id + "&fix_rpi_id="+fix_rpi_id; 
	$.artDialog.open(uri , {title: '新增图片',width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow(){window.location.reload();}
function pictrueRecognitionFinish(rp_id){
	var tuijia_caiji = '<%=gather_picture_status%>';
	if(tuijia_caiji * 1  < 1){
		showMessage("请先确定,退件图片采集完成.","alert");
		return ;
	}
	if('<%=ReturnProductPictureRecognitionkey.RecognitionFinish%>'*1 != '<%=picture_recognition%>'*1)
	{
	    $.artDialog.confirm('确定所有的图片关联商品完成?', function(){
		 	pictrueRecognitionFinishAjax(rp_id);
		 }, function(){
			
		});
	}
	else{alert("图片识别已完成");}     
}
function pictrueGatherFinish(rp_id){
	if('<%=gather_picture_status%>'*1 == 1)
	{
		alert("图片采集已完成");
	}
	else
	{
		$.artDialog.confirm("确定所有退件图片采集完成", function(){
		  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/GatherPictureStatusAction.action';
			 var obj = {rp_id:rp_id};
		     commonAjax(uri,obj);
	    }, function(){
		     
		});
	}
}
function pictrueRecognitionFinishAjax(rp_id){
    $.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductPictureRecognitionFinishAction.action',
	    dataType:'json',
	    data:{rp_id:rp_id},
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data.flag == "noproduct"){
				alert("请先添加退件");
				return ;
		    }else if(data.flag == "notfinish" ){
				alert("实物图片关联未完成,请先上关联商品或者确定该图片无法识别.");
				return ;
			}else if(data.flag == "success"){
			     window.location.reload();	
 			}
	},
	error:function(){$.unblockUI();alert("系统错误.");}
	})
 }
function cancel(){$.artDialog && $.artDialog.close();}
function deleteReturnProductItemFix(fix_rpi_id){
    $.artDialog.confirm('确定删除改明细图片?', function(){
		ajaxDeleteReturnProductItemFix(fix_rpi_id);
	 }, function(){
		
	});
}
function ajaxDeleteReturnProductItemFix(fix_rpi_id){
    $.ajax({
	    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/DeleteReturnProductItemFixAction.action',
	    dataType:'json',
	    data:{fix_rpi_id:fix_rpi_id},
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
			    window.location.reload();
			}	 
	},
	error:function(){$.unblockUI();alert("系统错误.");}
	})
}
function printLabel(p_name,fix_rpi_id,pc_id){
    var obj = {rp_id:'<%= rp_id%>',
	    		p_name:p_name,
	    		fix_rpi_id:fix_rpi_id,
	    		pc_id:pc_id
	    	  }
     var uri = "../lable_template/returned_print_one_product.html?"+jQuery.param(obj); 
     $.artDialog.open(uri , {title: '退货标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
 }
// 这个按钮标识了,在先创建退货单的时候.如果没有对应的图片。
function returnProductNoPictrue(rpi_id){
    $.artDialog.confirm('确定未收到退件?', function(){
		  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductItemZrNoPictureAction.action';
		  var data = {rpi_id:rpi_id,rp_id:'<%= rp_id%>'};
		  commonAjax(uri,data);
	 }, function(){
		
	});
    
}

function commonAjax(uri,_data){
    $.ajax({
	    url:uri,
	    dataType:'json',
	    data:_data,
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
		    if(data && data.flag === "success"){
			    window.location.reload();
			}	 
	},
	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function replyCheck(check_return_product_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/check_return_product_reply.html?check_return_product_id=" + check_return_product_id; 
	 $.artDialog.open(uri , {title: '商品测试结果',width:'400px',height:'360px', lock: true,opacity: 0.3,fixed: true});
}
function returnHandle(rp_id ,rpi_id,is_receive_item){
	if(1 == <%=isCanRegister%>*1)
	{
	    if(is_receive_item * 1 == -1){
			showMessage("没有收到该退件,所以无法处理.","alert");
			return ;
		}
		
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/handle_return_product_item.html?rp_id='+rp_id+"&rpi_id="+rpi_id;
	    $.artDialog.open(uri , {title: '退货处理',width:'700px',height:'430px', lock: true,opacity: 0.3,fixed: true,
		     ok:function(){ 
		     	this.iframe.contentWindow.updateForm && this.iframe.contentWindow.updateForm(); 
		     	return false;
		     },
		     cancel:function(){}
		    });
	}
	else
	{
		alert("不在同一个仓库，禁止登记");
	}
}
 
function updateReturnProduct(fix_rpi_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_picture_detail.html?rp_id="+ <%= rp_id%>+"&fix_rpi_id="+fix_rpi_id; 
	 $.artDialog.open(uri , {title: '添加明细图片',width:'600px',height:'330px', lock: true,opacity: 0.3,fixed: true});
}
function pictrueNotMatch(fix_rpi_id){
    if (confirm("确认标记该图片内容?")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/SetReturnProductFixItemNotMatchAction.action' +"?fix_rpi_id="+fix_rpi_id,
			dataType:'json',
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}
			},
			error:function(){alert("更新失败,请稍后再试.");return ;}
		})
	}
}
//显示在线图片
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_return_product_detail")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
function relationReturnProductItem(fix_rpi_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/relation_return_product_item.html?rp_id="+ <%= rp_id%>+"&fix_rpi_id="+fix_rpi_id; 
	$.artDialog.open(uri , {title: '添加明细图片',width:'500px',height:'460px', lock: true,opacity: 0.3,fixed: true});
}
function refreshWindow(){window.location.reload();}

function returnProductItemHandleFinish(rp_id){
	if(1 == <%=isCanRegister%>*1)
	{
    	//首先要识别完成。
	    var reco = '<%= returnProductRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish)%>';
	    if(reco * 1 == '<%= ReturnProductPictureRecognitionkey.RecognitionNoneFinish%>' * 1){
			showMessage("请先识别完成.","alert");
			return ;
		}	
	    $.artDialog.confirm('确定所有退件处理完成?', function(){
		 	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/ReturnProductItemHandleFinishAction.action';
			  var data = {rp_id:'<%= rp_id%>'};
			  returnProductHandleOver(uri,data);
		}, function(){
			
		});
	}
	else
	{
		alert("不在同一个仓库，禁止登记");
	}
}
function returnProductHandleOver(uri,_data){
    $.ajax({
	    url:uri,
	    dataType:'json',
	    data:_data,
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
		    
		    if(data && data.flag === "success"){
			    window.location.reload();
			}else if(data.flag === "not_finish"){
				showMessage("请先完成每一个退件处理","alert");
			}else{
			    showMessage("系统错误.","error");
			}	 
	},
	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function modReturnItemCount(rpi_id, pc_count,p_name)
{
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/mod_return_item_count.html?rp_id=<%=rp_id%>&rpi_id='+rpi_id+'&pc_count='+pc_count+'&p_name='+p_name; 
	  $.artDialog.open(uri , {title: '修改明细及子明细商品数量',width:'300px',height:'120px', lock: true,opacity: 0.3,fixed: true});
}
function modReturnSubItemCount(rpsi_id)
{
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/mod_return_sub_item_count.html?rp_id=<%=rp_id%>&rpsi_id='+rpsi_id; 
	  $.artDialog.open(uri , {title: '修改子明细商品数量',width:'300px',height:'120px', lock: true,opacity: 0.3,fixed: true});
}
function returnProductRegister(rp_id,oid,isCanRegister){
	if(1 == isCanRegister*1)
	{
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_order_handle_product.html?rp_id='+rp_id;
		 $.artDialog.open(uri , {title:'['+rp_id+']退货登记',width:'800px',height:'430px', lock: true,opacity: 0.3,fixed: true,
		     ok:function(){ 
		     	this.iframe.contentWindow.updateForm && this.iframe.contentWindow.updateForm(); 
		     	return false;
		     },
		     cancel:function(){}
		    });
	}
	else
	{
		alert("不在同一个仓库，禁止登记");
	}
}
function viewReturnProductRegister(rp_id)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_items_register.html?rp_id="+rp_id;
	$.artDialog.open(uri , {title: '['+rp_id+']退货登记完成',width:'800px',height:'420px', lock: true,opacity: 0.3,fixed: true});
}
</script>
</head>
<body onload="onLoadInitZebraTable()">
	<div style="padding:10px;">
		<%
			//退货单已经完成，不能再操作
			if(ReturnProductKey.FINISHALL!=returnProductRow.get("status",0))
			{
		%>
				<input type="button" value="添加退件图片" onclick="addPictureDetail();" class="long-button"/>
				<% String pg = (1 == gather_picture_status)?"退件采集已完成":"退件采集完成"; %>
		 	 	<input type="button" value='<%=pg %>' class="long-button" onclick="pictrueGatherFinish('<%=rp_id %>')"/>
		 	 	<% String pr = (ReturnProductPictureRecognitionkey.RecognitionFinish == picture_recognition)?"识别已完成":"识别完成";%>
		 	 	<input type="button" value='<%=pr %>' class="long-button" onclick="pictrueRecognitionFinish('<%= rp_id %>');"/>
<%--				<input type="button" value="退件处理完成" class="long-button" onclick="returnProductItemHandleFinish('<%=rp_id %>')"/>--%>
		<%
			}
		%>
		 <!-- 如果图片识别完成 应该有按钮 这个时候弹出窗口去确定完成。可以有和服务单关联上-->
	 <!-- 图片识别完成 && 退货没有完成。出现这个按钮 -->
	<%if(returnProductRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish) == ReturnProductPictureRecognitionkey.RecognitionFinish)
	{
		int isCanRegisterRet = adminLoggerBean.getPs_id() == returnProductRow.get("ps_id",0L)?1:2;
		if(ReturnProductKey.FINISHALL == returnProductRow.get("status",0))
		{
	%>
			<input type="button" value="退货已完成" class="long-button" style="cursor:pointer;" onclick="viewReturnProductRegister('<%=returnProductRow.get("rp_id",0l) %>');">
	<%		
		}
		else
		{
	%>
			<input type="button" value="登记退货" class="long-button" style="cursor:pointer;" onclick="returnProductRegister('<%=returnProductRow.get("rp_id",0l) %>','<%=returnProductRow.get("oid",0l) %>','<%=isCanRegisterRet %>');">
	<%		
		}
	
	}
	%>
 	 
		
	</div>
	 <table width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0"    class="zebraTable" id="stat_table" >
		<thead>
		<tr>
			<th width="3%"  class="right-title" style="text-align:center;"></th>
			<th width="40%" class="right-title" style="text-align:center;">实物图片</th>
	 	 
		    <th width="30%" class="right-title" style="text-align:center;">商品明细</th>
	 
		    <th width="15%"  class="right-title" style="text-align: center;">其他信息</th>
		</tr>
		</thead>
		<tbody>
			<%if((rows != null && rows.length > 0 ) || (fixItemRows != null && fixItemRows.length > 0 )){ %>
				<!-- 遍历如果有重新上传了,仓库不认识的商品(有RMA 但是有商品仓库不认识) -->
				<%for(DBRow tempRow : fixItemRows){
					//无退货明细
					if(tempRow.get("rpi_id",0l) == 0l)
					{ 
						boolean isReturnHandleFinish = false;
						int is_relation_product = tempRow.get("is_relation_product",ReturnProductItemMatchKey.NOT_RELATION); 
	 	 				boolean isNotMatch = (is_relation_product == ReturnProductItemMatchKey.NOT_MATCH);
	 	 			
	 	 				String borderColor  = isNotMatch?"silver":"#993300";
	 	 			%>
						<tr>
							<td></td>
							<td>
									<fieldset class="set" style="padding-bottom:4px;border:2px solid <%=borderColor %>;width:360px;">
				 	 		 	  			<legend>
												<span>
													<span style="font-weight:bold;color:green;"><%= returnProductItemMatchKey.getStatusById(is_relation_product) %><font color="#f60"> 数量 : <%=tempRow.get("quantity",0.0d) %></font></span>
													&nbsp;&nbsp;<span><%=(isReturnHandleFinish?"退件处理完成":"退件处理未完成") %></span>		
												</span> 
											</legend>
												<%
						 	 		 	  				String basePath = ConfigBean.getStringValue("systenFolder") + "/upload/" +systemConfig.getStringConfigValue("file_path_return_product_detail") +"/";
						 	 		 	  				DBRow[] fileRows = fileMgrZr.getFilesByFileWithIdAndFileWithType(tempRow.get("fix_rpi_id",0l), FileWithTypeKey.RETURN_PRODUCT_DETAIL);
						 	 		 	  				if(fileRows != null && fileRows.length > 0 ){		 	 		 	  				 
			 	 		 	  								for(DBRow fileTempRow : fileRows){
			 	 		 	  								String filePath = basePath + fileTempRow.getString("file_name");
			 		 	  							   %> 
			 		 	  							   <span style="border:1px solid silver ;border:1px solid silver;float:left;margin-left:2px;margin-top:2px;">
			 		 	  							   		<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_PRODUCT_DETAIL %>','<%=tempRow.get("fix_rpi_id",0l) %>','<%=fileTempRow.getString("file_name") %>');">
			 		 	  							   			<img src="<%=filePath %>" onload="setImgWeightHeight(this,100,100)" title='<%=fileTempRow.getString("file_name") %>'/>
			 		 	  							   	 	</a>
			 		 	  							   </span>		
			 		 	  						<% 		 
			 	 		 	  					}%>
			 	 		 	  				<%
			 	 		 	  				}else{
			 	 		 	  				 %>
			 	 		 	  				 	&nbsp;
			 	 		 	  				 <%
			 	 		 	  				}
			 	 		 	  			%>
										<p style="clear:both;padding-left:10px;">
			 	 		 	  						<a href="javascript:void(0)" class="normal" onclick="updateReturnProduct('<%=tempRow.get("fix_rpi_id",0l) %>')">添加退件图片</a>
			 	 		 	  			</p>
									</fieldset>
							</td>
							<td>
								<%if(isNotMatch){ %>
									上传图片无法识别.
								<%} else{%>
									<%if(tempRow.get("rpi_id",0l) == 0l){ %>
										<input type="button" class="long-button" value="添加商品" onclick="relationReturnProductItem('<%=tempRow.get("fix_rpi_id",0l) %>');"/> 
										<input type="button" class="long-button" value="该图片无法识别" onclick="pictrueNotMatch('<%=tempRow.get("fix_rpi_id",0l) %>')"/>
									<%} %>
								<%} %>	
							</td>
							<td>
								 	<input type="button" class="long-button" style="cursor:pointer;" value="打签" onclick="printLabel('<%=tempRow.getString("p_name") %>','<%=tempRow.get("fix_rpi_id",0l) %>','0');"/><br /><br />
							</td>
						</tr>
				
					<%} %>
				<%} %>
					
					
				
			
				<%for(DBRow tempRow : rows){
					boolean hasReturnProductItemFix = false ;
					boolean isReturnHandleFinish = tempRow.get("is_return_handle",0) == 1; 
					long fix_rpi_id = 0l;
					StringBuffer fix_ids = new StringBuffer();
				 %>
					<tr>
						<td></td>
						<td>
							<!-- 根据rpi_id 去查询是否有图片了信息了 -->
						 	<%
						 		DBRow[] itemsFix = returnProductItemsFixMgrZr.getReturnProductItemfixByRpiId(tempRow.get("rpi_id",0l));
						 		if(itemsFix == null || itemsFix.length < 1){
						 		%>
						 			<div class="" style="padding-top:20px;padding-left:5px;padding-bottom:10px;">
						 				 
						 				<a href="javascript:void(0)" class="normal" onclick="addPictureDetail('<%=tempRow.get("rpi_id",0l) %>');"/>添加退件图片</a>
 						 			 	 
						 			</div>		
						 		<%
						 		}else{
						 			hasReturnProductItemFix = true ;
						 			//读取fix_rpi_id 对应的图片信息
						 			String basePath = ConfigBean.getStringValue("systenFolder") + "/upload/" +systemConfig.getStringConfigValue("file_path_return_product_detail") +"/";
						 			for(DBRow fixRow : itemsFix){
						 				fix_rpi_id = fixRow.get("fix_rpi_id",0l);
						 				fix_ids.append(",").append(fix_rpi_id);
						 				int is_relation_product = fixRow.get("is_relation_product",ReturnProductItemMatchKey.NOT_RELATION); 
					 	 				boolean isNotMatch = (is_relation_product == ReturnProductItemMatchKey.NOT_MATCH);
					 	 			
					 	 				String borderColor  = isNotMatch?"silver":"#993300";
						 				 
						 				%>
								 		<fieldset class="set" style="padding-bottom:4px;border:2px solid <%=borderColor %>;width:360px;">
				 	 		 	  			<legend>
												<span>
													<span style="font-weight:bold;color:green;"><%= returnProductItemMatchKey.getStatusById(is_relation_product) %><font color="#f60"> 数量 : <%=fixRow.get("quantity",0.0d) %></font></span>
													&nbsp;&nbsp;<span><%=(isReturnHandleFinish?"退件处理完成":"退件处理未完成") %></span>
													
												</span> 
											</legend>
						 				<% 
						 				DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(fixRow.get("fix_rpi_id",0l),FileWithTypeKey.RETURN_PRODUCT_DETAIL);
						 				for(DBRow fileTempRow : files){
 	 		 	  							String filePath = basePath + fileTempRow.getString("file_name");
						 					%>
						 						 <span style="border:1px solid silver ;border:1px solid silver;float:left;margin-left:2px;margin-top:2px;">
 		 	  							   				<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.RETURN_PRODUCT_DETAIL %>','<%=fixRow.get("fix_rpi_id",0l) %>','<%=fileTempRow.getString("file_name") %>');">
 		 	  							   					<img src="<%=filePath %>" onload="setImgWeightHeight(this,100,100)" title='<%=fileTempRow.getString("file_name") %>'/>
 		 	  							   	 			</a>
 		 	  							  		 </span>
						 					<% 
						 				}
						 				%>
						 				<p style = "clear:both;padding-top:2px;padding-left:5px;">
						 					<a href="javascript:void(0)" class="normal" onclick="updatePictureDetail('<%=tempRow.get("rpi_id",0l) %>','<%= fixRow.get("fix_rpi_id",0l)%>');"/>添加退件图片</a>
						 			 
 						 				</p>
 						 			 
						 				</fieldset>
						 				<% 
						 			}
						 		}
						 	%>
						</td>
						<td>
							<span  style="font-weight:bold;">
							<%
								String not_on_rma_color = "black";
								String not_on_rma_title = "";
								if(1 == tempRow.get("is_product_not_on_rma",0))
								{
									not_on_rma_color = "#f60";
									not_on_rma_title = "非原退货单上商品";
								}
							%>
							<a href="javascript:void(0)" onclick="showProductPictrue('<%= tempRow.get("pid",0l)%>')"><span style='color:<%=not_on_rma_color %>;' title='<%=not_on_rma_title %>'><%= tempRow.getString("p_name") %></span></a>
							</span> 
							<span style="color:blue">x  <%= tempRow.get("quantity",0.0)%>&nbsp;&nbsp;<%=tempRow.getString("unit_name") %></span>	
							<%
							if(ReturnProductKey.FINISHALL!=returnProductRow.get("status",0))
							{
							%>
							&nbsp;<a href="javascript:void(0);" onclick="modReturnItemCount('<%=tempRow.get("rpi_id",0l)%>','<%= tempRow.get("quantity",0.0)%>','<%= tempRow.getString("p_name") %>')"><img src="../imgs/application_edit.png" title="修改明细及子明细数量" width="14" height="14" border="0"></a>
							<%
							}%>
							<!-- 查询是否是套装-->
 	 		 	 	 		 		 	<%if(tempRow.get("product_type",0) == 2){
 	 		 	 	 		 		 			DBRow[] subItems = returnProductSubItemMgrZr.getReturnProductSubItemsByRpiId(tempRow.get("rpi_id",0l));
 	 		 	 	 		 		 	 	for(DBRow subItem : subItems){
 	 		 	 	 		 		 	 %>
 	 		 	 	 		 		 			<p>
 	 		 	 	 		 		 				├<%=subItem.getString("p_name") %> x &nbsp;<%=subItem.getString("quantity") %> &nbsp;<%=subItem.getString("unit_name") %>
 	 		 	 	 		 		 				<%
			 	 		 	 	 		 		 		if(ReturnProductKey.FINISHALL != returnProductRow.get("status",0))
			 	 		 	 	 		 		 		{
			 	 		 	 	 		 		 	%>
			 	 		 	 	 		 		 	&nbsp;<a href="javascript:void(0);" onclick="modReturnSubItemCount('<%=subItem.get("rpsi_id",0l)%>')"><img src="../imgs/application_edit.png" title="修改子明细数量" width="14" height="14" border="0"></a>
			 	 		 	 	 		 		 	<%
			 	 		 	 	 		 		 		}
			 	 		 	 	 		 		 	%> 
 	 		 	 	 		 		 			</p>	
 	 		 	 	 		 		 			
 	 		 	 	 		 		 	<%}}%>
							<!-- 查询是否有需要check -->
							
							<% 
								DBRow[] checkItems = null ;
	 	 			 				if(tempRow.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){
	 	 			 					 checkItems = checkReturnProductItemMgrZr.getCheckReturnProductItemByRpIId(tempRow.get("rpi_id",0l));
	 	 			 			}
	 	 			 			if(checkItems != null && checkItems.length > 0){
	 	 			 				for(DBRow checkRow : checkItems){
	 	 			 					%>
	 	 			 						
	 	 			 					<div style="" class="checkDiv">
 	 		 	 	 		  				<span style="color:#f60;">Q:<%=checkRow.getString("create_employee_name") %></span> <span>[<%= tDate.getFormateTime(checkRow.getString("create_date") )%>]</span>
 	 		 	 	 		  				<p style="margin-top:2px;margin-bottom:3px;">
 	 		 	 	 		  					<%= checkRow.getString("check_product_context") %>
  	 		 	 	 		  					<a href="javascript:void(0)" title="回答" onclick="replyCheck('<%=checkRow.get("check_return_product_id",0l) %>');"><img width="12" height="12" border="0" alt="回答测试" src="../imgs/help.gif"></a>
 	 		 	 	 		  				</p>
 	 		 	 	 		  				<%if(checkRow.getString("result_context").length() > 0){ %>
 	 		 	 	 		  				<span style="color:#f60;">A:<%=checkRow.getString("handle_employ_name") %></span> <span>[<%= tDate.getFormateTime(checkRow.getString("handle_date") )%>]</span>
 	 		 	 	 		  				<p style="margin-top:2px;">
 	 		 	 	 		  				  	<%=checkRow.getString("result_context") %>
 	 		 	 	 		  				</p>
 	 		 	 	 		  				<%} %>
 	 		 	 	 		  			</div>
	 	 			 					<% 
	 	 			 				}
	 	 			 				
	 	 			 			}
 	 			 			%>
 	 			 			<!-- 处理结果 -->
 	 			 			 <p>
 	 			 			 	处理要求: <font style="font-weight:bold;"><%=handleResultRequestTypeKey.getStatusById(tempRow.get("handle_result_request",0)) %></font><br />
  	 			 			 </p>
  	 			 			 <p>
  	 			 			 	退件处理:<font style="font-weight:bold;"><%= tempRow.get("is_return_handle",0)== 0? "未完成":"完成"%></font><br /> 
  	 			 			 </p>
  	 			 			 	<%if(tempRow.get("is_receive_item",0l) == -1){ %>
	  	 			 			 <p>
	  	 			 			 	商品退件:<font style="font-weight:bold;">未收到该商品退件</font>
	  	 			 			 </p>
  	 			 			 <%} %>
 	 			 			
						</td>
						<td>
						<!-- 有关联上图片的时候才可能有打签 删除明细 -->
							<% //if(hasReturnProductItemFix){%>
<%-- 	 		 	 				<input type="button" class="long-button" style="cursor:pointer;" value="打签" onclick="printLabel('<%=tempRow.getString("p_name") %>','<%=fix_ids.substring(1) %>','<%=tempRow.get("pid",0l) %>');"/><br /><br />--%>
							<%
								//}
								//else
								//{
									if(ReturnProductKey.FINISHALL!=returnProductRow.get("status",0))
									{	
							%>
								<input type="button" class="long-button" value="未收到退件" onclick="returnProductNoPictrue('<%= tempRow.get("rpi_id",0l)%>');" style="cursor:pointer;"/><br /><br />
								<input type="button" class="long-button" style="cursor:pointer;" value="打签" onclick="printLabel('<%=tempRow.getString("p_name") %>','<%= tempRow.get("rpi_id",0l)%>','<%=tempRow.get("pid",0l) %>');"/><br /><br />
							<%
									}
								//} %>
							<%
							//退货单已经完成，不能再操作
							if(ReturnProductKey.FINISHALL!=returnProductRow.get("status",0))
							{
							%>
<%--								<input type="button" value="退货处理" class="long-button" onclick="returnHandle('<%=rp_id %>','<%=tempRow.get("rpi_id",0l) %>','<%=tempRow.get("is_receive_item",0) %>')"/><br /><br />--%>
 	 			 			<%
							}
 	 			 			%>
						</td>
					</tr>	
					
					
			<%	} 
			}else{%>
			<tr>
				<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>	
			<%} %>
		</tbody>
	</table>
	<script type="text/javascript">
	 function showProductPictrue(pid){ 
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_file_upload.html?pc_id='+pid+"&open_type=read_only"
	    $.artDialog.open(uri , {title: '查看商品:图片',width:'700px',height:'430px', lock: true,opacity: 0.3,fixed: true});
	 }
	</script>
</body>
</html>