<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ReturnProductStatusKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductStatusKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductSourceKey"%>
<%@page import="com.cwc.app.key.ReturnProductPictureRecognitionkey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.ReturnTypeKey"%>
<%@page import="com.cwc.app.api.AdminMgr"%>
<%@page import="com.cwc.app.key.ReturnProductLogTypeKey"%>
 
 <jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
 
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
	String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
int search_mode = StringUtil.getInt(request,"search_mode");
PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(10);
TDate date = new TDate();
date.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
String	input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
String 	input_st_date =	  date.getStringYear()+"-"+date.getStringMonth()+"-"+date.getStringDay();
DBRow[] rows = null ;
long create_adid = 0l;
int return_product_source = -1;
int  status = 0;
int is_print_over = -1;
int return_product_type = -1 ;
int picture_recognition = -1;
int check_item_type = -1 ;
int gather_picture_status = -1 ;
int is_need_return = ReturnTypeKey.NEED;
if(cmd.equals("filter")){
	   input_st_date = StringUtil.getString(request,"st");
	   input_en_date = StringUtil.getString(request,"end");
	   create_adid = StringUtil.getLong(request,"create_adid");
	   return_product_source = StringUtil.getInt(request,"return_product_source");
	   status = -1==StringUtil.getInt(request,"status")?0:StringUtil.getInt(request,"status");
	   is_need_return = StringUtil.getInt(request, "is_need_return");
	   is_print_over = StringUtil.getInt(request,"is_print_over");
	   return_product_type = StringUtil.getInt(request,"return_product_type");
	   picture_recognition = StringUtil.getInt(request,"picture_recognition");
	   check_item_type = StringUtil.getInt(request,"check_item_type");
 	   rows = returnProductMgrZr.filterReturnProductRows(input_st_date, input_en_date, create_adid, return_product_source, status, is_print_over, return_product_type, pc,picture_recognition,check_item_type,gather_picture_status,is_need_return);
}else if(cmd.equals("tuijiancaiji")){
	   input_st_date = StringUtil.getString(request,"st");
	   input_en_date = StringUtil.getString(request,"end");
	   create_adid = StringUtil.getLong(request,"create_adid");
	   return_product_source = StringUtil.getInt(request,"return_product_source");
	   gather_picture_status = 0;
 	   rows = returnProductMgrZr.filterReturnProductRows(input_st_date, input_en_date, create_adid, return_product_source, status, is_print_over, return_product_type, pc,picture_recognition,check_item_type,gather_picture_status,is_need_return);

	   
	   
}else if(cmd.equals("recognition")){
	   input_st_date = StringUtil.getString(request,"st");
	   input_en_date = StringUtil.getString(request,"end");
	   create_adid = StringUtil.getLong(request,"create_adid");
	   return_product_source = StringUtil.getInt(request,"return_product_source");
	   gather_picture_status = 1;
	   picture_recognition =  ReturnProductPictureRecognitionkey.RecognitionNoneFinish;
 	   rows = returnProductMgrZr.filterReturnProductRows(input_st_date, input_en_date, create_adid, return_product_source, status, is_print_over, return_product_type, pc,picture_recognition,check_item_type,gather_picture_status,is_need_return);  
}else if(cmd.equals("return_product_item_handle")){
	   input_st_date = StringUtil.getString(request,"st");
	   input_en_date = StringUtil.getString(request,"end");
	   create_adid = StringUtil.getLong(request,"create_adid");
	   return_product_source = StringUtil.getInt(request,"return_product_source");
	   gather_picture_status = 1;
	   picture_recognition =  ReturnProductPictureRecognitionkey.RecognitionFinish;
	   status = -ReturnProductKey.FINISHALL;//StorageReturnProductStatusKey.FINISH;
	   rows = returnProductMgrZr.filterReturnProductRows(input_st_date, input_en_date, create_adid, return_product_source, status, is_print_over, return_product_type, pc,picture_recognition,check_item_type,gather_picture_status,is_need_return);  
}else if(cmd.equals("archiving")){
	  input_st_date = StringUtil.getString(request,"st");
	   input_en_date = StringUtil.getString(request,"end");
	   create_adid = StringUtil.getLong(request,"create_adid");
	   return_product_source = StringUtil.getInt(request,"return_product_source");
	   picture_recognition =  ReturnProductPictureRecognitionkey.RecognitionFinish;
	   gather_picture_status = 1;
	   status = ReturnProductKey.FINISHALL;//StorageReturnProductStatusKey.FINISH;
	   rows = returnProductMgrZr.filterReturnProductRows(input_st_date, input_en_date, create_adid, return_product_source, status, is_print_over, return_product_type, pc,picture_recognition,check_item_type,gather_picture_status,is_need_return);  
}
else if(cmd.equals("search"))
{
	rows = returnProductOrderMgrZyj.searchReturnOrderByNumber(key,search_mode,pc);
}
else if("need_gather_pic_count".equals(cmd))
{
	rows = returnProductOrderMgrZyj.statNeedGatherPicReturnOrders(pc);
}
else if("need_recog_count".equals(cmd))
{
	rows = returnProductOrderMgrZyj.statNeedRecognitionReturnOrders(pc);
}
else if("need_register_count".equals(cmd))
{
	rows = returnProductOrderMgrZyj.statNeedRegisterReturnOrders(pc);
}
else if("need_register_finish".equals(cmd))
{
	rows = returnProductOrderMgrZyj.statRegisterFinishReturnOrders(pc);
}
else if("need_relation_other_order".equals(cmd))
{
	rows = returnProductOrderMgrZyj.statReturnOrderNotRelationOtherOrders(pc);
}
else{
	rows = returnProductMgrZr.getReturnProductBypage(pc);
}

StorageReturnProductStatusKey returnProductStatusKey = new StorageReturnProductStatusKey();
ReturnProductKey returnProductKey = new ReturnProductKey();
StorageReturnProductTypeKey returnProductTypeKey = new StorageReturnProductTypeKey();
ReturnProductSourceKey returnProductSourceKey = new ReturnProductSourceKey();
ReturnProductPictureRecognitionkey pictureRecognitionkey = new ReturnProductPictureRecognitionkey();
ReturnProductCheckItemTypeKey checkItemTypeKey = new ReturnProductCheckItemTypeKey();
ReturnProductItemHandleResultRequestTypeKey itemHandleRequestType = new ReturnProductItemHandleResultRequestTypeKey();
ReturnProductItemHandleResultTypeKey handleResultTypeKey = new ReturnProductItemHandleResultTypeKey();
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/return_order/ReturnProductFileDownLoadAction.action";
String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean( request.getSession(true) );
%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货单列表</title>

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
 

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
<script type="text/javascript" src="../js/setImgWeightHeight.js"></script>
<style type="text/css">

	
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 3px;}
span.valueName{width:40%;border:0px solid red;display:block;float:left;text-align:right;}
span.valueSpan{width:56%;border:0px solid green;float:left;overflow:hidden;text-align:left;text-indent:4px;}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}

</style>
<script type="text/javascript">
jQuery(function($){
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/return_order/GetSearchReturnOrderJSONAction.action",
			"merge_field","rp_id");
})
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("请输入要查询的关键字");
	}
	else
	{
		var val_search = "\'"+val.toLowerCase()+"\'";
		$("#search_key").val(val_search);
		document.search_form.key.value = val_search;
		document.search_form.search_mode.value = 1;
		document.search_form.submit();
	}
}

function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);
		document.search_form.key.value = val;
		document.search_form.search_mode.value = 2;
		document.search_form.submit();
	}
}
function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
	function createReturnProduct(){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/previous_add_return_product.html"; 
	    $.artDialog.open(uri , {title: '新增退货',width:'800px',height:'580px', lock: true,opacity: 0.3,fixed: true});
	}
	 
	function addDetail(rp_id,create_type){
		var uri  ;
	 	if(create_type  *  1 == 1){
	   		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/add_return_product_detail_list.html?rp_id="+ rp_id; 
	 	}else{
	   		 uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_detail_list.html?rp_id="+ rp_id; 
		}
		 $.artDialog.open(uri , {title: '退货单['+rp_id+']明细',width:'950px',height:'430px', lock: true,opacity: 0.3,fixed: true,close:function(){window.location.reload();}});
	}
	function refreshWindow() {window.location.reload();}
	function showPackPictrue(return_product_id){
	    var obj = {
			file_with_type:'<%= FileWithTypeKey.RETURN_PRODUCT_FILE%>',
			file_with_id : return_product_id,
			current_name : '' ,
			file_with_class:0,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_return_product")%>'
		}
	 
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
	function relationOver(rp_id){
	    $.artDialog.confirm('确认退货图片关联完成？', function(){
		    var uri =  '<%=ConfigBean.getStringValue("systenFolder")%>' + "action/administrator/returnProduct/UpdateReturnProductRelationOverAction.action"; 
			var obj =  {rp_id:rp_id};
			commonAjax(uri, obj);
		}, function(){
			
		});
	}
	// json 放回结果为success的适用
	function commonAjax(uri,dataJson){
	    $.ajax({
		    url:uri,
		    data:dataJson,
		    dataType:'json',
		    beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
		    success:function(data){
			    $.unblockUI();
		    	if(data && data.flag == "success"){
					window.location.reload();
			   	}else{
			   		 alert("系统错误.");
				}
	    	},
	    	error:function(){$.unblockUI();alert("系统错误.");}
		})
	}
	function filterReturnProduct(){
		var filterForm = $("#filterForm");
 
		filterForm.submit();
	}
	jQuery(function($){
		var filterForm ;
	    if('<%= cmd%>' === "filter"){
		      filterForm = $("#filterForm");
		}
		if('<%= cmd%>' === "recognition"){
		    filterForm = $("#recognitionForm");
		}
		if('<%= cmd%>' === "directArchiving"){		    
		    filterForm = $("#directArchivingForm");
		}
		if('<%= cmd%>' === "testArchiving"){
		    filterForm = $("#testArchivingForm");	    
		}
	    $("select[name='create_adid'] option[value='"+<%= create_adid%>+"']" , filterForm).attr("selected",true);
	    $("select[name='return_product_source'] option[value='"+<%= return_product_source%>+"']",filterForm).attr("selected",true);
	    $("select[name='status'] option[value='"+<%= status%>+"']",filterForm).attr("selected",true);
	    $("select[name='is_print_over'] [value='"+<%= is_print_over%>+"']",filterForm).attr("selected",true);
	    $("select[name='return_product_type'] [value='"+<%= return_product_type%>+"']",filterForm).attr("selected",true);
	    $("select[name='picture_recognition'] [value='"+<%= picture_recognition%>+"']",filterForm).attr("selected",true);
	    $("select[name='check_item_type'] [value='"+<%= check_item_type%>+"']",filterForm).attr("selected",true);
	    $("select[name='is_need_return'] [value='"+<%= is_need_return%>+"']",filterForm).attr("selected",true);
	})
	function filterRecognitionForm(){
		var recognitionForm = $("#recognitionForm");
		recognitionForm.submit();
	}
	function filterzTuijiancaijiForm(){
		var tuijiancaijiForm = $("#tuijiancaijiForm");
		tuijiancaijiForm.submit();
	}
	function filterReturnProductItemHandleForm(){
	    var returnProductItemHandleForm = $("#returnProductItemHandleForm");
	    returnProductItemHandleForm.submit();
	}
	function filterArchivingForm(){
		var testArchivingForm = $("#archivingForm");
		testArchivingForm.submit();
	}
	function returnProductFinish(rp_id) {
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_finish.html?rp_id="+rp_id; 
	    $.artDialog.open(uri , {title: '退货完成',width:'340px',height:'220px', lock: true,opacity: 0.3,fixed: true});
	}
	function returnProductEnforcementFinish(rp_id)
	{
		$.artDialog.confirm('确定此退货单完成?', function(){
			  $.ajax({
				    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnProductItemEnforcementFinishAction.action',
				    dataType:'json',
				    data:{rp_id:rp_id, status:'<%=ReturnProductKey.FINISHALL%>'},
				    beforeSend:function(request){
						
					},
				    success:function(data){
						window.location.reload();
					},
					error:function(){}
					})
			 
		}, function(){
			
		});
	}
	function returnNotNeedCreateBill(rp_id)
	{
		$.artDialog.confirm('确定此退货单无需账单?', function(){
			  $.ajax({
				    url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_order/ReturnIsNeedBillAction.action',
				    dataType:'json',
				    data:{rp_id:rp_id, is_need_create_bill:2},
				    beforeSend:function(request){
						
					},
				    success:function(data){
						window.location.reload();
					},
					error:function(){}
					})
			 
		}, function(){
			
		});
	}
	function relationService(rp_id){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_relation_service.html?rp_id="+rp_id; 
	    $.artDialog.open(uri , {title: '关联单据',width:'500px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	    
	}
	function returnProductRegister(rp_id,oid,isCanRegister){
		if(1 == isCanRegister*1)
		{
			 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_order_handle_product.html?rp_id='+rp_id;
			 $.artDialog.open(uri , {title:'['+rp_id+']退货登记',width:'800px',height:'430px', lock: true,opacity: 0.3,fixed: true,
			     ok:function(){ 
			     	this.iframe.contentWindow.updateForm && this.iframe.contentWindow.updateForm(); 
			     	return false;
			     },
			     cancel:function(){}
			    });
		}
		else
		{
			alert("不在同一个仓库，禁止登记");
		}
	}
	function viewReturnProductRegister(rp_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/return_product_items_register.html?rp_id="+rp_id;
		$.artDialog.open(uri , {title: '['+rp_id+']退货登记完成',width:'800px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	}
	//图片在线显示
	function showPictrueOnline(fileWithType,fileWithId , currentName){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>returnImg'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	function deleteFile(file_id, file_name){
		if(confirm("确定要删除文件：["+file_name+"]吗？")){
			$.ajax({
				url:'<%= deleteFileAction%>',
				dataType:'json',
				data:{table_name:'file',file_id:file_id,pk:'file_id'},
				success:function(data){
					if(data && data.flag === "success"){
						window.location.reload();
					}else{
						showMessage("系统错误,请稍后重试","error");
					}
				},
				error:function(){
					showMessage("系统错误,请稍后重试","error");
				}
			})
		}
	}
	function modReturnOrderPsId(rp_id, ps_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/mod_return_order_psId.html?rp_id='+rp_id+'&ps_id='+ps_id;
		$.artDialog.open(uri , {title: '修改['+rp_id+']的收货仓库',width:'220px',height:'120px', lock: true,opacity: 0.3,fixed: true});
	}
	function returnOrderLogs(rp_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/return_product/return_order_logs.html?rp_id='+rp_id;
		$.artDialog.open(uri , {title: '退货单['+rp_id+']的日志',width:'900px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
 

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload = "onLoadInitZebraTable()">
 
 <form action='' method="post" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>


<div class="demo" style="width:98%;" >
	<div id="tabs">
		<ul>
			<li><a href="#warranty_search">常用工具</a></li>
			<li><a href="#filter">过滤</a></li>	
			<!-- 新创建(退件图片还没有采集完成) -->
			<li><a href="#tuijiancaiji">退件采集</a></li>
			<!-- 需识别(退件图片采集完成,但是识别没有完成客服人员需要去识别图片的) -->
			<li><a href="#recognition">需识别</a></li>	
			<!--  退件处理(图片识别完成,但是退件处理没有完成.)-->
			<li><a href="#return_product_item_handle">退件处理</a></li>	
			<!-- 归档(表示退件处理完成,但是退货没有完成) -->
			<li><a href="#archiving">归档</a></li>	
		</ul>
		<div id="warranty_search">
		 
			<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
								<td style="padding:0px;">
									<span style="margin-top:-10px;width:86px;height:33px;line-height:33px;text-align:center;font-size:15px;cursor:pointer;display:block;background:url(../imgs/product/normal_green_long.gif)" onclick="createReturnProduct();">收到退货</span>
								</td>
							</tr>
						</table>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
				  </td>
	            </tr>
	          </table>
			<form action="" id="loadReturnHtml" method="post">
				
			</form>
			<input type="hidden" name="cmd" value="search">
		 
		</div>
		<div id="filter">
		<form id="filterForm" method="post">
				 	<input type="hidden" name="cmd" value="filter"/>
				 	开始:<input type="text" name="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;<input type="text" name="end" class="end" value="<%= input_en_date %>" style="width:100px;"/>
				 	 <select name="create_adid" id="create_adid">
				 				<option value="-1">全部客服报价人</option>
				 			<%
								DBRow admins[] = adminMgr.getAllAdmin(null);
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 			</select>
				 		来源: <select id="return_product_source" name="return_product_source" style="width:80px;">
				 				<option value="-1">全部退货来源</option>
				 				 <%
				 				 ArrayList<String> list = (ArrayList<String>)returnProductSourceKey.getStatus();
									for(String str : list){
										%>
										<option value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				 %>	
				 			</select>
				 			<br /><br /> 
					 	状态: <select id="status" name="status">
					 	 		<option value="-1">全部状态</option>
					 			 <%
						 			 ArrayList<String> statusList = (ArrayList<String>)returnProductKey.getReturnProductStatus();
									 for(String str : statusList){
								%>
								 <option value='<%=str %>'><%=returnProductKey.getReturnProductStatusById(Integer.parseInt(str)) %></option>	
								<% 
									 }
					 			 %>
					 	</select>
					 图片识别: 
					 <select id="picture_recognition" name="picture_recognition">
					 		<option value="-1">全部状态</option>
					 		 <%
				 				 ArrayList<String> recognitionlist = (ArrayList<String>)pictureRecognitionkey.getStatus();
									for(String str : recognitionlist){
										%>
										<option value='<%=str %>'><%=pictureRecognitionkey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 		 %>	
					 </select>
					退货测试:
					<select id="check_item_type" name="check_item_type">
					 		<option value="-1">全部状态</option>
					 		 <%
				 				 ArrayList<String> checkList = (ArrayList<String>)checkItemTypeKey.getStatus();
									for(String str : checkList){
										%>
										<option value='<%=str %>'><%=checkItemTypeKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
							}
				 		 %>	
					 </select>
					 
				  	打标签: <select id="is_print_over" name="is_print_over">
					 	 		<option value="-1">全部状态</option>
					 			<option value="0">打签未完成</option>
					 			<option value="1">打签完成</option>
					 	</select>
				  
				  包裹:<select id="return_product_type" name="return_product_type">
					 	 		<option value="-1">全部状态</option>
					 	 		  <%
				 				 ArrayList<String> productTypelist = (ArrayList<String>)returnProductTypeKey.getStatus();
									for(String str : productTypelist){
										%>
										<option value='<%=str %>'><%=returnProductTypeKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				%>	
					 </select>
					 是否退货:<select name="is_need_return">
					 			<option value="-1">全部状态</option>
					 			<%
					 				ReturnTypeKey returnTypeKey = new ReturnTypeKey();
					 				ArrayList<String> returnTypeKeyList = returnTypeKey.getReturnTypeKeys();
					 				for(String str:returnTypeKeyList)
					 				{
							 	%>
							 				<option value="<%=str %>"><%=returnTypeKey.getReturnTypeKeyValue(str) %></option>
							 	<%
					 				}
					 			%>
					 		 </select>
			 		 <input id="seachByCondition" class="button_long_refresh" type="button" name="seachByCondition" value="过 滤" onclick="filterReturnProduct();">
				 
				 	</form>
		</div>
		<div id="tuijiancaiji">
				<form id="tuijiancaijiForm" method="post">
					<input type="hidden" name="cmd" value="tuijiancaiji"/> 
					开始:<input type="text" name="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;<input type="text" name="end" class="end" value="<%= input_en_date %>" style="width:100px;"/>
				 <select name="create_adid" >
				 				<option value="-1">全部客服报价人</option>
				 			<%
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 </select>
				 	来源: <select name="return_product_source" style="width:80px;">
				 				<option value="-1">全部退货来源</option>
				 				 <%
									for(String str : list){
										%>
										<option value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				 %>	
				 	</select>
				 	<input id="seachByCondition" class="button_long_refresh" type="button" name="seachByCondition" value="过 滤" onclick="filterzTuijiancaijiForm();">
					
				</form>
		</div>
		<div id="recognition">
			<form id="recognitionForm" method="post">
 				<input type="hidden" name="cmd" value="recognition"/> 
					开始:<input type="text" name="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;<input type="text" name="end" class="end" value="<%= input_en_date %>" style="width:100px;"/>
				 <select name="create_adid" >
				 				<option value="-1">全部客服报价人</option>
				 			<%
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 </select>
				 	来源: <select name="return_product_source" style="width:80px;">
				 				<option value="-1">全部退货来源</option>
				 				 <%
									for(String str : list){
										%>
										<option value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				 %>	
				 	</select>
				 	<input id="seachByCondition" class="button_long_refresh" type="button" name="seachByCondition" value="过 滤" onclick="filterRecognitionForm();">
				 	
			</form>
		</div>
		<div id="return_product_item_handle"> 
			<form id="returnProductItemHandleForm" method="post">
				<input type="hidden" name="cmd" value="return_product_item_handle"/> 
					开始:<input type="text" name="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;<input type="text" name="end" class="end" value="<%= input_en_date %>" style="width:100px;"/>
				 <select name="create_adid" >
				 				<option value="-1">全部客服报价人</option>
				 			<%
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 </select>
				 	来源: <select name="return_product_source" style="width:80px;">
				 				<option value="-1">全部退货来源</option>
				 				 <%
									for(String str : list){
										%>
										<option value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				 %>	
				 	</select>
				 	<input id="seachByCondition" class="button_long_refresh" type="button" name="seachByCondition" value="过 滤" onclick="filterReturnProductItemHandleForm();">
			
			</form>
			
		</div>
		
		<div id="archiving">
			<form id="archivingForm" method="post">
				<input type="hidden" name="cmd" value="archiving"/>
				开始:<input type="text" name="st" value="<%= input_st_date %>" class="st" style="width:100px;"/>&nbsp;-&nbsp;<input type="text" name="end" class="end" value="<%= input_en_date %>" style="width:100px;"/>
				 <select name="create_adid" >
				 				<option value="-1">全部客服报价人</option>
				 			<%
								for (int i=0; i<admins.length; i++){
							%>
								   <option value="<%=admins[i].get("adid",0l)%>" > 
								    <%=admins[i].getString("account")%>  </option>
							 <%
								}
								%>
				 </select>
				 	来源: <select name="return_product_source" style="width:80px;">
				 				<option value="-1">全部退货来源</option>
				 				 <%
									for(String str : list){
										%>
										<option value='<%=str %>'><%=returnProductSourceKey.getStatusById(Integer.parseInt(str)) %></option>	
										<% 
									}
				 				 %>	
				 	</select>
				 	<input id="seachByCondition" class="button_long_refresh" type="button" name="seachByCondition" value="过 滤" onclick="filterArchivingForm();">	
			</form>
		</div>
	</div>
</div>
<table id="stat_table" width="98%" border="0" align="center"  style="margin-top:5px;" cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table" isNeed="true" isBottom="true">
<thead>
<tr>
	<th width="20%" class="right-title"  style="text-align:center;">基本信息</th>
   
    <th width="25%"  class="right-title"  style="text-align: center;">客户信息</th>
    <th width="35%"  class="right-title"  style="text-align: center;">退货情况</th>
    <th  width=""  class="right-title" style="text-align: center;">备注及日志</th>
</tr>
</thead>
<tbody>
 	<%
 		if(rows !=null && rows.length > 0){ 
 		 for(DBRow rowTemp : rows){
 			DBRow userInfo = null;
		    userInfo = waybillMgrZR.getUserNameById(rowTemp.get("create_user",0l));
		    String userName = "";
			if(userInfo != null ){
				  userName =  userInfo.getString("employe_name");
			}
 	%>
	 		<tr>
	 			<td>
 				 	<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
		  				 <legend>
								<span class="title">
									<span style="font-weight:bold;">[<%= rowTemp.get("rp_id",0l)%>] 
										<font color="green"><%=  returnProductKey.getReturnProductStatusById(rowTemp.get("status",8)) %></font>
										<font color="#f60">[<%=returnProductSourceKey.getStatusById(rowTemp.get("return_product_source",1))  %>]</font>
									</span>
								</span> 
						</legend>
								<p>
									<span class="alert-text stateName" style="font-weight:bold;">采集:</span>
									<span class="stateValue" style="font-weight:bold;"><%= (rowTemp.get("gather_picture_status",0) == 0 ? "图片采集未完成":"图片采集完成") %></span>
								</p>
								<p>
									<span class="alert-text stateName" style="font-weight:bold;">图片:</span>
									<span class="stateValue" style="font-weight:bold;"><%= pictureRecognitionkey.getStatusById(rowTemp.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish))%></span>
								</p>
								<p>
									<span class="alert-text stateName" style="font-weight:bold;">退货:</span>
									<span class="stateValue" style="font-weight:bold;"><%= checkItemTypeKey.getStatusById(rowTemp.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK))%></span>
								</p>
								
								<p>
									<span class="alert-text stateName"><img title="退货类型" src="../imgs/invoice.gif" /></span>
			  						<span class="stateValue"><%=returnProductTypeKey.getStatusById(rowTemp.get("return_product_type",1))%></span>
								</p>
								<p>
			  						<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
			  						<span class="stateValue"><%=userName %> &nbsp;
			  							<%
			  								if(1 == rowTemp.get("create_type",0))
			  								{
			  									out.println("(仓库)");
			  								}
			  								else
			  								{
			  									out.println("(客服)");
			  								}
			  							%>
			  						</span>
			  					</p>
				  				<p>
				  					<span class="alert-text stateName"><img  title="创建时间" src="../imgs/alarm-clock--arrow.png" /></span>
				  					<span class="stateValue"><%=tDate.getFormateTime(rowTemp.getString("create_date"))%></span>
				  				</p>
				  				<%
				  					if(0 != rowTemp.get("oid",0L) || 0 != rowTemp.get("wid",0L) || 0 != rowTemp.get("sid",0L))
				  					{
				  				%>
				  				<p>
									<span class="alert-text stateName" style="font-weight:bold;">关联:</span>
									<span class="stateValue" style="font-weight:bold;">
										<%
											if(0 != rowTemp.get("oid",0L))
											{
										%>	
											订单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=<%=rowTemp.get("oid",0L)%>' target="_blank" style="cursor: pointer;"><%=rowTemp.get("oid",0L)%></a>
										<%
											}
											if(0 != rowTemp.get("wid",0L))
											{
										%>
											运单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid=<%=rowTemp.get("wid",0L) %>' target="_blank" style="cursor: pointer;"><%=rowTemp.get("wid",0L) %></a>
										<%
											}
											if(0 != rowTemp.get("sid",0L))
											{
										%>
											服务单:<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=rowTemp.get("sid",0L) %>'" target="_blank" style="cursor: pointer;"><%=rowTemp.get("sid",0L) %></a>
										<%
											}
										%>
									</span>
								</p>
								<%
									}
				  				%>
					</fieldset>
					<!--  即使是没有包裹的情况,那么也有可能是运输公司 -->
					 
		 				<fieldset class="set"  style="padding-bottom:4px;border:2px solid silver;color:black;">
		 					 <legend>
		 						<span class="title" style="font-weight:bold;" title="运输公司">
									 运输公司:  <%= rowTemp.getString("express_company_").length() < 1 ? "未知":rowTemp.getString("express_company_") %>
								</span> 
		 					 </legend>
		 					 <p>
		 					 	Tracking : <%=rowTemp.getString("tracking_num") %>
		 					 </p>
		 					 <%if(rowTemp.get("return_product_type",StorageReturnProductTypeKey.NOT_PACK) == StorageReturnProductTypeKey.HAS_PACK){ %>
		 						 <p><a href="javascript:void(0)" onclick="showPackPictrue('<%=rowTemp.get("rp_id",0l) %>');"> 查看外包装图片</a></p>
		 					 <%} %>
		 				</fieldset>
	 			 
	 			</td>
	 			<td style="padding-top:5px;padding-bottom:5px;">
 					 <p>	
	  					<span class="alert-text valueName">First Name:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("first_name")%>&nbsp;</span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">Last Name:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("last_name")%>&nbsp;</span>
					 </p>
				 	 <p>	
	  					<span class="alert-text valueName">Street:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("address_street")%>&nbsp;</span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">City:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("address_city")%>&nbsp;</span>
					 </p>
					 <p>	
	  					<span class="alert-text valueName">Country:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("address_country")%>&nbsp;</span>
					 </p>
					  <p>	
	  					<span class="alert-text valueName">State:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("address_state")%>&nbsp;</span>
					 </p>
	  				<p>	
	  					<span class="alert-text valueName">Tel:</span>
	  					<span class="valueSpan"><%=rowTemp.getString("tel")%>&nbsp;</span>
					 </p>
					 <p>	
	  					<span class="alert-text valueName">Zip:</span>
	  					<span class="valueSpan"><%= rowTemp.getString("address_zip")%>&nbsp;</span>
					 </p>
	 			</td>
	 			<td>
	 				<!--  目前是查询reson 表中的数据到时候要改 -->
	 				<fieldset class="set" style="padding-bottom:4px;border:2px solid #993300;">
	 					<legend>
								<span class="title">
									<span style="font-weight:bold;">
									<%
									DBRow productStoreCatalog = catalogMgr.getDetailProductStorageCatalogById(rowTemp.get("ps_id",0l));
									if(null != productStoreCatalog)
									{
										out.println(productStoreCatalog.getString("title"));
									}
									%>
									<%
										if(ReturnProductKey.FINISHALL != rowTemp.get("status",0))
										{
									%>
									&nbsp;<a href="javascript:void(0);" onclick="modReturnOrderPsId('<%=rowTemp.get("rp_id",0l)%>','<%=rowTemp.get("ps_id",0L)%>')"><img src="../imgs/application_edit.png" width="14" title="修改仓库" height="14" border="0"></a>
									<%
										}
									%>
									&nbsp;|&nbsp;退货商品</span>
								</span> 
						</legend>
	 				<%
	 					DBRow[] itemsRow = returnProductItemsFixMgrZr.getReturnItemsByRpId(rowTemp.get("rp_id",0l));
	 					if(itemsRow != null && itemsRow.length > 0 ){
	 						
	 						for(DBRow item : itemsRow){
	 							%>
	 							<p>
	 								<%
										String not_on_rma_color = "black";
										String not_on_rma_title = "";
										if(1 == item.get("is_product_not_on_rma",0))
										{
											not_on_rma_color = "#f60";
											not_on_rma_title = "非原退货单上商品";
										}
									%>
	 								 <span style='font-weight:bold;color:<%=not_on_rma_color %>;' title='<%=not_on_rma_title %>'><%=item.getString("p_name") %></span> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=item.getString("unit_name") %></span> <br />
	 								 
	 								 <%if(item.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK) != ReturnProductCheckItemTypeKey.OUTCHECK){ %>
	 									├ 商品:<%=checkItemTypeKey.getStatusById(item.get("return_product_check",0)) %><br />
	 								 <%} %>
	 								 
	 								 <%if(item.get("handle_result_request",0) != 0){ %>
	 	 								├ 处理要求:<span style="color:blue"><%= itemHandleRequestType.getStatusById(item.get("handle_result_request",0))%></span><br />
	 								 <% }%>
	 								 	├ 退货处理:<%=(item.get("is_return_handle",0) == 0 ? "未完成":"完成") %><br />
	 								 <%=!"".equals(item.getString("return_reason"))?"├退货原因:"+item.getString("return_reason"):""%>
	 							</p>
	 							<% 
	 						}
	 					}
	 				%>
	 				</fieldset><br/>
	 					<%
		 					int[] fileTypes = {FileWithTypeKey.ProductReturn};
			 				DBRow[] files = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(rowTemp.get("rp_id", 0L), fileTypes);
							if(files.length > 0)
							{
								for(int f = 0; f < files.length; f ++)
								{
									DBRow file = files[f];
						%>
								 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
				 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
				 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
				 			 	 <p>
			 			 	 	<%
			 			 	 		if(StringUtil.isPictureFile(file.getString("file_name")))
			 			 	 	 	{ 
			 			 	 	%>
					 			 	 
					 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.ProductReturn %>','<%=rowTemp.get("rp_id", 0L)%>','<%=file.getString("file_name") %>');"><%=file.getString("file_name") %></a>
				 			 	 <%
				 			 	 	}
									else if(StringUtil.isOfficeFile(file.getString("file_name")))
									{
								%>
				 			 	 		<a href="javascript:void(0)"  file_id='<%=file.get("file_id",0l) %>' onclick="openOfficeFileOnlineReturnFile(this,'<%=ConfigBean.getStringValue("systenFolder")%>','returnImg')" file_is_convert='<%=file.get("file_is_convert",0) %>'><%=file.getString("file_name") %></a>
				 			 	 <%	}
									else
									{
								%>
				 			 	 	  	<a href='<%= downLoadFileAction%>?file_name=<%=file.getString("file_name") %>&folder=returnImg'><%=file.getString("file_name") %></a> 
				 			 	 <%
				 			 	 	}
								%>
								&nbsp;<%=null == adminMgrLL.getAdminById(file.getString("upload_adid"))?"":adminMgrLL.getAdminById(file.getString("upload_adid")).getString("employe_name") %>
								<% if(!"".equals(file.getString("upload_time"))){
										out.println("&nbsp;"+DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(file.getString("upload_time"))));
									}
								%>
										&nbsp;<a href="javascript:deleteFile('<%=file.get("file_id",0l) %>','<%=file.getString("file_name") %>')">删除</a>
									</p><br/>
						<%	
								}
							}
		 				%>
	 			</td>
	 			<td style="padding:5px;">
	 				<span style="color: blue;">备注</span>:<%=rowTemp.getString("note") %>
	 			 	<%
	 			 		int returnLogsCount = returnProductOrderMgrZyj.getReturnProductLogsCountByIdTypeStatus(rowTemp.get("rp_id", 0L),0,0);
	 			 		if(returnLogsCount > 0)
	 			 		{
	 			 			ReturnProductLogTypeKey returnProductLogTypeKey = new ReturnProductLogTypeKey();
	 			 			DBRow[] returnLogs = returnProductOrderMgrZyj.getReturnProductLogsByIdTypeStatus(rowTemp.get("rp_id",0L),0,0);
	 			 			int displayCount = returnLogsCount>3?3:returnLogsCount;
	 			 	%>
	 			 		<div style="border-top: 2px silver dotted;line-height: 20px;padding-top: 2px;padding-bottom: 2px;float: left; width: 100%;">
	 			 			<%
	 			 				for(int m = 0; m < displayCount; m ++)
	 			 				{
	 			 			%>		
	 			 					<p>
	 			 						<span style="color: #f60;"><%=returnProductLogTypeKey.getReturnProductLogTypeName(returnLogs[m].get("follow_type",0)) %></span>:
	 			 						<span style="font-weight: bold;"><%=null==adminMgrLL.getAdminById(returnLogs[m].getString("operator"))?"":adminMgrLL.getAdminById(returnLogs[m].getString("operator")).getString("employe_name")%></span>&nbsp;
	 			 						<span style="color: silver;"><%=returnLogs[m].getString("operate_time").substring(5,16)%></span>
	 			 					</p>
	 			 					<p style="border-bottom: 2px silver dotted;line-height: 20px;padding-top: 2px;padding-bottom: 2px;float: left; width: 100%;">
	 			 						<%=returnLogs[m].getString("content") %>
	 			 					</p>
	 			 			<%		
	 			 				}
	 			 			%>
	 			 			<%
	 			 				if(returnLogsCount > 3)
	 			 				{
	 			 			%>
	 			 				<a href="javascript:void(0)" onclick='returnOrderLogs(<%=rowTemp.get("rp_id",0L)%>)' style="color:green;">更多</a>
	 			 			<%
	 			 				}
	 			 			%>
	 			 		
	 			 		</div>	
	 			 	<%		
	 			 		}
	 			 	%>
	 			 	
	 			 	
	 			</td>
	 		</tr>
	 		<tr class="split">
				<td colspan="4" align="right">
					<input type="button" value="退货部件明细" class="long-button" style="cursor:pointer;" onclick="addDetail('<%=rowTemp.get("rp_id",0l) %>','<%=rowTemp.get("create_type",1) %>');" />&nbsp;
	 					<!-- 关联单据的按钮 -->
	 					<%
	 						//当创建此退货单的是仓库，则显示其与哪个单据关联
							if(1 == rowTemp.get("create_type",0))
							{
 						%>
 						<input type="button" value="关联单据" class="long-button" onclick="relationService('<%=rowTemp.get("rp_id",0l) %>');"/>&nbsp;
 						<%
							}
						%>
	 				 <!-- 如果图片识别完成 应该有按钮 这个时候弹出窗口去确定完成。可以有和服务单关联上-->
	 				 <!-- 图片识别完成 && 退货没有完成。出现这个按钮 -->
	 				<%if(rowTemp.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish) == ReturnProductPictureRecognitionkey.RecognitionFinish)
	 				{
	 					int isCanRegister = adminLoggerBean.getPs_id() == rowTemp.get("ps_id",0L)?1:2;
	 					if(ReturnProductKey.FINISHALL == rowTemp.get("status",0))
	 					{
	 				%>
 	 						<input type="button" value="退货已完成" class="long-button" style="cursor:pointer;" onclick="viewReturnProductRegister('<%=rowTemp.get("rp_id",0l) %>');">
	 				<%		
	 					}
	 					else
	 					{
	 				%>
	 						<input type="button" value="登记退货" class="long-button" style="cursor:pointer;" onclick="returnProductRegister('<%=rowTemp.get("rp_id",0l) %>','<%=rowTemp.get("oid",0l) %>','<%=isCanRegister %>');">
 	 						<input type="button" value="退货完成" class="long-button" style="cursor:pointer;" onclick="returnProductEnforcementFinish('<%=rowTemp.get("rp_id",0l) %>');">
	 				<%		
	 					}
	 				
	 				}
	 				%>
	 				<%
	 					if(2 != rowTemp.get("is_need_create_bill",0))
	 					{
	 				%>
	 					<input type="button" value="无需创建账单" class="long-button" style="cursor:pointer;" onclick="returnNotNeedCreateBill('<%=rowTemp.get("rp_id",0l) %>');">
					<%
	 					}
					%>
				</td>	 		
	 		</tr>
 	<%
 		 }
 		}else{ %>
 			<tr>
	 			<td colspan="4" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 		</tr>
 	<%} %>
</tbody>
</table>
<script>
$("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show: function(event, ui) {
			
		},
		select: function(event, ui) {
		
			if(ui.index * 1 == 2){
			    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				$("#tuijiancaijiForm").submit();
			}
			if(ui.index * 1 == 3){
			    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				$("#recognitionForm").submit();
			}
			if(ui.index * 1 == 4){
			    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				$("#returnProductItemHandleForm").submit();
			}
			if(ui.index * 1 == 5){
			    $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				$("#archivingForm").submit();
			}
		}
	});
function go(numb){$("#page_no").val(numb);$("#pageForm").submit();}
	
	
</script>
<br>
 <form name ="pageForm" id="pageForm" method="post">
 	<input type="hidden" name="p" id="page_no"/>
 	<input type="hidden" name="cmd" value= "<%=cmd %>"/>
 	<input type="hidden" name="create_adid" value= "<%=create_adid %>"/>
 	<input type="hidden" name="end" value= "<%=input_en_date %>"/>
 	<input type="hidden" name="st" value= "<%=input_st_date %>"/>
 	<input type="hidden" name="is_print_over" value= "<%=is_print_over %>"/>
 	<input type="hidden" name="return_product_source" value= "<%=return_product_source %>"/>
 	<input type="hidden" name="return_product_type" value= "<%=return_product_type %>"/>
 	<input type="hidden" name="status" value= "<%=status %>"/>
 	<input type="hidden" name="gather_picture_status" value='<%=gather_picture_status %>'>
 	<input type="hidden" name="is_need_return" value='<%=is_need_return %>'/>
 </form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
   
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br>
  	 <script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
 
$('.st , .end').datepicker({
	dateFormat:"yy-mm-dd",
	changeMonth: true,
	changeYear: true
});
$("#ui-datepicker-div").css("display","none");
</script>
</body>

</html>