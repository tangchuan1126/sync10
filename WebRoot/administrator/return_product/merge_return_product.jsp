<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ReturnProductItemHandleResultRequestTypeKey"%>

<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>图片关联退货</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />
 <style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.opacityrollover.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>

<!-- auto compelet -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<%
	String fix_rpi_ids = StringUtil.getString(request,"fix_rpi_ids");
 	String[] array = fix_rpi_ids.split(",");
 	long rp_id = StringUtil.getLong(request,"rp_id");
 	ReturnProductItemHandleResultRequestTypeKey handleResultRequestTypeKey = new ReturnProductItemHandleResultRequestTypeKey();
	ArrayList<String> returnRequestTypelist = ( ArrayList<String> )handleResultRequestTypeKey.getStatus();
	DBRow row = returnProductMgrZr.getReturnProductByRpId(rp_id);
	boolean isRelationOrder = false; //是否已经关联上单据(订单,运单,服务单);
	if(row != null){
		isRelationOrder = (row.get("sid",0l) != 0l) || (row.get("wid",0l) != 0l) ||( row.get("oid",0l) !=0l ); 
	}
%>
<style type="text/css">
html,body{background-image:url("../js/picture_online_show/images/bg.jpg");background-repeat:repeat;}
*{margin:0px padding:0px;font-size:12px;}
div.content{margin:0px auto;}


div.image_div{width:230px;margin:0px auto;list-style-type:none;display:block;padding:2px;background:white;box-shadow: 0 1px 3px #BBBBBB;}
div.image_div img{width:230px}
div.image_div:hover{cursor:pointer;}

a.abs{ color: #222222;overflow: hidden;padding: 0 0 6px;vertical-align: baseline;overflow: hidden;padding: 0 0 6px;display: block;font-size: 12px;line-height: 14px;text-decoration: none;}
div.info{padding:12px 6px 5px;}
.smooth_zoom_preloader {background-image: url("../js/picture_online_show/images/preloader.gif");}	
.smooth_zoom_icons {background-image: url("../js/picture_online_show/images/icons.png");}
span.size_info{color:#f60;} 
.pp_description{line-height:25px;}
div.active{box-shadow: 0 1px 3px #f60;}
</style>
<script type="text/javascript">
var isAddProduct = 0 ;
function imageLoad(_this){
	var node = $(_this);
	var image = new Image();
	image.src = node.attr("src");
	image.onload = function(){
		var imageHeight =  image.height ;
		var imageWidth = image.width;
		 
		var changeHeight = imageHeight * (230 /imageWidth );
		node.attr("height",changeHeight+"px");
		var liNode = (node.parent().parent().parent());
		var spanSize = $(".size_info",liNode).html(imageWidth+"x"+imageHeight);
	};
}	
 
 function merge(){
	 
	if(!validate()){return ;}
	 
  
 	var submitForm = $("#myform");
	var myCheckBox = $("#myCheckBox");
 	var addUriStr = "" ;
 	if(myCheckBox.attr("checked")){
 	   addUriStr = "&all_select=1";
 	}
 	var testCheckBox = $("#testCheckBox");
 	if(testCheckBox.attr("checked")){
 	   addUriStr += "&is_test=1"
 	}
 
 	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/returnProduct/MergeReutnProductAction.action',
		data:submitForm.serialize() +addUriStr ,
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){
				$.artDialog && $.artDialog.close();
				$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();	
			}
		}
 	})
 }
 
 function changeTest(_this){
	var node = $(_this);
	if(node.attr("checked")){
	 
		$("#check_product_context").attr("disabled",false);
	}else{
	    $("#check_product_context").attr("disabled",true);
	}
}
 function removeCheckSelected(){
     $(".item_check").attr("checked",false);
  }
 function selectCheck(_this){
     var node = $(_this);
     var p_name = node.attr("p_name");
     var quantity = node.attr("quantity");
     if(node.attr("checked")){
         removeCheckSelected();
         node.attr("checked",true);
         setValue(p_name,quantity);
     }
  }
 function setValue(p_name , quantity){
     $("#p_name").val(p_name);
     $("#quantity").val(quantity);
     getProduct();
  }
 function getProduct(){
     
	var p_name = $("#p_name").val();
	var quantity = $("#quantity").val();
	if($.trim(quantity).length < 1){
		showMessage("请先输入数量","alert");
		$("#quantity").focus();
		return ;
	}
	if(p_name.length < 1){
		showMessage("请先输入商品","alert");
		$("#p_name").focus();
		return ;
	}
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/return_product/product_items.html",
		data:{p_name:p_name,quantity:quantity},
		dataType:'html',
		beforeSend:function(request){
	     		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success:function(data){
		    $.unblockUI();
			var product_item_tr = $("#product_item_tr");
			product_item_tr.attr("style","");
			$("#product_item").html(data);
		},
		error:function(data){
		    $.unblockUI();
		    showMessage("系统错误.","error");
		}
	})
}
 function validate(){
     var p_name = $("#p_name").val();
 	if($.trim(p_name).length < 1){
 		showMessage("请先输入商品.","alert");
 		$("#p_name").focus();
 		return false;
 	}
 	
 	var quantity = $("#quantity").val();
 	if($.trim(quantity).length < 1){
 		showMessage("请先输入商品数量","alert");
 		$("#quantity").focus();
 		return false;
 	}
 	var handle_result_request = $("#handle_result_request");
 	if(handle_result_request.val() * 1 == -1){
 	    showMessage("请先选择处理要求","alert");
 	    $("#handle_result_request").focus();
 		return false ;
 	}
 	if(isAddProduct * 1 == 0){
 		showMessage("请先添加退货商品","alert");
 		return false ;
 	}
 	return true ;
     
 }
</script>
</head>
<body>
	 <div style="z-index:100;width:100%;">
	 	<div id="innerHTML"  style="repeat-x scroll 0 0 transparent;padding:10px;border-bottom:1px solid silver;background:white;padding-top:0px;">
	 		<form id="myform">
	 			<input type="hidden" name="fix_rpi_ids" value='<%=fix_rpi_ids %>'/>
	 			<input type="hidden" name="rp_id" value='<%=rp_id %>' />
	 		 <table>
 	 			<%if(isRelationOrder){ %>
 	 				<tr>
 	 					<td class="right">关联单据:</td>
 	 					<td>
 	 						<%
 	 						boolean isOrder = false ;
 	 						DBRow[] items = null ;
 	 						if(row.get("sid",0l) != 0l ){
 	 							items =  serviceOrderMgrZyj.getServiceOrderItemsByServiceId(row.get("sid",0l));
 	 						}else if(row.get("oid",0l) != 0l){
 	 							isOrder = true ;
 	 							items =  orderMgr.getPOrderItemsByOid(row.get("oid",0l));
 	 						}else if(row.get("wid",0l) != 0l){
								items = wayBillMgrZJ.getWayBillOrderItems(row.get("wid",0l));; 	 							
 	 						}
 	 						%>
 	 						<%if(items != null && items.length > 0){ %>
 	 							<div style="margin-left:2px;">
 	 								<fieldset class="set" style="padding-left:14px;border:2px solid #993300;">
					 					<legend>
												<span class="title">
												 关联明细 
												</span> 
										</legend>
 	 								<%for(DBRow item : items){ %>
 	 								 		<input type="checkbox" onclick="selectCheck(this)" quantity="<%=item.get("quantity",0.0f)  %>" p_name="<%=isOrder?item.getString("name"):item.getString("p_name") %>" class="item_check"/><%=isOrder?item.getString("name"):item.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> <%=item.getString("unit_name") %></span> <br />
 	 								<%} %>
 	 								</fieldset>
 	 							</div>
 	 						<%} %>
 	 						
 	 					</td>
 	 				</tr>
 	 			<%} %>
 	 			<tr>
 	 				<td class="right" >商品名:&nbsp;</td>
 	 				<td>
 	 					<input type="text" id="p_name" class="p_name" name="p_name"/>
 	 					&nbsp;&nbsp;数量:&nbsp;<input type="text" value="1" id="quantity" name="quantity" style="font-weight:bold;color:#FF3300;width:35px;"/>
 	 					<input type="button" onclick="getProduct();" value="添加商品" class="short-button"/>
 	 				</td>
 	 			</tr>
 	 			 <tr id="product_item_tr" style="display:none;">
 	 			 	<td></td>
 	 			 	<td id="product_item">
 	 			  
 	 			 	</td>
 	 			 </tr>
 	 			<tr>
 	 				<td class="right">处理要求:&nbsp;</td>
 	 				<td>
 	 					<select id="handle_result_request" name="handle_result_request">
 	 		 	 	 				<option value="-1">处理要求</option>
 	 		 	 	 				<%for(String str : returnRequestTypelist){ %>
 	 		 	 	 					<option value="<%=str %>"><%=handleResultRequestTypeKey.getStatusById(Integer.parseInt(str)) %></option>
 	 		 	 	 				<%} %>
 	 		 	 	 	</select>
 	 				</td>
 	 			</tr>
 	 			<tr>
 	 				<td class="right">退货测试:&nbsp;</td>
 	 				<td><input type="checkbox" id="testCheckBox" onclick="changeTest(this)"/>&nbsp;是</td>
 	 			</tr>
 	 			<tr>
 	 				<td class="right">测试要求:&nbsp;</td>
 	 				<td>
 	 					<textarea name="check_product_context" disabled="disabled" id="check_product_context" style="width:500px;height:70px;"></textarea>
 	 				</td>
 	 			</tr>
 	 		 	<tr>
 	 		 		<td class="right"></td>
 	 		 		<td><input type="button" onclick="merge()" value="添加" class="short-button"/></td>
 	 		 	</tr>
 	 		</table>
 	 		</form>
	 	</div>
 	 </div>
	 <%
	 	String basePath = ConfigBean.getStringValue("systenFolder") + "upload/" +  systemConfig.getStringConfigValue("file_path_return_product_detail")+"/";
	 	for(String fix_rpi_id : array){
	 		int file_with_id = Integer.parseInt(fix_rpi_id);
	 		DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(file_with_id,FileWithTypeKey.RETURN_PRODUCT_DETAIL);
	 			 if(files != null && files.length > 0){
	 		%>	
			 		 
			 			 <%for(DBRow fileTemp : files){
			 			  	String filePath = basePath + fileTemp.getString("file_name");
			 			  %>
			 			 	<div class="image_div" style="float:left;margin:5px;" id="div_pictrue_<%=fileTemp.get("file_id",0l) %>">
								<a class="image_a" href='<%=filePath %>' rel="prettyPhoto[gallery2]" title ='文件名：<%= fileTemp.getString("file_name")%>'>
									<img src='<%=filePath%>' onload="imageLoad(this)" width="230px"/>
								</a>
								 
							</div>
						
			 			 <%} %>
			 		 
	 		<% 
	 			 }
	 	}
	 %>
<script type="text/javascript">
	jQuery(function($){
	   
		
		var canvasWidth =  500;
		var canvasHeight = 347;
	 
		$("a.image_a").prettyPhoto({
			default_width: canvasWidth,
			default_height: canvasHeight,	
			 slideshow:false, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.50, /* opacity of background black */
			theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			modal: true, /* If set to true, only the close button will close the window */	
			overlay_gallery: false,
			changepicturecallback: setZoom,
			callback: closeZoom,
			social_tools: false,
			image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><img id="fullResImage" src="{path}" /></div>',
			fixed_size: true,
			 
			responsive: false,
			responsive_maintain_ratio: true,
			max_WIDTH: '',
			max_HEIGHT: ''
		});	
		
	})
	function setZoom (){
		$('#fullResImage').smoothZoom('destroy').smoothZoom();
	}

	function closeZoom (){
		$('#fullResImage').smoothZoom('destroy');
	}
	addAutoComplete($("#p_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"p_name");
</script>
</body>
</html>