<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.key.ReturnProductKey"%>
<%@page import="com.cwc.app.key.ReturnProductSourceKey"%>
<%@page import="com.cwc.app.key.ReturnProductPictureRecognitionkey"%>
<%@page import="com.cwc.app.key.ReturnProductCheckItemTypeKey"%>
<%@page import="com.cwc.app.key.StorageReturnProductTypeKey"%>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@ include file="../../include.jsp"%> 
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>服务单详细</title>
 
<%
String type = StringUtil.getString(request,"type");
long sid = StringUtil.getLong(request,"sid");
boolean isOid = false;
boolean isWid = false ;
String typeStr  ;
DBRow[] returnOrders = new DBRow[0];
if(type.equals("sid")){
	typeStr = "服务单";
	returnOrders = returnProductOrderMgrZyj.getReturnOrderBySid(sid);//退货单
}else if(type.equals("oid")){
	typeStr =  "订单";
	isOid	= true ;
}else{
	isWid = true ;
	typeStr = "运单";
}

DBRow order = null ;
DBRow[] serviceItems = null ;
if(type.equals("oid")){
	//关联订单了 name 改成p_name
	order = orderMgr.getDetailPOrderByOid(sid);
	serviceItems = orderMgr.getPOrderItemsByOid(sid);
}else if(type.equals("sid")){
	DBRow serviceOrder = serviceOrderMgrZyj.getServiceOrderDetailBySid(sid);
	if(serviceOrder != null  && serviceOrder.get("oid",0l) != 0l){
		order  = orderMgrZr.getDetailById(serviceOrder.get("oid",0l),"porder","oid");
	}
	if(serviceOrder != null){
		serviceItems = serviceOrderMgrZyj.getServiceOrderItemsByServiceId(sid);
	}
}else if(type.equals("wid")){
	order = waybillMgrZR.getWaybillInfo(sid);
	serviceItems = wayBillMgrZJ.getWayBillOrderItems(sid);
}





%>
 
<script type="text/javascript">
 	var sid = '<%= sid%>';
 	var type =  '<%= type%>';
 	var typestr = '<%= typeStr%>';
</script>
<style type="text/css">
	table{border-collapse:collapse;}
	table td{border:1px solid silver;line-height:22px;height:22px;}
	table td.left{text-indent:3px;width:200px;}
</style>
</head>
<body>
  	<%if(order != null){ %>
  		<script type="text/javascript">relationFlag = true ;</script>
  			<div>
  				<%if(order != null){ %>
	  				<table>
	  					<thead>
	  						<tr>
	  							<td colspan="2" style="text-align:center;">客户基本信息</td>
	  						</tr>
	  					</thead>
		  		 		<tr>
		  		 			<td class="right">Name:&nbsp;</td>
		  		 			<td class="left">
		  		 			<%
		  		 				if(isWid)
		  		 				{ 
		  		 			%>
 									<%=order.getString("address_name") %>
 							<%
 								}
		  		 				else
		  		 				{
		  		 					if("".equals(order.getString("first_name")) || "".equals(order.getString("last_name")))
		  		 					{
 		 					%>
								<%=order.getString("address_name") %>
	 						<%			
		  		 					}
		  		 					else
		  		 					{
		  		 			%>
									<%=order.getString("first_name") %>&nbsp;<%=order.getString("last_name") %>
							<%	
		  		 					}
								} 
							%>
		  		 			</td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">Street:&nbsp;</td>
		  		 			<td class="left"><%=order.getString("address_street") %></td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">City:&nbsp;</td>
		  		 			<td class="left"><%= order.getString("address_city") %></td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">Country:&nbsp;</td>
		  		 			<td class="left">
		  		 				<%=order.getString("address_country") %>
		  		 				<%if(order.getString("address_country_code").trim().length() > 0){ %>
		  		 				- <%=order.getString("address_country_code") %>
		  		 				<%} %>
		  		 			</td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">State:&nbsp;</td>
		  		 			<td class="left"><%=order.getString("address_state") %></td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">Zip:&nbsp;</td>
		  		 			<td class="left"><%=order.getString("address_zip") %></td>
		  		 		</tr>
		  		 		<tr>
		  		 			<td class="right">Tel:&nbsp;</td>
		  		 			<td class="left"><%=order.getString("tel") %></td>
		  		 		</tr>
		  			</table>
		  			<%} %>
		  			<%
		  				if(serviceItems != null && serviceItems.length > 0 ) {
		  			%>
	  					<fieldset class="set" style="padding-bottom:4px;padding-top:5px;border:2px solid #993300;width: 80%;">
		 					<legend>
									<span class="title">
										<span style="font-weight:bold;"><%=typeStr %>明细</span>
									</span> 
							</legend>
				  			<% for(DBRow item : serviceItems){%>
				  				<%=isOid?item.getString("name"):item.getString("p_name") %> x	<span style="color:blue"><%=item.get("quantity",0.0f) %> </span> <br />
				  			<% }%>
		  				</fieldset>
		  			<%} %>
		  		<%
		  		if(returnOrders.length > 0)
		  		{
		  			DBRow returnRow = returnOrders[0];
		  			ReturnProductKey returnProductKey = new ReturnProductKey();
		  			ReturnProductSourceKey returnProductSourceKey = new ReturnProductSourceKey();
		  			ReturnProductPictureRecognitionkey pictureRecognitionkey = new ReturnProductPictureRecognitionkey();
		  			ReturnProductCheckItemTypeKey checkItemTypeKey = new ReturnProductCheckItemTypeKey();
		  			StorageReturnProductTypeKey returnProductTypeKey = new StorageReturnProductTypeKey();
				%>
				<script type="text/javascript">isServiceHasReturn = true;</script>
		  		<fieldset class="set" style="padding-bottom:4px;padding-top:5px;border:2px solid #993300;width: 80%;">
 				 <legend>
					<span class="title">
						<span style="font-weight:bold;">[<%= returnRow.get("rp_id",0l)%>] 
							<font color="green"><%=  returnProductKey.getReturnProductStatusById(returnRow.get("status",8)) %></font>
							<font color="#f60">[<%=returnProductSourceKey.getStatusById(returnRow.get("return_product_source",1))  %>]</font>
						</span>
					</span> 
				</legend>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">采集:</span>
						<span class="stateValue" style="font-weight:bold;"><%= (returnRow.get("gather_picture_status",0) == 0 ? "图片采集未完成":"图片采集完成") %></span>
					</p>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">图片:</span>
						<span class="stateValue" style="font-weight:bold;"><%= pictureRecognitionkey.getStatusById(returnRow.get("picture_recognition",ReturnProductPictureRecognitionkey.RecognitionNoneFinish))%></span>
					</p>
					<p>
						<span class="alert-text stateName" style="font-weight:bold;">退货:</span>
						<span class="stateValue" style="font-weight:bold;"><%= checkItemTypeKey.getStatusById(returnRow.get("return_product_check",ReturnProductCheckItemTypeKey.OUTCHECK))%></span>
					</p>
					
					<p>
						<span class="alert-text stateName"><img title="退货类型" src="../imgs/invoice.gif" /></span>
  						<span class="stateValue"><%=returnProductTypeKey.getStatusById(returnRow.get("return_product_type",1))%></span>
					</p>
					<p>
  						<span class="alert-text stateName"><img title="操作人" src="../imgs/order_client.gif" /></span>
  						<span class="stateValue">
  							<%
	  							DBRow userInfo = null;
	  						    userInfo = waybillMgrZR.getUserNameById(returnRow.get("create_user",0l));
	  						    String userName = "";
	  							if(userInfo != null ){
	  								  userName =  userInfo.getString("employe_name");
	  							}
  							%>
  							<%=userName %> &nbsp;
  							<%
  								if(1 == returnRow.get("create_type",0))
  								{
  									out.println("(仓库)");
  								}
  								else
  								{
  									out.println("(客服)");
  								}
  							%>
  						</span>
  					</p>
	  				<p>
	  					<span class="alert-text stateName"><img  title="创建时间" src="../imgs/alarm-clock--arrow.png" /></span>
	  					<span class="stateValue"><%=tDate.getFormateTime(returnRow.getString("create_date"))%></span>
	  				</p>
	  				<%
	  					if(0 != returnRow.get("oid",0L) || 0 != returnRow.get("wid",0L) || 0 != returnRow.get("sid",0L))
	  					{
	  				%>
	  				<p>
						<span class="alert-text stateName" style="font-weight:bold;">关联:</span>
						<span class="stateValue" style="font-weight:bold;">
							<%
								if(0 != returnRow.get("oid",0L))
								{
							%>	
								订单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/ct_order_auto_reflush.html?&st=&en=&p=0&business=&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=<%=returnRow.get("oid",0L)%>' target="_blank" style="cursor: pointer;"><%=returnRow.get("oid",0L)%></a>
							<%
								}
								if(0 != returnRow.get("wid",0L))
								{
							%>
								运单:<a href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/waybill/waybill_list.html?cmd=waybillId&search_mode=1&oid=<%=returnRow.get("wid",0L) %>' target="_blank" style="cursor: pointer;"><%=returnRow.get("wid",0L) %></a>
							<%
								}
								if(0 != returnRow.get("sid",0L))
								{
							%>
								服务单:<a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/service_order/service_order_list.html?cmd=search&search_mode=1&key='<%=returnRow.get("sid",0L) %>'" target="_blank" style="cursor: pointer;"><%=returnRow.get("sid",0L) %></a>
							<%
								}
							%>
						</span>
					</p>
					<%
						}
	  				%>
		</fieldset>
		<%}else{%>
			<script type="text/javascript">isServiceHasReturn = false;</script>
		<%} %>
  			</div>
	  	
  	
  	
  	<%}else{ %>
  		<p style="line-height:30px;height:30px;margin-top:20px;margin:0 auto;text-align:center;font-weight:bold;font-size:14px;">
  				<%=typeStr %><font color="#f60">[<%=sid %>]</font>不存在.
  				<script type="text/javascript">relationFlag = false ;</script>
  		</p>
  
  	<%} %>
</body>
</html>