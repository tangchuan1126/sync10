<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long pid = StringUtil.getLong(request,"pid");
long ps_id = StringUtil.getLong(request,"ps_id");
String ps_name = StringUtil.getString(request,"ps_name");
String p_name = StringUtil.getString(request,"p_name");
String cmd = StringUtil.getString(request,"cmd");

if (cmd.equals("add"))//第一次此做定制
{
	//把套装的标准配置初始化到定制购物车中
	customCart.removeSetProduct(session,pid);
	DBRow inSetPros[] = productMgr.getProductsInSetBySetPid(pid);
	for (int i=0; i<inSetPros.length; i++)
	{
		customCart.put2Cart(session,pid,inSetPros[i].get("pc_id",0l),inSetPros[i].get("quantity",0f));
	}
}
else
{
	customCart.copyFinalSet2TempSet(session,pid);
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>定制商品</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type='text/javascript' src='../js/jquery.form.js'></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	/**
	function formatItem(row) {
		return(row[0]);
	}
	
	function formatResult(row) {
		return row[0].replace(/(<.+?>)/gi, '');
	}
	**/

	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
	
	ajaxCartForm();
	
	loadCustomCart();
	
});


function checkForm()
{
	var theForm = document.search_union_form;
	
	if(theForm.p_name.value=="")
	{
		alert("商品名不能为空");
		return(false);
	}
	else if(theForm.quantity.value=="")
	{
		alert("数量不能为空");
		return(false);
	}
	else if(!isNum(theForm.quantity.value))
	{
		alert("请正确填写数量");
		return(false);
	}
	else
	{

				//先检查购物车中的商品在当前仓库是否已经建库
				var ps_id = <%=ps_id%>;
				var para = "set_pid=<%=pid%>&ps_id="+ps_id+"&p_name="+theForm.p_name.value+"&quantity="+theForm.quantity.value;
			
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/putCustomProduct2Cart.action',
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("添加商品失败！");
					},
					
					success: function(msg)
					{				
						if (msg=="ProductNotExistException")
						{
							alert("商品不存在！");
						}
						else if (msg=="ProductUnionSetCanBeProductException")
						{
							alert("套装不能作为定制商品");
						}
						else if (msg=="ProductNotCreateStorageException")
						{
							alert("[<%=ps_name%>] 不发 ["+theForm.p_name.value+"]");
						}
						else if(msg=="ProductDataErrorException")
						{
							alert("商品基础数据有误，请联系调度！")
						}
						else if (msg=="ok")
						{
							//商品成功加入购物车后，要清空输入框
							theForm.p_name.value = "";
							theForm.quantity.value = "";
							loadCustomCart();
							return(false);
						}
						else
						{
							alert(msg);
						}

					}
				});	
				

	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
 
 var cartProQuanHasBeChange = false;
function onChangeQuantity(obj)
{
	if ( !isNum(obj.value) )
	{
		alert("请正确填写数字");
		return(false);
	}
	else
	{
		obj.style.background="#FFB5B5";
		cartProQuanHasBeChange = true;
		return(true);
	}
}
 
 //把表单ajax化
function ajaxCartForm()
{

	var options = {    
		   //target:        '#output1',   // target element(s) to be updated with server response      
		   // other available options:    
		   //url:       url         // override for form's 'action' attribute    
		   //type:      type        // 'get' or 'post', override for form's 'method' attribute    
		   //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)    
		   //clearForm: true        // clear all form fields after successful submit    
		   //resetForm: true        // reset the form after successful submit    
		   // $.ajax options can be used here too, for example:    
		   //timeout:   3000   
   		   beforeSubmit:  // pre-submit callback    
				function(formData, jqForm, options)
				{
					return(true);
				}	
		   ,  
		   success:       // post-submit callback  
				function(responseText, statusText)
				{
					var msg = responseText;
					loadCustomCart();
					cartProQuanHasBeChange = false;
				}
		       
	};
	

   $('#custom_union_form').bind('submit', function() { 
        //绑定submit事件 
        $(this).ajaxSubmit(options); 
        //异步提交 
        return false; 
    });
	

	// pre-submit callback 
	//function showRequest(formData, jqForm, options) { 
		// formData is an array; here we use $.param to convert it to a string to display it 
		// but the form plugin does this for you automatically when it submits the data 
		//var queryString = $.param(formData); 
	 
		// jqForm is a jQuery object encapsulating the form element.  To access the 
		// DOM element for the form do this: 
		// var formElement = jqForm[0]; 
	 
		//alert('About to submit: \n\n' + queryString); 
	 
		// here we could return false to prevent the form from being submitted; 
		// returning anything other than false will allow the form submit to continue 
		//return true; 
	//} 
	 
	// post-submit callback 
	//function showResponse(responseText, statusText)  { 
		// for normal html responses, the first argument to the success callback 
		// is the XMLHttpRequest object's responseText property 
	 
		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'xml' then the first argument to the success callback 
		// is the XMLHttpRequest object's responseXML property 
	 
		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'json' then the first argument to the success callback 
		// is the json data object returned by the server 
	 
		//alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
			//'\n\nThe output div should have already been updated with the responseText.'); 
	//} 


}

</script>


<script type="text/javascript">
function removeCustomProductCart(set_pid,pid){
		var para = "pid="+pid+"&set_pid="+set_pid;
	
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/removeCustomProductCart.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				alert("删除商品失败！");
			},
			
			success: function(msg)
			{
				if (msg=="ok")
				{
					loadCustomCart();
				}
				else
				{
					alert(msg);
				}
			}
		});	
}


function loadCustomCart()
{
	//alert(para);

	$.ajax({
		url: 'custom_product_mini_cart.html',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		data:"set_pid=<%=pid%>",
		
		beforeSend:function(request){
			
		},
		
		error: function(){
			alert("加载购物车失败！");
		},
		
		success: function(html){
			$("#mini_cart_page").html(html);
		}
	});
}

function modQuantity()
{
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modCustomCart.action',
		type: 'post',
		dataType: 'html',
		timeout: 60000,
		cache:false,
		
		beforeSend:function(request){

		},
		
		error: function(){
			alert("修改数量失败！");
		},
		
		success: function(html){
			if (html=="ok")
			{
				loadCustomCart();
			}
			else
			{
				alert(html);
			}
		}
	});
}

function saveCustomProduct(){
		var para = "p_name=<%=p_name%>&set_pid=<%=pid%>&cmd=<%=cmd%>";
		
		if (cartIsEmpty)
		{
			alert("请添加商品");
		}
		else if ( cartProQuanHasBeChange )
		{
			alert("请先保存修改数量");
		}
		else
		{
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/customProduct.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					
				},
				
				error: function(){
					alert("保存失败！");
				},
				
				success: function(msg)
				{
					if (msg=="ok")
					{
						//parent.closeTBWin();
						$.artDialog.opener.loadCart(0);
						$.artDialog.close();
					}
					else
					{
						alert(msg);
					}
				}
			});	
		}
}

var cartIsEmpty = false;
</script>


<style type="text/css">
<!--
.STYLE1 {font-weight: bold}
.STYLE3 {color: #999999}

#union_table td{
height:22px;
border-bottom:1px #dddddd solid;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

 <form name="search_union_form" method="post" action="">
 <input type="hidden" name="setpid" value="<%=pid%>">
<br>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="3">

  <tr> 
    <td width="94%" height="30" align="left" bgcolor="#D3EB9A" style="padding-left:10px;">
	<strong><span class="STYLE1">
	商品
	</span></strong>      <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:150px;"/> 
	<strong>数量 </strong>
        <input name="quantity" type="text" id="quantity"  style="color:#FF3300;width:40px;"/>
&nbsp;&nbsp;
      <input name="Submit" type="button" class="short-button-yellow" value="添加" onClick="checkForm()">	</td>
  </tr>
  
</table>
</form>
<br>

 <form name="custom_union_form" id="custom_union_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modCustomCart.action" >
 <input type="hidden" name="set_pid" value="<%=pid%>">
 <input type="hidden" name="p_name" value="<%=p_name%>">
 <input type="hidden" name="cmd" value="<%=cmd%>">
 <table width="95%" border="0" align="center" cellpadding="2" cellspacing="3">
   <tr>
     <td>
	 
 <fieldset style="border:2px #cccccc solid;padding:7px;width:95%;background:#FFFFFF;-webkit-border-radius:5px;-moz-border-radius:5px;">
<legend style="font-size:12px;font-weight:bold;color:#000000;"><%=p_name%><%=cmd.equals("add")?"（定制）":""%></legend>
<table width="100%" border="0" cellspacing="2" cellpadding="3" id="union_table">
 <tbody>
<div id="mini_cart_page" style="color:#CCCCCC">数据加载中……</div>
</tbody>
</table>
 </fieldset>	 </td>
   </tr>
   <tr>
     <td align="right">
	 <br>
      <input name="Submit2" id="save_custom_but" type="button" class="long-button-yellow" value="保存定制" onClick="saveCustomProduct()">	 </td>
   </tr>
 </table>
 </form>
 <br>
</body>
</html>
