<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.SystemConfig"%>
<%@ include file="../../include.jsp"%> 
<%
long pc_id = StringUtil.getLong(request,"pc_id");
long catalog_id = StringUtil.getLong(request,"cat_id");
String fromIframe = StringUtil.getString(request,"iframe");
if(catalog_id>0){
	DBRow cat = catalogMgr.getDetailProductCatalogById(catalog_id);
	int[] cat_ids = {0,0,0};	
	cat_ids[0] = cat.get("id", 0);
	
	for(int i =0, parent_id = cat.get("parentid",0); parent_id!=0;){
		cat_ids[++i] = parent_id;
		parent_id = catalogMgr.getDetailProductCatalogById(cat_ids[i]).get("parentid", 0);
	}
	%>
	<script>
		var cat_ids_array = [<%=cat_ids[0]%>,<%=cat_ids[1]%>,<%=cat_ids[2]%>];
	</script>
	<%
}	
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Manage Product Category</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<%-- 
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
--%>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

 <script type="text/javascript" src="../js/fg.menu/fg.menu.js"></script>
    
 <link type="text/css" href="../js/fg.menu/fg.menu.css" media="screen" rel="stylesheet" />
 <link type="text/css" href="../js/fg.menu/theme/ui.all.css" media="screen" rel="stylesheet" />
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<script>
var selected_cat_ids = [0,0,0];
	function addCondition(id,name,type){
	
	$('#condition_block').css('display','block');
	$('#condition_place_holder').css('display','none');
	$("#lastCatalog").val(id);
	if(type=='catalog_first'){
		$('#catalog_first').css('display','none');
		$('#catalog_2').css('display','block');
		selected_cat_ids[0] = id;
		ty='Category: ';
	}else if(type=='catalog_2'){
		$('#catalog_first').css('display','none');
		$('#catalog_2').css('display','none');
		$('#catalog_3').css('display','block');
		selected_cat_ids[1] = id;
		ty='Sub-Category: ';
	}else if(type=='catalog_3'){
		$('#catalog_first').css('display','none');
		$('#catalog_2').css('display','none');
		$('#catalog_3').css('display','none');
		selected_cat_ids[2] = id;
		ty='Sub-Sub-Category: ';
	}
	
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;">'+ty+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';
	var av=$(html).appendTo($('#condition')); //添加到页面

	$(av).click(function(){   //绑定删除事件
	
		if($(this).attr('type')=='catalog_first'){
			$('#catalog_first').css('display','block');
			$('#catalog_2').css('display','none');
			$('#catalog_3').css('display','none');
			$("div[type='catalog_first']",$('#condition')).remove();//同时去掉子级	
			$("div[type='catalog_2']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalog_3']",$('#condition')).remove();//同时去掉子级
			$("#lastCatalog").val("");
			selected_cat_ids=[0,0,0];
		}else if($(this).attr('type')=='catalog_2'){
			$('#catalog_2').css('display','block');
			$('#catalog_first').css('display','none');
			$('#catalog_3').css('display','none');
			$("div[type='catalog_2']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalog_3']",$('#condition')).remove();//同时去掉子级
			selected_cat_ids[1] = 0;
			selected_cat_ids[2] = 0;
			$("#lastCatalog").val(""+selected_cat_ids[0]);
			ajaxLoadPurchasePage(selected_cat_ids[0],2,$("div[type='catalog_first']",$('#condition')).type,$("div[type='catalog_first']",$('#condition')).title,false);
		}else if($(this).attr('type')=='catalog_3'){
			$('#catalog_3').css('display','block');
			$('#catalog_2').css('display','none');
			$('#catalog_first').css('display','none');
			$("div[type='catalog_3']",$('#condition')).remove();//同时去掉子级
			selected_cat_ids[2] = 0;
			$("#lastCatalog").val(""+selected_cat_ids[1]);
			ajaxLoadPurchasePage(selected_cat_ids[1],3,$("div[type='catalog_2']",$('#condition')).type,$("div[type='catalog_2']",$('#condition')).title,false);
		}
		
		$('#'+this.id).remove();  //删除当前条件
		
		if($('#condition').html()==''){
			$('#condition_block').css('display','none');
			$('#condition_place_holder').css('display','block');
		} 
		
	});
}
	var firstTimeLoad = true;
	function ajaxLoadPurchasePage(id,divId,parent_container,name,add_condition)
		{
		
			var para = "id="+id+"&count="+divId;
			$.ajax({
				url: 'product_catalog_menu.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					//var c=divId;	// check if any children of previously selected category exist, clear it.
					//while($("#catalog_"+c).length){$("#catalog_"+c).html("");c=c+1;}
					
					if(add_condition) addCondition(id,name,parent_container);
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#catalog_"+divId).html(html);
					
					$("#lastCatalog").val(id);
					
					//onLoadInitZebraTable();//表格斑马线
				}
			});
		}
		
		function ajaxModProductCatalog()
		{	
			if(!<%=fromIframe%>){
			
				document.mod_product_catalog_form.catalog_id.value = $("#lastCatalog").val();
				document.mod_product_catalog_form.submit();
			}
			else{
			
				var f = document.mod_product_catalog_form;
				var para ="catalog_id="+$("#lastCatalog").val();
				
				$.ajax({
						url:  '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductWithCatalog.action',
						type: 'post',
						timeout: 60000,
						cache:false,
						data:para,
						async:false,
						
						beforeSend:function(request){
						},
						
						error: function(jqXHR, textStatus, errorThrown) {
						  
						},
						
						success: function(data)
						{	
							showMessage("Product Category added successfully","succeed");
							
						}
					});
			
			}
			
		}
		function checkSelectedCategory(cat_id,level,parent_container,name){
		
			if (cat_ids_array.length>0){
				if($.inArray(cat_id,cat_ids_array)!=-1){
					ajaxLoadPurchasePage(cat_id,level,parent_container,name,true);
					cat_ids_array = jQuery.grep(cat_ids_array, function(value) {
					  return value != cat_id;
					});
				}
			}
		
		}
</script>
<style type="text/css">
	body { font-size:62.5%; margin:0; padding:0; }
	#menuLog { font-size:1.4em; margin:20px; }
	.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
	
	.fg-button { clear:left; margin:0 4px 40px 20px; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
	.fg-button .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
	a.fg-button { float:left;  }
	button.fg-button { width:auto; overflow:visible; }  /*removes extra button width in IE */
	
	.fg-button-icon-left { padding-left: 2.1em; }
	.fg-button-icon-right { padding-right: 2.1em; }
	.fg-button-icon-left .ui-icon { right: auto; left: .2em; margin-left: 0; }
	.fg-button-icon-right .ui-icon { left: auto; right: .2em; margin-left: 0; }
	.fg-button-icon-solo { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
	
	.fg-button.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
#cat_table span{padding:2px 5px; margin:5px 0px; display:block;float:left;width:22%}

#cat_table span.selected{background-color:#E4FDBB;}
.cat_item{font-size:13px;color:#069;text-decoration:none;outline:none;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="mod_product_catalog_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductWithCatalog.action">
	<input type="hidden" name="pc_id" value="<%=pc_id%>"/>
	<input type="hidden" name="catalog_id"/>
</form>
<div id="condition_block" style="border-bottom:1px #CCC dashed; display:none;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td width="10%" height="33px;" align="right">
				<span style="font-weight:bold;font-size:12px;">Selected:</span>
			</td>
			<td width="90%" valign="middle" >
				<div id="condition" style="min-width; float:left"></div>
			</td>
		</tr>
	
				
	</table>
</div>

<%
	DBRow ca1[] = catalogMgr.getProductCatalogByParentId(0,null);
	
%>

	<table width="100%" height="330px" id="cat_table">
		<tr>
			<td valign="top">
				
				<div id="catalog_first" >
					<div style="padding:5px;background-color:#AAAAAA;color:#fff;">Parent Category:</div>
					<div style="padding:10px;">
						<%
							for(int i=0;i<ca1.length;i++)
							{
						%>
						<span id="category_span_<%=ca1[i].get("id",0l)%>"  title="<%=ca1[i].getString("title") %>">
							<a href="#catalog_first" class="cat_item" onclick="ajaxLoadPurchasePage(<%=ca1[i].get("id",0l)%>,2,'catalog_first','<%=ca1[i].getString("title")%>',true)"><%=ca1[i].getString("title") %></a>
						</span>
						<script>
							checkSelectedCategory(<%=ca1[i].get("id",0l)%>,2,"catalog_first",'<%=ca1[i].getString("title")%>');
						</script>
						<%
							}
						%>
					</div>
				</div>
				<div id="catalog_2" style="padding:10px 0px;">
				</div>
				<div id="catalog_3" style="padding:10px 0px; ">
				</div>
				<input id="lastCatalog" type="hidden"/>			
			</td>
		</tr>	
		</table>
		
				<div id="condition_place_holder" style="height:35px;">&nbsp;</div>
		<table width="100%" >
		<tr>
			<td align="right" valign="bottom" >				
				<table width="100%" style="" class="win-bottom-line place_at_bottom">
					<tr>
						<td colspan="2" align="right" valign="middle" >				
						  <input type="button" name="Submit2" value="OK" class="normal-green" onClick="ajaxModProductCatalog()">
						<%if(fromIframe.equals("false")){%>  
							<input type="button" name="Submit2" value="Cancel" class="normal-white" onClick="$.artDialog.close()">
						<%} %>	
						</td>
					</tr>
				</table>
			</td>
		</tr>	
	</table>
	<script>firstTimeLoad=false;</script>
</body>
</html>
