<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.CodeTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
long pc_id = StringUtil.getLong(request,"pc_id");
DBRow[] productCodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);

CodeTypeKey codeTypeKey = new CodeTypeKey();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Mod Product Codes</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script type="text/javascript">
	function addProductCode()
	{
		if($("#p_code").val().length>0)
		{
			document.add_product_code_form.submit();
		}
		else
		{
			alert("Please Input Code");
			$("#p_code").focus();
		}
	}
	function windowClose()
	{
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.modCatalogCloseWin  && $.artDialog.opener.modCatalogCloseWin(<%=pc_id%>);
	}
</script>	
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
	<table width="100%"  border="0" cellpadding="0" cellspacing="0">
	  <tr>
	    <td colspan="2" align="center" valign="top">
					<form name="add_product_code_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_code/addProductCode.action" >
						<input type="hidden" value="<%=pc_id%>" name="pc_id"/>
						<table width="98%" border="0" cellspacing="5" cellpadding="2">
						  <tr>
						    <td width="8%" align="right" valign="middle" class="text-line" >&nbsp;</td>
						    <td width="92%" align="left" valign="middle" >&nbsp;</td>
						  </tr>
						  <tr>
						    <td align="left" valign="middle" class="STYLE1 STYLE2" >Code</td>
						    <td align="left" valign="middle" >
						    	<input name="p_code" type="text" class="input-line" id="p_code" >
						    	<input type="button" name="Submit2" value="Add" class="short-button" onclick="addProductCode()"/>
						    </td>
						  </tr>
						  <tr>
						    <td align="left" valign="middle" class="STYLE3" >Type</td>
						    <td align="left" valign="middle">
						    	<input type="radio" id="main_type" checked="checked" name="code_type" value="<%=CodeTypeKey.Main%>"/>
						    	<label for="main_type">Main Code</label>
						    	<input type="radio" id="amazon_type" name="code_type" value="<%=CodeTypeKey.Amazon%>"/>
						    	<label for="amazon_type">Amazon Code</label>
						    	<input type="radio" id="upc_type" name="code_type" value="<%=CodeTypeKey.UPC%>"/>
						    	<label for="upc_type">UPC Code</label>
						    </td>
						  </tr>
						</table>
					</form>
	    </td>
	  </tr>
	  <tr style="padding-top:10px">
	  	<td colspan="2" valign="top" style="padding-top:30px">
	  		<table width="98%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
	  			<tr>
	  				<th class="right-title" style="text-align: center;">Code</th>
	  				<th class="right-title" style="text-align: center;">Type</th>
	  			</tr>
	  			<%
	  				for(int i=0;i<productCodes.length;i++)
	  				{
	  			%>
	  				<tr style="height:25px;">
	  					<td><%=productCodes[i].getString("p_code")%></td>
	  					<td><%=codeTypeKey.getCodeTypeKeyById(productCodes[i].get("code_type",0))%></td>
	  				</tr>
	  			<%
	  				}
	  			%>
	  		</table>
	  	</td>
	  </tr>
<%--	  <tr>--%>
<%--	    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>--%>
<%--	    <td width="49%" align="right" class="win-bottom-line">--%>
<%--		 	<input type="button" name="Submit2" value="添加" class="normal-green" onclick="addProductCode()"/>--%>
<%--		  	<input type="button" name="Submit2" value="关闭" class="normal-white" onclick="windowClose()"/>--%>
<%--		</td>--%>
<%--	  </tr>--%>
	</table> 
	<script>onLoadInitZebraTable();</script>
</body>
</html>
