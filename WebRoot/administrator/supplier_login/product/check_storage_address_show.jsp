<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.ImportStorageAddressErrorKey" %>
<%@ page import="com.cwc.app.key.StorageTypeKey" %>
<%@page import="com.cwc.service.purchase.exception.zj.FileTypeException"%>
<%

 	DBRow[] rows = null;
 	String fileName = StringUtil.getString(request,"fileNames");
 	int storageType = StringUtil.getInt(request,"storageType");
 	try
 	{
 		HashMap<String,DBRow[]> error = storageCatalogMgrZyj.excelShow(fileName,"show",storageType); 
 		
 		rows = error.get("errorStorageAddress");
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	
 	ImportStorageAddressErrorKey importStorageAddressErrorKey = new ImportStorageAddressErrorKey();
 	StorageTypeKey storageTypeKey = new StorageTypeKey();
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>检测地址</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>


<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>			
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>

$.blockUI.defaults = {
	css: { 
		padding:        '10px',
		margin:         0,
		width:          '200px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #aaa',
		backgroundColor:'#fff'
	},
	
	// 设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.8' 
	},

centerX: true,
centerY: true, 

	fadeOut:  2000
};


</script>
<script type="text/javascript">
	
	function ajaxSaveStorageAdress()
	{
		if(<%=submit%>)
		{
			var tempfilename = $("#tempfilename").val();
			var storageType = $("#storageType").val();	
			
			var para ="tempfilename="+ tempfilename+"&storageType="+storageType;
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/saveImportStorageAddressAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 6000000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					showMessage("提交失败，请重试！","alert");
				},
				
				success: function(date){
					if (date["flag"])
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">保存失败！</span>' });
					}
				}
			});
		}
		else
		{
			alert("<%=msg%>");
		}
	};
	 function closeWin()
		{
			$.artDialog.close();
		}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="onLoadInitZebraTable()" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top" width="98%" align="center">
	<div id="tabs" style="width:95%; height:100%" align="center">
	<ul>
			<li><a href="#error_address">仓库地址信息错误提示</a></li>
	</ul>
			<div id="error_address" align="center">
				<table width="100%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
					 <% 
						if(rows!=null&&rows.length>0)
						{	
							submit = false;
					%>
						<tr>
							<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">仓库名</th>
							<th width="9%" class="right-title "style="vertical-align: center;text-align: left;">仓库类型</th>
							<th width="10%"  class="left-title " style="vertical-align: center;text-align: center;">错误原因</th>
						</tr>
					<%
						for(int i=0;i<rows.length;i++)
						{
					%>
						  <tr align="center" valign="middle">
							<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;">
							  <%=rows[i].getString("title")%>
							&nbsp;</td>
							<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("storage_type") %>&nbsp;</td>
							<td style="border-bottom: 1px solid #dddddd;padding-left:10px;" align="left">
								<% 
									out.print("<font color='red'>位置:仓库地址表内第"+rows[i].getString("errorStorageAddressRow")+"行</font><br/>");
									String[] errorP = rows[i].getString("errorMessage").split(",");
									for(int j = 0;j<errorP.length;j++)
									{
										out.println("<font color='red'>"+(j+1)+".</font>"+importStorageAddressErrorKey.getStorageAddressErrorMessageById(errorP[j].toString()));
										if(j<errorP.length-1)
										{
											out.print("<br/>");
										}
									}
								%>
							</td>
						  </tr>
					  <%
							}
						}
						else
						{
					  %>
							<tr align="center" valign="middle">
							<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">仓库地址信息检查无误</th>
							</tr>
					  <%
					  	}
					  %>
			  </table>
			</div>
			
	</div>
	</td>
  </tr>
  <script>
	$("#tabs").tabs({	
		cookie: { expires: 30000 } 
	});
</script>
 <tr>
 	<td valign="bottom">
 		<table width="100%" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>			  &nbsp;</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveStorageAdress()"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit3" value="取消" class="normal-white" onClick="closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/saveImportStorageAddressAction.action" name="saveStorageAddress_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=fileName%>"/>
	<input type="hidden" name="storageType" id="storageType" value="<%=storageType %>"/>
</form>
</body>
</html>



