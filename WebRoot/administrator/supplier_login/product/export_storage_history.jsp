<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String store_date = StringUtil.getString(request,"store_date");
long psId = StringUtil.getLong(request,"psId");
long productLine = StringUtil.getLong(request,"productLine");

TDate tDate = new TDate();
String input_date ;
if ( store_date.equals("") )
{	
	input_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}
else
{
	input_date = store_date;
}
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
%>
<html>
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<!---// load the mcDropdown CSS stylesheet //--->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script type="text/javascript">
$(document).ready(function(){
	  
	   $("#store_date").date_input();
			  		  
	  });


function closeWindow()
{
	  $.artDialog.close();
}

function checkForm()
{
	var cansubmit = true;

	 if ($("#psId").val()==0)
	{
		alert("请选择仓库");
		cansubmit = false;
	}

	if (cansubmit)
	{
		$.blockUI({ message: '<div style="font-size:12px;font-weight:normal;color:#666666;padding:15px;">正在导出，请稍后......</div>'});
			var para = "productLine="+$("#product_line_id").val()+"&psId="+$("#psId").val()+"&store_date="+$("#store_date").val();
		
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ExportProductStorageHistory.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(msg)
				{
					if (msg=="WareHouseErrorException")
					{
						alert("此选项下没有数据,请重新选择！");
						$.unblockUI();
					}
					else
					{
						parent.window.location = "../../"+msg;
						closeWindow();
					}
				}
			});

	}
}

</script>


  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><br />

      <table width="550" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td align="center">日期：</td>
        <td width="466" >
            <input name="store_date" type="text" id="store_date" size="9" value="<%=input_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
		</td>
      </tr>
      <tr>
        <td align="center">仓库：</td>
        <td >
        	  <input type="hidden" name="cmd" value="search">
							<select name="psId" id="psId" style="border:1px #CCCCCC solid"  >
				              <option value="0">请选择...</option>
				          <%
			
									String qx;
									
									for ( int i=0; i<treeRows.length; i++ )
									{
										if ( treeRows[i].get("parentid",0) != 0 )
										 {
										 	qx = "├ ";
										 }
										 else
										 {
										 	qx = "";
										 }
										 
										 if (treeRows[i].get("level",0)>1)
										 {
										 	continue;
										 }
									%>
									          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==psId?"selected":""%> > 
									          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> 
									          <%=qx%> 
									          <%=treeRows[i].getString("title")%> </option>
									          <%
									}
									%>
				        </select>
		</td>
      </tr>
      <tr>
      <td align="center">产品线：</td>
      	<td align="left">
     <ul id="productLinemenu" class="mcdropdown_menu">
    			<li rel="0">所有产品线</li>
					  <%
		
					  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,"2");
						  for (int i=0; i<ca1.length; i++)
						  {
								//out.println("<li onmousemove='catagory("+ca1[i].get("id",0l)+")' value='"+ca1[i].get("id",0l)+"'><a href='"+ca1[i].get("id",0l)+"'> "+ca1[i].getString("chName")+" </a>");
								  DBRow[] ca2 = null;
								  	ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");
								 
								  for (int ii=0; ii<ca2.length; ii++)
								  {
										out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
										
										out.println("</li>");				
								  }
							
					  }
					  %>
				</ul>
					  <input type="text" name="productLine" id="productLine" value="" />
					  <input type="hidden" name="product_line_id" id="product_line_id" value="0" />

       <script>
			$("#productLine").mcDropdown("#productLinemenu",{
								allowParentSelect:true,
								  select: 
								  
										function (id,name)
										{
											$("#product_line_id").val(id);
											//alert($("#filter_lid1").val());
										}
						
						});								
			$("#productLine").mcDropdown("#productLinemenu").setValue(0);
		
			<%
			//System.out.println(productLine);
			if (productLine>0)
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue(<%=productLine%>);
			<%
			}
			else
			{
			%>
			$("#productLine").mcDropdown("#productLinemenu").setValue(0);
			<%
			}
			%>
		</script>
			
    </td>
      </tr>
</table>

	
	
    </td></tr>
  <tr>
    <td align="right" class="win-bottom-line">
      <input type="button" name="Submit2" value="导出" class="normal-green-long" onClick="checkForm();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();">
    </td>
  </tr>
</table> 
  </body>
</html>
