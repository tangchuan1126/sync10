<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
	long waybill_id = StringUtil.getLong(request,"waybill_id");
	
	DBRow[] lackingProducts;
	if(waybill_id==0)
	{
		lackingProducts = productMgr.outStockProductForOrder(request,ps_id);
	}
	else
	{
		lackingProducts = productMgr.outStockProductForWaybill(waybill_id);
	}
	
	
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
 <title>订单拆货建议</title>
 <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
 
 <link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

 <link href="../comm.css" rel="stylesheet" type="text/css">
 <script type="text/javascript">
 	function adviceProduct()
 	{
 		var pcids = "";
		
 		$("[id='check_lacking']").each(function(){
 			if(this.checked)
 			{
 				pcids += this.value+",";
 			}
			
		});
		
 		$.ajax({
			url: 'advice_product.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{pcids:pcids,ps_id:<%=ps_id%>,waybill_id:<%=waybill_id%>},
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				
			},
			
			success: function(html)
			{
				$("#advice_product").html(html);
			}
		});
 	}
 	
 	function openSplit(storage_name,s_pid,s_pc_id,cid)
	{	
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/split_union.html?psid="+cid+"&s_pid="+s_pid+"&s_pc_id="+s_pc_id+"&storage_name="+storage_name+"&cid="+cid, {title: "拆散套装",width:'700px',height:'300px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function openSplitOrder(store_type,s_pid,s_pc_id,cid)
	{	
		var pcids = "";
		$("[id='check_lacking']").each(function(){
 			if(this.checked)
 			{
 				pcids += this.value+",";
 			}
			
		});
		$.artDialog.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/split_union_order.html?psid="+cid+"&s_pid="+s_pid+"&s_pc_id="+s_pc_id+"&pcids="+pcids+"&cid="+cid+"&waybill_id=<%=waybill_id%>&store_type="+store_type, {title: "拆散套装",width:'700px',height:'300px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function refush()
	{
		window.location.reload();
	}
 </script> 
</head>

  <body>
    <table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
    	<tr>
    		<td width="35%" valign="top" height="100%" style="font-size:14px;border-right-width:1px;border-right-style:solid;">
    			<table>
    			<%
    				for(int i = 0;i<lackingProducts.length;i++)
    				{
    			%>
    				<tr>
    					<td valign="middle" nowrap="nowrap">
    						<input onclick="adviceProduct()" id="check_lacking" type="checkbox" checked="checked" value="<%=lackingProducts[i].get("pc_id",0l)%>"/><%=lackingProducts[i].getString("p_name")%>
    					</td>
    					<td align="right" nowrap="nowrap">
    						缺货个数:<%=lackingProducts[i].get("lackingCount",0f)%>
    					</td>
    				</tr>
    			<%
    				}
    			%>
    			</table>
    		</td>
    		<td valign="top">
    			<div id="advice_product">
    			</div>
    		</td>
    	</tr>
    	<tr>
    		<td colspan="2">
    			<table height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
    				<td width="68%" align="left" class="win-bottom-line" style="color:#999999;padding-left:10px;font-size:12px;">
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle">
						<font color="red">提出拆货要求后运单不可取消,请慎重操作</font>
					</td>
		    		<td align="right" class="win-bottom-line" style="color:#999999;font-size:12px;">
		    			<input type="button" name="Submit2" value="关闭" class="normal-white" onClick="$.artDialog.close();">
		    		</td>
    			</table>			
    		</td>
    		
    	</tr>
    </table>
    <script type="text/javascript">
  		adviceProduct();
  		<%
  			if(lackingProducts.length==0)
  			{
  		%>
  				$.artDialog.opener.closeTBWin();
  				$.artDialog.close();
  		<%
  			}
  		%>
  	</script>
  </body>
</html>
