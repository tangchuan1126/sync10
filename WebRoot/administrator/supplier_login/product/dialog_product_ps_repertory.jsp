<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
    long productId=StringUtil.getLong(request,"productId");       //商品id
    long titleId=StringUtil.getLong(request,"title_id");
    String productName=StringUtil.getString(request,"productName");
    long lot_number_id=StringUtil.getLong(request,"lot_number_id"); //批次
	long ps_id=StringUtil.getLong(request,"ps_id");

     DBRow[] rows=mgrZwb.selectWarehouse(productId,titleId,lot_number_id);

%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<title>库存位置</title>

</head>
<body>
   <fieldset style="border:1px #999 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
		
     	 <%for(int i=0;i<rows.length;i++){ %>
	     	 <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
			 	<legend style="font-size:15px;font-weight:bold;"> 
				 		<span style="font-size:13px; font-weight: bold;">仓库:</span>
	        			<span style="font-size:13px; font-weight: bold; color:green">
	        			<%= catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l)).getString("title")%>
	        			</span>
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">可用数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("theoretical_quantity_count",0d) %></span>
			          <span style="font-size:13px; font-weight: bold; margin-left: 20px">物理数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("physical_quantity_count",0d) %></span>
			          <a href="javascript:void(0)" onclick="openLocation('<%=productName%>',<%=productId%>,<%=titleId%>,'','',<%=rows[i].get("ps_id",0l)%>)" style="text-decoration:none; color: green" >
			          	<span style="font-size:13px; font-weight: bold; margin-left: 20px">查看位置</span>
			          </a>
			    </div>
			 </fieldset> 
		 <%}%>
      </fieldset>
      <input type="hidden" id="ps_id" value="<%=ps_id%>"/>
</body>
</html>
<script>

function openLocation(p_name,pc_id,title_id,lot_name,title_name,ps_id){

	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/dialog_product_ps_location.html?productId="+pc_id+"&titleId="+title_id+"&titleName="+title_name+"&productName="+p_name+"&lotName="+lot_name+"&ps_id="+ps_id; 
	$.artDialog.open(uri , {title: '商品位置库存',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}

</script>

