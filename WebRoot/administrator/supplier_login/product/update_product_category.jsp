<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Product Category</title>

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet"
	href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
</head>
<%
	String id = StringUtil.getString(request, "id");
%>
<style>
.error {
	display: none;
}

.error_show {
	color: red;
	margin-left: 10px;
	padding: 3px;
	font-size: 11px;
	border: red 1px solid;
	background-color: #FFF5F5;
}
</style>
<script>
function hide_all_error_messages(){
var form_data= $("#subForm").serializeArray();
for (var input in form_data){
var error_element=$("#"+form_data[input]['name']+"_error");
error_element.removeClass("error_show").addClass("error");
}
}
function validate(){
name = $("#name").val();
id=$("#id").val();
var para ="id="+id+"&name="+name;
hide_all_error_messages();
$.ajax({
url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/updateProductCategoryJSONAction.action',
type: 'post',
timeout: 60000,
cache:false,
dataType: 'json',
data:para,
async:false,
beforeSend:function(request){
},
error: function(jqXHR, textStatus, errorThrown) {
showMessage("Error while updating product category","error");
},
success: function(data){
if(data.availability=="exists"){
show_error_message("name_error","product category '"+name+"' already exists");
}else{
submitData();
}
}
});

}

function submitData(){
var f = document.subForm;
var para ="id="+f.id.value+"&title="+f.name.value;
$.ajax({
type: "POST",
url: "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductCatalog.action",
data:para,
success: function(data)
{
showMessage("product category updated successfully","succeed");
setTimeout("closeWindow()", 1000);
}
});

}
function closeWindow(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
}
function show_error_message(error_box_id,message){
$("#"+error_box_id).html(message);
$("#"+error_box_id).removeClass("error").addClass("error_show");
}

</script>
<body style="margin:0px;">
	<form action="" id="subForm" name="subForm">
		<table width="100%">
			<tr>
				<td valign="top">
					<table cellpadding="4" height="">
						<tr>
							<td>Product Category Name:</td>
							<td><input name='name' type='text' id='name'
								style='width:300px;' /> <input name="id" id="id" type="hidden"
								value="<%=id%>" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><span id="name_error" class="error"></span>
							</td>
						</tr>
					</table></td>
			</tr>
			
		</table>
	</form>
	<div style="vertical-align:bottom;position:absolute;bottom:0;width:100%" >

		<table align="right"  width="100%">
			<tr align="right">
				<td class="win-bottom-line"><input type="button" class="normal-green"
					value="Update" onclick="validate();" />
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript">
var para="parentid="+<%=id%>;
$.ajax({
url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action',
					type : 'post',
					timeout : 60000,
					cache : false,
					dataType : 'json',
					data : para,
					async : false,
					beforeSend : function(request) {
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus, errorThrown);
					},
					success : function(data) {
						$("#name").val(data.title);
					}
				});
	</script>
</body>
</html>