<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.zj.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.api.AdminLoggerBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session);
//DBRow[] titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
String title_id = StringUtil.getString(request, "title_id");				//title
String lot_number_id = StringUtil.getString(request, "lot_number_id");      //批次

//System.out.println(title_id+","+lot_number_id);
int[] status = new int[1];
status[0] = WayBillOrderStatusKey.WAITPRINT;

String pcid = StringUtil.getString(request,"pcid");                           //商品id


PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);
String cid = StringUtil.getString(request,"cid");                    
int type = StringUtil.getInt(request,"type");								//是否有货
String name = StringUtil.getString(request,"name");							//商品名
String cmd = StringUtil.getString(request,"cmd");

String pro_line_id = StringUtil.getString(request,"pro_line_id");		   //产品线或产品分类
int union_flag = StringUtil.getInt(request,"union_flag",-1);

if (cmd.equals("")){
	cid = adminLoggerBean.getPs_id()+"";
}
//System.out.println("title_id:"+title_id+",lot_number_id:"+lot_number_id+",cid:"+cid+",name:"+name+",cmd:"+cmd+",pro_line_id:"+pro_line_id);

DBRow rows[] = productStoreMgrZJ.getProductStoreCount(cid,0,0, title_id,0,"","", 0);




Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

int[] purchase_status = {PurchaseKey.OPEN,PurchaseKey.AFFIRMPRICE,PurchaseKey.AFFIRMTRANSFER,PurchaseKey.AFFIRMTRANSFER,PurchaseKey.DELIVERYING};
int[] delivery_status = {DeliveryOrderKey.READY,DeliveryOrderKey.INTRANSIT,DeliveryOrderKey.APPROVEING};
int[] transport_status = {TransportOrderKey.PACKING,TransportOrderKey.INTRANSIT,TransportOrderKey.APPROVEING};
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>存货与备货</title>


<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {		
	}
	addAutoComplete($("#filter_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"merge_info",
		"p_name");
	
	$("#filter_name").keydown(function(event){
		if (event.keyCode==13){
			searchByName();
		}
	});
	
});


//修改仓库分类
function modStoreCatalog(pid,pcid,pname,cid){

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
String storeCatalogQx;

countryCodeSB.append("<select name='mod_store_cid' id='mod_store_cid' >");
countryCodeSB.append("<option value='0'>选择仓库...</option>");
for (int countryCodeI=0; countryCodeI<treeRows.length; countryCodeI++){
	if ( treeRows[countryCodeI].get("parentid",0) != 0 ){
	 	storeCatalogQx = "├ ";
	 }else{
	 	storeCatalogQx = "";
	 } 
	countryCodeSB.append("<option value='"+treeRows[countryCodeI].getString("id")+"'>");
	countryCodeSB.append(Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[countryCodeI].get("level",0)));
	countryCodeSB.append(storeCatalogQx);
	countryCodeSB.append(treeRows[countryCodeI].getString("title"));  
	countryCodeSB.append("</option>");
	
}
	countryCodeSB.append("</select>");
%>

	$.prompt(
	
	"<div id='title'>修改仓库[商品:"+pname+"]</div><br /> <%=countryCodeSB.toString()%>",
	{
	      submit: promptCheckStoreCatalog,
   		  loaded:
				function (){
					$("#mod_store_cid").setSelectedValue(cid);
				}
		  ,
		  callback: 
				function (v,m,f){
					if (v=="y"){
						document.mod_store_catalog_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modStoreCatalog.action";
						document.mod_store_catalog_form.mod_store_cid.value = f.mod_store_cid;
						document.mod_store_catalog_form.pid.value = pid;
						document.mod_store_catalog_form.pcid.value = pcid;
						document.mod_store_catalog_form.submit();		
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function promptCheckStoreCatalog(v,m,f){
	if (v=="y"){
		  if(f.mod_store_cid == 0){
			   alert("请选择仓库");
			   return false;
		  }
		  return true;
	}
}


function del(){
	if (confirm("确认操作吗？")){
		return(true);
	}else{
		return(false);
	}
}


function selectOne(){
	var flag = false;
	
	if (document.listForm.pid_batch.length>0){
		for (i=0; i<document.listForm.pid_batch.length; i++){
			if(document.listForm.pid_batch[i].checked){
				flag = true;
				break;
			}
		}
	}else{
			if(document.listForm.pid_batch.checked){
				flag = true;
			}
	}
	return(flag);
}

function batchModPrice(){
	if ( !selectOne() ){
		alert("请选择你要修改的商品");
	}else{
		if ( del() ){
			document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductStorage.action";
			document.listForm.submit();
		}
	}
}


function delPro(pid,p_name){
	if ( confirm("确认删除["+p_name+"]库存？") ){
		document.temp_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductStorage.action";
		document.temp_form.pid.value = pid;
		document.temp_form.submit();
	}
}

function rowSel(index){
	if ( document.listForm.pid_batch[index].checked ){
		document.listForm.pid_batch[index].checked = false;
	}else{
		document.listForm.pid_batch[index].checked = true;
	}
}

function onMOBg(row,cl,index){
	if ( !document.listForm.pid_batch[index].checked ){
		row.style.background=cl;
	}
}





function syncode(val){
	document.add_pro_form.code.value=val;
}

function searchByCid(){
	document.search_form.cmd.value = "cid";
	document.search_form.cid.value = $("#filter_cid").val();	
	document.search_form.pcid.value = $("#filter_pcid").val();
	document.search_form.pro_line_id.value = $("#filter_productLine").val();
	document.search_form.type.value = $("#filter_type").val();
	document.search_form.union_flag.value = $("#union_flag").val();	
	document.search_form.title_id.value = $("#title_id").val();	
	document.search_form.lot_number_id.value = $("#lot_number_id").val();	
	document.search_form.submit();
}



function report(){
	document.report_form.action="report.html";
	document.report_form.target="_blank";
	document.report_form.export_flag.value="0";
	document.report_form.submit();
}

function exportData(){
	document.report_form.action="report.html";
	document.report_form.target="_self";
	document.report_form.export_flag.value="1";
	document.report_form.submit();
}

function showAddProductTable()
{
	document.getElementById("add_product_table").style.display="";
}

function hiddenAddProductTable()
{
	document.getElementById("add_product_table").style.display="none";
}

function openConvertProduct(storage_name,s_pid,s_pc_id)
{	
	tb_show('转换商品','convert_product.html?s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function addDamageProduct(storage_name,storage_id,pid,store_count,damage_count)
{	
	tb_show('增加残损商品','add_damaged.html?storage_id='+storage_id+'&pid='+pid+'&store_count='+store_count+'&damage_count='+damage_count+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function returnProduct(storage_name,storage_id,pid)
{	
	//tb_show('退件进库','add_return_product.html?storage_id='+storage_id+'&storage_name='+storage_name+'&pid='+pid+'&TB_iframe=true&height=300&width=650',false);
	
	$.artDialog.open("add_return_product.html?storage_id="+storage_id+"&storage_name="+storage_name+"&pid="+pid, {title: "退件进库",width:'650px',height:'420px',fixed:true, lock: true,opacity: 0.3});
}

function selectLabel(product_id)
{
	tb_show('制作商品标签','../lable_template/lable_template_show.html?pc_id='+product_id+'&TB_iframe=true&height=500&width=800',false);
}

function openRenewProduct(storage_name,s_pid,s_pc_id) 
{	
	tb_show('残损翻新','renew_product.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function openCombination(storage_name,s_pid,s_pc_id,cid)
{	
	tb_show('预拼装套装','combinnation_union.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&cid='+cid+'&TB_iframe=true&height=300&width=650',false);
}

function openSplit(storage_name,s_pid,s_pc_id,cid)
{	
	tb_show('拆散套装','split_union.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&cid='+cid+'&TB_iframe=true&height=300&width=650',false);
}

function convertProduct(s_pid,s_pc_id,p_name,quantity)
{
	document.convert_product_form.s_pid.value = s_pid;
	document.convert_product_form.s_pc_id.value = s_pc_id;
	document.convert_product_form.p_name.value = p_name;
	document.convert_product_form.quantity.value = quantity;
	document.convert_product_form.submit();
}

/**
function addDamageProduct(name,pid,store_count){	
	store_count = store_count*1;

	$.prompt(
	
	"<div id='title'>残损件登记["+name+"]</div><br />新增数量 <input name='proQuantity' type='text' id='proQuantity' style='width:100px;'><br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (!isNum(f.proQuantity))
						{
							alert("请正确填写数量");
							return(false);
						}
						else if (store_count<f.proQuantity)
						{
							alert("残损件数不能比现有库存多");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						if ( confirm("确认["+name+"]新增["+f.proQuantity+"]件残损？") )
						{
							document.add_damaged_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addDamageProduct.action";
							document.add_damaged_product_form.quantity.value = f.proQuantity;
							document.add_damaged_product_form.pid.value = pid;
							document.add_damaged_product_form.submit();
						}
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}



function addReturnProduct(name,pid)
{	
	$.prompt(
	
	"<div id='title'>退货登记["+name+"]</div><br />&nbsp;&nbsp;退件检测 &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   <select name='preDamaged' id='preDamaged'>  <option value='-1' selected>请选择</option>    <option value='0'>完好</option>    <option value='1'>损坏</option>  </select> <br><br>&nbsp;&nbsp;相关订单号 &nbsp;&nbsp;&nbsp;<input name='proOid' type='text' id='proOid' style='width:100px;'> <br><br>&nbsp;&nbsp;数量 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proQuantity' type='text' id='proQuantity' style='width:100px;'><br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.preDamaged==-1)
						{
							alert("请选择退件检测情况");
							return(false);
						}
						else if (!isNum(f.proOid))
						{
							alert("请正确填写订单号");
							return(false);
						}
						else if (!isNum(f.proQuantity))
						{
							alert("请正确填写数量");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{

						if ( confirm("确认订单 ["+f.proOid+"] 退件 ["+name+"] X "+f.proQuantity+"？") )
						{
							document.add_return_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addReturnProduct.action";
							document.add_return_product_form.quantity.value = f.proQuantity;
							document.add_return_product_form.pid.value = pid;
							document.add_return_product_form.oid.value = f.proOid;
							document.add_return_product_form.damaged.value = f.preDamaged;
							document.add_return_product_form.submit();
						}
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

**/

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function reCheckLackingOrders()
{
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};		
			
				$.prompt(
				"<div id='title'>重算缺货</div>确定重新计算缺货订单库存？<br />",
				{
					  
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
									var ps_id = $("#filter_cid").val();
										$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
										$.ajax({
											url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ReCheckLackingOrders.action',
											type: 'post',
											dataType: 'html',
											timeout: 60000,
											cache:false,
											data:{ps_id:ps_id},
											
											beforeSend:function(request){
												
											},
											
											error: function(){
												alert("网络错误，请重试");
											},
											
											success: function(msg){
												if (msg=="ok")
												{
													$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
													$.unblockUI();
													//alert("计算完成！");
													
												}
												else
												{
													alert("计算出错！");
												}
											}
										});		
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
/**
function reCheckLackingOrders()
{
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};
	
	
	if ( confirm("确定重新计算缺货订单库存？") )
	{
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ReCheckLackingOrders.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"",
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				alert("网络错误，请重试");
			},
			
			success: function(msg){
				if (msg=="ok")
				{
					$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
					$.unblockUI();
					//alert("计算完成！");
					
				}
				else
				{
					alert("计算出错！");
				}
			}
		});	
	}

}**/

function demandAndPlanAnalysis()
{
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};
	
	$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/orderProcess/demandAndPlanAnalysis.action',
			type: 'post',
			dataType: 'html',
			timeout: 300000,
			cache:false,
			data:"",
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				alert("网络错误，请重试");
				$.unblockUI();
			},
			
			success: function(msg){
				if (msg=="ok")
				{
					$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
					$.unblockUI();
				}
				else
				{
					alert("计算出错！");
				}
			}
		});	
}

function closeWin()
{
	tb_remove();
}

function renewProduct(psid,pid,pcid,damaged_count_m,damaged_package_count_m)
{
		document.renew_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/RenewProduct.action";
		document.renew_product_form.damaged_count_m.value = damaged_count_m;
		document.renew_product_form.damaged_package_count_m.value = damaged_package_count_m;
		document.renew_product_form.psid.value = psid;
		document.renew_product_form.pid.value = pid;
		document.renew_product_form.pcid.value = pcid;
		document.renew_product_form.submit();
}

function combinationProduct(psid,pcid,combination_quantity)
{
		document.combination_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/CombinationProduct.action";
		document.combination_product_form.combination_quantity.value = combination_quantity;
		document.combination_product_form.psid.value = psid;
		document.combination_product_form.pcid.value = pcid;
		document.combination_product_form.submit();
}

function splitProduct(psid,pcid,combination_quantity)
{
	document.split_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/SplitProduct.action";
	document.split_product_form.split_quantity.value = combination_quantity;
	document.split_product_form.psid.value = psid;
	document.split_product_form.pcid.value = pcid;
	document.split_product_form.submit();
}

function addedProductStorage()
{
	if($("#filter_cid").val()==0)
	{
		alert("请选择一个仓库再进行商品库存补全！");
	}
	else
	{
		document.added_product_storage_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addedProdcutStorage.action";
		document.added_product_storage_form.added_ps_id.value = $("#filter_cid").val();
		document.added_product_storage_form.submit();
	}
	
	
}

function downloadProductStorage(){
	var ps_id = $("#filter_cid").val();
	var catalog_id = $("#filter_pcid").val();
	var product_line = $("#filter_productLine").val();
	
	var para = "ps_id="+ps_id+"&catalog_id="+catalog_id+"&product_line="+product_line;
	

		$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">生成文件中......</span>'});
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_storage/downloadProductStorage.action',
				type: 'post',
				dataType: 'json',
				timeout: 600000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
					
				success: function(date){
					if(date["canexport"]=="true")
					{
						$.unblockUI();
						document.download_form.action=date["fileurl"];
						document.download_form.submit();
					}
					else
					{
						alert("无法下载！");
					}
				}
			});
}
function downloadStorageCost()
{
	var ps_id = $("#filter_cid").val();
	var catalog_id = $("#filter_pcid").val();
	var product_line = $("#filter_productLine").val();
	var para = "ps_id="+ps_id+"&catalog_id="+catalog_id+"&product_line="+product_line;
	if(ps_id!=0)
	{
		$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		    centerX: true,
		    centerY: true, 
				fadeOut:  2000
			};
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">生成文件中......</span>'});
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_storage/downloadStorageCostAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 600000,
				cache:false,
				data:para,
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(date){
					if(date["canexport"]=="true")
					{
						$.unblockUI();
						document.download_form.action=date["fileurl"];
						document.download_form.submit();
					}
					else
					{
						alert("无法下载！");
					}
				}
			});
	}	
	else
	{
		alert("请选择一个仓库再下载！")
	}
}

function uploadProductStorage()
{

	tb_show('拆散套装','storage_alert_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}
function uploadCostStorage()
{
	tb_show('库存金额与运费','storage_cost_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
	function ajaxLoadCatalogMenuPage(id, title_id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id+"&title_id="+title_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function splitAdvice(pc_id,ps_id,count)
	{
		$.artDialog.open("split_advice.html?pc_id="+pc_id+"&ps_id="+ps_id+"&lacking_count="+count, {title: "拆货建议",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function unionSplitAdvice()
	{
		var pids = "";
		
		var psid = 0;
		
		var submit = "true";
		$("[name='pid_batch']").each(function(){
			if(this.checked)
			{
				pids += this.value+",";
				
				var ps_id = $("#"+this.value).val();
				if(psid!=0)
				{
					if(psid!=ps_id)
					{
						submit = "false";
					}
				}
				psid = ps_id;
			}	
		})
		
		if(pids==="")
		{
			alert("请勾选你想一起获得的商品！");
		}
		else if(submit=="false")
		{
			alert("请选择同一仓库下的商品进行拆货建议！");
		}
		else
		{
			$.artDialog.open("union_split_advice.html?pids="+pids+"&ps_id="+psid,{title: "拆货建议",width:'870px',height:'500px',fixed:true, lock: true,opacity: 0.3});
		}
	}
	
	function puchaseDeliveryCount(pc_id,ps_id,type)
	{
		$.artDialog.open("purchase_delivery_count.html?pc_id="+pc_id+"&ps_id="+ps_id+"&type="+type,{title: "采购及在途数",width:'600px',height:'400px',fixed:true, lock: true,opacity: 0.3});
	}
	function changeTitle()
	{
		ajaxLoadProductLineMenuPage($("#title_id").val(), "");
		ajaxLoadProductLotNumberSelectPage($("#title_id").val(),"<%=lot_number_id%>", "");
		//ajaxLoadCatalogMenuPage($("#filter_productLine").val(),"",0, $("#title_id").val());
		
	}
	function ajaxLoadProductLineMenuPage(title_id, pc_line_id,divId)
	{
			var para = "title_id="+title_id+"&product_line_id="+pc_line_id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_line_menu_for_title.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#productLinemenu_td").html(html);
				}
			});
	}
	function ajaxLoadProductLotNumberSelectPage(title_id, lot_number_id,divId)
	{
			var para = "title_id="+title_id+"&lot_number_id="+lot_number_id+"&divId="+divId;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/ct_storage_lot_number_by_title.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#lot_number_id_td").html(html);
				}
			});
	}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">

<form action="" name="download_form" id="download_form">
</form>
<form method="post" name="combination_product_form">
<input type="hidden" name="combination_quantity">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>

<form method="post" name="split_product_form">
<input type="hidden" name="split_quantity">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>

<form method="post" name="renew_product_form">
<input type="hidden" name="damaged_count_m">
<input type="hidden" name="damaged_package_count_m">
<input type="hidden" name="pid">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>


<form method="post" name="add_return_product_form">
<input type="hidden" name="pid">
<input type="hidden" name="quantity">
<input type="hidden" name="oid">
<input type="hidden" name="damaged">
</form>


<form method="post" name="add_damaged_product_form">
	<input type="hidden" name="pid">
	<input type="hidden" name="quantity">
</form>

<form method="post" name="convert_product_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/convertProduct.action">
	<input type="hidden" name="s_pid" >
	<input type="hidden" name="s_pc_id" >
	<input type="hidden" name="p_name" >
	<input type="hidden" name="quantity" >
</form>

<form method="post" name="mod_store_catalog_form">
	<input type="hidden" name="mod_store_cid">
	<input type="hidden" name="pid">
	<input type="hidden" name="pcid">
</form>

<form action="" method="post" name="temp_form">
	<input type="hidden" name="pid">
</form>

<form method="post" name="report_form">
	<input type="hidden" name="cid"  value="<%=cid%>" >
	<input type="hidden" name="name" value="<%=name%>">
	<input type="hidden" name="cmd"  value="<%=cmd%>" >
	<input type="hidden" name="type" value="<%=type%>">
	<input type="hidden" name="export_flag" value="0" >
</form>

<form action="" name="added_product_storage_form" id="added_product_storage_form">
	<input id="added_ps_id" name="added_ps_id" type="hidden"/>
</form>

<form action="ct_product_storage.html" method="post" name="search_form" id="search_form">		
   <input type="hidden" name="cmd"> 
   <input type="hidden" name="cid"> 
   <input type="hidden" name="type"> 
   <input type="hidden" name="pcid"> 
   <input type="hidden" name="name">
   <input type="hidden" name="pro_line_id"/> 
   <input type="hidden" name="union_flag"/>
   <input type="hidden" name="title_id"/>
   <input type="hidden" name="lot_number_id"/>
</form>

	</td>
  </tr>
</table>
<br/>

<form name="listForm" method="post" >
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
	  <tr> 
	      <th width="5%" style="vertical-align: center;text-align: center;" class="left-title">&nbsp;</th>
	      <th width="23%" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品信息</th>
	      <th width="22%"  style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
	      <th width="10%" class="right-title"  style="vertical-align: center;text-align: center;">数值库存</th>
	      <th width="15%" class="right-title"  style="vertical-align: center;text-align: center;">残损</th>
	      <th width="10%" class="right-title"  style="vertical-align: center;text-align: center;">周期平均</th>
          <th width=""  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
	  </tr>
	    <%
			String delPCID = "";
			DBRow proGroup[];
			for ( int i=0; i<rows.length; i++ ){
			delPCID += rows[i].getString("pc_id")+",";
		%>
    <tr > 
      <td width="72" height="60" align="center" valign="middle"   > 
		  <a name="<%=rows[i].getString("p_name")%>" id="<%=rows[i].getString("p_name")%>"></a>
	      <input name="pid_batch"  type="checkbox" value="<%=rows[i].getString("pid")%>">
	      <input type="hidden" value="<%=rows[i].getString("cid")%>" id="<%=rows[i].getString("pid")%>"/>
      </td>
      <td height="60" valign="middle"   style='font-size:14px;' >
	      <fieldset style="border:1px blue solid;padding:7px;width:260px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		      <legend style="font-size:15px;font-weight:bold;color:black;"> 
					 <%=rows[i].getString("p_name")%>
				      <% 
				      	DBRow[] purchaseCount = productStorageMgrZJ.getPurchaseCount(rows[i].get("cid",0l),rows[i].get("pc_id",0l),purchase_status,true);
				      	if(purchaseCount!=null&&purchaseCount.length>0){
				      %>
				      	<br/><a style="text-decoration: none;" href="javascript:puchaseDeliveryCount(<%=rows[i].get("pc_id",0l)%>,<%=rows[i].get("cid",0l)%>,'purchaseCount')">采购数量:<font color="blue"><%=purchaseCount[0].get("count",0f)%></font></a>
				      <%}%>    
				      <%
				      	DBRow[] deliveryCount = productStorageMgrZJ.getDeliveryingCount(rows[i].get("cid",0l),rows[i].get("pc_id",0l),delivery_status,transport_status,true);
				      	if(deliveryCount!=null&&deliveryCount.length>0){
				      %>
				      	<br/><a style="text-decoration: none;" href="javascript:puchaseDeliveryCount(<%=rows[i].get("pc_id",0l)%>,<%=rows[i].get("cid",0l)%>,'deliveryCount')">在途数量:<font color="blue"><%=deliveryCount[0].get("count",0f)%></font></a>
				      <%}%>				
			  </legend>	
			
				
				    <p style="font-size: 12px">商品分类:</p>
				    <p style="padding-left: 70px;font-size: 12px">
				         <span>
							  <%
							  DBRow allCatalogFather[] = catalogTree.getAllFather(rows[i].get("catalog_id",0l));
							  for (int jj=0; jj<allCatalogFather.length-1; jj++){
							  	out.println("<a style='color:#36F' class='nine4'  href='?cmd=cid&pcid="+allCatalogFather[jj].getString("id")+"&cid="+cid+"&type=0'>"+allCatalogFather[jj].getString("title")+"</a><br>");
							  }
							  %>
						  </span>
						  <%
						  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
						  if (catalog!=null){
						  	out.println("<a style='color:#36F' href='?cmd=cid&pcid="+rows[i].getString("catalog_id")+"&cid="+cid+"&type=0'>"+catalog.getString("title")+"</a>");
						  }
						  %> 
					</p>	  								
		  </fieldset>	
      </td>
      <td align="center" valign="middle"   >
     	<% 
     		DBRow[] productCountsGroupByTitle = productStoreMgrZJ.getProductStoreCountGroupByTitle(cid,0,0, title_id,rows[i].get("pc_id",0l),"","", 0);
     		for(int j=0;j<productCountsGroupByTitle.length;j++)
     		{
     	%>
			  <fieldset style="border:1px green solid;padding:7px;width:80%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
					<legend style="font-size:15px;font-weight:bold;color:green;">
						<%
							DBRow title = proprietaryMgrZyj.getDetailTitleByTitleId(productCountsGroupByTitle[j].get("title_id",0l));
						%>
						<%=title.getString("title_name")%>
					   <span style="font-size:13px;color:#999999;font-weight:normal"></span>
					</legend>  
						<p align="left"><a href="javascript:void(0)" onclick="openlot('<%=productCountsGroupByTitle[j].getString("p_name")%>',<%=productCountsGroupByTitle[j].getString("pc_id")%>,<%=title.getString("title_id")%>,'<%=title.getString("title_name") %>')" style="text-decoration:none">按批次查看库存</a></p>
						<p align="left"><a href="javascript:void(0)" onclick="openLocation('<%=productCountsGroupByTitle[j].getString("p_name")%>',<%=productCountsGroupByTitle[j].getString("pc_id")%>,<%=productCountsGroupByTitle[j].get("title_id",0l)%>,'<%=title.getString("title_name")%>')" style="text-decoration:none">按位置查看</a></p>
						<p align="left"><a href="javascript:void(0)" onclick="openlp('<%=productCountsGroupByTitle[j].getString("p_name")%>',<%=productCountsGroupByTitle[j].getString("pc_id")%>,<%=productCountsGroupByTitle[j].get("title_id",0l)%>)" style="text-decoration:none">按配置查看</a></p>
							<div>可用数量：<%=productCountsGroupByTitle[j].get("available",0) %></div>
						 	<div>物理数量：<%=productCountsGroupByTitle[j].get("physical",0) %></div>
			  </fieldset>
		<%
     		}
     	%>  
		  &nbsp;
	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;' nowrap="nowrap"> 
		<input name="store_count_<%=rows[i].getString("pid")%>" type="hidden" value="<%=rows[i].get("store_count",0f)%>"  >
		<%
			DBRow compensation = outboundOrderMgrZJ.compensationValueForPsAndP(rows[i].get("cid",0l),status,rows[i].get("pc_id",0l));
			DBRow outStore = outboundOrderMgrZJ.outStoreValueForPsAndP(rows[i].get("cid",0l),status,rows[i].get("pc_id",0l));
		%>	
		<span style="display: block;overflow: inherit"><font color="blue">有效库存:<%=rows[i].get("available",0)%></font></span>
		<span style="display: block;overflow: inherit"><font color="red">盘点库存:<%=rows[i].get("physical",0)%></font></span>
		<span style="display: block;overflow: inherit">出库中:<%=rows[i].get("locked",0)%></span>
	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;font-weight:bold'>
	  	功能残损:<%=rows[i].get("damaged_count",0f)%><br/>
	  	外观残损:<%=rows[i].get("damaged_package_count",0f)%>
	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;font-weight:bold'>
	  	<div style="width:80%" align="left">
	  		采样周期:<%=rows[i].get("sampling period",0)%><br/>
	  		<font color="green">需求数量:<%=rows[i].get("storage_need_avgcount",0f)%></font><br/>
		  	<font color="blue">计划备货:<%=rows[i].get("storage_plan_avgcount",0f)%></font><br/>
		  	<font color="">发货数量:<%=rows[i].get("storage_send_avgcount",0f)%></font><br/> 
		  	销售系数:<%=rows[i].get("coefficient_sales",0)%><br/>
		  	备货天数:<%=rows[i].getString("store_count_alert")%>
	  	</div>	  	
	  </td>
      <td align="center" valign="middle"   >
	  <div style="margin-bottom:10px;margin-top:10px;">
	  	  <input name="Submit52" type="button" class="short-short-button-damage"  onClick="addDamageProduct('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>,<%=rows[i].get("store_count",0f)%>,<%=rows[i].get("damaged_count",0f)%>)" value="残损">
	 	  &nbsp;&nbsp;
	  	  <input name="Submit52" type="button" class="short-short-button-return"  onClick="returnProduct('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>)" value="退件">
	  </div>
	
	<div style="margin-bottom:10px;">
	  <input name="Submit52" type="button" class="short-short-button-convert"  onClick="openConvertProduct('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>)" value="改货">
	  &nbsp;&nbsp;
	  <input name="Submit52" type="button" class="short-short-button-convert"  onClick="openRenewProduct('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>)" value="翻新">
    </div>
	<div style="margin-bottom:10px;">
	  <% 
	  	if(rows[i].get("store_count",0f)<0)
	  	{
	  %>
	  	<tst:authentication bindAction="com.cwc.app.api.ProductMgr.splitProduct">
	  		<input id="split_advice" value="建议" class="short-short-button-convert" onClick="splitAdvice(<%=rows[i].get("pc_id",0l)%>,<%=rows[i].get("cid",0l)%>,<%=rows[i].get("store_count",0f)%>)"/>
	  	</tst:authentication>
	  <%
	  	}
	  %>
	</div>

<div style="margin-bottom:10px;">
<%
if (rows[i].get("union_flag",0)==1)
{
%>
	  <input name="Submit52" type="button" class="short-short-button-convert"  onClick="openCombination('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>,<%=rows[i].getString("cid")%>)" value="拼装">
&nbsp;&nbsp;
 <input name="Submit52" type="button" class="short-short-button-convert"  onClick="openSplit('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>,<%=rows[i].getString("cid")%>)" value="拆散">
<%
}
%>
</div>
<div style="margin-bottom:10px;">
<input type="button" class="long-button-print" value="制作标签" onClick="selectLabel(<%=rows[i].get("pc_id",0l)%>)">
</div>
 </td>
    </tr>
    <%
}
%>
</table>
</form>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
      <input type="hidden" name="p" >
	  <input type="hidden" name="cid" id="ps_id"  value="<%=cid%>">
	  <input type="hidden" name="cmd"  value="<%=cmd%>">
	  <input type="hidden" name="name" value="<%=name%>">
	  <input type="hidden" name="type" value="<%=type%>">
	  <input type="hidden" name="pcid" value="<%=pcid%>">
	  <input type="hidden" name="pro_line_id" value="<%=pro_line_id%>"/>
	  <input type="hidden" name="union_flag" value="<%=union_flag%>">	
	  <input type="hidden" name="title_id" id="title_id" value="<%=title_id%>"/>
	  <input type="hidden" name="lot_number_id" id="lot_number_id" value="<%=lot_number_id%>">	  
  </form>
  <tr>       
    <td height="28" align="right" valign="middle">
      <%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	 %>
              跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">



function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
<script>


function openLocation(p_name,pc_id,title_id,titleName){
	var lot_number_id=$('#lot_number_id').val();
//	if(lot_number_id==""){
//		lot_number_id=0;
//	}
//	var title_id=$('#title_id').val();
	
	var ps_id=$('#ps_id').val(); 
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/dialog_product_repertory.html?productId="+pc_id+"&productName="+p_name+"&ps_id="+ps_id+"&titleId="+title_id+"&titleName="+titleName+"&lot_number_id="+lot_number_id;
	$.artDialog.open(uri , {title: '商品位置库存',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}


function openlp(p_name,pc_id,title_id){
	var lot_number_id=$('#lot_number_id').val();
//	if(lot_number_id==""){
//		lot_number_id=0;
//	}
	var ps_id=$('#ps_id').val();  
	var uri='<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/product/dialog_product_lp.html?ps_id="+ps_id+"&productId="+pc_id+"&productName="+p_name+"&title_id="+title_id+"&lot_number_id="+lot_number_id;
	$.artDialog.open(uri , {title: '商品批次库存',width:'820px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

function openlot(p_name,pc_id,title_id,title_name)
{
	var lot_number_id=$('#lot_number_id').val();
//	if(lot_number_id==""){
//		lot_number_id=0;
//	}
	var ps_id=$('#ps_id').val();   //仓库id
	var uri='<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/product/dialog_product_lot.html?productId="+pc_id+"&titleId="+title_id+"&titleName="+title_name+"&productName="+p_name+"&lot_number_id="+lot_number_id+"&ps_id="+ps_id;
	$.artDialog.open(uri , {title: '商品批次库存',width:'820px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
</script>
