<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="containerTypeKey" class="com.cwc.app.key.ContainerTypeKey"></jsp:useBean>
<%
	long ps_id = StringUtil.getLong(request,"ps_id");
    long productId=StringUtil.getLong(request,"productId");       
    String productName=StringUtil.getString(request,"productName");         
	String titleName=StringUtil.getString(request,"title_name");	  
	String lotNumberId=StringUtil.getString(request,"lot_number_id");
	long titleId=StringUtil.getLong(request,"title_id");
	//DBRow[] rows=mgrZwb.selectLotNuberLpCount(productId,Long.parseLong("0"),Long.parseLong("0"),Long.parseLong("0"));
	
	DBRow[] containerCounts = productStoreMgrZJ.getProductStoreContainerCount(ps_id,0,titleId,productId,lotNumberId);
	//不满container_product，满type
	
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<title>库存位置</title>
</head>
<body>
   <fieldset style="border:1px red solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
             <%
             	for(int i=0;i<containerCounts.length;i++)
             	{ 
             %>
	     	 <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
			 	<legend style="font-size:15px;font-weight:bold; color: green"> 	
			 		<a href="javascript:void(0)" onclick="openContainer('<%=productName%>',<%=productId%>,<%=titleId%>,<%=containerCounts[i].get("container_type",0)%>)" style="text-decoration:none; color:#C00">
	        			<span style="font-size:13px; font-weight: bold;">容器类型:</span>
	        			<span style="font-size:13px; font-weight: bold;margin-left: 3px;">[<%=containerTypeKey.getContainerTypeKeyValue(containerCounts[i].get("container_type",0))%>]</span> 
			 		</a>	 
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">容器数量:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=containerCounts[i].get("container_count",0) %></span>
			    </div>
			 </fieldset>
			 <%
			 	} 
			 %>
   </fieldset>
</body>
</html>
<script>
function openContainer(p_name,pc_id,title_id,containType)
{
	var lot_number_id='<%=lotNumberId%>';
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/dialog_product_container_lp.html?ps_id=<%=ps_id%>&productId="+pc_id+"&titleId="+title_id+"&productName="+p_name+"&lot_number_id="+lot_number_id+"&containType="+containType;
	$.artDialog.open(uri , {title: '商品位置库存',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}
</script>
