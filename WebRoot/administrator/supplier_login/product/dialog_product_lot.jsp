<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
    long productId=StringUtil.getLong(request,"productId");       //商品id
	long titleId=StringUtil.getLong(request,"titleId");	          //titleid
	String titleName=StringUtil.getString(request,"titleName");	  //title名
    String productName=StringUtil.getString(request,"productName");  //商品名
    String lot_number_id=StringUtil.getString(request,"lot_number_id"); //批次
    long ps_id=StringUtil.getLong(request,"ps_id");       //仓库
    
    DBRow[] lotNumber = productStoreMgrZJ.getProductStoreCountGroupByLotNumber(ps_id, titleId, productId, lot_number_id);
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<title>库存位置</title>

</head>
<body>
   <fieldset style="border:1px #C00 solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
		 <div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
      
		 	<span style="font-size:12px; font-weight: bold; color:black">当前TITLE:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=titleName %></span><br/>
            <hr size="1px;" color="#dddddd">
     	 </div>
     	 <%if(lotNumber!=null && lotNumber.length>0){%>
			<%for(int mm=0;mm<lotNumber.length;mm++){ %>	
				<div style="border:0px solid red;width:245px; float:left;" align="center">
					<fieldset style="border:1px green solid;padding:7px;width:160px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
						<legend style="font-size:15px;font-weight:bold;color:#999999;"> 
							<div style="color:#00F; font-size: 12px">LotNumber:<%=lotNumber[mm].getString("lot_number")%></div>
						</legend>
							<a href="javascript:void(0)" onclick="openLocation('<%=productName%>',<%=productId%>,<%=titleId%>,'<%=lotNumber[mm].getString("lot_number")%>','<%=titleName%>')" style="text-decoration:none; color:#C00">
						    	<div align="left">按位置查看</div>
						    </a>
							<div>可用数量:<%=lotNumber[mm].get("available",0d)%></div>
						 	<div>物理数量:<%=lotNumber[mm].get("physical",0d)%></div>	
					 </fieldset>
				</div>
			 <%}%>
		 <%}%>
      </fieldset>
</body>
</html>
<script>

function openLocation(p_name,pc_id,title_id,lot_name,title_name){
	var ps_id=<%=ps_id%>;
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/dialog_product_repertory.html?productId="+pc_id+"&titleId="+title_id+"&titleName="+title_name+"&productName="+p_name+"&lotName="+lot_name+"&ps_id="+ps_id; 
	$.artDialog.open(uri , {title: '商品位置库存',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
}

</script>

