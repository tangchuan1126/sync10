<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%

    String cmd = StringUtil.getString(request,"cmd");
	int p = StringUtil.getInt(request,"p");
    PageCtrl pc = new PageCtrl();
    pc.setPageNo(p);
    pc.setPageSize(30);
    
    long psId = StringUtil.getLong(request,"psId");
    String input_st_date = StringUtil.getString(request,"input_st_date");
    String input_en_date = StringUtil.getString(request,"input_en_date");
    String p_name = StringUtil.getString(request,"p_name");
    
    
	TDate tDate = new TDate();
	
	if (input_en_date.equals(""))
	{
		input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	}
	
	tDate.addDay(-30);
	if (input_st_date.equals(""))
	{
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
	}


    DBRow productStorageHistory[];
    if (cmd.equals("search"))
    {
      productStorageHistory = ProductStorageHistoryMgrZJ.getProductStorageHistoryByPsid(input_st_date,input_en_date,p_name,psId,pc);
    }else{
    	productStorageHistory=new  DBRow[0];
    }
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
 %>
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>product_storage_history</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script type="text/javascript">
$().ready(function() {
addAutoComplete($("#in_store_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");


			$("#in_store_p_name").keydown(function(event){
				if (event.keyCode==13)
				{
					filter();
				}
			});
		});

function dynCalendarCallback(date,month,year,objName)
{
	day = date;
	date = year+"-"+month+"-"+day;
	$("#"+objName+"_date").val(date);
}

function filter()
    {
        document.search_form.submit();
	}

//导出
function exportData(){

	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/export_storage_history.html";
	$.artDialog.open(uri,{title:"导出库存历史数据",width:'550px',height:'370px', lock: true,opacity: 0.3,fixed: true});
}

</script>
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
  <br>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   库存历史记录 </td>
	  </tr>
	</table>
  <br/>
	<table width="98%">
		<tr>
			<td>
			<form action="" method="post" name="search_form"> 
			   <table>
			     <tr>
			       <td >
			   					开始时间&nbsp;&nbsp;<input name="input_st_date" type="text" id="in_store_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					in_store_input_st = new dynCalendar('in_store_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   		</td>
			   		<td>
			   					&nbsp;&nbsp;&nbsp;结束时间&nbsp;&nbsp;<input name="input_en_date" type="text" id="in_store_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				in_store_input_en = new dynCalendar('in_store_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
					</td>
					
					<td style="">	   					
		               			 &nbsp;&nbsp;&nbsp;商品名 <input style="width: 240px;" name="p_name" type="text" id="in_store_p_name" value="<%=p_name%>">
				    </td>
					
					<td>
				           &nbsp;&nbsp;&nbsp;<input type="hidden" name="cmd" value="search">
							<select name="psId" id="psId" style="border:1px #CCCCCC solid"  >
				              <option value="0">所有仓库</option>
				          <%
			
									String qx;
									
									for ( int i=0; i<treeRows.length; i++ )
									{
										if ( treeRows[i].get("parentid",0) != 0 )
										 {
										 	qx = "├ ";
										 }
										 else
										 {
										 	qx = "";
										 }
										 
										 if (treeRows[i].get("level",0)>1)
										 {
										 	continue;
										 }
									%>
									          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==psId?"selected":""%> > 
									          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> 
									          <%=qx%> 
									          <%=treeRows[i].getString("title")%> </option>
									          <%
									}
									%>
				        </select>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input name="Submit2" type="submit" class="button_long_refresh" value="过滤"/>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" class="long-button-export" onClick="exportData();" value="导出"/>
				   </td>
			    
			  </tr>
			</table>
		</form>
			</td>
		</tr>
	</table>
	<br/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="17%" class="right-title" style="vertical-align: center;text-align: center;">商品</th>
        <th width="13%" class="left-title" style="vertical-align: center;text-align: center;">仓库</th>
        <th width="13%" style="vertical-align: center;text-align: center;" class="left-title">库存数量</th>
        <th width="13%" style="vertical-align: center;text-align: center;" class="right-title" >功能残损数量</th>
        <th width="12%" class="right-title" style="vertical-align: center;text-align: center;" >外观残损数量</th>
        <th width="15%" class="right-title" style="vertical-align: center;text-align: center;">记录时商品价格</th>
        <th width="12%" class="right-title" style="vertical-align: center;text-align: center;" >记录时间</th>
    </tr>
    <%
		for (int i=0; i<productStorageHistory.length; i++)
		{
	%>
    <tr>
      <td  width="17%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=productStorageHistory[i].getString("p_name") %></td>
      <td  width="13%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=productStorageHistory[i].getString("title") %></td>
      <td  width="13%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'><%=productStorageHistory[i].get("store_count",0f) %></td>
      <td  width="13%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=productStorageHistory[i].get("damaged_count",0f) %></td>
      <td  width="12%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%= productStorageHistory[i].get("damaged_package_count",0f)%></td>
      <td  width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=productStorageHistory[i].get("unit_price",0d) %></td>
      <td  width="12%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=productStorageHistory[i].getString("post_date").substring(0,10) %></td>
    </tr>
    <%} %>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" method="post">
	          <input type="hidden" name="p">
	          <input type="hidden" name="cmd" value="<%=StringUtil.getString(request,"cmd") %>"/>
			  <input type="hidden" id="psId" name="psId" value="<%=StringUtil.getLong(request,"psId") %>"/>
			  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		      <input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		      <input type="hidden" name="p_name" value="<%=p_name%>">
	          
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      跳转到 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table> 
  </body>
</html>
