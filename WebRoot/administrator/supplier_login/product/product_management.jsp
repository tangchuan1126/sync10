<%@page import="com.cwc.app.key.MoreLessOrEqualKey"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.zj.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@page import="com.cwc.app.api.AdminLoggerBean"%>
<%@include file="../../include.jsp"%> 
<%@taglib uri="/turboshop-tag" prefix="tst" %>

<%

	AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session); 
	long adminId=adminLoggerBean.getAdid();
	
	//根据登录帐号判断是否为客户
	Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
	//如果是用户登录 查询用户下的产品
	DBRow[] titles;
	DBRow[] p1;
	long number=0;
	if(bl){
		number=1;
		titles=adminMgrZwb.getProprietaryByadminId(adminLoggerBean.getAdid());
	}else{
		number=0;
		titles=mgrZwb.selectAllTitle();
	}
	
	
	
	
	
%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript" ></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<title>Product Relevant Info</title>
<style>
 <!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<script type="text/javascript">
function down(){

	var catagoryIdSel = 0;
	if(catalogThreeId*1 > 0)
	{
		catagoryIdSel = catalogThreeId;
	}
	else if(catalogTwoId*1 > 0)
	{
		catagoryIdSel = catalogTwoId;
	}
	else if(catalogOneId*1 > 0)
	{
		catagoryIdSel = catalogOneId;
	}
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var para='pcid='+catagoryIdSel+'&cmd='+cmd+'&union_flag='+productType+'&pro_line_id='+lineId+'&title_id='+titleId;
	//alert(para);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownProductAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}
	function importProductTitles(_target)
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function importInitData()
	{
	    var fileNames = $("#file_names").val();
	    var obj  = {
		     reg:"xlsm",
		     limitSize:2,
		     limitNum:1,
		     target:"initData"
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 )
		 {
			uri += "&file_names=" + fileNames;
		 }
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target)
	{
		if($.trim(fileNames).length > 0 )
		{
			if(target=="")
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/check_product_title_show.html?fileNames="+fileNames;
				$.artDialog.open(uri,{title: "检测上传用户title信息",width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
			}
			else
			{
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/initData_show.html?file_name="+fileNames;
				$.artDialog.open(uri,{title: "Basic Data Initialize",width:'1200px',height:'700px', lock: true,opacity: 0.3,fixed: true});
			}
		}
	}
	function showProductDetails(pcid, pc_name)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_details.html?pc_id="+pcid;
		$.artDialog.open(uri,{title: pc_name,width:'850px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	}
	function deleteBasicDatas()
	{
		var d = $.artDialog({
			lock: true,opacity: 0.3,fixed: true,
		    title: 'Delete Basic Data',
		    content: 'Delete all data except the titles ?',
		    button:[
					{
						name: 'Sure',
						callback: function () {
							ajaxDeleteBasicDatas(d);
							return false ;
						},
						focus:true
						
					},
					{
						name: 'Cancel'
					}]
		});
		d.show();
	}
	//删除
	function ajaxDeleteBasicDatas(d){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/DeleteAllBasicDatasExceptTitlesAction.action',
			type:'post',
			dataType:'json',
			beforeSend:function(){
				 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
		    success:function(data){
			    $.unblockUI();
			    d.close();
			    if(data && data.flag == "true"){
			    	refreshWindow();
			    }
		    },
			error:function(){
		    	$.unblockUI();
				showMessage("System error","error");
		    }
		});
	}
</script>
</head>
<body>
<form action="" name="download_form" id="download_form"> </form>
	<div style="border-bottom:1px solid #999; height:25px">
		<img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">
		<span style="font-size: 12px;font-weight: bold"> Basic Data » Product Relevant Info</span>
	</div>
	<br/>
	<div id="tabs">
		<ul>
			<li><a href="#av1">Advanced Search</a></li>
			<li><a href="#av2">Product Name Search</a></li>		 
		</ul>
		<div id="av1">
				<div id="tiaojian" style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Condition：</span>
							</td>
							<td width="90%" valign="middle" >
								<div id="condition" style="min-width; float:left"></div>
							    	<div style="float:left">							    	 
							    		<input type="button" value="Search" style="margin-left:10px" class="button_long_search" onclick="seachProductByCondition()"/>
							    	</div>
							</td>
						</tr>
					</table>
				</div>	
				<div id="title"  style="border-bottom:1px #CCC dashed;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">TITLE：</span>
							</td>
							<td width="90%" valign="middle">
							    <div style="overflow:auto" id="titleCondition">
									<%for(int a=0;a<titles.length;a++){ %> 
								 	  <div style="width:200px;float:left;margin-left:10px; height:20px">
								 	  	 <a href="javascript:void(0)" onclick="addCondition(<%=titles[a].get("title_id",0l)%>,'<%=titles[a].getString("title_name")%>','title')" style="color:#069;text-decoration:none;outline:none;">
								 	  	 	<%=titles[a].getString("title_name")%>							 	  	 						
								 	  	 </a>
								 	  </div>
									<%}%>				
								</div>
								<script>
									//计算高度 超过出现滚动条
									function clertHeight(id){
										 $('#'+id).css("height","");
									}
									function computeHeight(id){									 
									   var $av=$('#'+id);
									   var height=$av.height();									
									   if(height>=60){
										  $av.css("height","60px");
									   }
									}
									computeHeight('titleCondition');
								</script>
							</td>
						</tr>
					</table>
				</div>
				<div id="line"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Product Line：</span>
							</td>
							<td width="90%" valign="middle">
								 <div style="overflow:auto" id="lineCondition">
								 </div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogOne"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Parent Category：</span>
							</td>
							<td width="90%" valign="middle" >
								 <div style="overflow:auto;" id="catalogOneCondition">
								 </div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogTwo"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Sub Category：</span>
							</td>
							<td width="90%" valign="middle" >
								<div style="overflow:auto" id="catalogTwoCondition">
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="catalogThree"  style="border-bottom:1px #CCC dashed; display:none;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Sub-Sub Category：</span>
							</td>
							<td width="90%" valign="middle">
								<div style="overflow:auto" id="catalogThreeCondition">
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="productType"  style="border-bottom:1px #CCC dashed;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="10%" height="30px;" align="right">
								<span style="font-weight:bold;font-size:12px;">Product Type：</span>
							</td>
							<td width="90%" valign="middle">
								<div style="width:95px;float:left;margin-left:10px;">
							       	<a href="javascript:void(0)" onclick="addCondition(0,'ITEM','productType')" style="color:#069;text-decoration:none;outline:none;">ITEM</a>
							    </div>
							    <div style="width:95px;float:left;margin-left:10px;">
							       	<a href="javascript:void(0)" onclick="addCondition(1,'UNIT','productType')" style="color:#069;text-decoration:none;outline:none;">UNIT</a>
							    </div>		
							</td>
						</tr>
					</table>
			    </div>
		</div>
		<div id="av2">
		    <!-- 名字搜索 -->
	        <div id="easy_search_father">
				<div id="easy_search" style="margin-top:-2px;">
					<a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a>
				</div>
		    </div>
	           <table width="400" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="418">
						<div  class="search_shadow_bg">
						 <input name="seachInput" id="seachInput" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
						</div>
					</td>
				</tr>
			  </table>	
	    </div>
	</div>
	<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>
	<br/>
	<div>
		<input name="importProductTitles" type="button" class="long-long-180-button" value="Import Product Linked Title" onClick="importProductTitles()"/>
		<input type="button" class="long-long-180-button" value="Export Product Linked Title" onclick="down()" />
		<input type="button" class="long-long-button" value="Initialize Basic Data" onclick="importInitData()" />
		<input type="button" class="long-long-button" value="Delete Basic Data" onclick="deleteBasicDatas()" />
		<input name="file_names" id="file_names" type="hidden" />
	</div>
	<br/>
	<div id="result">查询结果显示</div>
	<!-- 隐藏值 -->
	<input type="hidden" id="hiddenProductLineId" value=""/>
	<!-- 分页用隐藏值 -->
	<input type="hidden" id="lineId" />
	<input type="hidden" id="catalogId" />
	<input type="hidden" id="productType" />
	<input type="hidden" id="cmd" />
</body>
</html>
<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

jQuery(function($){
    $("#tabs").tabs("select",0);  //默认选中第一个
    addAutoComplete($("#seachInput"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
});

(function(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html?titleId='+titleId,
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
})();


//高级查询控件事件
function query(){
	var seachValue=$('#seachInput').val();  //获取文本框值
	var cmd='name';
	var str='cmd='+cmd+'&seachValue='+seachValue+'&titleId='+titleId;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
	
}

function go(p){
	var lineId=$('#lineId').val();
	var catalogId=$('#catalogId').val();
	var cmd=$('#cmd').val();
	var productType=$('#productType').val();
	var params = {p:p,lineId:lineId,catalogId:catalogId,productType:productType,titleId:titleId,cmd:cmd};
	var str = jQuery.param(params);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}
var lineId=0;
var catalogOneId=0;
var catalogTwoId=0;
var catalogThreeId=0;
var productType=0;
var params='';
var catalogId=0;
var cmd='seach';
var titleId=0;
//根据条件查询商品
function seachProductByCondition(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开

	lineId=0;
	catalogOneId=0;
	catalogTwoId=0;
	catalogThreeId=0;
	productType=0;
	params='';
	catalogId=0;
	cmd='seach';
	titleId=0;
	if($("div[type='line']",$('#condition')).html()!=null){
		lineId=$("div[type='line']",$('#condition')).attr("zhi");
	}
	if($("div[type='catalogOne']",$('#condition')).html()!=null){
		catalogOneId=$("div[type='catalogOne']",$('#condition')).attr("zhi");
	} 
	if($("div[type='catalogTwo']",$('#condition')).html()!=null){
		catalogTwoId=$("div[type='catalogTwo']",$('#condition')).attr("zhi");
	} 
	if($("div[type='catalogThree']",$('#condition')).html()!=null){
		catalogThreeId=$("div[type='catalogThree']",$('#condition')).attr("zhi");
	} 
	if($("div[type='productType']",$('#condition')).html()!=null){
		productType=$("div[type='productType']",$('#condition')).attr("zhi");
	} 
	if($("div[type='title']",$('#condition')).html()!=null){
		titleId=$("div[type='title']",$('#condition')).attr("zhi");
	}
	if(catalogThreeId!=0){
		catalogId=catalogThreeId;
	}else if(catalogTwoId!=0){
		catalogId=catalogTwoId;
	}else{
		catalogId=catalogOneId;
	}
	params = {lineId:lineId,catalogId:catalogId,productType:productType,titleId:titleId,cmd:cmd};

	$('#lineId').val(lineId);
	$('#catalogId').val(catalogId);
	$('#cmd').val('seach');
	$('#productType').val(productType);
	
	var str = jQuery.param(params);
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_management_show_list.html',
		type: 'post',
		dataType: 'html',
		async:false,
		data:str,
		success: function(html){
			$("#result").html(html);
			$.unblockUI();       //遮罩关闭
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	 
}

//title 点击更多时触发
function morePcTitles(pc_id, p_name){

	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_add_title.html?pc_id="+pc_id;
	$.artDialog.open(url , {title: 'Manage ['+p_name+'] Linked Title',width:'600px',height:'450px', lock: true,opacity: 0.3,fixed: true,close:function(){loadTitlesByPcId(pc_id);}});
}

function refreshWindow(){
	//alert(pc_id);
	window.location.reload();
}
function loadTitlesByPcId(pc_id)
{
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/FindProprietaryByPcIdAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			$("#myulPcTitles_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0)
			{
				var len = data.length > 5 ? 5: data.length;
				for(var i = 0; i < len; i ++)
				{
					html += '<li style="margin-left:1px; height:20px">';
					html += data[i].title_name;
					html += '</li>';
				}
				if(data.length > 5)
				{
					$("#titleMore_"+pc_id).css("display","");
				}
				else
				{
					$("#titleMore_"+pc_id).css("display","none");
				}
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcTitles_"+pc_id).html(html);
		}
	});
}

function loadIlpsByPcId(pc_id)
{
	var para = "pc_id="+pc_id;
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/IlpGetByPcidAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			$("#myulPcIlps_"+pc_id).html("");
			var html = '';
			if(data && data.length > 0)
			{
				var len = data.length > 5 ? 5: data.length;
				for(var i = 0; i < len; i ++)
				{
					html += '<li style="font-size:13px;font-weight: bold;float:left;width:80px;" title="Contain Product">';
					html += data[i].ibt_total_length+'*'+data[i].ibt_total_width+'*'+data[i].ibt_total_height;
					html += '</li>';
					html += '<li style="color:#8B8386;float:left;width:160px;" title="Contain Product"> -- ';
					html += data[i].type_name;
					html += '</li>';
				}
				if(data.length > 5)
				{
					$("#ilpMore_"+pc_id).css("display","");
				}
				else
				{
					$("#ilpMore_"+pc_id).css("display","none");
				}
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcIlps_"+pc_id).html(html);
		}
	});
}

function loadBlpsByPcId(pc_id)
{
	var para = "pc_id="+pc_id+"&length=5";
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/BlpGetByPcidAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			var html = '';
			if(data)
			{
				$("#myulPcBlps_"+pc_id).html("");
				var is_more_blp = data.is_more;
				var ship_to_name = data.ship_to_name;
				var box_types = data.box_types;
				var box_ship_tos = data.box_ship_tos;
				if(is_more_blp == '<%=MoreLessOrEqualKey.MORE%>')
				{
					$("#blpMore_"+pc_id).css("display","");
				}
				else
				{
					$("#blpMore_"+pc_id).css("display","none");
				}
				if(box_types && box_types.length > 0)
				{
					for(var i = 0; i < box_types.length; i++)
					{
						var box_type = box_types[i];
						var color = '';
						var title = '';
						if(box_type.box_inner_type == 0)
						{
							color = '#8B8386';
							title = 'Contain Product';
						}
						else
						{
							color = 'red';
							title = "Contain ILP["+box_type.ibt_total_length+"*"+box_type.ibt_total_width+"*"+box_type.ibt_total_height+"]";
						}
						html += '<li style="font-size:13px;font-weight: bold;float:left;width:80px;" title="'+title+'">';
						html += box_type.box_total_length+'*'+box_type.box_total_width+'*'+box_type.box_total_height;
						html += '</li>';
						html += '<li style="float:left;width:180px;" title="'+title+'"> -- ';
						html += '<span style="color: '+color+';">';
						html += box_type.type_name;
						html += '</span>';
						html += '</li>';
					}
				}
				if(box_ship_tos && box_ship_tos.length > 0)
				{
					html += '<li>';
					html += '<fieldset style="border:1px green solid;padding:1px;width:90%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;">';
					html += '<legend>';
					html += '<span style="font-size: 16px;color:blue; ">';
					html += data.ship_to_name;
					html += '</span>';
					html += '</legend>';
					for(var i = 0; i < box_ship_tos.length; i ++)
					{
						var box_ship_to = box_ship_tos[i];
						var color = '';
						var title = '';
						if(box_ship_to.box_inner_type == 0)
						{
							color = '#8B8386';
							title = 'Contain Product';
						}
						else
						{
							color = 'red';
							title = "Contain ILP["+box_ship_to.ibt_total_length+"*"+box_ship_to.ibt_total_width+"*"+box_ship_to.ibt_total_height+"]";
						}
						html += '<ul class="myulPcTitles">';
						html += '<li style="font-size:13px;font-weight: bold;float:left;width:70px;" title="'+title+'">';
						html += box_ship_to.box_total_length+'*'+box_ship_to.box_total_width+'*'+box_ship_to.box_total_height;
						html += '</li>';
						html += '<li style="float:left;width:160px;" title="'+title+'"> -- ';
						html += '<span style="color: '+color+';">';
						html += box_ship_to.type_name;
						html += '</span>';
						html += '</li>';
						html += '</ul>';
					}
					html += '</fieldset>';
					html += '</li>';
				}
			}
			else
			{
				html += '<li style="height: 35px;"></li>';
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcBlps_"+pc_id).html(html);
		}
	});
}

function loadClpsByPcId(pc_id)
{
	var para = "pc_id="+pc_id+"&length=5";
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/container/ClpGetByPcidAction.action',
		type:'post',
		dataType:'json',
		data:para,
		async:false,
		success:function(data){
			var html = '';
			if(data)
			{
				$("#myulPcClps_"+pc_id).html("");
				var is_more_blp = data.is_more;
				var ship_to_name = data.ship_to_name;
				var box_types = data.box_types;
				var box_ship_tos = data.box_ship_tos;
				if(is_more_blp == '<%=MoreLessOrEqualKey.MORE%>')
				{
					$("#clpMore_"+pc_id).css("display","");
				}
				else
				{
					$("#clpMore_"+pc_id).css("display","none");
				}
				if(box_types && box_types.length > 0)
				{
					for(var i = 0; i < box_types.length; i++)
					{
						var box_type = box_types[i];
						var color = '';
						var title = '';
						if(box_type.sku_lp_box_type == 0)
						{
							color = '#8B8386';
							title = 'Contain Product';
						}
						else
						{
							color = 'red';
							title = "Contain BLP["+box_type.box_total_length+"*"+box_type.box_total_width+"*"+box_type.box_total_height+"]";
						}
						html += '<li style="font-size:13px;font-weight: bold;float:left;width:80px;" title="'+title+'">';
						html += box_type.sku_lp_box_length+'*'+box_type.sku_lp_box_width+'*'+box_type.sku_lp_box_height;
						html += '</li>';
						html += '<li style="color:red;float:left;width:180px;" title="'+title+'"> -- ';
						html += '<span style="color: '+color+';">';
						html += box_type.type_name;
						html += '</span>';
						html += '</li>';
					}
				}
				if(box_ship_tos && box_ship_tos.length > 0)
				{
					html += '<li>';
					html += '<fieldset style="border:1px green solid;padding:1px;width:90%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;">';
					html += '<legend>';
					html += '<span style="font-size: 16px;color:blue; ">';
					html += data.ship_to_name;
					html += '</span>';
					html += '</legend>';
					for(var i = 0; i < box_ship_tos.length; i ++)
					{
						var box_ship_to = box_ship_tos[i];
						var color = '';
						var title = '';
						if(box_ship_to.sku_lp_box_type == 0)
						{
							color = '#8B8386';
							title = 'Contain Product';
						}
						else
						{
							color = 'red';
							title = "Contain BLP["+box_ship_to.box_total_length+"*"+box_ship_to.box_total_width+"*"+box_ship_to.box_total_height+"]";
						}
						html += '<ul class="myulPcTitles">';
						html += '<li style="font-size:13px;font-weight: bold;float:left;width:70px;" title="'+title+'">';
						html += box_ship_to.sku_lp_box_length+'*'+box_ship_to.sku_lp_box_width+'*'+box_ship_to.sku_lp_box_height;
						html += '</li>';
						html += '<li style="color:red;float:left;width:160px;" title="'+title+'"> -- ';
						html += '<span style="color: '+color+';">';
						html += box_ship_to.type_name;
						html += '</span>';
						html += '</li>';
						html += '</ul>';
					}
					html += '</fieldset>';
					html += '</li>';
				}
			}
			else
			{
				html += '<li style="height: 35px;"></li>';
			}
			if('' == html)
			{
				html += '<li style="height: 35px;"></li>';
			}
			$("#myulPcClps_"+pc_id).html(html);
		}
	});
}


//添加条件
function addCondition(id,name,type){
	//alert(id+","+name+","+type);
	$('#tiaojian').css('display','block');      //条件显示

	if(type=='line'){
		$('#line').css('display','none');
		//把产品线id保存下来
		$('#hiddenProductLineId').val(id);
		loadProductCatalogOne(id);	//加载一级产品分类
		ty='产品线：';
	}else if(type=='catalogOne'){
		$('#catalogOne').css('display','none');
		loadProductCatalogTwo(id);       
		ty='一级分类：';
	}else if(type=='catalogTwo'){
		$('#catalogTwo').css('display','none');
		loadProductCatalogThree(id);
		ty='二级分类：'
	}else if(type=='catalogThree'){
		$('#catalogThree').css('display','none');
		ty='三级分类：'
	}else if(type=='productType'){
		$('#productType').css('display','none');
		ty='商品类型：';
	}else if(type=='title'){
        $('#title').css('display','none');
        loadProductLine(id);
        ty='TITLE：';
	}
	
	var html='<div id="'+type+''+id+'" type="'+type+'" title="'+name+'" zhi="'+id+'" style="border:1px #090 solid;float:left;min-width;font-size:13px;cursor:pointer;margin-left:10px;padding-left:2px;">'+ty+'<span style="color:red;font-weight: bold;">'+name+'&nbsp;×<span></div>';
	var av=$(html).appendTo($('#condition')); //添加到页面

	$(av).click(function(){   //绑定删除事件
		if($(this).attr('type')=='line'){
			$('#line').css('display','block');	
			$('#catalogOne').css('display','none');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='catalogOne']",$('#condition')).remove();//同时去掉子级	
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogOne'){
			$('#catalogOne').css('display','block');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogTwo'){
			$('#catalogTwo').css('display','block');
			$('#catalogThree').css('display','none');
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}else if($(this).attr('type')=='catalogThree'){
			$('#catalogThree').css('display','block');
		}else if($(this).attr('type')=='productType'){
			$('#productType').css('display','block');
		}else if($(this).attr('type')=='title'){
 			$('#title').css('display','block');
 			$('#line').css('display','none');
 			$('#catalogOne').css('display','none');
			$('#catalogTwo').css('display','none');
			$('#catalogThree').css('display','none');
			$("div[type='line']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogOne']",$('#condition')).remove();//同时去掉子级	
			$("div[type='catalogTwo']",$('#condition')).remove();//同时去掉子级
			$("div[type='catalogThree']",$('#condition')).remove();//同时去掉子级
		}
		
		$('#'+this.id).remove();  //删除当前条件
		
		if($('#condition').html()==''){
			$('#tiaojian').css('display','none');
		} 
	});
}

//加载产品线根据title查询 
function loadProductLine(titleId){
	clertHeight('lineCondition');   //清除容器高度
	var para = 'id='+titleId+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadProductLineAction.action',
		type: 'post',
		dataType: 'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'line'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#lineCondition').html(html);
				$('#line').css('display','block');
			}
		}
	});	
	computeHeight('lineCondition');	//计算高度
}

//加载一级产品分类
function loadProductCatalogOne(productLineId,titleId,admin_user_id){
	clertHeight('catalogOneCondition');   //清除容器高度
	var para = 'admin_user_id='+admin_user_id+'&id='+productLineId+'&title_id='+titleId+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',
		type: 'post',
		dataType: 'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogOne'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogOneCondition').html(html);
				$('#catalogOne').css('display','block');
			}
		}
	});	
	computeHeight('catalogOneCondition');	//计算高度 
}

//加载二级分类
function loadProductCatalogTwo(product_catalog_id){
	clertHeight('catalogTwoCondition');   //清除容器高度
	var product_line_id=$('#hiddenProductLineId').val();
	var para = 'id='+product_catalog_id+'&product_line_id='+product_line_id+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',
		type: 'post',
		dataType:'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogTwo'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogTwoCondition').html(html);			
				$('#catalogTwo').css('display','block');
			}
		}
	});	 
	computeHeight('catalogTwoCondition');	//计算高度
}

//加载三级分类
function loadProductCatalogThree(product_catalog_id){
	clertHeight('catalogThreeCondition');   //清除容器高度
	var product_line_id=$('#hiddenProductLineId').val();
	var para = 'id='+product_catalog_id+'&product_line_id='+product_line_id+'&atomicBomb=<%=number%>';
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',
		type: 'post',
		dataType:'json',
		data:para,
		async:false,
		success: function(data){
			if(data.length>0){
				var html='';
				for(var i=0;i<data.length;i++){
					html+='<div style="width:200px;float:left;margin-left:10px; height:20px">'+
							'<a href="javascript:void(0)" onclick="addCondition('+data[i].id+','+"'"+data[i].name+"'"+','+"'catalogThree'"+')" style="color:#069;text-decoration:none;outline:none;">'+
								data[i].name+
							'</a>'+
					     '</div>';	
				}
				$('#catalogThreeCondition').html(html);
				$('#catalogThree').css('display','block');
			}
		}
	});
	computeHeight('catalogThreeCondition');	//计算高度	 
}

function openBoxList(pc_id,product_name){
	   var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_and_ps_info_list.html?pc_id="+pc_id; 
	  // var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_list.html?pc_id="+pc_id; 
		$.artDialog.open(uri , {title:"["+ product_name +"]"+ " BOX TYPE AND SHIP TO",width:'880px',height:'480px', lock: true,opacity: 0.3,fixed: true,close:function(){loadBlpsByPcId(pc_id);}});
}
function addBoxPsIdIndex(pc_id , product_name){
  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/box_sku_ps_index.html?pc_id="+pc_id; 
	 $.artDialog.open(uri , {title:"["+ product_name +"]"+ " 盒子到ShipTo顺序",width:'830px',height:'480px', lock: true,opacity: 0.3,fixed: true});
}
function openClpList(pc_id,title_id,product_name){
  	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/clp_box_sku_ps_type_list.html?pc_id="+pc_id+"&title_id="+title_id; 
	 $.artDialog.open(uri , {title:"["+ product_name +"]"+ "CLP TYPE AND SHIP TO",width:'880px',height:'480px', lock: true,opacity: 0.3,fixed: true,close:function(){loadClpsByPcId(pc_id);}});
}
function addilp(pc_id,title_id,product_name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/box/ilp_sku_list.html?pc_id="+pc_id+"&title_id="+title_id+"&product_name="+product_name; 
	 $.artDialog.open(uri , {title:"["+ product_name +"]"+ "ILP TYPE ADD",width:'830px',height:'480px', lock: true,opacity: 0.3,fixed: true,close:function(){loadIlpsByPcId(pc_id);}});
}
</script>
