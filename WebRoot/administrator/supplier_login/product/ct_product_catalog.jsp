<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");

DBRow[] titles;

titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);
//parentCategory = proprietaryMgrZyj.findProductCatagorysByTitleId(true, 0L, "", -1L+"", "", title_id, 0, 0, null, request);

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Categories</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 下拉菜单 -->
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script> 
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addProductCatalogPage(parentid, parentName)
{
	if(!parentName){parentName="";}
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/product_category_add.html?parentid="+parentid+"&parentName="+parentName;
	$.artDialog.open(url , {title: 'Add Product Category',width:'600px',height:'380px', lock: true,opacity: 0.3,fixed: true});
}
function addProductCatalog(parentid)
{
	$.prompt(
	
	"<div id='title'>Add Product Category</div><br />Parent Category：<input name='proTextParentTitle' type='text' id='proTextParentTitle' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>Add New<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{
	      submit: promptCheckAddProductCatalog,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:parentid},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextParentTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
					
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductCatalog.action";
						document.add_form.parentid.value = parentid;
						document.add_form.title.value = f.proTextTitle;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}


function promptCheckAddProductCatalog(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}

function updateProductCategory(id){
 var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/update_product_category.html?id="+id;
 $.artDialog.open(url , {title: 'Rename Product Category',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}

function modProductCatalog(id)
{
	$.prompt(
	
	"<div id='title'>Mod Category</div><br />Category Name<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{

   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductCatalog.action";
						document.mod_form.id.value = id;
						document.mod_form.title.value = f.proTextTitle;		
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}






function delC(id,title)
{
	if (confirm("Are you sure you want to delete "+title+" ?"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductCatalog.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}

function moveCatalog(pc_id)
{
	lvl = getCategoryLevel(pc_id);
	//tb_show('移动商品分类','select_product_catalog.html?catalog_id='+pc_id+'&TB_iframe=true&height=500&width=800',false);
	height = getCategoryHeight(pc_id);
	if(height>1){
		showMessage("Cannot move this category as it already has 3 levels.","alert");
	}else{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product/select_product_catalog.html?catalog_id="+pc_id+"&level="+lvl+"&height="+height;
		$.artDialog.open(url , {title: 'Move Product Category',width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true,});
	}
	
}
function changeTitle()
{
	$("#changeTitleForm").submit();
}
function refreshWindow()
{
	window.location.reload();
}
$(function(){
	$(".chzn-select").chosen({no_results_text: "no this option:"});
});
var arr_of_category = new Array();
var height_of_category = new Array();
var level_of_category = new Array();
function checkCategoryLevel(id,parent_id){
	level = 1;
	arr_of_category[id]= parent_id;
	key = id;
	while(arr_of_category[key]){
		
		if(height_of_category[arr_of_category[key]]){
			height_of_category[arr_of_category[key]] = height_of_category[arr_of_category[key]] < getCategoryHeight(key)+1 ? getCategoryHeight(key)+1 : height_of_category[arr_of_category[key]] ;
		}else{
			height_of_category[arr_of_category[key]] = getCategoryHeight(key)+1;
		}
		key=arr_of_category[key];
		level++;
	}
	level_of_category[id]=level;
	return level;
}
function getCategoryHeight(id){
	if(height_of_category[id]){
		return height_of_category[id];
	}
	return 0;
}
function getCategoryLevel(id){
	if(level_of_category[id]){
		return level_of_category[id];
	}
	return 0;
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; Basic Data »   Product Categories</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title" >
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
</form>

<div style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:950px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="30%">
	    	<form action="" id="changeTitleForm" method="post">
	    	<div class="side-by-side clearfix">
				<select name="title_id" id="title_id" onchange="changeTitle()" class="chzn-select" data-placeholder="Choose a Title..." tabindex="1"  style="width:250px">
				<option value="-1">Please Select Title...</option>
				<%
					for(int i = 0; i < titles.length; i ++)
					{
						String isSelected = "";
						if(title_id.equals(titles[i].getString("title_id")))
						{
							isSelected = "selected='selected'";
						}
				%>
					<option value='<%=titles[i].get("title_id", 0L) %>' <%=isSelected %>><%=titles[i].getString("title_name") %></option>
				<%
					}
				%>
				</select>
			</div>
			</form>
	    </td>
	    <td width="20%">
		    <input type="button" value="Import Category Titles" onclick="upload('jquery_file_up')"  class="long-long-180-button" />
			&nbsp;&nbsp;<input type="hidden" id="file_names" name="file_names" value=""/>
	    </td>
	    <td>
	   		 <input type="button" value="Export Category Titles" onclick="down()"  class="long-long-180-button"/>
	    </td>
	  </tr>
	</table>
</div>
<br/>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	    <input type="hidden" name="id">
	    <input type="hidden" name="parentid">
	    <input type="hidden" name="imp_color">
	       <tr> 
	          <th width="35%" colspan="2"  class="left-title">Product Category Name</th>
	          <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">Product Quantity</th>
	          <th width="55%"  class="right-title">&nbsp;</th>
	       </tr>
	    <tr > 
	      <td height="60" colspan="5">
	      		<script type="text/javascript">
					d = new dTree('d');
					d.add(0,-1,'Product Category Name</td><td align="left" valign="middle" width="35%"><input name="Submit" type="button" class="theme-button-add" onClick="addProductCatalogPage(0)" value="  Add Category"></td></tr></table>');
					
					<%
						DBRow [] parentCategory = proprietaryMgrZyj.findParentProductCatagorysByTitleId(true, 0L, "",title_id,null, request);
						for(int i=0;i<parentCategory.length;i++){	
					%>
						catlevel = checkCategoryLevel(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>);
						
						if(catlevel<3){
							d.add(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>,'<%=parentCategory[i].getString("title")+"&nbsp;&nbsp;HS CODE "+parentCategory[i].getString("hs_code")+":"+parentCategory[i].getString("tax")%></td><td align="center" valign="middle" width="10%"><%=productMgrZyj.getProductsByCategoryid(parentCategory[i].get("id",0l),null)%></td><td align="left" valign="middle" width="55%"><input name="Submit32" type="button" class="theme-button-del" onClick="delC(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="theme-button-edit" onClick="updateProductCategory(<%=parentCategory[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input class="theme-button-edit" type="button" value="Move" onClick="moveCatalog(<%=parentCategory[i].getString("id")%>)" />&nbsp;&nbsp; <input name="Submit" type="button" class="theme-button-add" onClick="addProductCatalogPage(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Add Sub Category">&nbsp;&nbsp;<input type="button" value="Link Product Line" class="theme-button-add" onclick="update_product_line(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/>&nbsp;&nbsp;<input type="button" value="Manage Linked Title" class="theme-button-add" onclick="addTitle(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/></td></tr></table>','');
						}else{
							d.add(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>,'<%=parentCategory[i].getString("title")+"&nbsp;&nbsp;HS CODE "+parentCategory[i].getString("hs_code")+":"+parentCategory[i].getString("tax")%></td><td align="center" valign="middle" width="10%"><%=productMgrZyj.getProductsByCategoryid(parentCategory[i].get("id",0l),null)%></td><td align="left" valign="middle" width="55%"><input name="Submit32" type="button" class="theme-button-del" onClick="delC(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="theme-button-edit" onClick="updateProductCategory(<%=parentCategory[i].getString("id")%>)" value="Rename">&nbsp;&nbsp;<input class="theme-button-edit" type="button" value="Move" onClick="moveCatalog(<%=parentCategory[i].getString("id")%>)" />&nbsp;&nbsp;<input type="button" value="Link Product Line" class="theme-button-add" onclick="update_product_line(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/>&nbsp;&nbsp;<input type="button" value="Manage Linked Title" class="theme-button-add" onclick="addTitle(<%=parentCategory[i].getString("id")%>,\'<%=parentCategory[i].getString("title")%>\')"/></td></tr></table>','');
						}	
						
					<%
						}
					%>
					document.write(d);
					
					function closeWin()
					{
						tb_remove();
					}
				</script>
	      </td>
	    </tr>
	</table>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/moveProductCatalog.action" name="move_catalog_form">
	<input type="hidden" name="move_to" id="move_to"/>
	<input type="hidden" name="catalog_id" id="catalog_id"/>	
</form>
<form action="" name="download_form" id="download_form">
</form>
</body>
</html>
<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();
function update_product_line(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/category_link_product_line.html?category_id="+id+"&category_name="+name; 
	 $.artDialog.open(uri , {title: 'Link Product Line',width:'500px',height:'300px', lock: true,opacity: 0.3,fixed: true});
}
function addTitle(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_catalog_add_title.html?productCatalogId="+id+"&productCatalogName="+name; 
	 $.artDialog.open(uri , {title: 'Manage Product Category Linked Title',width:'700px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
function down(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var title_id=$('#title_id').val();
	var para='title_id='+title_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxDownCatalogAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}
function upload(_target){
	var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
	   var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/dialog_product_catalog_title_upload.html?fileName='+fileNames; 
	   $.artDialog.open(uri , {title: '上传产品分类TITLE关系',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
}
</script>
