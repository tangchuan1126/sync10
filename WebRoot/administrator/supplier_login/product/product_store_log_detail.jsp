<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ProductStoreLogsDetailOperationTypeKey"%>
<%@ include file="../../include.jsp"%>
<%
	String cmd = StringUtil.getString(request,"cmd");
	
	DBRow[] productStoreLogDetails = new DBRow[0];
	if(cmd.equals("fromLog"))
	{
		long psl_id = StringUtil.getLong(request,"psl_id");
		
		productStoreLogDetails = productStoreLogsDetailMgrZJ.getProductStoreDetailLogsByPslid(psl_id);
	}
	else if(cmd.equals("backlog"))
	{
		long pc_id = StringUtil.getLong(request,"pc_id");
		long ps_id = StringUtil.getLong(request,"ps_id");
		int backlog_day = StringUtil.getInt(request,"backlog_day");
		
		productStoreLogDetails = productStoreLogsDetailMgrZJ.backlogGoodsStoreLogDetals(pc_id,ps_id,backlog_day);
	}
	
	
	ProductStoreLogsDetailOperationTypeKey productStoreLogsDetailOperationTypeKey = new ProductStoreLogsDetailOperationTypeKey();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>无标题文档</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script type="text/javascript">
	function showDetailHistory(psld_id)
	{	
		$.artDialog.open("product_store_log_detail_history.html?psld_id="+psld_id+"&next_back=2", {title: '批次历史',width:'800px',height:'600px', lock: true,opacity: 0.3});
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
	    <tr>
	    	<th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">仓库</th>
	    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">商品名</th>
	    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">采购价</th>
	    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">运费</th>
	    	<th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">操作</th>
	        <th nowrap="nowrap" class="left-title" style="vertical-align: center;text-align: center;">已用数量/数量</th>
	        <th nowrap="nowrap" class="right-title" style="vertical-align: center;text-align: center;">创建时间</th>
	        <th class="right-title">&nbsp;</th>
	  	</tr>
	  	<%
	  		for(int i = 0;i<productStoreLogDetails.length;i++)
	  		{
	  	%>
	  		<tr>
	  			<td align="center" height="30"><%=productStoreLogDetails[i].getString("title")%></td>
	  			<td align="left"><%=productStoreLogDetails[i].getString("p_name")%></td>
	  			<td align="center"><%=productStoreLogDetails[i].get("purchase_unit_price",0d)%></td>
	  			<td align="center"><%=productStoreLogDetails[i].get("shipping_fee",0d)%></td>
	  			<td align="center"><%=productStoreLogsDetailOperationTypeKey.getProductStoreLogsDetailOperationTypeById(productStoreLogDetails[i].getString("operation_type"))%></td>
	  			<td align="center"><%=productStoreLogDetails[i].get("used_quantity",0f)%>/<%=productStoreLogDetails[i].get("quantity",0f)%></td>
	  			<td align="center"><%=productStoreLogDetails[i].getString("create_time").subSequence(2,19)%></td>
	  			<td><input type="button" value="翻看历史" class="long-button" onclick="showDetailHistory(<%=productStoreLogDetails[i].get("psld_id",0l)%>)"/></td>
	  		</tr>
	  	<%
	  		}
	  	%>
	 </table>
</body>
</html>
