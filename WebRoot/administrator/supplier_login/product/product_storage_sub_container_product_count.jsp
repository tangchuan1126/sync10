<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="containerTypeKey" class="com.cwc.app.key.ContainerTypeKey"></jsp:useBean>
<%
long ps_id = StringUtil.getLong(request,"ps_id");
long pc_id = StringUtil.getLong(request,"pc_id");
String productName	= StringUtil.getString(request,"p_name");
long titleId		= StringUtil.getLong(request,"title_id");	         
String titleName	= StringUtil.getString(request,"title_name");	  
String lotName		= StringUtil.getString(request,"lot_name");
String locationName	= StringUtil.getString(request,"location_name");
long locationId		= StringUtil.getLong(request,"location_id"); 
long con_id		= StringUtil.getLong(request, "sub_con_id");
String sub_con_name	= StringUtil.getString(request, "sub_con_name");
double physical     = StringUtil.getDouble(request,"physical");

DBRow[] rows = productStoreMgrZJ.productCountOfContainer(ps_id, con_id, titleId, pc_id);
HashMap containerInnerTree = productStoreMgrZJ.getContainerInnerTree(ps_id, con_id, pc_id);
DBRow directlyInnerProduct = (DBRow)containerInnerTree.get("innerProduct");


%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>LP</title>
	<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
	<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript">
	function openlp(sub_sub_con_id, sub_sub_con_name, lpTypeName)
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + 
		"administrator/product/product_storage_sub_sub_container_product_count.html?p_name=<%=productName%>"
		+"&title_name=<%=titleName%>&lot_name=<%=lotName%>&location_name=<%=locationName%>"
		+"&sub_con_name=<%=sub_con_name%>&sub_sub_con_id="+sub_sub_con_id+"&sub_sub_con_name="+sub_sub_con_name;
		$.artDialog.open(url , {title: lpTypeName+'&nbsp;[&nbsp;'+sub_sub_con_name+'&nbsp;]',width:'800px',height:'650px', lock: true,opacity: 0.3,fixed: true});
	}
		
	</script>
  </head>
  
  <body>
     <fieldset style="border:1px red solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
		 <legend style="font-size:15px;font-weight:bold;"> 
			<%=productName %>
		 </legend>
		 <div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		 	<%
		 		if(!titleName.equals(""))
		 		{
		 	%>
		 	<span style="font-size:12px; font-weight: bold; color:#999">当前TITLE:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=titleName %></span><br/>
            <hr size="1px;" color="#dddddd">
            <%	
            	}
             %>
           <%
           		if(!lotName.equals(""))
           		{
           %>
            <span style="font-size:12px; font-weight: bold; padding-left:8px;color:#999">当前批次:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=lotName %></span>
            <hr size="1px;" color="#dddddd">
           <%	
           		}
           %>
           <%if(!locationName.equals("")){ %>
            <span style="font-size:12px; font-weight: bold; padding-left:8px;color:#999">当前位置:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=locationName %></span>
            <hr size="1px;" color="#dddddd">
           <%} %>
            <span style="font-size:12px; font-weight: bold; padding-left:8px;color:#999">接地容器:</span>
            <span style="font-size:12px; font-weight: bold; color:#00F"><%=sub_con_name %></span>
     	 </div>
     	 
     	     <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
     			<legend style="font-size:15px;font-weight:bold; color: red"> 	
			 		<span style="font-size:13px; font-weight: bold;">散件物理数量</span>
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">可用数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=directlyInnerProduct.get("available",0) %></span>
			          <span style="font-size:13px; font-weight: bold;">物理数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=directlyInnerProduct.get("physical",0) %></span>
			          <span style="font-size:13px; font-weight: bold;">保留数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=directlyInnerProduct.get("locked",0) %></span>
			    </div>
			 </fieldset>
     	 
     	 
             <%for(int i=0;i<rows.length;i++){ %>
	     	 <fieldset style="border:1px green solid;padding:7px;width:95%;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
			 	<legend style="font-size:15px;font-weight:bold; color: green"> 	
			 			<%
			 				boolean hasInnerContainer = productStoreMgrZJ.hasInnerContainer(ps_id,rows[i].get("con_id",0l));
			 				DBRow innerContainer = containerMgrZYZ.getDetailContainer(rows[i].get("con_id",0l));
			 				if(hasInnerContainer)
			 				{
			 			%>
			 			<a href="javascript:void(0)" onclick="openlp('<%=rows[i].get("con_id",0l) %>','','')" style="text-decoration:none; color: green" >
			 				<span style="font-size:13px; font-weight: bold;">所在托盘编码:</span>
	        				<span style="font-size:13px; font-weight: bold;"><%=innerContainer.getString("container")%></span>
							<span style="font-size:13px; font-weight: bold;margin-left: 3px;">[]</span>
			 			</a>
			 			<%			
			 				}
			 				else
			 				{
			 			%>
			 			<span style="font-size:13px; font-weight: bold;">所在托盘编码:</span>
	        			<span style="font-size:13px; font-weight: bold;"><%=innerContainer.getString("container")%></span>
	        			<span style="font-size:13px; font-weight: bold;margin-left: 3px;">[<%=containerTypeKey.getContainerTypeKeyValue(rows[i].get("container_type",0))%>]</span>
			 			<%		
			 				}
			 			%>
				 		 
			 	</legend>
			 	<div style="padding-left: 100px">
			       	  <span style="font-size:13px; font-weight: bold;">可用数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("available",0) %></span>
			          <span style="font-size:13px; font-weight: bold;">物理数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("physical",0) %></span>
			          <span style="font-size:13px; font-weight: bold;">保留数:</span>
			          <span style="font-size:13px; font-weight: bold; color:#00F"><%=rows[i].get("locked",0) %></span>
			    </div>
			 </fieldset>
			 <%} %>
   </fieldset>
  </body>
</html>
