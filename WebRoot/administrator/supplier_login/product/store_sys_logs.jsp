<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.ProductStoreOperationKey"%>
<%@page import="com.cwc.app.key.ProductStoreBillKey"%>
<%@page import="com.cwc.app.key.ProductStoreCountTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
//仓库颜色
HashMap storageColor = new HashMap();
storageColor.put("LA","<font color='#0000FF'><b>洛杉矶仓库</b></font>");
storageColor.put("SZ","<font color='#FF9900'><b>深圳仓库</b></font>");
storageColor.put("BJ","<font color='#333333'><b>北京仓库</b></font>");
storageColor.put("GZ","<font color='#FF6600'><b>广州仓库</b></font>");
storageColor.put("海运中转库","<font color='#FF6600'><b>海运中转库</b></font>");


PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

String cmd = StringUtil.getString(request,"cmd");
long oid = StringUtil.getLong(request,"oid");
String p_name = StringUtil.getString(request,"p_name");
long account = StringUtil.getLong(request,"account");

String input_st_date = StringUtil.getString(request,"input_st_date");
String input_en_date = StringUtil.getString(request,"input_en_date");

long product_line_id = StringUtil.getLong(request,"filter_productLine");
long catalog_id = StringUtil.getLong(request,"filter_pcid");
int bill_type = StringUtil.getInt(request,"bill_type");
int operotion = StringUtil.getInt(request,"operotion");
int quantity_type = StringUtil.getInt(request,"quantity_type");
long ps_id = StringUtil.getLong(request,"ps_id");
int onGroup = StringUtil.getInt(request,"onGroup");
int in_or_out = StringUtil.getInt(request,"in_or_out",0);
 
TDate tDate = new TDate();

if (input_en_date.equals(""))
{
	input_en_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}

tDate.addDay(-30);
if (input_st_date.equals(""))
{
	input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
}


DBRow storeLogs[];
if (cmd.equals("search"))
{
	storeLogs = productMgr.getSearchProductStoreSysLogsNew(input_st_date,input_en_date,product_line_id,catalog_id,p_name,oid,bill_type,operotion,quantity_type,ps_id,account,onGroup,in_or_out,pc); 
	//productMgr.getSearchProductStoreSysLogs(input_st_date,input_en_date, oid, p_name, account,0, pc);
}
else if(cmd.equals("costLog")){
	storeLogs = storageMgrLL.getCostLog(ps_id,p_name,pc);
}
else
{

	storeLogs = new DBRow[0];//productMgr.getProductStoreSysLogs(pc);
}

DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

ProductStoreOperationKey productOperationKey = new ProductStoreOperationKey();
ProductStoreBillKey productStoreBillKey = new ProductStoreBillKey();
ProductStoreCountTypeKey productStoreCountTypeKey = new ProductStoreCountTypeKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<style type="text/css">
	td.categorymenu_td div{margin-top:-3px;}
</style>

<script language="javascript">
$().ready(function() {
addAutoComplete($("#all_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
addAutoComplete($("#in_store_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
addAutoComplete($("#out_store_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
addAutoComplete($("#in_store_damaged_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
addAutoComplete($("#out_store_damaged_p_name"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
			
addAutoComplete($("#p_name1"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		"merge_info",
		"p_name");
//全部选项卡中的商品回车事件
$("#all_p_name").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("all_");
	}
});
//正常商品入库选项卡中的商品回车事件
$("#in_store_p_name").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("in_store_");
	}
});
//正常商品出库回车事件
$("#out_store_p_name").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("out_store_");
	}
});
//残损商品入库回车事件
$("#in_store_damaged_p_name").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("in_store_damaged_");
	}
});
//残损商品车库回车事件
$("#out_store_damaged_p_name").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("out_store_damaged_");
	}
});
//库存成本运费日志回车事件
$("#p_name1").keydown(function(event){
	if (event.keyCode==13)
	{
		filter("store_sys_logs_");
	}
});

		});

function dynCalendarCallback(date,month,year,objName)
{
	day = date;
	date = year+"-"+month+"-"+day;
	$("#"+objName+"_date").val(date);
}


function filter(form)
{
    form=form+"search_form";
   $("#"+form+"").submit();
}


	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:{
					id:id,
					catalog_id:catalog_id,
					divId:divId
				},
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#"+divId+"categorymenu_td").html(html);
				}
			});
	}
	
	function upload()
	{
		//tb_show('商品入库','upload_inoutfile.html?title=商品入库&filetype=in&backurl=<%=StringUtil.getCurrentURL(request)%>&TB_iframe=true&height=500&width=1000',false);
		
		$.artDialog.open("upload_inoutfile.html?title=商品入库&filetype=in&backurl=<%=StringUtil.getCurrentURL(request)%>", {title: "商品入库",width:'1000px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
	
	function ref()
	{
		window.location.reload();
	}
	
	function showStoreLogDetail(psl_id)
	{
		
		$.artDialog.open("product_store_log_detail.html?psl_id="+psl_id+"&cmd=fromLog", {title: "日志详细",width:'800px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   库存日志</td>
  </tr>
</table>
<br>
<div class="demo">
  <div align="left" id="tabs">
	<ul>
		<li><a href="#all">全部</a></li>
  	    <li><a href="#in_store">正常商品&nbsp;&nbsp;入库</a></li>
  	    <li><a href="#out_store">正常商品&nbsp;&nbsp;出库</a></li>
  	    <li><a href="#in_store_damaged">残损商品&nbsp;&nbsp;入库</a></li>
  	    <li><a href="#out_store_damaged">残损商品&nbsp;&nbsp;出库</a></li>
	  	<li><a href="#search">单号搜索</a></li>
	  	<tst:authentication bindAction="com.cwc.app.api.ProductMgr.addInProduct">
	  		<li><a href="#upload">上传入库</a></li>
	  	</tst:authentication>
	  	<li><a href="#costLog">库存成本运费日志</a></li>
	</ul>
	<div id="all" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="store_sys_logs.html" method="post" id="all_search_form" name="all_search_form">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="all_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					all_input_st = new dynCalendar('all_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="all_productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  DBRow p1[] = productLineMgrTJH.getAllProductLine();
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="all_productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="all_filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表<input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="submit" class="button_long_refresh" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="all_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				all_input_en = new dynCalendar('all_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="all_categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="all_categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="all_category" value="" />
						  <input type="hidden" name="filter_pcid" id="all_filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										String qx;
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
			   					<select id="operotion" name="operotion">
			   						<option>全部操作</option>
			   						<%
			   							ArrayList allOperationList = productOperationKey.getProductStoreOperation();
			   							
			   							for(int i = 0;i<allOperationList.size();i++)
			   							{
			   						%>
			   						<option <%=allOperationList.get(i).toString().equals(String.valueOf(operotion))?"selected=\"selected\"":""%> value="<%=allOperationList.get(i)%>"><%=productOperationKey.getProductStoreOperationId(allOperationList.get(i).toString())%></option>
			   						<%
			   							}
			   						%>
			   					</select>
			   					&nbsp;&nbsp;&nbsp;
			   					 
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									DBRow admins[] = adminMgr.getAllAdmin(null);
									
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="all_p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<select id="quantity_type" name="quantity_type">
				           			<option value="0">全部</option>
				           			<option <%=quantity_type==ProductStoreCountTypeKey.STORECOUNT?"selected=\"selected\"":""%> value="<%=ProductStoreCountTypeKey.STORECOUNT%>">正常商品</option>
				           			<option <%=quantity_type==ProductStoreCountTypeKey.DAMAGED?"selected=\"selected\"":""%> value="<%=ProductStoreCountTypeKey.DAMAGED%>">残损商品</option>
				           		</select>
				           		&nbsp;&nbsp;
				           		<select id="in_or_out" name="in_or_out">
				           			<option value="0">全部</option>
				           			<option <%=in_or_out==1?"selected=\"selected\"":""%> value="1">入库</option>
				           			<option <%=in_or_out==2?"selected=\"selected\"":""%> value="2">出库</option>
				           		</select>
				           	</td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
	
	<div id="in_store" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="store_sys_logs.html" method="post" id="in_store_search_form">
				<input type="hidden" name="in_or_out" value="1">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="in_store_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					in_store_input_st = new dynCalendar('in_store_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="in_store_productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="in_store_productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="in_store_filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表<input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="submit" class="button_long_refresh" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="in_store_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				in_store_input_en = new dynCalendar('in_store_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="in_store_categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="in_store_categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="in_store_category" value="" />
						  <input type="hidden" name="filter_pcid" id="in_store_filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
			   					<select id="operotion" name="operotion">
			   						<option>全部正常商品入库</option>
			   						<%
			   							ArrayList inStoreOperationList = productOperationKey.getProdcutInStoreOperation();
			   							
			   							for(int i = 0;i<inStoreOperationList.size();i++)
			   							{
			   						%>
			   						<option <%=inStoreOperationList.get(i).toString().equals(String.valueOf(operotion))?"selected=\"selected\"":""%> value="<%=inStoreOperationList.get(i)%>"><%=productOperationKey.getProductStoreOperationId(inStoreOperationList.get(i).toString())%></option>
			   						<%
			   							}
			   						%>
			   					</select>
			   					&nbsp;&nbsp;&nbsp;
			   					 
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="in_store_p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<input type="hidden" id="quantity_type" name="quantity_type" value="<%=ProductStoreCountTypeKey.STORECOUNT%>"/>
				           	</td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
		
		<div id="out_store" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="store_sys_logs.html" method="post" id="out_store_search_form">
				<input type="hidden" name="in_or_out" value="2">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="out_store_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					out_store_input_st = new dynCalendar('out_store_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="out_store_productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="out_store_productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="out_store_filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表<input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="submit" class="button_long_refresh" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="out_store_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				out_store_input_en = new dynCalendar('out_store_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="out_store_categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="out_store_categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="out_store_category" value="" />
						  <input type="hidden" name="filter_pcid" id="out_store_filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
			   					<select id="operotion" name="operotion">
			   						<option>全部类型</option>
			   						<%
			   							ArrayList outStoreOperationList = productOperationKey.getProductOutStoreOperation();
			   							for(int i = 0;i<outStoreOperationList.size();i++)
			   							{
			   						%>
			   						<option <%=outStoreOperationList.get(i).toString().equals(String.valueOf(operotion))?"selected=\"selected\"":""%> value="<%=outStoreOperationList.get(i)%>"><%=productOperationKey.getProductStoreOperationId(outStoreOperationList.get(i).toString())%></option>
			   						<%
			   							}
			   						%>
			   					</select>
			   					&nbsp;&nbsp;&nbsp;
			   					 
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="out_store_p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<input type="hidden" id="quantity_type" name="quantity_type" value="<%=ProductStoreCountTypeKey.STORECOUNT%>">
				           	</td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
		
		<div id="in_store_damaged" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="store_sys_logs.html" method="post" id="in_store_damaged_search_form">
				<input type="hidden" name="in_or_out" value="1">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="in_store_damaged_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					in_store_damaged_input_st = new dynCalendar('in_store_damaged_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="in_store_damaged_productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="in_store_damaged_productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="in_store_damaged_filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表<input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="submit" class="button_long_refresh" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="in_store_damaged_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				in_store_damaged_input_en = new dynCalendar('in_store_damaged_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="in_store_damaged_categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="in_store_damaged_categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="in_store_damaged_category" value="" />
						  <input type="hidden" name="filter_pcid" id="in_store_damaged_filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
			   					<select id="operotion" name="operotion">
			   						<option>全部残损商品入库</option>
			   						<%
			   							ArrayList inStoreDamagedOperationList = productOperationKey.getProductInStoreDamagedOperation();
			   							for(int i = 0;i<inStoreDamagedOperationList.size();i++)
			   							{
			   						%>
			   						<option <%=inStoreDamagedOperationList.get(i).toString().equals(String.valueOf(operotion))?"selected=\"selected\"":""%> value="<%=inStoreDamagedOperationList.get(i)%>"><%=productOperationKey.getProductStoreOperationId(inStoreDamagedOperationList.get(i).toString())%></option>
			   						<%
			   							}
			   						%>
			   					</select>
			   					&nbsp;&nbsp;&nbsp;
			   					 
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="in_store_damaged_p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<input type="hidden" id="quantity_type" name="quantity_type" value="<%=ProductStoreCountTypeKey.DAMAGED%>">
				           	</td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
		
		<div id="out_store_damaged" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<form action="store_sys_logs.html" method="post" id="out_store_damaged_search_form">
				<input type="hidden" name="in_or_out" value="2">
			   		<table width="100%" border="0">
			   			<tr>
			   				<td width="13%">
			   					开始&nbsp;&nbsp;<input name="input_st_date" type="text" id="out_store_damaged_input_st_date" size="9" value="<%=input_st_date%>" readonly="readonly"> 
		    			     <script language="JavaScript" type="text/javascript" >
		    					out_store_damaged_input_st = new dynCalendar('out_store_damaged_input_st', 'dynCalendarCallback', '../js/images/');
		    				 </script>
			   				</td>
			   				<td align="left" width="416">
			   					<ul id="out_store_damaged_productLinemenu" class="mcdropdown_menu">
					             <li rel="0">所有产品线</li>
					             <%
									  for (int i=0; i<p1.length; i++)
									  {
											out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
											 
											out.println("</li>");
									  }
								  %>
					           </ul>
					           <input type="text" name="productLine" id="out_store_damaged_productLine" value="" />
					           <input type="hidden" name="filter_productLine" id="out_store_damaged_filter_productLine" value="0"/>
			   				</td>
			   				<td>
			   					<input name="onGroup" type="radio" value="0" <%=onGroup==0?"checked=\"checked\"":""%>/>列表<input type="radio" name="onGroup" <%=onGroup==1?"checked=\"checked\"":""%> value="1">汇总
						        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						        <input name="Submit" type="submit" class="button_long_refresh" value="过滤">
			   				</td>
			   			</tr>
			   			<tr>
			   				<td>
			   					结束&nbsp;&nbsp;<input name="input_en_date" type="text" id="out_store_damaged_input_en_date" size="9" value="<%=input_en_date%>" readonly="readonly">
							   <script language="JavaScript" type="text/javascript">
				    				out_store_damaged_input_en = new dynCalendar('out_store_damaged_input_en', 'dynCalendarCallback', '../js/images/');
								</script>
							</td>
			   				<td id="out_store_damaged_categorymenu_td" class="categorymenu_td" width="423" align="left" valign="bottom" style="padding-top:10px;">
			   					<ul id="out_store_damaged_categorymenu" class="mcdropdown_menu">
								  <li rel="0">所有分类</li>
								  <%
									  for (int i=0; i<c1.length; i++)
									  {
											out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
								
											  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
											  if (c2.length>0)
											  {
											  		out.println("<ul>");	
											  }
											  for (int ii=0; ii<c2.length; ii++)
											  {
													out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
													
														DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
														  if (c3.length>0)
														  {
																out.println("<ul>");	
														  }
															for (int iii=0; iii<c3.length; iii++)
															{
																	out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																	out.println("</li>");
															}
														  if (c3.length>0)
														  {
																out.println("</ul>");	
														  }
														  
													out.println("</li>");				
											  }
											  if (c2.length>0)
											  {
											  		out.println("</ul>");	
											  }
											  
											out.println("</li>");
									  }
									  %>
								</ul>
						  <input type="text" name="category" id="out_store_damaged_category" value="" />
						  <input type="hidden" name="filter_pcid" id="out_store_damaged_filter_pcid" value="0"/>
			   				</td>
			   				<td>
			   					<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
			   					<select id="operotion" name="operotion">
			   						<option>全部残损商品出库</option>
			   						<%
			   							ArrayList outStoreDamagedOperationList = productOperationKey.getProductOutStoreDamagedOperation();
			   							for(int i = 0;i<outStoreDamagedOperationList.size();i++)
			   							{
			   						%>
			   						<option <%=outStoreDamagedOperationList.get(i).toString().equals(String.valueOf(operotion))?"selected=\"selected\"":""%> value="<%=outStoreDamagedOperationList.get(i)%>"><%=productOperationKey.getProductStoreOperationId(outStoreDamagedOperationList.get(i).toString())%></option>
			   						<%
			   							}
			   						%>
			   					</select>
			   					&nbsp;&nbsp;&nbsp;
			   					 
			   				</td>
			   				
			   					
			   			</tr>
			   			<tr>
			   				<td>
			   					<select name="account" id="account">
					  				<option value="">全部管理员</option>
								<%
									for ( int i=0; i<admins.length; i++ )
									{
								%>
									<option value="<%=admins[i].get("adid",0l)%>" <%=account == admins[i].get("adid",0l)?"selected":""%>> 
										<%=admins[i].getString("employe_name")%>       
									</option>
								<%
									}
									%>
		         				</select>
		         			</td>
			   				<td style="">	   					
		               			商品名 <input style="width: 375px;" name="p_name" type="text" id="out_store_damaged_p_name" value="<%=p_name%>">
				           	</td>
				           <td>
				           		<input type="hidden" id="quantity_type" name="quantity_type" value="<%=ProductStoreCountTypeKey.DAMAGED%>">
				           	</td>
			   			</tr>
			   		</table>
				<input type="hidden" name="cmd" value="search">
		    </form>
		</div>
		
		<div id="search">
			<form action="store_sys_logs.html" method="post">
				<input name="cmd" value="search" type="hidden"/>
				<table width="100%">
					<tr>
						<td nowrap="nowrap">
					       <input type="radio" name="bill_type" value="0" <%=bill_type==0?"checked=\"checked\"":""%>/>全部
					          <%
					           		ArrayList<String> billList = productStoreBillKey.getProductStoreBill();
					           		for(int j = 0;j<billList.size();j++)
					           		{
					          %>
					          		<input <%=billList.get(j).equals(String.valueOf(bill_type))?"checked=\"checked\"":""%> type="radio" name="bill_type" value="<%=billList.get(j)%>"/><%=productStoreBillKey.getProductStoreBillId(billList.get(j))%>
					          <%
					          		}
					          %>
					         <input name="oid" type="text" id="oid" style="width:70px;" value="<%=oid%>">号
					         &nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" class="button_long_search" value="查询">
					    </td>
					</tr>
				</table>
			</form>
		</div>
   <div id="upload" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
   	<table width="100%">
   		<tr>
   			<td>
   				<tst:authentication bindAction="com.cwc.app.api.ProductMgr.addInProduct">
					<input name="Submit23" type="button" class="long-button-upload" onClick="upload()" value=" 上传日志 ">
				</tst:authentication>
   			</td>
   		</tr>
   		
   	</table>
   </div>
   <div id="costLog" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
   	<table width="100%">
   		<tr>
   			<td>
   			<form action="store_sys_logs.html" method="post" id="store_sys_logs_search_form">
				<input name="cmd" value="costLog" type="hidden"/>
				商品名 <input style="width: 375px;" name="p_name" type="text" id="p_name1" value="<%=p_name%>">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<select name="ps_id" id="ps_id">
									<option value="0">所有仓库</option>
										<%
										qx="";
										for ( int i=0; i<treeRows.length; i++ )
										{
											if ( treeRows[i].get("parentid",0) != 0 )
											 {
											 	qx = "├ ";
											 }
											 else
											 {
											 	qx = "";
											 }
										%>
										          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
										          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
										          <%=qx%>
										          <%=treeRows[i].getString("title")%>          </option>
										          <%
										}
										%>
						        </select>
						        &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" class="button_long_refresh" onClick="" value="过滤">
			</form>
   			</td>
   		</tr>
   	</table>
   </div>
   </div>
</div>
				  <script>
					$("#tabs").tabs({
						cache: true,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						cookie: {expires: 30000},
						load: function(event, ui) {onLoadInitZebraTable();}	
					});
					</script>	



<br>
<%
	if(!cmd.equals("costLog")) {
%>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
    	<th style="vertical-align: center;text-align: center;" class="left-title">日志ID</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="left" class="right-title" style="vertical-align: center;text-align: left;">商品</th>
        <th width="13%" style="vertical-align: center;text-align: center;" class="right-title" >操作</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">正常/残损</th>
        <th width="6%" style="vertical-align: center;text-align: center;" class="right-title">消费库存</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">操作人</th>
        <th width="8%" style="vertical-align: center;text-align: center;" class="right-title">关联单据类型</th>
        <th width="7%" style="vertical-align: center;text-align: center;" class="right-title">关联单号</th>
        <th width="14%" style="vertical-align: center;text-align: center;" class="right-title">时间</th>
    </tr>

    <%


for ( int i=0; i<storeLogs.length; i++ )
{

%>
    <tr>
      <td align="center"><a href="javascript:showStoreLogDetail(<%=storeLogs[i].get("psl_id",0l)%>)"><%=storeLogs[i].get("psl_id",0l)%></a></td>
      <td width="6%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageColor.get(storeLogs[i].getString("title"))%></td>
      <td width="20%" align="left" valign="middle" style='word-break:break-all;' ><%=storeLogs[i].getString("p_name")%></td>
      <td align="center" valign="middle" >
      	<%
      		if(onGroup==0)
      		{
      	%>
      		<%=productOperationKey.getProductStoreOperationId(storeLogs[i].getString("operation")).equals("")?storeLogs[i].getString("operation"):productOperationKey.getProductStoreOperationId(storeLogs[i].getString("operation"))%>
      	<%
      		}
      	%>
      	&nbsp;
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
      	<%=productStoreCountTypeKey.getProductStoreCountTypeKeyId(storeLogs[i].getString("quantity_type"))%>
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
      	<%=storeLogs[i].get("quantity",0f)>=0?storeLogs[i].get("quantity",0f):"<span style='color:red;'>"+storeLogs[i].get("quantity",0f)+"</span>"%></td>
      <td  align="center" valign="middle" style='word-break:break-all;'>
      	<% 
      		if(onGroup==0)
      		{
	      		DBRow admin = adminMgrLL.getAdminById(storeLogs[i].getString("adid"));
	      		if(admin==null)
	      		{
	      			admin = new DBRow();
	      		}
	      		
	      		out.print(admin.getString("employe_name"));
	      	}
      	%>&nbsp;
      </td>
      <td  align="center" valign="middle" style='word-break:break-all;'>
      	<%
      		if(onGroup==0)
      		{
      	%>
      	<%=productStoreBillKey.getProductStoreBillId(storeLogs[i].getString("bill_type"))%>
      	<%
      		}
      	%>&nbsp;
      </td>
      <td   align="center" valign="middle" style='word-break:break-all;'>
	  <%
	  	if(onGroup==0)
	  	{
	  		if (storeLogs[i].get("oid",0l)>0)
			{
			 	out.println("<a href='../order/ct_order_auto_reflush.html?val="+storeLogs[i].getString("oid")+"&cmd=search&search_field=3' target='_blank'>"+storeLogs[i].getString("oid")+"</a>");
			}
			else
			{
			  	out.println(storeLogs[i].get("oid",0l));
			}
	  	}
	  %>&nbsp;
	  </td>
      <td align="center" valign="middle">
      	<% 
      		if(onGroup==0)
	  		{
      	%>
      	<%=storeLogs[i].getString("post_date")%>
      	<%
      		}
      	%>&nbsp;
      </td>
    </tr>
    <%
}
%>
<% 
	}else {
%>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="8%" style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">商品</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">数量</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">运费</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">成本</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">修改人</th>
        <th style="vertical-align: center;text-align: center;" class="left-title">修改时间</th>
    </tr>
    <%
    for ( int i=0; i<storeLogs.length; i++ ) {
    %>
    <tr>
    	<td height="60" align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("title") %></td>
    	<td align="left" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("p_name") %></td>
    	<td align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("store_count") %></td>
    	<td align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("storage_unit_freight") %></td>
    	<td align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("storage_unit_price") %></td>
    	<td align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("employe_name") %></td>
    	<td align="center" valign="middle" style='word-break:break-all;'><%=storeLogs[i].getString("post_date") %></td>
    </tr>
    <%
    }
    %>
</table>
<%		
	}
%>
</form>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
		  
		  <input type="hidden" name="oid" value="<%=oid%>">
		  <input type="hidden" name="p_name" value="<%=p_name%>">
		  <input type="hidden" name="account" value="<%=account%>">
		  <input type="hidden" name="input_st_date" value="<%=input_st_date%>">
		  <input type="hidden" name="input_en_date" value="<%=input_en_date%>">
		  <input type="hidden" name="cmd" value="<%=cmd%>">
		  
		  <input type="hidden" name="filter_productLine" value="<%=product_line_id%>"/>
		  <input type="hidden" name="filter_pcid" value="<%=catalog_id%>"/>
		  <input type="hidden" name="bill_type" value="<%=bill_type%>"/>
		  <input type="hidden" name="operotion" value="<%=operotion %>"/>
		  <input type="hidden" name="quantity_type" value="<%=quantity_type%>"/>
		  <input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		  <input type="hidden" name="onGroup" value="<%=onGroup%>"/>
		  <input type="hidden" name="in_or_out" value="<%=in_or_out%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">
$("#all_category").mcDropdown("#all_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#all_filter_pcid").val(id);
				}
});
$("#all_productLine").mcDropdown("#all_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#all_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"all_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"all_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("in_store_");
					}
				}
});

$("#in_store_category").mcDropdown("#in_store_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#in_store_filter_pcid").val(id);
				}
});
$("#in_store_productLine").mcDropdown("#in_store_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#in_store_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"in_store_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"in_store_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("in_store_");
					}
				}
});


$("#out_store_category").mcDropdown("#out_store_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#out_store_filter_pcid").val(id);
				}
});
$("#out_store_productLine").mcDropdown("#out_store_productLinemenu",{
		allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#out_store_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"out_store_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"out_store_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("out_store_");
					}
				}
});

$("#in_store_damaged_category").mcDropdown("#in_store_damaged_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#in_store_damaged_filter_pcid").val(id);
				}
});
$("#in_store_damaged_productLine").mcDropdown("#in_store_damaged_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#in_store_damaged_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"in_store_damaged_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"in_store_damaged_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("in_store_damaged_");
					}
				}
});

$("#out_store_damaged_category").mcDropdown("#out_store_damaged_categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#out_store_damaged_filter_pcid").val(id);
				}
});
$("#out_store_damaged_productLine").mcDropdown("#out_store_damaged_productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#out_store_damaged_filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"out_store_damaged_",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"out_store_damaged_",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("out_store_damaged_");
					}
				}
});
<%
if (catalog_id>0)
{
%>
$("#all_category").mcDropdown("#all_categorymenu").setValue(<%=catalog_id%>);
$("#in_store_category").mcDropdown("#in_store_categorymenu").setValue(<%=catalog_id%>);
$("#out_store_category").mcDropdown("#out_store_categorymenu").setValue(<%=catalog_id%>);
$("#in_store_damaged_category").mcDropdown("#in_store_damaged_categorymenu").setValue(<%=catalog_id%>);
$("#out_store_damaged_category").mcDropdown("#out_store_damaged_categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#all_category").mcDropdown("#all_categorymenu").setValue(0);
$("#in_store_category").mcDropdown("#in_store_categorymenu").setValue(0);
$("#out_store_category").mcDropdown("#out_store_categorymenu").setValue(0);
$("#in_store_damaged_category").mcDropdown("#in_store_damaged_categorymenu").setValue(0);
$("#out_store_damaged_category").mcDropdown("#out_store_damaged_categorymenu").setValue(0);
<%
}
%> 

<%
if (product_line_id!=0)
{
%>
$("#all_productLine").mcDropdown("#all_productLinemenu").setValue(<%=product_line_id%>);
$("#in_store_productLine").mcDropdown("#in_store_productLinemenu").setValue(<%=product_line_id%>);
$("#out_store_productLine").mcDropdown("#out_store_productLinemenu").setValue(<%=product_line_id%>);
$("#in_store_damaged_productLine").mcDropdown("#in_store_damaged_productLinemenu").setValue(<%=product_line_id%>);
$("#out_store_damaged_productLine").mcDropdown("#out_store_damaged_productLinemenu").setValue(<%=product_line_id%>);
<%
}
else
{
%>
$("#all_productLine").mcDropdown("#all_productLinemenu").setValue(0);
$("#in_store_productLine").mcDropdown("#in_store_productLinemenu").setValue(0);
$("#out_store_productLine").mcDropdown("#out_store_productLinemenu").setValue(0);
$("#in_store_damaged_productLine").mcDropdown("#in_store_damaged_productLinemenu").setValue(0);
$("#out_store_damaged_productLine").mcDropdown("#out_store_damaged_productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
