<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long ps_id = StringUtil.getLong(request,"ps_id");
String ps_name = StringUtil.getString(request,"ps_name");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/tree/jquery.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.js"></script>
<script type="text/javascript" src="../js/tree/jquery.tree.checkbox.js"></script>

<script type="text/javascript" src="../js/loadmask/jquery.loadmask.js"></script>
<link rel="stylesheet" href="../js/loadmask/jquery.loadmask.css" type="text/css" media="screen">
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

	<script type="text/javascript" class="source">
	$(function () { 
		$("#demo_1").tree({
			ui : {
				theme_name : "checkbox"
			},
			rules : {
				// only nodes of type root can be top level nodes
				valid_children : [ "storage" ],
				multiple:true,	//支持多选
				drag_copy:false	//禁止拷贝
			},
			types : {
				// all node types inherit the "default" node type
				"default" : {
					deletable : false,
					renameable : false
				},
				"storage" : {
					draggable : false,
					valid_children : [ "country" ],

				},
				"country" : {
					valid_children : "none",
					max_depth :0,
					
				}
			},

			plugins : {
						checkbox : {}
					}

			
		});
	});
	
	</script>
	
	<style type="text/css">
	html, body { margin:0px; padding:0px; }
	body, td, th, pre, code, select, option, input, textarea { font-family:"Trebuchet MS", Sans-serif; font-size:10pt; }
	.demo {
		 float:left; 
		 margin:5px;
		 border:0px solid gray; 
		 font-family:Verdana;
		 font-size:12px;
		 background:white; 

	}
	</style>

<script>
	function saveCheckBox()
	{
		var checkedNode = $.tree.plugins.checkbox.get_checked($.tree.reference("#demo_1"));
		var para = "sid=<%=ps_id%>&";
		
			for (i=0; i<checkedNode.length-1; i++)
			{
				para += "ccid="+checkedNode[i].id+"&";
			}
			
			if (checkedNode.length>0)
			{
				para += "ccid="+checkedNode[i].id;
			}

			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/BatchModStorageCountry.action',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
						
				beforeSend:function(request){
				},
						
				error: function(){
					alert("网络错误！");
				},
						
				success: function(html){
					if (html=="ok")
					{
						alert("保存成功！");
						parent.tb_remove();
						parent.location.reload();
					}
					else
					{
						alert("保存出错！");
					}
				}
		    });

	}


function initCheckBox()
{
	<%
	DBRow matchCountry[] = productMgr.getStorageCountryBySid(ps_id);
	for (int i=0; i<matchCountry.length; i++)
	{
	%>
	$.tree.plugins.checkbox.check($("#<%=matchCountry[i].get("ccid",0l)%>"));
	<%
	}
	%>
}

	
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="initCheckBox()">
<div id="loadingmask" style="width:100%;height:30%;">
	<div class="demo" id="demo_1">
		<ul>

			<li id="0" rel="storage" class="open"><a href="#" style="font-weight:bold"><ins>&nbsp;</ins><%=ps_name%></a>
				<ul>
			<%
			   DBRow country[] = productMgr.getFreeStorageCountrys(ps_id);
			    for (int j=0; j<country.length; j++){
			%>
						<li id="<%=country[j].getString("ccid")%>" rel="country"><a href="#"><ins>&nbsp;</ins><%=country[j].getString("c_country")%></a></li>
			<%}%>
				</ul>
			</li>

		</ul>
	</div>
</div>

<br>

<div style="padding-left:20px;margin-top:30px;">
<input name="Submit" type="button" class="long-button-ok" value="保存设置" onClick="saveCheckBox()">
&nbsp;&nbsp;&nbsp;
<input name="Submit2" type="button" class="short-short-button" value="取消" onClick="closeWin()">

</div>

<br>
<br>
<script type="text/javascript">
function closeWin(){
	$.artDialog.close();
}
</script>


</body>
</html>
