<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.WayBillOrderStatusKey"%>
<%@page import="com.cwc.app.api.zj.PurchaseKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="javax.mail.Transport"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.api.AdminLoggerBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%

String cmd = StringUtil.getString(request,"cmd");

String p_name = StringUtil.getString(request,"p_name");

long catalog_id = StringUtil.getLong(request,"catalog_id");
long product_line_id = StringUtil.getLong(request,"product_line_id");
long ps_id = StringUtil.getLong(request,"ps_id");
int backlog_day = StringUtil.getInt(request,"backlog_day",90);

AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session);

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


if (cmd.equals(""))
{
	ps_id = adminLoggerBean.getPs_id();
}


DBRow rows[] = productStoreLogsDetailMgrZJ.backlogOfGoods(product_line_id,catalog_id,ps_id,p_name,backlog_day,pc);
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>积压商品管理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 
 

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<!--  自动填充 -->

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	addAutoComplete($("#filter_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"merge_info",
		"p_name");
	
	$("#filter_name").keydown(function(event){
		if (event.keyCode==13)
		{
			searchByName();
		}
	});
	
});


function onMOBg(row,cl,index)
{
	if ( !document.listForm.pid_batch[index].checked )
	{
		row.style.background=cl;
	}
}

function searchByCid()
{
	document.search_form.cmd.value = "filter";
	document.search_form.ps_id.value = $("#filter_cid").val();	
	document.search_form.catalog_id.value = $("#filter_pcid").val();
	document.search_form.product_line_id.value = $("#filter_productLine").val();
	document.search_form.backlog_day.value = $("#backlog_day_text").val();
	document.search_form.submit();
}

function searchByName()
{
	if ($("#filter_name").val()=="")
	{
		alert("请填写关键词");
	}
	else
	{
		document.search_form.cmd.value = "search";
		document.search_form.p_name.value = $("#filter_name").val();	
		document.search_form.ps_id.value = $("#filter_cid").val();
		document.search_form.backlog_day.value = $("#backlog_day_text").val();
		document.search_form.submit();
	}
}


function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 




function closeWin()
{
	tb_remove();
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}

	function ajaxLoadCatalogMenuPage(id,divId,catalog_id)
	{
		var para = "id="+id+"&catalog_id="+catalog_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
				},
				
				success: function(html)
				{
					$("#categorymenu_td").html(html);
				}
			});
	}
	
	function showStoreLogDetails(pc_id,ps_id,backlog_day)
	{
		$.artDialog.open("product_store_log_detail.html?pc_id="+pc_id+"&ps_id="+ps_id+"&backlog_day="+backlog_day+"&cmd=backlog", {title: "日志详细",width:'800px',height:'500px',fixed:true, lock: true,opacity: 0.3});
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>

<form action="backlog_goods.html" method="get" name="search_form">		
  <input type="hidden" name="cmd"> 
  <input type="hidden" name="ps_id"> 
  <input type="hidden" name="catalog_id"> 
  <input type="hidden" name="p_name">
  <input type="hidden" name="product_line_id"/> 
  <input type="hidden" name="backlog_day"/>
</form>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td width="54%" height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td width="416" style="padding-left:10px;">
             <ul id="productLinemenu" class="mcdropdown_menu">
             <li rel="0">所有产品线</li>
             <%
				  DBRow p1[] = productLineMgrTJH.getAllProductLine();
				  for (int i=0; i<p1.length; i++)
				  {
						out.println("<li rel='"+p1[i].get("id",0l)+"'> "+p1[i].getString("name"));
						 
						out.println("</li>");
				  }
			  %>
           </ul>
           <input type="text" name="productLine" id="productLine" value="" />
           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"/>
	</td>
              <td width="317" height="30"  style="border-bottom:1px #dddddd solid">
              <select name="filter_cid" id="filter_cid">
				<option value="0">所有仓库</option>
				<%
				String qx;
				
				for ( int i=0; i<treeRows.length; i++ )
				{
					if ( treeRows[i].get("parentid",0) != 0 )
					 {
					 	qx = "├ ";
					 }
					 else
					 {
					 	qx = "";
					 }
				%>
				          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
				          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
				          <%=qx%>
				          <%=treeRows[i].getString("title")%>          </option>
				          <%
				}
				%>
        </select>&nbsp;&nbsp;
		存放时间<input style="width:40px" id="backlog_day_text" value="<%=backlog_day%>"/>天
		&nbsp;&nbsp;
        <input name="Submit4" type="button" class="button_long_refresh" onClick="searchByCid()" value="过滤">			  
        </td>
 		</tr>
            <tr>
			<td id="categorymenu_td" width="423" align="left" valign="bottom" bgcolor="#eeeeee" style="padding-left:10px;padding-top:10px;"> 
			           <ul id="categorymenu" class="mcdropdown_menu">
						  <li rel="0">所有分类</li>
						  <%
							  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
							  for (int i=0; i<c1.length; i++)
							  {
									out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
						
									  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
									  if (c2.length>0)
									  {
									  		out.println("<ul>");	
									  }
									  for (int ii=0; ii<c2.length; ii++)
									  {
											out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
											
												DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
												  if (c3.length>0)
												  {
														out.println("<ul>");	
												  }
													for (int iii=0; iii<c3.length; iii++)
													{
															out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
															out.println("</li>");
													}
												  if (c3.length>0)
												  {
														out.println("</ul>");	
												  }
												  
											out.println("</li>");				
									  }
									  if (c2.length>0)
									  {
									  		out.println("</ul>");	
									  }
									  
									out.println("</li>");
							  }
							  %>
						</ul>
				  <input type="text" name="category" id="category" value="" />
				  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"/>
			</td>
              <td height="30">
              	<input name="filter_name" type="text" id="filter_name" value="<%=p_name%>" style="width:200px;" onClick="cleanProductLine('')"> &nbsp;&nbsp;
        <input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="searchByName()">			  </td>
            </tr>
          </table>

			</div>
		  </td>
    <td width="46%" align="right" style="padding-right:10px;">
    &nbsp;&nbsp;
	</td>
  </tr>
</table>
	</td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">


    <tr> 
        <th width="325" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
        <th width="258" align="left" class="right-title"  style="vertical-align: center;text-align: left;">商品分类</th>
        <th width="325"  style="vertical-align: center;text-align: center;" class="right-title">积压数量</th>
        <th width="204"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

    <%
String delPCID = "";
DBRow proGroup[];
for ( int i=0; i<rows.length; i++ )
{
delPCID += rows[i].getString("pc_id")+",";
%>
    <tr > 
      <td height="60" valign="middle" style='font-size:14px;'>
      	<%=rows[i].getString("p_name")%>
      </td>
      <td height="60" align="left" valign="middle"   style='line-height:20px;' >
	   <span style="color:#999999">
	  <%
	  DBRow allCatalogFather[] = catalogTree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allCatalogFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=search&catalog_id="+allCatalogFather[jj].getString("id")+"&ps_id="+ps_id+"&backlog_day="+backlog_day+"'>"+allCatalogFather[jj].getString("title")+"</a><br>");

	  }
	  %>
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=search&catalog_id="+rows[i].getString("catalog_id")+"&ps_id="+ps_id+"&backlog_day='"+backlog_day+">"+catalog.getString("title")+"</a>");
	  }
	  %>	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;color:red;font-weight:bold'>
	  	<%=rows[i].get("backlog_quantity",0f) %>
	  </td>
      <td align="center" valign="middle">
      	<input type="button" class="long-button" onclick="showStoreLogDetails(<%=rows[i].get("pc_id",0l)%>,<%=rows[i].get("ps_id",0l)%>,<%=backlog_day%>)" value="查看明细"/>
 	  </td>
    </tr>
<%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
     <input type="hidden" name="p" >
	 <input type="hidden" name="ps_id" value="<%=ps_id%>">
	 <input type="hidden" name="cmd" value="<%=cmd%>">
	 <input type="hidden" name="p_name" value="<%=p_name%>">
	 <input type="hidden" name="catalog_id" value="<%=catalog_id%>">
	 <input type="hidden" name="product_line_id" value="<%=product_line_id%>"/>
	 <input type="hidden" name="backlog_day" value="<%=backlog_day%>"/>
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">

function ref()
{
	window.location.reload();
}
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}
});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id==<%=product_line_id%>)
					{
						ajaxLoadCatalogMenuPage(id,"",<%=catalog_id%>);
					}
					else
					{
						ajaxLoadCatalogMenuPage(id,"",0);
					}
					
					if(id!=0)
					{
						cleanSearchKey("");
					}
				}
});
<%
if (catalog_id>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=catalog_id%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (product_line_id!=0)
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=product_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>

function cleanProductLine(divId)
{
	$("#"+divId+"category").mcDropdown("#"+divId+"categorymenu").setValue(0);
	$("#"+divId+"productLine").mcDropdown("#"+divId+"productLinemenu").setValue(0);
}

function cleanSearchKey(divId)
{
	$("#"+divId+"filter_name").val("");
}
</script>
</body>
</html>
