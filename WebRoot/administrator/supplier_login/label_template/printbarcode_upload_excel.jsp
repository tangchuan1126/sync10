<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<%
	int refresh = StringUtil.getInt(request,"refresh");
	
	long lable_template_id = StringUtil.getLong(request,"lable_template_id");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>
<script>
function uploadProduct()
{
	 if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		if(filename[filename.length-1]=="xls")
		{
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">检查文件内容是否有错误<br/>请稍后......</span>'});
			
			document.upload_form.submit();			
		}
		else
		{
			alert("只可上传2003版excel文件");
		}	
	}
}

function refresh()
{
	<%
		if(refresh==1)
		{
	%>
		parent.closeWin();
	<%
		}
		else
		{
	%>
		parent.closeWinNotRefresh();
	<%
		}
	%>
}

function exportProductBarcodeByPurchase()
{
	var purchase_id = $("#purchase_id").val();
	if(purchase_id!=0)
	{
		var para = "id="+purchase_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductBarcodeByPurchase.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(){
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.export_product_barcode.action=date["fileurl"];
							document.export_product_barcode.submit();
						}
						else
						{
							alert("采购单不存在或无任何商品，无法导出");
						}
					}
				});
	}
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
<link href="../../comm.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="export_product_barcode"></form>
<form action="upload_product_barcode_show.html" name="upload_form" enctype="multipart/form-data" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td colspan="2" align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
						上传需打印条码商品
						</td>
					  </tr>
					  <tr>
					  	<td colspan="2">&nbsp;</td>
					  </tr>
					 <tr style=" padding-bottom:15px; padding-left:15px;">
							<td width="9%" align="left" valign="middle" nowrap class="STYLE3" >&nbsp;</td>
							<td width="91%" align="left" valign="middle" >&nbsp;</td>
					 </tr>
					 <tr>
					  	<td colspan="2">&nbsp;</td>
					  </tr>
					 <tr  style=" padding-bottom:15px; padding-left:15px;">
						<td width="9%" align="left" valign="top" nowrap class="STYLE3" style="padding-right: 10px;">上传EXCEL</td>
						<td width="91"align="left" valign="top" >
							<input type="file" name="file" id="file">
						</td>
					 </tr>
					 <tr>
					  	<td colspan="2">&nbsp;</td>
					 </tr>
					<tr style=" padding-bottom:15px; padding-left:15px;">
						<td width="9%" align="left" valign="top" nowrap class="STYLE3" style="padding-right: 10px;">采购单号</td>
						<td width="91"align="left" valign="top" nowrap>
					  		<input type="text" id="purchase_id" name="purchase_id"/>&nbsp;&nbsp;
					  		<input type="button" class="long-button-export" onClick="exportProductBarcodeByPurchase()" value="生成条码"/></td>
					</tr>
				  </table>
				</td></tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="uploadProduct()">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"></td>
              </tr>
            </table>
            <input type="hidden" name="lable_template_id" value="<%=lable_template_id%>"/>
</form>
</body>
</html>
