<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 
<%
	long user_adid = StringUtil.getLong(request, "user_adid");
	String userName = StringUtil.getString(request, "userName");
	int p = StringUtil.getInt(request,"p");
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(p);
	pc.setPageSize(10);
	DBRow[] rows = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(false, user_adid, "", pc, request);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>title列表</title>
<style type="text/css" media="all">
	@import "../js/thickbox/global.css";
	@import "../js/thickbox/thickbox.css";
</style>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<!-- jquery UI -->
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<!-- Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<!-- 排序按钮 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<style type="text/css">
	.set {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.title{font-size:12px;color:green;font-weight:blod;}
	a.common{width:16px;height:16px;display:block;float:left;border: 1px solid silver ; margin-left: 3px;}
	a.up{background-image: url('../imgs/arrow_up.png');}
	a.down{background-image: url('../imgs/arrow_down.png');}
	a.delete{width:13px;height:13px;display:block;float:left; border:1px solid silver;background-image: url('../imgs/del.gif'); margin-left: 3px;margin-top: 2px;}
</style>
<script type="text/javascript">

function addProprietary(){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_add.html?user_adid=<%=user_adid%>&userName=<%=userName%>";
	$.artDialog.open(url , {title: 'ADD TITLE',width:'400px',height:'180px', lock: true,opacity: 0.3,fixed: true});
}

function search(){
	$("#search_form").submit();
}

function refreshWindow(){
	window.location.reload();
}

function deleteById(title_admin_id,title_name){
	if(confirm("确定要删除：["+title_name+"]吗？")){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAdminDeleteAction.action',
			data:{title_admin_id:title_admin_id},
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag=="true")
				{
					showMessage("删除成功","success");
					refreshWindow();
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		});	
	}
}

function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}

function selectProprietary(){

	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_list_for_admin_select.html?user_adid=<%=user_adid%>&userName=<%=userName%>";
	$.artDialog.open(url , {title: '[&nbsp<%=userName%>&nbsp;]选择title',width:'700px',height:'450px', lock: true,opacity: 0.3,fixed: true,close:function(){refreshWindow();}});
}

function sortTitle(direction,id){
	
	//调整排序[取得需要调整的两项的{ID和排序值}]
	
	var changeNum = $("#id_priority_"+id).val();
	
	//交换
	$.ajax({
	
		url:'<%=ConfigBean.getStringValue("systenFolder")%>' + "action/administrator/proprietary/UserTitleManageSortTitleAction.action",
			data:"idPriority="+changeNum+"&direction="+direction,
			dataType:'json',
			type:'post',
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		    },
			success:function(data){
				$.unblockUI();
				if(data != null && data.flag){
					if(data.flag  === "success"){
						refreshWindow();
 	 				}else{
 	 					showMessage("系统错误,调整顺序失败.","error");
 	 				}
				}else{
					showMessage("系统错误,调整顺序失败.","error");
				}
			},
			error:function(){
				showMessage("系统错误,调整顺序失败.","error");
				$.unblockUI();
			}
	});
}
</script>
<style type="text/css">
	table.zebraTable tr{height: 30;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">

<br/>

<form action="" name="search_form" id="search_form" method="post"></form>

<table width="98%">
	<tr>
		<td>
			<!-- 选择 -->
			<input name="select" type="button" class="long-button" value="CHOICE TITLE" onclick="selectProprietary()"/>
			<!-- 添加 -->
			<input name="add" type="button" class="long-button" value="ADD TITLE" onclick="addProprietary()"/>
		</td>
	</tr>
</table>

<br/>
	
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
	<tr> 
		<!-- TITLE NAME -->
		<th width="50%" class="left-title" style="vertical-align: center;text-align: center;">
			TITLE NAME
		</th>
		<!-- 优先级 -->
		<th width="15%" class="left-title" style="vertical-align: center;text-align: center;">
			PRIORITY
		</th>
		<th width="15%" class="left-title" style="vertical-align: center;text-align: center;">
			ADJUST PRIORITY
		</th>
		<!-- 操作 -->
		<th width="10%" class="left-title" style="vertical-align: center;text-align: center;">
			OPERATE
       </th>
	</tr>
	<%if(rows.length > 0){
		for(int i = 0; i < rows.length; i ++){%>
	<tr>
		<td width="40%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
			<%=rows[i].getString("title_name") %>
		</td>
		<td width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
			<%=rows[i].get("title_admin_sort", 0) %>
		</td>
		<!-- 优先级 -->
		<td width="20%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
			
			<input type="hidden" name="id_priority_<%=i%>" id="id_priority_<%=i%>" value="<%=rows[i].get("title_admin_id",0L) %>_<%=rows[i].get("title_admin_sort", 0) %>" />
			&nbsp;
			<%if(pc.getPageNo() == 1 && i == 0){%>
				<a class="common down" title="向下调整顺序" href="javascript:void(0)"
					onclick="sortTitle('down','<%=i%>');" />
			<%}else if(pc.getPageCount() == pc.getPageNo() && i == (rows.length -1)){ %>
				<a class="common up" title="向上调整顺序" href="javascript:void(0)"
					onclick="sortTitle('up','<%=i%>');" />
			<%}else{ %>
				<a class="common up" title="向上调整顺序" href="javascript:void(0)"
					onclick="sortTitle('up','<%=i%>');" />
				<a class="common down" title="向下调整顺序" href="javascript:void(0)"
					onclick="sortTitle('down','<%=i%>');" />
			<%} %>
		</td>
		<td width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
			<input name="delete" type="button" class="short-short-button" value="DELETE" onclick="deleteById('<%=rows[i].get("title_admin_id",0L) %>','<%=rows[i].getString("title_name") %>')"/>
		</td>
	</tr>
		<%}
	}else{%>
	<tr>
		<td colspan="5" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">
			NO DATA
		</td>
	</tr>
	<%}%>
</table>
	
<br/>

<!-- 分页 -->
<form action="" id="pageForm" method="post">
	<input type="hidden" name="p" id="pageCount"/>
	<input type="hidden" name="cmd" value=""/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	<tr>
		<td height="28" align="right" valign="middle">
		<%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
		%>
			跳转到
      		<input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      		<input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" 
      			onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    	</td>
	</tr>
</table>
</body>
</html>
