<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 

<%
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
long pc_id = StringUtil.getLong(request, "pc_id");
DBRow[] rows = new DBRow[0];
DBRow[] selRows = new DBRow[0];

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品选择title列表</title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<script type="text/javascript">

function search()
{
	$("#search_form").submit();
}
function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}
function submitSelected(){
	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAdminSelAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag)
				{
					if(data.flag == "-1")
					{
						showMessage("保存失败，此title已存在","alert");
					}
					else if(data.flag == "-2")
					{
						showMessage("保存失败，此title已被此用户所有","alert");
					}
					else if(data.flag == "-3")
					{
						showMessage("保存失败，此级别的title已存在","alert");
					}
					else
					{
						showMessage("保存成功","success");
						windowClose();
					}
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})	
	}
};
$(function()
{
	$("input:checkbox[name=proprietaryCheck]").click(
		function(){
			if($(this).attr("checked"))
			{
				var ht = "<input type='hidden' id='proprietary_admin_"+$(this).val()+"' type='text' name='proprietary_admin'/>";
				$("#subForm").append($(ht));
			}
			else
			{
				$("#proprietary_admin_"+$(this).val()).remove();
				$("#priority_"+$(this).val()).val("");
			}
		}
	);
});
function volidate(){
	var checks = $("input:checkbox[name=proprietaryCheck]:checked");
	if(0 == checks.length)
	{
		alert("请选择title");
	}
	else
	{
		for(var i = 0; i < checks.length; i ++)
		{
			var val = checks[i].value;
			var sort_val = $('#priority_'+val).val();
			if(""==sort_val)
			{
				alert(val+"的优先级不能为空");
				return false;
			}
			else
			{
				var reg = /^[1-9]$/;
				if(!reg.test(sort_val))
				{r
					alert(val+"的优先级只能为数字");
					return false;
				}
			}
			$("#proprietary_admin_"+val).val(val+"_"+sort_val);
		}
	}
	
	return true;
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
</script>
<style type="text/css">
table.zebraTable tr{height: 30;}

</style>
</head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
	<br/>
	<form action="" name="search_form" id="search_form" method="post"> 
	</form>
		<table width="98%">
			<tr>
				<td>
					<input name="search" type="button" class="button_long_refresh" value="搜索" onclick="search()"/>
					<input name="search" type="button" class="button_long_refresh" value="确定" onclick="submitSelected()"/>
				</td>
			</tr>
		</table>
	
	<br/>
	<form action="" id="subForm">
		<input type="hidden" name="pc_id" value="<%=pc_id %>"/>
		<%
		for(int j = 0; j < selRows.length; j ++)
		{
		%>
		<input type='hidden' id='proprietary_admin_<%=selRows[j].getString("title_admin_title_id") %>' name="proprietary_admin"
			 value='<%=selRows[j].getString("title_admin_title_id")+"_"+selRows[j].getString("title_admin_sort") %>'/>
		<%	
		}
		%>
		
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
    		<th width="15%" class="left-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
	        <th width="20%" class="right-title" style="vertical-align: center;text-align: center;">titleId</th>
	        <th width="55%" class="left-title" style="vertical-align: center;text-align: center;">titleName</th>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">titleSort</th>
		</tr>
		<%
			for(int i = 0; i < rows.length; i ++)
			{
		%>
		<tr>
			<td width="15%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
   				<input type="checkbox" name="proprietaryCheck" <%=(0!=rows[i].get("title_admin_adid",0))?"checked":"" %> value='<%=rows[i].get("title_id",0L) %>'/>
   			</td>
   			<td width="30%" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
   				<%=rows[i].get("title_id",0L) %>
   			</td>
   			<td width="55%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
   				<%=rows[i].getString("title_name") %>
   			</td>
   			<td>
   				<input type="text" name='priority_<%=rows[i].get("title_id",0L) %>' style="width: 40px;" id='priority_<%=rows[i].get("title_id",0L) %>' value="<%=rows[i].getString("title_admin_sort") %>"/>
   			</td>
  		</tr>
		<%
			}
		%>
	</table>
	</form>
    <br/>
<form action="" id="pageForm" method="post">
		<input type="hidden" name="p" id="pageCount"/>
	
		<input type="hidden" name="cmd" value=""/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
          跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
</body>
</html>
