<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%>
<%
    long p = StringUtil.getLong(request,"p");
	long user_adid = StringUtil.getLong(request, "user_adid");
	String userName = StringUtil.getString(request, "userName");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>title添加</title>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<script type="text/javascript">

function submitData(){

	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag){
					if(data.flag == "-1"){
						showMessage("添加失败，此TITLE已存在","alert");
					}else{
						showMessage("添加成功","success");
						windowClose();
						$.artDialog.opener.go(<%=p%>);
					}
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		});
	}
};

function volidate(){
	if($("#title_name").val() == ""){
		showMessage("TITLE NAME不能为空","alert");
		return false;
	}
	return true;
};

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
</script>
</head>
<body>
	<fieldset style="border:2px #cccccc solid;padding:4px;-webkit-border-radius:5px;-moz-border-radius:5px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			ADD TITLE<%if(0!=user_adid){ %><br>建立[&nbsp;<%=userName%>&nbsp;]与添加TITLE的关联<%} %>
		</legend><br>
		<form action="" id="subForm">
			<input type="hidden" name="user_adid" id="user_adid" value='<%=user_adid %>'/>
			<table style="margin-top: 3px;margin-bottom: 3px;">
				<tr>
					<td>TITLE NAME</td>
					<td>
						<input name="title_name" id="title_name"  style="width: 250px;"/>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<br/><br/>
	<table align="right">
		<tr align="right">
			<td colspan="2"><input type="button" class="normal-green" value="提交" onclick="submitData();"/></td>
		</tr>
	</table>
</body>
</html>