<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 

<%
long user_adid = StringUtil.getLong(request, "user_adid");
long pc_id = StringUtil.getLong(request, "pc_id");
String p_name = StringUtil.getString(request, "p_name");
String cmd = StringUtil.getString(request, "cmd");
int p = StringUtil.getInt(request,"p");
int pc_line_p = 0;
int pc_catagory_p = 0;
int pc_admin_p = 0;
int pc_self_p = 0;
if("cmd_pc_line".equals(cmd))
{
	pc_line_p = p;
}
else if("cmd_pc_catagory".equals(cmd))
{
	pc_catagory_p = p;
}
else if("cmd_pc_admin".equals(cmd))
{
	pc_admin_p = p;
}
else if("cmd_pc_self".equals(cmd))
{
	pc_self_p = p;
}

DBRow productRow = productMgr.getAllDetailProduct(pc_id);
long pc_line_id = 0;
long pc_catagory_id = 0;
String pc_line_title = "";
String pc_category_title = "";
if(null != productRow)
{
	pc_line_id = productRow.get("product_line_id", 0L);
	pc_catagory_id = productRow.get("catalog_id", 0L);
	pc_line_title = productRow.getString("product_line_name");
	pc_category_title = productRow.getString("catalog_name");
}

//System.out.println("relate:"+pc_id+","+pc_line_id+","+pc_catagory_id+","+p);

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Manage Product Linked Title</title>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<script type="text/javascript">
/*
function go(index){
	var form = $("#pageForm");
	$("input[name='p']",form).val(index);
	form.submit();
}
*/
function windowCloseAndRefreshParent(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

	<div class="demo">
		<div id="tabs">
			<ul>
		<!-- 		<li><a href="proprietary_list_by_product_line_for_select.html?user_adid=<%=user_adid %>&pc_id=<%=pc_id %>&p=<%=pc_line_p %>&pc_line_id=<%=pc_line_id %>&pc_line_title=<%=pc_line_title %>">所属产品线的所有title</a></li>
	  			<li><a href="proprietary_list_by_product_catagory_for_select.html?user_adid=<%=user_adid %>&pc_id=<%=pc_id %>&p=<%=pc_catagory_p %>&pc_catagory_id=<%=pc_catagory_id %>&pc_category_title=<%=pc_category_title %>">所属分类的所有title</a></li>
				
				
				<li><a href="proprietary_list_by_admin_for_select.html?user_adid=<%=user_adid %>&pc_id=<%=pc_id %>&p=<%=pc_admin_p %>">当前用户的所有title</a></li>
				
			 -->
			 <li><a href="proprietary_list_by_product_selected.html?user_adid=<%=user_adid %>&pc_id=<%=pc_id %>&p=<%=pc_self_p %>&pc_line_id=<%=pc_line_id %>&pc_catagory_id=<%=pc_catagory_id %>&p_name=<%=p_name%>">Product Linked Title</a></li>
			</ul>
		</div>
	</div>
	


<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: { expires: 30000 } ,
	load: function(event, ui) {onLoadInitZebraTable();}	
});
</script>
</body>
</html>
