<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Title Linked Category</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="JavaScript1.2">

function addProductCatalog(parentid)
{
	$.prompt(
	
	"<div id='title'>增加子类</div><br />父类：<input name='proTextParentTitle' type='text' id='proTextParentTitle' style='width:200px;border:0px;background:#ffffff' disabled='disabled'><br>新建类别<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{
	      submit: promptCheckAddProductCatalog,
   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:parentid},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextParentTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
					
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addProductCatalog.action";
						document.add_form.parentid.value = parentid;
						document.add_form.title.value = f.proTextTitle;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function promptCheckAddProductCatalog(v,m,f)
{
	if (v=="y")
	{
		 if(f.proTextTitle == "")
		 {
				alert("请选填写新建分类名称");
				return false;
		 }
		 
		 return true;
	}

}



function modProductCatalog(id)
{
	$.prompt(
	
	"<div id='title'>修改分类</div><br />分类名称<br><input name='proTextTitle' type='text' id='proTextTitle' style='width:300px;'>",
	
	{

   		  loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductCatalogJSON.action",
							{parentid:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#proTextTitle").setSelectedValue(data.title);
							}
					);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductCatalog.action";
						document.mod_form.id.value = id;
						document.mod_form.title.value = f.proTextTitle;		
						document.mod_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}






function delC(id,title)
{
	if (confirm("您确认删除商品分类 "+title+" 吗？"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductCatalog.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}

function moveCatalog(pc_id)
{
	tb_show('移动商品分类','select_product_catalog.html?catalog_id='+pc_id+'&TB_iframe=true&height=500&width=800',false);
}

</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
<%--    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; </td>--%>
  </tr>
</table>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title" >
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
</form>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	    <input type="hidden" name="id">
	    <input type="hidden" name="parentid">
	    <input type="hidden" name="imp_color">
	       <tr> 
	          <th width="50%" colspan="2"  class="left-title">Category Name</th>
	          <th width="25%"  class="right-title" style="vertical-align: center;text-align: center;">Product Quantity</th>
	       </tr>
	    <tr > 
	      <td height="60" colspan="5">
	      		<script type="text/javascript">
					d = new dTree('d');
					d.add(0,-1,'Product Category</td><td align="center" valign="middle" width="70%">&nbsp;</td><td align="center" valign="middle" width="10%">&nbsp;</td></tr></table>');
					<%
						DBRow [] parentCategory = proprietaryMgrZyj.findProductCatagorysByTitleId(false, 0L, "", -1+"", "", title_id, 0, 0, null, request);
							//proprietaryMgrZyj.findProductCatagorysByTitleId(false, 0L, 0L, title_id, 0, 0, null, request);
							//catalogMgr.getAllProductCatalog();
							//productCatalogMgrZJ.getAllProductCatalogChild();
						for(int i=0;i<parentCategory.length;i++){	
					%>
						d.add(<%=parentCategory[i].getString("id")%>,<%=parentCategory[i].getString("parentid")%>,'<%=parentCategory[i].getString("title")+"&nbsp;&nbsp;HS CODE "+parentCategory[i].getString("hs_code")+":"+parentCategory[i].getString("tax")%></td><td align="center" valign="middle" width="30%"><%=productMgrZyj.getProductsByCategoryid(parentCategory[i].get("id",0l),null)%></td></tr></table>','');
					<%
						}
					%>
					document.write(d);
					
					function closeWin()
					{
						tb_remove();
					}
				</script>
	      </td>
	    </tr>
	</table>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/moveProductCatalog.action" name="move_catalog_form">
	<input type="hidden" name="move_to" id="move_to"/>
	<input type="hidden" name="catalog_id" id="catalog_id"/>	
</form>
<br>
</body>
</html>
<script>
	function addTitle(id,name){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_catalog_add_title.html?productCatalogId="+id+"&productCatalogName="+name; 
		 $.artDialog.open(uri , {title: '给产品分类添加TITLE',width:'600px',height:'470px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
