<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="org.directwebremoting.json.types.JsonArray"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>Manage Titles</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<!-- 条件搜索 -->
<script type="text/javascript" src="../js/custom_seach/custom_seach.js" ></script>

<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<%
AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session); 
long adminId=adminLoggerBean.getAdid();
Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
String admin_user = StringUtil.getString(request, "admin_user");
long admin_user_id = StringUtil.getLong(request, "admin_user_id");
DBRow[] rows = proprietaryMgrZyj.findProductLinesIdAndNameByTitleId(true, 0l, "","", 0, 0, null, request);
%>
<script>
function loadData(){
	var productLine = <%=new JsonObject(rows).toString()%> ;
	var data=[
			 {
				 key:'Product Line：',
				 type:'line',

				 son_key:'Parent Category：',
				 url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadFirstProductCatalogAction.action',

				 son_key2:'Sub Category：',
				 url2:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadSecondProductCatalogAction.action',	
						
				 son_key3:'Sub-Sub Category：',
			 	 url3:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/AjaxLoadThirdProductCatalogAction.action',
				 
				 array:productLine}
		     ];
	initializtion(data);  //初始化
}

function searchForm(){
	var admin_user_id=$('#admin_user_id').val();
    var para='admin_user_id='+admin_user_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function adminUserSelect(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:1, 					// 1表示的 单选
			 user_ids:$("#admin_user_id").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowSelect'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}


var line_id='';
var catalog_id='';
function custom_seach(){
    var array=getSeachValue();
    line_id='';
    catalog_id='';
    for(var i=0;i<array.length;i++){
        if(array[i].key=='line'){
        	line_id=array[i].val;
        }else if(array[i].key=='line_son'){
 		   catalog_id=array[i].val;
 	    }else if(array[i].key=='line_son_son'){
 		   catalog_id=array[i].val;
 	    }else if(array[i].key=='line_son_son_son'){
 		   catalog_id=array[i].val;
 	    }
    }

    var para='filter_productLine='+line_id+'&filter_pcid='+catalog_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		data:para,
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
}

function go(number){
	 var para='filter_productLine='+line_id+'&filter_pcid='+catalog_id+'&p='+number;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	
}
$(function(){
	addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryGetSearchJSONAction.action",
			"merge_field","title_id");
});
function search()
{
	var val = $("#search_key").val();
			
	if(val.trim()=="")
	{
		alert("Please input search key");
	}
	else
	{
		var val_search = "\'"+val.toLowerCase()+"\'";
		$("#search_key").val(val_search);
		var titleId = val_search;
		var search_mode = 1;
		var para = "titleId="+titleId+"&search_mode="+search_mode;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	

	}
}

function searchRightButton()
{
	var val = $("#search_key").val();
			
	if (val=="")
	{
		alert("你好像忘记填写关键词了？");
	}
	else
	{
		val = val.replace(/\'/g,'');
		$("#search_key").val(val);

		var titleId = val;
		var search_mode = 1;
		var para = "titleId="+titleId+"&search_mode="+search_mode;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
			type: 'post',
			dataType: 'html',
			data:para,
			async:false,
			success: function(html){
				$("#showList").html(html);
				onLoadInitZebraTable(); //重新调用斑马线样式
			}
		});	

	}
}

function eso_button_even()
{
	document.getElementById("eso_search").oncontextmenu=function(event) 
	{  
		if (document.all) window.event.returnValue = false;// for IE  
		else event.preventDefault();  
	};  
		
	document.getElementById("eso_search").onmouseup=function(oEvent) 
	{  
		if (!oEvent) oEvent=window.event;  
		if (oEvent.button==2) 
		{  
		   searchRightButton();
		}  
	}  
}
function refreshWindow(){
	go($("#jump_p2").val());
}
function submitData(){
	$("#title_name").val($("#titleName").val());
	if(volidate()){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryAddAction.action',
			data:$("#subForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag){
					if(data.flag == "-1"){
						showMessage("This Title already exists.","alert");
					}else{
						showMessage("Successfully added title","succeed");
						go();
					}
				}
				$("#titleName").val("");
				$("#title_name").val("");
			},
			error:function(){
				showMessage("System error","error");
			}
		});
	}
	else
	{
		return false;
	}
};

function volidate(){
	if($("#title_name").val() == ""){
		showMessage("Please input the Title Name","alert");
		return false;
	}
	return true;
};
function clickAddTitleTab()
{
	$("#titleName").focus();
}
</script>
</head>
<body onload="loadData()">   
	     
<div id="tabs" >
	<ul>
		<li><a href="#av1">Common Tools</a></li>
		<li><a href="#av2">Advanced Search</a></li>
		<li><a href="#av3" onclick="clickAddTitleTab()">Add Title</a></li>
	</ul>
	<div id="av1">
        <div id="easy_search_father">
			<div id="easy_search">
				<a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
			</div>
			<input name="search_key" id="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key"
				  onkeydown="if(event.keyCode==13)search()" value='' />
	</div>	  
	<div id="av2">
		<div id="av"></div>
		<input type="hidden" id="atomicBomb" value=""/>
		<input type="hidden" id="title" />
	</div>
	<div id="av3">
		<table style="margin-top: 3px;margin-bottom: 3px;">
			<tr>
				<td>TITLE NAME:</td>
				<td>
					<input name="titleName" id="titleName"  style="width: 250px;" onkeydown="if(event.keyCode==13)submitData()"/>
					<!--  onkeydown="if(event.keyCode==13)submitData()" -->
				</td>
				<td>
					<input name="add" type="button" class="theme-button-add" value="Add Title" onclick="submitData()"/>
				</td>
			</tr>
		</table>
		
	</div>
</div>
<form action="" id="subForm" method="post">
	<input name="title_name" id="title_name" style="width: 250px;" type="hidden"/>
</form>
<script type="text/javascript">
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
<div id="showList">1213231</div>
	  
</body>
</html>
<script>
(function(){	
	$("#tabs").tabs("select",0);
	
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },		
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/proprietary_list_show.html',
		type: 'post',
		dataType: 'html',
		async:false,
		success: function(html){
			$("#showList").html(html);
			onLoadInitZebraTable(); //重新调用斑马线样式
		}
	});	
	 
})();
</script>
