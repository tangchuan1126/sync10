<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@ include file="../../include.jsp"%> 

<%
String titleId = StringUtil.getString(request, "titleId");
int search_mode = StringUtil.getInt(request, "search_mode");
String filter_productLine = StringUtil.getString(request,"filter_productLine");
String search_key = StringUtil.getString(request,"search_key");
long admin_user_id = StringUtil.getLong(request, "admin_user_id");
String admin_user = StringUtil.getString(request, "admin_user");
String filter_categoryId = StringUtil.getString(request, "filter_pcid");



AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session); 
long adminId=adminLoggerBean.getAdid();
Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录


int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);
int begin = 0;
int length = 3;
DBRow[] rows = new DBRow[0];
if(!"".equals(titleId))
{
	rows = proprietaryMgrZyj.searchTitleByNumber(titleId, search_mode, pc, begin, length, request);
}
else
{
	rows = proprietaryMgrZyj.findProprietaryAllRelatesByAdminProductNameLineCatagory(true, admin_user_id,filter_productLine,filter_categoryId,filter_categoryId,search_key, begin, length, pc, request);
}
%>

<html>
<head>
<title>Manage Title</title>


<script type="text/javascript">
function cleanSearchKey(divId){
	$("#"+divId+"search_product_name").val("");
}

function addProprietary(){
	var p=$('#pageCount').val();
	var url='';
	if(<%=bl%>){
		url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_add.html?user_adid="+<%=adminId%>+"&p="+p;
	}else{
		url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_add.html?p="+p;
	}
	$.artDialog.open(url , {title: '添加title',width:'400px',height:'180px', lock: true,opacity: 0.3,fixed: true});
}

function adminUserSelect(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:1, 					// 1表示的 单选
			 user_ids:$("#admin_user_id").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowSelect'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}


function editById(title_id, title_name){
	var p=$('#pageCount').val();
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_update.html?title_id="+title_id+"&p="+p;
	$.artDialog.open(url , {title: 'Update Title:&nbsp;[&nbsp;'+title_name+"&nbsp;]",width:'400px',height:'150px', lock: true,opacity: 0.3,fixed: true});
}
function deleteByIdConfirm(title_id, title_name)
{
	var d = $.artDialog({
		lock: true,opacity: 0.3,fixed: true,
	    title: 'delete '+title_name + ' ?',
	    content: 'Are you sure to delete [ '+title_name+'] ?',
	    button:[
				{
					name: 'Sure',
					callback: function () {
						deleteById(title_id, title_name,d);
						return false ;
					},
					focus:true
					
				},
				{
					name: 'Cancel'
				}]
	});
	d.show();
}
function deleteById(title_id, title_name,d)
{
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/proprietary/ProprietaryDeleteAction.action',
			data:{title_id:title_id},
			dataType:'json',
			type:'post',
			success:function(data){
				d.close();
				if(data && data.flag=='1')
				{
					showMessage("Title deleted successfully","succeed");
				}
				else if(data && data.flag=='2')
				{
					showMessage("Cannot delete title, It is linked to others.","alert");
				}
		       go(<%=p%>);
			},
			error:function(){
				d.close();
				showMessage("System error","error");
			}
		})	
}

function setParentUserShowSelect(user_ids , user_names){
	if("" == user_names)
	{
		$("#admin_user_id").val(0);
		$("#admin_user").val("*请选择用户");
		$("#admin_user").attr("class","searchbarGray");
	}
	else
	{
		 $("#admin_user").removeAttr("class");
		 $("#admin_user_id").val(user_ids);
		 $("#admin_user").val(user_names);
	}
	
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);};
function moreAdmins(title_id, title_name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_ct_admin_view.html?title_id="+title_id;
	 $.artDialog.open(uri , {title: 'Title[&nbsp;'+title_name+'&nbsp;] Linked Users',width:'1000px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function morePcLines(title_id, title_name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_product_line_view.html?title_id="+title_id;
	 $.artDialog.open(uri , {title: 'Title[&nbsp;'+title_id+'&nbsp;] Linked Product Line',width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function morePcCatas(title_id, title_name)
{
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_ct_product_catalog_view.html?title_id="+title_id;
	 $.artDialog.open(uri , {title: 'Title[&nbsp;'+title_name+'&nbsp;] Linked Product Category',width:'800px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function moreProducts(title_id, title_name)
{
	var cmd = '';
	var pcid = '';
	var key = '';
	var pro_line_id = '';
	var admin_user_id = '<%=admin_user_id%>';
	if('' == '<%=search_key%>')
	{
		cmd = 'filter';
		key = '<%=search_key%>';
		pcid = '<%=filter_categoryId%>';
		pro_line_id = '<%=filter_productLine%>';
	}
	else
	{
		cmd = 'search';
		key = '';
		pcid = '';
		pro_line_id = '';
	}
	var para = 'title_id='+title_id+'&cmd='+cmd+'&pcid='+pcid+'&key='+key+'&pro_line_id='+pro_line_id+'&admin_user_id='+admin_user_id;
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/proprietary/proprietary_products_view.html?"+para;
	 $.artDialog.open(uri , {title: 'Title[&nbsp;'+title_name+'&nbsp;] Linked Products',width:'1000px',height:'600px', lock: true,opacity: 0.3,fixed: true});
}
function cleanProductName()
{
	$("#search_key").val("");
}
</script>

<style type="text/css">
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:413px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
table.zebraTable tr{height: 30;}
.set{
padding:2px;
width:93%;
word-break:break-all;
margin-top:10px;
margin-top:5px;
line-height:18px;
-webkit-border-radius:5px;
-moz-border-radius:5px;
 margin-bottom: 10px;
 border: 2px solid silver;}
ul.myulAdmin, ul.myulPcLine, ul.myulPcCata{list-style-type:none;padding-left: 3px;}
ul.myulAdmin li{float:left;width:90%;line-height:25px;height:25px;text-align: left;}
ul.myulPcLine li{float:left;width:100%;line-height:25px;height:25px;text-align: left;}
ul.myulPcCata li{float:left;width:100%;line-height:25px;height:25px;text-align: left;}
.searchbarGray{
       color:#c4c4c4;font-family: Arial;
 }
</style>
</head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<br/>
	<br/>
	<div style="height:30px">
<%--		<input name="add" type="button" class="long-button" value="添加" onclick="addProprietary()"/>--%>
		<input type="button" class="long-button" value="Import Titles" onclick="upload('jquery_file_up')" />
		<input type="hidden" id="file_names" name="file_names" value=""/>
		<input type="button" class="long-button" value="Export Titles" onclick="down()" />
	</div>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    	<tr> 
<%--	        <th width="5%" class="right-title" style="vertical-align: center;text-align: center;">TITLEID</th>--%>
	        <th width="15%" class="left-title" style="vertical-align: center;text-align: center;">TITLE NAME</th>
	        <th width="15%" class="left-title" style="vertical-align:center;text-align:center;">Linked Users</th>
	        <th width="19%" class="left-title" style="vertical-align:center;text-align:center;">Linked Product Line</th>
	        <th width="19%" class="left-title" style="vertical-align:center;text-align:center;">Linked Product Categories</th>
	        <th width="19%" class="left-title" style="vertical-align:center;text-align:center;">Linked Products</th>
	        <th  class="left-title" style="vertical-align: center;text-align: center;">Operation</th>
		</tr>
		<%if(rows.length > 0){%>
			<%for(int i = 0; i < rows.length; i ++){%>
			<tr>
<%--	   			<td align="center" valign="middle" style='word-break:break-all;font-weight:bold'>--%>
<%--	   				<%=rows[i].get("title_id",0L) %>--%>
<%--	   			</td>--%>
	   			<td align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
	   				<%=rows[i].getString("title_name") %>
	   			</td>
	   			<td>
	   				<!-- 与title关联的用户 -->
	   				<%	DBRow[] adminRows = (DBRow[]) rows[i].get("admins", new DBRow[0]);%>
	   				<%	if(null != adminRows && adminRows.length != 0){%>
		   				<fieldset class="set" style="border-color: #993300;">
		   				<legend>		   	
			   				<a style="color: #4A8EBC;" href="javascript:void(0)" onclick="moreAdmins(<%=rows[i].get("title_id",0L) %>,'<%=rows[i].getString("title_name") %>')">Details&nbsp;|&nbsp;More</a>
		   				</legend>
		   				<ul class="myulAdmin">
		   				<%	for(int j = 0; j < adminRows.length; j ++){	%>
		   					<li style="font-size:13px;font-weight: bold;float:left;width:140px;"><%=adminRows[j].getString("employe_name") %></li>
							<li style="color:red;float:left;width:30px;"> -- <%=adminRows[j].get("title_admin_sort",0) %></li>
		   				<%}%>
		   				</ul>
		   				</fieldset>
		   			<%}%>
	   				&nbsp;
	   			</td>
	   			<td>
	   				<!-- 与title关联的产品线 -->   				
	   				<%DBRow[] pcLineRows = (DBRow[])rows[i].get("pcLines", new DBRow[0]);%>
	   				<%if(null != pcLineRows && pcLineRows.length != 0){ %>
	   					<fieldset class="set" style="border-color: #008000;">
		   				<legend>
			   				<a style="color:#4A8EBC;" href="javascript:void(0)" onclick="morePcLines(<%=rows[i].get("title_id",0L) %>,'<%=rows[i].getString("title_name") %>')">Details&nbsp;|&nbsp;More</a>
		   				</legend>
		   				<ul class="myulPcLine">
		   			<%for(int j = 0; j < pcLineRows.length; j ++){%>
		   					<li><%=pcLineRows[j].getString("name") %></li>
		   			<%}%>	
		   				</ul>
		   				</fieldset>
	   				<%}%>
	   				&nbsp;
	   			</td>
	   			<td>
	   			   <!-- 与title关联的产品分类 -->
	   				<%DBRow[] pcCataRows = (DBRow[]) rows[i].get("pcCatagorys", new DBRow[0]);	%>
	   				<%if(null != pcCataRows && pcCataRows.length != 0){%>
	   					<fieldset class="set" style="border-color: #0000FF;">
		   				<legend>
		   					<a style="color: #4A8EBC;" href="javascript:void(0)" onclick="morePcCatas(<%=rows[i].get("title_id",0L) %>,'<%=rows[i].getString("title_name") %>')">Details&nbsp;|&nbsp;More</a>
		   				</legend>
		   				<ul class="myulPcCata">
		   			<%for(int j = 0; j < pcCataRows.length; j ++){%>
		   					<li><%=pcCataRows[j].getString("title") %></li>
		   			<%}%>	
		   				</ul>
		   				</fieldset>
	   				<%}%>
	   				&nbsp;
	   			</td>
	   			<td>
	   				<%DBRow[] productRows = (DBRow[])rows[i].get("products", new DBRow[0]);%>
	   					<%if(null != productRows && productRows.length > 0){%>
	   					<fieldset class="set" style="border-color:#f60;">
		   				<legend>
							<a style="color: #4A8EBC;" href="javascript:void(0)" onclick="moreProducts(<%=rows[i].get("title_id",0L) %>,'<%=rows[i].getString("title_name") %>')">Details&nbsp;|&nbsp;More</a>
		   				</legend>
		   				<ul class="myulPcCata">
		   			<%for(int j = 0; j < productRows.length; j ++){%>
		   					<li><%=productRows[j].getString("p_name") %></li>
		   			<%}%>	
		   				</ul>
		   				</fieldset>
	   				<%}%>
	   				&nbsp;
	   			</td>
	   			<td  align="center" valign="middle" style='word-break:break-all;font-weight:bold;padding-bottom: 8px;padding-top: 8px;'>
	   				<input name="delete" type="button" class="theme-button-del" value="Del" onclick="deleteByIdConfirm(<%=rows[i].get("title_id",0L) %>, '<%=rows[i].getString("title_name") %>')"/>
	   				<input name="edit" type="button" class="theme-button-edit" value="Rename" onclick="editById(<%=rows[i].get("title_id",0L) %>, '<%=rows[i].getString("title_name") %>')"/>

	   				
	   			</td>
  			</tr>
			<%}%>
		<%}else{%>
		<tr>
			<td colspan="7" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">No Data</td>
		</tr>
		<%}%>
	</table>
    <br/>
<form action="" id="pageForm" method="post">
		<input type="hidden" name="p" id="pageCount" value="<%=p %>"/>
		<input type="hidden" name="cmd" value=""/>
		<input type="hidden" name="admin_user" value='<%=admin_user %>'/>
		<input type="hidden" name="admin_user_id" id="admin_user_id" value="<%=admin_user_id %>"/>
		<input type="hidden" name="filter_productLine" id="filter_productLine" value="<%=filter_productLine %>"/>
		<input type="hidden" name="search_key" value='<%=search_key %>'/>
		<input type="hidden" name="filter_categoryId" id="filter_categoryId" value="<%=filter_categoryId %>"/>
</form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle">
	<%
		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;
		out.println("Page：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;Total：" + pc.getAllCount() + " &nbsp;&nbsp;");
		out.println(HtmlUtil.aStyleLink("gop","First","javascript:go(1)",null,pc.isFirst()));
		out.println(HtmlUtil.aStyleLink("gop","Previous","javascript:go(" + pre + ")",null,pc.isFornt()));
		out.println(HtmlUtil.aStyleLink("gop","Next","javascript:go(" + next + ")",null,pc.isNext()));
		out.println(HtmlUtil.aStyleLink("gop","Last","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
          Goto
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<script type="text/javascript">
function ajaxLoadCatalogMenuPage(pcLineId, pcCategoryId,divId)//pcLineId, pcCategoryId,divId
{
		var para = "id="+pcLineId+"&catalog_id="+pcCategoryId+"&admin_user_id=<%=admin_user_id%>";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
			},
			
			error: function(){
			},
			
			success: function(html)
			{
				$("#categorymenu_td").html(html);
			}
		});
}
</script>
<form action="" name="download_form" id="download_form">
</form>

</body>
</html>
<script>
//上传
function upload(_target){
	    var fileNames = $("input[name='file_names']").val();
	    var obj  = {
		     reg:"xls",
		     limitSize:2,
		     target:_target,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: 'Upload File',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,});
}

//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/proprietary/dialog_proprietary_upload.html?fileName='+fileNames; 
	  $.artDialog.open(uri , {title: '上传TITLE',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
}

//导出
function down(){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var productLine=$('#filter_productLine').val();
	var productCatalog=$('#filter_categoryId').val();
	var admin_user_id=$('#admin_user_id').val();
	var search_key=$('#search_key').val();
	var para='productLine='+productLine+'&productCatalog='+productCatalog+'&admin_user_id='+admin_user_id+'&search_key='+search_key;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/title/AjaxDownTitleListAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}
				
		}
	});
}
</script>
