<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.lucene.zr.PurchaseIndexMgr"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%>
<jsp:useBean id="deliveryOrderKey" class="com.cwc.app.key.DeliveryOrderKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
<jsp:useBean id="qualityInspectionKey" class="com.cwc.app.key.QualityInspectionKey"/>
<jsp:useBean id="purchaseProductModelKey" class="com.cwc.app.key.PurchaseProductModelKey"></jsp:useBean>
<%@page import="java.text.DecimalFormat"%>
<%@ page import="java.util.HashMap"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 
<%

	String cmd = StringUtil.getString(request,"cmd");
	
	long supplier_id = adminMgr.getAdminLoginBean(session).getAttach().get("id",0l);
	long ps_id = StringUtil.getLong(request,"ps_id",0l);
	
	int purchase_status = StringUtil.getInt(request,"purchase_status",0);
	int money_status = StringUtil.getInt(request,"money_status",-1);
	int arrival_time = StringUtil.getInt(request,"arrival_time",0);
	
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
	String search_key = StringUtil.getString(request,"search_key");
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	
	DBRow[] rows;
	
	if(cmd.equals("fillter"))
	{
		rows = purchaseMgr.getPurchaseSupplierFillter(supplier_id,ps_id,purchase_status,money_status,arrival_time,st,en,pc);
	}
	else if(cmd.equals("search"))
	{
		rows = purchaseMgr.supplierSearchPurchaseBySupplierOrId(search_key,pc,supplier_id);
	}
	else
	{
		rows = purchaseMgr.getPurchaseSupplierFillter(supplier_id,ps_id,purchase_status,money_status,arrival_time,st,en,pc);
	}
	
	
	
	PurchaseKey purchasekey = new PurchaseKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"转账记录");
	followuptype.put(3,"修改采购单详细记录");
	followuptype.put(4,"发票记录");
	followuptype.put(5,"退税记录");
	followuptype.put(8,"采购单制签记录");
	followuptype.put(12,"商品标签记录");
	followuptype.put(13,"采购单质检要求");
	followuptype.put(23,"商品范例记录");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.title{font-size:12px;color:green;font-weight:blod;}
-->
</style>
<script language="javascript" src="../../../common.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>


<link href="../../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	 
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
 
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body >

	
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
    <tr> 
        <th width="30%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">采购信息</th>
        <th width="20%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">供应商</th>
        <th width="10%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">体积</th>
        <th width="30%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">各流程状态</th>
  </tr>
  <%
  if(rows != null && rows.length > 0 ){
  	for(int i=0;i<rows.length;i++)
  	{
  %>
  	<tr align="center" valign="middle">
	  	<td height="80" nowrap="nowrap">
	  	<fieldset class="set" style="border-color:#993300;">
	  		<legend>
	  			<span class="title"><a href="javascript:void(0)" onclick="window.location.href='supplier_purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>'">P<%=rows[i].getString("purchase_id")%></a></span>
	  			&nbsp;<span style="font-weight:bold;"><%DBRow storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l));out.print(storage.getString("title"));%></span>
	  		</legend>
	  		<p style="text-align:left;clear:both;"><span class="stateName">采购:</span> <span class="stateValue"><%=rows[i].getString("proposer") %></span></p>
	  		<p style="text-align:left;clear:both;"><span class="stateName">采购时间:</span><span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd",new SimpleDateFormat("yyyy-MM-dd").parse(rows[i].getString("purchase_date")))%></span></p>
	  		<p style="text-align:left;clear:both;"><span class="stateName">更新日期:</span><span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("updatetime")))%></span></p>
	 		<p style="text-align:left;clear:both;"><span class="stateName">流程状态:</span>
	 		<span class="stateValue">
	 			<%=purchasekey.getQuoteStatusById(rows[i].getString("purchase_status"))%>&nbsp; 
	 		</span>
	 		</p>
	  	</fieldset>	
	  	</td>
	  	<td nowrap="nowrap"><div align="left">
	  	<%
			try
			{
				DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(rows[i].getString("supplier")));
				if(supplier!=null)
				{
					out.println(supplier.getString("sup_name"));
				}
				
			}
			catch(NumberFormatException e)
			{
				out.println(rows[i].getString("supplier"));
			}
				  			
		%>
	  	</div></td>
	  	<td nowrap="nowrap">
	  		<%=purchaseMgr.getPurchaseVolume(rows[i].get("purchase_id",0l)) %>
	  	</td>
	  	<td nowrap="nowrap" align="left">
	  	<table cellpadding="0" cellspacing="0" width="100%">
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				交货:<%=purchasearrivekey.getQuoteStatusById(rows[i].getString("arrival_time"))%>
	  				<%
	  					//采购员Ids, names
				  		DBRow[] purchaserPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
				  		String purchserPersonNames	= "";
				  		if(purchaserPersons.length > 0)
				  		{
				  			purchserPersonNames	+= purchaserPersons[0].getString("employe_name");
				  			for(int h = 1; h < purchaserPersons.length; h ++)
				  			{
				  				DBRow purchaserPerson = purchaserPersons[h];
				  				purchserPersonNames	 += ","+purchaserPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(purchserPersonNames)){
				  			out.println("<br/>"+purchserPersonNames);
				  		}
				  		//调度员Ids, names
				  		DBRow[] dispatcherPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER);
				  		String dispatcherPersonNames= "";
				  		if(dispatcherPersons.length > 0){
				  			dispatcherPersonNames	+= dispatcherPersons[0].getString("employe_name");
				  			for(int j = 1; j < dispatcherPersons.length; j ++){
				  				DBRow dispatcherPerson = dispatcherPersons[j];
				  				dispatcherPersonNames+= ","+dispatcherPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(dispatcherPersonNames))
				  		{
				  			out.println("<br/>调度:"+dispatcherPersonNames);
				  		}
				  		else
				  		{
				  			out.println("<br/>调度:");
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%=rows[i].get("apply_money_over",0d)==0?df.format(tDate.getDiffDate(rows[i].getString("purchase_date"),"dd"))+"天":rows[i].get("apply_money_over",0d)+"天完成<br/>" %>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					out.println("开票:"+invoiceKey.getInvoiceById(rows[i].getString("invoice")));
				  		//发票员Ids, names
				  		DBRow[] invoicePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
				  		String invoicePersonNames= "";
				  		if(invoicePersons.length > 0){
				  			invoicePersonNames	+= invoicePersons[0].getString("employe_name");
				  			for(int k = 1; k < invoicePersons.length; k ++){
				  				DBRow invoicePerson = invoicePersons[k];
				  				invoicePersonNames+= ","+invoicePerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(invoicePersonNames)){
				  			out.println("<br/>"+invoicePersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(InvoiceKey.FINISH == rows[i].get("invoice",0))
			  			{
			  				out.println("开票:"+rows[i].get("invoice_over_date",0.0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("invoice",0)==InvoiceKey.INVOICE||rows[i].get("invoice",0)==InvoiceKey.INVOICING)
			  			{
			  				double invoice_time = purchaseMgrZyj.getPurchaseDiffTime(rows[i].get("purchase_id",0l),ProcessKey.INVOICE,InvoiceKey.INVOICING,"dd");
			  				if(invoice_time==0)
			  				{
			  					out.println("开票:未开始<br/>");
			  				}
			  				else
			  				{
			  					out.println("开票:"+invoice_time+"天<br/>");
			  				}
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<% 
	  					out.println("退税:"+drawbackKey.getDrawbackById(rows[i].getString("drawback")));
				  		//退税Ids, names
				  		DBRow[] drawbackPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
				  		String drawbackPersonNames= "";
				  		if(drawbackPersons.length > 0){
				  			drawbackPersonNames	+= drawbackPersons[0].getString("employe_name");
				  			for(int m = 1; m < drawbackPersons.length; m ++){
				  				DBRow drawbackPerson = drawbackPersons[m];
				  				drawbackPersonNames+= ","+drawbackPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(drawbackPersonNames)){
				  			out.println("<br/>"+drawbackPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(DrawbackKey.FINISH == rows[i].get("drawback",0))
			  			{
			  				out.println("退税:"+rows[i].get("drawback_over_date",0.0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("drawback",0)==DrawbackKey.DRAWBACK||rows[i].get("drawback",0)==DrawbackKey.DRAWBACKING)
			  			{
			  				double drawback_time = purchaseMgrZyj.getPurchaseDiffTime(rows[i].get("purchase_id",0l),ProcessKey.DRAWBACK,DrawbackKey.DRAWBACKING,"dd");
			  				if(drawback_time==0)
			  				{
			  					out.println("退税:未开始<br/>");
			  				}
			  				else
			  				{
			  					out.println("退税:"+drawback_time+"天<br/>");
			  				}
			  				
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					if(1 == rows[i].get("need_tag",0))
				  		{
				  		
				  			out.println("制签:不需要");
				  		}
				  		else if(2 == rows[i].get("need_tag",0))
				  		{
				  			out.println("制签:制签中");
				  		}
				  		else if(0 == rows[i].get("need_tag",0))
				  		{
				  			out.println("制签:");
				  		}
				  		else if(3 == rows[i].get("need_tag",0))
				  		{
				  			out.println("制签:"+transportTagKey.getTransportTagById(rows[i].getString("need_tag")));
				  		}
				  		//制签Ids, names
				  		DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				  		String tagPersonNames= "";
				  		if(tagPersons.length > 0){
				  			tagPersonNames	+= tagPersons[0].getString("employe_name");
				  			for(int n = 1; n < tagPersons.length; n ++){
				  				DBRow tagPerson = tagPersons[n];
				  				tagPersonNames  += ","+tagPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(tagPersonNames)){
				  			out.println("<br/>"+tagPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(TransportTagKey.FINISH == rows[i].get("need_tag",0))
			  			{
			  				out.println("制签:"+rows[i].get("need_tag_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("need_tag",0) == TransportTagKey.TAG)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
					
							double tag_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println("制签:"+MoneyUtil.round(tag_time,2)+"天<br/>");
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;border-collapse:collapse;">
	  				<%
	  					out.println("质检:"+qualityInspectionKey.getQualityInspectionById(rows[i].getString("quality_inspection")));
				  		//质检Ids, names
				  		DBRow[] qualityPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
				  		String qualityPersonNames= "";
				  		if(qualityPersons.length > 0)
				  		{
				  			qualityPersonNames	+= qualityPersons[0].getString("employe_name");
				  			for(int o = 1; o < qualityPersons.length; o ++)
				  			{
				  				DBRow qualityPerson = qualityPersons[o];
				  				qualityPersonNames  += ","+qualityPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(qualityPersonNames))
				  		{
				  			out.println("<br/>"+qualityPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					out.println("商品范例:"+purchaseProductModelKey.getProductModelKeyName(rows[i].getString("product_model")));
				  		//商品范例Ids, names
				  		DBRow[] productModelPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
				  		String productModelPersonNames= "";
				  		if(productModelPersons.length > 0){
				  			productModelPersonNames	+= productModelPersons[0].getString("employe_name");
				  			for(int o = 1; o < productModelPersons.length; o ++){
				  				DBRow productModelPerson = productModelPersons[o];
				  				productModelPersonNames  += ","+productModelPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(productModelPersonNames)){
				  			out.println("<br/>"+productModelPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(PurchaseProductModelKey.FINISH == rows[i].get("product_model",0))
			  			{
			  				out.println("范例:"+rows[i].get("product_model_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("product_model",0) == PurchaseProductModelKey.NEED_PRODUCT_MODEL)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
					
							double tag_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println("范例:"+MoneyUtil.round(tag_time,2)+"天<br/>");
			  			}
	  				%>
	  			</td>
	  		</tr>
	  	</table>
	  	</td>
  </tr>
  <tr class="split">
  		<td colspan="3" style="text-align:left;padding-left:20px;">
	  <%
  	  	if(rows[i].get("purchase_status",0)!=PurchaseKey.CANCEL&&rows[i].get("arrival_time",0)!=PurchaseArriveKey.WAITAPPROVE&&rows[i].get("purchase_status",0)!=PurchaseKey.FINISH)
	  	{
  	  %>
  	  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.cancelPurchase">
		  	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="short-short-button-cancel" value="取消" onclick="cancelPurchase(<%=rows[i].get("purchase_id",0)%>)"/>
  	  </tst:authentication>
  	  <% 
	  	}
	  %>
	  	</td>
  		<td>
  			<%
  			if(rows[i].get("purchase_status",0)!=PurchaseKey.CANCEL){
  				%>
  				<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.followupPurchase">
  		  	  		<input type="button" class="short-short-button-mod" onclick='followup(<%=rows[i].getString("purchase_id")%>,<%=rows[i].get("invoice",0) %>,<%=rows[i].get("drawback",0) %>)' value="跟进"/>
  				</tst:authentication>
  				<% 
  	  }
  	  %>
  	  <input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,4)" value="发票"/>
	  <input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,5)" value="退税"/>
  	  </td>
  </tr>
  <%
  	}
  }else{
	  %>
	  	 <tr>
	 		 <td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
  </tr>
  <%
  	}
   %>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go2page(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go2page(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go2page(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go2page(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2page(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<script type="text/javascript">
	function applicationApprove(purchase_id)
	  {
	  	if(confirm("确定申请此采购单完成"))
	  	{
	  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
	  		document.application_approve.purchase_id.value = purchase_id;
	  		document.application_approve.submit();
	  	}
	  }
</script>
<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id"/>
</form>
</body>
</html>



