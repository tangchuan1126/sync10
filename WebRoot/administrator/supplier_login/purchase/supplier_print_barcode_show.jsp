<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
float print_range_width = Float.parseFloat(StringUtil.getString(request,"print_range_width"));
float print_range_height = Float.parseFloat(StringUtil.getString(request,"print_range_height"));

long purchase_id = StringUtil.getLong(request,"purchase_id");

DBRow rows[] = productMgrZJ.getProductsByPurchaseId(purchase_id,null);



DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

boolean productnull = true;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../../js/popmenu/common.js"></script>
		<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../js/select.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>

<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src='../../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.autocomplete.css" />

<script language="javascript">

function searchByName()
{
	document.search_form.cmd.value = "name";
	document.search_form.name.value = $("#filter_name").val();	
	document.search_form.pcid.value = $("#filter_pcid").val();	
	document.search_form.submit();
}

function importPrint()
{
	tb_show('上传要打印条码的商品','printbarcode_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}

function exportProductBarcode()
{
	var catalog_id= $("#filter_pcid").val();
	var para = "catalog_id="+catalog_id;
	$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/exportProductBarcode.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date){
					if(date["canexport"]=="true")
					{
						document.export_product_barcode.action=date["fileurl"];
						document.export_product_barcode.submit();
					}
					else
					{
						alert("该分类与子类下无所选形式商品，无法导出");
					}
				}
			});
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}

function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(e.keyCode==8||e.keyCode==46||e.keyCode==37||e.keyCode==38||e.keyCode==39||e.keyCode==40)
		{
			return true;
		}
		else if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar)
	}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div align="center">
<br>
<form name="export_product_barcode" method="post"></form>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 采购单商品条码</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">


    <tr> 
        <th width="150" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
        <th width="157" align="left" class="right-title"  style="vertical-align: center;text-align: left;">商品条码</th>
        <th width="84" class="right-title"  style="vertical-align: center;text-align: center;">打印数量</th>
    </tr>

    <%
	for ( int i=0; i<rows.length; i++ )
	{
		if(rows[i].getString("error").equals("error"))
		{
			productnull = false;
		}
	%>
    <tr> 
      <td height="60" valign="middle"   style='font-size:14px;' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
        
      <%=rows[i].getString("p_name")%></td>
      <td height="60" align="left" valign="middle"   style='line-height:20px;' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
	  	<%=rows[i].getString("p_code") %>
	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;color:#0000FF;font-weight:bold' <%=rows[i].getString("error").equals("error")?"bgcolor='red'":"" %>>
	  
	  <input id="copies_<%=rows[i].getString("pc_id")%>" type="text" value="<%=(int)rows[i].get("count",0f)%>" style="width:50px;color:#FF0000" onKeyPress="return noNumbers(event)"></td>
      </tr>
    <%
	}
	%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
  	<td align="center">
		<input type="button" onClick="printBarcodes()" class="long-button-print" value="打印"/>
		&nbsp;&nbsp;&nbsp;&nbsp;
		 
	</td>
  </tr>
</table> 
<br>
</div>
<form action="printbarcode.html" target="_blank" method="get" name="print_barcode">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
	<input type="hidden" id="counts" name="counts"/>
</form>
<script type="text/javascript">
	function printBarcodes()
	{
		var productids = "";
		var copies = "";
		var counts = "1";
		
		var submit = true;
		var submitproduct = <%=productnull%>;
		<%
			for(int i= 0;i<rows.length;i++)
			{
		%>
			if(parseInt(document.getElementById("copies_<%=rows[i].getString("pc_id")%>").value)!=document.getElementById("copies_<%=rows[i].getString("pc_id")%>").value)
			{
				submit = false;
			}
			productids += <%=rows[i].getString("pc_id")%>+",";
			copies += document.getElementById("copies_<%=rows[i].getString("pc_id")%>").value+",";
		<%
			}
		%>

		if(submit&&submitproduct)
		{	
				var printer;
				var paper;
				var print_range_width;
				var print_range_height;
				var template_path;
				
				printer = "LabelPrinter";
				paper = "<%=String.valueOf(print_range_width)+"X"+String.valueOf(print_range_height)%>";
				print_range_width = <%=print_range_width%>;
				print_range_height = <%=print_range_height%>;
				template_path = "lable_template/productbarcode.html";
														
				document.getElementById("pc_id").value = productids;
				document.getElementById("copies").value = copies;
				document.getElementById("counts").value = counts;
							
				document.print_barcode.copies.value = copies;
				document.print_barcode.counts.value = counts;
				document.print_barcode.printer.value= printer;
				document.print_barcode.paper.value = paper;
				document.print_barcode.print_range_width.value = print_range_width;
				document.print_barcode.print_range_height.value = print_range_height;
				document.print_barcode.action ="<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
				
				document.print_barcode.submit();
				parent.closeWin();
		}
		else if(submit == false)
		{
			alert("有条码打印数量错误");
		}
		else if(submitproduct == false)
		{
			alert("商品不存在");
		}
		
	}
</script>
</body>
</html>
