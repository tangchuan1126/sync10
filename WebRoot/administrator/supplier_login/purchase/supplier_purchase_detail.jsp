<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="deliveryOrderKey" class="com.cwc.app.key.DeliveryOrderKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 
<% 
    try
    {
	String purchase_id = "";
	String[] fileSubmit		= new String[5];
	boolean isFileSubmit	= ServletFileUpload.isMultipartContent(request);
	if(isFileSubmit){
		fileSubmit = purchaseMgr.updatePurchaseTermsAndQuality(request);
	}else{
		purchase_id = StringUtil.getString(request, "purchase_id");
	}
	if(null != fileSubmit[0] && !"".equals(fileSubmit[0])){
		purchase_id = fileSubmit[1];
	}
	//String purchase_id = StringUtil.getString(request,"purchase_id");
	//String purchase_name = StringUtil.getString(request,"purchase_name");
	String lastEtaTime = purchaseMgrIfaceZr.getLastPurchaseDetailEta(Long.parseLong(purchase_id));
	//String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	
	DBRow searchPurchaseDetail = null;
	if(purchase_id!=null&&!purchase_id.equals(""))
	{
		searchPurchaseDetail=purchaseMgr.getPurchaseDetailOneByPurchaseId(purchase_id);
	}
	String expectArrTime = "";
	if(null != searchPurchaseDetail){
		expectArrTime = searchPurchaseDetail.getString("eta");
	}
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	DBRow supplier;
	long sid;
	try
	{
		sid = Long.parseLong(purchase.getString("supplier"));
	}
	catch(NumberFormatException e)
	{
		sid = 0;
	}
	if(sid ==0)
	{
		supplier = new DBRow();
		String name = purchase.getString("supplier");
		supplier.add("sup_name",name);
	}
	else
	{
		supplier = supplierMgrTJH.getDetailSupplier(sid);
	}
	HashMap followuptype = new HashMap();
	followuptype.put(4,"发票跟进");
	followuptype.put(5,"退税跟进");
	
	boolean edit = false;
	
	if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
	{
		edit = true;
	}
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("purchase_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
				String tempStr = arraySelected[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(Long.parseLong(purchase_id),productFileTyps );
	if(imagesRows != null && imagesRows.length > 0 ){
		for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
			DBRow tempRow = imagesRows[index];
			String product_file_type = tempRow.getString("product_file_type");
			List<DBRow> tempListRow = imageMap.get(product_file_type);
			if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(tempRow);
			imageMap.put(product_file_type,tempListRow);
		}
	}
	//读取配置文件中的transport_product_file
		String valueProduct = systemConfig.getStringConfigValue("transport_product_file");
		String flagProduct = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
		String[] arraySelectedProduct = valueProduct.split("\n");
		//将arraySelectedProduct组成一个List
		ArrayList<String> selectedListProduct= new ArrayList<String>();
		for(int index = 0 , count = arraySelectedProduct.length ; index < count ; index++ ){
				String tempStr = arraySelectedProduct[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedListProduct.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMapProduct = new HashMap<String,List<DBRow>>();
		int[] productFileTypes = {FileWithTypeKey.PURCHASE_PRODUCT_FILE};
		DBRow[] imagesRowsProduct = purchaseMgrZyj.getAllProductTagFilesByPcId(Long.parseLong(purchase_id),productFileTypes );
		if(imagesRowsProduct.length > 0 ){
			for(int index = 0 , count = imagesRowsProduct.length ; index < count ; index++ ){
				DBRow tempRow = imagesRowsProduct[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMapProduct.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMapProduct.put(product_file_type,tempListRow);
			}
		}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>采购单商品列表 </title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
#ui-datepicker-div{z-index:9999;display:none;}
-->
</style>
<link href="../../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../../common.js"></script>

<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>

<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>
	
<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>

<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/select.js"></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>

<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>

<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />



<script type="text/javascript" src="../../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>



	
<script type="text/javascript" src="../../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	body{text-align:left;}
</style>
<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/jgrowl/jquery.jgrowl.css"/>

<link type="text/css" href="../../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>


<script type="text/javascript">	
	function searchPurchaseDetail()
	{
		if($("#search_key").val().trim()=="")
		{
			alert("请输入要关键字")
		}
		else
		{			
			tb_show('采购产品搜索','search_purchase_detail.html?purchase_id='+<%=purchase_id%>+'&purchase_name='+$("#search_key").val()+'&TB_iframe=true&height=500&width=800',false);
		}	
	}
	//点击弹出制作麦头的界面
   function shippingMark()
   {
	    var uri = "../../lable_template/print_shpping_marjsp.html"; 
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function downloadPurchase(purchase_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/downloadPurchase.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{purchase_id:purchase_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							if(date.result.length>0)
							{
								alert(date.result);
							}
							else
							{
								alert("无法下载！");
							}
							
						}
					}
				});
	}
	
	function uploadPurchaseDetail()
	{
		var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_upload_excel.html?purchase_id=<%=purchase_id%>&isOutterUpdate=2&expectArrTime=<%=expectArrTime%>";
		$.artDialog.open(uri,{title:'上传采购单详细', height:500, width:800, lock:true,opacity:0.3,fixed:true});
		//tb_show('上传采购单详细','purchase_upload_excel.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		document.reach_form.submit();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function uploadIncominglog()
	{
		tb_show('上传入库日志','incoming_log.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function purchaseLogs(purchase_id)
	{
		tb_show('日志','followuplogs.html?purchase_id='+purchase_id+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function modPurchaseDelivery()
	{
		tb_show('修改交货信息','mod_purchase_delivery.html?purchase_id='+<%=purchase.get("purchase_id",0l)%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function print()
	{
	
		//var printer = visionariPrinter.SELECT_PRINTER();
		//if(printer!=-1)
		//{
			visionariPrinter.PRINT_INIT("采购单打印");
		
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../../purchase/print.html?purchase_id=<%=purchase_id%>");
		
			visionariPrinter.PREVIEW();
			//visionariPrinter.PRINT();	
		//}
			
	}
	function showTransit(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_transit_update.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '采购单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function applyMoney() {
		apply_form.submit();
	}
	function refreshWindow(){
		window.location.reload();
	}
	function updatePurchaseTerms(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_add_terms.html?isOutterUpdate=2&purchase_id=<%=purchase_id%>';
		$.artDialog.open(uri, {title: '修改条款信息',width:'535px',height:'500px', lock: true,opacity: 0.3});
	}
	function updatePurchaseProcedures(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_all_procedures.html?purchase_id=<%=purchase_id%>&isOutterUpdate=2';
		$.artDialog.open(uri, {title: '修改各流程信息',width:'550px',height:'500px', lock: true,opacity: 0.3});
	}
	function purchaseInvoiceState(follow_up_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_invoice_state.html?purchase_id=<%=purchase_id%>&follow_up_type="+follow_up_type;
		var procedure = "";
		if(4 == follow_up_type){
			procedure = "发票流程";
		}else if(5 == follow_up_type){
			procedure = "退税流程";
		}else if(8 == follow_up_type){
			procedure = "制签流程";
		}
		$.artDialog.open(uri, {title: procedure+'['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	}
	function purchaseQualityInspect(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_quality_inspection.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '质检要求['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	};
	function qualityInspectionLogs(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_quality_inspection_logs.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '质检要求日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function purchaseTagLogs(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_tag_logs.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '制签日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function purchaseProductModel(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '商品范例['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	}
	function productModelLogs(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model_logs.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '商品范例日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function addProductPicture(pc_id, purchase_id){
		 //添加商品图片
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_product_file.html?purchase_id="+purchase_id + "&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "商品图片上传["+purchase_id+"]",width:'770px',height:'470px', lock: true,close:function(){window.location.reload();},opacity: 0.3,fixed: true});
	}
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",

				

			 		  
			
		
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function ajaxLoadPurchaseDetail(obj,rowid)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getPurchaseDetailJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{purchase_detail_id:rowid},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function downLoad(fileName , tableName , folder){
		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>');
	}
	function submitQualityFile(){
		if($("#file_path").val() < 1){
			alert("请先上传文件");
		}else{
			$("#qualityFileForm").submit();
		}
	}
	function addDelveryOrder()
	{
		$.artDialog.open("../delivery/supplier_add_purchase_transport.html?purchase_id=<%=purchase_id%>", {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3});
	}
</script>

<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.addr_table tr td
{
	line-height:20px;
	height:18px;
}
#moneyTable tr td{
	line-height: 25px;
	height: 25px;
	border: 1px solid silver ;
}
#moneyTable{border-collapse: collapse;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>

</head>

<body >
<form action="purchase_detail.html" name="reach_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>
<form action="" name="download_form"></form>

<form name="del_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/clearPurchaseDetail.action">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form name="apply_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applyMoneyAction.action">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id %>'"/>
</form>

<div class="demo">
<div id="purchaseTabs">
	<ul>
		<li><a href="#purchase_basic_info">采购单<span> </span></a></li>
		<li><a href="#purchase_address_info">地址信息<span> </span></a></li>
		<li><a href="#purchase_tag_make">标签制作<span> </span></a></li>

		<% 
			if(purchase.get("quality_inspection",0)!=QualityInspectionKey.NO_NEED_QUALITY)
			{
		%>
		<li><a href="#purchase_quality_inspection">质检<span> </span></a></li>
		<%
			}
		%>
		<li><a href="#purchase_product_model">商品范例<span> </span></a></li>
	</ul>
	<div id="purchase_basic_info">
		<table style="width: 100%">
			<tr>
				<td style="width:65%">
					<table style="width:100%">
						<tr >
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">采购单号：</font></td>
							<td align="left" style="width:87%"><font style="font-family: 黑体; font-size: 16px;">P<%=purchase_id%></font></td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">到货情况：</font></td>
							<td align="left" style="width:87%" valign="middle">
								<table>
									<tr>
										<%
											DBRow[] deliveryOrderRows = purchaseMgrLL.getDeliveryOrderByPurchaseId(purchase_id);
											int rowSpanNum = deliveryOrderRows.length >1?(deliveryOrderRows.length-1):0;
										%>
										<td align='left' rowspan="<%=rowSpanNum %>" style="width:50%">
											<font style="font-family: 黑体; font-size: 15px;">
											<%=purchasearrivekey.getQuoteStatusById(purchase.getString("arrival_time"))%>
											</font>
										</td>
										<td style="width:50%;" align="left">
											<table>
												<%
											  		if(deliveryOrderRows.length == 0){
											  			out.println("<tr><td align='left'>&nbsp;</td></tr>");
											  		}else{
											  			out.println("<tr><td align='left'>"+deliveryOrderRows[0].getString("delivery_order_number")+" "+deliveryOrderKey.getDeliveryOrderStatusById(deliveryOrderRows[0].get("delivery_order_status",0))+"</td></tr>");
											  			for(int ii=1; deliveryOrderRows != null && ii<deliveryOrderRows.length; ii++) {
												  			out.println("<tr><td align='left'>"+deliveryOrderRows[ii].getString("delivery_order_number")+" "+deliveryOrderKey.getDeliveryOrderStatusById(deliveryOrderRows[ii].get("delivery_order_status",0))+"</td></tr>");
												  		}
											  		}
										  		 %>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">返修条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("repair_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">贴标条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("labeling_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">交货条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("delivery_product_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font style="font-family: 黑体; font-size: 16px;">其他条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("purchase_terms")%>
							</td>
						</tr>
					</table>
				</td>
				<td style="width:35%" align="left">
					&nbsp;
				</td>
			</tr>
			<tr align="left">
				<td colspan="2"><br/>
					<%
						if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
						{
					%>
					<input name="button" type="button" class="long-button" value="修改采购单信息" onClick="showTransit()"/>
					<input name="button" type="button" class="long-button" value="修改条款信息" onClick="updatePurchaseTerms()"/>
					<%
						}
					%>
					
					 <%
						if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
						{
					  %>
					  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.savePurchaseDetail">
					  <input  type="button" class="long-button-upload" value="上传采购单" onclick="uploadPurchaseDetail()"/>
					  </tst:authentication>
					  <% 
						}
					  %>
					 	<input  type="button" class="long-button-next" value="下载采购单" onclick="downloadPurchase(<%=purchase_id%>)"/>
					  
					  <input type="button" class="long-button-print" value="打印" onclick="print()"/>
					   <%
				 	   	if(purchase.get("purchase_status",0)==PurchaseKey.AFFIRMTRANSFER||purchase.get("purchase_status",0)==PurchaseKey.DELIVERYING)
				 	   	{
				 	  %>
							
				 	  <%
				 	   	}
				 	  %>
				</td>
			</tr>
		</table>
	</div>
	<div id="purchase_address_info">
			<table width="100%" style="border:1px solid silver;">
				<tr>
					<td style="width:10%;text-align:right;">
<%--						<font style="font-family: 黑体; font-size: 14px;">地址：</font>--%>&nbsp;
					</td>
					<td style="width:90%;">
						<table style="width:100%;" class="addr_table">
							<tr>
								<td style="width:45%;">
									<table style="width:100%;border-collapse:collapse ;">
										<tr>
											<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("address_houme_number"):"" %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("address_street"):"" %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("city_input"):"" %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("zip_code"):"" %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ps_id = null!=supplier?supplier.get("province_id",0L):0L ;
													DBRow psIdRow = productMgr.getDetailProvinceByProId(ps_id);
												%>
												<%=(psIdRow != null? psIdRow.getString("pro_name"):supplier.getString("address_pro_input") ) %>
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ccid = null!=supplier?supplier.get("nation_id",0L):0L; 
													DBRow ccidRow = orderMgr.getDetailCountryCodeByCcid(ccid);
												%>
												<%= (ccidRow != null?ccidRow.getString("c_country"):"" ) %>	
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("linkman"):"" %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=null!=supplier?supplier.getString("phone"):"" %></td>
										</tr>
									</table>
								</td>
								<td style="width:10%;text-align: center;font-size: 18px;">
									-->
								</td>
								<td style="width:45%;">
									<table style="width:100%;border-collapse:collapse ;" class="addr_table">
										<tr>
											<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ps_id1 = null!=purchase?purchase.get("deliver_provice",0l):0L;
													DBRow psIdRow1 = productMgr.getDetailProvinceByProId(ps_id1);
												%>
												<%=(psIdRow1 != null? psIdRow1.getString("pro_name"):purchase.getString("deliver_pro_input") ) %>
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ccid1 = null!=purchase?purchase.get("deliver_native",0l):0L; 
													DBRow ccidRow1 = orderMgr.getDetailCountryCodeByCcid(ccid1);
												%>
												<%= (ccidRow1 != null?ccidRow1.getString("c_country"):"" ) %>	
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("delivery_linkman")%></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("linkman_phone")%></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">
						<font style="font-family: 黑体; font-size: 14px;">供应商：</font>
					</td>
					<td>
						<%=null!=supplier?supplier.getString("sup_name"):"" %>
					</td>
				</tr>
				<tr>
				<td colspan="3" align="left">
					<br/>
					<input name="button" type="button" class="long-button" value="修改" onClick="showTransit()"/>
				</td>
			</tr>
			</table>
	</div>
	<div id="purchase_tag_make">
		<div id="tagTabs" style="margin-top:5px;">
			<table>
				<tr>
					<td colspan="3">
						<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
						<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
						<input type="button" class="long-button-print" value="跟进制签" onclick="purchaseInvoiceState(8)"/>
						<input type="button" class="long-button-print" value="查看日志" onclick="purchaseTagLogs()"/>
					</td>
				</tr>
				<tr><td height="20px;" colspan="2"></td></tr>
			</table>
   		 	<ul>
	<%
			 		if(selectedList != null && selectedList.size() > 0){
			 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
	%>	
			 				<li><a href="#purchase_product_tag_<%=index %>"> <%=selectedList.get(index) %></a></li>
			 			<% 	
			 			}
			 		}
		 		%>
   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
   		 			%>
   		 			 <div id="purchase_product_tag_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 			<tr> 
			  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
			  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				<tr style="height: 25px;">
   		 			 					<td>
   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%= row.getString("file_name") %>&folder=<%=systemConfig.getStringConfigValue("file_path_product")%>'><%= row.getString("file_name") %></a>
   		 			 					</td>
   		 			 					<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %></td>
   		 			 					<td>
   		 			 						<% if(!(purchase.get("nee_tag",0) == TransportTagKey.FINISH && purchase.get("need_tag",0.0d) > 0)){ %>
   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 		<%} %>	
   		 			 				 	</td>
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
		</div>
	<%	
		} 
	%>
</div>
		<br/>
</div>
	<%
		if(QualityInspectionKey.NO_NEED_QUALITY != purchase.get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY))
		{
	%>
	<div id="purchase_quality_inspection">
	<div id="qualityInspection">
		<form action="" method="post" enctype="multipart/form-data" id="qualityFileForm">
		<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
		<input type="hidden" name="sn" value="P_quality"/>
		<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURHCASE_QUALITY_INSPECTION %>" />
		<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>" />
	<table style="width: 70%;">
	<tr>
		<td colspan="2">
			<input name="button" type="button" class="long-button" value="跟进质检要求" onClick="purchaseQualityInspect()"/>
			<input name="button" type="button" class="long-button" value="查看日志" onClick="qualityInspectionLogs()"/>
		</td>
	</tr>
	<tr><td height="20px;" colspan="2"></td></tr>
	<%
		//if(2 != isOutterUpdate){
	%>	
	<tr>
		<td width="10%" align="right" valign="top" nowrap="nowrap" class="STYLE3" >质检要求:</td>
		<td width="90%" align="left" valign="top" style="padding-left: 13px;">
			<input type="file" name="file" id="file_path"/>
		 	<input type="button" value="上传" onclick="submitQualityFile()"/>
		</td>
	</tr>
	<%
		int[] fileTypes = {9};
		DBRow[] purchaseQualityFiles = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.parseLong(purchase_id), fileTypes);
		if(purchaseQualityFiles.length > 0){
	%>
		<tr>
		<td width="10%" align="right" valign="middle" nowrap="nowrap" class="STYLE3">&nbsp;</td>
		<td width="90%" align="left" valign="top" >
			<table width="75%">
				<%
					if(purchaseQualityFiles.length > 0){
						for(int f = 0; f < purchaseQualityFiles.length; f ++){
							DBRow purchaseQualityFile = purchaseQualityFiles[f];
				%>
					<tr style="height: 25px;">
						<td width="60%" align="left">
						<a href='<%= downLoadFileAction%>?file_name=<%=purchaseQualityFile.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'><%=purchaseQualityFile.getString("file_name") %></a>
						</td>
						<td width="15%" align="left">
							<%=null == adminMgrLL.getAdminById(purchaseQualityFile.getString("upload_adid"))?"":adminMgrLL.getAdminById(purchaseQualityFile.getString("upload_adid")).getString("employe_name") %>
						</td>
						<td width="25%" align="left">
						<% if(!"".equals(purchaseQualityFile.getString("upload_time"))){
								out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(purchaseQualityFile.getString("upload_time"))));
							}
						%>
						</td>
					</tr>
				<%	
						}
					}
				%>
			</table>
			</td>
		</tr>
		<%
			}
		//}
		%>
</table>
</form>
	</div>
</div>
	<%	} %>
	<div id="purchase_product_model">
		<div id="productModel">
		<table width="98%">
			<tr>
				<td colspan="3" align="left">
					<input name="button" type="button" class="long-button" value="跟进商品范例" onClick="purchaseProductModel()"/>
					<input name="button" type="button" class="long-button" value="查看日志" onClick="productModelLogs()"/>
				</td>
			</tr>
			<tr><td height="20px;" colspan="3"></td></tr>
		</table>
			<ul>
	<%
					 		if(selectedListProduct != null && selectedListProduct.size() > 0){
					 			for(int index = 0 , count = selectedListProduct.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#transport_product_<%=index %>"> <%=selectedListProduct.get(index) %></a></li>
					 			<% 	
		} 
					 		}
	%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedListProduct.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRowsProduct = imageMapProduct.get((index+1)+"");
   		 			%>
   		 			 <div id="transport_product_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 			<tr> 
			  						<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						<th width="30%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
			  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
   		 			 	<%
   		 			 		if(tempListRowsProduct != null && tempListRowsProduct.size() > 0){
   		 			 			 for(DBRow row : tempListRowsProduct){
   		 			 			%>
   		 			 				<tr style="height: 25px;">
   		 			 					<td>
   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=row.getString("file_name") %></a>
   		 			 					</td>
   		 			 					<td>
   		 			 						<%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %>
   		 			 					</td>
   		 			 					<td>
   		 			 						<% if(!(purchase.get("product_model",0) == TransportProductFileKey.FINISH && purchase.get("product_model_over",0.0d) > 0)){ %>
   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 		<%} %>	
   		 			 				 	</td> 
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
	</div>
</div>
</div>
</div>
<br/>
<script>
	$("#purchaseTabs,#tagTabs,#productModel").tabs({
		cache: true,
		spinner: '<img src="../../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
	
	
	<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
	function checkEta(value, colname)
	{
		var a=/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})/
		if (!a.test(value))
		{
			return [false,"请输入正确的日期格式"];
		}
		else
		{
			return [true,""];
		}
	}
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'../../purchase/dataPurchaseDetails.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{purchase_id:<%=purchase_id%>},
					jsonReader:{
				   			id:'purchase_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['purchase_detail_id','product_id','商品名','工厂型号','预计到达日期','备用件','采购数','总数','收货数','在途数','单位','采购价','体积','操作','purchase_id'], 
				   	colModel:[ 
				   		{name:'purchase_detail_id',index:'purchase_detail_id',hidden:true,sortable:false},
				   		{name:'product_id',index:'product_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'factory_type',index:'factory_type',align:'left',editable:true},
				   		{name:'eta',index:'eta',align:'left',editable:true,editrules:{custom:true,custom_func:checkEta}},
				   		{name:'backup_count',index:'backup_count',width:50,editable:false,align:'center',formatter:'number',editrules:{number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'order_count',index:'order_count',width:50,editable:false,align:'center',formatter:'number',editrules:{number:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'purchase_count',index:'purchase_count',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'reap_count',index:'reap_count',align:'center',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'transit_count',index:'transit_count',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'unit_name',index:'unit_name',align:'center',width:60},
				   		{name:'price',index:'price',editable:<%=edit%>,align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},sortable:false},
				   		{name:'volume',index:'volume',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'button',index:'button',align:'left',sortable:false},
				   		{name:'purchase_id',index:'purchase_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=purchase_id%>},hidden:true,sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'purchase_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
					multiselect: true,
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   					}
				   					if(name='eta')
				   					{
				   						//jQuery("#"+iRow+"_eta","#gridtest").date_input();
				   					}
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	ajaxLoadPurchaseDetail(jQuery("#gridtest"),rowid);
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/gridEditPurchaseDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/gridEditPurchaseDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
	</script>
<table width="95%">
	<tr>
	  <td align="left" style="padding-left:25px;">
	  	<%if(purchase.get("purchase_status",0)!=PurchaseKey.CANCEL&&purchase.get("purchase_status",0)!=PurchaseKey.FINISH&&!purchase.getString("purchasedetail").equals("")&&purchase.get("purchase_status",0)!=PurchaseKey.OPEN&&purchase.get("purchase_status",0)!=PurchaseKey.AFFIRMPRICE&&(purchase.get("arrival_time",0)!= PurchaseArriveKey.WAITAPPROVE&&purchase.get("arrival_time",0)!= PurchaseArriveKey.ALLARRIVE))
	  	  {
	  	%>
	  	<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseApproveMgrZJ.addPuchaseApprove">
	  		<input type="button" name="Submit2" value="申请完成" class="normal-white" onClick="applicationApprove()"/>&nbsp;&nbsp;
	  	</tst:authentication>
	  	<%
	  	  }
	  	%>
	  	<%
	  		if(purchase.get("purchase_status",0) == PurchaseKey.OPEN)
	  		{
	  	%>
		<input type="button" value="通知采购" class="normal-white" onclick="notice()"/>	  	
		<%
			}
		%>
		<%  
			if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
  	  		{
  	  			DBRow[] db = purchaseMgr.getPurchaseDetailByPurchaseid(purchase.get("purchase_id",0l),null,null);
  	  			if(db.length>0)
  	  			{
		%>
		<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.affirmPricePurchase">
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="normal-white" onclick="affirmPrice(<%=purchase.getString("purchase_id")%>)" value="价格确定"/>
		</tst:authentication>
	  <%
	  			}
	  		}
	  %>
	  </td>
	  <td align="center" style="padding-right:50px;">
		&nbsp;
	  </td>
   	  <td align="right"><input type="button" name="Submit2" value="返回" class="normal-white" onClick="window.location.href='purchase.html'"/></td>
  	</tr>
</table>
<form action="purchase_detail.html" name="search_form" method="get">
	<input type="hidden" name="purchase_name" id="purchase_name"/>
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id %>"/>
</form>
<form name="dataForm" action="purchase_detail.html" method="post">
    <input type="hidden" name="p"/>
    <input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
</form>
  <form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="followup_ptype_sub"/>
</form>

<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/notice.action" method="post" name="notice_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form action="../../lable_template/made_purchase_lable.html" name="print_label_form" target="_blank" method="post"">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form name="transfer_form" method="post">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmTransferPurchase.action" id="apply_funds_form" name="apply_funds_form" method="post">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="type" value="<%=ApplyTypeKey.PURCHASE%>"/>
	<input type="hidden" name="supplier_id"/>
	<input type="hidden" name="remark"/>
	<input type="hidden" name="amount" />
	<input type="hidden" name="payee" />
	<input type="hidden" name="payment_information" />
</form>

  <script type="text/javascript">
//点击弹出制作麦头的界面
  function shippingMark()
  {
	    var purchase_id=<%=purchase_id%>;
	    var menpai="<%=purchase.getString("deliver_house_number") %>";
	    var jiedao="<%=purchase.getString("deliver_street") %>";
	    var chengshi="<%=purchase.getString("deliver_city") %>";
	    var youbian="<%=purchase.getString("deliver_zip_code") %>";
	    var shengfen="<%=ps_id%>";
	    var guojia="<%= ccid %>";
	    var lianxiren="<%=purchase.getString("delivery_linkman")%>";
	    var dianhua="<%=purchase.getString("linkman_phone")%>";
	    var uri = "../lable_template/print_purchase_shipping_mark.html?purchase_id="+purchase_id+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai;
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
  }
  function applicationApprove()
  {
  	if(confirm("确定申请此采购单完成"))
  	{
  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
  		document.application_approve.submit();
  	}
  }
  
  function promptCheck(v,m,f)
			{
				if (v=="y")
				{
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 return true;
				}
			}
		  	function followup(purchase_id,purchase_invoice,purhcase_drawback)
			{
				if((1 == purchase_invoice && 1 == purhcase_drawback) || (4 == purchase_invoice && 4 == purhcase_drawback) ||(1 == purchase_invoice && 4 == purhcase_drawback)||(4 == purchase_invoice && 1 == purhcase_drawback)){
					alert("没有跟进项目...");
				}else{
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/purchase_log/purchase_log_invoice_drawback_add.html?purchase_id="+purchase_id+"&purchase_invoice="+purchase_invoice+"&purhcase_drawback="+purhcase_drawback;
					 $.artDialog.open(uri , {title: "日志跟进["+purchase_id+"]",width:'450px',height:'350px', lock: true,opacity: 0.3,fixed: true});
				}
			}
		  	function changeLogType(){
				$("#ptypeSub") && $("#ptypeSub").remove();
				$("#ptypeSubTitle") && $("#ptypeSubTitle").remove();
				var selVal = $("#ptype").val();
				if(3 == selVal || 4 == selVal){
					var appendElem = "<span id='ptypeSubTitle'>阶段</span><select id = 'ptypeSub' name='ptypeSub'>";
					if(3 == selVal){
						<%
			  			ArrayList statuses = invoiceKey.getInvoices();
			  			for(int i=2; i<statuses.size(); i++) {
			  				int key = Integer.parseInt(statuses.get(i).toString());
			  				String value = invoiceKey.getInvoiceById(String.valueOf(key));
				  		%>
				  			appendElem += "<option value='<%=key%>'><%=value%></option>";
				  		<%
				  			}
				  		%>
					}else{
						<%	
			  			ArrayList statuses4 = drawbackKey.getDrawbacks();
			  			for(int i = 2; i < statuses4.size(); i ++) {
			  				int key1 = Integer.parseInt(statuses4.get(i).toString());
				  			String value1 = drawbackKey.getDrawbackById(String.valueOf(key1));
						%>
							appendElem += "<option value='<%=key1%>'><%=value1%></option>";
						<%
				  			}
				  		%>
					}
					appendElem += "</select>";
					$("#ptype").after($(appendElem));
					
				}
			};
			
			function remark(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>备注</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/logsPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function notice()
			{
				document.notice_form.submit();
			}
			
			function modPurchaseTerms()
			{
				tb_show('上传采购单详细','purchase_terms.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
			}
			
			 //点击制作标签后修改成弹出dialog
			function printLabel()
			{
				var value='<%=purchase_id%>';
				var supplierName='<%=supplier.getString("sup_name") %>';
				var supplierSid='<%=sid%>';
				var uri = "../../lable_template/made_purchase_lable.html?purchase_id="+value+"&supplierName="+supplierName+"&supplierSid="+supplierSid; 
				$.artDialog.open(uri , {title: '采购单标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
				//document.print_label_form.submit();
			}
			
			function transfer(purchase_id,money_status)
			{
				$.prompt(
				
				"<div id='title'>转款</div><br />转款类型:<input type='radio' value='all' id='transfertype' name='transfertype'/>全款&nbsp;&nbsp;<input type='radio' value='part' id='transfertype' name='transfertype'/>部分转款<br/>转款备注:<br/><textarea rows='5' cols='60' id='content' name='content'/><br>",
				{
					  submit:promptCheckTransfer,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.transfer_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/purchaseTransfer.action"
										document.transfer_form.purchase_id.value = purchase_id;
										document.transfer_form.followup_content.value = f.content;		
										document.transfer_form.transfer_type.value=f.transfertype;
										document.transfer_form.money_status.value=money_status;
										document.transfer_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function promptCheckTransfer(v,m,f)
			{
				
				if (v=="y")
				{
				
					  if(f.transfertype ==null)
					 {
					 	alert("请选择转款类型");
						return false;
					 }
					 
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 
					
					 
					 return true;
				}
			}
			
			function affirmPrice(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>价格确认</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmPricePurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = "价格确定:"+f.content;		
										document.followup_form.submit();	
								}
							}
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function ffirmTransfer(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>可以转款</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmTransferPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function affirmTransfer(purchase_id)
			{
				//tb_show('资金申请','purchase_apply_funds.html?purchase_id='+purchase_id+'&type=<%=ApplyTypeKey.PURCHASE%>&TB_iframe=true&height=500&width=800',false);
				 var uri = "purchase_apply_funds.html?purchase_id="+purchase_id+"&type=<%=ApplyTypeKey.PURCHASE%>";
				 $.artDialog.open(uri , {title: "[ "+purchase_id+" ]资金申请",width:'900px',height:'560px', lock: true,opacity: 0.3,fixed: true});
			}
			function submitApplyFundsForm(o){
				if(o){
					var str = "" ;
					for(var obj in  o){
						str += obj +" : "+ o[obj];
					}
					 document.apply_funds_form.remark.value = o["remark"];
					 document.apply_funds_form.amount.value = o["amount"];
					 document.apply_funds_form.supplier_id.value = o["supplier_id"];
					 document.apply_funds_form.payee.value = o["payee"];
					 document.apply_funds_form.payment_information.value = o["payment_information"];
					 document.apply_funds_form.submit();
				}
			}
			function uploadInvoice(transport_id)
			{
				$.artDialog.open("transport_upload_invoice.html?transport_id="+transport_id, {title: '转运单上传发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
			}
			function addProductTagTypesFile(purchase_detail_id,pc_id, purchase_id){
				 //添加商品标签
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_tag_type_file.html?purchase_id="+purchase_id ;
				uri += getSelectedIdAndNames(purchase_detail_id)
				$.artDialog.open(uri , {title: "商品标签上传["+purchase_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3 ,fixed: true});
			}
			function getSelectedIdAndNames(purchase_detail_id){
				s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";  
			  	if($.trim(s).length < 1){
					s = ""+purchase_detail_id;
				} 
			 	var array = s.split(",");
				var strIds = "";
			 	for(var index = 0 , count = array.length ; index < count ; index++ ){
				   var number = array[index];
			 	   var thisid= $("#gridtest").getCell(number,"product_id");
			 	  	strIds += (","+thisid);
				}
				if(strIds.length > 1 ){
				    strIds = strIds.substr(1);
				}
				return  "&pc_id="+strIds;
			}
  </script>
</body>
</html>
<%} 
	catch(Exception e)
	{
		out.print("<script>alert('系统异常，请您重新登录。带来不便，非常抱歉')</script>");
		response.sendRedirect("http://www.1you.com/");
	}
%>
