<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.ProductStorageTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.TransportStockInSetKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.exception.supplier.SupplierLoginTimeOutException"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="fileWithTypeKey" class="com.cwc.app.key.FileWithTypeKey"/>
<jsp:useBean id="transportCertificateKey" class="com.cwc.app.key.TransportCertificateKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
 <jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"/>
<jsp:useBean id="transportQualityInspectionKey" class="com.cwc.app.key.TransportQualityInspectionKey"/>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../../include.jsp"%> 
<%
	
	if(adminMgr.getAdminLoginBean(session).getAttach()==null)
	{
		throw new SupplierLoginTimeOutException();
	}
	long supplier_id = adminMgr.getAdminLoginBean(session).getAttach().get("id",0l);
		
	String key = StringUtil.getString(request,"key");
	
	long send_psid = StringUtil.getLong(request,"send_psid",0);
	long receive_psid = StringUtil.getLong(request,"receive_psid",0);
	int status = StringUtil.getInt(request,"status",0);
	int declaration = StringUtil.getInt(request,"declarationStatus",0);
	int clearance = StringUtil.getInt(request,"clearanceStatus",0);
	int invoice = StringUtil.getInt(request,"invoiceStatus",0);
	int drawback = StringUtil.getInt(request,"drawbackStatus",0);
	int day = StringUtil.getInt(request,"day",3);
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	int transport_status = StringUtil.getInt(request,"transport_status",0);
	int stock_in_set = StringUtil.getInt(request,"stock_in_set",0);
	long dept = StringUtil.getLong(request,"dept",0);
	long create_account_id = StringUtil.getLong(request,"create_account_id",0);
	long dept1 = StringUtil.getLong(request,"dept1",0);
	long create_account_id1 = StringUtil.getLong(request,"create_account_id1",0);
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	String cmd = StringUtil.getString(request,"cmd");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	DBRow[] rows = null;
	if(cmd.equals("search"))
	{
		rows = transportMgrZJ.supplierSearchTransport(key,supplier_id,pc);
	}
	else
	{
		rows = transportMgrZJ.getTransportBySupplierId(supplier_id,pc);
	}
	

	
	TransportOrderKey transportOrderKey = new TransportOrderKey();
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
	HashMap followuptype = new HashMap();
	//操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"商品图片流程");
	followuptype.put(11,"质检流程");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>转运单处理</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;border: 2px solid #993300;} 
p{text-align:left;}
tr.split td input{margin-top:2px;}
-->
</style>

 <script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../../common.js"></script>

<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/icon.css"/>


<script type="text/javascript" src="../../js/select.js"></script>

<script type='text/javascript' src='../../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ajaxQueue.js'></script>
 

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
 
 

<script type="text/javascript" src="../../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
span.stateName{width:15%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:80%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
</style>
<script>
	function search()
	{
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("请输入要查询单号");
		}
		else
		{
			if(val.substring(0,1).toUpperCase()=="P"||val.substring(0,1).toUpperCase()=="T")
			{
				document.search_form.key.value = val;
				document.search_form.submit();
			}
			else
			{
				alert("输入格式不对！（采购单:Pxxxxx或转运单Txxxx）");
			}
			
		}
	}
	
	
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 
			 return true;
		}
	}

	function changeType(obj) {
		$('#invoice').attr('style','display:none');
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		$('#drawback').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}else if($(obj).val()==3) {
			$('#invoice').attr('style','');
		}else if($(obj).val()==4) {
			$('#drawback').attr('style','');
		}
	}

	function remark(transport_id)
	{
		var text = "<div id='title'>跟进</div><br />";
		text += "备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
		$.prompt(text,
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportRemarkAction.action"
										document.followup_form.transport_id.value = transport_id;
										document.followup_form.transport_content.value = "备注:"+f.content;
										document.followup_form.type.value = 1;
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
	}
	
	function followup(transport_id){
	
	
	 		
	
	  		
	  		
							
				  
 	 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_follow_up.html"; 
 			uri += "?transport_id="+transport_id+"&declaration="+declaration+"&clearance="+clearance+"&stock_in_set="+stock_in_set+"&transport_status="+transport_status;
	 		$.artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
		 
	}
	function goFundsTransferListPage(id)
	{
	 window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;      
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	$(document).ready(function(){
		//alert('');
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,'<%=dept%>');
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#dept"),vname,'<%=create_account_id%>');
		getLevelSelect(0, nameArray1, widthArray1, centerAccounts, null,vname1,'<%=dept1%>');
		getLevelSelect(1, nameArray1, widthArray1, centerAccounts, $("#dept1"),vname1,'<%=create_account_id1%>');
	});
	<%//level_id,value,name
		String str = "var centerAccounts = new Array(";
		str += "new Array('01',0,'选择部门')";
		str += ",new Array('01.0',0,'选择职员')";
		for(int i=0;i<adminGroups.length;i++) {//部门
			DBRow adminGroup = adminGroups[i];
			str += ",new Array('0"+(i+2)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
			str += ",new Array('0"+(i+2)+".0',0,'选择职员')";
			for(int ii=0;ii<accounts.length;ii++) {
				DBRow account = accounts[ii];
				if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
					str += ",new Array('0"+(i+2)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
			}
		}
		str+= ");";
		out.println(str);
	%>
	var nameArray = new Array('dept','create_account_id');
	var widthArray = new Array(120,120);
	var vname = new Array('nameArray','widthArray','centerAccounts','vname');
	var nameArray1 = new Array('dept1','create_account_id1');
	var widthArray1 = new Array(120,120);
	var vname1 = new Array('nameArray1','widthArray1','centerAccounts','vname1');
	function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
			if(level<nameArray.length) {
				var name = nameArray[level];
				var width = widthArray[level];
				var levelId = o==null?"":$("option:selected",o).attr('levelId');
				var onchangeStr = "";
				if(level==nameArray.length-1)
					onchangeStr = "";
				else
					onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";
				var selectHtml = "&nbsp;&nbsp;<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
				//alert(selectArray);
				for(var i=0;i<selectArray.length;i++) {
					if(levelId!="") {
						var levelIdChange = selectArray[i][0].replace(levelId+".");
						var levelIds = levelIdChange.split(".");	
						//alert(levelIdChange);
						if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
							//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
					else {
						var levelIdChange = selectArray[i][0];
						//alert(levelIdChange);
						var levelIds = levelIdChange.split(".");
						if(levelIds.length==1){
							//alert(levelId+","+selectArray[i][0]+levelId1);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
				}
				selectHtml += "</select>";
				$('#'+name+'_div').html('');
				$('#'+name+'_div').append(selectHtml);
				//alert(selectHtml);
				$('#'+name).val(value);
				getLevelSelect(level+1,nameArray,widthArray,centerAccounts,$('#'+name),vnames);
			}
	}
	
	function uploadFile(association_id,association_type)
	{
		var url = "transport_upload_image.html?association_id="+association_id+"&association_type="+association_type;
		$.artDialog.open(url, {title: '交货单文件',width:'700px',height:'600px',lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	
	function addDeliveryTransportOrder()
	{
		var url = "add_purchase_transport.html";
		$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	function refreshWindow(){
		go($("#jump_p2").val());
	}
	function transport_certificate(transport_id,file_with_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate.html"; 
		uri += "?transport_id="+transport_id + "&file_with_type="+file_with_type;
		$.artDialog.open(uri, {title: '单证流程',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function clearanceButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
		$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function declarationButton(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function stock_in_set(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_stockinset.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "运费流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
	function quality_inspectionKey(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
	function product_file(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "商品图片["+transport_id+"]",width:'850px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function tag(transport_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_tag.html?transport_id="+transport_id;
		$.artDialog.open(uri , {title: "制签["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#transport_search">常用工具</a></li>
		</ul>
		<div id="transport_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
				</td>
	              <td width="33%"></td>
	              <td width="24%" align="right" valign="middle">
	              &nbsp;
				  </td>
	            </tr>
	          </table>
		</div>
	</div>
</div>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>

	<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
    <tr> 
        <th width="11%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">转运单号</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">库房信息</th>
        <th width="12%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运输信息</th>
        <th width="12%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">体积</th>
        <th width="12%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">流程信息</th>
        <th width="6%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">最后更新时间</th>
  	</tr>
  	<%
  		for(int i = 0 ;i<rows.length;i++)
  		{
  	%>
  		<tr align="center">
  			<td height="40" nowrap="nowrap">
  				<fieldset class="set">
  					<legend><a style="color:#f60;" href="delivery_order_detail.html?transport_id=<%=rows[i].getString("transport_id")%>">T<%=rows[i].getString("transport_id")%></a></legend>
  					<p class="showP" style="clear:both;">
  						 创建人:
  						 <%
  						 	DBRow createAdmin = adminMgr.getDetailAdmin(rows[i].get("create_account_id",0l));
  						 	if(createAdmin!=null)
  						 	{
  						 		out.print(createAdmin.getString("employe_name"));
  						 	} 
  						 %> 
  					</p>
  					<p class="" style="clear:both;">
  						允许装箱:
  						 <%
  						 	DBRow packingAdmin = adminMgr.getDetailAdmin(rows[i].get("packing_account",0l));
  						 	if(packingAdmin!=null)
  						 	{
  						 		out.print(packingAdmin.getString("employe_name"));
  						 	} 
  						 %> 
  					</p>
  					<p class="showP" style="clear:both;">
  						创建时间:<%=tDate.getFormateTime(rows[i].getString("transport_date"))%>
  					</p>
  					<%
  						if(rows[i].get("purchase_id",0l) > 0 ){
  						%>
  							<p>
  								采购单号:P<%=rows[i].getString("purchase_id") %>
  							</p>
  							<p>
  								&nbsp;&nbsp;批次:<%=rows[i].getString("transport_number") %>
  							</p>
  						<% 	
  						}
  					%>
  					<p>
  					   更新时间:<%= tDate.getFormateTime(rows[i].getString("updatedate")) %>
  					</p>
  				</fieldset>
  			</td>
  			<td nowrap="nowrap" align="left">
  				<p>
  				提货仓库:
  				<% 
  					int fromPsType = rows[i].get("from_ps_type",0);
  					//如果是供应商的Type那么就需要去查询供应商的名称
 	  				if(fromPsType == ProductStorageTypeKey.SupplierWarehouse){
 	  					DBRow temp = supplierMgrTJH.getDetailSupplier(rows[i].get("send_psid",0l));
 	  					if(temp != null){
 	  						out.println(temp.getString("sup_name"));
 	  					}
 	  				}
 	  				else
 	  				{
 	  					DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l));
 	  					if(storageCatalog != null){
 	  						out.print(storageCatalog.getString("title"));
 	  					}
 	  				}
  				%>
  				</p>
  				<p>
  					收货仓库:<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title")%> 
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;收货人:
  					<%	 
  						long deliveryer_id = rows[i].get("deliveryer_id",0l);
  						if(deliveryer_id != 0l){
  							out.println(adminMgr.getDetailAdmin(deliveryer_id).getString("employe_name"));
  						}
  					%>
  				</p>
  				<p>
  					收货时间:<%
  						String deliveryTime = rows[i].getString("deliveryed_date");
  						 out.print(deliveryTime.length() > 10 ?deliveryTime.substring(0,10): deliveryTime);
  					%>
  				</p>
  				<p>
  					&nbsp;&nbsp;&nbsp;&nbsp;入库人:<%
  						long warehousinger_id = rows[i].get("warehousinger_id",0l);
						if(deliveryer_id != 0l){
							out.println(adminMgr.getDetailAdmin(warehousinger_id).getString("employe_name"));
						}
  					%>
  				</p>
  				<p>
  					入库时间:
  					<%  
  						String warehousingTime = rows[i].getString("deliveryed_date");
  						out.print(warehousingTime.length() > 10 ?warehousingTime.substring(0,10): deliveryTime);
  					%>
  				</p>
  			</td>
  			<td nowrap="nowrap" align="left">
  				运单号:<%=rows[i].getString("transport_waybill_number")%><br/>
  				货运公司:<%=rows[i].getString("transport_waybill_name")%><br/>
  				承运公司:<%=rows[i].getString("carriers")%><br/>
  				<%
  					long transport_send_country = rows[i].get("transport_send_country",0l);
  					DBRow send_country_row = transportMgrLL.getCountyById(Long.toString(transport_send_country));
  					long transport_receive_country = rows[i].get("transport_receive_country",0l);
  					DBRow receive_country_row = transportMgrLL.getCountyById(Long.toString(transport_receive_country));					
  				%>
  				始发国/始发港:<%=send_country_row==null?"无":send_country_row.getString("c_country")%>/<%=rows[i].getString("transport_send_place").equals("")?"无":rows[i].getString("transport_send_place")%><br/>
  				目的国/目的港:<%=receive_country_row==null?"无":receive_country_row.getString("c_country")%>/<%=rows[i].getString("transport_receive_place").equals("")?"无":rows[i].getString("transport_receive_place")%><br/>
 			<%
			if(rows[i].get("stock_in_set",1)==1) {
				out.println("<font color='red'><b>");
			}
			%>
			运费:<%=computeProductMgrCCC.getSumTransportFreightCost(rows[i].getString("transport_id")).get("sum_price",0d) %>RMB<br/>
			<%
				if(rows[i].get("stock_in_set",1)==1) {
					out.println("</b></font>");
				}
			%>
			<%
				DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("transport_id"),6);//
				for(int j=0; j<applyMoneys.length; j++){
		 			out.println("<a href=\"javascript:void(0)\" onClick=\"goApplyFundsPage('"+applyMoneys[j].get("apply_id",0)+"')\">F"+applyMoneys[j].get("apply_id",0)+"</a>申请资金"+applyMoneys[j].get("amount",0f)+"RMB<br/>");
		 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByBusiness(rows[i].getString("transport_id"),6);
		 			if(applyTransferRows.length>0) {
		 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
		 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
			  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
			  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
			  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
			  				int moneyStatus = applyTransferRows[ii].get("status",0);
			  				if(moneyStatus != 0 )
			  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+"<br/>");
			  				else
			  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+"<br/>");
			  			}
		 			}
		 		}
		 		}
		 	%>
  			</td>
  			<td>
  				<%=transportMgrZJ.getTransportVolume(rows[i].get("transport_id",0l))%>
  			</td>
  			<td nowrap="nowrap" style="text-align:left;">
  				货物状态:<%=transportOrderKey.getTransportOrderStatusById(rows[i].get("transport_status",0)) %>&nbsp;<%=rows[i].getString("all_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":"共用"+rows[i].getString("all_over")+"天" %><br />
	  			出口报关:<%=declarationKey.getStatusById(rows[i].get("declaration",01)) %><%=rows[i].get("declaration",01)==1?"":(rows[i].getString("declaration_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":"共用"+rows[i].getString("declaration_over")+"天")%><br/>
	  			进口清关:<%=clearanceKey.getStatusById(rows[i].get("clearance",01)) %><%=rows[i].get("clearance",01)==1?"":(rows[i].getString("clearance_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":"共用"+rows[i].getString("clearance_over")+"天")%><br/>
	  			制签状态:<%=transportTagKey.getTransportTagById(rows[i].get("tag",0))%><%=rows[i].get("tag",01)==1?"":(rows[i].getString("tag_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":"共用"+rows[i].getString("tag_over")+"天")%><br/>
	  			单证状态:<%=transportCertificateKey.getStatusById(rows[i].get("certificate",0))%><%=rows[i].get("certificate",01)==1?"":(rows[i].getString("certificate_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("transport_date"),"dd"))+"天":"共用"+rows[i].getString("certificate_over")+"天")%><br/>
	  			质检流程:<%=transportQualityInspectionKey.getStatusById(rows[i].get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY)+"") %><br/>
	  			运费流程:<%=transportStockInSetKey.getStatusById(rows[i].get("stock_in_set",TransportStockInSetKey.SHIPPINGFEE_NOTSET)) %><br/>
	  			图片流程:<%=transportProductFileKey.getStatusById(rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE)) %><br />
		  	</td>
		  	<td>
  				<%=rows[i].getString("updatedate").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("updatedate")))%>
  			</td>
	  	</tr>
	  	<tr class="split">
  			<td colspan="7" style="text-align:right;padding-right:20px;">
  				
<%--  				&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="transportLogs(<%=rows[i].get("transport_id",0l)%>)" value="日志"/> --%>
  				<%
  					if(rows[i].get("transport_status",0)==TransportOrderKey.READY)
  					{
  				%> 
  					&nbsp;&nbsp; 
  					&nbsp;&nbsp; 
  				<%
  					}
  				%>
	  	  		<% if(rows[i].get("product_file",TransportProductFileKey.NOPRODUCTFILE) != TransportProductFileKey.NOPRODUCTFILE){ %>
	  	  			&nbsp;&nbsp;<input  type="button" class="long-button" value="商品图片" onclick="product_file(<%=rows[i].get("transport_id",0l)%>)"/>
	  	  		<%} %>
	  	  			<!--  <input type="button" class="long-button" value="相关文件" onclick="uploadFile(<%=rows[i].get("transport_id",0) %>,<%=fileWithTypeKey.transport %>)"/>-->
  				<%
  				 	if(rows[i].get("declaration",1) != declarationKey.NODELARATION ){  				 	//有报关 
  				%>
  				 		&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="declarationButton(<%=rows[i].get("transport_id",0l)%>)" value="报关"/> 	
  				<%
  					}
  				%>
	  	  		<%
  				 	if(rows[i].get("clearance",1) != clearanceKey.NOCLEARANCE ){// 有清关
  					%>
  					&nbsp;&nbsp;<input class="short-short-button-merge" type="button" onclick="clearanceButton(<%=rows[i].get("transport_id",0l)%>)" value="清关"/> 
  					<% 		
  				  	}
  				 %>
  				  <%if(rows[i].get("stock_in_set",1) != transportStockInSetKey.SHIPPINGFEE_NOTSET){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="运费" onclick="stock_in_set(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_STOCKINSET %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%if(rows[i].get("quality_inspection",1) != transportQualityInspectionKey.NO_NEED_QUALITY){%>
  				 	&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="质检" onclick="quality_inspectionKey(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_QUALITYINSPECTION %>)"/>
  				 <%	 
  					 }
  				 %>
  				 <%
  				 	if(rows[i].get("tag",TransportTagKey.NOTAG) != transportTagKey.NOTAG){
  				 		%>
					&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="制签" onclick="tag(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.TRANSPORT_TAG %>)"/>
  				 		<% 
  				 	}
  				 %>
	  	  		<%
	  	  			if(rows[i].get("certificate",1) !=  transportCertificateKey.NOCERTIFICATE){ //单证不等于 不需要显示
	  	  		%>
	  	  			&nbsp;&nbsp;<input type="button" class="short-short-button-merge" value="单证" onclick="transport_certificate(<%=rows[i].get("transport_id",0l) %>,<%=FileWithTypeKey.transport_certificate %>)"/>
	  	  		<%
	  	  			}
	  	  		%>
  			</td>
	  	</tr>
  	<%
  		}
  	%>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>

<form action="" method="get" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
	<input type="hidden" name="search_mode"/>
</form>

<form action="transport_order_index.html" method="post" name="filter_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="status"/>
	<input type="hidden" name="declarationStatus"/>
	<input type="hidden" name="clearanceStatus"/>
	<input type="hidden" name="drawbackStatus"/>
	<input type="hidden" name="invoiceStatus"/>
	<input type="hidden" name="stock_in_set"/>
	<input type="hidden" name="dept"/>
	<input type="hidden" name="create_account_id"/>	
	<input type="hidden" name="cmd" value="filter"/>
</form>

<form name="dataForm" method="post">
        <strong>
        <input type="hidden" name="p" />
        <input type="hidden" name="status" value="<%=status%>" />
        <input type="hidden" name="cmd" value="<%=cmd%>" /><input type="hidden" name="number" value="<%=key%>" />
        <input type="hidden" name="receive_psid" value="<%=receive_psid%>"/>
        <input type="hidden" name="send_psid" value="<%=send_psid%>"/>
        <input type="hidden" name="declarationStatus" value="<%=declaration%>"/>
		<input type="hidden" name="clearanceStatus" value="<%=clearance%>"/>
		<input type="hidden" name="drawbackStatus" value="<%=drawback%>"/>
		<input type="hidden" name="invoiceStatus" value="<%=invoice%>"/>
		<input type="hidden" name="st" value="<%=st%>"/>
	    <input type="hidden" name="en" value="<%=en%>"/>
	    <input type="hidden" name="analysisType" value="<%=analysisType%>"/>
	    <input type="hidden" name="analysisStatus" value="<%=analysisStatus%>"/>
	    <input type="hidden" name="day" value="<%=day%>"/>
	    <input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
	    <input type="hidden" name="dept" value="<%=dept %>"/>
		<input type="hidden" name="create_account_id" value="<%=create_account_id %>"/>	
		<input type="hidden" name="dept1" value="<%=dept1 %>"/>
		<input type="hidden" name="create_account_id1" value="<%=create_account_id1 %>"/>	
        </strong>
        
		
		
  </form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action" name="add_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/administrator_delivery_order_detail.html"/>
</form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="transport_id"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/approveTransportOutbound.action" method="post" name="applicationOutboundApprove_form">
	<input type="hidden" name="transport_id"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="transport_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="transport_type" value="1"/>
	<input type="hidden" name="transport_content"/>
</form>
<script type="text/javascript">
	
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps").getSelectedValue();
		document.filter_form.receive_psid.value = $("#receive_ps").getSelectedValue();
		document.filter_form.status.value = $("#transport_status").getSelectedValue();
		document.filter_form.declarationStatus.value = $("#declarationStatus").getSelectedValue();
		document.filter_form.clearanceStatus.value = $("#clearanceStatus").getSelectedValue();
		document.filter_form.invoiceStatus.value = $("#invoiceStatus").getSelectedValue();
		document.filter_form.drawbackStatus.value = $("#drawbackStatus").getSelectedValue();
		document.filter_form.stock_in_set.value = $("#stock_in_set").getSelectedValue();
		document.filter_form.dept.value = $("#dept").getSelectedValue();
		document.filter_form.create_account_id.value = $("#create_account_id").getSelectedValue();
		   
		document.filter_form.submit();
	}
	
	function delTransport(transport_id)
	{
		if(confirm("确定删除转运单T"+transport_id+"？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/delTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(transport_id)
	{
		if(confirm("确定申请T"+transport_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.transport_id.value = transport_id;
			document.applicationApprove_form.submit();
		}
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}

	function transportLogs(transport_id)
	{
		var uri = 'transport_logs.html?transport_id='+transport_id;
		$.artDialog.open(uri, {title: '日志 转运单号:'+transport_id,width:'800px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function addTransportOrder()
	{
		var uri = "add_transport.html";
		$.artDialog.open(uri, {title: '创建转运单',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function createDeliveryOrder(purchase_id)
	{
		document.add_deliveryOrder_form.purchase_id.value = purchase_id;
		document.add_deliveryOrder_form.submit();	
		tb_remove();
	}	
	
	
	
	function approveOutbound(transport_id)
	{
		document.applicationOutboundApprove_form.transport_id.value = transport_id;
		document.applicationOutboundApprove_form.submit();
	}
	
	
	function readyPacking(transport_id)
	{
		tb_show('转运缺货商品','transport_storage_show.html?transport_id='+transport_id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function reBackTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"停止装箱？（将按照转运商品回退库存）"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reBackTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function reStorageTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"中止运输？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reStorageTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>



