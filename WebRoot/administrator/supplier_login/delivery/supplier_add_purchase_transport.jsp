<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.exception.supplier.SupplierLoginTimeOutException"%>
<%@ include file="../../../include.jsp"%> 
<%
try
{
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	
	if(adminMgr.getAdminLoginBean(session).getAttach()==null)
	{
			throw new SupplierLoginTimeOutException();
	}
	long supplier_id = adminMgr.getAdminLoginBean(session).getAttach().get("id",0l);
	DBRow[] noFinishPurchases = purchaseMgr.getNoFinishPurchase(supplier_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购转运单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../../js/select.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function stateDiv(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div").css("display","inline");
		}
		else
		{
			$("#state_div").css("display","none");
		}
	}

function getAddress(purchase_id)
{
	if(purchase_id !=-1)
	{
		$.ajax({													
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getAllAddressJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{purchase_id:purchase_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#deliver_ccid").val(data["deliver_ccid"]);
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id");
					$("#deliver_name").val(data["deliver_name"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
					
					
					
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_name);
					$("#send_linkman_phone").val(data.send_phone);
					$("#send_city").val(data.send_city);
					$("#send_ccid").val(data["send_ccid"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
					
					$("#receive_psid").val(data.receive_psid);
					$("#send_psid").val(data.send_psid);
				}
			});
	}
}
	//国家地区切换
function getStorageProvinceByCcid(ccid,pro_id,id)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#"+id).clearAll();
					$("#"+id).addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#"+id).addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#"+id).addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#"+id).setSelectedValue(pro_id);
					stateDiv(pro_id);
				}
}

	function selectDeliveryStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{ps_id:ps_id},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					//$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.city);
					$("#deliver_ccid").val(data["deliver_nation"]);
					
					getStorageProvinceByCcid($("#deliver_ccid").val(),data["deliver_pro_id"],"deliver_pro_id");
					
					$("#deliver_name").val(data["deliver_contact"]);
					$("#deliver_linkman_phone").val(data["deliver_phone"]);
				}
			});
	}
	
	function selectSendStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_house_number").val(data["send_house_number"]);
					$("#send_street").val(data["send_street"]);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.send_zip_code);
					$("#send_name").val(data.send_contact);
					$("#send_linkman_phone").val(data.deliver_phone);
					$("#send_city").val(data.deliver_city);
					$("#send_ccid").val(data["send_nation"]);
					
					getStorageProvinceByCcid($("#send_ccid").val(),data["send_pro_id"],"send_pro_id");
				}
			});
	}
	
	function submitCheck()
	{
		if($("#receive_psid").val()==0)
		{
			alert("请选择目的仓库");
			
		}
		else if($("#deliver_ccid").val()==0)
		{
			alert("请选择目的国家");
			
		}
		else if($("#deliver_city").val()=="")
		{
			alert("请填写目的城市");
			$("#deliver_city").focus();
			
		}
		else if($("#deliver_house_number").val()==""||$("#deliver_street").val()=="")
		{
			alert("请填写目的地址");
		}
		else if($("#deliver_zip_code").val()=="")
		{
			alert("交货地址邮编");
			$("#deliver_zip_code").focus();
		}
		else if($("#deliver_name").val()=="")
		{
			alert("请填写交货联系人");
			$("#deliver_name").focus();
		}
		else if($("#psid").val()==0)
		{
			alert("请选择发货仓库");
		}
		else if($("#send_ccid").val()==0)
		{
			alert("请选择发货国家");
			
		}
		else if($("#send_city").val()=="")
		{
			alert("请填写发货城市");
			$("#send_city").focus();
		}
		else if($("#send_house_number").val()==""||$("#send_street").val()=="")
		{
			alert("请填写发货地址");
		}
		else if($("#send_zip_code").val()=="")
		{
			alert("请填写发货地邮编");
			$("#send_zip_code").focus();
		}
		else if($("#send_name").val()=="")
		{
			alert("请填写发货联系人");
			$("#send_name").focus();
		}
		else if($("purchase_id").val()==-1)
		{
			alert("请选择采购单");
		}
		else
		{
			parent.document.supplier_add_purchase_transport.purchase_id.value = <%=purchase_id%>;
			parent.document.supplier_add_purchase_transport.send_psid.value = $("#send_psid").val();
			parent.document.supplier_add_purchase_transport.receive_psid.value = $("#receive_psid").val();
			
			parent.document.supplier_add_purchase_transport.deliver_ccid.value = $("#deliver_ccid").val();
			parent.document.supplier_add_purchase_transport.deliver_pro_id.value = $("#deliver_pro_id").val();
			parent.document.supplier_add_purchase_transport.deliver_city.value = $("#deliver_city").val();
			parent.document.supplier_add_purchase_transport.deliver_house_number.value = $("#deliver_house_number").val();
			parent.document.supplier_add_purchase_transport.deliver_street.value = $("#deliver_street").val();
			parent.document.supplier_add_purchase_transport.deliver_zip_code.value = $("#deliver_zip_code").val();
			parent.document.supplier_add_purchase_transport.deliver_name.value = $("#deliver_name").val();
			parent.document.supplier_add_purchase_transport.deliver_linkman_phone.value = $("#deliver_linkman_phone").val();
			
			parent.document.supplier_add_purchase_transport.send_ccid.value = $("#send_ccid").val();
			parent.document.supplier_add_purchase_transport.send_pro_id.value = $("#send_pro_id").val();
			parent.document.supplier_add_purchase_transport.send_city.value = $("#send_city").val();
			parent.document.supplier_add_purchase_transport.send_house_number.value = $("#send_house_number").val();
			parent.document.supplier_add_purchase_transport.send_street.value = $("#send_street").val();
			parent.document.supplier_add_purchase_transport.send_zip_code.value = $("#send_zip_code").val();
			parent.document.supplier_add_purchase_transport.send_name.value = $("#send_name").val();
			parent.document.supplier_add_purchase_transport.send_linkman_phone.value = $("#send_linkman_phone").val();
			
			parent.document.supplier_add_purchase_transport.submit();
		}
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<form name="add_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/supplierAddTransportForPurchase.action">
	<input type="hidden" name="receive_psid" id="receive_psid"/>
	<input type="hidden" name="send_psid" id="send_psid"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="center" valign="top"><br>
            	  <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="left" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:5px;">
						<select id="purchase_id" name="purchase_id" onchange="getAddress(this.value)">
							<option value="-1">请选择采购单</option>
								<%
									for(int i = 0;i<noFinishPurchases.length;i++)
									{
								%>
									<option <%=purchase_id==noFinishPurchases[i].get("purchase_id",0l)?"selected=\"selected\"":""%> value="<%=noFinishPurchases[i].get("purchase_id",0l)%>"><%="P"+noFinishPurchases[i].getString("purchase_id")%></option>
								<%
									}
								%>
							</select>
						</td>
					  </tr>
				  </table>
				  <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title">
							收货地址	
						</span>
					</legend>
						 <table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr>
						 		<td align="right">目的国家</td>
						 		<td>
						 			<%
									DBRow countrycode[] = orderMgr.getAllCountryCode();
									String selectBg="#ffffff";
									String preLetter="";
									%>
							      <select name="deliver_ccid" id="deliver_ccid" onChange="getStorageProvinceByCcid(this.value,0,'deliver_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的省份</td>
						 		<td>
						 			<select name="deliver_pro_id" id="deliver_pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div">
											<input type="text" name="address_state" id="address_state"/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址门牌</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_house_number" id="deliver_house_number"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_street" id="deliver_street"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的城市</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_city" id="deliver_city"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" id="deliver_zip_code"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_name" id="deliver_name"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" id="deliver_linkman_phone"/></td>
						 	</tr>
						 </table>
						</fieldset>
						<fieldset class="set" style="padding-bottom:4px;text-align: left;">
			  				<legend>
								<span style="" class="title">
									提货地址	
								</span>
							</legend>
						<table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr>
						 		<td align="right">发货国家</td>
						 		<td>
							      <select name="send_ccid" id="send_ccid" onChange="getStorageProvinceByCcid(this.value,0,'send_pro_id');">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货省份</td>
						 		<td>
						 			<select name="send_pro_id" id="send_pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div">
											<input type="text" name="address_state" id="address_state"/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="send_house_number" id="send_house_number"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="send_street" id="send_street"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货城市</td>
						 		<td><input style="width: 300px;" type="text" name="send_city" id="send_city"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="send_zip_code" id="send_zip_code"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="send_name" id="send_name"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="send_linkman_phone" id="send_linkman_phone"/></td>
						 	</tr>
						 </table>
						</fieldset>
				</td>
			 </tr>
             <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				 	<input type="button" name="Submit2" value="下一步" class="normal-green" onclick="submitCheck()">
			
			    	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();"></td>
             </tr>
            </table>
      </form>
      <script type="text/javascript">
      	getAddress(<%=purchase_id%>);
      </script>
</body>
</html>
<%}
  catch(SupplierLoginTimeOutException e)
  {
  	out.print("登录超时请从新登陆！");
  }
%>