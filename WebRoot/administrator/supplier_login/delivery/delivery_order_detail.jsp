<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.TransportCertificateKey"%>
<%@page import="com.cwc.app.key.TransportQualityInspectionKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../../include.jsp"%> 
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<jsp:useBean id="transportStockInSetKey" class="com.cwc.app.key.TransportStockInSetKey"></jsp:useBean>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/> 
<% 
	long transport_id = StringUtil.getLong(request,"transport_id");
	//fileFlag
	String fileFlag= StringUtil.getString(request,"flag");
	DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
	DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(Long.toString(transport_id),6);//
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	
	int number = 0;
	boolean edit = false;
	
	//准备中的转运单,并且没有货款申请,转运单明细可修改
	if(transport.get("transport_status",0)==TransportOrderKey.READY&&applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,transport_id,FinanceApplyTypeKey.TRANSPORT_ORDER).length==0)
	{
		edit = true;
	}
	HashMap followuptype = new HashMap();
	followuptype.put(1,"跟进记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改转运单详细记录");
	followuptype.put(4,"货物状态");
	followuptype.put(5,"运费流程");
	followuptype.put(6,"进口清关");
	followuptype.put(7,"出口报关");
	followuptype.put(8,"制签流程");
	followuptype.put(9,"单证流程");
	followuptype.put(10,"商品图片流程");
	followuptype.put(11,"质检流程");
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
	String updateTransportCertificateAction =   ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportCertificateCompleteAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("transport_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
				String tempStr = arraySelected[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(transport_id,productFileTyps );//临时拷贝样式，使用方法为采购单，后续开发请注意
		if(imagesRows != null && imagesRows.length > 0 )
		{
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ )
			{
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1))
				{
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
			}
		}
		// 单证
		String valueCertificate = systemConfig.getStringConfigValue("transport_certificate");
	String[] arraySelectedCertificate = valueCertificate.split("\n");
	//将arraySelected组成一个List
	ArrayList<String> selectedListCertificate= new ArrayList<String>();
	for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
			String tempStr = arraySelectedCertificate[index];
			if(tempStr.indexOf("=") != -1){
				String[] tempArray = tempStr.split("=");
				String tempHtml = tempArray[1];
				selectedListCertificate.add(tempHtml);
			}
	}
	int file_with_type = StringUtil.getInt(request,"file_with_type");
	String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
	String file_with_class = StringUtil.getString(request,"file_with_class");
	DBRow[] imageListCertificate = fileMgrZJ.getFileByWithIdAndType(transport_id, file_with_type);
 	Map<String,List<DBRow>> mapCertificate = new HashMap<String,List<DBRow>>();
 	TransportCertificateKey certificateKey = new TransportCertificateKey();
	if(imageListCertificate != null && imageListCertificate.length > 0){
		//map<string,List<DBRow>>
		for(int index = 0 , count = imageListCertificate.length ; index < count ;index++ ){
			DBRow temp = imageListCertificate[index];
			List<DBRow> tempListRow = mapCertificate.get(temp.getString("file_with_class"));
			if(tempListRow == null){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(temp);
			mapCertificate.put(temp.getString("file_with_class"),tempListRow);
		}
	}
	// 商品图片
	String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
	String valueProductFile = systemConfig.getStringConfigValue("transport_product_file");
	String[] arrayProductFileSelected = valueProductFile.split("\n");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单<%=transport.getString("transport_number")%></title>

<link href="../../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../../common.js"></script>

<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.widget.js"></script>

<script type="text/javascript" src="../../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../../js/autocomplete/jquery.ui.tabs.js"></script>
	
<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>

<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/select.js"></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.js'></script>

<script type='text/javascript' src='../../js/autocomplete/jquery.ui.autocomplete.html.js'></script>

<script type='text/javascript' src='../../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />



<script type="text/javascript" src="../../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>

<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>

	
<script type="text/javascript" src="../../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>
<style type="text/css">
	body{text-align:left;}
</style>
<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../../js/art/skins/aero.css" />
<script src="../../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="../../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>
<link type="text/css" href="../../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>

<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function print()
	{
		visionariPrinter.PRINT_INIT("转运单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../../transport/print_transport_order_detail.html?transport_id=<%=transport_id%>");
				
			visionariPrinter.PREVIEW();
	}
		
		function searchPurchaseDetail()
		{
			if($("#search_key").val().trim()=="")
			{
				alert("请输入要关键字")
			}
			else
			{
				document.search_form.purchase_name.value=$("#search_key").val();
				document.search_form.submit();
			}	
		}
	
	function uploadTransportOrderDetail()
	{
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_upload_excel.html?transport_id=<%=transport_id%>';
		$.artDialog.open(url, {title: '上传转运单',width:'800px',height:'500px', lock: true,opacity: 0.3});
		//tb_show('上传转运单','transport_upload_excel.html?transport_id=<%=transport_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function virtualDeliveryOrder(filename,purchase_id)
	{
		document.virtual_transport_form.filename.value = filename;
		document.virtual_transport_form.purchase_id.value = purchase_id;
		
		document.virtual_transport_form.submit();
		
		tb_remove();
		
	}
	
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
	
	function downloadTransportOrder(transport_id)
	{
		var para = "transport_id="+transport_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/downTransportOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function saveDeliveryOrder()
	{
			if(confirm("确定保存对此交货单的修改吗？"))
			{
				jQuery("#gridtest").jqGrid('saveCell',select_iRow,select_iCol);
				<%
					if(transport_id !=0)
					{
				%>
						document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrder.action";
				<%	
					}
					else
					{
				%>
					
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addDeliveryOrder.action";
				<%	
					}
				%>
				
				document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
				document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
				document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
				document.save_deliveryOrder_form.submit();
			}
	}
	
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",

				

			 		  
			
		
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function refreshWindow(){
		window.location.reload();
	}
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="transport_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/getTransportDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function showStockOut(transport_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_out.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '转运单出库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function showStockIn(transport_id) {
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_stock_temp_in.html?transport_id='+transport_id;
		$.artDialog.open(url, {title: '转运单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	
	function createWaybill(transport_id)
	{
		
		$.artDialog.open("transport_waybill.html?transport_id="+transport_id, {title: '转运单创建运单',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function uploadInvoice(transport_id)
	{
		$.artDialog.open("transport_upload_invoice.html?transport_id="+transport_id, {title: '转运单上传商业发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function printWayBill(id,print_page)
	{
		 
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?id="+id+"&type=T";
		
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
   	}
	function changeType(obj) {
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}
	}
	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 return true;
		}
	}
	function followup(transport_id,declaration,clearance)
	{
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)) {
			var text = "<div id='title'>转运单跟进[单号:"+transport_id+"]</div><br />";
			text += "跟进范围:<select id='type' name='type' onchange='changeType(this)'>";
			text += declaration>1&&declaration!=4?"<option value='2'>出口报关</option>":"";
			text += clearance>1&&clearance!=4?"<option value='1'>进口清关</option>":"";
			text += "</select>&nbsp;&nbsp;";
			text += "跟进阶段:";
			var b = false;		
			b1 = declaration>1&&declaration!=4;
	  		text += "<select name='declaration' id='declaration' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses2 = declarationKey.getStatuses();
	  			for(int i=2;i<statuses2.size();i++) {
	  				int statuse = Integer.parseInt(statuses2.get(i).toString());
	  				String key1 = declarationKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%
	  			}
	  		%>
	  		text += "</select>";
			b = b?b:b1;  		
			b1 = clearance>1&&clearance!=4;
	  		text += "<select name='clearance' id='clearance' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses3 = clearanceKey.getStatuses();
	  			for(int i=2;i<statuses3.size();i++) {
	  				int statuse = Integer.parseInt(statuses3.get(i).toString());
	  				String key1 = clearanceKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key1%></option>";
			<%
	  			}
	  		%>
	  		text += "</select>";
			b = b?b:b1;
			text += '<br/>ETA:<input type="text" value="" id="eta" name="eta" />';
			$('#eta').datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true,
				onSelect:function(dateText, inst){
					var content = $("#content").val();
					 if(content.indexOf("预计时间") != -1){
						 content = content.replace("预计时间","").substr(11);
					}
					 $("#content").val("预计时间" + dateText + ":"+content);
				}
			});
			text += "<br/>备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
			$.prompt(text,
			{
				  submit:promptCheck,
		   		  loaded:
						function ()
						{
						}
				  ,
				  callback: 
						function (v,m,f)
						{
							if (v=="y")
							{
									document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/transportLogsInsertAction.action"
									document.followup_form.transport_id.value = transport_id;
									document.followup_form.type.value = f.type;
									if(f.type==1) {
										document.followup_form.stage.value = f.clearance;
									}else if(f.type==2) {
										document.followup_form.stage.value = f.declaration;
									}
									document.followup_form.transport_content.value = f.content;
									document.followup_form.transport_type.value = 1;
									document.followup_form.expect_date.value = f.eta;
									document.followup_form.submit();	
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 提交: "y", 取消: "n" }
			});
		}else {
			alert("没有跟进项目");
		}
	}
	function followup_clearance_declaration(transport_id,declaration,clearance){
		if((declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)){
		 		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport_log/transport_log_declaration_clear_add.html"; 
				uri += "?transport_id="+transport_id+"&declaration="+declaration+"&clearance="+clearance;
	 		$.artDialog.open(uri , {title: "转运单跟进["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
		}else{
			alert("无跟进项目!");
		}
	}
	function followupTransport(transport_id)
	{
	 	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/transport_log/transport_log_declaration_clear_add.html?transport_id="+transport_id;
		 $.artDialog.open(uri , {title: "日志跟进["+transport_id+"]",width:'450px',height:'320px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function printLabel()
	{
		//如果不是0就是交货型转运单
		var purchaseId = <%=transport.get("purchase_id",0l)%>;
		<%
			if(transport.get("purchase_id",0l)==0)
			{
		%>
               
             var supplierSid =<%=transport.get("send_psid",0l) %>;
             var transport_id=<%=transport_id%>;
             var warehouse="<%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%>";
         	 var uri = "../../lable_template/made_tranoport_internal_label.html?warehouse="+warehouse+"&transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+""+"&supplierSid="+supplierSid; 
 			 $.artDialog.open(uri , {title: '转运单内部标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});		
        <%	}
        	else
        	{
        %>
            var supplierName="<%=supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name")%>";
            var supplierSid =<%=transport.get("send_psid",0l) %>;
            var transport_id=<%=transport_id%>;
        	var uri = "../../lable_template/made_tranoport_label.html?transport_id="+transport_id+"&purchase_id="+purchaseId+"&supplierName="+supplierName+"&supplierSid="+supplierSid; 
			$.artDialog.open(uri , {title: '转运单交货型标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
        <%
        	}	
		%>
	}
</script>



<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
.set{
	border:2px #999999 solid;
	padding:2px;
	width:90%;
	word-break:break-all;
	margin-top:10px;
	margin-top:5px;
	line-height:18px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	margin-bottom: 10px;
}
	.create_order_button{background-attachment: fixed;background: url(../../imgs/create_order.jpg);background-repeat: no-repeat;background-position: center center;height: 51px;width: 129px;color: #000000;border: 0px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	.zebraTable td {line-height:25px;height:25px;}
	.right-title{line-height:20px;height:20px;}
</style>
<script>
function openApplyMoneyInsert() {
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_transport_insert.html?associationId=<%=transport.getString("transport_id")%>";

	$.artDialog.open(uri, {title: '申请费用',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showTransit()
{
	//var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_transit_update.html?transport_id=<%=transport.getString("transport_id")%>";
	<%
		String uri = "";
		if(transport.get("purchase_id",0l)==0)
		{
			uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id");
		}
		else
		{
			uri = uri = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_purchase_basic_update.html?isOutter=2&transport_id="+transport.getString("transport_id"); 
		}
	%>
	$.artDialog.open('<%=uri%>', {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function updateTransportAllProdures(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?isOutter=2&transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '修改各流程信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreight(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_freight_update.html?transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showFreightCost(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/setTransportFreight.html?transport_id=<%=transport.getString("transport_id")%>&fr_id=<%=transport.getString("fr_id")%>";
	$.artDialog.open(uri, {title: '修改运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showWayout(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transport_wayout_update.html?transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showSetDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/transport/transportSetDrawback.html?transport_id=<%=transport.getString("transport_id")%>";
	$.artDialog.open(uri, {title: '交货单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
}

function applyFreigth(amount) {
	var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_money_transport_insert.html?associationId=<%=transport_id%>&amount="+amount+"&subject=1";
	$.artDialog.open(url, {title: '申请运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function fileWithClassCertifycateChange(){
	var file_with_class = $("#file_with_class_certificate").val();
   	$("#tabsCertificate").tabs( "select" , file_with_class * 1-1 );
}
function deleteFileCommon(file_id , tableName , folder,pk){
	$.ajax({
		url:'<%= deleteFileAction%>',
		dataType:'json',
		data:{table_name:tableName,file_id:file_id,folder:folder,pk:pk},
		success:function(data){
			if(data && data.flag === "success"){
				window.location.reload();
			}else{
				showMessage("系统错误,请稍后重试","error");
			}
		},
		error:function(){
			showMessage("系统错误,请稍后重试","error");
				}
})
}
function uploadCertificateImage(){
	if($('#voucherCertificate').val() == ""){
		alert("请输入上传文件!");
	}else {
		$('#uploadCertificateImageForm').submit();
	}
}
jQuery(function($){
	if($.trim('<%= fileFlag%>') === "1"){
		showMessage("上传文件出错","error");
				}
	if($.trim('<%= fileFlag%>') === "2"){
		showMessage("请选择正确的文件类型","error");
	}
})
function cerficateCompelte(){
	$.artDialog.confirm('单证流程完成后文件只能上传不能删除,确认继续吗？', function(){
		$.ajax({
			url:'<%= updateTransportCertificateAction%>',
			data:{transport_id:'<%= transport_id%>',transport_date:'<%= transport.getString("transport_date")%>'},
			dataType:'json',
			success:function(data){
				if(data && data.flag == "success"){
					window.location.reload();
				}else{showMessage("系统错误,请稍后重试","alert");}
			},
			error:function(){
			  showMessage("系统错误,请稍后重试","alert");
				}
		})
	}, function(){
	});
}
function productFileCompelte(){
	var isFlag =  window.confirm("所有商品相关文件都上传了吗？");
	 if(isFlag){
		 $.ajax({
	    			url:'<%= updateTransportAction%>',
	    			data:{transport_id:'<%= transport_id%>',transport_date:'<%= transport.getString("transport_date")%>'},
	    			dataType:'json',
	    			success:function(data){
	    				if(data && data.flag == "success"){
	    					window.location.reload();
	    				}else{showMessage("系统错误,请稍后重试","alert");}
	    			},
	    			error:function(){
	    			  showMessage("系统错误,请稍后重试","alert");
				}
	    		})
		   }
}
function followUpCerficate(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_certificate_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "单证跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function productFileFollowUp(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_file_follow_up.html?transport_id="+<%= transport_id%>;
	$.artDialog.open(uri , {title: "商品图片跟进["+<%= transport_id%>+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});	
}
function showSingleLogs(transport_id,transport_type){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_show_single_log.html?transport_id="+<%= transport_id%>+"&transport_type="+transport_type;
	$.artDialog.open(uri , {title: "商品图片日志["+<%= transport_id%>+"]",width:'870px',height:'470px', lock: true,opacity: 0.3,fixed: true});	
}
function clearanceButton(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_clearance.html?transport_id="+transport_id; 
	$.artDialog.open(uri , {title: "清关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
		  	  			}
function declarationButton(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_declaration.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "报关流程["+transport_id+"]",width:'570px',height:'270px', lock: true,opacity: 0.3,fixed: true});
}
function quality_inspectionKey(transport_id){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_quality_inspection.html?transport_id="+transport_id;
	$.artDialog.open(uri , {title: "质检流程["+transport_id+"]",width:'570px',height:'300px', lock: true,opacity: 0.3,fixed: true});
		  	  		}
</script>
</head>
<body onload="onLoadInitZebraTable();">
<div class="demo">
<div id="transportTabs">
	<ul>
		<li><a href="#transport_basic_info">基础信息<span> </span></a></li>
		<li><a href="#transport_info">运单信息<span> </span></a></li>
		<%if(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET) != transportStockInSetKey.SHIPPINGFEE_NOTSET) {%>
		<li><a href="#transport_price_info">运费信息<span> </span></a></li>
		<%} %>
	 	<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION) {%>
			<li><a href="#transport_out_info">出口报关<span> </span></a></li>
		<%} %>
		<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE) {%>
			<li><a href="#transport_in_info">进口清关<span> </span></a></li>
		<%} %>
		<%if(transport.get("tag",transportTagKey.NOTAG) != transportTagKey.NOTAG) {%>
		<li><a href="#transport_tag_make">标签制作<span> </span></a></li>
		<%} %>
 		<%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {%>
			<li><a href="#transport_order_prove">单证信息<span> </span></a></li>
		<%} %>
		<%if(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE) {%>
			<li><a href="#product_file">商品图片<span> </span></a></li>
		<%} %>
		<%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY) {%>
			<li><a href="#quality_inspection">质检<span> </span></a></li>
		<%} %>
	</ul>
	<div id="transport_basic_info">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td colspan="2">
					<table width="100%">
						<tr>
							<td width="7%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">转运单号：</font></td>
							<td align="left" width="10%">T<%=transport_id%></td>
							<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">预计到达日期：</font></td>
							<td align="left" width="10%"><%=transport.getString("transport_receive_date").equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():transport.getString("transport_receive_date")%>
					</td>
							<td width="10%" align="right"height="25"><font style="font-family: 黑体; font-size: 14px;">创建人：</font></td>
							<td align="left" width="10%"><%=transport.getString("create_account")%></td>
							<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">创建时间：</font></td>
							<td align="left" width="13%"><%=transport.getString("transport_date")%></td>
							<td align="right" width="10%" height="25"><font style="font-family: 黑体; font-size: 14px;">允许装箱：</font></td>
							<td align="left" width="10%"><%=transport.getString("packing_account")%></td>
				</tr>
				<tr>
							<td width="7%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货物状态：</font></td>
							<td align="left" width="10%"><%=transportOrderKey.getTransportOrderStatusById(transport.get("transport_status",0)) %></td>
							<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">出口报关：</font></td>
							<td align="left" width="10%"><%=declarationKey.getStatusById(transport.get("declaration",01)) %></td>
							<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">进口清关：</font></td>
							<td align="left" width="10%"><%=clearanceKey.getStatusById(transport.get("clearance",01)) %></td>
							<td width="13%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运费状态：</font></td>
							<td align="left" width="10%"><%=transportStockInSetKey.getStatusById(transport.get("stock_in_set",0)) %></td>
							<td width="10%"></td><td width="10%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="2" height="15px"></td></tr>
			<tr>
				<td style="width:7%;text-align:right;" align="right">
					<font style="font-family: 黑体; font-size: 14px;">&nbsp;</font>
				</td>
				<td style="width:93%;">
					<table style="width:100%;" class="addr_table">
						<tr>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">提货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;">
						<%
							if(transport.get("purchase_id",0l)==0)
							{
								out.print(catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"));
							}
							else
							{
								out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
							}
						%>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_street"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_city"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ps_id = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow = productMgr.getDetailProvinceByProId(ps_id);
						%>
											<%=(psIdRow != null? psIdRow.getString("pro_name"):transport.getString("send_pro_input") ) %>
					</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow = orderMgr.getDetailCountryCodeByCcid(ccid);
											%>
											<%= (ccidRow != null?ccidRow.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_name"):"" %></td>
				</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
									</tr>
			</table>
							</td>
							<td style="width:10%;text-align: center;font-size: 18px;">
								-->
							
							
							  	  		
							</td>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;" class="addr_table">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">收货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l))?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_house_number") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_street") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_city") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_zip_code") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
			<% 
												long ps_id1 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow1 = productMgr.getDetailProvinceByProId(ps_id1);
											%>
											<%=(psIdRow1 != null? psIdRow1.getString("pro_name"):transport.getString("deliver_pro_input") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid1 = null!=transport?transport.get("deliver_ccid",0l):0L; 
												DBRow ccidRow1 = orderMgr.getDetailCountryCodeByCcid(ccid1);
			%>
											<%= (ccidRow1 != null?ccidRow1.getString("c_country"):"" ) %>	
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman")%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman_phone")%></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tr>
			<tr>
				<td colspan="2" align="left"><br/>
					<input name="button" type="button" class="long-button" value="明细修改" onClick="showTransit()"/>
					<%
						if(transport.get("transport_status",0)==TransportOrderKey.READY)
						{
					%>
					<input  type="button" class="long-button-upload" value="上传转运单" onclick="uploadTransportOrderDetail()"/>
					<% 
						}
					%>
					<input  type="button" class="long-button-next" value="下载转运单" onclick="downloadTransportOrder(<%=transport_id%>)"/>
			  		<input type="button" class="long-button-print" value="打印转运单" onclick="print()"/>
				</td>
			</tr>
		</table>
	</div>
	<div id="transport_info">
		<table border="0" width="100%">
					<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">货运公司：</font></td>
			  	<td width="40%" align="left" height="25"><%=transport.getString("transport_waybill_name") %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运单号：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_waybill_number") %></td>
					</tr>
					<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">运输方式：</font></td>
			  	<td width="40%" align="left" height="25"><%=transportWayKey.getStatusById(transport.get("transportby",0)) %></td>
			  	<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">承运公司：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("carriers") %></td>
					</tr>
					<tr>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">发货港：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_send_place")%></td>
				<td width="10%" align="right" height="25"><font style="font-family: 黑体; font-size: 14px;">目的港：</font></td>
				<td width="40%" align="left" height="25"><%=transport.getString("transport_receive_place")%></td>
			</tr>
			<tr><td colspan="4" height="15"></td></tr>
			<tr>
				<td style="width:10%;text-align:right;">
					<font style="font-family: 黑体; font-size: 14px;">&nbsp;</font>
				</td>
				<td style="width:90%;" colspan="3">
					<table style="width:100%;" class="addr_table">
						<tr>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">提货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												if(transport.get("purchase_id",0l)==0)
												{
													out.print(catalogMgr.getDetailProductStorageCatalogById(transport.get("send_psid",0l)).getString("title"));
												}
												else
												{
													out.print(supplierMgrTJH.getDetailSupplier(transport.get("send_psid",0l)).getString("sup_name"));
												}
											%>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_house_number"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_street"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_city"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_zip_code"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
						<%
												long ps_id3 = null!=transport?transport.get("send_pro_id",0L):0L ;
												DBRow psIdRow3 = productMgr.getDetailProvinceByProId(ps_id3);
											%>
											<%=(psIdRow3 != null? psIdRow3.getString("pro_name"):transport.getString("send_pro_input") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid3 = null!=transport?transport.get("send_ccid",0L):0L; 
												DBRow ccidRow3 = orderMgr.getDetailCountryCodeByCcid(ccid3);
						%>
											<%= (ccidRow3 != null?ccidRow3.getString("c_country"):"" ) %>	
						</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_name"):"" %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=transport?transport.getString("send_linkman_phone"):"" %></td>
									</tr>
								</table>
							</td>
							<td style="width:10%;text-align: center;font-size: 18px;">
								-->
							</td>
							<td style="width:45%;">
								<table style="width:100%;border-collapse:collapse ;" class="addr_table">
									<tr>
										<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">收货仓库：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=null!=catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title")?catalogMgr.getDetailProductStorageCatalogById(transport.get("receive_psid",0l)).getString("title"):""%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_house_number") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_street") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_city") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("deliver_zip_code") %></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
										<td style="border: 1px solid silver;width: 80%;">
						<%
												long ps_id4 = null!=transport?transport.get("deliver_pro_id",0l):0L;
												DBRow psIdRow4 = productMgr.getDetailProvinceByProId(ps_id4);
											%>
											<%=(psIdRow4 != null? psIdRow4.getString("pro_name"):transport.getString("deliver_pro_input") ) %>
										</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
										<td style="border: 1px solid silver;width: 80%;">
											<%
												long ccid4 = null!=transport?transport.get("deliver_ccid",0l):0L; 
												DBRow ccidRow4 = orderMgr.getDetailCountryCodeByCcid(ccid4);
						%>
											<%= (ccidRow4 != null?ccidRow4.getString("c_country"):"" ) %>	
						</td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman")%></td>
									</tr>
									<tr>
										<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
										<td style="border: 1px solid silver;width: 80%;"><%=transport.getString("transport_linkman_phone")%></td>
					</tr>
				</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table><br/>
		<table width="100%">
			<tr>
				<td align="left"">
					<input name="button" type="button" value="运单修改" onClick="showFreight()" class="long-button"/>
					<input  type="button" class="long-button-upload" value="上传商业发票" onclick="uploadInvoice(<%=transport_id %>)"/>
				<%
				if(transport.get("transport_status",0)==TransportOrderKey.READY&&transport.get("purchase_id",0l)==0)
				{
			%>
<%--								&nbsp;&nbsp;<a href="#" onClick="readyPacking(<%=transport_id%>,'<%=transport.getString("transport_number")%>')">装箱</a>--%>
								<input name="button" type="button" class="long-button" value="装箱" onClick="readyPacking(<%=transport_id%>)"/>
			<% 
				}
			%>
			  <% 
				if(transport.get("transport_status",0)==TransportOrderKey.PACKING||(transport.get("purchase_id",0l)!=0l&&transport.get("transport_status",0)==TransportOrderKey.READY))
				{
			  %>
<%--						  		&nbsp;&nbsp;<a href="#" onClick="showStockOut(<%=transport_id%>)">转运单出库</a>--%>
						  		<input name="button" type="button" class="long-button" value="转运单出库" onClick="showStockOut(<%=transport_id%>)"/>
			  <%
				}
			  %>
			  <% 
				if((transport.get("transport_status",0)==TransportOrderKey.INTRANSIT) && transport.get("stock_in_set",0)==2)
				{
			  %>
<%--						  		&nbsp;&nbsp;<a href="#" onClick="showStockIn(<%=transport_id%>)">转运单入库</a>--%>
						  		<input name="button" type="button" class="long-button" value="转运单入库" onClick="showStockIn(<%=transport_id%>)"/>
			  <%
				}
			  %>
			<%
  					if(transport.get("transport_status",0)==TransportOrderKey.INTRANSIT)
  					{
  			%>
<%--									&nbsp;&nbsp;<a href="#" onClick="createWaybill(<%=transport.get("transport_id",0l)%>)">生成快递单</a>--%>
									<input name="button" type="button" value="生成快递单" onClick="createWaybill(<%=transport.get("transport_id",0l)%>)" class="long-button"/>
			 <%
		  	  			DBRow shippingCompany = expressMgr.getDetailCompany(transport.get("sc_id",0l));
		  	  			if(shippingCompany !=null)
		  	  			{
		  	 %>
<%--					  	  				&nbsp;&nbsp;<a href="#" onClick="printWayBill(<%=transport.get("transport_id",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')">打印快递单</a>--%>
					  	  				<input type="button" class="long-button-print" value="打印快递单" onclick="printWayBill(<%=transport.get("transport_id",0l)%>,'<%=shippingCompany.getString("internal_print_page")%>')"/>
		  	 <%	
		  	  		}
							}
						%>
					</td>
				</tr>
			</table>
	</div>
 <%if(transport.get("stock_in_set",transportStockInSetKey.SHIPPINGFEE_NOTSET) != transportStockInSetKey.SHIPPINGFEE_NOTSET) {%>
	<div id="transport_price_info">
						<%
					DBRow[] rows = freightMgrLL.getTransportFreightCostByTransportId(transport.getString("transport_id"));//更新数据
					DBRow transportRow = transportMgrLL.getTransportById(transport.getString("transport_id"));;
				%>
		<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" id="stat_table">
			     <tr>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
			       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>	
			     </tr>
			     <%
		    	 float sum_price = 0;
			     if(rows!=null){
				     for(int i = 0;i<rows.length;i++)
				     {
				    	 DBRow row = rows[i];
				    	 long tfc_id = row.get("tfc_id",0l);
			    		 double tfc_unit_count = row.get("tfc_unit_count",0d);
			    		 String tfc_project_name = "";
			    		 String tfc_way = "";
			    		 String tfc_unit = "";
			    		 String tfc_currency = "";
			    		 double tfc_unit_price = 0;
			    		 double tfc_exchange_rate = 0;
				    	 if(tfc_id != 0) {//以前有数据
				    		 tfc_project_name = row.getString("tfc_project_name");
				    		 tfc_way = row.getString("tfc_way");
				    		 tfc_unit = row.getString("tfc_unit");
				    		 tfc_unit_price = row.get("tfc_unit_price",0d);
				    		 tfc_currency = row.getString("tfc_currency");
				    		 tfc_exchange_rate = row.get("tfc_exchange_rate",0d);
				    	 }
		    	 String bgCol = "#FFFFFF";
     			if(1 == i%2){
     				bgCol = "#F9F9F9";
     			}
			      %>
	     <tr height="30px" style="padding-top:3px;background-color: <%=bgCol %>">
			        <td valign="middle" style="padding-top:3px;">
			        	<input type="hidden" name="tfc_id" id="tfc_id" value="<%=tfc_id %>"/>
			        	<input type="hidden" name="tfc_project_name" id="tfc_project_name" value="<%=tfc_project_name %>"/>
			        	<input type="hidden" name="tfc_way" id="tfc_way" value="<%=tfc_way %>"/>
			        	<input type="hidden" name="tfc_unit" id="tfc_unit" value="<%=tfc_unit %>"/>
			        	
						<%=tfc_project_name %>	
			        </td>
			         <td valign="middle" nowrap="nowrap" >       
			      		<%=tfc_way %>
			        </td>
			        <td align="left" valign="middle">
						<%=tfc_unit %>
			        </td>
			        <td >
						<%=tfc_unit_price %>
			        </td>
			        <td >
						<%=tfc_currency%>
			        </td>
			        <td >
						<%=tfc_exchange_rate %>
			        </td>
			        <td valign="middle" >       
						<%=tfc_unit_count %>
			        </td>
			        <td valign="middle" nowrap="nowrap">       
					<%=MoneyUtil.round(tfc_unit_price*tfc_exchange_rate*tfc_unit_count,2)%>
		        </td>
			     </tr>
			     <%
			     		sum_price += tfc_unit_price*tfc_exchange_rate*tfc_unit_count;
			    	 }
			     %>
				 <tr height="30px">
				 	<td colspan="2">
		 		<fieldset class="set" style="border-color:#993300;">
					<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
						<span style="font-size:12px;color:#000000;font-weight:normal;">运费:<%=computeProductMgrCCC.getSumTransportFreightCost(transport.get("transport_id",0l)+"").get("sum_price",0d) %>RMB</span>
					</legend>
					<table style="width:100%">
				 	<%
				 		for(int j=0; j<applyMoneys.length; j++){
				 			out.println("<a href=\"javascript:void(0)\" onClick=\"goApplyFundsPage('"+applyMoneys[j].get("apply_id",0)+"')\">F"+applyMoneys[j].get("apply_id",0)+"</a>申请资金"+applyMoneys[j].get("amount",0f)+applyMoneys[j].getString("currency")+"<br/>");
				 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByBusiness(Long.toString(transport_id),6);
				 			if(applyTransferRows.length>0) {
				 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
				 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
						  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
						  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
						  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
						  				int moneyStatus = applyTransferRows[ii].get("status",0);
						  				if(moneyStatus != 0 )
						  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+"<br/>");
						  				else
						  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+"<br/>");
				 					}
					  			}
				 			}
				 		}
				 	%>
					</table>
				  </fieldset>
				 	</td>
				 	<td colspan="5" align="right">总计</td>
				 	<td nowrap="nowrap"><%=sum_price %></td>
				 </tr>
				 <%
			     }
			     %>
	     </table><br/>
	     <table style="border: 0; width: 98%">
			 <tr>
	     		<td colspan="10" align="left" style="border: 0">
				
	     			<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button"/>
		     	<%
		     		if(transport.get("stock_in_set",1) == 2) {
		     	%>
			        	<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=sum_price %>')" class="long-button"/>
		     	<%
		     		}
		     	%>
	     		</td>
	     	</tr>
	 </table>
    </div>
<%} %>	
<%if(transport.get("declaration",declarationKey.NODELARATION) != declarationKey.NODELARATION) {%>
	<div id="transport_out_info">
		<p style="margin-bottom:5px;">
			 <input class="short-short-button-merge" type="button" onclick='declarationButton(<%=transport_id%>)' value="跟进"/>
		</p>
		<table  width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] transport_type = {7};
				DBRow [] transportRows = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type);
		     	if(null != transportRows && transportRows.length > 0){
		     		for(int i = transportRows.length - 1; i >=0 ; i--){
		     			DBRow transRow = transportRows[i];
		     %>			
		     			 <tr height="30px">
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%=transRow.getString("transport_date") %></td>
		  					 <td><%=followuptype.get(transRow.get("transport_type",0))%></td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;"> 无数据 </td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
<%if(transport.get("clearance",clearanceKey.NOCLEARANCE) != clearanceKey.NOCLEARANCE) {%>
	<div id="transport_in_info">
	<p style="margin-bottom:5px;">
				<input class="short-short-button-merge" type="button" onclick='clearanceButton(<%=transport_id%>)' value="跟进"/>
			</p>
		<table id="tableLog" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable"  >
			<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] transport_type_clearance = {6};
				DBRow [] transportRowsClearance = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type_clearance);
		     	if(null != transportRowsClearance && transportRowsClearance.length > 0){
		     		for(int i =( transportRowsClearance.length-1); i >=0 ; i-- ){
		     			DBRow transRow = transportRowsClearance[i];
		     %>			
		     			 <tr height="30px"  >
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%=transRow.getString("transport_date") %></td>
		  					 <td><%=followuptype.get(transRow.get("transport_type",0))%></td>
				</tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>	
<%if(transport.get("tag",transportTagKey.NOTAG) != transportTagKey.NOTAG) {%>
	<div id="transport_tag_make">
		 <p style="margin-bottom:5px;">
			<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
			<input type="button" class="long-button-print" value="跟进制签" onclick="purchaseInvoiceState(8)"/>
	  	 </p>
		<div id="tagTabs" style="margin-top:15px;">
   		<ul>
		<%
				 		if(selectedList != null && selectedList.size() > 0){
				 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
		%>	
				 				<li><a href="#transport_product_tag_<%=index %>"> <%=selectedList.get(index) %></a></li>
				 			<% 	
				 			}
				 		}
			 		%>
	   		 	</ul>
	   		 	<%
	   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
	   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
	   		 			%>
	   		 			 <div id="transport_product_tag_<%= index%>">
	   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
	   		 			 			<tr> 
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
	   		 			 	<%
	   		 			 		if(tempListRows != null && tempListRows.size() > 0){
	   		 			 			 for(DBRow row : tempListRows){
	   		 			 			%>
	   		 			 				<tr style="height: 25px;">
	   		 			 					<td>
	   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%= row.getString("file_name") %>&folder=<%=systemConfig.getStringConfigValue("file_path_product")%>'><%= row.getString("file_name") %></a>
	   		 			 					</td>
	   		 			 					<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %></td>
	   		 			 					<td>
	   		 			 						<% if(!(transport.get("nee_tag",0) == TransportTagKey.FINISH && transport.get("need_tag",0.0d) > 0)){ %>
	   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
	   		 			 				 		<%} %>	
	   		 			 				 	</td>
	   		 			 				</tr>	
	   		 			 			<% 
	   		 			 		 }
	   		 			 		}else{
	   		 			 			%>
	 		 			 			<tr>
	 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 								</tr>
	   		 			 			<%
	   		 			 		}
	   		 			 	%>
	   		 			 	 	</table>
			</div>
		<%	
			} 
		%>
	</div>
	</div>
<%} %>	 
 <%if(transport.get("certificate",certificateKey.NOCERTIFICATE) != certificateKey.NOCERTIFICATE) {%>
	<div id="transport_order_prove">
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:-8px;">
		  <tr>
		    <td>
				 <fieldset style="border:2px #cccccc solid;padding:2px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
					<form name="uploadCertificateImageForm" id="uploadCertificateImageForm" value="uploadCertificateImageForm" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportCertificateUpFileAction.action" %>' enctype="multipart/form-data" method="post">	
						<input type="hidden" name="backurl" id="backurl" value="administrator/transport/transport_order_detail.html?transport_id=<%=transport_id %>&file_with_type=<%=file_with_type %>"/>
						<input type="hidden" name="file_with_id" value="<%=transport_id %>" />
						<input type="hidden" name="sn" id="sn" value="T"/>
						<input type="hidden" name="path" id="path" value="<%=systemConfig.getStringConfigValue("file_path_transport") %>"/>
						<input type="hidden" name="file_with_type" value="<%= file_with_type %>"/>
						<table width="90%" border="0">
						 	<td height="25" align="right"  class="STYLE2" nowrap="nowrap">上传文件:</td>
							  <td>&nbsp;</td>
						       <td colspan="3">
						        <input type="file" id="voucherCertificate" name="voucher" /><font style="color: red">*文件格式为:jpg,gif,bmp,jpeg,png</font>
						 	 		<input type="button" name="voucherSubmit" id="voucherSubmit" value="上传" onclick="uploadCertificateImage()" />
						 	   </td>
							 </tr>
				<tr>
							 	<td class="STYLE2" style="text-align:right;">文件分类:</td>
							 	<td>&nbsp;</td>
							 	<td colspan="2">
						        	<select name="file_with_class" id="file_with_class_certificate" onchange="fileWithClassCertifycateChange();">
		 								<%if(arraySelectedCertificate != null && arraySelectedCertificate.length > 0){
		 									for(int index = 0 , count = arraySelectedCertificate.length ; index < count ; index++ ){
		 									String tempStr = arraySelectedCertificate[index];
		 									String tempValue = "" ;
		 									String tempHtml = "" ;
		 									if(tempStr.indexOf("=") != -1){
		 										String[] tempArray = tempStr.split("=");
		 										tempValue = tempArray[0];
		 										tempHtml = tempArray[1];
		 									}
		 								%>		
		 									<option value="<%=tempValue %>"><%=tempHtml %></option>
		 								<%	} 
		 								}
		 								%>
						        	</select>
						        	单证流程阶段:<span style="color:green;"><%= certificateKey.getStatusById(transport.get("certificate",0)) %></span>
						      		<input type="button" class="short-button" value="单证跟进" onclick="followUpCerficate();"/>
	 								<input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',9)"/> 
					</td>
				</tr>
			</table>
					</form>
		</fieldset>
			</td>
		  </tr>
		</table>
			<div id="tabsCertificate" style="margin-top:10px;">
			 <ul>	
			 	<%
			 		if(selectedListCertificate != null && selectedListCertificate.size() > 0){
			 			for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
			 			%>
			 				<li><a href="#transport_certificate_<%=index %>"> <%=selectedListCertificate.get(index) %></a></li>
			 			<% 	
			 			}
			 		}
			 	%>
			 </ul>
			 	<!-- 遍历出Div 然后显示出来里面的 数据-->
			 	<%
					for(int index = 0 , count = selectedListCertificate.size() ; index < count ; index++ ){
						%>
							<div id="transport_certificate_<%=index %>">
			 					<%
			 						List<DBRow> arrayLisTemp = mapCertificate.get(""+(index+1));
			 						%>
			 					<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
			 						<tr> 
				  						<th width="75%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
			 						<% 
			 						if(arrayLisTemp != null && arrayLisTemp.size() > 0 ){
			 							for(int listIndex = 0 , total = arrayLisTemp.size() ; listIndex < total ; listIndex++ ){
			 							 %>
			 								<tr>
			 									<td>
			 						<a href='<%= downLoadFileAction%>?file_name=<%=arrayLisTemp.get(listIndex).getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_transport")%>'><%=arrayLisTemp.get(listIndex).getString("file_name") %></a>
			 									<td>
			 										<!--  如果是整个的单据流程已经结束那么就是不应该有删除的文件的按钮的 -->
			 										<%
			 											if(!(transport.get("certificate",0) == TransportCertificateKey.FINISH )){
			 												%>
			 												 <a href="javascript:deleteFileCommon('<%=arrayLisTemp.get(listIndex).get("file_id",0l)%>','file','<%=systemConfig.getStringConfigValue("file_path_transport") %>','file_id')">删除</a>
			 												<%  
			 											}
			 										%>	
	 		 									</td>
			 								</tr>
			 							 <%
			 							}	
			 						}else{
			 					 	%>
			 								<tr>
			 									<td colspan="2" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			 								</tr>
			 							<%
			 						}
			 					%>
			 					</table>	
							 </div>
						<%
					}
			 	%>
		</div>
	</div>
<%} %>	
 <%if(transport.get("product_file",transportProductFileKey.NOPRODUCTFILE) != transportProductFileKey.NOPRODUCTFILE) {%>
	<div id="product_file">
		<div style="padding-top:15px;padding-bottom:15px;padding-left:10px;">
			  图片流程阶段:<span style="color:green;"><%= transportProductFileKey.getStatusById(transport.get("product_file",0))%></span>
   		  <input type="button" value="图片跟进" class="short-button" onclick="productFileFollowUp();"/>
   		 <input type="button" value="查看日志" class="short-button" onclick="showSingleLogs('<%=transport_id %>',10)"/>
   			</div>		  
 			<div id="tabs">
  	            <ul>
					   <%if(arrayProductFileSelected != null && arrayProductFileSelected.length > 0){
 									for(int index = 0 , count = arrayProductFileSelected.length ; index < count ; index++ ){
 									String tempStr = arrayProductFileSelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 				  <li><a href="transport_product_file_tright.html?transport_id=<%=transport_id %>&product_file_type=<%=tempValue %>&file_type_name=<%=tempHtml %>"><%=tempHtml %></a></li>
 								<%	} 
 								}
 						%>
			    </ul>
			  </div>
	</div>
<%} %>
 <%if(transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY) != TransportQualityInspectionKey.NO_NEED_QUALITY) {%>
	<div id="quality_inspection">
		<p style="margin-bottom:5px;">
			 <input class="short-short-button-merge" type="button" onclick='quality_inspectionKey(<%=transport_id%>)' value="跟进"/>
		</p>
		<table  width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" >
				<tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] transport_type = {11};
				DBRow [] transportRows = transportMgrLL.getTransportLogsByTransportIdAndType(transport_id,transport_type);
		     	if(null != transportRows && transportRows.length > 0){
		     		for(int i = transportRows.length - 1; i >=0 ; i--){
		     			DBRow transRow = transportRows[i];
		     %>			
		     			 <tr height="30px">
					     	<td><%=null == adminMgrLL.getAdminById(transRow.getString("transporter_id"))?"":adminMgrLL.getAdminById(transRow.getString("transporter_id")).getString("employe_name") %></td>
					     	<td><%=transRow.getString("transport_content") %></td>
					     	<td><%=transRow.getString("time_complete") %></td>
					     	<td><%=transRow.getString("transport_date") %></td>
		  					 <td><%=followuptype.get(transRow.get("transport_type",0))%></td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" align="center" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;"> 无数据 </td>
		     	</tr>
		     <%		
		     	}
			%>
			</table>
	</div>
<%} %>
</div><br/>
<script>
	$("#transportTabs").tabs({
		cache: true,
		spinner: '<img src="../../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	 
	});
	$("#tagTabs").tabs({
		cache: true,
		spinner: '<img src="../../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	$("#tabsCertificate").tabs({
 		cache: true,
 		cookie: { expires: 30000 } ,
		select: function(event, ui){
			 $("#file_with_class_certificate option[value='"+(ui.index+1)+"']").attr("selected",true);
		} 
 	 });
	 // 首先获取 大的tabs,然后获取小的tabs。
	var bigIndex = ($("#transportTabs").tabs("option","selected")) * 1;
	if(bigIndex == 5){
		var selectedIndex = '<%= file_with_class%>' * 1 ;
		$("#file_with_class_certificate option[value='"+selectedIndex+"']").attr("selected",true);
	}
</script>
	<div id="detail" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'supplierDataDeliveryOrderDetails.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{transport_id:<%=transport_id%>},
					jsonReader:{
				   			id:'transport_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['transport_detail_id','商品名','商品条码','计划装箱数','体积','实际装箱数','到达数量','重量',<%=transport.get("transport_status",0)==TransportOrderKey.FINISH?"'运费'":"'估算运费'"%>,'所在箱号','transport_id','transport_pc_id','操作'], 
				   	colModel:[ 
				   		{name:'transport_detail_id',index:'transport_detail_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'transport_count',index:'transport_count',editrules:{required:true,number:true,minValue:1},sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'transport_volume',index:'transport_volume',align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'transport_send_count',index:'transport_send_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'transport_reap_count',index:'transport_reap_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'weight',index:'weight',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","}},
				   		{name:'freight_cost',index:'freight_cost',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false},
				   		{name:'transport_box',index:'transport_box',editable:<%=edit%>},
				   		{name:'transport_id',index:'transport_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=transport_id%>},hidden:true,sortable:false},
				   		{name:'transport_pc_id',index:'transport_pc_id',hidden:true,sortable:false},	
				   		{name:'button',index:'button',align:'left'},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'transport_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',

				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   					}
				   					select_iRow = iRow;
				   					select_iCol = iCol;
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	if(name=='p_name') 
				   					{
				   						ajaxModProductName(jQuery("#gridtest"),rowid);
				   				  	}
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/gridEditTransportDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/gridEditTransportDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
			   
			   function intransitDelivery(transport_id,number)
				{
			
					var para = "transport_id="+transport_id;
					$.ajax({
							url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/intransitTransport.action',
							type: 'post',
							dataType: 'json',
							timeout: 60000,
							cache:false,
							data:para,
										
							beforeSend:function(request)
							{
							},
							error: function(e)
							{
								alert(e);
								alert("提交失败，请重试！");
							},
							success: function(data)
							{
								if(data.rel)
								{
									window.location.reload();
								}
								
							}
						});						
				}
	</script>
		</td>
	</tr>
</table>
	</div>
  </div>
</div>

<form name="download_form" method="post"></form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="transport_id"/>
</form>
<form action="" method="post" name="followup_form">
	<input type="hidden" name="transport_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="transport_type" value="1"/>
	<input type="hidden" name="transport_content"/>
	<input type="hidden" name="expect_date"/>
</form>
<script type="text/javascript">
	//$("#eta").date_input();
	
	function modTransport()
	{
		tb_show('修改转运单','mod_transport.html?transport_id=<%=transport_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function readyPacking(transport_id)
	{
		tb_show('转运缺货商品','transport_storage_show.html?transport_id='+transport_id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function applicationApprove(transport_id)
	{
		if(confirm("确定申请T"+transport_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.transport_id.value = transport_id;
			document.applicationApprove_form.submit();
		}
	}
	function addProductPicture(pc_id, transport_id){
		 //添加商品图片。 是在转运单的时候添加d
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_picture_up.html?transport_id="+transport_id + "&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "商品图片上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,fixed: true});
	}
</script>
<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}	
});
function addProductTagTypesFile(pc_id, transport_id){
	 //添加商品标签
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/transport/transport_product_tag_file.html?transport_id="+transport_id + "&pc_id="+pc_id;
	$.artDialog.open(uri , {title: "商品标签上传["+transport_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
}
</script>	
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>

