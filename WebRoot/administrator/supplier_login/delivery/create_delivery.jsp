<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.exception.supplier.SupplierLoginTimeOutException"%>
<%@ include file="../../../include.jsp"%> 
<%
	try{
		if(adminMgr.getAdminLoginBean(session).getAttach()==null)
		{
			throw new SupplierLoginTimeOutException();
		}
	long supplier_id = adminMgr.getAdminLoginBean(session).getAttach().get("id",0l);
	DBRow[] noFinishPurchases = purchaseMgr.getNoFinishPurchase(supplier_id);
	
	int refresh = StringUtil.getInt(request,"refresh");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />


<script>
function uploadDeliveryOrderDetail()
{
	var purchase_id = $("#purchase_id").val();
	
	var para = "purchase_id="+purchase_id; 
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/supplier_login/supplier/PuchaseNoSaveDeliveryOrder.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
			},
					
			error: function(data){
				alert("系统错误，请您稍后再试");
			},
					
			success: function(data){
				if(data["nosave"]=="true")
				{
					if(confirm("该采购单有未保存的交货单，你确定要创建新的交货单吗（我们将删除未保存的交货单）？"))
					{
						parent.createDeliveryOrder(purchase_id);				
					}
				}
				else
				{
					parent.createDeliveryOrder(purchase_id);
				}
			}
	});				
}

function refresh()
{
	<%
		if(refresh==1)
		{
	%>
		parent.closeWin();
	<%
		}
		else
		{
	%>
		parent.closeWinNotRefresh();
	<%
		}
	%>
}

</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="administrator_delivery_order_detail_show.html" name="upload_form" enctype="multipart/form-data" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">创建交货单</td>
					  </tr>
				  </table>
					<br/>
					<br/>
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						<tr  style=" padding-bottom:15px; padding-left:15px;">
						<td width="9%" align="left" valign="top" nowrap class="STYLE3" ><u>选择采购单</u></td>
						<td width="91"align="left" valign="top" >
							<select id="purchase_id">
								<%
									for(int i = 0;i<noFinishPurchases.length;i++)
									{
								%>
									<option value="<%=noFinishPurchases[i].get("purchase_id",0l)%>"><%="P"+noFinishPurchases[i].getString("purchase_id")%></option>
								<%
									}
								%>
							</select>
							
						 
						</td>
					  </tr>
				  </table>
				</td></tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="uploadDeliveryOrderDetail();">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="refresh();"></td>
              </tr>
            </table>
</form>
</body>
</html>
<%}
  catch(SupplierLoginTimeOutException e)
  {
  	out.print("登录超时请从新登陆！");
  }
%>