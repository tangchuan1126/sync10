<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.ProductShapeTypeKey"%>
<%
long pc_id = StringUtil.getLong(request,"pc_id");
long  type = StringUtil.getLong(request,"type");
String  p_name = StringUtil.getString(request,"p_name");
AdminLoggerBean adminLoggerBean = adminMgr.getAdminLoggerBean(session); 
long ps_id=adminLoggerBean.getPs_id();  
DBRow[] treeRows = catalogMgr.getProductStorageCatalogTree();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>AddProductChange</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
function addProduct()
{
	var quantity = $("#quantity").val();
	
	if (!isNum(quantity))
	{
		alert("请正确填写数量");
	}
    else
	{										
    	$.ajax({
    		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_change/AddProductChangeAction.action',
    		data:'pc_id='+<%=pc_id%>+'&quantity='+quantity+'&type='+<%=type%>+'&ps_id='+$("#ps_id").val(),
    		dataType:'json',
    		type:'post',
    		beforeSend:function(request){
    	    
    		},
    		success:function(data){
    			
    			closeWin();
    			
    		},
    		error:function(){
    			alert("系统错误"); 
    		}
    	});
    		
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
function closeWin(){
	$.artDialog && $.artDialog.close();
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		 <form name="mod_form" id="mod_form" >
			  <table width="100%" height="75%" border="0" cellpadding="0" cellspacing="0">
			     <tr>
				    <td colspan="2" align="left" valign="top">	
						<table width="98%" border="0" cellspacing="5" cellpadding="2">
						  <tr>
						    <td colspan="2" align="left" valign="middle" class="text-line" style="border-bottom:1px solid #999999">
							     <table width="100%" border="0" cellpadding="5" cellspacing="0" >
							      <tr>
							        <td width="96%" style="color:#666666;font-size:30px;font-weight:bold;font-family:Arial, 宋体"><span style="color:#990000"> </span><%=p_name%></td>
							      </tr>
							    </table>
						    </td>
						   </tr>
						</table>
						<table  width="100%" border="0" cellspacing="3" cellpadding="2">
						 <tr>
						    <td width="7%" align="left" valign="middle" class="STYLE1 STYLE2" >仓库</td>
						    <td width="93%" align="left" valign="middle" >
							    <select name="ps_id" id="ps_id" style="width:150px">
											<%
											String qx;
											
											for ( int i=0; i<treeRows.length; i++ )
											{
												if ( treeRows[i].get("parentid",0) != 0 )
												 {
												 	qx = "├ ";
												 }
												 else
												 {
												 	qx = "";
												 }
											%>
											          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==ps_id?"selected":""%>> 
											          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
											          <%=qx%>
											          <%=treeRows[i].getString("title")%>          
											          </option>
											          <%
											}
											%>
							        </select>
						        </td>
					    	</tr>
							<tr>
							    <td width="7%" align="left" valign="middle" class="STYLE1 STYLE2" >数量</td>
							    <td width="93%" align="left" valign="middle" >
							    	<input name="quantity" type="text" class="input-line" id="quantity" style="width:150px;" >
							    </td>
					  		</tr>
						  </table>
				   </td>
				 </tr>
			   </table>
			</form>	
		</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
	    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
	    <td width="49%" align="right" class="win-bottom-line">
		 <input type="button" name="Submit2" value="增加" class="normal-green" onClick="addProduct();">
	
		  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
		</td>
  </tr>
</table> 
</body>
</html>
