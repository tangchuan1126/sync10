<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long product_line_id = StringUtil.getLong(request,"product_line_id");
DBRow row = productLineMgrTJH.getProductLineById(product_line_id);
long pcid = StringUtil.getLong(request,"pcid");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add Product Category</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all"/>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<script>
function addProductCatalog()
{
	var f = document.add_product_catalog_form;
	 if (f.catalog_id.value == "")
	 {
		alert("Please select product category");
		return false;
	 }
	 else
	 {
		parent.document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/addProductCatalogByLineId.action";
		parent.document.add_form.catalog_id.value = f.catalog_id.value;
		parent.document.add_form.product_line_id.value=<%=product_line_id%>;
		parent.document.add_form.submit();	
	 }
}


</script>
<style type="text/css">
.input-line
{
	width:200px;
	font-size:12px;
}
.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
    <table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="add_product_catalog_form" method="post" action="" >

<table width="98%" border="0" cellspacing="5" cellpadding="2">
  <tr height="30">
    <td align="left" valign="middle" class="STYLE1 STYLE2" style="width:25%">Product Line Name</td>
    <td align="left" valign="middle"  style="width:75%"><input name="name" value="<%=row.getString("name") %>" type="text" readonly="readonly" class="input-line" id="name" ></td>
  </tr>
  <tr height="30">
  	 <td align="left" valign="middle" class="STYLE3" >Product Category</td>
    <td align="left" valign="middle" >
	<ul id="categorymenu" class="mcdropdown_menu">
	  <li rel="0">All Category</li>
	  <%
	  DBRow c1[] = proprietaryMgrZyj.findProductCatagoryParentsByTitleId(true, 0, "", "0", "", "", 0, 0, null, request);
		  //catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="catalog_id" id="catalog_id" value="" />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	  

<script>
$("#catalog_id").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});
<%
if (pcid>0)
{
%>
$("#catalog_id").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#catalog_id").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 
</script>	
    	</td>
  </tr>
</table>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="Submit" class="normal-green" onClick="addProductCatalog();">

	  <input type="button" name="Submit2" value="Cancel" class="normal-white" onClick="$.artDialog.close();">
	</td>
  </tr>
</table>
 <form name="dataForm" action="add_product_line.html">
<input type="hidden" name="pcid" value="<%=pcid%>">
  </form>
</body>
</html>
