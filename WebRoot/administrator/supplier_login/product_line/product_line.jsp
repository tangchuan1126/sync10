<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long expandID = StringUtil.getLong(request,"expandID");
String title_id = StringUtil.getString(request, "title_id");


DBRow[] titles;
DBRow [] productLines;
titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(true, 0L, "", null, request);  //客户登录
productLines = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", title_id, 0, 0, null, request);


//productLineMgrTJH.getAllProductLine();

%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Product Line</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
	
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 

<script language="JavaScript1.2">
$(function(){
	$(".chzn-select").chosen({no_results_text: "没有该选项:"});
});
function addProductLinePage(parentId)
{
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_add.html";
	$.artDialog.open(url , {title: 'Add Product Line',width:'500px',height:'350px', lock: true,opacity: 0.3,fixed: true});
}

function addProductLine(parentId)
{
	$.prompt(
	
	"<div id='title'>增加产品线</div><br />新建类别<br>产品线名称：<input name='name' type='text' id='name' style='width:300px;'><br>",
	{
	      submit: checkAddAssetsCategory,
		  callback: 
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/addProductLineAction.action";
						document.add_form.name.value = f.name;		
						document.add_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}


function checkAddAssetsCategory(v,m,f)
{
	if (v=="y")
	{
		 if(f.name == "")
		 {
				alert("请选填写产品线名称");
				return false;
		 }
		 return true;
	}

}

function delProductLine(id){
	if (confirm("Are you sure you want to delete this product line?"))
	{
		document.del_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/delProductLine.action";
		document.del_form.id.value = id;
		document.del_form.submit();
	}
}

function cleanProductCatalog(pro_line_id,catalog_id)
{
	if(confirm("Are you sure to delete this category and its sub-categories move out from this product line?"))
	{
		document.clean_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/cleanProductCatalog.action";
		document.clean_form.product_line_id.value = pro_line_id;
		document.clean_form.catalog_id.value = catalog_id;
		document.clean_form.submit();
	}
}
function updateProductLine(id){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/update_product_line.html?id="+id;
	$.artDialog.open(url , {title: 'Rename Product Line',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});

}
function updateProductCategory(id){
	$.prompt(
		"<div id='title'>Update Porduct Line</div><br />Product Line Name：<input name='name' type='text' id='name' style='width:300px;'><br>",
		{
			submit: checkAddAssetsCategory,
			loaded:
		  
				function ()
				{

					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/getDetailProductLineJSON.action",
							{id:id},//{name:"test",age:20},
							function callback(data)
							{
								$("#name").setSelectedValue(data.name);
							}
					);
				}
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_line/modProductLineAction.action";
						document.mod_form.id.value=id;
						document.mod_form.name.value = f.name;		
						document.mod_form.submit();
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { Submit: "y", Cancel: "n" }
	});
}
function addAssetsCategory(productLineId,productLineName)
{
	//tb_show('增加产品类别','add_product_line.html?product_line_id='+productLineId+'&TB_iframe=true&height=350&width=550',false);
	$.artDialog.open("add_product_line.html?product_line_id="+productLineId, {title: "Manage Porduct Category for Product Line:["+productLineName+"]",width:'600px',height:'350px',fixed:true, lock: true,opacity: 0.3});
}
function closeWin()
{
	$.artDialog.close();
}
function changeTitle()
{
	$("#changeTitleForm").submit();
}
function refreshWindow()
{
	window.location.reload();
}
</script>
<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; Basic Data »   Product Line</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
	<input type="hidden" name="id" >
</form>
<form method="post" name="mod_form">
	<input type="hidden" name="name">
	<input type="hidden" name="id"> 
</form>
<form method="post" name="add_form">
	<input type="hidden" name="name">
	<input type="hidden" name="catalog_id">
	<input type="hidden" name="product_line_id">
	<input type="hidden" name="id"> 
</form>
<form action="post" name="clean_form">
	<input type="hidden" name="product_line_id"/>
	<input type="hidden" name="catalog_id"/>
</form>

<div style=" border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:950px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="30%">
	    	<form action="" id="changeTitleForm" method="post">
					<div class="side-by-side clearfix">
						<select  name="title_id" id="title_id" class="chzn-select" data-placeholder="Choose a Title..." style="width:250px;" tabindex="1" onchange="changeTitle()">
							<option value="-1">Select TITLE...</option>
							<%
								for(int i = 0; i < titles.length; i ++){
									String isSelected = "";
									if(title_id.equals(titles[i].getString("title_id"))){
										isSelected = "selected='selected'";
									}
							%>
								<option value='<%=titles[i].get("title_id", 0L) %>' <%=isSelected %>><%=titles[i].getString("title_name") %></option>
							<%}%>
						</select>
					</div>
		    </form>
	    </td>
	    <td width="20%">
		    <input type="button" value="Import Product Line Titles" onclick="upload('jquery_file_up')"  class="long-long-180-button" />
			&nbsp;&nbsp;<input type="hidden" id="file_names" name="file_names" value=""/>
	    </td>
	    <td>
	   		 <input type="button" value="Export Product Line Titles" onclick="down()"  class="long-long-180-button"/>
	    </td>
	  </tr>
	</table>
</div>
<br/>
</select>
<form name="listForm" method="post">
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
	      <tr> 
	        <th width="40%" class="left-title" colspan="2">Product Line Name</th>
	        <th width="16%" class="right-title" style="vertical-align: center;text-align: center;">Contains Product Qty</th>
	        <th width="25%" class="right-title">&nbsp;</th>
	      </tr>			
			<tr>
				<td colspan="5">
					<script type="text/javascript">
						d = new dTree('d');
						d.add('0','-1','Product Line</td><td align="center" valign="middle" width="20%">&nbsp;</td><td align="center" valign="middle" width="8%">&nbsp;</td><td align="center" valign="middle" width="30%"><input name="Submit" type="button" class="theme-button-add" onClick="addProductLinePage(0)" value="Add Product Line"></td></tr></table>');
						<%
							
							if(productLines.length != 0){
								for(int i=0;i<productLines.length;i++){
						%>
							d.add('<%=productLines[i].getString("id")%>','0','<%=productLines[i].getString("name")%></td><td align="center" valign="middle" width="30%">&nbsp;</td><td align="center" valign="middle" width="30%"><input name="Submit32" type="button" class="theme-button-del" onClick="delProductLine(<%=productLines[i].getString("id")%>)" value="Del">&nbsp;&nbsp;<input name="Submit3" type="button" class="theme-button-edit" onClick="updateProductLine(<%=productLines[i].getString("id")%>)" value="Rename">&nbsp;&nbsp; <input name="Submit" type="button" class="theme-button-add" onClick="addAssetsCategory(<%=productLines[i].getString("id")%>,\'<%=productLines[i].getString("name")%>\')" value="Add Category">&nbsp;<input type="button" value="Manage Title" onclick="addTitle(<%=productLines[i].getString("id")%>,'+"'<%=productLines[i].getString("name")%>'"+')" class="theme-button-add"/></td></tr></table>','');
						<%
									DBRow [] catalogs = productLineMgrTJH.getProductCatalogByProductLineId(productLines[i].get("id",0l));
									for(int j=0;j<catalogs.length;j++){
										if(catalogs[j].get("parentid",0l)==0){
						%>
							d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>','<%=productLines[i].getString("id")%>','<%=catalogs[j].getString("title")%></td><td align="center" valign="middle" width="30%"><%=productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null)%></td><td align="center" valign="middle" width="25%"><%=catalogs[j].get("product_line_id",0l)==productLines[i].get("id",0l)?"<input class=\"theme-button-del\" type=\"button\" value=\"Remove\" onclick=\"cleanProductCatalog("+productLines[i].get("id",0l)+","+catalogs[j].get("id",0l)+")\"/>":"&nbsp;"%></td></tr></table>','');
						<%				
										}
										else
										{
						%>
							d.add('<%=catalogs[j].getString("id")+"-"+productLines[i].getString("id")%>','<%=catalogs[j].getString("parentid")+"-"+productLines[i].getString("id")%>','<%=catalogs[j].getString("title")%></td><td align="center" valign="middle" width="30%"><%=productMgrZyj.getProductsByCategoryid(catalogs[j].get("id",0l),null)%></td><td align="center" valign="middle" width="25%"><%=catalogs[j].get("product_line_id",0l)==productLines[i].get("id",0l)?"<input class=\"theme-button-del\" type=\"button\" value=\"Remove\" onclick=\"cleanProductCatalog("+productLines[i].get("id",0l)+","+catalogs[j].get("id",0l)+")\"/>":"&nbsp;"%></td></tr></table>','');
						<%
										}
									}
								}
							}
						%>
						document.write(d);
					</script>
				</td>
			</tr>
	</table>
</form>
<br>
<br>
<br>
<form action="" name="download_form" id="download_form">
</form>
</body>
</html>
<script>
(function(){
	$.blockUI.defaults = {
	 css: { 
	  padding:        '8px',
	  margin:         0,
	  width:          '170px', 
	  top:            '45%', 
	  left:           '40%', 
	  textAlign:      'center', 
	  color:          '#000', 
	  border:         '3px solid #999999',
	  backgroundColor:'#ffffff',
	  '-webkit-border-radius': '10px',
	  '-moz-border-radius':    '10px',
	  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	 },
	 //设置遮罩层的样式
	 overlayCSS:  { 
	  backgroundColor:'#000', 
	  opacity:        '0.6' 
	 },
	 
	 baseZ: 99999, 
	 centerX: true,
	 centerY: true, 
	 fadeOut:  1000,
	 showOverlay: true
	};
})();

function addTitle(id,name){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/product_line/product_line_add_title.html?product_line_id="+id+"&product_line_name="+name; 
	 $.artDialog.open(uri , {title: 'Manage Product Line Linked Title',width:'700px',height:'505px', lock: true,opacity: 0.3,fixed: true});
}

function down(){

	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	var title_id=$('#title_id').val();
	var para='title_id='+title_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/product_line/AjaxDownLineAndTitleAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:para,
		cache:false,
		success: function(date){
			$.unblockUI();       //遮罩关闭
			if(date["canexport"]=="true"){
				document.download_form.action=date["fileurl"];
				document.download_form.submit();				
			}	
		}
	});
}
function upload(_target){
	var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: 'Upload Files',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
	  var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product_line/dialog_product_line_title_upload.html?fileName='+fileNames; 
	  $.artDialog.open(uri , {title: '上传TITLE',width:'600px',height:'300px', lock: true,opacity: 0.3,fixed: true});
	}
}
</script>


