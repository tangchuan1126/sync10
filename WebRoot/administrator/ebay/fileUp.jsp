<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
 
<%@ include file="../../include.jsp"%> 
 
 
 
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Best Match Item</title>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
 	*{margin:0px;padding:0px;font-size:12px;color:black;}
 	body{background:white;}
 	ul{list-style-type:none;}
 	ul.fileUl{};
 	ul.fileUl li{};
	 
 	p{padding: 2px 0;}
	a{color:black;}
	
	div.main{margin:0px auto;width:80%;}
 	 
</style>
 <%
	String fileUp =  ConfigBean.getStringValue("systenFolder")+ "action/administrator/ebay/FileUpAction.action";
	String show = StringUtil.getString(request,"show");
	String ids = StringUtil.getString(request,"ids");
	String downLoadUrl = 	ConfigBean.getStringValue("systenFolder")+"action/fileDown/fileDown.action";

 %>
<script type="text/javascript">
	 function upFiles(){
		 var form = $("#updateItemForm");
			var fileName = $("#filePath").val();
			if(fileName.length < 1){
				showMessage("Select A Right File" , "alert");
				return ;
			}
			form.submit();
	 }
	 function getFile(fileName){
		 window.open('<%= downLoadUrl%>?file_name=' +fileName);
	 }
	 function parentShowFileList(){
		var as = $(".fileUl li a");
		var array = [];
		for(var index = 0 , count = as.length ; index < count ; index++ ){
			var object = new Object();
			object["fileId"] = $(as.get(index)).attr("fileId");
			object["fileName"] = $(as.get(index)).html();
			array.push(object);
		}
		$.artDialog.opener.showFileList  && $.artDialog.opener.showFileList(array);	
		$.artDialog && $.artDialog.close();
		
	 	
	}
</script>


</head>
<body>
 <form  method="post" enctype="multipart/form-data" id="updateItemForm" action="<%=fileUp %>">
 
	  <table style="margin:0px auto;margin-top:20px;">
	  	<tbody>
	  		<tr>
	  			<td style="width:100px;" align="right">文件:</td>
	  			<td><input type="file" name="file" id="filePath"/></td>
	  		</tr>
	  		<tr>
	  			<td align="right">已上传文件:</td>
	  			<td>
	  				  <ul class="fileUl">
	  				  		
	  				  	 
	  							 		 
	  			
	  				<%
	  					if(show != null && show.equals("true") && ids != null  && ids.length() > 0 ){
	  						String[] idArray = ids.split(",");
	  						for(int index = 0 , count = idArray.length ; index < count ; index++ ){
	  							long queryFileId = Long.parseLong(idArray[index]);
	  							DBRow temp = commonFileMgrIfaceZr.getCommonFileById(queryFileId);
	  							%>
	  								<li><a fileId="<%= temp.get("id",0l)%>" href="javascript:getFile('<%= temp.getString("file_name")%>')"><%= temp.getString("file_name") %></li>
 	  							<% 
	  						}	  						
	  					}
	  				%>
	  				  </ul>		
	  			</td>
	  		</tr>
	  	</tbody>
	  	<tfoot>
	  		<tr>
	  			<td colspan="2" style="text-align:center;padding-top:5px;">
	  				<input type="button" value="上传" onclick="upFiles()"/>&nbsp;
	  				<input type="button" value="返回" onclick="parentShowFileList();">
	  			</td>
	  			 
	  		</tr>
	  	</tfoot>
	  </table>
	</form>
 
</body>
 <script type="text/javascript">
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
	$("#tabs").tabs({});
 </script>
</html>
 