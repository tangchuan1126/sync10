<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
 
<%@ include file="../../include.jsp"%> 
 <%@page import="com.cwc.app.key.EbaySite"%>
 
 
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Best Match Item</title>
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>
    <script type='text/javascript' src='../../dwr/engine.js'></script>
    <script type='text/javascript' src='../../dwr/interface/ReadItem.js'></script>
    <!-- 引入Art -->
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<style type="text/css">
 	*{margin:0px;padding:0px;font-size:12px;color:black;}
 	body{background:white;}
 	ul{list-style-type:none;}
 	ul.bar {height:40px;background:#E6F3C5;}
 	ul.bar li{float:left;width:150px;line-height:25px;height:25px;border:1px solid silver;text-align:center;cursor:pointer;margin-left:10px;margin-top:7px;}
	ul.bar li.on{background:white;}
	div.updatecssProcess , div.cssProcess , div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
	p{padding: 2px 0;}
	a{color:black;}
	
	div.main{margin:0px auto;width:80%;}
	div.process{width:500px;height:100px;border:1px solid silver;background:white;margin:0 auto; margin-top:100px;text-align:center;}
	.ui-progressbar .ui-progressbar-value { background-image: url(../imgs/pbar-ani.gif); height:19px;}
	ul.udpate_Ul{border:0px solid silver;}
	ul.udpate_Ul li{line-height:18px;height:18px;text-indent:5px;border-bottom:1px dotted silver;}
	ul.udpate_Ul li a{position:relative;text-decoration:none;}
	ul.udpate_Ul li a:hover{left:-1px;top:-1px;}
</style>
<%
	String findItemsAdvancedAction =  ConfigBean.getStringValue("systenFolder")+"action/ebay/FindItemsAdvancedAction.action";
	String getEbayItemInfoByItemNumbersAction = ConfigBean.getStringValue("systenFolder")+"action/ebay/GetEbayItemInfoByItemNumbersAction.action";
	String bestMatchItemDetailsAction = ConfigBean.getStringValue("systenFolder")+"action/ebay/BestMatchItemDetails.action";
	String getEbayItemInfoBySingleItemAction =  ConfigBean.getStringValue("systenFolder")+ "action/ebay/GetEbayItemInfoBySingleItemAction.action";
	String updateEbayItemsAction =  ConfigBean.getStringValue("systenFolder")+ "action/administrator/ebay/UpdateEbayItemsAction.action";
	
	String downLoadUrl = 	ConfigBean.getStringValue("systenFolder")+"action/fileDown/fileDown.action";
	DBRow[] userToken = bestMatchItemDetaiMgrZr.getAllEbayUserOnPaypal20();
	EbaySite sites = new EbaySite();
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	long adid = adminLoggerBean.getAdid(); 
	DBRow loginUserInfo = adminMgr.getDetailAdmin(adid);
%>
<script type="text/javascript">
	var canClose = false;
	var filePath = "";
	var updateItemResult = [];
	 
	 
	function BestMatchItemDetails(){

		if(!validate()){
			 return ;
		}
		var form= $("#search");
		$(".cssDivschedule").css("display","block");
	 
		$.ajax({
			url:'<%= bestMatchItemDetailsAction%>',
			data:form.serialize(),
			dataType:'text',
			success:function(data){
				$(".cssDivschedule").fadeOut();
				$("#resultA").html(data).attr("file",data);
				$("#getMoreInfo").css("display","block");
				$("#moreInfoXls").html("");
				 
			}
		})
	}
	function getFileLoad(id){
		var fileName= $("#" + id).html();
	 	//window.location.href = '<%= downLoadUrl%>?file_name=' +fileName;
	 	window.open('<%= downLoadUrl%>?file_name=' +fileName);
	}
	function validate(){
		var keywords = $("#keywords").val();
		if($.trim(keywords).length < 1){
			alert("input keyword");
			return false;
		}
		var top = $("#top").val();
		if($.trim(top).length < 1 || top * 1 < 1){
			alert("input top number");
			return false;
		}
		return true;
	}
	function getMoreInfo_button(){
	 
		var fileName= $("#resultA").html();
		var time = fileName.replace(/[^\d]/g,"");
	  
		$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%= getEbayItemInfoByItemNumbersAction%>' + "?time="+time+"&file_name=" +fileName +"&login_on_seller_id=" + $("#login_on_seller_id").val(),
			dataType:'text',
			success:function(data){
 
				$(".cssDivschedule").fadeOut();
				$("#moreInfoXls").html(data);
			},
			error:function(){
				$(".cssDivschedule").fadeOut();
			}
		})
	}
	function volidateFindItemAdvanced(){
		var start = $("#start").val();
		var find_count = $("#find_count").val();
		var find_seller_id = $("#find_seller_id").val();
		var find_keywords = $("#find_keywords").val();
		if($.trim(find_seller_id).length < 1){
			showMessage("输入seller id","alert");
			return false;
		}
		if($.trim(find_keywords).length < 1){
			showMessage("输入keywords","alert");
			return false;
		}
		if($.trim(find_count) * 1  < 1){
			showMessage("输入结尾查询的数量","alert");
			return false;
		}
		if($.trim(start) * 1 % 50 > 0){
			showMessage("起始数量为50的倍数","alert");
			return false;
		}
		if($.trim(find_count) * 1 % 50 > 0){
			showMessage("结尾数量为50的倍数","alert");
			return false;
		}
		if($.trim(start) * 1  >= $.trim(find_count) * 1){
			showMessage("起始数量应该小于结尾数量","alert");
			return false;
		}
		return true;
	
	}
	/*
	function getFindItemsAdvanced(){
 
		if(!volidateFindItemAdvanced()){
			return;
		}
 
		var form = $("#find_form");
		 
		lockWindow();
		$("#findAdcanced_a").html("");
		$.ajax({
			url:'<%= findItemsAdvancedAction%>',
			dataType:'json',
			data:form.serialize(),
			success:function(data){
				UnlockWindow();
				if(data.flag === "success"){
				 
					$("#findAdcanced_a").html(data.file);
					showMessage("获取数据   " + data.total + " 条","alert");
				}else{
					showMessage(data.message,"error");
				}
			},
			error:function(data){
				UnlockWindow();
			}
		})
	}
	*/
	function getFindItemsAdvanced(){
		 
		if(!volidateFindItemAdvanced()){
			return;
		}
 
		var form = $("#find_form");
		 
		lockWindow();
		$("#findAdcanced_a").html("");
		$.ajax({
			url:'<%= findItemsAdvancedAction%>',
			dataType:'json',
			data:form.serialize(),
			success:function(data){
				UnlockWindow();
				if(data.flag === "success"){
				 	// 这里使用DWR去读取信息String  itemNumbers,String filePath, String title , String token , String usreId
					// result.add("ids", StringUtil.convertStringArrayToString(itemNumberSet.toArray(new String[itemNumberSet.size()])) );
				 	//result.add("path", baseFilePath + fileName);
				 	//result.add("title", title);
					 //result.add("token", token);
					 readIndex(0,data.total,0,0);
					 filePath =  data.file;
					 ReadItem.getMoreInfoByItemNumbersAndNotAjax(data.ids,data.path,data.title,data.token,""+'<%= adid%>');
				}else{
					showMessage(data.message,"error");
				}
			},
			error:function(data){
				UnlockWindow();
			}
		})
	}

	function readIndex(readIndex, total,successIndex , errorIndex){
		var value = GetPercent(readIndex,total);
		 
	 	//alert(readIndex + " : " + total);
	 	$("#readIndexValue").html(readIndex);
		$("#totalCountValue").html(total);
		$("#successIndex").html(successIndex);
		$("#errorIndex").html(errorIndex);
		 setProgressBar(value * 1);
	}
	
	function lockWindow(){$(".cssDivschedule").css("display","block");}
	function UnlockWindow(){$(".cssDivschedule").fadeOut();}
	function GetSingleItem(){
		var item_number_single = $("#item_number_single").val();
		if($.trim(item_number_single).length < 1){
			showMessage("请输入Item Number","alert");
			return ;
		}
		$("#singleXls").html("");
		lockWindow();
		$.ajax({
			url:'<%= getEbayItemInfoBySingleItemAction%>' + "?item_number="+item_number_single + "&login_on_seller_id=" + $("#sellerIdSelected").val() ,
			dataType:'text',
			
			success:function(data){
				UnlockWindow();
				$("#singleXls").html(data);
		 
			},
			error:function(){
				UnlockWindow();
				showMessage("系统错误请稍后再试!","error");
			}
		})
	}
	jQuery(function($){
		 var progressbar = $("#progressbar");
		 progressbar.progressbar({value: 0});
	 
		 var progressbarUpdate = $("#progressbarUpdate");
		 progressbarUpdate.progressbar({value: 9});
	})
	function setProgressBar(length){
		var progressbar = $("#progressbar");
		$(".cssProcess").css("display","block");
		 progressbar.progressbar('option', 'value',length);
	 
	}
	 

	function GetPercent(num, total) {
		num = parseFloat(num);
		total = parseFloat(total);
		if(isNaN(num) || isNaN(total)) {
			return "-";
		}
		return total <= 0 ? "0" : (Math.round(num / total * 10000) / 100.00 );
	 } 
	 function readComplete(value){
		 showMessage("成功读取" + (value*1) + "条数据","alert");
		
		 var progressbar = $("#progressbar");
		 progressbar.progressbar({value: 0});
		 $(".cssProcess").css("display","none");
		 $("#findAdcanced_a").html(filePath);
	}
	function stopRead(){
		canClose = true;
		ReadItem.setReadStop('<%= adid%>');
	}
	dwr.engine._errorHandler = function(message, ex) {dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);};
	
	function fixIntValue(_this)
	{
		var node = $(_this);
		var value = node.val();
		var fixValue = value.replace(/[^0-9]/g,'');
		node.val(fixValue * 1);
	}
	function updateItems(){
	 	var sellerIdSelectedUpdateItem = $("#sellerIdSelectedUpdateItem");
	 	var optionSelected =  $("option:selected",sellerIdSelectedUpdateItem);
		var token = optionSelected.attr("userToken");
		 
		var fileList = $(".fileList");
		var a = fileList.find("a");
		if(a.length > 1){
			showMessage("目前只支持一个文件的修改","alert")
			return ;
		}
		$(".udpate_Ul li").remove();
		$(".showUpdateErrorInfo").html("");
	 
		lockWindow();
 		ReadItem.setUpdateEbayItem(a.attr("fileId"),token ,'<%= adid%>');
	}
	function addFile(){
		 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/ebay/fileUp.html"; 
		 $.artDialog.open(uri , {title: '文件上传',width:'400px',height:'150px', lock: true,opacity: 0.3,fixed: true});
	}
	function showFileList(arrayList){
	 
		 if(arrayList && arrayList.length > 0){
			 
			var ul = $(".fileList");
			for(var index = 0 , count = arrayList.length ; index < count ; index++ ){
				 ul.append(createLi(arrayList[index]["fileId"],arrayList[index]["fileName"]));
			}
			$("#addFileButton").remove();
		} 
	}
	 function getFile(fileName){
		 window.open('<%= downLoadUrl%>?file_name=' +fileName);
	 }
	function createLi(fileId,fileName){
		var li = $("<li><a fileId='"+fileId+"' href=javascript:getFile('"+fileName+"')>" + fileName+"</a></li>");
 
		return li;
	}
	 
	function fileNotFound(){
		UnlockWindow();
		showMessage("文件不正确,或者文件中没有对应的信息","alert");
	}
	function setUpdateProgressBar(value){
		var progressbar = $("#progressbarUpdate");
		$(".updatecssProcess").css("display","block");
		 progressbar.progressbar('option', 'value',value);
	}
	function showUpdateProcess(index,updateSuccessIndex,updateFailIndex,total,message,itemId,flag){
		UnlockWindow();
		var value = GetPercent((index+1),total);
		 if((index*1 + 1) == total * 1){
			 canClose = true;
		  }
	 	//alert(readIndex + " : " + total);
	 	$("#readUpdateIndexValue").html((index+1));
		$("#totalUpdateCountValue").html(total);
		$("#successUpdateIndex").html(updateSuccessIndex);
		$("#errorUpdateIndex").html(updateFailIndex);
		 setUpdateProgressBar(value * 1);
		var obj = new Object();
		obj["message"] = message;
		obj["item_id"] = itemId;
		 
		obj["flag"] = flag;
		updateItemResult.push(obj); 
		createLiInUpdateItem(obj);
	}
	function createLiInUpdateItem(obj){
		var ul = $(".udpate_Ul");
		var li ;
		if(obj["flag"] === "error"){
			li = $("<li><a style='color:red;' href=javascript:selectUpdateItem("+obj["item_id"]+")>"+obj["item_id"]+"</li>");
		}else{
			li = $("<li><a style='color:green;' href='javascript:selectUpdateItem("+obj["item_id"]+")'>"+obj["item_id"]+"</li>");
		}
		 
		ul.append(li);
	 
		$(".showUpdateErrorInfo").html(obj["message"]);
	}
	function selectUpdateItem(id){

		if(updateItemResult && updateItemResult.length > 0){
			for(var index = 0 , count = updateItemResult.length ; index < count ; index++ ){
				if(updateItemResult[index]["item_id"]*1 == id*1){
			 
					$(".showUpdateErrorInfo").html(updateItemResult[index]["message"]);
					break ;
				}
			}
		}
	}
	function stopUpdateProcess(){
		showMessage("停止更新","alert");
	}
	function closeUpdateProcess(){
		 
		if(canClose){
			$(".updatecssProcess").css("display","none");
		}else{
			showMessage("正在更新不能关闭该窗口","alert");
		}
	}
</script>


</head>
<body onload="dwr.engine.setActiveReverseAjax(true);">
<div class="cssDivschedule">
 		<div class="ui-corner-all innerDiv">
 			正在加载数据中.......
 		</div>
</div>
<div class="updatecssProcess" >
	<div class="ui-corner-all updateProcess" style="background:white;display:block;height:410px;width:500px;border:1px solid silver;margin:0 auto;margin-top:40px;">
		 <p style="text-align:center;">正在更新 <span id="readUpdateIndexValue">0 </span> / <span id="totalUpdateCountValue">0</span> &nbsp;&nbsp;成功更新 ：<span id="successUpdateIndex">0</span>&nbsp;&nbsp;失败：<span id="errorUpdateIndex">0</span></p>
 		 <p id="progressbarUpdate" class="ui-corner-all" style="height:20px;width:400px;margin:0px auto;border:0px;"></p>
 		<p style="text-align:center;padding-bottom:4px;"> <input type="button" value="停止" onclick="stopRead();" /></p>
 		 <div>
 		 	<div class="" style="border:1px solid silver;float:left; margin-left: 20px;padding: 2px;width: 155px;overflow:auto;height:300px;">
 		 		<ul class="udpate_Ul">
 		 			 
 		 		</ul>
 		 	</div>
 		 	<div class="showUpdateErrorInfo" style="padding:2px;border:1px solid silver;width:280px;float:left;margin-left:10px;height:300px;">
 		 	  
 
 		 	</div>
 		 	<div style="clear:both;text-align:center;padding-top:5px;"><input type="button" onclick="closeUpdateProcess();" value="关闭"/></div>
 		 </div>
	</div>
</div>
 <div class="cssProcess" style="display:none;">
 		<div class="ui-corner-all process">
 			 <p style="text-align:center;">正在读取 <span id="readIndexValue">0 </span> / <span id="totalCountValue">0</span> &nbsp;&nbsp;成功读取 ：<span id="successIndex">0</span>&nbsp;&nbsp;失败：<span id="errorIndex">0</span></p>
 			 <p id="progressbar" class="ui-corner-all" style="height:20px;width:400px;margin:0px auto;border:0px;"></p>
 			<input type="button" value="停止" onclick="stopRead();"/>
 		</div>
</div>
 
<div id="tabs">
		 <ul>
			 <li><a href="#usual_tools">BestMatch</a></li>
			 <li><a href="#advan_search">GetItem</a></li>
			 <li><a href="#single_item">GetSingleItem</a></li>	 
			 <li><a href="#update_items">UpdateItems</a></li>
		 </ul>
		 <div id="usual_tools">
		 <form action="" name="" id="search">
		 	
		 	<table>
		 		<tr>
		 			<td  style="text-align:right;"> Login On Seller Id : </td>
		 			<td><select name="seller_id" id="login_on_seller_id">
				  		<%
				  			if(userToken != null && userToken.length > 0){
				  				for(DBRow row : userToken){
				  					%>
				  					<option value='<%= row.getString("user_id") %>'><%= row.getString("user_id") %></option>
				  					<%
				  				}
				  			}
				  		%> 
				  		</select>
		  			</td>
		 		</tr>
		 		<tr>
		 			<td  style="text-align:right;"> Site : </td>
		 			<td>
		 				<select name="site">
		  				<%
		  				 
			  				List<String> list = sites.getStatus();
			  				for(String s : list){
			  					String  value = sites.getEbaySiteById(Integer.valueOf(s));
			  					String[] array = value.split(",");
			  				%>
			  					<option value='<%=s %>'><%= array[1] %></option>
			  				<%	 
			  				}
		  					
		  				%>
		  			</select>
		 			</td>
		 		</tr>
		 		 <input type="hidden" name="name" value='<%= loginUserInfo.getString("account") %>'/> 
		 	 	 <input type="hidden" name="search_seller" style="width:150px;"/>
		 	<!--  
		 		<tr>
		 			<td>Search SellerId :</td>
		 			<td> <input type="text" name="search_seller" style="width:150px;"/></td>
		 		</tr>
		 		-->
		 		 <tr>
		 		 	<td  style="text-align:right;">
		 		 		Key Words  : 
		 		 	</td>
		 		 	<td><input type="text" value="" name= "keywords" id="keywords"/></td>
		 		 </tr>
		 		 <tr>
		 		 	<td  style="text-align:right;">Top number :</td>
		 		 	<td><input type="text" name="top" id="top" /> </td>
		 		 </tr>
		 		 <tr>
		 		 	<td colspan="2">  <input type="button" onclick="BestMatchItemDetails() ;" value="BestMatchItemDetails" /></td>
		 		 	 
		 		 </tr>
		 		 <tr>
		 		 	<td> <a href="javascript:getFileLoad('resultA')" file= "ebay_1346749378875.xls" id="resultA"></a>  </td>
		 		 	<td><input type="button" id="getMoreInfo" value="GetItem" onclick="getMoreInfo_button()" style="display:none;"/></td>
		 		 </tr>
		 		 <tr>
		 		 	<td colspan="2">
		 		 			<a href="javascript:getFileLoad('moreInfoXls')" file="" id="moreInfoXls"></a>
		 		 	</td>
		 		 </tr>
		 	</table>	
		 	 	</form>
		 
		  </div>
		 <div id="advan_search">
		 	 	<form id="find_form">
 	 		<table>
 	 			 <tr>
		 			<td style="text-align:right;"> Login On Seller Id : </td>
		 			<td><select name="login_on_seller_id">
				  		<%
				  			if(userToken != null && userToken.length > 0){
				  				for(DBRow row : userToken){
				  					%>
				  					<option value='<%= row.getString("user_id") %>'><%= row.getString("user_id") %></option>
				  					<%
				  				}
				  			}
				  		%> 
				  		</select>
		  			</td>
		 		</tr>
 	 				<tr>
 	 					<td  style="text-align:right;">Site:</td>
 	 					<td>
 	 						<select name="site">
				  				<%
					  				List<String> temp = sites.getStatus();
					  				for(String s : temp){
					  					String  value = sites.getEbaySiteById(Integer.valueOf(s));
					  					String[] array = value.split(",");
					  				%>
					  					<option value='<%=s %>'><%= array[1] %></option>
					  				<%	 
					  				}
				  					
				  				%>
  							</select>
 	 					</td>
 	 				</tr>
 	 				<tr>
 	 					<td  style="text-align:right;">key words:</td>
 	 					<td><input type="text" name="keywords" value="front" id = "find_keywords" /></td>
 	 				</tr>
 	 				<tr>
 	 					<td  style="text-align:right;">Search Seller Id:</td>
 	 					<td><input type="text" name="seller_id" id = "find_seller_id"  value="brakemotive76"/></td>
 	 				</tr>
 	 				<tr>
 	 					<td  style="text-align:right;">Count:</td>
 	 					<td><input type="text" name="start" style="width:50px;" value="0" id="start" onkeyup="fixIntValue(this);" />&nbsp;到&nbsp;<input type="text" name="end" value="50" id = "find_count" style="width:50px;" onkeyup="fixIntValue(this);"/></td>
 	 				</tr>
 	 				<tr>
 	 					<td colspan="2"><input type="button" onclick="getFindItemsAdvanced()" value="GetItem"/></td>
 	 				</tr>
 	 				<tr>
 	 					<td colspan="2"><a href="javascript:getFileLoad('findAdcanced_a')" id="findAdcanced_a"></a></td>
 	 				</tr>
 	 		</table>
 	 	
 	 	</form>
		 
		 </div>
		 <div id="single_item">
		 		<form action="singleItemForm">
		 			<table>
			 			<tr>
			 				<td  style="text-align:right;">Login On Seller Id :</td>
			 				<td>
			 					<select name="login_on_seller_id" id="sellerIdSelected">
							  		<%
							  			if(userToken != null && userToken.length > 0){
							  				for(DBRow row : userToken){
							  					%>
							  					<option value='<%= row.getString("user_id") %>'><%= row.getString("user_id") %></option>
							  					<%
							  				}
							  			}
							  		%> 
							  		</select>
			 				</td>
			 			</tr>	
			 			<tr>
		 					<td  style="text-align:right;">Item Number :</td>
		 					<td>
		 						<input type="text" name="item_number" id="item_number_single"/>
		 					</td>
		 				</tr>
		 				<tr>
 	 						<td colspan="2"> <input type="button" onclick="GetSingleItem()" value="GetSingleItem"/></td>
 	 					</tr>
 	 					<tr>
 	 						<td colspan="2">
		 		 				<a href="javascript:getFileLoad('singleXls')" file="" id="singleXls"></a>
		 		 			</td>
 	 					</tr>
				  	 </table>	
		 		</form>
		 </div>
		 <div id="update_items">
	 
		 	<form>
		 			<table>
			 			<tr>
			 				<td  style="text-align:right;">Login On Seller Id :</td>
			 				<td>
			 					<select name="login_on_seller_id" id="sellerIdSelectedUpdateItem">
							  		<%
							  			if(userToken != null && userToken.length > 0){
							  				for(DBRow row : userToken){
							  					%>
							  					<option userToken='<%= row.getString("user_token") %>' value='<%= row.getString("user_id") %>'><%= row.getString("user_id") %></option>
							  					<%
							  				}
							  			}
							  		%> 
							  		</select>
			 				</td>
			 			</tr>	
			 			<tr>
		 					<td  style="text-align:right;">XLS File:</td>
		 					<td>
		 						<p>
		 							<ul class="fileList">
		 								    
		 							</ul>
		 						</p>
		 						<input type="button" id="addFileButton" value="添加文件" onclick="addFile();"/>
		 					</td>
		 				</tr>
		 				<tr>
		 					<td style="text-align:right;"><input type="button" value="UpdateItems" onclick="updateItems()"/></td>
		 					<td></td>
		 				</tr>
				  	 </table>	
		 		</form>
		 	 
		 </div>
	
	
	</div>

 
 </body>
 <script type="text/javascript">
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
	$("#tabs").tabs({});
 </script>
</html>
 