<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ClearanceKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
 
<%@ include file="../../include.jsp"%> 
<%@ page import="java.util.ArrayList" %>
 
<html>
<head>
<%
 
	long purchase_id	= StringUtil.getLong(request,"purchase_id");
	DBRow purchaseRow	= purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id+"");
	int invoice			= purchaseRow.get("invoice",0);
	int drawback		= purchaseRow.get("drawback",0);	
	int need_tag		= purchaseRow.get("need_tag",0);
	int need_third_tag	= purchaseRow.get("need_third_tag",0);
 	InvoiceKey key		= new InvoiceKey();
	TDate tdate			= new TDate();
	String handleTransportClearance = ConfigBean.getStringValue("systenFolder") + "action/administrator/purchase/PurchaseInvoiceStateCertificateAction.action";
	int follow_up_type	= StringUtil.getInt(request, "follow_up_type");
	DrawbackKey drawBackKey = new DrawbackKey();
	TransportTagKey transportTagKey = new TransportTagKey();
	int fileType		= 0;
	String followUpType = "";
	if(4 == follow_up_type)
	{
		fileType		= FileWithTypeKey.PURCHASE_INVOICE;
		followUpType	= "发票";
	}
	else if(5 == follow_up_type)
	{
		fileType		= FileWithTypeKey.PURCHASE_DRAWBACK;
		followUpType	= "退税";
	}
	else if(8 == follow_up_type)
	{
		fileType		= FileWithTypeKey.PURCHASE_TAG_FILE;
		followUpType	= "内部标签";
	}
	else if(33 == follow_up_type)
	{
		fileType		= FileWithTypeKey.PURCHASE_THIRD_TAG_FILE;
		followUpType	= "第三方标签";
	}
	int[] types			= {fileType};
	DBRow[] fileArray	= purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(purchase_id, types);
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	String handSubmitAction =  	ConfigBean.getStringValue("systenFolder")+"action/administrator/purchase/PurchaseInvoiceStateCertificateAction.action";  
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
 %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=followUpType %>流程跟进</title>

<!--  基本样式和javascript -->
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
 <!-- 在线阅读 -->
 <script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
	
	ul.fileUL{list-style-type:none;}
	ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
</style>
<style type="text/css">
 	select.key{display:none;}
</style>
<!-- 遮罩 -->
 <script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
 <script type="text/javascript">
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>

<script type="text/javascript">
 jQuery(function($){
	  
		if(4 == <%=follow_up_type%>){
			if('<%=invoice%>' * 1 > 0){
				$("#invoice option[value='"+'<%=invoice%>'+"']").attr("selected",true);
			}
		}else if(5 == <%=follow_up_type%>){
			if('<%=drawback%>' * 1 > 0){
				$("#invoice option[value='"+'<%=drawback%>'+"']").attr("selected",true);
			}
		}else if(8 == <%=follow_up_type%>){
			if('<%=need_tag %>' * 1 > 0){
				$("#invoice option[value='"+'<%=need_tag%>'+"']").attr("selected",true);
			}
		}
		else if(33 == <%=follow_up_type%>){
			if('<%=need_third_tag %>' * 1 > 0){
				$("#invoice option[value='"+'<%=need_third_tag%>'+"']").attr("selected",true);
			}
		}
	 	$('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		 
			onSelect:function(dateText, inst){

				setContentHtml(dateText);
				
			}
			
		});
	 	setContentHtml($("#eta").val());
	 	changeClearanceType();
	 	if(4 == <%=follow_up_type%>){
			if(4 == <%=invoice%>){
				$("#invoice option[value=4]").attr("selected", true);
				$("#invoice").attr("disabled",true);
			}
		}
	 	if(5 == <%=follow_up_type%>){
			if(4 == <%=drawback%>){
				$("#invoice option[value=4]").attr("selected", true);
				$("#invoice").attr("disabled",true);
			}
		}
		if(8 == <%=follow_up_type%>)
		{
			if(3 == <%=need_tag %>){
				$("#invoice").attr("disabled",true);
			}
		}
		if(33 == <%=follow_up_type%>)
		{
			if(3 == <%=need_third_tag %>){
				$("#invoice").attr("disabled",true);
			}
		}
})
function setContentHtml(value){
	 var clearanceHTML = "["+$.trim($("#invoice option:selected").html())+"]";
	 var html = "";
	
	 var eta = "";
	 if(4 == <%=follow_up_type%>)
	 {
		if($("#invoice").val() * 1 != '<%=InvoiceKey.FINISH %>')
		{
			eta = "阶段预计"+$("#eta").val()+"完成:";
		}
		else
		{
			eta = "完成:";
		}
	 }
	 else if(5 == <%=follow_up_type%>)
	 {
		 if($("#invoice").val() * 1 != '<%=DrawbackKey.FINISH %>')
		 {
			 eta = "阶段预计"+$("#eta").val()+"完成:";
		 }
		 else
		 {
			 eta = "完成:"
		 }
	 }
	 else if(8 == <%=follow_up_type%>)
	 {
		 if($("#invoice").val() * 1 != '<%=TransportTagKey.FINISH %>')
		 {
			eta = "阶段预计"+$("#eta").val()+"完成:";
		 }
		 else
		 {
			 eta = "完成:"
		 }
	 }
	 else if(33 == <%=follow_up_type%>)
	 {
		 if($("#invoice").val() * 1 != '<%=TransportTagKey.FINISH %>')
		 {
			eta = "阶段预计"+$("#eta").val()+"完成:";
		 }
		 else
		 {
			 eta = "完成:"
		 }
	 }
	var contentNode = $("#context");
	var contentNodeValue = contentNode.val();
	html += clearanceHTML + eta;
	if(contentNodeValue.indexOf("完成:") != -1){
		var index = contentNodeValue.indexOf("完成:")
		html += contentNodeValue.substr(index +3);
		$("#context").val(html);
	}else{
		$("#context").val(html + contentNodeValue);
	}
}
function submitForm(){
	 
	if(!validate()){return ;}
	ajaxSubmit();
}
 
function validate(){
	
	if($("#context").val().length < 1){
		showMessage("请输入备注","alert");
		return false;
	}
	var clearance = $("#invoice");
	if(4 == <%=follow_up_type%>){
		if(clearance.val()*1  == '<%= InvoiceKey.FINISH%>' * 1){
		    $("#eta").val("");
		}else{
			$("input[name='file_names']").val("");
			if($("#eta").val().length < 1){
				showMessage("请选择时间","alert");
				return false;
			}
		}
	}else if(5 == <%=follow_up_type%>){
		if(clearance.val()*1  == '<%= DrawbackKey.FINISH%>' * 1){
		    $("#eta").val("");
		}else{
		    $("input[name='file_names']").val("");
			if($("#eta").val().length < 1){
				showMessage("请选择时间","alert");
				return false;
			}
		}
	}else if(8 == <%=follow_up_type%>){
		if(clearance.val()*1  == '<%= TransportTagKey.FINISH%>' * 1){
		    $("#eta").val("");
		}else{
		    $("input[name='file_names']").val("");
			if($("#eta").val().length < 1){
				showMessage("请选择时间","alert");
				return false;
			}
		}
	}
	else if(33 == <%=follow_up_type%>){
		if(clearance.val()*1  == '<%= TransportTagKey.FINISH%>' * 1){
		    $("#eta").val("");
		}else{
		    $("input[name='file_names']").val("");
			if($("#eta").val().length < 1){
				showMessage("请选择时间","alert");
				return false;
			}
		}
	}
	return true ;
}
function changeClearanceType(){
	var clearance = $("#invoice");
	if(4 == <%=follow_up_type%>){
		if(clearance.val() != '<%= InvoiceKey.FINISH%>'){
			$("#notfinish_span").css("display","inline-block");
			$("#file_up_span").css("display","none");
			$("#file_up_tr").css("display","none");
		}else{
			$("#notfinish_span").css("display","none");
			$("#file_up_span").css("display","inline-block");
			if($(".fileUL li").length > 0 ){
				$("#file_up_tr").attr("style","");
			}
		}
	}else if(5 == <%=follow_up_type%>){
		if(clearance.val() != '<%= DrawbackKey.FINISH%>'){
			$("#notfinish_span").css("display","inline-block");
			$("#file_up_span").css("display","none");
			$("#file_up_tr").css("display","none");
		}else{
			$("#notfinish_span").css("display","none");
			$("#file_up_span").css("display","inline-block");
			if($(".fileUL li").length > 0 ){
				$("#file_up_tr").attr("style","");
			}
		}
	}
	else if(8 == <%=follow_up_type%>){
		if(clearance.val() != '<%= TransportTagKey.FINISH%>'){
			$("#notfinish_span").css("display","inline-block");
			$("#file_up_span").css("display","none");
			$("#file_up_tr").css("display","none");
		}else{
			$("#notfinish_span").css("display","none");
			$("#file_up_span").css("display","inline-block");
			if($(".fileUL li").length > 0 ){
				$("#file_up_tr").attr("style","");
			}
		}
	}
	else if(33 == <%=follow_up_type%>){
		if(clearance.val() != '<%= TransportTagKey.FINISH%>'){
			$("#notfinish_span").css("display","inline-block");
			$("#file_up_span").css("display","none");
			$("#file_up_tr").css("display","none");
		}else{
			$("#notfinish_span").css("display","none");
			$("#file_up_span").css("display","inline-block");
			if($(".fileUL li").length > 0 ){
				$("#file_up_tr").attr("style","");
			}
		}
	}
	setContentHtml();
}
function downLoad(fileName , tableName , folder){
	 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>');
}
//文件上传
//做成文件上传后,然后页面刷新提交数据如果是有添加文件
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames && fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
    		 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
   		 }});
}
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
	 	//判断是不是有了
	 	if($("a",$("#over_file_td")).length > 0){
			var td = $("#over_file_td");
			td.append(lis);
		}else{
			$("#file_up_tr").attr("style","");
			$("#jquery_file_up").append(lis);
		}
	}
}
function ajaxSubmit(){

    $.ajax({
		url:'<%= handSubmitAction%>',
		dataType:'json',
		data:$("#myform").serialize(),
		beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	    },
		success:function(data){
	    	$.unblockUI();
			if(data && data.flag == "success"){
			    cancel();
				$.artDialog.opener.windowRefresh  && $.artDialog.opener.windowRefresh();
			}
			else
			{
				alert(data.flag);
			}
		},
		error:function(){
		    showMessage("系统错误!","error");
		}
	})
}

function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_"+'<%= purchase_id%>'+"_" + fileName;
    var  a = "<p class='new'><a href="+uri+">"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;

}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

//图片在线显示
function showPictrueOnline(fileWithType,fileWithId , currentName){
    var obj = {
		file_with_type:fileWithType,
		file_with_id : fileWithId,
		current_name : currentName ,
		cmd:"multiFile",
		table:'file',
		base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'
	}
    if(window.top && window.top.openPictureOnlineShow){
		window.top.openPictureOnlineShow(obj);
	}else{
	    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
	}
    
}
</script>
</head>
<body>
	 <form id="myform">
		 <input type="hidden" name="backurl" id="backurl" value="administrator/purchase/purchase_invoice_state.html?purchase_id=<%=purchase_id %>&follow_up_type=<%=follow_up_type %>"/>
		<input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
		<input type="hidden" name="follow_up_type" value="<%=follow_up_type %>"/>
		<table width="100%" height="100%">
			<tr width="100%" height="80%" valign="top">
				<td width="100%" height="100%">
					<table width="100%">
					 	<tr valign="top">
					 		<td style="text-align:right;">当前阶段:</td>
					 		<td>
					 			<select name="invoice" id="invoice" onchange="changeClearanceType();">
					 				<%
					 					if(4 == follow_up_type){
					 				%>	
				 						<option value='<%= InvoiceKey.INVOICING %>'><%= key.getInvoiceById(InvoiceKey.INVOICING) %></option>
				 						<option value='<%= InvoiceKey.FINISH %>'><%= key.getInvoiceById(InvoiceKey.FINISH) %></option>
					 				<%
					 					}else if(5 == follow_up_type){
					 				%>		
					 					<option value='<%= DrawbackKey.DRAWBACKING %>'><%= drawBackKey.getDrawbackById(DrawbackKey.DRAWBACKING) %></option>
					 					<option value='<%= DrawbackKey.FINISH%>'><%= drawBackKey.getDrawbackById(DrawbackKey.FINISH) %></option>
					 				<%	
					 					}else if(8 == follow_up_type){
				 					%>		
					 					<option value='<%= TransportTagKey.TAG %>'>制签中</option>
					 					<option value='<%= TransportTagKey.FINISH%>'><%= transportTagKey.getTransportTagById(TransportTagKey.FINISH) %></option>
					 				<%			
					 					}else if(33 == follow_up_type){
				 					%>		
					 					<option value='<%= TransportTagKey.TAG %>'>制签中</option>
					 					<option value='<%= TransportTagKey.FINISH%>'><%= transportTagKey.getTransportTagById(TransportTagKey.FINISH) %></option>
					 				<%			
					 					}
					 				%>
					 				
					 			</select>
					 			<span id="notfinish_span" style=''>
					 		 		预计本阶段完成时间:<input type="text" id="eta" name="eta" value="<%= tdate.getYYMMDDOfNow() %>"/></span>
					 			</span>
					 			<span id="file_up_span" style="display:none;">
					 				<%
					 					if(4 == follow_up_type){
					 				%>	
					 				<input type="hidden" id="sn" name="sn" value="P_invoice"/>
					 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURCHASE_INVOICE %>" />
					 				<%
					 					}else if(5 == follow_up_type){
					 				%>	
					 				<input type="hidden" id="sn" name="sn" value="P_drawback"/>
					 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURCHASE_DRAWBACK %>" />
					 				<%	
					 					}else if(8 == follow_up_type){
					 				%>	
					 				<input type="hidden" id="sn" name="sn" value="P_tag"/>
					 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURCHASE_TAG_FILE %>" />
					 				<%	
					 					}else if(33 == follow_up_type){
					 				%>	
					 				<input type="hidden" id="sn" name="sn" value="P_third_tag"/>
					 				<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURCHASE_THIRD_TAG_FILE %>" />
					 				<%	
					 					}
					 				%>
					 				<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>" />
					 				<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
			                        <input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
					 			</span>
					 		</td>
					 		 
					 	</tr>
						 <tr id="file_up_tr" valign="top" style='display:none;'>
					 		 	<td align="right">完成凭证:</td>
					 		 	<td align="left">
					 		 		 
						              	<div id="jquery_file_up">	
						              		<input type="hidden" name="file_names" value=""/>
						                
						              	</div>
					 		 	</td>
					 	 </tr>
					 	<%
					 		if(fileArray.length > 0){
					 	%>
					 	<tr valign="top">
					 		<td style="text-align:right;" valign="middle">凭证文件:</td>
					 		<td style="text-align:left;">
					 			<table>
					 				<%
					 					if(fileArray.length > 0){
					 						for(int i = 0; i < fileArray.length; i ++){
					 							DBRow fileRow = fileArray[i];
									%>
										<tr>
											
											<td id="over_file_td">
											 			
											 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	                     <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	                     <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	                 <%if(StringUtil.isPictureFile(fileRow.getString("file_name"))){ %>
				 			 	             <p>
				 			 	 	            <a href="javascript:void(0)" onclick="showPictrueOnline('<%=fileType %>','<%=purchaseRow.get("purchase_id",0l) %>','<%=fileRow.getString("file_name") %>');"><%=fileRow.getString("file_name") %></a>
				 			 	             </p>
			 			 	                 <%} else if(StringUtil.isOfficeFile(fileRow.getString("file_name"))){%>
			 			 	 		          <p>
			 			 	 			          <a href="javascript:void(0)"  file_id='<%=fileRow.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>')" file_is_convert='<%=fileRow.get("file_is_convert",0) %>'><%=fileRow.getString("file_name") %></a>
			 			 	 		          </p>
			 			 	                <%}else{ %>
 		 			 	 	  		           <p><a href='<%= downLoadFileAction%>?file_name=<%=fileRow.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'><%=fileRow.getString("file_name") %></a></p> 
 		 			 	 	                <%} %>
											</td>
										</tr>
									<%	
					 						}
					 					}
					 				%>
					 			</table>
					 		</td>
					 	</tr>
					 	<% } %>
					 	<tr valign="top">
					 		<td style="text-align:right;">备注:</td>
					 		<td>
					 			<textarea style="width:400px;height:185px;" id="context" name="context"></textarea>
					 		</td>
					 	</tr>
					 	<%
					 		if(8 == follow_up_type)
					 		{
					 	%>
					 	<tr valign="top">
					 		<td>&nbsp;</td>
					 		<td>
					 			注:本阶段完成时，请上传打印出来的本公司标签照片作为完成凭证<br/>
					 		</td>
					 	</tr>
					 	<%		
					 		}
					 	%>
					 	<%
					 		if(33 == follow_up_type)
					 		{
					 	%>
					 	<tr valign="top">
					 		<td>&nbsp;</td>
					 		<td>
					 			注:本阶段完成时，请上传打印出来的第三方标签照片作为完成凭证<br/>
					 		</td>
					 	</tr>
					 	<%		
					 		}
					 	%>
					 </table>
				</td>
			</tr>
			<tr width="100%" height="80%" valign="bottom">
				<td align="right">
				 <div class="buttonDiv" style="margin-top:5px;"> 
				 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
						<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
				  </div>
				</td>
			</tr>
		</table>
		 
 	 
 	  </form>		
 	  		 
 	<script type="text/javascript">
 	function cancel(){
 		$.artDialog && $.artDialog.close();
 	}
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script> 
	 
</body>
</html>