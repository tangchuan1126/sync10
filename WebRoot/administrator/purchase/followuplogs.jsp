<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id = StringUtil.getLong(request,"purchase_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>采购单号：<%=purchase_id %></title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>

</head>

<body >
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:15px;">
  	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
				<span style="font-family:'黑体'; font-size:25px;"></span><br/>
            </td>
          </tr>
        </table>
	    
	  <br>
  	  <table width="98%" height="100%"  border="0" cellpadding="0" cellspacing="0">
  	    <tr>
  	      <td  align="left" valign="top">
  	        <div class="demo">
  	          <div id="tabs">
  	            <ul>
  	              <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>">全部日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=1">进度日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=2">财务日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=3">修改日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=4">发票日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=5">退税日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=8">内部标签日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=33">第三方标签日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=13">质检要求日志<span> </span></a></li>
				  <li><a href="followuplogs_tright.html?purchase_id=<%=purchase_id %>&followup_type=23">商品范例日志<span> </span></a></li>
			    </ul>
			  </div>
		    </div>
				  <script>
					$("#tabs").tabs({
						cache: true,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						cookie: { expires: 30000 } ,
						load: function(event, ui) {onLoadInitZebraTable();}	
					});
					</script>	</td>
	    </tr>
      </table>
  </td>
 </tr>
<%-- <tr>--%>
<%-- 	<td align="right" class="win-bottom-line">--%>
<%--     <input type="button" name="Submit2" value="关闭" class="normal-white" onclick="closeDialogCancel();"/>--%>
     </td>
<%-- </tr>--%>
</table>
<script type="text/javascript">
function closeDialogCancel(){
	//alert($.artDialog);
	//alert($.artDialog.close());
	//$.artDialog && $.artDialog.close();
	parent.closeWin();
};
</script>
</body>
</html>

