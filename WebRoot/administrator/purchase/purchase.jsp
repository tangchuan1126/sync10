<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.PurchaseLogTypeKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryApplyMoneyKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<% 
	String cmd = StringUtil.getString(request,"cmd");
	
	String product_line_id = StringUtil.getString(request, "product_line_id");
	String process_key  = StringUtil.getString(request, "process_key");

	long supplier_id = StringUtil.getLong(request,"supplier",0);
	long productline_id = StringUtil.getLong(request,"productline",0l);
	long ps_id = StringUtil.getLong(request,"ps",0);
	long day = StringUtil.getLong(request,"day",3);
	
	int purchase_status = StringUtil.getInt(request,"purchase_status",0);
	int money_status = StringUtil.getInt(request,"money_status",-1);
	int arrival_time = StringUtil.getInt(request,"arrival_time",0);
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	
	int invoice = StringUtil.getInt(request,"invoice",0);
	int drawback = StringUtil.getInt(request,"drawback",0);
	int need_tag = StringUtil.getInt(request,"need_tag",0);
	int quality_inspection = StringUtil.getInt(request,"quality_inspection",0);
	long purchaseProductLineId		= StringUtil.getLong(request, "purchaseProductLineId");
	String purchaseProductLineName	= StringUtil.getString(request, "purchaseProductLineName");
	int purchaseFollowUpProcessKey	= StringUtil.getInt(request, "purchaseFollowUpProcessKey");
	int purchaseFollowUpActivity	= StringUtil.getInt(request, "purchaseFollowUpActivity");
	
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
	String search_key = StringUtil.getString(request,"search_key");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	String p = StringUtil.getString(request,"p","0");
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
									
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	DBRow[] productline =  productLineMgrTJH.getAllProductLine();//供应商所在的产品线
	
	PurchaseKey purchasekey = new PurchaseKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	DeliveryApplyMoneyKey deliveryApplyMoneyKey = new DeliveryApplyMoneyKey();
	InvoiceKey invoiceKey = new InvoiceKey();
	DrawbackKey drawbackKey = new DrawbackKey();
	TransportTagKey transportTagKey = new TransportTagKey();
	QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();  
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>采购单列表</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
 
-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style>
.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

	<script type="text/javascript">
			jQuery(function($document){
					var productline = "<%=productline_id %>";
					var supplier = "<%=supplier_id%>";
					if(productline != 0){
						getSupplierByProductline(productline,supplier);
					}
					//alert($('#supplier').val()+","+supplier);
					 addAutoComplete($("#searchKey"),
					 		"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/purchase/GetPurchaseJSONAction.action",
					 			"merge_field","purchase_id");
				})
			
			function searchPurchase()
			{
				var val = $("#searchKey").val();
				
				if(val.trim()=="")
				{
					alert("请输入要查询的关键字");
				}
				else
				{
					val = val.replace(/\'/g,'');
					var val_search = "\'"+val+"\'";
					$("#searchKey").val(val_search);
					document.search_form.search_key.value = val_search;
					document.search_form.search_mode.value = 1;
					document.search_form.submit();
				}
			}
			
			function searchPurchaseRightButton()
			{
				var val = $("#searchKey").val();
				
				if (val=="")
				{
					alert("你好像忘记填写关键词了？");
				}
				else
				{
					val = val.replace(/\'/g,'');
					$("#search_key").val(val);
					document.search_form.search_key.value = val;
					document.search_form.search_mode.value = 2;
			 		document.search_form.submit();
				}
			}
			
			function eso_button_even()
			{
				document.getElementById("eso_search").oncontextmenu=function(event) {  
					if (document.all) window.event.returnValue = false;// for IE  
					else event.preventDefault();  
				};  
			
				document.getElementById("eso_search").onmouseup=function(oEvent) {  
					if (!oEvent) oEvent=window.event;  
					if (oEvent.button==2) 
					{  
			         	searchPurchaseRightButton();
			    	}  
				}  
			}
			
			function addPurchase()
			{
				var uri	= '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/add_purchase.html";
				$.artDialog.open(uri,{title:'新增采购单', height:535, width:650, lock:true,opacity:0.3,fixed:true});
				//tb_show('新增采购单','add_purchase.html?TB_iframe=true&height=500&width=800',false);
			}
			 function refreshWindow(){
					window.location.reload();
			  };
			  function refreshWindowAndPurchaseDetail(purchase_id)
				{
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_detail.html?purchase_id="+purchase_id;
					window.location.href = uri;
				}
			function purchaseLogs(purchase_id)
			{
				//tb_show('日志 采购单号:'+purchase_id,'followuplogs.html?purchase_id='+purchase_id+'&TB_iframe=true&height=500&width=800',false);
				//art
				var uri = "followuplogs.html?purchase_id="+purchase_id;
				 $.artDialog.open(uri , {title: "日志采购单号["+purchase_id+"]",width:'900px',height:'560px', lock: true,opacity: 0.3,fixed: true});
			}
			
			function closeWin()
			{
				tb_remove();
				window.location.href="purchase.html";
			}
			
			function closeWinNotRefresh()
			{
				tb_remove();
			}
			
			function go2page(p)
			{
				var uri = "cmd=<%=cmd%>&p="+p+"&supplier_id=<%=supplier_id%>&ps_id=<%=ps_id%>&purchase_status=<%=purchase_status%>";
				uri		+="&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&st=<%=st%>&en=<%=en%>";
				uri		+="&search_key=<%=search_key%>&analysisType=<%=analysisType%>&analysisStatus=<%=analysisStatus%>&day=<%=day%>&productline_id=<%=productline_id%>";
				uri		+="&purchaseProductLineId=<%=purchaseProductLineId%>&purchaseProductLineName=<%=purchaseProductLineName%>";
				uri		+="&purchaseFollowUpProcessKey=<%=purchaseFollowUpProcessKey%>&purchaseFollowUpActivity=<%=purchaseFollowUpActivity%>";
				ajaxLoadPurchasePage(uri);
				//ajaxLoadPurchasePage("cmd=<%=cmd%>&p="+p+"&supplier_id=<%=supplier_id%>&ps_id=<%=ps_id%>&purchase_status=<%=purchase_status%>&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&st=<%=st%>&en=<%=en%>&search_key=<%=search_key%>&analysisType=<%=analysisType%>&analysisStatus=<%=analysisStatus%>&day=<%=day%>&productline_id=<%=productline_id%>");
				//ajaxLoadPurchasePage("cmd=purchaseFollowUpProcessSearch&purchaseProductLineId=<%=purchaseProductLineId%>&purchaseFollowUpProcessKey=<%=purchaseFollowUpProcessKey%>&purchaseFollowUpActivity=<%=purchaseFollowUpActivity%>");
			}
			
			
	function getSupplierByProductline(productlineid,supplier){
		var para = "productline_id="+productlineid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#supplier").clearAll();
					$("#supplier").addOption("全部供应商","0");
					if(data != ""){
						$.each(data,function(i){
							$("#supplier").addOption(data[i].sup_name,data[i].id);
						});
					}
					$('#supplier').val(supplier);
				}
			});
	}
	function purchaseInvoiceState(purchase_id, follow_up_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_invoice_state.html?purchase_id="+purchase_id+"&follow_up_type="+follow_up_type;
		var procedure = "";
		if(4 == follow_up_type){
			procedure = "发票流程";
		}else if(5 == follow_up_type){
			procedure = "退税流程";
		}else if(8 == follow_up_type){
			procedure = "内部标签流程";
		}else if(33 == follow_up_type){
			procedure = "第三方标签流程";
		}
		$.artDialog.open(uri, {title: procedure+'['+purchase_id+']',width:'570px',height:'270px', lock: true,opacity: 0.3});
	}
	function purchaseQualityInspect(purchase_id){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_quality_inspection.html?purchase_id="+purchase_id;
		$.artDialog.open(uri, {title: '质检要求['+purchase_id+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	};
	function purchaseProductModel(purchase_id)
	{
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model.html?purchase_id="+purchase_id;
		$.artDialog.open(uri, {title: '商品范例['+purchase_id+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	}
	function addDeliveryTransportOrder(purchase_id)
	{


		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder") %>action/administrator/purchase/CheckPurchaseTransportApplyPromptAction.action',
			data:'purchase_id='+purchase_id+"&deposit_or_delivery=2",
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.result)
				{
					alert(data.result);
				}
				else
				{
					var url = "../transport/add_purchase_transport.html?purchase_id="+purchase_id;
					$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		});
		
	}
	function addDeliveryTransportOrderOld(purchase_id)
	{
		var url = "../transport/add_purchase_transport.html?purchase_id="+purchase_id;
		$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	function purchaseNeedFollowUpCount(cmdType,productLineId,productLineName){
		$("#follow_up").html("");
		var msg = "";
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/GetPurchaseNeedFollowUpCountAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			data:{cmdType:cmdType,productLineId:productLineId},
			cache:false,
			async:false,
			beforeSend:function(request){
			},
			error: function(){
				
			},
			success: function(data)
			{
				if(1 == cmdType){
					msg += "采购单<a style='text-decoration:none' href='javascript:purchaseNeedFollowUpCount(2,-1,-1)'>("+data.purchase_need_follow_up_count+")</a>";
				}
				else if(2 == cmdType)
				{
					msg += "<a style='text-decoration:none' href='javascript:purchaseNeedFollowUpCount(1,-1,-1)'>产品线:</a>";
					$.each(data, function(i){
						msg += "<a style='text-decoration:none' href='javascript:purchaseNeedFollowUpCount(3,"+data[i].product_line_id+",\""+data[i].product_line_name+"\")'>"+data[i].product_line_name+"("+data[i].purchase_need_follow_count+")</a>";
					})
				}
				else if(3 == cmdType)
				{
					$("#purchaseProductLineName").val(productLineName);
					msg += "<a style='text-decoration:none' href='javascript:purchaseNeedFollowUpCount(2,-1,-1)'>"+productLineName+"</a>:";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.INVOICE%>,<%=InvoiceKey.INVOICING %>)'>发票("+data.purchase_need_invoice+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.DRAWBACK%>,<%=DrawbackKey.DRAWBACKING %>)'>退税("+data.purchase_need_drawback+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.NEED_TAG%>,<%=TransportTagKey.TAG %>)'>内部标签("+data.purchase_need_tag+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.THIRD_TAG%>,<%=TransportTagKey.TAG %>)'>第三方标签("+data.purchase_need_tag_third+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.QUALITY%>,<%=QualityInspectionKey.NEED_QUALITY %>)'>质检要求("+data.purchase_need_quality+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRODUCT_MODEL%>,<%=PurchaseProductModelKey.NEED_PRODUCT_MODEL %>)'>商品范例("+data.purchase_need_product_model+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.OPEN %>)'>需确认价格("+data.purchase_need_affirm_price+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.AFFIRMTRANSFER %>)'>需跟进交货("+data.purchase_affirm_price+")</a>";
					msg += "<a style='text-decoration:none' href='javascript:purchaseFollowUpProcessSearch("+productLineId+",<%=PurchaseLogTypeKey.PRICE%>,<%=PurchaseKey.APPROVEING %>)'>到货完成审核中("+data.purchase_arrival_examine+")</a>";
				}
				$("#follow_up").html(msg);
			}		
		});
	};
	function purchaseFollowUpProcessSearch(productLineId, processKey, activity){
		$("#cmd").val("purchaseFollowUpProcessSearch");
		$("#purchaseProductLineId").val(productLineId);
		$("#purchaseFollowUpProcessKey").val(processKey);
		$("#purchaseFollowUpActivity").val(activity);
		$("#purchaseFollowUpSearch").submit();
	};

	</script>
</head>

<body >
			

<div class="demo" style="width:98%">
<form name="purchaseFollowUpSearch" id="purchaseFollowUpSearch" method="post" action="purchase.html">
	<input type="hidden" name="cmd" id="cmd" value='<%=cmd %>'/>
	<input type="hidden" name="purchaseProductLineId" id="purchaseProductLineId" value='<%=purchaseProductLineId %>'/>
	<input type="hidden" name="purchaseFollowUpProcessKey" id="purchaseFollowUpProcessKey" value='<%=purchaseFollowUpProcessKey %>'/>
	<input type="hidden" name="purchaseFollowUpActivity" id="purchaseFollowUpActivity" value='<%=purchaseFollowUpActivity %>'/>
	<input type="hidden" name="purchaseProductLineName" id="purchaseProductLineName" value='<%=purchaseProductLineName %>'/>
</form>
	<div id="tabs">
		<ul>
			<li><a href="#supplier_search">常用工具</a></li>
			<li><a href="#purchase_filter">高级搜索</a></li>
			<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.getPurchaseFOLLOWUP">
			<li><a href="#follow_up">需跟进</a></li>
			<li><a href="#purchase_analysis">采购单监控</a></li>
			</tst:authentication>
		</ul>
		<div id="supplier_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:searchPurchase()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
										<input type="text" name="searchKey" id="searchKey" value="<%=search_key%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" onkeydown="if(event.keyCode==13)searchPurchase()"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
						<script>eso_button_even();</script>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
	              	<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.addPurchase">
			  			<a href="javascript:addPurchase();"><img src="../imgs/create_purchase_bg.jpg" width="129" height="51" border="0"/></a>
			  		</tst:authentication>
				  </td>
	            </tr>
	          </table>
		</div>
		 
		<div id="purchase_filter">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td>
	              	<form action="purchase.html" name="fillter_form">
		              	<table height="61" border="0" cellpadding="0" cellspacing="5" align="left">
							<tr>
								<td align="right">		  
									开始
								</td>
									<td align="left">
										<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
									</td>
									<td align="left">
										<select id="purchase_status" name="purchase_status" style="width:100%">
											<option value="0">主流程</option>
											<option value="<%=PurchaseKey.NOFINISH%>" <%=purchase_status==PurchaseKey.NOFINISH?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.NOFINISH)) %></option>
											<option value="<%=PurchaseKey.OPEN%>" <%=purchase_status==PurchaseKey.OPEN?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.OPEN))%></option>
											<option value="<%=PurchaseKey.AFFIRMTRANSFER%>" <%=purchase_status==PurchaseKey.AFFIRMTRANSFER?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.AFFIRMTRANSFER))%></option>
											<option value="<%=PurchaseKey.APPROVEING%>" <%=purchase_status==PurchaseKey.APPROVEING?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.APPROVEING))%></option>
											<option value="<%=PurchaseKey.CANCEL%>" <%=purchase_status==PurchaseKey.CANCEL?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.CANCEL))%></option>
											<option value="<%=PurchaseKey.FINISH%>" <%=purchase_status==PurchaseKey.FINISH?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.FINISH))%></option>
										</select>
									</td>
									<td align="left">
										<select id="money_status" name="money_status" style="width:100%">
											<option value="-1">财务状态</option>
											<option value="<%=FundStatusKey.NO_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.NO_APPLY_TRANSFER+"")%></option>
									        <option value="<%=FundStatusKey.HAD_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.HAD_APPLY_TRANSFER+"")%></option>
									        <option value="<%=FundStatusKey.PART_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.PART_PAYMENT+"")%></option>
									        <option value="<%=FundStatusKey.FINISH_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.FINISH_PAYMENT+"")%></option>
									        <option value="<%=FundStatusKey.CANCEL %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANCEL+"")%></option>
										</select>
									</td>
									<td align="left">
										<select id="arrival_time" name="arrival_time" style="width:100%">
											<option value="0">到货状态</option>
											<option value="<%=PurchaseArriveKey.NOARRIVE%>" <%=arrival_time==PurchaseArriveKey.NOARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.NOARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.PARTARRIVE%>" <%=arrival_time==PurchaseArriveKey.PARTARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.PARTARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.ALLARRIVE%>" <%=arrival_time==PurchaseArriveKey.ALLARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ALLARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.ARRIVEAPPROVE%>" <%=arrival_time==PurchaseArriveKey.ARRIVEAPPROVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ARRIVEAPPROVE)) %></option>
										</select>
									</td>
									<td align="left">
										<select id="invoice" name="invoice" style="width:100%">
											<option value="0">发票流程</option>
											<%
					   							ArrayList invoiceList = invoiceKey.getInvoices();
					   							
					   							for(int i = 0;i<invoiceList.size();i++)
					   							{
					   						%>
					   						<option <%=invoiceList.get(i).toString().equals(String.valueOf(invoice))?"selected=\"selected\"":""%> value="<%=invoiceList.get(i)%>"><%=invoiceKey.getInvoiceById(invoiceList.get(i).toString())%></option>
					   						<%
					   							}
					   						%>
										</select>
									</td>
									<td>
										<select id="drawback" name="drawback" style="width:100%">
											<option value="0">退税流程</option>
											<%
					   							ArrayList drwabackList = drawbackKey.getDrawbacks();
					   							
					   							for(int i = 0;i<drwabackList.size();i++)
					   							{
					   						%>
					   						<option <%=drwabackList.get(i).toString().equals(String.valueOf(drawback))?"selected=\"selected\"":""%> value="<%=drwabackList.get(i)%>"><%=drawbackKey.getDrawbackById(drwabackList.get(i).toString())%></option>
					   						<%
					   							}
					   						%>
										</select>								
									</td>
									<td>
										<select id="need_tag" name="need_tag" style="width:100%">
											<option value="0">制签流程</option>
											<%
					   							ArrayList transportTagList = transportTagKey.getTransportTags();
					   							
					   							for(int i = 0;i<transportTagList.size();i++)
					   							{
					   						%>
					   						<option <%=transportTagList.get(i).toString().equals(String.valueOf(need_tag))?"selected=\"selected\"":""%> value="<%=transportTagList.get(i)%>"><%=transportTagKey.getTransportTagById(transportTagList.get(i).toString())%></option>
					   						<%
					   							}
					   						%>
										</select>								
									</td>
									<td>
										<select id="quality_inspection" name="quality_inspection" style="width:100%">
											<option value="0">质检要求</option>
					   						<option value="<%=QualityInspectionKey.NO_NEED_QUALITY%>">不需要质检要求</option>
					   						<option value="<%=QualityInspectionKey.NEED_QUALITY%>">质检要求上传中</option>
					   						<option value="<%=QualityInspectionKey.FINISH%>">质检要求完成</option>
					   					</select>								
									</td>
							</tr>
							<tr>
								<td align="right">截止</td>
								<td align="left">
									<input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;"/>
								</td>
								<td align="left">
				              	<select name="productline" id="productline" onChange="getSupplierByProductline(this.value)">
				              		<option value="0">供应商所在产品线</option>
				              		<%
						      		for(int i=0;i<productline.length;i++){
						      		if(productline_id == productline[i].get("id",0l))
						      		{
						      		%>
						      		<option value="<%=productline[i].getString("id") %>" selected="selected"><%=productline[i].getString("name") %></option>
						      		<%
						      		}else{
						      	%>
						      	<option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
						      	<%
						      	 }
						      		}
						      	%>
				              	</select>
								</td>
								
								<td align="left">
				              	<select id="supplier" style="width:100%" name="supplier" onChange="getProductlineBySupplier(this.value)">
				              		<option value="0">供应商</option>
				              	</select>
								</td>
								
								<td align="left">
									<select id="ps" name="ps" style="width:100%">
											<option value="0">仓库</option>
											<%
												for(int j = 0;j<ps.length;j++)
												{
											%>
												<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
											<%
												}
											%>
										</select>
								</td>
								<td>
									<input type="submit" class="button_long_refresh" value="过滤" onclick=""/>
									<input type="hidden" name="cmd" value="fillter"/>
								</td>
							</tr>
						</table>
					</form>
				</td>
	            </tr>
	          </table>
		</div>
		
		<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.getPurchaseFOLLOWUP">
		<div id="follow_up">
<%--			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">--%>
<%--	            <tr>--%>
<%--	              <td>--%>
<%--	              	<form action="purchase.html" name="follow_up">--%>
<%--		              	<table height="61" border="0" cellpadding="0" cellspacing="5" align="left">--%>
<%--							<tr>--%>
<%--								<td>--%>
<%--									<select id="ps" name="ps">--%>
<%--											<option value="0">仓库</option>--%>
<%--											<%--%>
<%--												for(int j = 0;j<ps.length;j++)--%>
<%--												{--%>
<%--											%>--%>
<%--												<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>--%>
<%--											<%--%>
<%--												}--%>
<%--											%>--%>
<%--									</select>--%>
<%--										<select id="purchase_status" name="purchase_status">--%>
<%--											<option value="0">主流程</option>--%>
<%--											<option value="<%=PurchaseKey.NOFINISH%>" <%=purchase_status==PurchaseKey.NOFINISH||purchase_status==0?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.NOFINISH)) %></option>--%>
<%--											<option value="<%=PurchaseKey.OPEN%>" <%=purchase_status==PurchaseKey.OPEN?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.OPEN))%></option>--%>
<%--											<option value="<%=PurchaseKey.AFFIRMTRANSFER%>" <%=purchase_status==PurchaseKey.AFFIRMTRANSFER?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.AFFIRMTRANSFER))%></option>--%>
<%--											<option value="<%=PurchaseKey.APPROVEING%>" <%=purchase_status==PurchaseKey.APPROVEING?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.APPROVEING))%></option>--%>
<%--											<option value="<%=PurchaseKey.CANCEL%>" <%=purchase_status==PurchaseKey.CANCEL?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.CANCEL))%></option>--%>
<%--											<option value="<%=PurchaseKey.FINISH%>" <%=purchase_status==PurchaseKey.FINISH?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.FINISH))%></option>--%>
<%--										</select>--%>
<%--										<select id="arrival_time" name="arrival_time">--%>
<%--											<option value="0">到货状态</option>--%>
<%--											<option value="<%=PurchaseArriveKey.NOARRIVE%>" <%=arrival_time==PurchaseArriveKey.NOARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.NOARRIVE)) %></option>--%>
<%--											<option value="<%=PurchaseArriveKey.PARTARRIVE%>" <%=arrival_time==PurchaseArriveKey.PARTARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.PARTARRIVE)) %></option>--%>
<%--											<option value="<%=PurchaseArriveKey.ALLARRIVE%>" <%=arrival_time==PurchaseArriveKey.ALLARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ALLARRIVE)) %></option>--%>
<%--											<option value="<%=PurchaseArriveKey.ARRIVEAPPROVE%>" <%=arrival_time==PurchaseArriveKey.ARRIVEAPPROVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ARRIVEAPPROVE)) %></option>--%>
<%--										</select>--%>
<%--										<select id="money_status" name="money_status">--%>
<%--											<option value="-1">财务状态</option>--%>
<%--											<option value="<%=FundStatusKey.NO_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.NO_APPLY_TRANSFER+"")%></option>--%>
<%--									        <option value="<%=FundStatusKey.HAD_APPLY_TRANSFER %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.HAD_APPLY_TRANSFER+"")%></option>--%>
<%--									        <option value="<%=FundStatusKey.PART_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.PART_PAYMENT+"")%></option>--%>
<%--									        <option value="<%=FundStatusKey.FINISH_PAYMENT %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.FINISH_PAYMENT+"")%></option>--%>
<%--									        <option value="<%=FundStatusKey.CANCEL %>"><%=fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANCEL+"")%></option>--%>
<%--										</select>--%>
<%--								</td>--%>
<%--								<td>--%>
<%--									<input type="text" name="day" id="day" value="<%=day%>" size="4"/>天内未跟进--%>
<%--								</td>--%>
<%--								<td>--%>
<%--									<input type="submit"" class="button_long_refresh" value="跟进" onclick=""/>--%>
<%--									<input type="hidden" name="cmd" value="followup"/>--%>
<%--								</td>		  --%>
<%--							</tr>--%>
<%--						</table>--%>
<%--					</form>--%>
<%--				</td>--%>
<%--	            </tr>--%>
<%--	          </table>--%>
		</div>
		
		<div id="purchase_analysis">
		<form action="purchase.html">
			<input type="hidden" id="cmd" name="cmd" value="analysis"/>
			<table width="100%">
				<tr>
					<td align="left">
						开始:<input name="st" type="text" id="st1"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						截止:<input name="en" type="text" id="en1" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						<select id="analysisStatus" name="analysisStatus">
							<option value="1" <%=analysisStatus==1?"selected":"" %>>监控</option>
							<option value="2" <%=analysisStatus==2?"selected":"" %>>统计</option>
						</select>
						监控分类:
						<select id="analysisType" name="analysisType">
							<option value="1" <%=analysisType==1?"selected":"" %>>价格确认过程</option>
							<option value="2" <%=analysisType==2?"selected":"" %>>申请资金过程</option>
							<option value="3" <%=analysisType==5?"selected":"" %>>采购单完成</option>
						</select>
						大于
						<input type="text" id="day" name="day" value="<%=day %>" size="2"/>
						天
						<input type="submit" class="button_long_refresh" value="监控"/>
					</td>
				</tr>
			</table>
		</form>
		</div>
		</tst:authentication>
	</div>
</div>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#st1").date_input();
	$("#en1").date_input(); 
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		show:function(event,ui)
		 {
		 	if(ui.index==2)
		 	{
		 		<%
		 			if(cmd.equals("purchaseFollowUpProcessSearch"))
		 			{
		 		%>
		 			purchaseNeedFollowUpCount(3,$("#purchaseProductLineId").val(),$("#purchaseProductLineName").val());
		 		<%	
		 			}
		 			else
		 			{
		 		%>
		 			purchaseNeedFollowUpCount(1,-1);
		 		<%
		 			}
		 		%>
		 	}
		 }
	});
</script>

<form action="purchase.html" name="search_form" method="get">
	<input type="hidden" name="search_key" id="search_key"/>
	<input type = "hidden" name="search_mode" id="search_mode" />
	<input type="hidden" name="cmd" value="search"/>
</form>

<form action="purchase.html" name="supplier_filter_form" method="get">
	<input type="hidden" name="supplier_id" id="supplier_id"/>
</form>

<form name="dataForm" action="purchase.html">
    <input type="hidden" name="p"/>
    <input type="hidden" name="cmd" value="<%=cmd%>"/>
    <input type="hidden" name="search_key" value="<%=search_key%>"/>
    <input type="hidden" name="purchase_status" value="<%=purchase_status%>"/>
    <input type="hidden" name="money_status" value="<%=money_status%>"/>
    <input type="hidden" name="arrival_time" value="<%=arrival_time%>"/>
    <input type="hidden" name="supplier" value="<%=supplier_id%>"/>
    <input type="hidden" name="ps" value="<%=ps_id%>"/>
    <input type="hidden" name="st" value="<%=st%>"/>
    <input type="hidden" name="en" value="<%=en%>"/>
    <input type="hidden" name="analysisType" value="<%=analysisType%>"/>
    <input type="hidden" name="analysisStatus" value="<%=analysisStatus%>"/>
    <input type="hidden" name="day" value="<%=day%>"/>
    <input type="hidden" name="invoice" value="<%=invoice%>"/>
    <input type="hidden" name="drawback" value="<%=drawback%>"/>
    <input type="hidden" name="need_tag" value="<%=need_tag%>"/>
    <input type="hidden" name="quality_inspection" value="<%=quality_inspection%>"/>
</form>
<script type="text/javascript">
			function promptCheck(v,m,f)
			{
				if (v=="y")
				{
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 
					 return true;
				}
			}
			
			function followup(purchase_id)
			{
			 	//art
			 	 var uri = "purchase_log_add.html?purchase_id="+purchase_id;
				 $.artDialog.open(uri , {title: "跟进价格与交货["+purchase_id+"]",width:'450px',height:'350px', lock: true,opacity: 0.3,fixed: true});
				return ;
				
			}
			
			function remark(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>备注</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/logsPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = "备注:"+f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function cancelPurchase(purchase_id)
			{
				if(confirm("确定取消P"+purchase_id+"采购单吗？"))
				{
					document.cancel_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/cancelPurchase.action";
					document.cancel_form.purchase_id.value = purchase_id;
					document.cancel_form.submit();
				}
			}
			
			function finish(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>强制完成</div><br />完成理由:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/finishPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function supplierfilter()
			{
				document.supplier_filter_form.supplier_id.value = $("#supplier").getSelectedValue();
				
				document.supplier_filter_form.submit(); 
			}
			
			$("#st").date_input();
			$("#en").date_input();
</script>
<br/>
<div id="purchaseList">
</div>
<script type="text/javascript">
	var t = null;
	var interval = <%=systemConfig.getStringConfigValue("order_flush_second")%>;
	var leaveSec = interval;
	
	function applicationApprove(purchase_id)
	  {
	  	if(confirm("确定申请此采购单完成"))
	  	{
	  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
	  		document.application_approve.purchase_id.value = purchase_id;
	  		document.application_approve.submit();
	  	}
	  }
	  
	  $.blockUI.defaults = {
			css: { 
				padding:        '8px',
				margin:         0,
				width:          '170px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #999999',
				backgroundColor:'#eeeeee',
				'-webkit-border-radius': '10px',
				'-moz-border-radius':    '10px',
			    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	    		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			},
			
			// 设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.8' 
			},
		baseZ: 99999, 
	    centerX: true,
	    centerY: true, 
		
		fadeOut:  1000
	};
	  
	  function ajaxLoadPurchasePage(para)
		{
			//alert(para);
		
			$.ajax({
				url: 'purchase_tright.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#purchaseList").html(html);
					//onLoadInitZebraTable();//表格斑马线
					$.scrollTo(0,500);//页面向上滚动
				}
			});
		
			clearTimeout(t);
			t = setTimeout("ajaxLoadPurchasePage('"+para+"')",interval);
		
		}
		
		<%
		//这里处理默认打开页面和backurl返回路径
		if(cmd.equals("fillter"))
		{
		%>
			ajaxLoadPurchasePage("cmd=fillter&supplier_id=<%=supplier_id%>&ps_id=<%=ps_id%>&purchase_status=<%=purchase_status%>&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&st=<%=st%>&en=<%=en%>&p=<%=p%>&productline_id=<%=productline_id%>&invoice=<%=invoice%>&drawback=<%=drawback%>&need_tag=<%=need_tag%>&quality_inspection<%=quality_inspection%>");
		<%
		}
		else if(cmd.equals("followup"))
		{
		%>
			ajaxLoadPurchasePage("cmd=followup&p=<%=p%>&ps_id=<%=ps_id%>&day=<%=day%>&purchase_status=<%=purchase_status%>&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&productline_id=<%=productline_id%>");
		<%
		}
		else if(cmd.equals("analysis"))
		{
		%>
			ajaxLoadPurchasePage("cmd=analysis&p=<%=p%>&st=<%=st%>&en=<%=en%>&analysisType=<%=analysisType%>&analysisStatus=<%=analysisStatus%>&day=<%=day%>");
		<%
		}
		else if(cmd.equals("search"))
		{
		%>
			ajaxLoadPurchasePage("cmd=search&p=<%=p%>&search_key=<%=search_key%>&search_mode=<%=search_mode%>");
		<%
		}
		else if(cmd.equals("purchaseFollowUpProcessSearch"))
		{
		%>	
		ajaxLoadPurchasePage("cmd=purchaseFollowUpProcessSearch&purchaseProductLineId=<%=purchaseProductLineId%>&purchaseProductLineName=<%=purchaseProductLineName%>&purchaseFollowUpProcessKey=<%=purchaseFollowUpProcessKey%>&purchaseFollowUpActivity=<%=purchaseFollowUpActivity%>");
		<%	
		}
		else 
		{
		%>
			ajaxLoadPurchasePage("st=<%=st%>&en=<%=en%>");
		<%
		}
		%>
</script>
<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id"/>
</form>
 

<form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_ptype" value="0"/>
	<input type="hidden" name="followup_content"/>
</form>
 

<form name="cancel_form" method="post">
	<input type="hidden" name="purchase_id"/>
</form>
</body>
</html>