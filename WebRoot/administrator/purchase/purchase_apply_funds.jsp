<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@ include file="../../include.jsp"%> 
<%
	String purchase_id = StringUtil.getString(request,"purchase_id");
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
	String type = StringUtil.getString(request,"type");
	
	ApplyTypeKey applyTypeKey = new ApplyTypeKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>资金申请</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
<!--
var va = ['一','二','三','四','五','六','七','八','九'];
function btnSubmit(){

	var isCheck=checkData();
   if(isCheck)
   {
     $.post(
     "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/addApplyMoneyAction.action",
	   {type:$("#types").val(),association_id:$("#association_id").val(),amount:$("#amount").val(),payee:$("#payee").val(),payment_information:$("#payment_information").val(),remark:$("#payment_information").val()},
	   function (data)
	   {
	      window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html";
	   }
	   )
   }	    
}
function addUSD(){
	//r_name,r_account,swift,r_address,r_bank_name,r_bank_address
	var len = $(".usd").length;	 
	var table =  "<table class='receiver usd next'>";
	table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder' name='r_name'/></td>";
	table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
	table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder' name='r_account'/></td></tr>";
	table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder' name='swift'/></td></tr>";
	table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder' name='r_address'></td></tr>";
	table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder' name='r_bank_name'></td></tr>" ;
	table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder' name='r_bank_address'></td></tr></table>" ;

 	//	
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function addRMB(){
	var len  = $(".rmb").length ;
	var table =  "<table class='receiver rmb next'>";
	table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td>";
	table += "<td><input type='text' name='r_name' class='noborder'/></td>";
	table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

	table += "<tr><td class='left'>收款人账号:</td>";
	table += "<td><input type='text' name='r_account' class='noborder'/></td></tr>";
	table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='r_bank' class='noborder'/></td></tr>";
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='r_phone' class='noborder'/></td></tr></table>";
 	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}

//-->
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">资金申请单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr>
		          <td align="right" class="STYLE2">类型:</td>
		          <td>&nbsp;</td>
		          <td>		          
		          <%=applyTypeKey.getApplyTypeStatusById(type)%>
		          </td>
		        </tr>
		    <tr> 
			
		    <td width="12%" align="right"><span class="STYLE2">关联单号:</span></td>
		      <td width="2%">&nbsp;</td>
		      <td>
		       <%=purchase_id%>
			   </td>
		    </tr>
		  <tr> 
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td><input name="amount" type="text" id="amount" size="15" value='<%=purchaseMgr.getPurchasePrice(Long.valueOf(purchase_id))%>'></td>
		    </tr>
				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input type="text" style="width:300px;" id="payee" name="payee" value='<%=supplier.getString("sup_name")%>'/>
		      </td>
		    </tr>		 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      		<h1 id="addBar"> 
		      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
		      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
		      		</h1>
		      		<table class="receiver rmb next">
		      			<tr>
		      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息一</td>
		      				<td class="left">收款人户名:</td>
		      				<td><input type="text" name="r_name" class="noborder"/></td>
		      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人账号:</td>
		      				<td><input type="text" name="r_account" class="noborder"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款开户行:</td>
		      				<td><input type="text" name="r_bank" class="noborder"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人电话:</td>
		      				<td><input type="text" name="r_phone" class="noborder"/></td>
		      			</tr>
		      		</table>
		      		  <!--  
		      		<table class="receiver usd">
		      			<tr>
		      				<td rowspan="7" style='width:150px;text-align:center;'>外币收款信息</td>
		      				<td class="left">收款人户名:</td>
		      				<td><input type="text" class="noborder" name="r_name"/></td>
		      				<td rowspan="7" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)">删除</span></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人账号:</td>
		      				<td><input type="text" class="noborder" name="r_account"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">SWIFT:</td>
		      				<td><input type="text" class="noborder" name="swift"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款人地址:</td>
		      				<td><input type="text" class="noborder" name="r_address"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款行名称:</td>
		      				<td><input type="text" class="noborder" name="r_bank_name"/></td>
		      			</tr>
		      			<tr>
		      				<td class="left">收款行地址:</td>
		      				<td><input type="text" class="noborder" name="r_bank_address"/></td>
		      			</tr>
		      		</table>
		          	    	-->  
		          <p>
		          		<span style='font-weight:bold;'>参考账号信息: </span>
		      	<%=supplier.getString("bank_information")%>
		          </p>
		      </td>
		    </tr>			    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="80"></textarea></td>
		    </tr>
		</table>
		
	</fieldset>	
	</td>
  </tr>

</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
	function submitApply()
	{
			var str  = ""
			var rmbInfo = $(".rmb");
			if(rmbInfo.length > 0 ){
				for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
					var _table = $(rmbInfo.get(index));
					var r_name =  $("input[name='r_name']",_table).val();
					var r_account = $("input[name='r_account']",_table).val();
					var r_bank = $("input[name='r_bank']",_table).val();
					var r_phone = $("input[name='r_phone']",_table).val();
					str +="\n";
					if($.trim(r_name).length > 0 ){
						str += "收款人户名:"+r_name+"\n";
					}else{
						alert("输入收款人户名!");
						return ;
					}
					if($.trim(r_account).length > 0 ){
						str += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return ;
					}
					if($.trim(r_bank).length > 0 ){
						str += "收款开户行:"+r_bank+"\n";
					}
					if($.trim(r_phone).length > 0 ){
						str += "联系电话:"+r_phone+"\n";
					}	
				}
			}
			 str += getUsdPayMentInfo();
		 	 // 计算组织出所有收款信息
		 	 if(str.length <= 4){
				alert("请先输入收款人信息");
				return ;
			 }
		 	 if($("#payee").val().trim().length < 1){
		 		$("#payee").focus();
		 		alert("输入供应商的名称!");
					return ;
			 }
			 if($("#amount").val().trim()==""||$("#amount").val()==null)
			 {
			     $("#amount").focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($("#amount").val().trim()))
		 	 {
			     $("#amount").focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($("#amount").val().trim()<=0)
			 {
			 	 $("#amount").focus();
			     alert("金额必须大于零！");
			 }
			 else if($("#remark").val().trim()==""||$("#remark").val()==null)
			 {
			     $("#remark").focus();
			     alert("请填写备注！");
			 }
			 else if($("#remark").val().trim().length>200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
				var o = {
						remark:"资金申请:"+$("#remark").val(),
						amount:$("#amount").val(),
						supplier_id:<%=supplier.get("id",0l)%>,
						payee: $("#payee").val(),
						payment_information:str
			 }
				$.artDialog.opener.submitApplyFundsForm && $.artDialog.opener.submitApplyFundsForm(o);
			 }
	
	}
			function getUsdPayMentInfo(){
				var str  = "";
				var usdInfo = $(".usd");
				if(usdInfo.length > 0 ){
					for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
					//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
					//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
						var _table = $(usdInfo.get(index));
						var r_name =  $("input[name='r_name']",_table).val();
						var r_account = $("input[name='r_account']",_table).val();
						var swift = $("input[name='swift']",_table).val();
						var r_address = $("input[name='r_address']",_table).val();
						var r_bank_name = $("input[name='r_bank_name']",_table).val();
						var r_bank_address = $("input[name='r_bank_address']",_table).val();
						str +="\n";
						if($.trim(r_name).length > 0 ){
							str += "收款人户名:"+r_name+"\n";
						}else{
							alert("输入收款人户名!");
							return ;
						}
						if($.trim(r_account).length > 0 ){
							str += "收款人账号:"+r_account+"\n";
						}else{
							alert("输入收款人账号!");
							return ;
						}
						if($.trim(swift).length > 0 ){
							str += "SWIFT:"+swift+"\n";
						}
						if($.trim(r_address).length > 0 ){
							str += "收款人地址:"+r_address+"\n";
						}
						if($.trim(r_bank_name).length > 0 ){
							str += "收款行名称:"+r_bank_name+"\n";
						}
						if($.trim(r_bank_address).length > 0 ){
							str += "收款行地址:"+r_bank_address+"\n";
						}
					}
					return str;
				}
				return "" ;
			}
			function deletePaymentInfo(_this){
				var parentNode = $(_this).parent().parent().parent().parent();
				if($(".receiver").length == 1){
					alert("至少包含一个收款信息");
					return ;
				}
 				 parentNode.remove();
			}
</script>
</body>
</html>
