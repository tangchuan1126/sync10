<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="java.text.SimpleDateFormat"%>
 <jsp:useBean id="qualityInspectionKey" class="com.cwc.app.key.QualityInspectionKey"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../include.jsp"%>
<%
	String purchase_id = StringUtil.getString(request, "purchase_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
<link href="../comm.css" rel="stylesheet" type="text/css">
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<script type="text/javascript">
	function closeDialog(){$.artDialog && $.artDialog.close();};
</script>
</head>
<body onload="onLoadInitZebraTable();">
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:15px;">
	<table id="tableLog" width="98%" border="0" align="center" height="100%" cellpadding="0" cellspacing="0" class="zebraTable" onload="onLoadInitZebraTable();">
		     <tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] state_type = {ProcessKey.QUALITY_INSPECTION};
		    	DBRow [] logRows = purchaseMgrIfaceZr.getPurchaseLogsByPurchaseIdAndType(Long.parseLong(purchase_id), state_type);
		     	if(logRows.length > 0){
		     		for(int i = 0; i < logRows.length; i ++){
		     			DBRow logRow = logRows[i];
		     %>			
		     			 <tr height="30px" style="padding-top:3px;">
					     	<td><%=null == adminMgrLL.getAdminById(logRow.getString("follower_id"))?"":adminMgrLL.getAdminById(logRow.getString("follower_id")).getString("employe_name") %></td>
					     	<td><%=logRow.getString("followup_content") %></td>
					     	<td>
					     	<% if(!"".equals(logRow.getString("followup_expect_date"))){
					     		out.println(DateUtil.FormatDatetime("yy-MM-dd",new SimpleDateFormat("yyyy-MM-dd").parse(logRow.getString("followup_expect_date"))));
					        	}
					        %>
					     	</td>
					     	<td>
					     		<% if(!"".equals(logRow.getString("followup_date"))){
					     			out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(logRow.getString("followup_date"))));
					     		}
					     		%>
					     	</td>
		  					<td>
		  						<%=qualityInspectionKey.getQualityInspectionById(logRow.get("followup_type_sub",0)) %>
		  					</td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		     <%		
		     	}
			%>
	     </table>
	     </td>
	  </tr>
	  <tr>
	 	<td align="right" valign="bottom">				
		  <table width="100%">
	     	<tr>
				<td colspan="2" align="right" valign="middle" class="win-bottom-line">		
				 <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeDialog();">
				</td>
			</tr>
	     </table>
	   </td>
	  </tr>
	 </table>
</body>
</html>