<%@ page contentType="text/html;charset=UTF-8"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<% 
	String cmd = StringUtil.getString(request,"cmd");
	
	long supplier_id = adminMgr.getAdminLoginBean(session).getAttach().get("id",0l);
	long ps_id = StringUtil.getLong(request,"ps",0);
	
	int purchase_status = StringUtil.getInt(request,"purchase_status",0);
	int money_status = StringUtil.getInt(request,"money_status",0);
	int arrival_time = StringUtil.getInt(request,"arrival_time",0);
	
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
	String search_key = StringUtil.getString(request,"search_key");
	
	String p = StringUtil.getString(request,"p","0");
	
	TDate tDate = new TDate();
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
									
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	
	DBRow[] suppliers = supplierMgrTJH.getAllSupplier(null);
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	PurchaseKey purchasekey = new PurchaseKey();
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

	<script language="JavaScript1.2">
			function searchPurchase()
			{
				if($("#searchKey").val().trim()=="")
				{
					alert("请输入要查询的关键字");
				}
				else
				{
					document.search_form.search_key.value=$("#searchKey").val();
					document.search_form.submit();
				}
			}
			
			function addPurchase()
			{
				tb_show('新增采购单','add_purchase.html?TB_iframe=true&height=500&width=800',false);
			}
			
			function purchaseLogs(purchase_id)
			{
				tb_show('日志','followuplogs.html?purchase_id='+purchase_id+'&TB_iframe=true&height=500&width=800',false);
			}
			
			function closeWin()
			{
				tb_remove();
				window.location.href="purchase.html";
			}
			
			function closeWinNotRefresh()
			{
				tb_remove();
			}
			
			function go2page(p)
			{
				ajaxLoadPurchasePage("cmd=<%=cmd%>&p="+p+"&supplier_id=<%=supplier_id%>&ps_id=<%=ps_id%>&purchase_status=<%=purchase_status%>&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&st=<%=st%>&en=<%=en%>&search_key=<%=search_key%>");
			}
			
	</script>
</head>

<body >
			
<br/>

<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="56%" class="page-title" style="border-bottom:0px;" align="left"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 采购管理 »   采购单列表 </td>
    <td width="44%" align="right" ><SPAN id="loadifo" ></SPAN></td>
  </tr>
</table>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#supplier_search">常用工具</a></li>
			<li><a href="#purchase_filter">高级搜索</a></li>
		</ul>
		<div id="supplier_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:searchPurchase()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
										<input type="text" name="searchKey" id="searchKey" value="<%=search_key%>" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" onkeydown="if(event.keyCode==13)searchPurchase()"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
	              	<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.addPurchase">
			  			<a href="javascript:addPurchase();"><img src="../imgs/create_purchase_bg.jpg" width="129" height="51" border="0"/></a>
			  		</tst:authentication>
				  </td>
	            </tr>
	          </table>
		</div>
		
		<div id="purchase_filter">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td>
	              	<form action="purchase.html" name="fillter_form">
		              	<table height="61" border="0" cellpadding="0" cellspacing="5" align="left">
							<tr>
								<td align="right">		  
								  <%
									
								%>
									开始</td>
									<td align="left">
										<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
									</td>
									<td align="left">
										<select id="purchase_status" name="purchase_status" style="width:100%">
											<option value="0">全部流程</option>
											<option value="<%=PurchaseKey.OPEN%>" <%=purchase_status==PurchaseKey.OPEN?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.OPEN))%></option>
											<option value="<%=PurchaseKey.LOCK%>" <%=purchase_status==PurchaseKey.LOCK?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.LOCK))%></option>
											<option value="<%=PurchaseKey.AFFIRMPRICE%>" <%=purchase_status==PurchaseKey.AFFIRMPRICE?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.AFFIRMPRICE))%></option>
											<option value="<%=PurchaseKey.AFFIRMTRANSFER%>" <%=purchase_status==PurchaseKey.AFFIRMTRANSFER?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.AFFIRMTRANSFER))%></option>
											<option value="<%=PurchaseKey.DELIVERYING%>" <%=purchase_status==PurchaseKey.DELIVERYING?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.DELIVERYING))%></option>
											<option value="<%=PurchaseKey.APPROVEING%>" <%=purchase_status==PurchaseKey.APPROVEING?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.APPROVEING))%></option>
											<option value="<%=PurchaseKey.CANCEL%>" <%=purchase_status==PurchaseKey.CANCEL?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.CANCEL))%></option>
											<option value="<%=PurchaseKey.FINISH%>" <%=purchase_status==PurchaseKey.FINISH?"selected":""%>><%=purchasekey.getQuoteStatusById(String.valueOf(PurchaseKey.FINISH))%></option>
										</select>
									</td>
									<td align="left">
										<select id="money_status" name="money_status" style="width:100%">
											<option value="0">全部转款状态</option>
											<option value="<%=PurchaseMoneyKey.NOTTRANSFER%>" <%=money_status==PurchaseMoneyKey.NOTTRANSFER?"selected":""%>><%=purchasemoneykey.getQuoteStatusById(String.valueOf(PurchaseMoneyKey.NOTTRANSFER))%></option>
											<option value="<%=PurchaseMoneyKey.PARTTRANSFER%>" <%=money_status==PurchaseMoneyKey.PARTTRANSFER?"selected":""%>><%=purchasemoneykey.getQuoteStatusById(String.valueOf(PurchaseMoneyKey.PARTTRANSFER))%></option>
											<option value="<%=PurchaseMoneyKey.ALLTRANSFER%>" <%=money_status==PurchaseMoneyKey.ALLTRANSFER?"selected":""%>><%=purchasemoneykey.getQuoteStatusById(String.valueOf(PurchaseMoneyKey.ALLTRANSFER))%></option>
											<option value="<%=PurchaseMoneyKey.CANNOTTRANSFER%>" <%=money_status==PurchaseMoneyKey.CANNOTTRANSFER?"selected":""%>><%=purchasemoneykey.getQuoteStatusById(String.valueOf(PurchaseMoneyKey.CANNOTTRANSFER))%></option>
											
										</select>
									</td>
									<td align="left">
										<select id="arrival_time" name="arrival_time" style="width:100%">
											<option value="0">全部到货状态</option>
											<option value="<%=PurchaseArriveKey.NOARRIVE%>" <%=arrival_time==PurchaseArriveKey.NOARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.NOARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.PARTARRIVE%>" <%=arrival_time==PurchaseArriveKey.PARTARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.PARTARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.ALLARRIVE%>" <%=arrival_time==PurchaseArriveKey.ALLARRIVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ALLARRIVE)) %></option>
											<option value="<%=PurchaseArriveKey.ARRIVEAPPROVE%>" <%=arrival_time==PurchaseArriveKey.ARRIVEAPPROVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.ARRIVEAPPROVE)) %></option>
											<option value="<%=PurchaseArriveKey.WAITAPPROVE%>" <%=arrival_time==PurchaseArriveKey.WAITAPPROVE?"selected":""%>><%=purchasearrivekey.getQuoteStatusById(String.valueOf(PurchaseArriveKey.WAITAPPROVE)) %></option>
										</select>
									</td>
								
							</tr>
							<tr>
								<td align="right">截止</td>
								<td align="left"><input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" /></td>
								<td align="left">
									<select id="ps" name="ps" style="width:100%">
											<option value="0">全部仓库</option>
											<%
												for(int j = 0;j<ps.length;j++)
												{
											%>
												<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
											<%
												}
											%>
										</select>
								</td>
								<td>
									<input type="submit"" class="button_long_refresh" value="过滤" onclick=""/>
									<input type="hidden" name="cmd" value="fillter"/>
								</td>
								<td align="left">
				              		&nbsp;
								</td>
							</tr>
						</table>
					</form>
				</td>
	            </tr>
	          </table>
		</div>
	</div>
</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>

<form action="purchase.html" name="search_form" method="get">
	<input type="hidden" name="search_key" id="search_key"/>
	<input type="hidden" name="cmd" value="search"/>
</form>

<form action="purchase.html" name="supplier_filter_form" method="get">
	<input type="hidden" name="supplier_id" id="supplier_id"/>
</form>

<form name="dataForm" action="purchase.html">
    <input type="hidden" name="p"/>
    <input type="hidden" name="cmd" value="<%=cmd%>"/>
    <input type="hidden" name="search_key" value="<%=search_key%>"/>
    <input type="hidden" name="purchase_status" value="<%=purchase_status%>"/>
    <input type="hidden" name="money_status" value="<%=money_status%>"/>
    <input type="hidden" name="arrival_time" value="<%=arrival_time%>"/>
    <input type="hidden" name="supplier" value="<%=supplier_id%>"/>
    <input type="hidden" name="ps" value="<%=ps_id%>"/>
    <input type="hidden" name="st" value="<%=st%>"/>
    <input type="hidden" name="en" value="<%=en%>"/>    
</form>
<script type="text/javascript">
			function promptCheck(v,m,f)
			{
				if (v=="y")
				{
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 
					 return true;
				}
			}
			
			function followup(purchase_id)
			{
				$.prompt("<div id='title'>跟进</div><br/>备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/followupPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function remark(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>备注</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/logsPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function cancelPurchase(purchase_id)
			{
				if(confirm("确定取消P"+purchase_id+"采购单吗？"))
				{
					document.cancel_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/cancelPurchase.action";
					document.cancel_form.purchase_id.value = purchase_id;
					document.cancel_form.submit();
				}
			}
			
			function finish(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>强制完成</div><br />完成理由:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/finishPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function supplierfilter()
			{
				document.supplier_filter_form.supplier_id.value = $("#supplier").getSelectedValue();
				
				document.supplier_filter_form.submit(); 
			}
			
			$("#st").date_input();
			$("#en").date_input();
</script>
<br/>
<div id="purchaseList">
</div>
<script type="text/javascript">
	var t = null;
	var interval = <%=systemConfig.getStringConfigValue("order_flush_second")%>;
	var leaveSec = interval;
	
	function applicationApprove(purchase_id)
	  {
	  	if(confirm("确定申请此采购单完成"))
	  	{
	  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
	  		document.application_approve.purchase_id.value = purchase_id;
	  		document.application_approve.submit();
	  	}
	  }
	  
	  $.blockUI.defaults = {
			css: { 
				padding:        '8px',
				margin:         0,
				width:          '170px', 
				top:            '45%', 
				left:           '40%', 
				textAlign:      'center', 
				color:          '#000', 
				border:         '3px solid #999999',
				backgroundColor:'#eeeeee',
				'-webkit-border-radius': '10px',
				'-moz-border-radius':    '10px',
			    '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
	    		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			},
			
			// 设置遮罩层的样式
			overlayCSS:  { 
				backgroundColor:'#000', 
				opacity:        '0.8' 
			},
		baseZ: 99999, 
	    centerX: true,
	    centerY: true, 
		
		fadeOut:  1000
	};
	  
	  function ajaxLoadPurchasePage(para)
		{
			//alert(para);
		
			$.ajax({
				url: 'purchase_tright.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
					$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
				
				error: function(){
					$.blockUI({message: '<span style="font-size:12px;color:#666666;font-weight:blod;float:left"><span style="font-weight:bold;font-size:20px;color:#666666;font-family:黑体">加载失败</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>请重试或者重新登录</span>' });
				},
				
				success: function(html){
					$.unblockUI();
					$("#purchaseList").html(html);
					//onLoadInitZebraTable();//表格斑马线
					$.scrollTo(0,500);//页面向上滚动
				}
			});
		
			clearTimeout(t);
			t = setTimeout("ajaxLoadPurchasePage('"+para+"')",interval);
		
		}
		
		<%
		//这里处理默认打开页面和backurl返回路径
		if(cmd.equals("fillter"))
		{
		%>
			ajaxLoadPurchasePage("cmd=fillter&supplier_id=<%=supplier_id%>&ps_id=<%=ps_id%>&purchase_status=<%=purchase_status%>&money_status=<%=money_status%>&arrival_time=<%=arrival_time%>&st=<%=st%>&en=<%=en%>&p=<%=p%>");
		<%
		}
		else if(cmd.equals("followup"))
		{
		%>
			ajaxLoadPurchasePage("cmd=followup&p=<%=p%>&ps_id=<%=ps_id%>");
		<%
		}
		else if(cmd.equals("search"))
		{
		%>
			ajaxLoadPurchasePage("cmd=search&p=<%=p%>&search_key=<%=search_key%>");
		<%
		}
		else 
		{
		%>
			ajaxLoadPurchasePage("st=<%=st%>&en=<%=en%>");
		<%
		}
		%>
</script>
<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>

<form name="cancel_form" method="post">
	<input type="hidden" name="purchase_id"/>
</form>
</body>
</html>