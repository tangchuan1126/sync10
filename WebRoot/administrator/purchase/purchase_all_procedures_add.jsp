<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.CurrencyKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
	String purchase_id		= StringUtil.getString(request, "purchase_id");
	String tempfilename		= StringUtil.getString(request, "tempfilename");
	String expectArrTime	= StringUtil.getString(request, "expectArrTime");
	DBRow purchaseRow		= purchaseMgrLL.getPurchaseById(purchase_id);
%>
<title>采购单的各种流程信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
$(function(){
	$("input:radio[name=hasInvoice]").click(
			function(){
				var hasInvoinceCheck = $(this).val();
				if(2 == hasInvoinceCheck){
					$("#invoiceDetailTr").attr("style","");
					$("#invoicePersonTr").attr("style","");
				}else{
					$("#invoiceDetailTr").attr("style","display:none");
					$("#invoicePersonTr").attr("style","display:none");
				}
			}
	);
	$("input:radio[name=isDrawback]").click(function(){
		var isDrawbackCheck = $(this).val();
		$("#rebate_rate_title") && $("#rebate_rate_title").remove();
		$("#rebate_rate") && $("#rebate_rate").remove();
		$("#dot2") && $("#dot2").remove();
		if(2 == isDrawbackCheck){
			$("#drawbackPersonTr").attr("style","");
			$("#drawBackTd").append($('<span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" style="width:50px;"/><span id="dot2">%</span>'));
		}else{
			$("#drawbackPersonTr").attr("style","display:none");
			$("#rebate_rate_title") && $("#rebate_rate_title").remove();
			$("#rebate_rate") && $("#rebate_rate").remove();
			$("#dot2") && $("#dot2").remove();
		}
	});
	$("input:radio[name=needTag]").click(function(){
		var isNeedTagCheck = $(this).val();
		if(2 == isNeedTagCheck){
			$("#tagPersonTr").attr("style","");
		}else{
			$("#tagPersonTr").attr("style","display:none");
		}
	});
	$("input:radio[name=qualityInspection]").click(function(){
		var isNeedTagCheck = $(this).val();
		if(2 == isNeedTagCheck){
			$("#qualityPersonTr").attr("style","");
		}else{
			$("#qualityPersonTr").attr("style","display:none");
		}
	});
	$("input:radio[name=productModel]").click(function(){
		var isNeedProductModelCheck = $(this).val();
		if(2 == isNeedProductModelCheck){
			$("#productModelPersonTr").attr("style","");
		}else{
			$("#productModelPersonTr").attr("style","display:none");
		}
	});
});

function previousStep(){
	document.previousStepForm.submit();
}


function ajaxSavePurchaseDetail(){
	if("" == $("#adminUserNamesPurchaser").val())
	{
		alert("采购负责人不能为空");
	}
	else if("" == $("#adminUserNamesDispatcher").val())
	{
		alert("调度负责人不能为空");
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" == $("#adminUserNamesInvoice").val())
	{
		alert("发票流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=isDrawback]:checked").val() && "" == $("#adminUserNamesDrawback").val())
	{
		alert("退税流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=needTag]:checked").val() && "" == $("#adminUserNamesTag").val())
	{
		alert("标签流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=qualityInspection]:checked").val() && "" == $("#adminUserNamesQuality").val())
	{
		alert("质检要求流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=productModel]:checked").val() && "" == $("#adminUserNamesProductModel").val()){
		alert("商品范例负责人不能为空");
	}
	else{
		document.proceduresForm.invoice.value = $("input:radio[name=hasInvoice]:checked").val();
		document.proceduresForm.drawback.value = $("input:radio[name=isDrawback]:checked").val();
		document.proceduresForm.need_tag.value = $("input:radio[name=needTag]:checked").val();
		document.proceduresForm.quality_inspection.value = $("input:radio[name=qualityInspection]:checked").val();
		document.proceduresForm.product_model.value = $("input:radio[name=productModel]:checked").val();
		document.proceduresForm.invoince_amount.value = $("#invoince_amount").val();
		document.proceduresForm.invoince_currency.value = $("#invoince_currency").val();		
		document.proceduresForm.invoince_dot.value = $("#invoince_dot").val();
		document.proceduresForm.rebate_rate.value = $("#rebate_rate").val();
		document.proceduresForm.adminUserNamesPurchaser.value = $("#adminUserNamesPurchaser").val();
		document.proceduresForm.adminUserNamesDispatcher.value = $("#adminUserNamesDispatcher").val();
		document.proceduresForm.adminUserNamesInvoice.value = $("#adminUserNamesInvoice").val();
		document.proceduresForm.adminUserNamesDrawback.value = $("#adminUserNamesDrawback").val();
		document.proceduresForm.adminUserNamesTag.value = $("#adminUserNamesTag").val();
		document.proceduresForm.adminUserNamesQuality.value = $("#adminUserNamesQuality").val();
		document.proceduresForm.adminUserNamesProductModel.value = $("#adminUserNamesProductModel").val();
	
		if($("input:checkbox[name=isMailInvoice]").attr("checked")){
			document.proceduresForm.needMailInvoice.value = 2;
		}
		if($("input:checkbox[name=isMessageInvoice]").attr("checked")){
			document.proceduresForm.needMessageInvoice.value = 2;
		}
		if($("input:checkbox[name=isPageInvoice]").attr("checked")){
			document.proceduresForm.needPageInvoice.value = 2;
		}
		if($("input:checkbox[name=isMailDrawback]").attr("checked")){
			document.proceduresForm.needMailDrawback.value = 2;
		}
		if($("input:checkbox[name=isMessageDrawback]").attr("checked")){
			document.proceduresForm.needMessageDrawback.value = 2;
		}
		if($("input:checkbox[name=isPageDrawback]").attr("checked")){
			document.proceduresForm.needPageDrawback.value = 2;
		}
		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.proceduresForm.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.proceduresForm.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.proceduresForm.needPageTag.value = 2;
		}
		if($("input:checkbox[name=isMailQuality]").attr("checked")){
			document.proceduresForm.needMailQuality.value = 2;
		}
		if($("input:checkbox[name=isMessageQuality]").attr("checked")){
			document.proceduresForm.needMessageQuality.value = 2;
		}
		if($("input:checkbox[name=isPageQuality]").attr("checked")){
			document.proceduresForm.needPageQuality.value = 2;
		}
		if($("input:checkbox[name=isMailProductModel]").attr("checked")){
			document.proceduresForm.needMailProductModel.value = 2;
		}
		if($("input:checkbox[name=isMessageProductModel]").attr("checked")){
			document.proceduresForm.needMessageProductModel.value = 2;
		}
		if($("input:checkbox[name=isPageProductModel]").attr("checked")){
			document.proceduresForm.needPageProductModel.value = 2;
		}
		if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" != $("#invoince_amount").val() && 0 == $("#invoince_currency").val()){
			alert("请选择币种");
		}else if(2 == $("input:radio[name=hasInvoice]:checked").val() && 0 != $("#invoince_currency").val() && "" == $("#invoince_amount").val()){
			alert("请填写开票金额");
		}
		else if($("#invoince_amount") && $("#invoince_amount").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_amount").val())){
			alert("开票金额，请填写数值");
		}else if($("#invoince_dot") && $("#invoince_dot").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_dot").val())){
			alert("票点，请填写数值");
		}else if($("#rebate_rate") && $("#rebate_rate").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#rebate_rate").val())){
			alert("退税率，请填写数值");
		}
		else{
			$("#savePurchaseProceduresButtion").attr("disabled", true);
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/PurchaseAllProceduresAddAction.action',
				data:$("#proceduresForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("创建成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("创建失败","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		}
	}
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function adminUserPurchaser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsPurchaser").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowPurchaser'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesPurchaser").val(user_names);
}
function adminUserDispatcher(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsDispatcher").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowDispatcher'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);}
function setParentUserShowDispatcher(user_ids , user_names){
	$("#adminUserIdsDispatcher").val(user_ids);
	$("#adminUserNamesDispatcher").val(user_names);
}
function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowInvoice'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowInvoice(user_ids , user_names){
	$("#adminUserIdsInvoice").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
}
function adminUserDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsDrawback").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowDrawback'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowDrawback(user_ids , user_names){
	$("#adminUserIdsDrawback").val(user_ids);
	$("#adminUserNamesDrawback").val(user_names);
}
function adminUserTag(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowTag'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowTag(user_ids , user_names){
	$("#adminUserIdsTag").val(user_ids);
	$("#adminUserNamesTag").val(user_names);
}
function adminUserQuality(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsQuality").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowQuality'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowQuality(user_ids , user_names){
	$("#adminUserIdsQuality").val(user_ids);
	$("#adminUserNamesQuality").val(user_names);
}
function adminUserProductModel(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsProductModel").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowProductModel'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowProductModel(user_ids , user_names){
	$("#adminUserIdsProductModel").val(user_ids);
	$("#adminUserNamesProductModel").val(user_names);
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

#purchaseTable tr td{
	line-height:30px;
	height:30px;
}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="previousStepForm" action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail_show.html'>
<input type="hidden" name="tempfilename" id="tempfilename" value="<%=tempfilename%>"/>
<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
<input type="hidden" name="expectArrTime" id="expectArrTime" value="<%=expectArrTime %>"/>
</form>
<form action="" id="proceduresForm" name="proceduresForm" method="post" >
<input type="hidden" name="tempfilename" id="tempfilename" value="<%=tempfilename%>"/>
<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
<input type="hidden" name="expectArrTime" id="expectArrTime" value="<%=expectArrTime %>"/>
<!-- 各流程 -->
<input type="hidden" name="invoice"/>
<input type="hidden" name="drawback"/>
<input type="hidden" name="invoince_amount"/>
<input type="hidden" name="invoince_currency"/>
<input type="hidden" name="invoince_dot"/>
<input type="hidden" name="rebate_rate"/>
<input type="hidden" name="need_tag"/>
<input type="hidden" name="quality_inspection"/>
<input type="hidden" name="product_model"/>

<input type="hidden" name="needMailInvoice"/>
<input type="hidden" name="needMessageInvoice"/>
<input type="hidden" name="needPageInvoice"/>
<input type="hidden" name="needMailDrawback"/>
<input type="hidden" name="needMessageDrawback"/>
<input type="hidden"name="needPageDrawback"/>
<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>
<input type="hidden" name="needMailQuality"/>
<input type="hidden" name="needMessageQuality"/>
<input type="hidden" name="needPageQuality"/>
<input type="hidden" name="needMailProductModel"/>
<input type="hidden" name="needMessageProductModel"/>
<input type="hidden" name="needPageProductModel"/>
<!-- 相关人员的ID -->
<input type="hidden" id="adminUserIdsPurchaser" name="adminUserIdsPurchaser"/>
<input type="hidden" id="adminUserIdsDispatcher" name="adminUserIdsDispatcher"/>
<input type="hidden" id="adminUserIdsInvoice" name="adminUserIdsInvoice"/>
<input type="hidden" id="adminUserIdsDrawback" name="adminUserIdsDrawback"/>
<input type="hidden" id="adminUserIdsTag" name="adminUserIdsTag"/>
<input type="hidden" id="adminUserIdsQuality" name="adminUserIdsQuality"/>
<input type="hidden" id="adminUserIdsProductModel" name="adminUserIdsProductModel"/>

<input type="hidden" name="adminUserNamesPurchaser"/>
<input type="hidden" name="adminUserNamesDispatcher"/>
<input type="hidden" name="adminUserNamesInvoice"/>
<input type="hidden" name="adminUserNamesDrawback"/>
<input type="hidden" name="adminUserNamesTag"/>
<input type="hidden" name="adminUserNamesQuality"/>
<input type="hidden" name="adminUserNamesProductModel"/>

</form>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="25px;">
		<td colspan="2">
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
		 <tr>
		<td align="left" style="font-family:'黑体'; font-size:18px;border-bottom:1px solid #999999;color:#000000;">
		采购单各流程信息<br/></td>
		 </tr>
		</table>
		</td>
	</tr>
	<tr height="15px;"><td colspan="2"></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">采购员：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesPurchaser" name="adminUserNamesPurchaser" style="width:180px;" onclick="adminUserPurchaser()"/>
	    </td>
	</tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">调度：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   		<input type="text" id="adminUserNamesDispatcher" name="adminUserNamesDispatcher" style="width:180px;" onclick="adminUserDispatcher()"/>
	    </td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">发票：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    <input type="radio" name="hasInvoice" value="2" checked="checked"/>需要
	    <input type="radio" name="hasInvoice" value="1"/>不需要
	    </td>
	</tr>
	<tr id="invoiceDetailTr" style="" height="25px;">
		<td width="13%" align="right">
			开票金额：
		</td>
		<td width="87%">
			<input id="invoince_amount" name="invoince_amount" style="width:50px;"/>
	    	&nbsp;币种：
	    	<select id="invoince_currency" name="invoince_currency">
	    	<option value="0">请选择</option>
	    	<% 
	    		CurrencyKey currencyKey	= new CurrencyKey();
	    		List currencyList		= currencyKey.CurrencyKeyList();
	    		for(int i = 0; i < currencyList.size(); i ++){
  			%>		
	    			<option value="<%=currencyList.get(i) %>"><%=currencyKey.getCurrencyNameById((String)currencyList.get(i)) %></option>
	    	<%		
	     		}
	    	%>
	  	  </select>
	  	  &nbsp;票点：<input id="invoince_dot" name="invoince_dot" style="width:50px;"/><span id="dot1">%</span>
		</td>
	</tr>
	<tr height="25px;" id="invoicePersonTr" style="">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" style="width:180px;" onclick="adminUserInvoice()"/>
	    	通知：
	   		<input type="checkbox" checked="checked" name="isMailInvoice"/>邮件
	   		<input type="checkbox" checked="checked" name="isMessageInvoice"/>短信
	   		<input type="checkbox" checked="checked" name="isPageInvoice"/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">退税：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap" id="drawBackTd">
	   	<input type="radio" name="isDrawback" value="2" checked="checked"/>需要
	   	<input type="radio" name="isDrawback" value="1"/>不需要
	   	 <span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" style="width:50px;"/><span id="dot2">%</span>
	    </td>
	</tr>
	<tr height="25px;" style="" id="drawbackPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesDrawback" name="adminUserNamesDrawback" style="width:180px;" onclick="adminUserDrawback()"/>
	    	通知：
	   		<input type="checkbox" checked="checked" name="isMailDrawback"/>邮件
	   		<input type="checkbox" checked="checked" name="isMessageDrawback"/>短信
	   		<input type="checkbox" checked="checked" name="isPageDrawback"/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">标签要求：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<input type="radio" name="needTag" value="2" checked="checked"/>需要
	   	<input type="radio" name="needTag" value="1"/>不需要
	    </td>
	</tr>
	<tr height="25px;" style="" id="tagPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" style="width:180px;" onclick="adminUserTag()"/>
	    	通知：
	   		<input type="checkbox" checked="checked" name="isMailTag"/>邮件
	   		<input type="checkbox" checked="checked" name="isMessageTag"/>短信
	   		<input type="checkbox" checked="checked" name="isPageTag"/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">质检要求：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<input type="radio" name="qualityInspection" value="2" checked="checked"/>需要
	   	<input type="radio" name="qualityInspection" value="1"/>不需要
	    </td>
	</tr>
	<tr height="25px;" style="" id="qualityPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesQuality" name="adminUserNamesQuality" style="width:180px;" onclick="adminUserQuality()"/>
	    	通知：
	   		<input type="checkbox" checked="checked" name="isMailQuality"/>邮件
	   		<input type="checkbox" checked="checked" name="isMessageQuality"/>短信
	   		<input type="checkbox" checked="checked" name="isPageQuality"/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">商品范例：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<input type="radio" name="productModel" value="2" checked="checked"/>需要
	   	<input type="radio" name="productModel" value="1"/>不需要
	    </td>
	</tr>
	<tr height="25px;" style="" id="productModelPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesProductModel" name="adminUserNamesProductModel" style="width:180px;" onclick="adminUserProductModel()"/>
	    	通知：
	   		<input type="checkbox" checked="checked" name="isMailProductModel"/>邮件
	   		<input type="checkbox" checked="checked" name="isMessageProductModel"/>短信
	   		<input type="checkbox" checked="checked" name="isPageProductModel"/>页面
		</td>
	</tr>
	<tr>
 	<td valign="bottom" colspan="2">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="right" valign="middle" class="win-bottom-line"> 
 				<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">	
			    <input type="button" id="savePurchaseProceduresButtion" name="Submit2" value="确定" class="normal-green" onClick="ajaxSavePurchaseDetail()"/>
				<input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>

</body>
</html>