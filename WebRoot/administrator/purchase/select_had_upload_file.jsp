<%@ page contentType="text/html;charset=UTF-8" import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.FileSelectTypeKey"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<%
	long file_with_id = StringUtil.getLong(request, "file_with_id");
	int file_with_type = StringUtil.getInt(request, "file_with_type");
	String pc_id	= "".equals(StringUtil.getString(request, "pc_id"))?"0":StringUtil.getString(request, "pc_id");
	String pc_id_single = "".equals(StringUtil.getString(request, "pc_id_single"))?"0":StringUtil.getString(request, "pc_id_single");
	DBRow[] rows	= new DBRow[0];
	if(!"".equals(pc_id_single))
	{
		rows = productMgrZyj.getAllProductFileByPcId(pc_id_single,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id_single, 0);
	}
	else
	{
		rows = productMgrZyj.getAllProductFileByPcId(pc_id,FileWithTypeKey.PRODUCT_SELF_FILE,pc_id, 0);
	}
	int[] file_with_types = {file_with_type};
	String target = StringUtil.getString(request, "target");
	rows  = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(file_with_id, file_with_types);
	DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品文件列表</title>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<!-- jquery UI -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
var scaleImage = function(o, w, h){
	var img = new Image();
	img.src = o.src;
	if(img.width >0 && img.height >0)
	{
		if(img.width/img.height >= w/h)
		{
			if(img.width > w)
			{
				o.width = w;
				o.height = (img.height*w) / img.width;
			}
			else
			{
				o.width = img.width;
				o.height = img.height;
			}
		}
		else
		{
			if(img.height > h)
			{
				o.height = h;
				o.width = (img.width * h) / img.height;
			}
			else
			{
				o.width = img.width;
				o.height = img.height;
			}
		}
	}
} 
function delProductFile(_this)
{
	var parentNode = $(_this).parent().parent();
	parentNode.remove();
}
function topCheckIsChecked(){
	if($("#topCheckbox").attr("checked"))
	{
		$("input:checkbox[name=productCheckbox]").attr("checked", true);
	}
	else
	{
		$("input:checkbox[name=productCheckbox]").attr("checked", false);
	}
}
function searchFileByProductName(pc_id)
{
	$("#pc_id_single").val(pc_id);
	$("#searchProductFileForm").submit();
}
function savePurchaseProductAndFile()
{
	var fileIds = '';
	var checkeds = $("input:checkbox[name=productCheckbox][checked=checked]");
	checkeds.each(function(){
		var _this = $(this);
		fileIds += "," + _this.val();
	})
	fileIds = fileIds.length > 1 ? fileIds.substr(1):"";
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.setFileNamesShow  && $.artDialog.opener.setFileNamesShow(fileIds,'<%=target%>');
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
</script>
<style type="text/css">
ul.ul_p_name{list-style-type:none;margin-left:-44px;}
ul.ul_p_name li{margin-top:1px;margin-left:3px;line-height:25px;height:25px;padding-left:5px;padding-right:5px;float:left;border:1px solid silver;cursor: pointer;}
</style>
</head>
<body onload= "onLoadInitZebraTable()">
<form id="saveProductFileForm" action='' method="post">
	<input type="hidden" name="file_ids" id="file_ids">
	<input type="hidden" name="file_with_id" value='<%=file_with_id %>'/>
	<input type="hidden" name="file_with_type" value='<%=file_with_type %>'/>
	<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
</form>
<form action="" method="post" id="searchProductFileForm">
	<input type="hidden" name="pc_id" value='<%=pc_id %>'/>
	<input type="hidden" name="pc_id_single" id="pc_id_single"/>
	<input type="hidden" name="file_with_id" value='<%=file_with_id %>'/>
	<input type="hidden" name="file_with_type" value='<%=file_with_type %>'/>
</form>
<fieldset style="width:98%;border:2px #cccccc solid;-webkit-border-radius:2px;-moz-border-radius:5px;margin-bottom: 10px; margin-top: 15px;text-align: center;">
	<legend style="font-size:15px;font-weight:normal;color:#999999;">选择文件</legend>
	<table style="width:95%; margin-top: 6px;margin-bottom: 6px; margin-left: 7px;">
		<tr>
			<td width="20%">
			<%
			 	if(productRows != null && productRows.length > 0)
			 	{
			%>
				<span style="cursor: pointer;" onclick="searchFileByProductName('<%=pc_id %>')">关联商品:</span>
			<%
			 	}
			%>
				<input type="button" class="short-short-button" value="提交" onclick="savePurchaseProductAndFile()"/>
			</td>
			<%
			 	if(productRows != null && productRows.length > 0)
			 	{
			%>
			<td style="text-align:left;width:80%;">
				
					<ul class="ul_p_name" style="float:left;">	
				 	<% 
				 		for(DBRow tempRow : productRows){
				 	%>
				 		 	<li onclick="searchFileByProductName(<%=tempRow.get("pc_id",0L) %>)"><%=tempRow.getString("p_name") %></li>
				 	<% 
				 		}
					%>
					</ul>
			</td>
			<%
			 	}
			%>
		</tr>
	</table>
	<table width="95%" border="0" align="center" cellpadding="1" cellspacing="0"  class="zebraTable" style="margin-left:20px;margin-top:5px;">
	  <tr> 
	  	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">
			<input type="checkbox" id="topCheckbox" onclick="topCheckIsChecked()"/>
		</th>
	  	<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">文件</th>
	  	<th width="48%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
        <th width="27%" style="vertical-align: center;text-align: center;" class="right-title">文件类型</th>
       	<th width="5%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
      </tr>
      
      	<%
      		if(rows.length > 0)
      		{
	      		for(int i = 0; i < rows.length; i ++)
	      		{
	      	%>
	   			<tr>
	   				<td>
	   					<input type="checkbox" name="productCheckbox" value='<%=rows[i].getString("file_name") %>'/>
	   				</td>
	   				<td>
	   					<%
	   						if(rows[i].getString("file_name").endsWith(".gif") 
								||rows[i].getString("file_name").endsWith(".jpg")
								||rows[i].getString("file_name").endsWith(".jpeg") 
								||rows[i].getString("file_name").endsWith(".png"))
	   						{
	   					%>
	   						<img style="margin-top: 10px; margin-bottom: 10px;" src='../../upload/purchase/<%=rows[i].getString("file_name") %>' onload="scaleImage(this,80,80)">
	   					<%	
	   						}
	   					%>
					</td>
	   				<td>
	   					<a href='<%= downLoadFileAction%>?file_name=<%=rows[i].getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("file_path_product")%>'><%=rows[i].getString("file_name") %></a>
					</td>
					<td>
						<%
							FileWithTypeKey fileWithTypeKey = new FileWithTypeKey();
							out.println(fileWithTypeKey.getFileWithTypeId(file_with_type));
						%>
					</td>
	   				<td>
	    	  			<input type="button" class="short-short-button-del" onclick="delProductFile(this)" value="删除">
	   				</td>
	   			</tr>
	    <%
	      		}
      		}
      		else
      		{
		%>
			<tr>
				<td colspan="6" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
			</tr>
		<%
		 	}
     	%>
  		 </table>
</fieldset>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
</script>
  </body>
   
</html>
