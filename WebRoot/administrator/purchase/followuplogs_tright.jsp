<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	int followup_type = StringUtil.getInt(request,"followup_type");
	DBRow purchaseRow = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id+"");
	DBRow [] rows;
	
	rows = purchaseMgr.getfollowuplogs(purchase_id,followup_type);
	HashMap followuptype = new HashMap();
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"发票跟进");
	followuptype.put(5,"退税跟进");
	followuptype.put(6,"价格跟进");
	followuptype.put(7,"交货跟进");
	followuptype.put(8,"内部标签");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"采购单质检要求");
	followuptype.put(23,"商品范例记录");
	followuptype.put(33,"第三方标签");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
function closeDialog(){
	$.artDialog && $.artDialog.close();
}
</script>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body >
<form name="transfer_form" method="post">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="7%"  class="left-title " style="vertical-align: center;text-align: center;">操作员</th>
        <th width="11%"  class="left-title " style="vertical-align: center;text-align: center;">记录备注</th>
        <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">预计完成时间</th>
        <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">记录时间</th>
        <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">操作类型</th>
  	</tr>
  	<% 
  		for(int i=0;i<rows.length;i++)
  		{
  	%>
  		<tr align="center" valign="middle">
		    <td height="30"><%=adminMgrLL.getAdminById(rows[i].getString("follower_id")).getString("employe_name") %></td>
		    <td align="left">
		    	<%
		    		out.print(rows[i].getString("followup_content"));
		    	%>	      
		    </td>
		     <td>
		     	<% if(!"".equals(rows[i].getString("followup_expect_date"))){
						out.println(DateUtil.FormatDatetime("yy-MM-dd",new SimpleDateFormat("yyyy-MM-dd").parse(rows[i].getString("followup_expect_date"))));
					}else{
				%>
					&nbsp;
				<%
					}
				%>
			</td>
		    <td>
		    	<% if(!"".equals(rows[i].getString("followup_date"))){
						out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("followup_date"))));
		    		}else{
				%>
					&nbsp;
				<%
					}
				%>
		    </td>
		    <td>
		    	<%
		    	if(4 == rows[i].get("followup_type",0)){
		    	%>	
		    		<%=followuptype.get(rows[i].get("followup_type",0))%>-
		    		<%=invoiceKey.getInvoiceById(String.valueOf(rows[i].get("followup_type_sub",0)))%>
		    	<%	
		    	}else if(5 == rows[i].get("followup_type",0)){
		    	%>	
		    		<%=followuptype.get(rows[i].get("followup_type",0))%>-
		    		<%=drawbackKey.getDrawbackById(rows[i].get("followup_type_sub",0)) %>
		    	<%	
		    	}else if(8 == rows[i].get("followup_type",0)){
		    	%>	
		    		<%=followuptype.get(rows[i].get("followup_type",0))%>-
		    		<%=transportTagKey.getTransportTagById(rows[i].get("followup_type_sub",0)) %>
		    	<%	
		    	}else if(33 == rows[i].get("followup_type",0)){
		    	%>	
		    		<%=followuptype.get(rows[i].get("followup_type",0))%>-
		    		<%=transportTagKey.getTransportTagById(rows[i].get("followup_type_sub",0)) %>
		    	<%	
		    	}else if(12 == rows[i].get("followup_type",0)){
		    	%>	
		    		<%=followuptype.get(rows[i].get("followup_type",0))%>-
		    		<%=transportTagKey.getTransportTagById(purchaseRow.get("need_tag",0)) %>
		    	<%	
		    	}else{
		    	%>
		    		 <%=followuptype.get(rows[i].get("followup_type",0))%>
		    	<%	
		    	}
		    	%>
		   </td>
	  	</tr>
  	<%
  		}
  	%>
 	
</table>
</body>
</html>



