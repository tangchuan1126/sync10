<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String purchase_id = StringUtil.getString(request,"purchase_id");
DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script>
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function modTerms()
	{
		if($("#terms").val().length<100)
		{
			document.mod_terms.purchase_terms.value = $("#terms").val()
			document.mod_terms.submit();
		}
		else
		{
			alert("内容太多，请控制在100个字内");
		}
	}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><br>
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						  <tr>
							<td align="left" style="font-family:'黑体'; font-size:25px;border-bottom:1px solid #999999;color:#000000;">
							填写与供应商约定的条款							</td>
						  </tr>
					  </table>
						<br/>
                        <br>
                        <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;">
							<tr valign="bottom">
							  <td colspan="2"  align="left" class="STYLE1 STYLE2" style="padding:0px;">
							  	<textarea id="terms" name="terms" rows="20" cols="125"><%=purchase.getString("purchase_terms") %></textarea>
							  </td>
						    </tr>
					  </table>
				
				    </td></tr>
				
				<tr>
					<td align="right" valign="bottom">				
						<table width="100%">
							<tr>
								<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
								  <input type="button" name="Submit2" value="确定" class="normal-green" onClick="modTerms();">
								  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWinNotRefresh();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
			<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/modPurchaseTerms.action" method="post" name="mod_terms">
				<input type="hidden" name="purchase_terms" id="purchase_terms"/>
				<input type="hidden" name="purchase_id" value="<%=purchase.get("purchase_id",0l)%>"/>
			</form>
</body>
</html>
