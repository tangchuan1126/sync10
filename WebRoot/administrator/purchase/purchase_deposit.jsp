<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<%@ include file="../../include.jsp"%> 
<%
	String purchase_id = StringUtil.getString(request,"purchase_id");
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchase.getString("supplier")));
	String type = StringUtil.getString(request,"type");
	
	ApplyTypeKey applyTypeKey = new ApplyTypeKey();
	double purchasePrice = purchaseMgr.getPurchasePrice(Long.valueOf(purchase_id));
	double maxMoney =MoneyUtil.round(purchasePrice,2) ;
	double usdCurrency =Double.parseDouble(systemConfig.getStringConfigValue("USD")) ;
	double maxRMBMoney = MoneyUtil.round(purchasePrice + purchasePrice * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")),2);
	double maxUSDMoney = MoneyUtil.round((purchasePrice + purchasePrice * 0.01 * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))) /usdCurrency ,2);
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	long supplier_id = Long.parseLong(purchase.getString("supplier")) ;
	DBRow[] accountInfo = null ;
	if(supplier_id != 0l){
		accountInfo = accountMgrZr.getAccountByAccountIdAndAccountWithType(supplier_id,AccountKey.SUPPLIER);

	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>申请采购单定金</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 时间控件 -->
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script>
    	  
var va = ['一','二','三','四','五','六','七','八','九'];
function btnSubmit(){
   var isCheck=checkData();
   if(isCheck)
   {
     $.post(
     "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/financial_management/addApplyMoneyAction.action",
	   {type:$("#types").val(),association_id:$("#association_id").val(),amount:$("#amount").val(),payee:$("#payee").val(),payment_information:$("#payment_information").val(),remark:$("#payment_information").val()},
	   function (data)
	   {
	      window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html";
	   }
	   )
   }	    
}
function addUSD(){
  //r_name,r_account,swift,r_address,r_bank_name,r_bank_address
	var len = $(".usd").length;	 
	var table =  "<table class='receiver usd next'>";
	table += "<tr><td rowspan='7' style='"+"width:150px;text-align:center;"+"'>外币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td><td><input type='text' class='noborder account_name' name='account_name'/></td>";
	table += "<td rowspan='7' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";
	table += "<tr><td class='left'>收款人账号:</td><td><input type='text' class='noborder account_number' name='account_number'/></td></tr>";
	table +="<tr><td class='left'>SWIFT:</td><td><input type='text' class='noborder account_swift' name='account_swift'/></td></tr>";
	table +="<tr><td class='left'>收款人地址:</td><td><input type='text' class='noborder account_address' name='account_address' /></td></tr>";
	table +="<tr><td class='left'>收款行名称:</td><td><input type='text' class='noborder account_blank' name='account_blank' /></td></tr>" ;
	table +="<tr><td class='left'>收款行地址:</td><td><input type='text' class='noborder account_blank_address' name='account_blank_address' /><input type='hidden' name='account_currency' value='usd'/></td></tr>" ;
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' class='noborder account_phone' name='account_phone'/></td></tr></table>" ;
	//	
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
function addRMB(){
    var len  = $(".rmb").length ;
	var table =  "<table class='receiver rmb next'>";
	table += "<tr><td rowspan='4' style='"+"width:150px;text-align:center;"+"'>人民币收款信息"+va[len]+"</td>";
	table += "<td class='left'>收款人户名:</td>";
	table += "<td><input type='text' name='account_name' class='noborder account_name'/></td>";
	table += "<td rowspan='4' style='width:80px;text-align:center;'><span onclick='deletePaymentInfo(this)' class='_hand'>删除</span></td></tr>";

	table += "<tr><td class='left'>收款人账号:</td>";
	table += "<td><input type='text' name='account_number' class='noborder account_number'/></td></tr>";
	table +="<tr><td class='left'>收款开户行:</td><td><input type='text' name='account_blank' class='noborder account_blank'/></td></tr>";
	table +="<tr><td class='left'>收款人电话:</td><td><input type='text' name='account_phone' class='noborder account_phone'/><input type='hidden' name='account_currency' value='rmb'/></td></tr></table>";
	// 获取最后一个RMB的table 然后添加到他的后面。如果是没有的话,那么就选取H1标签然后添加到他的后面
	var addBar = $(".receiver").last();
	addBar.after($(table));
}
//重新梳理账号信息
function fixAccountInfo(){
	$(".receiver").each(function(index){
		//将里面的name 都重新加上数字
		var fixIndex = index + 1 ;
		 
		var node = $(this);
		var inputs = $("input",node);
	 	for(var inputIndex = 0 , count = inputs.length ; inputIndex < count ; inputIndex++ ){
			var nodeInput = $(inputs[inputIndex]);
			var name = nodeInput.attr("name");
			if(!(isCanChangeName(name))){
				nodeInput.attr("name",name+"_"+fixIndex);
			}
			
	    }

	})
}

function isCanChangeName(name) {
    var r = /[0-9]$/;
    return r.test(name);
}
function changeValue(){
    var amount = $("#amount").val();
	var currency = $("select[name='currency']").val();
	 
	if($.trim(amount).length > 0){
	 	if(currency === "USD"){
	 	  var tempValue = parseFloat(amount) / parseFloat('<%= usdCurrency%>');
	 	  $("#amount").val(decimal(tempValue,2));
	 	}else{
	 	   var tempValue = parseFloat(amount) * parseFloat('<%= usdCurrency%>');
	  	  $("#amount").val(decimal(tempValue,2));
	 	}
	}
}
function changeInitRate(){
    var init_rate = $("#init_rate").val();
    // 读取currency的值
    var currency = $("select[name='currency']").val();
	var showValue = "" ;
    if(currency === "USD"){
		showValue = parseFloat(init_rate) * parseFloat('<%= maxMoney/usdCurrency%>');
	}else{
	    showValue = parseFloat(init_rate) * parseFloat('<%= maxMoney%>');
	}
	$("#amount").val(decimal(showValue,2));
}
jQuery(function($){
    changeInitRate();
})
//文件上传
function uploadFile(_target){
 
    var fileNames = $("input[name='file_names']").val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames){
    $("#file_up_td").html("");
	if($.trim(fileNames).length > 0 ){
	    $("input[name='file_names']").val(fileNames);
	    //将要上传的文件回显出来
	    var arrayFile = fileNames.split(",");
	    var as = "";
	    for(var index = 0 , count = arrayFile.length ; index < count ; index++ ){
			as += createA(arrayFile[index]);
		}
	    $("#file_up_td").html(as);
	}
	
}
function createA(fileName){
   var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
   var fixedFileName = $("#sn").val()+"_"+ fileName;
   var a = "<a href='"+uri+"'  >"+fixedFileName+"</a><br />"; 
   return a ;
}
function onlineScanner(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
	$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
</script>
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
table.receiver td {border:1px solid silver;}
table.receiver td input.noborder{border-bottom:1px solid silver;width:200px;}
table.receiver td.left{width:120px;text-align:right;}
table.next{margin-top:5px;}
table.receiver{border-collapse:collapse ;}
span._hand{cursor:pointer;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
	
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">申请采购单定金信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
		        <tr>
		          <td align="right" class="STYLE2">资金项目:</td>
		          <td>&nbsp;</td>
		          <td>		          
		          <%=applyTypeKey.getApplyTypeStatusById(type)%>
		          </td>
		        </tr>
		    <tr> 
			
		    <td width="12%" align="right"><span class="STYLE2">关联单号:</span></td>
		      <td width="2%">&nbsp;</td>
		      <td>
		       P<%=purchase_id%>
			   </td>
		    </tr>
		  	 <tr>
		  	 	<td width="12%" align="right"><span class="STYLE2">允许申请金额:</span></td>
		  	 	<td width="2%">&nbsp;</td>
		  	 	<td>
		  	 			<span id="rmbValue"><%= maxMoney%></span>  <span style="color:#f60;font-weight:bold;">RMB</span> ,<span id="usdValue"><%=MoneyUtil.round(maxMoney/usdCurrency,2) %></span>  <span style="color:#f60;font-weight:bold;">USD</span>
		  	 	</td>
		 	 </tr>
		  <tr> 
		
		      <td align="right" class="STYLE2">金额:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<select id="init_rate" onchange="changeInitRate()">
		      		<option value="0.1" selected>10%</option>
		      		<option value="0.15">15%</option>
		      		<option value="0.2">20%</option>
		      		<option value="0.25">25%</option>
		      		<option value="0.3">30%</option>
		      	</select>
		      	<input name="amount" type="text" id="amount" size="15" >
		      	<select name="currency" onchange="changeValue()">
		      		<option value="RMB" selected>RMB</option>
		      		<option value="USD">USD</option>
		      	</select>
		       
		      </td>
		    </tr>
				  
		    <tr> 
		      <td align="right" class="STYLE2">收款人:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input type="text" style="width:300px;" id="payee" name="payee" value='<%=supplier.getString("sup_name")%>'/>
		      </td>
		    </tr>
		    <tr> 
		      <td align="right" class="STYLE2">最迟转款时间:</td>
		      <td>&nbsp;</td>
		      <td>
		      	<input name="st" type="text" id="st_date"  value=""  style="border:1px #CCCCCC solid;width:70px;"/>
		      </td>
		    </tr>			 
		    <tr> 
		      <td align="right" class="STYLE2">付款信息:</td>
		      <!-- 兼容以前的 -->
		      <!-- 去account表中查询是不是有该供应商的收款信息,如果有的话就按照现在的方式回显出来 -->
		      <!-- 没有的话就按照以前的方式 -->
		      <td>&nbsp;</td>
		      <td>		      		    		     
		      		<h1 id="addBar"> 
		      			<input type="button" value="添加人民币收款" onclick="addRMB();"/>
		      			<input type="button" value="添加外币收款" onclick="addUSD();"/>
		      		</h1>
		      		<%if(accountInfo != null && accountInfo.length > 0 ){
		      			for(DBRow tempAccount : accountInfo){
		      				int index = 0;
		      				char[] indexChar = new char[]{'一','二','三','四','五','六','七','八','九','十'};
		      				boolean isOutCurrency = !tempAccount.getString("account_currency").toUpperCase().equals("RMB");
		      				int colLength = isOutCurrency ?7:4 ;
		      				%>
		      					<table class="receiver <%=(isOutCurrency?"usd":"rmb") %> next">
					      			<tr>
					      				<td rowspan="<%=colLength %>" style='width:150px;text-align:center;'><%= isOutCurrency?"外币":"人民币" %>收款信息<%=indexChar[index] %></td>
					      				<td class="left">收款人户名:</td>
					      				<td><input type="text" name="account_name" class="noborder account_name" value='<%=tempAccount.getString("account_name") %>'/></td>
					      				<td rowspan="<%=colLength %>" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人账号:</td>
					      				<td><input type="text" name="account_number" class="noborder account_number" value='<%=tempAccount.getString("account_number") %>'/></td>
					      			</tr>
					      			<%if(isOutCurrency){ %>
						      			<tr>
						      				<td  class="left">SWITH:</td>
						      				<td><input type="text" name="account_swift" class="noborder account_swift" value='<%=tempAccount.getString("account_swift") %>'></td>
						      	
						      			</tr>
						      			<tr>
						      				<td class="left">收款人地址</td>
						      				<td><input type="text" name="account_address" class="noborder account_address" value='<%=tempAccount.getString("account_address") %>' /></td>
						      			</tr>
						      			<tr>
						      				<td class="left">开户行地址</td>
						      				<td><input type="text" name="account_blank_address" class="noborder account_blank_address" value='<%=tempAccount.getString("account_blank_address") %>' /></td>
						      			</tr>
					      			<%
					      			} %>
					      			<tr>
					      				<td class="left">收款开户行:</td>
					      				<td><input type="text" name="account_blank" class="noborder account_blank" value='<%=tempAccount.getString("account_blank") %>'/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人电话:</td>
					      				<td><input type="text" name="account_phone" class="noborder account_phone" value='<%=tempAccount.getString("account_phone") %>'/><input type='hidden' name='account_currency' value='<%=tempAccount.getString("account_currency") %>'/></td>
					      			</tr>
					      		</table>
		      					
		      					
		      				<% 
		      				index++;
		      			}
		      		%>
		      			
		      		<%} else{%>
		      				<table  class="receiver rmb next">
					      			<tr>
					      				<td rowspan="4" style='width:150px;text-align:center;'>人民币收款信息 一</td>
					      				<td class="left">收款人户名:</td>
					      				<td><input type="text" name="account_name" class="noborder account_name"/></td>
					      				<td rowspan="4" style="width:80px;text-align:center;"><span onclick="deletePaymentInfo(this)" class="_hand">删除</span></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人账号:</td>
					      				<td><input type="text" name="account_number" class="noborder account_number"/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款开户行:</td>
					      				<td><input type="text" name="account_blank" class="noborder account_blank"/></td>
					      			</tr>
					      			<tr>
					      				<td class="left">收款人电话:</td>
					      				<td><input type="text" name="account_phone" class="noborder account_phone"/><input type='hidden' name='account_currency' value='rmb'/></td>
					      			</tr>
					       </table>
		       		<%} %>
		          <p>
		          		<span style='font-weight:bold;'>参考账号信息: </span>
		      	<%=supplier.getString("bank_information")%>
		          </p>
		      </td>
		    </tr>
		    <tr>
		      <td class="STYLE2" align="right">财务负责人:</td>
		      <td>&nbsp;</td>
		      <td>
		      <input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" style="width:180px;" onclick="adminUserInvoice()"/>
		      <input type="hidden" id="adminUserIdsInvoice" value=""/>
		            通知：
	   		<input type="checkbox"  name="isMailInvoice" id="isMailInvoice"/>邮件
	   		<input type="checkbox"  name="isMessageInvoice" id="isMessageInvoice" />短信
	   		<input type="checkbox"  name="isPageInvoice" id="isPageInvoice" />页面
		      </td>
		    </tr>					    
		    <tr> 
		      <td align="right" class="STYLE2">备注:</td>
		      <td>&nbsp;</td>
		      <td><textarea id="remark" name="remark" rows="4" cols="80"></textarea></td>
		    </tr>
		     <tr> 
		      <td align="right" class="STYLE2">凭证:</td>
		      <td>&nbsp;</td>
		      <td><input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件"/>
		      &nbsp;<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
		      </td>
		    </tr>
		      <tr>
		    	<td align="right" class="STYLE2"></td>
		    	<td>&nbsp;</td>
		    	<td id="file_up_td">
		    	
		    	</td>
		    </tr>
		</table>
			<input type="hidden" name="folder" id="folder" value="<%=systemConfig.getStringConfigValue("file_path_financial") %>"/>
	 		<input type="hidden" name="sn" value="F_applyMoneny" id="sn"/>
	 		<input type="hidden" name="file_names" id="file_names"/>
		
	</fieldset>	
	</td>
  </tr>
  <!-- 
  <tr>
    <td>
       <iframe width="98%"  frameborder="0" scrolling="no" name="upFile" id="upFile" src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/upload_purchase_image.html">

       </iframe>
    </td>
  </tr>
   -->
</table>
	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="Submit" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="$.artDialog && $.artDialog.close()" value="取消" >
    </td>
  </tr>
</table>
<script type="text/javascript">
//上传凭证
function uploadImage(){

	if($('#voucher').val() == ""){
		alert("请输入上传文件!");
	}else {

		//根据采购单id 查询资金申请表是否存在
		//$.ajax({
		//		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/Purchase/FindPurchaseDepositAction.action',
		//		dataType:'json',
		//		data:"purchaseId="+<%=purchase_id %>,
		//		success:function(data){        
		//		   if(data==0){				 
		//			  submitApply();
		//		   }					
		//	    }
		//});
		
		$('#uploadImageForm').submit();
	}
	
	
	
	
	
}

function setParentUserShow(ids,names,  methodName)
{
	eval(methodName)(ids,names);
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
	$("#adminUserIdsInvoice").val(user_ids);
}

function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowPurchaser'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}

	function submitApply()
	{
		 var pageMessage=false;
		 var mail=false;
		 var shortMessage=false;
           //判断 页面  邮件 短信
		  if($("#isMailInvoice").attr("checked")){
		     mail=true;
		  }
		  if($("#isMessageInvoice").attr("checked")){
			 shortMessage=true;
		  }
		  if($("#isPageInvoice").attr("checked")){
			 pageMessage=true;
		  }  
		  
			var str  = ""
			var rmbInfo = $(".rmb");
			if(rmbInfo.length > 0 ){
				for(var index = 0 , count = rmbInfo.length ; index < count ; index++ ){
				    var _table = $(rmbInfo.get(index));
					var fixIndex =  index+1;
					var r_name =  $("input.account_name",_table).val();
					var r_account = $("input.account_number",_table).val();
					var r_bank = $("input.account_blank",_table).val();
					var r_phone = $("input.account_phone",_table).val();
					str +="\n";
					if($.trim(r_name).length > 0 ){
						str += "收款人户名:"+r_name+"\n";
					}else{
					 
						alert("输入收款人户名!");
						return;
					}
					if($.trim(r_account).length > 0 ){
						str += "收款人账号:"+r_account+"\n";
					}else{
						alert("输入收款人账号!");
						return;
					}
					if($.trim(r_bank).length > 0 ){
						str += "收款开户行:"+r_bank+"\n";
					}
					if($.trim(r_phone).length > 0 ){
						str += "联系电话:"+r_phone+"\n";
					}	
				}
			}
			 str += getUsdPayMentInfo();
		 	 // 计算组织出所有收款信息
		 	 if(str.length <= 4){
				alert("请先输入收款人信息");
				return ;
			 }
		 	 if($("#payee").val().trim().length < 1){
		 		$("#payee").focus();
		 		alert("输入供应商的名称!");
					return ;
			 }
		 	 if($.trim($("#st_date").val()).length < 1){
				 alert("输入最迟转款时间");
					return ;
			 }
			 if($.trim($("#amount").val())==""||$("#amount").val()==null)
			 {
			     $("#amount").focus();
			     alert("金额不能为空！");
			 }
			 else if(isNaN($("#amount").val().trim()))
		 	 {
			     $("#amount").focus();
			     alert("金额必须为数字！");
		 	 }
			 else if($("#amount").val().trim()<=0)
			 {
			 	 $("#amount").focus();
			     alert("金额必须大于零！");
			 }
			 else if($.trim($("#remark").val()).length>200)
			 {
			     $("#remark").focus();
			     alert("备注字数不能超过200！");
			 }
			 else
			 {
				// 如果是有file_names ，那么读取子页面的file_names
				
					var _file_names = $("input[name='file_names']").val() ;
					var amount = $("#amount").val();
					var currency = $("select[name='currency']").val();
					// 判断金额是不是都正确
 					if(currency === "USD"){
						if(parseFloat(amount) >  parseFloat ('<%= maxUSDMoney%>') ){
						    alert("申请金额过多！");
							return ;
						}
					}else{
					    if(parseFloat(amount) > parseFloat('<%= maxRMBMoney%>')){
						    alert("申请金额过多！");
							return ;
						}
					}
 				 
				var o = {
						remark:"资金申请:"+$("#remark").val(),
						amount:$("#amount").val(),
						supplier_id:<%=supplier.get("id",0l)%>,
						payee: $("#payee").val(),
						payment_information:str,
						last_time:$("#st_date").val(),
						admin_id:$("#adminUserIdsInvoice").val(),
						mail:mail,
						shortMessage:shortMessage,
						pageMessage:pageMessage	,
						folder:$("#folder").val(),
						sn:$("#sn").val(),
						file_names:_file_names,
						currency:$("select[name='currency']").val()
			            }
		 		//读取账号信息
		 		//
		 		var fields = ["account_name","account_number","account_swift","account_address","account_blank","account_blank_address","account_phone","account_currency"];
			 	fixAccountInfo();

			 	var accountInfo = [] ;
				var indexOfAccount = 1 ;
				var accountNameTemp = $("input[name='account_name_"+indexOfAccount+"']");
				 
				while(accountNameTemp && accountNameTemp.val() &&  accountNameTemp.val().length > 0){
					var account = {} ;
					for(var fieldIndex = 0 , count = fields.length ; fieldIndex < count ; fieldIndex++ ){
						var tempField =  fields[fieldIndex]+"_"+indexOfAccount;
						var tempFieldValue = $("input[name='"+tempField+"']").val();
						if(tempFieldValue && tempFieldValue.length > 0 ){
						    account[tempField] = tempFieldValue;
						}
					}
					
					accountInfo.push(account);
					indexOfAccount++ ;
					accountNameTemp = $("input[name='account_name_"+indexOfAccount+"']");
			  
				}
				     
		 
				  $.artDialog.opener.deposit && $.artDialog.opener.deposit(o,accountInfo);
			 }
	
	}
			function getUsdPayMentInfo(){
				var str1  = "";
				var usdInfo = $(".usd");
				if(usdInfo && usdInfo.length > 0 ){
					for(var index = 0 , count = usdInfo.length ; index < count ; index++ ){
					//	r_name,r_account,swift,r_address,r_bank_name,r_bank_address
					//	收款人户名,收款人账号,SWIFT,收款人地址,收款行名称,收款行地址
					    var _table = $(usdInfo.get(index));
						 
						var r_name =  $("input.account_name",_table).val();
						var r_account = $("input.account_number",_table).val();
						var swift = $("input.account_swift",_table).val();
						var r_address = $("input.account_address",_table).val();
						var r_bank_name = $("input.account_blank",_table).val();
						var r_bank_address = $("input.account_blank_address",_table).val();
						str1 +="\n";
						if($.trim(r_name).length > 0 ){
							str1 += "收款人户名:"+r_name+"\n";
						}else{
							alert("输入收款人户名!");
							return;
						}
						if($.trim(r_account).length > 0 ){
							str1 += "收款人账号:"+r_account+"\n";
						}else{
							alert("输入收款人账号!");
							return;
						}
						if($.trim(swift).length > 0 ){
							str1 += "SWIFT:"+swift+"\n";
						}
						if($.trim(r_address).length > 0 ){
							str1 += "收款人地址:"+r_address+"\n";
						}
						if($.trim(r_bank_name).length > 0 ){
							str1 += "收款行名称:"+r_bank_name+"\n";
						}
						if($.trim(r_bank_address).length > 0 ){
							str1 += "收款行地址:"+r_bank_address+"\n";
						}
					}
					return str1;
				}
				return "" ;
			}
			function decimal(num,v){
				var vv = Math.pow(10,v);
				return Math.round(num*vv)/vv;
			} 
			function deletePaymentInfo(_this){
				var parentNode = $(_this).parent().parent().parent().parent();
				if($(".receiver").length == 1){
					alert("至少包含一个收款信息");
					return ;
				}
 				 parentNode.remove();
			}
			function showMessage(_content,_state){
				var o =  {
					state:_state || "succeed" ,
					content:_content,
					corner: true
				 };
			 
				 var  _self = $("body"),
				_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);	
				 
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
			 }
			}
</script>
<script type="text/javascript">
	$("#st_date").date_input();
</script>
</body>
</html>
