<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id=StringUtil.getLong(request,"purchase_id");
	int purchase_status = StringUtil.getInt(request,"purchase_status");
	String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
	
	DBRow[] rows; 
	
	rows = purchaseMgr.getPurchaseDetailByPurchaseid(purchase_id,null,null);
	
	DBRow supplier;
	long sid;
	try
	{
		sid = Long.parseLong(purchase.getString("supplier"));
	}
	catch(NumberFormatException e)
	{
		sid = 0;
	}
	
	if(sid ==0)
	{
		supplier = new DBRow();
		
		String name = "";
		
		if(session.getAttribute("supplier_name")!=null)
		{
			name = session.getAttribute("supplier_name").toString();
		}
		 
		supplier.add("sup_name",name);
	}
	else
	{
		supplier = supplierMgrTJH.getDetailSupplier(sid);
	}
	
	double sum = 0;
%>
<style type="text/css">
<!--
.STYLE1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>

<table width="720px" border="0" align="left" cellpadding="0" cellspacing="0">
<thead>
  <tr>
  	<td colspan="5">
		<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2" align="center" style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;">Visionari LLC</td>
				<td align="center" nowrap="nowrap"  style="font-family: 黑体;font-size: large;padding-bottom:10px;">微尘大业采购通知单</td>
				<td colspan="2" align="left" style="font-family:黑体;font-size: 15px;padding-bottom:10px;">采购批号：P<%=purchase_id%><br />
				  下单日期：<%=DateUtil.FormatDatetime("yyyy年MM月dd日",DateUtil.StrtoDate(purchase.getString("purchase_date").split(" ")[0]))%></td>
			  </tr>
		
			   <tr>
			   	<td colspan="5">
					<hr/>
					<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<table width="100%">
						<tr>
							<td width="12%" style="font-family:黑体;font-size: 16px; padding-top:10px;" align="right">联系人：</td>
							<td width="88%" align="left" valign="middle" style="font-family:黑体;font-size: 16px; padding-top:10px;"><%=purchase.getString("delivery_linkman")%></td>
							

						</tr>
						<tr>
							<td width="12%"  style="font-family:黑体;font-size: 16px; padding-top:10px;" align="right">收货电话：</td>
							<td width="88%"  align="left" valign="middle" style="font-family:黑体;font-size: 16px; padding-top:10px;"><%=purchase.getString("linkman_phone")%></td>
						</tr>
						<tr>
							<td width="12%" align="right" valign="middle" nowrap="nowrap" style="font-family:黑体;font-size: 16px;">交货地点：</td>
							<td width="88%" align="left" valign="middle" style="font-family:黑体;font-size: 16px; padding-top:5px;">
								<%
									long ccid = purchase.get("deliver_native",0l);
									long ps_id = purchase.get("deliver_provice",0l);
									DBRow ccidRow = orderMgr.getDetailCountryCodeByCcid(ccid);
									DBRow psIdRow = productMgr.getDetailProvinceByProId(ps_id);
								%>
									<%=purchase.getString("deliver_house_number") %>,<%=purchase.getString("deliver_street") %>,<%=purchase.getString("deliver_zip_code") %>,<%=purchase.getString("deliver_city") %>,<%=(psIdRow != null? psIdRow.getString("pro_name"):"" ) %>,<%= (ccidRow != null?ccidRow.getString("c_country"):"" ) %>					
							</td>
						</tr>
						<tr>
							<td width="12%" align="right" style="font-family:黑体;font-size: 16px;">供应商：</td>
							<td width="88%" align="left"><%=supplier.getString("sup_name") %></td>
						</tr>
					</table>
					</div>
					<hr/>
				</td>
				
			  </tr>
			 
		 </table>
	</td>
  </tr>
  <tr>
    <th width="39%" align="left"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">产品名称</th>
    <th width="17%"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">订购数量</th>
    <th width="15%"  class="right-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">单位</th>
    <th width="12%"  class="right-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">单价</th>
    <th width="17%" class="right-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">金额</th>
  </tr>
  </thead>
  <%
  	DecimalFormat  df  =  new  DecimalFormat(".00");
  	for(int i=0;i<rows.length;i++)
  	{
  		String etadelay = "";
  		String unit_name = rows[i].getString("unit_name");
  		String unit = "";
  		if(purchase_status != PurchaseKey.CANCEL&&purchase_status!=PurchaseKey.FINISH)
  	  	{
	  		if(!rows[i].getString("eta").equals(""))
	  		{
	  			TDate tdate =new TDate();
	  			long eta = tdate.getDateTime(rows[i].getString("eta"));
	  			long now = tdate.getDateTime(DateUtil.NowStr());
		  		if(eta<now&&rows[i].get("reap_count",0f)==0.0)
		  		{
		  			etadelay = "background-color: #FFFF99;";
		  		}
	  		}
  		}
  		
  		if(unit_name.equals("ITEM"))
  		{
  			unit = "条";
  		}
  		else if(unit_name.equals("PAIR"))
  		{
  			unit = "对";
  		}
  		else if(unit_name.equals("SET"))
  		{
  			unit = "套";
  		}
  		else
  		{
  			unit = unit_name;
  		}
  %>
  <tr align="center" valign="middle">
    <td height="15" align="left"" style="font-family:黑体;font-size: 15px;border-left: 2px #000000 solid;border-bottom: 1px #000000 solid;<%=etadelay%>"><%=rows[i].getString("purchase_name")%></td>
    <td height="15" style="font-family:黑体;font-size: 15px;border-left:1px #000000 solid;border-bottom: 1px #000000 solid;<%=etadelay%>"><%=rows[i].getString("purchase_count")%></td>
    <td height="15" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;<%=etadelay%>"><%=rows[i].getString("unit_name")%></td>
    	<%
    		String color = "";

	    	if(rows[i].get("price",0.00)>rows[i].get("product_history_price",0.00))
	    	{
	    		color = "red";
	    	}
	    	if(rows[i].get("price",0.00)<rows[i].get("product_history_price",0.00))
	    	{
	    		color = "green";
	    	}
    	%>
    <td height="15" style="font-family:Airal Black;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;<%=etadelay%>"><font><%=rows[i].getString("price")%></font>
    </td>
    <td height="15" style="font-family:Airal Black;font-size: 15px;border-left: 1px #000000 solid;;border-bottom: 1px #000000 solid;border-right: 2px #000000 solid;<%=etadelay%>"><% out.print(df.format(rows[i].get("price",0.00)*rows[i].get("purchase_count",0f))); %></td>
  </tr>
  <%
  			sum += rows[i].get("price",0.00)*rows[i].get("purchase_count",0f);
  	}
   %>
   <tr>
   	<td height="15" align="center"" style="border-left: 2px #000000 solid;border-bottom: 2px #000000 solid;">合计金额：</td>
    <td height="15" style="border-left: 1px #000000 solid;border-bottom: 2px #000000 solid;"></td>
    <td height="15" style="border-left: 1px #000000 solid;border-bottom: 2px #000000 solid;"></td>
    <td height="15" style="border-left: 1px #000000 solid;border-bottom: 2px #000000 solid;">
    </td>
    <td height="15" style="border-left: 1px #000000 solid;;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid" align="center"><% out.print(df.format(sum)); %></td>
   </tr>
   <tfoot>
   	 <tr>
	 	<td colspan="5" style="padding-top: 15px;">
			
			<table width="100%" height="31">
				<tr>
					<td colspan="4">
						<hr/>
						<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
						<table width="100%">
							<tr>
								<td width="120" height="40" align="left" valign="top" style="font-family:黑体;font-size: 16px;">返修条款：</td>
								<td height="40" valign="top" align="left"><%=purchase.getString("repair_terms")%></td>
							</tr>
							<tr>
								<td width="120" height="40" align="left" valign="top" style="font-family:黑体;font-size: 16px;">贴标条款：</td>
								<td height="40" valign="top" align="left"><%=purchase.getString("labeling_terms")%></td>
							</tr>
							<tr>
								<td width="120" height="40" align="left" valign="top" style="font-family:黑体;font-size: 16px;">交货条款：</td>
								<td height="40" valign="top" align="left"><%=purchase.getString("delivery_product_terms")%></td>
							</tr>
							<tr>
								<td width="120" height="40" align="left" valign="top" style="font-family:黑体;font-size: 16px;">其他条款：</td>
								<td height="40" valign="top" align="left"><%=purchase.getString("purchase_terms")%></td>
							</tr>
						</table>
						</div>
						<hr/>
					</td>
					
				</tr>
				<tr>
					<td height="40px;" width="124" style="font-family:黑体;font-size: 16px;"><p>供应商签章：<br />
					  <br />
					_______________</p>
			      </td>
					<td height="40px;" width="120" style="font-family:黑体;font-size: 16px;"><p>采购方签章：<br />
					  <br />
					_______________</p>
			      </td>
					<td width="28">&nbsp;				  </td>
					<td width="428" rowspan="2" align="left" style="font-family:黑体;font-size: 15px;"><p>1. 交货时请通过本系统创建交货单后交货<br />
					2. 请打印一份交货单随货物发出<br />
					3. 如有外包装箱，请务必标示箱号,并打印粘贴我公司外箱标签<br />
					  4. 在商品本身包装上，必须贴有我公司商品条码标签<br />
					</p>
			      </td>
				</tr>
				<tr>
					<td height="40">日期：_________</td>
					<td height="40">日期：_________</td>
					<td></td>
				</tr>
		  </table>
		</td>
  	</tr>
   </tfoot>
</table>



