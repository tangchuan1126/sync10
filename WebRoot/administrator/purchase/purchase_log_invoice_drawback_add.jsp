<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
 <jsp:useBean id="tracingOrderKey" class="com.cwc.app.key.TracingOrderKey"/>
<jsp:useBean id="handStatusleKey" class="com.cwc.app.key.HandStatusleKey"/>
<jsp:useBean id="productStatusKey" class="com.cwc.app.key.ProductStatusKey"/>
<jsp:useBean id="orderRateKey" class="com.cwc.app.key.OrderRateKey"/>
<jsp:useBean id="returnProductKey" class="com.cwc.app.key.ReturnProductKey"/>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<%@ page import="com.cwc.app.beans.AdminLoginBean,com.cwc.app.key.AfterServiceKey,com.cwc.app.api.OrderMgr"%>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加采购单日志</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
 
<style type="text/css">
*{margin:0px;padding:0px;}
div.buttonDiv{background-color: #F4F4F4;border: 1px solid #EEEEEE;padding: 5px 0;text-align: right;}
 input.buttonSpecil {background-color: #BF5E26;}
 .jqidefaultbutton { background-color: #2F6073;border: 1px solid #F4F4F4; color: #FFFFFF;font-size: 12px;font-weight: bold;margin: 0 10px;padding: 3px 10px;}
 .specl:hover{background-color: #728a8c;}
 .title{ background: url("../js/popmenu/pro_title.jpg") no-repeat scroll left center transparent;color: #FFFFFF;display: table;font-size: 14px; height: 27px;padding-left: 5px; padding-top: 3px;width: 356px;line-height:27px;}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:80px;text-align:center;border:1px solid silver;margin:0px auto;width:300px;height:30px;background:white;line-height:30px;font-size:14px;}	
	#ui-datepicker-div{z-index:9999;display:none;}
</style>
<%
 // 查询数据库中detail 中的最大的到货时间。如果没有就是空.
 // 在修改的时候如果是改变了就修改所有的detail中的时间.如果是没有改变的话。那么就不修改
 long purchase_id = StringUtil.getLong(request,"purchase_id");
 int purchase_invoice = StringUtil.getInt(request,"purchase_invoice");
 int purhcase_drawback = StringUtil.getInt(request, "purhcase_drawback");
 String lastEtaTime = purchaseMgrIfaceZr.getLastPurchaseDetailEta(purchase_id);
 String addPurchaseLogAction = ConfigBean.getStringValue("systenFolder") + "action/administrator/purchase/PurchaseLogAdd.action";
 DBRow purchaseRow = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id+"");
 int invoice = purchaseRow.get("invoice",0);
 int drawback = purchaseRow.get("drawback",0);


%>
<script type="text/javascript">
	var lastEtaTime = '<%=lastEtaTime %>'.replace("-","");
	jQuery(function($){
		$('#eta').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			onSelect:function(dateText, inst){
				var selVal = $("#ptype").val();
				
				var html1 = "["+$("#ptype option:selected").html()+"]流程["+$("#periodSpan").text()+"]";
					var content = $("#content").val();
					 if(content.indexOf("阶段预计完成时间") != -1){
						// content = content.replace("阶段预计完成时间","").substr(11);
						 content = content.substr(content.indexOf("阶段预计完成时间")+19);
					}
					$("#content").val(html1+"阶段预计完成时间" + dateText + ":"+content);
			}
		});
		var selVal = $("#ptype").val();
		var appendElem = "";
		if(4 == selVal)
		{
	  		appendElem += "<%=invoiceKey.getInvoiceById(String.valueOf(invoice))%>";
		}
		else
		{
			appendElem += "<%=drawbackKey.getDrawbackById(String.valueOf(drawback))%>";
		}
		$("#periodSpan").html(appendElem);
	})	
	 
 
	 function submitForm(){
	 
		if(!volidateForm()){
			return ;
		}
		var uri = '<%=  addPurchaseLogAction%>' + "?purchase_id=" + '<%= purchase_id%>' + "&followup_ptype="+$("#ptype").val()+"&followup_content="+$("#content").val();
		// eta 时间的计算如果是没有改的话，或者是为空那么就不要把这个字段传入到后台
		if(4 == $("#ptype").val()){
			uri += "&followup_ptype_sub=<%=invoice%>";
		}else if(5 == $("#ptype").val()){
			uri += "&followup_ptype_sub=<%=drawback%>";
		}
		var eta = $("#eta").val();
		if($.trim(eta).length > 0 ){
			uri += "&eta="+$("#eta").val();
		}
		 
  //update purchase_detail set update purchase_detail set eta ='2012-12-13' where purchase_id =100773
		$.ajax({
			url:uri,
			dataType:'json',
			 
			success:function(data){
				if(data && data.flag == "success"){
					setTimeout("windowClose()", 1000);
				}else{
					showMessage("系统错误","error");
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		})
	 }
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	 function volidateForm(){
		var val = $("#content").val();
 
		if($.trim(val).length < 1){
			showMessage("请输入备注");
			return false;
		}
		return true;
	 }
	 function cancel(){
		$.artDialog && $.artDialog.close();
	 }
	function changeLogType(){
		$("#periodSpan").html("");
		var selVal = $("#ptype").val();
		if(4 == selVal || 5 == selVal){
			var appendElem = "";
			if(4 == selVal){
		  		appendElem += "<%=invoiceKey.getInvoiceById(String.valueOf(invoice))%>";
			}else{
				appendElem += "<%=drawbackKey.getDrawbackById(String.valueOf(drawback))%>";
			}
			$("#periodSpan").html(appendElem);
			$("#eta_title_span").html("&nbsp;&nbsp;&nbsp;&nbsp;本阶段预计完成时间:");
		}else if(2 == selVal){
			$("#eta_title_span").html("&nbsp;&nbsp;&nbsp;&nbsp;预计交货时间:");
		}else{
			$("#eta_title_span").html("&nbsp;&nbsp;&nbsp;&nbsp;本阶段预计完成时间:");
		}
	};
 </script>
</head>
<body   >
<!-- 遮盖层 -->
  <div class="cssDivschedule">
 		<div class="innerDiv">
 			请稍等.......
 		</div>
 </div>
  
<%-- 	<div class="title" style="font-weight: bold;margin-bottom:15px;">采购单跟进[单号:<%= purchase_id %>]</div>--%>
<br/>
 	<form>
 		<table>
 			<tr>
 				<td style="text-align:right;width:20%;">流程:</td>
 				<td style="30%">
 					<select id='ptype' name='ptype' onchange="changeLogType()">
 						<%	
 	  						if(1 != purchase_invoice && 4 != purchase_invoice){
 	  					%>	
 	  							<option value='4'>发票进度</option>
 	  					<%		
 	  						}	
 	  					%>
 	  					<%	
 	  						if(1 != purhcase_drawback && 4 != purhcase_drawback){
 	  					%>	
 	  							<option value='5'>退税进度</option>
 	  					<%		
 	  						}	
 	  					%>
 	  				</select>
 				</td>
 				<td style="text-align:right;width:20%;">阶段:</td>
 				<td id="periodTd" style='width:30%;'>
 				<span id="periodSpan"><%=invoiceKey.getInvoiceById(String.valueOf(invoice))%></span>
 				</td>
 			</tr>
 			<tr>
 				<td  colspan="4" id="eta_title_td" align="left">
 					<span id="eta_title_span">&nbsp;&nbsp;&nbsp;&nbsp;本阶段预计完成时间:</span>
 					<input type="text" value="<%=lastEtaTime %>" id="eta" name="eta" />  
 				</td>
 			</tr>
 			<tr>
 				<td style="text-align:right;width:20%;">备注:</td>
 				<td colspan="3" style="width: 80%"><textarea rows='12' cols='60' id='content' name='followup_content'/></textarea></td>
 			</tr>
 		</table>
 	  	</form>	
 	  		 <div class="buttonDiv" style="margin-top:5px;"> 
	 	 	<input type="button" id="jqi_state0_button提交" class="jqidefaultbutton buttonSpecil"  onclick="submitForm()" value="提交"> 
			<button id="jqi_state0_button取消" value="n" class="jqidefaultbutton specl" name="jqi_state0_button取消" onclick="cancel();">取消</button>
	 	 </div>
 	<script type="text/javascript">
 	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 		
 	</script>
</body>
</html>
