<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.CurrencyKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%> 
<%

DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

DBRow[] suppliers = supplierMgrTJH.getAllSupplier(null);

DBRow[] productline =  productLineMgrTJH.getAllProductLine();//供应商所在的产品线
String expectArrTime	= StringUtil.getString(request, "expectArrTime");
String purchase_id = StringUtil.getString(request,"purchase_id").equals("")?"0":StringUtil.getString(request,"purchase_id");
DBRow purchaseRow  = purchaseMgrLL.getPurchaseById(purchase_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
$(function(){
	$.ajax({
			url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+<%=purchaseRow.get("deliver_native",0)%>,
			dataType:'json',
			type:'post',
			success:function(data){
				if("" != data){
					var provinces = eval(data);
					$("#pro_id").attr("disabled",false);
					if(0 == <%=purchaseRow.get("deliver_provice",0)%> || -1 == <%=purchaseRow.get("deliver_provice",0)%>){
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择</option>");
							$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
							for(var i = 0; i < data.length; i ++){
								if(<%=purchaseRow.get("deliver_provice",0)%> == data[i].pro_id){
									$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
								}else{
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
						}
						if(-1 == $("#pro_id").val()){
							$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value='<%=purchaseRow.getString("deliver_pro_input") %>'/>");
						}
					}else{
						$("#pro_id").attr("disabled",false);
						$("#pro_id").append("<option value=0>请选择</option>");
						for(var i = 0; i < data.length; i ++){
							
								if(<%=purchaseRow.get("deliver_provice",0)%> == data[i].pro_id){
									$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
								}else{
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
						}
						$("#pro_id").append("<option value=-1>手动输入</option>");
					}
				}else{
					$("#pro_id").attr("disabled",false);
					$("#pro_id").append("<option value=0>请选择</option>");
					$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
					if(-1 == $("#pro_id").val()){
					$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value='<%=purchaseRow.getString("deliver_pro_input") %>'/>");
					}
				}
			},
			error:function(){
			}
		})	
		$("#pro_id").change();
		initSupplierByProductline('<%=purchaseRow.getString("product_line_id")%>','<%=purchaseRow.getString("supplier") %>');
		$("#supplier_native").change();
		$("#supplier_province").change();
	$('#expectArriveTime').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
	});
	$("#ui-datepicker-div").css("display","none");
});
	
				     	

	function addpurchase()
	{
		if($("#product_line_id").val() == 0){
			alert("请选择产品线");
		}
		else if($("#supplier").val() == 0){
			alert("请填写供应商");
		}
		else if($("#ps_id").val()==0){
			alert("请选择存储仓库");
		}
		else{
			document.add_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/UpdatePurchaseBasic.action"
			document.add_form.supplier.value = $("#supplier").val();
			document.add_form.product_line_id.value = $("#product_line_id").val();
			document.add_form.purchase_id.value = <%=purchase_id%>
			document.add_form.ps_id.value = $("#ps_id").val();		
			document.add_form.delivery_linkman.value = $("#linkman").val();
			document.add_form.linkman_phone.value = $("#linkman_number").val();
			document.add_form.deliver_native.value = $("#ccid_hidden").val();
			document.add_form.deliver_provice.value = $("#pro_id").val();		
			document.add_form.deliver_city.value = $("#deliver_city").val();
			document.add_form.deliver_zip_code.value = $("#deliver_zip_code").val();
			document.add_form.deliver_street.value = $("#deliver_street").val();
			document.add_form.deliver_house_number.value = $("#deliver_house_number").val();
			document.add_form.supplier_house_number.value = $("#supplier_house_number").val();
			document.add_form.supplier_street.value = $("#supplier_street").val();
			document.add_form.supplier_city.value = $("#supplier_city").val();
			document.add_form.supplier_zip_code.value = $("#supplier_zip_code").val();
			document.add_form.supplier_province.value = $("#supplier_province").val();		
			document.add_form.supplier_pro_input.value = $("#supplier_pro_input").val();
			document.add_form.supplier_native.value = $("#supplier_native").val();
			document.add_form.supplier_link_man.value = $("#supplier_link_man").val();
			document.add_form.supplier_link_phone.value = $("#supplier_link_phone").val();
			document.add_form.deliver_pro_input.value = $("#address_state_input").val();
			document.add_form.expectArrTime.value = $("#expectArriveTime").val();
			document.add_form.submit();	
		}
	}
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	function removeAfterProIdInput(){$("#address_state_input").remove();}
	function setPro_id(deliver_proId, deliver_proInput){
		removeAfterProIdInput();
		var node = $("#ccid_hidden");
		var value = node.val();
	 	$("#pro_id").empty();
		 	  $.ajax({
					url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
					dataType:'json',
					type:'post',
					success:function(data){
						if("" != data){
							var provinces = eval(data);
							$("#pro_id").attr("disabled",false);
							if(deliver_proId == -1){
								$("#pro_id").attr("disabled",false);
								for(var i = 0; i < data.length; i ++){
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
								$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
								$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value="+deliver_proInput+"/>");
							}else{
								$("#pro_id").append("<option value=0>请选择......</option>");
								for(var i = 0; i < data.length; i ++){
									if(deliver_proId == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
								}
								$("#pro_id").append("<option value=-1>手动输入</option>");
							}
							
						}else{
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择......</option>");
							$("#pro_id").append("<option value=-1>手工输入</option>");
						}
					},
					error:function(){
					}
			  })
	 };
	 function handleProInput(){
	 	var value = $("#pro_id").val();
		removeAfterProIdInput();
	 	if(-1 == value){
	 		$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input'/>");
	 	}else{
	 		removeAfterProIdInput();
	 	}
	 };
	 function removeAfterProIdInputSupplier(){$("#supplier_pro_input").remove();}
	 function setPro_idSupplier(getCcid,getProId,getProInput){
			removeAfterProIdInputSupplier();
			var node = $("#supplier_native");
			var value = node.val();
			if(getCcid){
				value = getCcid;
			}
		 	$("#supplier_province").empty();
		 	$.ajax({
						url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
						dataType:'json',
						type:'post',
						success:function(data){
							if("" != data){
								$("#supplier_province").append("<option value=0>请选择</option>");
								var provinces = eval(data);
								$("#supplier_province").attr("disabled",false);
								for(var i = 0; i < data.length; i ++){
									$("#supplier_province").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
								$("#supplier_province").append("<option value=-1>手动输入</option>");
								if(0 != '<%=purchaseRow.get("supplier_province",0)%>'){
									$("#supplier_province option[value="+'<%=purchaseRow.get("supplier_province",0)%>'+"]").attr("selected",true);
								}
								if(getProId){
									$("#supplier_province option[value="+getProId+"]").attr("selected", true);
								}
							}else{
								$("#supplier_province").attr("disabled",false);
								$("#supplier_province").append("<option value=0 selected='selected'>请选择</option>");
								$("#supplier_province").append("<option value=-1>手工输入</option>");
								var proInputGet = '<%=purchaseRow.getString("supplier_pro_input") %>';
								if(getProInput){
									proInputGet = getProInput;
								}
								if(-1 == $("#supplier_province").val()){
									$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputGet+"'/>");
								}
							}
						},
						error:function(){
						}
					})	
		 }
		function handleProInputSupplier(supplierProInput){
		 	var value = $("#supplier_province").val();
		 	removeAfterProIdInputSupplier();
		 	if(-1 == value){
		 		var proInputSupplier = "";
			 	if(supplierProInput){
			 		proInputSupplier = supplierProInput;
			 	}
		 		$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputSupplier+"'/>");
		 	}else{
		 		removeAfterProIdInputSupplier();
	 	}
	 };
	function selectStorage(){
		var para = "ps_id="+$("#ps_id").val();
		$("#deliver_house_number").val("");
		$("#deliver_street").val("");
		$("#deliver_zip_code").val("");
		$("#deliver_city").val("");
		$("#ccid_hidden").val("");
		$("#ccid_hidden").change();
		$("#linkman").val("");
		$("#linkman_number").val("");
		
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/GetStorageCatalogDetailDeliver.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					
				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data){
					//$("#address").val(data.deliver_address);
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#linkman").val(data.deliver_contact);
					$("#linkman_number").val(data.deliver_phone);
					var ccid_selected = "#ccid_hidden option[value="+data.deliver_nation+"]";
					$(ccid_selected).attr("selected", "selected");
					setPro_id(data.deliver_pro_id,data.deliver_pro_input);
				}
			});
	}
function initSupplierByProductline(productlineid,supplierId){
			var para = "productline_id="+productlineid;
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					error: function(e){
						alert("数据读取出错！");
					},
					success: function(data){
						$("#supplier").clearAll();
						$("#supplier").addOption("请选择...","");
						if(data != ""){
							$.each(data,function(i){
								$("#supplier").addOption(data[i].sup_name,data[i].id);
							});
							$("#supplier option[value="+supplierId+"]").attr("selected",true);
				}
					}
			});
	}
	
	function getSupplierByProductline(productlineid){
		$("#supplier_house_number").val("");
		$("#supplier_street").val("");
		$("#supplier_city").val("");
		$("#supplier_zip_code").val("");
		$("#supplier_native").val("");
		$("#supplier_link_man").val("");
		$("#supplier_link_phone").val("");
		removeAfterProIdInputSupplier();
		setPro_idSupplier();
		handleProInputSupplier();
		var para = "productline_id="+productlineid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#supplier").clearAll();
					$("#supplier").addOption("请选择...","");
					if(data != ""){
						$.each(data,function(i){
							$("#supplier").addOption(data[i].sup_name,data[i].id);
						});
					}
				}
			});
	}
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function changeSupplierAddress(){
		$("#supplier_house_number").val("");
		$("#supplier_street").val("");
		$("#supplier_city").val("");
		$("#supplier_zip_code").val("");
		$("#supplier_native").val("");
		$("#supplier_link_man").val("");
		$("#supplier_link_phone").val("");
		removeAfterProIdInputSupplier();
		setPro_idSupplier();
		handleProInputSupplier();
			var supplierId = $("#supplier").val();
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetSupplierInfoAjax.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:"supplier_id="+supplierId,
				error: function(e){
					alert("数据读取出错！");
				},
				success: function(data){
					if(null != data){
						$("#supplier_house_number").val(data.address_houme_number);
						$("#supplier_street").val(data.address_street);
						$("#supplier_city").val(data.city_input);
						$("#supplier_zip_code").val(data.zip_code);
						$("#supplier_native option[value="+data.nation_id+"]").attr("selected",true);
						$("#supplier_link_man").val(data.linkman);
						$("#supplier_link_phone").val(data.phone);
						removeAfterProIdInputSupplier();
						setPro_idSupplier(data.nation_id,data.province_id);
						handleProInputSupplier(data.supplier_pro_input);
					}
				}
			});
		}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

#purchaseTable tr td{
	line-height:30px;
	height:30px;
}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
			<form method="post" name="add_form">
			<input type="hidden" name="purchase_id"/>
			<input type="hidden" name="supplier"/>
			<input type="hidden" name="product_line_id"/>
			<input type="hidden" name="ps_id"/>
			<input type="hidden" name="deliver_house_number"/>
			<input type="hidden" name="deliver_street"/>
			<input type="hidden" name="deliver_zip_code"/>
			<input type="hidden" name="deliver_city"/>
			<input type="hidden" name="deliver_provice"/>
			<input type="hidden" name="deliver_native"/>
			<input type="hidden" name="deliver_pro_input"/>
			<input type="hidden" name="delivery_linkman"/>
			<input type="hidden" name="linkman_phone"/>
			<input type="hidden" name="supplier_house_number"/>
			<input type="hidden" name="supplier_street"/>
			<input type="hidden" name="supplier_city"/>
			<input type="hidden" name="supplier_zip_code"/>
			<input type="hidden" name="supplier_province"/>
			<input type="hidden" name="supplier_native"/>
			<input type="hidden" name="supplier_pro_input"/>
			<input type="hidden" name="supplier_link_man"/>
			<input type="hidden" name="supplier_link_phone"/>
			<input type="hidden" name="expectArrTime" id="expectArrTime" value="<%=expectArrTime %>"/>
			<input type="hidden" name="tempfilename" value='<%=StringUtil.getString(request, "tempfilename") %>'/>
			<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_add_terms.html"/>
			</form>
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				
				<tr>
					<td valign="top"><br>
						<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						  <tr>
							<td align="left" style="font-family:'黑体'; font-size:18px;border-bottom:1px solid #999999;color:#000000;">
							填写供应商，并选择收货仓库</td>
						  </tr>
						 </table>
						<br/>
                      <fieldset class="set" style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<legend style="font-size:15px;font-weight:normal;color:#999999;">供应商及提货地址信息</legend>
                        <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;" id="purchaseTable">
							<tr>
							<td width="10%"  align="right" style="padding:0px;">产品 线：</td>
				            <td width="46%" height="35" align="left" valign="middle" nowrap="nowrap">
						      	<select style="width:150px" name="product_line_id" id="product_line_id" onChange="getSupplierByProductline(this.value)">
						      	<option value="0">请选择...</option>
						      		<%
						      		for(int i=0;i<productline.length;i++){
						      			if(productline[i].getString("id").equals(purchaseRow.get("product_line_id",0)+"")){
					      			%>
							      		<option value="<%=productline[i].getString("id") %>" selected="selected"><%=productline[i].getString("name") %></option>
							      	<%	
						      			}else{
					      			%>
							      		<option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
							      	<%	
						      			}
						      		}
						      	%>
						      </select>
						  </td>
				</tr>
				<tr valign="bottom">
				  <td width="10%"  align="right" style="padding:0px;">供应商：</td>
			      <td width="90%" height="20%"  align="left" style="padding:0px;">
			      	<select name='supplier' id='supplier' onchange="changeSupplierAddress();">
			      		<option value="0">请选择供应商...</option>
			      	</select>
			      </td>
				</tr>
				 <tr height="29">
					           <td width="10%"  align="right" valign="middle" nowrap="nowrap">门牌号:</td>
					           <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_house_number" id="supplier_house_number" value='<%=purchaseRow.getString("supplier_house_number") %>' style="width:300">
					          </td>
					        </tr>
					        <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">街道:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_street" id="supplier_street" value='<%=purchaseRow.getString("supplier_street") %>' style="width:300">
					          </td>
					        </tr>
					        <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">城市:</td>
					          <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_city" id="supplier_city" value='<%=purchaseRow.getString("supplier_city") %>' style="width:300">
					          </td>
					        </tr>
					        <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">邮编:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_zip_code" id="supplier_zip_code" value='<%=purchaseRow.getString("supplier_zip_code") %>' style="width:300">
					          </td>
					        </tr>
					        <tr>
								<td width="10%"  align="right" valign="middle" nowrap="nowrap">省份:</td>
								<td width="90%"  align="left" valign="middle" nowrap="nowrap">
									<span id="addBillProSpanSupplier">
										<select disabled="disabled" name="supplier_province" id="supplier_province" onchange="handleProInputSupplier()" style="margin-right:5px;"></select>
									</span>
								</td>
							 </tr>
							 <tr>
							    <td width="10%"  align="right" valign="middle" nowrap="nowrap">国家:</td>
							   <td width="90%"  align="left" valign="middle" nowrap="nowrap">
								    <%
								   		DBRow countrycode[] = orderMgr.getAllCountryCode();
										String selectBgSupplier="#ffffff";
										String preLetterSupplier="10990";
									%>
							      	<select  id="supplier_native"  name="supplier_native" onChange="removeAfterProIdInputSupplier();setPro_idSupplier();">
								 	 	<option value="0">请选择...</option>
										  <%
										  for (int i=0; i<countrycode.length; i++){
										  	if (!preLetterSupplier.equals(countrycode[i].getString("c_country").substring(0,1))){
												if (selectBgSupplier.equals("#eeeeee")){
													selectBgSupplier = "#ffffff";
												}else{
													selectBgSupplier = "#eeeeee";
												}
											}  	
										  	preLetterSupplier = countrycode[i].getString("c_country").substring(0,1);
										  	if(purchaseRow.getString("supplier_native").equals(countrycode[i].getString("ccid"))){
									  		%>
									  		  <option style="background:<%=selectBgSupplier%>;"  code="<%=countrycode[i].getString("c_code") %>" value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
											<%	
										  	}else{
									  		%>
									  		  <option style="background:<%=selectBgSupplier%>;"  code="<%=countrycode[i].getString("c_code") %>" value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
										    <%	
										  	}
										  }
										%>
							   		 </select>
					   			</td>
						 	</tr>
						 	<tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系人:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_link_man" id="supplier_link_man" value='<%=purchaseRow.getString("supplier_link_man") %>'>
					          </td>
					        </tr>
					        <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系电话:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_link_phone" id="supplier_link_phone" value='<%= purchaseRow.getString("supplier_link_phone") %>'>
					          </td>
					        </tr>
			</table>
			</fieldset>
			<fieldset class="set" style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<legend style="font-size:15px;font-weight:normal;color:#999999;">收货地址信息</legend>
				<table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;" id="purchaseTable">
				<tr>
				  <td width="10%"  align="right" valign="middle" nowrap="nowrap">收货仓库：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
				      <select name='select' id='ps_id' onChange="selectStorage()">
                              <option value="0">请选择...</option>
                              <%
							  for ( int i=0; i<treeRows.length; i++ )
							  {
								  if(treeRows[i].getString("id").equals(purchaseRow.get("ps_id",0)+"")){
							  %>
                              <option value='<%=treeRows[i].getString("id")%>' selected="selected"><%=treeRows[i].getString("title")%></option>
                              <%	  
								  }else{
						      %>
                              <option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
                              <%
								  }
								}
							%>
                            </select>
                           </td>
				</tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">门牌号：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      	<input name="deliver_house_number" type="text" id="deliver_house_number" value='<%=purchaseRow.getString("deliver_house_number") %>' style="width:400px;"/>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">街道：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      <input name="deliver_street" type="text" id="deliver_street" value='<%=purchaseRow.getString("deliver_street") %>' style="width:400px;"/>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">城市：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      <input name="deliver_city" type="text" id="deliver_city" value='<%=purchaseRow.getString("deliver_city") %>' style="width:400px;"/>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">邮编：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      <input name="deliver_zip_code" type="text" id="deliver_zip_code" value='<%=purchaseRow.getString("deliver_zip_code") %>' style="width:400px;"/>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">省份：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      	<span id="addBillProSpan">
						<select disabled="disabled" id="pro_id" name="pro_id" onchange="handleProInput()" style="margin-right:5px;"></select>
					</span>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">国家：</td>
			     <td width="90%"  align="left" valign="middle" nowrap="nowrap">
						<%
							String selectBg="#ffffff";
							String preLetter="";
						%>
				      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();">
					 	 	<option value="0">请选择...</option>
							  <%
							  for (int i=0; i<countrycode.length; i++){
							  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
									if (selectBg.equals("#eeeeee")){
										selectBg = "#ffffff";
									}else{
										selectBg = "#eeeeee";
									}
								}  	
								preLetter = countrycode[i].getString("c_country").substring(0,1);
								if(countrycode[i].getString("ccid").equals(purchaseRow.get("deliver_native",0)+"")){
							 %>
					  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
							 <%	
								}else{
							 %>
					  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
							 <%
								}
							  }
							%>
			   			 </select>
				  </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系人：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      <input type="text" name="linkman" id="linkman" value='<%=purchaseRow.getString("delivery_linkman") %>'/>
			      </td>
			  </tr>
			  <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系电话：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
			      <input type="text" name="linkman_number" id="linkman_number" width="200px"  value='<%=purchaseRow.getString("linkman_phone") %>'/>
			      </td>
			  </tr>
			   <tr>
			  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">预计到货时间：</td>
			      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input type="text" name="expectArriveTime" id="expectArriveTime" width="200px" value="<%=expectArrTime %>"/></td>
			  </tr>
			      
		  </table>
		 </fieldset>
	    </td>
	    </tr>
		<tr>
			<td align="right" valign="bottom">				
				<table width="100%">
					<tr>
						<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
						  <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="addpurchase();">
						  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="windowClose();">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
</body>
</html>
