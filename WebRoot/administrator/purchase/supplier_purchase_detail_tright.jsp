<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id=StringUtil.getLong(request,"purchase_id");
	int purchase_status = StringUtil.getInt(request,"purchase_status");
	String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p",1));
	pc.setPageSize(30);
	
	DBRow[] rows; 
	
	rows = purchaseMgr.getPurchaseDetailByPurchaseid(purchase_id,pc,type);
	
	if(pc.getPageCount()<StringUtil.getInt(request,"p",1))
	{
		pc.setPageNo(1);
		rows = purchaseMgr.getPurchaseDetailByPurchaseid(purchase_id,pc,type);
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>



<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body >
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <tr>
    <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;"><div align="left">商品名</div></th>
    <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;"><div align="center">预计到达日期</div></th>
    <th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">到达数量</th>
	<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">订购数量</th>
	<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">单位</th>
    <th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">采购价格</th>
    <th width="10%" class="right-title " style="vertical-align: center;text-align: center;">当前价格</th>
  </tr>
  <%
  	for(int i=0;i<rows.length;i++)
  	{
  		String etadelay = "";
  		if(purchase_status != PurchaseKey.CANCEL&&purchase_status!=PurchaseKey.FINISH)
  	  	{
	  		if(!rows[i].getString("eta").equals(""))
	  		{
	  			TDate tdate =new TDate();
	  			long eta = tdate.getDateTime(rows[i].getString("eta"));
	  			long now = tdate.getDateTime(DateUtil.NowStr());
		  		if(eta<now&&rows[i].get("reap_count",0f)==0.0)
		  		{
		  			etadelay = "style='background-color: #FFFF99'";
		  		}
	  		}
  		}
  %>
  <tr align="center" valign="middle" >
    <td height="40" nowrap="nowrap" align="left" <%=etadelay %>><%=rows[i].getString("purchase_name")%></td>
    <td height="40" align="left" <%=etadelay %>><div align="center">
    <%
    	if(!rows[i].getString("eta").equals(""))
    	{
    		out.print(rows[i].getString("eta").substring(0,10));
    	} 
    %></div></td>
    <td height="40" <%=etadelay %>><%=rows[i].getString("reap_count") %></td>
	<td height="40" <%=etadelay %>><%=rows[i].getString("purchase_count")%></td>
	<td height="40" <%=etadelay %>><%=rows[i].getString("unit_name")%></td>
    	<%
    		String color = "";

	    	if(rows[i].get("price",0.00)>rows[i].get("product_history_price",0.00))
	    	{
	    		color = "red";
	    	}
	    	if(rows[i].get("price",0.00)<rows[i].get("product_history_price",0.00))
	    	{
	    		color = "green";
	    	}
    	%>
    <td height="40" <%=etadelay %>><font color="<%=color %>"><%=rows[i].getString("price")%></font>
    <%
    	if(color.equals("red"))
    	{
    		out.print("<img src='../imgs/arrow_up.png' style='border: 0px'/>");
    	}
    	else if(color.equals("green"))
    	{
    		out.print("<img src='../imgs/arrow_down.png' style='border: 0px'/>");
    	}
    %>
    </td>
    <td height="40" <%=etadelay %>><%=rows[i].getString("product_history_price")%></td>
  </tr>
  <%
  	}
   %>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
   		<td height="40" align="center" style="font-size: 12px; font-weight:bold;">总金额：<%=purchaseMgr.getPurchasePrice(purchase_id)%></td>
   </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
</body>
</html>



