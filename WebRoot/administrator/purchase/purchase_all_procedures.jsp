<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.CurrencyKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
	String purchase_id		= StringUtil.getString(request, "purchase_id");
	String tempfilename		= StringUtil.getString(request, "tempfilename");
	String expectArrTime	= StringUtil.getString(request, "expectArrTime");
	DBRow purchaseRow			= purchaseMgrLL.getPurchaseById(purchase_id);
	//采购员Ids, names
	DBRow[] purchaserPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
	String purchserPersonNames	= "";
	String purchserPersonIds	= "";
	if(purchaserPersons.length > 0){
		purchserPersonIds		+= purchaserPersons[0].get("schedule_execute_id",0);
		purchserPersonNames		+= purchaserPersons[0].getString("employe_name");
		for(int i = 1; i < purchaserPersons.length; i ++){
			DBRow purchaserPerson = purchaserPersons[i];
			purchserPersonNames	 += ","+purchaserPerson.getString("employe_name");
			purchserPersonIds	 += ","+purchaserPerson.get("schedule_execute_id",0);
		}
	}
	//调度员Ids, names
	/*
	DBRow[] dispatcherPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER);
	String dispatcherPersonNames= "";
	String dispatcherPersonIds	= "";
	if(dispatcherPersons.length > 0){
		dispatcherPersonIds		+= dispatcherPersons[0].get("schedule_execute_id",0);
		dispatcherPersonNames	+= dispatcherPersons[0].getString("employe_name");
		for(int i = 1; i < dispatcherPersons.length; i ++){
			DBRow dispatcherPerson = dispatcherPersons[i];
			dispatcherPersonNames+= ","+dispatcherPerson.getString("employe_name");
			dispatcherPersonIds	 += ","+dispatcherPerson.get("schedule_execute_id",0);
		}
	}
	*/
	//adminUserIdsInvoice
	//发票员Ids, names
	DBRow[] invoicePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
	String invoicePersonNames= "";
	String invoicePersonIds	= "";
	if(invoicePersons.length > 0){
		invoicePersonIds		+= invoicePersons[0].get("schedule_execute_id",0);
		invoicePersonNames	+= invoicePersons[0].getString("employe_name");
		for(int i = 1; i < invoicePersons.length; i ++){
			DBRow invoicePerson = invoicePersons[i];
			invoicePersonNames+= ","+invoicePerson.getString("employe_name");
			invoicePersonIds	 += ","+invoicePerson.get("schedule_execute_id",0);
		}
	}
	//adminUserIdsDrawback
	//退税Ids, names
	DBRow[] drawbackPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
	String drawbackPersonNames= "";
	String drawbackPersonIds	= "";
	if(drawbackPersons.length > 0){
		drawbackPersonIds		+= drawbackPersons[0].get("schedule_execute_id",0);
		drawbackPersonNames	+= drawbackPersons[0].getString("employe_name");
		for(int i = 1; i < drawbackPersons.length; i ++){
			DBRow drawbackPerson = drawbackPersons[i];
			drawbackPersonNames+= ","+drawbackPerson.getString("employe_name");
			drawbackPersonIds	 += ","+drawbackPerson.get("schedule_execute_id",0);
		}
	}
	//adminUserIdsTag
	//内部标签Ids, names
	DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
	String tagPersonNames= "";
	String tagPersonIds	= "";
	if(tagPersons.length > 0){
		tagPersonIds		+= tagPersons[0].get("schedule_execute_id",0);
		tagPersonNames	+= tagPersons[0].getString("employe_name");
		for(int i = 1; i < tagPersons.length; i ++){
			DBRow tagPerson = tagPersons[i];
			tagPersonNames  += ","+tagPerson.getString("employe_name");
			tagPersonIds	+= ","+tagPerson.get("schedule_execute_id",0);
		}
	}
	//第三方标签Ids, names
	DBRow[] tagThirdPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
	String tagThirdPersonNames= "";
	String tagThirdPersonIds	= "";
	if(tagThirdPersons.length > 0){
		tagThirdPersonIds		+= tagThirdPersons[0].get("schedule_execute_id",0);
		tagThirdPersonNames		+= tagThirdPersons[0].getString("employe_name");
		for(int i = 1; i < tagThirdPersons.length; i ++){
			DBRow tagThirdPerson = tagThirdPersons[i];
			tagThirdPersonNames  += ","+tagThirdPerson.getString("employe_name");
			tagThirdPersonIds	+= ","+tagThirdPerson.get("schedule_execute_id",0);
		}
	}
	//adminUserIdsQuality
	//质检Ids, names
	DBRow[] qualityPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
	String qualityPersonNames= "";
	String qualityPersonIds	= "";
	if(qualityPersons.length > 0){
		qualityPersonIds		+= qualityPersons[0].get("schedule_execute_id",0);
		qualityPersonNames	+= qualityPersons[0].getString("employe_name");
		for(int i = 1; i < qualityPersons.length; i ++){
			DBRow qualityPerson = qualityPersons[i];
			qualityPersonNames  += ","+qualityPerson.getString("employe_name");
			qualityPersonIds	+= ","+qualityPerson.get("schedule_execute_id",0);
		}
	}
	//adminUserIdsProductModel
	//商品范例Ids, names
	DBRow[] productModelPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
	String productModelPersonNames= "";
	String productModelPersonIds	= "";
	if(productModelPersons.length > 0){
		productModelPersonIds		+= productModelPersons[0].get("schedule_execute_id",0);
		productModelPersonNames	+= productModelPersons[0].getString("employe_name");
		for(int i = 1; i < productModelPersons.length; i ++){
			DBRow productModelPerson = productModelPersons[i];
			productModelPersonNames  += ","+productModelPerson.getString("employe_name");
			productModelPersonIds	+= ","+productModelPerson.get("schedule_execute_id",0);
		}
	}
%>
<title>采购单的各种流程信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
$(function(){
	$("input:radio[name=hasInvoice]").click(
			function(){
				var hasInvoinceCheck = $(this).val();
				if(2 == hasInvoinceCheck){
					$("#invoiceDetailTr").attr("style","");
					$("#invoicePersonTr").attr("style","");
				}else{
					$("#invoiceDetailTr").attr("style","display:none");
					$("#invoicePersonTr").attr("style","display:none");
				}
			}
	);
	$("input:radio[name=isDrawback]").click(function(){
		var isDrawbackCheck = $(this).val();
		$("#rebate_rate_title") && $("#rebate_rate_title").remove();
		$("#rebate_rate") && $("#rebate_rate").remove();
		$("#dot2") && $("#dot2").remove();
		if(2 == isDrawbackCheck){
			$("#drawbackPersonTr").attr("style","");
			$("#drawBackTd").append($('<span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" style="width:50px;"/><span id="dot2">%</span>'));
		}else{
			$("#drawbackPersonTr").attr("style","display:none");
			$("#rebate_rate_title") && $("#rebate_rate_title").remove();
			$("#rebate_rate") && $("#rebate_rate").remove();
			$("#dot2") && $("#dot2").remove();
		}
	});
	$("input:radio[name=needTagThird]").click(function(){
		var isNeedTagCheck = $(this).val();
		if(2 == isNeedTagCheck){
			$("#tagPersonTrThird").attr("style","");
		}else{
			$("#tagPersonTrThird").attr("style","display:none");
		}
	});
});
function previousStep(){
	document.previousStepForm.submit();
}
function ajaxSavePurchaseDetail(){
	if("" == $("#adminUserNamesPurchaser").val())
	{
		alert("采购负责人不能为空");
	}
	//else if("" == $("#adminUserNamesDispatcher").val())
	//{
	//	alert("调度负责人不能为空");
	//}
	else if("" == $("#adminUserNamesTag").val())
	{
		alert("内部标签流程负责人不能为空");
	}
	else if("" == $("#adminUserNamesQuality").val())
	{
		alert("质检要求流程负责人不能为空");
	}
	else if("" == $("#adminUserNamesProductModel").val())
	{
		alert("商品范例负责人不能为空");
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" == $("#adminUserNamesInvoice").val())
	{
		alert("发票流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=isDrawback]:checked").val() && "" == $("#adminUserNamesDrawback").val())
	{
		alert("退税流程负责人不能为空");
	}
	else if(2 == $("input:radio[name=needTagThird]:checked").val() && "" == $("#adminUserNamesTagThird").val())
	{
		alert("第三方标签负责人不能为空");
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" != $("#invoince_amount").val() && 0 == $("#invoince_currency").val()){
		alert("请选择币种");
	}else if(2 == $("input:radio[name=hasInvoice]:checked").val() && 0 != $("#invoince_currency").val() && "" == $("#invoince_amount").val()){
		alert("请填写开票金额"); 
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && $("#invoince_amount") && $("#invoince_amount").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_amount").val())){
		alert("开票金额，请填写数值");
	}else if(2 == $("input:radio[name=hasInvoice]:checked").val() && $("#invoince_dot") && $("#invoince_dot").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_dot").val())){
		alert("票点，请填写数值");
	}else if(2 == $("input:radio[name=isDrawback]:checked").val() && $("#rebate_rate") && $("#rebate_rate").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#rebate_rate").val())){
		alert("退税率，请填写数值");
	}
	else{
		document.proceduresForm.invoice.value = $("input:radio[name=hasInvoice]:checked").val();
		document.proceduresForm.drawback.value = $("input:radio[name=isDrawback]:checked").val();
		document.proceduresForm.need_tag_third.value = $("input:radio[name=needTagThird]:checked").val();
		document.proceduresForm.invoince_amount.value = $("#invoince_amount").val();
		document.proceduresForm.invoince_currency.value = $("#invoince_currency").val();		
		document.proceduresForm.invoince_dot.value = $("#invoince_dot").val();
		document.proceduresForm.rebate_rate.value = $("#rebate_rate").val();
		document.proceduresForm.adminUserNamesPurchaser.value = $("#adminUserNamesPurchaser").val();
		//document.proceduresForm.adminUserNamesDispatcher.value = $("#adminUserNamesDispatcher").val();
		document.proceduresForm.adminUserNamesInvoice.value = $("#adminUserNamesInvoice").val();
		document.proceduresForm.adminUserNamesDrawback.value = $("#adminUserNamesDrawback").val();
		document.proceduresForm.adminUserNamesTag.value = $("#adminUserNamesTag").val();
		document.proceduresForm.adminUserNamesTagThird.value = $("#adminUserNamesTagThird").val();
		document.proceduresForm.adminUserNamesQuality.value = $("#adminUserNamesQuality").val();
		document.proceduresForm.adminUserNamesProductModel.value = $("#adminUserNamesProductModel").val();
	
		if($("input:checkbox[name=isMailInvoice]").attr("checked")){
			document.proceduresForm.needMailInvoice.value = 2;
		}
		if($("input:checkbox[name=isMessageInvoice]").attr("checked")){
			document.proceduresForm.needMessageInvoice.value = 2;
		}
		if($("input:checkbox[name=isPageInvoice]").attr("checked")){
			document.proceduresForm.needPageInvoice.value = 2;
		}
		if($("input:checkbox[name=isMailDrawback]").attr("checked")){
			document.proceduresForm.needMailDrawback.value = 2;
		}
		if($("input:checkbox[name=isMessageDrawback]").attr("checked")){
			document.proceduresForm.needMessageDrawback.value = 2;
		}
		if($("input:checkbox[name=isPageDrawback]").attr("checked")){
			document.proceduresForm.needPageDrawback.value = 2;
		}
		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.proceduresForm.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.proceduresForm.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.proceduresForm.needPageTag.value = 2;
		}
		if($("input:checkbox[name=isMailTagThird]").attr("checked")){
			document.proceduresForm.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked")){
			document.proceduresForm.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked")){
			document.proceduresForm.needPageTagThird.value = 2;
		}
		if($("input:checkbox[name=isMailQuality]").attr("checked")){
			document.proceduresForm.needMailQuality.value = 2;
		}
		if($("input:checkbox[name=isMessageQuality]").attr("checked")){
			document.proceduresForm.needMessageQuality.value = 2;
		}
		if($("input:checkbox[name=isPageQuality]").attr("checked")){
			document.proceduresForm.needPageQuality.value = 2;
		}
		if($("input:checkbox[name=isMailProductModel]").attr("checked")){
			document.proceduresForm.needMailProductModel.value = 2;
		}
		if($("input:checkbox[name=isMessageProductModel]").attr("checked")){
			document.proceduresForm.needMessageProductModel.value = 2;
		}
		if($("input:checkbox[name=isPageProductModel]").attr("checked")){
			document.proceduresForm.needPageProductModel.value = 2;
		}
		$(".cssDivschedule").css("display","block");
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/PurchaseAllProceduresSaveAction.action',
			data:$("#proceduresForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "true"){
					showMessage("修改成功","success");
					setTimeout("windowClose()", 1000);
				}else{
					showMessage("修改失败","error");
					$(".cssDivschedule").css("display","none");
				}
			},
			error:function(){
				showMessage("系统错误","error");
				$(".cssDivschedule").css("display","none");
			}
		})	
	}
};
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function adminUserPurchaser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsPurchaser").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowPurchaser'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesPurchaser").val(user_names);
}
//function adminUserDispatcher(){
//	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
//	 var option = {
// 			 single_check:0, 					// 1表示的 单选
// 			 user_ids:$("#adminUserIdsDispatcher").val(), //需要回显的UserId
// 			 not_check_user:"",					//某些人不 会被选中的
// 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
// 			 ps_id:'0',						//所属仓库
// 			 handle_method:'setParentUserShowDispatcher'
// 	 };
// 	 uri  = uri+"?"+jQuery.param(option);
// 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
//}
//function setParentUserShowDispatcher(user_ids , user_names){
//	$("#adminUserIdsDispatcher").val(user_ids);
//	$("#adminUserNamesDispatcher").val(user_names);
//}
function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowInvoice'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowInvoice(user_ids , user_names){
	$("#adminUserIdsInvoice").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
}
function adminUserDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsDrawback").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowDrawback'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowDrawback(user_ids , user_names){
	$("#adminUserIdsDrawback").val(user_ids);
	$("#adminUserNamesDrawback").val(user_names);
}
function adminUserTag(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowTag'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowTag(user_ids , user_names){
	$("#adminUserIdsTag").val(user_ids);
	$("#adminUserNamesTag").val(user_names);
}
function adminUserTagThird(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsTagThird").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowTagThird'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowTagThird(user_ids , user_names){
	$("#adminUserIdsTagThird").val(user_ids);
	$("#adminUserNamesTagThird").val(user_names);
}
function adminUserQuality(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsQuality").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowQuality'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowQuality(user_ids , user_names){
	$("#adminUserIdsQuality").val(user_ids);
	$("#adminUserNamesQuality").val(user_names);
}
function adminUserProductModel(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsProductModel").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowProductModel'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowProductModel(user_ids , user_names){
	$("#adminUserIdsProductModel").val(user_ids);
	$("#adminUserNamesProductModel").val(user_names);
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

#purchaseTable tr td{
	line-height:30px;
	height:30px;
}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:9999999;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="cssDivschedule" style=""></div>
<form name="previousStepForm" action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail_show.html'>
<input type="hidden" name="tempfilename" id="tempfilename" value="<%=tempfilename%>"/>
<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
<input type="hidden" name="expectArrTime" id="expectArrTime" value="<%=expectArrTime %>"/>
</form>
<form action="" id="proceduresForm" name="proceduresForm" method="post" >
<input type="hidden" name="tempfilename" id="tempfilename" value="<%=tempfilename%>"/>
<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id%>"/>
<input type="hidden" name="expectArrTime" id="expectArrTime" value="<%=expectArrTime %>"/>
<input type="hidden" name="invoice"/>
<input type="hidden" name="drawback"/>
<input type="hidden" name="invoince_amount"/>
<input type="hidden" name="invoince_currency"/>
<input type="hidden" name="invoince_dot"/>
<input type="hidden" name="rebate_rate"/>
<input type="hidden" name="need_tag_third"/>

<input type="hidden" name="needMailInvoice"/>
<input type="hidden" name="needMessageInvoice"/>
<input type="hidden" name="needPageInvoice"/>
<input type="hidden" name="needMailDrawback"/>
<input type="hidden" name="needMessageDrawback"/>
<input type="hidden"name="needPageDrawback"/>
<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>
<input type="hidden" name="needMailTagThird"/>
<input type="hidden" name="needMessageTagThird"/>
<input type="hidden" name="needPageTagThird"/>
<input type="hidden" name="needMailQuality"/>
<input type="hidden" name="needMessageQuality"/>
<input type="hidden" name="needPageQuality"/>
<input type="hidden" name="needMailProductModel"/>
<input type="hidden" name="needMessageProductModel"/>
<input type="hidden" name="needPageProductModel"/>
<!-- 各流程相应的人的ID 及name -->
<input type="hidden" id="adminUserIdsPurchaser" name="adminUserIdsPurchaser" value="<%=purchserPersonIds %>"/>
<%--<input type="hidden" id="adminUserIdsDispatcher" name="adminUserIdsDispatcher" value="<%=dispatcherPersonIds %>"/>--%>
<input type="hidden" id="adminUserIdsInvoice" name="adminUserIdsInvoice" value="<%=invoicePersonIds %>"/>
<input type="hidden" id="adminUserIdsDrawback" name="adminUserIdsDrawback" value="<%=drawbackPersonIds %>"/>
<input type="hidden" id="adminUserIdsTag" name="adminUserIdsTag" value="<%=tagPersonIds %>"/>
<input type="hidden" id="adminUserIdsTagThird" name="adminUserIdsTagThird" value="<%=tagThirdPersonIds %>"/>
<input type="hidden" id="adminUserIdsQuality" name="adminUserIdsQuality" value="<%=qualityPersonIds %>"/>
<input type="hidden" id="adminUserIdsProductModel" name="adminUserIdsProductModel" value='<%=productModelPersonIds %>'/>

<input type="hidden" name="adminUserNamesPurchaser" value="<%=purchserPersonNames %>"/>
<%--<input type="hidden" name="adminUserNamesDispatcher" value="<%=dispatcherPersonNames %>"/>--%>
<input type="hidden" name="adminUserNamesInvoice" value="<%=invoicePersonNames %>"/>
<input type="hidden" name="adminUserNamesDrawback" value="<%=drawbackPersonNames %>"/>
<input type="hidden" name="adminUserNamesTag" value="<%=tagPersonNames %>"/>
<input type="hidden" name="adminUserNamesTagThird" value="<%=tagThirdPersonNames %>"/>
<input type="hidden" name="adminUserNamesQuality" value="<%=qualityPersonNames %>"/>
<input type="hidden" name="adminUserNamesProductModel" value="<%=productModelPersonNames %>"/>

</form>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="25px;">
		<td colspan="2">
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
		 <tr>
		<td align="left" style="font-family:'黑体'; font-size:18px;border-bottom:1px solid #999999;color:#000000;">
		采购单各流程信息<br/></td>
		 </tr>
		</table>
		</td>
	</tr>
	<tr height="15px;"><td colspan="2"></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">采购员：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesPurchaser" name="adminUserNamesPurchaser" value="<%=purchserPersonNames %>" style="width:180px;" onclick="adminUserPurchaser()"/>
	    </td>
	</tr>
<%--	<tr height="25px;">--%>
<%--		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">调度：</td>--%>
<%--	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">--%>
<%--	   		<input type="text" id="adminUserNamesDispatcher" name="adminUserNamesDispatcher" value="<%=dispatcherPersonNames %>" style="width:180px;" onclick="adminUserDispatcher()"/>--%>
<%--	    </td>--%>
<%--	</tr>--%>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">商品范例：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<%
	    	out.print(new PurchaseProductModelKey().getProductModelKeyName(purchaseRow.getString("product_model")));
	   	%>
	    </td>
	</tr>
	<tr height="25px;" id="productModelPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesProductModel" name="adminUserNamesProductModel" value="<%=productModelPersonNames %>" style="width:180px;" onclick="adminUserProductModel()"/>
	    	通知：
	    	<%
	    		DBRow scheduleProductModel		= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
	    		int mailProductModel			= 2;//1提醒
	    		int messageProductModel			= 2;
	    		int pageProductModel			= 2;
	    		if(null != scheduleProductModel){
	    			mailProductModel			= scheduleProductModel.get("sms_email_notify",2);//1提醒
		    		messageProductModel			= scheduleProductModel.get("sms_short_notify",2);
		    		pageProductModel			= scheduleProductModel.get("is_need_replay",2);
	    		}
	    	%>
	   		<input type="checkbox" name="isMailProductModel" <%if(1 == mailProductModel || 2 == mailProductModel){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageProductModel" <%if(1 == messageProductModel || 2 == messageProductModel){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageProductModel" <%if(1 == pageProductModel || 2 == pageProductModel){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">内部标签：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<%
	   		TransportTagKey transportTagKey = new TransportTagKey();
	   		out.println(transportTagKey.getTransportTagById(purchaseRow.get("need_tag",0)));
	   %>
	    </td>
	</tr>
	<tr height="25px;" id="tagPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" value="<%=tagPersonNames %>" style="width:180px;" onclick="adminUserTag()"/>
	    	通知：
	    	<%
	    		DBRow scheduleTag		= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
		    	int mailTag				= 2;//1提醒
	    		int messageTag			= 2;
	    		int pageTag				= 2;	
	   		 	if(null != scheduleTag){
		   		 	mailTag				= scheduleTag.get("sms_email_notify",2);//1提醒
		    		messageTag			= scheduleTag.get("sms_short_notify",2);
		    		pageTag				= scheduleTag.get("is_need_replay",2);	
	    		}
	    	%>
	   		<input type="checkbox" name="isMailTag" <%if(1 == mailTag || 2 == mailTag){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageTag" <%if(1 == messageTag || 2 == messageTag){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageTag" <%if(1 == pageTag || 2 == pageTag){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">质检要求：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	   	<%
	    	out.print(new QualityInspectionKey().getQualityInspectionById(purchaseRow.get("quality_inspection",0)));
	   	%>
	    </td>
	</tr>
	<tr height="25px;" id="qualityPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesQuality" name="adminUserNamesQuality" value="<%=qualityPersonNames %>" style="width:180px;" onclick="adminUserQuality()"/>
	    	通知：
	    	<%
	    		DBRow scheduleQuality		= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
	    		int mailQuality				= 2;//1提醒
	    		int messageQuality			= 2;
	    		int pageQuality				= 2;
	    		if(null != scheduleQuality){
	    			mailQuality				= scheduleQuality.get("sms_email_notify",2);//1提醒
		    		messageQuality			= scheduleQuality.get("sms_short_notify",2);
		    		pageQuality				= scheduleQuality.get("is_need_replay",2);
	    		}
	    	%>
	   		<input type="checkbox" name="isMailQuality" <%if(1 == mailQuality || 2 == mailQuality){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageQuality" <%if(1 == messageQuality || 2 == messageQuality){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageQuality" <%if(1 == pageQuality || 2 == pageQuality){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">发票：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    <%
	   	 if(1 == purchaseRow.get("invoice",0) || 2 == purchaseRow.get("invoice",0) || 0 == purchaseRow.get("invoice",0)){
	    %>
	    <input type="radio" name="hasInvoice" value="2" <%if(2 == purchaseRow.get("invoice",0) || 0 == purchaseRow.get("invoice",0)){out.print("checked='checked'");}%>/>需要
	    <input type="radio" name="hasInvoice" value="1" <%if(1 == purchaseRow.get("invoice",0)){out.print("checked='checked'");}%>/>不需要
	    <%}else{
	    	out.print(new InvoiceKey().getInvoiceById(purchaseRow.get("invoice",0)));
	    	out.print("&nbsp;&nbsp;开票金额："+purchaseRow.get("invoince_amount",0.0));
    	 	CurrencyKey currencyKey	= new CurrencyKey();
    	 	out.print(" "+currencyKey.getCurrencyNameById(String.valueOf(purchaseRow.get("invoince_currency",-1))));
    	 	out.print("，&nbsp;&nbsp;票点："+purchaseRow.get("invoince_dot",0.0)+"%");
	    } %>
	    </td>
	</tr>
	<%
		String invoiceDetailTrStyle = "display:none";
	   	 if(2 == purchaseRow.get("invoice",0) || 0 == purchaseRow.get("invoice",0)){
			invoiceDetailTrStyle = "";
		}   	
	%>
	<tr id="invoiceDetailTr" height="25px;" style='<%=invoiceDetailTrStyle %>'">
		<td width="13%" align="right">
			开票金额：
		</td>
		<td width="87%">
			<input id="invoince_amount" name="invoince_amount" value='<%=purchaseRow.get("invoince_amount",0.0) %>' style="width:50px;"/>
	    	&nbsp;币种：
	    	<select id="invoince_currency" name="invoince_currency">
	    	<option value="0">请选择</option>
	    	<% 
	    		CurrencyKey currencyKey	= new CurrencyKey();
	    		List currencyList		= currencyKey.CurrencyKeyList();
	    		for(int i = 0; i < currencyList.size(); i ++){
	    			if(((String)currencyList.get(i)).equals(purchaseRow.get("invoince_currency",-1)+"")){
  			%>		
   				<option value="<%=currencyList.get(i) %>" selected="selected"><%=currencyKey.getCurrencyNameById((String)currencyList.get(i)) %></option>
   			<%		
	     			}else{
	    	%>		
	    			<option value="<%=currencyList.get(i) %>"><%=currencyKey.getCurrencyNameById((String)currencyList.get(i)) %></option>
	    	<%		
	     			}
	     		}
	    	%>
	  	  </select>
	  	  &nbsp;票点：<input id="invoince_dot" name="invoince_dot" style="width:50px;" value='<%=purchaseRow.get("invoince_dot",0.0) %>'/><span id="dot1">%</span>
		</td>
	</tr>
	<%
		String invoicePersonTrStyle = "display:none";
		if(1 != purchaseRow.get("invoice",0)){
			invoicePersonTrStyle = "";
		}
	%>
	<tr height="25px;" id="invoicePersonTr" style="<%=invoicePersonTrStyle %>">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" value="<%=invoicePersonNames %>" style="width:180px;" onclick="adminUserInvoice()"/>
	    	通知：
	    	<%
	    		DBRow scheduleInvoice	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
		    	int mailInvoice			= 2;//1提醒
	    		int messageInvoice		= 2;
	    		int pageInvoice			= 2;	
	   		 	if(null != scheduleInvoice){
		   		 	mailInvoice			= scheduleInvoice.get("sms_email_notify",2);//1提醒
		    		messageInvoice		= scheduleInvoice.get("sms_short_notify",2);
		    		pageInvoice			= scheduleInvoice.get("is_need_replay",2);
	    		}
	    		
	    	%>
	   		<input type="checkbox" name="isMailInvoice" <%if(1 == mailInvoice || 2 == mailInvoice){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageInvoice" <%if(1 == messageInvoice || 2 == messageInvoice){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageInvoice" <%if(1 == pageInvoice || 2 == pageInvoice){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">退税：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap" id="drawBackTd">
	     <%
	   	 	if(1 == purchaseRow.get("drawback",0) || 2 == purchaseRow.get("drawback",0) || 0 == purchaseRow.get("drawback",0)){
	     %>
	   	<input type="radio" name="isDrawback" value="2" <%if(2 == purchaseRow.get("drawback",0) || 0 == purchaseRow.get("drawback",0)){out.print("checked='checked'");}%>/>需要
	   	<input type="radio" name="isDrawback" value="1" <%if(1 == purchaseRow.get("drawback",0)){out.print("checked='checked'");}%>/>不需要
	   		<%if(2 == purchaseRow.get("drawback",0) || 0 == purchaseRow.get("drawback",0)){ %>
	   	 <span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" value="<%=purchaseRow.get("rebate_rate",0.0) %>" style="width:50px;"/><span id="dot2">%</span>
	    <%	  }
	    	}else if(1 == purchaseRow.get("drawback",0)){}else{
	    		out.print(new DrawbackKey().getDrawbackById(purchaseRow.get("drawback",0)));
	    		out.print("&nbsp;&nbsp;退税率："+purchaseRow.get("rebate_rate",0.0)+"%");	    		
	    	}
	    %>
	    </td>
	</tr>
	 <%
	 	String drawbackPersonTrStyle = "display:none";
   	 	if(1 != purchaseRow.get("drawback",0)){
   	 		drawbackPersonTrStyle = "";
    	}
	 %>
	<tr height="25px;" style="<%=drawbackPersonTrStyle %>" id="drawbackPersonTr">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesDrawback" name="adminUserNamesDrawback" value="<%=drawbackPersonNames %>" style="width:180px;" onclick="adminUserDrawback()"/>
	    	通知：
	    	<%
	    		DBRow scheduleDrawback	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
		    	int mailDrawback		= 2;//1提醒
	    		int messageDrawback		= 2;
	    		int pageDrawback		= 2;
	    		if(null != scheduleDrawback){
	    			mailDrawback		= scheduleDrawback.get("sms_email_notify",2);//1提醒
		    		messageDrawback		= scheduleDrawback.get("sms_short_notify",2);
		    		pageDrawback		= scheduleDrawback.get("is_need_replay",2);
	    		}
	    	%>
	   		<input type="checkbox" name="isMailDrawback" <%if(1 == mailDrawback || 2 == mailDrawback){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageDrawback" <%if(1 == messageDrawback || 2 == messageDrawback){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageDrawback" <%if(1 == pageDrawback || 2 == pageDrawback){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
	<tr height="25px;">
		 <td width="13%"  align="right" valign="middle" nowrap="nowrap">第三方标签：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	     <%
	   	 	if(1 == purchaseRow.get("need_third_tag",0) || 2 == purchaseRow.get("need_third_tag",0) || 0 == purchaseRow.get("need_third_tag",0)){
	     %>
	   	<input type="radio" name="needTagThird" value="2" <%if(2 == purchaseRow.get("need_third_tag",0) || 0 == purchaseRow.get("need_third_tag",0)){out.print("checked='checked'");}%>/>需要
	   	<input type="radio" name="needTagThird" value="1" <%if(1 == purchaseRow.get("need_third_tag",0)){out.print("checked='checked'");}%>/>不需要
	   	<%}else{
	   		TransportTagKey transportThirdTagKey = new TransportTagKey();
	   		out.println(transportThirdTagKey.getTransportTagById(purchaseRow.get("need_third_tag",0)));
	   	}%>
	    </td>
	</tr>
	 <%
	 	String tagPersonTrStyle = "display:none";
	   	 if(1 != purchaseRow.get("need_third_tag",0)){
	   		tagPersonTrStyle = "";
	   	 }
	 %>
	<tr height="25px;" style="<%=tagPersonTrStyle %>" id="tagPersonTrThird">
		<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
	    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
	    	<input type="text" id="adminUserNamesTagThird" name="adminUserNamesTagThird" value="<%=tagThirdPersonNames %>" style="width:180px;" onclick="adminUserTagThird()"/>
	    	通知：
	    	<%
	    		DBRow scheduleTagThird	= scheduleMgrZR.getScheduleByAssociate(Long.parseLong(purchase_id), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
		    	int mailTagThird		= 2;//1提醒
	    		int messageTagThird		= 2;
	    		int pageTagThird		= 2;	
	   		 	if(null != scheduleTagThird){
		   		 	mailTagThird		= scheduleTag.get("sms_email_notify",2);//1提醒
		    		messageTagThird		= scheduleTag.get("sms_short_notify",2);
		    		pageTagThird		= scheduleTag.get("is_need_replay",2);	
	    		}
	    	%>
	   		<input type="checkbox" name="isMailTagThird" <%if(1 == mailTagThird || 2 == mailTagThird){out.print("checked='checked'");}%>/>邮件
	   		<input type="checkbox" name="isMessageTagThird" <%if(1 == messageTagThird || 2 == messageTagThird){out.print("checked='checked'");}%>/>短信
	   		<input type="checkbox" name="isPageTagThird" <%if(1 == pageTagThird || 2 == pageTagThird){out.print("checked='checked'");}%>/>页面
		</td>
	</tr>
	<tr>
 	<td valign="bottom" colspan="2">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="right" valign="middle" class="win-bottom-line"> 
 				<%
 					if(2 != StringUtil.getInt(request, "isOutterUpdate")){
 				%>
 				<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">	
 				<% }
 				%>
			    <input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSavePurchaseDetail()"/>
				<input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>

</body>
</html>