<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String purchaseid = StringUtil.getString(request,"purchase_id");

DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchaseid);

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<script>
	
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>

<script type="text/javascript">
	function modPurchaseDelivery()
	{
		if($("#address").val()=="")
		{
			alert("请填写交货地址！");
		}
		else if($("#linkman").val()=="")
		{
			alert("请填写联系人！");
		}
		else if($("#linkman_number").val()=="")
		{
			alert("必须留下联系方式！");
		}
		
		else
		{
			document.mod_delivery_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/modPurchaseDelivery.action";
			document.mod_delivery_form.delivery_address.value = $("#address").val();
			document.mod_delivery_form.delivery_linkman.value = $("#linkman").val();
			document.mod_delivery_form.linkman_phone.value = $("#linkman_number").val();
			document.mod_delivery_form.submit();
		}
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<form method="post" name="mod_delivery_form">
			<input type="hidden" name="purchase_id" value="<%=purchase.get("purchase_id",0l) %>"/>
			<input type="hidden" name="delivery_address"/>
			<input type="hidden" name="delivery_linkman"/>
			<input type="hidden" name="linkman_phone"/>
			<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_upload_excel.html"/>
			</form>
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><br>
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						  <tr>
							<td align="left" style="font-family:'黑体'; font-size:25px;border-bottom:1px solid #999999;color:#000000;">
							修改交货地址							</td>
						  </tr>
					  </table>
						<br/>
                        <br>
                        <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;">						      
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4" >交货地点：</td>
						      <td width="90%"  align="left" valign="middle" nowrap><input name="address" type="text" id="address" style="width: 600px;" value="<%=purchase.getString("delivery_address")%>"/></td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4" >联系人：</td>
						      <td width="90%"  align="left" valign="middle" nowrap class="STYLE1 STYLE2"><input type="text" name="linkman" id="linkman" value="<%=purchase.getString("delivery_linkman")%>"/></td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4">联系电话：</td>
						      <td width="90%"  align="left" valign="middle" nowrap class="STYLE1 STYLE2" ><input type="text" name="linkman_number" id="linkman_number" value="<%=purchase.getString("linkman_phone")%>" width="200px"/></td>
						  </tr>
						  <tr>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2 STYLE4" ></td>
							  <td  align="left" valign="middle" nowrap class="STYLE1 STYLE2" ></td>
						  </tr>
					  </table>
				<script>
					$("#eta").date_input();
				</script>
				    </td></tr>
				
				<tr>
					<td align="right" valign="bottom">				
						<table width="100%">
							<tr>
								<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
								  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modPurchaseDelivery();">
								  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWinNotRefresh();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
</body>
</html>
