<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
long purchase_id = StringUtil.getLong(request,"purchase_id");
long pcid = StringUtil.getLong(request,"pcid");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");

DBRow[] rows;

if(cmd.equals("search"))
{
	rows = productMgrZJ.searchProductsByPurchaseIdKey(purchase_id,name,null);
}
else
{
	rows = productMgrZJ.getProductsByPurchaseId(purchase_id,null);
}

DBRow[] options = lableTemplateMgrZJ.getAllLableTemplate(null);

Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签打印</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script language="javascript">

function search()
{
	
	document.search_form.name.value = $("#filter_name").val();	
	
	if($("#filter_name").val()!="")
	{
		document.search_form.cmd.value = "search";
	}
	else
	{
		document.search_form.cmd.value = "";	
	}
	
	document.search_form.submit();
}

function importPrint(width,height)
{
	tb_show('打印采购单内的商品条码','<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/print_purchase_barcode_show.html?purchase_id=<%=purchase_id%>&print_range_width='+width+'&print_range_height='+height+'&TB_iframe=true&height=500&width=800',false);
}

function noNumbers(e)
	{
		var keynum
		var keychar
		var numcheck
		if(e.keyCode==8||e.keyCode==46||e.keyCode==37||e.keyCode==38||e.keyCode==39||e.keyCode==40)
		{
			return true;
		}
		else if(window.event) // IE
		{
		  keynum = e.keyCode
		}
		else if(e.which) // Netscape/Firefox/Opera
		{
		  keynum = e.which
		}
		keychar = String.fromCharCode(keynum)
		numcheck = /[^\d]/
		return !numcheck.test(keychar);
	}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 


function closeWin()
{
	tb_remove();
}

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#filter_name").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action", {
		minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill:false,//自动填充匹配
		max:15,
		selectFirst:false,
		cacheLength:1,	//不缓存
		updownSelect:true,
		dataType: 'json',
		
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},
		scroll:false,
				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name,
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
});
//-->

	function printProductLabel(pc_id,width,height)
	{
		printer = "LabelPrinter";
		paper = width+"X"+height;
		print_range_width = width;
		print_range_height = height;
		template_path = "lable_template/productbarcode.html";
													
							
		var counts = document.getElementById("productCount_"+pc_id).value;
		document.print_product_label.pc_id.value = pc_id;
		if(parseInt(document.getElementById("productprintcount_"+pc_id).value)!=document.getElementById("productprintcount_"+pc_id).value)
		{
			alert("打印数量有问题！");
		}
		else
		{
			document.print_product_label.copies.value = $("#productprintcount_"+pc_id).val();
			document.print_product_label.printer.value= printer;
			document.print_product_label.paper.value = paper;
			document.print_product_label.print_range_width.value = print_range_width;
			document.print_product_label.print_range_height.value = print_range_height;
			document.print_product_label.counts.value = counts;
							
			document.print_product_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_product_label.submit();
		}
	}
	
	function printBoxLabel(pc_id)
	{
		printer = "LabelPrinter";
		paper = "100X50";
		print_range_width = 100;
		print_range_height = 50;
		template_path = "lable_template/productOutSize.html";
													
							
		var counts = document.getElementById("productCount_"+pc_id).value;
		
		if(parseFloat(document.getElementById("productCount_"+pc_id).value)!=document.getElementById("productCount_"+pc_id).value)
		{
			alert("装箱数量有误！");
		}
		else if((parseInt(document.getElementById("boxprintcount_"+pc_id).value))!=document.getElementById("boxprintcount_"+pc_id).value)
		{
			alert("打印数量有误！");
		}
		else
		{
			document.print_box_label.pc_id.value = pc_id
			document.print_box_label.copies.value = $("#boxprintcount_"+pc_id).val();
			document.print_box_label.printer.value= printer;
			document.print_box_label.paper.value = paper;
			document.print_box_label.print_range_width.value = print_range_width;
			document.print_box_label.print_range_height.value = print_range_height;
			document.print_box_label.counts.value = counts;
						
			document.print_box_label.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+template_path;
			document.print_box_label.submit();
		}
	}
	
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<div align="center">
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"> 商品条码</td>
  </tr>
</table>
<br>
	  <form action="print_purchase_label.html" method="get" name="search_form">		
		  <input type="hidden" name="cmd"> 
		  <input type="hidden" name="cid"> 
		  <input type="hidden" name="type"> 
		  <input type="hidden" name="pcid"> 
		  <input type="hidden" name="name">
		  <input type="hidden" name="purchase_id" value="<%=purchase_id%>"/> 
	</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td width="54%" height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td valign="middle"" width="317"><input name="filter_name" type="text" id="filter_name" value="<%=name%>" style="width:200px;" onKeyDown="if(event.keyCode==13)search()">&nbsp;&nbsp;&nbsp;&nbsp;
        		<input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="search()"> 
        	  </td>
        	  <td align="center" valign="middle">
        	  批量打印商品标签：
        	  	<input type="button" onClick="importPrint(80,35)" class="long-button-print" value="80X35"/>
        	  	&nbsp;&nbsp;&nbsp;&nbsp;
        	  	<input type="button" onClick="importPrint(60,30)" class="long-button-print" value="60X30"/>
        	  </td>
            </tr>
          </table>

			</div>
		  </td>
    <td width="46%" align="right" style="padding-right:10px;"><br>
    <br></td>
  </tr>
</table>
	<td><br></td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">
    <tr>
        <th width="179" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
		<th width="264"  style="vertical-align: center;text-align: center;" class="right-title">商品条码</th>
        <th width="246"  style="vertical-align: center;text-align: center;" class="right-title">打印商品条码</th>
        <th width="245"  style="vertical-align: center;text-align: center;" class="right-title">打印外箱条码</th>
    </tr>

    <%
String delPCID = "";
DBRow proGroup[];
for ( int i=0; i<rows.length; i++ )
{
delPCID += rows[i].getString("pc_id")+",";
%>
    <tr> 
      <td height="100" valign="middle"   style="font-size:14px;">
        
      <%=rows[i].getString("p_name")%></td>
	  <td align="center" valign="middle">
      	<%=rows[i].getString("p_code") %>	  
	  </td>
      <td align="center" valign="middle" nowrap="nowrap">
      	<div style="padding-bottom:10px;">标签打印数&nbsp;&nbsp;<input style="width:25px;" maxlength="3" type="text" onKeyPress="return noNumbers(event)" id="productprintcount_<%=rows[i].get("pc_id",0l) %>" name="productprintcount_<%=rows[i].get("pc_id",0l) %>" value="1"/></div>
      	<input name="button" type="button" class="long-button-print" onClick="printProductLabel(<%=rows[i].get("pc_id",0l)%>,80,35)" value="80X35">
      	&nbsp;&nbsp;&nbsp;&nbsp;
      	<input name="button" type="button" class="long-button-print" onClick="printProductLabel(<%=rows[i].get("pc_id",0l)%>,60,30)" value="60X30">
      </td>
      <td align="center" valign="middle" style="padding-top: 5px;">
      	<div style="padding-bottom:10px;">标签打印数&nbsp;&nbsp;<input style="width:25px;" type="text" maxlength="3" onKeyPress="return noNumbers(event)" id="boxprintcount_<%=rows[i].get("pc_id",0l) %>" name="boxprintcount_<%=rows[i].get("pc_id",0l) %>" value="1"/></div>
      	<div style="padding-bottom: 10px;">箱内商品数&nbsp;&nbsp;<input name="productCount_<%=rows[i].get("pc_id",0l) %>" id="productCount_<%=rows[i].get("pc_id",0l) %>" type="text" value="1" style="width:25px;color:#FF0000" onKeyPress="return noNumbers(event)"/></div>
      	<input type="button" class="long-button-print" value="100X50" onClick="printBoxLabel(<%=rows[i].get("pc_id",0l)%>)">      	
	  </td>
	  </tr>
    <%
	}
	%>
  </form>
</table>
<!--
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
          <input type="hidden" name="p" >
		  <input type="hidden" name="name" value="<%=name%>">
		  <input type="hidden" name="purchase_id" value="<%=purchase_id%>">
		
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
-->
<form target="_blank" method="get" name="print_product_label">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>

<form target="_blank" method="get" name="print_box_label">
	<input type="hidden" id="pc_id" name="pc_id"/>
	<input type="hidden" id="copies" name="copies"/>
	<input type="hidden" id="counts" name="counts"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>
</div>
</body>
</html>
