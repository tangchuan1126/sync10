<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	String purchase_id = StringUtil.getString(request, "purchase_id");
	String expectArrTime = StringUtil.getString(request, "expectArrTime");
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:9999999;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
//采购单详细的上传
function uploadFilePurchaseDetail(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input",targetNode).val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 };
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
	var targetNode = $("#"+target);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			lis += "<li><a href='###'>"+array[index]+"</a></li>"
		}
		var ulNode = $("ul",targetNode);
		ulNode.html(lis);
		$("input",targetNode).val(fileNames);
	 
	}
	if(fileNames && $.trim(fileNames).length > 0)
	{
		loadUplaodFile(fileNames);
	}
}
function loadUplaodFile(file_name)
{
		$(".cssDivschedule").css("display","block");
		showMessage("正在加载采购单详细，请稍候...","alert");
		$.ajax({
			url: 'purchase_detail_show.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
				$(".cssDivschedule").css("display","none");
			},
			
			success: function(html)
			{
				$(".cssDivschedule").css("display","none");
				$("#purchase_details").html(html);
			}
	});
}
function submitPurchaseDetail(){
	$(".cssDivschedule").css("display","block");
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/PurchaseDetailUpdateAction.action',
		data:$("#detailForm").serialize(),
		dataType:'json',
		type:'post',
		success:function(data){
			$(".cssDivschedule").css("display","none");
			if(data && data.flag == "true"){
				showMessage("修改成功","success");
				setTimeout("windowClose()", 1000);
			}else{
				showMessage("修改失败","error");
			}
		},
		error:function(){
			$(".cssDivschedule").css("display","none");
			showMessage("系统错误","error");
		}
	})	
}
function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function windowCloseNotRef(){
	$.artDialog && $.artDialog.close();	
}
</script>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<div class="cssDivschedule" style=""></div>
<form action="" id="detailForm" method="post">
	<input type="hidden" name="expectArrTime" value='<%=expectArrTime %>'/>
	<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
	<table width="100%" height="100%">
		<tr width="100%" height="90%" valign="top">
			<td width="100%" height="100%" valign="top">
	<table width="100%">
		 <tr>
			<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
			采购单号：<%=purchase_id %>
			</td>
		  </tr>
		  <tr height="15px"><td></td></tr>
		  <tr>
			<td width="100%">
				<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;width: 95%">
								<legend style="font-size:15px;font-weight:normal;color:#999999;">上传采购单详细</legend>
					<table width="98%" cellpadding="0" cellspacing="0" height="100%">
					<tr>
						<td>
							<div id="purchase_details"></div>
							<span style="text-align: left;float: left;height: 50px;" class="STYLE3">
								上传EXCEL:
						   		<input type="button"  class="long-button" onclick="uploadFilePurchaseDetail('jquery_file_up_detail');" value="选择文件" />
						   		<span class="STYLE12"><a href="upl_purchase_excel/template.xls">下载模板</a></span>	
							</span><br/>
							<div id="jquery_file_up_detail">	
					             <input type="hidden" name="tempfilename" value=""/>
					        </div>
						</td>
					</tr>
					</table>
			    </fieldset>
			</td>
		</tr>
				</table>
			</td>
		</tr>
		<tr width="100%" height="10%">
		 	<td width="100%" height="10%" valign="bottom">
		 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		 			<tr>
		 				<td align="right" valign="middle" class="win-bottom-line"> 
					    <input type="button" name="Submit2" value="确定" class="normal-green" onClick="submitPurchaseDetail()"/>
						<input type="button" name="Submit2" value="取消" class="normal-white" onClick="windowCloseNotRef();"/>
					  </td>
		 			</tr>
		 		</table>
		 	</td>
		 </tr>
	</table>
</form>
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
 
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>



