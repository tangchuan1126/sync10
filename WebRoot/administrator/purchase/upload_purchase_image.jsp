<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>文件上传</title>
    <style type="text/css">
	<!--
	.create_order_button
	{
		background-attachment: fixed;
		background: url(../imgs/create_order.jpg);
		background-repeat: no-repeat;
		background-position: center center;
		height: 51px;
		width: 129px;
		color: #000000;
		border: 0px;
		
	}
	ul.fileUl{list-style-type:none;margin-left:-47px;}
	ul.fileUl li{float:left;border:1px solid silver;padding-left:3px;margin-top:2px;padding-right:3px;margin-left:5px;line-height:25px;height:25px;}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	span.deleteSpan{cursor:pointer;}
	-->
	</style>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<%
		String flag = StringUtil.getString(request,"flag");
		String fileName = StringUtil.getString(request,"fileName");
		String[] arrayFile = null ;
		if(fileName.trim().length() > 0){
			arrayFile = fileName.split(",");
		}
		String downLoadtempFile = ConfigBean.getStringValue("systenFolder")+"action/administrator/CommonFileDownLoadTempFolderAction.action";
		String downLoadFile =  ConfigBean.getStringValue("systenFolder")+"action/administrator/CommonFileDownLoadAction.action";
	%>
	<script type="text/javascript">
	jQuery(function($){
		if('<%= flag%>' *1  == 2){
			parent.showMessage("文件格式不正确","alert");
		}
		if('<%= flag%>' *1 == 1){
		    parent.showMessage("上传文件出错","error");
		}
		 
	})
	function uploadImage()
	{
		if($('#voucher').val() == ""){
			alert("请输入上传文件!");
		}else {
			$('#uploadImageForm').submit();
		}
	}

	function delImage(id) {
		$('delId').val(id);
		$('#delImageForm').attr('action',$('#delImageForm').attr('action')+'?id='+id);
		$('#delImageForm').submit();
	}
	function deleteFile(_this){
		var node = $(_this);
		node.parent().remove();
		// 删除的时候应该重新的计算backUrl
		$("#backUrl").val("administrator/purchase/upload_purchase_image.html?fileName="+	getFileNames());
 
	}
	function  getFileNames(){
		var  tempAList = $(".temp_a");
		var names = "" ;
		tempAList.each(function(){
			var _this = $(this);
			names += ","+_this.html();
		})
		return names.length > 1 ? names.substr(1):"";
	}
	
	</script>
  </head>
  
  <body>
 
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;width:95%;">
		   <legend style="font-size:15px;font-weight:normal;color:#999999;">凭证信息</legend>
			<form name="uploadImageForm" id="uploadImageForm" value="uploadImageForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/Purchase/UploadImageAction.action" enctype="multipart/form-data" method="post">
				<table width="90%" border="0">
					<tr>
						<input type="hidden" name="backurl" id="backUrl" value="administrator/purchase/upload_purchase_image.html?fileName=<%=fileName %>"/>
						<td height="25" align="right"  class="STYLE2" nowrap="nowrap" style="width:100px;">上传凭证:</td>
				        <td style="width:120px;">
				          <input type="file" id="voucher" name="file" />
					    </td>
						<td align="left" nowrap="nowrap">
							<font style="color: red">*文件格式为："jpg、gif、bmp"</font>
							<input type="button" name="voucherSubmit" id="voucherSubmit" value="上传" onclick="uploadImage()">
						</td>
				  </tr>
				  <tr>
				  		<td height="25" align="right"  class="STYLE2" nowrap="nowrap">
				  			已经上传凭证:
				  		</td>
				  		<td colspan="2">
				  		<% if(arrayFile != null && arrayFile.length > 0){ %>
				  		
				  			<ul class="fileUl">
				  				<%	for(String tempStr : arrayFile){
				  						if(tempStr.trim().length() > 0){
				  					%>
				  					<li>
				  						<a class="temp_a" href='<%= downLoadtempFile%>?file_name=<%=tempStr%>&folder=upl_imags_tmp'><%=tempStr %></a>
				  						&nbsp;&nbsp;<span class="deleteSpan" onclick="deleteFile(this)">删除</span>
				  					</li>
				  				<%} 
				  				}%>
				  			</ul>
				  		<%} %>	  			
				  		</td>
				  </tr>
				</table>
			 </form>
		 </fieldset>
<script type="text/javascript">
function closeWindow(){
	parent.location.reload();
	$.artDialog.close();
}
</script>
  </body>
</html>
