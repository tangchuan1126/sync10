<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ include file="../../include.jsp"%> 
<%
long purchase_id		= 0L;
int refresh 			= 0;
String purchase_id_str	= "";
DBRow purchaseInfo 		= new DBRow();
String tempfilename		= "";
int isOutterUpdate		= 0;
String qualityFileName  = "";
String expectArrTime	= "";
String[] fileSubmit		= new String[5];
boolean isFileSubmit	= ServletFileUpload.isMultipartContent(request);
if(isFileSubmit){
	fileSubmit = purchaseMgr.updatePurchaseTermsAndQuality(request);
}
if(null != fileSubmit[0] && !"".equals(fileSubmit[0])){
	try
	{
		purchase_id = Long.parseLong(fileSubmit[1]);
	}
	catch(NumberFormatException e)
	{
		purchase_id = 0L;
	}
	
	purchase_id_str = fileSubmit[1];
	tempfilename = fileSubmit[2];
	
	try
	{
		isOutterUpdate = Integer.parseInt(fileSubmit[3]);
	}
	catch(NumberFormatException e)
	{
		isOutterUpdate =  0;
	}
	qualityFileName = fileSubmit[0];
	expectArrTime	= fileSubmit[4];
	purchaseInfo	= purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id_str);
}else{
	purchase_id		= StringUtil.getLong(request,"purchase_id");
	refresh			= StringUtil.getInt(request,"refresh");
	purchase_id_str = StringUtil.getString(request, "purchase_id");
	purchaseInfo	= purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id_str);
	tempfilename	= StringUtil.getString(request, "tempfilename");
	isOutterUpdate	= StringUtil.getInt(request, "isOutterUpdate");
	expectArrTime	= StringUtil.getString(request, "expectArrTime");
	
}
String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>采购单条款填写</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
	function previousStep(){
		document.previousSetpForm.submit();
	}
	function purchaseTermsUpdate(){
		//if($("#file_path") && $("#file_path").val().length > 1){
		//	$("#addPurchaseForm").submit();
		//}else{
			$.ajax({
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/AddPurchaseTerms.action',
				data:$("#addPurchaseForm").serialize(),
				dataType:'json',
				type:'post',
				success:function(data){
					if(data && data.flag == "true"){
						showMessage("修改成功","success");
						setTimeout("windowClose()", 1000);
					}else{
						showMessage("修改失败","error");
					}
				},
				error:function(){
					showMessage("系统错误","error");
				}
			})	
		//}
	}
	function windowClose(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	};
	function submitQualityFile(){
		if($("#file_path").val() < 1){
			alert("请先上传文件");
		}else{
			$("#isFileSubmit").val(2);
			$("#qualityFileForm").submit();
		}
	}
	function downLoad(fileName , tableName , folder){
		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>');
	}
	//文件上传
	function uploadFile(_target){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input",targetNode).val();
	    var obj  = {
		     reg:"picture_office",
		     limitSize:2,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		 if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
			 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
			 }});
	}
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
		var targetNode = $("#"+target);
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				lis += "<li><a href='javascript:void(0);'>"+array[index]+"</a></li>"
			}
			var ulNode = $("ol",targetNode);
			ulNode.html(lis);
			$("input",targetNode).val(fileNames);
		}
	    if(fileNames.length > 0 ){
			$("#file_up_tr").attr("style","");
		}else{
			$("#file_up_tr").css('display','none');
		}
	}
</script>
<style type="text/css">
ol.fileUL{list-style-type:none;}
ol.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
</style>
</head>
<body>
<form id="addPurchaseForm" action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/AddPurchaseTerms.action' method="post">
<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_upload_excel.html'/>
<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
<input type="hidden" id="isOutterUpdate" name="isOutterUpdate" value='<%=isOutterUpdate%>'/>
<input type="hidden" name="tempfilename" value='<%=tempfilename %>'/>	
<input type="hidden" name="expectArrTime" value='<%=expectArrTime %>'/>
<input type="hidden" name="quality_inspection" value='<%=purchaseInfo.getString("quality_inspection") %>'/>
<table style="width: 100%">
	<tr>
		<td colspan="2">
			<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
			  <tr>
				<td align="left" style="font-family:'黑体'; font-size:18px;border-bottom:1px solid #999999;color:#000000;">
				填写采购单的各项条款及质检要求</td>
			  </tr>
			 </table>
		</td>
	</tr>
	<tr><td height="30px;"></td></tr>
	<%
		//if(2 != isOutterUpdate){
	%>	
	<tr style=" padding-bottom:15px; padding-left:15px;">
		<td width="15%" align="right" valign="top" nowrap="nowrap" class="STYLE3" >质检要求:</td>
		<td width="85%" align="left">
			<input type="hidden" name="file_with_class_name" id="file_with_className"/>
			<input type="hidden" name="sn" value="P_quality"/>
			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURHCASE_QUALITY_INSPECTION %>" />
			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>" />
			<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
		</td>
		</tr>
		<tr id="file_up_tr" style='display:none;' width="100%">
 		 	<td width="15%" align="right">待上传文件:</td>
 		 	<td width="85%" align="left" style="text-align: left;"><div id="jquery_file_up" style="text-align: left;"><input type="hidden" name="file_names" value=""/><ol class="fileUL"></ol></div></td>
		</tr>
	</tr>
	<%
		int[] fileTypes = {9};
		DBRow[] purchaseQualityFiles = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(purchase_id, fileTypes);
		if(purchaseQualityFiles.length > 0){
	%>
	<tr style=" padding-bottom:15px; padding-left:15px;">
		<td width="15%" align="right" valign="middle" nowrap="nowrap" class="STYLE3">已上传文件:</td>
		<td width="85%" align="left" valign="top" >
			<table>
				<%
					if(purchaseQualityFiles.length > 0){
						for(int f = 0; f < purchaseQualityFiles.length; f ++){
							DBRow purchaseQualityFile = purchaseQualityFiles[f];
				%>
					<tr>
						<td><a href="javascript:downLoad('<%=purchaseQualityFile.getString("file_name") %>')"><%=purchaseQualityFile.getString("file_name") %></a></td>
<%--						<%=purchaseQualityFile.getString("file_name") %></td>--%>
					</tr>
				<%	
						}
					}
				%>
			</table>
		</td>
	</tr>
		<%
			}
		//}
		%>
	<tr height="29">
       <td align="right" width="15%">返修条款:</td>
       <td align="left" valign="middle" width="85%">
       	<textarea rows="2" cols="67" name="repair_terms_purchase" id="repair_terms_purchase"><%=purchaseInfo.getString("repair_terms") %></textarea>
       </td>
     </tr>
     <tr height="29">
       <td align="right" width="15%">贴标条款:</td>
       <td align="left" valign="middle" width="85%">
       	<textarea rows="2" cols="67" name="labeling_terms_purchase" id="labeling_terms_purchase"><%=purchaseInfo.getString("labeling_terms") %></textarea>
       </td>
     </tr>
     <tr height="29">
       <td align="right" width="15%">交货条款:</td>
       <td align="left" valign="middle" width="85%">
       	<textarea rows="2" cols="67" name="delivery_product_terms_purchase" id="delivery_product_terms_purchase"><%=purchaseInfo.getString("delivery_product_terms") %></textarea>
       </td>
     </tr>
     <tr height="29">
       <td align="right" width="15%">其他条款:</td>
       <td align="left" valign="middle" width="85%">
       	<textarea rows="2" cols="67" name="purchase_terms_purchase" id="purchase_terms_purchase"><%=purchaseInfo.getString("purchase_terms") %></textarea>
       </td>
     </tr>
     <tr height="100px;"><td colspan="2"></td></tr>
    <tr>
		<td align="right" valign="bottom" colspan="2">				
			<table width="100%">
				<tr>
					<td colspan="2" align="right" valign="middle" class="win-bottom-line">
						<%
							if(2 == isOutterUpdate){
						%>	
							<input type="button" name="Submit2" value="提交" class="normal-green" onclick="purchaseTermsUpdate()" >
						<%
							}else{
						%>
							<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">				
					 	    <input type="submit" name="Submit2" value="下一步" class="normal-green">
						<%
							}
						%>
					  
					  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="windowClose()">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<form name="previousSetpForm" action='<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_update_basic.html' method="post">
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id %>"/>
	<input type="hidden" name="tempfilename" value='<%=tempfilename %>'/>
	<input type="hidden" name="expectArrTime" value="<%= expectArrTime %>"/>
</form>
<script type="text/javascript">
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
 
	 
  </script>
</body>
</html>