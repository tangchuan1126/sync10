<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	try
	{
	String purchaseid = StringUtil.getString(request,"purchase_id");
	 
	
	long supplier_id = StringUtil.getLong(request,"supplier_id");
	
	String purchase_name = StringUtil.getString(request,"purchase_name");
	
	String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchaseid);
		
	long purchase_id = purchase.get("purchase_id",0l);
	
	DBRow[] searchPurchaseDetail = null;
	if(purchaseid!=null&&!purchaseid.equals("")&&!purchase_name.equals(""))
	{
		searchPurchaseDetail=purchaseMgr.getPurchaseDetailByPurchasename(purchase.get("purchase_id",0l),purchase_name);
	}
	
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	
	DBRow[] delivery_orders = deliveryMgrZJ.getDeliveryOrders(purchase_id);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>批号<%=purchase_id%></title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../../js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="../../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/jquery/ui/ui.tabs.js"></script>



<script type="text/javascript" src="../../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../../js/popmenu/common.js"></script>
<script type="text/javascript" src="../../js/select.js"></script>
<link href="../../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../../comm.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../../js/thickbox/global.css";
@import "../../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../../js/thickbox/1024.css" title="1024 x 768" />
<script src="../../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../../js/thickbox/global.js" type="text/javascript"></script>
<script language="javascript" src="../../../common.js"></script>
<script type="text/javascript" src="../../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../../js/print/m.js"></script>

<script src="../../js/zebra/zebra.js" type="text/javascript"></script>
<script type="text/javascript">
		function print()
		{
		
			//var printer = visionariPrinter.SELECT_PRINTER();
			//if(printer!=-1)
			//{
				visionariPrinter.PRINT_INIT("采购单打印");
				
				visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
				visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../purchase/print.html?purchase_id=<%=purchase_id%>");
			
				visionariPrinter.PREVIEW();
				//visionariPrinter.PRINT();	
			//}
		}
		
		function searchPurchaseDetail()
		{
			if($("#search_key").val().trim()=="")
			{
				alert("请输入要关键字")
			}
			else
			{
				tb_show('采购产品搜索','search_purchase_detail.html?purchase_id='+<%=purchase_id%>+'&purchase_name='+$("#search_key").val()+'&TB_iframe=true&height=500&width=800',false);
			}	
		}
	
	function uploadPurchaseDetail()
	{
		tb_show('上传采购单详细','purchase_upload_excel.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		document.reach_form.submit();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
</script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

</style>

</head>

<body>
<div align="center">
<form action="supplier_purchase_detail.html" name="reach_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form name="del_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/clearPurchaseDetail.action">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>


<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="56%" class="page-title" style="border-bottom:0px;" align="left">
    	<table width="100%" height="36" border="0" cellpadding="3" cellspacing="0">
		<form name="logout_form" method="post"  action="<%=ConfigBean.getStringValue("systenFolder")%>action/admin/supplierLoginOut.action" target="_top">
			<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>login_supplier/supplier.html"/>
		</form>
		
		    <tr>
		      <td width="45%" height="36" background="../imgs/forum_r1_c1.gif" style="padding-left:10px;"><a href="http://www.vvme.com/" target="_blank"><img src="../imgs/system_logo.gif" border="0" /></a>  </td>
		      <td width="55%" align="right" valign="middle" background="../imgs/forum_r1_c1.gif" style="padding-right:10px;">
		      	<span style="font-size:12px;padding-right:12px;"><img src="../imgs/account.gif" alt="当前登录" width="19" height="20" align="absmiddle"/><%=session.getAttribute("supplier_name").toString()%></span><a class="logout" href="javascript:logout();" onFocus="this.blur()">退出</a>
			  </td>
		    </tr>
		  </table>
    </td>
  </tr>
</table>
<table width="95%"  border="0" cellpadding="0" cellspacing="0" style="margin:10px;">
  <tr>
    <td  align="left" valign="top" colspan="2">
		
		 <table width="100%" height="63" border="0" cellpadding="3" cellspacing="6">
            <tr>
              <td width="19%" nowrap="nowrap" style="padding:5px;">
			  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			  <font style="font-family: 黑体; font-size: 16px;">批&nbsp;&nbsp;号：<%=purchase_id%></font><br/>
			  <font style="font-family: 黑体; font-size: 15px;">转款情况：<%=purchasemoneykey.getQuoteStatusById(purchase.getString("money_status"))%>
			  <br/>到货情况：<%=purchasearrivekey.getQuoteStatusById(purchase.getString("arrival_time"))%></font>			  </div>			  </td>
              <td width="43%" align="left" valign="top" style="padding:5px;">
			  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px; height:100%;">
              	<font style="font-family: 黑体; font-size: 16px;">补充条款</font>
              	  <%
			 	  	if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
			 	  	{
			 	  %>
			 	  	<input  type="button" class="long-button-mod" value="合同条款" onclick="modPurchaseTerms()"/>&nbsp;&nbsp;
			 	  <%
			 	  	}
			 	  %><br/>
              	<hr/>
              	<%
              		if(!purchase.getString("purchase_terms").equals(""))
              		{
              	%>
              		<div align="left">
              		
              		&nbsp;&nbsp;&nbsp;&nbsp;<%=purchase.getString("purchase_terms")%>              		</div>
              	<%
              		}
              	%>
          		</div>              </td>
              <td width="38%" align="right" valign="middle" nowrap="nowrap">
			  <table cellpadding="0" cellspacing="0" border="0">
			  	<tr>
					<td nowrap="nowrap" align="right"><font style="font-size:12px">商品名称：</font></td>
					<td nowrap="nowrap" align="left"><input type="text" name="search_key" id="search_key" value="<%=purchase_name %>" class="input-line" onkeydown="if(event.keyCode==13)searchPurchaseDetail()"/>&nbsp;&nbsp;<input name="Submit" type="button" class="button_long_search" value="查询" onclick="searchPurchaseDetail()"/></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td nowrap="nowrap" align="right"><font style="font-size:12px">交货记录：</font></td>
					<td nowrap="nowrap" align="left"><select name="deliveryOrder_id" id="deliveryOrder_id" style="width:204px;">
				<option value="0">请选择交货单...</option>
				<%
					for(int i =0;i<delivery_orders.length;i++)
					{
				%>
					<option value="<%=delivery_orders[i].get("delivery_order_id",0l)%>"><%=delivery_orders[i].getString("delivery_order_number")%></option>
				<%
					}
				%>
			</select>&nbsp;&nbsp;<input type="button" class="button_long_search" onclick="delveryOrderDetail()" value="查看"/></td>
				</tr>
			  </table>
			  </td>
            </tr>
			<tr>
				<td colspan="2">
					<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
						<font style="font-family: 黑体; font-size: 14px;">交货地点：</font><%=purchase.getString("delivery_address") %><br/>
						<font style="font-family: 黑体; font-size: 14px;">交货联系人：</font><%=purchase.getString("delivery_linkman")%><br/>
						<font style="font-family: 黑体; font-size: 14px;">联系电话：</font><%=purchase.getString("linkman_phone")%><br/>
					</div>				</td>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td align="right">
								<%
									if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
									{
								  %>
											  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.savePurchaseDetail">
												<input name="button"  type="button" class="long-button-upload" onclick="uploadPurchaseDetail()" value="上传"/>&nbsp;&nbsp;
											  </tst:authentication>
											  <% 
									}
								  %>
											  <%
									if(!purchase.getString("purchasedetail").equals(""))
									{
								  %>
											  <input name="button"  type="button" class="long-button-next" onclick="window.location.href='upl_purchase_excel/<%=purchase.getString("purchasedetail") %>'" value="下载"/>
								   <%
									}
								   %>
								    <%
									if(purchase.get("purchase_status",0)!=PurchaseKey.CANCEL&&purchase.get("purchase_status",0)!=PurchaseKey.OPEN&&purchase.get("purchase_status",0)!=PurchaseKey.FINISH)
									{
								   %>
										&nbsp;&nbsp;<input name="button" type="button" class="long-button" onclick="addDelveryOrder()" value="交货"/>
										  <%
									}
								   %>
							</td>
						</tr>
						<tr>
							<td align="right" style="padding-top:20px;">
								   <%
									if(purchase.get("purchase_status",0)==PurchaseKey.AFFIRMTRANSFER||purchase.get("purchase_status",0)==PurchaseKey.DELIVERYING)
									{
								   %>
										<input type="button" class="long-button-print" value="打印" onclick="print()"/>&nbsp;&nbsp;
								   <%
									}
								   %>
								   <input type="button" class="long-button-print" value="打印标签" onclick="printLabel()"/>
							</td>
						</tr>
					</table>
       </td>
			</tr>
      </table>
	</td>
  </tr>
 <tr>
 	<td align="left">&nbsp;</td>
	  <td align="right">
    </td>
 </tr>
  <tr>
    <td  align="left" valign="top" colspan="2"><br />

<div class="demo">

<div id="tabs">
	<ul>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&purchase_status=<%=purchase.get("purchase_status",0)%>&type=all&p=<%=StringUtil.getInt(request,"p")%>">全部商品<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=reapzero&p=<%=StringUtil.getInt(request,"p")%>">未到货<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=reappart&p=<%=StringUtil.getInt(request,"p")%>">部分到货<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=reapall&p=<%=StringUtil.getInt(request,"p")%>">全部到货<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=differencesreap&p=<%=StringUtil.getInt(request,"p")%>">到货差异<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=goup&p=<%=StringUtil.getInt(request,"p")%>">涨价商品<span> </span></a></li>
		<li><a href="supplier_purchase_detail_tright.html?purchase_id=<%=purchase_id %>&type=godown&p=<%=StringUtil.getInt(request,"p")%>">降价商品<span> </span></a></li>
	</ul>
</div>
</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>	</td>
  </tr>
</table>
<table width="95%">
	<tr>
	  <td align="left" style="padding-left:25px;">
	  	<%
	  		if(purchase.get("purchase_status",0) == PurchaseKey.OPEN)
	  		{
	  	%>
	  	<input type="button" value="通知采购" class="normal-white" onclick="notice()"/>
	  	<% 
	  		}
	  	%>	  	
	  </td>
   	  <td align="right">&nbsp;</td>
	</tr>
</table>

<form action="supplier_purchase_detail.html" name="search_form" method="get">
	<input type="hidden" name="purchase_name" id="purchase_name"/>
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id %>"/>
</form>
<form name="dataForm" action="supplier_purchase_detail.html" method="post">
    <input type="hidden" name="p"/>
    <input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
    <input type="hidden" name="type" value="<%=type %>"/>
</form>
  <form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
</form>
<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/notice.action" method="post" name="notice_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_order_detail.html" name="add_dleveryOrder_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="supplier_id" value="<%=supplier_id%>"/>
</form>

<form action="" name="deliveryOrder_detali_form">
	<input type="hidden" name="delivery_order_id" id="delivery_order_id"/>
</form>

<form action="supplier_print_label.html" name="print_label_form" target="_blank" method="post"">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>
  <script type="text/javascript">
  function applicationApprove()
  {
  	if(confirm("确定申请此采购单完成"))
  	{
  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
  		document.application_approve.submit();
  	}
  }
  
  function promptCheck(v,m,f)
			{
				if (v=="y")
				{
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 
					 return true;
				}
			}
			
			function notice()
			{
				document.notice_form.submit();
			}
			
			function modPurchaseTerms()
			{
				tb_show('修改合同条款','purchase_terms.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
			}
			
			function addDelveryOrder()
			{
				document.add_dleveryOrder_form.submit();
			}
			
			function delveryOrderDetail()
			{
				if($("#deliveryOrder_id").getSelectedValue()>0)
				{
					document.deliveryOrder_detali_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_order_detail.html";
					document.deliveryOrder_detali_form.delivery_order_id.value = $("#deliveryOrder_id").getSelectedValue();
					document.deliveryOrder_detali_form.submit();
				}
				else
				{
					alert("选择一个交货单后才能查看")
				}
				
			}
			
			function printLabel()
			{
				document.print_label_form.submit();
			}
  </script>
  </div>
</body>
</html>
<%} 
	catch(Exception e)
	{
		out.print("<script>alert('系统异常，请您重新登录。带来不便，非常抱歉')</script>");
		response.sendRedirect("http://www.1you.com/");
	}
%>
