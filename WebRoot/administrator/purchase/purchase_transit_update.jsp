<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.CurrencyKey"%>
<%@page import="java.util.List"%>
<%@ include file="../../include.jsp"%>
<%
	String purchase_id = StringUtil.getString(request,"purchase_id").equals("")?"0":StringUtil.getString(request,"purchase_id");
	DBRow row = purchaseMgrLL.getPurchaseById(purchase_id);
	
	String delivery_address = row.getValue("delivery_address")==null?"":row.getValue("delivery_address").toString();
	String delivery_linkman = row.getValue("delivery_linkman")==null?"":row.getValue("delivery_linkman").toString();
	String linkman_phone = row.getValue("linkman_phone")==null?"":row.getValue("linkman_phone").toString();
	String purchase_terms = row.getValue("purchase_terms")==null?"":row.getValue("purchase_terms").toString();
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	DBRow[] rows = purchaseMgr.getPurchaseDetailByPurchaseid(Long.parseLong(purchase_id),pc,null);
	DBRow purchaseDetailOne = purchaseMgr.getPurchaseDetailOneByPurchaseId(purchase_id);
	String eta = "";
	if(null != purchaseDetailOne){
		eta = purchaseDetailOne.getString("eta");
	}
	DBRow[] countrycode		= orderMgr.getAllCountryCode();
	DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
	DBRow[] productline =  productLineMgrTJH.getAllProductLine();//供应商所在的产品线
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>采购单基本信息</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.set {
    border: 2px solid #999999;
    font-weight: bold;
    line-height: 18px;
    margin-bottom: 10px;
    margin-top: 5px;
    padding: 2px;
    width: 90%;
    word-break: break-all;
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>

	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){	
		$("#delivery_address").val('<%=delivery_address%>');
		$("#delivery_linkman").val('<%=delivery_linkman%>');
		$("#linkman_phone").val('<%=linkman_phone%>');

		init();
	});
	$(function(){
		$.ajax({
				url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+<%=row.get("deliver_native",0)%>,
				dataType:'json',
				type:'post',
				success:function(data){
					if("" != data){
						var provinces = eval(data);
						$("#pro_id").attr("disabled",false);
						if(0 == <%=row.get("deliver_provice",0)%> || -1 == <%=row.get("deliver_provice",0)%>){
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择</option>");
							for(var i = 0; i < data.length; i ++){
								if(<%=row.get("deliver_provice",0)%> == data[i].pro_id){
									$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
								}else{
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
							}
							$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
							if(-1 == $("#pro_id").val()){
								$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value='<%=row.getString("deliver_pro_input") %>'/>");
							}
						}else{
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择</option>");
							for(var i = 0; i < data.length; i ++){
									if(<%=row.get("deliver_provice",0)%> == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
							}
							$("#pro_id").append("<option value=-1>手动输入</option>");
						}
					}else{
						$("#pro_id").attr("disabled",false);
						$("#pro_id").append("<option value=0>请选择</option>");
						$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
						if(-1 == $("#pro_id").val()){
						$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value='<%=row.getString("deliver_pro_input") %>'/>");
						}
					}
				},
				error:function(){
				}
			})	
			$("#pro_id").change();
			initSupplierByProductline('<%=row.getString("product_line_id")%>','<%=row.getString("supplier") %>');
			$("#supplier_native").change();
			$("#supplier_province").change();
	});
	function removeAfterProIdInput(){$("#address_state_input").remove();}
	function setPro_id(deliver_proId, deliver_proInput){
		removeAfterProIdInput();
		var node = $("#ccid_hidden");
		var value = node.val();
	 	$("#pro_id").empty();
	 	$.ajax({
					url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
					dataType:'json',
					type:'post',
					success:function(data){
						if("" != data){
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择</option>");
							if(deliver_proId == -1){
								$("#pro_id").attr("disabled",false);
								for(var i = 0; i < data.length; i ++){
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
								$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
								$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value="+deliver_proInput+"/>");
							}else{
								for(var i = 0; i < data.length; i ++){
									if(deliver_proId == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
								}
							$("#pro_id").append("<option value=-1>手动输入</option>");
							}
						}else{
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0 selected='selected'>请选择</option>");
							$("#pro_id").append("<option value=-1>手工输入</option>");
							if(-1 == $("#pro_id").val()){
							$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input'/>");
							}
						}
					},
					error:function(){
					}
				})	
	 }
	function handleProInput(){
	 	var value = $("#pro_id").val();
	 	removeAfterProIdInput();
	 	if(-1 == value){
	 		$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input'/>");
	 	}else{
	 		removeAfterProIdInput();
	 	}
	 };
	 function removeAfterProIdInputSupplier(){$("#supplier_pro_input").remove();}
	 function setPro_idSupplier(getCcid,getProId,getProInput){
			removeAfterProIdInputSupplier();
			var node = $("#supplier_native");
			var value = node.val();
			if(getCcid){
				value = getCcid;
			}
		 	$("#supplier_province").empty();
		 	$.ajax({
						url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
						dataType:'json',
						type:'post',
						success:function(data){
							if("" != data){
								$("#supplier_province").append("<option value=0>请选择</option>");
								var provinces = eval(data);
								$("#supplier_province").attr("disabled",false);
								if(data.length > 0)
								{
									data[0].pro_name;
								}
								for(var i = 0; i < data.length; i ++){
									$("#supplier_province").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
								$("#supplier_province").append("<option value=-1>手动输入</option>");
								if(0 != '<%=row.get("supplier_province",0)%>'){
									$("#supplier_province option[value="+'<%=row.get("supplier_province",0)%>'+"]").attr("selected",true);
								}
								if(getProId){
									$("#supplier_province option[value="+getProId+"]").attr("selected", true);
								}
								var proInputGet = '<%=row.getString("supplier_pro_input") %>';
								if(getProInput){
									proInputGet = getProInput;
								}
								if(-1 == $("#supplier_province").val()){
									$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputGet+"'/>");
								}
							}else{
								$("#supplier_province").attr("disabled",false);
								$("#supplier_province").append("<option value=0 selected='selected'>请选择</option>");
								$("#supplier_province").append("<option value=-1>手工输入</option>");
								var proInputGet = '<%=row.getString("supplier_pro_input") %>';
								if(getProInput){
									proInputGet = getProInput;
								}
								if(-1 == $("#supplier_province").val()){
									$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputGet+"'/>");
								}
							}
						},
						error:function(){
						}
					})	
		 }
		function handleProInputSupplier(supplierProInput){
		 	var value = $("#supplier_province").val();
		 	removeAfterProIdInputSupplier();
		 	if(-1 == value){
		 		var proInputSupplier = "";
			 	if(supplierProInput){
			 		proInputSupplier = supplierProInput;
			 	}
		 		$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputSupplier+"'/>");
		 	}else{
		 		removeAfterProIdInputSupplier();
		 	}
		 };
	 function selectStorage(){
			var para = "ps_id="+$("#ps_id").val();
			$("#deliver_house_number").val("");
			$("#deliver_street").val("");
			$("#deliver_zip_code").val("");
			$("#deliver_city").val("");
			$("#ccid_hidden").val("");
			$("#ccid_hidden").change();
			$("#delivery_linkman").val("");
			$("#linkman_phone").val("");
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/GetStorageCatalogDetailDeliver.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					error: function(e){
						alert(e);
						alert("请求失败")
					},
					success: function(data){
						$("#deliver_house_number").val(data.deliver_house_number);
						$("#deliver_street").val(data.deliver_street);
						$("#deliver_zip_code").val(data.deliver_zip_code);
						$("#deliver_city").val(data.deliver_city);
						$("#delivery_linkman").val(data.deliver_contact);
						$("#linkman_phone").val(data.deliver_phone);
						var ccid_selected = "#ccid_hidden option[value="+data.deliver_nation+"]";
						$(ccid_selected).attr("selected", "selected");
						setPro_id(data.deliver_pro_id,data.deliver_pro_input);
					}
				});
		}
	 function initSupplierByProductline(productlineid,supplierId){
			var para = "productline_id="+productlineid;
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					error: function(e){
						alert("数据读取出错！");
					},
					success: function(data){
						$("#supplier").clearAll();
						$("#supplier").addOption("请选择...","");
						if(data != ""){
							$.each(data,function(i){
								$("#supplier").addOption(data[i].sup_name,data[i].id);
							});
							$("#supplier option[value="+supplierId+"]").attr("selected",true);
						}
					}
				});
		}
	 function getSupplierByProductline(productlineid){
			$("#supplier_house_number").val("");
			$("#supplier_street").val("");
			$("#supplier_city").val("");
			$("#supplier_zip_code").val("");
			$("#supplier_native").val("");
			$("#supplier_link_man").val("");
			$("#supplier_link_phone").val("");
			removeAfterProIdInputSupplier();
			setPro_idSupplier();
			handleProInputSupplier();
			var para = "productline_id="+productlineid;
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					beforeSend:function(request){
					},
					error: function(e){
						alert("数据读取出错！");
					},
					success: function(data){
						$("#supplier").clearAll();
						$("#supplier").addOption("请选择...","");
						if(data != ""){
							$.each(data,function(i){
								$("#supplier").addOption(data[i].sup_name,data[i].id);
							});
						}
					}
				});
		}
	 function changeSupplierAddress(){
			$("#supplier_house_number").val("");
			$("#supplier_street").val("");
			$("#supplier_city").val("");
			$("#supplier_zip_code").val("");
			$("#supplier_native").val("");
			$("#supplier_link_man").val("");
			$("#supplier_link_phone").val("");
			removeAfterProIdInputSupplier();
			setPro_idSupplier();
			handleProInputSupplier();
				var supplierId = $("#supplier").val();
				$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetSupplierInfoAjax.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:"supplier_id="+supplierId,
					error: function(e){
						alert("数据读取出错！");
					},
					success: function(data){
						if(null != data){
							$("#supplier_house_number").val(data.address_houme_number);
							$("#supplier_street").val(data.address_street);
							$("#supplier_city").val(data.city_input);
							$("#supplier_zip_code").val(data.zip_code);
							$("#supplier_native option[value="+data.nation_id+"]").attr("selected",true);
							$("#supplier_link_man").val(data.linkman);
							$("#supplier_link_phone").val(data.phone);
							removeAfterProIdInputSupplier();
							setPro_idSupplier(data.nation_id,data.province_id);
							handleProInputSupplier(data.supplier_pro_input);
						}
					}
				});
			}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/purchaseTransitUpdateAction.action">
<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_transit_update.html?purchase_id=<%=purchase_id%>&updated=1"/>
<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
	  <fieldset class="set" style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">采购单基本信息信息</legend>
			<table width="100%" border="0" cellspacing="5" cellpadding="0" align="left">
		        <tr height="29">
		          <td align="right" class="STYLE2" width="15%">采购单号:</td>
		          <td align="left" valign="middle" colspan="4" width="85%">
		          	<%=purchase_id%>
		          </td>
		        </tr>
		        <tr height="29">
		          <td align="right" class="STYLE2" width="15%">预计到货时间:</td>
		          <td align="left" valign="middle" colspan="4" width="85%">
		          	<input name="eta" type="text" id="eta"  value="<%=eta%>"  style="border:1px #CCCCCC solid;width:85px;" />
		          </td>
		        </tr>
			</table>
		</fieldset>	
		<fieldset class="set" style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">提货地址</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
				<tr>
					<td align="right" class="STYLE2">产品线:</td>
					<td>&nbsp;</td>
		         	<td align="left" valign="middle" colspan="4">
				      	<select style="width:150px" name="product_line_id" id="product_line_id" onChange="getSupplierByProductline(this.value)">
				      	<option value="0">请选择...</option>
				      	<%
				      		for(int i=0;i<productline.length;i++){
				      			if(row.getString("product_line_id").equals(productline[i].getString("id"))){
				      			%>
						      		<option value="<%=productline[i].getString("id") %>" selected="selected"><%=productline[i].getString("name") %></option>
						      	<%
				      			}else{
						      	%>
						      		<option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
						      	<%
				      			}
				      		}
				      	%>
				      </select>
			 		</td>
				</tr>
				<tr valign="bottom">
					<td align="right" class="STYLE2">供应商:</td>
				    <td>&nbsp;</td>
		         	<td align="left" valign="middle" colspan="4">
				      	<select name='supplier' id='supplier' onchange="changeSupplierAddress();">
				      		<option value="0">请选择供应商...</option>
				      	</select>
				      </td>
				</tr>
	        <tr height="29">
		          <td align="right" class="STYLE2">门牌号:</td>
	          <td>&nbsp;</td>
		          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="supplier_house_number" id="supplier_house_number" value='<%=row.getString("supplier_house_number") %>' style="width:300">
	          </td>
		        </tr>
		          <tr height="29">
		          <td align="right" class="STYLE2">街道:</td>
	          <td>&nbsp;</td>
		          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="supplier_street" id="supplier_street" style="width:300" value='<%=row.getString("supplier_street") %>'>
	          </td>
	        </tr>
	        <tr height="29">
		          <td align="right" class="STYLE2">城市:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="supplier_city" id="supplier_city" style="width:300" value='<%=row.getString("supplier_city") %>'>
		          </td>
		        </tr>
		        <tr height="29">
		          <td align="right" class="STYLE2">邮编:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="supplier_zip_code" id="supplier_zip_code" style="width:300" value='<%= row.getString("supplier_zip_code") %>'>
		          </td>
		        </tr>
		        <tr>
					<td align="right" class="STYLE2">省份</td>
	          <td>&nbsp;</td>
					<td align="left" valign="middle" colspan="4"> 
						<span id="addBillProSpanSupplier">
							<select disabled="disabled" name="supplier_province" id="supplier_province" onchange="handleProInputSupplier()" style="margin-right:5px;"></select>
						</span>
					</td>
				 </tr>
				 <tr>
				    <td align="right" class="STYLE2">国家</td>
				    <td>&nbsp;</td>
				    <td align="left" valign="middle" colspan="4">
					    <%
							String selectBgSupplier="#ffffff";
							String preLetterSupplier="10990";
						%>
				      <select  id="supplier_native"  name="supplier_native" onChange="removeAfterProIdInputSupplier();setPro_idSupplier();">
					 	 	<option value="0">请选择...</option>
							  <%
							  for (int i=0; i<countrycode.length; i++){
							  	if (!preLetterSupplier.equals(countrycode[i].getString("c_country").substring(0,1))){
									if (selectBgSupplier.equals("#eeeeee")){
										selectBgSupplier = "#ffffff";
									}else{
										selectBgSupplier = "#eeeeee";
									}
								}  	
							  	preLetterSupplier = countrycode[i].getString("c_country").substring(0,1);
								if(countrycode[i].getString("ccid").equals(row.get("supplier_native",0)+"")){
							%>	
								 <option style="background:<%=selectBgSupplier%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
							<%	
								}else{
							  %>
					  		  <option style="background:<%=selectBgSupplier%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
							  <%
								}
							  }
							%>
				   		 </select>
	          </td>
			 	</tr>
			 	<tr height="29">
		          <td align="right" class="STYLE2">联系人:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
		          	<input type="text" name="supplier_link_man" id="supplier_link_man" value='<%=row.getString("supplier_link_man") %>'>
	          </td>
	        </tr>
	        <tr height="29">
		          <td align="right" class="STYLE2">联系电话:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
		          	<input type="text" name="supplier_link_phone" id="supplier_link_phone" value='<%=row.getString("supplier_link_phone") %>'>
	          </td>
		        </tr>
			</table>
		</fieldset>
		<fieldset class="set" style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">收货地址</legend>
			<table width="100%" border="0" cellspacing="5" cellpadding="0">
				<tr>
				   <td align="right" class="STYLE2">收货仓库：</td>
	          <td>&nbsp;</td>
			       <td align="left" valign="middle" colspan="4">
				      <select name='ps_id' id='ps_id' onChange="selectStorage()">
                              <option value="0">请选择...</option>
                              <%
							  for ( int i=0; i<treeRows.length; i++ )
							  {
								  if(treeRows[i].getString("id").equals(row.get("ps_id",0)+"")){
							  %>
                              <option value='<%=treeRows[i].getString("id")%>' selected="selected"><%=treeRows[i].getString("title")%></option>
                              <%	  
								  }else{
						      %>
                              <option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
                              <%
								  }
								}
							%>
                     </select>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">门牌号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="deliver_house_number" id="deliver_house_number" value='<%=row.getString("deliver_house_number") %>' style="width:300">
	          </td>
	        </tr>
	          <tr height="29">
	          <td align="right" class="STYLE2">街道:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="deliver_street" id="deliver_street" style="width:300" value='<%=row.getString("deliver_street") %>'>
	          </td>
	        </tr>
	          <tr height="29">
	          <td align="right" class="STYLE2">城市:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="deliver_city" id="deliver_city" style="width:300" value='<%=row.getString("deliver_city") %>'>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">邮编:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
		          	<input type="text" name="deliver_zip_code" id="deliver_zip_code" style="width:300" value='<%= row.getString("deliver_zip_code") %>'>
	          </td>
	        </tr>
	        <tr>
				<td align="right" class="STYLE2">省份</td>
					<td>&nbsp;</td>
				<td align="left" valign="middle" colspan="4"> 
						<span id="addBillProSpan" style="width:100%;">
							<select disabled="disabled" name="deliver_provice" id="pro_id" onchange="handleProInput()" style="margin-right:5px;"></select>
					</span>
				</td>
			 </tr>
			 <tr>
			    <td align="right" class="STYLE2">国家</td>
				    <td>&nbsp;</td>
				    <td align="left" valign="middle" colspan="4">
				    <%
						String selectBg="#ffffff";
						String preLetter="10990";
					%>
				      	<select  id="ccid_hidden"  name="deliver_native" onChange="removeAfterProIdInput();setPro_id();">
				 	 	<option value="0">请选择...</option>
						  <%
						  for (int i=0; i<countrycode.length; i++){
						  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
								if (selectBg.equals("#eeeeee")){
									selectBg = "#ffffff";
								}else{
									selectBg = "#eeeeee";
								}
							}  	
							preLetter = countrycode[i].getString("c_country").substring(0,1);
							if(countrycode[i].getString("ccid").equals(row.get("deliver_native",0)+"")){
						%>	
							 <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>" selected="selected"><%=countrycode[i].getString("c_country")%></option>
						<%	
							}else{
						  %>
				  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
						  <%
							}
						  }
						%>
			   		 </select>
	   			</td>
		 	</tr>
			 	<tr height="29">
		          <td align="right" class="STYLE2">联系人:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="delivery_linkman" id="delivery_linkman">
		          </td>
		        </tr>
		        <tr height="29">
		          <td align="right" class="STYLE2">联系电话:</td>
		          <td>&nbsp;</td>
		          <td align="left" valign="middle">
		          	<input type="text" name="linkman_phone" id="linkman_phone">
		          </td>
		        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitApply()" value="提交" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply()
	{
			document.apply_money_form.submit();
	}
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
	$("#eta").date_input();
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

