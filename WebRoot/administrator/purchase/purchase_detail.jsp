<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.ApplyTypeKey"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cwc.app.key.PurchaseLogTypeKey"%>
<%@page import="com.cwc.app.key.AccountKey"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="deliveryOrderKey" class="com.cwc.app.key.TransportOrderKey"/>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	String purchase_id = "";
	String[] fileSubmit		= new String[5];
	boolean isFileSubmit	= ServletFileUpload.isMultipartContent(request);
	if(isFileSubmit){
		fileSubmit = purchaseMgr.updatePurchaseTermsAndQuality(request);
	}else{
		purchase_id = StringUtil.getString(request, "purchase_id");
	}
	if(null != fileSubmit[0] && !"".equals(fileSubmit[0])){
		purchase_id = fileSubmit[1];
	}
	//String purchase_id = StringUtil.getString(request,"purchase_id");
	//String purchase_name = StringUtil.getString(request,"purchase_name");
	String lastEtaTime = purchaseMgrIfaceZr.getLastPurchaseDetailEta(Long.parseLong(purchase_id));
	//String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	
	DBRow searchPurchaseDetail = null;
	if(purchase_id!=null&&!purchase_id.equals(""))
	{
		searchPurchaseDetail=purchaseMgr.getPurchaseDetailOneByPurchaseId(purchase_id);
	}
	String expectArrTime = "";
	if(null != searchPurchaseDetail){
		expectArrTime = searchPurchaseDetail.getString("eta");
	}
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	DBRow supplier;
	long sid;
	try
	{
		sid = Long.parseLong(purchase.getString("supplier"));
	}
	catch(NumberFormatException e)
	{
		sid = 0;
	}
	if(sid ==0)
	{
		supplier = new DBRow();
		String name = purchase.getString("supplier");
		supplier.add("sup_name",name);
	}
	else
	{
		supplier = supplierMgrTJH.getDetailSupplier(sid);
	}
	HashMap followuptype = new HashMap();
	followuptype.put(4,"发票跟进");
	followuptype.put(5,"退税跟进");
	
	boolean edit = false;
	
	if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
	{
		edit = true;
	}
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
	//读取配置文件中的purchase_tag_types
		String tagType = systemConfig.getStringConfigValue("purchase_tag_types");
		String[] arraySelected = tagType.split("\n");
		//将arraySelected组成一个List
		ArrayList<String> selectedList= new ArrayList<String>();
		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
				String tempStr = arraySelected[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
		int[] productFileTyps = {FileWithTypeKey.PRODUCT_TAG_FILE};
		DBRow[] imagesRows = purchaseMgrZyj.getAllProductTagFilesByPcId(Long.parseLong(purchase_id),productFileTyps );
	if(imagesRows != null && imagesRows.length > 0 ){
		for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
			DBRow tempRow = imagesRows[index];
			String product_file_type = tempRow.getString("product_file_type");
			List<DBRow> tempListRow = imageMap.get(product_file_type);
			if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
				tempListRow = new ArrayList<DBRow>();
			}
			tempListRow.add(tempRow);
			imageMap.put(product_file_type,tempListRow);
		}
	}
	//读取配置文件中的transport_product_file
		String valueProduct = systemConfig.getStringConfigValue("transport_product_file");
		String flagProduct = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
		String[] arraySelectedProduct = valueProduct.split("\n");
		//将arraySelectedProduct组成一个List
		ArrayList<String> selectedListProduct= new ArrayList<String>();
		for(int index = 0 , count = arraySelectedProduct.length ; index < count ; index++ ){
				String tempStr = arraySelectedProduct[index];
				if(tempStr.indexOf("=") != -1){
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedListProduct.add(tempHtml);
				}
		}
		// 获取所有的关联图片 然后在页面上分类
		Map<String,List<DBRow>> imageMapProduct = new HashMap<String,List<DBRow>>();
		int[] productFileTypes = {FileWithTypeKey.PURCHASE_PRODUCT_FILE};
		DBRow[] imagesRowsProduct = purchaseMgrZyj.getAllProductTagFilesByPcId(Long.parseLong(purchase_id),productFileTypes );
		if(imagesRowsProduct.length > 0 ){
			for(int index = 0 , count = imagesRowsProduct.length ; index < count ; index++ ){
				DBRow tempRow = imagesRowsProduct[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMapProduct.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMapProduct.put(product_file_type,tempListRow);
			}
		}
		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
		//查询预申请表
		DBRow preparePurchaseFunds=preparePurchaseMgrZwb.getPreparePurchaseFunds(Long.parseLong(purchase_id));
		TDate tDate = new TDate();
		DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
		df.applyPattern("0.0");
		int[] qualityfileTypes = {FileWithTypeKey.PURHCASE_QUALITY_INSPECTION};
		DBRow[] purchaseQualityFiles = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.parseLong(purchase_id), qualityfileTypes);
		int isExistQualityFile = purchaseQualityFiles.length > 0?1:2;
		DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,purchase.get("purchase_id",0L),FinanceApplyTypeKey.PURCHASE_ORDER);
		int applyMoneyLen   = applyMoneys.length;
		DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(purchase_id));
		int deliveryOrderLen	= deliveryOrderRows.length;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>采购单详细 </title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
#ui-datepicker-div{z-index:9999;display:none;}
.ui-jqgrid tr.jqgrow td{  white-space: normal !important;}
-->
</style>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style type="text/css">
	body{text-align:left;}
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>
 
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<!-- jqgrid 引用 -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet"/>
<!-- 在线阅读 -->
<script type="text/javascript" src="../js/office_file_online/officeFileOnline.js"></script>

<script type="text/javascript">	
	function searchPurchaseDetail()
	{
		if($("#search_key").val().trim()=="")
		{
			alert("请输入要关键字")
		}
		else
		{			
			tb_show('采购产品搜索','search_purchase_detail.html?purchase_id='+<%=purchase_id%>+'&purchase_name='+$("#search_key").val()+'&TB_iframe=true&height=500&width=800',false);
		}	
	}
	
	function getPurchaseSumVWP(purchase_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getPurchaseSumVWPJson.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{purchase_id:purchase_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date)
					{
						$("#sum_volume").html("总体积:"+date.all_volume+" cm³");
						$("#sum_weight").html("总重量:"+date.all_weight+" Kg");
						$("#sum_price").html("总货款:"+date.all_price+" RMB");
					}
				});
	}
	
	function updatePurchaseVW(purchase_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/updatePurchaseDetailVW.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{purchase_id:purchase_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date)
					{
						if(date.result=="ok")
						{
							window.location.reload();
						}
					}
				});
	}
	
	
	function downloadPurchase(purchase_id)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/downloadPurchase.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:{purchase_id:purchase_id},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							if(date.result.length>0)
							{
								alert(date.result);
							}
							else
							{
								alert("无法下载！");
							}
							
						}
					}
				});
	}
	
	function uploadPurchaseDetail()
	{
		$.artDialog.open('purchase_upload_excel.html?purchase_id='+<%=purchase_id%>,{title:'上传采购单详细', height:500, width:830, lock:true,opacity:0.3,fixed:true});
		//tb_show('上传采购单详细','purchase_upload_excel.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		document.reach_form.submit();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function addDeliveryTransportOrder(purchase_id)
	{


		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder") %>action/administrator/purchase/CheckPurchaseTransportApplyPromptAction.action',
			data:'purchase_id='+purchase_id+"&deposit_or_delivery=2",
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.result)
				{
					alert(data.result);
				}
				else
				{
					var url = "../transport/add_purchase_transport.html?purchase_id="+purchase_id;
					$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3,fixed: true});
				}
			},
			error:function(){
				showMessage("系统错误","error");
			}
		});
		
	}
	
	function uploadIncominglog()
	{
		tb_show('上传入库日志','incoming_log.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function purchaseLogs(purchase_id)
	{
		tb_show('日志','followuplogs.html?purchase_id='+purchase_id+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function modPurchaseDelivery()
	{
		tb_show('修改交货信息','mod_purchase_delivery.html?purchase_id='+<%=purchase.get("purchase_id",0l)%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function print()
	{
	
		//var printer = visionariPrinter.SELECT_PRINTER();
		//if(printer!=-1)
		//{
			visionariPrinter.PRINT_INIT("采购单打印");
		
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../purchase/print.html?purchase_id=<%=purchase_id%>");
		
			visionariPrinter.PREVIEW();
			//visionariPrinter.PRINT();	
		//}
			
	}
	function showTransit(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_transit_update.html?purchase_id=<%=purchase_id%>&isOutter=2";
		$.artDialog.open(uri, {title: '采购单信息',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	function applyMoney() {
		apply_form.submit();
	}
	function refreshWindow(){
		window.location.reload();
	}
	function updatePurchaseTerms(){
		if('0' != '<%=applyMoneyLen%>')
		{
			alert("已经申请定金，不能修改条款信息");	
		}
		else if('0' != '<%=deliveryOrderLen%>')
		{
			alert("已经创建转运单，不能修改条款信息");
		}
		else
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_add_terms.html?isOutterUpdate=2&purchase_id=<%=purchase_id%>';
			$.artDialog.open(uri, {title: '修改条款信息',width:'600px',height:'500px', lock: true,opacity: 0.3});
		}
	}
	function updatePurchaseProcedures(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_all_procedures.html?purchase_id=<%=purchase_id%>&isOutterUpdate=2';
		$.artDialog.open(uri, {title: '修改各流程信息',width:'550px',height:'500px', lock: true,opacity: 0.3});
	}
	function purchaseInvoiceState(follow_up_type){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_invoice_state.html?purchase_id=<%=purchase_id%>&follow_up_type="+follow_up_type;
		var procedure = "";
		if(4 == follow_up_type){
			procedure = "发票流程";
		}else if(5 == follow_up_type){
			procedure = "退税流程";
		}else if(8 == follow_up_type){
			procedure = "内部标签流程";
		}else if(33 == follow_up_type){
			procedure = "第三方标签流程";
		}
		$.artDialog.open(uri, {title: procedure+'['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	}
	function purchaseQualityInspect(isExistQualityFile){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_quality_inspection.html?purchase_id=<%=purchase_id%>&isExistQualityFile=<%=isExistQualityFile%>";
		$.artDialog.open(uri, {title: '质检要求['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	};
	function qualityInspectionLogs(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_quality_inspection_logs.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '质检要求日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function purchaseTagLogs(followUpType){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_tag_logs.html?purchase_id=<%=purchase_id%>&followUpType="+followUpType;
		var titlePage = "";
		if('<%=PurchaseLogTypeKey.NEED_TAG %>' == followUpType)
		{
			titlePage = "内部标签";
		}
		else
		{
			titlePage = "第三方标签";
		}
		$.artDialog.open(uri, {title: titlePage+'日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function purchaseProductModel(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '商品范例['+<%=purchase_id%>+']',width:'570px',height:'350px', lock: true,opacity: 0.3});
	}
	function productModelLogs(){
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model_logs.html?purchase_id=<%=purchase_id%>";
		$.artDialog.open(uri, {title: '商品范例日志['+<%=purchase_id%>+']',width:'750px',height:'400px', lock: true,opacity: 0.3});
	}
	function addProductsPicture(){
		 //添加商品图片
		var purchase_id = '<%= purchase_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_product_file.html?purchase_id="+purchase_id;
		var addUri =  getSelectedIdAndNames();
	 	if($.trim(addUri).length < 1 ){
			showMessage("请先选择商品!","alert");
			return ;
		}else{uri += addUri;}
		$.artDialog.open(uri , {title: "商品范例上传["+purchase_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
	}
	function addProductPicture(pc_id){
		 //添加商品图片
		var purchase_id = '<%= purchase_id%>';
		var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_product_file.html?purchase_id="+purchase_id+"&pc_id="+pc_id;
		$.artDialog.open(uri , {title: "商品范例上传["+purchase_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3,close:function(){window.location.reload();},fixed: true});
	}
	function autoComplete(obj)
	{
		addAutoComplete(obj,
			"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
			"merge_info",
			"p_name");
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	function ajaxLoadPurchaseDetail(obj,rowid)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getPurchaseDetailJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:{purchase_detail_id:rowid},
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function downLoad(fileName , tableName , folder){
		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>');
	}
	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
	function deleteFileQuality(file_id, file_name){
		if(confirm("确定要删除文件：["+file_name+"]吗？")){
			$.ajax({
				url:'<%= deleteFileAction%>',
				dataType:'json',
				data:{table_name:'file',file_id:file_id,pk:'file_id'},
				success:function(data){
					if(data && data.flag === "success"){
						window.location.reload();
					}else{
						showMessage("系统错误,请稍后重试","error");
					}
				},
				error:function(){
					showMessage("系统错误,请稍后重试","error");
				}
			})
		}
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);      	 
	}
	//文件上传
	function uploadFile(_target){
	    var obj  = {
		     reg:"all",
		     limitSize:8,
		     target:_target
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		$.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
			 close:function(){
					//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
					 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		}});
	}
	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target){
		if($.trim(fileNames).length > 0 ){
			var targetNode = $("#"+target);
			$("input",targetNode).val(fileNames);
			$("#qualityFileForm").submit();
		}
	}
	function submitQuality(){
		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/UploadPurchaseQualityAction.action',
			data:$("#qualityFileForm").serialize(),
			dataType:'json',
			type:'post',
			success:function(data){
				if(data && data.flag == "true"){
					showMessage("上传成功","success");
					window.location.reload();
				}else{
					showMessage("上传失败","error");
					window.location.reload();
				}
			},
			error:function(){
				showMessage("系统错误","error");
				window.location.reload();
			}
		})	
	}
	function windowRefresh(){
		window.location.reload();
	}

	 function onlineScanner(){
 	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_scanner.html?target=jquery_file_up"; 
 		$.artDialog.open(uri , {title: '在线获取',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
 	}
	//图片在线显示
	function showPictrueOnline(fileWithType,fileWithId , currentName)
	{
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			cmd:"multiFile",
			table:'file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow)
	    {
			window.top.openPictureOnlineShow(obj);
		}
		else
		{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	}
  //product_file表中的图片显示，
	function showPictrueOnline1(fileWithType,fileWithId , currentName,product_file_type){
	    var obj = {
			file_with_type:fileWithType,
			file_with_id : fileWithId,
			current_name : currentName ,
			product_file_type:product_file_type,
			cmd:"multiFile",
			table:'product_file',
			base_path:'<%= ConfigBean.getStringValue("systenFolder")%>' + "upload/"+'<%= systemConfig.getStringConfigValue("file_path_product")%>'
		}
	    if(window.top && window.top.openPictureOnlineShow){
			window.top.openPictureOnlineShow(obj);
		}else{
		    openArtPictureOnlineShow(obj,'<%= ConfigBean.getStringValue("systenFolder")%>');
		}
	    
	}
	function purchaseProductModelFinishToNeed()
	{
		if(confirm("您确定要修改商品范例流程从完成到需要阶段吗？"))
		{
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/purchase/purchase_product_model_finish_to_need.html?purchase_id=<%=purchase_id%>";
			$.artDialog.open(uri, {title: '修改商品范例完成-->需要['+<%=purchase_id%>+']',width:'570px',height:'280px', lock: true,opacity: 0.3});

		}
	}
	
</script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.addr_table tr td
{
	line-height:20px;
	height:18px;
}
#moneyTable tr td{
	line-height: 25px;
	height: 25px;
	border: 1px solid silver ;
}
#moneyTable{border-collapse: collapse;}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
ol.fileUL{list-style-type:none;}
ol.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
</style>

</head>

<body >
<form action="purchase_detail.html" name="reach_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>
<form action="" name="download_form"></form>

<form name="del_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/clearPurchaseDetail.action">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form name="apply_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applyMoneyAction.action">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id %>'"/>
</form>

<div class="demo">
<div id="purchaseTabs">
	<ul>
		<li><a href="#purchase_basic_info">采购单<span> </span></a></li>
		<li><a href="#purchase_address_info">地址信息<span> </span></a></li>
		<%
			String fontStyleInvoice	 = "";
			String fontStyleDrawback = "";
			if(InvoiceKey.NOINVOICE != purchase.get("invoice",InvoiceKey.NOINVOICE) || DrawbackKey.NODRAWBACK != purchase.get("drawback",DrawbackKey.NODRAWBACK))
			{
		 		int[] invoiceFileType = {FileWithTypeKey.PURCHASE_INVOICE};
		 		DBRow[] fileArrayInvoice = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.valueOf(purchase_id), invoiceFileType);
		 		if(InvoiceKey.FINISH == purchase.get("invoice",InvoiceKey.NOINVOICE))
		 		{
		 			if(fileArrayInvoice.length > 0)
		 			{
		 				fontStyleInvoice = "color: green; font-weight: bold";
		 			}else{
		 				fontStyleInvoice = "color: red; font-weight: bold";
		 			}
		 		}
		 		else if(InvoiceKey.NOINVOICE == purchase.get("invoice",InvoiceKey.NOINVOICE))
		 		{
		 			fontStyleInvoice = "";
		 		}
		 		else
		 		{
		 			fontStyleInvoice = "color: blue; font-weight: bold";
		 		}
		 		int[] drawbackFileType = {FileWithTypeKey.PURCHASE_DRAWBACK};
		 		DBRow[] fileArrayDrawback = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.valueOf(purchase_id), drawbackFileType);
		 		if(DrawbackKey.FINISH == purchase.get("drawback",DrawbackKey.NODRAWBACK))
		 		{
		 			if(fileArrayDrawback.length > 0)
		 			{
		 				fontStyleDrawback = "color: green; font-weight: bold";
		 			}
		 			else
		 			{
		 				fontStyleDrawback = "color: red; font-weight: bold";
		 			}
		 		}
		 		else if(DrawbackKey.NODRAWBACK == purchase.get("drawback",DrawbackKey.NODRAWBACK))
		 		{
		 			fontStyleDrawback = "";
		 		}
		 		else
		 		{
		 			fontStyleDrawback = "color: blue; font-weight: bold";
		 		}
		%>
		
			<li><a href="#purchase_bill_drawback_info"><font style='<%=fontStyleInvoice %>'>发票</font><font style='<%=fontStyleDrawback %>'>退税</font><span> </span></a></li>
		<%
			}
		%>
	 	<%
	 		int[] tagFileType = {FileWithTypeKey.PURCHASE_TAG_FILE};
	 		DBRow[] fileArrayTag = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.valueOf(purchase_id), tagFileType);
	 		String fontStyle	 = "";
	 		if(TransportTagKey.FINISH == purchase.get("need_tag",TransportTagKey.NOTAG))
	 		{
	 			if(fileArrayTag.length > 0)
	 			{
	 				fontStyle = "color: green; font-weight: bold";
	 			}else{
	 				fontStyle = "color: red; font-weight: bold";
	 			}
	 		}
	 		else if(TransportTagKey.NOTAG == purchase.get("need_tag",TransportTagKey.NOTAG))
	 		{
	 			fontStyle = "";
	 		}
	 		else
	 		{
	 			fontStyle = "color: blue; font-weight: bold";
	 		}
	 	%>
		<li><a href="#purchase_tag_make"><font style="<%=fontStyle %>">内部标签</font><span> </span></a></li>
		<%
		String fontStyleThird	 = "";
		if(TransportTagKey.NOTAG != purchase.get("need_third_tag",TransportTagKey.NOTAG))
		{
	 		if(TransportTagKey.FINISH == purchase.get("need_third_tag",TransportTagKey.NOTAG))
	 		{
	 			int[] fileType	= {FileWithTypeKey.PRODUCT_TAG_FILE};
	  			if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(Long.parseLong(purchase_id),fileType,"purchase_tag_types"))
	 			{
	 				fontStyleThird = "color: green; font-weight: bold";
	 			}else{
	 				fontStyleThird = "color: red; font-weight: bold";
	 			}
	 		}
	 		else
	 		{
	 			fontStyleThird = "color: blue; font-weight: bold";
	 		}
	 	%>
		<li><a href="#purchase_tag_third"><font style="<%=fontStyleThird %>">第三方标签</font><span> </span></a></li>
		<%} %>
		<%
			String fontStyleQuality = "";
			if(QualityInspectionKey.NO_NEED_QUALITY != purchase.get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY))
			{
				if(QualityInspectionKey.FINISH == purchase.get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY)){
					if(purchaseQualityFiles.length > 0){
						fontStyleQuality = "color: green; font-weight: bold";
					}else{
						fontStyleQuality = "color: red; font-weight: bold";
					}
				}
				else if(QualityInspectionKey.NO_NEED_QUALITY == purchase.get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY))
				{
					fontStyleQuality = "";
				}
				else
				{
					fontStyleQuality = "color: blue; font-weight: bold";
				}
		%>
		<li><a href="#purchase_quality_inspection"><font style='<%=fontStyleQuality %>'>质检要求</font><span> </span></a></li>
		<%		
			}
		%>
		 <%
		 	String fontStyleProductModel = "";
		 	if(PurchaseProductModelKey.FINISH == purchase.get("product_model",PurchaseProductModelKey.NOT_PRODUCT_MODEL))
		 	{
		 		if(imagesRowsProduct.length > 0)
		 		{
		 			fontStyleProductModel = "color: green; font-weight: bold";
		 		}else{
		 			fontStyleProductModel = "color: red; font-weight: bold";
		 		}
		 	}
		 	else if(PurchaseProductModelKey.NOT_PRODUCT_MODEL == purchase.get("product_model",PurchaseProductModelKey.NOT_PRODUCT_MODEL))
		 	{
		 		fontStyleProductModel = "";
		 	}
		 	else
		 	{
		 		fontStyleProductModel = "color: blue; font-weight: bold";
		 	}
		 %>
		<li><a href="#purchase_product_model"><font style='<%=fontStyleProductModel %>'>商品范例</font><span> </span></a></li>
		 
	</ul>
	<div id="purchase_basic_info">
		<table style="width: 100%">
			<tr>
				<td style="width:65%" valign="top">
					<table style="width:100%">
						<tr >
							<td align="left" style="width:13%"><font>采购单号：</font></td>
							<td align="left" style="width:87%"><font>P<%=purchase_id%></font></td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font>到货情况：</font></td>
							<td align="left" style="width:87%" valign="middle">
								<table>
									<tr>
										<%
											int rowSpanNum = deliveryOrderRows.length >1?(deliveryOrderRows.length-1):0;
										%>
										<td align='left' rowspan="<%=rowSpanNum %>" style="width:50%">
											<font>
											<%=purchasearrivekey.getQuoteStatusById(purchase.getString("arrival_time"))%>
											</font>
										</td>
										<td style="width:50%;" align="left">
											<table>
												<%
											  		if(deliveryOrderRows.length == 0){
											  			out.println("<tr><td align='left'>&nbsp;</td></tr>");
											  		}else{
											  			out.println("<tr><td align='left'>T"+deliveryOrderRows[0].getString("transport_id")+" "+deliveryOrderKey.getTransportOrderStatusById(deliveryOrderRows[0].get("transport_status",0))+"</td></tr>");
											  			for(int ii=1; deliveryOrderRows != null && ii<deliveryOrderRows.length; ii++) {
												  			out.println("<tr><td align='left'>T"+deliveryOrderRows[ii].getString("transport_id")+" "+deliveryOrderKey.getTransportOrderStatusById(deliveryOrderRows[ii].get("transport_status",0))+"</td></tr>");
												  		}
											  		}
										  		 %>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font>返修条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("repair_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font>贴标条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("labeling_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font>交货条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("delivery_product_terms")%>
							</td>
						</tr>
						<tr>
							<td align="left" style="width:13%"><font>其他条款：</font></td>
							<td align="left" style="width:87%">
								<%=purchase.getString("purchase_terms")%>
							</td>
						</tr>
					</table>
				</td>
				<td style="width:35%" align="left">
					<table style="width:100%">
						<tr>
							<td style="width:80%">
								<fieldset class="set" style="border-color:blue;">
								<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
									<span style="font-size:12px;color:#000000;font-weight:normal;">P<%=purchase.getString("purchase_id") %>采购单定金
									<%
										String statusName = "";
										if(applyMoneys.length > 0)
										{
											List imageList = applyMoneyMgrLL.getImageList(applyMoneys[0].getString("apply_id"),"1");
											//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
										 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[0].getString("status")) && imageList.size() < 1)
										 	{
										 		statusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
										 	}else{
										 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[0].getString("status"));	
										 	}
										 	statusName = "("+statusName+")";
										}
									%>
									(<%=purchase.get("apply_money_over",0d)==0?df.format(tDate.getDiffDate(purchase.getString("purchase_date"),"dd"))+"天":purchase.get("apply_money_over",0d)+"天完成"%>)
									</span>
								</legend>
								<table style="width:100%">
									<tr style="width:100%">
										<td style="width:100%;padding-left: 10px;">
											采购单金额:<%=purchaseMgr.getPurchasePrice(purchase.get("purchase_id",0l))%>RMB
										</td>
									</tr>
									<%
						  				if(applyMoneys.length>0) {
						  					String moneyStandardStr = "";
						  					if(!"RMB".equals(applyMoneys[0].getString("currency")))
						  					{
						  						moneyStandardStr = "/"+applyMoneys[0].getString("standard_money")+"RMB";
						  					}
							  				out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneys[0].get("apply_id",0l)+")\">F"+applyMoneys[0].get("apply_id",0)+"</a>"+ statusName +applyMoneys[0].get("amount",0f)+applyMoneys[0].getString("currency")+moneyStandardStr+"</td></tr>");
							  				DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[0].getString("apply_id"));
							  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
							  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
							  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
							  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
							  				int moneyStatus = applyTransferRows[ii].get("status",0);
							  				String transferMoneyStandardStr = "";
							  				if(!"RMB".equals(currency))
							  				{
							  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
							  				}
							  				if(moneyStatus != 0 )
								  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
							  				else
								  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
								  			}
							  			}
						  		%>
								</table>
								</fieldset>
								<%
									if(deliveryOrderRows.length > 0)
									{
										for(int de = 0; de < deliveryOrderRows.length; de ++)
										{
											String statusNameTr = "";
											String appMoneyStateStr = "";
											DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[de].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
												if(applyMoneyDelivers.length > 0)
												{
													List imageListTr = applyMoneyMgrLL.getImageList(applyMoneyDelivers[0].getString("apply_id"),"1");
													//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
												 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneyDelivers[0].getString("status")) && imageListTr.size() < 1)
												 	{
												 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
												 	}else{
												 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(applyMoneyDelivers[0].getString("status"));	
												 	}
												 	statusNameTr = "("+statusNameTr+")";
												appMoneyStateStr = statusNameTr + applyMoneyDelivers[0].get("amount",0.0) + applyMoneyDelivers[0].getString("currency");
												}
								%>			
										<fieldset class="set" style="border-color:#993300;">
											<legend>
												<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=deliveryOrderRows[de].getString("transport_id") %>'>T<%=deliveryOrderRows[de].getString("transport_id") %></a>
												交货单货款
											</legend>
											<table style="width:100%">
												<tr style="width:100%">
													<td style="width:100%;padding-left: 10px;">
														交货单金额:<%=transportMgrZJ.getTransportSendPrice(deliveryOrderRows[de].get("transport_id",0L)) %>RMB
													</td>
												</tr>
											<%
												if(applyMoneyDelivers.length > 0)
												{
													String moneyStandardDeliverStr = "";
								  					if(!"RMB".equals(applyMoneyDelivers[0].getString("currency")))
								  					{
								  						moneyStandardDeliverStr = "/"+applyMoneyDelivers[0].getString("standard_money")+"RMB";
								  					}
													out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneyDelivers[0].get("apply_id",0l)+")\">F"+applyMoneyDelivers[0].get("apply_id",0)+"</a>"+appMoneyStateStr+moneyStandardDeliverStr+"</td></tr>");
													DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneyDelivers[0].getString("apply_id"));
										  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
										  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
										  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
										  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
										  				int moneyStatus = applyTransferRows[ii].get("status",0);
										  				String transferMoneyStandardDeliverStr = "";
										  				if(!"RMB".equals(currency))
										  				{
										  					transferMoneyStandardDeliverStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
										  				}
									  					if(moneyStatus != 0 )
										  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
										  				else
										  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
										  			}
												}
											%>
								</table>
								</fieldset>
								<%			
											
										}
									}
								%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr align="left">
				<td colspan="2"><br/>
					<input name="button" type="button" class="long-button" value="修改采购单信息" onClick="showTransit()"/>
					<input name="button" type="button" class="long-button" value="修改条款信息" onClick="updatePurchaseTerms()"/>
					<input name="button" type="button" class="long-button" value="修改各流程信息" onClick="updatePurchaseProcedures()"/>
					 <%
						if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
						{
					  %>
					  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.savePurchaseDetail">
					  <input  type="button" class="long-button-upload" value="上传采购单" onclick="uploadPurchaseDetail()"/>
					  </tst:authentication>
					  <% 
						}
					  %>
					  <input  type="button" class="long-button-next" value="下载采购单" onclick="downloadPurchase(<%=purchase_id%>)"/>
					  <input type="button" class="long-button-print" value="打印" onclick="print()"/>
					  <input type="button" class="long-button" value="创建交货" onclick="addDeliveryTransportOrder(<%=purchase_id%>)"/>
				</td>
			</tr>
		</table>
	</div>
	<div id="purchase_address_info">
			<table width="100%" style="border:1px solid silver;">
				<tr>
					<td style="width:10%;text-align:right;">
<%--						<font style="font-family: 黑体; font-size: 14px;">地址：</font>--%>&nbsp;
					</td>
					<td style="width:90%;">
						<table style="width:100%;" class="addr_table">
							<tr>
								<td style="width:45%;">
									<table style="width:100%;border-collapse:collapse ;">
										<tr>
											<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">提货地址</font></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_house_number") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_street") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_city") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_zip_code") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ps_id = purchase.get("supplier_province",0L) ;
													DBRow psIdRow = productMgr.getDetailProvinceByProId(ps_id);
												%>
												<%=(psIdRow != null? psIdRow.getString("pro_name"):supplier.getString("supplier_pro_input") ) %>
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ccid = purchase.get("supplier_native",0L); 
													DBRow ccidRow = orderMgr.getDetailCountryCodeByCcid(ccid);
												%>
												<%= (ccidRow != null?ccidRow.getString("c_country"):"" ) %>	
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_link_man") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("supplier_link_phone") %></td>
										</tr>
									</table>
								</td>
								<td style="width:10%;text-align: center;font-size: 18px;">
									-->
								</td>
								<td style="width:45%;">
									<table style="width:100%;border-collapse:collapse ;" class="addr_table">
										<tr>
											<td colspan="2" align="center" style="border: 1px;"><font style="font-family: 黑体; font-size: 14px;">收货地址</font></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">门牌号：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_house_number") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">街道：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_street") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">城市：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_city") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">邮编：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("deliver_zip_code") %></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属省份：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ps_id1 = null!=purchase?purchase.get("deliver_provice",0l):0L;
													DBRow psIdRow1 = productMgr.getDetailProvinceByProId(ps_id1);
												%>
												<%=(psIdRow1 != null? psIdRow1.getString("pro_name"):purchase.getString("deliver_pro_input") ) %>
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">所属国家：</td>
											<td style="border: 1px solid silver;width: 80%;">
												<%
													long ccid1 = null!=purchase?purchase.get("deliver_native",0l):0L; 
													DBRow ccidRow1 = orderMgr.getDetailCountryCodeByCcid(ccid1);
												%>
												<%= (ccidRow1 != null?ccidRow1.getString("c_country"):"" ) %>	
											</td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系人：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("delivery_linkman")%></td>
										</tr>
										<tr>
											<td style="border: 1px solid silver;text-align:right;width: 20%;">联系电话：</td>
											<td style="border: 1px solid silver;width: 80%;"><%=purchase.getString("linkman_phone")%></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">
						<font style="font-family: 黑体; font-size: 14px;">供应商：</font>
					</td>
					<td>
						<%=null!=supplier?supplier.getString("sup_name"):"" %>
					</td>
				</tr>
				<tr>
				<td colspan="3" align="left">
					<br/>
					<input name="button" type="button" class="long-button" value="修改" onClick="showTransit()"/>
				</td>
			</tr>
			</table>
	</div>
	<%
		if(InvoiceKey.NOINVOICE != purchase.get("invoice",InvoiceKey.NOINVOICE) || DrawbackKey.NODRAWBACK != purchase.get("drawback",DrawbackKey.NODRAWBACK ))
		{
	%>
	<div id="purchase_bill_drawback_info">
		<table width="98%" border="0" cellpadding="0" cellspacing="0">
		     <tr width="98%">
		     	<td style="height:30px;line-height: 28px;width: 10%;" align="left">
		     		发票流程:<span style="color: green"><%=invoiceKey.getInvoiceById(purchase.get("invoice",0)) %></span>
		     	</td>
		     	<td style="height:30px;line-height: 28px;width: 90%;" align="left">
		     		<input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(4)" value="发票"/>
		     	</td>
		     </tr>
		     <tr>
		     	<td style="height:30px;line-height: 28px;width: 10%;" align="left">
		     		退税流程:<span style="color: green"><%=drawbackKey.getDrawbackById(purchase.get("drawback",0)) %></span>
		     	</td>
		     	<td style="height:30px;line-height: 28px;width: 90%;" align="left">
		     		<input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(5)" value="退税"/>
		     	</td>
		     </tr>
		</table>
		<table id="tableLog" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" class="zebraTable" onload="onLoadInitZebraTable();">
		     <tr>
		       <th width="10%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作员</th>
		       <th width="40%" nowrap="nowrap" class="right-title"  style="text-align: center;">内容</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">预计完成时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">记录时间</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">操作类型</th>
		     </tr>
		     <%
		    	int[] state_type = {4, 5};
		    	DBRow [] logRows = purchaseMgrIfaceZr.getPurchaseLogsByPurchaseIdAndType(Long.parseLong(purchase_id), state_type);
		     	if(logRows.length > 0){
		     		for(int i = 0; i < logRows.length; i ++){
		     			DBRow logRow = logRows[i];
		     %>			
		     			 <tr height="30px" style="padding-top:3px;">
					     	<td><%=null == adminMgrLL.getAdminById(logRow.getString("follower_id"))?"":adminMgrLL.getAdminById(logRow.getString("follower_id")).getString("employe_name") %></td>
					     	<td><%=logRow.getString("followup_content") %></td>
					     	<td>
					     	<% if(!"".equals(logRow.getString("followup_expect_date"))){
					     		out.println(DateUtil.FormatDatetime("yy-MM-dd",new SimpleDateFormat("yyyy-MM-dd").parse(logRow.getString("followup_expect_date"))));
					        	}
					        %>
					     	</td>
					     	<td>
					     		<% if(!"".equals(logRow.getString("followup_date"))){
					     			out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(logRow.getString("followup_date"))));
					     		}
					     		%>
					     	</td>
		  					<td>
		  						<%=followuptype.get(logRow.get("followup_type",0))%>-
		  						<%
		  							if(4 == logRow.get("followup_type",0)){
		  						%>
		  							<%=invoiceKey.getInvoiceById(String.valueOf(logRow.get("followup_type_sub",0))) %>
		  						<%	
		  							}else{
	  							%>
		  							<%=drawbackKey.getDrawbackById(logRow.get("followup_type_sub",0)) %>
		  						<%	
	  								}
						  		%>
		  					</td>
					     </tr>
		     <%		
		     		}
		     	}else{
		     %>		
		     	<tr>
		     		<td colspan="5" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
		     	</tr>
		     <%		
		     	}
			%>
	     </table>
	</div>
	<%	}%>
	 
	<div id="purchase_tag_make">
		<table style="margin-top:8px;">
			<tr>
				<td align="right">
					内部标签流程:<span style="color: green"><%=new TransportTagKey().getTransportTagById(purchase.get("need_tag",0)) %></span>
				&nbsp;</td>
				<td align="left">
					<input type="button" class="long-button" value="跟进内部标签" onclick="purchaseInvoiceState(8)"/>
  					<input type="button" class="long-button" value="内部标签日志" onclick="purchaseTagLogs('<%=PurchaseLogTypeKey.NEED_TAG %>')"/>
					<input type="button" class="long-button-print" value="制作标签" onclick="printLabel()"/>
					<input type="button" class="long-button-print" value="制作唛头" onclick="shippingMark()"/>
				</td>
			</tr>
		</table>
		<br/>
</div>
<%
if(TransportTagKey.NOTAG != purchase.get("need_third_tag",TransportTagKey.NOTAG))
{
%>
<div id="purchase_tag_third" style='<%=fontStyleThird %>'>
	<table style="margin-top:8px;">
		<tr>
			<td align="right">
			第三方标签流程:<span style="color: green"><%=new TransportTagKey().getTransportTagById(purchase.get("need_third_tag",0)) %></span>
			&nbsp;</td>
			<td align="left">
				<input type="button" class="long-button" value='跟进第三方标签' onclick="purchaseInvoiceState('<%=PurchaseLogTypeKey.THIRD_TAG %>')"/>
				<input type="button" class="long-button" value="第三方标签日志" onclick="purchaseTagLogs('<%=PurchaseLogTypeKey.THIRD_TAG %>')"/>
			</td>
		</tr>
		<tr><td height="8px;" colspan="2"></td></tr>
	</table>
	<div id="tagTabs" style="margin-top:4px;">
	   		 	<ul>
		<%
				 		if(selectedList != null && selectedList.size() > 0){
				 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
		%>	
				 				<li><a href="#purchase_product_tag_<%=index %>"> <%=selectedList.get(index) %></a></li>
				 			<% 	
				 			}
				 		}
			 		%>
	   		 	</ul>
	   		 	<%
	   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
	   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
	   		 			%>
	   		 			 <div id="purchase_product_tag_<%= index%>">
	   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
	   		 			 			<tr> 
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
				  						<th width="40%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
				  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
				  					</tr>
	   		 			 	<%
	   		 			 		if(tempListRows != null && tempListRows.size() > 0){
	   		 			 			 for(DBRow row : tempListRows){
	   		 			 			%>
	   		 			 				<tr style="height: 25px;">
	   		 			 					<td>
	   		 			 					<a href='<%= downLoadFileAction%>?file_name=<%= row.getString("file_name") %>&folder=<%=systemConfig.getStringConfigValue("file_path_product")%>'><%= row.getString("file_name") %></a>
	   		 			 					</td>
	   		 			 					<td><%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %></td>
	   		 			 					<td>
	   		 			 						<% if(purchase.get("need_third_tag",0) != TransportTagKey.FINISH){ %>
	   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
	   		 			 				 		<%} %>	
	   		 			 				 	</td>
	   		 			 				</tr>	
	   		 			 			<% 
	   		 			 		 }
	   		 			 		}else{
	   		 			 			%>
	 		 			 			<tr>
	 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
	 								</tr>
	   		 			 			<%
	   		 			 		}
	   		 			 	%>
	   		 			 	 	</table>
			</div>
		<%	
			} 
		%>
	</div>
</div>
<%} %>
	<%
		if(QualityInspectionKey.NO_NEED_QUALITY != purchase.get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY))
		{
	%>
	<div id="purchase_quality_inspection">
	<div id="qualityInspection">
	<form action='<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/UploadPurchaseQualityAction.action' method="post" id="qualityFileForm">
		<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder") %>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id %>'/>
		<input type="hidden" name="purchase_id" value='<%=purchase_id %>'/>
		<input type="hidden" name="quality_inspection" value='<%=purchase.get("quality_inspection",0) %>'/>
	<table style="width: 80%;">
	<tr>
		<td width="10%" align="right" nowrap="nowrap" class="STYLE3" >
			质检要求流程:<span style="color: green"><%=new QualityInspectionKey().getQualityInspectionById(purchase.get("quality_inspection",0)) %></span>
		</td>
		<td width="90%" align="left">
			<input name="button" type="button" class="long-button" value="跟进质检要求" onClick="purchaseQualityInspect(<%=isExistQualityFile %>)"/>
			<input name="button" type="button" class="long-button" value="查看日志" onClick="qualityInspectionLogs()"/>
		</td>
	</tr>
	<tr><td height="20px;" colspan="2"></td></tr>
	<%
		//if(2 != isOutterUpdate){
	%>	
	<tr width="100%">
		<td width="10%" align="right" nowrap="nowrap" class="STYLE3" >
			上传质检要求:
		</td>
		<td width="90%" align="left">
			<input type="hidden" name="sn" value="P_quality"/>
			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURHCASE_QUALITY_INSPECTION %>" />
			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>" />
			<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
			<input type="button" class="long-button" onclick="onlineScanner();" value="在线获取" />
			<div id="jquery_file_up">	
          		<input type="hidden" name="file_names" id="file_names" value=""/>
          		<ol class="fileUL"> 
          		</ol>
          	</div>
		</td>
	</tr>
	<%
		int[] fileTypes = {9};
		purchaseQualityFiles = purchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(Long.parseLong(purchase_id), fileTypes);
		if(purchaseQualityFiles.length > 0){
	%>
		<tr>
		<td width="10%" align="right" valign="middle" nowrap="nowrap" class="STYLE3">&nbsp;</td>
		<td width="90%" align="left" valign="top" >
			<table width="75%">
				<%
					if(purchaseQualityFiles.length > 0){
						for(int f = 0; f < purchaseQualityFiles.length; f ++){
							DBRow purchaseQualityFile = purchaseQualityFiles[f];
				%>
					<tr style="height: 25px;">
						<td width="50%" align="left">
						 <!-- 如果是图片文件那么就是要调用在线预览的图片 -->
		 			 	 <!-- 如果是其他的Office文件那么就要提供在线阅读的页面 -->
		 			 	 <!-- 在提供在线阅读的时候是要进行文件的装换的。(如果没有转化) -->
		 			 	 	<%if(StringUtil.isPictureFile(purchaseQualityFile.getString("file_name"))){ %>
				 			 	 <p>
				 			 	 	<a href="javascript:void(0)" onclick="showPictrueOnline('<%=FileWithTypeKey.PURHCASE_QUALITY_INSPECTION %>','<%=purchase.get("purchase_id",0l) %>','<%=purchaseQualityFile.getString("file_name") %>');"><%=purchaseQualityFile.getString("file_name") %></a>
				 			 	 </p>
			 			 	 <%} else if(StringUtil.isOfficeFile(purchaseQualityFile.getString("file_name"))){%>
			 			 	 		<p>
			 			 	 			<a href="javascript:void(0)"  file_id='<%=purchaseQualityFile.get("file_id",0l) %>' onclick="openOfficeFileOnline(this,'<%=ConfigBean.getStringValue("systenFolder")%>','<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>')" file_is_convert='<%=purchaseQualityFile.get("file_is_convert",0) %>'><%=purchaseQualityFile.getString("file_name") %></a>
			 			 	 		</p>
			 			 	 <%}else{ %>
 		 			 	 	  		<p><a href='<%= downLoadFileAction%>?file_name=<%=purchaseQualityFile.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'><%=purchaseQualityFile.getString("file_name") %></a></p> 
 		 			 	 	  <%} %>
						
						</td>
						<td width="15%" align="left">
							<%=null == adminMgrLL.getAdminById(purchaseQualityFile.getString("upload_adid"))?"":adminMgrLL.getAdminById(purchaseQualityFile.getString("upload_adid")).getString("employe_name") %>
						</td>
						<td width="25%" align="left">
						<% if(!"".equals(purchaseQualityFile.getString("upload_time"))){
								out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(purchaseQualityFile.getString("upload_time"))));
							}
						%>
						</td>
						<td width="10%">
							<%
								if(QualityInspectionKey.FINISH != purchase.get("quality_inspection",0))
								{
							%>
								<a href="javascript:deleteFileQuality('<%=purchaseQualityFile.get("file_id",0l) %>','<%=purchaseQualityFile.getString("file_name") %>')">删除</a>
							<%		
								}
								else
								{
							%>
								&nbsp;
							<%		
								}
							%>
							  
						</td>
					</tr>
				<%	
						}
					}
				%>
			</table>
			</td>
		</tr>
		<%
			}
		//}
		%>
</table>
</form>
	</div>
	</div>
	<%	} %>
	 
	<div id="purchase_product_model">
		<div id="productModel">
		<table width="98%">
			<tr>
				<td colspan="3" align="left">
					商品范例流程:<span style="color: green"><%=new PurchaseProductModelKey().getProductModelKeyName(purchase.getString("product_model")) %></span>
					<input name="button" type="button" class="long-button" value="跟进商品范例" onClick="purchaseProductModel()"/>
					<input name="button" type="button" class="long-button" value="查看日志" onClick="productModelLogs()"/>
					<%
						if(PurchaseProductModelKey.FINISH == purchase.get("product_model",0))
						{
					%>
						<input name="button" type="button" class="long-long-button" value="商品范例完成-->需要" onClick="purchaseProductModelFinishToNeed()"/>
					<%		
						}
					%>
				</td>
			</tr>
			<tr><td height="20px;" colspan="3"></td></tr>
		</table>
			<ul>
	<%
					 		if(selectedListProduct != null && selectedListProduct.size() > 0){
					 			for(int index = 0 , count = selectedListProduct.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#transport_product_<%=index %>"> <%=selectedListProduct.get(index) %></a></li>
					 			<% 	
								} 
					 		}
	%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedListProduct.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRowsProduct = imageMapProduct.get((index+1)+"");
   		 			%>
   		 			 <div id="transport_product_<%= index%>">
   		 			 <table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 	<tr>
		   		 			 <%
		   		 			 	String product_photo_description = systemConfig.getStringConfigValue("product_photo_description");
		   		 			 	if(!"".equals(product_photo_description))
		   		 			 	{
		   		 			 		String[] product_photo_description_arr = product_photo_description.split("\n");
		   		 			 		if(index < product_photo_description_arr.length)
		   		 			 		{
		   		 			 %>
		   	   		 			 	<td colspan="3">注:<%=product_photo_description_arr[index] %></td>
		   	   		 		 <%				
		   		 			 		}
		   		 			 	}
		   		 			 %>
   		 			 	</tr>
 		 			 	<tr> 
	  						<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
	  						<th width="30%" style="vertical-align: center;text-align: center;" class="right-title">商品名</th>
	  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
	  					</tr>
   		 			 	<%
   		 			 		if(tempListRowsProduct != null && tempListRowsProduct.size() > 0){
   		 			 			 for(DBRow row : tempListRowsProduct){
   		 			 			%>
   		 			 				<tr style="height: 25px;">
   		 			 					<td>
   		 			 					<!-- 如果是图片文件那么就是要调用在线预览的图片 -->
			 			 	 	          <%if(StringUtil.isPictureFile(row.getString("file_name"))){ %>
					 			 	       <p>
					 			 	 	        <a href="javascript:void(0)" onclick="showPictrueOnline1('<%=FileWithTypeKey.PURCHASE_PRODUCT_FILE %>','<%=purchase.get("purchase_id",0l) %>','<%=row.getString("file_name") %>',<%=row.get("product_file_type",01) %>);"><%=row.getString("file_name") %></a>
					 			 	       </p>
				 			 	          <%} else{ %>
	 		 			 	 	  		     <p><a href='<%= downLoadFileAction%>?file_name=<%=row.getString("file_name") %>&folder=<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>'><%=row.getString("file_name") %></a></p> 
	 		 			 	 	          <%} %>
   		 			 					
   		 			 					</td>
   		 			 					<td>
   		 			 						<%=null != productMgr.getDetailProductByPcid(row.get("pc_id",0))?productMgr.getDetailProductByPcid(row.get("pc_id",0)).getString("p_name"):""  %>
   		 			 					</td>
   		 			 					<td>
   		 			 						<% if(purchase.get("product_model",0) != TransportProductFileKey.FINISH){ %>
   		 			 							<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 		<%} %>	
   		 			 				 	</td> 
   		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
	</div>
</div>
 
</div>
</div>
 <table style="margin-top:4px;margin-bottom:4px;" width="98%" border="0" cellpadding="0" cellspacing="0">
 	<tr>
 		<td align="left">
 			<input type='button' class='long-button' value='商品范例照片' onclick='addProductsPicture()'/>
 			<input type='button' class='long-long-button' value='第三方标签打印文件' onclick='addProductTagTypesFile()' />		
 		</td>
 		<td align="right">
 			<input type='button' class='long-button' value="更新重量与体积" onclick='updatePurchaseVW(<%=purchase_id%>)'/>
 		</td>
 	</tr>
 </table>
<script>
	$("#purchaseTabs,#tagTabs,#productModel").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
	
	
	<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
	function checkEta(value, colname)
	{
		var a=/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})/
		if (!a.test(value))
		{
			return [false,"请输入正确的日期格式"];
		}
		else
		{
			return [true,""];
		}
	}
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'dataPurchaseDetails.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.98),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{purchase_id:<%=purchase_id%>},
					jsonReader:{
				   			id:'purchase_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['purchase_detail_id','product_id','商品名','工厂型号','预计到达日期','备用件','采购数','总数','收货数','在途数','单位','采购价(RMB)','当前价(RMB)','体积(cm³)','操作','purchase_id'], 
				   	colModel:[ 
				   		{name:'purchase_detail_id',index:'purchase_detail_id',hidden:true,sortable:false},
				   		{name:'product_id',index:'product_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'factory_type',index:'factory_type',align:'left',editable:true},
				   		{name:'eta',index:'eta',align:'left',width:65,editable:true,editrules:{custom:true,custom_func:checkEta}},
				   		{name:'backup_count',index:'backup_count',width:50,editable:true,align:'center',formatter:'number',editrules:{number:true,integer:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'order_count',index:'order_count',width:50,editable:<%=edit%>,align:'center',formatter:'number',editrules:{number:true,integer:true},formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'purchase_count',index:'purchase_count',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'reap_count',index:'reap_count',align:'center',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'transit_count',index:'transit_count',width:50,formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'unit_name',index:'unit_name',align:'center',width:60},
				   		{name:'price',index:'price',editable:<%=edit%>,align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false},
				   		{name:'unit_price',index:'unit_price',align:'center',width:60},
				   		{name:'purchase_volume',index:'purchase_volume',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","}},
				   		{name:'button',index:'button',align:'left',sortable:false},
				   		{name:'purchase_id',index:'purchase_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=purchase_id%>},hidden:true,sortable:false},
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'purchase_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
					multiselect: true,
					onSelectCell:function(id,name)
					{
	                		if(name=="button")
   					 		{
	                			var product_id = jQuery("#gridtest").jqGrid('getCell',id,'product_id');
   					 			addProductPicture(product_id);
   					 		}
					},
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   					}
				   					if(name='eta')
				   					{
				   						//jQuery("#"+iRow+"_eta","#gridtest").date_input();
				   					}
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	ajaxLoadPurchaseDetail(jQuery("#gridtest"),rowid);
				   				  	getPurchaseSumVWP(<%=purchase_id%>);
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/gridEditPurchaseDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/gridEditPurchaseDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
	</script>
<table width="95%">
	<tr>
		<td colspan="2">
			<span id="sum_volume"></span>
			<span id="sum_price"></span>
			<span id="sum_weight"></span>
		</td>
	</tr>
	<tr>
	  <td align="left" style="padding-left:25px;">
	  	<%if(purchase.get("purchase_status",0)!=PurchaseKey.CANCEL&&purchase.get("purchase_status",0)!=PurchaseKey.FINISH&&purchase.get("purchase_status",0)!=PurchaseKey.OPEN&&purchase.get("purchase_status",0)!=PurchaseKey.AFFIRMPRICE&&(purchase.get("arrival_time",0)!= PurchaseArriveKey.WAITAPPROVE&&purchase.get("arrival_time",0)!= PurchaseArriveKey.ALLARRIVE))
	  	  {
	  	%>
	  	<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseApproveMgrZJ.addPuchaseApprove">
	  		<input type="button" name="Submit2" value="申请完成" class="normal-white" onClick="applicationApprove()"/>&nbsp;&nbsp;
	  	</tst:authentication>
	  	<%
	  	  }
	  	%>
	  	<%
	  		if(purchase.get("purchase_status",0) == PurchaseKey.OPEN)
	  		{
	  	%>
		<input type="button" value="通知采购" class="normal-white" onclick="notice()"/>	  	
		<%
			}
		%>
		<%  
			if(purchase.get("purchase_status",0)==PurchaseKey.OPEN)
  	  		{
  	  			DBRow[] db = purchaseMgr.getPurchaseDetailByPurchaseid(purchase.get("purchase_id",0l),null,null);
  	  			if(db.length>0)
  	  			{
		%>
		<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.affirmPricePurchase">
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="normal-white" onclick="affirmPrice(<%=purchase.getString("purchase_id")%>)" value="价格确定"/>
		</tst:authentication>
	  <%
	  			}
	  		}
	  %>
	  
<%--	  		价格确认后的，未取消的采购单，并且没有非取消状态的定金申请--%>
		  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.affirmTransferPurchase">
	 	  	 <input type="button" class="normal-white" onclick="openDeposit(<%=purchase.getString("purchase_id")%>)" value="申请定金"/>
	  	  </tst:authentication>
		  <%if(preparePurchaseFunds == null){ %>
		  <input type="button" class="normal-white" onclick="affirmTranReady(<%=purchase.getString("purchase_id")%>)" value="预申请"/>
		  <%} %>
	  </td>
	  <td align="center" style="padding-right:50px;">
		&nbsp;
	  </td>
   	  <td align="right"><input type="button" name="Submit2" value="返回" class="normal-white" onClick="window.location.href='purchase.html'"/></td>
  	</tr>
</table>
<form action="purchase_detail.html" name="search_form" method="get">
	<input type="hidden" name="purchase_name" id="purchase_name"/>
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=purchase_id %>"/>
</form>
<form name="dataForm" action="purchase_detail.html" method="post">
    <input type="hidden" name="p"/>
    <input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
</form>
  <form action="" method="post" name="followup_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="followup_ptype_sub"/>
</form>

<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id" value="<%=purchase_id %>"/>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/admini strator/purchase/notice.action" method="post" name="notice_form">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form action="../lable_template/made_purchase_lable.html" name="print_label_form" target="_blank" method="post"">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
</form>

<form name="transfer_form" method="post">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="followup_type" value="2"/>
	<input type="hidden" name="followup_content"/>
	<input type="hidden" name="transfer_type"/>
	<input type="hidden" name="money_status"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmTransferPurchase.action" id="apply_funds_form" name="apply_funds_form" method="post">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="type" value="<%=ApplyTypeKey.PURCHASE%>"/>
	<input type="hidden" name="supplier_id"/>
	<input type="hidden" name="remark"/>
	<input type="hidden" name="amount" />
	<input type="hidden" name="payee" />
	<input type="hidden" name="payment_information" />
</form>
<!-- 预申请 -->
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/applyMoneyReady/AddApplyMoneyReadyAction.action" id="myForm" name="myForm" method="post">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="type" value="<%=ApplyTypeKey.PURCHASE%>"/>
	<input type="hidden" name="supplier_id"/>
	<input type="hidden" name="remark"/>
	<input type="hidden" name="amount" />
	<input type="hidden" name="payee" />
	<input type="hidden" name="payment_information" />
	<input type="hidden" name="last_time" />
	<input type="hidden" name="admin_id" />
	<input type="hidden" name="mail" />
	<input type="hidden" name="shortMessage" />
	<input type="hidden" name="pageMessage" />
	<input type="hidden" name="account_with_type" value="<%= AccountKey.PRE_APPLY_MONEY %>" />
</form>
<!-- 定金申请 -->
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/AddPurchaseDepositAction.action" id="depositForm" name="depositForm" method="post">
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="type" value="<%=ApplyTypeKey.PURCHASE%>"/>
	<input type="hidden" name="supplier_id"/>
	<input type="hidden" name="remark"/>
	<input type="hidden" name="amount" />
	<input type="hidden" name="payee" />
	<input type="hidden" name="payment_information" />
	<input type="hidden" name="last_time" />
	<input type="hidden" name="admin_id" />
	<input type="hidden" name="mail" />
	<input type="hidden" name="shortMessage" />
	<input type="hidden" name="pageMessage" />
	<input type="hidden" name="sn" />
	<input type="hidden" name="folder" />
	<input type="hidden" name="file_names" />
	<input type="hidden" name="currency"/>
	<input type="hidden" name="account_with_type" value="<%=AccountKey.APPLI_MONEY %>" />
</form>

  <script type="text/javascript">
//点击弹出制作麦头的界面
  function shippingMark()
  {
	    var purchase_id=<%=purchase_id%>;
	    var menpai="<%=purchase.getString("deliver_house_number") %>";
	    var jiedao="<%=purchase.getString("deliver_street") %>";
	    var chengshi="<%=purchase.getString("deliver_city") %>";
	    var youbian="<%=purchase.getString("deliver_zip_code") %>";
	    var shengfen="<%=ps_id%>";
	    var guojia="<%= ccid %>";
	    var lianxiren="<%=purchase.getString("delivery_linkman")%>";
	    var dianhua="<%=purchase.getString("linkman_phone")%>";
	    var uri = "../lable_template/print_purchase_shipping_mark.html?purchase_id="+purchase_id+"&dianhua="+dianhua+"&lianxiren="+lianxiren+"&guojia="+guojia+"&shengfen="+shengfen+"&youbian="+youbian+"&chengshi="+chengshi+"&jiedao="+jiedao+"&menpai="+menpai;
		$.artDialog.open(uri , {title: '唛头标签打印',width:'600px',height:'420px', lock: true,opacity: 0.3,fixed: true});
  }
  function applicationApprove()
  {
  	if(confirm("确定申请此采购单完成"))
  	{
  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
  		document.application_approve.submit();
  	}
  }
  
  function promptCheck(v,m,f)
			{
				if (v=="y")
				{
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 return true;
				}
			}
		  	function followup(purchase_id,purchase_invoice,purhcase_drawback)
			{
				if((1 == purchase_invoice && 1 == purhcase_drawback) || (4 == purchase_invoice && 4 == purhcase_drawback) ||(1 == purchase_invoice && 4 == purhcase_drawback)||(4 == purchase_invoice && 1 == purhcase_drawback)){
					alert("没有跟进项目...");
				}else{
					var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/purchase_log/purchase_log_invoice_drawback_add.html?purchase_id="+purchase_id+"&purchase_invoice="+purchase_invoice+"&purhcase_drawback="+purhcase_drawback;
					 $.artDialog.open(uri , {title: "日志跟进["+purchase_id+"]",width:'450px',height:'350px', lock: true,opacity: 0.3,fixed: true});
				}
			}
		  	function changeLogType(){
				$("#ptypeSub") && $("#ptypeSub").remove();
				$("#ptypeSubTitle") && $("#ptypeSubTitle").remove();
				var selVal = $("#ptype").val();
				if(3 == selVal || 4 == selVal){
					var appendElem = "<span id='ptypeSubTitle'>阶段</span><select id = 'ptypeSub' name='ptypeSub'>";
					if(3 == selVal){
						<%
			  			ArrayList statuses = invoiceKey.getInvoices();
			  			for(int i=2; i<statuses.size(); i++) {
			  				int key = Integer.parseInt(statuses.get(i).toString());
			  				String value = invoiceKey.getInvoiceById(String.valueOf(key));
				  		%>
				  			appendElem += "<option value='<%=key%>'><%=value%></option>";
				  		<%
				  			}
				  		%>
					}else{
						<%	
			  			ArrayList statuses4 = drawbackKey.getDrawbacks();
			  			for(int i = 2; i < statuses4.size(); i ++) {
			  				int key1 = Integer.parseInt(statuses4.get(i).toString());
				  			String value1 = drawbackKey.getDrawbackById(key1);
						%>
							appendElem += "<option value='<%=key1%>'><%=value1%></option>";
						<%
				  			}
				  		%>
					}
					appendElem += "</select>";
					$("#ptype").after($(appendElem));
					
				}
			};
			
			function remark(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>备注</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/logsPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function notice()
			{
				document.notice_form.submit();
			}
			
			function modPurchaseTerms()
			{
				tb_show('上传采购单详细','purchase_terms.html?purchase_id='+<%=purchase_id%>+'&TB_iframe=true&height=500&width=800',false);
			}
			
			 //点击制作标签后修改成弹出dialog
			function printLabel()
			{
				var value='<%=purchase_id%>';
				var supplierName='<%=supplier.getString("sup_name") %>';
				var supplierSid='<%=sid%>';
				var uri = "../lable_template/made_purchase_lable.html?purchase_id="+value+"&supplierName="+supplierName+"&supplierSid="+supplierSid; 
				$.artDialog.open(uri , {title: '采购单标签制作',width:'840px',height:'450px', lock: true,opacity: 0.3,fixed: true});
				//document.print_label_form.submit();
			}
			
			function transfer(purchase_id,money_status)
			{
				$.prompt(
				
				"<div id='title'>转款</div><br />转款类型:<input type='radio' value='all' id='transfertype' name='transfertype'/>全款&nbsp;&nbsp;<input type='radio' value='part' id='transfertype' name='transfertype'/>部分转款<br/>转款备注:<br/><textarea rows='5' cols='60' id='content' name='content'/><br>",
				{
					  submit:promptCheckTransfer,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.transfer_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/purchaseTransfer.action"
										document.transfer_form.purchase_id.value = purchase_id;
										document.transfer_form.followup_content.value = f.content;		
										document.transfer_form.transfer_type.value=f.transfertype;
										document.transfer_form.money_status.value=money_status;
										document.transfer_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function promptCheckTransfer(v,m,f)
			{
				
				if (v=="y")
				{
				
					  if(f.transfertype ==null)
					 {
					 	alert("请选择转款类型");
						return false;
					 }
					 
					 if(f.content == "")
					 {
							alert("请填写适当备注");
							return false;
					 }
					 
					 else
					 {
					 	if(f.content.length>300)
					 	{
					 		alert("内容太多，请简略些");
							return false;
					 	}
					 }
					 
					
					 
					 return true;
				}
			}
			
			function affirmPrice(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>价格确认</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmPricePurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = "价格确定:"+f.content;		
										document.followup_form.submit();	
								}
							}
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function ffirmTransfer(purchase_id)
			{
				$.prompt(
				
				"<div id='title'>可以转款</div><br />备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>",
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/affirmTransferPurchase.action"
										document.followup_form.purchase_id.value = purchase_id;
										document.followup_form.followup_content.value = f.content;		
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
			
			function affirmTransfer(purchase_id)
			{
				//tb_show('资金申请','purchase_apply_funds.html?purchase_id='+purchase_id+'&type=<%=ApplyTypeKey.PURCHASE%>&TB_iframe=true&height=500&width=800',false);
				 var uri = "purchase_apply_funds.html?purchase_id="+purchase_id+"&type=<%=ApplyTypeKey.PURCHASE%>";
				 $.artDialog.open(uri , {title: "[ "+purchase_id+" ]资金申请",width:'900px',height:'560px', lock: true,opacity: 0.3,fixed: true});
			}
			//原来申请资金的，现在不用了
			function submitApplyFundsForm(o){
				if(o){
					var str = "" ;
					for(var obj in  o){
						str += obj +" : "+ o[obj];
					}
					 document.apply_funds_form.remark.value = o["remark"];
					 document.apply_funds_form.amount.value = o["amount"];
					 document.apply_funds_form.supplier_id.value = o["supplier_id"];
					 document.apply_funds_form.payee.value = o["payee"];
					 document.apply_funds_form.payment_information.value = o["payment_information"];
					 document.apply_funds_form.submit();
				}
			}
		            //添加预申请
		    function addApplyMoneyReady(o,accountInfo){
		            	if(o){
							var str = "" ;
							 document.myForm.remark.value = o["remark"];
							 document.myForm.amount.value = o["amount"];
							 document.myForm.supplier_id.value = o["supplier_id"];
							 document.myForm.payee.value = o["payee"];
							 document.myForm.payment_information.value = o["payment_information"];
							 document.myForm.last_time.value = o["last_time"];
							 document.myForm.admin_id.value = o["admin_id"];
							 document.myForm.mail.value = o["mail"];
							 document.myForm.shortMessage.value = o["shortMessage"];
							 document.myForm.pageMessage.value = o["pageMessage"];		
							 if(accountInfo && accountInfo.length > 0 ){
									var form = $(document.myForm);
								 for(var index = 0 , count = accountInfo.length ; index < count ; index++ ){
										var o = accountInfo[index];
							   			for(var strTemp in  o){
											form.append("<input type='hidden' name='"+strTemp+"' value='"+o[strTemp]+"' />")
									   	}
								  }
							 }	
							 document.myForm.submit();
						}
	 	 }
			function uploadInvoice(transport_id)
			{
				$.artDialog.open("transport_upload_invoice.html?transport_id="+transport_id, {title: '转运单上传发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
			}
			function addProductTagTypesFile(){
				 //添加商品标签
				var purchase_id = '<%= purchase_id%>';
				var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/purchase/purchase_tag_type_file.html?purchase_id="+purchase_id ;
				var addUri =  getSelectedIdAndNames();
			 	if($.trim(addUri).length < 1 ){
					showMessage("请先选择商品!","alert");
					return ;
				}else{uri += addUri;}
				$.artDialog.open(uri , {title: "第三方标签上传["+purchase_id+"]",width:'770px',height:'470px', lock: true,opacity: 0.3 ,close:function(){window.location.reload();},fixed: true});
			}
			//申请定金
			function deposit(o , accountInfo){
				if(o){
					var str = "" ;
					 
					 document.depositForm.remark.value = o["remark"];
					 document.depositForm.amount.value = o["amount"];
					 document.depositForm.supplier_id.value = o["supplier_id"];
					 document.depositForm.payee.value = o["payee"];
					 document.depositForm.payment_information.value = o["payment_information"];
					 document.depositForm.last_time.value = o["last_time"];
					 document.depositForm.admin_id.value = o["admin_id"];
					 document.depositForm.mail.value = o["mail"];
					 document.depositForm.shortMessage.value = o["shortMessage"];
					 document.depositForm.pageMessage.value = o["pageMessage"];	
					 document.depositForm.folder.value = o["folder"];	
					 document.depositForm.sn.value = o["sn"];	
					 document.depositForm.file_names.value = o["file_names"];
					 document.depositForm.currency.value = o["currency"];	
					 if(accountInfo){
						 var form = $(document.depositForm);
						 var accountInfoStr  = "" ;
						 for(var index = 0 , count = accountInfo.length ; index < count ; index++ ){
								var o = accountInfo[index];
					   			for(var strTemp in  o){
									form.append("<input type='hidden' name='"+strTemp+"' value='"+o[strTemp]+"' />")
							   	}
						  }
					}
					 document.depositForm.submit();
				}
			}
			//打开 预申请dialog
			function affirmTranReady(purchase_id){
				var uri = "purchase_apply_funds_ready.html?purchase_id="+purchase_id+"&type=<%=ApplyTypeKey.PURCHASE%>";
				 $.artDialog.open(uri , {title: "[ "+purchase_id+" ]预申请",width:'900px',height:'560px', lock: true,opacity: 0.3,fixed: true});
			}
			 //打开预付款dialog
			function openDeposit(purchase_id){
				
				$.ajax({
					url:'<%=ConfigBean.getStringValue("systenFolder") %>action/administrator/purchase/CheckPurchaseTransportApplyPromptAction.action',
					data:'purchase_id='+purchase_id+"&deposit_or_delivery=1",
					dataType:'json',
					type:'post',
					success:function(data){
						if(data && data.result)
						{
							alert(data.result);
						}
						else
						{
							var uri = "purchase_deposit.html?purchase_id="+purchase_id+"&type=<%=ApplyTypeKey.PURCHASE%>";
							$.artDialog.open(uri , {title: "[ "+purchase_id+" ]申请定金",width:'900px',height:'560px', lock: true,opacity: 0.3,fixed: true});
						}
					},
					error:function(){
						showMessage("系统错误","error");
					}
				});
			}
			function getSelectedIdAndNames(){
				s = jQuery("#gridtest").jqGrid('getGridParam','selarrrow')+"";  
			  	 
			 	var array = s.split(",");
				var strIds = "";
		 		var strNames = "";
			 	for(var index = 0 , count = array.length ; index < count ; index++ ){
				   var number = array[index];
			 	   var thisid= $("#gridtest").getCell(number,"product_id");
				    var thisName =$("#gridtest").getCell(number,"p_name"); 
			 	  	strIds += (","+thisid);
			 	  	strNames += (","+thisName);
				}
				if(strIds.length > 1 ){
				    strIds = strIds.substr(1);
				    strNames = strNames.substr(1);
				}
				if(strIds+"" === "false"){return "";}
				return  "&pc_id="+strIds;
			}
			
			getPurchaseSumVWP(<%=purchase_id%>);
			function showMessage(_content,_state)
			{
				var o =  {
					state:_state || "succeed" ,
					content:_content,
					corner: true
				 };
				 var  _self = $("body"),
				_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
				_self.append(_stateBox);	
				if(o.corner){
					_stateBox.addClass("ui-corner-all");
				}
				if(o.state === "succeed"){
					_stateBox.addClass("ui-stateBox-succeed");
					setTimeout(removeBox,1500);
				}else if(o.state === "alert"){
					_stateBox.addClass("ui-stateBox-alert");
					setTimeout(removeBox,2000);
				}else if(o.state === "error"){
					_stateBox.addClass("ui-stateBox-error");
					setTimeout(removeBox,2800);
				}
				_stateBox.fadeIn("fast");
				function removeBox(){
					_stateBox.fadeOut("fast").remove();
			 	}
			}
  </script>
</body>
</html>

