<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%

	String cmd = StringUtil.getString(request,"cmd");
	
	long supplier_id = StringUtil.getLong(request,"supplier_id");
	long ps_id = StringUtil.getLong(request,"ps_id");
	
	int purchase_status = StringUtil.getInt(request,"purchase_status");
	int money_status = StringUtil.getInt(request,"money_status");
	int arrival_time = StringUtil.getInt(request,"arrival_time");
	
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	
	String search_key = StringUtil.getString(request,"search_key");
	
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	
	DBRow[] rows;
	
	if(cmd.equals("fillter"))
	{
		rows = purchaseMgr.getPurchaseFillter(supplier_id,ps_id,purchase_status,money_status,arrival_time,st,en,pc);
	}
	else if(cmd.equals("followup"))
	{
		rows = purchaseMgr.getPurchaseFOLLOWUP(pc,ps_id);
	}
	else if(cmd.equals("search"))
	{
		rows = purchaseMgr.searchPurchaseBySupplierOrId(search_key,pc);
	}
	else
	{
		rows = purchaseMgr.getPurchaseFillter(0,0,0,0,0,st,en,pc);
	}
	
	
	
	PurchaseKey purchasekey = new PurchaseKey();
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
%>



