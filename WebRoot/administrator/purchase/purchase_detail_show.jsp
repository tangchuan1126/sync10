<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%
	String file_name = StringUtil.getString(request,"file_name");
 	
 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	DBRow[] rows = new DBRow[0];
 	try
 	{
 		rows = purchaseMgr.excelshow(file_name,"show");
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	catch(FileException e)
 	{
 		out.print("<script>alert('上传失败，请重试');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	HashMap barcode = new HashMap();
 	
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;">
		  <tr>
			<td width="30%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</td>
			<th width="20%"  class="left-title " style="vertical-align: center;text-align: center;">采购价格</th>
			<th width="15%"  class="right-title " style="vertical-align: center;text-align: center;">备用数量</th>
			<th width="15%"  class="right-title " style="vertical-align: center;text-align: center;">订购数量</th>
			<th width="20%"  class="left-title " style="vertical-align: center;text-align: center;">工厂型号</th>
		  </tr>
			 <%
				if(rows!=null)
				{	
					for(int i=0;i<rows.length;i++)
					{
						String q = rows[i].getString("purchase_name");
						DBRow product = productMgr.getDetailProductByPname(rows[i].getString("purchase_name"));
						boolean colorchange = false;//增加有问题行变色
						if(barcode.containsKey(rows[i].getString("1")))
						{
							submit = false;
							msg = "采购单内有重复商品";
							colorchange = true;
						}
			  %>
				  <tr align="center" valign="middle" 
					  <%if(product==null)
						{
							submit = false;
							msg = "采购单内有未知商品";
							colorchange = true;
						}
						
						if(!rows[i].getString("price").matches("^[0-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|\\d*$"))
						{
							submit = false;
							msg = "采购商品中有价格异常，请检查！";
							colorchange = true;
						}
						
						else if(rows[i].getString("price").equals("")||Double.valueOf(rows[i].getString("price"))==0)
						{
							submit = false;
							msg = "采购商品中有价格为空0或为空的，请检查上传的excel文件";
							colorchange = true;
						}
						
						if(!rows[i].getString("backup_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|\\d*$")) {
							submit = false;
							msg = "备用件中数量异常，请检查！";
							colorchange = true;
						}
						 
						if(!rows[i].getString("order_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|\\d*$"))
						{
							submit = false;
							msg = "采购商品中采购数量异常，请检查！";
							colorchange = true;
						}
						else if(rows[i].getString("order_count").equals(""))
						{
							submit = false;
							msg = "采购商品中有数量为空的，请检查上传的excel文件！";
							colorchange = true;
						}

						if(colorchange)
						{
							out.print(" bgcolor='#FFC1C1'");
						}
					  %>>
					<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("purchase_name") %>&nbsp;</td>
					<td style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("price") %>&nbsp;</td>
					<td style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("backup_count") %>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("order_count")%>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("factory_type")%>&nbsp;</td>
				  </tr>
			  <%
					}
				}
			  %>
	  </table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>
					&nbsp;
				</td>
 				<td align="right" valign="middle" class="">
 					<%
 						if(submit)
 						{
 					%>
<%-- 						<input type="submit" name="Submit2" value="确定" class="normal-green" onClick=""/>--%>
 					<%
 						}
 					%>&nbsp;
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
</table>

</body>
</html>



