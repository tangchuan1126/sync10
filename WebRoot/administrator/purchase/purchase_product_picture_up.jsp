<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportProductFileKey"%>
<%@ include file="../../include.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.cwc.app.key.FileWithTypeKey" %>
<jsp:useBean id="transportProductFileKey" class="com.cwc.app.key.TransportProductFileKey"/> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>商品图片上传</title>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
	<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
	<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
	<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
	<!-- table 斑马线 -->
	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
	<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
	 
  	
  	<%
  		String updateTransportAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/transport/TransportProductFileCompleteAction.action";
		String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";
  		String deleteFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDeleteAction.action";
  		String pc_id = StringUtil.getLong(request,"pc_id");
  		DBRow[] productRows = transportMgrZr.getProductByPcIds(pc_id);
  		long purchase_id = StringUtil.getLong(request,"purchase_id");
  		 
  		//读取配置文件中的transport_product_file
  	 
  		String value = systemConfig.getStringConfigValue("transport_product_file");
  		String flag = StringUtil.getString(request,"flag"); // 文件上传成功过来刷新页面的时候判断是否失败了
  		String file_with_class = StringUtil.getString(request,"file_with_class");
  		String[] arraySelected = value.split("\n");
  		//将arraySelected组成一个List
  		ArrayList<String> selectedList= new ArrayList<String>();
  		for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
  				String tempStr = arraySelected[index];
  				if(tempStr.indexOf("=") != -1){
  					String[] tempArray = tempStr.split("=");
  					String tempHtml = tempArray[1];
  					selectedList.add(tempHtml);
  				}
  		}
  		// 获取所有的关联图片 然后在页面上分类
  		Map<String,List<DBRow>> imageMap = new HashMap<String,List<DBRow>>();
  		DBRow[] imagesRows = transportMgrZr.getAllProductFileByPcId(pc_id,FileWithTypeKey.PURCHASE_PRODUCT_FILE,purchase_id);
  		 
		if(imagesRows != null && imagesRows.length > 0 ){
			for(int index = 0 , count = imagesRows.length ; index < count ; index++ ){
				DBRow tempRow = imagesRows[index];
				String product_file_type = tempRow.getString("product_file_type");
				List<DBRow> tempListRow = imageMap.get(product_file_type);
				if(tempListRow == null || (tempListRow != null && tempListRow.size() < 1)){
					tempListRow = new ArrayList<DBRow>();
				}
				tempListRow.add(tempRow);
				imageMap.put(product_file_type,tempListRow);
		
			}
		}
  	%>
  	<script type="text/javascript">
  		
  		function submitForm(){
  	  		var file = $("#file");
  	  		if(file.val().length < 1){
  	  	  		showMessage("请选择上传文件","alert");
				return ;
  	  	  	}
			var myform = $("#myform");
			myform.submit();
  	  	}
  	  	function fileWithClassTypeChange(){
			var file_with_className = $("#file_with_className");
			var selectedHtml = $("#file_with_class option:selected").html();
			file_with_className.val(selectedHtml);
			 
    	 }
   	 jQuery(function($){
   
   	    if($.trim('<%= flag%>') === "1"){
	   		showMessage("上传文件出错","error");
	   	}
	   	if($.trim('<%= flag%>') === "2"){
	   		showMessage("请选择正确的文件类型","error");
	   	}
	   	
	   	  $("#tabs").tabs({
	   			cache: true,
	  			cookie: { expires: 30000 } 
	   	 });
	   	if('<%= file_with_class%>'.length > 0){
			 $("#tabs").tabs( "select" , '<%= file_with_class%>' * 1-1 );
			 $("#file_with_class option[value='"+'<%= file_with_class%>'+"']").attr("selected",true);
		}
	   	 fileWithClassTypeChange();
   	  })
  	function deleteFile(file_id){
		$.ajax({
			url:'<%= deleteFileAction%>',
			dataType:'json',
			data:{table_name:'product_file',file_id:file_id,folder:'product',pk:'pf_id'},
			success:function(data){
				if(data && data.flag === "success"){
					window.location.reload();
				}else{
					showMessage("系统错误,请稍后重试","error");
				}
			},
			error:function(){
				showMessage("系统错误,请稍后重试","error");
			}
		})
	}
   	function downLoad(fileName , tableName , folder){
   		 window.open('<%= downLoadFileAction%>?file_name=' +fileName + "&folder="+ '<%= systemConfig.getStringConfigValue("file_path_product")%>');
   }
   
    
  	</script>
  	<style type="text/css">
  		.zebraTable td {line-height:25px;height:25px;}
		.right-title{line-height:20px;height:20px;}
  	</style>
  </head>
  
  <body onload = "onLoadInitZebraTable()">

   	<form id="myform" enctype="multipart/form-data" method="post" action='<%=ConfigBean.getStringValue("systenFolder")+"action/administrator/transport/TransportProductPictureUpAction.action" %>'>
   		<input type="hidden" name="pc_id" value="<%=pc_id %>" />
   		 <input type="hidden" name="backurl" id="backurl" value="administrator/purchase/purchase_product_picture_up.html?purchase_id=<%=purchase_id %>&pc_id=<%=pc_id %>"/>
   		<input type="hidden" name="transport_id" value="<%=purchase_id %>"/>
   		<table>
   			<tr>
   			<td style="width:55px;">
   				关联商品:
   			</td>
   			<td colspan="2" style="text-align:left;">
   				 <%
   				 	if(productRows != null && productRows.length > 0){
   				 		 %>
   					<ul class="ul_p_name" style="float:left;">	
   				 		 <% 
   				 		for(DBRow tempRow : productRows){
   				 		 %>
   				 		 	<li> <%=tempRow.getString("p_name") %></li>
   				 		 <% 
   				 		}
  						 %>
  					</ul>
  						 <% 
   				 	}
   				 %>
   			</td>
   		</tr>
   			<tr>
   				<td>文件类型:</td>
   				<td>
   					<select id="file_with_class" name="file_with_class" onchange="fileWithClassTypeChange();">
   					 	<%if(arraySelected != null && arraySelected.length > 0){
 									for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
 									String tempStr = arraySelected[index];
 									String tempValue = "" ;
 									String tempHtml = "" ;
 									 
 									if(tempStr.indexOf("=") != -1){
 										String[] tempArray = tempStr.split("=");
 										tempValue = tempArray[0];
 										tempHtml = tempArray[1];
 									}
 								%>		
 									<option value="<%=tempValue %>"><%=tempHtml %></option>
 
 								<%	} 
 								}
 								%>
   					</select>
   				</td>
   				<td>
   					<input type="hidden" name="file_with_class_name" id="file_with_className"/>
   					<input type="hidden" name="sn" value="P_purchase"/>
		 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURCHASE_PRODUCT_FILE %>" />
		 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("file_path_product")%>" />
   					<input type="file" name="file" id="file"/> <font style="color: red">*文件格式为:jpg,gif,bmp,jpeg,png小于3M</font>
   					 <input type="button" onclick="submitForm();" value="提交"/>
   				</td>
   				 
   			</tr>
   			 
   		</table>
   		 <!-- 显示出所有的商品图片 tabs-->
   		 <div id="tabs" style="margin-top:15px;">
		   		 	<ul>
		   		 		<%
					 		if(selectedList != null && selectedList.size() > 0){
					 			for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
					 			%>
					 				<li><a href="#transport_product_<%=index %>"> <%=selectedList.get(index) %></a></li>
					 			<% 	
					 			}
					 		}
				 		%>
		   		 	</ul>
   		 	<%
   		 		for(int index = 0 , count = selectedList.size() ; index < count ; index++ ){
   		 				List<DBRow> tempListRows = imageMap.get((index+1)+"");
   		 				
   		 			%>
   		 			 <div id="transport_product_<%= index%>">
   		 			 	<table width="98%" border="0" align="center" cellpadding="1" cellspacing="0" isNeed="no" class="zebraTable">
   		 			 			<tr> 
			  						<th width="35%" style="vertical-align: center;text-align: center;" class="right-title">文件名</th>
			  						 <th width="30%" style="vertical-align: center;text-align: center;" class="right-title">商品名称</th>
			  						
			  						<th width="20%" style="vertical-align: center;text-align: center;" class="right-title">操作</th>
			  					</tr>
   		 			
   		 			 	<%
   		 			 		if(tempListRows != null && tempListRows.size() > 0){
   		 			 		 
   		 			 			 for(DBRow row : tempListRows){
   		 			 			%>
   		 			 				<tr>
   		 			 					<td><%= row.getString("file_name") %></td>
   		 			 					<td><%= row.getString("p_name") %></td>
   		 			 					<td>
    		 			 				 
    		 			 					<a href="javascript:deleteFile('<%=row.get("pf_id",0l) %>')">删除</a>  
   		 			 				 	 </td> 
    		 			 				</tr>	
   		 			 			<% 
   		 			 		 }
   		 			 		}else{
   		 			 			%>
 		 			 			<tr>
 									<td colspan="3" style="text-align:center;line-height:80px;height:80px;background:#E6F3C5;border:1px solid silver;">无数据</td>
 								</tr>
   		 			 			<%
   		 			 		}
   		 			 	%>
   		 			 	 	</table>
   		 			 </div>
   		 			<% 
   		 		}
   		 	%>
   		 	
   		 </div>
   		 

   	</form>	
  </body>
  <script type="text/javascript">
//stateBox 信息提示框
  function showMessage(_content,_state){
  	var o =  {
  		state:_state || "succeed" ,
  		content:_content,
  		corner: true
  	 };
   
  	 var  _self = $("body"),
  	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
  	_self.append(_stateBox);	
  	 
  	if(o.corner){
  		_stateBox.addClass("ui-corner-all");
  	}
  	if(o.state === "succeed"){
  		_stateBox.addClass("ui-stateBox-succeed");
  		setTimeout(removeBox,1500);
  	}else if(o.state === "alert"){
  		_stateBox.addClass("ui-stateBox-alert");
  		setTimeout(removeBox,2000);
  	}else if(o.state === "error"){
  		_stateBox.addClass("ui-stateBox-error");
  		setTimeout(removeBox,2800);
  	}
  	_stateBox.fadeIn("fast");
  	function removeBox(){
  		_stateBox.fadeOut("fast").remove();
   }
  }
  </script>
</html>
