<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.CurrencyKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@ include file="../../include.jsp"%> 
<%

DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();

DBRow[] suppliers = supplierMgrTJH.getAllSupplier(null);

DBRow[] productline =  productLineMgrTJH.getAllProductLine();//供应商所在的产品线
String backurl =  ConfigBean.getStringValue("systenFolder") +"administrator/purchase/add_purchase.html" ;

String purchase_id_last = purchaseMgrZyj.getPurchaseLastTimeCreateByAdid(request);

String purchserPersonIds		= "";
String purchserPersonNames		= "";
String invoicePersonIds			= "";
String invoicePersonNames		= "";
String drawbackPersonIds		= "";
String drawbackPersonNames		= "";
String tagPersonIds				= "";
String tagPersonNames			= "";
String qualityPersonIds			= "";
String qualityPersonNames		= "";
String productModelPersonIds	= "";
String productModelPersonNames	= "";
String tagPersonIdsThird 		= "";
String tagPersonNamesThird		= "";

//查询各流程任务的负责人
if(!"".equals(purchase_id_last))
{
	purchserPersonIds		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
	purchserPersonNames		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
	invoicePersonIds		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
	invoicePersonNames		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
	drawbackPersonIds		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
	drawbackPersonNames		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
	tagPersonIds			= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
	tagPersonNames			= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
	qualityPersonIds		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
	qualityPersonNames		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
	productModelPersonIds	= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
	productModelPersonNames	= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
	tagPersonIdsThird		= transportMgrZyj.getSchedulePersonIdsByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
	tagPersonNamesThird		= transportMgrZyj.getSchedulePersonNamesByTransportIdAndType(purchase_id_last, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
}

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>

<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type='text/javascript' src='../js/jquery.form.js'></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

 
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css">

<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
<%--
@import "../js/thickbox/global.css";
--%>
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"/>

<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript">
	$(function(){
		$('#expectArrTime').datepicker({
			dateFormat:"yy-mm-dd",
			changeMonth: true,
			changeYear: true,
		});
		$("#ui-datepicker-div").css("display","none");
	});
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	function removeAfterProIdInput(){$("#address_state_input").remove();}
	
	function setPro_id(deliver_proId, deliver_proInput){
		removeAfterProIdInput();
		var node = $("#ccid_hidden");
		var value = node.val();
	 	$("#pro_id").empty();
		 	  $.ajax({
					url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
					dataType:'json',
					type:'post',
					success:function(data){
						if("" != data){
							var provinces = eval(data);
							$("#pro_id").attr("disabled",false);
							if(deliver_proId == -1){
								$("#pro_id").attr("disabled",false);
								for(var i = 0; i < data.length; i ++){
									$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
								}
								$("#pro_id").append("<option value=-1 selected='selected'>手动输入</option>");
								$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input' value="+deliver_proInput+"/>");
							}else{
								$("#pro_id").append("<option value=0>请选择......</option>");
								for(var i = 0; i < data.length; i ++){
									if(deliver_proId == data[i].pro_id){
										$("#pro_id").append("<option value="+data[i].pro_id+" selected='selected'>"+data[i].pro_name+"</option>");
									}else{
										$("#pro_id").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
									}
								}
								$("#pro_id").append("<option value=-1>手动输入</option>");
							}
							
						}else{
							$("#pro_id").attr("disabled",false);
							$("#pro_id").append("<option value=0>请选择......</option>");
							$("#pro_id").append("<option value=-1>手工输入</option>");
						}
					},
					error:function(){
					}
			  })
	 };
	 function handleProInput(){
	 	var value = $("#pro_id").val();
	 	if(-1 == value){
	 		$("#addBillProSpan").append("<input type='text' name='deliver_pro_input' id='address_state_input'/>");
	 	}else{
	 		removeAfterProIdInput();
	 	}
	 };
	function removeAfterProIdInputSupplier(){$("#supplier_pro_input").remove();}
	function setPro_idSupplier(getCcid,getProId,getProInput){
		removeAfterProIdInputSupplier();
		var node = $("#supplier_native");
		var value = node.val();
		if(getCcid){
			value = getCcid;
		}
	 	$("#supplier_province").empty();
	 	$.ajax({
					url:"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action?ccid="+value,
					dataType:'json',
					type:'post',
					success:function(data){
						if("" != data){
							$("#supplier_province").append("<option value=0>请选择</option>");
							var provinces = eval(data);
							$("#supplier_province").attr("disabled",false);
							for(var i = 0; i < data.length; i ++){
								$("#supplier_province").append("<option value="+data[i].pro_id+">"+data[i].pro_name+"</option>");
							}
							$("#supplier_province").append("<option value=-1>手动输入</option>");
							if(getProId){
								$("#supplier_province option[value="+getProId+"]").attr("selected", true);
							}
						}else{
							$("#supplier_province").attr("disabled",false);
							$("#supplier_province").append("<option value=0 selected='selected'>请选择</option>");
							$("#supplier_province").append("<option value=-1>手工输入</option>");
							var proInputGet = "";
							if(getProInput){
								proInputGet = getProInput;
							}
							if(-1 == $("#supplier_province").val()){
								$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputGet+"'/>");
							}
						}
					},
					error:function(){
					}
				})	
	 }
	function handleProInputSupplier(supplierProInput){
	 	var value = $("#supplier_province").val();
	 	removeAfterProIdInputSupplier();
	 	if(-1 == value){
		 	var proInputSupplier = "";
		 	if(supplierProInput){
		 		proInputSupplier = supplierProInput;
		 	}
	 		$("#addBillProSpanSupplier").append("<input type='text' name='supplier_pro_input' id='supplier_pro_input' value='"+proInputSupplier+"'/>");
	 	}else{
	 		removeAfterProIdInputSupplier();
	 	}
	 };
	 function changeSupplierAddress(){
		$("#supplier_house_number").val("");
		$("#supplier_street").val("");
		$("#supplier_city").val("");
		$("#supplier_zip_code").val("");
		$("#supplier_native").val("");
		$("#supplier_link_man").val("");
		$("#supplier_link_phone").val("");
		removeAfterProIdInputSupplier();
		setPro_idSupplier();
		handleProInputSupplier();
			var supplierId = $("#supplier").val();
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/supplier/GetSupplierInfoAjax.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:"supplier_id="+supplierId,
				error: function(e){
					alert("数据读取出错！");
				},
				success: function(data){
					if(null != data){
						$("#supplier_house_number").val(data.address_houme_number);
						$("#supplier_street").val(data.address_street);
						$("#supplier_city").val(data.city_input);
						$("#supplier_zip_code").val(data.zip_code);
						$("#supplier_native option[value="+data.nation_id+"]").attr("selected",true);
						$("#supplier_link_man").val(data.linkman);
						$("#supplier_link_phone").val(data.phone);
						removeAfterProIdInputSupplier();
						setPro_idSupplier(data.nation_id,data.province_id);
						handleProInputSupplier(data.supplier_pro_input);
					}
				}
			});
		}
	function selectStorage(){
		var para = "ps_id="+$("#ps_id").val();
		$("#deliver_house_number").val("");
		$("#deliver_street").val("");
		$("#deliver_zip_code").val("");
		$("#deliver_city").val("");
		$("#ccid_hidden").val("");
		$("#ccid_hidden").change();
		$("#delivery_linkman").val("");
		$("#linkman_phone").val("");
		
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_catalog/GetStorageCatalogDetailDeliver.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
					
				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data){
					//$("#address").val(data.deliver_address);
					$("#deliver_house_number").val(data.deliver_house_number);
					$("#deliver_street").val(data.deliver_street);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.deliver_city);
					$("#delivery_linkman").val(data.deliver_contact);
					$("#linkman_phone").val(data.deliver_phone);
					var ccid_selected = "#ccid_hidden option[value="+data.deliver_nation+"]";
					$(ccid_selected).attr("selected", "selected");
					setPro_id(data.deliver_pro_id,data.deliver_pro_input);
				}
			});
	}
	
	function getSupplierByProductline(productlineid){
		$("#supplier_house_number").val("");
		$("#supplier_street").val("");
		$("#supplier_city").val("");
		$("#supplier_zip_code").val("");
		$("#supplier_native").val("");
		$("#supplier_link_man").val("");
		$("#supplier_link_phone").val("");
		removeAfterProIdInputSupplier();
		setPro_idSupplier();
		handleProInputSupplier();
		var para = "productline_id="+productlineid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#supplier").clearAll();
					$("#supplier").addOption("请选择...","");
					if(data != ""){
						$.each(data,function(i){
							$("#supplier").addOption(data[i].sup_name,data[i].id);
						});
					}
				}
			});
	}
	function closeAndRefreshParent(){
		$.artDialog && $.artDialog.close();
		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
	}
	
	$(function(){
	$(".cssDivschedule").css("display","none");
	$("input:radio[name=hasInvoice]").click(
			function(){
				var hasInvoinceCheck = $(this).val();
				if(2 == hasInvoinceCheck){
					$("#invoiceDetailTr").attr("style","");
					$("#invoicePersonTr").attr("style","");
				}else{
					$("#invoiceDetailTr").attr("style","display:none");
					$("#invoicePersonTr").attr("style","display:none");
				}
			}
	);
	$("input:radio[name=isDrawback]").click(function(){
		var isDrawbackCheck = $(this).val();
		$("#rebate_rate_title") && $("#rebate_rate_title").remove();
		$("#rebate_rate") && $("#rebate_rate").remove();
		$("#dot2") && $("#dot2").remove();
		if(2 == isDrawbackCheck){
			$("#drawbackPersonTr").attr("style","");
			$("#drawBackTd").append($('<span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" style="width:50px;"/><span id="dot2">%</span>'));
		}else{
			$("#drawbackPersonTr").attr("style","display:none");
			$("#rebate_rate_title") && $("#rebate_rate_title").remove();
			$("#rebate_rate") && $("#rebate_rate").remove();
			$("#dot2") && $("#dot2").remove();
		}
	});
	$("input:radio[name=needTagThird]").click(function(){
		var isNeedTagCheck = $(this).val();
		if(2 == isNeedTagCheck){
			$("#tagThirdPersonTr").attr("style","");
		}else{
			$("#tagThirdPersonTr").attr("style","display:none");
		}
	});
});

function adminUserPurchaser(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsPurchaser").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowPurchaser'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowPurchaser(user_ids , user_names){
	$("#adminUserIdsPurchaser").val(user_ids);
	$("#adminUserNamesPurchaser").val(user_names);
}
function setParentUserShow(ids,names,  methodName){eval(methodName)(ids,names);}
//function adminUserDispatcher(){
//	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
// 	 var option = {
// 			 single_check:0, 					// 1表示的 单选
//			 user_ids:$("#adminUserIdsDispatcher").val(), //需要回显的UserId
// 			 not_check_user:"",					//某些人不 会被选中的
// 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
// 			 ps_id:'0',						//所属仓库
// 			 handle_method:'setParentUserShowDispatcher'
// 	 };
// 	 uri  = uri+"?"+jQuery.param(option);
// 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
//}
//function setParentUserShowDispatcher(user_ids , user_names){
//	$("#adminUserIdsDispatcher").val(user_ids);
//	$("#adminUserNamesDispatcher").val(user_names);
//}
function adminUserInvoice(){
	 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
 	 var option = {
 			 single_check:0, 					// 1表示的 单选
 			 user_ids:$("#adminUserIdsInvoice").val(), //需要回显的UserId
 			 not_check_user:"",					//某些人不 会被选中的
 			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
 			 ps_id:'0',						//所属仓库
 			 handle_method:'setParentUserShowInvoice'
 	 };
 	 uri  = uri+"?"+jQuery.param(option);
 	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowInvoice(user_ids , user_names){
	$("#adminUserIdsInvoice").val(user_ids);
	$("#adminUserNamesInvoice").val(user_names);
}
function adminUserDrawback(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsDrawback").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowDrawback'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowDrawback(user_ids , user_names){
	$("#adminUserIdsDrawback").val(user_ids);
	$("#adminUserNamesDrawback").val(user_names);
}
function adminUserTag(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsTag").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowTag'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowTag(user_ids , user_names){
	$("#adminUserIdsTag").val(user_ids);
	$("#adminUserNamesTag").val(user_names);
}
function adminUserTagThird(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#tagPersonIdsThird").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowTagThird'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowTagThird(user_ids , user_names){
	$("#adminUserIdsTagThird").val(user_ids);
	$("#adminUserNamesTagThird").val(user_names);
}
function adminUserQuality(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsQuality").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowQuality'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowQuality(user_ids , user_names){
	$("#adminUserIdsQuality").val(user_ids);
	$("#adminUserNamesQuality").val(user_names);
}
function adminUserProductModel(){
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/admin/admin_user.html"; 
	 var option = {
			 single_check:0, 					// 1表示的 单选
			 user_ids:$("#adminUserIdsProductModel").val(), //需要回显的UserId
			 not_check_user:"",					//某些人不 会被选中的
			 proJsId:"-1",						// -1全 部,1普通员工,5副主管,10主管
			 ps_id:'0',						//所属仓库
			 handle_method:'setParentUserShowProductModel'
	 };
	 uri  = uri+"?"+jQuery.param(option);
	 $.artDialog.open(uri , {title: '人员列表',width:'700px',height:'530px', lock: true,opacity: 0.3,fixed: true});
}
function setParentUserShowProductModel(user_ids , user_names){
	$("#adminUserIdsProductModel").val(user_ids);
	$("#adminUserNamesProductModel").val(user_names);
}
function previousStep(){
	var selected = $('#tabs').tabs('option', 'selected');
			$("#tabs").tabs("select", selected-1);	
}
function nextStep(){
	var selected = $('#tabs').tabs('option', 'selected');
			$("#tabs").tabs("select", selected+1);	
}
function checkPurchaseSupplier(){
	if($("#product_line_id").val() == 0){
		alert("请选择产品线(供应商及提货地址)");
		return false;
	}
	else if($("#supplier").val() == 0){
		alert("请填写供应商(供应商及提货地址)");
		return false;
	}
	else
	{
		return true;
	}
}
function checkPurchaseReceiveAddress(){
	if($("#ps_id").val()==0){
		alert("请选择收货仓库(收货地址)");
		return false;
	}else{
		return true;
	}
}
function checkPurchaseProdures(){
	if("" == $("#adminUserNamesPurchaser").val())
	{
		alert("采购负责人不能为空(流程指派)");
		return false;
	}
	//else if("" == $("#adminUserNamesDispatcher").val())
	//{
	//	alert("调度负责人不能为空(流程指派)");
	//	return false;
	//}
	else if("" == $("#adminUserNamesTag").val())
	{
		alert("内部标签负责人不能为空(流程指派)");
		return false;
	}
	else if("" == $("#adminUserNamesQuality").val())
	{
		alert("质检要求流程负责人不能为空(流程指派)");
		return false;
	}
	else if("" == $("#adminUserNamesProductModel").val())
	{
		alert("商品范例负责人不能为空(流程指派)");
		return false;
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" == $("#adminUserNamesInvoice").val())
	{
		alert("发票流程负责人不能为空(流程指派)");
		return false;
	}
	else if(2 == $("input:radio[name=isDrawback]:checked").val() && "" == $("#adminUserNamesDrawback").val())
	{
		alert("退税流程负责人不能为空(流程指派)");
		return false;
	}
	else if(2 == $("input:radio[name=needTagThird]:checked").val() && "" == $("#adminUserNamesTagThird").val())
	{
		alert("第三方标签负责人不能为空(流程指派)");
		return false;
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && "" != $("#invoince_amount").val() && 0 == $("#invoince_currency").val())
	{
		alert("请选择币种(流程指派)");
		return false;
	}
	else if(2 == $("input:radio[name=hasInvoice]:checked").val() && 0 != $("#invoince_currency").val() && "" == $("#invoince_amount").val())
	{
		alert("请填写开票金额(流程指派)");
		return false;
	}
	else if($("#invoince_amount") && $("#invoince_amount").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_amount").val()))
	{
		alert("开票金额，请填写数值(流程指派)");
		return false;
	}
	else if($("#invoince_dot") && $("#invoince_dot").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#invoince_dot").val()))
	{
		alert("票点，请填写数值(流程指派)");
		return false;
	}
	else if($("#rebate_rate") && $("#rebate_rate").val() && !/^[0-9]+([.]{1}[0-9]{1,2})?$/.test($("#rebate_rate").val()))
	{
		alert("退税率，请填写数值(流程指派)");
		return false;
	}else{
		return true;
	}
}
function checkFormAndSubmit(){
	if(checkPurchaseSupplier() && checkPurchaseReceiveAddress() && checkPurchaseProdures()){
		$(".cssDivschedule").css("display","block");
		//处理采购单地址信息
		document.purchaseAllInfoForm.deliver_native.value = $("#ccid_hidden").val();
		document.purchaseAllInfoForm.deliver_provice.value = $("#pro_id").val();	
		//处理采购单各流程所涉及的信息
		document.purchaseAllInfoForm.invoice.value = $("input:radio[name=hasInvoice]:checked").val();
		document.purchaseAllInfoForm.drawback.value = $("input:radio[name=isDrawback]:checked").val();
		document.purchaseAllInfoForm.need_tag_third.value = $("input:radio[name=needTagThird]:checked").val();
		if($("input:checkbox[name=isMailInvoice]").attr("checked")){
			document.purchaseAllInfoForm.needMailInvoice.value = 2;
		}
		if($("input:checkbox[name=isMessageInvoice]").attr("checked")){
			document.purchaseAllInfoForm.needMessageInvoice.value = 2;
		}
		if($("input:checkbox[name=isPageInvoice]").attr("checked")){
			document.purchaseAllInfoForm.needPageInvoice.value = 2;
		}
		if($("input:checkbox[name=isMailDrawback]").attr("checked")){
			document.purchaseAllInfoForm.needMailDrawback.value = 2;
		}
		if($("input:checkbox[name=isMessageDrawback]").attr("checked")){
			document.purchaseAllInfoForm.needMessageDrawback.value = 2;
		}
		if($("input:checkbox[name=isPageDrawback]").attr("checked")){
			document.purchaseAllInfoForm.needPageDrawback.value = 2;
		}
		if($("input:checkbox[name=isMailTag]").attr("checked")){
			document.purchaseAllInfoForm.needMailTag.value = 2;
		}
		if($("input:checkbox[name=isMessageTag]").attr("checked")){
			document.purchaseAllInfoForm.needMessageTag.value = 2;
		}
		if($("input:checkbox[name=isPageTag]").attr("checked")){
			document.purchaseAllInfoForm.needPageTag.value = 2;
		}
		if($("input:checkbox[name=isMailTagThird]").attr("checked")){
			document.purchaseAllInfoForm.needMailTagThird.value = 2;
		}
		if($("input:checkbox[name=isMessageTagThird]").attr("checked")){
			document.purchaseAllInfoForm.needMessageTagThird.value = 2;
		}
		if($("input:checkbox[name=isPageTagThird]").attr("checked")){
			document.purchaseAllInfoForm.needPageTagThird.value = 2;
		}
		if($("input:checkbox[name=isMailQuality]").attr("checked")){
			document.purchaseAllInfoForm.needMailQuality.value = 2;
		}
		if($("input:checkbox[name=isMessageQuality]").attr("checked")){
			document.purchaseAllInfoForm.needMessageQuality.value = 2;
		}
		if($("input:checkbox[name=isPageQuality]").attr("checked")){
			document.purchaseAllInfoForm.needPageQuality.value = 2;
		}
		if($("input:checkbox[name=isMailProductModel]").attr("checked")){
			document.purchaseAllInfoForm.needMailProductModel.value = 2;
		}
		if($("input:checkbox[name=isMessageProductModel]").attr("checked")){
			document.purchaseAllInfoForm.needMessageProductModel.value = 2;
		}
		if($("input:checkbox[name=isPageProductModel]").attr("checked")){
			document.purchaseAllInfoForm.needPageProductModel.value = 2;
		}
		$("#savePurchaseAllInfoButtion").attr("disabled", true);
		document.purchaseAllInfoForm.submit();
	}
}
function windowClose(purchase_id){
	$.artDialog && $.artDialog.close();
	//$.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
	$.artDialog.opener.refreshWindowAndPurchaseDetail  && $.artDialog.opener.refreshWindowAndPurchaseDetail(purchase_id);
};
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input",targetNode).val();
    var obj  = {
	     reg:"picture_office",
	     limitSize:2,
	     target:_target
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//采购单详细的上传
function uploadFilePurchaseDetail(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input",targetNode).val();
    var obj  = {
	     reg:"xls",
	     limitSize:2,
	     target:_target,
	     limitNum:1
	 };
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
	var targetNode = $("#"+target);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			lis += "<li><a href='###'>"+array[index]+"</a></li>"
		}
		var ulNode = $("ul",targetNode);
		ulNode.html(lis);
		$("input",targetNode).val(fileNames);
	}
	if(target === "jquery_file_up"){
	    if(fileNames.length > 0 ){
			$("#file_up_tr").attr("style","");
		}else{
			$("#file_up_tr").css('display','none');
		}
	}else{
		if(fileNames && fileNames.length > 0)
		{
			loadUplaodFile(fileNames);
		}
	}
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

#purchaseTable tr td{
	line-height:30px;
	height:30px;
}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
.set {
    border: 2px solid #999999;
    font-weight: bold;
    line-height: 18px;
    margin-bottom: 10px;
    margin-top: 5px;
    padding: 2px;
    width: 90%;
    word-break: break-all;
}
div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:9999999;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
}
ul.fileUL{list-style-type:none;}
ul.fileUL li{line-height:20px;height:20px;padding-left:3px;padding-right:3px;margin-left:2px;float:left;margin-top:1px;border:1px solid silver;}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<div class="cssDivschedule" style=""></div>
<input type="hidden" id="currentSelectedTab" value="0"/>
	<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/PurchaseAllInfoAddAction.action" name="purchaseAllInfoForm" id="purchaseAllInfoForm" method="post">
	<!-- 采购单基本信息的地址 -->
<input type="hidden" name="deliver_provice"/>
<input type="hidden" name="deliver_native"/>
<!-- 各流程 -->
<input type="hidden" name="invoice"/>
<input type="hidden" name="drawback"/>
<input type="hidden" name="need_tag_third"/>
<input type="hidden" name="needMailInvoice"/>
<input type="hidden" name="needMessageInvoice"/>
<input type="hidden" name="needPageInvoice"/>
<input type="hidden" name="needMailDrawback"/>
<input type="hidden" name="needMessageDrawback"/>
<input type="hidden"name="needPageDrawback"/>
<input type="hidden" name="needMailTag"/>
<input type="hidden" name="needMessageTag"/>
<input type="hidden" name="needPageTag"/>
<input type="hidden" name="needMailTagThird"/>
<input type="hidden" name="needMessageTagThird"/>
<input type="hidden" name="needPageTagThird"/>
<input type="hidden" name="needMailQuality"/>
<input type="hidden" name="needMessageQuality"/>
<input type="hidden" name="needPageQuality"/>
<input type="hidden" name="needMailProductModel"/>
<input type="hidden" name="needMessageProductModel"/>
<input type="hidden" name="needPageProductModel"/>
<!-- 相关人员的ID -->
<input type="hidden" id="adminUserIdsPurchaser" name="adminUserIdsPurchaser" value='<%=purchserPersonIds %>'/>
<%--<input type="hidden" id="adminUserIdsDispatcher" name="adminUserIdsDispatcher"/>--%>
<input type="hidden" id="adminUserIdsInvoice" name="adminUserIdsInvoice" value='<%=invoicePersonIds %>'/>
<input type="hidden" id="adminUserIdsDrawback" name="adminUserIdsDrawback" value='<%=drawbackPersonIds %>'/>
<input type="hidden" id="adminUserIdsTag" name="adminUserIdsTag" value='<%=tagPersonIds %>'/>
<input type="hidden" id="adminUserIdsQuality" name="adminUserIdsQuality" value='<%=qualityPersonIds %>'/>
<input type="hidden" id="adminUserIdsProductModel" name="adminUserIdsProductModel" value='<%=productModelPersonIds %>'/>
<input type="hidden" id="adminUserIdsTagThird" name="adminUserIdsTagThird" value='<%=tagPersonIdsThird %>'/>

<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html"/>
<table width="100%" height="100%">
	<tr width="100%" height="90%" valign="top">
		<td width="100%" height="90%" valign="top">
			<div class="demo" align="center" style="height: 100%">
	  		 <div id="tabs" style="width: 98%;height: 100%">
	  	    <ul>
	  	      <li><a href="#supplier_and_send" >供应商及提货地址</a></li>
	  	      <li><a href="#receive">收货地址</a></li>
			  <li><a href="#terms">各项条款及质检要求</a></li>
			  <li><a href="#procedures">流程指派</a></li>
			  <li><a href="#details">采购详细</a></li>
			</ul>
			<div id="supplier_and_send">
			  		<table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;" id="purchaseTable">
						<tr>
							<td width="10%"  align="right" style="padding:0px;">产品线:</td>
			              	<td width="46%" height="35" align="left" valign="middle" nowrap="nowrap">
						      	<select style="width:150px" name="product_line_id" id="product_line_id" onChange="getSupplierByProductline(this.value)">
						      	<option value="0">请选择...</option>
						      	<%
						      		for(int i=0;i<productline.length;i++){
						      	%>
						      		<option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
						      	<%
						      		}
						      	%>
						      </select>
					  		</td>
						</tr>
						<tr valign="bottom">
							<td width="10%"  align="right" style="padding:0px;">供应商:</td>
			      			<td width="90%" height="20%"  align="left" style="padding:0px;">
						      	<select name='supplier' id='supplier' onchange="changeSupplierAddress();">
						      		<option value="0">请选择供应商...</option>
						      	</select>
						     </td>
						</tr>
						<tr height="29">
					           <td width="10%"  align="right" valign="middle" nowrap="nowrap">门牌号:</td>
					           <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_house_number" id="supplier_house_number" style="width:300">
					          </td>
					    </tr>
					    <tr height="29">
					       <td width="10%"  align="right" valign="middle" nowrap="nowrap">街道:</td>
					       <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					       		<input type="text" name="supplier_street" id="supplier_street" style="width:300">
					       </td>
					     </tr>
					     <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">城市:</td>
					          <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_city" id="supplier_city" style="width:300">
					          </td>
					     </tr>
					     <tr height="29">
					         <td width="10%"  align="right" valign="middle" nowrap="nowrap">邮编:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_zip_code" id="supplier_zip_code" style="width:300">
					         </td>
					     </tr>
					     <tr>
							 <td width="10%"  align="right" valign="middle" nowrap="nowrap">省份:</td>
							 <td width="90%"  align="left" valign="middle" nowrap="nowrap">
									<span id="addBillProSpanSupplier">
										<select disabled="disabled" name="supplier_province" id="supplier_province" onchange="handleProInputSupplier()" style="margin-right:5px;"></select>
									</span>
							 </td>
						</tr>
						<tr>
							 <td width="10%"  align="right" valign="middle" nowrap="nowrap">国家:</td>
							 <td width="90%"  align="left" valign="middle" nowrap="nowrap">
								    <%
								   		DBRow countrycode[] = orderMgr.getAllCountryCode();
										String selectBgSupplier="#ffffff";
										String preLetterSupplier="10990";
									%>
							      	<select  id="supplier_native"  name="supplier_native" onChange="removeAfterProIdInputSupplier();setPro_idSupplier();">
								 	 	<option value="0">请选择...</option>
										  <%
										  for (int i=0; i<countrycode.length; i++){
										  	if (!preLetterSupplier.equals(countrycode[i].getString("c_country").substring(0,1))){
												if (selectBgSupplier.equals("#eeeeee")){
													selectBgSupplier = "#ffffff";
												}else{
													selectBgSupplier = "#eeeeee";
												}
											}  	
										  	preLetterSupplier = countrycode[i].getString("c_country").substring(0,1);
										  %>
								  		  <option style="background:<%=selectBgSupplier%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
										  <%
										  }
										%>
							   		 </select>
					   		 </td>
						 	</tr>
						 	<tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系人:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_link_man" id="supplier_link_man">
					          </td>
					        </tr>
					        <tr height="29">
					          <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系电话:</td>
					         <td width="90%"  align="left" valign="middle" nowrap="nowrap">
					          	<input type="text" name="supplier_link_phone" id="supplier_link_phone">
					          </td>
					        </tr>
					        <tr height="20px"><td colspan="2"></td></tr>
					        <tr>
					        	<td colspan="2" align="center">
									<input type="button" name="Submit2" value="下一步" class="normal-green" onClick="nextStep();">
					        	</td>
					        </tr>
					</table>
			  </div>
			  <div id="receive">
					<table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;" id="purchaseTable">
									<tr>
									  <td width="10%"  align="right" valign="middle" nowrap="nowrap">收货仓库:</td>
					      			  <td width="90%"  align="left" valign="middle" nowrap="nowrap">
									      <select name='ps_id' id='ps_id' onChange="selectStorage()">
			                                <option value="0">请选择...</option>
			                                <%
												  for ( int i=0; i<treeRows.length; i++ )
												  {
												%>
			                                <option value='<%=treeRows[i].getString("id")%>'><%=treeRows[i].getString("title")%></option>
			                                <%
												}
												%>
			                              </select>
		                              </td>
									</tr>
								  <tr>
					  	 			  <td width="10%"  align="right" valign="middle" nowrap="nowrap">门牌号:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="deliver_house_number" type="text" id="deliver_house_number" style="width:400px;"/></td>
								  </tr>
								  <tr>
					  	 			  <td width="10%"  align="right" valign="middle" nowrap="nowrap">街道:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="deliver_street" type="text" id="deliver_street" style="width:400px;"/></td>
								  </tr>
								  <tr>
					  			      <td width="10%"  align="right" valign="middle" nowrap="nowrap">城市:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="deliver_city" type="text" id="deliver_city" style="width:400px;"/></td>
								  </tr>
								  <tr>
					  				  <td width="10%"  align="right" valign="middle" nowrap="nowrap">邮编:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="deliver_zip_code" type="text" id="deliver_zip_code" style="width:400px;"/></td>
								  </tr>
								  <tr>
					  				  <td width="10%"  align="right" valign="middle" nowrap="nowrap">省份:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
								      	<span id="addBillProSpan">
											<select disabled="disabled" id="pro_id" name="pro_id" onchange="handleProInput()" style="margin-right:5px;"></select>
										</span>
								      </td>
								  </tr>
								  <tr>
					  				  <td width="10%"  align="right" valign="middle" nowrap="nowrap">国家:</td>
								      <td width="90%"  align="left" valign="middle" nowrap="nowrap">
											<%
												String selectBg="#ffffff";
												String preLetter="";
											%>
									      	 <select  id="ccid_hidden"  name="ccid" onChange="removeAfterProIdInput();setPro_id();" >
										 	 	<option value="0">请选择...</option>
												  <%
												  for (int i=0; i<countrycode.length; i++){
												  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1))){
														if (selectBg.equals("#eeeeee")){
															selectBg = "#ffffff";
														}else{
															selectBg = "#eeeeee";
														}
													}  	
													preLetter = countrycode[i].getString("c_country").substring(0,1);
												  %>
										  		  <option style="background:<%=selectBg%>;"  code="<%=countrycode[i].getString("c_code") %>"  value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
												  <%
												  }
												%>
								   			 </select>
									  </td>
								  </tr>
								  <tr>
					  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系人:</td>
					      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input type="text" name="delivery_linkman" id="delivery_linkman"/></td>
								  </tr>
								  <tr>
					  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">联系电话:</td>
					      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input type="text" name="linkman_phone" id="linkman_phone" width="200px"/></td>
								  </tr>
					  <tr>
					  	 <td width="10%"  align="right" valign="middle" nowrap="nowrap">预计到货时间:</td>
					      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input type="text" name="expectArrTime" id="expectArrTime" width="200px"/></td>
					  </tr>
					  <tr height="20px"><td colspan="2"></td></tr>
					  <tr>
				        	<td colspan="2" align="center">
				        		<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">							
								<input type="button" name="Submit2" value="下一步" class="normal-green" onClick="nextStep();">
				        	</td>
				       </tr>
					</table>
				</div>
				<div id="terms">
					<table align="left" width="100%">
						<tr width="100%">
							<td width="15%" align="right">
								上传质检要求:
							</td>
							<td width="85%" align="left">
			   					<input type="hidden" name="file_with_class_name" id="file_with_className"/>
			   					<input type="hidden" name="sn" value="P_quality"/>
					 			<input type="hidden" name="file_with_type" value="<%= FileWithTypeKey.PURHCASE_QUALITY_INSPECTION %>" />
					 			<input type="hidden" name="path" value="<%= systemConfig.getStringConfigValue("purchase_upload_file_dir")%>" />
			   					<input type="button"  class="long-button" onclick="uploadFile('jquery_file_up');" value="选择文件" />
			   				</td>
						</tr>
						<tr id="file_up_tr" style='display:none;' width="100%">
				 		 	<td width="15%" align="right">待上传凭证:</td>
				 		 	<td width="85%" align="left">
				              	<div id="jquery_file_up">	
				              		<input type="hidden" name="file_names" value=""/>
				              		<ul class="fileUL"> 
				              		</ul>
				              	</div>
							</td>
						</tr>
					</table>
					<table style="width: 100%">
						<tr height="29">
					       <td align="right" width="15%">返修条款:</td>
					       <td align="left" valign="middle" width="85%">
					       	<textarea rows="2" cols="67" name="repair_terms_purchase" id="repair_terms_purchase"></textarea>
					       </td>
					     </tr>
					     <tr height="29">
					       <td align="right" width="15%">贴标条款:</td>
					       <td align="left" valign="middle" width="85%">
					       	<textarea rows="2" cols="67" name="labeling_terms_purchase" id="labeling_terms_purchase"></textarea>
					       </td>
					     </tr>
					     <tr height="29">
					       <td align="right" width="15%">交货条款:</td>
					       <td align="left" valign="middle" width="85%">
					       	<textarea rows="2" cols="67" name="delivery_product_terms_purchase" id="delivery_product_terms_purchase"></textarea>
					       </td>
					     </tr>
					     <tr height="29">
					       <td align="right" width="15%">其他条款:</td>
					       <td align="left" valign="middle" width="85%">
					       	<textarea rows="2" cols="67" name="purchase_terms_purchase" id="purchase_terms_purchase"></textarea>
					       </td>
					     </tr>
					     <tr height="20px"><td colspan="2"></td></tr>
					     <tr>
					        	<td colspan="2" align="center">
					        		<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">							
									<input type="button" name="Submit2" value="下一步" class="normal-green" onClick="nextStep();">
					        	</td>
					     </tr>
				    </table>
				</div>
				<div id="procedures">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr height="25px;">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">采购员：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						    	<input type="text" id="adminUserNamesPurchaser" name="adminUserNamesPurchaser" style="width:180px;" onclick="adminUserPurchaser()" value='<%=purchserPersonNames %>'/>
						    </td>
						</tr>
<%--						<tr height="25px;">--%>
<%--							 <td width="13%"  align="right" valign="middle" nowrap="nowrap">调度：</td>--%>
<%--						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">--%>
<%--						   		<input type="text" id="adminUserNamesDispatcher" name="adminUserNamesDispatcher" style="width:180px;" onclick="adminUserDispatcher()"/>--%>
<%--						    </td>--%>
<%--						</tr>--%>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">商品范例：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						  		 <input type="text" id="adminUserNamesProductModel" name="adminUserNamesProductModel" style="width:180px;" onclick="adminUserProductModel()" value='<%=productModelPersonNames %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailProductModel"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageProductModel"/>短信
						   		<input type="checkbox" checked="checked" name="isPageProductModel"/>页面
						    </td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							 <td width="13%"  align="right" valign="middle" nowrap="nowrap">内部标签：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						   	<input type="text" id="adminUserNamesTag" name="adminUserNamesTag" style="width:180px;" onclick="adminUserTag()" value='<%=tagPersonNames %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailTag"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageTag"/>短信
						   		<input type="checkbox" checked="checked" name="isPageTag"/>页面
						    </td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">质检要求：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						   		<input type="text" id="adminUserNamesQuality" name="adminUserNamesQuality" style="width:180px;" onclick="adminUserQuality()" value='<%=qualityPersonNames %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailQuality"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageQuality"/>短信
						   		<input type="checkbox" checked="checked" name="isPageQuality"/>页面
						    </td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">发票：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						    <input type="radio" name="hasInvoice" value="2" checked="checked"/>需要
						    <input type="radio" name="hasInvoice" value="1"/>不需要
						    </td>
						</tr>
						<tr id="invoiceDetailTr" style="" height="25px;">
							<td width="13%" align="right">
								开票金额：
							</td>
							<td width="87%">
								<input id="invoince_amount" name="invoince_amount" style="width:50px;"/>
						    	&nbsp;币种：
						    	<select id="invoince_currency" name="invoince_currency">
						    	<option value="0">请选择</option>
						    	<% 
						    		CurrencyKey currencyKey	= new CurrencyKey();
						    		List currencyList		= currencyKey.CurrencyKeyList();
						    		for(int i = 0; i < currencyList.size(); i ++){
					  			%>		
						    			<option value="<%=currencyList.get(i) %>"><%=currencyKey.getCurrencyNameById((String)currencyList.get(i)) %></option>
						    	<%		
						     		}
						    	%>
						  	  </select>
						  	  &nbsp;票点：<input id="invoince_dot" name="invoince_dot" style="width:50px;"/><span id="dot1">%</span>
							</td>
						</tr>
						<tr height="25px;" id="invoicePersonTr" style="">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						    	<input type="text" id="adminUserNamesInvoice" name="adminUserNamesInvoice" style="width:180px;" onclick="adminUserInvoice()" value='<%=invoicePersonNames %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailInvoice"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageInvoice"/>短信
						   		<input type="checkbox" checked="checked" name="isPageInvoice"/>页面
							</td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							 <td width="13%"  align="right" valign="middle" nowrap="nowrap">退税：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap" id="drawBackTd">
						   	<input type="radio" name="isDrawback" value="2" checked="checked"/>需要
						   	<input type="radio" name="isDrawback" value="1"/>不需要
						   	 <span id="rebate_rate_title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退税率：</span><input id="rebate_rate" name="rebate_rate" style="width:50px;"/><span id="dot2">%</span>
						    </td>
						</tr>
						<tr height="25px;" style="" id="drawbackPersonTr">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						    	<input type="text" id="adminUserNamesDrawback" name="adminUserNamesDrawback" style="width:180px;" onclick="adminUserDrawback()" value='<%=drawbackPersonNames %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailDrawback"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageDrawback"/>短信
						   		<input type="checkbox" checked="checked" name="isPageDrawback"/>页面
							</td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="25px;">
							 <td width="13%"  align="right" valign="middle" nowrap="nowrap">第三方标签：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						   	<input type="radio" name="needTagThird" value="2" checked="checked"/>需要
						   	<input type="radio" name="needTagThird" value="1"/>不需要
						    </td>
						</tr>
						<tr height="25px;" style="" id="tagThirdPersonTr">
							<td width="13%"  align="right" valign="middle" nowrap="nowrap">负责人：</td>
						    <td width="87%"  align="left" valign="middle" nowrap="nowrap">
						    	<input type="text" id="adminUserNamesTagThird" name="adminUserNamesTagThird" style="width:180px;" onclick="adminUserTagThird()" value='<%=tagPersonNamesThird %>'/>
						    	通知：
						   		<input type="checkbox" checked="checked" name="isMailTagThird"/>邮件
						   		<input type="checkbox" checked="checked" name="isMessageTagThird"/>短信
						   		<input type="checkbox" checked="checked" name="isPageTagThird"/>页面
							</td>
						</tr>
						<tr height="15px;" align="center" valign="middle"><td colspan="2"><hr width="96%" color="silver"/></td></tr>
						<tr height="20px"><td colspan="2"></td></tr>
						<tr>
					        	<td colspan="2" align="center">
					        		<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">							
									<input type="button" name="Submit2" value="下一步" class="normal-green" onClick="nextStep();">
					        	</td>
					    </tr>
				</table>
				</div>
				<div id="details">
					<span style="text-align: left;float: left;height: 50px;">
						上传采购单详细:
				   		<input type="button"  class="long-button" onclick="uploadFilePurchaseDetail('jquery_file_up_detail');" value="选择文件" />
				   		<span class="STYLE12"><a href="upl_purchase_excel/template.xls">下载模板</a></span>	
					</span><br/>
					<div id="purchase_details"></div>
					<div id="jquery_file_up_detail">	
		              <input type="hidden" name="tempfilename" value=""/>
		            </div>
		            <table width="100%">
		            	<tr height="20px"><td colspan="2"></td></tr>
			            <tr width="100%">
				        	<td colspan="2" align="center" width="100%">
				        		<input type="button" name="Submit2" value="上一步" class="normal-green" onClick="previousStep();">							
				        	</td>
				        </tr>
					</table>
				</div>
		</div>
	</div>
		</td>
	</tr>
	<tr width="100%" height="10%" valign="bottom">
		<td width="100%" height="10%" valign="bottom">
			<table width="100%" height="100%">
				<tr width="100%" height="100%">	
				 	<td valign="bottom" width="100%" height="100%">
				 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				 			<tr width="100%">
				 				<td align="right" valign="middle" class="win-bottom-line" width="100%">
				 					<input type="button" name="Submit2" id="savePurchaseAllInfoButtion" value="提交" class="normal-green" onClick="checkFormAndSubmit();"/>
							  </td>
				 			</tr>
				 		</table>
				 	</td>
	 			</tr>
			</table>
		</td>
	</tr>
</table>
	</form>
	<script type="text/javascript">
		
		var lastTabs;//上一个tabs的Index
		$("#tabs").tabs({
		cache: false,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		load: function(event, ui) {onLoadInitZebraTable();}	,
		selected:0,
		show:function(event,ui)
			 {
			 	var isChangeLastTabs = true;
			 	if(lastTabs ==2)
			 	{
			 		
			 	}
			 	if(ui.index==2)
			 	{
			 		
			 	}
			 	if(ui.index==3)
			 	{
			 		
			 	}
			 			 	
			 	if(isChangeLastTabs)
			 	{
			 		lastTabs = ui.index;
			 	}
			 },
		select:function(event, ui)
			{
				var selected = $("#currentSelectedTab").val();
				if(0 == selected)
				{
					if(checkPurchaseSupplier())
					{
						$("#currentSelectedTab").val(ui.index);
						return true;
					}
					else
					{
						return false;
					}
			 }
				else if(1 == selected)
				{
					if(checkPurchaseReceiveAddress())
					{
						$("#currentSelectedTab").val(ui.index);
						return true;
					}
					else
					{
						return false;
					}
				}
				else if(3 == selected)
				{
					if(checkPurchaseProdures())
					{
						$("#currentSelectedTab").val(ui.index);
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					$("#currentSelectedTab").val(ui.index);
					return true;
				}
			}
		});
		
	function loadUplaodFile(file_name)
	{
			$(".cssDivschedule").css("display","block");
			showMessage("正在加载采购单详细，请稍候...","alert");
			$.ajax({
			url: 'purchase_detail_prefill.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:{file_name:file_name},
			
			beforeSend:function(request){
			},
			
			error: function(){
					$(".cssDivschedule").css("display","none");
			},
			
			success: function(html)
			{
					$(".cssDivschedule").css("display","none");
					$("#purchase_details").html(html);
					
					addAutoComplete($("#p_name"),
					"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
					"merge_info",
					"p_name");
			}
		});
	}
	
		
//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
	</script>
</body>
</html>
