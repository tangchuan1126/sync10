<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	String purchase_id = StringUtil.getString(request,"purchase_id");
	String purchase_name = StringUtil.getString(request,"purchase_name");
	
	String type=StringUtil.getString(request,"type");
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id);
	
	DBRow[] searchPurchaseDetail = null;
	if(purchase_id!=null&&!purchase_id.equals("")&&!purchase_name.equals(""))
	{
		searchPurchaseDetail=purchaseMgr.getPurchaseDetailByPurchasename(Long.parseLong(purchase_id),purchase_name);
	}
	PurchaseMoneyKey purchasemoneykey = new PurchaseMoneyKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script language="javascript" src="../../common.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}

</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" align="center">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" style="margin:10px;">
			  <tr>
				<td  align="left" valign="top" colspan="2">
						<%
							if(searchPurchaseDetail!=null&&searchPurchaseDetail.length>0)
							{
						%>
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
							<tr> 
								<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
								<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">预计到达日期</th>
								<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">到达数量</th>
								<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">订购数量</th>
								<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">单位</th>
								<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">采购价格</th>
								<th width="10%" class="right-title " style="vertical-align: center;text-align: center;">历史价格</th>
							</tr>
							<% 
								for(int i = 0; i<searchPurchaseDetail.length;i++)
								{
							%>
							<tr align="center" valign="middle">
							  <td height="40" nowrap="nowrap" align="left"><%=searchPurchaseDetail[i].getString("purchase_name") %></td>
								<td align="center">
									<%
										if(!searchPurchaseDetail[i].getString("eta").equals(""))
										{
											out.print(searchPurchaseDetail[i].getString("eta").substring(0,10));
										}
										else
										{
											out.print("&nbsp;");
										}
									%>				  </td>
								<td><%=searchPurchaseDetail[i].getString("reap_count") %></td>
								<td><%=searchPurchaseDetail[i].getString("purchase_count") %>
								<td><%=searchPurchaseDetail[i].getString("unit_name")%></td>
								<td><font color="<%=searchPurchaseDetail[i].getString("compare") %>"><%=searchPurchaseDetail[i].getString("price")%></font></td>
								<td><%=searchPurchaseDetail[i].getString("historyprice")%></td>
							</tr>
							<%
								}
							%>
						</table>
						<%
							}
							else if(purchase_id!=null&&!purchase_id.equals("")&&!purchase_name.equals(""))
							{
								out.println("<font color='red' size='14'>该商品未包括在此采购单内</font>");
							}
						%>
				</td>
			  </tr>
			</table>
	  </td>
	</tr>
	<tr>
		<td valign="bottom">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
						<input type="button" name="Submit2" value="关闭" class="normal-white" onClick="parent.closeWinNotRefresh();">
				  </td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
</body>
</html>

