<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.lucene.zr.PurchaseIndexMgr"%>
<%@page import="com.cwc.app.key.InvoiceKey"%>
<%@page import="com.cwc.app.key.DrawbackKey"%>
<%@page import="com.cwc.app.key.ModuleKey"%>
<%@page import="com.cwc.app.key.ProcessKey"%>
<%@page import="com.cwc.app.key.TransportTagKey"%>
<%@page import="com.cwc.app.key.QualityInspectionKey"%>
<%@page import="com.cwc.app.key.PurchaseProductModelKey"%>
<%@page import="com.cwc.app.key.FundStatusKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.app.key.FinanceApplyTypeKey"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.key.PurchaseLogTypeKey"%>
<jsp:useBean id="transportOrderKey" class="com.cwc.app.key.TransportOrderKey"></jsp:useBean>
<jsp:useBean id="fundStatusKey" class="com.cwc.app.key.FundStatusKey"></jsp:useBean>
<jsp:useBean id="deliveryOrderKey" class="com.cwc.app.key.DeliveryOrderKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="transportTagKey" class="com.cwc.app.key.TransportTagKey"/>
<jsp:useBean id="qualityInspectionKey" class="com.cwc.app.key.QualityInspectionKey"/>
<jsp:useBean id="purchaseProductModelKey" class="com.cwc.app.key.PurchaseProductModelKey"></jsp:useBean>
<%@page import="java.text.DecimalFormat"%>
<%@ page import="java.util.HashMap"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%

	String cmd = StringUtil.getString(request,"cmd");

	long supplier_id = StringUtil.getLong(request,"supplier_id",0);
	long productline_id = StringUtil.getLong(request,"productline_id",0l);
	long ps_id = StringUtil.getLong(request,"ps_id",0);
	int day = StringUtil.getInt(request,"day",3);
	
	int purchase_status = StringUtil.getInt(request,"purchase_status",0);
	int money_status = StringUtil.getInt(request,"money_status",-1);
	int arrival_time = StringUtil.getInt(request,"arrival_time",0);

	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	
	int invoice = StringUtil.getInt(request,"invoice",0);
	int drawback = StringUtil.getInt(request,"drawback",0);
	int need_tag = StringUtil.getInt(request,"need_tag",0);
	int quality_inspection = StringUtil.getInt(request,"quality_inspection",0);
	
	String search_key = StringUtil.getString(request,"search_key");
	int search_mode = StringUtil.getInt(request,"search_mode");
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(systemConfig.getIntConfigValue("order_page_count"));
	//systemConfig.getIntConfigValue("order_page_count")
	
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	long purchaseProductLineId		= StringUtil.getLong(request, "purchaseProductLineId");
	String purchaseProductLineName	= StringUtil.getString(request, "purchaseProductLineName");
	int purchaseFollowUpProcessKey	= StringUtil.getInt(request, "purchaseFollowUpProcessKey");
	int purchaseFollowUpActivity	= StringUtil.getInt(request, "purchaseFollowUpActivity");
	
	DBRow[] rows;
	
	if(cmd.equals("fillter"))
	{
		rows = purchaseMgr.getPurchaseFillter(supplier_id,ps_id,purchase_status,money_status,arrival_time,st,en,pc,0,productline_id,invoice,drawback,need_tag,quality_inspection);
	}
	else if(cmd.equals("followup"))
	{
		rows = purchaseMgr.getPurchaseFillter(0l,ps_id,purchase_status,money_status,arrival_time,"","",pc,day,0l,0,0,0,0);
	}
	else if(cmd.equals("search"))
	{
		rows =  purchaseMgr.searchPurchase(search_key,search_mode,pc);
	}
	else if(cmd.equals("analysis")) {
		rows = purchaseMgrLL.getAnalysis(st,en,analysisType,analysisStatus,day,pc);
	}
	else if(cmd.equals("purchaseFollowUpProcessSearch"))
	{
		rows = purchaseMgrZyj.getPurchaseRowsByProductLineIdAndProcessKey(purchaseProductLineId, purchaseFollowUpProcessKey, purchaseFollowUpActivity,pc);
	}
	else
	{
		rows = purchaseMgr.getPurchaseFillter(supplier_id,ps_id,purchase_status,money_status,arrival_time,st,en,pc,0,0,invoice,drawback,need_tag,quality_inspection);
	}

	PurchaseKey purchasekey = new PurchaseKey();
	PurchaseArriveKey purchasearrivekey = new PurchaseArriveKey();
	HashMap followuptype = new HashMap();
	followuptype.put(1,"创建记录"); 
	followuptype.put(2,"财务记录");
	followuptype.put(3,"修改记录");
	followuptype.put(4,"发票跟进");
	followuptype.put(5,"退税跟进");
	followuptype.put(6,"价格跟进");
	followuptype.put(7,"交货跟进");
	followuptype.put(8,"内部标签");
	followuptype.put(12,"商品标签");
	followuptype.put(13,"采购单质检要求");
	followuptype.put(23,"商品范例记录");
	followuptype.put(33,"第三方标签");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
.title{font-size:12px;color:green;font-weight:blod;}
-->
</style>
 
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>


<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	 
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
 
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
.zebraTable td {border:none;}
span.stateName{width:70px;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:200px;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
tr.split td input{margin-top:2px;}
</style>
<script>
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id);      
	}
	function goApplyFunds(id)
	{
		var id="'F"+id+"'";
		window.open("<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=searchby_index&search_mode=1&apply_id="+id);     
	}
	jQuery(function($){
		onLoadInitZebraTable();
	})
	function windowRefresh(){
		window.location.reload();
	}
</script>
</head>

<body >

	
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true" isBottom="true">
    <tr> 
        <th width="35%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">采购信息</th>
        <th width="28%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">资金情况</th>
        <th width="22%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">各流程状态</th>
        <th width="15%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">跟进</th>
  	</tr>
  <%
  if(rows != null && rows.length > 0 ){
  	for(int i=0;i<rows.length;i++)
  	{
  %>
  	<tr align="center" valign="middle">
	  	<td height="80" nowrap="nowrap">
	  	<fieldset class="set" style="border-color:blue;">
	  		<legend>
	  			<span class="title"><a href="javascript:void(0)" onclick="window.open('purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>')">P<%=rows[i].getString("purchase_id")%></a></span>
	  			&nbsp;<span style="font-weight:bold;"><%DBRow storage = catalogMgr.getDetailProductStorageCatalogById(rows[i].get("ps_id",0l));out.print(storage.getString("title"));%></span>
	  		</legend>
			<%
				String styleWrap = "<p style='text-align:left;clear:both;'>";
				String supplierStyle = "<p style='text-align:left;clear:both;'><span class='stateName'>&nbsp;</span><span class='stateValue'>";
				try
				{
					DBRow supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(rows[i].getString("supplier")));
					if(supplier!=null)
					{
						DBRow productLine = productLineMgrTJH.getProductLineById(supplier.get("product_line_id",0l));
						if(productLine !=null)
						{
							out.println(styleWrap+"<span class='stateName'>产品线:</span><span class='stateValue'>"+productLine.getString("name")+"</span></p>");
						}
						out.println(supplierStyle+supplier.getString("sup_name")+"</span></p>");
					}
				}
				catch(NumberFormatException e)
				{
					out.println(supplierStyle+rows[i].getString("supplier")+"</span></p>");
				}
			%>
			<p style="text-align:left;clear:both;"><span class="stateName">主流程:</span>
	 		<span class="stateValue">
	 			<%=purchasekey.getQuoteStatusById(rows[i].getString("purchase_status"))%>&nbsp; 
	 		</span>
	 		</p>
	  		<p style="text-align:left;clear:both;"><span class="stateName">负责人:</span> <span class="stateValue"><%=rows[i].getString("proposer") %></span></p>
	  		<p style="text-align:left;clear:both;"><span class="stateName">采购时间:</span><span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(rows[i].getString("purchase_date")))%></span></p>
	  		<p style="text-align:left;clear:both;"><span class="stateName">更新日期:</span><span class="stateValue"><%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rows[i].getString("updatetime")))%></span></p>
			<p style="text-align:left;clear:both;"><span class="stateName">总体积:</span> <span class="stateValue"><%=purchaseMgr.getPurchaseVolume(rows[i].get("purchase_id",0l))%> cm³</span></p>
			<p style="text-align:left;clear:both;"><span class="stateName">总重量:</span> <span class="stateValue"><%=purchaseMgr.getPurchaseWeight(rows[i].get("purchase_id",0l))%> Kg</span></p>
			<p style="text-align:left;clear:both;"><span class="stateName">总金额:</span> <span class="stateValue"><%=purchaseMgr.getPurchasePrice(rows[i].get("purchase_id",0l))%> RMB</span></p>
			<p style="text-align:left;clear:both;"><span class="stateName">价格确认:</span> <span class="stateValue"><%=rows[i].get("price_affirm_over",0d)==0?df.format(tDate.getDiffDate(rows[i].getString("purchase_date"),"dd"))+"天":rows[i].get("price_affirm_over",0d)+"天完成"%></span></p>
			<%//读取预申请单
              DBRow prepareDBRow = preparePurchaseMgrZwb.getPreparePurchaseFunds(Long.valueOf(rows[i].getString("purchase_id")));%>
               <%if(prepareDBRow!=null){ %>
                  <%if(prepareDBRow.getString("currency").equals("USD")){ %>
                      <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span> <span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%>/<%=prepareDBRow.getString("standard_money")%>RMB</span></p>
                  <%}else{ %>
                      <p style="text-align:left;clear:both;"><span class="stateName">预申请金额：</span> <span class="stateValue"><%=prepareDBRow.getString("amount")%><%=prepareDBRow.getString("currency")%></span></p>
                  <%} %>   
               <%} %>
	  	</fieldset>
	  	<%
	  		DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(rows[i].getString("purchase_id")));
	  		for(int dt = 0; dt < deliveryOrderRows.length; dt ++)
	  		{
		%>
	  	<fieldset class="set" style="border-color:#993300;">
	  		<legend>
  				<a style='color:mediumseagreen;' target='_blank' href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/transport_order_detail.html?transport_id=<%=deliveryOrderRows[dt].getString("transport_id")%>'>T<%=deliveryOrderRows[dt].getString("transport_id")%></a>
 				&nbsp;<%=transportOrderKey.getTransportOrderStatusById(deliveryOrderRows[dt].get("transport_status",0)) %>
	  		</legend>
	  		<p style="text-align:left;clear:both;">
	  			<span class="stateName">创建人:</span>
				<span class="stateValue">
					 <%
					 	DBRow createAdmin = adminMgr.getDetailAdmin(deliveryOrderRows[dt].get("create_account_id",0l));
					 	if(createAdmin!=null)
					 	{
					 		out.print(createAdmin.getString("employe_name"));
					 	} 
					 %> 
				</span>
			</p>
	  		<p style="text-align:left;clear:both;">
	  			<span class="stateName">允许装箱:</span>
	  			<span class="stateValue">
					<%
					 	DBRow packingAdmin = adminMgr.getDetailAdmin(deliveryOrderRows[dt].get("packing_account",0l));
					 	if(packingAdmin!=null)
					 	{
					 		out.print(packingAdmin.getString("employe_name"));
					 	} 
					 %>
				</span>
			</p>
	  		<p style="text-align:left;clear:both;">
	  			<span class="stateName">创建时间:</span>
	  			<span class="stateValue">
	  				<%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(deliveryOrderRows[dt].getString("transport_date")))%>
	  			</span>
	  		</p>
			<p style="text-align:left;clear:both;">
				<span class="stateName">更新时间:</span>
				<span class="stateValue">
					<%=DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(deliveryOrderRows[dt].getString("updatedate")))%>
				</span>
			</p>
	  	</fieldset>	
	  	<%		
	  		}
	  	%>
	  	</td>
	  	<td nowrap="nowrap" align="left">
	  		<fieldset class="set" style="border-color:blue;">
			<%
				DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,rows[i].get("purchase_id",0L),FinanceApplyTypeKey.PURCHASE_ORDER);
					//applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("purchase_id"),4);//
			%>
				<legend style="font-size:12px;font-weight:normal;color:#000000;background:none;">
				<span style="font-size:12px;color:#000000;font-weight:normal;">
				<a href="javascript:void(0)" onclick="window.open('purchase_detail.html?purchase_id=<%=rows[i].get("purchase_id",0) %>')">P<%=rows[i].getString("purchase_id")%></a>
					<%
						String statusName = "";
						if(applyMoneys.length > 0)
						{
							List imageList = applyMoneyMgrLL.getImageList(applyMoneys[0].getString("apply_id"),"1");
							//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
						 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneys[0].getString("status")) && imageList.size() < 1)
						 	{
						 		statusName = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
						 	}else{
						 		statusName = fundStatusKey.getFundStatusKeyNameById(applyMoneys[0].getString("status"));	
						 	}
						 	statusName = "("+statusName+")";
						}
					%>
				采购单定金(<%=rows[i].get("apply_money_over",0d)==0?df.format(tDate.getDiffDate(rows[i].getString("purchase_date"),"dd"))+"天":rows[i].get("apply_money_over",0d)+"天完成"%>)
				</span>
				</legend>
			<table style="width:100%">
				<tr style='width:100%'>
					<td style='width:100%'>
						采购单金额:<%=purchaseMgr.getPurchasePrice(rows[i].get("purchase_id",0l))%>RMB
					</td>
				</tr>
				<%
	  				if(applyMoneys.length>0) {
	  					String moneyStandardStr = "";
	  					if(!"RMB".equals(applyMoneys[0].getString("currency")))
	  					{
	  						moneyStandardStr = "/"+applyMoneys[0].getString("standard_money")+"RMB";
	  					}
		  				out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneys[0].get("apply_id",0l)+")\">F"+applyMoneys[0].get("apply_id",0)+"</a>"+ statusName +applyMoneys[0].get("amount",0f)+applyMoneys[0].getString("currency")+moneyStandardStr+"</td></tr>");
		  			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneys[0].getString("apply_id"));
		  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
		  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
		  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
		  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
		  				int moneyStatus = applyTransferRows[ii].get("status",0);
		  				String transferMoneyStandardStr = "";
		  				if(!"RMB".equals(currency))
		  				{
		  					transferMoneyStandardStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
		  				}
		  				if(moneyStatus != 0 )
			  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
		  				else
			  					out.println("<tr style='width:100%'><td style='width:100%'><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardStr+"</td></tr>");
			  			}
		  			}
	  		%>
			</table>
	  			</fieldset>
	  			<%
					if(deliveryOrderRows.length > 0)
					{
						for(int de = 0; de < deliveryOrderRows.length; de ++)
						{
							String statusNameTr = "";
							String appMoneyStateStr = "";
							DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[de].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
								if(applyMoneyDelivers.length > 0)
								{
									List imageListTr = applyMoneyMgrLL.getImageList(applyMoneyDelivers[0].getString("apply_id"),"1");
									//在状态等于0（未申请转账），如果未上传凭证，为不可申请转账
								 	if((String.valueOf(FundStatusKey.NO_APPLY_TRANSFER)).equals(applyMoneyDelivers[0].getString("status")) && imageListTr.size() < 1)
								 	{
								 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(FundStatusKey.CANNOT_APPLY_TRANSFER+"");
								 	}else{
								 		statusNameTr = fundStatusKey.getFundStatusKeyNameById(applyMoneyDelivers[0].getString("status"));	
								 	}
								 	statusNameTr = "("+statusNameTr+")";
								appMoneyStateStr = statusNameTr + applyMoneyDelivers[0].get("amount",0.0) + applyMoneyDelivers[0].getString("currency");
								}
					%>			
						<fieldset class="set" style="border-color:#993300;">
							<legend>
								<a target="_blank" href='<%=ConfigBean.getStringValue("systenFolder") %>administrator/transport/transport_order_detail.html?transport_id=<%=deliveryOrderRows[de].getString("transport_id") %>'>T<%=deliveryOrderRows[de].getString("transport_id") %></a>
								交货单货款
							</legend>
							<table style="100%">
								<tr style="100%">
									<td style="100%">
										交货单金额:<%=transportMgrZJ.getTransportSendPrice(deliveryOrderRows[de].get("transport_id",0L)) %>RMB
	  								</td>
								</tr>
							<%
								if(applyMoneyDelivers.length > 0)
								{
									String moneyStandardDeliverStr = "";
				  					if(!"RMB".equals(applyMoneyDelivers[0].getString("currency")))
				  					{
				  						moneyStandardDeliverStr = "/"+applyMoneyDelivers[0].getString("standard_money")+"RMB";
				  					}
									out.println("<tr><td><a href='javascript:void(0)' onClick=\"goApplyFunds("+applyMoneyDelivers[0].get("apply_id",0l)+")\">F"+applyMoneyDelivers[0].get("apply_id",0)+"</a>"+appMoneyStateStr+moneyStandardDeliverStr+"</td></tr>");
									DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByApplyMoney(applyMoneyDelivers[0].getString("apply_id"));
						  			for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) 
						  			{
						  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
						  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
						  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
						  				int moneyStatus = applyTransferRows[ii].get("status",0);
						  				String transferMoneyStandardDeliverStr = "";
						  				if(!"RMB".equals(currency))
						  				{
						  					transferMoneyStandardDeliverStr = "/"+applyTransferRows[ii].getString("standard_money")+"RMB";
						  				}
					  					if(moneyStatus != 0 )
						  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>转完"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
						  				else
						  					out.println("<tr><td><a href='javascript:void(0)' onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请"+amount+" "+currency+transferMoneyStandardDeliverStr+"</td></tr>");
						  			}
								}
							%>
							</table>
						</fieldset>	
				<%			
						}
					}
				%>
	  	</td>
	  	<td nowrap="nowrap" align="left">
	  	<table cellpadding="0" cellspacing="0" width="100%">
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<font style="font-weight: bold;">交货:<%=purchasearrivekey.getQuoteStatusById(rows[i].getString("arrival_time"))%></font>
	  				<%
	  					//采购员Ids, names
				  		DBRow[] purchaserPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
				  		String purchserPersonNames	= "";
				  		if(purchaserPersons.length > 0)
				  		{
				  			purchserPersonNames	+= purchaserPersons[0].getString("employe_name");
				  			for(int h = 1; h < purchaserPersons.length; h ++)
				  			{
				  				DBRow purchaserPerson = purchaserPersons[h];
				  				purchserPersonNames	 += ","+purchaserPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(purchserPersonNames)){
				  			out.println("<br/>采购:"+purchserPersonNames);
				  		}
				  		//调度员Ids, names
				  		/*
				  		DBRow[] dispatcherPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER);
				  		String dispatcherPersonNames= "";
				  		if(dispatcherPersons.length > 0){
				  			dispatcherPersonNames	+= dispatcherPersons[0].getString("employe_name");
				  			for(int j = 1; j < dispatcherPersons.length; j ++){
				  				DBRow dispatcherPerson = dispatcherPersons[j];
				  				dispatcherPersonNames+= ","+dispatcherPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(dispatcherPersonNames))
				  		{
				  			out.println("<br/>调度:"+dispatcherPersonNames);
				  		}
				  		else
				  		{
				  			out.println("<br/>调度:");
				  		}
				  		*/
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%=rows[i].get("apply_money_over",0d)==0?df.format(tDate.getDiffDate(rows[i].getString("purchase_date"),"dd"))+"天":rows[i].get("apply_money_over",0d)+"天完成<br/>" %>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
		  				DBRow tagFileSumRow = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("purchase_id",0),FileWithTypeKey.PURCHASE_TAG_FILE);
	  					int tagFileSum = tagFileSumRow.get("sum_file",0);
	  					String fontStyleTag	 = "";
	  					if(1 == rows[i].get("need_tag",0))
				  		{
				  		
				  			out.println("内部标签:不需要");
				  		}
				  		else if(2 == rows[i].get("need_tag",0))
				  		{
				  			fontStyleTag = "color: blue; font-weight: bold";
					 		out.println("<font style='"+fontStyleTag+"'>内部标签:制签中</font>");
				  		}
				  		else if(0 == rows[i].get("need_tag",0))
				  		{
				  			out.println("内部标签:");
				  		}
				  		else if(3 == rows[i].get("need_tag",0))
				  		{
				  			if(tagFileSum > 0){
	  							fontStyleTag = "color: green;font-weight: bold;";
	  			 			}else{
	  			 				fontStyleTag = "color: red;font-weight: bold;";
	  			 			}
				  			out.println("<font style='"+fontStyleTag+"'>内部标签:"+transportTagKey.getTransportTagById(rows[i].getString("need_tag"))+"</font>");
				  		}
				  		//制签Ids, names
				  		DBRow[] tagPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				  		String tagPersonNames= "";
				  		if(tagPersons.length > 0){
				  			tagPersonNames	+= tagPersons[0].getString("employe_name");
				  			for(int n = 1; n < tagPersons.length; n ++){
				  				DBRow tagPerson = tagPersons[n];
				  				tagPersonNames  += ","+tagPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(tagPersonNames)){
				  			out.println("<br/>"+tagPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(TransportTagKey.FINISH == rows[i].get("need_tag",0))
			  			{
			  				out.println(rows[i].get("need_tag_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("need_tag",0) == TransportTagKey.TAG)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
					
							double tag_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println(MoneyUtil.round(tag_time,2)+"天<br/>");
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;border-collapse:collapse;">
	  				<%
		  				DBRow qualityFileSumRow = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("purchase_id",0),FileWithTypeKey.PURHCASE_QUALITY_INSPECTION);
	  					int qualityFileSum = qualityFileSumRow.get("sum_file",0);
	  					String fontStyleQuality	 = "";
	  					if(QualityInspectionKey.FINISH == rows[i].get("quality_inspection",0)){
	  						if(qualityFileSum > 0){
	  							fontStyleQuality = "color: green;font-weight: bold;";
	  			 			}else{
	  			 				fontStyleQuality = "color: red;font-weight: bold;";
	  			 			}
	  					}
	  					else if(QualityInspectionKey.NO_NEED_QUALITY == rows[i].get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY))
	  					{
	  						fontStyleQuality = "";
	  					}
	  					else
	  					{
	  						fontStyleQuality = "color: blue; font-weight: bold";
	  					}
	  					out.println("<font style='"+fontStyleQuality+"'>质检要求:"+qualityInspectionKey.getQualityInspectionById(rows[i].getString("quality_inspection"))+"</font>");
				  		//质检Ids, names
				  		DBRow[] qualityPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
				  		String qualityPersonNames= "";
				  		if(qualityPersons.length > 0)
				  		{
				  			qualityPersonNames	+= qualityPersons[0].getString("employe_name");
				  			for(int o = 1; o < qualityPersons.length; o ++)
				  			{
				  				DBRow qualityPerson = qualityPersons[o];
				  				qualityPersonNames  += ","+qualityPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(qualityPersonNames))
				  		{
				  			out.println("<br/>"+qualityPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(QualityInspectionKey.FINISH == rows[i].get("quality_inspection",0))
			  			{
			  				out.println(rows[i].get("quality_inspection_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("quality_inspection",0) == QualityInspectionKey.NEED_QUALITY)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
							double quality_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println(MoneyUtil.round(quality_time,2)+"天<br/>");
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					int[] productFileTypes = {FileWithTypeKey.PURCHASE_PRODUCT_FILE};
	  					DBRow[] imagesRowsProduct = purchaseMgrZyj.getAllProductTagFilesByPcId(rows[i].get("purchase_id",0),productFileTypes );
		  				String fontStyleProductModel = "";
		  			 	if(PurchaseProductModelKey.FINISH == rows[i].get("product_model",0))
		  			 	{
		  			 		if(imagesRowsProduct.length > 0)
		  			 		{
		  			 			fontStyleProductModel = "color: green;font-weight: bold;";
		  			 		}else{
		  			 			fontStyleProductModel = "color: red;font-weight: bold;";
		  			 		}
		  			 	}
		  				else if(PurchaseProductModelKey.NOT_PRODUCT_MODEL == rows[i].get("product_model",PurchaseProductModelKey.NOT_PRODUCT_MODEL))
	  				 	{
	  				 		fontStyleProductModel = "";
	  				 	}
	  				 	else
	  				 	{
	  				 		fontStyleProductModel = "color: blue; font-weight: bold";
	  				 	}
	  					out.println("<font style='"+fontStyleProductModel+"'>商品范例:"+purchaseProductModelKey.getProductModelKeyName(rows[i].getString("product_model"))+"</font>");
				  		//商品范例Ids, names
				  		DBRow[] productModelPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
				  		String productModelPersonNames= "";
				  		if(productModelPersons.length > 0){
				  			productModelPersonNames	+= productModelPersons[0].getString("employe_name");
				  			for(int o = 1; o < productModelPersons.length; o ++){
				  				DBRow productModelPerson = productModelPersons[o];
				  				productModelPersonNames  += ","+productModelPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(productModelPersonNames)){
				  			out.println("<br/>"+productModelPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(PurchaseProductModelKey.FINISH == rows[i].get("product_model",0))
			  			{
			  				out.println(rows[i].get("product_model_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("product_model",0) == PurchaseProductModelKey.NEED_PRODUCT_MODEL)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
					
							double tag_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println(MoneyUtil.round(tag_time,2)+"天<br/>");
			  			}
	  				%>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					DBRow invoiceFileSumRow = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("purchase_id",0),FileWithTypeKey.PURCHASE_INVOICE);
	  					int invoiceFileSum = invoiceFileSumRow.get("sum_file",0);
	  					String fontStyleInvoice	 = "";
	  					if(InvoiceKey.FINISH == rows[i].get("invoice",InvoiceKey.NOINVOICE)){
	  						if(invoiceFileSum > 0){
	  							fontStyleInvoice = "color: green;font-weight: bold;";
	  			 			}else{
	  			 				fontStyleInvoice = "color: red;font-weight: bold;";
	  			 			}
	  					}
	  					else if(InvoiceKey.NOINVOICE == rows[i].get("invoice",InvoiceKey.NOINVOICE))
	  			 		{
	  			 			fontStyleInvoice = "";
	  			 		}
	  			 		else
	  			 		{
	  			 			fontStyleInvoice = "color: blue; font-weight: bold";
	  			 		}
	  					out.println("<font style='"+fontStyleInvoice+"'>开票:"+invoiceKey.getInvoiceById(rows[i].getString("invoice"))+"</font>");
				  		//发票员Ids, names
				  		DBRow[] invoicePersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
				  		String invoicePersonNames= "";
				  		if(invoicePersons.length > 0){
				  			invoicePersonNames	+= invoicePersons[0].getString("employe_name");
				  			for(int k = 1; k < invoicePersons.length; k ++){
				  				DBRow invoicePerson = invoicePersons[k];
				  				invoicePersonNames+= ","+invoicePerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(invoicePersonNames)){
				  			out.println("<br/>"+invoicePersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(InvoiceKey.FINISH == rows[i].get("invoice",0))
			  			{
			  				out.println(rows[i].get("invoice_over_date",0.0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("invoice",0)==InvoiceKey.INVOICE||rows[i].get("invoice",0)==InvoiceKey.INVOICING)
			  			{
			  				double invoice_time = purchaseMgrZyj.getPurchaseDiffTime(rows[i].get("purchase_id",0l),ProcessKey.INVOICE,InvoiceKey.INVOICING,"dd");
			  				if(invoice_time==0)
			  				{
			  					out.println("未开始<br/>");
			  				}
			  				else
			  				{
			  					out.println(invoice_time+"天<br/>");
			  				}
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<% 
		  				DBRow drawbackFileSumRow = transportMgrZr.getFileNumberByFileWithIdAndFileWithType("file",rows[i].get("purchase_id",0),FileWithTypeKey.PURCHASE_DRAWBACK);
	  					int drawbackFileSum = drawbackFileSumRow.get("sum_file",0);
	  					String fontStyleDrawback	 = "";
	  					if(drawbackKey.FINISH == rows[i].get("drawback",DrawbackKey.NODRAWBACK)){
	  						if(drawbackFileSum > 0){
	  							fontStyleDrawback = "color: green;font-weight: bold;";
	  			 			}else{
	  			 				fontStyleDrawback = "color: red;font-weight: bold;";
	  			 			}
	  					}
	  					else if(DrawbackKey.NODRAWBACK == rows[i].get("drawback",DrawbackKey.NODRAWBACK))
	  			 		{
	  			 			fontStyleDrawback = "";
	  			 		}
	  			 		else
	  			 		{
	  			 			fontStyleDrawback = "color: blue; font-weight: bold";
	  			 		}
	  					out.println("<font style='"+fontStyleDrawback+"'>退税:"+drawbackKey.getDrawbackById(rows[i].getString("drawback"))+"</font>");
				  		//退税Ids, names
				  		DBRow[] drawbackPersons	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
				  		String drawbackPersonNames= "";
				  		if(drawbackPersons.length > 0){
				  			drawbackPersonNames	+= drawbackPersons[0].getString("employe_name");
				  			for(int m = 1; m < drawbackPersons.length; m ++){
				  				DBRow drawbackPerson = drawbackPersons[m];
				  				drawbackPersonNames+= ","+drawbackPerson.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(drawbackPersonNames)){
				  			out.println("<br/>"+drawbackPersonNames);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(DrawbackKey.FINISH == rows[i].get("drawback",0))
			  			{
			  				out.println(rows[i].get("drawback_over_date",0.0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("drawback",0)==DrawbackKey.DRAWBACK||rows[i].get("drawback",0)==DrawbackKey.DRAWBACKING)
			  			{
			  				double drawback_time = purchaseMgrZyj.getPurchaseDiffTime(rows[i].get("purchase_id",0l),ProcessKey.DRAWBACK,DrawbackKey.DRAWBACKING,"dd");
			  				if(drawback_time==0)
			  				{
			  					out.println("未开始<br/>");
			  				}
			  				else
			  				{
			  					out.println(drawback_time+"天<br/>");
			  				}
			  				
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  		<tr>
	  			<td style="border-bottom:1px dashed silver;">
	  				<%
	  					String fontStyleTagThird	 = "";
	  					if(1 == rows[i].get("need_third_tag",0))
				  		{
				  		
				  			out.println("第三方标签:不需要");
				  		}
				  		else if(2 == rows[i].get("need_third_tag",0))
				  		{
				  			fontStyleTagThird = "color: blue; font-weight: bold";
					 		out.println("<font style='"+fontStyleTagThird+"'>第三方标签:制签中</font>");
				  		}
				  		else if(0 == rows[i].get("need_third_tag",0))
				  		{
				  			out.println("第三方标签:");
				  		}
				  		else if(3 == rows[i].get("need_third_tag",0))
				  		{
				  			int[] fileType	= {FileWithTypeKey.PRODUCT_TAG_FILE};
				  			if(purchaseMgrZyj.checkPurchaseThirdTagFileFinish(rows[i].get("purchase_id",0L),fileType, "purchase_tag_types")){
	  							fontStyleTagThird = "color: green;font-weight: bold;";
	  			 			}else{
	  			 				fontStyleTagThird = "color: red;font-weight: bold;";
	  			 			}
				  			out.println("<font style='"+fontStyleTagThird+"'>第三方标签:"+transportTagKey.getTransportTagById(rows[i].getString("need_third_tag"))+"</font>");
				  		}
				  		//制签Ids, names
				  		DBRow[] tagPersonsThird	= scheduleMgrZR.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(rows[i].getString("purchase_id")), ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
				  		String tagPersonNamesThird= "";
				  		if(tagPersonsThird.length > 0){
				  			tagPersonNamesThird	+= tagPersonsThird[0].getString("employe_name");
				  			for(int n = 1; n < tagPersonsThird.length; n ++){
				  				DBRow tagPersonThird = tagPersonsThird[n];
				  				tagPersonNamesThird  += ","+tagPersonThird.getString("employe_name");
				  			}
				  		}
				  		if(!"".equals(tagPersonNamesThird)){
				  			out.println("<br/>"+tagPersonNamesThird);
				  		}
	  				%>
	  			</td>
	  			<td align="right" style="border-bottom:1px dashed silver;">
	  				<%
	  					if(TransportTagKey.FINISH == rows[i].get("need_third_tag",0))
			  			{
			  				out.println(rows[i].get("need_third_tag_over",0)+"天完成<br/>");
			  			}
			  			else if(rows[i].get("need_third_tag",0) == TransportTagKey.TAG)
			  			{
			  				TDate tNow = new TDate();
			  				TDate endDate = new TDate(rows[i].getString("purchase_date"));
					
							double tag_time = tNow.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),"dd");
			  				out.println(MoneyUtil.round(tag_time,2)+"天<br/>");
			  			}
	  				%>
	  				&nbsp;
	  			</td>
	  		</tr>
	  	</table>
	  	</td>
	  	<td style="text-align:left;">
  	  <%
	  		 		DBRow[] logs = purchaseMgr.getfollowuplogsLastNumber(rows[i].get("purchase_id",0),0,4);
	  		 		if(logs != null && logs.length > 0 ){
	  		 			int count = logs.length >= 4 ? 3 :  logs.length;
	  		 			boolean isHasMore = (logs.length == 4 ? true:false);
	  		 			 for(int index = 0 ; index < count ; index++){
	  		 				DBRow tempLog = logs[index];
	  		 				 %>
	  		 				 <div style="font-size:12px;margin-bottom:3px;line-height:15px;word-wrap:break-word;width:220px;border-bottom:1px dashed silver;padding-bottom:3px;padding-top:3px;">
								<span style="font-size:12px;">
							 		<font style="color:#f60;"><%=followuptype.get(tempLog.get("followup_type",0))%></font>:
									<strong><%=null!=adminMgrLL.getAdminById(tempLog.getString("follower_id"))?adminMgrLL.getAdminById(tempLog.getString("follower_id")).getString("employe_name"):"" %></strong>
									<span style="color:#999999;font-size:11px;font-farmliy:Verdana">
									<% if(!"".equals(tempLog.getString("followup_date"))){
											out.println(DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(tempLog.getString("followup_date"))));
										}
									%>
									</span>
								</span><br />
								<%=tempLog.getString("followup_content") %>
 							  </div>
	  		 				 <% 	
	  		 			 }
	  		 			 if(isHasMore){
  	  %>
	  		 				 	<a href="javascript:purchaseLogs(<%=rows[i].getString("purchase_id")%>)">更多</a>
  	  <% 
  	  }
	  		 		}
  	  %>
	  	</td>
  </tr>
  <tr class="split">
  		<td style="text-align:left;padding-left:20px;">
	  <%
	  	//只要有资金或者交货单，不能取消此采购单
	  	if(0 == deliveryOrderRows.length && 0 == applyMoneys.length)
  	  	//if(rows[i].get("purchase_status",0)!=PurchaseKey.CANCEL && rows[i].get("arrival_time",0)!=PurchaseArriveKey.WAITAPPROVE&&rows[i].get("purchase_status",0)!=PurchaseKey.FINISH)
  	  	{
  	  %>
  	  <tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.cancelPurchase">
		  	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="short-short-button-cancel" value="取消" onclick="cancelPurchase(<%=rows[i].get("purchase_id",0)%>)"/>
  	  </tst:authentication>
  	  <% 
		  	  }
		  	  %>
  		</td>
  		<td colspan="5" style="text-align:right;padding-left:20px;">
  			<%
  			if(rows[i].get("purchase_status",0)!=PurchaseKey.CANCEL){
  				%>
  				<tst:authentication bindAction="com.cwc.app.api.zj.PurchaseMgr.followupPurchase">
  		  	  		<input type="button" class="long-button" onclick='followup(<%=rows[i].getString("purchase_id")%>)' value="跟进价格与交货"/>
  				</tst:authentication>
  				<% 
  	  	if(InvoiceKey.NOINVOICE != rows[i].get("invoice",InvoiceKey.NOINVOICE )){
  	  %>
  	  <input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,4)" value="发票"/>
  	  <%
  	  	}
  	  %>
  	  <%
  	  	if(DrawbackKey.NODRAWBACK != rows[i].get("drawback",DrawbackKey.NODRAWBACK)){
  	  %>
	  <input class="short-short-button-merge" type="button" onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,5)" value="退税"/>
  	  <%
  	  	}
  	  %>
	  <%
	  	if(TransportTagKey.NOTAG != rows[i].get("need_tag",TransportTagKey.NOTAG)){
	  %>		
	  <input type="button" class="long-button" value='跟进内部标签' onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,8)"/>
	  <%
	  	}
	  %>
	  <%
	  	if(TransportTagKey.NOTAG != rows[i].get("need_third_tag",TransportTagKey.NOTAG)){
	  %>		
	  <input type="button" class="long-button" value='跟进第三方标签' onclick="purchaseInvoiceState(<%=rows[i].getString("purchase_id")%>,<%=PurchaseLogTypeKey.THIRD_TAG %>)"/>
	  <%
	  	}
	  %>
	  <%
	  	if(QualityInspectionKey.NO_NEED_QUALITY != rows[i].get("quality_inspection",QualityInspectionKey.NO_NEED_QUALITY)){
	  %>
	  <input name="button" type="button" class="long-button" value="跟进质检要求" onClick="purchaseQualityInspect(<%=rows[i].getString("purchase_id")%>)"/>
	  <%		
	  	}
	  %>
	  <%
	  	if(PurchaseProductModelKey.NOT_PRODUCT_MODEL != rows[i].get("product_model",PurchaseProductModelKey.NOT_PRODUCT_MODEL)){
	  %>
	   <input name="button" type="button" class="long-button" value="跟进商品范例" onClick="purchaseProductModel(<%=rows[i].getString("purchase_id")%>)"/>
	  <%		
	  	}
	  %>
	  	
	  <% 
  	  }
  	  %>
  	  </td>
  </tr>
  <%
  	}
  }else{
	  %>
	  	 <tr>
	 		 <td colspan="6" style="text-align:center;line-height:180px;height:180px;background:#E6F3C5;border:1px solid silver;">无数据</td>
  </tr>
  <%
  	}
   %>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go2page(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go2page(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go2page(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go2page(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2page(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>
<script type="text/javascript">
	function applicationApprove(purchase_id)
	  {
	  	if(confirm("确定申请此采购单完成"))
	  	{
	  		document.application_approve.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/applicationApprovePurchase.action";
	  		document.application_approve.purchase_id.value = purchase_id;
	  		document.application_approve.submit();
	  	}
	  }
</script>
<form action="" method="post" name="application_approve">
	<input type="hidden" name="purchase_id"/>
</form>
</body>
</html>



