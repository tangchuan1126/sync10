<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp"%> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退款处理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css">	
	table{border-collapse:collapse;}
	table td{border:1px solid silver;}
	td.td_left{background:#eeeeee;width:23%;height:30px;line-height:30px;text-align:right;}
	td.td_right{background:#FFFFFF;width:77%;height:30px;line-height:30px;text-indent:5px;}
</style>

<%
	long refund_id = StringUtil.getLong(request,"refund_id");
	DBRow refund = refundMgrZr.getRefundByRefundId(refund_id);
	long schedule_id = StringUtil.getLong(request,"schedule_id");
	long schedule_sub_id = StringUtil.getLong(request,"schedule_sub_id");
	
%>
<script type="text/javascript">
function cancel(){$.artDialog && $.artDialog.close();}
function openWindow(url){
	window.open(url);
}
function examineProcessRegect(){
   var reject1 = $("#reject1");
   var reject2 = $("#reject2");
   $("#bohui_tr").attr("style","");
   reject1.css("display","none");
   reject2.attr("style","");
}
function examineProcessRegect2(){
	var reject = $("#reject").val();
	if($.trim(reject).length < 1){
		showMessage("请输入驳回理由.","alert");
		return ;
	}
	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/refund/RejectRefundAction.action';
	var json =  $("#myform").serialize();
	commonAjax(uri,json);
}
//json 放回结果为success的适用
function commonAjax(uri,dataJson){
    $.ajax({
	    url:uri,
	    data:dataJson,
	    dataType:'json',
	    beforeSend:function(request){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
	    success:function(data){
		    $.unblockUI();
	    	if(data && data.flag == "success"){
	    		cancel();
	    		$.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
		   	}else{
		   		showMessage("系统错误.","error");
			}
    	},
    	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	})
}
function windowOpen(oid){
    window.open('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid);
}
function executeRefund(){
	//var param = "amount="+$("#amount").val()+"&refundType="+$("#refundType").val()+"&note="+$("#note").val()+"&tranID="+$("#tranID").val();

 	var obj = {
		amount:$("#amount").val(),
		refundType:$("#refundType").val(),
		note:$("#note").val(),
		tranID:$("#txn_id").val(),
		currencyDoce:$("#currency").val()
	}   
	$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span  font-size:13px;font-weight:bold;color:#666666">正在退款，请稍后......</span>' });
	
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/executeRefundProcessAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		cache:false,
		data:obj,
		
		beforeSend:function(request){
		},
		
		error: function(data){
			$.unblockUI();
		 	alert("网络问题，请稍后重试...");
		 	cancel();
		},
		
		success: function(data){
				if(data["rs"]=="Success")
				{
					$.unblockUI();
					alert("退款成功！") ;
					$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="width:40px;  font-size:13px;font-weight:bold;color:#666666">页面正在加载，请稍后......</span>'});
					backSuccess();
	 					
				}else
				{
					$.unblockUI();
					showMessage("退款失败！\n原因："+data["reasion"]+"\n请根据失败原因重新选择操作！","error");
				}
		} 
	});
 	
}
function backSuccess(){
    $.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/refund/RefundAgreeAction.action',
		data:$("#myform").serialize(),
		dataType:'json',
		success:function(data){
			if(data && data.flag === "success"){
			    cancel();
			    $.artDialog.opener.refreshWindow && $.artDialog.opener.refreshWindow();
			}else{showMessage("更新退款数据失败.","error");}
		   
		},
		error:function(){showMessage("更新退款数据失败.","error");}
	})
}

</script>
</head>
<body>
<form id="myform">
	<input type="hidden" name="refund_id" value='<%=refund.get("refund_id",0l) %>'/>
	<input type="hidden" name="schedule_id" value='<%=schedule_id %>'/>
	<input type="hidden" name="schedule_sub_id" value='<%= schedule_sub_id%>'/>
	<input type="hidden" name="oid" value='<%=refund.get("oid",0l) %>'/>
	<input type="hidden" name="create_adid" value='<%=refund.get("create_adid",0l) %>'/>
 	<input type="hidden" name="refundType" id="refundType" value='<%=refund.getString("refund_type") %>'/>
	<input type="hidden" name="note" id="note" value='<%=refund.getString("note") %>'/>
	<input type="hidden" name="txn_id" id="txn_id" value='<%=refund.getString("txn_id") %>'/>
	<input type="hidden" name="currency" id="currency" value='<%=refund.getString("currency_code") %>' />
	<p style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;text-align:center;">审批退款申请</p>
	<table style="width:98%;">
		<tr>
			<td class="td_left">退款类型：</td>
			<td class="td_right">
				<%if(refund.getString("refund_type").equals("Full")){ %>
					全额退款
				<%}else{ %>
					部分退款
				<%} %>
			</td>
		</tr>
		<tr>
			<td class="td_left">退款金额：</td>
			<td class="td_right">
				<%if(refund.getString("refund_type").equals("Full")){ %>
						<%=refund.get("total_amount",0.0d)  %>	
						<input type="hidden" id="amount" name="amount" value='<%=refund.get("total_amount",0.0d) %>'/>
				<%}else{ %>
						<%=refund.get("amount",0.0d)  %>	
						<input type="hidden" id="amount" name="amount" value='<%=refund.get("amount",0.0d) %>'/>
				<%} %>
			</td>
		</tr>
		<tr>
			<td class="td_left">订单：</td>
			<td class="td_right"><a  onclick="windowOpen('<%=refund.get("oid",0l) %>')" href="javascript:void(0)" >
                    <%=  refund.get("oid",0l)%></a>			  </td>
		</tr>
		<tr id="bohui_tr" style="display:none;">
			<td class="td_left">否决理由：</td>
			<td class="td_right">
				<textarea id="reject" name="reject" style="width:200px;height:60px;"></textarea>
				<font style="color:red;">请填写否决理由</font>
			</td>
		</tr>
		<tr>
			<td class="td_left">备注信息：</td>
			<td class="td_right"><%= refund.getString("note") %></td>
		</tr>
		<tr>
			<td class="td_left">退款申请人：</td>
			<td class="td_right"><%=adminMgr.getDetailAdmin(refund.get("create_adid",0l)).getString("employe_name")%></td>
		</tr>
	 </table>
		<p style="margin-top:10px;text-align:right;">
				<input type="button" value="同意" class="normal-green"  onClick="executeRefund();" />
			 	<input id="reject1" type="button" value="否决"  class="normal-white"  onClick="examineProcessRegect();"/> 
			 	<input style="display:none" id="reject2" type="button" value="否决"     class="normal-white"  onClick="examineProcessRegect2();"/> 
			 	<input type="button" value="关闭"   class="normal-white"  onClick="cancel();"/>
		</p>
</form>
 <script type="text/javascript">
//遮罩
$.blockUI.defaults = {
	css: { 
		padding:        '8px',
		margin:         0,
		width:          '170px', 
		top:            '45%', 
		left:           '40%', 
		textAlign:      'center', 
		color:          '#000', 
		border:         '3px solid #999999',
		backgroundColor:'#eeeeee',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':    '10px',
		'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
		'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
	},
	//设置遮罩层的样式
	overlayCSS:  { 
		backgroundColor:'#000', 
		opacity:        '0.6' 
	},
	baseZ: 99999, 
	centerX: true,
	centerY: true, 
	fadeOut:  1000,
	showOverlay: true
};
</script>
</body>
</html>