<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean;"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
 
 
 
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>

<script type="text/javascript">
function cancel(){$.artDialog && $.artDialog.close();}

</script>




</head>
<html>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();"> 

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" > 
  <tr>
  <td  colspan="2"    style="border-bottom:1px #cccccc solid">
  		<table  width="100%" height="20" border="0" cellpadding="5" cellspacing="0" >
  			<tr>
  				<td colspan="2"  align="center" style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  ">   退款申请</td>
  			</tr>
  		</table>  </td>
  </tr>
  <tr><td colspan="2" style="border-bottom:1px #cccccc solid">
	  	<form name="refundForm" id="refundForm">
	  	<table width="100%" border="0" cellspacing="9" cellpadding="2" align="center"  >
	  	  <tr>
	  	  	<td height="30" align="right"> 订单号： </td>
	  	  	<td> <a  onclick="window.open('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$", StringUtil.getString(request,"oid"))%>')" href="#" > <%=StringUtil.getString(request,"oid") %></a> </td>
	  	  </tr>
	  	  <tr>
	  	  	<td height="30" align="right">订单付款金额：</td>
	  	  	<td><%=StringUtil.getString(request,"mc_gross") %>&nbsp;<%=StringUtil.getString(request,"currencyCode") %></td>
	  	  </tr>
          <tr>
            <td  width="31%" align="right" height="30"  >退款类型：</td>
            <td  width="69%" align="left"  >
            	<select name="refundType"   style="WIDTH: 238px"  onChange="checkFull()" >
            		<option value="" >请选择...</option>
               	 	<option value="Full" >全额退款</option>
                	<option value="Partial">部分退款</option>
       		  </select>           
            </td>
          </tr>
          <tr style="display:none" id="amount_id">
            <td  width="31%"  height="30" align="right"  id="td1"  >退款金额：</td>
            <td  width="69%" align="left"  id="td2" ><input type="text" style="WIDTH: 80px"  name="amount"   /><%=StringUtil.getString(request,"currencyCode") %> </td>
          </tr>
          <tr>
	  	  	<td width="31%"  height="30" align="right">备注信息：</td>
            <td width="69%"   align="left"  >
            	<textarea rows="3" cols="35" id="note" name="note"></textarea> 
			</td>
  	      </tr>
        </table>
 	        <input id="create_adid" name="create_adid" type="hidden" value="<%=adminLoggerBean.getAdid()%>" > 
 	         <input id="userAccount" name="userAccount" type="hidden" value="<%=adminLoggerBean.getAccount() %>" > 
			<input name="oid" id="oid" type="hidden" value='<%=request.getParameter("oid") %>'> 
			<input type="hidden" readonly="true"   name="tranID" value="<%=request.getParameter("txn_id").toString() %>"  />
			<input type="hidden" name="totalAmount" value="<%=StringUtil.getString(request,"mc_gross") %>">
			<input type="hidden" name="currencyCode" value="<%=StringUtil.getString(request,"currencyCode") %>">
    	</form> 
	  	  </td>
	 </tr>
	 <tr>
		<td width="51%"   align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">请认真填写申请退款信息</span>         </td>
    	<td width="49%" align="right" class="win-bottom-line">
				<input type="button" value="申请"  class="normal-green" name="ok" onClick="startRefundPro();" />
				<input type="button" value="取消"  class="normal-white" name="cancel" onClick="cancel();" />	</td>	  		  
	 </tr>	
</table>
</body>
</html>

<script language="JavaScript1.2" >
	function applyRefund(uri,_data){
	    $.ajax({
	   	    url:uri,
	   	    dataType:'json',
	   	    data:_data,
	   	    beforeSend:function(request){
	   			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   		},
	   	    success:function(data){
	   		    $.unblockUI();
	   		    if(data && data.flag === "success"){
	   				cancel();
	   				 
		   		}else{
		   		 	showMessage("系统错误.","error");
			    }	  
	   	},
	   	error:function(){$.unblockUI();showMessage("系统错误.","error");}
	   	})
	}
	function startRefundPro()
	{
		var f = document.refundForm;
		if(checkNull())
		{	
		    applyRefund('<%=ConfigBean.getStringValue("systenFolder")%>'+"action/administrator/refund/ApplyRefundAction.action",$("#refundForm").serialize());
			return ;
		  
		}
	}
 
	function checkNull()
	{
		var form = document.refundForm;
		var amount = form.amount.value;
		var note = form.note.value;
		var type = form.refundType.value;
	 	 if(type=="")
		 {
		 	showMessage("请选择退款类型","alert");
		 	return false;
		 } else if(amount==""&&type=="Partial" )
		{
		     showMessage("请填写退款金额！","alert");
			return false;
		}else if(type=="Partial" &&!isNum(amount))
		{
		    showMessage("退款金额请填写数字！","alert");
			return false;
		}else if(note=="")
		{
		    showMessage("请填写备注信息！","alert");
			return false;
		}else 
		{
			return true;
		}
	}
	
	 
	function checkFull(){
		if(document.refundForm.refundType.value=="Partial"){
			 document.getElementById("amount_id").style.display="";
		} else{
			document.getElementById("amount_id").style.display="none";
		}
	}
	
	function isNum(keyW){
		var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
		return( reg.test(keyW) );
  	} 
	$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>


