<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退款处理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script> 

<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<style type="text/css">	
	table{border-collapse:collapse;}
	table td{border:1px solid silver;}
	td.td_left{background:#eeeeee;width:23%;height:30px;line-height:30px;text-align:right;}
	td.td_right{background:#FFFFFF;width:77%;height:30px;line-height:30px;text-indent:5px;}
</style>

<%
	long refund_id = StringUtil.getLong(request,"refund_id");
	DBRow refund = refundMgrZr.getRefundByRefundId(refund_id);
	long schedule_id = StringUtil.getLong(request,"schedule_id");
	long schedule_sub_id = StringUtil.getLong(request,"schedule_sub_id");
	
%>
<script type="text/javascript">
 
function windowOpen(oid){
    window.open('../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val='+oid);
}
</script>
</head>
<body>
<form id="myform">
	<input type="hidden" name="refund_id" value='<%=refund.get("refund_id",0l) %>'/>
	<input type="hidden" name="schedule_id" value='<%=schedule_id %>'/>
	<input type="hidden" name="schedule_sub_id" value='<%= schedule_sub_id%>'/>
	<input type="hidden" name="oid" value='<%=refund.get("oid",0l) %>'/>
	<input type="hidden" name="create_adid" value='<%=refund.get("create_adid",0l) %>'/>
 
	<p style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;text-align:center;border-bottom:1px solid silver;"> 退款申请</p>
 
	<table style="width:98%;">
		<tr>
			<td class="td_left">退款类型：</td>
			<td class="td_right">
				<%if(refund.getString("refund_type").equals("Full")){ %>
					全额退款
				<%}else{ %>
					部分退款
				<%} %>
			</td>
		</tr>
		<tr>
			<td class="td_left">退款金额：</td>
			<td class="td_right">
				<%if(refund.getString("refund_type").equals("Full")){ %>
						<%=refund.get("total_amount",0.0d)  %>	
				<%}else{ %>
						<%=refund.get("amount",0.0d)  %>	
				<%} %>
			</td>
		</tr>
		<tr>
			<td class="td_left">订单：</td>
			<td class="td_right"><a  onclick="windowOpen('<%=refund.get("oid",0l) %>')" href="javacript:void(0)" >
                    <%=  refund.get("oid",0l)%></a>			  </td>
		</tr>

		<tr>
			<td class="td_left">备注信息：</td>
			<td class="td_right"><%= refund.getString("note") %></td>
		</tr>
		<tr>
			<td class="td_left">退款申请人：</td>
			<td class="td_right"><%=adminMgr.getDetailAdmin(refund.get("create_adid",0l)).getString("employe_name")%></td>
		</tr>
		 
	 </table>
		 
</form>
 
</body>
</html>