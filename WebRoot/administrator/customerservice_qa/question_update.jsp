<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>问答修改</title>
 <!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 编辑控件 -->
<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
 <%  
     long catalog_id=StringUtil.getLong(request,"catalog_id");
 	 long question_id = StringUtil.getLong(request,"question_id");
 	 DBRow questionRow = questionMgrZr.getQuestionById(question_id);
 	 String questionCatlogTile=questionCatalogMgrZwb.getTitleByUpdateQuestion(question_id);
 	 String line_value = StringUtil.getString(request,"line_value");
 	 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	 String updateQuestionAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/customerservice_qa/UpdateQuestionAction.action";
 %>

<script type="text/javascript">

(function(){
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },
			 
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
})();

//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target 
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		var td = $("#over_file_td");
		
		td.append(lis); 	 
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_" + fileName;
    var  a = "<p style='color:#F30' class='new' ><a href="+uri+" style='color:#00F' >"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
}
function updateCancel(){
    $.artDialog && $.artDialog.close();
}
function updateQuestion(){
     var question_title = $.trim($("#question_title").val());
     
     if(question_title.length < 1){alert("请输入标题"); return ;}
     if (CKEDITOR.instances.content.getData() == "")
	 {
		alert("请填写问题内容");
		return ;
	 }
	 var ajaxContent=editor.document.getBody().getText();  //取得纯文本
	 $('#ajaxContent').val(ajaxContent);
	 //遮罩
	 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	 $.ajax({
			type:'post',
			url:'<%= updateQuestionAction%>',
			dataType:'json',
			data:$("#update_question_form").serialize(),
			success:function(json){	
				var pageNum=$('#jump_p2', window.parent.document).val();
				//window.parent.go(pageNum);
				window.parent.findQuestion(<%=catalog_id%>,<%=catalog_id%>,"question_update_time",1);
				$.artDialog && $.artDialog.close();
			}
	});
}
</script>
</head>
<body> 
 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td colspan="2" align="center" valign="top">
				<form name="update_question_form" method="post" id="update_question_form" action = '<%= updateQuestionAction%>'>
					<input type="hidden" name="product_catalog_id" value='<%=questionRow.getString("product_catalog_id") %>'/>
					<input type="hidden" name="question_catalog_id" value='<%=questionRow.getString("question_catalog_id") %>'/>
					<input type="hidden" name="question_id" value='<%=questionRow.getString("question_id") %>' />
					<input type="hidden" name="ajaxContent" id="ajaxContent" />
					<table width="98%" border="0" cellspacing="7" cellpadding="2">
						<tr>
							<td width="8%" align="right" valign="middle" class="text-line" >所在目录</td>
							<td>
							     <span style="color:blue; font-weight: bold;"><%=questionCatlogTile %></span>							  
							</td>
						</tr>
						 <tr>
						    <td align="right" valign="middle" class="STYLE1 STYLE2">问题标题</td>
						    <td align="left" valign="middle" ><input style="width: 300px;" id="question_title" name="question_title" type="text" class="input-line" id="question_title"  value='<%= questionRow.getString("question_title") %>'/></td>
						 </tr>
						 <tr>
							  <td align="right" valign="middle" class="STYLE3" >
							    	问题内容
							  </td>
							  <td align="left" valign="middle" >
									<textarea name="content" id="content">
										<%= questionRow.getString("contents")%>
									</textarea>
							   		<script type="text/javascript">
							   		var editor=CKEDITOR.replace( 'content' ,
										{
											 height:150,			 
							        		 filebrowserUploadUrl : '/uploader/upload.php',
							        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
										}
									);
									CKEDITOR.config.toolbarStartupExpanded = false;					
									</script>	
							    </td>
					 	  </tr>
					 	  <tr>
					 			<td  align="right" valign="middle" class="STYLE3" >
					 			 	上传附件
					 			</td>
					 			<td>
					 			 	<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传附件" />
					 			 	<div id="jquery_file_up">
				 						<input type="hidden" name="file_names"/>
				 						<input type="hidden" name="sn" id="sn" value="K_knowledge"/>
				 						<input type="hidden" name="path" value="<%=systemConfig.getStringConfigValue("file_path_knowledge") %>"/>
				 					 
				 					</div>
					 			</td>
					 		</tr>	
					 		<tr>
					 			<td></td>
					 			<td id="over_file_td">
					 				<!-- 读取附件 -->
					 				<%
					 					DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(question_id,FileWithTypeKey.KNOWLEDGE);
					 					 
					 					for(DBRow row : files){
					 						%>
					 						<p><%=row.getString("file_name") %></p>
					 						<% 
					 					}
					 				%>
					 			</td>
					 		</tr>	
					   </table>
					   
				</form>		
		    </td>
		  </tr>
		  <tr>
			  <td width="51%" align="left" valign="middle" class="win-bottom-line" >
			    	&nbsp;
			  </td>
			  <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
				  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="updateQuestion();" id="saveButton" />
				  <input type="button"   value="取消" class="normal-white" onClick="updateCancel();" />
					 
				  &nbsp;
			  </td>
		  </tr>
		</table>
	 
</body>
</html>