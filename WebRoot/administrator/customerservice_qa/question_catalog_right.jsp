<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<!-- table 斑马线 -->
<script src="../js/question/zebra.js" type="text/javascript"></script>
<link href="../js/question/zebra.css" rel="stylesheet" type="text/css" />

<!-- 所有产品 -->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<%  
    long productLineId=StringUtil.getLong(request,"productLine");
	String backurl = StringUtil.getString(request,"backurl");
%>
<style>
 <!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<script>



</script>
</head>
<body>
<!-- 过滤 -->
<div style="width:97%; margin:0px auto; border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
      <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
      	<tr>
      		<td colspan="2" style=" padding-left:6px; font-size:12px; font-weight: bold; color:#00F" id="pLineClick">
      	        
      		</td>
      	</tr>
      	<tr>
      		<td width="420px;" height="30xp;" style=" padding-left:6px;">
      		    标题<input name="title" type="checkbox" id="title" />
      		    原文<input name="question_title" type="checkbox" id="question_title" />
      		    描述<input name="question_describe" type="checkbox" id="question_describe"/>
      		    附件<input name="appendix" type="checkbox" id="appendix" />      
      		</td>
      		<td width="510px;" height="30xp;" style="padding-right:10px;" id="categorymenu_td" class ="categorymenu_td" >
      			<!-- 商品分类 -->
      			<ul id="categorymenu" class="mcdropdown_menu">
		  <li rel="0">所有商品分类</li>
		  <%DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
		    for (int i=0; i<c1.length; i++){
				out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
				  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
				  if (c2.length>0){
				  		out.println("<ul>");	
				  }
				  for (int ii=0; ii<c2.length; ii++){
						out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
							DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
							  if (c3.length>0){
									out.println("<ul>");	
							  }
								for (int iii=0; iii<c3.length; iii++){
										out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
										out.println("</li>");
								}
							  if (c3.length>0){
									out.println("</ul>");	
							  }
						out.println("</li>");				
				  }
				  if (c2.length>0){
				  		out.println("</ul>");	
				  }
				out.println("</li>");
		  }%>
	</ul>
	<input type="text" name="category" id="category" value="" />
                     	<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />                          
      		</td>				          
      	</tr>
      	<tr>
      		<td colspan="2">
      		
       		<table width="100%" border="0" cellpadding="0" cellspacing="0">
       			<tr>
       				<td style="padding-left:7px" width="45%">
       					产品：<span class="STYLE1 STYLE2">
				      		<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" onBlur="checkProduct()"/>
				      		<span id="errorMessage">&nbsp;</span>
			      	 </span>
			      	 <input type="hidden" id="backurl" name="backurl" value="<%=backurl%>">
			         <input type="hidden" id="product_id" name="product_id"/>	
       				</td>
       				<td>
       					<!-- 搜索文本框 -->
		          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
		             <tr>
		            	<td width="18%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
			            	 <div id="easy_search_father">
										<div id="easy_search" style="margin-top:-2px;"><a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
							  </div>
			            	  <table width="400" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="418">
											<div  class="search_shadow_bg">
											 <input name="seachInput" id="seachInput" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
											</div>
										</td>
									</tr>
							  </table>	
						  </td>	    
		              </tr>
		            </table>
       				  </td>
       			   </tr>
       		  </table>
       		 <script type="text/javascript">
       				 	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");		
       		 </script>	
			</td>
      	</tr>
      </table>			         			            							  
   </div>
   <br/>
   <!-- 问题容器 -->
<div id="container" style="width:97%; margin:0px auto; border:2px #dddddd solid; padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
  
</div>   

<script type="text/javascript">

//验证
function checkProduct(){
	var p_name = $("#p_name").val();
	if(p_name!=""){
		var para ="p_name="+p_name;	
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
				},
				error: function(){
					alert("提交失败，请重试！");
				},
				success: function(date){
					if(date["product_id"]>0){
						$("#product_id").val(date["product_id"]);				
						$("#errorMessage").html("");
					}else{
						$("#errorMessage").html("<img src='../imgs/product/warring.gif' width='14' height='12' align='absmiddle'><span style='color:#FF0033'><strong style='font-size: 11px;'>无此商品</strong></span>");
					}
				}
			});
	}else{
	
	}
}

//点击产品线返填商品类型
function ajaxLoadCatalogMenuPage(id){
	    //选择子级值 下拉列表
	    cleanSearchKey("");
		var para = "id="+id;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
			},
			error: function(){
			},
			success: function(html)
			{
				$("#categorymenu_td").html(html);
			}
		});
}

//选择子级值 下拉列表
function cleanSearchKey(divId){
	$("#"+divId+"search_key").val("");
}

//加载商品分类控件
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name){
					$("#filter_pcid").val(id);
				}
});
$("#category").mcDropdown("#categorymenu").setValue(0);
</script>

       
</body>

</html>
