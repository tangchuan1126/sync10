<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<!-- tree -->
<script type="text/javascript" src="../js/question/dtree.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../js/question/dtree.css" type="text/css"></link>  

<!-- table 斑马线 -->
<script src="../js/question/zebra.js" type="text/javascript"></script>
<link href="../js/question/zebra.css" rel="stylesheet" type="text/css" />


<script>



</script>
</head>
<body>
    <!-- 左侧树 -->
	<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0"  class="zebraTable">
		<tr>
		   <td>
			   <script type="text/javascript">
					d = new dTree('d');
					var productLine='产品线</td>'+
										  '<td align="left" valign="middle" width="20%">&nbsp;</td>'+
								    	  '<td align="left" valign="middle" width="8%">&nbsp;</td>'+
										  '<td valign="middle" width="22%" align="left" >'+
											 '&nbsp;'+
										  '</td>'+
										'</tr>'+
									 '</table>';
					d.add(0,-1,productLine);
					<% DBRow [] pLines = productLineMgrTJH.getAllProductLine();%>
					<% for(int t=0;t<pLines.length;t++){%>
					   var productLineId=<%=pLines[t].get("id",0l)%>;			
					   var lineName="";
					   <%if(pLines[t].getString("name").length()>7){%>
					      lineName="<%=pLines[t].getString("name").substring(0,7)%>..."
					   <%}else{%>
					      lineName="<%=pLines[t].getString("name")%>";
					   <%}%>
					   var line='<span onClick="window.parent.pLineClick(1,this.title,'+productLineId+','+productLineId+')" title="<%=pLines[t].getString("name")%>" >'+lineName+'</span></td>'+
						  	    '<td valign="middle" width="20%" align="left">'+
						 			'<input  type="button" style="background-image:url(imgs/button_questions_small.gif); border-width:0px; width:12px; height:12px;" onClick="window.parent.add_question_classify('+productLineId+','+"'<%=pLines[t].getString("name")%>'"+')" />'+
						  	    '</td>'+
						  	  '</tr>'+
						   '</table>';
					   d.add(<%=pLines[t].getString("id")%>,0,line,'javascript:do(0)'); 
					     <% DBRow[] questionCatalogs=questionCatalogMgrZwb.getQuestionCatalogByPid(pLines[t].get("id",0l));%>											 
						     <%for(int j=0;j<questionCatalogs.length;j++){%>
						         <%if(questionCatalogs[j].get("grade",0l)==1){%>
								         var catalogId=<%=questionCatalogs[j].getString("question_catalog_id")%>;
								         var catalogParentId=<%=questionCatalogs[j].getString("parent_id")%>
								         var catalogName="";
							             <%if(questionCatalogs[j].getString("catalog_title").length()>7){%>
							            	 catalogName="<%=questionCatalogs[j].getString("catalog_title").substring(0,7)%>...";
							             <%}else{%>
							            	 catalogName="<%=questionCatalogs[j].getString("catalog_title")%>";
							             <%}%>
								         var catalogTitle='<a onClick="window.parent.pLineClick('+"'<%=pLines[t].getString("name")%>'"+',this.title,'+productLineId+','+catalogId+')" href="javascript:void(0);" title="<%=questionCatalogs[j].getString("catalog_title")%>">'+catalogName+'</a>'+
			                                '<td valign="middle" width="20%" align="left">'+
	                                    					'<input  type="button" style="background-image:url(imgs/button_questions_small.gif); border-width:0px; width:12px; height:12px;" onClick="window.parent.add_queston_classify_small('+catalogId+','+productLineId+','+"'<%=pLines[t].getString("name")%>'"+','+"'<%=questionCatalogs[j].getString("catalog_title")%>'"+')" />'+
		                                    '</td>'+
								           '</tr>'+
								         '</table>';
									    d.add(<%=questionCatalogs[j].getString("question_catalog_id")+"-"+pLines[t].getString("id")%>,<%=questionCatalogs[j].getString("parent_id")%>,catalogTitle,'','','','img/catalogOpen.gif','img/catalog.gif');
							     <%}else if(questionCatalogs[j].get("grade",0l)==2){%>
									     var catalogId=<%=questionCatalogs[j].getString("question_catalog_id")%>;
								         var catalogParentId=<%=questionCatalogs[j].getString("parent_id")%>
									     var catalogName="";
							             <%if(questionCatalogs[j].getString("catalog_title").length()>7){%>
							            	 catalogName="<%=questionCatalogs[j].getString("catalog_title").substring(0,7)%>...";
							             <%}else{%>
							            	 catalogName="<%=questionCatalogs[j].getString("catalog_title")%>";
							             <%}%>
							             var catalogTitleSmall='<a onClick="window.parent.getAllTitle(<%=questionCatalogs[j].getString("question_catalog_id")%>,'+productLineId+')" href="javascript:void(0);" title="<%=questionCatalogs[j].getString("catalog_title")%>">'+catalogName+'</a>'+
					                                '<td valign="middle" width="20%" align="left">'+
	                                   					'<input  type="button" style="background-image:url(imgs/button_questions_small.gif); border-width:0px; width:12px; height:12px;" onClick="window.parent.add_queston_classify_very_small('+catalogId+','+productLineId+','+"'<%=pLines[t].getString("name")%>'"+','+catalogParentId+','+"'<%=questionCatalogs[j].getString("catalog_title")%>'"+')" />'+
				                                    '</td>'+
										           '</tr>'+
										         '</table>';
							            d.add(<%=questionCatalogs[j].getString("question_catalog_id")+"-"+pLines[t].getString("id")%>,<%=questionCatalogs[j].getString("parent_id")+"-"+pLines[t].getString("id")%>,catalogTitleSmall,'','','','img/empty.gif','img/empty.gif');
							     <%}%>
							 <%}%>     
					<%}%>
					document.write(d);
					//打开某级 时同级关闭
					//d.config.closeSameLevel=true;
					//d.config.useCookies=false;
				</script>
			</td>
		 </tr>
	   </table>       
  </body>
</html>
