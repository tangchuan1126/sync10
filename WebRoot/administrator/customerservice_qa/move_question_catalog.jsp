<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>


<%
   long questionId=StringUtil.getLong(request,"questionId");
   String questionCatlogTile=questionCatalogMgrZwb.getTitleByUpdateQuestion(questionId);
   DBRow row=questionCatalogMgrZwb.getQuestionByCatlogId(questionId);
   DBRow[] catalogRow=questionCatalogMgrZwb.moveGetQuestionCatalogByLineId(row.get("product_line_id",0l));
%>

<script>

	(function(){
		$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },
			 
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
	})();

 function moveQuestion(){
	 var value= $("input[name='moveCatalogId']:checked").val();
	 if(value==undefined){
        alert("请选择一个问题分类");
        return false;
	 }
	    //遮罩
	        $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   	$.ajax({
	   		type:'post',
	   		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/MoveQuestionCatalogAction.action',
	   		dataType:'json',
	   		data:$("#myForm").serialize(),
	   		success:function(msg){
		   		if(msg.flag=="ok"){
	   				$.artDialog && $.artDialog.close();
		   		}else{
			   		alert("转移出错");
		   		}
	   		}
	   	});
  
 }
</script>
</head> 

<body>
	<form action="" id="myForm">
		<div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		   <div style="font-size:13px; font-weight: bold;">当前问题位置</div>
		   <hr size="1px;" color="#dddddd" />
		   <span style="font-size:13px; font-weight: bold; color:#00F"><%=questionCatlogTile%></span> 
		</div>
		<br/>
		<input type="hidden" value="<%=row.get("question_id",0l) %>"  name="questionId"/>
		<input type="hidden" name="path" value="<%=systemConfig.getStringConfigValue("file_path_knowledge") %>"/>
		<div align="left" style="height:305px;border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		   <div align="center" style="width:240px; height:300px; float:left; background-color:#FFF; border:2px #dddddd solid;">
		  		 <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
		  		 	<tr>
		  		 		<td align="left" valign="middle">
		  		 			<span style="font-weight:bold;font-size:14px;padding-left:5px; color:#036;"><%=row.getString("question_title") %></span>
		  		 		</td>
		  		 	</tr>
		  		 </table>
		   </div>
		   <div align="center" style="width:90px; height:300px; float:left; line-height:300px;">
		    	&nbsp;<img alt="" src="./imgs/jiantou.gif">&nbsp;
		   </div>
		   <div style="background-color:#FFF;width:310px; border:2px #dddddd solid; height:300px; float:left; overflow:auto">
		   		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zebraTable">
		   		<% for(int i=0;i<catalogRow.length;i++){ %>
				  <tr>
				    <td height="30px;"><input name="moveCatalogId" type="radio" value="<%=catalogRow[i].get("question_catalog_id",0l) %>" /></td>
				    <td height="30px;"><%=catalogRow[i].getString("catalog_title") %></td>
				  </tr>
				<%} %>
				</table>
		   </div>
		   <div style="width:100px; height:300px; float:left; line-height:300px; ">
		   		&nbsp;<input id="saveButton" class="normal-green" type="button" onclick="moveQuestion()" value="确定" name="">&nbsp;
		   </div>
		</div>
	</form>
</body>
</html>
