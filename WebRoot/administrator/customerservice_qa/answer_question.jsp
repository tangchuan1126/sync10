<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>回答问题</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css?v=1" />
<%
	String question_title = StringUtil.getString(request,"question_title");
	String backurl = StringUtil.getString(request,"backurl"); 
	String question_id = StringUtil.getString(request,"question_id");
	 String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
%>
<script>
//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target 
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		var td = $("#over_file_td");
		
		td.append(lis); 	 
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_" + fileName;
    var  a = "<p style='color:#F30' class='new' ><a href="+uri+" style='color:#00F' >"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
}

function answerQuestion()
{
	var f = document.answer_form;
	
	 if (CKEDITOR.instances.answer_content.getData() == "")
	 {
	 	
		alert("请填写答案的内容");
	 }
	 else
	 {
		f.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AddQuestionAnswerAction.action";
		f.submit();	
	 }
}
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	
	$().ready(function() {

	function log(event, data, formatted) {

		
	}

	});
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客服问答 »  回答问题</td>
  </tr>
</table>
<br>
<form name="answer_form" method="post" action="" >

<table width="98%" border="0" cellspacing="7" cellpadding="2">
  <tr>
    <td align="left" width="60px" valign="middle" class="STYLE1 STYLE2">问题标题</td>
    <td align="left" valign="middle" style="font-size:12px; font-weight: bold;"><%=question_title %></td>
    <td align="right">
    </td>
     </tr>
   <tr>
							  <td align="right" width="60px" valign="middle" class="STYLE3" >
							    	问题答案
							  </td>
							  <td align="left" valign="middle" colspan="2">
							    	<textarea  name="answer_content" id="answer_content" ></textarea>
							   		<script type="text/javascript">
									CKEDITOR.replace( 'answer_content' ,
										{
											 height:200,			 
							        		 filebrowserUploadUrl : '/uploader/upload.php',
							        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
										}
									);				
									</script>	
							    </td>
					 		</tr>
					 		<tr>
					 			<td  align="right" valign="middle" class="STYLE3" >
					 			 	上传附件
					 			</td>
					 			<td id="over_file_td">
					 			 	<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传附件" />
					 			 	<div id="jquery_file_up">
				 						<input type="hidden" name="file_names"/>
				 						<input type="hidden" name="sn" id="sn" value="K_knowledge"/>
				 						<input type="hidden" name="path" value="<%=systemConfig.getStringConfigValue("file_path_knowledge") %>"/>
				 						<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.KNOWLEDGE %>"/>
				 					</div>
					 			</td>
					 		</tr>		
</table>
<input type="hidden" name="question_id" value="<%=question_id %>"/>
<input type="hidden" name="answer_content" id="answer_content"/>
<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request) %>">
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">
    	&nbsp;
    </td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="提交回答" class="normal-green" onClick="answerQuestion();" id="saveButton">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table> 
<div style="display:none" id="keep_alive"></div>
</body>
<script>
function closeWin()
{
	$.artDialog.close();
}
</script>
</html>
