<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String backurl = StringUtil.getString(request,"backurl");
long productLineId=StringUtil.getLong(request,"productLine");
long pcid = StringUtil.getLong(request,"pcid");
String pro_line_id = StringUtil.getString(request,"pro_line_id");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新建问题</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>

<!-- 所有产品 -->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />

<script>
function addQuestion()
{
	var f = document.add_question_form;
	
	 if(f.p_name.value =="")
	 {
	 	alert("请填写商品名称");
	 }
	 else if (f.question_title.value=="")
	 {
		alert("请填写问题标题");
	 }
	 else if (CKEDITOR.instances.content.getData() == "")
	 {
	 	
		alert("请填写问题内容");
	 }
	 else
	 {
	 	
		document.add_question_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/addQuestion.action";
		document.add_question_form.submit();
		<%
	 		if(backurl.equals(""))
	 		{
	 	%>
	 		window.close(); 
	 	<%
	 		}
	 	%>
	 }
}
	
	var keepAliveObj=null;
	function keepAlive(){
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	
	$().ready(function() {

	function log(event, data, formatted) {

		
	}
	
	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
});
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
		<form name="add_question_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/addQuestion.action" >
			<table width="98%" border="0" cellspacing="7" cellpadding="2">
				<tr>
					<td align="right" valign="middle" class="STYLE1 STYLE2">产品线</td>
					<td>
						 <!-- 产品线 -->
			             <ul id="productLinemenu" class="mcdropdown_menu">
				    			<li rel="0">所有产品线</li>
									  <%
									  	  DBRow ca1[] = assetsCategoryMgr.getAssetsCategoryChildren(0,"2");
										  for (int i=0; i<ca1.length; i++){
												  DBRow[] ca2 = null;
												  ca2 = assetsCategoryMgr.getAssetsCategoryChildren(ca1[i].get("id",0l),"3");													
												  for (int ii=0; ii<ca2.length; ii++){
													out.println("<li rel='"+ca2[ii].get("id",0l)+"'> "+ca2[ii].getString("chName"));		
													out.println("</li>");				
												  }	
									  }%>
						  </ul>
								<input type="text" name="productLine" id="productLine" value=""  />
							    <input type="hidden" name="product_line_id" id="product_line_id" value="0" />
					      <script>
					  				$("#productLine").mcDropdown("#productLinemenu",{
											allowParentSelect:true,
											select: function (id,name){
												 $("#product_line_id").val(id);
											}
									});
									$("#productLine").mcDropdown("#productLinemenu").setValue(0);				
								<%if (productLineId!=0){%>
									$("#productLine").mcDropdown("#productLinemenu").setValue(<%=productLineId%>);
								<%}else{%>
									$("#productLine").mcDropdown("#productLinemenu").setValue(0);
								<%}%>
				  		  </script>
					</td>
				</tr>
				<tr>
					<td align="right" valign="middle" class="STYLE1 STYLE2">产品分类</td>
					<td>
						<!-- 商品分类 -->
	          			<ul id="categorymenu" class="mcdropdown_menu">
							  <li rel="0">所有商品分类</li>
							  <%DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
							  for (int i=0; i<c1.length; i++){
									out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
									  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
									  if (c2.length>0){
									  		out.println("<ul>");	
									  }
									  for (int ii=0; ii<c2.length; ii++){
											out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
												DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
												  if (c3.length>0){
														out.println("<ul>");	
												  }
													for (int iii=0; iii<c3.length; iii++){
															out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
															out.println("</li>");
													}
												  if (c3.length>0){
														out.println("</ul>");	
												  }
											out.println("</li>");				
									  }
									  if (c2.length>0){
									  		out.println("</ul>");	
									  }
									out.println("</li>");
							  }%>
						</ul>
						<input type="text" name="category" id="category" value="" />
                          	<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
						<script type="text/javascript">
							$("#category").mcDropdown("#categorymenu",{
									  allowParentSelect:true,
									  select: 
											function (id,name){
												$("#filter_pcid").val(id);
											}
							});
							
							$("#productLine").mcDropdown("#productLinemenu",{
									allowParentSelect:true,
									  select: 
											function (id,name){
												$("#filter_productLine").val(id);
												if(id!=0){
													cleanSearchKey("");
													ajaxLoadCatalogMenuPage(id);
												}
											}									
							});
							<%if (pcid>0){%>
								$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
							<%}else{%>
								$("#category").mcDropdown("#categorymenu").setValue(0);
							<%}%> 	
							<%if (!pro_line_id.equals("0") && !pro_line_id.equals("")){%>
								$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
							<%}else{%>
								$("#productLine").mcDropdown("#productLinemenu").setValue(0);
							<%}%>
						</script>
					</td>
				</tr>
				<tr>
				    <td width="8%" align="right" valign="middle" class="text-line" ><span class="STYLE1 STYLE2">商品名称</span></td>
				    <td align="left" valign="middle" ><span class="STYLE1 STYLE2">
				      <input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" onBlur="checkProduct()"/></span>
				    </td>
				 </tr>
				 <tr>
				    <td align="right" valign="middle" class="STYLE1 STYLE2">问题标题</td>
				    <td align="left" valign="middle" ><input style="width: 300px;" name="question_title" type="text" class="input-line" id="question_title" ></td>
				 </tr>
				 <tr>
					  <td align="right" valign="middle" class="STYLE3" >
					    	问题内容
					  </td>
					  <td align="left" valign="middle" >
					    	<textarea  name="content" id="content" ></textarea>
					   		<script type="text/javascript">
							CKEDITOR.replace( 'content' ,
								{
									 height:200,			 
					        		 filebrowserUploadUrl : '/uploader/upload.php',
					        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
								}
							);				
							</script>	
					    </td>
			 		</tr>
			   </table>
			   <input type="hidden" id="backurl" name="backurl" value="<%=backurl%>">
			   <input type="hidden" id="product_id" name="product_id"/>
		</form>		
    </td>
  </tr>
  <tr>
	  <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">
	    	&nbsp;
	  </td>
	  <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
		  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addQuestion();" id="saveButton">
			 <%if(!backurl.equals("")){%>
		  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='question_index.html'">
			 <% }%>
	  </td>
  </tr>
</table>
<script type="text/javascript">
	function checkProduct()
	{
		var p_name = $("#p_name").val();
		if(p_name!="")
		{
		var para ="p_name="+p_name;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date)
				{
					if(date["product_id"]>0)
					{
						$("#product_id").val(date["product_id"]);
						$("#saveButton").css({display:"inline"});
						$("#errorMessage").html("");
					}
					else
					{
						$("#errorMessage").html("<img src='../imgs/product/warring.gif' width='16' height='15' align='absmiddle'><span style='color:#FF0033'><strong style='font-size: 20px;'>无此商品，无法创建问题</strong></span>");
						$("#saveButton").css({display:"none"});
					}
					
				}
			});
		}
		else
		{
			alert("请填写商品名称!");
		}
	}
</script>
<div style="display:none" id="keep_alive"></div>
</body>
</html>
