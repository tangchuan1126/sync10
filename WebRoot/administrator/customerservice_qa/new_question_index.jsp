<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.QAKey"%>
<%@ include file="../../include.jsp"%> 
<%
long pcid = StringUtil.getLong(request,"pcid");
String cmd = StringUtil.getString(request,"cmd");
String key = StringUtil.getString(request,"key");
String pname = StringUtil.getString(request,"pname");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[];

if (cmd.equals("filter")&&(pcid!=0||pname.equals("")))
{
	rows = qaMgrZJ.searchQuestionByCatalog(pcid,pname,pc,QAKey.UNANSWERED);
}
else if(cmd.equals("filter_product"))
{
	rows = qaMgrZJ.searchQuestionByProductId(pcid,pc,QAKey.UNANSWERED);
}
else if (cmd.equals("search"))
{
	rows = qaMgrZJ.searchQuestionByIndexKey(key,pcid,pname,pc,QAKey.UNANSWERED);
}
else
{
	rows = qaMgrZJ.getAllQuestion(pc,QAKey.UNANSWERED);
}

Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
QAKey qakey = new QAKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>
		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

		
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<script language="javascript">
<!--

function filter()
{
		document.filter_search_form.cmd.value = "filter";		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();
		document.filter_search_form.pname.value = $("#p_name").val();	
		document.filter_search_form.submit();
}

function search()
{
	if ($("#search_key").val()==""&&$("#p_name").val()=="")
	{
		filter();
	}
	else
	{
		document.filter_search_form.cmd.value = "search";		
		document.filter_search_form.key.value = $("#search_key").val();		
		document.filter_search_form.pcid.value = $("#filter_pcid").val();
		document.filter_search_form.pname.value = $("#p_name").val();	
		document.filter_search_form.submit();
	}
}

function addQuestion()
{
	document.add_question_form.submit();
}

function modQuestion(question_id)
{
	document.mod_form.question_id.value = question_id;
	document.mod_form.submit();
}

function closeWin()
{
	tb_remove();
}

function openCatalogMenu()
{
	$("#category").mcDropdown("#categorymenu").openMenu();
}
//-->

</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客服问答 »   新问题</td>
  </tr>
</table>
<br>
 <form name="add_question_form" method="post" action="add_question.html" >
 <input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
 </form>
 

<form name="mod_form" method="post" action="mod_question.html">
	<input type="hidden" name="question_id">
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
</form>


<form name="filter_search_form" method="get"  action="new_question.html">
<input type="hidden" name="cmd" >
<input type="hidden" name="pcid" >
<input type="hidden" name="pname"/>
<input type="hidden" name="key" >

</form>

<form name="switch_alive_form" method="post">
<input type="hidden" name="pc_id">
<input type="hidden" name="curAlive">
</form>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td width="423" rowspan="2" align="left" bgcolor="#eeeeee" style="padding-left:10px;">
	<div style="padding-bottom:5px;color:#666666;font-size:12px;">
	可以选择一个商品分类：	</div>

	<ul id="categorymenu" class="mcdropdown_menu">
	  <li rel="0">所有分类</li>
	  <%
	  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value="" />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	  

<script>
$("#category").mcDropdown("#categorymenu",{
		allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
				}

});
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 


</script>	</td>
    <td height="25px;" align="left" valign="top" bgcolor="#eeeeee" style="border-bottom:1px #dddddd solid">&nbsp;&nbsp;商品名称&nbsp;&nbsp;&nbsp;<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:255px;font-size:10px;height:17px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" value="<%=pname %>"/>&nbsp;&nbsp;&nbsp;
      <input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()"></td>
  </tr>
  <tr>
    <td valign="bottom" bgcolor="#eeeeee">&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;关键字&nbsp;&nbsp;&nbsp;<input type="text" name="search_key" id="search_key" value="<%=key%>" style="width:255px;">
&nbsp;&nbsp;&nbsp;</td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">

	
    <tr> 
        <th class="left-title">问题标题</th>
        <th align="left"  class="right-title" style="vertical-align: center;text-align: left;">类别</th>
        <th align="center"  class="right-title" style="vertical-align: center;text-align: center;">最后更新时间</th>
        <th class="right-title" style="vertical-align: center;text-align: center;">提问人</th>
        <th class="right-title" style="vertical-align: center;text-align: center;">问题状态</th>
    </tr>
    <%
    	if(rows!=null)
    	{
    		for(int i = 0;i<rows.length;i++)
    		{
    %>
    	<tr>
    		<td height="75"><a href="javascript:void(0)" onClick="location='answer-<%=rows[i].getString("question_id")%>.html'"><%=rows[i].getString("question_title") %></a></td>
    		<td>
    		<span style="color:#999999">
		  <%
		  DBRow product = productMgr.getDetailProductByPcid(rows[i].get("product_id",0l) );
		  
		  DBRow allFather[] = tree.getAllFather(product.get("catalog_id",0l));
		  for (int jj=0; jj<allFather.length-1; jj++)
		  {
		  	out.println("<a class='nine4' href='?cmd=filter&pcid="+allFather[jj].getString("id")+"&key="+key+"'>"+allFather[jj].getString("title")+"</a><br>");
		
		  }
		  %>
		  </span>
		  
		  <%
		  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(product.getString("catalog_id")));
		  if (catalog!=null)
		  {
		  	out.println("<a href='?cmd=filter&pcid="+catalog.getString("id")+"&key="+key+"'>"+catalog.getString("title")+"</a>");
		  }
		  %>	
	  	  <br/>
	  	  <a href="?cmd=filter_product&pcid=<%=product.get("pc_id",0l) %>&key=<%=key%>" style="font-size:12px;color: black; "><%=product.getString("p_name")%></a>    		</td>
    		<td align="center"><%=rows[i].getString("question_update_time") %></td>
    		<td align="center"><%=rows[i].getString("questioner") %></td>
    		<td align="center">
    			<%=qakey.getQuoteStatusById(rows[i].getString("question_status")) %>
    		</td>
   		</tr>
    <%		}
    	}
    %>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="new_question.html">
    <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="pcid" value="<%=pcid%>">
<input type="hidden" name="key" value="<%=key%>">

  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<script type="text/javascript">
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
});

$().ready(function() {

	function log(event, data, formatted) {
		
	}

	$("#search_key").autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/getKeyIndex.action", {
		minChars: 0,
		width: 254,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		selectFirst:false,
		updownSelect:true,
		
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<font style="font-size:12px;" color="#FF3300">$1</font>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:"<font style='font-size:12px;'>"+data[i].keyword+"(大约"+data[i].count+"条记录)</font>",
    				value:data[i].keyword,
        			result:data[i].keyword
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
	});
	
	//搜索回车支持
	$("#p_name").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});
	
	$("#search_key").keydown(function(event){
		if (event.keyCode==13)
		{
			search();
		}
	});
});
</script>
</body>
</html>
