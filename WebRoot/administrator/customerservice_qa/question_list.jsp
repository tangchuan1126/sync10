<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<%@page import="com.cwc.app.lucene.zr.QuestionIndexMgr"%>
<html>
<head>
 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<title>问答</title>
 
 <%
  	PageCtrl pc = new PageCtrl();
  	pc.setPageNo(StringUtil.getInt(request,"p"));
  	pc.setPageSize(5);	
   
   	long product_line = StringUtil.getLong(request,"product_line");
  	long product_id = StringUtil.getLong(request,"product_id");
  	String search_key = StringUtil.getString(request,"search_key");
  	long catalog_id = StringUtil.getLong(request,"catalog_id");
  	long category = StringUtil.getLong(request,"category");
  	String cmd = StringUtil.getString(request,"cmd");
  	String product_line_ids = StringUtil.getString(request,"product_line_ids");
  	
  	
  	long[] question_catalog_ids = null ;
  	if(catalog_id != 0l){
  		question_catalog_ids = questionCatalogMgrZwb.getAllQuestionByQid(catalog_id); 
  	}
   	long[] productCatalogId = null ;
   	//如果是page_query
   	//应该是如果category为0的话,在查询的时候就不要添加prodcutCataLogId.
   	
  	if( category != 0l){		 
  		 if(cmd.equals("page_query") && product_line_ids.length() > 0){
  	 String[] arrayProductLine = product_line_ids.split(",");
  	 productCatalogId = new long[arrayProductLine.length];
  	 for(int index = 0 , count = arrayProductLine.length ; index < count ; index++ ){
  		 productCatalogId[index] = Long.parseLong(arrayProductLine[index]);
  	 }
  		 }else if(cmd.equals("click_query") || (cmd.equals("page_query") && product_line_ids.length() < 1) ){ 
  	 DBRow[] productCatalogs = questionMgrZr.getSubProductCatalogByProductLineId(product_line);
  	 productCatalogId = new long[productCatalogs.length];
  	 for(int index = 0 , count = productCatalogs.length ; index < count  ; index++ ){
  		 productCatalogId[index] = productCatalogs[index].get("id",0l);
  	 }
  		 }
   	}
  	String deleteQuestionUrl = ConfigBean.getStringValue("systenFolder") + "action/administrator/customerservice_qa/DeleteQuestionAction.action";
  	String updateQuestionUrl = ConfigBean.getStringValue("systenFolder") + "administrator/customerservice_qa/question_update.html";
  	String AjaxModAnswerAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/customerservice_qa/AjaxModAnswerAction.action";
  	String AjaxQuestionAppendixUrl = ConfigBean.getStringValue("systenFolder") + "action/administrator/customerservice_qa/AjaxQuestionAppendixAction.action";
  	String deleteQuestionAnswerUrl = ConfigBean.getStringValue("systenFolder") + "action/administrator/customerservice_qa/DeleteQuestionAnswerAction.action";
  	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
  	String AjaxAnswerAppendixUrl = ConfigBean.getStringValue("systenFolder") +"action/administrator/customerservice_qa/AjaxAnswerAppendixAction.action";
  	String AnswerQuestionUrl = ConfigBean.getStringValue("systenFolder") + "administrator/customerservice_qa/answer_question.html";
  	String moveQuestionCatalogUrl= ConfigBean.getStringValue("systenFolder") + "administrator/customerservice_qa/move_question_catalog.html";
  	
  	String quFen=StringUtil.getString(request,"quFen");
  	String single=StringUtil.getString(request,"single");
  	long question_id=StringUtil.getLong(request,"question_id");
  	String genjin=StringUtil.getString(request,"genjin");
  	
  	DBRow[] rows=null;
  	if(quFen!=null && quFen!=""){
  		rows = questionCatalogMgrZwb.getAllQuestionByFlool(catalog_id,quFen,pc);
  	}else if(single!=null && single.equals("single") && single!=""){
  		rows=questionCatalogMgrZwb.getQuestionSingle(question_id);
  	}else if(genjin!=null && genjin!="" && genjin.equals("genjin")){
  		rows=questionCatalogMgrZwb.getQuestionproductLineStatus(catalog_id,pc);
  	}else{	
  		rows = QuestionIndexMgr.getInstance().getSearchResults(search_key,product_id,productCatalogId,question_catalog_ids,pc);
  	}
  	
  	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
  	long userId = adminLoggerBean.getAdid();
  	DBRow[] callRow=questionCatalogMgrZwb.getAllQuestionCallById(userId);
  %>
<link rel="stylesheet" id="cs" type="text/css" />

<script type="text/javascript">

(
	function(){
		if((screen.width == 1024) && (screen.height == 768)){
			$('#cs').attr("href","../js/question/hidderDiv1024.css");
		}else{
			$('#cs').attr("href","../js/question/hidderDiv.css");
		}	
    } 
)();

(function(){
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#ffffff',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },
			 
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
})();
//文件上传
function uploadFile(_target,questionId){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"all",
	     limitSize:2,
	     target:_target 
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_answer_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 uri += "&questionId="+questionId;
	 
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}

	//jquery file up 回调函数
	function uploadFileCallBack(fileNames,target,questionId){
	    $("p.new").remove();
	    var targetNode = $("#"+target);
		$("input[name='file_names']",targetNode).val(fileNames);
		if($.trim(fileNames).length > 0 ){
			var array = fileNames.split(",");
			var lis = "";
			for(var index = 0 ,count = array.length ; index < count ; index++ ){
				var a =  createA(array[index]) ;
				 
				if(a.indexOf("href") != -1){
				    lis += a;
				}
			}
			var td = $("#over_file_td"+questionId+"");
			
			td.append(lis); 	 
		} 
	}
	
	function createA(fileName){
	    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
	    var showName = $("#sn").val()+"_" + fileName;
	    var  a = "<p style='color:#F30' class='new' ><a href="+uri+" style='color:#00F' >"+showName+"</a>&nbsp;&nbsp;(New)</p>";
	    return a ;
	}

	function deleteQuestion(question_id,productLineId){
		if(confirm("确认删除问题吗？")){
			$.ajax({
				url:'<%= deleteQuestionUrl%>' + "?question_id="+question_id,
				dataType:'json',
				success:function(data){
					if(data && data.flag === "success"){
						var newcount= $('#newcount'+productLineId+'').html();
		  				if(newcount==1){
		  					$('#newproductline'+productLineId+'').html('');
		  				}else{
		  					 newcount=newcount-1;
			  				$('#newcount'+productLineId+'').html(newcount);
		  				}
						var pageNum=$('#jump_p2').val();
						go(pageNum);
					}
				}
			});
		}else{
			return false;
		}		
	}
	function updateQuestion(question_id,catalog_id){
	   var line_value = $("#pLineClick").html();
	   uri =  '<%= updateQuestionUrl%>' + "?question_id="+question_id + "&line_value="+line_value+"&catalog_id="+catalog_id;
	   $.artDialog.open(uri , {title: "修改问题",width:'800px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	}

	//回答问题
	function questionAnswer(question_title,question_id){
		uri = '<%=AnswerQuestionUrl%>' + "?question_title="+question_title+"&question_id="+question_id;
		$.artDialog.open(uri , {title: "回答问题",width:'800px',height:'450px', lock: true,opacity: 0.3,fixed: true});
	}
	//点击修改答案的编辑按钮
	function modAnswer(question_id,answer_id){
       if(confirm("该问题已回答,确定重新编辑吗？")){
	   var answer_content=$("#answerShow"+question_id+" span:eq(0)").text();

	   var answer_time=$("#time"+question_id).text();
	   var answerer=$("#answerer"+question_id).text();
	   var file_path = '<%=systemConfig.getStringConfigValue("file_path_knowledge") %>';
		//显示编辑框
	   var appendix=getAnswerAppendixList(answer_id);
	   var htmlE= '<div  style="background-color:#eeeeee;">'+
			   	     '<div align="center"><textarea style="width:98%"  rows="5" id="content'+question_id+'">'+answer_content+'</textarea></div>'+
				     	'<table width="98%" border="0" cellspacing="0" cellpadding="0" class="table">'+
						'<tr>'+
				 			'<td id="over_file_td'+question_id+'">'+
				 			 	'<input type="button" class="long-button" onclick="uploadFile('+"'jquery_file_up"+question_id+"'"+','+question_id+');" value="上传附件" />'+
				 			 	'<div id="jquery_file_up'+question_id+'">'+
			 						'<input type="hidden" name="file_names"/>'+
			 						'<input type="hidden" name="sn" id="sn" value="K_knowledge"/>'+
			 						'<input type="hidden" name="path" value="'+file_path+'"/>'+
			 					'</div>'+
				 			'</td>'+
				 		'</tr>'+
				 		'<tr><td padding-left="20px">'+ appendix+
			 			'</td> </tr>'+
				 		'</table>'+
				 		'<div  align="right" style="height:30px;">'+
			     		   '<input type="button" onclick="ajaxModAnswer('+answer_id+','+question_id+','+"'jquery_file_up"+question_id+"'"+')" class="long-button" style="font-size:12px; margin-top:5px;" value="修改答案" />  &nbsp;&nbsp;'+
			     		   '<input type="button" onclick="updateCancel('+question_id+','+"'"+answer_content+"'"+','+answer_id+','+"'"+answer_time+"'"+','+"'"+answerer+"'"+')" class="short-button" style="font-size:12px; margin-top:5px;" value="取消"/>'+
			     	   '</div>'+
	   	    	   '</div>';
	   	    	  
	     	$('#superAnswer'+question_id+'').html(htmlE);
       }else
           return false;
	}

	//修改答案
	 function ajaxModAnswer(answer_id,question_id,target){
	       var targetNode=$("#"+target);
	       var fileNames =  $("input[name='file_names']",targetNode).val();
		   var sn = $("#sn").val();
			var path = $("input[name='path']").val();
            var answer_content=$("#content"+question_id).val();
            if(answer_content==""){
            	alert("请填写问题内容");
        		return false;
                }
            var param='answer_id='+answer_id+'&answer_content='+answer_content+'&file_names='+fileNames+'&sn='+sn+'&path='+path;
            $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
            $.ajax({
				url:'<%=AjaxModAnswerAction%>',
				dataType:'json',
				data:param,
				success:function(data){
					$.unblockUI();
					 var AnsAppendixs = getAnswerAppendixList(answer_id);
				     if(AnsAppendixs!=''){
					     AnsAppendixs='<div style="padding-left:20px;">'+AnsAppendixs+'</div>';
				          }
					var content=data.answer_content;
				    var html='<div style="background-color:#E3FEE2; padding-left:10px; padding-right:10px;" id="answerShow'+question_id+'">'+	
					                '<br/>'+
					                '<span style=" padding-left:10px; font-size:13px;">'+answer_content+'</span>'+
					                '<div style="font-size:12px;">'+AnsAppendixs+'</div>'+
					                '<div style="text-align:right;padding-right:10px;">'+
					                	'<span style="font-size:12px; color:#999;" id="time'+question_id+'"> 回答时间：'+data.update_time.substring(0,10) + '</span>'+
					                	'&nbsp;<span style="font-size:12px; color:#999;" id="answerer'+question_id+'">  回答人：'+data.answerer+'</span>  '+
			    		            	'&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="deleteAnswer('+answer_id+','+question_id+')">删除</a>'+
			 			           		'&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="modAnswer('+question_id+','+answer_id+')">编辑</a>'+
					                '</div>'+
			    	         '</div>';     
						$('#superAnswer'+question_id+'').html(html);
						}
					});
			}

		//取消修改答案
		function updateCancel(question_id,answer_content,answer_id,answer_time,answerer){
			   var appendix=getAnswerAppendixList(answer_id);
			   if(appendix!=''){
				   appendix="<div style='padding-left:20px;'>"+appendix+"</div>";
			   }
			   var html='<div style="background-color:#E3FEE2" id="answerShow'+question_id+'">'+	
                         	'<br/>'+
                         	'<span style=" padding-left:10px;">'+answer_content+'</span>'+
                         	'<div style="font-size:12px;">'+appendix+'</div>'+
                         	'<div style="text-align:right;padding-right:10px;">'+
		                         '<span style="font-size:12px; color:#999;" id="time'+question_id+'">'+answer_time +'</span>'+
		                            '&nbsp;<span style="font-size:12px; color:#999;" id="answerer'+question_id+'">'+answerer +'</span>'+
			                        '&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="deleteAnswer('+answer_id+','+question_id+')">删除</a>'+
			                        '&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="modAnswer('+question_id+','+answer_id+')">编辑</a>'+
                         	'</div>'+
                   		 '</div>';     
	           $('#superAnswer'+question_id+'').html(html);
			
		}
	 
	//删除答案
	function deleteAnswer(answer_id,question_id){
		var file_path = '<%=systemConfig.getStringConfigValue("file_path_knowledge") %>';
		if(confirm("确定删除答案吗？")){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			$.ajax({
				url:'<%=deleteQuestionAnswerUrl%>'+"?answer_id="+answer_id+"&question_id="+question_id,
				dataType:'json',
				success:function(data){
					if(data && data.flag ==="success"){
						$.unblockUI();
						var html = '<div style="background-color:#eeeeee">'+			  
								     	'<div align="center">'+
								     	'<textarea style="width:98%" rows="5" id="content'+question_id+'"></textarea></div>'+
								     	'<table width="98%" border="0" cellspacing="0" cellpadding="0" class="table">'+
											'<tr>'+
									 			'<td width="60px;" height="30px;"><input type="button" class="long-button" onclick="uploadFile('+"'jquery_file_up"+question_id+"'"+','+question_id+');" value="上传附件" /></td>'+
									 			'<td height="30px;">&nbsp;</td>'+
									 		'</tr>'+
									 		'<tr>'+
									 			'<td>&nbsp;</td>'+
									 			'<td id="over_file_td'+question_id+'">'+
									 			 	'<div id="jquery_file_up'+question_id+'">'+
								 						'<input type="hidden" name="file_names"/>'+
								 						'<input type="hidden" name="sn" id="sn" value="K_knowledge"/>'+
								 						'<input type="hidden" name="path" value="'+file_path+'"/>'+
								 					'</div>'+
									 			'</td>'+
									 		'</tr>'+
									 		'</table>'+
								     	'<div align="right" style="height:30px;">'+
								     		'<input type="button" onclick="ajaxAnswerQuestion('+question_id+','+"'jquery_file_up"+question_id+"'"+')" class="long-button" style="font-size:12px; margin-top:5px;" value="提交回答"/>'+
								     	'</div>'+
								     '</div>';
				     
					$('#superAnswer'+question_id+'').html(html);
					 var html2='<span  style="font-size:12px; color:red;">未回答</span>';
			         $('#status'+question_id+'').html(html2);
					}
				}
			});
		}else{
			return false;
		}
	}
	
	function go(number){
		var genjin='<%=genjin%>';
        var quFen=$('#quFen').val();
        $("#page_no").val(number);
        
        if(quFen=="question_create_time"){
            var catalogId=<%=catalog_id%>;
            var productLineId=<%=product_line%>;
        	findQuestion(catalogId,productLineId,quFen,number);
        }else if(genjin=='genjin'){
        	 var catalogId=<%=catalog_id%>;
        	 goGenjin(catalogId,genjin,number);
        }else{
    		var obj = {
    			p:$("#page_no").val(),
    			product_line:$("#pageForm input[name='product_line']").val(),
    			product_id:$("#pageForm input[name='product_id']").val(),
    			search_key:$("#pageForm input[name='search_key']").val(),
    			catalog_id:$("#pageForm input[name='catalog_id']").val(),
    			category:$("#pageForm input[name='category']").val(),
    			cmd:$("#pageForm input[name='cmd']").val(),
    			product_line_ids:$("#pageForm input[name='product_line_ids']").val()
    		}  
    		loadQuestionPage(obj);
        }
	}


	//根据问题id查询该问题下的 附件列表
	function getAppendixList(catalogId){
		var value='';
		var para="catalogId="+catalogId;
		$.ajax({
			async:false,
			type:'post',
			url:'<%=AjaxQuestionAppendixUrl %>',
			dataType:'json',
			data:para,
			success:function(data){
	           for(var i=0;i<data.length;i++){
	        	   var str=data[i].file_name;
	               for(var s=0;s<3;s++){
	               var length=str.length;
	               var num=str.indexOf("_");
	               str=str.substring(num+1,length);
	               }
	               var fileName=str;
	               value+='<a style="color:blue;font-size:12px;" href="javascript:void(0)" onclick="question_answer_detailed('+data[i].file_id+')" >'+fileName+'</a><br/>';   
	           }  
			}
		});
		return value;
	}

	//根据答案id查询该答案下的 附件列表
	function getAnswerAppendixList(answerId){
		var answer='';
		var para="answerId="+answerId;
		$.ajax({
			async:false,
			type:'post',
			url:'<%=AjaxAnswerAppendixUrl %>',
			dataType:'json',
			data:para,
			success:function(data){
	           for(var i=0;i<data.length;i++){
	        	   var str=data[i].file_name;
	               for(var s=0;s<3;s++){
	            	var length=str.length;
	                var num=str.indexOf("_");
	                str=str.substring(num+1,length);
	               }
	               var fileName=str;
	              answer+='<a style="color:blue;font-size:12px;" href="javascript:void(0)" onclick="question_answer_detailed('+data[i].file_id+')" >'+fileName+'</a><br/>';   
	           } 
			}
		});
		return answer;
	}

	//打开附件
	function question_answer_detailed(catalogId){
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/show_file_online.html?file_id="+catalogId; 
		 windowSchedule = $.window({
			title: "文件查看",
			url: uri,
			resizable: false,
			width: 800,
			height: 530,
			showModal:true,
			x:-1,
			y:0,
			showFooter: false,
		 
			withinBrowserWindow:true,
			checkBoundary:true,
			onClose:function(){}
		});
	}
    //标记为已阅读方法
	function ajaxDetQuestionCall(questionId,productLineId){
	      $.ajax({
		  		type:'post',
		  		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxDetQuestionCallAction.action',
		  		dataType:'json',
		  		data:'questionId='+questionId+'&userId='+<%=userId%>,
		  		success:function(msg){
		  			if(msg.flag==1){
		  				$('#new'+questionId+'').html('');
		  				var newcount= $('#newcount'+productLineId+'').html();
		  				if(newcount==1){
		  					$('#newproductline'+productLineId+'').html('');
		  				}else{
		  					 newcount=newcount-1;
			  				$('#newcount'+productLineId+'').html(newcount);
		  				}
		  			}
		  		}
	  	  });
	}

	 function hidderDiv(questionId,productLineId){
		 var news=$('#new'+questionId+'').html();
		 if(news!=null && news!=''){
			//标记为已读
			ajaxDetQuestionCall(questionId,productLineId);	
		 }	
		 var op= $('#block'+questionId+'').is(":hidden");
		 if(op==false){
			//加载附件列表
			 var fujian=getAppendixList(questionId);
	         if(fujian!=''){
	           fujian="<div style='padding-left:18px;'>"+fujian+"</div>";
	         }
			 $('#appendixs'+questionId+'').html(fujian);	 
			 $('#block'+questionId+'').hide();
			 $('#'+questionId+'').slideToggle(500);
		 }else{
			 $('#'+questionId+'').slideToggle('500',function(){
				 $('#block'+questionId+'').slideToggle('1000');
			 });
		 }
		     
	 }

	 function ajaxAnswerQuestion(questionId,target){
		var value=$('#content'+questionId+'').val();
		var targetNode = $("#"+target);
	    var fileNames = $("input[name='file_names']",targetNode).val();
		var sn = $("#sn").val();
		var path = $("input[name='path']").val();
		var para='question_id='+questionId+'&answer_content='+value+'&file_names='+fileNames+'&sn='+sn+'&path='+path;
		$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		$.ajax({
			type:'post',
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AddQuestionAnswerAction.action',
			dataType:'json',
			data:para,
			success:function(json){	
				$.unblockUI();
              	var AnsAppendixs = getAnswerAppendixList(json.answer_id);
				if(AnsAppendixs!=''){
					AnsAppendixs="<div style='padding-left:20px;'>"+AnsAppendixs+"</div>";
			        
				 }
				 
			 var html='<div style="background-color:#E3FEE2" id="answerShow'+questionId+'">'+	
						    '<br/>'+
				            '<span style=" padding-left:10px;">'+json.answer_content+'</span>'+
				            '<div style="font-size:12px;">'+AnsAppendixs+'</div>'+
				            '<div style="text-align:right;padding-right:10px;">'+
				            '<span style="font-size:12px; color:#999;" id="time'+questionId+'">回答时间：'+json.create_time.substring(0,10)+'</span>'+
				    		'&nbsp;<span style="font-size:12px; color:#999;" id="answerer'+questionId+'">回答人：'+json.answerer+'</span>'+
				 			'&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="deleteAnswer('+json.answer_id+','+questionId+' )">删除</a>'+
						    '&nbsp;<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="modAnswer('+questionId+','+json.answer_id+')">编辑</a>'+
				            '</div>'+
	    			  '</div>';   
	         $('#superAnswer'+questionId+'').html(html);
	         var html2='<span  style="font-size:12px; color:#0C0;">已回答</span>';
	         $('#status'+questionId+'').html(html2);
			}
		});
	 }
      //问题移动到其他分类
	 function questionCatalogMove(question_id){
       uri = '<%=moveQuestionCatalogUrl%>'+'?questionId='+question_id;
   	   $.artDialog.open(uri , {title: "转移问题",width:'800px',height:'410px', lock: true,opacity: 0.3,fixed: true});      
	 }
</script>
</head>
<body > 
<input type="hidden" id="quFen" value="<%=quFen %>"/>
<%if(rows != null && rows.length > 0 ){ %>	
 	<%for(int index = 0 , count = rows.length ; index < count ; index++){ %>
 	 	<%
     		long QuestionId=rows[index].get("question_id",0);
 	 	 
     		DBRow row=questionCatalogMgrZwb.getQuestionByCatlogId(QuestionId); 
     		long questionCallLineId=row.get("product_line_id",0l);
   		%>
 		<div style="font-weight:bold;font-size:14px;padding-left:5px; color:#036;"><%=rows[index].getString("question_title") %>
 		<!-- 标记是否是未读 -->
 		<%if(callRow!=null){%>
	      <%for(int p=0;p<callRow.length;p++){%>
	          <%if(callRow[p].get("question_id",0l)==rows[index].get("question_id",0)){%>
	     			<span id="new<%=QuestionId%>"><img src="./imgs/new.gif" /></span>
	          <%}%>
	      <%}%>
	    <%}%>
 		</div>
 		<div style=" padding-left:5px; height:30px; line-height:30px;">
 		    <span style="font-size:12px; color:#999;">创建时间：<%=row.getString("question_create_time").substring(0,10) %></span>
 		    <span style="font-size:12px; color:#999;">创建人：<%=row.getString("questioner")%></span>
 			<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="deleteQuestion(<%= rows[index].getString("question_id") %>,<%=row.getString("product_line_id")%>)">删除</a>
		    <a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="updateQuestion(<%= rows[index].getString("question_id") %>,<%=catalog_id %>)">编辑</a>
		    <a href="javascript:void(0)" onclick="questionCatalogMove(<%=rows[index].getString("question_id") %>)" style="font-size;12px; color:#999">move</a>
		    <span id="status<%=QuestionId %>">
			    <%if(row.get("question_status",0)==1){%>
			     <span  style="font-size:12px; color:red">未回答</span>
			    <%}else{ %>
			    <span  style="font-size:12px; color:#0C0;">已回答</span>
			    <%} %>
		    </span>
		    <a href="javascript:void(0)" onclick="hidderDiv(<%=QuestionId %>,<%=questionCallLineId%>)" style=" color:#00F; text-decoration: NONE;">全文↓</a>
 		</div>
 		<!-- 显示的内容 -->
 		<div id="block<%=QuestionId %>" style="padding-left:20px; font-size:13px;line-height:20px;">
 		    <!-- 判断 是索引查询还是数据库查询 -->
 		    <% if(quFen!=null && quFen!=""){ %>
	 		    <%if(rows[index].getString("contents").length() > 60){ %>
	 		       <%=rows[index].getString("contents").substring(0,60)%>...
	 		    <%}else{%>
	 		       <%=rows[index].getString("contents")%>
	 		    <%} %>
 		    <%}else if(single!=null && single==single && single!=""){%>
 		   		<%if(rows[index].getString("contents").length() > 60){ %>
	 		       <%=rows[index].getString("contents").substring(0,60)%>...
	 		    <%}else {%>
	 		       <%=rows[index].getString("contents")%>
	 		    <%} %>
	 		<%}else if(genjin!=null && genjin!="" && genjin==genjin){ %>  
	 		    <%if(rows[index].getString("contents").length() > 60){ %>
	 		       <%=rows[index].getString("contents").substring(0,60)%>...
	 		    <%}else {%>
	 		       <%=rows[index].getString("contents")%>
	 		    <%} %>
 		    <%}else{ %>
 		   		 <%=rows[index].getString("content")%>
 		    <%} %>
 		</div>
   		<!-- 隐藏的内容 -->
   		<div id="<%=QuestionId %>" class="hidderDiv" style="display:none;">
		   <div class="top" >	
		   		<div class="top-left"></div>
		        <div class="top-centter"></div>
		        <div class="top-right"></div>
		   </div>
		   <div class="center">
		       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
		          <tr>
		            <td width="6" class="left-td"></td>
		            <td class="content-td">
		                <div align="right"><img src="./divImgs/cha.gif" width="17" height="17"  onclick="hidderDiv(<%=QuestionId %>)"/></div>
					  	<div class="content" >    
		                 	<span id="hiddeContent<%=QuestionId %>" style="font-size:13px;" ><%=row.getString("contents") %></span>		                         
                       		<div id="appendixs<%=QuestionId %>" style="font-size:12px;"></div>
                       		<div style="border-bottom:1px #E1E1E1 solid ;margin-bottom:8px; margin-top:8px;" ></div>
                       		
                       		<!-- 如果有问题显示问题及状态没有显示回答框 -->
                       		
                       		<div id="superAnswer<%=QuestionId %>">
	                       		 <%if(row.get("question_status",0)==1){%>
								     <div style="background-color:#eeeeee">		  
								     	<div align="center">
								     		<textarea  rows="5" style="width:98%" id="content<%=QuestionId %>"></textarea>
								        </div>
										<table width="98%" border="0" cellspacing="0" cellpadding="0" class="table">
											<tr>
									 			<td width="60px;" height="30px;">
									 			 	<input type="button" class="long-button" onclick="uploadFile('jquery_file_up<%=QuestionId %>',<%=QuestionId %>);" value="上传附件" />
									 			</td>
									 			<td height="30px;">&nbsp;</td>
									 		</tr>
									 		<tr>
									 			<td>&nbsp;</td>
									 			<td id="over_file_td<%=QuestionId %>">
									 			 	<div id="jquery_file_up<%=QuestionId %>">
								 						<input type="hidden" name="file_names"/>
								 						<input type="hidden" name="sn" id="sn" value="K_knowledge"/>
								 						<input type="hidden" name="path" value="<%=systemConfig.getStringConfigValue("file_path_knowledge") %>"/>
								 					</div>
									 			</td>
									 		</tr>
								 		</table>			
								     	<div align="right" style="height:30px;">
								     		<input type="button" onclick="ajaxAnswerQuestion(<%=QuestionId %>,'jquery_file_up<%=QuestionId %>')" class="long-button" style="font-size:12px; margin-top:5px;" value="提交回答"/>
								     	</div>
								     </div>
							     <%}else{ %>
							     	    <%DBRow answerRow = questionCatalogMgrZwb.getAnswerByQuestionId(QuestionId);%>	
								    	<div style="background-color:#E3FEE2; padding-left:10px; padding-right:10px;" id="answerShow<%=QuestionId %>">
										    <br/>
										    <span style=" padding-left:10px; font-size:13px;"><%=answerRow.getString("answer_content") %></span>
										    <br/>
										    <table>
										    
										    <%
									 			DBRow[] files = fileMgrZr.getFilesByFileWithIdAndFileWithType(answerRow.get("answer_id",0),FileWithTypeKey.ANSWER);
										    	if(files.length!=0 && files!=null){
									 		%>
										    <tr>
										    <td>&nbsp;</td>
					 							<td>
									 				<!-- 读取附件 -->
									 				<% 
									 					for(DBRow fileRow : files){
									 						 String str = fileRow.getString("file_name");
									 		
									 						for(int s=0;s<3;s++){
									 							  int length = str.length();
									 				              int num=str.indexOf("_");
									 				              str=str.substring(num+1,length);
									 				             }
									 						
									 						%>
									 						<div style="padding-top:7px;"><a style="color:blue;font-size:12px;" href="javascript:void(0)" onclick="question_answer_detailed('<%=fileRow.getString("file_id") %>')" ><%=str %></a></div>
									 				<%} %>
									 			</td>
										 		</tr>
										 	<%}%>	
										 	</table>
										 	<div style="text-align:right;padding-right:10px;">
								    		<span style="font-size:12px; color:#999;" id="time<%=QuestionId %>">回答时间：<%=answerRow.getString("create_time").substring(0,10) %></span>
								    		<span style="font-size:12px; color:#999;" id="answerer<%=QuestionId %>">回答人：<%=answerRow.getString("answerer")%></span>
								 			<a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="deleteAnswer(<%=answerRow.get("answer_id",0)%>,<%=answerRow.get("question_id",0) %> )">删除</a>
										    <a style="font-size:12px; color:#999;" href="javascript:void(0)" onclick="modAnswer(<%=QuestionId %>,<%=answerRow.get("answer_id",0) %>)">编辑</a>
										    </div>
								    	</div>
							     <%} %>
						     </div>
						    
						     
		                </div>
		            </td>
		            <td width="6" class="right-td"></td>
		          </tr>
		        </table>
		   </div>
		   <div class="bottom">
		       <div class="bottom-left"></div>
		       <div class="bottom-centter"></div>
		       <div class="bottom-right"></div>
		  </div>
		</div>
		<br/>
   		<hr color="#dddddd";size="1px"; />
 	<%} %>	
<%} %>				     	
					     	
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td height="28" align="right" valign="middle">
	   <%
			int pre = pc.getPageNo() - 1;
			int next = pc.getPageNo() + 1;
			out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
			out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
			out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
			out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
			out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
		%>
	      跳转到
	     	<input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
	        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO" />
	    </td>
	  </tr>
	</table>			     	
					 
					 
					 
		<form id="pageForm">
 			<input type="hidden" id="page_no" name="p" />
			<input type="hidden" name="product_line" id="product_line" value='<%=product_line %>'/>
			<input type="hidden" name="product_id" id="product_id" value='<%=product_id %>'/>
			<input type="hidden" name="search_key" id="search_key" value='<%=search_key %>'/>
			<input type="hidden" name="catalog_id" id="catalog_id" value='<%=catalog_id %>'/>
			<input type="hidden" name="category" id="category" value='<%=category %>'/>
			<input type="hidden" name="cmd" id="cmd" value='<%=cmd %>'/>
			<input type="hidden" name="product_line_ids" id="product_line_ids" value='<%=product_line_ids %>'/>
		</form>			     	
</body>
</html>