<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>添加子分类</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
	long productLineId=StringUtil.getLong(request,"productLineId");
	String productLineName=StringUtil.getString(request,"productLineName");
%>
<script>
//添加问题分类
function addCatalog(){
	var catalogName=$('#catalogName').val();
	if(catalogName==" " || catalogName.length==0){
       alert("请输入分类名称");
       return false;
	}
    	$.ajax({
    		type:'post',
    		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AddQuestionCatalogAction.action',
    		dataType:'json',
    		data:$("#myForm").serialize(),
    		success:function(msg){
    			$.artDialog && $.artDialog.close();
			    $.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
    		}
    	});
}
</script>
</head>
<body>

	<form id="myForm" method="post">
 		 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
			   <tr>
			  	   <td align="left" height="100px;" style="padding-left:20px;">
			  	  		 请输入分类名称：<input type="text" name="catalogName" id="catalogName" value="" size="30"/>
			  	  		 <input type="hidden" name="productLineId" value="<%=productLineId %>" />
			  	  		 <input type="hidden" name="productLineName" value="<%=productLineName %>" />
			  	   </td>
			   </tr>
			    <tr>
			  	   <td align="right" class="win-bottom-line">
			  	   	    <input type="button"  value="确定" class="normal-green" onClick="addCatalog()" id="addQuestionClassify">
			  	   </td>
			   </tr>
	  	 </table>
	</form>	

   
</body>
</html>