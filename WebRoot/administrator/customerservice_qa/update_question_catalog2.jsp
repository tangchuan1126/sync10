<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>添加子分类</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
long catalogId=StringUtil.getLong(request,"catalogId");
String catalogName=StringUtil.getString(request,"catalogName");
%>
<script>
//修改问题分类
function updateCatalog(){
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/UpdateQuestionCatalogSmallAction.action',
		dataType:'json',
		data:$("#updateCatalogForm").serialize(),
		success:function(msg){
			$.artDialog && $.artDialog.close();
	   		$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
		}
	});
}
</script>
</head>
<body>

	<!-- 修改 -->

	       <form id="updateCatalogForm" method="post">
		   <table width="100%"  border="0" cellpadding="0" cellspacing="0">
			   <tr>
			  	   <td align="left" height="100px;" style="padding-left:20px;">
			  	  		 修改分类名称：<input type="text" name="catalogName" value="<%=catalogName %>" size="30"/>
			  	  		 <input type="hidden" name="catalogId" value="<%=catalogId %>" />
			  	   </td>
			   </tr>
			    <tr>
			  	   <td align="right" class="win-bottom-line">
			  	   	    <input type="button"  value="确定" class="normal-green" onClick="updateCatalog()" >
			  	   </td>
			   </tr>
		   </table>
		</form>	
	
  
</body>
</html>