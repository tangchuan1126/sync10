<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
int modType = StringUtil.getInt(request,"modType",0);//0普通修改，1回答修改
String backurl = StringUtil.getString(request,"backurl");
long question_id = StringUtil.getLong(request,"question_id");
DBRow question_detail = qaMgrZJ.getDetailQuestionById(question_id);
DBRow product;
%>
<%
	if(question_detail==null)
	{
		question_detail=new DBRow();
		product = new DBRow();
	}
	else
	{
		product = productMgr.getDetailProductByPcid(question_detail.get("product_id",0l));
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改问题</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type="text/javascript" src="../js/select.js"></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css?v=1" />

<script>
function modQuestion()
{
	var f = document.mod_question_form;
	
	 if (f.question_title.value == "")
	 {
		alert("请填写问题标题");
	 }
	 else if (CKEDITOR.instances.content.getData() == "")
	 {
	 	
		alert("请填写问题内容");
	 }
	 else
	 {
	 	
	 	document.mod_question_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/modQuestion.action";
		document.mod_question_form.submit();	
	 }
}
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	
	$().ready(function() {

	function log(event, data, formatted) {

		
	}

	});
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客服问答 »  修改问题</td>
  </tr>
</table>
<br>
<form name="mod_question_form" method="post" action="" >

<table width="98%" border="0" cellspacing="7" cellpadding="2">
  <tr>
    <td width="8%" align="left" valign="middle" class="text-line" ><span class="STYLE1 STYLE2">商品名称</span></td>
    <td align="left" valign="middle" ><span class="STYLE1 STYLE2">
      <span style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold"><%=product.getString("p_name") %></span>
    </span></td>
  </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE1 STYLE2">问题标题</td>
    <td align="left" valign="middle" ><input name="question_title" type="text" class="input-line" id="question_title" value="<%=question_detail.getString("question_title") %>" ></td>
     </tr>
  <tr>
    <td align="left" valign="middle" class="STYLE3" >问题内容</td>
    <td align="left" valign="middle" >
    <textarea rows="50" cols="80" name="content" id="content" style="width: 100%;height: 800px"><%=question_detail.getString("content") %></textarea>
    		<script type="text/javascript">
			CKEDITOR.replace( 'content' ,
				{
					 height:500,
					 
	        		 filebrowserUploadUrl : '/uploader/upload.php',
	        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
				}
			);				
			</script>	</td>
  </tr>
</table>
<input type="hidden" name="backurl" value="<%=backurl%>">
<input type="hidden" name="question_id" value="<%=question_id %>"/>
<input type="hidden" name="modType" value="<%=modType%>"/>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">
    	&nbsp;
    </td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modQuestion();" id="saveButton">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='question_index.html'">
	</td>
  </tr>
</table> 
<div style="display:none" id="keep_alive"></div>
</body>
</html>
