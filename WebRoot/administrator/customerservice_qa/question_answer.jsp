<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>问答</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- tree -->
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
	
<style type="text/css">
	a:link {
		color: #00C;
	}
</style>	

<script type="text/javascript">
//加载选项卡控件
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
});

function openDialog(id){
	url='<%=ConfigBean.getStringValue("systenFolder")%>administrator/customerservice_qa/question_answer_detailed.html';
	$.artDialog.open(url, {title: '问答详细',width:'600px',height:'300px', lock: true});
}
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

</head>
<body onload="onLoadInitZebraTable();"> 
	<div style="border-bottom:2px #CCC solid; height:25px;">
	   <img width="17" height="12" align="absmiddle" alt="title" src="../imgs/page_title.gif">
	   <span style="font-size:14px; font-weight: bold;">内容应用》》问答</span>
	</div>
	<br/>
	<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	   <input class="long-button-add" type="button"  value="提出问题" />
	</div>
	<br/>
	<div id="tabs" >
		<ul>
			<li><a href="#maintain">问答管理</a></li>
			<li><a href="#wait">待回答</a></li>		 
		</ul>
		<!-- 产品问题内容 -->
		<div id="maintain">
		   <!-- 过滤 -->
		   <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	            <select style="border:1px #CCCCCC solid" name="selectSortTime">
					<option value="">产品线</option>
				</select>
				<select style="border:1px #CCCCCC solid" name="selectSortTime">
					<option value="">产品分类</option>
				</select>
				<select style="border:1px #CCCCCC solid" name="selectSortTime">
					<option value="">产品名</option>
				</select>
				<select style="border:1px #CCCCCC solid" name="selectSortTime">
					<option value="">问题分类</option>
				</select>
				<input class="button_long_refresh" type="submit" value="过 滤" name="">
	       </div>
	       <br/>
	       <!-- 问答列表 -->
	       <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="zebraTable">
			  <tr>
				 <td>
				    <a href="#" style="color:#00F" onclick="openDialog(id)">问题1</a>
				 </td>
			  </tr>
			  <tr>
				 <td>
				        答案1
				 </td>
			  </tr> 
			  <tr>
				 <td>
				     &nbsp;
				 </td>
			  </tr> 
		   </table>
		</div>
		<!-- 运费问题内容 -->
		<div id="wait">
	                     待回答
	    </div>
	</div>
</body>
</html>