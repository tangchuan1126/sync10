<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>问题分类</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />





<script type="text/javascript">
//加载选项卡控件
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$("#tabs").tabs("select",1);
});

//显示隐藏 div
function hideDiv(id){
	var temp= $("#"+id+"").is(":visible");//是否可见 
	//问题原文简写的那一行
	 var tr=$("#questionList"+id+" tr:eq(1)");
	 //点击详细后弹出的内容
	 var detail=$("#questionList"+id+" tr:eq(2) ");
	if(temp==true){
		$("#"+id+"").hide('slow');
	        tr.show('slow');
	        detail.show('slow');
	}else{
		$("#"+id+"").show('slow');
        tr.hide('slow');
        detail.hide('slow');
	}
}

//打开问题详细
function question_answer_detailed(catalogId){

    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/knowledge/show_file_online.html?file_id="+catalogId; 
	// $.artDialog.open(uri , {title: '文件查看',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true});
	 windowSchedule = $.window({
		title: "文件查看",
		url: uri,
		resizable: false,
		width: 1100,
		height: 530,
		showModal:true,
		x:-1,
		y:0,
		showFooter: false,
	 
		withinBrowserWindow:true,
		checkBoundary:true,
		onClose:function(){}
	});
}

//打开添加问题页面
//function addQuestion(){
//	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question.html"; 
//    $.artDialog.open(uri , {title: "添加问题",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
//}

//子类添加子类和同级页面
function add_queston_classify_very_small(catalogId,productLineId,productLineName,catalogParentId,catalogName){
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
		dataType:'text',
		data:'catalogId='+catalogId,
		success:function(text){
			uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify_very_small.html?parentName="+text+"&catalogParentId="+catalogParentId+"&catalogId="+catalogId+"&productLineId="+productLineId+"&productLineName="+productLineName+"&catalogName="+catalogName; 
		    $.artDialog.open(uri , {title: "添加问题子分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
		}
	});	
}

//打开添加子级页面
function add_queston_classify_small(catalogId,productLineId,productLineName,catalogName){

	//返回所有父类名字
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
		dataType:'text',
		data:'catalogId='+catalogId,
		success:function(text){
			uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify_small.html?parentName="+text+"&catalogId="+catalogId+"&productLineId="+productLineId+"&productLineName="+productLineName+"&catalogName="+catalogName; 
		    $.artDialog.open(uri , {title: "添加问题子分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
		}
	});   
}

//开打添加问题分类
function add_question_classify(productLineId,productLineName){
	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify.html?productLineId="+productLineId+"&productLineName="+productLineName; 
    $.artDialog.open(uri , {title: "添加问题分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
}
//激活的产品线
function pLineClick(lineName,catalogName,productLineId,catalogId){
	var str=lineName+" >> "+catalogName;
	if(lineName==1){
		$(window.frames["iframeRight"].document).find("#pLineClick").html(catalogName);
	}else{
		$(window.frames["iframeRight"].document).find("#pLineClick").html(str);
	}	
	//返填商品分类下拉列表控件
	iframeRight.window.ajaxLoadCatalogMenuPage(productLineId);
	//调查询各分类下的所有问题
	findQuestion(catalogId);

}
	//查询各分类下的所有问题
function findQuestion(catalogId){
	var para = "catalogId="+catalogId;
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionByProductLineIdAction.action',
		dataType:'json',
		data:para,
		success:function(data){
		  			     
		}
	});
}

//根据问题id查询该问题下的 附件列表
function getAppendixList(catalogId){
	var value='';
	var para="catalogId="+catalogId;
	$.ajax({
		async:false,
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionAppendixAction.action',
		dataType:'json',
		data:para,
		success:function(data){
           for(var i=0;i<data.length;i++){
              value+='<a style="color:blue" href="javascript:void(0)" onclick="question_answer_detailed('+data[i].file_id+')" >'+data[i].file_name+'</a><br/>';   
           }  
		}
	});
	return value;
}

//子页面调的方法
function refreshWindow(){
   window.location.reload();
}

//获取当前子类的所有父类名字
function getAllTitle(catalogId,productLineId){
	//返填商品分类下拉列表控件
	iframeRight.window.ajaxLoadCatalogMenuPage(productLineId);
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
		dataType:'text',
		data:'catalogId='+catalogId,
		success:function(text){
			$(window.frames["iframeRight"].document).find("#pLineClick").html(text);
		}
	});
	//调查询各分类下的所有问题
	findQuestion(catalogId);
}



function closeWinRefresh(){
	window.location.reload();
}

</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

</head>
<body onload="onLoadInitZebraTable();"> 
	<div style="border-bottom:2px #CCC solid; height:25px;">
	   <img width="17" height="12" align="absmiddle" alt="title" src="../imgs/page_title.gif">
	   <span style="font-size:14px; font-weight: bold;">内容应用》》问题分类</span>
	</div>
	<br/>
	<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	   <input class="long-button-add" type="button"  value="添加分类 "  onclick="" />
	   <input class="short-short-button-del" type="button" value="删除" />
	</div>
	<br/>
	<div id="tabs" >
		<ul>
			<li><a href="#product">产品问题</a></li>
			<li><a href="#carriage">运费问题</a></li>		 
		</ul>
		<!-- 选项卡产品问题内容 -->
		<div id="product" style="padding:1em 0.5em;">
		    <!-- 容器 左树 右问题 -->
		    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
		       <tr>
		         <td style="width:19%;"  valign="top" algin="left">
		             <!-- 加载左侧问题分类树 -->
				   <!--   <a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/customerservice_qa/question_catalog_tree.html" >sdfsdfsf</a> -->
				     <iframe src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/customerservice_qa/question_catalog_tree.html" frameborder="0" height="800px;" width="100%;"></iframe>
				    
		         </td>
		         <td valign="top" algin="center">
		               <iframe id="iframeRight" name="iframeRight" src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/customerservice_qa/question_catalog_right.html" frameborder="0" height="800px;" width="100%;"></iframe>
		         </td>
		       </tr>
		    </table>
		</div>		
		<!-- 运费问题内容 -->
		<div id="carriage">
	                     这是运费问题
	    </div>
	</div>
</body>
<!-- windows -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<script src="../js/window/jquery.window.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../js/window/jquery.window.css" />
</html>