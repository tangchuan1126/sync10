<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
 
<%@ include file="../../include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>文件预览</title>
  
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<scr >
 
 
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="../js/flexpaper206/css/flexpaper.css" />
 
<script type="text/javascript" src="../js/flexpaper206/js/flexpaper.js"></script>
<script type="text/javascript" src="../js/flexpaper206/js/flexpaper_handlers.js"></script>


<%
 	long fileId = StringUtil.getLong(request,"file_id");
	DBRow file = fileMgrZr.getFileByFileId(fileId);
	String path = file.getString("file_convert_file_path");
	String downLoadFileAction =  ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadAction.action";

%>
<script type="text/javascript">
	 
</script>
   
</head>

<body>
 <div style="border:0px solid silver;line-height:30px;height:30px;">
 	<%
 		String fileName = file.getString("file_name");
 		int start = path.indexOf("/");
 		int end = path.lastIndexOf("/");
 		String folder = path.substring(start+1,end) ;
 		 
 	%>
 	文件: <a href='<%=downLoadFileAction %>?file_name=<%=fileName %>&folder=<%=folder %>' ><%=fileName  %></a>
  
  
 </div>
 <div id="documentViewer" class="flexpaper_viewer" style="width:700px;height:500px;border:1px solid silver;"></div>
 <script type="text/javascript">
 
 
 

 $('#documentViewer').FlexPaperViewer(
         { config : {
             SWFFile : '../../<%= path%>',

             Scale :0.7,
             ZoomTransition : 'easeOut',
             ZoomTime : 0.5,
             ZoomInterval : 0.2,
             FitPageOnLoad : true,
             FitWidthOnLoad : false,
             FullScreenAsMaxWindow : false,
             ProgressiveLoading : false,
             MinZoomSize : 0.2,
             MaxZoomSize : 5,
             SearchMatchAll : false,
             InitViewMode : 'Portrait',
             RenderingOrder : 'flash,html',
             StartAtPage : '',

             ViewModeToolsVisible : true,
             ZoomToolsVisible : true,
             NavToolsVisible : true,
             CursorToolsVisible : true,
             SearchToolsVisible : true,
             WMode : 'window',
             localeChain: 'en_US',
             jsDirectory:"../js/flexpaper206/js/"
         }}
 );
</script>
 
 
 
<script type="text/javascript">
//stateBox 信息提示框
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
 
	 var  _self = $("body"),
	_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	 
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
 }
}
</script>
</body>
</html>