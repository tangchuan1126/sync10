<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
<title>问题分类</title>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/question/zebra.js" type="text/javascript"></script>
<link href="../js/question/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- tree -->
<script type="text/javascript" src="../js/question/dtree.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../js/question/dtree.css" type="text/css"></link>
<!-- 所有产品 -->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />
<!-- qusetionRightList -->
<script type="text/javascript" src="../js/question/questionRightList.js"></script>
		
<style>
 <!-- 高级搜索的样式-->
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;	
	border:1px #bdbdbd solid;	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<%
	long productLineId=StringUtil.getLong(request,"productLine");
	String backurl = StringUtil.getString(request,"backurl");
	String questionListUri =  ConfigBean.getStringValue("systenFolder") + "administrator/customerservice_qa/question_list.html";
	String suggestUri =  ConfigBean.getStringValue("systenFolder")+"action/administrator/customerservice_qa/KeyWorldSuggestAction.action";
    String url=ConfigBean.getStringValue("systenFolder");
    String genjin=StringUtil.getString(request,"genjin");
    
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
	long userId = adminLoggerBean.getAdid();
	
	DBRow[] callRow=questionCatalogMgrZwb.getQuestionCallByUserId(userId);
%>
<script type="text/javascript">
//加载选项卡控件
jQuery(function($){
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	$("#tabs").tabs("select",0);
});

//子类添加子类和同级页面
function add_queston_classify_very_small(catalogId,productLineId,productLineName,catalogParentId,catalogName){
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
		dataType:'text',
		data:'catalogId='+catalogId,
		success:function(text){
			uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify_very_small.html?parentName="+text+"&catalogParentId="+catalogParentId+"&catalogId="+catalogId+"&productLineId="+productLineId+"&productLineName="+productLineName+"&catalogName="+catalogName; 
		    $.artDialog.open(uri , {title: "添加问题子分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
		}
	});	
}

//打开添加子级页面
function add_queston_classify_small(catalogId,productLineId,productLineName,catalogName){

	//返回所有父类名字
	$.ajax({
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionCatalogTitleAction.action',
		dataType:'text',
		data:'catalogId='+catalogId,
		success:function(text){
			uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify_small.html?parentName="+text+"&catalogId="+catalogId+"&productLineId="+productLineId+"&productLineName="+productLineName+"&catalogName="+catalogName; 
		    $.artDialog.open(uri , {title: "添加问题子分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
		}
	});   
}

//开打添加问题分类
function add_question_classify(productLineId,productLineName){
	uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/add_question_classify.html?productLineId="+productLineId+"&productLineName="+productLineName; 
    $.artDialog.open(uri , {title: "添加问题分类",width:'800px',height:'550px', lock: true,opacity: 0.3,fixed: true});
}


	//查询各分类下的所有问题
function findQuestion(catalogId,productLineId,op,p){
	  $("#catalog_id").val(catalogId);
	  $("#product_line").val(productLineId);
	  //返回所有问题  
		$.ajax({
			url:'<%= questionListUri%>',
			dataType:'html',
			data:'catalog_id='+catalogId+'&quFen='+op+'&p='+p,
			success:function(data){
				 $("#container").html(data);
			}
		});    
}
	//分页调方法
	function goGenjin(catalogId,genjin,p){
		//返回所有问题  
		$.ajax({
			url:'<%= questionListUri%>',
			dataType:'html',
			data:'catalog_id='+catalogId+'&genjin=genjin&p='+p,
			success:function(data){
				 $("#container").html(data);
			}
		});    
	}

//添加问题的时候提示
function singleSelect(question_id,question_catalog_id){
	$.ajax({
		url:'<%= questionListUri%>',
		dataType:'html',
		data:'question_id='+question_id+'&single=single'+'&catalog_id='+question_catalog_id,
		success:function(data){
			 $("#container").html(data);
		}
	});    
}


function getSubProductLineId(productLineId){
	var category = productLineId;
	var idStr = "" ;
	if(category.length < 1 || category *1 < 1 ){}
	else{
		var parentLi  = $("li[rel='"+category*1+"']").get(0);
		if(parentLi)
		var lis =  ($("li",$(parentLi)));
	 
		if(lis && lis.length > 0 ){
			lis.each(function(){
				var _this = $(this);
				idStr += ","+_this.attr("rel");
			})
		}
		idStr += "," + category;
		if(idStr.length > 1){idStr = idStr.substr(1);}
	}
 
	return idStr ;
}
//根据问题id查询该问题下的 附件列表
function getAppendixList(catalogId){
	var value='';
	var para="catalogId="+catalogId;
	$.ajax({
		async:false,
		type:'post',
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionAppendixAction.action',
		dataType:'json',
		data:para,
		success:function(data){
           for(var i=0;i<data.length;i++){
              value+='<a style="color:blue" href="javascript:void(0)" onclick="question_answer_detailed('+data[i].file_id+')" >'+data[i].file_name+'</a><br/>';   
           }  
		}
	});
	return value;
}

//子页面调的方法
function refreshWindow(){
   window.location.reload();
}



//点击产品线返填商品类型
function ajaxLoadCatalogMenuPage(id){
	    //选择子级值 下拉列表
	    cleanSearchKey("");
		var para = "id="+id;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
			},
			error: function(){
			},
			success: function(html)
			{
				$("#categorymenu_td").html(html);
				
			}
		});
}

//高级查询控件事件
function query(){
	  var value=$("#seachInput").val();
	  var obj = {} ;
      obj["product_id"] =  $("#product_id").val();
      obj["search_key"] = value;
      obj["product_line"] = $("#product_line").val();
      obj["catalog_id"] = $("#catalog_id").val();
      obj["cmd"] = "page_query";
      obj["product_line_ids"] =  getSubProductLineId($("#category").val());
      obj["category"] = $("#category").val();
      loadQuestionPage(obj);
 
}

//选择子级值 下拉列表
function cleanSearchKey(divId){
	$("#"+divId+"search_key").val("");
}

function closeWinRefresh(){
	window.location.reload();
}

function search(){
    //组织数据
    var obj = {} ;
    obj["product_id"] =  $("#product_id").val();
    obj["search_key"] = $("#seachInput").val();
    obj["product_line"] = $("#product_line").val();
    obj["catalog_id"] = $("#catalog_id").val();
    obj["cmd"] = "click_query";
    loadQuestionPage(obj);
}
</script>

</head>
<body onload="onLoadInitZebraTable();"> 

 <!-- 右键隐藏div -->
    <div id="rightList" align="left" style="width:100px; border:1px #999 solid; display:none; position: fixed; z-index:999; background-color:#FFF;border-right:2px #CCC outset;border-bottom:2px #CCC outset">
     
	   <div id="rightAddQuestion" style="padding-left:13px; height:20px; line-height:20px; cursor:pointer" >添加问题</div>
	   <hr size="1" color="#CCCCCC"/>
	   <div id="rightAddChild" style="padding-left:13px; height:20px; line-height:20px; cursor:pointer" >添加子类</div>
	   <div id="rightAddQuestionCatalog" style="padding-left:13px; height:20px; line-height:20px; cursor:pointer" >添加同级分类</div>
	   <hr id="rightHr" size="1" color="#CCCCCC"/>
	   <div id="rightUpdateQuestionCatalogName" style="padding-left:13px; height:20px; line-height:20px; cursor: pointer" >修改分类名</div>
	</div>
    
	<form action="">
		<input type="hidden" id="catalog_id" name="catalog_id" value="0" />
		<input type="hidden" id="product_line" name="product_line" value="0" />
	</form>
	<div style="border-bottom:2px #CCC solid; height:25px;">
	   <img width="17" height="12" align="absmiddle" alt="title" src="../imgs/page_title.gif">
	   <span style="font-size:14px; font-weight: bold;">内容应用》》问题分类</span>
	</div>
	<br/>
	<div align="right" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	   <input class="long-button-add" type="button"  value="添加分类 "  onclick="" />
	   <input class="short-short-button-del" type="button" value="删除" />
	</div>
	<br/>
	<div id="tabs" >
		<ul>
			<li><a href="#product">产品问题</a></li>
			<li><a href="#carriage">运费问题</a></li>		 
		</ul>
		<!-- 选项卡产品问题内容 -->
		<div id="product" style="padding:1em 0.5em;">
		    <!-- 容器 左树 右问题 -->
		    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
		       <tr>
		         <td style="width:19%;" valign="top" algin="left">
		            <!-- 左侧树 -->
						<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0"  class="zebraTable">
							<tr>
							   <td>
								   <script type="text/javascript">
										d = new dTree('d');
										var productLine='产品线</td>'+
															  '<td align="left" valign="middle" width="20%">&nbsp;</td>'+
													    	  '<td align="left" valign="middle" width="8%">&nbsp;</td>'+
															  '<td valign="middle" width="22%" align="left" >'+
																 '&nbsp;'+
															  '</td>'+
															'</tr>'+
														 '</table>';
										d.add(0,-1,productLine);
										<% DBRow [] pLines = productLineMgrTJH.getAllProductLine();%>
										<% for(int t=0;t<pLines.length;t++){%>
										   var productLineId=<%=pLines[t].get("id",0l)%>;			
										   var lineName="";
										   <%if(pLines[t].getString("name").length()>9){%>
										      lineName="<%=pLines[t].getString("name").substring(0,9)%>..."
										   <%}else{%>
										      lineName="<%=pLines[t].getString("name")%>";
										   <%}%>
										   //标记是否是未读
										   var call='&nbsp;';
										   <%if(callRow!=null){%>
										      <%for(int p=0;p<callRow.length;p++){%>
										          <%if(callRow[p].get("product_line_id",0l)==pLines[t].get("id",0l)){%>
										             call='<span id="newproductline<%=callRow[p].get("product_line_id",0l)%>"><img src="./imgs/new.gif" /><span style="background-color:; color:red; font-weight: bold; font-size:9px;">(<span id="newcount<%=callRow[p].get("product_line_id",0l)%>"><%=callRow[p].get("count",0l)%></span>)</span></span>';
										          <%}%>
										      <%}%>
										   <%}%>
										   var line='<span onmousedown="pLineClick(1,this.title,'+productLineId+','+productLineId+',event,0,0,<%=url%>)" title="<%=pLines[t].getString("name")%>" >'+lineName+'</span></td>'+
											  	    '<td valign="middle" width="29%" align="left">'+
											 			call+
											  	    '</td>'+
											  	  '</tr>'+
											   '</table>';
										   d.add(<%=pLines[t].getString("id")%>,0,line,'javascript:void(0)'); 
										     <% DBRow[] questionCatalogs=questionCatalogMgrZwb.getQuestionCatalogByPid(pLines[t].get("id",0l));%>											 
											     <%for(int j=0;j<questionCatalogs.length;j++){%>
											         <%if(questionCatalogs[j].get("grade",0l)==1){%>
													         var catalogId=<%=questionCatalogs[j].getString("question_catalog_id")%>;
													         var catalogParentId=<%=questionCatalogs[j].getString("parent_id")%>
													         var catalogName="";
												             <%if(questionCatalogs[j].getString("catalog_title").length()>9){%>
												            	 catalogName="<%=questionCatalogs[j].getString("catalog_title").substring(0,9)%>...";
												             <%}else{%>
												            	 catalogName="<%=questionCatalogs[j].getString("catalog_title")%>";
												             <%}%>
													         var catalogTitle='<a onmousedown="pLineClick('+"'<%=pLines[t].getString("name")%>'"+',this.title,'+productLineId+','+catalogId+',event,1,'+"'<%=questionCatalogs[j].getString("catalog_title")%>'"+',<%=url%>)" href="javascript:void(0);" title="<%=questionCatalogs[j].getString("catalog_title")%>">'+catalogName+'</a>'+
								                                '<td valign="middle" width="2%" align="left">'+
		                                        					'&nbsp;'+
							                                    '</td>'+
													           '</tr>'+
													         '</table>';
														    d.add(<%=questionCatalogs[j].getString("question_catalog_id")+"-"+pLines[t].getString("id")%>,<%=questionCatalogs[j].getString("parent_id")%>,catalogTitle,'','','','img/catalogOpen.gif','img/catalog.gif');
												     <%}else if(questionCatalogs[j].get("grade",0l)==2){%>
														     var catalogId=<%=questionCatalogs[j].getString("question_catalog_id")%>;
													         var catalogParentId=<%=questionCatalogs[j].getString("parent_id")%>
														     var catalogName="";
												             <%if(questionCatalogs[j].getString("catalog_title").length()>9){%>
												            	 catalogName="<%=questionCatalogs[j].getString("catalog_title").substring(0,9)%>...";
												             <%}else{%>
												            	 catalogName="<%=questionCatalogs[j].getString("catalog_title")%>";
												             <%}%>
												             var catalogTitleSmall='<a onmousedown="rightClickItems('+"'<%=pLines[t].getString("name")%>'"+',this.title,'+productLineId+','+catalogId+',event,'+"'<%=questionCatalogs[j].getString("catalog_title")%>'"+',<%=url%>,'+catalogParentId+')" href="javascript:void(0);" title="<%=questionCatalogs[j].getString("catalog_title")%>">'+catalogName+'</a>'+
										                                '<td valign="middle" width="2%" align="left">'+
				                                     					'&nbsp;'+
									                                    '</td>'+
															           '</tr>'+
															         '</table>';
												            d.add(<%=questionCatalogs[j].getString("question_catalog_id")+"-"+pLines[t].getString("id")%>,<%=questionCatalogs[j].getString("parent_id")+"-"+pLines[t].getString("id")%>,catalogTitleSmall,'','','','img/empty.gif','img/empty.gif');
												     <%}%>
												 <%}%>     
										<%}%>
										document.write(d);
										//打开某级 时同级关闭
										d.config.closeSameLevel=true;
										
									</script>
								</td>
							</tr>
					    </table>
		         </td>
		         <td valign="top" algin="center">
		            <!-- 过滤 -->
					   <div style="width:97%; margin:0px auto; border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
				
				          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
				          	<tr>
				          		<td colspan="2" width="420px;" style=" padding-left:6px; font-size:12px; font-weight: bold; color:#00F" id="pLineClick">
				          			问题分类目录
				          		</td>
				          	 </tr>
				          	<tr>
				          		<td colspan="2">
				          			<div id="showDiv" style="display:none;">
							          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
								          	<tr>
												<td width="510px;" height="30xp;" style="padding-left:5px;" id="categorymenu_td" class ="categorymenu_td"  style="display:none;">
								          			<!-- 商品分类 -->
								          			<ul id="categorymenu" class="mcdropdown_menu">
														  <li rel="0">所有商品分类</li>
														  <%DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
														    for (int i=0; i<c1.length; i++){
																out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
																  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
																  if (c2.length>0){
																  		out.println("<ul>");	
																  }
																  for (int ii=0; ii<c2.length; ii++){
																		out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
																			DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
																			  if (c3.length>0){
																					out.println("<ul>");	
																			  }
																				for (int iii=0; iii<c3.length; iii++){
																						out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																						out.println("</li>");
																				}
																			  if (c3.length>0){
																					out.println("</ul>");	
																			  }
																		out.println("</li>");				
																  }
																  if (c2.length>0){
																  		out.println("</ul>");	
																  }
																out.println("</li>");
														  }%>
													</ul>
													<input type="text" name="category" id="category" value="" />
					                            	<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />     
								          		</td>
								          		<td>
									          		<table width="100%" border="0" cellpadding="0" cellspacing="0">
									          			<tr>
									          				<td  width="45%">
									          					产品：<span class="STYLE1 STYLE2">
																      		<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" onBlur="checkProduct()"/>
																      		<span id="errorMessage">&nbsp;</span>
															      	  </span>
															      	  <input type="hidden" id="backurl" name="backurl" value="<%=backurl%>">
															          <input type="hidden" id="product_id" name="product_id"/>	
									          				</td>
									          			 </tr>
									          		 </table>
									          		 <script type="text/javascript">
									          				 	addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");		
									          		 </script>	
												</td>	
								          	</tr>
							           </table>	
							       </div>   
				          		</td>
				          	</tr>
				          	<tr>
				          		<td>
			          				<div id ="search">
			          					  <!-- 搜索文本框 -->
								          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
								             <tr>
								            	<td width="18%" align="left" style="padding-top:3px;padding-left: 5px" nowrap="nowrap">
									            	 <div id="easy_search_father">
															<div id="easy_search" style="margin-top:-2px;">
																<a href="javascript:query()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a>
															</div>
													 </div>
									            	 <table width="400" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td width="418">
																	<div  class="search_shadow_bg">
																	 <input name="seachInput" id="seachInput" value=""  type="text" class="search_input" style="font-size:14px;font-family:  Arial;color:#333333;width:400px;font-weight:bold;"   onkeydown="if(event.keyCode==13)query()"/>
																	</div>
																</td>
															</tr>
													 </table>	
												</td>	  
												<td>
													  <div id="imageUnfold" sytle="cursor:pointer;">
													  		<img alt="展开" src="../imgs/arrow_down.png" id="unfold" onclick="unfoldInfo();">
													  </div>
												</td>  
								             </tr>
								          </table>
								     </div>
		          				  </td>
		          				  <td width="420px;" height="30xp;" style=" padding-left:6px;">
					          		 <div style="display:none;">
						          		    标题<input name="title" type="checkbox" id="title" />
						          		    原文<input name="question_title" type="checkbox" id="question_title" />
						          		    描述<input name="question_describe" type="checkbox" id="question_describe"/>
						          		    附件<input name="appendix" type="checkbox" id="appendix" />  
					          		 </div>    
				          		 </td>
				          	</tr>
				          	</table>
				       </div>
				       <br/>
				       <!-- 问题容器 -->
					   <div id="container" style="width:97%; margin:0px auto; border:2px #dddddd solid; padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
						       
					   </div>    
		         </td>
		       </tr>
		    </table>
		</div>		
		<!-- 运费问题内容 -->
		<div id="carriage">
	                     这是运费问题
	    </div>
	</div>
</body>

<script>

	//展开显示头信息
	function unfoldInfo(){
		$("#unfold").css("cursor","pointer");
		$("#showDiv").slideToggle(500);
		$("#imageUnfold").html("<img alt='收起' src='../imgs/arrow_up.png' id='stop' onclick ='stopinfo();'>");
	}
	//收起隐藏头信息
	function stopinfo(){
		$("#stop").css("cursor","pointer");
		$("#showDiv").slideToggle(500);
		$("#imageUnfold").html("<img alt='展开' src='../imgs/arrow_down.png' id='unfold' onclick='unfoldInfo();'>");
	};
</script>
<script type="text/javascript">

//验证
function checkProduct(){
	var p_name = $("#p_name").val();
	if(p_name!="" && $.trim(p_name).length > 0){
		var para ="p_name="+p_name;	
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
				},
				error: function(){
					alert("提交失败，请重试！");
				},
				success: function(date){
					if(date["product_id"]>0){
						$("#product_id").val(date["product_id"]);				
						$("#errorMessage").html("");
					}else{
					    $("#product_id").val("");
						$("#errorMessage").html("<img src='../imgs/product/warring.gif' width='14' height='12' align='absmiddle'><span style='color:#FF0033'><strong style='font-size: 11px;'>无此商品</strong></span>");
					}
				}
		});
	}else{
	
	}
}


//加载商品分类控件
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name){
					$("#filter_pcid").val(id);
				}
});
$("#category").mcDropdown("#categorymenu").setValue(0);


function loadQuestionPage(obj){
    if(!obj){obj = {};}
    $.ajax({
		url:'<%= questionListUri%>',
		dataType:'html',
		data:jQuery.param(obj),
		success:function(data){
			 $("#container").html(data);
		}
	});
}
//loadQuestionPage();


//查询字符串的Suggest
$("#seachInput").autocomplete({
	       dataType:'json', 
		   html:true,
			source:function( request, response ){
				$.ajax({
				    url: '<%= suggestUri%>',
					dataType:'json',
					data:{key_world:$("#seachInput").val()},
					success: function( data ) {
						 response( $.map( data, function( item ) {
						 	return {
						 		label:highlight(item["value"],$("#seachInput").val()),
						 		value: item["value"]
						 	};
						 }));
					}
	
				})
		   } 
	});

//判断 是否是跟进的 如果是跟进的就加载list页面
(function(){
	var genjin='<%=genjin%>';
	var catalogId=<%=productLineId%>;
	if(genjin=='genjin'){
		//返回所有问题  
		$.ajax({
			url:'<%= questionListUri%>',
			dataType:'html',
			data:'catalog_id='+catalogId+'&genjin='+genjin+'&p=1',
			success:function(data){
				 $("#container").html(data);
			}
		});    
	}
})();
</script>
<!-- windows -->
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<script src="../js/window/jquery.window.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../js/window/jquery.window.css" />
</html>