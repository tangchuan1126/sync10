<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cwc.app.key.QAKey"%>
<%@ include file="../../include.jsp"%> 
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%
long question_id = StringUtil.getLong(request,"question_id");
DBRow question_detail = qaMgrZJ.getDetailQuestionById(question_id);
DBRow product;
DBRow[] answers = qaMgrZJ.getAnswersByQuestionId(question_id);
%>
<%
	if(question_detail==null)
	{
		question_detail=new DBRow();
		product = new DBRow();
	}
	else
	{
		product = productMgr.getDetailProductByPcid(question_detail.get("product_id",0l));
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>产品问题</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>


<style type="text/css">
<!--
.nav-title
{
	font-size:12px;
	color:#999999;
	border-bottom:1px #dddddd solid;
	height:40px;
}

#content ul li
{
	font-size:14px;
	font-weight:bold;
	color:#0066CC;
}
-->
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客服问答 »   问题管理</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="1189" align="left" valign="top" style="padding-left:10px;">
	<table width="99%" border="0" cellspacing="0" cellpadding="6">
      <tr>
        <td class="nav-title"><img src="nav_logo.gif" width="16" height="16" align="absmiddle">
          <span style="color:#999999" id="title">
          相关分类：
		  <%
		  Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
		  DBRow allFather[] = tree.getAllFather(product.get("catalog_id",0l));
		  for (int jj=0; jj<allFather.length-1; jj++)
		  {
		  	out.println(allFather[jj].getString("title"));
		
		  }
		  %>
		   » 
	      <%
		  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(product.getString("catalog_id")));
		  if (catalog!=null)
		  {
		  	out.println(catalog.getString("title"));
		  }
		  %></span>
		  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#000000" style="font-size:16px; font-weight:bolder">对应产品:<%=product.getString("p_name")%></font></td>
        <td align="right" class="nav-title">&nbsp;</td>
      </tr>
    </table>
      <br>
      <table width="99%" border="0" cellspacing="0" cellpadding="0" id="test">

        <tr>
          <td align="left" valign="middle" style="font-size:20px;font-weight:bold;color:#000000;padding-left:10px;">
    <%=question_detail.getString("question_title")%>
          <%
		  if (question_detail.getString("question_title").equals(""))
		  {
		  	out.println("问题不存在！");
		  }
		  %>		  </td>
          <td align="right" valign="middle" style="font-size:20px;font-weight:bold;color:#000000;padding-left:10px;">
          	<tst:authentication bindAction="com.cwc.app.api.zj.QAMgrZJ.modQuestionHasAnswer">
          	<input type="button" class="long-button-mod" value="修改问题" onclick="modQuestion()"/>
          	</tst:authentication>
          </td>
        </tr>
        <tr>
          <td align="left" colspan="2" valign="middle" style="font-size:13px;font-weight:normal;color:#999999;padding-bottom:40px;padding-left:10px;font-family:Arial, Helvetica, sans-serif">
		  <% 
          		if(!question_detail.getString("question_title").equals(""))
          		{
          	%>
          		<%=question_detail.getString("questioner")%> | <%=question_detail.getString("question_update_time") %>
          	<%
          		}
          	%>			</td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="top" id="content" style="padding-left:10px;color:#333333;line-height:30px;">
			<% 
          		if(!question_detail.getString("question_title").equals(""))
          		{
      				out.print(question_detail.getString("content"));
          		}
        	%>		</td>
        </tr>
    </table>
	</td>
  </tr>
  <tst:authentication bindAction="com.cwc.app.api.zj.QAMgrZJ.addAnswer">
  <% 
  	if(question_detail.get("question_status",0)==QAKey.UNANSWERED)
  	{
  %>
  <tr>
  	<td style="padding-top: 20px;">
  		<form action="" method="post" name="answer_form">
  		<div style="padding: 10px;background-color:#EFF4EA;" align="right">
  		<div align="left">
  		<span style="font-size: 30;">请在下方填写答案</span>
  		</div>
  		<textarea rows="50" cols="80" name="answer_content" id="answer_content" style="width: 100%;height: 800px"></textarea>
  		<br/>
  		<input type="button" value="提交回答" onClick="answerQuestion()" class="normal-green"/>
    	</div>
    		<script type="text/javascript">
			CKEDITOR.replace('answer_content' ,
				{
					 height:300,
					 
	        		 filebrowserUploadUrl : '/uploader/upload.php',
	        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
				}
			);				
			</script>
			<input type="hidden" name="question_id" id="question_id" value="<%=question_id %>"/>
			<input type="hidden" name="answer_content" id="answer_content"/>
			<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request) %>">
		</form>
  	</td>
  </tr>
  <%
  	}
  %>
  </tst:authentication>
  <tr>
  	<td>
  		<%
  			if(answers.length>0)
  			{
  				for(int a = 0;a<answers.length;a++ )
  				{
  		%>
			<table cellpadding="3" cellspacing="3" width="100%">
				<tr>
					<td style="border-top:1px #dddddd dashed;">
						<div align="right">
							<div align="left" style="padding-bottom: 10px;" id="answer">
								<div id="show<%=answers[a].getString("answer_id")%>">
								<%=answers[a].getString("answer_content") %>
								</div>
								<div id="mod<%=answers[a].getString("answer_id")%>" style='padding: 10px;background-color:#CEDEC1;display: none' align='right'>
						  		<div align='left'>
						  		<span style='font-size: 30;'>请在下方填写答案</span>
						  		</div>
						  		<textarea rows='50' cols='80' name='mod_content_<%=answers[a].get("answer_id",0l) %>' id='mod_content_<%=answers[a].get("answer_id",0l) %>' style='width: 100%;height: 300px'><%=answers[a].getString("answer_content") %></textarea>
						  		<br/>
						  		<input type='button' value='修改回答' onclick='modAnswerSubmit(<%=answers[a].get("answer_id",0l) %>)' class='normal-green'/>
						  		<input type="button" name="Submit2" value="取消" class="normal-white" onClick="cancel(<%=answers[a].get("answer_id",0l) %>)">
						    	</div>
							</div>
							<span id="answer_information<%=answers[a].getString("answer_id")%>" style="font-size: 13px;">回答人：<%=answers[a].getString("answerer") %>&nbsp;|&nbsp;<%=answers[a].getString("answer_time")%>&nbsp;<tst:authentication bindAction="com.cwc.app.api.zj.QAMgrZJ.modAnswer">|&nbsp;<input type="button" class="long-button-mod" onClick="modAnswer(<%=answers[a].getString("answer_id")%>)" value="修改答案"/></tst:authentication>
							</span>
						</div>
					</td>
				</tr>
			</table>  		
  		<%
  				}
  			}
  		%>
  	</td>
  </tr>
</table>

<script type="text/javascript">
	function addColor()
	{
		key = "";
		if(key!="")
		{
			var str = document.getElementById('test').innerHTML;
			var title = document.getElementById('title').innerHTML;
   			re = new RegExp(key,'g');//创建含变量的正则表达式
	   		newstr = "<span style='color:red'>" + key + "</span>";//将关键字替换成newstr的形式 
	   		targetstr = str.replace(re,newstr);//执行替换
	   		targettitle = title.replace(re,newstr); 
	   		document.getElementById('test').innerHTML = targetstr;//重新输出替换后的内容
	   		document.getElementById('title').innerHTML = targettitle;
		}
	}
	addColor();
	function answerQuestion()
	{
		var f = document.answer_form;
		
		 if (CKEDITOR.instances.answer_content.getData() == "")
		 {
		 	
			alert("请填写答案的内容");
		 }
		 else
		 {
			document.answer_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/answerQuestion.action";
			document.answer_form.submit();	
		 }
	}
	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	
	$().ready(function(){

		function log(event, data, formatted) 
		{
	
			
		}
	})
	
	<%
		if(answers.length>0)
		{
	%>
	function modAnswer(answer_id)
	{
		$("#mod"+answer_id).css("display","");
		$("#show"+answer_id).css("display","none");
		$("#answer_information"+answer_id).css("display","none");
		var c = CKEDITOR.instances['mod_content_'+answer_id];
        if(c)
        {
        	CKEDITOR.instances['mod_content_'+answer_id].destroy();
        }
      	 CKEDITOR.replace('mod_content_'+answer_id,
				{
					 height:300,
				     filebrowserUploadUrl : '/uploader/upload.php',
				     filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
				}
			);
	   
	}
	<%
		}
	%>
	
	function modAnswerSubmit(answer_id)
	{
		if(CKEDITOR.instances['mod_content_'+answer_id].getData() == "")
		{
			alert("请填写答案内容");
		}
		else
		{
			document.update_answer_form.answer_id.value=answer_id;
			document.update_answer_form.answer_content.value = CKEDITOR.instances['mod_content_'+answer_id].getData();
			document.update_answer_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/modAnswer.action";
			document.update_answer_form.submit();
		}
	}
	
	function cancel(answer_id)
	{
		$("#mod"+answer_id).css("display","none");
		$("#show"+answer_id).css("display","");
		$("#answer_information"+answer_id).css("display","");
		CKEDITOR.instances['mod_content_'+answer_id].destroy();
	}
	
	function modQuestion()
	{
		if(confirm("此问题已被回答，确定要修改吗？"))
		{
			document.update_question_form.submit();
		}
	}
</script>
<form method="post" name="update_answer_form">
	<input type="hidden" name="answer_id"/>
	<input type="hidden" name="answer_content"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request) %>"/>
</form>
<form method="post" name="update_question_form" action="mod_question.html">
	<input type="hidden" name="question_id" value="<%=question_id%>">
	<input type="hidden" name="modType" value="1"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">
</form>

</body>
</html>
