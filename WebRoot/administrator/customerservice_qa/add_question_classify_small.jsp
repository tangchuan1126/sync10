<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.FileWithTypeKey"%>
<html>
<head>
<title>问题分类管理</title>

<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- 所有产品 -->
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet" media="all" />
<!-- 编辑控件 -->
<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<% 
    long catalogId=StringUtil.getLong(request,"catalogId");
    long productLineId=StringUtil.getLong(request,"productLineId");
    String productLineName=StringUtil.getString(request,"productLineName");
	String backurl = StringUtil.getString(request,"backurl");
	String catalogName=StringUtil.getString(request,"catalogName");
	String parentName=StringUtil.getString(request,"parentName");
	
	String convertFile = ConfigBean.getStringValue("systenFolder")+"action/administrator/knowledge/ConvertFileTestAction.action";
	String downLoadTempFileAction = ConfigBean.getStringValue("systenFolder") +"action/administrator/CommonFileDownLoadTempFolderAction.action";
	String knowledgeAddAction =   ConfigBean.getStringValue("systenFolder") +"action/administrator/knowledge/KnowledgeAddAction.action";

%>
<script>
(function(){
	$.blockUI.defaults = {
			 css: { 
			  padding:        '8px',
			  margin:         0,
			  width:          '170px', 
			  top:            '45%', 
			  left:           '40%', 
			  textAlign:      'center', 
			  color:          '#000', 
			  border:         '3px solid #999999',
			  backgroundColor:'#eeeeee',
			  '-webkit-border-radius': '10px',
			  '-moz-border-radius':    '10px',
			  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
			 },
			 //设置遮罩层的样式
			 overlayCSS:  { 
			  backgroundColor:'#000', 
			  opacity:        '0.6' 
			 },
			 baseZ: 99999, 
			 centerX: true,
			 centerY: true, 
			 fadeOut:  1000,
			 showOverlay: true
			};
})();

//加载编辑控件
var keepAliveObj=null;
function keepAlive(){
 $("#keep_alive").load("../imgs/account.gif"); 
 clearTimeout(keepAliveObj);
 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}
$().ready(function() {
function log(event, data, formatted) {	
}
 addAutoComplete($("#p_name"),"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action","p_name");
});


//文件上传
function uploadFile(_target){
    var targetNode = $("#"+_target);
    var fileNames = $("input[name='file_names']",targetNode).val();
    var obj  = {
	     reg:"question",
	     limitSize:2,
	     target:_target 
	 }
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
	uri += jQuery.param(obj);
	 if(fileNames.length > 0 ){
		uri += "&file_names=" + fileNames;
	}
	 $.artDialog.open(uri , {id:'file_up',title: '上传文件',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
		 close:function(){
				//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
				 this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
		 }});
}
//jquery file up 回调函数
function uploadFileCallBack(fileNames,target){
    $("p.new").remove();
    var targetNode = $("#"+target);
	$("input[name='file_names']",targetNode).val(fileNames);
	if($.trim(fileNames).length > 0 ){
		var array = fileNames.split(",");
		var lis = "";
		for(var index = 0 ,count = array.length ; index < count ; index++ ){
			var a =  createA(array[index]) ;
			 
			if(a.indexOf("href") != -1){
			    lis += a;
			}
		}
		var td = $("#over_file_td");
		
		td.append(lis); 	 
	} 
}
function createA(fileName){
    var uri = '<%= downLoadTempFileAction%>'+"?file_name="+fileName+"&folder=upl_imags_tmp";
    var showName = $("#sn").val()+"_" + fileName;
    var  a = "<p style='color:#F30' class='new' ><a href="+uri+" style='color:#00F' >"+showName+"</a>&nbsp;&nbsp;(New)</p>";
    return a ;
  
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
	<!-- 提问 -->
	<div id="addQuestion">
		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td colspan="2" align="center" valign="top">
				<form name="add_question_form" id="add_question_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/addQuestion.action" >
					<table width="98%" border="0" cellspacing="7" cellpadding="2">
						<tr>
							<input type="hidden" name="catalogId" value="<%=catalogId %>" />
							<input type="hidden" name="productLineName" value="<%=productLineName %>" />
							<td align="right" valign="middle" class="STYLE1 STYLE2">所在目录</td>
							<td>
							     <span style="color:blue; font-weight: bold;"> <%=parentName %></span>
								 <input type="hidden" name="productLine" id="productLine" value="<%=productLineId %>" />
								 <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"  /> 
								 <input type="hidden" name="ajaxContent" id="ajaxContent" />
							</td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="STYLE1 STYLE2">产品分类</td>
							<td id="categorymenu_td" class ="categorymenu_td">
								<!-- 商品分类 -->
			          			<ul id="categorymenu" class="mcdropdown_menu">
										  <li rel="0">所有商品分类</li>
										  <%DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
										    for (int i=0; i<c1.length; i++){
												out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));
												  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
												  if (c2.length>0){
												  		out.println("<ul>");	
												  }
												  for (int ii=0; ii<c2.length; ii++){
														out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
															DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
															  if (c3.length>0){
																	out.println("<ul>");	
															  }
																for (int iii=0; iii<c3.length; iii++){
																		out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
																		out.println("</li>");
																}
															  if (c3.length>0){
																	out.println("</ul>");	
															  }
														out.println("</li>");				
												  }
												  if (c2.length>0){
												  		out.println("</ul>");	
												  }
												out.println("</li>");
										  }%>
									</ul>
									<input type="text" name="category" id="category" value="" />
	                            	<input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  /> 
							</td>
						</tr>
						<tr>
						    <td width="8%" align="right" valign="middle" class="text-line" ><span class="STYLE1 STYLE2">商品名称</span></td>
						    <td align="left" valign="middle" >
						    	<span class="STYLE1 STYLE2">
						      		<input type="text" id="p_name" name="p_name"  style="color:#FF3300;width:300px;font-size:17px;height:25px;font-family:Arial, Helvetica, sans-serif;font-weight:bold" onBlur="checkProduct()"/>
						      		<span id="errorMessage"></span>
					      		</span>
						    </td>
						 </tr>
						 <tr>
						    <td align="right" valign="middle" class="STYLE1 STYLE2">问题标题</td>
						    <td align="left" valign="middle" >
						    	<input onkeyup="backFilling()" style="width: 300px;" name="question_title" type="text" class="input-line" id="question_title" >
						    </td>
						 </tr>
						 <tr>
						 	<td>&nbsp;</td>
						 	<td>
						 		<div id="ConPrompt" style="border:0px #093 solid; width:500px; display:none" >您要创建的问题是不是：<span id="prompt"></span></div>
						 	</td>
						 </tr>
						 <tr>
							  <td align="right" valign="middle" class="STYLE3" >
							    	问题内容
							  </td>
							  <td align="left" valign="middle" >
							    	<textarea  name="content" id="content" ></textarea>
							   		<script type="text/javascript">
							   		var editor=CKEDITOR.replace( 'content' ,
										{
											 height:150,			 
							        		 filebrowserUploadUrl : '/uploader/upload.php',
							        		 filebrowserImageUploadUrl : '<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/customerservice_qa/uploadImage.action?type=Images',//上传方法地址
										}
									);
									CKEDITOR.config.toolbarStartupExpanded = false;					
									</script>	
							    </td>
					 	  </tr>
					 	  <tr>
					 			<td  align="right" valign="middle" class="STYLE3" >
					 			 	上传附件
					 			</td>
					 			<td id="over_file_td">
					 			 	<input type="button" class="long-button" onclick="uploadFile('jquery_file_up');" value="上传附件" />
					 			 	<div id="jquery_file_up">
				 						<input type="hidden" name="file_names"/>
				 						<input type="hidden" name="sn" id="sn" value="K_knowledge"/>
				 						<input type="hidden" name="path" value="<%=systemConfig.getStringConfigValue("file_path_knowledge") %>"/>
				 						<input type="hidden" name="file_with_type" value="<%=FileWithTypeKey.KNOWLEDGE %>"/>
				 					</div>
					 			</td>
					 		</tr>		
					   </table>
					   <input type="hidden" id="backurl" name="backurl" value="<%=backurl%>">
					   <input type="hidden" id="product_id" name="product_id"/>
				</form>		
		    </td>
		  </tr>
		  <tr>
			  <td width="51%" align="left" valign="middle" class="win-bottom-line" >
			    	&nbsp;
			  </td>
			  <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
				  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addQuestion();" id="saveButton">
					 <%if(!backurl.equals("")){%>
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="window.location.href='question_index.html'">
					 <% }%>
				  &nbsp;
			  </td>
		  </tr>
		</table>
		<div style="display:none" id="keep_alive"></div>
	</div>
</body>
<script type="text/javascript">

function single(question_id,question_catalog_id){
	window.parent.singleSelect(question_id,question_catalog_id);
	$.artDialog && $.artDialog.close();
}

  function backFilling(){
	  var value=$("#question_title").val();
	    $.ajax({
	   	type:'post',
			url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AjaxQuestionHintAction.action',
			dataType:'json',
			data:'questionTitle='+value+'&productLineId='+<%=catalogId%>,
			success:function(json){	
				var html='';
				if(json.length==0){
					$('#ConPrompt').hide();
				}else{
					for(var i=0;i<json.length;i++){
						var question_id    = json[i].question_id;
						var question_title = json[i].question_title;
						var question_catalog_id=json[i].question_catalog_id;
						html+='<a href="javascript:single('+question_id+','+question_catalog_id+')" style="text-decoration:none;color:#F00;font-weight: bold;">'+question_title+'</a><br/>';
					}
					$('#prompt').html(html);
					$('#ConPrompt').show();
				}
			}
		});
    //CKEDITOR.instances.content.setData(value); 
  }

	 (function(){
	    var id=<%=productLineId%>;
	    ajaxLoadCatalogMenuPage(id);
	 })();

//加载商品品类下拉列表控件									
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
				function (id,name){
					$("#filter_pcid").val(id);
				}
});
		

//选择子级值 下拉列表
function cleanSearchKey(divId){
$("#"+divId+"search_key").val("");
}

//点击产品线返填商品类型
function ajaxLoadCatalogMenuPage(id){
		var para = "id="+id;
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/product/product_catalog_menu_for_productline.html',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:para,
			beforeSend:function(request){
			},
			error: function(){
			},
			success: function(html)
			{
				$("#categorymenu_td").html(html);
			}
		});
}

//添加问题
function addQuestion()
{
	var f = document.add_question_form;
	
	if (f.question_title.value==""){
		alert("请填写问题标题");
	 }
	 else if (CKEDITOR.instances.content.getData() == ""){
		 alert("请填写问题内容");
	 } else{
		//把内容控件内容赋值给隐藏域
		 var ajaxContent=editor.document.getBody().getText();  //取得纯文本
		 $('#ajaxContent').val(ajaxContent);
		 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		 $.ajax({
				type:'post',
				url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/customerservice_qa/AddQuestionByCatalogAction.action',
				dataType:'json',
				data:$("#add_question_form").serialize(),
				success:function(json){	
					$('#catalog_id', window.parent.document).val(<%=catalogId%>);
					//window.parent.search();
					window.parent.findQuestion(<%=catalogId%>,<%=catalogId%>,"question_create_time",1);
					$.artDialog && $.artDialog.close();
				}
		 });
	 }
}
//验证
function checkProduct(){
	var p_name = $("#p_name").val();
	if(p_name!=""){
	var para ="p_name="+p_name;
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
			},
			
			error: function(){
				alert("提交失败，请重试！");
			},
			
			success: function(date){
				if(date["product_id"]>0){
					$("#product_id").val(date["product_id"]);
					//$("#saveButton").css({display:"inline"});
					$("#errorMessage").html("");
				}else{
					$("#errorMessage").html("<img src='../imgs/product/warring.gif' width='16' height='15' align='absmiddle'><span style='color:#FF0033'><strong style='font-size: 14px;'> 无此商品，无法创建问题</strong></span>");
					//$("#saveButton").css({display:"none"});
				}
			}
		});
	}else{
		
	}
}
</script>
</html>