<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%>
<!-- ?????????????art??????????????????iframe  -->
 <%
 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<TITLE><%=systemConfig.getStringConfigValue("webtitle")%> &#8482; </TITLE>

<link rel="Shortcut Icon" href="/Sync10/favicon.png"  type="image/x-icon" /> 
<link rel="Bookmark" href="/Sync10/favicon.png" type="image/x-icon" />  

<style type="text/css">
	* { padding:0; margin:0; }
	html, body { height:100%; border:none 0; }
	#iframe { width:100%; height:100%; border:none 0; }
	
/* 	============================set top menu=========================================== */

.hide{display:none;}

.dropdown-menu li a {
  padding: 3px 20px;
  min-height: 0;
}
.dropdown-menu>li>a:hover,
.dropdown-menu>li>a:focus {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5;
}


.dropdown-menu {
 /* width: 300px; */
  position: absolute;
  top: 100%;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
}

ul, ol {
  margin-top: 0;
  margin-bottom: 10px;
  list-style: none;
  padding: 0;
  }

:before, :after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

:before, :after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  }

  .dropdown-menu li a {
  padding: 3px 20px;
  min-height: 0;
}

.dropdown-menu>li>a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: 400;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap;
}

a {
  color: #337ab7;
  text-decoration: none;
}
a {
  background-color: transparent;
}

/* ============================end top menu=========================================== */

</style>
<script src="/Sync10-ui/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	  -->
<script type="text/javascript" src="/Sync10-ui/bower_components/jquery.browser/dist/jquery.browser.min.js"></script>
<!-- <link type="text/css" href="js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" /> -->
<!-- <link type="text/css" href="js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/> -->

<!-- <link rel="stylesheet" type="text/css" href="js/art/skins/aero.css" /> -->
<!-- <script src="js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script> -->
<!-- <script src="js/art/plugins/iframeTools.source.js" type="text/javascript"></script> -->
<!-- <script src="js/art/plugins/iframeTools.js" type="text/javascript"></script> -->



<link type="text/css" rel="stylesheet" href="/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css">
<!-- <link type="text/css" rel="stylesheet" href="/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
<link type="text/css" rel="stylesheet" href="/Sync10-ui/bower_components/Font-Awesome/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="/Sync10-ui/lib/Miniwindow/css/style.css">
<!-- <link type="text/css" rel="stylesheet" href="/Sync10-ui/bower_components/Font-Awesome/fonts/fontawesome-webfont.woff"> -->

<script src="js/window/jquery.window.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="js/window/jquery.window.css" />
<link rel="stylesheet" type="text/css" href="js/sticky/sticky.full.css" />

<script type="text/javascript" src="js/sticky/sticky.full.js"></script>
<!-- old miniwindow -->
<!-- <script src="/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/Miniwindow/lhgdialog/lhgcore.lhgdialog.min.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/Miniwindow/webim_win.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/Miniwindow/nicEdit.js" type="text/javascript"></script>
<script src="/Sync10-ui/bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/Sync10-ui/lib/Miniwindow/Recorder.js"></script> -->
<!-- new miniwindow -->
<script src="/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script src='/Sync10-ui/lib/strophejs-1.2.0/js/strophe.js'></script>
<script src='/Sync10-ui/lib/strophejs-1.2.0/js/flensed/flXHR.js'></script>
<script src='/Sync10-ui/lib/strophejs-1.2.0/js/strophe.flxhr.js'></script>
<script type="text/javascript" src="../administrator/js/jquery/jquery.cookie.js"></script>
<script src="/Sync10-ui/lib/webIM-Client/lhgdialog/lhgcore.lhgdialog.min.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/webIM-Client/webim_win.js" type="text/javascript"></script>
<script src="/Sync10-ui/lib/webIM-Client/nicEdit.js" type="text/javascript"></script>
<script src="/Sync10-ui/bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/Sync10-ui/lib/webIM-Client/Recorder.js"></script>
<script type="text/javascript" src="/Sync10-ui/lib/Miniwindow/Recorder.js"></script>


<link rel="stylesheet" type="text/css" href="/Sync10-ui/lib/artDialog/dist/ui-dialog.css" />
<script src="/Sync10-ui/lib/artDialog/dist/dialog-min.js" "text/javascript"></script>
<script type="text/javascript">

//============================set top menu===========================================
	var topbtn = "";
	
	/*
function top_dropdown_menu(btn,openmenu,droplist){
	$("#dropdown-"+openmenu).html(droplist);	
	
	topbtn = btn;
    var docleft = btn.offset().left;
    var left = docleft + btn.parents(".dropdown").eq(0).width();
    $(".dropdown-menu").hide();
    var winw = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var riw = winw - left;
    var menu_box = $(".dropdown-" + openmenu + "");
    menu_box.css({
        right: riw + "px",
        top: "47px",
        "display": "block"
    });
    topbtn.css("background-color", "#ff");
    
};


//2015-05-13 19:00

 */
 
 var mun_setTimeout = "";
 
function top_hover(topbtn,ulidsring) {

        var dropdown = $(".dropdown-" + ulidsring + "");        

        

         var showul=$(".dropdown-menu:not(:hidden)");
         if(showul.length > 0){
         	if(!showul.hasClass("dropdown-" + ulidsring + "")){
                showul.hide();  
         	}
         }


        if (mun_setTimeout != "" && typeof mun_setTimeout != "undefined") {
            window.clearTimeout(mun_setTimeout);
        }

        if (menutimeset != "" && typeof menutimeset != "undefined") {
            window.clearTimeout(menutimeset);
        }

       if(dropdown.is(":visible")){
        mun_setTimeout = window.setTimeout(function() {
            dropdown.hide().removeClass('hide');
            topbtn.removeClass('active');
            topbtn.css("background-color", "transparent");
        }, 300);
    }
    

    }
    
    
 
    //------------end 2015-05-13 17:51------------
var menutimeset = "";
$(function() {

  
    var menulall = $(".dropdown-menu");
    menulall.hover(function() {
    	if (mun_setTimeout != "" && typeof mun_setTimeout != "undefined") {
            window.clearTimeout(mun_setTimeout);
        }
        var ulthis = $(this);
        ulthis.show();

    }, function() {
        if (menutimeset != "" && typeof menutimeset != "undefined") {
            window.clearTimeout(menutimeset);
        }

        menutimeset = window.setTimeout(function() {

            menulall.hide();
            topbtn.removeClass('active');
            //2015-05-13 --new style
            topbtn.css("background-color", "transparent");


        }, 300);

    });



    var __lia = menulall.find("li");
    __lia.click(function(e) {
        e.stopPropagation();
        var _this_a = $(this).find('a');
        var urlsring = _this_a.attr("href");
        if (!/^javascript:/.test(urlsring))
            mainFrameOpenUrl(urlsring);
        return false;
    });

})


//============================end top menu===========================================

var windowSchedule ;
var windowBill ;
var windowPictureShow ;


function logout()
{
	var d = dialog({
		width:200,
	    title:' ',
	    content: '<i class="fa fa-exclamation-triangle" style="font-size:14px;color:yellow;"></i>&nbsp;&nbsp;Logout ?',
	    opacity:0.2,
	    button: [{
	            value: 'Logout',
	            callback: function () {
	            	document.logout_form.submit();
	            },
	            autofocus: true
	        },
	        {
	            value: 'Cancel',
	            callback: function () {
	            	$(this).hide();
	            }
	        }]
	});
	d.showModal();
}

function openWindowAtBottom(){
    //???????????????????????????????????????????????
    //?????????????????????????λ??
    
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/schedule_management/notify_schedule.html"; 
    if(windowSchedule){
			 $.window.getWindow(windowSchedule.getWindowId()).setUrl(uri);
    }else{
			var winWidth = ($(window).width())*1 - 320 - 2;
			var winHeight = ($(window).height())*1 - 240 -2;
			windowSchedule = $.window({
				title: "VVME",
				url: uri,
				resizable: false,
				width: 320,
				height: 240,
				maxWidth: 400,
				maxHeight: 300,
				x:winWidth,
				y:winHeight,
				resizable: false,
				maximizable: false,
				minimizable: false,
				showFooter: false,
				scrollable:false,
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
	    
		 
    }
}
function mainFrameOpenUrl(url){
    window.frames["iframe"].frames["main"].location.href=url;
}
var settings = {
	'speed' : 'fast',	// animations: fast, slow, or integer
	'duplicates' : true,	// true or false
	'autoclose' : false // integer or false
};
function showSystemNotifyContent(content){
   
    if(content.length > 0){
   		 $.sticky(content,settings);
    }
}
function sheduleAClick(schedule_id , is_schedule){
    var url =  '<%=ConfigBean.getStringValue("systenFolder")%>' +"administrator/schedule_management/show_schedule.html";
	url += "?schedule_id="+schedule_id+"&is_schedule="+is_schedule+"&from=out";
	mainFrameOpenUrl(url);
}
function openBillBy(){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/order/add_bill.html"; 
    if(windowBill){
			 $.window.getWindow(windowBill.getWindowId()).setUrl(uri);
    }else{
			 
			windowSchedule = $.window({
				title: "Invoice",
				url: uri,
				resizable: false,
				width: 1100,
				height: 600,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    }
}
//在线查看图片的页面
function openPictureOnlineShow(obj){
	//如果已经存在那么就要刷新这个页面。不过不是的话那么就打开重新建立一个window
	//如果是分辨率小于1440那么就
	var fixWidth = 1100, fixHeight = 700; 
	if($(window).width() * 1 < 1400){
	    fixWidth = 900;
	    fixHeight = 500 ;
	}
	 
	
	if(!obj){return ;}
	var fileWithType = obj.file_with_type;
	var param = jQuery.param(obj);
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/picture_online_show.html?" +param; 
    //如果fileWithType为52（即checkIn模块 使用新的图片查看 -------wfh
	if(fileWithType==52){
		uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + 'administrator/file/picture_online_show_check_in.html?'+param;
	//	$.artDialog.open(uri , {title: "Show Pictures",width:fixWidth,height:fixHeight, lock: true,opacity: 0.3,fixed: true});
	}
		if(windowPictureShow){
		    $.window.getWindow(windowPictureShow.getWindowId()).setUrl(uri);
		}else{
			    windowSchedule = $.window({
					title: "Photo Preview",
					url: uri,
					resizable: false,
					width: fixWidth,
					height: fixHeight,
					showModal:true,
					x:-1,
					y:-1,
					showFooter: false,
				 
					withinBrowserWindow:true,
					checkBoundary:true,
					onClose:function(){windowSchedule = false;}
				});
		}
	
	
    
}
//OFfice文件在线阅读
function openOfficeFileOnlineShow(file_id){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/customerservice_qa/show_file_online.html?file_id="+file_id; 
	 windowSchedule = $.window({
		title: "File look up",
		url: uri,
		resizable: false,
		width: 800,
		height: 530,
		showModal:true,
		x:-1,
		y:0,
		showFooter: false,
	 
		withinBrowserWindow:true,
		checkBoundary:true,
		onClose:function(){}
	});
} 
function openUrl(uri){
 	 windowSchedule = $.window({
		title: "Webcam",
		url: uri,
		resizable: false,
		width: 860,
		height: 660,
		showModal:true,
		x:-1,
		y:-1,
		showFooter: false,
		withinBrowserWindow:true,
		checkBoundary:true,
		onClose:function(){}
	});
} 
/**
 * 打开android打印的任务列表
 */
function openPrintTaskList(who){
    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/print/print_task_list.html?who="+who; 
    
			 
			windowSchedule = $.window({
				title: "Print Task",
				url: uri,
				resizable: false,
				width: 350,
				height: 600,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    
}
function openPrintTaskPreview(uri){
			windowSchedule = $.window({
				title: "Task Preview",
				url: uri,
				resizable: false,
				width: 900,
				height: 500,
				showModal:true,
				x:-1,
				y:-1,
				showFooter: false,
			 
				withinBrowserWindow:true,
				checkBoundary:true,
				onClose:function(){windowSchedule = false;}
			});
    
}
</script>
</head>
<body>
<form name="logout_form" method="post"  action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/adminLogout.action" target="_top">
	<input type="hidden" name="backurl" value="/Sync10/administrator/admin/"/>
	<input type="hidden" name="type" value="<%=adminLoggerBean.getAttach()==null?"admin":"supplier"%>"/>
</form>

<!-- ============================set top menu=========================================== -->
	<ul id="dropdown-user" class="dropdown-menu dropdown-user hide">
	    <li><a href="schedule_management/show_schedule.html"><i class="fa fa-user fa-fw"></i> Fei Han</a></li><li><a href="javascript:void(0);"><i class="fa fa-gear fa-fw"></i>Valley</a></li><li class="divider"></li><li><a href="javascript:void(0);"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li></ul>
	<!-- <ul id="dropdown-bell" class="dropdown-menu dropdown-bell hide" style="width:100px;"><li><a href="help_center/index.html" target="_blank">Help</a></li></ul> -->
	<ul id="dropdown-bell" class="dropdown-menu dropdown-bell hide" style="width:100px;"><li><a href="schedule_management/show_schedule.html" target="main"><i class="fa fa-tasks fa-fw"></i> TASK</a></li></ul>

<!-- ============================end top menu=========================================== -->

	<div id="galleria" style=""></div>
	<!-- <iframe id="iframe" name="iframe" src="frameset.html"></iframe> -->
	<iframe id="iframe" name="iframe" src="homePage.html"></iframe> <!-- frameset.html -->
	<div id="miniwindow"></div>
<script type="text/javascript">
$(function() {
	
	//logout();
	
	
	var pah_="/Sync10/action/pushMessageByOpenfire/";
   	var crewin={
        renderTo: "body",
        //用户列表
        dataUrl:pah_+"PushMessageByOpenfireAction.action",                        
        postData:{Method:"AquireContacts"},
        //监听当前用户所有消息
        ListeningmsgData:{Method:"RoundRobin"},
        //当前用户发起消息
        AmessageData:{Method:"SendMsg"},
        scrollH: 500,
        refreshdata:1000,//毫秒，刷新
        openbtntxt:"私信聊天",
        fileservpath:"/Sync10/_fileserv/upload"

    }

	/*  var win = new webim_win(crewin);
   	window.onunload=function(){
	     var rid=Chat.connection.rid;
	     $.cookie('rid', rid,{ path: '/Sync10/' });
	}  */
});
</script>
</body>
</html>
