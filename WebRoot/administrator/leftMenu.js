require(['/Sync10-ui/requirejs_config.js'], function() {


	require(["jquery", "bootstrap", "/Sync10-ui/lib/Navigation_Search/NavigationMenu.js", "json2htmlEvents", "mCustomScrollbar"], function($, bootstrap, NavigationMenu, jqjson2html) {


		function windowofset() {

		var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;

		return {
			h: height,
			w: width
		}
	};

	  /* 2015-05-19 */
    var bootmh=88;
    function re_window_h(){

         var Scrollbar_tbody_h = $(window).outerHeight() - bootmh;
            $("#Compond_navbar .mCustomScrollbar").height(Scrollbar_tbody_h);

    };
	
		$(function() {

      var _h=windowofset().h;
      var _w=windowofset().w;    
      var dom=window.parent.document;
      //top.window
			//Menu,参数：容器，资源路径，点击事件,搜索按钮
      /* 2015-05-19 */
      var Menuset = {
          renderTo: "#Compond_navbar",
          dataUrl: "/Sync10/action/loginFunctionMenu.action",
          scrollH: (_h - bootmh)
      };

      var left_meun = new NavigationMenu(Menuset);
      left_meun.render();

      left_meun.on("events.showMenuItem", function(itemData) {

          var main = $(dom).find("frame[name='main']");
          //{itemData.url,itemData.itme}
          main.attr("src", itemData.url);

      });

      left_meun.on("events.menuItemError", function(itemData) {

          window.parent.window.alert(itemData.error);

      });
			
			/* 2015-05-19 */
            $(window).resize(function() {
                /* Act on the event */
               re_window_h();
            });

		});

	});


});