 <%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp" %>
<%@ page import="org.jbpm.api.task.*" %>
 <%@ page import="com.cwc.app.beans.AdminLoginBean"%>
 <%@ page import="java.util.List" %>
 <%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
 <link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
 
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<html>
  <style type="text/css">
<!--
.STYLE2 {color: #EEEEEE}
.STYLE4 {color: #000000}
.STYLE5 {color: #FF0000}
-->
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<table width="100%"  height="100%"  border="0" align="center" cellpadding="3" cellspacing="0">
<tr>
	<td colspan="2"     height="20"  style="border-bottom:1px #cccccc solid">
		<table width="100%" height="20" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td  align ="center" >&nbsp;&nbsp;<span style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  ">退款申请被驳回</span></td> 
	  		</tr>
		</table>	</td>
</tr>
<tr>
<td colspan="2" align="center" > <span class="STYLE5">&nbsp;&nbsp; 请根据驳回理由对申请信息进行修改再重新申请</span></td>
</tr>
<tr>
<td colspan="2" >
  <%
  		String taskId = request.getParameter("task_id");
  %>
  <form name="refundForm"   method="post" action="">
    <input id="taskId" name="taskId" type="hidden" value="<%=taskId %>">
    <input id="tranID" name="tranID" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"tranID") %>">
    <input id="oid" name="oid"  type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"oid") %>">
    <table  width="95%"  height="100%" border="0"    align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc" >
      <tr id="refundTypeHistory">
        <td width="31%" height="30" align="center" bgcolor="#eeeeee" >退款类型： </td>
        <td width="69%" height="30" align="left" bgcolor="#FFFFFF"  >
        <% String refundType = jbpmMgr.getTaskStringVaviable(taskId,"refundType");
		  			 		if(refundType.equals("Full"))
		  			 		{
			  			 		%> 全额退款  <%
		  			 		}else
		  			 		{
		  			 			%>  部分退款  <%
		  			 		}%>	    </td>
      </tr>
					      <%  if(!refundType.equals("Full"))
							  {
		  			 	  %>
      <tr id="refundAmountHistory">
        <td width="31%" height="30" align="center" bgcolor="#eeeeee">退款金额：</td>
        <td width="69%" height="30" align="left" bgcolor="#FFFFFF"> <%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %>&nbsp; <%=jbpmMgr.getTaskStringVaviable(taskId,"currencyCode") %>	</td>
      </tr>
      					  <%}%>
      <tr id="modifyType" style="display:none">
      		<td width="31%" height="34" align="center" bgcolor="#eeeeee"><span class="STYLE4">退款类型：</span></td>
      		<td width="69%" height="34" align="left" bgcolor="#FFFFFF"><span class="STYLE2">
      		  <select name="modifyRefundType"  onChange="checkType()">
      		    <%
      					if(refundType.equals("Full"))
      					{
      					    %>
      		    <option value="Full" selected="selected">全额退款</option>
      		    <option value="Partial">部分退款</option>
      		    <% 
      					}else
      					{
      					    %>	
      		    <option value="Full" >全额退款</option>
      		    <option value="Partial" selected="selected">部分退款</option>
      		        <% 
      					}
      				%>
   		      </select>
    		  </span> </td>
      </tr>
      	  <tr id="modifyAmount" style="display:none" >
      		<td width="31%" height="34" align="center" bgcolor="#eeeeee"><span class="STYLE4">退款金额：</span></td>
      		<td width="69%" height="34" align="left" bgcolor="#FFFFFF"> 
      			 <input name="modifyAmount" id="modifyAmount" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %>">&nbsp;  <%=jbpmMgr.getTaskStringVaviable(taskId,"currencyCode") %>		</td>
     	  </tr>
      <tr>
        <td width="31%" height="34" align="center" bgcolor="#eeeeee"> 订单号：</td>
        <td width="69%" height="34" align="left" bgcolor="#FFFFFF"><a  onClick="openWindow('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$",jbpmMgr.getTaskStringVaviable(taskId,"oid"))%>')" href="#" > <%=  jbpmMgr.getTaskStringVaviable(taskId,"oid")%></a> </td>
      </tr>
      <tr>
        <td width="31%" height="30" align="center" bgcolor="#eeeeee" >备注信息： </td>
        <td width="69%" height="30" align="left" bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"note") %></td>
      </tr>
      <tr>
        <td width="31%" height="42" align="center" bgcolor="#eeeeee" >驳回理由： </td>
        <td width="69%" height="42" align="left" bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"rejectNote") %></td>
      </tr>
    </table>
  </form></td>
</tr>
  	<tr>
  		<td width="20%" align="left"    valign="middle" class="win-bottom-line">&nbsp;       </td>
        <td width="80%" align="right" class="win-bottom-line">
        	<input type="button" value="修改" class="normal-green" name="modify" id="modify"   onClick="modify();" /> 
  			<input style="display:none" type="button" value="重新申请" class="normal-green" name="apply"  id="apply"   onClick="reStartProcess();" />  
  			<input type="button" value="关闭"   class="normal-white"  onClick="parent.closeWin();"/>		</td>
  	 </tr>		
  	</table>
  </body>
</html>

<script language="javaScript">
function modify()
{
	document.getElementById("apply").style.display="";	
	document.getElementById("modifyType").style.display="";
	document.getElementById("modify").style.display="none";
	var type ="<%=refundType%>";
	if(type!="Full")
	{
	document.getElementById("modifyAmount").style.display="";
	}
	document.getElementById("refundTypeHistory").style.display="none";
	document.getElementById("refundAmountHistory").style.display="none";
	
}
function openWindow(url)
{
	window.open(url);
}
function endProcess()
{
 		 var f = document.refundForm;
		 parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/endRefundProcessAction.action";	
		 parent.document.refundForm.taskId.value = f.taskId.value;
		 parent.document.refundForm.submit();
}
function reStartProcess()
{
		if(checkNull())
		{
			var f = document.refundForm;
	 		parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/reStartRefundProcessAction.action";	
			parent.document.refundForm.taskId.value = f.taskId.value;
			parent.document.refundForm.modifyAmount.value = f.modifyAmount.value;
			parent.document.refundForm.modifyRefundType.value = f.modifyRefundType.value;
			parent.document.refundForm.oid.value = f.oid.value;
			parent.document.refundForm.submit();	
		}
}
function checkType()
	{
		if(document.refundForm.modifyRefundType.value=="Partial")
		{
			 document.getElementById("modifyAmount").style.display="";
		} else 
		{
			 document.getElementById("modifyAmount").style.display="none";
		}
	}
	
function checkNull()
{
	var amount = document.refundForm.modifyAmount.value;
	var type = document.refundForm.modifyRefundType.value;
	if(amount==""&&type=="Partial")
	{
		alert("请填写退款金额！");
		return false;
	}else if(type=="Partial"&&!isNum(amount))
	{
		alert("退款金额请填写数字！");
		return false;
	}else
	{
		return true;
	}

}
function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

</script>
