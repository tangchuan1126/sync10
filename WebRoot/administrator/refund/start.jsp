<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean;"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="../js/select.js"></script>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
</head>
<html>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();"> 
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" > 
  <tr>
  <td  colspan="2"    style="border-bottom:1px #cccccc solid">
  		<table  width="100%" height="20" border="0" cellpadding="5" cellspacing="0" >
  			<tr>
  				<td colspan="2"  align="center" style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  ">   退款申请</td>
  			</tr>
  		</table>  </td>
  </tr>
  <tr><td colspan="2" style="border-bottom:1px #cccccc solid">
	  	<form name="refundForm"   method="post" action="">
	  	<table width="100%" border="0" cellspacing="9" cellpadding="2" align="center"  >
	  	  <tr>
	  	  	<td height="30" align="right"> 订单号： </td>
	  	  	<td> <a  onclick="window.open('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$", StringUtil.getString(request,"oid"))%>')" href="#" > <%=StringUtil.getString(request,"oid") %></a> </td>
	  	  </tr>
	  	  <tr>
	  	  	<td height="30" align="right">订单付款金额：</td>
	  	  	<td><%=StringUtil.getString(request,"mc_gross") %>&nbsp;<%=StringUtil.getString(request,"currencyCode") %></td>
	  	  </tr>
          <tr>
            <td  width="31%" align="right" height="30"  >退款类型：</td>
            <td  width="69%" align="left"  >
            	<select name="refundType"   style="WIDTH: 238px"  onChange="checkFull()" >
            		<option value="" >请选择...</option>
               	 	<option value="Full" >全额退款</option>
                	<option value="Partial">部分退款</option>
       		  </select>           
            </td>
          </tr>
          <tr style="display:none" id="amount_id">
            <td  width="31%"  height="30" align="right"  id="td1"  >退款金额：</td>
            <td  width="69%" align="left"  id="td2" ><input type="text" style="WIDTH: 80px"  name="amount"   /><%=StringUtil.getString(request,"currencyCode") %> </td>
          </tr>
          <tr>
	  	  	<td width="31%"  height="30" align="right">备注信息：</td>
            <td width="69%"   align="left"  >
            	<textarea rows="3" cols="35" id="note" name="note"></textarea> 
			</td>
  	      </tr>
        </table>
 	        <input id="userName" name="userName" type="hidden" value="<%=adminLoggerBean.getAdid()%>" > 
 	         <input id="userAccount" name="userAccount" type="hidden" value="<%=adminLoggerBean.getAccount() %>" > 
			<input name="oid" id="oid" type="hidden" value='<%=request.getParameter("oid") %>'> 
			<input type="hidden" readonly="true"   name="tranID" value="<%=request.getParameter("txn_id").toString() %>"  />
			<input type="hidden" name="totalAmount" value="<%=StringUtil.getString(request,"mc_gross") %>">
			<input type="hidden" name="currencyCode" value="<%=StringUtil.getString(request,"currencyCode") %>">
    	</form> 
	  	  </td>
	 </tr>
	 <tr>
		<td width="51%"   align="left" valign="middle" class="win-bottom-line"><img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999">请认真填写申请退款信息</span>         </td>
    	<td width="49%" align="right" class="win-bottom-line">
				<input type="button" value="申请"  class="normal-green" name="ok" onClick="startRefundPro();" />
				<input type="button" value="取消"  class="normal-white" name="cancel" onClick="parent.closeTBWin();" />	</td>	  		  
	 </tr>	
</table>
</body>
</html>

<script language="JavaScript1.2" >
 
	function startRefundPro()
	{
		var f = document.refundForm;
		if(checkNull())
		{	
		 	parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/startRefundProcessAction.action";	
			parent.document.refundForm.note.value = f.note.value;
			parent.document.refundForm.amount.value = f.amount.value;
			parent.document.refundForm.refundType.value = f.refundType.value;
			parent.document.refundForm.tranID.value = f.tranID.value;
			parent.document.refundForm.userName.value = f.userName.value;
			parent.document.refundForm.oid.value = f.oid.value; 
			parent.document.refundForm.userAccount.value=f.userAccount.value;
			parent.document.refundForm.totalAmount.value=f.totalAmount.value;
			parent.document.refundForm.currencyCode.value=f.currencyCode.value;
			parent.document.refundForm.submit();
		}
	}
 
	function checkNull()
	{
		var form = document.refundForm;
		var amount = form.amount.value;
		var note = form.note.value;
		var type = form.refundType.value;
	 	 if(type=="")
		 {
		 	alert("请选择退款类型");
		 	return false;
		 } else if(amount==""&&type=="Partial" )
		{
			alert("请填写退款金额！");
			return false;
		}else if(type=="Partial" &&!isNum(amount))
		{
			alert("退款金额请填写数字！");
			return false;
		}else if(note=="")
		{
			alert("请填写备注信息！");
			return false;
		}else 
		{
			return true;
		}
	}
	
	 
	function checkFull()
	{
		if(document.refundForm.refundType.value=="Partial")
		{
			 document.getElementById("amount_id").style.display="";
		} else
		{
			document.getElementById("amount_id").style.display="none";
		}
	}
	
	function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
</script>


