<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp" %>
<%@ page import="org.jbpm.api.task.*" %>
 <%@ page import="com.cwc.app.beans.AdminLoginBean"%>
 <%@ page import="java.util.List" %>
 <link href="../comm.css" rel="stylesheet" type="text/css">
 
<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 <%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<html>
  <style type="text/css">
<!--
.STYLE1 {color: #009966}
-->
  </style>
  <%
  		String taskId = request.getParameter("task_id");
  		long userName = adminLoggerBean.getAdid();
  %>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<table width="100%"    height="100%" border="0" align="center" cellpadding="3" cellspacing="0">
	 <tr>
	 	 <td  colspan="2"  height="20"  style="border-bottom:1px #cccccc solid">
	  		<table  width="100%"   border="0" cellpadding="5" cellspacing="0" >
	  			<tr>
	  				<td  align="center" style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  ">订单退款审批结果 </td>
	  			</tr>
	  		</table>	 	 </td>
 	 </tr>
  	 <tr>
   			<td colspan="2" align="center" style="font-size: 20px;font-weight: bold;">
	   			<%
	  		   		if(jbpmMgr.getTaskStringVaviable(taskId,"result").equals("同意"))
	  		   		{
	  		   		%>
	  		   			 <span class="STYLE1">退款成功</span> 
	   				<%
	  		   		}else
	  		   		{
	  		   		 %>
	  		   		 	 <span class="STYLE1">退款被否决</span>  
		   	    <%
	  		   		}
	  		    %>   			</td>
      </tr>
<tr><td  colspan="2" >
  	  <form name="refundForm"  width="100%" height="100%" method="post" action="">
  		<input id="userName" name="userName" type="hidden" value="<%=adminLoggerBean.getAccount()%>" > 
  		<input id="taskId" name="taskId" type="hidden" value="<%=taskId %>"> 
  		<input id="tranID" name="tranID" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"tranID") %>">
  	    <table  width="95%"  height="31%" border="0"    align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc" >
          <tr>
            <td width="32%" height="30" align="right" bgcolor="#eeeeee" >退款类型：</td>
            <td width="68%" height="30" align="left" bgcolor="#FFFFFF"  ><% String refundType = jbpmMgr.getTaskStringVaviable(taskId,"refundType");
		  			 		if(refundType.equals("Full"))
		  			 		{
		  			 			%>
					           		   全额退款
					            <%
		  			 		}else
		  			 		{
		  			 			%>
							                         部分退款
							   <%
		  			 		}%>            </td>
          </tr>
          <%  if(!refundType.equals("Full"))
		  			 		{
		  			 			%>
          <tr>
            <td width="32%" height="30" align="right" bgcolor="#eeeeee">退款金额：</td>
            <td width="68%" height="30" align="left" bgcolor="#FFFFFF"> <%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %>&nbsp; <%=jbpmMgr.getTaskStringVaviable(taskId,"currencyCode") %></td>
          </tr>
          <%
		  			 		}%>
          <tr>
            <td width="32%" height="30" align="right" bgcolor="#eeeeee"> 订单号： </td>
            <td width="68%" height="30" align="left" bgcolor="#FFFFFF"><a  onClick="openWindow('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$",jbpmMgr.getTaskStringVaviable(taskId,"oid"))%>')" href="#" > <%=  jbpmMgr.getTaskStringVaviable(taskId,"oid")%></a> </td>
          </tr>
          <tr>
            <td width="32%" height="30" align="right" bgcolor="#eeeeee" >备注信息：</td>
            <td width="68%" height="30" align="left" bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"note") %></td>
          </tr>
        </table>
  	  </form>
   </td>
</tr>
  	<tr> 
  		<td width="30%" align="left"  height="15%" valign="middle" class="win-bottom-line">&nbsp;  </td>
        <td width="70%" align="right"   class="win-bottom-line">
  			<input type="submit" class="normal-green" value="确认" name="result"   onClick="endProcess();"/> 
  			 <input type="button" value="关闭"   class="normal-white"  onClick="parent.closeWin();"/>		 </td>
  	</tr>
  	</table>
  </body>
</html>

<script language="javaScript">
function endProcess()
{
		 var f = document.refundForm;
		 parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/endRefundProcessAction.action";	
		 parent.document.refundForm.taskId.value = f.taskId.value;
		 parent.document.refundForm.submit();
}


</script>