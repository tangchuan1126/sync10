 <%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp" %>


<%
	if(StringUtil.getString(request,"cmd").equals("pre"))
	{
		String task_id = StringUtil.getString(request,"task_id");
		response.sendRedirect("../order/previewRefund.html?task_id="+task_id);
		return;
	}
%>
<%@ page import="org.jbpm.api.task.*" %>
 <%@ page import="com.cwc.app.beans.AdminLoginBean"%>
 <%@ page import="java.util.List" %>
 <link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>
<link rel="stylesheet" href="../dtree.css" type="text/css"></link>
<script type="text/javascript" src="../js/office_tree/dtree.js"></script>		
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 <%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<html>
  <style type="text/css">
<!--
.STYLE1 {color: #FF0000}
.STYLE4 {color: #000000}
-->
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<table width="100%" height="100%"    border="0" align="center" cellpadding="3" cellspacing="0">
	 <tr>
	 	 <td height="41"  colspan="2"    style="border-bottom:1px #cccccc solid">
	  		<table  width="98%" height="20"  border="0" cellpadding="5" cellspacing="0" >
	  			<tr>
	  				<td align ="center" style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  ">审批退款申请</td>
	  			</tr>
	  		</table>
 	   </td>
 	 </tr>
   <tr>
	<td colspan="2" >
 		<%
  		String taskId = request.getParameter("task_id");
  		%>
  	  <form name="refundForm"   method="post" action="">
  	  <input id="userName" name="userName" type="hidden" value="<%=adminLoggerBean.getAdid()%>" > 
  	  <input id="userAccount" name="userAccount" type="hidden" value="<%=adminLoggerBean.getAccount()  %>" > 
  		<input id="taskId" name="taskId" type="hidden" value="<%=taskId %>"> 
  		<input id="oid" name="oid"  type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"oid") %>">
  		<input id="tranID" name="tranID" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"tranID") %>">
  		<input name="refundType"  id="refundType" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"refundType") %>">
  		<input name="note"  id="note" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"note") %>">
  		<input name="amount" id="amount" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %>">
  		<input name="tranID" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"tranID") %>">
  		<table width="95%"  height="100%" border="0"    align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc" >
  			
  			 <tr>
  			 	<td width="23%" height="30" align="right" bgcolor="#eeeeee" >退款类型： </td>
  			 	<td width="77%" height="30" align="left" bgcolor="#FFFFFF"  >
  			   <% String refundType = jbpmMgr.getTaskStringVaviable(taskId,"refundType");
		  			 		if(refundType.equals("Full"))
		  			 		{
		  			 			%> 全额退款  <%
		  			 		}else
		  			 		{
		  			 			%>部分退款<%
		  			 		}%>	  		   </td>
  			 </tr> 
  			 <%  if(!refundType.equals("Full"))
		  			 		{
		  			 			%>  <tr>
			  			 				<td width="23%" height="30" align="right" bgcolor="#eeeeee">退款金额：</td>
			  			 			   	<td width="77%" height="30" align="left" bgcolor="#FFFFFF"> <%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %> &nbsp; <%=jbpmMgr.getTaskStringVaviable(taskId,"currencyCode") %></td>
		  			 			   	 </tr>
		  			 			<%
		  			 		}%>
  			 <tr>
			 		<td width="23%" height="30" align="right" bgcolor="#eeeeee">
	  		  		订单号： 		  		</td>
	  		  		<td width="77%" height="30" align="left" bgcolor="#FFFFFF">
	  		  			<a  onclick="openWindow('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$",jbpmMgr.getTaskStringVaviable(taskId,"oid"))%>')" href="#" >
                    <%=  jbpmMgr.getTaskStringVaviable(taskId,"oid")%></a>			  		</td>
		    </tr>
			<tr>
  			 	<td width="23%" height="30" align="right" bgcolor="#EEEEEE" >备注信息：  </td>
  				<td width="77%" height="30" align="left" bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"note") %></td>
 			</tr>
			<tr id="reject_td" style="display:none">
			 	 <td height="30" align="right" bgcolor="#EEEEEE"  >驳回理由：</td>
		 	  <td height="30"  align="left" bgcolor="#FFFFFF" >
				 <textarea rows="2" cols="30" name="rejectNote" id="rejectNote" onBlur="show()"  ></textarea>
				 <span  class="STYLE1" name="reasion" id="reasion">请填写驳回理由！</span>			 </td>			 
		    </tr>
		  	 			<%
			 				if(!jbpmMgr.getTaskStringVaviable(taskId,"rejectNote").equals(""))
			 				{
			 				%>
			 				<tr>
								<td width="23%" height="30" align="right" bgcolor="#EEEEEE"  >
			 						驳回历史信息：		 					</td>
			 					<td width="77%" height="30"  align="left" bgcolor="#FFFFFF" ><%= jbpmMgr.getTaskStringVaviable(taskId,"rejectNote")%>			 					</td>
			 			   </tr> 
			 				<% 
			 				}		
			 				 %>
			 <tr>
		  	  	<td width="31%"  height="30" align="right" bgcolor="#EEEEEE"><span class="STYLE4">退款申请人：</span></td>
	            <td width="69%"   align="left" bgcolor="#FFFFFF"  ><span class="STYLE4"><%=jbpmMgr.getTaskStringVaviable(taskId,"startUser") %>			</span></td>
	  	     </tr>
	    </table>
		 	 <input type="hidden" name="result" id="result"/> 
  	</form>
  	 </td>
  	</tr>
  	<tr> 
  		<td width="30%" align="left"   valign="middle" class="win-bottom-line">&nbsp;        </td>
        <td width="70%" align="right" class="win-bottom-line">
  			 <input type="button" value="同意"     class="normal-green"  onClick="executeRefund();" />
			 <input id="reject1" type="button" value="驳回"     class="normal-white"  onClick="examineProcessRegect();"/> 
			 <input style="display:none" id="reject2" type="button" value="驳回"     class="normal-white"  onClick="examineProcessRegect2();"/> 
			 <input type="button" value="否决"   class="normal-white"  onClick="examineProcessNo();"/>
			  <input type="button" value="关闭"   class="normal-white"  onClick="parent.closeWin();"/>
		</td>
  	</tr>
  	</table>
  </body>
</html>

<script language="javaScript">

function openWindow(url)
{
	window.open(url);
}
function show()
{
	if(document.refundForm.rejectNote.value!="")
	{
		 document.getElementById("reasion").style.disply="none";
	}
}
function executeRefund()
{

	var param = "amount="+$("#amount").val()+"&refundType="+$("#refundType").val()+"&note="+$("#note").val()+"&tranID="+$("#tranID").val();
	$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span  font-size:13px;font-weight:bold;color:#666666">正在退款，请稍后......</span>' });
					
	$.ajax({
	url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/executeRefundProcessAction.action',
	//url:dataUrl,
	type: 'post',
	dataType: 'json',
	timeout: 60000,
	cache:false,
	data:param,
	
	beforeSend:function(request){
	},
	
	error: function(data){
		$.unblockUI();
	 	alert("网络问题，请稍后重试...");
	 	parent.closeWin();
	},
	
	success: function(data){
			if(data["rs"]=="Success")
			{
				$.unblockUI();
				alert("退款成功！") ;
				$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="width:40px;  font-size:13px;font-weight:bold;color:#666666">页面正在加载，请稍后......</span>'});
				examineProcess();
 					
			}else
			{
				$.unblockUI();
				alert("退款失败！\n原因："+data["reasion"]+"\n请根据失败原因重新选择操作！");
			}
	} 
	});
}
function examineProcess()
{
		 var f = document.refundForm;
		 document.getElementById("result").value="同意";
		 parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/examineRefundProcessAction.action";	
		 parent.document.refundForm.result.value = f.result.value;
		 parent.document.refundForm.taskId.value = f.taskId.value;
		 parent.document.refundForm.tranID.value = f.tranID.value;
		 parent.document.refundForm.oid.value = f.oid.value;
		 parent.document.refundForm.submit();
	 
}
function examineProcessNo()
{
		 var f = document.refundForm;
		 document.getElementById("result").value="否决";	
		 parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/examineRefundProcessAction.action";	
		 parent.document.refundForm.result.value = f.result.value;
		 parent.document.refundForm.taskId.value = f.taskId.value;
		 parent.document.refundForm.tranID.value = f.tranID.value;
		 parent.document.refundForm.submit();
	 
}
 function examineProcessRegect()
{
		 document.getElementById("reject_td").style.display="";
		 document.getElementById("reject2").style.display="";
		 document.getElementById("reject1").style.display="none";
		
}
 function examineProcessRegect2()
{
		 document.getElementById("result").value="驳回";
		 if(checkNull())
		 {
		  	 execute();
	   	 }
}
function checkNull()
{
	var note=document.getElementById("rejectNote").value;
	if(note=="")
	{
	    alert("请填写驳回理由！");
		return false;
	}else
	{
		return true;
	}
}
function execute()
{
	var f = document.refundForm;
	parent.document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/examineRefundProcessAction.action";	
	parent.document.refundForm.rejectNote.value = f.rejectNote.value;
	parent.document.refundForm.userName.value = f.userName.value;
	parent.document.refundForm.userAccount.value = f.userAccount.value;
	parent.document.refundForm.result.value = f.result.value;
	parent.document.refundForm.taskId.value = f.taskId.value;
	parent.document.refundForm.tranID.value = f.tranID.value;
    parent.document.refundForm.submit();
}


</script>
