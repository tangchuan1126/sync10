 <%@ page contentType="text/html;charset=UTF-8"  %>
<%@ include file="../../include.jsp" %>
<%@ page import="org.jbpm.api.task.*" %>
 <%@ page import="com.cwc.app.beans.AdminLoginBean"%>
 <%@ page import="java.util.List" %>
  <link href="../comm.css" rel="stylesheet" type="text/css">
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
 <%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
 %>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style type="text/css">
<!--
.STYLE1 {color: #FFFFFF}
-->
  </style>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();">
<table width="100%"  height="100%"   border="0" align="center" cellpadding="3" cellspacing="0">
<tr>
	<td colspan="2"     height="25"  style="border-bottom:1px #cccccc solid">
		<table width="100%" height="25" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" style="color:#555555; font-family: Arial,宋体; font-weight: bold; font-size: 20px;  "> 退款申请信息 </td>
	  		</tr>
	  </table>	</td>
</tr>
<tr><td colspan="2" >
 	<%
  		String taskId = request.getParameter("task_id");//保存的是流程号  当前任务号是未知的
  		long userName = adminLoggerBean.getAdid();	 
  %>
  	    <form name="refundForm"   method="post" action="">
  	  <input id="userName" name="userName" type="hidden" value="<%=adminLoggerBean.getAdid()%>" > 
  		<input id="taskId" name="taskId" type="hidden" value="<%=taskId %>"> 
  		<input id="tranID" name="tranID" type="hidden" value="<%=jbpmMgr.getTaskStringVaviable(taskId,"tranID") %>">
  		<table width="95%"  height="100%" border="0"    align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc" >
  			
  			 <tr>
  			 	<td width="28%" height="30" align="right" bgcolor="#eeeeee" >退款类型： </td>
  			 	<td width="72%" height="30" align="left" bgcolor="#FFFFFF"  >
  			   <% String refundType = jbpmMgr.getTaskStringVaviable(taskId,"refundType");
		  			 		if(refundType.equals("Full"))
		  			 		{
		  			 			%> 全额退款  <%
		  			 		}else
		  			 		{
		  			 			%>部分退款		<%
		  			 		}%>	  		   </td>
  			 </tr> 
  			 <%  if(!refundType.equals("Full"))
		  			 		{
		  			 			%>  <tr>
			  			 				<td width="28%" height="30" align="right" bgcolor="#eeeeee">退款金额：</td>
			  			 			   	<td width="72%" height="30" align="left" bgcolor="#FFFFFF"> <%=jbpmMgr.getTaskStringVaviable(taskId,"amount") %>&nbsp;<%=jbpmMgr.getTaskStringVaviable(taskId,"currencyCode") %></td>
		  			 			   	 </tr>
		  			 			<%
		  			 		}%>
  			 <tr>
			 		<td width="28%" height="30" align="right" bgcolor="#eeeeee">
	  		  		订单号：	  		  		</td>
	  		  		<td width="72%" height="30" align="left" bgcolor="#FFFFFF">
	  		  			<a  onclick="openWindow('<%=StringUtil.replaceString("../order/ct_order_auto_reflush.html?p=0&handle=0&handle_status=0&product_type_int=0&product_status=0&cmd=search&search_field=3&val=$","$",jbpmMgr.getTaskStringVaviable(taskId,"oid"))%>')" href="#" >
                    <%=  jbpmMgr.getTaskStringVaviable(taskId,"oid")%></a>			  		</td>
		  	</tr>
			<tr>
  			 	<td width="28%" height="30" align="right" bgcolor="#eeeeee" >备注信息：  </td>
  				<td width="72%" height="30" align="left" bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"note") %></td>
 			</tr>
		
			<tr id="reject_td" style="display:none">
			 	 <td height="30" align="right" bgcolor="#eeeeee"  >驳回理由：</td>
			 	 <td height="30"  align="left" bgcolor="#FFFFFF" >
				 <textarea rows="2" cols="15" name="rejectNote" id="rejectNote" ></textarea></td>	  				 						  </td>
		    </tr>
		  	 			<%
			 				if(!jbpmMgr.getTaskStringVaviable(taskId,"rejectNote").equals(""))
			 				{
			 				%>
			 				<tr>
								<td width="28%" height="30" align="right" bgcolor="#eeeeee"  >
			 						驳回历史信息：			 					</td>
			 					<td width="72%" height="30"  align="left" bgcolor="#FFFFFF" ><%= jbpmMgr.getTaskStringVaviable(taskId,"rejectNote")%>			 					</td>
			 			   </tr> 
			 				<% 
			 				}		
			 				 %>
			  <tr>
		  	  	<td width="31%"  height="30" bgcolor="#eeeeee"align="right">退款申请人：</td>
	            <td width="69%"   align="left"  bgcolor="#FFFFFF"  ><%=jbpmMgr.getTaskStringVaviable(taskId,"startUser") %>			</td>
	  	     </tr>
	    </table>
  </form>
  	    <input type="hidden" name="result" id="result"/></td>
      </tr>
  	<tr> 
  		<td width="30%"  height="15%" align="left" valign="middle" class="win-bottom-line STYLE1">&nbsp;       </td>
        <td width="70%" align="right" class="win-bottom-line">
			 <input type="button" value="关闭"   class="normal-white"  onClick="parent.closeWin();"/>		 </td>
  	</tr>
  </table>
  </body>
</html>

<script language="javaScript">
function openWindow(url)
{
	window.open(url);
}
function examineProcess()
{
 		document.refundForm.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/order/examineRefundProcessAction.action";	
		document.refundForm.submit();	
		parent.closeWinRefresh();
}


</script>
