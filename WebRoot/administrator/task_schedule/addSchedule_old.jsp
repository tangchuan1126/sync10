<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" />
 
	<%
 		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 		 
 		    long deptId = adminLoggerBean.getAdgid(); 
 		   	DBRow[] rows =  adminMgr.getAdminByAdgid(deptId,null);
 	 		String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/AddSchedule.action";
 	   	 	DBRow[] userInfoDept = scheduleMgrZR.getAllUserInfo();
 	%>
<style type="text/css">
	body {font-size: 62.5%;}
 
	table.info{border:1px solid silver;margin:0 auto;border-collapse:collapse;}
	table.info tr{line-height:35px;height:35px;}
	table.info td{border:1px solid silver;}
	table.info td{width:250px;}
	table.info td.right{text-indent:5px;}
	table.info td.left{text-align:right;width:80px;}
	input.txt{width:200px;}
	.are{height:120px;}
	 div.mcdropdown{width:210px;top:-10px;}
	 div.mcdropdown input{width:210px;}	 
	 
</style>

<title>Insert title here</title>
<script type="text/javascript">
// 这里添加一个UserinfoDept的缓存
	var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>
	var startTime = "";
	var endTime = "";
	var tr ;
	var newa = [] ;
	jQuery(function($){
		initDatePick();
		tr = $("#timeSet");
	 	initDateIsSet();
	 	initUserInfoDept();
	})
	function initUserInfoDept(){
		//创建一个<ul><li></li></ul>的集合
		
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
					flag = false;
					break;
				}
			}
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].name;
				objs.deptId = userInfoDept[index].adgid;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		}
		 
		/*
			 <select data-placeholder="选择一个地址" style="width:550px;" multiple class="chzn-select"  tabindex="8">
			 <option value=""></option>
			  <optgroup label="NFC EAST">
				<option>Dallas Cowboys</option>
				<option>New York Giants</option>
				<option>Philadelphia Eagles</option>
				<option>Washington Redskins</option>
			  </optgroup>
			  <optgroup label="NFC NORTH">
				<option>Chicago Bears</option>
				<option>Detroit Lions</option>
				<option>Green Bay Packers</option>
				<option>Minnesota Vikings</option>
			  </optgroup>
        	</select>
		*/
		 var selected = "<option value=''></option>";
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
			for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
				var option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
				optgroup += option;
			}
			optgroup +="</optgroup>";
			selected += optgroup;
		 }
		 var $selected = $(selected);
		 $selected.find("option[value='"+"<%=adminLoggerBean.getAdid()%>"+"']").attr("selected",true);
		 $("#execute_user_id").append($selected).chosen({no_results_text: "没有该用户:"});
	}
	function showUserInfo(deptId){
		var where = 0;
		for(var index = 0  , count = newa.length ; index < count ; index++ ){
			if(deptId == newa[index].deptId * 1){
				where = index;
				break;	
			}
		}
		if(!newa[where].str){
			var v = "<select data-placeholder='选择人' style='width:200px;'  class='chzn-select' id='userInfo'><option value=''></option>";
			var values = newa[where].values;
			for(var index = 0 , count = values.length ; index < count ; index++ ){
				v += "<option value='"+values[index].adid+"'>"+values[index].employe_name+"</option>";
			}
			v += "</select>";
			newa[where].str = v;
		} 
		 
		$("#userinfoDiv").html("");
		$("#userinfoDiv").append(newa[where].str );
		$("#deptNoDiv").animate({marginLeft: '-205px'}, "slow");
		$("#userInfo").append(newa[where].str).chosen({no_results_text: "没有该部门:"});

	}
	//得到所有的dept;
	 
	//挺奇怪的
	function initDateIsSet(){
		$("#is_schedule").click(function(){
			var _this = $(this);
			if(_this.attr("checked")){
				$("#startTime , #endTime").attr("disabled",false);
				_this.val("1");
			}else{
				 $("#startTime , #endTime").attr("disabled",true);
				 _this.val("0");
			}
		})
	}
	function initDatePick(){
		$("#startTime , #endTime").datetimepicker({
				showSecond: false,
				showMinute: false,
				changeMonth: true,
				changeYear: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'yy-mm-dd'
		}).val(getCurrentDay());
		//初始化开始时间和结束时间
	}
	function getCurrentDay(){
		 
		  var s = "";
		  var d = new Date();  
		  var c = ":";
		   s += d.getFullYear()+"-";
		   s += fixNumber(d.getMonth() + 1) + "-";
		   s += fixNumber(d.getDate()) + " ";   
		   s += fixNumber(d.getHours()) + c + "00:00";
		return s;
	}
	function beforeFormSubmit(){
		
	}
	
function fixNumber(num){
	return num < 10 ? "0"+num : num;
}
function ajaxAddSchedule(){
 	var flag = checkForm_r();
	//ajaxSubmit 
 	
	var param =  $("#add_schedule_form").serialize();
	if(!$("#is_schedule").attr("checked")){
		param += "&is_schedule=0"; 
	}
	var execute_user_id = $("#execute_user_id").val();
	param +="&execute_user_id="+execute_user_id;
	 
 	var index =  execute_user_id.indexOf($.trim($("#assign_user_id").val()));
  
	if(flag){
		$.ajax({
			url:'<%= action%>',
			dataType:'json',
			data:param,
			success:function(data){
				if(data && data.flag === "success"){
					showMessage("添加成功");
					//如果是立即添加的话就把任务立即添加到那个人在某个时候做什么请示
					//如果不是的话，就是把。这个任务添加到没有安排的任务中。这个时候的时间是没有任何的效果的。
					//如果是当前安排人当中有 系统登录人那么就是要刷新的。这里的assign_user_id其实就是当前的登录人
					
					cloesWin();
					if(index != -1){
						if($("#is_schedule").attr("checked")){
							parent.addEventNotRefresh && parent.addEventNotRefresh(createObject(data.entityid));		
						}else{
							window.parent.location.reload();
						}
					}
				}else{
					showMessage("添加失败","error");
				};
			}
		})
		
	}
}


function createObject(_id){
	var obj = {
		id:_id,
		title:$("#schedule_overview").val(),
		start:parseISO8601($.trim($("#startTime").val())),
		end:parseISO8601($.trim($("#endTime").val())),
		allDay:$("input[name='is_all_day']:checked").val() ==="1"?true:false,
		schedule_is_note:$("input[name='schedule_is_note']:checked").val(),
		is_need_replay:$("input[name='is_need_replay']:checked").val(),
		schedule_state:0,
		editable:$("input[name='is_update']:checked").val() ==="1"?true:false
		
	};
	return obj;
}


function parseISO8601(s, ignoreTimezone) {
	var m = s.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
	if (!m) {
		return null;
	}
	var date = new Date(m[1], 0, 1);
	if (ignoreTimezone || !m[13]) {
		var check = new Date(m[1], 0, 1, 9, 0);
		if (m[3]) {
			date.setMonth(m[3] - 1);
			check.setMonth(m[3] - 1);
		}
		if (m[5]) {
			date.setDate(m[5]);
			check.setDate(m[5]);
		}
		fixDate(date, check);
		if (m[7]) {
			date.setHours(m[7]);
		}
		if (m[8]) {
			date.setMinutes(m[8]);
		}
		if (m[10]) {
			date.setSeconds(m[10]);
		}
		if (m[12]) {
			date.setMilliseconds(Number("0." + m[12]) * 1000);
		}
		fixDate(date, check);
	}else{
		date.setUTCFullYear(
			m[1],
			m[3] ? m[3] - 1 : 0,
			m[5] || 1
		);
		date.setUTCHours(
			m[7] || 0,
			m[8] || 0,
			m[10] || 0,
			m[12] ? Number("0." + m[12]) * 1000 : 0
		);
		if (m[14]) {
			var offset = Number(m[16]) * 60 + (m[18] ? Number(m[18]) : 0);
			offset *= m[15] == '-' ? 1 : -1;
			date = new Date(+date + (offset * 60 * 1000));
		}
	}
	return date;
}
function fixDate(d, check) { // force d to be on check's YMD, for daylight savings purposes
	if (+d) { // prevent infinite looping on invalid dates
		while (d.getDate() != check.getDate()) {
			d.setTime(+d + (d < check ? 1 : -1) * HOUR_MS);
		}
	}
}

//通过form对象中的数据创建一个新的
 
function checkForm_r(){
	 
		if(isNull("execute_user_id")){
			showMessage("请选择执行人 ","alert");
			return false;
		}
		//alert($("#is_schedule").attr("checked"));
		if($("#is_schedule").attr("checked")){
			if(isNull("startTime")){
				showMessage("请填写开始时间","alert");
				return false;
			}
			if(isNull("endTime")){
				showMessage("请填写结束时间","alert");
				return false;
			}
			if(timeCompare("startTime","endTime")){
				showMessage("开始时间不能大于结束时间","alert");
				return false;
			}
		 }
		if(isNull("schedule_overview")){
			showMessage("请填写任务概述","alert");
			return false;
		}
		if(isNull("schedule_detail")){
			showMessage("请填写任务描述","alert");
			return false;
		}
		return true;
}
// 如果前面的大于后面的就是返回 true 否则就是返回false
function timeCompare(before,after){
	before = ($("#"+before).val().replace(/-/gi,"/"));
	after = ($("#"+after).val().replace(/-/gi,"/"));
	before = new Date(before).getTime();
	after = new Date(after).getTime();
	return before > after?true:false;
}
 
function isNull(_id){
	var dom =  $("#"+_id);
	if(dom.length > 0 &&  $.trim(dom.val()).length > 0 ){
		return false;
	}else{
		return true;
	}
}
function cloesWin(){
	parent.closeTBWin && parent.closeTBWin();
}
 
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox = $("<div style='font-size:12px;'/>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
}
}
</script>
</head>
<body>
	 <form action="" id="add_schedule_form" name="add_schedule_form" action="<%=action %>">
	  
	 	<table class="info">
	 
	 			<tr>
	 				<td class="left">任务安排人:</td><td class="right"><input type="hidden" id="assign_user_id" name="assign_user_id" value="<%=adminLoggerBean.getAdid() %>"/><%=adminLoggerBean.getAccount() %></td>
	 				<td class="left">任务执行人:</td><td class="right">
	 					<select data-placeholder='选择任务执行人' style='width:300px;' multiple class='chzn-select' id="execute_user_id">
	 					</select>
	 				</td>
	 			</tr>
	 			 <tr id="set">
	 				<td class="left">指定时间:</td>
	 				<td class="right"><input type="checkbox" name="is_schedule" id="is_schedule" value="0"/></td>
	 				<td class="left"></td>
	 				<td class="right"></td>
	 			</tr>
	 			<tr id="timeSet" style="">
	 			    <td class="left">任务开始时间:</td><td class="right"><input type="text" class="txt" id="startTime" value="" name="startTime" disabled="disabled"/></td>
	 			    <td class="left">任务结束时间:</td><td class="right"><input type="text" class="txt" id="endTime" name="endTime" value="" disabled = "disabled"/></td>
	 			</tr>
	 			
	 			<tr>
	 				<td class="left">提醒:</td><td class="right">
	 					<input type="radio" name="schedule_is_note" value = "1" />是
						<input type="radio" name="schedule_is_note" value = "0" checked="checked"/>否
	 					<!-- <input type="checkbox" id="schedule_is_note" name="schedule_is_note" value="0"/> -->
	 				</td>
	 				<td class="left">回复:</td><td class="right">
	 					<input type="radio" name="is_need_replay" value = "1" />是
						<input type="radio" name="is_need_replay" value = "0" checked="checked"/>否
	 					<!--  <input type="checkbox" id="is_need_replay" name="is_need_replay" value="0"/>-->
	 				</td>
	 			</tr>
	 			<tr>
	 				<td class="left">全天任务:</td><td class="right">
	 					<input type="radio" name="is_all_day" value = "1" />是
						<input type="radio" name="is_all_day" value = "0" checked="checked"/>否
	 					<!-- <input type="checkbox" id="is_all_day" name="is_all_day" value="0"/> -->
	 				</td>
	 				<td class="left">自由调整:</td>
	 				<td class="right">
	 					<input type="radio" name="is_update" value = "1" />是
						<input type="radio" name="is_update" value = "0" checked="checked"/>否
	 					<!--  <input type="checkbox" id="is_update" name="is_update" value="0"/>-->
	 				</td>
	 			</tr>
	 			<tr>
	 				<td class="left">任务概述:</td>
	 				<td colspan="3" class="right" >
	 					<input type="text" style="width:500px;height:25px;" name="schedule_overview" id="schedule_overview" /> <span style="color:silver;">可以填写50字</span>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td class="left">任务描述:</td>
	 				<td colspan="3" class="are right" style="">
	 	 
	 					<textarea style="width:500px;height:100px;" name="schedule_detail" id="schedule_detail"></textarea>
	 			 
	 				 		
	 				 	 
	 			   
	 				</td>
	 			</tr>
	 	 	 <tr>
	 	 	 
	 			<td colspan="4" style="text-align:center;" style="line-height:40px;height:40px;">
	 				<input class="normal-green" type="button"   value="保存" name="Submit2" style="margin-top:5px;" onclick="ajaxAddSchedule();" />
					<input class="normal-white" type="button"   value="取消" name="Submit2" style="margintop:5px;" onclick="cloesWin()"/>
				</td>
	 		</tr>
	 	</table>
	 </form>
</body>
</html>