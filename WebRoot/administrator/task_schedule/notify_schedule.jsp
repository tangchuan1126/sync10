<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../../include.jsp"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>未完成任务</title>
<!-- 基本css 和javascript -->
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/thickbox.css";
</style>
<!-- 引入Art -->

<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 

	long adid = adminLoggerBean.getAdid(); 
	DBRow[] rows = scheduleMgrZR.getScheduleNotComplete(adid,5);
	TDate tdate = new TDate();
%>
<style type="text/css">
	ul.schedule{list-style-type:none;color:black;}
	ul.schedule li {padding-bottom:5px;padding-top:5px;;border:1px solid silver;;margin-top:2px;text-indent:10px;cursor:pointer; }
	.cor_li{border-bottom-right-radius:4px;border-bottom-left-radius: 4px;border-top-right-radius: 4px; border-top-left-radius: 4px;}
	 .colorGreen{color:green;}
	 .colorRed{color:#f60;}
</style>
<script type="text/javascript">
	function openSchedule(schedule_id,is_schedule){
		 
		var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/schedule_management/show_schedule.html";
		url += "?schedule_id="+schedule_id+"&is_schedule="+is_schedule+"&from=out"
		window.top.mainFrameOpenUrl(url);
	}
</script>
</head>
<body style=" ">
<%
	if(rows != null && rows.length > 0 ){
%>	
	<h1 style="text-align:center;font-size:12px;">未完成任务</h1>
	 <ul class="schedule">
	 <%	
	 for(DBRow rowTemp : rows){ 
	 	DBRow userDBRow = adminMgr.getDetailAdmin(rowTemp.get("assign_user_id",0l));
	 	String userName = userDBRow != null ? "["+userDBRow.getString("employe_name")+"]":"";
	 	String time = rowTemp.getString("start_time");
	 	String fixTime = time.trim().length() > 1 ? "["+tdate.getFormateTime(time)+"]" :"" ;
	  	
	 %>
	 	<li class="cor_li" onclick="openSchedule('<%=rowTemp.get("schedule_id",0l) %>','<%=rowTemp.get("is_schedule",0) %>')"><span class="colorGreen"><%=userName %></span><%=rowTemp.getString("schedule_overview")%><span class="colorRed"><%=fixTime%></span></li>
	 <%} %>	 
	 </ul>
 
<%
}else{
%>
	<div style="border:0px solid silver ;margin:0px auto;margin-top:40px;width:125px;">
		<img src="../imgs/icon_confirmation.gif" width="36" height="38" align="absmiddle">
   		<span style="color: #66AA00;font-weight: bold;font-size: 12px;font-family:'黑体';">任务全部完成</span>
    </div>
<%} %>
</body>
</html>