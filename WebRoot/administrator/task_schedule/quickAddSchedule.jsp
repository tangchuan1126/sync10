<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	
 <link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css">
	table.add_schedule_table{width:98%;border-collapse:collapse;margin:0px auto;}
	table.add_schedule_table tr{line-height:25px;height:25px;}
	table.add_schedule_table tr td{border:1px solid silver;}
	table.add_schedule_table tr td.left{text-align:right;}
 	table.add_schedule_table tr td.right{text-indent:3px;}
</style>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<title>Insert title here</title>

<%
	DBRow assignUserInfo  = adminMgr.getDetailAdmin(Long.parseLong(StringUtil.getString(request,"assign_user_id")));
	DBRow executeUserInfo = adminMgr.getDetailAdmin(Long.parseLong(StringUtil.getString(request,"excute_user_id")));
	String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/AddSchedule.action";
	DBRow[] userInfoDept = scheduleMgrZR.getAllUserInfo();	 
%>
<script type="text/javascript">
	var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>
	var is_all_day =  '<%= StringUtil.getString(request,"allDay")%>';
	var newa = [];
	jQuery(function($){
		initForm();
		initDept();
	})
	function initDept(){
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
					flag = false;
					break;
				}
			}
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].name;
				objs.deptId = userInfoDept[index].adgid;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		} 
		 var selected = "";
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
			for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
				var option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
				optgroup += option;
			}
			optgroup +="</optgroup>";
			selected += optgroup;
		 } 
		 var $selected = $(selected);
		 $selected.find("option[value='"+"<%=executeUserInfo.getString("adid") %>"+"']").attr("disabled",true);
	 	 var join_user_idSelected = $("#join_user_id").append($selected);
	 



		 //任务参与人
		join_user_idSelected.multiselect({
			 checkAllText:'全选',
			 uncheckAllText:'取消',
			 noneSelectedText:'选择任务参与人',
			 selectedText: '# 个人已选择',
			 selectedList: 200,
			 minWidth:400
		 }).multiselectfilter({
			 label:'搜索',
			 placeholder:'输入姓名'
		 });
		
	}
	function initForm(){
		var isAllDayText = $("#isAllDayText");
		
		if(is_all_day === "true"){
			isAllDayText.html("是");
			$("#is_all_day").val("1");
		}else{
			$("#is_all_day").val("0");
			isAllDayText.html("否");
		}
	}

	function ajaxAddSchedule(){
		if($("#schedule_overview").val().length < 1){showMessage("任务标题不能为空","alert");return ;}
	  	if($("#schedule_detail").val().length < 1){showMessage("任务描述不能为空","alert");return ;}
		var param =  $("#add_schedule_form").serialize();
		
		var schedule_is_note = $("#schedule_is_note").attr("checked")?"1":"0";
		var is_need_replay = $("#is_need_replay").attr("checked")?"1":"0";
		var sms_short_notify = $("#sms_short_notify").attr("checked")?"1":"0";
		var sms_email_notify = $("#sms_email_notify").attr("checked")?"1":"0";
		var is_update = $("#is_update").attr("checked")?"1":"0";
		param += "&schedule_is_note="+schedule_is_note;
		param += "&is_need_replay="+is_need_replay;
		param += "&is_update="+is_update;
		param += "&sms_short_notify="+sms_short_notify;
		param += "&sms_email_notify="+sms_email_notify;
		var schedule_join_execute_id = $("#join_user_id").val();
		var fix_join_id = schedule_join_execute_id?schedule_join_execute_id : "";
		param += "&schedule_join_execute_id="+fix_join_id;
		 
			$.ajax({
				url:'<%= action%>',
				dataType:'json',
				data:param,
				success:function(data){
					if(data && data.flag === "success"){
						$.artDialog && $.artDialog.close();
						$.artDialog.opener.addEventNotRefresh  && $.artDialog.opener.addEventNotRefresh(createObject(data.entityid,schedule_is_note,is_need_replay,is_update));	
						
					//	parent.closeTBWin && parent.closeTBWin();
					//	parent.addEventNotRefresh && parent.addEventNotRefresh(createObject(data.entityid,schedule_is_note,is_need_replay,is_update));
					}else{
						showMessage("添加失败","error");
					};
				}
			})
	}
	function cloesWin(){
		
	}
	function createObject(_id,_schedule_is_note,_is_need_replay,_is_update){
		var obj = {
			id:_id,
			title:$("#schedule_overview").val(),
			start:parseISO8601($.trim($("#startTime").val())),
			end:parseISO8601($.trim($("#endTime").val())),
			allDay:is_all_day === "true"?true:false,
			schedule_is_note:_schedule_is_note,
			is_need_replay:_is_need_replay,
			schedule_state:0,
			schedule_execute_id:$("#execute_user_id").val(),
			assign_user_id:$("#assign_user_id").val(),
			is_update:_is_update,
			editable:_is_update ==="1"?true:false
			
		};
		return obj;
	}
	function parseISO8601(s, ignoreTimezone) {
		var m = s.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
		if (!m) {
			return null;
		}
		var date = new Date(m[1], 0, 1);
		if (ignoreTimezone || !m[13]) {
			var check = new Date(m[1], 0, 1, 9, 0);
			if (m[3]) {
				date.setMonth(m[3] - 1);
				check.setMonth(m[3] - 1);
			}
			if (m[5]) {
				date.setDate(m[5]);
				check.setDate(m[5]);
			}
			fixDate(date, check);
			if (m[7]) {
				date.setHours(m[7]);
			}
			if (m[8]) {
				date.setMinutes(m[8]);
			}
			if (m[10]) {
				date.setSeconds(m[10]);
			}
			if (m[12]) {
				date.setMilliseconds(Number("0." + m[12]) * 1000);
			}
			fixDate(date, check);
		}else{
			date.setUTCFullYear(
				m[1],
				m[3] ? m[3] - 1 : 0,
				m[5] || 1
			);
			date.setUTCHours(
				m[7] || 0,
				m[8] || 0,
				m[10] || 0,
				m[12] ? Number("0." + m[12]) * 1000 : 0
			);
			if (m[14]) {
				var offset = Number(m[16]) * 60 + (m[18] ? Number(m[18]) : 0);
				offset *= m[15] == '-' ? 1 : -1;
				date = new Date(+date + (offset * 60 * 1000));
			}
		}
		return date;
	}
	function fixDate(d, check) { // force d to be on check's YMD, for daylight savings purposes
		if (+d) { // prevent infinite looping on invalid dates
			while (d.getDate() != check.getDate()) {
				d.setTime(+d + (d < check ? 1 : -1) * HOUR_MS);
			}
		}
	}
	function showMessage(_content,_state){
		var o =  {
				state:_state || "succeed" ,
				content:_content,
				corner: true
			 };
			 var  _self = $("body"),
			_stateBox = $("<div />").addClass("ui-stateBox").html(o.content);
			_self.append(_stateBox);	
			if(o.corner){
				_stateBox.addClass("ui-corner-all");
			}
			if(o.state === "succeed"){
				_stateBox.addClass("ui-stateBox-succeed");
				setTimeout(removeBox,1500);
			}else if(o.state === "alert"){
				_stateBox.addClass("ui-stateBox-alert");
				setTimeout(removeBox,2000);
			}else if(o.state === "error"){
				_stateBox.addClass("ui-stateBox-error");
				setTimeout(removeBox,2800);
			}
			_stateBox.fadeIn("fast");
			function removeBox(){
				_stateBox.fadeOut("fast").remove();
		}
	}
</script>
</head>
<body>
	 <form action="" id="add_schedule_form" name="add_schedule_form">
	 <!-- 安排是 -->
	 	<input type="hidden"  id="is_all_day" name="is_all_day" />
	 	<input type="hidden"   value="1" name="is_schedule" />
	 	<table class="add_schedule_table">
			 		<tr>
			 		    <td class="left">任务安排人:</td><td class="right"><input type="hidden" id="assign_user_id" name="assign_user_id" value='<%=assignUserInfo.getString("adid") %>'/><%=assignUserInfo.getString("employe_name") %></td>
			 			<td class="left">任务执行人:</td><td class="right"><input type="hidden" name="execute_user_id" id="execute_user_id" value='<%=executeUserInfo.getString("adid") %>'/><%=executeUserInfo.getString("employe_name") %></td>
			 		</tr>
			 		<tr>
			 			<td class="left">任务开始时间:</td><td class="right"><input type="hidden" class="txt" id="startTime"  name="startTime" value='<%=StringUtil.getString(request,"start") %>'/><%=StringUtil.getString(request,"start") %></td>
			 			<td class="left">任务结束时间:</td><td class="right"><input type="hidden" class="txt" id="endTime" name="endTime" value='<%=StringUtil.getString(request,"end") %>'/><%=StringUtil.getString(request,"end") %></td>
			 		 </tr>
	 				<tr>
		 				<td class="left"></td>
		 				<td class="right" colspan="3">
		 	 
							回复:<input type="checkbox" id="is_need_replay"  />
							自由调整:<input type="checkbox" id="is_update" />
		 				</td> 
	 				</tr>
	 				<tr>
	 					<td class="left">任务提醒:</td>
	 					<td class="right" colspan="3">
	 						页面提醒:<input type="checkbox" id="schedule_is_note" />			<!-- 页面提醒 -->
							短信提醒:<input type="checkbox" id="sms_short_notify"  />
							邮件提醒:<input type="checkbox" id="sms_email_notify" checked = "true"/>
	 					</td>
	 				</tr>
		 			<tr>
		 				<td class="left">全天任务:</td>
		 				<td class="right" id="isAllDayText" colspan="3">
		 			</td>
		 			</tr>
		 			<tr>
		 				<td class="left">任务参与人:</td>
		 				<td class="right" colspan="3">
		 					<select style='width:300px;' multiple="multiple"  id="join_user_id"> </select>
		 				</td>
		 			</tr>
	 				<tr>
		 				<td class="left">任务标题:</td>
		 				<td colspan="3" class="right" >
		 					<input type="text" style="width:350px;height:25px;" name="schedule_overview" id="schedule_overview" /> <span style="color:silver;">(可以填写50字)</span>
		 				</td>
		 			</tr>
		 			<tr>
		 				<td class="left">任务描述:</td>
		 				<td colspan="3" class="are right" >
		 					<textarea style="width:400px;height:130px;" name="schedule_detail" id="schedule_detail"></textarea>
		 				</td>
		 			</tr>
			 	 	 <tr>
			 			<td colspan="4" style="text-align:center;" style="line-height:40px;height:40px;">
			 				<input class="normal-green" type="button"   value="保存" name="Submit2" style="margin-top:5px;" onclick="ajaxAddSchedule();" />
							<input class="normal-white" type="button"   value="取消" name="Submit2" style="margintop:5px;" onclick="$.artDialog.close()"/>
						</td>
			 		</tr>
	 		 
	 	</table>
	 </form>
</body>
</html>