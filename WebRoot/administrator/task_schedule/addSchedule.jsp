<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>

<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 
	<%
 		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 		 
 		    long deptId = adminLoggerBean.getAdgid(); 
 		   	DBRow[] rows =  adminMgr.getAdminByAdgid(deptId,null);
 	 		String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/AddSchedule.action";
 	   	 	DBRow[] userInfoDept = scheduleMgrZR.getAllUserInfo();
 	%>
<style type="text/css">
	.ui-datepicker{font-size: 0.9em;}
	.ui-multiselect-all a span{color:red;font-size:10px;}
 
	table.info{border:1px solid silver;margin:0 auto;border-collapse:collapse;}
	table.info tr{line-height:35px;height:35px;}
	table.info td{border:1px solid silver;}
	table.info td{width:250px;}
	table.info td.right{text-indent:5px;}
	table.info td.left{text-align:right;width:80px;}
	input.txt{width:140px;}
	.are{height:120px;}
	 
	 /*repeat css */
    .repeatDiv{margin:0 auto;width:400px;border:1px solid silver;position:absolute;left:0px;top:0px;display:none;background:white;top:40px;z-index:99;}
	.repeatDiv span.closeFlag{cursor:pointer;float:right;margin-right:5px;}
	.repeatDiv h1{line-height:25px;height:25px;}
	table.repeatTable td{height:25px;}
	table.repeatTable td.left{width:100px;text-align:right;} 
	table.repeatTable td.right{padding-left:20px;}
	span.typeText{display:block;margin-top:3px;margin-right:3px;}
	div.offbackground{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjgzJSIgc3RvcC1jb2xvcj0iI2RjZGNkYyIgc3RvcC1vcGFjaXR5PSIwLjIzIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijg1JSIgc3RvcC1jb2xvcj0iI2RiZGJkYiIgc3RvcC1vcGFjaXR5PSIwLjIzIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(220,220,220,0.23) 83%, rgba(219,219,219,0.23) 85%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(83%,rgba(220,220,220,0.23)), color-stop(85%,rgba(219,219,219,0.23)));
	background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#3bdbdbdb',GradientType=0 );
	}
</style>

<title>Insert title here</title>
<script type="text/javascript">
// 这里添加一个UserinfoDept的缓存
	var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>
	var startTime = "";
	var endTime = "";
	var tr ;
	var newa = [] ;
	var repeatFlag = false; 
	var cornExpress ;
	jQuery(function($){
		initDatePick();
		tr = $("#timeSet");
	 	initDateIsSet();
	 	initUserInfoDept();
	 	$("#ui-datepicker-div").css("display","none");
	 	$(".repeatDiv").live("click",function(){
		 	repeatInfo();
		});
	 	
	})
	//首先是判断是不是初始化了repeat。如果没有的话就先初始化一次。
	function repeatSchedule(){
		var rpeat = $("#repeat");
		if(!repeatFlag){
			initScheduleRepeat();
		}
	 	var left = (($(window).width()- 400) / 2);
	 
		if(rpeat.attr("checked")){
		 	$(".repeatDiv").css("display","block").css("left",left);
		 	$(".offbackground").css("display","block");
		}else{
			$(".repeatDiv").css("display","none");
			cornExpress = null;
			$("#aRepeat").css("display","none");
			$("#showInfoStr").html("");
		}
	}
	function openRepeat(){
		$(".repeatDiv").css("display","block");
	 	$(".offbackground").css("display","block");
	}
	//初始化重复任务
	function initScheduleRepeat(){
		repeatFlag = true;
		 initDaySelect();
		 initMonthSelect();
		 initWeekSelect();
		 initCheckBoxSelect();

		 // 初始化repeatStartTime时间
		 $("#repeatStartTime").val(getCurrentDay().split(/\s+/)[0]);
		 var radios = $(":radio",$("#repeatTD"));
		 $(":text",$("#repeatTD")).attr("disabled",true);
		 radios.live("click",function(){
				var _this = $(this);
				var target = (_this.attr("target"));
				 $(":text",$("#repeatTD")).attr("disabled",true);
				if(target){
					$("#"+target).attr("disabled",false);
				}
		 })
		 //
		 var date = new Date();
		 var endDate = new Date(date.getFullYear(),date.getMonth() + 1 ,date.getDate() + 1 );
		 var endDateString = getCurrentDay(endDate).split(/\s+/)[0];
		 $("#repeatEndTime").datepicker({
				dateFormat:"yy-mm-dd",
				changeMonth: true,
				changeYear: true
			}).val(endDateString);
	}
	function cencelRepeat(){
		$(".repeatDiv").css("display","none");
	 	$(".offbackground").css("display","none");
	 	if(!cornExpress){
		 	$("#repeat").attr("checked",false);
		 	$("#aRepeat").css("display","none");
		 }else{
			 $("#aRepeat").css("display","block");
		}
	}
	//repeat 提示信息
	function repeatInfo(){
		// 读取月份信息
		var infoStr = "";
		var allMonthSelected = $("#allMonthSelected");
		if(allMonthSelected.attr("checked")){
			infoStr += "每月 ";
		}else{
			var array =  $("#monthSelect").val();
			if(array && array.length > 0){
			 infoStr += array + "月 ";
			}
		}
		// 读取周和天的信息(如果是周和天都选择的话那么就用周的来计算结果)
		var allWeekSelected =  $("#allWeekSelected");
	 
		var allDaySelected = $("#allDaySelected");
		if(allWeekSelected.attr("checked")){
			infoStr += " 每周";
		}else{
			// 然后在计算天的东西
			var weekArray = $("#weekSelect").val();
			
			if(weekArray && weekArray.length > 0){
				var weekStr = "" ;
				var weekValue = [',周末',',星期一',',星期二',',星期三',',星期四',',星期五',',星期六'];
				for(var index = 0 , count = weekArray.length ; index < count ; index++){
					 weekStr += weekValue[(weekArray[index] * 1 -1)]; 
				} 
				infoStr += weekStr.substr(1);
			}else{
				//天
				 if(allDaySelected.attr("checked")){
					 infoStr += "每天"; 
				}else{
					var daySelect = $("#daySelect").val();
					if(daySelect && daySelect.length > 0){
						infoStr += daySelect + "号"; 
					}
				}
			}
		}
		if(infoStr.length > 1){
			infoStr += " 执行";
		}
		// 读取结束时间
		var radio = $(":checked",$("#repeatTD"));
		var target = radio.attr("target");
		if(target){
			if(target ==="countTime" ){
				infoStr += "  发生"+$("#countTime").val()+"次后结束";
			}
			if(target === "repeatEndTime"){
				infoStr += "  在"+$("#repeatEndTime").val()+"之前结束";
			}
		};
		
		$("#repeatInfo").html(infoStr);
	}
	function initUserInfoDept(){
		//创建一个<ul><li></li></ul>的集合
		
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
					flag = false;
					break;
				}
			}
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].name;
				objs.deptId = userInfoDept[index].adgid;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		}
		 
	 
		 var selected = "";
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
			for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
				var option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
				optgroup += option;
			}
		 
			optgroup +="</optgroup>";
			selected += optgroup;
		 }
		 var $selected = $(selected);
		 var join = selected;
	 
		 $selected.find("option[value='"+"<%=adminLoggerBean.getAdid()%>"+"']").attr("selected",true);
		// $("#execute_user_id").append($selected).chosen({no_results_text: "没有该用户:"});
	 	var execute_user_idSelected = $("#execute_user_id").append($selected);
	 	var join_user_idSelected = $("#join_user_id").append(join);
		//任务执行人 和任务参与人的 multiselect
		 execute_user_idSelected.multiselect({
			 checkAllText:'全选',
			 uncheckAllText:'取消',
			 noneSelectedText:'选择执行人',
			 selectedText: '# 个人已选择',
			 selectedList: 200,
			 click: function(event, ui){
				changeList(ui.value,"join_user_id",ui.checked);
			 },
			 optgrouptoggle: function(event, ui){
					var values = $.map(ui.inputs, function(checkbox){
						return checkbox.value;
					}).join(", ");
					changeList(values,"join_user_id",ui.checked );
				},
			 checkAll: function(){
					var values = ($("#execute_user_id").val());
					changeList(values,"join_user_id",true);
				},
			 uncheckAll: function(){
					var values = [] ;
					 var option = $("#execute_user_id option");
					 option.each(function(){
						var _this = $(this);
						values.push(_this.val());
					 })
				  changeList(values,"join_user_id",false );
			   },
			   create:function(){
					var values = ($(this).val());
					changeList(values,"join_user_id",true);
			 }
		 }).multiselectfilter({
			 label:'搜索',
			 placeholder:'输入姓名'
		 });



		 //任务参与人
		join_user_idSelected.multiselect({
			 checkAllText:'全选',
			 uncheckAllText:'取消',
			 noneSelectedText:'选择执行参与人',
			 selectedText: '# 个人已选择',
			 selectedList: 200,
			 click: function(event, ui){
				changeList(ui.value,"execute_user_id",ui.checked);
			 },
			 optgrouptoggle: function(event, ui){
					var values = $.map(ui.inputs, function(checkbox){
						return checkbox.value;
					}).join(",");
					changeList(values,"execute_user_id",ui.checked );
			 },
			 checkAll: function(){
					var values = ($("#join_user_id").val());
					changeList(values,"execute_user_id",true);
			 },
			 uncheckAll: function(){execute_user_id
					 var values = [] ;
					 var option = $("#join_user_id option");
					 option.each(function(){
						var _this = $(this);
						values.push(_this.val());
					 })
				  changeList(values,"execute_user_id",false );
			 },
			 create:function(){
					var values = ($(this).val());
					changeList(values,"execute_user_id",true);
			}
		 }).multiselectfilter({
			 label:'搜索',
			 placeholder:'输入姓名'
		 });
	}
	function changeList(value,target,checkFlag){
		 var array 
		if(typeof value  == "object"){
			array = value;
		}else{
			array  = value.split(",");
		}
	  
		if(array){ 
			for(var index = 0 , count = array.length ; index < count ; index++ ){
				 
				var option = $("#" + target+" option[value='"+$.trim(array[index])+"']");
				if(checkFlag){
					//true
					option.attr("disabled","disabled");
				 }else{
					option.removeAttr("disabled");
				}	
			}
		}
		
		$("#"+target).multiselect("refresh");
	}
	
	function showUserInfo(deptId){
		var where = 0;
		for(var index = 0  , count = newa.length ; index < count ; index++ ){
			if(deptId == newa[index].deptId * 1){
				where = index;
				break;	
			}
		}
		if(!newa[where].str){
			var v = "<select data-placeholder='选择人' style='width:200px;'  class='chzn-select' id='userInfo'><option value=''></option>";
			var values = newa[where].values;
			for(var index = 0 , count = values.length ; index < count ; index++ ){
				v += "<option value='"+values[index].adid+"'>"+values[index].employe_name+"</option>";
			}
			v += "</select>";
			newa[where].str = v;
		} 
		 
		$("#userinfoDiv").html("");
		$("#userinfoDiv").append(newa[where].str );
		$("#deptNoDiv").animate({marginLeft: '-205px'}, "slow");
		$("#userInfo").append(newa[where].str).chosen({no_results_text: "没有该部门:"});

	}
	//得到所有的dept;
	 
	//挺奇怪的
	function initDateIsSet(){
		$("#is_schedule").click(function(){
			var _this = $(this);
			if(_this.attr("checked")){
				$("#startTime , #endTime").attr("disabled",false);
				_this.val("1");
			}else{
				 $("#startTime , #endTime").attr("disabled",true);
				 _this.val("0");
			}
		})
	}
	function initDatePick(){
		$("#startTime , #endTime").datetimepicker({
				showSecond: false,
				showMinute: false,
				changeMonth: true,
				changeYear: true,
				timeFormat: 'HH:mm:ss',
				dateFormat: 'yy-mm-dd'
		});
		$("#startTime").val(getCurrentDay());
		$("#endTime").val(getLastOfDay());
		//初始化开始时间和结束时间
	}
	function getLastOfDay(){
		var s = "";
		  var d = new Date();  
		  var c = ":";
		   s += d.getFullYear()+"-";
		   s += fixNumber(d.getMonth() + 1) + "-";
		   s += fixNumber(d.getDate()) + " ";   
		   s += fixNumber(23) + c + "59:59";
		return s;
	}
	function getCurrentDay(_date){
		 
		  var s = "";
		  var d = _date || new Date();  
		  var c = ":";
		   s += d.getFullYear()+"-";
		   s += fixNumber(d.getMonth() + 1) + "-";
		   s += fixNumber(d.getDate()) + " ";   
		   s += fixNumber(0) + c + "00:00";
		return s;
	}
	function beforeFormSubmit(){
		
	}
	
function fixNumber(num){
	return num < 10 ? "0"+num : num;
}
function ajaxAddSchedule(){
 	var flag = checkForm_r();
	//ajaxSubmit 
 	
	var param =  $("#add_schedule_form").serialize();
	if(!$("#is_schedule").attr("checked")){
		param += "&is_schedule=0"; 
	}
	var execute_user_id = $("#execute_user_id").val();
	param +="&execute_user_id="+execute_user_id;
	var schedule_is_note = $("#schedule_is_note").attr("checked")?"1":"0";	// dwr页面通知
	var sms_short_notify = $("#sms_short_notify").attr("checked")?"1":"0";	// dwr页面通知
	var sms_email_notify = $("#sms_email_notify").attr("checked")?"1":"0";	// dwr页面通知
	
	var is_need_replay = $("#is_need_replay").attr("checked")?"1":"0";		
	var is_update = $("#is_update").attr("checked")?"1":"0";
	//如果开始时间不是00:00:00 或者结束时间不是 23:59:59那么就不是全天任务
   	var is_all_day = "1";
   	if($("#startTime").val().indexOf("00:00:00") > 0 && $("#endTime").val().indexOf("23:59:59") > 0 ){
   	    //是全天
   		param += "&is_all_day=1";
   	}else{
 		param += "&is_all_day=0";
 		is_all_day = "0";
   	}

   	param += "&sms_email_notify="+sms_email_notify; 
	param += "&sms_short_notify="+sms_short_notify;
	param += "&schedule_is_note="+schedule_is_note;
	param += "&is_need_replay="+is_need_replay;
	param += "&is_update="+is_update;
	 
	param += "&is_all_day="+is_all_day;
 	var index =  execute_user_id.indexOf($.trim($("#assign_user_id").val()));
 
	var schedule_join_execute_id = $("#join_user_id").val();
	var fix_join_id = schedule_join_execute_id?schedule_join_execute_id : "";
	param += "&schedule_join_execute_id="+fix_join_id;
	 //是否是repeat任务
	 if($("#repeat").attr("checked")){
		param += "&corn_express="+cornExpress;
		var radio = $(":checked",$("#repeatTD"));
		var target = radio.attr("target");
		if(target){
			if(target ==="countTime" ){
				param += "&repeat_times="+$("#countTime").val();
			}
			if(target === "repeatEndTime"){
				param += "&repeat_end_time="+$("#repeatEndTime").val()+" 00:00:00";	 
			}
		};
		param +="&repeat_start_time="+$("#repeatStartTime").val()+" 00:00:00";
	 
	 }
	 if($("#repeat").attr("checked") && !$("#is_schedule").attr("checked")){
		 showMessage("周期任务请选择立即安排","alert");
		 return ;
	 }
	if(flag){
		$.ajax({
			url:'<%= action%>',
			dataType:'json',
			data:param,
			success:function(data){
				if(data && data.flag === "success"){
					showMessage("添加成功");
					//如果是立即添加的话就把任务立即添加到那个人在某个时候做什么请示
					//如果不是的话，就是把。这个任务添加到没有安排的任务中。这个时候的时间是没有任何的效果的。
					//如果是当前安排人当中有 系统登录人那么就是要刷新的。这里的assign_user_id其实就是当前的登录人 
					if(index != -1){
						if($("#is_schedule").attr("checked")){
							// 如果是重复的任务那么就是要在父页面进行一个Ajax的调用。
							//然后在后台计算出当前的日历是不是需要添加一些repeat的任务。
							//这些repeat的要直接生成一些普通的任务。
							 
							if($("#repeat").attr("checked")){
								 //执行父页面的ajax方法
						 
								 $.artDialog.opener.repeatShedule && 	 $.artDialog.opener.repeatShedule(data.entityid,'show');
							}else{
								 $.artDialog.opener.addEventNotRefresh &&
								 $.artDialog.opener.addEventNotRefresh(createObject(data.entityid,schedule_is_note,is_need_replay,is_update,is_all_day));		
							}
					    }else{
					   	 $.artDialog.opener &&  $.artDialog.opener.refreshReload();
					 
						}
					}else{
						if($("#repeat").attr("checked")){parent.repeatShedule && parent.repeatShedule(data.entityid,'notshow');}
					}
					cloesWin();
				}else{
					// 如果是没有
					showMessage("添加失败","error");
				};
			}
		})
		
	}
}


function createObject(_id,_schedule_is_note,_is_need_replay,_is_update,_is_all_day){
	var obj = {
		id:_id,
		title:$("#schedule_overview").val(),
		start:parseISO8601($.trim($("#startTime").val())),
		end:parseISO8601($.trim($("#endTime").val())),
		allDay:_is_all_day ==="1"?true:false,
		schedule_is_note:_schedule_is_note,
		is_need_replay:_is_need_replay,
		schedule_state:0,
		editable:_is_update ==="1"?true:false
		
	};
	return obj;
}


function parseISO8601(s, ignoreTimezone) {
	var m = s.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
	if (!m) {
		return null;
	}
	var date = new Date(m[1], 0, 1);
	if (ignoreTimezone || !m[13]) {
		var check = new Date(m[1], 0, 1, 9, 0);
		if (m[3]) {
			date.setMonth(m[3] - 1);
			check.setMonth(m[3] - 1);
		}
		if (m[5]) {
			date.setDate(m[5]);
			check.setDate(m[5]);
		}
		fixDate(date, check);
		if (m[7]) {
			date.setHours(m[7]);
		}
		if (m[8]) {
			date.setMinutes(m[8]);
		}
		if (m[10]) {
			date.setSeconds(m[10]);
		}
		if (m[12]) {
			date.setMilliseconds(Number("0." + m[12]) * 1000);
		}
		fixDate(date, check);
	}else{
		date.setUTCFullYear(
			m[1],
			m[3] ? m[3] - 1 : 0,
			m[5] || 1
		);
		date.setUTCHours(
			m[7] || 0,
			m[8] || 0,
			m[10] || 0,
			m[12] ? Number("0." + m[12]) * 1000 : 0
		);
		if (m[14]) {
			var offset = Number(m[16]) * 60 + (m[18] ? Number(m[18]) : 0);
			offset *= m[15] == '-' ? 1 : -1;
			date = new Date(+date + (offset * 60 * 1000));
		}
	}
	return date;
}
function fixDate(d, check) { // force d to be on check's YMD, for daylight savings purposes
	if (+d) { // prevent infinite looping on invalid dates
		while (d.getDate() != check.getDate()) {
			d.setTime(+d + (d < check ? 1 : -1) * HOUR_MS);
		}
	}
}

//通过form对象中的数据创建一个新的
 
function checkForm_r(){
	 
		if(isNull("execute_user_id")){
			showMessage("请选择执行人 ","alert");
			return false;
		}
		//alert($("#is_schedule").attr("checked"));
		if($("#is_schedule").attr("checked")){
			if(isNull("startTime")){
				showMessage("请填写开始时间","alert");
				return false;
			}
			if(isNull("endTime")){
				showMessage("请填写结束时间","alert");
				return false;
			}
			if(timeCompare("startTime","endTime")){
				showMessage("开始时间不能大于结束时间","alert");
				return false;
			}
		 }
		if(isNull("schedule_overview")){
			showMessage("请填写任务概述","alert");
			return false;
		}
		if(isNull("schedule_detail")){
			showMessage("请填写任务描述","alert");
			return false;
		}
		return true;
}
// 如果前面的大于后面的就是返回 true 否则就是返回false
function timeCompare(before,after){
	before = ($("#"+before).val().replace(/-/gi,"/"));
	after = ($("#"+after).val().replace(/-/gi,"/"));
	before = new Date(before).getTime();
	after = new Date(after).getTime();
	return before > after?true:false;
}
 
function isNull(_id){
	var dom =  $("#"+_id);
	if(dom.length > 0 &&  $.trim(dom.val()).length > 0 ){
		return false;
	}else{
		return true;
	}
}
function cloesWin(){
 
	$.artDialog && $.artDialog.close();
}
 
function showMessage(_content,_state){
	var o =  {
		state:_state || "succeed" ,
		content:_content,
		corner: true
	 };
	 var  _self = $("body"),
	_stateBox = $("<div style='font-size:12px;'/>").addClass("ui-stateBox").html(o.content);
	_self.append(_stateBox);	
	if(o.corner){
		_stateBox.addClass("ui-corner-all");
	}
	if(o.state === "succeed"){
		_stateBox.addClass("ui-stateBox-succeed");
		setTimeout(removeBox,1500);
	}else if(o.state === "alert"){
		_stateBox.addClass("ui-stateBox-alert");
		setTimeout(removeBox,2000);
	}else if(o.state === "error"){
		_stateBox.addClass("ui-stateBox-error");
		setTimeout(removeBox,2800);
	}
	_stateBox.fadeIn("fast");
	function removeBox(){
		_stateBox.fadeOut("fast").remove();
}
}
</script>
</head>
<body>
	 <form action="" id="add_schedule_form"  name="add_schedule_form" action="<%=action %>">
	  
	 	<table class="info">
	 			<tr>
	 				<td class="left">任务安排人:</td><td class="right" colspan="3"><input type="hidden" id="assign_user_id" name="assign_user_id" value="<%=adminLoggerBean.getAdid() %>"/><%=adminLoggerBean.getAccount() %></td>
	 			</tr>
	 			<tr>
	 				<td  class="left">任务执行人:</td>
	 				<td class="right" colspan="3">
	 				<select style='width:300px;' multiple="multiple"  id="execute_user_id">
	 				 </select>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td class="left">任务参与人:</td>
	 				
	 				<td class="right" colspan="3">
	 					<select style='width:300px;' multiple="multiple"  id="join_user_id">
	 				 </select>
	 				</td>
	 			</tr>
	 			 <tr id="set">
	 				<td class="left">立即安排:</td>
	 				<td class="right" colspan="3">
	 					<input type="checkbox" name="is_schedule" id="is_schedule" value="0"/>
	 					&nbsp;&nbsp;&nbsp; <input type="text" class="txt" id="startTime" value="" name="startTime" disabled="disabled"/> &nbsp;&nbsp; 到 &nbsp;&nbsp;<input type="text" class="txt" id="endTime" name="endTime" value="" disabled = "disabled"/>
	 				</td> 
	 			</tr>
	 			 <tr>
	 			 	<td class="left"></td>
	 			 	<td class="right" colspan="3"><span style="display:block;float:left;"><input id="repeat" onclick="repeatSchedule()" type="checkbox" />重复...</span><span id="showInfoStr" style="display:block;float:left;color:green;"></span><span id="aRepeat" style="display:none;"><a href="javascript:openRepeat()">修改</a></span></td>
	 			 </tr>
	 			
	 			<tr>
	 				<td class="left"></td><td class="right" colspan="3">
	 				 
						回复:<input type="checkbox" id="is_need_replay"  /> 
						自由调整:<input type="checkbox" id="is_update" /> 
						
	 				</td>
	 			 
	 			</tr>
	 			 	<tr>
	 			 
	 				<td class="left">任务提醒</td>
	 				<td class="right" colspan="3">
	 					页面提醒:<input type="checkbox" id="schedule_is_note" /> 
						短信提醒:<input type="checkbox" id="sms_short_notify" /> 
					 	邮箱提醒:<input type="checkbox" id="sms_email_notify" checked="true" /> 
						
	 				</td>
	 			 
	 			</tr>
	 			<tr>
	 				<td class="left">任务概述:</td>
	 				<td colspan="3" class="right" >
	 					<input type="text" style="width:400px;height:25px;" name="schedule_overview" id="schedule_overview" value="任务概述"/> <span style="color:silver;">可以填写50字</span>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td class="left">任务描述:</td>
	 				<td colspan="3" class="are right" style="">
	 					<textarea style="width:500px;height:100px;" name="schedule_detail" id="schedule_detail"></textarea>		
	 				</td>
	 			</tr>
	 	 	 <tr>
	 	 	 
	 			<td colspan="4" style="text-align:center;" style="line-height:40px;height:40px;">
	 				<input class="normal-green" type="button"   value="保存" name="Submit2" style="margin-top:5px;" onclick="ajaxAddSchedule();" />
					<input class="normal-white" type="button"   value="取消" name="Submit2" style="margintop:5px;" onclick="cloesWin()"/>
				</td>
	 		</tr>
	 	</table>
	 		<div class="repeatDiv">
			<table class="repeatTable">
				 
				<tr id="dayTimeSelect">
					<td class="left">重复时间:</td>
					<td class="right">
						<span class="typeText" ><input type="checkbox" id="allMonthSelected" target="monthSelect"/>每月
						 <select id="monthSelect" multiple="multiple">
							<option value="1">1月</option>
							<option value="2">2月</option>
							<option value="3">3月</option>
							<option value="4">4月</option>
							<option value="5">5月</option>
							<option value="6">6月</option>
							<option value="7">7月</option>
							<option value="8">8月</option>
							<option value="9">9月</option>
							<option value="10">10月</option>
							<option value="11">11月</option>
							<option value="12">12月</option>
						</select>
						</span>
						<span class="typeText" ><input type="checkbox" id="allWeekSelected" target="weekSelect"/>每周
							<select id="weekSelect" multiple="multiple" >
								<option value="1">周末</option>
								<option value="2">周一</option>
								<option value="3">周二</option>
								<option value="4">周三</option>
								<option value="5">周四</option>
								<option value="6">周五</option>
								<option value="7">周六</option>
							</select>
						</span>
						<span class="typeText" ><input type="checkbox" id="allDaySelected" target="daySelect"/>每天
							<select id="daySelect" multiple="multiple">
								<script type="text/javascript">
									var d = $("#daySelect");
									var str = "";
									for(var index = 1 ; index <= 31 ; index++ ){
										str += "<option value='"+index+"'> " + index +"号</option>"; 
									}
									d.append($(str));
								</script>
								 
							 
							</select>
						</span>
					</td>
				</tr>
				<tr>
					<td class="left">开始日期:</td>
					<td class="right"><input type="text" id="repeatStartTime" value="" disabled="true"/></td>
				</tr>
				<tr>
					<td class="left">结束日期:</td>
					<td class="right" id="repeatTD">
						<input type="radio" name="endDate" checked /> 从不 <br />
						<input type="radio" name="endDate" target="countTime"/>&nbsp;发生&nbsp;<input id="countTime" type="text" value="5" onkeyup="fixValue(this)"  style="width:40px;"/>&nbsp;次后<br /> 
						<input type="radio" name="endDate" target="repeatEndTime"/> <input type="text" id="repeatEndTime" value="" style="width:100px;" /> 之前
					</td>
				</tr>
				<tr>
					<td class="left">摘要:</td>
					<td class="rigth"><span id="repeatInfo"></span></td>
				</tr>
				<tr>
					 
					<td colspan="2" style="text-align:center;"> 
						<input type="button" value="保存" onclick="showResult();"/>
					    <input type="button" value="取消" onclick="cencelRepeat()"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="offbackground">
		</div>
	 </form>
	 <script type="text/javascript">
	 	function fixValue(_this){
			var node = $(_this);
			node.val(node.val().replace(/[^0-9]/g,'') * 1)
		}
		function initCheckBoxSelect(id){
			 $("#allMonthSelected").click(function(){
				var _this = $(this);
				var flag = (_this.attr("checked"))?'disable':'enable';;
				var target = _this.attr("target");
				 $("#"+target).multiselect(flag);
			 })
			 $("#allDaySelected").click(function(){
				var _this = $(this);
				var flag = (_this.attr("checked"))?'disable':'enable';;
				var target = _this.attr("target");
				 $("#"+target).multiselect(flag);
			 })
			 $("#allWeekSelected ").click(function(){
				var _this = $(this);
				var flag = (_this.attr("checked"))?'disable':'enable';;
				var target = _this.attr("target");
				 $("#"+target).multiselect(flag);
			 })
		}
		function initDaySelect(){
			$("#daySelect").multiselect({selectedList:6, noneSelectedText:'选择每月执行日期',minWidth:200, checkAllText:'全选',uncheckAllText:'取消'});
		}
		function initMonthSelect(){
			$("#monthSelect").multiselect({selectedList:6, noneSelectedText:'选择执行月',minWidth:200,checkAllText:'全选',uncheckAllText:'取消'});
		}
		function initWeekSelect(){
			$("#weekSelect").multiselect({selectedList:6, noneSelectedText:'选择执行周',minWidth:200,checkAllText:'全选',uncheckAllText:'取消'});
		}
		// 通过选择计算出 express
		function showResult(){
			var second = "0";
			var minute = "0";
			var hour = "0";
			var day = "";
			var month = "" ;
			var week = "";
			var split = " "; 
			var allMonthSelected = $("#allMonthSelected");
			var allWeekSelected = $("#allWeekSelected");
			var allDaySelected = $("#allDaySelected");
			if(allMonthSelected.attr("checked")){
				month="*";
			}else{
				month = ($("#monthSelect").val());
			}
			// 这里的 周不能和 天公用如果是公用了 那么就默认使用周的。
			if(allWeekSelected.attr("checked")){
				week = "*";
			}else{
				week = $("#weekSelect").val();
			}
			if(allDaySelected.attr("checked")){
				day = "*";
			}else{
				day = $("#daySelect").val();
			}
			if(month+"" === "null"){
				showMessage("请选择月","alert");
				return ;
			}
			if((allDaySelected.attr("checked") ||  $("#daySelect").val()) &&(allWeekSelected.attr("checked") ||  $("#weekSelect").val() )){
				showMessage("因为同时选择了天和周,我们将已周来计算","alert");
				day = "?";
			}
			
			// 如果是周不为null 那么就是天要为？ 如果是
			if(week+"" != "null" && day != "?"){
				day = "?";
			};
			if(day === "*"){
				week = "?";
			}
			if(week+"" === "null" && day+"" === "null"){
				showMessage("周和天要任选其一","alert");
				return;
			}
			if(week+"" === "null" && day+"" != null){
				week = "?";
			}
			var result = (second + split + minute + split + hour + split + day +split +  month + split+week);
			cornExpress = result;
			cencelRepeat();
			$("#showInfoStr").html($("#repeatInfo").html());
			return result;
		}
	 </script>
</body>
</html>