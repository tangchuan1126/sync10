<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 
 
 
 
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
 

<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery-ui-timepicker-addon.js"></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>	
 
 

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 
<style type="text/css">
	*{margin:0px;padding:0px;font-size:12px;}
	 body {font-size: 62.5%;}
	.showInfo{ margin-left:2px;font-size:14px;}
	 div.showInfo table tr{line-height:25px;height:25px;}
	 
	 div.showInfo table tr td.left{font-weight:bold;width:110px;text-align:right;}
	 div.showInfo table tr td.right{font-weight:bold;width:200px;text-align:right;}
	 div.Info_list{border:0px solid silver;height:300px;position:relative;}
	 div.Info_list h1{text-indent:5px;margin-bottom:5px;line-height:25px;height:25px;}
	 table.list {border-collapse:collapse;margin:0px auto;width:98%;}
	 table.list tr th{border:1px solid silver;}
	 table.list tr{line-height:25px;height:25px;}
	 table.list td {text-indent:5px;border:1px solid silver;}
	 /* 更新table css */
	 table.forupdate {border-collapse:collapse;margin:0px auto;width:98%;}
	 table.forupdate tr{line-height:25px;height:25px;}
	 table.forupdate td{border:1px solid silver;}
	 table.forupdate td.u_left{text-align:right;width:90px;}
	 table.forupdate td.u_right{text-indent:5px;width:225px;height:30px;}
	 .are{height:120px;}
	 input.txt{width:200px;}
	 
	 table td.td_state{text-align:center;color:green;}
	 .ui-datepicker{width:23em;}
	 /*process bar*/
	 div.process{width:400px;height:20px;line-height:20px;margin-left:10px;float:left;}
	 div.sub_process{width:285px;}
 	span.processSpan{float:left;display:block;margin-left:10px;margin-top:5px;color:green;}
 	span.buttonAdd{border: 1px solid silver;cursor: pointer;display: block;float: left;height: 22px;line-height: 22px; margin-left: 10px;margin-top: -3px;text-indent: 19px;width: 111px;}
	.leftTd{text-align:right;}
	span.closeFlag{display:block;float:right;border:0px solid red;width:5px;height:5px;cursor:pointer;height: 21px;line-height: 21px;text-align: center;width: 12px;}
	span.closeFlag:hover{color:red;}
	
	
	
	/*repeat css */
    .repeatDiv{margin:0 auto;width:400px;border:1px solid silver;position:absolute;left:186px;top:146px;display:none;background:white;top:40px;z-index:99;}
	.repeatDiv span.closeFlag{cursor:pointer;float:right;margin-right:5px;}
	.repeatDiv h1{line-height:25px;height:25px;}
	table.repeatTable td{height:25px;}
	table.repeatTable td.left{width:100px;text-align:right;} 
	table.repeatTable td.right{padding-left:20px;}
	span.typeText{display:block;margin-top:3px;margin-right:3px;}
 	div.offbackground{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjgzJSIgc3RvcC1jb2xvcj0iI2RjZGNkYyIgc3RvcC1vcGFjaXR5PSIwLjIzIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijg1JSIgc3RvcC1jb2xvcj0iI2RiZGJkYiIgc3RvcC1vcGFjaXR5PSIwLjIzIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(220,220,220,0.23) 83%, rgba(219,219,219,0.23) 85%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(83%,rgba(220,220,220,0.23)), color-stop(85%,rgba(219,219,219,0.23)));
	background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	background: linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(220,220,220,0.23) 83%,rgba(219,219,219,0.23) 85%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#3bdbdbdb',GradientType=0 );
	}
	div.updateRepeatSchedule ,div.deleteRepeatSchedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
	span.repeatDeleteSpan{-moz-transition: all 0.218s ease 0s;
    -moz-user-select: none;
    background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;
    color: #444444;
    cursor: pointer;
    font-size: 14px;
    font-weight: bold;
    height: 27px;
    line-height: 27px;
    min-width: 54px;
    outline: medium none;
    padding: 0 8px;
    text-align: center;
    display:block;
    margin-top:10px;
   }
   h1.title{ font-size: 14px; font-weight: normal; margin-bottom: 30px;margin-top: 20px;text-align: left;text-indent: 10px;}
   p.tip {text-align:left;text-indent: 10px;}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:300px;background:white;margin-top:100px;}
</style>
<title>Insert title here</title>
<%
	// userInfo EMPLOYE_NAME
	 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		
	 long adid = adminLoggerBean.getAdid(); 
	 long adgid = adminLoggerBean.getAdgid();
	 //得到当前部门下的主管和副主管的Id
	 DBRow[] adgidList = scheduleMgrZR.getManagerIdsByAdgid(adgid);
	 //得到总经理部门下的主管或者是副主管
	 DBRow[] managerList = scheduleMgrZR.getMangerIds();
	 DBRow userInfo = adminMgr.getDetailAdmin(adid);
	 String  employe_name = userInfo.getString("employe_name");
	 long id = Long.parseLong(StringUtil.getString(request,"id"));
	 DBRow[] rows = scheduleMgrZR.getScheduleAndUserInfoById(id);
	 DBRow row = rows[0];
	 DBRow repeatRow = null;
	 if(null != row.getString("repeat_id") && row.getString("repeat_id").length() > 0){
		 // 根据schedule_id 得到repeatRow
		 long repeatId = Long.parseLong(row.getString("repeat_id"));
		 repeatRow = scheduleMgrZR.getRepeatInfoById(repeatId);
	 }
	 String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/AddScheduleReplay.action";
	 String updateAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/UpdateAllScheduleAction.action";
	 String updateRepeat =  ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/UpdateRepeatScheduleAction.action";
	 
	 String deleteAction =  ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/DeleteSchedule.action";
 	 DBRow[]  replayRows = null;
	 if(id != 0l){
		replayRows = scheduleReplayMgrZR.getRowsByScheduleId(id);
 	 }
	 
	 // userInfoDept 
	 DBRow[] userInfoDept = scheduleMgrZR.getAllUserInfo();
	 StringBuffer executeIds = new StringBuffer() ;
	 if(null != rows && rows.length > 0){
		 for(int index = 0 , count = rows.length ; index < count ; index++ ){
	 executeIds.append(","+rows[index].getString("schedule_execute_id"));
		 }
	 }
	 DBRow[] joinIds = scheduleMgrZR.getScheduleJoinInfo(id);
%>
<script type="text/javascript">
var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>
var subScheduleList = <%= new JsonObject(rows).toString()%>
var adgidList = <%= new JsonObject(adgidList).toString() %>
var loginAdid = '<%=adid %>';
var assignAdid = '<%=row.getString("assign_user_id") %>';
var execAdid = '<%=executeIds.toString() %>'.substr(1);
var execAdidArray = execAdid.split(",");
var scheduleJoinUserId =  <%= new JsonObject(joinIds).toString()%>;
var joinIds =  [] ;
var repeatDivFlag = false;
var cornExpress  = '<%= repeatRow!=null ? repeatRow.getString("repeat_express") : ""%>';
var isRepeatDeleteFlag = cornExpress.length > 0 ? true:false;
var lastEnd = '<%= StringUtil.getString(request,"end")%>';
 
for(var index = 0 , count  = scheduleJoinUserId.length ; index < count ; index++ ){
	 joinIds.push(scheduleJoinUserId[index].schedule_join_execute_id);
}
// 这里添加一个UserinfoDept的缓存
	 jQuery(function($){
		// tag 页初始化
		 initTag();
	   //  跟进和回复内容的切换 
		 initSelect();
		 // 时间初始化
		 initTimePick();
		 //initForm();
		 initUserInfoDept();
		 //processBar
		  initProcessBar();
		  formInit();
		 $("#ui-datepicker-div").css("display","none");
		 $("#managerSave").live("click",function(){
			 ajaxSubmit(true);
		 })
		 // 如果是有repeatId那么就要进行初始化那个repeatDiv
		 var repeatId = ('<%=row.getString("repeat_id")%>');
		 if(repeatId && $.trim(repeatId).length > 0){
		 
			 initRepeatDiv();
			 handleExpress(cornExpress);
			 
		 }else{
			 $("#span_repeat_show_flag").remove();
		};
		$(".repeatDiv").live("click",function(){
		 	repeatInfo();
		});
	 })
	 
	 function handleExpress(v){
		var express = v || '<%= repeatRow !=null ? repeatRow.getString("repeat_express"):""%>';
		var array  = express.split(/\s+/);
		var month = array[4];
		var week = array[5];
		var day = array[3];
		// 初始化month
	  
		if(month === "*"){
			var monthSelect = $("#monthSelect");
			monthSelect.multiselect("disable");	 
			$("option",monthSelect).removeAttr("selected");
			 
			$("#allMonthSelected").attr("checked",true);
		}else{
			var monthSelect = $("#monthSelect");
			var values = month.split(",");
			$("option",monthSelect).removeAttr("selected");
			for(var index = 0 , count = values.length ; index < count ;index++ ){
					var option = $("option[value='"+values[index]+"']",monthSelect);
					option.attr("selected","select"); 
			}
			monthSelect.multiselect("refresh");
		}
		
		if(day === "?"){
		}else if(day === "*"){
			$("#daySelect").multiselect("disable");	 
			$("#allDaySelected").attr("checked",true);
		}else{
			var daySelect = $("#daySelect");
			$("option",daySelect).removeAttr("selected");
			var values = day.split(",");
			for(var index = 0 , count = values.length ; index < count ;index++ ){
					var option = $("option[value='"+values[index]+"']",daySelect);
					option.attr("selected","select"); 
			}
			daySelect.multiselect("refresh");
		}
		//week

		if(week === "?"){
		}else if(week === "*"){
			$("#weekSelect").multiselect("disable");	 
			$("#allWeekSelected").attr("checked",true);
		}else{
			var weekSelect = $("#weekSelect");
			$("option",weekSelect).removeAttr("selected");
			var values = week.split(",");
			for(var index = 0 , count = values.length ; index < count ;index++ ){
					var option = $("option[value='"+values[index]+"']",weekSelect);
					option.attr("selected","select"); 
			}
			weekSelect.multiselect("refresh");
		}
		// radio
		if($.trim('<%=repeatRow != null ? repeatRow.getString("repeat_never_flag") : ""%>').length > 0){
			
		}else if('<%=repeatRow != null ? repeatRow.getString("repeat_times") :""%>' * 1 > 0 ){
			 var radios = $("input[target='countTime']",$("#repeatTD"));
			  
			 var time = '<%= repeatRow != null ? repeatRow.getString("repeat_times") :"0"%>' * 1 ;
			 $("#countTime").val(time).attr("disabled",false);
			 radios.attr("checked",true);
			 
		}else{
			 var radios = $("input[target='repeatEndTime']",$("#repeatTD"));
			  
			 var time = '<%= repeatRow != null ? repeatRow.getString("repeat_end_time"):""%>' ;
			 $("#repeatEndTime").val(time.split(/\s+/)[0]).attr("disabled",false);
			 radios.attr("checked",true);
		}
		var str = repeatInfo();
		$("#a_span").html(str);
	 }
	 //repeatDiv init
	 function initRepeatDiv(){
		if(!repeatDivFlag){
			 initDaySelect();
			 initMonthSelect();
			 initWeekSelect();
			 initCheckBoxSelect();

			 // 初始化repeatStartTime时间
			 $("#repeatStartTime").val('<%= row.getString("start_time")%>'.split(/\s+/)[0]);
			 var radios = $(":radio",$("#repeatTD"));
			 $(":text",$("#repeatTD")).attr("disabled",true);
			 radios.live("click",function(){
					var _this = $(this);
					var target = (_this.attr("target"));
					 $(":text",$("#repeatTD")).attr("disabled",true);
					if(target){
						$("#"+target).attr("disabled",false);
					}
			 })
			 //
			 var date = new Date();
			 var endDate = new Date(date.getFullYear(),date.getMonth() + 1 ,date.getDate() + 1 );
			 var endDateString = getCurrentDay(endDate).split(/\s+/)[0];
			 $("#repeatEndTime").datepicker({
					dateFormat:"yy-mm-dd",
					changeMonth: true,
					changeYear: true
				}).val(endDateString);
			$("#is_repeat").attr("checked",true).attr("disabled",true);
		}else{
			repeatDivFlag = true;
		}
		 
	 }
	 function initProcessBar(){
		 $(".process").each(function(){
			 	var _this = $(this);
				var _value = _this.attr("value")? _this.attr("value")*10 :0; 
				_value =  formatFloat(_value,2);
				_this.next("span").html(_value + " %");
			 	_this.progressbar({value: _value}); 
		 });
		 $(".ui-progressbar-value").css("background","lime");
		 
		 
		 
	 }
	 //根据当前人登录人获取他的进度信息
	 function initSilder(obj){
		 
		 $('#slider').slider({
				max:10,
				min:0 ,
				animate: true,
				range:'min',
				change: function(event, ui) {
					var value = $("#slider").slider( "option", "value" );
					 $("#silder_value").html((value * 10)+"%");
					 $("#sch_state").val(value);
 

					 var process = $("div.process[sub_id='"+$("#schedule_sub_id").val()+"']") ;
					 process.progressbar("value",value * 10);
					 changeProcessValues(process,value * 10);
				}
		});
		 
		 var _value = obj["sub_state"] * 1;
		 $("#slider" ).slider( "option", "value",_value );
		 $("#silder_value").html((_value * 10)+"%");
		 $("#sch_state").val(_value);
		 
		 
	 }
	 // 修改进度条.根据所有子任务的进度的情况 进行计算总的进度,在Ajax提交过后那么就把页面上缓存的值改变
	 function changeProcessValues(process,value){
		 process.next("span.processSpan").html(value + " %");
		 //得到所有子任务的进度然后进行计算
		 var processesSpan = $("span.v");
		 var sum = 0;
		 for(var index = 0 , count = processesSpan.length ; index < count ; index++ ){
			 
			var value = $(processesSpan.get(index)).html().split(/\s+/)[0] * 1;
			sum += value; 
		 }
		 
		 var avg =formatFloat(sum / (processesSpan.length ),2);
		 $(".totalSpan").html(avg + " %");
		 $(".totalProcess").progressbar("value",avg);
		 $("#total_state").val(formatFloat(avg/10,2));
	 }
	 function formatFloat(src, pos){
	 	    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
	 }
	 function initUserInfoDept(){
			//创建一个<ul><li></li></ul>的集合
			var newa = [] ;
			for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
				var flag = true;
				for(var j = 0 , i = newa.length ; j < i ; j++ ){
					if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
						flag = false;
						break;
					}
				}
				if(flag){ //new dept
					var objs = new Object();
					objs.deptName = userInfoDept[index].name;
					objs.deptId = userInfoDept[index].adgid;
					objs.values = [];
					objs.values.push(userInfoDept[index]); 
					newa.push(objs);
				} else{
					newa[newa.length - 1].values.push(userInfoDept[index]);	
				}
			}
			var schedule_sub = [];   //执行人ID
		    for(var index = 0 , count = subScheduleList.length ; index < count ; index++ ){
		    	schedule_sub.push(subScheduleList[index]["schedule_execute_id"]);
			}
			// 页面上保存一个新的值为原始的执行人
			$("#old_execute_user").val(schedule_sub.join(","));
			$("#old_join_user").val(joinIds.join(","));
			var selected = "";
			 for(var index = 0 , count = newa.length ; index < count ; index++ ){
				var optgroup = "<optgroup label='"+newa[index].deptName+"'>";
				for(var j = 0 , length =  newa[index].values.length ; j < length ; j++ ){
					var option = "<option value='"+newa[index].values[j].adid+"'>"+newa[index].values[j].employe_name+"</option>";
					optgroup += option;
				}
				optgroup +="</optgroup>";
				selected += optgroup;
			 }
			 var $selected = $(selected);
			 var join = selected;
			 for(var index = 0 , count  = schedule_sub.length ; index < count ; index++ ){
				 $selected.find("option[value='"+schedule_sub[index]+"']").attr("selected",true);	
			 }
			 // 任务执行人。
			 // 任务参与人 初始化
		     var execute_user_idSelected = $("#execute_user_id").append($selected);
		     var join_user_idSelected = $("#join_user_id").append(join);
		     var countTotal = $("option",$selected).length  * 1 || 50;
		     
		     execute_user_idSelected.append($selected).multiselect({
				 checkAllText:'全选',
				 uncheckAllText:'取消',
				 noneSelectedText:'选择执行人',
				 selectedText: '# 个人已选择',
				 height:300,
				 minWidth:400,
				 selectedList: countTotal,
				 click: function(event, ui){
					changeList(ui.value,"join_user_id",ui.checked);
				 },
				 optgrouptoggle: function(event, ui){
						var values = $.map(ui.inputs, function(checkbox){
							return checkbox.value;
						}).join(", ");
						changeList(values,"join_user_id",ui.checked );
				 },
				 checkAll: function(){
						var values = ($("#execute_user_id").val());
						changeList(values,"join_user_id",true);
				 },
				 uncheckAll: function(){
						var values = [] ;
						 var option = $("#execute_user_id option");
						 option.each(function(){
							var _this = $(this);
							values.push(_this.val());
						 })
					  changeList(values,"join_user_id",false );
				 },
				 create:function(){
						var values = ($(this).val());
						changeList(values,"join_user_id",true);
				 }
			 }).multiselectfilter({
				 label:'搜索',
				 placeholder:'输入姓名'
			 });
			
			 if(joinIds.length > 0){
				 for(var index = 0 , count = joinIds.length ; index < count  ; index++ ){
					 $("option[value='"+joinIds[index]+"']",join_user_idSelected).attr("selected",true);
				 }
			 }
				 
			  
			 //任务参与人
				join_user_idSelected.multiselect({
					 checkAllText:'全选',
					 uncheckAllText:'取消',
					 noneSelectedText:'选择任务参与人',
					 selectedText: '# 个人已选择',
					 selectedList: countTotal,
					 minWidth:400,
					 click: function(event, ui){
						changeList(ui.value,"execute_user_id",ui.checked);
					 },
					 optgrouptoggle: function(event, ui){
							var values = $.map(ui.inputs, function(checkbox){
								return checkbox.value;
							}).join(",");
							changeList(values,"execute_user_id",ui.checked );
					 },
					 checkAll: function(){
							var values = ($("#join_user_id").val());
							changeList(values,"execute_user_id",true);
					 },
					 uncheckAll: function(){execute_user_id
							 var values = [] ;
							 var option = $("#join_user_id option");
							 option.each(function(){
								var _this = $(this);
								values.push(_this.val());
							 })
						  changeList(values,"execute_user_id",false );
					 } ,
					 create:function(){
							var values = ($(this).val());
							changeList(values,"execute_user_id",true);
					}
				 }).multiselectfilter({
					 label:'搜索',
					 placeholder:'输入姓名'
				 });
			  
		}
	 function changeList(value,target,checkFlag){
		 var array 
		if(typeof value  == "object"){
			array = value;
		}else{
			array  = value.split(",");
		}
	  
		if(array){ 
			for(var index = 0 , count = array.length ; index < count ; index++ ){
				 
				var option = $("#" + target+" option[value='"+$.trim(array[index])+"']");
				if(checkFlag){
					//true
					option.attr("disabled","disabled");
				 }else{
					option.removeAttr("disabled");
				}	
			}
		}
		
		$("#"+target).multiselect("refresh");
	}
	
	 // 先把任务的基本的信息显示出来。然后把所有的都变成disabled
	 /*
	 	1.如果是任务的安排人那么是有所有的权利
	 	2.如果登录人是改部门的主管或者是副主管那么有所有的权利
	 	3.如果登录人是deptId 是总经理办公司主管
	 */
	 function formInit(){
		 var is_schedule = $("#is_schedule");
			//is_schedule.attr("disabled",true);
			if(is_schedule.val() === "1"){
				is_schedule.attr("checked",true);
				$("#startTime , #endTime").attr("disabled",false);
			}
			$("#schedule_is_note,#is_need_replay,#is_update,#sms_short_notify,#sms_email_notify").each(function(){	
				var _this = $(this);
				if(_this.val() === "1"){
					_this.attr("checked",true);	
			 }	
		})
		is_schedule.click(function(){
			var _this = $(this);
			if(!_this.attr("checked")){
				$("#startTime , #endTime").attr("disabled",true);
			}else{
				$("#startTime , #endTime").attr("disabled",false);
			}
		})
		//.attr("disabled",true);

		 
	 }
	 function initForm(){
		var is_schedule = $("#is_schedule");
		//is_schedule.attr("disabled",true);
		if(is_schedule.val() === "1"){
			is_schedule.attr("checked",true);
			$("#startTime , #endTime").attr("disabled",false);
		 }
		$("#schedule_is_note,#is_need_replay,#is_all_day,#is_update").each(function(){	
				var _this = $(this);
				if(_this.val() === "1"){
					_this.attr("checked",true);	
			 }	
		}).attr("disabled",true);
		is_schedule.change(function(){
			 if(!is_schedule.attr("checked")){
				 $("#startTime , #endTime").attr("disabled",true);
			  }else{
				  $("#startTime , #endTime").attr("disabled",false);
			};
		})
		is_schedule.attr("disabled",true);
		var loginAdid = '<%=adid %>';
		var assignAdid = '<%=row.getString("assign_user_id") %>';
		var execAdid = '<%=row.getString("execute_user_id") %>';
		var isUpdate = '<%=row.getString("is_update") %>';
 		if(loginAdid === assignAdid){
 			is_schedule.attr("disabled",false);
 			$("#schedule_is_note,#is_need_replay,#is_all_day,#is_update").attr("disabled",false);
 	 	}else if(loginAdid == execAdid &&  isUpdate === "1"){
 	 		 $("#startTime , #endTime").attr("disabled",false);
 	 	}else{
 	 		 $("#startTime , #endTime").attr("disabled",true);
 	 	}
	 }
	 function initTimePick(){
		 $("#startTime , #endTime").datetimepicker({
			   showSecond: false,
				showMinute: false,
				changeMonth: true,
				changeYear: true,
				timeFormat: 'hh:mm:ss',
				dateFormat: 'yy-mm-dd'
		 });
	 }
	 function initSelect(){
		$("#sch_replay_type").change(function(){
				var _this = $(this);
				if(_this.val()	=== "0"){
					$("#showReplayInfoUser").css("display","block");
					$("#contentName").html("回复内容");
				}else{
					$("#showReplayInfoUser").css("display","none");
					$("#contentName").html("跟进内容");
				 };
		})	
	 }
	 function initTag(){
		 $("#tabs,#tabs_info ,#process_tabs").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 30000 } ,
		});
	 } 
	 function submit_info(){
		if($("#sch_replay_context").val().length < 1){
			showMessage("请填写跟进信息","alert");
			return ;
		}
		var _data =  $("#addform").serialize(); 
  
		$.ajax({
			url:'<%=action %>',
			data:_data,
			dataType:'json',
			success:function(data){
				if(data && data.flag === "success"){
					 //将信息动态的添加到新的table中
					 showMessage("添加成功");
					 $("#nulltr").remove();
					 $("table.list").append(createTr());
					 //将页面的SubList缓存信息修改。
					 updateSubList($("#sch_state").val(),$("#schedule_sub_id").val());
					 // 把父页面的event更新
					 
					 var _event = createObject($("#schedule_id").val(),true);
					 window.parent.updateEventNotRefresh && window.parent.updateEventNotRefresh(_event);
				}else{
					showMessage("添加失败","error");
				}
				cencel();
			}
		})
	 }
	 //将页面的缓存值改变 value 改变后的值,id 谁改变l
	function updateSubList(value,to){
		for(var index = 0 , count = subScheduleList.length ; index < count ; index++ ){
			if(subScheduleList[index]["schedule_sub_id"] * 1 == to*1 ){
				subScheduleList[index]["sub_state"] = value;
				break;
			}
		}
	}	
	 function cencel(){
		//把修改的东西huanyuan
		reBackProcessValue();
		 $("#info").animate({marginTop: '0px'}, "slow");
	 }
	 function reBackProcessValue(){
		 var to = $.trim($("#schedule_sub_id").val());
		 var process = $("div.process[sub_id='"+to+"']") ;
		 
		 var value = 0 ;
		 for(var index = 0 , count = subScheduleList.length ; index < count ; index++ ){
			 
				if(subScheduleList[index]["schedule_sub_id"] * 1 == to*1 ){
					 value = subScheduleList[index]["sub_state"];
					break;
				}
			}
			 
		 process.progressbar("value",value*10);	 
		 changeProcessValues(process,value*10);
	 }
	 function showAdd(){
		 $("#showAddReplayUserInfo").html("[" +'<%= employe_name%>' + "] 跟进信息");
		 // 如果登录人就是执行人那么就是说可以 是有 拖动的进度条的
		 if(-1 != execAdid.indexOf(loginAdid)){
			 // 显示可以拖动的进度条sliderShowFlag
			 $("#sliderShowFlag").css("display","block");
			 var obj = new Object();
			 for(var index = 0 , count = subScheduleList.length ; index < count ; index++ ){
				if(subScheduleList[index]["execute_name"] === '<%= employe_name%>'){	
					obj = subScheduleList[index];
					initSilder(obj)
					$("#schedule_sub_id").val(obj["schedule_sub_id"]);
					break;
				}
			 }
		 } 
		
		$("#info").animate({marginTop: '-522px'}, "slow");
	 
	 }
	 //添加成功过后createTr放在上面的table中 时间就是用当前客服端的时间。(简单处理)
	 function createTr(){    
		var str = "<tr>" ;
		str += "<td>"+$("#employe_name").val()+"</td>";
		str += "<td>"+getCurrentTime()+"</td>";
		str += "<td>"+$("#sch_replay_context").val()+"</td>";
		str += "<td>"+$("#sch_replay_type option:selected").html()+"</td>";
		str += "<td class='td_state'>"+$("#silder_value").html()+"</td></tr>";
		return str ;
	 }

	 //取得当前浏览器的时间
 	 function getCurrentTime(){
		var date = new Date();
		var year = date.getFullYear();
		var month = fixNumber(date.getMonth() + 1);
		var day = fixNumber(date.getDate());
		var hours = fixNumber(date.getHours());
		var minutes =fixNumber(date.getMinutes());
		var seconds = fixNumber(date.getSeconds());
	
		var value =  year+"-"+month+"-"+day + " "+hours+":"+minutes+":"+seconds;
	 
		return value;
 	 }
 	 function fixNumber(number){
			return number < 10 ? "0"+number:number;
 	 }
 	 // 新的的userIds 在execAdid 中 
 	 function selectedUserInO(id){
		var flag = false;

  		for(var index = 0 , count = execAdidArray.length ; index < count ; index++ ){
  	  	 
			if(execAdidArray[index] * 1 == id){
				flag = true;
				break;
			}
  	   	}
  	   	return flag;
 	 }
 	var param = "";
 	 function ajaxSubmit(manager){
 		param = "";
 	 	if(!checkUpdateForm()){return ;}
 	 	param =  $("#update_form").serialize();
 		$("#update_form :checkbox").each(function(){
 			var _this = $(this);
 			 // fales  时候将 
 			 if(!_this.attr("checked")){
 			 	param += ("&"+_this.attr("name")+"=" + "0");
 			 }
 			 
 		}) 
 		 // 修改人的保存
 		 /**
 		  1.如果是修改了任务的执行人。首先要给出提示 那些你删除了。那些是你新增的任务执行人
 		  2.这个时候保存成功过后要在页面重新的计算 任务的进度情况。包括新添加一个子任务
 		  3.在后台需要把子任务的执行中有这个的删除。没有的添加。&delete_user = 13234,234234&add_user=322342,2343242342
 		  4.保存的时候同时把页面的信息也保存了。
 		  
 		  
 		 */
 		 var execute_user_id = $("#execute_user_id").val();
 
 		 if(!execute_user_id || execute_user_id.length < 1){
 			showMessage("请选择任务执行人","alert");
 			return ;
 	 	 }
 	     
 	     var newAddMessage = "任务新添加执行人: ";
 	  	 var add_user = "";
 	 	 var old_user = "";
 	 	 for(var index = 0 , count = execute_user_id.length; index < count ; index++ ){
 	 		 if(!selectedUserInO(execute_user_id[index])){
				// new add
				newAddMessage += $("option[value='"+execute_user_id[index]+"']").html()+",";
 	 			add_user += ","+execute_user_id[index];
 	 	 	 }else{
 	 	 		old_user += ","+execute_user_id[index];
 	 	 	 }
 	 	 }
 	 	 // 计算deleteUserId,如果是当前的执行人被删除 的话那么就要在他的日历上移除这个任务
 	 	 
 	 	var deleteMessage = "删除任务执行人 : " ;
 	    var deleteId = "";
 		for(var index = 0 , count = execAdidArray.length ; index < count ; index++ ){
 			 if(old_user.indexOf(execAdidArray[index]) == -1){
 				deleteId += ","+execAdidArray[index];
 				deleteMessage +=  $("option[value='"+execAdidArray[index]+"']").html()+",";
 	 		 }
  	   	}
  	   	//如果任务的执行人没有改变的话那么就是按照以前的方式进行。如果是改变了的话。就要提示出来
  	   	if(!(newAddMessage === "任务新添加执行人: " && deleteMessage == "删除任务执行人 : ")){
	  	   	if(!window.confirm("你 "+ newAddMessage + "\n" + deleteMessage+"\n" + "你确定继续吗?")){
				return ;
		 	}else{
			 	// param 后面添加删除或者添加的userId
		 		deleteId = deleteId.substr(1);
		 		add_user = add_user.substr(1);
		 		if(deleteId.length > 0){param += "&deleteusers="+deleteId;}
		 		if(add_user.length > 0){param += "&addusers="+add_user;}
		 		//原来的人员-删除的人员 == 不变的人员
		 		var oldids = $("#old_execute_user").val();
		 		var notchange = strPl(oldids,deleteId);
		 		if(notchange.length > 0 ){param +=("&notchange="+notchange);}
			};
  	  	}else{
				//表示的是没有添加或者修改任何人
				var joinUserIds = $("#old_join_user").val();
  	  			param+= "&notchange="+($("#old_execute_user").val()+(joinUserIds.length > 1 ? (","+joinUserIds) :""))
  	  	 }
 	  	 
  	  	var deleteFlag =( deleteId.indexOf('<%= StringUtil.getString(request,"userInfoId")%>') != -1) ? true: false;
		//主管保存
   		if(manager){
   	 	  param += "&assign_user_id="+loginAdid;
   	   	}
   	   	//如果开始时间不是00:00:00 或者结束时间不是 23:59:59那么就不是全天任务
   	   	var is_all_day = "1";
   	   	if($("#startTime").val().indexOf("00:00:00") > 0 && $("#endTime").val().indexOf("23:59:59") > 0 ){
   	   	    //是全天
   	   		param += "&is_all_day=1";
   	   	}else{
   	 		param += "&is_all_day=0";
   	 		is_all_day = "0";
   	   	 }
   		var schedule_join_execute_id = $("#join_user_id").val();
   		var fix_join_id = schedule_join_execute_id?schedule_join_execute_id : "";
   		 
   		// 还是计算出那些参与人的Id变化了。那些没有变法 &add_user_id=234234,234234234,324234&del_join_id=11111,2222,34343;
   		//计算新添加的add_user_id 要是选择出来的Id 没有在以前的id中那么 就是新添加的user_id;
   		//删除的schedule_join_id 如果是original Id 没有再新的集合id中那么就是删除的UserId 
   		
   		var del_join_id = "";
   		var add_join_user_id = "";
   		var original = joinIds.join(",");
		if(typeof fix_join_id == "string"){
			// 表示没有新添加的任务参与人
		}else{
			for(var index = 0 , count = fix_join_id.length ; index < count ; index++ ){
				if(original.indexOf(fix_join_id[index]) == -1){
					add_join_user_id += ","+fix_join_id[index];
				}
			}
		}
		


   		
   		fix_join_id = fix_join_id.length < 1?"": fix_join_id.join(",") ;
   	 	for(var index = 0 , count = joinIds.length ; index < count ; index++ ){
   	 		if(fix_join_id.indexOf(joinIds[index]) == -1 ){
   	 			del_join_id += ","+joinIds[index];
   	   	 	}
   	   	}
   		if(del_join_id.length > 0){
   			del_join_id = del_join_id.substr(1);
   	   	}
   	   	if(add_join_user_id.length > 0){
   	   		add_join_user_id = add_join_user_id.substr(1);
   	   	 }
   		 
   		param += "&add_join_user_id="+add_join_user_id+"&del_join_user_id="+del_join_id;
   	 	if(isRepeatDeleteFlag){
   	   	 		// 重复任务的保存
	   	 	$(".updateRepeatSchedule").css("display","block");	
	   	 		
   	   	}else{
   	   		ajax_(param);
   	   	}
 	 }
 	 function ajax_(param,flag){
 	 	 
 		$.ajax({
			url: '<%=updateAction %>',
			data:param,
			dataType:'text',
			success:function(data){
				if(data && data === "success"){
					showMessage("修改成功");
					var is_all_day = "1";
			   	   	if($("#startTime").val().indexOf("00:00:00") > 0 && $("#endTime").val().indexOf("23:59:59") > 0 ){
			   	   	    //是全天
			   	   	 	is_all_day = "1";
			   	   	}else{
			   	 		is_all_day = "0";
			   	   	 }
					 
					//  需要schedule_is_note,is_need_replay,schedule_state,
					var obj = createObject($("#schedule_id").val(),false,is_all_day);
					 window.parent.updateEventAllInfoNotRefresh && window.parent.updateEventAllInfoNotRefresh(obj);
					 window.parent.closeTBWin && window.parent.closeTBWin();
					 // 如果这个人被删除了。不在任务的执行当中的时候就要执行下面的语句
					 if(deleteFlag){
						 window.parent.removeEventNotRefresh && window.parent.removeEventNotRefresh(<%=row.getString("schedule_id")  %>);
					 }
					 
				}else{
					showMessage("修改失败","error");
				}
			}
		})	
 	 }
 	// repeate schedule update
 	function multiUpdateSchedule(flag){
 	 	if(flag === "this"){
 	 		ajax_(param,"mutil");
 	 	};
 	 	 
 	 	if(flag === "after"){
 	 		param += "&current_schedule_end="+'<%= row.getString("end_time")%>'+"&current_schedule_start="+'<%= row.getString("start_time")%>'+"&end="+lastEnd+"&repeat_schedule_id="+'<%= repeatRow!=null ? repeatRow.getString("repeat_schedule_id") : ""%>';
 	 		ajaxUpdateRepeat(param); 
 	 	}
 	}
 	function strPl(oldStr,newStr){
		var oldStrArray =  oldStr.split(",");
		var newStrArray =	newStr.split(",");
		 if(newStr.length < 1){return oldStr;}
		for(var index = 0 , count =oldStrArray.length ; index < count ;index++){
			 for(var j = 0 , len = newStrArray.length ; j < len ;j++){
				if(oldStrArray[index] == newStrArray[j]){
					oldStrArray.splice(index,1);
				 
				} 
			}
		}
		return 	oldStrArray.join(",");
	}
 	function ajaxUpdateRepeat(param,flag){
 		$.ajax({
			url: '<%=updateRepeat %>',
			data:param,
			dataType:'json',
			success:function(data){
			 
				if(data && data.flag === "success"){
					cancelUpdateRepeat();
					if(data.value && data.value.length > 0){
						 window.parent.updateRepeatSchedule && window.parent.updateRepeatSchedule(data.value,'<%= repeatRow!=null ? repeatRow.getString("repeat_schedule_id") : ""%>','<%= row.getString("start_time")%>');
					}
					 window.parent.closeTBWin && window.parent.closeTBWin();
				}else{
					showMessage("修改失败","error");
				}
			}
		})	
 	 	
 	}
 	function cancelUpdateRepeat(){
 	 	$(".updateRepeatSchedule").css("display","none");
 	}
 	function parseISO8601(s, ignoreTimezone) {
		var m = s.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
		if (!m) {
			return null;
		}
		var date = new Date(m[1], 0, 1);
		if (ignoreTimezone || !m[13]) {
			var check = new Date(m[1], 0, 1, 9, 0);
			if (m[3]) {date.setMonth(m[3] - 1);check.setMonth(m[3] - 1);}
			if (m[5]) {date.setDate(m[5]);check.setDate(m[5]);}
			fixDate(date, check);
			if (m[7]) {date.setHours(m[7]);}
			if (m[8]) {date.setMinutes(m[8]);}
			if (m[10]) {date.setSeconds(m[10]);	}
			if (m[12]) {date.setMilliseconds(Number("0." + m[12]) * 1000);}
			fixDate(date, check);
		}else{
			date.setUTCFullYear(m[1],m[3] ? m[3] - 1 : 0,m[5] || 1);
			date.setUTCHours(m[7] || 0,m[8] || 0,m[10] || 0,m[12] ? Number("0." + m[12]) * 1000 : 0);
			if (m[14]) {
				var offset = Number(m[16]) * 60 + (m[18] ? Number(m[18]) : 0);
				offset *= m[15] == '-' ? 1 : -1;
				date = new Date(+date + (offset * 60 * 1000));
			}
		}
		return date;
	}
	function fixDate(d, check) { // force d to be on check's YMD, for daylight savings purposes
		if (+d) { // prevent infinite looping on invalid dates
			while (d.getDate() != check.getDate()) {
				d.setTime(+d + (d < check ? 1 : -1) * HOUR_MS);
			}
		}
	} 
	// flag 表示的是是否需要 背景颜色的变化
 	function createObject(_id,flag,_is_all_day){
 		var allDayFlag = false;
	   if(_is_all_day){
		   allDayFlag = _is_all_day === "1"?true:false;
	    }else{allDayFlag = $("#is_all_day").val()  === "1"?true:false;}
	   
 	  
 	  
 		var obj = {
 			id:_id,
 			title:$("#schedule_overview").val(),
 			start:parseISO8601($.trim($("#startTime").val())),
 			end:parseISO8601($.trim($("#endTime").val())),
 			allDay:allDayFlag,
 			schedule_is_note:$("input[name='schedule_is_note']:checked").val() || "0",
 			is_need_replay:$("input[name='is_need_replay']:checked").val() || "0",
 			schedule_overview:$("#schedule_overview").val(),
 			editable:$("input[name='is_update']:checked").val() ==="1"?true:false
 		};
 		if(flag){
 			obj.schedule_state = $("#sch_state").val()*1
 	 	 }
 	 
 		return obj;
 	}
 	 function checkUpdateForm(){
 		$("#update_form input:checked").val("1");
 	 	var is_schedule = $("#is_schedule");
		if(is_schedule.attr("checked") ==="checked" ){
			if(isNull("startTime")){
				showMessage("请选择开始时间","alert");
				return false;
			} 
			if(isNull("endTime")){
				showMessage("请选择结束时间","alert");
				return false;
			}
			if(timeCompare("startTime","endTime")){
				showMessage("开始时间不能小于等于结束时间","alert");
				return false;
			}
		}
		if(isNull("schedule_overview")){
			showMessage("请输入任务概述","alert");
			return false;
		}
		if(isNull("schedule_detail")){
			showMessage("请输入任务描述","alert");
			return false;
		}
		return true;
 	 }

 	// 如果前面的大于后面的就是返回 true 否则就是返回false
 	function timeCompare(before,after){
 	 	var length ;
 	 	before = $("#"+before).val().replace(/-/gi,"/");
 	 	after = $("#"+after).val().replace(/-/gi,"/");
 	 	if((length = before.indexOf(".")) != -1){
 	 		before = before.substr(0,length);
 	 		after = after.substr(0,length);
 	 	}
 		before = new Date(before).getTime();
 		after = new Date(after).getTime();
 		return before > after?true:false;
 	}
 	function isNull(_id){
 		var dom =  $("#"+_id);
 		if(dom.length > 0 &&  $.trim(dom.val()).length > 0 ){
 			return false;
 		}else{
 			return true;
 		}
 	}
 	//statebox
 	function showMessage(_content,_state){
					var o =  {
						state:_state || "succeed" ,
						content:_content,
						corner: true
					 };
					 var  _self = $("body"),
					_stateBox = $("<div />").addClass("ui-stateBox").html(o.content);
					_self.append(_stateBox);	
					if(o.corner){
						_stateBox.addClass("ui-corner-all");
					}
					if(o.state === "succeed"){
						_stateBox.addClass("ui-stateBox-succeed");
						setTimeout(removeBox,1500);
					}else if(o.state === "alert"){
						_stateBox.addClass("ui-stateBox-alert");
						setTimeout(removeBox,2000);
					}else if(o.state === "error"){
						_stateBox.addClass("ui-stateBox-error");
						setTimeout(removeBox,2800);
					}
					_stateBox.fadeIn("fast");
					function removeBox(){
						_stateBox.fadeOut("fast").remove();
			 }
		}
 	function cloesWin(){$.artDialog && $.artDialog.close(); j}
 	function deleteSubmit(){
 	 	if(isRepeatDeleteFlag){
			// 表示的是重复任务的删除
			$(".deleteRepeatSchedule").css("display","block");
			
 	 	}else{
			if(window.confirm("你确定删除该任务?注意会删除改任务所有信息")){
				ajaxDeleteTask();
			};
 	 	}
 	}
 	// 重复任务的删除 当是多条记录的时候
 	function multiDeleteSchedule(value){
 	 	if(value === "this"){
 	 		ajaxDeleteTask();
 	 	}
 	 	if(value === "after"){
 	 	 		ajaxDeleteMuti('<%=row.getString("repeat_id")  %>','<%= row.getString("start_time")%>'.substr(0,19));
 	 	 		 
 	 	}
 	 	if(value === "all"){
 	 			ajaxDeleteMuti('<%=row.getString("repeat_id")  %>',"all"); 
 	 	}
 	 	$(".deleteRepeatSchedule").css("display","none");	
 	}
 	function ajaxDeleteMuti(_id,_flag){
 		 var params = { id:_id,flag:_flag };
 	     var str = jQuery.param(params);
 	   
 	 	$.ajax({
 	 		url:'<%=deleteAction %>',
 	 	 	data:str,
 	 	 	dataType:'json',
 	 	 	success:function(data){
 	 	 		if(data && data.flag === "success"){
 	 	 			window.parent.removeMutilNotRefresh && window.parent.removeMutilNotRefresh(data.value);
 	 	 			cloesWin();
 	 	 	 	}else{
 	 	 	 		showMessage("删除失败","error");
 	 	 	 	}
 	 	 	}
 	 	})
 	}
 	//删除成功过后把 任务中的任务移除
 	function ajaxDeleteTask(){
		$.ajax({
			url:'<%=deleteAction %>',
			data:"id="+<%=row.getString("schedule_id")  %>,
			dataType:'json',
			success:function(data){
				if(data && data.flag === "success"){
				 	//后面页面的不刷新,把原来的去掉。
				 	window.parent.removeEventNotRefresh && window.parent.removeEventNotRefresh(<%=row.getString("schedule_id")  %>);
				 	cloesWin();
				}else{	
					showMessage("删除失败","error");
				} 
			}
		})
 	}
 	function changeClose(){
 	 	var closeFlag = $("#closeFlag");
		if(closeFlag.attr("flag") == "close"){
			$("#processDetail").animate({left: '0px'}, "slow");
		  
			closeFlag.attr("flag","notClose") ;
		}else{
			$("#processDetail").animate({left: '-451px'}, "slow");
			closeFlag.attr("flag","close") ;
		}
 	}
 	var startFlag = false;
 	var cancelSetInterval = -1;
 	var oldSchedule_detail =$("#schedule_detail").val();
 	function autoSaveStart(){
 	 	if(!startFlag){
 	 	 	startFlag = true;
 	 		cancelSetInterval =  window.setInterval(ajaxAutoSubmit,30000);
 	 	 }
 	}
 	//每隔多长的时间自动的保存 30000 默认是30秒
 	/**
 		1.如果到了保存点。数据没有改变就不要保存
 	*/
 	function ajaxAutoSubmit(){
		var _schedule_id = '<%=row.getString("schedule_id") %>';
		var _schedule_detail = $("#schedule_detail").val();
 		
		if(_schedule_detail === oldSchedule_detail || _schedule_detail.length < 1){
			return ;
		 }else{oldSchedule_detail = _schedule_detail;}
		 var param = jQuery.param({
			 schedule_id:_schedule_id,
			 schedule_detail:_schedule_detail
		  })
		 $.ajax({
			 url: '<%=updateAction %>',
			 data:param,
			 dataType:'text',
			 success:function(data){
				 if(data && data =="success" ){
					 
				 }
			 }
		})
 	}
 	//取消这个
 	function cancelAutoSubmit(){
 	 	 
 	 	 if(cancelSetInterval != -1){
 	 		clearInterval(cancelSetInterval);
 	 		startFlag = false;
 	 	 }
 	}
 	function showOrHidden(){
 	 	var is_repeat = $("#is_repeat");
 	 	if(is_repeat.attr("checked")){
 	 	 	$(".repeatDiv").css("display","block");
 	 	 	$(".offbackground").css("display","block");
 	 	 	$("#a_span").css("display","block");
 	 	 	$("#a_repeat").css("display","block");
 	 	}else{
 	 	 	// 
 	 	 	$("#a_span").css("display","none");
 	 	 	$("#a_repeat").css("display","none");
 	 	  
 	 	}
 	}
 	 
</script>
</head>
<body>
    <div id="processDetail" style="z-index:999;width:450px;position:absolute;left:-451px;top:50px;float:right;background:white;" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-corner-top">
		 					<h1 style="height:25px;line-height:25px;font-weight:normal;" class="ui-tabs ui-widget ui-widget-content ui-corner-all ui-widget-header">任务进度 
		 						 
		 						<span class="closeFlag" onclick="changeClose();" id="closeFlag" flag="close">X</span>
		 					</h1>
					  		<div style="overflow:auto;">
					  			<table>
					  				<tr>
							 	 		<td class="leftTd">任务总进度:</td>
							 	 		<td colspan="3"><div class="process sub_process totalProcess"  value='<%=row.getString("schedule_state") %>'></div>
							 	 		<span class="processSpan totalSpan" >当前任务总进度:</span></td>
							 	 	</tr>
							 	 	<%
							 	 	 if(null != rows && rows.length > 0){
							 	 		 for(int index = 0 , count = rows.length; index < count ; index++ ){
							 	 			 StringBuffer tr = new StringBuffer("<tr><td class='leftTd'><span style='color:red;margin-right:2px;'>"+rows[index].getString("execute_name ")+"</span>进度:").append("</td>");
								 	 		 tr.append("<td colspan='3'><div class='process sub_process'  sub_id='"+rows[index].getString("schedule_sub_id")+"' value='"+rows[index].getString("sub_state")+"'></div>").append("<span class='processSpan v'>").append("该子任务进度:").append("</span></td></tr>");
								 	 		 out.println(tr);
							 	 		 }
							 	 		
							 	 	 }
							 	 	%>
					  		 	</table>
					  		</div>
					  </div>
	<!-- tag标签页 -->
	<div id="tabs" style="width:98%;margin-left:2px;margin-top:3px;">
		 <ul>
			 <li><a href="#schedule_info_">任务跟进</a></li>
			 <li><a href="#schedule_update">任务修改</a></li>		 
		 </ul>
		 <div id="schedule_info_" style="height:500px;overflow:hidden;">
		 	<div class="Info_list">
		 		
	 			<div id="info" style="border:1px solid silver;height:506px;position:relative;top:0px;left:0px;overflow:auto;">
	 				<h1>跟进信息 <input type="button" class="long-button-add" value="新  增" onclick="showAdd()"/></h1>
	 				<table class="list">	
	 					<tr>
	 						<th width="10%;">跟进人</th>
	 						<th width="22%">时间</th>
	 						<th width="52%">内容</th>
	 						<th width="8%">类型</th>
	 						<th width="8%">进度</th>
	 					</tr> 
	 					<%
	 						if(null != replayRows && replayRows.length > 0){
	 							for(int index = 0 , count = replayRows.length ; index < count ; index++ ){
	 								StringBuffer sb = new StringBuffer();
	 								String state = replayRows[index].getString("sch_state");
	 								String stateFix = null;
	 								if(null != state && state.length() > 0 && !state.equals("0")){
	 									stateFix = Integer.parseInt(state)+"0%";
	 								}else{
	 									stateFix = "无";
	 								}
	 								sb.append("<tr><td>").append(replayRows[index].getString("employe_name")).append("</td><td>").append(replayRows[index].getString("sch_replay_time").substring(0,19))
	 								.append("</td><td>").append(replayRows[index].getString("sch_replay_context")).append("</td><td>").append((replayRows[index].getString("sch_replay_type").equals("0") ? "回复" : "一般跟进"))
	 								.append("</td><td class='td_state'>").append(stateFix).append("</td></tr>");
	 								out.print(sb.toString());
	 							}  
	 						}else{
	 							out.print("<tr id='nulltr'><td colspan='5' style='text-align:center;'>没有任何跟进信息</td></tr>");
	 						}
	 					%>
	 				</table>
		 		</div>
		 
		 		<div class="add_info" style="margin-top:5px;height:250px;">
		 		<fieldset style="border:2px #cccccc solid;-webkit-border-radius:7px;-moz-border-radius:7px;width:100%;padding-top:10px;">
		 			<legend style="font-size:15px;font-weight:normal;color:#999999;" id="showAddReplayUserInfo"></legend>
		 			<form id="addform" action="">
			 			<input type="hidden" value="<%=row.getString("schedule_id")  %>" id="schedule_id" name="schedule_id"/>
			 			<input type="hidden" value="<%=employe_name %>" name="employe_name" id="employe_name"/>
			 			<input type="hidden" value="<%=adid %>" name="adid"/>
			 			<input type="hidden" value="" name="sch_state" id="sch_state"/>
			 			<input type="hidden"  name="schedule_sub_id" id="schedule_sub_id" />
			 			<input type="hidden" name="total_state" id="total_state"/>
			 			<table>
			 				<tr>
			 					<td>跟进类型:</td>
			 					<td width="88%">
			 						<select name="sch_replay_type" id="sch_replay_type" style="float:left;">
			 							<option value="1">一般跟进</option>
			 							<option value="0">回复</option>
			 						</select>
			 						<span id="showReplayInfoUser" style="display:none;margin-left:10px;float:left;">回复给 :<span style="color:green;">
			 						 </span></span>
			 						  <div style="display:none;" id="sliderShowFlag">
			 							<div id="slider" style="height:10px;width:400px;margin-left:10px;margin-top:5px;float:left;"></div>
			 							<span style="margin-left:12px;margin-top:3px;float:left;">进度<span id="silder_value" style="color:green;"></span></span>
			 						  </div>
			 					 
			 						</span></span>
			 					</td>
			 				</tr>
			 				<tr>
			 					<td><span id="contentName">跟进内容</span>:</td>
			 					<td>
			 						<textarea style="height:232px;width:566px" id="sch_replay_context" name="sch_replay_context"></textarea>
			 					</td>
			 				</tr>
			 				<tr>
			 					<td>提  交  人  :</td>
			 					<td><span style="color:green;"><%=employe_name %></span></td>
			 				</tr>
			 				<tr>
				 				 <td colspan="2" style="text-align:center;" style="line-height:40px;height:40px;">
						 				<input class="normal-green" type="button"   value="保存" name="Submit2" style="margin-top:5px;" onclick="submit_info();" />
										<input class="normal-white" type="button"   value="取消" name="Submit2" style="margintop:5px;" onclick="cencel()"/>
								 </td>
			 				</tr>
			 			</table>
		 			</form>
		 			</fieldset>
		 		</div>
		 	 
		 	</div>
		 </div>
		 <!--  
		 	1.如果是当前安排人来点击的这个页面，那么他就有这个任务的所有的update的权利。(包括重新制定执行人)
		 	2.如果是执行人来点的这个页面,这个时候是需要看这个 任务中的字段 是否是 自由调整 字段。如果这个是1 .那么就是他可以调整这个任务的时间
		 	3.如果两种人都不是,这个目前还没有考虑。
		  -->
		 <div id="schedule_update" style="overflow:auto;">
		 		<form action="" id="update_form">
		 			<input type="hidden" id="old_execute_user" />
		 			<input type="hidden" id="old_join_user" />
		 			<input type="hidden" name="schedule_id" value='<%=row.getString("schedule_id") %>'/>
		 			<input type="hidden"  id="assign_user_id" value='<%=row.getString("assign_user_id") %>'/>
			 		<table class="forupdate">
				 			 <tr>
				 				<td class="u_left">任务安排人:</td><td class="u_right" colspan="3"><span style="color:green;"><%=row.getString("assign_name") %></span></td>
				 			</tr>	
				 			<tr>
				 				<td class="u_left" height="30px">任务执行人:</td>
				 				<td class="u_right" id="td_execute_user" colspan="3">
				 					<select  style='width:300px;' multiple="multiple" id="execute_user_id">
	 								</select>
				 				</td>
				 			</tr>	
				 			<tr>
				 				<td class="u_left">任务参与人:</td>
				 				<td class="u_right" id="td_enjoy_user" colspan="3">
				 					<select style='width:300px;' multiple="multiple" id="join_user_id">
	 								</select>
				 				</td>
				 			</tr>	
				 			<tr>
				 				<td class="u_left">立即安排:</td>
		 						<td class="u_right" colspan="3" >
		 							<input type="checkbox" name="is_schedule" id="is_schedule" value='<%=row.getString("is_schedule") %>' />
		 							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="startTime" name="start_time" class="txt" value='<%=row.getString("start_time") %>'/> &nbsp;&nbsp;到 &nbsp;&nbsp; <input type="text" id="endTime" name="end_time" class="txt" value='<%=row.getString("end_time") %>'/>
		 						</td>
				 			</tr>
				 			 <% if(null != repeatRow) {%>
				 			 <tr>
				 			 	<td class="u_left"></td>
				 			 	<td class="u_right" colspan="3">
				 			 		<span id="span_repeat_show_flag" >
				 			 			<span style="display:block;float:left;">重复:<input type="checkbox" id="is_repeat" name="is_repeat" onclick="showOrHidden()"/></span>
										<span id="a_span" style="color:green;display:block;float:left;"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<!--  <span id="a_repeat" style="display:block;float:left;" onclick="openRepeat();">修改</span>-->
									</span>
				 			 	</td>
				 			 </tr>
				 			 <%} %>
				 			 
				 			<tr>
				 				<td class="u_left">任务总进度:</td>
		 						<td class="u_right" colspan="3">
		 							<div class="process totalProcess" onclick="changeClose()" id="" style="margin-top:3px;" value='<%=row.getString("schedule_state") %>'></div>
							 	 		<span class="processSpan totalSpan"  style="margin-top:2px;">当前任务总进度:</span>
								</td>
				 			</tr>
				 			<tr>
				 				<td class="u_left"></td>
				 				<td class="u_right">
				 			 
				 					回复:<input type="checkbox" id="is_need_replay" name="is_need_replay" value='<%=row.getString("is_need_replay") %>'/>&nbsp;&nbsp;
				 					 <input type="hidden" id="is_all_day"  value='<%=row.getString("is_all_day") %>'/>
				 					自由调整:<input type="checkbox" id="is_update" name="is_update" value='<%=row.getString("is_update") %>'/>&nbsp;
				 					
				 				</td>
			 				</tr>
			 				 <tr>
	 			 
				 				<td class="u_left">任务提醒:</td>
				 				<td class="u_right" colspan="3">
				 					  页面提醒:<input type="checkbox" id="schedule_is_note" name="schedule_is_note" value='<%=row.getString("schedule_is_note") %>'/>
									短信提醒:<input type="checkbox" id="sms_short_notify" name="sms_short_notify" value='<%=row.getString("sms_short_notify") %>'/> 
								 	邮箱提醒:<input type="checkbox" id="sms_email_notify" name="sms_email_notify" value='<%=row.getString("sms_email_notify") %>'/> 
									
				 				</td>
				 			 
				 			</tr>
			 				<tr>
			 					<td class="u_left">任务概述:</td>
			 					<td colspan="3" class="u_right" style="height:30px;">
			 						<input type="input" style="width:500px;height:17px;" name="schedule_overview" id="schedule_overview" value= '<%=row.getString("schedule_overview") %>'  /> 
			 					</td>
			 				</tr>
			 				<tr>
			 					<td class="u_left">任务描述:</td>
			 					<td colspan="3" class="u_right are">
			 						<textarea style="  height: 251px;width: 554px;" name="schedule_detail" id="schedule_detail" onclick="autoSaveStart();" onblur="cancelAutoSubmit();"><%=row.getString("schedule_detail") %></textarea>
			 						 
			 					</td>
			 				</tr>
			 				<tr>
			 					<td class="u_left"></td>
			 					<td colspan="3" style="text-align:center;">
			 					<%
			 						//如果是当前登录是 任务的安排人才让其看见这个按钮 或者是总经理级别的人是可以删除的。
			 						if(row.getString("assign_user_id").equals(String.valueOf(adid)) || adminLoggerBean.getAdgid() == 10000){
			 							out.println("<input class='normal-green' type='button' id='deleteButton' value='删除' style='margin-top:5px;margin-bottom:5px;' onclick='deleteSubmit();'/>");
			 						};
			 					%>
			 					<!-- 
			 						 1.上级保存,这个按钮只给上级看见。比如当前登录人的主管或者是总经理中的主管
			 					 	 2.点击保存过后将该任务的安排人变成保存人的
			 					 	 3.如果这个人不是总经理的主管或者副主管。那么他点击保存过后就是变成总经理的为任务的安排人
			 					 -->
			 					 <% 
			 					 boolean isShowFlag = false;
			 					 String loginAdid = adid+"";
			 					   for(DBRow r : adgidList){
			 						   if(loginAdid.equals(r.getString("adid"))){
			 							  isShowFlag = true;
			 							   break;
			 						   }
			 					   }
			 					   //如果还是false,那么继续判断是不是总经理下的主管和副主管
			 					  if(!isShowFlag){
			 						 for(DBRow r : managerList){
			 					 		 if(loginAdid.equals(r.getString("adid"))){
			 					 			isShowFlag = true;
			 					 			break;
			 					 		 }
			 					 	 } 
			 					  }
				 			 	if(isShowFlag){
				 			 		out.print("<input type='button' class='normal-green' id='managerSave' value='上级保存'   />");
				 			 	}
			 					 %>
			 						
			 							<input class="normal-green" type="button"  id="updateButton" value="保存" name="Submit2" style="margin-top:5px;margin-bottom:5px;" onclick="ajaxSubmit();" />
										<input class="normal-white" type="button"   value="取消" name="Submit2" style="margin-top:5px;margin-bottom:5px;" onclick="cloesWin()"/>
			 					</td>
			 				</tr>
			 	 	</table>
		 	 </form>
		 	 <div class="repeatDiv" style="top:106px;">
					<table class="repeatTable" >
						<tr id="dayTimeSelect">
							<td class="left">重复时间:</td>
							<td class="right">
								<span class="typeText" ><input type="checkbox" id="allMonthSelected" target="monthSelect"/>每月
								 <select id="monthSelect" multiple="multiple">
									<option value="1">1月</option>
									<option value="2">2月</option>
									<option value="3">3月</option>
									<option value="4">4月</option>
									<option value="5">5月</option>
									<option value="6">6月</option>
									<option value="7">7月</option>
									<option value="8">8月</option>
									<option value="9">9月</option>
									<option value="10">10月</option>
									<option value="11">11月</option>
									<option value="12">12月</option>
								</select>
								</span>
								<span class="typeText" ><input type="checkbox" id="allWeekSelected" target="weekSelect"/>每周
									<select id="weekSelect" multiple="multiple" >
										<option value="1">周末</option>
										<option value="2">周一</option>
										<option value="3">周二</option>
										<option value="4">周三</option>
										<option value="5">周四</option>
										<option value="6">周五</option>
										<option value="7">周六</option>
									</select>
								</span>
								<span class="typeText" ><input type="checkbox" id="allDaySelected" target="daySelect"/>每天
									<select id="daySelect" multiple="multiple">
										<option value="1">1号</option>
										<option value="2">2号</option>
										<option value="3">3号</option>
										<option value="4">4号</option>
										<option value="5">5号</option>
										<option value="6">6号</option>
										<option value="7">7号</option>
										<option value="8">8号</option>
										<option value="9">9号</option>
										<option value="10">10号</option>
										<option value="11">11号</option>
										<option value="12">12号</option>
										<option value="13">13号</option>
										<option value="14">14号</option>
									 
									</select>
								</span>
							</td>
						</tr>
						<tr>
							<td class="left">开始日期:</td>
							<td class="right"><input type="text" id="repeatStartTime" value="" disabled="true"/></td>
						</tr>
						<tr>
							<td class="left">结束日期:</td>
							<td class="right" id="repeatTD">
								<input type="radio" name="endDate" checked /> 从不 <br />
								<input type="radio" name="endDate" target="countTime"/>&nbsp;发生&nbsp;<input id="countTime" type="text" value="5" style="width:40px;"/>&nbsp;次后<br /> 
								<input type="radio" name="endDate" target="repeatEndTime"/> <input type="text" id="repeatEndTime" value="" style="width:100px;" /> 之前
							</td>
						</tr>
						<tr>
							<td class="left">摘要:</td>
							<td class="rigth"><span id="repeatInfo"></span></td>
						</tr>
						<tr>
							 
							<td colspan="2" style="text-align:center;"> 
								<input type="button" value="保存" onclick="showResult();"/>
							    <input type="button" value="取消" onclick="cencelRepeat()"/>
							</td>
						</tr>
					</table>
				</div>
		 </div>
		 
	</div>
	<div class="offbackground">
	 </div>
	 <div class="deleteRepeatSchedule">
	 	<div class="ui-corner-all innerDiv">
	 		<h1 class="title">编辑重复任务</h1>
	 		<p class="tip">您是要仅更改此任务，或是更改该系列中的所有任务，还是同时更改此任务及该系列中的所有后续任务？</p>
	 		<table style = "margin:0px auto;">
	 			<tr>
	 				<td width="150px;"><span class="repeatDeleteSpan" onclick="multiDeleteSchedule('this')">仅此任务</span></td>
	 				<td style="width:300px;text-align:left;text-indent:10px;">该系列中的其他所有任务都将保持不变。</td>
	 			</tr>
	 			<tr>
	 				<td><span class="repeatDeleteSpan" onclick="multiDeleteSchedule('after')">所有后续任务</span></td>
	 				<td style="width:300px;text-align:left;text-indent:10px;">此任务及其相关的所有后续任务均会发生更改。<br />
						对将来任务进行的任何更改都会丢失。
					</td>
	 			</tr>
	 			<tr>
	 				<td><span class="repeatDeleteSpan" onclick="multiDeleteSchedule('all')">此系列中所有任务</span></td>
	 				<td style="width:300px;text-align:left;text-indent:10px;">此系列中的所有任务均会发生更改。<br />对其他任务进行的任何更改均会保留</td>
	 			</tr>
	 		</table>
	  			<div style="float: right; margin-right: 30px;margin-top: 25px;width: 150px;">
	 	 			<span class="repeatDeleteSpan" onclick="cancelDeleteRepeat()">取消</span>
	 	 		</div>
	 	</div>
	 </div>
	 <div class="updateRepeatSchedule">
	 	<div style="" class="ui-corner-all innerDiv">
	 		<h1 class="title">编辑重复任务</h1>
	 		<p class="tip">您是要仅更改此任务，或是更改此任务及该系列中的所有后续任务？</p>
	 		<table style = "margin:0px auto">
	 			<tr>
	 				<td width="150px;"><span class="repeatDeleteSpan" onclick="multiUpdateSchedule('this')">仅此任务</span></td>
	 				<td style="width:300px;text-align:left;text-indent:10px;">该系列中的其他所有任务都将保持不变。</td>
	 			</tr>
	 			<tr>
	 				<td><span class="repeatDeleteSpan" onclick="multiUpdateSchedule('after')">所有后续任务</span></td>
	 				<td style="width:300px;text-align:left;text-indent:10px;">此任务及其相关的所有后续任务均会发生更改。<br />
						对将来任务进行的任何更改都会丢失。
					</td>
	 			</tr>
	 		</table>
	 		<div style="float: right; margin-right: 30px;margin-top: 75px;width: 150px;">
	 			<span class="repeatDeleteSpan" onclick="cancelUpdateRepeat()">取消</span>	
	 		</div>
	 	</div>
	 </div>
	 <form action="">
	 </form>
	 <script type="text/javascript">
	 function cancelDeleteRepeat(){
		 	$(".deleteRepeatSchedule").css("display","none");
	 }
	 function initCheckBoxSelect(id){
		 $("#allMonthSelected").click(function(){
			var _this = $(this);
		 
			var flag = (_this.attr("checked"))?'disable':'enable';;
			var target = _this.attr("target");
			 $("#"+target).multiselect(flag);
		 })
		 $("#allDaySelected").click(function(){
			var _this = $(this);
			var flag = (_this.attr("checked"))?'disable':'enable';;
			var target = _this.attr("target");
			 $("#"+target).multiselect(flag);
		 })
		 $("#allWeekSelected ").click(function(){
			var _this = $(this);
			var flag = (_this.attr("checked"))?'disable':'enable';;
			var target = _this.attr("target");
			 $("#"+target).multiselect(flag);
		 })
	 }
		function initDaySelect(){
			$("#daySelect").multiselect({selectedList:6, noneSelectedText:'选择每月执行日期',minWidth:200, checkAllText:'全选',uncheckAllText:'取消'});
		}
		function initMonthSelect(){
			$("#monthSelect").multiselect({selectedList:6, noneSelectedText:'选择执行月',minWidth:200,checkAllText:'全选',uncheckAllText:'取消'});
		}
		function initWeekSelect(){
			$("#weekSelect").multiselect({selectedList:6, noneSelectedText:'选择执行周',minWidth:200,checkAllText:'全选',uncheckAllText:'取消'});
		}
		function openRepeat(){
		 
			handleExpress(cornExpress);
			$(".repeatDiv").css("display","block");
		 	$(".offbackground").css("display","block");
		}
		function cencelRepeat(){
			$(".repeatDiv").css("display","none");
		 	$(".offbackground").css("display","none");
		  
		}
		function getCurrentDay(_date){
			 
			  var s = "";
			  var d = _date || new Date();  
			  var c = ":";
			   s += d.getFullYear()+"-";
			   s += fixNumber(d.getMonth() + 1) + "-";
			   s += fixNumber(d.getDate()) + " ";   
			   s += fixNumber(0) + c + "00:00";
			return s;
		}
		//repeat 提示信息
		function repeatInfo(){
			// 读取月份信息
			var infoStr = "";
			var allMonthSelected = $("#allMonthSelected");
			if(allMonthSelected.attr("checked")){
				infoStr += "每月 ";
			}else{
				var array =  $("#monthSelect").val();
				if(array && array.length > 0){
				 infoStr += array + "月 ";
				}
			}
			// 读取周和天的信息(如果是周和天都选择的话那么就用周的来计算结果)
			var allWeekSelected =  $("#allWeekSelected");
		 
			var allDaySelected = $("#allDaySelected");
			if(allWeekSelected.attr("checked")){
				infoStr += " 每周";
			}else{
				// 然后在计算天的东西
				var weekArray = $("#weekSelect").val();
				
				if(weekArray && weekArray.length > 0){
					var weekStr = "" ;
					var weekValue = [',周末',',星期一',',星期二',',星期三',',星期四',',星期五',',星期六'];
					for(var index = 0 , count = weekArray.length ; index < count ; index++){
						 weekStr += weekValue[(weekArray[index] * 1 -1)]; 
					} 
					infoStr += weekStr.substr(1);
				}else{
					//天
					 if(allDaySelected.attr("checked")){
						 infoStr += "每天"; 
					}else{
						var daySelect = $("#daySelect").val();
						if(daySelect && daySelect.length > 0){
							infoStr += daySelect + "号"; 
						}
					}
				}
			}
			if(infoStr.length > 1){
				infoStr += " 执行";
			}
			// 读取结束时间
			var radio = $(":checked",$("#repeatTD"));
			var target = radio.attr("target");
			if(target){
				if(target ==="countTime" ){
					infoStr += "  发生"+$("#countTime").val()+"次后结束";
				}
				if(target === "repeatEndTime"){
					infoStr += "  在"+$("#repeatEndTime").val()+"之前结束";
				}
			};
			
			$("#repeatInfo").html(infoStr);
			return infoStr;
		}
		// 通过选择计算出 express
		function showResult(){
			var second = "0";
			var minute = "0";
			var hour = "0";
			var day = "";
			var month = "" ;
			var week = "";
			var split = " "; 
			var allMonthSelected = $("#allMonthSelected");
			var allWeekSelected = $("#allWeekSelected");
			var allDaySelected = $("#allDaySelected");
			if(allMonthSelected.attr("checked")){
				month="*";
			}else{
				month = ($("#monthSelect").val());
			}
			// 这里的 周不能和 天公用如果是公用了 那么就默认使用周的。
			if(allWeekSelected.attr("checked")){
				week = "*";
			}else{
				week = $("#weekSelect").val();
			}
			if(allDaySelected.attr("checked")){
				day = "*";
			}else{
				day = $("#daySelect").val();
			}
			if(month+"" === "null"){
				showMessage("请选择月","alert");
				return ;
			}
			if((allDaySelected.attr("checked") ||  $("#daySelect").val()) &&(allWeekSelected.attr("checked") ||  $("#weekSelect").val() )){
				showMessage("因为同时选择了天和周,我们将已周来计算","alert");
				day = "?";
			}
			
			// 如果是周不为null 那么就是天要为？ 如果是
			if(week+"" != "null" && day != "?"){
				day = "?";
			};
			if(day === "*"){
				week = "?";
			}
			if(week+"" === "null" && day+"" === "null"){
				showMessage("周和天要任选其一","alert");
				return;
			}
			if(week+"" === "null" && day+"" != null){
				week = "?";
			}
			var result = (second + split + minute + split + hour + split + day +split +  month + split+week);
			cornExpress = result;
			 
			cencelRepeat();
			$("#a_span").html($("#repeatInfo").html());
			return result;
		}
	 </script>
</body>
</html>