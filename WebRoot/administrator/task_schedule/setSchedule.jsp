<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>


<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
 
 
 
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<script src="../js/fullcalendar/colorpicker.js" type="text/javascript"></script>
 <link type="text/css" href="../js/fullcalendar/css/colorpicker.css" rel="stylesheet" />
 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<%
	DBRow row	= scheduleSetMgrZR.getScheduleSet();
	String updateAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/UpdateSetSchedule.action";
	 
%>
 <style type="text/css">
 	
 	a{font-size:12px;}
 	td{height:25px;line-height:25px;}
 	td.bgColorSet , td.fontColorSet{text-indent:50px;}
 	span.colorShow{text-align:center;height:20px;line-height:20px;width:80px;border:1px solid silver;display:block;margin-left:200px;background:white;cursor:pointer;}
 	span.colorResult{display:block;float:left;text-align:center;height:28px;line-height:28px;width:100px;border:1px solid silver;display:block;margin-left:10px;background:white;cursor:pointer;}
 </style>
<script type="text/javascript">
	var objs = <%= new JsonObject(row).toString()%> ;
	jQuery(function($){ 
		initColorPicker();
		showAllColor();
		initSelect();
		 
	 
	})
	
	function initSelect(){
		var step  = objs["step"];
		$("#selectStep option[value='"+step+"']").attr("selected",true);	
	}

	function initColorPicker(){
		// 字体颜色设置
		$("span.colorShow").each(function(){
			
			var _this = $(this);
			var _value = _this.attr("value");
		 	var target = _this.attr("target");
		 	if(target === "bgColor"){
				_this.css('backgroundColor', '#' + _value);
			}else if(target === "bgBorder"){
				_this.css('borderColor', '#' + _value);}
			else{
				_this.css('color', '#' + _value);
			}
		 	var isCanUpdate = $("#isCanUpdate");
			if(isCanUpdate.length == 0){
				 return ;
			};
			_this.ColorPicker({
				color: '#'+_value,
				onShow: function (colpkr) {
					$(colpkr).fadeIn(500);
					return false;
				},
				onHide: function (colpkr) {
					$(colpkr).fadeOut(500);
					return false;
				},
				onChange: function (hsb, hex, rgb) {
					if(target === "bgColor"){
						_this.css('backgroundColor', '#' + hex).attr("value",hex);
					}else if(target === "bgBorder"){
						_this.css('borderColor', '#' + hex).attr("value",hex);
					}else {
						_this.css('color', '#' + hex).attr("value",hex);
					}
					showAllColor();
				}
			})
		})
		// 背景颜色
	}
		//#FF00FF 转化成 rgb数组
		function color2rgb(color){
			 var r = parseInt(color.substr(1, 2), 16);
			 var g= parseInt(color.substr(3, 2), 16);
			 var b = parseInt(color.substr(5, 2), 16);
			 return new Array(r, g, b);
		}
		// 颜色Array(255,0,255)格式转为#FF00FF
		function rgb2color(rgb){
			 var s = "#";
			 for (var i = 0; i < 3; i++){
			  var c = Math.round(rgb[i]).toString(16);
			  if (c.length == 1)
			   	 c = '0' + c;
			 	 s += c;
			 }
			 return s.toUpperCase();
	 	}
		// 生成渐变
		function gradient(){
			 var Step = 5;
			 var Gradient = new Array(3);
			 var A = color2rgb(ColorA);
			 var B = color2rgb(ColorB);
			 for (var N = 0; N <= Step; N++){
				  for (var c = 0; c < 3; c++)  {
				   Gradient[c] = A[c] + (B[c]-A[c]) / Step * N;
				  }
				  alert(rgb2color(Gradient));
			}
		}
		//计算出所有的组合颜色
		function showAllColor(){
			$("span.colorResult").remove();
			var  set = ['#morenSet','#wancSet'];
			var bgColor = $("span[target='bgColor']");
			var fontColor = $("span[target='font']");
			 
			// 分别取出数据进行计算
			for(var  index = 0,count = bgColor.length; index < count ; index++ ){
				// backgroundColor 
				var _backgrounColor = $(bgColor[index]).attr("value");
				 var appendTarget = $(set[index]);
				for(var j = 0 , length = fontColor.length ; j < length ; j++ ){
					// color  text
					var _$fontColor = $(fontColor[j]);
					var _fontColor = _$fontColor.attr("value");
					var text = _$fontColor.parent().prev().html();
		 			var span = createSpanWithColorAndText(text,"#"+_fontColor,"#"+_backgrounColor);
		 			appendTarget.append(span);
				}
			}
		}
		function createSpanWithColorAndText(text , color , bgColor){
			var span = "<span class='colorResult' style='color:"+color+";background:"+bgColor+"'>"+text+"<span>";
			return span;
		}
		// uri?userName=zhangsan&password=zhangsan
		function updateSubmit(){
			var param = "";
			$("span.colorShow").each(function(){
				var _this = $(this);
				var name = _this.attr("name");
				var value = _this.attr("value");
				param += ("&"+name+"="+value);
			});
			param += ("&step=" + $("#selectStep").val()+"&schedule_set_id="+objs["schedule_set_id"]);
			param = (param.substr(1));
			ajaxUpdate(param);
		}
		function ajaxUpdate(param){
			$.ajax({
				url:'<%=updateAction%>',
				data:param,
				dataType:'text',
				success:function(data){
					if(data && data==="success"){
						showMessage("修改成功");
					}else{
						showMessage("修改失败","error");
					}
				}
			})
		}
		function showMessage(_content,_state){
					var o =  {
						state:_state || "succeed" ,
						content:_content,
						corner: true
					 };
					 var  _self = $("body"),
					_stateBox = $("<div />").addClass("ui-stateBox").html(o.content);
					_self.append(_stateBox);	
					if(o.corner){
						_stateBox.addClass("ui-corner-all");
					}
					if(o.state === "succeed"){
						_stateBox.addClass("ui-stateBox-succeed");
						setTimeout(removeBox,1500);
					}else if(o.state === "alert"){
						_stateBox.addClass("ui-stateBox-alert");
						setTimeout(removeBox,2000);
					}else if(o.state === "error"){
						_stateBox.addClass("ui-stateBox-error");
						setTimeout(removeBox,2800);
					}
					_stateBox.fadeIn("fast");
					function removeBox(){
						_stateBox.fadeOut("fast").remove();
			 }
		}
</script>
</head>
<body onLoad="onLoadInitZebraTable()">
<!-- 
	这个页面不是所有的人都可以进行设置的。如果是管理员那么就是有权利的。如果是普通的用户那么就只有查看的权利
	 背景色     1.正常完成,延期,延期完成 (关于进度的设置。10 30 40 50 80 100% )
	 关于进度的颜色可以有10种。所以这个组合有点多。在用的时候可以设置的少一点。
 	字体颜色 1.需要通知 ,是否提醒, 通知加上提醒。默认是白色
 	
 	
 	在设置的时候     字体颜色的字变化的是字的颜色   , 背景设置变化的背景的颜色
 	在后面要实现把所有的组合 联合起来然后一起来查看是什么样子的。这样就可以防止在设置的时候不知道，最终的效果是什么样子的，导致用的时候
 	出现了显示不清楚的情况
 -->
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="80%" class="page-title" style="border-bottom:1px solid #BBBBBB;" align="left"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 任务管理 »   基础设置 </td>
  </tr>
</table>
 <br />
 <form action="" id="updateForm">
		<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0"   class="zebraTable">
		      <tr> 
		        <th width="29%" style="vertical-align: center;text-align: center;" class="left-title"> 描 述</th>
		        <th width="71%" style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
		      </tr>
		      <tr>
		      	<td colspan="2" style="text-align:center;">字体的设置主要是任务的一些基本属性颜色设置</td>
		      </tr>
		       <tr>
		      	<td>字体颜色 </td>
		      	<td></td>
		      </tr> 	
		      <tr>
		      	<td class="fontColorSet">默认</td>
		      	<td>
		      	<span name="default_font_color" value='<%=row.getString("default_font_color") %>' class="colorShow" target="font">任务描述</span></td>
		      </tr>
		      <tr>
		      	<td class="fontColorSet">需要回复</td>
		      	<td><span  name="need_replay_font_color" value='<%=row.getString("need_replay_font_color") %>' class="colorShow"  target="font">任务描述</span></td>
		      </tr>
		     <tr>
		      	<td class="fontColorSet">需要提醒</td>
		      	<td><span  name="need_notice_font_color" value='<%=row.getString("need_notice_font_color") %>' class="colorShow" target="font">任务描述</span></td>
		      </tr>
		 	 <tr>
		      	<td class="fontColorSet">需要提醒回复</td>
		      	<td><span name="need_replay_notice_color" value='<%=row.getString("need_replay_notice_color") %>' class="colorShow" target="font">任务描述</span></td>
		      </tr>
		      <tr>
		      	<td colspan="2"></td>
		      </tr>
		      <tr>
		      	<td colspan="2"></td>
		      </tr>
		      <tr>
		      	<td colspan="2" style="text-align:center;">任务背景颜色设置主要对任务进度显示的时候进行背景颜色的设置</td>
		      </tr>
		      
		      <tr>
		      	<td>背景颜色</td>
		      	<td></td>
		      </tr>
		      <tr>
		      	<td class="bgColorSet">默认颜色</td>
		      	<td><span name="default_bg_color"  value='<%=row.getString("default_bg_color") %>' class="colorShow" target="bgColor"></span></td>
		      </tr>
		      <tr>
		      	<td class="bgColorSet">完成颜色</td>
		      	<td><span name="complete_bg_color"  value='<%=row.getString("complete_bg_color") %>' class="colorShow" target="bgColor"></span></td>
		      </tr>
		       <tr>
		        <td class="bgColorSet">进度档次</td>
		        <td>
		      		<select id="selectStep" style="margin-left:200px;">
		      			<option value="2">2</option>
		      			<option value="3">3</option>
		      			<option value="4">4</option>
		      			<option value="5">5</option>
		      			<option value="6">6</option>
		      			<option value="7">7</option>
		      			<option value="8">8</option>
		      			<option value="9">9</option>
		      			<option value="10">10</option>
		      		</select>
		      	</td>
		      </tr>
		       <tr>
		      	<td colspan="2" style="text-align:center;">延期设置任务的边框颜色(正常情况下是没有边框的)</td>
		      </tr>
		      <tr>
		      	<td class="bgColorSet">延期边框颜色</td>
		      	<td><span name="delay_border_color" value='<%=row.getString("delay_border_color") %>' class="colorShow" target="bgBorder"></span></td>
		      </tr>
		       <tr>
		      	<td></td>
		      	<td></td>
		      </tr>
		       <tr>
		      	<td  style="height:35px;line-height:35px;width:100px;">
		      		<input type="button" value="计算所有的组合颜色" style="float:left;" onclick="showAllColor()"/>
		      	</td>
		      	<td>
		      		<p id="morenSet" style="margin:0px;padding:0px;height:30px;line-height:30px;">
		      			<span style="display:block;float:left;">默认背景颜色  ：</span> 
		      		</p> 
		      		<br/>
		      		 <p id="wancSet" style="margin:0px;padding:0px;height:30px;line-height:30px;">
		      			<span style="display:block;float:left;">完成背景颜色  ：</span> 
		      		</p> 
		      	</td>
		      	  
		      </tr>
		      <tr>
		      	<td>
		      		 <tst:authentication bindAction="com.cwc.app.api.zr.ScheduleSetMgrZr.updateScheduleSet">
						<input class="long-button-ok" type="button" onclick="updateSubmit()" value="保存修改" id="isCanUpdate" name="Submit3" />		  			
			  		</tst:authentication>
		      		
		      	
		      	</td>
		      	<td></td>
		      </tr>
		      
		</table>
 </form>
</body>
</html>