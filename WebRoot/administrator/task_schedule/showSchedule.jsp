<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.app.util.TDate" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>任务查看</title>

 <%
 	long  l = new Date().getTime();
  	String actionRepeatUri =  ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/AddScheduleRepeatAction.action";
 %>
<script type="text/javascript" src="../js/poshytip/jquery-1.4.min.js?date='<%=l%>'"></script>
<link type="text/css" href="../comm.css" rel="stylesheet" />
<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>


<script type="text/javascript" src="../js/fullcalendar/jquery-ui-1.7.3.custom.min.js"></script>	 
 
<link type="text/css" href="../js/fullcalendar/jquery-ui-1.7.3.custom.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link type="text/css" href="../js/fullcalendar/fullcalendar.css" rel="stylesheet" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 


 <style type="text/css">
 	a{font-size:12px;}
 	#wrap {width: 90%;margin-top:10px;}	
	#external-events {float: left;width: 13%;padding: 0 10px;border: 1px solid #ccc;background: #eee;text-align: left;}
	#external-events h4 {font-size: 16px;margin-top: 0;padding-top: 1em;}
	.external-event {margin: 10px 0;padding: 2px 4px;background: #3366CC;color: #fff;font-size: 1.35em;cursor: pointer;text-indent:10px;}	
	.self{background:#669933;}
	#external-events p {margin: 1.5em 0;font-size: 14px;color: #666;}
	#external-events p input {margin: 0;vertical-align: middle;}
	#calendar {float:left;width:83%;margin-left:5px;}
	.inputButton {border:1px solid silver;cursor:pointer;margin-left:5px;background:none;color:white;}
	.inputButton:hover{color:#f60;}
	div.info{border:2px solid #c6d880;width:350px;background:#e6efc2;position:absolute;overflow:auto;left:0px;top:5px;z-index:999;}
 	div.info p{font-size:12px;margin-top:8px;display:block;text-align:left;text-indent:10px;}
 	div.info h1{font-size:12px; margin:0px; padding:0px;}
 	#ui-datepicker-div{z-index:9999;}
 	span.closeFlag{margin-rigth:4px;cursor:pointer; display: block;float: right; height: 17px;margin-right: 3px; width: 15px;}
 	span.closeFlag:hover{color:red;}
  div.cssDivschedule{
	width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
	 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
	background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
	}
   div.innerDiv{margin:0 auto; margin-top:100px;text-align:center;border:1px solid silver;margin:0px auto;width:500px;height:30px;background:white;margin-top:200px;line-height:30px;font-size:14px;}
	
 </style>
 
 
 
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/fullcalendar/fullcalendar.min.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<%
	String action = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/GetScheduleByIdAction.action";
%>
 

 <%
  	//得到当前登录人的Id,根据Id去进行一个查询
  	DBRow newrow = new DBRow();
  	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
  	
   	long adid = adminLoggerBean.getAdid(); 
   	DBRow loginUserInfo = adminMgr.getDetailAdmin(adid);
   	//如何是查询时候提交的页面这个时候用request中的值，否则就是查询当前这个月的值
      TDate date = new TDate();
   	String first = date.getThisMonthFirstDay();
   	String end =  date.getThisMonthLastDay();
   	String st = StringUtil.getString(request,"st");
   	String en = StringUtil.getString(request,"en");
   	String assign_user_id = StringUtil.getString(request,"assign_user_id");
   	//用户信息
   	
   	DBRow userInfo = null;
  	newrow.add("executeUserId",adid);
  	// filter or search
  	String cmd = StringUtil.getString(request,"cmd");
  	DBRow[] rows = null;
  	//得到参与人的任务
  	DBRow[] joinRows = null;
  	DBRow queryJoinRow = new DBRow();
  	 if("search".equals(cmd)){
  		 String schedule_detail = StringUtil.getString(request,"schedule_detail");		
  		 newrow.add("schedule_detail",schedule_detail);
  		 rows = scheduleMgrZR.getScheduleByDetailAndUserId(newrow);
  		 userInfo = adminMgr.getDetailAdmin(adid);
  	 }else {
  		 if(null != st && st.length() > 0 && null != en && en.length() > 0){
  	 		if(st.length() < 19){
  	 			st = st.trim()+" 00:00:00";
  	 		}
  	 		if(en.length() < 19){
  	 			en = en.trim()+" 23:59:59";
  	 		}
  	 		newrow.add("start",st);
  	 		newrow.add("end",en);
  	 	 }else{
  	 		newrow.add("start",first);
  	 	 	newrow.add("end",end);
  	     }
  		 if("filter".equals(cmd)){
  	 //filter	高级查询
  		 	String execute_user_id = StringUtil.getString(request,"execute_user_id");
  		 	String assigndeptno = StringUtil.getString(request,"assigndeptno");
  		 	String executedeptno = StringUtil.getString(request,"executedeptno");
  		 	String state = StringUtil.getString(request,"state");
  		 	newrow.add("state",state);
  		 	if(executedeptno != null && executedeptno.length() >  1 && execute_user_id.length() < 1){
  		 		newrow.add("execute_user_id","");
  		 	}else{
  		 		newrow.add("execute_user_id",execute_user_id);
  		 	}
  		 	if(assigndeptno != null && assigndeptno.length() > 1 && assign_user_id.length() < 1){
  		 		newrow.add("assign_user_id","");
  		 	}else{
  		 		newrow.add("assign_user_id",assign_user_id);
  		 	}
  		    newrow.add("assigndeptno",assigndeptno);
  		    newrow.add("executedeptno",executedeptno);
  	rows = scheduleMgrZR.getAllScheduleByExecuteUserIdAndTime(newrow);
  	joinRows = scheduleMgrZR.getScheduleJoinSchedule(newrow);
  	if(null != execute_user_id && !"".equals(execute_user_id)){
  		  
  	 }else{
  		  execute_user_id  = adid + "";
  	}
  	 userInfo = adminMgr.getDetailAdmin(Long.parseLong(execute_user_id));
  		 }else {
  	//第一次进来的时候
  	  String execute_user_id = StringUtil.getString(request,"execute_user_id");
  	  if(null != execute_user_id && !"".equals(execute_user_id)){
  	  }else{
  		  execute_user_id  = adid + "";
  	  }
  	  userInfo = adminMgr.getDetailAdmin(Long.parseLong(execute_user_id));
  	  newrow.add("assign_user_id",assign_user_id);
  	  newrow.add("execute_user_id",execute_user_id);
  	  rows = scheduleMgrZR.getAllScheduleByExecuteUserIdAndTime(newrow);
  	  joinRows = scheduleMgrZR.getScheduleJoinSchedule(newrow);
  		 } 
  	 } 
  	
   	// 得到当前时间 和登录人的Adid
   	
   	
   	
   	// 下面是得到这个人在这个月没有安排的任务
   	DBRow queryRow = new DBRow();
   	
   	queryRow.add("starttime",first);
   	queryRow.add("endtime",end);
   	queryRow.add("execute_user_id",userInfo.getString("adid"));
    
    
   	DBRow[] unAssign = scheduleMgrZR.getScheduleByUnAssign(queryRow);
   	DBRow[] userInfoDept = scheduleMgrZR.getAllUserInfo();
  	
   	//scheduleSet value 
   	DBRow scheduleSet	= scheduleSetMgrZR.getScheduleSet();
   	// ajax getSchedule 
  	String ajaxGetSchedule = ConfigBean.getStringValue("systenFolder")+"action/administrator/schedule_management/GetScheduleByAjaxAction.action";
  	// 张睿添加,在提醒页面点击任务的时候弹开这个任务详细的页面
   	long schedule_id = StringUtil.getLong(request,"schedule_id");
  	int is_schedule = StringUtil.getInt(request,"is_schedule");
  	String from = StringUtil.getString(request,"from");
  %>
<script type="text/javascript">
	var loginUserInfo = <%=new JsonObject(loginUserInfo).toString()%>;
	 
	var isManager =  (loginUserInfo && loginUserInfo["projsid"] * 1 ? loginUserInfo["projsid"] * 1 : 1) >= 5 ? true : false;
	
	var objs = <%= new JsonObject(rows).toString()%> ;
	var joinRows = <%= new JsonObject(joinRows).toString()%> ;
	var userInfoDept =  <%= new JsonObject(userInfoDept).toString()%>;
	var scheduleSet = <%= new JsonObject(scheduleSet).toString()%>;
	var unAssignList ;
	var showContent = "sss";
	var infoCache = {};
	//颜色级别
	var bgColorSet = [];
	//颜色分的等级[0,5,10] , [0,3.3 , 6.6 , 9.9];
	var bgColor = [];
	var currentTime = '<%=date.getCurrentTime() %>' ;
	var YMD = (currentTime.split(/\s+/)[0].split(/-/));
	var newa = [] ;
	var state =  '<%=StringUtil.getString(request,"state")%>';
	var showInfoFlag = true;
	var datePickInitFlag = false;
	var newrow = <%= new JsonObject(newrow).toString()%>;
	calculateBgColor();

	var time  ;
	$(document).ready(function() {
		initFullCalendar(objs);
	 	 $(".fc-button-next").live("click",function(){
	 		getMoreCalendar("next");
		 })
		 $(".fc-button-prev").live("click",function(){
			 getMoreCalendar("prev");
		 })
	 	//初始化状态select
	    initStateSelected();
	 	unAssignList = $(".external-event");
		$("#ui-datepicker-div").css("display","none");
 		// initShowInfo();
		$('#external-events div.external-event').each(function() {
			var _this = $(this);
			//var context = "这个是由<span style='color:green;'>"+_this.attr("assEmployName")+"</span>安排。时间没有确定的任务<br />";
			//	context += "<span style='color:green;'>任务概述 :</span><em>"+ _this.attr("title")+"</em>";
				 /*
				 //toolTip 的初始化	
			_this.poshytip({
				className: 'tip-yellow',
				showTimeout: 1,
				alignTo: 'target',
				offsetY: 15,
				alignX: 'right',
				alignY: 'center',
				allowTipHover: false,
				fade: true,
				slide: true,
				content:function(updateCallback) {
						window.setTimeout(function() {
							updateCallback(context);
						}, 1000);
						return 'Loading...';
				}	
			});
			
			*/
		 
			var eventObject = {
					id:_this.attr("id"),
					title: _this.attr("title"),
					isAllDay:_this.attr("is_all_day") === "1"?true:false,
					editable:_this.attr("is_update") === "1"?true:false,	
					assign_user_id:_this.attr("assign_user_id"),
					schedule_execute_id:_this.attr("schedule_execute_id"),
					is_need_replay:_this.attr("is_need_replay"),
					schedule_state:_this.attr("schedule_state"),
					schedule_state:0,
					associate_id:_this.attr("associate_id")	
					
				};
			 
			/*
				1.登录人是该任务的执行人
				2.登录人是改任务执行人的主管或者是副主管
				3.登录人是总经理级别的主管或者是副主管
				4.登录人是改任务的安排人
				2,3 现在我就用只要是主管就可以来代替
				以上的这些人是有拖动权利。也就是安排权利
				6.如果是任务流程的任务是不可以让其拖动的
			*/
			    
				_this.data('eventObject', eventObject);
				var dragFlag = false; 
				var loginAdid = loginUserInfo["adid"];
				
			    if(loginAdid === eventObject.schedule_execute_id || isManager || loginAdid === eventObject.assign_user_id ){
			    	dragFlag = true;
			    	eventObject = fixEventEditable(eventObject);
				}
				if(eventObject.associate_id * 1 > 0){
					$(this).bind("click",function(){showMessage("该任务为流程任务，自动触发","alert");})
					dragFlag = false ;
				}
				if(dragFlag){
					$(this).draggable({
						zIndex: 999,
						revert: true,       
						revertDuration: 0,
						start: function(event, ui) {
							  //$(".tip-yellow").remove();
						}
					});
				}
		});
		initUserInfoDept();


		//张睿添加如果是系统提醒的点击任务的时候弹开这个任务
		if('<%= from%>' === "out"){
		    handSchedule('<%=schedule_id %>');
		}
		
	});	 
	function initStateSelected(){
		if(state.length >= 1){
		 	$("#state option[value='"+state+"']").attr("selected",true);
		}
		 
	}
	// 时间选择器chushi
	function initDatePick(target){
		 try{ 
			if(!datePickInitFlag){
				$('#st , #en').datepicker({
					dateFormat:"yy-mm-dd",
					changeMonth: true,
					changeYear: true
				});
				
				$("#"+target).blur();
				$("#ui-datepicker-div").css("z-index","20");
			}else{
				datePickInitFlag = true;
			}
		 }catch(e) {
			 showMessage("时间初始化错误","error");
		 }
	} 
	 
	// 这个方法是将后台数据装换成前台的Json数据格式的。可以是数组，也可以是 一个对象
	function initFullCalendar(objs){
		//addEvent declare_r
		var array = [];
		for(var index = 0  , count = objs.length ; index < count ; index++ ){
			var  _event = fixEvent(objs[index]);
			array.push(_event);
		}
		// joinSchedule
		if(null !=joinRows && joinRows.length > 0){
			for(var index = 0  , count = joinRows.length ; index < count ; index++ ){
				var  _event = fixEvent(joinRows[index]);
				if(_event){
					array.push(_event);
				}
			}
		}
		 
		$('#calendar').fullCalendar('addEventSource', array);
	}
	//因为在插件中没有提供很好的方法。是在往日历上拖的时候，这个时候异常了。把这个event还原到左边的那个地方。然后在日历上进行删除。
	function ifUnAssignAddFailed(_id){
		removeSchedule(_id);
		for(var i = 0 ; i < unAssignList.length ; i++ ){
			var _this = $(unAssignList.get(i));
			if(_this.attr("id") === _id){
				_this.appendTo($("#unShedule"));
				break;
			}
		}
	}
	// 将时间转化成字符串
	 function convertDateToString(date) {
		 return $.fullCalendar.formatDate(date,"yyyy-MM-dd HH:mm:ss");
	 }
 
	 
 	//根据当前的进度和设置的进度分级选择出当前背景应该用的颜色
 	function selectOneColor(value){
	   for(var index = 0 ,count = bgColor.length ; index < count ; index++ ){
		     
			if(bgColor[index]<= value){
				if(!bgColor[index+1] ||( bgColor[index+1] && bgColor[index+1] > value)){
					return bgColorSet[index];
					break;
				} 
			}
		}	
 	}
 	//根据设置的颜色的梯度计算出所有的颜色值,也就是渐变颜色的计算。首先是默认值就是开始值，然后完成值就是结束值
 	function calculateBgColor(){
		var step = scheduleSet["step"] * 1 - 1;
		var startColor  = "#"+scheduleSet["default_bg_color"];
		var endColor =  "#"+scheduleSet["complete_bg_color"];
		var Gradient = new Array(3);
		var A = color2rgb(startColor);
		var B = color2rgb(endColor);
		//计算某个梯度值
		var length =  formatFloat(10 / step,1);
		for(var index = 0 ; index <= step ; index++ ){
			for (var c = 0; c < 3; c++)  {
				Gradient[c] = A[c] + (B[c]-A[c]) / step * index;
			 } 
			var value =  0  + index * length ;
			bgColor.push(formatFloat(value,1));
			bgColorSet.push(rgb2color(Gradient));
		}
 	}
 	//计算小数点后面几位小数 src是13443.3434 pos 是几位小数如1 那么返回的结果是13443.3
 	function formatFloat(src, pos){
 	    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
 	}
 	//颜色如#FF00FF格式转为Array(255,0,255)
 	function color2rgb(color){
 		 var r = parseInt(color.substr(1, 2), 16);
 		 var g = parseInt(color.substr(3, 2), 16);
 		 var b = parseInt(color.substr(5, 2), 16);
 		 return new Array(r, g, b);
 	}

 	//颜色如Array(255,0,255)格式转为#FF00FF
 	function rgb2color(rgb){
	 	 var s = "#";
	 	 for (var i = 0; i < 3; i++){
	 	  var c = Math.round(rgb[i]).toString(16);
	 	  if (c.length == 1)
	 	   c = '0' + c;
	 	  s += c;
	 	 }
	 	 return s.toUpperCase();
 	}
 	
	
	//copy 的时候对drag和移动的时候做的处理。(主要是对背景颜色和字体的处理)
	function fixEventBackgroundColor(_event){
		 var fontColor = ""; 
		  
	 	 if(_event["schedule_is_note"]*1 == 1 ){
				if(_event["is_need_replay"]*1 == 1){
					fontColor = "#"+scheduleSet["need_replay_notice_color"];
				 				
				}else{
					fontColor = "#"+scheduleSet["need_notice_font_color"];
				}
		 }else if(_event["is_need_replay"]*1 == 1){
			   if(_event["schedule_is_note"]*1 == 1){
				   fontColor = "#"+scheduleSet["need_replay_notice_color"];	
			   }else{
				   fontColor = "#"+scheduleSet["need_replay_font_color"];
			}
		}else{
			fontColor = "#"+scheduleSet["default_font_color"];
		}
		//border颜色的设置
	 	if(_event.schedule_state * 1 < 10 &&  _event.end && timeCompare(currentTime,_event.end)){ 
  			_event.borderColor="#"+scheduleSet.delay_border_color;
		}else{
			_event.borderColor="#0033FF";
	    }
	   if(_event["schedule_state"]){
	 	  _event.backgroundColor = selectOneColor(_event["schedule_state"] * 1);
	 	  //alert("herhe ..... ");
	    }
	 	 
		 _event.textColor = fontColor;
		 return _event;
	}
	 
	//新创建一个对象。
	function ajaxCopyAddEvent(_event,revertFunc){
		var _schedule_id = _event.id;
		var _start_time =convertDateToString(_event.start);
		var _end_time =convertDateToString(_event.end);
		var isAllDay = _event.allDay ? "1":"0";
		var params = { 
				schedule_id:_schedule_id, 
				end_time:_end_time,
				start_time: _start_time,
				is_all_day:isAllDay,
				is_schedule:1
			};

	   var _data = jQuery.param(params);
	   $.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/schedule_management/CopyScheduleAction.action',
			dataType:'text',
			type:'get',
			data:_data,
			success:function(data){
				var data = eval('('+data+')');
				if(data && data.flag === "success"){
					//showMessage("安排成功");
					//这个时候把新的Id赋值上去
					_event.id = data.entityid;
					//这个时候要根据进度都是为0 (在根据是不是有通知 ,是不是有replay 确定其字体颜色)然后就是时间的安排是不是延期了。就是刚放置进去的时候。
					_event = fixEventBackgroundColor(_event);
					$('#calendar').fullCalendar('renderEvent', _event, true);
					
	 	 		}else{
	 	 			showMessage("安排失败","error");
	 	 			$("#ajaxFlag").val("error");
	 	 			revertFunc(_schedule_id);
	 	 	 	 }
			}
		})	
	}
 
	// 检查表单是不是合格
	function closeTBWin(){tb_remove();$('#calendar').fullCalendar('unselect');}
	function removeSchedule(_id){
		$('#calendar').fullCalendar( 'removeEvents',_id);
	}
	function fixNumber(num){return num < 10 ? ("0"+num) : num};
	//初始化dept 和UserInfo
	function initUserInfoDept(){
		//创建一个<ul><li></li></ul>的集合 admin.adid,admin.employe_name , admin_group.adgid , admin_group.name
		// 得到安排部门,安排人。执行部门,执行人 
		 
		var assginDept = '<%=StringUtil.getString(request,"assigndeptno")%>' ,
			assginUserinfo = '<%=StringUtil.getString(request,"assign_user_id") %>',
			executeDept = '<%=StringUtil.getString(request,"executedeptno")%>' ,
			executeUserinfo = '<%=userInfo.getString("adid") %>';
	 
		for(var index = 0 , count =  userInfoDept.length ; index < count ; index++ ){
			if(assginDept.length < 1 && userInfoDept[index]["adid"] * 1 == assginUserinfo * 1){
				assginDept = userInfoDept[index]["adgid"];
			}
		 	if(executeDept.length < 1 &&  userInfoDept[index]["adid"] * 1 == executeUserinfo * 1){executeDept = userInfoDept[index]["adgid"];}
			var flag = true;
			for(var j = 0 , i = newa.length ; j < i ; j++ ){
				if(newa[j] && (newa[j].deptId === userInfoDept[index].adgid)){
					flag = false;
					break;
				}
			}
			if(flag){ //new dept
				var objs = new Object();
				objs.deptName = userInfoDept[index].name;
				objs.deptId = userInfoDept[index].adgid;
				objs.values = [];
				objs.values.push(userInfoDept[index]); 
				newa.push(objs);
			} else{
				newa[newa.length - 1].values.push(userInfoDept[index]);	
			}
		}
	 
		
		// assginDept assginUserinfo
		//	<!-- <input type="text" name="assign_user_id" id="assign_user_id" value='<%=StringUtil.getString(request,"assign_user_id") %>'/>  -->
		//	<!--  <input type="text" name="execute_user_id" id="execute_user_id" value='<%=userInfo.getString("adid") %>'/>-->
		 var  options = "";
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			 options += "<option  value='"+newa[index].deptId+"'>"+newa[index].deptName+"</option>";
		 }
		 var  assOptions = $("<option value='' selected>任意</option>"+options);
		 
		 assOptions.each(function(){
			var _this = $(this);
			if(_this.attr("value")*1 == assginDept){
				_this.attr("selected",true);
			}
		 })
		var  execOptions = $("<option value='' selected>任意</option>"+options);
		 execOptions.each(function(){
			var _this = $(this);
			if(_this.attr("value")*1 == executeDept){
				_this.attr("selected",true);
			}
		 })
	 	 showUserInfo(assginDept,assginUserinfo,"assign_user_id");
		 showUserInfo(executeDept,executeUserinfo,"execute_user_id");
		 
		 $("#assignDeptNo").append(assOptions).chosen({no_results_text: "没有该部门:"}).live("change",function(){
			 showUserInfo($(this).val(),"","assign_user_id"); 
	 	 }) ;
	 
	 	 $("#executeDeptNo").append(execOptions).chosen({no_results_text: "没有该部门:"}).live("change",function(){
	 		 showUserInfo($(this).val(),"","execute_user_id"); 	
		 }) ;
	 	$("#assignDeptNo_chzn,#assign_user_id_chzn,#executeDeptNo_chzn,#execute_user_id_chzn").width(100);
	  	$(".chzn-drop").width(98);
	  	$(".chzn-search input").width(63);
	}
	 
	// 这个方法应该是显示的方法 参数 target , 选中userid
	function showUserInfo(deptId , userid , target ){
		var v = "<select data-placeholder='ch' style='width:100px;'  name='"+target+"' class='chzn-select' id='"+target+"'><option value='' selected>任意</option>";
		if(deptId.length > 0){
			var where = 0;
			for(var index = 0  , count = newa.length ; index < count ; index++ ){
				if(deptId*1 == newa[index].deptId * 1){
					where = index;
					break;	
				}
			}
			 
		 
			var values = newa[where].values;
			for(var index = 0 , count = values.length ; index < count ; index++ ){
				var selectedFlag = "";
				if(userid && userid.length > 1 && userid * 1 == values[index].adid ){
					selectedFlag = "selected";
				}
				v += "<option "+selectedFlag+" value='"+values[index].adid+"'>"+values[index].employe_name+"</option>";
			}
		}
		v += "</select>";
	 
		
		$("#"+target+"_div").html("");
		$("#"+target+"_div").append(v);
		$("#"+target).append(v).chosen({no_results_text: "没有该人:"});
	 
	}
	// 初始化一个Dept select
	function initDeptSelect(targetDiv , deptId ,userId,userSelected ){
		
		$("#"+targetDiv).html("");
		var selected = " <select data-placeholder='选择任务部门' style='width:100px;'  name='"+targetDiv.toLowerCase()+"no' class='chzn-select' id='"+targetDiv+"No"+"'>";
		
		var options = "<option value=''></option><option value='' selected>任意</option>";
		 
		 for(var index = 0 , count = newa.length ; index < count ; index++ ){
			 var flag = newa[index].deptId * 1 == deptId * 1 ?"selected":"";
			 options += "<option "+flag+"  value='"+newa[index].deptId+"'>"+newa[index].deptName+"</option>";
		 }
		 selected += options + "</select>";
		 var $selected = $(selected);
		 $("#"+targetDiv).append(selected);
		 
		 $("#"+targetDiv+"No").chosen({no_results_text: "没有该部门:"});
			
		 showUserInfo(deptId,userId,userSelected)
	}
	//当鼠标放在title上的时候显示信息 . infoCache页面缓存
	// 根据里面存放的Id值去数据库中查询最新的信息
	
 
	function searchByDetail(){
		var schedule_detail = $("#schedule_detail").val();
		if($.trim(schedule_detail).length < 1 || $.trim(schedule_detail) === "*任务描述"){
			showMessage("请先输入任务表述","alert");
			return ;
		}
		$("#searchForm").submit();
		
	}
	//stateBox 信息提示框
	function showMessage(_content,_state){
		var o =  {
			state:_state || "succeed" ,
			content:_content,
			corner: true
		 };
	 
		 var  _self = $("body"),
		_stateBox =  $("<div style='font-size:14px;'>").addClass("ui-stateBox").html(o.content);
		_self.append(_stateBox);	
		 
		if(o.corner){
			_stateBox.addClass("ui-corner-all");
		}
		if(o.state === "succeed"){
			_stateBox.addClass("ui-stateBox-succeed");
			setTimeout(removeBox,1500);
		}else if(o.state === "alert"){
			_stateBox.addClass("ui-stateBox-alert");
			setTimeout(removeBox,2000);
		}else if(o.state === "error"){
			_stateBox.addClass("ui-stateBox-error");
			setTimeout(removeBox,2800);
		}
		_stateBox.fadeIn("fast");
		function removeBox(){
			_stateBox.fadeOut("fast").remove();
	 }
	}
	// 如果前面的大于后面的就是返回 true 否则就是返回false
	function timeCompare(before,after){
	 
		before = before.replace(/-/gi,"/");
		if(typeof after ==="object"){
		}else{
			after = after.replace(/-/gi,"/");
			after = new Date(after).getTime();
		};
		
		
		before = new Date(before).getTime();
		var flag = before > after?true:false;
 
		return flag;
	}
	function addEventNotRefresh(_event){
		_event = fixEventBackgroundColor(_event);
		_event = fixEventEditable(_event);
		$('#calendar').fullCalendar('renderEvent', _event, true);
		$('#calendar').fullCalendar('unselect');
	}
	function removeEventNotRefresh(_id){
		$('#calendar').fullCalendar('removeEvents',_id);
	}
	function addRenderEvent(_event){
		_event = fixEvent(_event);
		$('#calendar').fullCalendar('addEventSource', _event);
	}
	//fixEvent editable
	// 下面是一些是否可以更改时间的判断。如果当前登录人是部门经理,总经理,或者安排人是自己。或者设置是自由动的那么就是可以自由移动(你是任务的执行人)。如果是
	function fixEventEditable(_event){
		var editableFlag = false;
	 	/*
	 		1.安排人就是登录人
	 		2.安排人就是执行人
	 		3.安排人允许你自由调整(你就是任务执行人 && 任务可以自由调整)
	 	*/
	 	var isSelf =  (_event.assign_user_id === loginUserInfo["adid"]) ? true:false;
	 	var isSelfAssign  = (_event.assign_user_id === _event.schedule_execute_id) ? true:false;
	 	
	 	//?目前是没有办法判断当前登录人 是不是任务执行人主管 所以都统一用主管来代替
      // alert( "isSelf :" + isSelf +"\n" + "isSelfAssign : " + isSelfAssign + "\n" + "isManager : " +isManager  + "\n" +_event.assign_user_id + "\n" + _event.schedule_execute_id);
	    if(isSelf ||  isManager || (_event.schedule_execute_id ===loginUserInfo["adid"] && _event.is_update === "1")){
	    	editableFlag = true;
		}else{
			editableFlag = false;
		}
		 _event.editable = editableFlag;
		 return _event;
	}
	
	//declare_r 添加的时候
	function fixEvent(_event){
	 	if(!_event) return null;
		 _event.id = _event.schedule_id,
		 _event.title = (_event.schedule_overview && _event.schedule_overview.length > 0)? _event.schedule_overview :"没有任务概述";
		  
		 if(_event.start_time){
			 
			 _event.start = $.fullCalendar.parseISO8601(_event.start_time+"");
		 }
		 if(_event.end_time){
			 _event.end =  $.fullCalendar.parseISO8601(_event.end_time+"");
		 }
		 _event.sub_id = _event.schedule_sub_id;
		 _event.allDay = (_event.is_all_day && _event.is_all_day ==="1") ? true:false;
		 
		_event = fixEventEditable(_event);
		 //样式的一些判断
		 _event=  fixEventBackgroundColor(_event);
		 return _event;
	}
	
  
    function closeInfo(flag){
        if(flag){
    		showInfoFlag = false;
        }
        var info = $(".info");
        info.fadeOut("slow");
    }
    function openInfo(_event){
    	   if(!showInfoFlag){
        	   return ;
    	   }
            
	    	var info = $(".info");
	    	$("#show_assName").html(_event.assemployname);
	    	$("#show_exeName").html(_event.exeemployname);
	    	$("#show_startTime").html(convertDateToString(_event.start));
	    	$("#show_endTime").html(convertDateToString(_event.end));
	    	$("#schedule_overview").html("&nbsp;&nbsp;"+ _event.title);
	    	$("#info_state").html(_event.schedule_state * 10 + "%");
	    	info.fadeIn("slow");
 
    }
    //当用户去点击next 和 prev的时候去异步的获取新的Calendar
    /*
    	1.这个时候要根据当前已经查询出来的时间的范围去计算新的更新的时间范围
    	2.如果这个时间范围是在已经有的时间范围内的话。就不用去查询了。
    	3.如果不是的那么就要用异步的方式去查询。
    	4.这个时候要考虑两种情况 如果是向前的话和向后的的不同 主要是时间的计算是不同的。
    	5.要注意的是在高级查询中才会有这个函数的执行。如果根据like出来的是没有的
    	6.要注意查询的条件的保存,比如什么时候是部门。什么时候根据人 来查询的。
    	7.用AddEvents的方法来添加新的
    */
 
    function getMoreCalendar(flag){
         // 判断是不是要更新新的 Calendar
		 var cmdFlag = '<%= cmd%>';
		 if(cmdFlag === "search"){
			return ;
		 }
		 // 获取查询的条件
		 if(!time){
			 time = {
				end:$.fullCalendar.parseISO8601(newrow.end),
				start:$.fullCalendar.parseISO8601(newrow.start)	 
			 }	
		 }
		 
		  
		// 然后进行一个时间的判断是不是在已经查出来的时间范围以内,如果是的话。就不要去查询了。 
		// 如果不是的话那么就要 计算出新的查询时间段
		 var view = $('#calendar').fullCalendar('getView');
		 // 向前比较时间
		 if(flag === "prev"){
			if(view.start.getTime() < time.start.getTime() ){
				newrow.start = convertDateToString(view.start);
				newrow.end = convertDateToString(time.start);
				newrow.direction = "prev";
				ajaxGetCalendar(newrow,"prev");
			}else{
				return ;
			}
		 }
		if(flag === "next"){
			if(view.end.getTime() - time.end.getTime() > 10000 ){
				newrow.start = convertDateToString(time.end);
				newrow.end = convertDateToString(view.end);
				newrow.direction = "next";
				ajaxGetCalendar(newrow,"next");
			}else{
				return ;
			}
		}
    }
   
    //异步的获取要跟新的时间区间的日历,在更新成功过后要把新的时间的区间加上去。同时要查询Join表和schedule_sub表
    function ajaxGetCalendar(obj,flag){
        //
        $.ajax({
        	url:'<%=ajaxGetSchedule %>',
        	dataType:'json',
        	data:obj,
        	beforeSend:function(XMLHttpRequest){
            	$(".cssDivschedule").css("display","block");
           	},
        	success:function(data){
           		$(".cssDivschedule").fadeOut("slow");
            	if(data && data.flag === "success"){
                	
					// 加载成功  吧时间更新到新的区间
					var join = data.join;
					var sub = data.sub;
            		var array = [];
            		for(var index = 0  , count = join.length ; index < count ; index++ ){
            			var  _event = fixEvent(join[index]);
            			array.push(_event);
            		}
            		if(null !=sub && sub.length > 0){
            			for(var index = 0  , count = sub.length ; index < count ; index++ ){
            				var  _event = fixEvent(sub[index]);
            				if(_event){
            					array.push(_event);
            				}
            			}
            		}
            	 
            		if(flag === "next"){
            			time.end = $.fullCalendar.parseISO8601(obj.end);
            			$("#en").val(obj.end);
                	}
            		if(flag === "prev"){
            			time.start = $.fullCalendar.parseISO8601(obj.start);
            			$("#st").val(obj.start);
                	} 
            		$('#calendar').fullCalendar('addEventSource', array);
                }else{
                    // 加载失败
                	showMessage("加载失败","error");
                }
            }
        })
    }
    function addScheduleByArtDialog(){
     
    	var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/schedule_management/add_schedule.html"; 
 		 $.artDialog.open(uri , {title: '任务快速添加',width:'620px',height:'500px', lock: true,opacity: 0.3,fixed: true});
    }
    function refreshReload(){
		window.location.reload();
   	}
</script>
</head>
<body>
 <div class="cssDivschedule">
 		<div class="ui-corner-all innerDiv">
 			正在加载数据中.......
 		</div>
 </div>
 <div id="tabs">
		 <ul>
			 <li><a href="#usual_tools">常用工具</a></li>
			 <li><a href="#advan_search">高级搜索</a></li>		 
		 </ul>
		 <div id="usual_tools">
		 	  <form method="post" action="show_schedule.html" id="searchForm">
					<input type="hidden" id="cmd" name="cmd" value="search"/>
			          <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
			            <tr>
			            <td width="16%" align="right" style="padding-top:3px;" nowrap="nowrap" >任务描述:</td>
			              <td width="13%" align="left" style="padding-top:3px;padding-left:5px">
						 <input type="text"   size="40"   id="schedule_detail" name="schedule_detail"   value='<%=StringUtil.getString(request,"schedule_detail") %>'/>
						 	
						</td>
			              <td width="50%" align="left" style="padding-top:3px;">	
			             	&nbsp; <input type="button" onclick="searchByDetail()" class="button_long_search" value="搜    索" /> 
			             	&nbsp; <input type="button" onclick="addScheduleByArtDialog();" class="long-button-add" id="btnSubmit" name="btnSubmit" value="新    增" /> 
			              </td>
			              <td width="18%" align="right" valign="middle">&nbsp;			  </td>
			            </tr>
			          </table>
			  </form> 
		  </div>
	 
		 <div id="advan_search">
			<form name="filterForm" action="show_schedule.html" method="post">
				 
				<input type="hidden" id="cmd" name="cmd" value="filter"/>
				   <table width="100%" height="30" border="0" cellpadding="1" cellspacing="0">
					    <tr>
					      <td>
					      	<span class="filterClick" onclick="initDeptSelect('assignDept','<%=adminLoggerBean.getAdgid() %>','<%=adminLoggerBean.getAdid() %>','assign_user_id')" deptId='' userId='' style="cursor:pointer;">安排人:</span>
					      </td>
					     <td style="text-align:left;width:202px;">
					     <div style="float:left;width:100px;" id="assignDept">
					      	 <select data-placeholder='选择任务部门' style='width:100px;' name="assigndeptno" class='chzn-select' id="assignDeptNo">
		 						 	<option value=''></option>
		 					 </select>
					    </div>
					      	<!-- 默认是当前登录人的信息 -->
					      	<!-- <input type="text" name="assign_user_id" id="assign_user_id" value='<%=StringUtil.getString(request,"assign_user_id") %>'/>  -->
					      	<!--  <input type="text" name="execute_user_id" id="execute_user_id" value='<%=userInfo.getString("adid") %>'/>-->
					      	<div id="assign_user_id_div" style="float:left;width:100px;">
					      		
					      	</div>
					      </td>
					      <td>
					      	 <span class="filterClick" onclick="initDeptSelect('executeDept','<%=adminLoggerBean.getAdgid() %>','<%=adminLoggerBean.getAdid() %>','execute_user_id')" deptId='' userId='' style="cursor:pointer;">执行人:</span>
					      </td>
					      <td style="text-align:left;width:202px;">
						       <div style="float:left;width:100px;" id="executeDept">
							      	<select data-placeholder='选择任务部门' style='width:100px;'  name="executedeptno" class='chzn-select' id="executeDeptNo">
					 						 	<option value=''></option>
					 				 </select>
					 			</div>
					 			<div id="execute_user_id_div" style="float:left;width:100px;">
					      		
					      		</div>
					      </td>
					     	<td>
					     		开始 :
					     	</td>
					     	<td style="text-align:left;width:95px;">
					       	 <input name="st" type="text" id="st" size="20" value="<%= (null != st && st.length() > 1)?st:first %>"  style="border:1px #CCCCCC solid;width:78px;" onclick="initDatePick('st')"/>
					        &nbsp; 
					        </td>
					        <td>
					     		     结束:
					        </td>
					        <td style="text-align:left;width:95px;" align="left">
					        <input name="en" type="text" id="en" size="20" value="<%=(null != en && en.length() > 1)?en:end %>"  style="border:1px #CCCCCC solid;width:78px;" onclick="initDatePick('en')"/>
					        &nbsp;</td>
					     	   <td>状态：
							      <select   id="state" name="state" style="border:1px #CCCCCC solid">
							        <option value="0">全部</option>
							        <option value="1">完成</option>
							        <option value="-1">未完成</option>
							      </select>
							   </td>
					      <td style="text-align:left;"><input type="submit" id="seachByCondition" name="seachByCondition" class="button_long_refresh" value="过    滤" /></td>
					      </tr>
					</table>
			</form>
		 </div>	 
 </div>
 <!--初始化tabs -->
 <script type="text/javascript">
 	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
 </script>
 	<div id='wrap'>
		<div id ='external-events'>
			<h4 style="font-weight:normal;font-size:14px;"><span style="color:green;font-weight:bold;"><%=userInfo.getString("employe_name") %></span>待安排任务</h4>
			<!--  <p style="margin:0px;padding:0px;"><span style="color:#3366CC;">蓝色</span>表示别人给自己安排,<span style="color:#669933;">绿色</span>表示自己给自己安排</p>-->
			<div id="unShedule">
				<%  
					//在这个里面做判断 如果是自己给自己安排的话,将任务的背景色改成其他的颜色。
					if(null != unAssign && unAssign.length > 0){
						int index = 0 ;
						for(DBRow row : unAssign){
							index++;
							StringBuffer s = null;
							if(row.getString("assign_user_id").equals(""+userInfo.getString("adid"))){
								s = new StringBuffer("<div class='external-event self' id='");
							}else{
								s = new StringBuffer("<div class='external-event' id='");
							}
							  
							 s.append(row.getString("schedule_id")).append("' start='").append(row.getString("start_time"))
							.append("' end='").append(row.getString("end_time")).append(" 'schedule_is_note='").append(row.getString("schedule_is_note")).append(" 'schedule_state='").append(row.getString("schedule_state"))
							.append("' is_need_replay='").append(row.getString("is_need_replay")).append(" 'schedule_execute_id='").append(row.getString("schedule_execute_id")).append("' is_update='")
							.append(row.getString("is_update")).append("' assign_user_id='").append(row.getString("assign_user_id")).append("' is_all_day='").append(row.getString("is_all_day"))
							.append("' assEmployName='").append(row.getString("assemployname")).append("' exeEmployName='").append(row.getString("exeemployname")).append("' title='")
							.append(row.getString("schedule_overview")).append("' associate_id='").append(row.get("associate_id",0l)).append("' >").append((index)+"."+row.getString("schedule_overview")).append("</div>");
		
							 out.println(s.toString());
						}
					}else{
						out.println("<span>没有未安排任务</span>");
					}
				%>
			</div>
			
				
			 <p>
				<input type='checkbox' id='drop-remove' checked="true"/> <label for='drop-remove'>安排后移除该任务</label>
			</p>
		</div>
	
		<div id='calendar'></div>
	 
		<script type="text/javascript">
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				weekMode:'variable', 
				droppable: true,
				year:YMD[0]*1,
				month:YMD[1]*1 - 1 ,
				date:YMD[2]*1,
				events: [],
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {
					closeInfo();
					 var excute_user_id = '<%=userInfo.getString("adid") %>';
					 var assign_user_id = '<%=adid%>';
					 var fixEnd = convertDateToString(end);
					 if(allDay){
						 fixEnd = fixEnd.split(/\s+/)[0] + " 23:59:59";
				     }
				      
				//	 var uri = "quick_add_schedule.html?excute_user_id="+excute_user_id+"&assign_user_id="+assign_user_id+"&start="+convertDateToString(start)+"&end="+fixEnd+"&allDay="+allDay+"&TB_iframe=true&height=360&width=550";
				//	 tb_show('任务快速添加',uri,false);
					 //换成art
					 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/schedule_management/quick_add_schedule.html"; 
					 uri += "?excute_user_id="+excute_user_id+"&assign_user_id="+assign_user_id+"&start="+convertDateToString(start)+"&end="+fixEnd+"&allDay="+allDay;
					 $.artDialog.open(uri , {title: '任务快速添加',width:'600px',height:'400px', lock: true,opacity: 0.3,fixed: true});
				},
				drop:function(date,allDay,jsEvent,ui){
					var originalEventObject = $(this).data('eventObject');
					var copiedEventObject = $.extend({}, originalEventObject);
					copiedEventObject.start = date;
					copiedEventObject.allDay = allDay;
					 if(allDay){
						  //如果是全天的任务就要设置今天的开始时间和今天的结束时间
						  var start = (convertDateToString(date));
						  var end = $.fullCalendar.parseISO8601(start.split(/\s+/)[0] + " 23:59:59");
						  copiedEventObject.start = date;
						  copiedEventObject.end = end;
					 }else{
						 var mill = date.getTime();
						 var endSeconds = 2*3600*1000 + mill;
						 copiedEventObject.end = new Date(endSeconds);
					 }
				    // 如果是allDay 表示的是没有再week 和day的视图下这个时候结束的时间是开始的后两个小时
				    // 如果是allDay 表示的是，那么就是全天的视图方式。
				    // 如果是在添加的时候失败了。就把所有的东西都还原。
				    // 如果下面第√上了的。那么我们就是添加一个新的，原来的那个任务还是在那里。
					if ($('#drop-remove').is(':checked')) {	
						$(this).remove();
						// 修改一个任务。
						ajaxUpdateEvent(copiedEventObject,ifUnAssignAddFailed,true);
					}else{
						// 新添加一个任务。
						alert("copy 任务去掉了");
						//ajaxCopyAddEvent(copiedEventObject,ifUnAssignAddFailed);
					}
				
				},
				//这个可能是做 点击的时候将一些具体的信息显示在上面
				eventClick: function(calEvent, jsEvent, view) {
					// alert(calEvent.id);
					closeInfo();
					handSchedule(calEvent.id);
			    },
			    eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
			    	 
					 ajaxUpdateEvent(event,revertFunc); 
				  },
			     eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
					closeInfo();
					ajaxUpdateEvent(event,revertFunc);
				  }
			});
			function handSchedule(id){
			    var userInfoId = '<%= userInfo.getString("adid")%>';
				  
				
				 //tb_show('任务操作',uri,false); 改成art
				 var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/schedule_management/handle_schedule.html"; 
				 uri += "?id="+id+"&userInfoId="+userInfoId+"&end="+newrow.end
				 $.artDialog.open(uri , {title: '任务操作',width:'750px',height:'640px', lock: true,opacity: 0.3,fixed: true});
			}
		</script>
		<script type="text/javascript">
			try{
				var span = "<span style='float:left;font-weight:bold;display:block;font-size:16px;'>"+"<%= userInfo.getString("employe_name")%>"+"</span>";
				// 日历上显示名字
		 	 	$(".fc-header-title h2").attr("style","float:left;clear:none;font-size:16px;margin-left:10px;");
		 	 	var html = $(".fc-header-title").html();  
		 	 	$(".fc-header-title").html(span+ html);
			}catch(error){
	
			}
		</script>
		<div style='clear:both'></div>
	</div>
 	<script type="text/javascript">
 	//如果是 全天任务的话也是要把 end 传入到后台中的。 这个方法主要是解决任务时间的长度和时间点的修改
	//然后根据新的时间去判断是不是有些颜色的变化。主要是是否是延期的改变。就是因为这个时候进度是没有发生变化的
	// flag 表示的是如果是Grag的方式来更新Event的时候，那么这个时候是要进行另外的处理
	
	function ajaxUpdateEvent(_event,revertFunc ,flag){
		var _schedule_id = _event.id;
		var _start_time =convertDateToString(_event.start);
		var _end_time =convertDateToString(_event.end);
		var isAllDay = _event.allDay ? "1":"0";
		var params = { 
				schedule_id:_schedule_id, 
				end_time:_end_time,
				start_time: _start_time,
				is_all_day:isAllDay,
				is_schedule:1
		};
	   var _data = jQuery.param(params);

		$.ajax({
			url:'<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/schedule_management/UpdateSchedule.action',
			dataType:'text',
			type:'get',
			data:_data,
			success:function(data){
 				if(data === "success"){
 					//showMessage("更新成功");
 					//根据end时间来进行判断,如果是小于 < 10 && 结束时间是小于 修改后的时间.改变 border的颜色。
			  		//到了结束的时间的时候还没有完成
			  		if(_event.schedule_state * 1 < 10 && timeCompare(currentTime,_event.end)){ 
			  			_event.borderColor="#"+scheduleSet.delay_border_color;
					}else{
						_event.borderColor="#0033FF";
				   }
					if(flag){
						_event = fixEventBackgroundColor(_event);
					}
 				   $('#calendar').fullCalendar('renderEvent', _event, true);	 
 	 	 		}else{
 	 	 			showMessage("更新失败","error");
 	 	 			revertFunc(_schedule_id);
 	 	 	 	 }
			}
		})	   
	}
	//更新Event,border , background 不刷新
	function updateEventNotRefresh(_event){
		 
		var o  = $('#calendar').fullCalendar('clientEvents',_event.id);
		 
		if(_event.schedule_state * 1 < 10 && timeCompare(currentTime,_event.end)){ 
  			_event.borderColor="#"+scheduleSet.delay_border_color;
		}else{
			_event.borderColor="#0033FF";
	    }
	 	 _event.backgroundColor = selectOneColor(_event["schedule_state"] * 1);
	 	if(o && o.length > 0 ){
		 	 o[0].borderColor = _event.borderColor;
		 	 o[0].backgroundColor = _event.backgroundColor;
		 	 o[0].schedule_state = _event.schedule_state;
	 		 $('#calendar').fullCalendar('updateEvent', o[0]);
		}
    }
    //更新包括title 开始时间 ,结束时间,是否全天,border , backgroundColor textColor,现在是没有考虑是否拖动;
    function updateEventAllInfoNotRefresh(_event){
    	var o  = $('#calendar').fullCalendar('clientEvents',_event.id);
    	if(o && o.length > 0 ){
    		 _event = fixEventBackgroundColor(_event)
    		 o[0].title = _event.title;
    		 o[0].start = _event.start;
    		 o[0].end = _event.end;
    		 o[0].textColor = _event.textColor;
		 	 o[0].borderColor = _event.borderColor;
		  
		 	 o[0].allDay = _event.allDay;
		 	 
	 		 $('#calendar').fullCalendar('updateEvent', o[0]);
		}
    }
    // 根据时间来计算repeat的任务 convertDateToString
 
    function repeatShedule(_schedule_id,flag){
	  if(!time){
			 time = {
				end:$.fullCalendar.parseISO8601(newrow.end),
				start:$.fullCalendar.parseISO8601(newrow.start)	 
			 }	
		}
		// 对于是不是要加载repeat schedule主要是看的登录人 是不是在执行人当中。是的话就去查询并显示结果,不是的话就不用了
	  var params = {schedule_id:_schedule_id, start:convertDateToString(time.start),end:convertDateToString(time.end),show:flag};
	    $.ajax({
            url:'<%=actionRepeatUri %>',
            data:jQuery.param(params),
            dataType:'json',
            success:function(data){
                if(data && data.flag === "success"){
                     var count = (data.schedule_repeat.length);
					var array = [];
					
 					for(var index = 0 ; index < count ; index++ ){
						array.push(fixEvent(data.schedule_repeat[index]));
					}
 				 
 					$('#calendar').fullCalendar('addEventSource', array);
                 }else{
                    ShowMessage("查询失败,请刷新试试","error");
               }
           	}
        })
    }
    function removeMutilNotRefresh(values){
        if(values && values.length > 0){
            for(var index = 0 , count = values.length ; index < count ; index++ ){
                	removeEventNotRefresh(values[index]["schedule_id"]);
            }
        }
    }
    //这个方法是 首先要移除日历上的那个repeat的ID的任务 在一个时间前d
    //然后在新添加任务
    function updateRepeatSchedule(values,repeat_id,start_time){
        var start = $.fullCalendar.parseDate(start_time);
        var list =  $('#calendar').fullCalendar("clientEvents");
        for(var index = 0 , count = list.length ; index < count ; index++ ){
            	var inTime = ($.fullCalendar.parseDate(list[index]["start_time"]).getTime());
             
               if(list[index].repeat_id === repeat_id &&  (inTime -  start) >= 0){
            	   removeEventNotRefresh(list[index]["schedule_id"]);
               }
        }
        // 添加新的
        var array = [];
        for(var index = 0 , count = values.length ; index < count ;index++ ){
        	array.push(fixEvent(values[index]));
       }
        $('#calendar').fullCalendar('addEventSource', array);
    }
 	</script>
</body>
</html>