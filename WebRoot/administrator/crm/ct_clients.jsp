<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css">
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}


});

function trace(title,target_type,cc_id,email)
{
	tb_show('跟进'+title+' ['+email+']',"trace.html?target_type="+target_type+"&cc_id="+cc_id+"&TB_iframe=true&height=500&width=850",false);
}

function go2(p)
{
	document.dataForm2.p2.value = p;
	document.dataForm2.submit();
}

function addSpecialClient(email,cc_id,target_type)
{
	$.prompt(
	
	"<div id='title'>特别关注["+email+"]</div><span style='color:#999999;font-weight:normal'>* 加入“特别关注”，客户需要每天跟进一次！</span><br><textarea name='proMemo' id='proMemo'  style='width:320px;height:70px;' ></textarea> ",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if (f.proMemo=="")
						  {
						  	alert("请填写备注信息");
							return(false);
						  }
						  else
						  {
						  	return(true);
						  }
					}
				}
		  ,
   		  loaded:
				function ()
				{
				}
		  , 
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.add_special_form.cc_id.value = cc_id;
						document.add_special_form.target_type.value = target_type;
						document.add_special_form.memo.value = f.proMemo;
						document.add_special_form.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}


function cancelSpecialClient(csc_id,email,cc_id,target_type)
{
	$.prompt(
	
	"<div id='title'>取消特别关注["+email+"]</div><br /><textarea name='proMemo' id='proMemo'  style='width:320px;height:70px;' ></textarea> ",
	
	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						  if (f.proMemo=="")
						  {
						  	alert("请填写备注信息");
							return(false);
						  }
						  else
						  {
						  	return(true);
						  }
					}
				}
		  ,
   		  loaded:
				function ()
				{
				}
		  , 
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.cancel_special_form.csc_id.value = csc_id;
						document.cancel_special_form.cc_id.value = cc_id;
						document.cancel_special_form.target_type.value = target_type;
						document.cancel_special_form.memo.value = f.proMemo;
						document.cancel_special_form.submit();
					}
				}
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
	
}

function closeTBWin()
{
	tb_remove();
}

function trueOrder(qoid,ps_id,sc_id,ccid,client_id,pro_id)
{

		document.record_form.qoid.value=qoid;
		document.record_form.ps_id.value=ps_id;			
		document.record_form.sc_id.value=sc_id;		
		document.record_form.ccid.value=ccid;			
		document.record_form.client_id.value=client_id;		
		document.record_form.pro_id.value=pro_id;					
		document.record_form.submit();
}

function delClient(cc_id,email)
{
	if ( confirm("确认放弃 "+email+" ？") )
	{
		document.del_client_form.cc_id.value=cc_id;				
		document.del_client_form.submit();
	}
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}


</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  >
<form name="export_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/ExportProfitQuote.action" >
<input type="hidden" name="pcid" />
<input type="hidden" name="cmd" />
<input type="hidden" name="key" />
<input type="hidden" name="union_flag" />

<input type="hidden" name="ps_id" />
<input type="hidden" name="ca_id" />
</form>

<form name="add_special_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/crm/AddSpecialClient.action" >
<input type="hidden" name="cc_id" />
<input type="hidden" name="target_type" />
<input type="hidden" name="memo" />
</form>

<form name="cancel_special_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/crm/DelSpecialClient.action" >
<input type="hidden" name="cc_id" />
<input type="hidden" name="target_type" />
<input type="hidden" name="memo" />
<input type="hidden" name="csc_id" />
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/quote/recordOrderQuote.action" method="post" name="record_form" id="record_form">
<input type="hidden" name="qoid" />
<input type="hidden" name="ps_id"/>
<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>"/>	
<input type="hidden" name="sc_id" />
<input type="hidden" name="ccid" />
<input type="hidden" name="pro_id" />
<input type="hidden" name="client_id" />
</form>

<form name="del_client_form" id="export_form" method="post" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/crm/DelCrmClient.action" >
<input type="hidden" name="cc_id" />
</form>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td align="left" class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 客户与报价应用 » 客户关系管理  </td>
  </tr>
</table>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	<div class="demo">

<div id="tabs">
	<ul>
		<li><a href="special_clients.html">特别关注顾客<span> </span></a></li>
		<li><a href="frequently_clients.html?p=<%=StringUtil.getInt(request,"p")%>&p2=<%=StringUtil.getInt(request,"p2")%>">购买频繁顾客<span> </span></a></li>
		<li><a href="large_clients.html">大宗购买顾客<span> </span></a></li>
		<li><a href="large_clients.html">高级发掘<span> </span></a></li>
	</ul>

</div>

</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
		load: function(event, ui) {onLoadInitZebraTable();}	
	});
	</script>
	
	</td>
  </tr>
</table>
<br />

</body>
</html>
