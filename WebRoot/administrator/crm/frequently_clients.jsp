<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
 	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);		

 PageCtrl pc = new PageCtrl();
 pc.setPageNo(StringUtil.getInt(request,"p"));
 pc.setPageSize(25);

 PageCtrl pc2 = new PageCtrl();
 pc2.setPageNo(StringUtil.getInt(request,"p2"));
 pc2.setPageSize(25);

 com.cwc.app.crm.ClientsIFrace frequentlyClients = (com.cwc.app.crm.ClientsIFrace)MvcUtil.getBeanFromContainer("frequentlyClients"); 
 DBRow readyClients[] = frequentlyClients.getReadyClients(adminLoggerBean.getAdid(),pc) ;
 DBRow mustTraceClients[] = frequentlyClients.getMustTraceClients(adminLoggerBean.getAdid(),pc2) ;
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>

<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>

</style>
</head>

<body >
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" style="font-size:15px;font-weight:bold">最近
      
      3
    月，购买&gt; 
      5
    次
	&nbsp;&nbsp;</td>
  </tr>
</table>

	
<br />
<br />

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="8%" style="color:#666666;font-weight:bold;font-size:15px;"><img src="../imgs/mini_win_logo.gif" width="20" height="20" align="absmiddle" /> 今日任务</td>
    <td width="92%" style="color:#666666;font-weight:bold;font-size:15px;"><hr width="100%" size="1" noshade="noshade" /></td>
  </tr>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
<thead>
  <tr>
    <th width="13%"  class="left-title" style="vertical-align: center;text-align: center;">姓 名</th>
    <th width="12%"  class="right-title" style="vertical-align: center;text-align: left;">国家-地区</th>
    <th width="15%"  class="right-title" style="vertical-align: center;text-align: left;">邮箱</th>
    <th width="11%"  class="right-title" style="vertical-align: center;text-align: center;">购买次数</th>
    <th width="16%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
    <th width="8%"  class="right-title" style="vertical-align: center;text-align: center;">跟进次数</th>
    <th width="25%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;   </th>
  </tr>
  </thead>
  <%
  for (int i=0; i<mustTraceClients.length; i++)
  {

  %>
  <tr>
    <td height="50"  align="center" valign="middle"><%=mustTraceClients[i].getString("name")%></td>
    <td  align="left" valign="middle"><%=mustTraceClients[i].getString("c_country")%> - <%=mustTraceClients[i].getString("pro_name")%></td>
    <td  align="left" valign="middle"><%=mustTraceClients[i].getString("email")%></td>
    <td  align="center" valign="middle"><%=mustTraceClients[i].get("c",0)%></td>
    <td  align="center" valign="middle">&nbsp;</td>
    <td  align="center" valign="middle"><%=mustTraceClients[i].get("trace_count",0)%></td>
    <td  align="center" valign="middle"><input type="button" name="Submit2" value="跟进" class="short-short-button"  onclick="trace('今日任务',<%=frequentlyClients.getTargetType()%>,<%=mustTraceClients[i].getString("cc_id")%>,'<%=mustTraceClients[i].getString("email")%>')"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="报价" class="short-short-button" onclick="tb_show('创建报价单','../order/quote_add.html?cmd=crm&target_type=<%=frequentlyClients.getTargetType()%>&cc_id=<%=mustTraceClients[i].getString("cc_id")%>&client_id=<%=mustTraceClients[i].getString("email")%>&ccid=<%=mustTraceClients[i].getString("ccid")%>&pro_id=<%=mustTraceClients[i].getString("pro_id")%>&TB_iframe=true&height=500&width=850',false);"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="特别关注" class="short-button-redtext" onclick="addSpecialClient('<%=mustTraceClients[i].getString("email")%>',<%=mustTraceClients[i].getString("cc_id")%>,<%=frequentlyClients.getTargetType()%>)"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="放弃" class="short-short-button-del" onclick="delClient(<%=mustTraceClients[i].getString("cc_id")%>,'<%=mustTraceClients[i].getString("email")%>')"/>
      &nbsp;&nbsp; </td>
  </tr>
  <%
  }
  %>
</table> 
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm2" method="get">
    <input type="hidden" name="p2">	
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc2.getPageNo() - 1;
int next = pc2.getPageNo() + 1;
out.println("页数：" + pc2.getPageNo() + "/" + pc2.getPageCount() + " &nbsp;&nbsp;总数：" + pc2.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go2(1)",null,pc2.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go2(" + pre + ")",null,pc2.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go2(" + next + ")",null,pc2.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go2(" + pc2.getPageCount() + ")",null,pc2.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go2(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="8%" style="color:#666666;font-weight:bold;font-size:15px;"><img src="../imgs/mini_win_logo.gif" width="20" height="20" align="absmiddle" /> 潜力客户</td>
    <td width="92%" style="color:#666666;font-weight:bold;font-size:15px;"><hr width="100%" size="1" noshade="noshade" /></td>
  </tr>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <thead>
    <tr>
      <th width="13%"  class="left-title" style="vertical-align: center;text-align: center;">姓 名</th>
      <th width="12%"  class="right-title" style="vertical-align: center;text-align: left;">国家-地区</th>
      <th width="15%"  class="right-title" style="vertical-align: center;text-align: left;">邮箱</th>
      <th width="11%"  class="right-title" style="vertical-align: center;text-align: center;">购买次数</th>
      <th width="16%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
      <th width="8%"  class="right-title" style="vertical-align: center;text-align: center;">跟进次数</th>
      <th width="25%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp; </th>
    </tr>
  </thead>
  <%
  for (int i=0; i<readyClients.length; i++)
  {

  %>
  <tr>
    <td height="50"  align="center" valign="middle"><%=readyClients[i].getString("name")%></td>
    <td  align="left" valign="middle"><%=readyClients[i].getString("c_country")%> - <%=readyClients[i].getString("pro_name")%></td>
    <td  align="left" valign="middle"><%=readyClients[i].getString("email")%></td>
    <td  align="center" valign="middle"><%=readyClients[i].get("c",0)%></td>
    <td  align="center" valign="middle">&nbsp;</td>
    <td  align="center" valign="middle"><%=readyClients[i].get("trace_count",0)%></td>
    <td  align="center" valign="middle"><input type="button" name="Submit2" value="跟进" class="short-short-button" onclick="trace('潜力客户',<%=frequentlyClients.getTargetType()%>,<%=readyClients[i].getString("cc_id")%>,'<%=readyClients[i].getString("email")%>')"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="报价" class="short-short-button" onclick="tb_show('创建报价单','../order/quote_add.html?cmd=crm&target_type=<%=frequentlyClients.getTargetType()%>&cc_id=<%=readyClients[i].getString("cc_id")%>&client_id=<%=readyClients[i].getString("email")%>&ccid=<%=readyClients[i].getString("ccid")%>&pro_id=<%=readyClients[i].getString("pro_id")%>&TB_iframe=true&height=500&width=850',false);"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="特别关注" class="short-button-redtext" onclick="addSpecialClient('<%=readyClients[i].getString("email")%>',<%=readyClients[i].getString("cc_id")%>,<%=frequentlyClients.getTargetType()%>)"/>
      &nbsp;&nbsp;
      <input type="button" name="Submit2" value="放弃" class="short-short-button-del"  onclick="delClient(<%=readyClients[i].getString("cc_id")%>,'<%=readyClients[i].getString("email")%>')"/>
      &nbsp;&nbsp; </td>
  </tr>
  <%
  }
  %>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" id="dataForm" method="get">
    <input type="hidden" name="p" />
  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
 pre = pc.getPageNo() - 1;
 next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p22" type="text" id="jump_p22" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>" />
        <input name="Submit222" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p22').value)" value="GO" />
    </td>
  </tr>
</table>
</body>
</html>

