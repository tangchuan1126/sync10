<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="crmClientKey" class="com.cwc.app.key.CrmClientKey"/>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);		

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(25);

com.cwc.app.crm.ClientsIFrace specialClients = (com.cwc.app.crm.ClientsIFrace)MvcUtil.getBeanFromContainer("specialClients"); 
DBRow mustTraceClients[] = specialClients.getMustTraceClients(adminLoggerBean.getAdid(),pc) ;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>

</style>

</head>

<body >
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <tr>
    <th width="20%"  class="left-title" style="vertical-align: center;text-align: center;">姓名</th>
    <th width="12%"  class="left-title" style="vertical-align: center;text-align: center;">国家-地区</th>
    <th width="13%"  class="left-title" style="vertical-align: center;text-align: center;">邮箱</th>
    <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">目标类型</th>
    <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;">跟进次数</th>
    <th width="35%"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
  </tr>
    <%
  for (int i=0; i<mustTraceClients.length; i++)
  {

  %>
  <tr>
    <td height="30"   align="center" valign="middle"><%=mustTraceClients[i].getString("name")%></td>
    <td   align="center" valign="middle"><%=mustTraceClients[i].getString("ccid")%>-<%=mustTraceClients[i].getString("pro_id")%></td>
    <td   align="center" valign="middle"><%=mustTraceClients[i].getString("email")%></td>
    <td   align="center" valign="middle"><%=crmClientKey.getStatusById(mustTraceClients[i].get("target_type",0))%></td>
    <td   align="center" valign="middle"><%=mustTraceClients[i].getString("trace_count")%></td>
    <td  align="center" valign="middle">
	<input type="button" name="Submit" value="跟进" class="short-short-button"   onclick="trace('特别关注',<%=mustTraceClients[i].getString("target_type")%>,<%=mustTraceClients[i].getString("cc_id")%>,'<%=mustTraceClients[i].getString("email")%>')"/>
	  &nbsp;&nbsp;
	        <input type="button" name="Submit" value="报价" class="short-short-button"/>
			&nbsp;&nbsp;
      <input type="button" name="Submit2" value="取消关注" class="short-button-redtext"  onclick="cancelSpecialClient(<%=mustTraceClients[i].getString("csc_id")%>,'<%=mustTraceClients[i].getString("email")%>',<%=mustTraceClients[i].getString("cc_id")%>,<%=mustTraceClients[i].getString("target_type")%>)"/>
	  &nbsp;&nbsp;

	        <input type="button" name="Submit" value="放弃" class="short-short-button-del"/>	</td>
  </tr>
    <%
  }
  %>
</table>
</body>
</html>

