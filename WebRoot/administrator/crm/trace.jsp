<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.key.CrmTraceTypeKey"%>
<%
int p = StringUtil.getInt(request,"p",1);

PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(10);

long target_type = StringUtil.getLong(request,"target_type");
long cc_id = StringUtil.getLong(request,"cc_id");
DBRow traces[] = crmMgr.getTraceByCcid(cc_id,pc);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<jsp:useBean id="crmClientKey" class="com.cwc.app.key.CrmClientKey"/>
<jsp:useBean id="crmTraceTypeKey" class="com.cwc.app.key.CrmTraceTypeKey"/>
<script language="javascript" src="../../common.js"></script>

<script>
function checkForm(theform)
{
	if (typeof $("input#trace_type:checked").val() == "undefined")
	{
		alert("请选择跟进类型");
		return(false);
	}
	else if ($("#memo").val()=="")
	{
		alert("请填写跟进内容");
		return(false);
	}
	else
	{
		return(true);
	}
}
</script>

<style>
</style>

</head>

<body >
<table width="100%" height="490" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
  <tr>
    <td align="left" valign="top" bgcolor="#FFFFFF" style="border:1px #CCCCCC solid;padding:5px;">
	<DIV  style="height:335px; overflow:auto; overflow-x:hidden;">
	<%
	if (traces.length>0)
	{
	%>
	  <table width="100%" border="0" cellspacing="0" cellpadding="5">
	  <%
	  for (int i=0; i<traces.length; i++)
	  {
	  %>
        <tr>
          <td width="64%" bgcolor="#eeeeee" style="color:#666666;font-weight:bold"><img src="../imgs/group_arrow.gif" width="19" height="18" align="absmiddle" /> <%=(p-1)*pc.getPageSize()+(i+1)%>.<%=crmClientKey.getStatusById(traces[i].get("target_type",0))%> - <%=crmTraceTypeKey.getStatusById(traces[i].get("trace_type",0))%></td>
          <td width="36%" align="right" bgcolor="#eeeeee" style="color:#999999;font-weight:normal;">
		  [<%=adminMgr.getDetailAdmin(traces[i].get("adid",0l)).getString("account")%>]
		  
		  [<%=traces[i].getString("trace_date")%>]
		  </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#f8f8f8"  style="border-bottom:1px #999999 dashed;line-height:20px;padding-top:5px;padding-bottom:5px;"><%=traces[i].getString("memo")%></td>
        </tr>
	  <%
	  }
	  %>
      </table>
	  <%
	  }
	  else
	  {
	  	out.println("暂无跟进记录！");
	  }
	  
	  %>
	  
	  
	</DIV>
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" id="dataForm" method="get">
    <input type="hidden" name="p" />
	<input type="hidden" name="target_type" value="<%=target_type%>"/>
	<input type="hidden" name="cc_id" value="<%=cc_id%>"/>

  </form>
  <tr>
    <td height="28" align="right" valign="middle"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p22" type="text" id="jump_p22" style="width:28px;" value="<%=p%>" />
        <input name="Submit222" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p22').value)" value="GO" />
    </td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td height="100" align="left" valign="top" bgcolor="#eeeeee" style="padding:5px;">
	<form  method="post" name="trace_form" id="trace_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/crm/Trace.action" onsubmit="return checkForm(this)">
	<input type="hidden" name="target_type" value="<%=target_type%>" >
	<input type="hidden" name="cc_id" value="<%=cc_id%>" >

      <input type="radio" name="trace_type" id="trace_type" value="<%=CrmTraceTypeKey.EMAIL%>" />

邮件

<input type="radio" name="trace_type"  id="trace_type" value="<%=CrmTraceTypeKey.PHONE%>" />

电话

<input type="radio" name="trace_type" id="trace_type"  value="<%=CrmTraceTypeKey.IM%>" />

IM<br />
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37%"><textarea name="memo" id="memo" style="width:500px;height:70px;"></textarea></td>
    <td width="63%" align="center">
      <input name="Submit" type="submit" class="normal-green-long" value="跟进" />
	  &nbsp;&nbsp;&nbsp;
      <input name="Submit2" type="button" class="normal-white" value="关闭" onclick="parent.location.reload();parent.tb_remove();"/>    </td>
  </tr>
</table>
</form>
</td>
  </tr>
</table>
</body>
</html>

