<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long repair_id = StringUtil.getLong(request,"repair_id");
	int refresh = StringUtil.getInt(request,"refresh");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script>
function uploadPurchaseDetail()
{
	 if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		if(filename[filename.length-1]=="xls")
		{
			document.upload_form.submit();
		}
		else
		{
			alert("只可上传2003版excel文件");
		}	
	}
}

function refresh()
{
	<%
		if(refresh==1)
		{
	%>
		parent.closeWin();
	<%
		}
		else
		{
	%>
		parent.closeWinNotRefresh();
	<%
		}
	%>
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="damaged_repair_detail_show.html" name="upload_form" enctype="multipart/form-data" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;"> 
						返修单号：R<%=repair_id%>
						</td>
					  </tr>
				  </table>
					<br/><br/>
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						<tr style=" padding-bottom:15px; padding-left:15px;">
							<td width="9%" align="left" valign="middle" nowrap="nowrap" class="STYLE3" >参考模板</td>
							<td width="91%" align="left" valign="middle" >								<span class="STYLE12"><a href="../return_step/repair_template.xls">下载</a></span>							</td>
						</tr>
				  	</table>
					<br/>
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						<tr  style=" padding-bottom:15px; padding-left:15px;">
						<td width="9%" align="left" valign="top" nowrap="nowrap" class="STYLE3" >上传EXCEL</td>
						<td width="91"align="left" valign="top" >
					
							<input type="file" name="file" id="file">
						 
					</td>
					  </tr>
				  </table>
				</td></tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="uploadPurchaseDetail();">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="refresh();"></td>
              </tr>
            </table>
<input type="hidden" name="repair_id" id="repair_id" value="<%=repair_id%>"/>
</form>
</body>
</html>
