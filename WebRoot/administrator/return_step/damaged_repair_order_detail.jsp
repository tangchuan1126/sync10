<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.DamagedRepairKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	long repair_id = StringUtil.getLong(request,"repair_id");
	DBRow damagedRepair = damagedRepairMgrZJ.getDetailDamagedRepair(repair_id);
	
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	
	int number = 0;
	boolean edit = false;
	
	if(damagedRepair.get("repair_status",0)==DamagedRepairKey.READY)//非准备中转运单明细不可修改
	{
		edit = true;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>返修单</title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script language="javascript" src="..//common.js"></script>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<script type="text/javascript">
	var select_iRow;
	var select_iCol;				
	
	function print()
	{
		visionariPrinter.PRINT_INIT("返修单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);//
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../damaged_repair/print_damaged_repair_order_detail.html?repair_id=<%=repair_id%>");
				
			visionariPrinter.PREVIEW();
	}

	function uploadDamagedRepairOrderDetail()
	{
		tb_show('上传转运单','damaged_repair_upload_excel.html?repair_id=<%=repair_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
				
	
	function downloadDamagedRepairOrder(repair_id)
	{
		var para = "repair_id="+repair_id;
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/downLoadDamagedRepair.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function autoComplete(obj)
	{
		obj.autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		{
			minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
		}); 
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#p_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="repair_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/getDamagedRepairDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
</script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
</style>

</head>

<body>
<div align="center">

<div align="center">

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="30%">
			<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px; width:95%">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" align="right" nowrap="nowrap"><font style="font-family: 黑体; font-size: 16px;">转运单号：</font></td>
				  <td width="70%" align="left"><font style="font-family: 黑体; font-size: 16px;"><a href="">R<%=repair_id%></a></font></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">转运仓库：</font></td>
					<td align="left"><font style="font-family:黑体; font-size:16px"><%=catalogMgr.getDetailProductStorageCatalogById(damagedRepair.get("send_psid",0l)).getString("title")%></font></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">目的仓库：</font></td>
					<td align="left"><font style="font-family:黑体; font-size:16px"><%=catalogMgr.getDetailProductStorageCatalogById(damagedRepair.get("receive_psid",0l)).getString("title")%></font></td>
				</tr>
			</table>
			</div>
		</td>
		<td width="40%">
			<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px; width:95%">
			<table style="padding-left: 15px;" width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="32%" align="right" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">货运公司：</font></td>
				  <td width="68%" align="left"><%=damagedRepair.getString("repair_waybill_name") %></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">运单号：</font></td>
					<td align="left"><%=damagedRepair.getString("repair_waybill_number") %></td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">预计到达日期：</font></td>
					<td align="left"><%=damagedRepair.getString("transport_receive_date").equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():damagedRepair.getString("transport_receive_date")%></td>
				</tr>
			</table>
			</div>
		</td>
		<td align="right"><span style="padding-top:10px;padding-bottom:10px;">
		  <%
		  	if(damagedRepair.get("repair_status",0)==DamagedRepairKey.READY)
		  	{
		  %>
		  	<input class="long-button" name="button2" type="button" value="上传" onclick="uploadDamagedRepairOrderDetail()"/>
		  <%
		  	}
		  %>
		  
&nbsp;&nbsp;
<input name="button2" type="button" value="下载" onclick="downloadDamagedRepairOrder(<%=repair_id%>)" class="long-button-export"/>
		</span>		</td>
		</tr>
	  <tr>
			<td align="left" colspan="2" style=" padding-top:10px;">
				<div align="left" style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<table style="padding-left: 15px;" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td align="right" valign="middle" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">交货地址：</font></td>
							<td align="left" valign="middle"><%=damagedRepair.getString("repair_address") %></td>
						</tr>
						<tr>
							<td valign="middle" align="right"><font style="font-family:黑体; font-size:16px">联系人：</font></td>
							<td align="left" valign="middle"><%=damagedRepair.getString("repair_linkman") %></td>
						</tr>
						<tr>
							<td valign="middle" align="right"><font style="font-family:黑体; font-size:16px">联系电话：</font></td>
							<td><%=damagedRepair.getString("repair_linkman_phone") %></td>
						</tr>
					</table>
				</div>
				<br/>
			</td>
			<td align="center">
				<table width="100%" cellpadding="0" border="0" cellspacing="0">
					<tr>
						<td align="right" nowrap="nowrap">
							 <% 
								if(damagedRepair.get("repair_status",0)==DamagedRepairKey.READY||damagedRepair.get("repair_status",0)==DamagedRepairKey.PACKING)
								{
							  %>
								<input name="button" class="long-button-mod" type="button" value="修改" onclick="modDamagedRepair()"/>&nbsp;&nbsp;
							  <%
								}
							  %>
							  	<input name="button" type="button" value="打印" class="long-button-print" onclick="print()"/>
							<% 
								if(damagedRepair.get("repair_status",0)==DamagedRepairKey.READY)
								{
							%>
					&nbsp;&nbsp;<input name="button" class="long-button" type="button" value="装箱" onclick="readyPacking(<%=repair_id%>)"/>
							<% 
								}
							%>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="right" nowrap="nowrap">
							 <% 
								if(damagedRepair.get("repair_status",0)==DamagedRepairKey.INTRANSIT)
								{
							  %>
								<!-- input name="button" class="long-button-upload" type="button" value="入库" onclick="inComingWareHouse()"/>&nbsp;&nbsp;-->
							  <%
								} 
							  %>
							  <% 
								if(damagedRepair.get("repair_status",0)==DamagedRepairKey.INTRANSIT)
								{
							  %>
								<input name="button" type="button" value="申请完成" class="long-button" onclick="applicationApprove(<%=repair_id%>,'<%=damagedRepair.getString("transport_number")%>')"/>
							  <%
								}
							  %>
						</td>
					</tr>
				</table>
				</td>
	  </tr>
	</tr>
	
</table>
</div>
<br/><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					sortable:true,
					url:'dataDamagedRepairDetail.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:255,
					autowidth: false,
					shrinkToFit:true,
					postData:{repair_id:<%=repair_id%>},
					jsonReader:{
				   			id:'repair_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['repair_detail_id','商品名','商品条码','转运量','装箱数量','到达数量','所在箱号','repair_id','repair_pc_id'], 
				   	colModel:[ 
				   		{name:'repair_detail_id',index:'transport_detail_id',hidden:true,sortable:false},
				   		{name:'p_name',index:'p_name',editable:<%=edit%>,align:'left',editrules:{required:true}},
				   		{name:'p_code',index:'p_code',align:'left'},
				   		{name:'repair_count',index:'repair_count',editrules:{required:true,number:true,minValue:0.5},editable:<%=edit%>,sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'repair_send_count',index:'repair_send_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'repair_reap_count',index:'repair_reap_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},sortable:false,align:'center'},
				   		{name:'repair_box',index:'repair_box',editable:<%=edit%>},
				   		{name:'repair_id',index:'repair_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=repair_id%>},hidden:true,sortable:false},
				   		{name:'repair_pc_id',index:'repair_pc_id',hidden:true,sortable:false},	
				   		], 
				   	rowNum:-1,//-1显示全部
				   	pgtext:false,
				   	pgbuttons:false,
				   	mtype: "GET",
				   	gridview: true, 
					rownumbers:true,
				   	pager:'#pager2',
				   	sortname: 'repair_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',

				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='p_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_p_name","#gridtest"));
				   					}
				   					select_iRow = iRow;
				   					select_iCol = iCol;
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	if(name=='p_name') 
				   					{
				   						ajaxModProductName(jQuery("#gridtest"),rowid);
				   				  	}
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/gridEditDamagedRepairDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/gridEditDamagedRepairDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}}); 
	</script>
		</td>
	</tr>
</table>
</div>
<form name="download_form" method="post"></form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="repair_id"/>
</form>

<script type="text/javascript">
	$("#eta").date_input();
	
	function modDamagedRepair()
	{
		tb_show('修改返修单','mod_damaged_repair_order.html?repair_id=<%=repair_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function readyPacking(repair_id)
	{
		tb_show('残损返修缺货商品','damaged_repair_storage_show.html?repair_id='+repair_id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function applicationApprove(repair_id)
	{
		if(confirm("确定申请T"+repair_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/addTransportApprove.action";
			document.applicationApprove_form.repair_id.value = repair_id;
			document.applicationApprove_form.submit();
		}
	}
</script>
</body>
</html>

