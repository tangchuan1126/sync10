<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderException"%>
<%@page import="com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderDetailsException"%>
<%@ include file="../../include.jsp"%> 
<%
	long repair_id = StringUtil.getLong(request,"repair_id");
	
	boolean submit = true;
	boolean existDetail = true;
	String errorMessage = "";
	DBRow[] rows = null;
	try
	{
		rows = damagedRepairMgrZJ.checkDamagedRepairStorage(repair_id);
	}
	catch (NoExistDamagedRepairOrderException e)
	{
		existDetail = false;
		errorMessage = "返修单不存在";
	}
	catch(NoExistDamagedRepairOrderDetailsException e)
	{
		existDetail = false;
		errorMessage = "返修单无明细，无法装箱";
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;" class="zebraTable">
		  <% 
		  	if(rows!=null&&rows.length>0)
		  	{
		  		submit = false;
		  %>
		  	
		  	 <tr>
				<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
				<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">转运数量</th>
			 </tr>
			  <%
			  	for(int i=0;i<rows.length;i++)
			  	{
			  %>
			  	<tr>
			  		<td height="40"><%=rows[i].getString("repair_p_name")%></td>
			  		<td align="center"><%=rows[i].get("repair_count",0f)%></td>
			  	</tr>
			  <%	
			  	}
			  %>
	  		
		  <%
		  	}
		  	else
		  	{
		  		if(existDetail)
		  		{
		  	%>
		  		<tr align="center" valign="middle">
					<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;">可以从该仓库返修转运发货</th>
				</tr>
		  	<%
		  		}
		  		else
		  		{
		  	%>
		  		<tr align="center" valign="middle">
					<th height="100%" align="center" style="padding-left:10px;font-size:xx-large;background-color: white;"><font color="red"><%=errorMessage%></font></th>
				</tr>
		  	<%
		  		}
		  %>
		  	
		  <%
		  	}
		  %>
		  </table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom">
 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">&nbsp;
				</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
 					<%
 						if(submit&&existDetail)
 						{
 					%>
 					<input type="button" name="Submit2" value="确定" class="normal-green" onClick="packingDamagedRepair(<%=repair_id%>)"/>
 					<%
 						}
 					%>
			      	
				  	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action="" name="">

</form>
<script type="text/javascript">
	function packingDamagedRepair(repair_id)
	{

		$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
		
		var para = "repair_id="+repair_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/packingDamagedRepair.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						$.unblockUI();
						parent.closeWin();
					}
					else
					{
						$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">系统异常，请重试</span>' });
					}
					
				}
			});						
	}
</script>

</body>
</html>



