<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.DamagedRepairKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	String key = StringUtil.getString(request,"key");
	
	long send_psid = StringUtil.getLong(request,"send_psid",0);
	long receive_psid = StringUtil.getLong(request,"receive_psid",0);
	int status = StringUtil.getInt(request,"status",0);
	
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	String cmd = StringUtil.getString(request,"cmd");
	
	DBRow[] rows; 
	if(cmd!=null)
	{
		if(cmd.equals("filter"))
		{
			rows = damagedRepairMgrZJ.filterDamagedRepair(send_psid,receive_psid,status,pc);
		}
		else if(cmd.equals("search"))
		{
			rows = damagedRepairMgrZJ.searchDamagedRepairByNumber(key,pc);
		}
		else
		{
			rows = damagedRepairMgrZJ.filterDamagedRepair(send_psid,receive_psid,status,pc);
		}
	}
	else
	{
		rows = damagedRepairMgrZJ.filterDamagedRepair(send_psid,receive_psid,status,pc);
	}
	
	DamagedRepairKey damagedRepairKey = new DamagedRepairKey();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<script language="javascript" src="../../common.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/default/easyui.css"/>
<link rel="stylesheet" type="text/css" href="../js/easyui/themes_visionari/icon.css"/>


<script type="text/javascript" src="../js/select.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<script src="../js/jgrowl/jquery.jgrowl.js"></script>
<link rel="stylesheet" type="text/css" href="../js/jgrowl/jquery.jgrowl.css"/>
<link type="text/css" href="../js/mcdropdown/css/jquery.order.mcdropdown.css" rel="stylesheet"  />


<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

.input-line
{
	width:200px;
	font-size:12px;

}

body
{
	font-size:12px;
}
.STYLE3 {font-family: Arial, Helvetica, sans-serif}

.aaaaaa
{
		border:1px #cccccc solid;
		background:#eeeeee;
		padding:0px;
}

.bbbbbb
{
		border-bottom:1px #cccccc solid;
		background:#ffffff;
		padding:0px;
}

.info {
	width:130px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 1px;
	border-left-width: 0px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #999999;
	border-right-color: #999999;
	border-bottom-color: #999999;
	border-left-color: #999999;
}

form
{
	padding:0px;
	margin:0px;
}

.print-button {
  width: 122px;
  height:44px;
  text-align: left;
  padding-left: 7px;
  padding-top: 7px;
  background: url(../imgs/print_button_bg.jpg);
  cursor: pointer;
  float:left;
}



			div.jGrowl div.manilla div.message {
				color: 					#ffffff;
				background:				#000000;
				
			}
			

			div.jGrowl div.manilla div.header {
				font-size:14px;
				font-weight:bold;
				color: 					#FFFF00;
			}
			
			
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
 
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	
	border:1px #bdbdbd solid;
	
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br/>
<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#damaged_repair_search">常用工具</a></li>
			<li><a href="#damaged_repair_filter">高级搜索</a></li>
		</ul>
		<div id="damaged_repair_search">
			 <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td width="30%" style="padding-top:3px;">
						<div id="easy_search_father">
						<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
						</div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()" value="<%=key%>"/>
									</div>
								</td>
								<td width="67">
									 
								</td>
							</tr>
						</table>
				</td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
			  			<a href="javascript:addDamagedRepairOrder();"><img src="../imgs/cerate_transport.jpg" width="129" height="51" border="0"/></a>
				  </td>
	            </tr>
	          </table>
		</div>
		
		<div id="damaged_repair_filter">
		    <table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" nowrap="nowrap" style="font-family: 宋体;font-size: 12px;">
						转运仓库：
						<select id="send_ps" name="send_ps">
							<option value="0">所有仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=send_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
&nbsp;&nbsp;
						目的仓库：
						<select id="receive_ps" name="receive_ps">
							<option value="0">所有仓库</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=receive_psid==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						&nbsp;&nbsp;
						状态：
						<select id="damaged_repair_status" name="damaged_repair_status">
							<option value="0">所有状态</option>
							<option value="<%=DamagedRepairKey.READY%>" <%=status==DamagedRepairKey.READY?"selected":""%>><%=damagedRepairKey.getDamagedRepairStatusById(DamagedRepairKey.READY)%></option>
							<option value="<%=DamagedRepairKey.INTRANSIT%>" <%=status==DamagedRepairKey.INTRANSIT?"selected":""%>><%=damagedRepairKey.getDamagedRepairStatusById(DamagedRepairKey.INTRANSIT)%></option>
							<option value="<%=DamagedRepairKey.PACKING%>" <%=status==DamagedRepairKey.PACKING?"selected":""%>><%=damagedRepairKey.getDamagedRepairStatusById(DamagedRepairKey.PACKING)%></option>
							<option value="<%=DamagedRepairKey.ERRORINTRANSIT%>" <%=status==DamagedRepairKey.ERRORINTRANSIT?"selected":""%>><%=damagedRepairKey.getDamagedRepairStatusById(DamagedRepairKey.ERRORINTRANSIT)%></option>
						</select>
				  &nbsp;&nbsp;
						<input type="button" class="button_long_refresh" value="过滤" onclick="filter()"/></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script>
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
	</script>

	<br/>
<table width="98%" border="0" align="center" cellpadding="0" isNeed="not" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="11%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">返修单号</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">目的仓库</th>
        <th width="10%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">转运仓库</th>
        <th width="11%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建时间</th>
        <th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">创建人</th>
		<th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">状态</th>
        <th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">允许装箱</th>
        <th width="12%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">货运公司</th>
        <th width="12%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运单号</th>
        <th width="14%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;"></th>
  	</tr>
  	<%
  		for(int i = 0 ;i<rows.length;i++)
  		{
  	%>
  		<tr> 
  			<td height="40" nowrap="nowrap">
  				<a href="damaged_repair_order_detail.html?repair_id=<%=rows[i].getString("repair_id")%>">R<%=rows[i].getString("repair_id")%></a>
  			</td>
  			<td nowrap="nowrap">
  				<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("receive_psid",0l)).getString("title")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("send_psid",0l)).getString("title")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=rows[i].getString("repair_date")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=rows[i].getString("repair_create_account")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=damagedRepairKey.getDamagedRepairStatusById(rows[i].get("repair_status",0)) %>
  			</td>
  			<td nowrap="nowrap">
  				<%=rows[i].getString("repair_packing_account")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=rows[i].getString("repair_waybill_number")%>
  			</td>
  			<td nowrap="nowrap">
  				<%=rows[i].getString("repair_waybill_name")%>
  			</td>
  			<td>
  				<%
  					if(rows[i].get("repair_status",0)==DamagedRepairKey.READY)
  					{
  				%>
  					<input type="button" value="装箱" class="short-short-button" onclick="readyPacking(<%=rows[i].get("repair_id",0l)%>)"/>&nbsp;&nbsp;
  					<!-- 
  					<input type="button" value="删除" class="short-short-button-del" onclick="delTransport(<%=rows[i].get("transport_id",0l)%>)"/>
  					 -->
  				<%
  					}
  				%>
  				<%
  					if(rows[i].get("transport_status",0)==DamagedRepairKey.PACKING)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.rebackTransport">
  					<input type="button" value="停止装箱" class="long-button-redtext" onclick="reBackTransport(<%=rows[i].get("transport_id",0l)%>)"/>
  					</tst:authentication>
  				<%
  					}
  				%>
  				
  				<%
  					if(rows[i].get("transport_status",0)==DamagedRepairKey.ERRORINTRANSIT||rows[i].get("transport_status",0)==DamagedRepairKey.INTRANSIT)
  					{
  				%>
  					<tst:authentication bindAction="com.cwc.app.api.zj.TransportMgrZJ.reStorageTransport">
  					<input type="button" value="中止运输" class="long-button-yellow" onclick="reStorageTransport(<%=rows[i].get("transport_id",0l)%>)"/><br/><br/>
  					</tst:authentication>
  				<%
  					}
  				%>
  					<!-- 
	  	  			<input type="button" class="long-button" value="到货申请完成" onclick="applicationApprove(<%=rows[i].get("repair_id",0l)%>)"/>
  					 -->
  			</td>
	  	</tr>
  	<%
  		}
  	%>
</table>
<br />
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>

<form action="damaged_repair_order_index.html" method="get" name="search_form">
	<input type="hidden" name="key"/>
	<input type="hidden" name="cmd" value="search"/>
</form>

<form action="damaged_repair_order_index.html" method="post" name="filter_form">
	<input type="hidden" name="send_psid"/>
	<input type="hidden" name="receive_psid"/>
	<input type="hidden" name="status"/>
	<input type="hidden" name="cmd" value="filter"/>
</form>

<form name="dataForm" method="post">
        <strong>
        <input type="hidden" name="p" />
        <input type="hidden" name="status" value="<%=status%>" />
        <input type="hidden" name="cmd" value="<%=cmd%>" /><input type="hidden" name="number" value="<%=key%>" />
        <input type="hidden" name="receive_psid" value="<%=receive_psid%>"/>
        <input type="hidden" name="send_psid" value="<%=send_psid%>"/>
        </strong>
        
		
		
  </form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action" name="add_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/administrator_delivery_order_detail.html"/>
</form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="repair_id"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/approveTransportOutbound.action" method="post" name="applicationOutboundApprove_form">
	<input type="hidden" name="transport_id"/>
</form>

<script type="text/javascript">
	function search()
	{
		if($("#search_key").val().length>0)
		{
			document.search_form.key.value = $("#search_key").val();
			document.search_form.submit();
		}
		else
		{
			alert("请输入转运单号");
		}
	}
	
	function filter()
	{
		document.filter_form.send_psid.value = $("#send_ps").getSelectedValue();
		document.filter_form.receive_psid.value = $("#receive_ps").getSelectedValue();
		document.filter_form.status.value = $("#damaged_repair_status").getSelectedValue();
		document.filter_form.submit();
	}
	
	function delTransport(transport_id)
	{
		if(confirm("确定删除转运单T"+transport_id+"？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/adminitrrator/transport/delTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(repair_id)
	{
		if(confirm("确定申请R"+repair_id+"转运单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/addApproveDamagedRepair.action";
			document.applicationApprove_form.repair_id.value = repair_id;
			document.applicationApprove_form.submit();
		}
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
		
	function closeWinNotRefresh()
	{
		tb_remove();
	}
	
	function addDamagedRepairOrder()
	{
		tb_show('创建转运单','add_damaged_repair_order.html?TB_iframe=true&height=400&width=700',false);
	}
	
	function createDeliveryOrder(purchase_id)
	{
		document.add_deliveryOrder_form.purchase_id.value = purchase_id;
		document.add_deliveryOrder_form.submit();	
		tb_remove();
	}	
	
	
	
	function approveOutbound(transport_id)
	{
		document.applicationOutboundApprove_form.transport_id.value = transport_id;
		document.applicationOutboundApprove_form.submit();
	}
	
	
	function readyPacking(repair_id)
	{
		tb_show('残损返修缺货商品','damaged_repair_storage_show.html?repair_id='+repair_id+'&TB_iframe=true&height=400&width=700',false);
	}
	
	function reBackTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"停止装箱？（将按照转运商品回退库存）"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reBackTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function reStorageTransport(transport_id)
	{
		if(confirm("确定转运单T"+transport_id+"中止运输？"))
		{
			var para = "transport_id="+transport_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/transport/reStorageTransport.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>



