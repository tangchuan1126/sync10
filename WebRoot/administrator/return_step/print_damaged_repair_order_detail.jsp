<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%
	long repair_id = StringUtil.getLong(request,"repair_id");
	
	DBRow damagedRepair = damagedRepairMgrZJ.getDetailDamagedRepair(repair_id);
	
	DBRow[] damagedRepairDetails = damagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id,null,null,null,null);
	
	double sum = 0;
%>
<style type="text/css">
<!--
.STYLE1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>

<table width="720px" border="0" align="left" cellpadding="0" cellspacing="0">
<thead>
  <tr>
  	<td colspan="6">
		<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2" align="center" valign="middle" style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;">Visionari LLC</td>
				<td width="45%" align="center" valign="middle" nowrap="nowrap"  style="font-family: 黑体;font-size: large;padding-bottom:10px;">微尘大业返修单</td>
				<td width="29%" colspan="2" align="left" style="font-family:黑体;font-size: 15px;padding-bottom:10px;">
				<br />
				返修单号：<%="R"+repair_id%><br/>
			  <span style="font-family: C39HrP48DmTt;font-size:40px;">*<%=repair_id%>*</span></td>
		  </tr>
			   <tr>
			   	<td colspan="5" style=" padding-bottom:5px; padding-top:5px;">
					<hr/>
					<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<table width="100%">
						<tr>
							<td align="right" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">货运公司：</td>
							<td align="left"><%=damagedRepair.getString("repair_waybill_name") %></td>
							<td nowrap="nowrap" style="font-family:黑体;font-size: 16px;" align="right">运单号：</td>
							<td align="left"><%=damagedRepair.getString("repair_waybill_number")%></td>
						</tr>
					</table>
					</div>
					<hr/>
				</td>
			  </tr>
		 </table>
	</td>
  </tr>
    <tr>
		<th nowrap="nowrap"  align="left"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">应发数量</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">实发数量</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">所在箱号</th>
	</tr>
  </tr>
  </thead>
  	<%
		for(int i = 0;i<damagedRepairDetails.length;i++)
		{
	%>
		<tr>
		<td align="left" height="30" style="font-family:黑体;font-size: 15px;border-left: 2px #000000 solid;border-bottom: 1px #000000 solid;padding-left:3px;"><%=damagedRepairDetails[i].getString("p_name")%></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;"><%=damagedRepairDetails[i].get("repair_count",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;font-style:oblique"><%=damagedRepairDetails[i].get("repair_send_count",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;;border-bottom: 1px #000000 solid;border-right: 2px #000000 solid;padding-left:3px;"><%=damagedRepairDetails[i].getString("repair_box")%></td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="middle" style="border-left: 2px #000000 solid;border-bottom: 1px #000000 solid; padding-top:5px; padding-bottom:5px;">
			<%
				DBRow product = productMgr.getDetailProductByPcid(damagedRepairDetails[i].get("repair_pc_id",0l));
			%>
			<span style="font-family: C39HrP48DmTt;font-size:40px;">*<%=product.getString("p_code")%>*</span>
			</td>
			<td colspan="4" style="border-right: 2px #000000 solid;border-bottom: 1px #000000 solid;"></td>
		</tr>
	<%
		}
	%>
	<tfoot>
		<tr>
			<td colspan="6" style="padding-top:15px;">
				<hr/>
				<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<table width="100%">
				  <tr>
					<td width="10%" align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">交货地址：</td>
					<td width="90%" align="left"><%=damagedRepair.getString("repair_address")%></td>
				  </tr>
				  <tr>
					<td align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">收货人：</td>
					<td align="left"><%=damagedRepair.getString("repair_linkman")%></td>
				  </tr>
				  <tr>
					<td align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">收货电话：</td>
					<td align="left"><%=damagedRepair.getString("repair_linkman_phone")%></td>
				  </tr>
				</table>
				</div>
				<hr/>
			</td>
		</tr>
		
	</tfoot>
</table>



