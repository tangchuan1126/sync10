<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.ApproveTransportKey"%>
<%@page import="com.cwc.app.key.ApproveDamagedRepairOutboundKey"%>
<%@ include file="../../include.jsp"%> 
<%
long rsa_id = StringUtil.getLong(request,"rsa_id");

int approve_status = StringUtil.getInt(request,"approve_status");
String backurl = StringUtil.getString(request,"backurl");

DBRow damagedRepairOutboundDifferents[];

damagedRepairOutboundDifferents = damagedRepairOutboundApproveMgrZJ.getDamagedRepairOutboundApproverDetailsByTsaid(rsa_id,null);




boolean checkstorage = false;//扣除多余数量仓库不缺货
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
<link rel="stylesheet" href="../js/dynCalendar.css" type="text/css" media="screen">
<script src="../js/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="../js/dynCalendar.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>


<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
var keepAliveObj=null;
function keepAlive()
{
	$("#keep_alive").load("../imgs/account.gif"); 

	clearTimeout(keepAliveObj);
	keepAliveObj = setTimeout("keepAlive()",1000*60*5);
}  

function checkForm(theForm)
{
	var reason = theForm.note;
	var rsad_ids = theForm.rsad_ids;
	
	var haveEmpty = false;
	var oneSelected = false;
	
	//先判断是否数组
	if(typeof rsad_ids.length == "undefined")
	{		
			if (reason.value=="")
			{
				reason.style.background = "#EDF36D";
				haveEmpty = true;
			}
	}
	else
	{
		for (i=0; i<rsad_ids.length; i++)
		{
			if (reason[i].value=="")
			{
				reason[i].style.background = "#EDF36D";
				haveEmpty = true;
			}
			else
			{
				reason[i].style.background = "";
			}
		}
	}
	
	if (haveEmpty)
	{
		alert("有商品差异原因没填写");
		return(false);
	}

	return(true);
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable();keepAlive()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 返修转运应用 » 返修发货审核</td>
  </tr>
</table>
<br>
<form name="form1" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/approveDamagedRepairOutbound.action" onSubmit="return checkForm(this)">
<input type="hidden" name="rsa_id" value="<%=rsa_id%>">
<input type="hidden" name="backurl" value="<%=backurl%>">

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="20%" class="left-title" style="vertical-align: center;text-align: center;">商品名称</th>
        <th width="28%" align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
        <th width="45%" style="vertical-align: center;text-align: left;" class="right-title">差异原因</th>
    </tr>

<%
for (int i=0; i<damagedRepairOutboundDifferents.length; i++)
{
%>

    <tr> 
      <td   width="20%" height="80" align="center" valign="middle">
      		<%=damagedRepairOutboundDifferents[i].getString("product_name")%>
      	<input name="rsad_ids" type="hidden"" value="<%=damagedRepairOutboundDifferents[i].getString("rsad_id")%>"/>
      </td>
      <td align="center" valign="middle"  ><%=damagedRepairOutboundDifferents[i].getString("product_code")%></td>
      <td   align="left" valign="middle" id="tad_<%=damagedRepairOutboundDifferents[i].getString("rsad_id")%>" >
      <table width="98%" border="0" cellspacing="4" cellpadding="0">
          <tr>
            <th  style="vertical-align: center;text-align: left;font-size:15px;color:#333333" >实际发货量：<span style="font-weight:bold;color:#0000FF"><%=damagedRepairOutboundDifferents[i].getString("repair_send_count")%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 应发货量：<span style="font-weight:bold;color:#FF0000"><%=damagedRepairOutboundDifferents[i].getString("repair_count")%></span>	
			</th>
          </tr>
          <tr>
            <th  style="vertical-align: center;text-align: left;font-size:15px;">
				<%
	if (approve_status==ApproveDamagedRepairOutboundKey.APPROVE||damagedRepairOutboundDifferents[i].get("approve_status",0)==ApproveDamagedRepairOutboundKey.APPROVE)
	{
	%>
			<span style="color:#990000;font-weight:normal"><%=damagedRepairOutboundDifferents[i].getString("note")%></span>
	<%
	}
	else
	{
	%>
		<input type="text" name="note_<%=damagedRepairOutboundDifferents[i].getString("rsad_id")%>" id="note" style="width:400px;height:25px;font-size:14px;border:#cccccc solid 1px;color:#FF0000" value="<%=damagedRepairOutboundDifferents[i].getString("note")%>"> 
	<%
	}
	%>
			
			</th>
          </tr>
        </table></td>
    </tr>
<%
}
%>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="8" cellspacing="0">
  <tr>
    <td align="center" valign="middle" bgcolor="#eeeeee">
	<%
	if (approve_status==ApproveDamagedRepairOutboundKey.WAITAPPROVE&&checkstorage==false)
	{
	%>
	  <input name="Submit" type="submit" class="long-button-redtext" value="审核通过"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
	}
	%>
    
      <input name="Submit2" type="button" class="long-button" value="返回" onClick="history.back();">
    </td>
  </tr>
</table>
</form>
<div id="keep_alive" style="display:none"></div>
<br>
<br>
</body>
</html>
