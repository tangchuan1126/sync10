<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>

<%
	long pcid = StringUtil.getLong(request,"pcid");
AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);
long cid = StringUtil.getLong(request,"cid");
int type = StringUtil.getInt(request,"type");
String name = StringUtil.getString(request,"name");
String cmd = StringUtil.getString(request,"cmd");

String pro_line_id = StringUtil.getString(request,"pro_line_id");

if (cmd.equals(""))
{
	cid = adminLoggerBean.getPs_id();
}


DBRow rows[];
if ( cmd.equals("cid") )
{
	rows = returnStepMgrZJ.filterReturnSteps(cid,pcid,pro_line_id,pc);
}
else if(cmd.equals("name"))
{
	rows = returnStepMgrZJ.searchReturnStepByName(pcid,cid,pro_line_id,name,pc);
}
else
{
	rows = returnStepMgrZJ.filterReturnSteps(cid,pcid,pro_line_id,pc);
}

Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/select.js"></script>
		
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
 
 

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">


<!--  自动填充 -->

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script language="javascript">
<!--
$().ready(function() {

	function log(event, data, formatted) {
		
	}

	addAutoComplete($("#filter_name"),
		"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProducts4RecordOrderJSON.action",
		"merge_info",
		"p_name");
	
	$("#filter_name").keydown(function(event){
		if (event.keyCode==13)
		{
			searchByName();
		}
	});
	
});


//修改仓库分类
function modStoreCatalog(pid,pcid,pname,cid)
{

<%
StringBuffer countryCodeSB = new StringBuffer("");
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
String storeCatalogQx;

countryCodeSB.append("<select name='mod_store_cid' id='mod_store_cid' >");
countryCodeSB.append("<option value='0'>选择仓库...</option>");
for (int countryCodeI=0; countryCodeI<treeRows.length; countryCodeI++)
{
	if ( treeRows[countryCodeI].get("parentid",0) != 0 )
	 {
	 	storeCatalogQx = "├ ";
	 }
	 else
	 {
	 	storeCatalogQx = "";
	 }
	 
	countryCodeSB.append("<option value='"+treeRows[countryCodeI].getString("id")+"'>");
	countryCodeSB.append(Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[countryCodeI].get("level",0)));
	countryCodeSB.append(storeCatalogQx);
	countryCodeSB.append(treeRows[countryCodeI].getString("title"));  
	countryCodeSB.append("</option>");
	
}
	countryCodeSB.append("</select>");
%>

	$.prompt(
	
	"<div id='title'>修改仓库[商品:"+pname+"]</div><br /> <%=countryCodeSB.toString()%>",

	{
	      submit: promptCheckStoreCatalog,
   		  loaded:
		  
				function ()
				{
					$("#mod_store_cid").setSelectedValue(cid);
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						document.mod_store_catalog_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modStoreCatalog.action";
						document.mod_store_catalog_form.mod_store_cid.value = f.mod_store_cid;
						document.mod_store_catalog_form.pid.value = pid;
						document.mod_store_catalog_form.pcid.value = pcid;
						document.mod_store_catalog_form.submit();		
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

function promptCheckStoreCatalog(v,m,f)
{
	if (v=="y")
	{
		  //an = m.children('#orderNoteWinText');
		  if(f.mod_store_cid == 0)
		  {
			   alert("请选择仓库");
			   
				return false;
		  }
		  return true;
	}
}





function del()
{
	if (confirm("确认操作吗？"))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}


function selectOne()
{
	var flag = false;
	
	if (document.listForm.pid_batch.length>0)
	{
		for (i=0; i<document.listForm.pid_batch.length; i++)
		{
			if(document.listForm.pid_batch[i].checked)
			{
				flag = true;
				break;
			}
		}
	}
	else
	{
			if(document.listForm.pid_batch.checked)
			{
				flag = true;
			}
	}
	
	return(flag);
}

function batchModPrice()
{
	if ( !selectOne() )
	{
		alert("请选择你要修改的商品");
	}
	else
	{
		if ( del() )
		{
			document.listForm.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/modProductStorage.action";
			document.listForm.submit();
		}
	}
}


function delPro(pid,p_name)
{
	if ( confirm("确认删除["+p_name+"]库存？") )
	{
		document.temp_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/delProductStorage.action";
		document.temp_form.pid.value = pid;
		document.temp_form.submit();
	}
}

function rowSel(index)
{
	if ( document.listForm.pid_batch[index].checked )
	{
		document.listForm.pid_batch[index].checked = false;
	}
	else
	{
		document.listForm.pid_batch[index].checked = true;
	}
}

function onMOBg(row,cl,index)
{
	if ( !document.listForm.pid_batch[index].checked )
	{
		row.style.background=cl;
	}
}





function syncode(val)
{
	document.add_pro_form.code.value=val;
}

function searchByCid()
{
	document.search_form.cmd.value = "cid";
	document.search_form.cid.value = $("#filter_cid").val();	
	document.search_form.pcid.value = $("#filter_pcid").val();
	document.search_form.pro_line_id.value = $("#filter_productLine").val();
	document.search_form.submit();
}

function searchByName()
{
	if ($("#filter_name").val()=="")
	{
		alert("请填写关键词");
	}
	else
	{
		document.search_form.cmd.value = "name";
		document.search_form.name.value = $("#filter_name").val();	
		document.search_form.cid.value = $("#filter_cid").val();	
		document.search_form.pcid.value = $("#filter_pcid").val();
		document.search_form.pro_line_id.value = $("#filter_productLine").val();
		document.search_form.submit();
	}
}

function report()
{
	document.report_form.action="report.html";
	document.report_form.target="_blank";
	document.report_form.export_flag.value="0";
	document.report_form.submit();
}

function exportData()
{
	document.report_form.action="report.html";
	document.report_form.target="_self";
	document.report_form.export_flag.value="1";
	document.report_form.submit();
}

function showAddProductTable()
{
	document.getElementById("add_product_table").style.display="";
}

function hiddenAddProductTable()
{
	document.getElementById("add_product_table").style.display="none";
}

function openConvertProduct(storage_name,s_pid,s_pc_id)
{	
	tb_show('转换商品','convert_product.html?s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function addDamageProduct(storage_name,storage_id,pid,store_count,damage_count)
{	
	tb_show('增加残损商品','add_damaged.html?storage_id='+storage_id+'&pid='+pid+'&store_count='+store_count+'&damage_count='+damage_count+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function returnProduct(storage_name,storage_id,pid)
{	
	tb_show('退件进库','add_return_product.html?storage_id='+storage_id+'&storage_name='+storage_name+'&pid='+pid+'&TB_iframe=true&height=300&width=650',false);
}

function openRenewProduct(storage_name,s_pid,s_pc_id) 
{	
	tb_show('残损翻新','return_step_renew_product.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&TB_iframe=true&height=300&width=650',false);
}

function openCombination(storage_name,s_pid,s_pc_id,cid)
{	
	tb_show('预拼装套装','combinnation_union.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&cid='+cid+'&TB_iframe=true&height=300&width=650',false);
}

function openSplit(storage_name,s_pid,s_pc_id,cid)
{	
	tb_show('拆散套装','return_split_union.html?psid=<%=cid%>&s_pid='+s_pid+'&s_pc_id='+s_pc_id+'&storage_name='+storage_name+'&cid='+cid+'&TB_iframe=true&height=300&width=650',false);
}

function convertProduct(s_pid,s_pc_id,p_name,quantity)
{
	document.convert_product_form.s_pid.value = s_pid;
	document.convert_product_form.s_pc_id.value = s_pc_id;
	document.convert_product_form.p_name.value = p_name;
	document.convert_product_form.quantity.value = quantity;
	document.convert_product_form.submit();
}

function selectLabel(product_id)
{
	tb_show('制作商品标签','../lable_template/lable_template_show.html?pc_id='+product_id+'&TB_iframe=true&height=500&width=800',false);
}


/**
function addDamageProduct(name,pid,store_count)
{	
	store_count = store_count*1;

	$.prompt(
	
	"<div id='title'>残损件登记["+name+"]</div><br />新增数量 <input name='proQuantity' type='text' id='proQuantity' style='width:100px;'><br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (!isNum(f.proQuantity))
						{
							alert("请正确填写数量");
							return(false);
						}
						else if (store_count<f.proQuantity)
						{
							alert("残损件数不能比现有库存多");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{
						if ( confirm("确认["+name+"]新增["+f.proQuantity+"]件残损？") )
						{
							document.add_damaged_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addDamageProduct.action";
							document.add_damaged_product_form.quantity.value = f.proQuantity;
							document.add_damaged_product_form.pid.value = pid;
							document.add_damaged_product_form.submit();
						}
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}



function addReturnProduct(name,pid)
{	
	$.prompt(
	
	"<div id='title'>退货登记["+name+"]</div><br />&nbsp;&nbsp;退件检测 &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   <select name='preDamaged' id='preDamaged'>  <option value='-1' selected>请选择</option>    <option value='0'>完好</option>    <option value='1'>损坏</option>  </select> <br><br>&nbsp;&nbsp;相关订单号 &nbsp;&nbsp;&nbsp;<input name='proOid' type='text' id='proOid' style='width:100px;'> <br><br>&nbsp;&nbsp;数量 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name='proQuantity' type='text' id='proQuantity' style='width:100px;'><br> ",

	{
	      submit: 
				function (v,m,f)
				{
					if (v=="y")
					{
						if (f.preDamaged==-1)
						{
							alert("请选择退件检测情况");
							return(false);
						}
						else if (!isNum(f.proOid))
						{
							alert("请正确填写订单号");
							return(false);
						}
						else if (!isNum(f.proQuantity))
						{
							alert("请正确填写数量");
							return(false);
						}
						else
						{
							return(true);
						}
					}
				}
		  ,
   		  loaded:
		  
				function ()
				{
					
				}
		  
		  ,
		  callback: 
		  
				function (v,m,f)
				{
					if (v=="y")
					{

						if ( confirm("确认订单 ["+f.proOid+"] 退件 ["+name+"] X "+f.proQuantity+"？") )
						{
							document.add_return_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addReturnProduct.action";
							document.add_return_product_form.quantity.value = f.proQuantity;
							document.add_return_product_form.pid.value = pid;
							document.add_return_product_form.oid.value = f.proOid;
							document.add_return_product_form.damaged.value = f.preDamaged;
							document.add_return_product_form.submit();
						}
					}
				}
		  
		  ,
		  overlayspeed:"fast",
		  buttons: { 提交: "y", 取消: "n" }
	});
}

**/

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 

function reCheckLackingOrders()
{
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};		
			
				$.prompt(
				"<div id='title'>重算缺货</div>确定重新计算缺货订单库存？<br />",
				{
					  
			   		  loaded:
							function ()
							{
								
							}
					  
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
										$.ajax({
											url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ReCheckLackingOrders.action',
											type: 'post',
											dataType: 'html',
											timeout: 60000,
											cache:false,
											data:"",
											
											beforeSend:function(request){
												
											},
											
											error: function(){
												alert("网络错误，请重试");
											},
											
											success: function(msg){
												if (msg=="ok")
												{
													$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
													$.unblockUI();
													//alert("计算完成！");
													
												}
												else
												{
													alert("计算出错！");
												}
											}
										});		
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
			}
/**
function reCheckLackingOrders()
{
	$.blockUI.defaults = {
		css: { 
			padding:        '10px',
			margin:         0,
			width:          '200px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #aaa',
			backgroundColor:'#fff'
		},
		
		// 设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.8' 
		},

    centerX: true,
    centerY: true, 
	
		fadeOut:  2000
	};
	
	
	if ( confirm("确定重新计算缺货订单库存？") )
	{
		$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">计算中，请稍后......</span>'});
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/ReCheckLackingOrders.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"",
			
			beforeSend:function(request){
				
			},
			
			error: function(){
				alert("网络错误，请重试");
			},
			
			success: function(msg){
				if (msg=="ok")
				{
					$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">计算完成！</span>' });
					$.unblockUI();
					//alert("计算完成！");
					
				}
				else
				{
					alert("计算出错！");
				}
			}
		});	
	}

}**/

function closeWin()
{
	tb_remove();
	window.location.reload();
}

function renewProduct(psid,pid,pcid,damaged_count_m,damaged_package_count_m)
{
		document.renew_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/RenewProduct.action";
		document.renew_product_form.damaged_count_m.value = damaged_count_m;
		document.renew_product_form.damaged_package_count_m.value = damaged_package_count_m;
		document.renew_product_form.psid.value = psid;
		document.renew_product_form.pid.value = pid;
		document.renew_product_form.pcid.value = pcid;
		document.renew_product_form.submit();
}

function combinationProduct(psid,pcid,combination_quantity)
{
		document.combination_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/CombinationProduct.action";
		document.combination_product_form.combination_quantity.value = combination_quantity;
		document.combination_product_form.psid.value = psid;
		document.combination_product_form.pcid.value = pcid;
		document.combination_product_form.submit();
}

function splitProduct(psid,pcid,combination_quantity)
{
		document.split_product_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/SplitProduct.action";
		document.split_product_form.split_quantity.value = combination_quantity;
		document.split_product_form.psid.value = psid;
		document.split_product_form.pcid.value = pcid;
		document.split_product_form.submit();
}

function addedProductStorage()
{
	if($("#filter_cid").val()==0)
	{
		alert("请选择一个仓库再进行商品库存补全！");
	}
	else
	{
		document.added_product_storage_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addedProdcutStorage.action";
		document.added_product_storage_form.added_ps_id.value = $("#filter_cid").val();
		document.added_product_storage_form.submit();
	}
	
	
}

function downloadProductStorage()
{
	var ps_id = $("#filter_cid").val();
	var catalog_id = $("#filter_pcid").val();
	var product_line = $("#filter_productLine").val();
	
	var para = "ps_id="+ps_id+"&catalog_id="+catalog_id+"&product_line="+product_line;
	
	if(ps_id!=0)
	{
		$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">生成文件中......</span>'});
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product_storage/downloadProductStorage.action',
				type: 'post',
				dataType: 'json',
				timeout: 600000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert(e);
					alert("提交失败，请重试！");
				},
					
				success: function(date){
					if(date["canexport"]=="true")
					{
						$.unblockUI();
						document.download_form.action=date["fileurl"];
						document.download_form.submit();
					}
					else
					{
						alert("无法下载！");
					}
				}
			});
	}	
	else
	{
		alert("请选择一个仓库再下载！")
	}
}

function uploadProductStorage()
{

	tb_show('拆散套装','storage_alert_upload_excel.html?TB_iframe=true&height=500&width=800',false);
}

function closeWinRefresh()
{
	window.location.reload();
	tb_remove();
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
td.categorymenu_td div{margin-top:6px;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   库存管理</td>
  </tr>
</table>
<br>
<form action="" name="download_form" id="download_form">
</form>
<form method="post" name="combination_product_form">
<input type="hidden" name="combination_quantity">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>

<form method="post" name="split_product_form">
<input type="hidden" name="split_quantity">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>

<form method="post" name="renew_product_form">
<input type="hidden" name="damaged_count_m">
<input type="hidden" name="damaged_package_count_m">
<input type="hidden" name="pid">
<input type="hidden" name="pcid">
<input type="hidden" name="psid">
</form>


<form method="post" name="add_return_product_form">
<input type="hidden" name="pid">
<input type="hidden" name="quantity">
<input type="hidden" name="oid">
<input type="hidden" name="damaged">
</form>


<form method="post" name="add_damaged_product_form">
<input type="hidden" name="pid">
<input type="hidden" name="quantity">
</form>

<form method="post" name="convert_product_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/convertProduct.action">
<input type="hidden" name="s_pid" >
<input type="hidden" name="s_pc_id" >
<input type="hidden" name="p_name" >
<input type="hidden" name="quantity" >
</form>

<form method="post" name="mod_store_catalog_form">
<input type="hidden" name="mod_store_cid">
<input type="hidden" name="pid">
<input type="hidden" name="pcid">
</form>

<form action="" method="post" name="temp_form">
<input type="hidden" name="pid">
</form>

<form method="post" name="report_form">
<input type="hidden" name="cid" value="<%=cid%>">
<input type="hidden" name="name" value="<%=name%>">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="type" value="<%=type%>">
<input type="hidden" name="export_flag" value="0">
</form>

<form action="" name="added_product_storage_form" id="added_product_storage_form">
	<input id="added_ps_id" name="added_ps_id" type="hidden"/>
</form>

	  <form action="return_step_index.html" method="get" name="search_form">		
		  <input type="hidden" name="cmd"> 
		  <input type="hidden" name="cid"> 
		  <input type="hidden" name="type"> 
		  <input type="hidden" name="pcid"> 
		  <input type="hidden" name="name">
		  <input type="hidden" name="pro_line_id"/> 
</form>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
          <td width="54%" height="33" >
		  <div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:800px;">
	 
		  <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td width="416" style="padding-left:10px;">
              <ul id="categorymenu" class="mcdropdown_menu">
 <li rel="0">所有分类</li>
 
    <%
	  DBRow c1[] = catalogMgr.getProductCatalogByParentId(0,null);
	  for (int i=0; i<c1.length; i++)
	  {
			out.println("<li rel='"+c1[i].get("id",0l)+"'> "+c1[i].getString("title"));

			  DBRow c2[] = catalogMgr.getProductCatalogByParentId(c1[i].get("id",0l),null);
			  if (c2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<c2.length; ii++)
			  {
					out.println("<li rel='"+c2[ii].get("id",0l)+"'> "+c2[ii].getString("title"));		
					
						DBRow c3[] = catalogMgr.getProductCatalogByParentId(c2[ii].get("id",0l),null);
						  if (c3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<c3.length; iii++)
							{
									out.println("<li rel='"+c3[iii].get("id",0l)+"'> "+c3[iii].getString("title"));
									out.println("</li>");
							}
						  if (c3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (c2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
</ul>
	  <input type="text" name="category" id="category" value=""  />
	  <input type="hidden" name="filter_pcid" id="filter_pcid" value="0"  />
	</td>
              <td width="317" height="30"  style="border-bottom:1px #dddddd solid"><select name="filter_cid" id="filter_cid">
		<option value="0">所有仓库</option>
          <%
String qx;

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
          <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==cid?"selected":""%>> 
          <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%>
          <%=qx%>
          <%=treeRows[i].getString("title")%>          </option>
          <%
}
%>
        </select>&nbsp;&nbsp;
        <input name="Submit4" type="button" class="button_long_refresh" onClick="searchByCid()" value="过滤">			  
        </td>
        
            </tr>
            <tr>
			<td width="423" class="categorymenu_td" align="left" bgcolor="#eeeeee" style="padding-left:10px;"> 
           <ul id="productLinemenu" class="mcdropdown_menu">
             <li rel="0">所有产品线</li>
             <%
	  DBRow p1[] = productLineMgrTJH.getAllProductLine();
	  for (int i=0; i<p1.length; i++)
	  {
			out.println("<li rel='"+p1[i].get("id",0l)+"-p'> "+p1[i].getString("name"));
			  DBRow p2[] = productLineMgrTJH.getProductCatalogByProductLineId(p1[i].get("id",0l));
			  if (p2.length>0)
			  {
			  		out.println("<ul>");	
			  }
			  for (int ii=0; ii<p2.length; ii++)
			  {
					out.println("<li rel='"+p2[ii].get("id",0l)+"'> "+p2[ii].getString("title"));		
					
						DBRow p3[] = catalogMgr.getProductCatalogByParentId(p2[ii].get("id",0l),null);
						  if (p3.length>0)
						  {
								out.println("<ul>");	
						  }
							for (int iii=0; iii<p3.length; iii++)
							{
									out.println("<li rel='"+p3[iii].get("id",0l)+"'> "+p3[iii].getString("title"));
									
									DBRow p4 [] = catalogMgr.getProductCatalogByParentId(p3[iii].get("id",0l),null);
									if(p4.length>0)
									{
										out.println("<ul>");
									}
									for(int k=0;k<p4.length;k++)
									{
										out.println("<li rel='"+p4[k].get("id",0l)+"'> "+p4[k].getString("title"));
										DBRow pl [] = catalogMgr.getProductCatalogByParentId(p4[k].get("id",0l),null);
										if(pl.length>0)
										{
											out.println("<ul>");
											for(int kk=0;kk<pl.length;kk++)
											{
												out.println("<li rel='"+pl[kk].get("id",0l)+"'> "+pl[kk].getString("title"));
												out.println("</li>");
											}
											if(pl.length>0)
											{
												out.println("</ul>");
											}
										}
										out.println("</li>");
									}
									if(p4.length>0)
									{
										out.println("</ul>");	
									}
									out.println("</li>");
							}
						  if (p3.length>0)
						  {
								out.println("</ul>");	
						  }
						  
					out.println("</li>");				
			  }
			  if (p2.length>0)
			  {
			  		out.println("</ul>");	
			  }
			  
			out.println("</li>");
	  }
	  %>
           </ul>
           <input type="text" name="productLine" id="productLine" value="" />
           <input type="hidden" name="filter_productLine" id="filter_productLine" value="0"  />
          &nbsp;&nbsp;

</td>
              <td style=""><input name="filter_name" type="text" id="filter_name" value="<%=name%>" style="width:200px;"> &nbsp;&nbsp;&nbsp;&nbsp;
        <input name="Submit3" type="button" class="button_long_search" value="搜索" onClick="searchByName()">			  </td>
            </tr>
          </table>

			</div>
		  </td>
    <td width="46%" align="right" style="padding-right:10px;">
	</td>
  </tr>
</table>
	</td>
  </tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
	  <tr>
		<td><span style="padding-right:10px;">
		  <!-- 
		  <input name="Submit2" type="button" class="long-button-ok" onClick="return batchModPrice()" value="批量修改">
		  
		  &nbsp;&nbsp;<input type="button" class="long-button-ok" onClick="addedProductStorage()" value="仓库补全商品"/>
		   -->&nbsp;
		</span></td>
	  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" >
  <form name="listForm" method="post">


    <tr> 
        <th  style="vertical-align: center;text-align: center;" class="left-title">&nbsp;</th>
        <th width="325" align="left" class="right-title"  style="vertical-align: center;text-align: cenleftter;">商品名称</th>
        <th width="258" align="left" class="right-title"  style="vertical-align: center;text-align: left;">商品分类</th>
        <th width="182"  style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
        <th width="94" class="right-title"  style="vertical-align: center;text-align: center;">功能残损</th>
        <th width="88" class="right-title"  style="vertical-align: center;text-align: center;">外观残损</th>
        <th width="204"  style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</th>
    </tr>

    <%
String delPCID = "";
DBRow proGroup[];
for ( int i=0; i<rows.length; i++ )
{
delPCID += rows[i].getString("pc_id")+",";
%>
    <tr > 
      <td width="72" height="60" align="center" valign="middle"   > 
	  <a name="<%=rows[i].getString("p_name")%>" id="<%=rows[i].getString("p_name")%>"></a>
      <input name="pid_batch"  type="checkbox" value="<%=rows[i].getString("pid")%>">      </td>
      <td height="60" valign="middle"   style='font-size:14px;' >
        
      <%=rows[i].getString("p_name")%></td>
      <td height="60" align="left" valign="middle"   style='line-height:20px;' >
	   <span style="color:#999999">
	  <%
	  DBRow allCatalogFather[] = catalogTree.getAllFather(rows[i].get("catalog_id",0l));
	  for (int jj=0; jj<allCatalogFather.length-1; jj++)
	  {
	  	out.println("<a class='nine4' href='?cmd=cid&pcid="+allCatalogFather[jj].getString("id")+"&cid="+cid+"&type=0'>"+allCatalogFather[jj].getString("title")+"</a><br>");

	  }
	  %>
	  </span>
	  
	  <%
	  DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(rows[i].getString("catalog_id")));
	  if (catalog!=null)
	  {
	  	out.println("<a href='?cmd=cid&pcid="+rows[i].getString("catalog_id")+"&cid="+cid+"&type=0'>"+catalog.getString("title")+"</a>");
	  }
	  %>	  </td>
      <td align="left" valign="middle"   >
	   <fieldset style="border:1px #999999 solid;padding:7px;width:160px;word-break:break-all;margin-top:10px;margin-bottom:10px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px;">
<legend style="font-size:15px;font-weight:bold;color:#999999;"> 
 <%=rows[i].getString("title")%>
 <span style="font-size:13px;color:#999999;font-weight:normal">warehouse</span></legend>
	  
	  	  <%
	  DBRow locations[] = returnStepMgrZJ.getReturnStepLocationByPcid(rows[i].get("cid",0l),rows[i].get("pc_id",0l),null);
	  for (int j=0; j<locations.length; j++)
	  {
	  %>
			<div style="padding:3px;color:#000000;font-weight:bold;font-size:13px;"><%=locations[j].getString("position")%> : <spab style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>
	  <%
	  }
	  %>
</fieldset>  </td>
      <td align="center" valign="middle"   style='word-break:break-all;color:#0000FF;font-weight:bold'>
	  
	  <input name="damaged_count_<%=rows[i].getString("pid")%>" type="text" value="<%=rows[i].get("damaged_count",0f)%>" style="width:50px;color:#FF0000">	  </td>
      <td align="center" valign="middle"   style='word-break:break-all;color:#0000FF;font-weight:bold'>
	  <input name="damaged_package_count_<%=rows[i].getString("pid")%>" type="text" value="<%=rows[i].get("damaged_package_count",0f)%>" style="width:50px;color:#FF0000">
	  </td>
      <td align="center" valign="middle"   >
	  <div style="margin-bottom:10px;margin-top:10px;">
<input name="Submit52" type="button" class="short-short-button-convert"  onClick="openRenewProduct('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>)" value="翻新">
</div>

	<div style="margin-bottom:10px;">
<%
if (rows[i].get("union_flag",0)==1)
{
%>
 <input name="Submit52" type="button" class="short-short-button-convert"  onClick="openSplit('<%=rows[i].getString("title")%>',<%=rows[i].getString("pid")%>,<%=rows[i].getString("pc_id")%>,<%=rows[i].getString("cid")%>)" value="拆散">
<%
}
%>
</div>
<div style="margin-bottom:10px;">
<input type="button" class="long-button-print" value="制作标签" onclick="selectLabel(<%=rows[i].get("pc_id",0l)%>)">
</div>
 </td>
    </tr>
    <%
}
%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm">
          <input type="hidden" name="p" >
		  <input type="hidden" name="cid" value="<%=cid%>">
		<input type="hidden" name="cmd" value="<%=cmd%>">
		<input type="hidden" name="name" value="<%=name%>">
		<input type="hidden" name="type" value="<%=type%>">
		 <input type="hidden" name="pcid" value="<%=pcid%>">
		
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle">
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<script type="text/javascript">
$("#category").mcDropdown("#categorymenu",{
		  allowParentSelect:true,
		  select: 
		  
				function (id,name)
				{
					$("#filter_pcid").val(id);
					if(id!=0)
					{
						$("#productLine").mcDropdown("#productLinemenu").setValue(0);
					}
				}

});

$("#productLine").mcDropdown("#productLinemenu",{
		allowParentSelect:true,
		  select: 
		  		
				function (id,name)
				{
					$("#filter_productLine").val(id);
					if(id!=0)
					{
						$("#category").mcDropdown("#categorymenu").setValue(0);
					}
					
				}

});
<%
if (pcid>0)
{
%>
$("#category").mcDropdown("#categorymenu").setValue(<%=pcid%>);
<%
}
else
{
%>
$("#category").mcDropdown("#categorymenu").setValue(0);
<%
}
%> 

<%
if (!pro_line_id.equals("0") && !pro_line_id.equals(""))
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue('<%=pro_line_id%>');
<%
}
else
{
%>
$("#productLine").mcDropdown("#productLinemenu").setValue(0);
<%
}
%>
</script>
</body>
</html>
