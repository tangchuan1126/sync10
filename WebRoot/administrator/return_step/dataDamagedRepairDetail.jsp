<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	
	long repair_id = StringUtil.getLong(request,"repair_id");//100005;
			
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));//是否使用了搜索功能
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");//排序顺序
	
	String filter = StringUtil.getString(request,"filters");//搜索条件
	
	FilterBean filterBean = null;//搜索条件bean
	
	if(search)//将搜索条件字符串转换成搜索条件bean，为方便后来搜索
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");//每页显示多少数据
	int pages = StringUtil.getInt(request,"page",1);//当前请求的是第几页
	
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(Integer.parseInt(c));
	pc.setPageNo(pages);
	
	
	if(oldpages != pages||pages==1)
	{
		DBRow[] details = damagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id,pc,sidx,sord,filterBean);
				
		DBRow data = new DBRow();
		data.add("page",pages);//page，当前是第几页
		data.add("total",pc.getPageCount());//total，总共页数
		
		data.add("rows",details);//rows，返回数据
		data.add("records",details.length);//records，总记录数

		out.println(new JsonObject(data).toString());
	}
%>