<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long psid = StringUtil.getLong(request,"psid");

long s_pid = StringUtil.getLong(request,"s_pid");

long s_pc_id = StringUtil.getLong(request,"s_pc_id");
long cid = StringUtil.getLong(request,"cid");

String storage_name = StringUtil.getString(request,"storage_name");
DBRow detailPro = productMgr.getDetailProductByPcid(s_pc_id);
DBRow detailDetailSetProductProductStorage = productMgr.getDetailProductProductStorageByPcid(cid,s_pc_id);

int combinationCount = (int)detailDetailSetProductProductStorage.get("damaged_count",0f);

int damaed_package_count = (int)detailDetailSetProductProductStorage.get("damaged_package_count",0f);
StringBuffer pids = new StringBuffer("");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script>

$().ready(function() {

	function log(event, data, formatted) {

		
	}
		
});

function checkForm()
{	
	var theForm = document.add_union_form;
	
	if( theForm.split_quantity.value=="" )
	{
		alert("需要拆散数不能为空");
	}
	else if( !isNum(theForm.split_quantity.value)||theForm.split_quantity.value><%=combinationCount%>||theForm.split_quantity.value<=0)
	{
		alert("需要拆散数超出允许范围");
	}
	else
	{
		parent.splitProduct(<%=psid%>,<%=s_pc_id%>,theForm.split_quantity.value)
	}
}

function isNum(keyW)
 {
	var reg=  /^(-[1-9]|[1-9]|(0[.])|(-(0[.])))[0-9]{0,}((\d{1,2})|[0-9]{0,})$/;
	return( reg.test(keyW) );
 } 
 

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top"><table width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		
<form name="spilt_union_form" method="get" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/return_step/returnStepSpiltUnion.action" onSubmit="return false">

<table width="98%" border="0" cellspacing="3" cellpadding="2">

  <tr>
    <td colspan="3" align="left" valign="middle" style="padding:10px;">
	<span style="font-weight:bold;color:#666666;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><span style="color:#990000"><%=storage_name%> - </span><%=detailPro.getString("p_name")%></span>	</td>
  </tr>
  <tr>
    <td width="66%" height="106" align="left" valign="top" style="padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="3">
	<%
	DBRow inSetProducts[] = productMgr.getProductsInSetBySetPid(s_pc_id);
	for (int i=0; i<inSetProducts.length; i++)
	{
		pids.append(inSetProducts[i].getString("pc_id"));
		
		if(i<inSetProducts.length-1)
		{
			pids.append(",");
		}
		//预计算下是否缺货，缺多少
		float sub_quantity = inSetProducts[i].get("quantity",0f);
	%>
      <tr>
        <td style="font-family:Arial, Helvetica, sans-serif">├  <%=inSetProducts[i].getString("p_name")%> x <span style="color:#CC0000"><%=sub_quantity%></span> 
			<input type="hidden" value="<%=sub_quantity%>" id="<%=inSetProducts[i].getString("pc_id")%>"/>
		</td>
      </tr>
	<%
	}
	%>
    </table>
    <script type="text/javascript">
     function spiltRetrunStepUnion()
	 {
	 	var submitIf = true;
	 	var pids = "<%=pids%>";
	 	var jspids = pids.split(",");

	 	if( typeof($("#split_quantity").val())!="undefined" && $("#split_quantity").val()=="" )
		{
			alert("功能残损拆散数不能为空");
		}
		else if( typeof($("#split_quantity").val())!="undefined" && isNum($("#split_quantity").val()) )
		{
			alert("功能残损拆散数不正确");
		}
		else if( typeof($("#split_quantity").val())!="undefined" && ($("#split_quantity").val()<0||$("#split_quantity").val()><%=combinationCount%>) )
		{
			alert("功能残损拆散数超过允许范围");
		}
		else if( typeof($("#split_package_quantity").val())!="undefined" && $("#split_package_quantity").val()=="" )
		{
			alert("外观残损拆散数不能为空");	
		}
		else if( typeof($("#split_package_quantity").val())!="undefined" && isNum($("#split_package_quantity").val()) )
		{
			alert("外观残损拆散数不正确");
		}
		else if( typeof($("#split_package_quantity").val())!="undefined" && ($("#split_package_quantity").val()<0||$("#split_package_quantity").val()><%=damaed_package_count%>) )
		{
			alert("外观残损拆散数超过允许范围");
		}
		else
		{
		 	for(var i=0;i<jspids.length;i++)
		 	{
		 		var c = $("[id*='"+jspids[i]+"_']");
		 		var count = 0;
		 		for(var j=0;j<c.length;j++)
		 		{
		 			count += parseFloat(c[j].value);
		 		}
		 		
		 		var all = parseFloat($("#"+jspids[i]).val())*(parseFloat($("#split_quantity").val())+parseFloat($("#split_package_quantity").val()));
		 		
		 		if(all!=count)
		 		{
		 			$("#td_"+jspids[i]).attr("bgcolor","red");
		 			submitIf = false; 
		 		}
		 		else
		 		{
		 			$("#td_"+jspids[i]).removeAttr("bgcolor","red");
		 		}
		 	}
		 	
		 	if(submitIf==true)
		 	{
		 		document.spilt_union_form.submit();
		 	}
		 }
	 }
    </script>
    </td>
    <td width="17%" align="left" valign="top" style="padding:10px;">
	
	<table width="145" border="0" cellpadding="1" cellspacing="0" bgcolor="#cccccc">
      <tr>
        <td>

			<table width="145" border="0" cellpadding="5" cellspacing="0" bgcolor="#f8f8f8">
		<%
	if (combinationCount>0)
	{
	%>
      <tr>
        <td width="545" style="font-size:12px;">功能残损可拆散 <span style="color:#990000;font-weight:bold"><%=combinationCount%></span> <%=detailPro.getString("unit_name")%></td>
        </tr>
      <tr>
        <td  style="font-size:12px;">需要拆散 
          <input type="text" name="split_quantity" id="split_quantity" style="width:40px;" value="0.0"> <%=detailPro.getString("unit_name")%>		 </td>
        </tr>
	<%
	}
	else
	{
	%>
			      <tr>
        <td  style="font-size:12px;color:#CC0000;background:#FFFFE1;">套装库存不足，不能拆散！
        	<input type="hidden" name="split_quantity" id="split_quantity" value="0">
        </td>
        </tr>
	<%
	}
	%>
	
	
    </table>		</td>
      </tr>
      
    </table></td>
	
	<td width="17%" align="left" valign="top" style="padding:10px;">
	
	<table width="145" border="0" cellpadding="1" cellspacing="0" bgcolor="#cccccc">
      <tr>
        <td>

			<table width="145" border="0" cellpadding="5" cellspacing="0" bgcolor="#f8f8f8">
		<%
	if (damaed_package_count>0)
	{
	%>
      <tr>
        <td width="545" style="font-size:12px;">外观残损可拆散 <span style="color:#990000;font-weight:bold"><%=damaed_package_count%></span> <%=detailPro.getString("unit_name")%></td>
        </tr>
      <tr>
        <td  style="font-size:12px;">需要拆散 
          <input type="text" name="split_package_quantity" id="split_package_quantity" style="width:40px;" value="0.0"> <%=detailPro.getString("unit_name")%>		 </td>
        </tr>
	<%
	}
	else
	{
	%>
			      <tr>
        <td  style="font-size:12px;color:#CC0000;background:#FFFFE1;">套装库存不足，不能拆散！	 
        <input type="hidden" value="0" name="split_package_quantity" id="split_package_quantity" style="width:40px;">
        </td>
        </tr>
	<%
	}
	%>
	
	
    </table>		</td>
      </tr>
      
    </table></td>
  </tr>
  <tr>
  	<td colspan="3">
  		<table width="98%" cellpadding="2" cellspacing="0">
  		<tr>
  			<td style="vertical-align: center;text-align: center;" class="right-title">&nbsp;</td>
  			<td style="vertical-align: center;text-align: center;" class="right-title">完好</td>
  			<td style="vertical-align: center;text-align: center;" class="right-title">功能残损</td>
  			<td style="vertical-align: center;text-align: center;" class="right-title">外观残损</td>
  			
  		</tr>
  		<%
  			for (int i=0; i<inSetProducts.length; i++)
			{
				//预计算下是否缺货，缺多少
				float sub_quantity = inSetProducts[i].get("quantity",0f);
			%>
		      <tr>
		        <td id="td_<%=inSetProducts[i].getString("pc_id")%>" align="left" style="font-family:Arial, Helvetica, sans-serif"><%=inSetProducts[i].getString("p_name")%></td>
		        <td align="center"><input type="text" name="<%=inSetProducts[i].getString("pc_id")%>_whole" id="<%=inSetProducts[i].getString("pc_id")%>_whole" style="width: 50px;" value="0.0"/></td>
		        <td align="center"><input type="text" name="<%=inSetProducts[i].getString("pc_id")%>_damaged" id="<%=inSetProducts[i].getString("pc_id")%>_damaged" style="width: 50px;" value="0.0"/></td>
		        <td align="center"><input type="text" name="<%=inSetProducts[i].getString("pc_id")%>_appearance" id="<%=inSetProducts[i].getString("pc_id")%>_appearance" style="width: 50px;" value="0.0"/></td>
		      </tr>
			<%
			}
			%>
  		</table>
  	</td>
  </tr>
</table>
<input type="hidden" name="ps_id" value="<%=psid%>"/>
<input type="hidden" name="pc_id" value="<%=s_pc_id %>"/>
<input type="hidden" name="s_pid" value="<%=s_pid%>"/>
<input type="hidden" name="pids" value="<%=pids.toString()%>"/>
</form>		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="68%" align="left" class="win-bottom-line" style="color:#999999;padding-left:10px;font-size:12px;">
	<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle">
	套装拆散后，套装库存减少，散件库存相应增加。</td>
    <td width="32%" align="right" class="win-bottom-line">
	<%
	if ( combinationCount>0 )
	{
	%>
      <input type="button" name="Submit2" value="拆散" class="normal-green" onClick="spiltRetrunStepUnion();">
<%
}
%>
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
	</td>
  </tr>
</table> 
</body>
</html>
