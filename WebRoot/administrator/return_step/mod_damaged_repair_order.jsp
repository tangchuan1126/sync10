<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.TransportOrderKey"%>
<%@page import="com.cwc.app.key.DamagedRepairKey"%>
<%@ include file="../../include.jsp"%> 
<%
DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
long repair_id = StringUtil.getLong(request,"repair_id");

DBRow damagedRepair = damagedRepairMgrZJ.getDetailDamagedRepair(repair_id);

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<script>
	function modTransport()
	{
		if($("#send_psid").val() == "")
		{
			alert("请选择转运仓库");
		}
		else if($("#receive_psid").val()=="")
		{
			alert("请选择接收仓库");
		}
		else if($("#address").val()=="")
		{
			alert("请填写交货地址！");
		}
		else if($("#linkman").val()=="")
		{
			alert("请填写联系人！");
		}
		else if($("#linkman_number").val()=="")
		{
			alert("必须留下联系方式！");
		}
		else
		{
			document.mod_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/damaged_repair/modDamagedRepair.action"
			document.mod_form.send_psid.value = $("#send_psid").val();
			document.mod_form.receive_psid.value = $("#receive_psid").val();		
			document.mod_form.repair_address.value = $("#address").val();
			document.mod_form.repair_linkman.value = $("#linkman").val();
			document.mod_form.linkman_phone.value = $("#linkman_number").val();
			document.mod_form.repair_waybill_name.value = $("#repair_waybill_name").val();
			document.mod_form.repair_waybill_number.value = $("#repair_waybill_number").val();
			
			document.mod_form.submit();	
		}
	}

	
	var keepAliveObj=null;
	function keepAlive()
	{
	 $("#keep_alive").load("../imgs/account.gif"); 
	
	 clearTimeout(keepAliveObj);
	 keepAliveObj = setTimeout("keepAlive()",1000*60*5);
	}
	
	function selectStorage()
	{
		var para = "ps_id="+$("#receive_psid").val();
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){

				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(data){
					$("#address").val(data.address);
					$("#linkman").val(data.contact);
					$("#linkman_number").val(data.phone);
				}
			});
	}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE4 {font-size: medium}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="keepAlive()">
			<form method="post" name="mod_form">
			<input type="hidden" name="repair_id" value="<%=repair_id%>"/>
			<input type="hidden" name="send_psid"/>
			<input type="hidden" name="receive_psid"/>
			<input type="hidden" name="repair_address"/>
			<input type="hidden" name="repair_linkman"/>
			<input type="hidden" name="linkman_phone"/>
			<input type="hidden" name="repair_waybill_name"/>
			<input type="hidden" name="repair_waybill_number"/>
			<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/transport/.html"/>
			</form>
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><br>
		<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						  <tr>
							<td align="left" style="font-family:'黑体'; font-size:25px;border-bottom:1px solid #999999;color:#000000;">
							请选择返修仓库与接收仓库
							</td>
						  </tr>
					  </table>
						<br/>
                        <br>
                        <table width="95%" align="center" cellpadding="0" cellspacing="0" style="padding-left:10px;">
							<tr valign="bottom">
							  <td align="left" class="STYLE1 STYLE2" style="padding:0px;"><p class="STYLE4">返修仓库：</p></td>
						      <td height="20%"  align="left" class="STYLE1 STYLE2" style="padding:0px;">
						      	<select name='select' id='send_psid' <%=damagedRepair.get("repair_status",0)!=DamagedRepairKey.READY?"disabled='disabled'":""%>>
                                <option value="">请选择...</option>
                                <%
									  for ( int i=0; i<treeRows.length; i++ )
									  {
									%>
                                <option value='<%=treeRows[i].getString("id")%>' <%=damagedRepair.get("send_psid",0l)==treeRows[i].get("id",0l)?"selected":""%>><%=treeRows[i].getString("title")%></option>
                                <%
									}
									%>
                              </select>
						      </td>
							</tr>
							<tr>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
							  <td  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" >&nbsp;</td>
						  </tr>
							<tr>
							  <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >目的仓库：</td>
						      <td align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" >
						      <select name='select' id='receive_psid' onChange="selectStorage()">
                                <option value="">请选择...</option>
                                <%
									  for ( int i=0; i<treeRows.length; i++ )
									  {
									%>
                                <option value='<%=treeRows[i].getString("id")%>' <%=damagedRepair.get("receive_psid",0l)==treeRows[i].get("id",0l)?"selected":""%>><%=treeRows[i].getString("title")%></option>
                                <%
									}
									%>
                              </select>
                              </td>
						  </tr>
						  	 <tr>
							  <td  align="left" colspan="2" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >交货地点：</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap"><input name="address" type="text" id="address" value="<%=damagedRepair.getString("repair_address")%>" style="width: 600px;"/></td>
						  </tr>
						  <tr>
							  <td  colspan="2" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >联系人：</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2"><input type="text" name="linkman" id="linkman" value="<%=damagedRepair.getString("repair_linkman")%>"/></td>
						  </tr>
						  <tr>
							  <td  colspan="2" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						  <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">联系电话：</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="linkman_number" id="linkman_number" value="<%=damagedRepair.getString("repair_linkman_phone")%>" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						   <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">货运公司：</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="repair_waybill_name" id="repair_waybill_name" value="<%=damagedRepair.getString("repair_waybill_name")%>" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
						   <tr>
						  	 <td width="10%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4">运单号：</td>
						      <td width="90%"  align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2" ><input type="text" name="repair_waybill_number" value="<%=damagedRepair.getString("repair_waybill_number")%>" id="repair_waybill_number" width="200px"/></td>
						  </tr>
						  <tr>
							  <td colspan="2" align="left" valign="middle" nowrap="nowrap" class="STYLE1 STYLE2 STYLE4" >&nbsp;</td>
						  </tr>
					  </table>
				
				    </td></tr>
				
				<tr>
					<td align="right" valign="bottom">				
						<table width="100%">
							<tr>
								<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
								  <input type="button" name="Submit2" value="保存" class="normal-green" onClick="modTransport();">
								  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
</body>
</html>
