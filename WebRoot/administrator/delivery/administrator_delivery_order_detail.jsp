<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderSaveKey"%>
<%@page import="com.cwc.app.exception.deliveryOrder.NoExistDeliveryOrderException"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
try
{
	long purchase_id = Long.parseLong(StringUtil.getString(request,"purchase_id","0"));
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
	String filename = StringUtil.getString(request,"filename");
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
	DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(Long.toString(delivery_order_id),5);//
	int number = 0;
	boolean edit = true;
	DBRow[] delivery_order_details;
	if(delivery_order == null)
	{
		DBRow[] delivery_orders = deliveryMgrZJ.getDeliveryOrders(purchase_id);
		number = delivery_orders.length+1;

		DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
		if(purchase ==null)
		{
			throw new NoExistDeliveryOrderException();
		}
		delivery_order = new DBRow();
		delivery_order.add("delivery_address",purchase.getString("delivery_address"));
		delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
		delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
		delivery_order.add("delivery_order_status",DeliveryOrderKey.READY);
		delivery_order.add("delivery_order_number","");
		
	}
	else
	{
		purchase_id = delivery_order.get("delivery_purchase_id",0l);
		if(delivery_order.get("delivery_order_status",0l)!=DeliveryOrderKey.READY)
		{
			edit = false;
		}
	}
	
	DBRow supplier; 
	try
	{
		supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id)).getString("supplier")));
	}
	catch(NumberFormatException e)
	{
		supplier = new DBRow();
	}
	
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单<%=delivery_order.getString("delivery_order_number")%></title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

<script type="text/javascript" src="../js/scrollto/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
	
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />

<link rel="stylesheet" href="../js/poshytip/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
<script type="text/javascript" src="../js/poshytip/jquery.poshytip.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.min.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />

<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />



<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>



<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" /> 

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<script type="text/javascript">
	function createWaybill(delivery_order_id)
	{
		
		$.artDialog.open("administrator_delivery_waybill.html?delivery_order_id="+delivery_order_id, {title: '交货单创建运单',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function uploadInvoice(delivery_order_id)
	{
		$.artDialog.open("delivery_upload_invoice.html?delivery_order_id="+delivery_order_id, {title: '交货单上传发票',width:'670px',height:'400px', lock: true,opacity: 0.3});
	}
	
	function printWayBill(id,print_page)
	{
		 
		 var uri = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/"+print_page+"?id="+id+"&type=D";
		
		 $.artDialog.open(uri, {title: '打印运单',width:'960px',height:'500px', lock: true,opacity: 0.3});
   	}
   	
	function onloadPrint(isPrint)
	{
		if(isPrint>0)
		{
			visionariPrinter.PRINT_INIT("交货单");
			
			visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
			visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../delivery/print_delivery_order_detail.html?purchase_id=<%=purchase_id%>&delivery_order_id=<%=delivery_order_id%>");
				
			visionariPrinter.PREVIEW();
		}
			
	}					
	
	function print()
	{
		//document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addPrintDeliveryOrder.action";
				document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
				document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
				document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
				document.save_deliveryOrder_form.submit();
	}
		function searchPurchaseDetail()
		{
			if($("#search_key").val().trim()=="")
			{
				alert("请输入要关键字")
			}
			else
			{
				document.search_form.purchase_name.value=$("#search_key").val();
				document.search_form.submit();
			}	
		}
	
	function uploadDeliveryOrderDetail()
	{
		tb_show('上传交货单','delivery_upload_excel.html?delivery_order_id='+<%=delivery_order_id%>+'&purchase_id=<%=purchase_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function inComingWareHouse()
	{
		tb_show('交货单入库','delivery_incoming_log.html?delivery_order_id='+<%=delivery_order_id%>+'&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function virtualDeliveryOrder(filename,purchase_id)
	{
		document.virtual_delivery_order_form.filename.value = filename;
		document.virtual_delivery_order_form.purchase_id.value = purchase_id;
		
		document.virtual_delivery_order_form.submit();
		
		tb_remove();
		
	}
	
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
	
	function downloadDeliveryOrder()
	{
		var para = "delivery_order_id=<%=delivery_order_id%>&purchase_id=<%=purchase_id%>";
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/downloadDeliveryOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function saveDeliveryOrder()
	{
			if(confirm("确定保存对此交货单的修改吗？"))
			{
				<%
					if(delivery_order_id !=0)
					{
				%>
						document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrder.action";
				<%	
					}
					else
					{
				%>
					
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addDeliveryOrder.action";
				<%	
					}
				%>
				
				document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
				document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
				document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
				document.save_deliveryOrder_form.submit();
			}
	}
	
	function autoComplete(obj)
	{
		obj.autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		{
			minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
		}); 
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="delivery_order_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/getDeliveryOrderDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("[");
		var error2 = error1[1].split("]");	
		
		return error2[0];
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
</script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../administrator/imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
</style>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
function openApplyMoneyInsert() {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_delivery_insert.html?delivery_order_id=<%=delivery_order.getString("delivery_order_id")%>&association_id=<%=delivery_order.getString("delivery_order_number").equals("")?"P"+purchase_id+"-"+number:delivery_order.getString("delivery_order_number")%>";
	$.artDialog.open(url, {title: '申请费用',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showTransit(){
	var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_transit_update.html?delivery_order_id=<%=delivery_order.getString("delivery_order_id")%>";
	$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'550px', lock: true,opacity: 0.3});
}
function showFreight(){
	var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_freight_update.html?delivery_order_id=<%=delivery_order.getString("delivery_order_id")%>";
	$.artDialog.open(url, {title: '交货单运单信息',width:'700px',height:'550px', lock: true,opacity: 0.3});
}
function showWayout(){
	var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_wayout_update.html?delivery_order_id=<%=delivery_order.getString("delivery_order_id")%>";
	$.artDialog.open(url, {title: '交货单报关退税信息',width:'700px',height:'550px', lock: true,opacity: 0.3});
}
function showFreightCost(){
	var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/setDeliveryFreight.html?delivery_order_id=<%=delivery_order.getString("delivery_order_id")%>&fr_id=<%=delivery_order.getString("fr_id")%>&association_id=<%=delivery_order.getString("delivery_order_number").equals("")?"P"+purchase_id+"-"+number:delivery_order.getString("delivery_order_number")%>";
	$.artDialog.open(url, {title: '交货单运费信息',width:'700px',height:'550px', lock: true,opacity: 0.3});
}
function showStockIn(delivery_order_id) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/delivery/delivery_stock_temp_in.html?delivery_order_id="+delivery_order_id;
	$.artDialog.open(url, {title: '交货单入库',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
function showDrawback(delivery_order_id) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/delivery/deliverySetDrawback.html?delivery_order_id="+delivery_order_id;
	$.artDialog.open(url, {title: '交货单退税',width:'960px',height:'200px', lock: true,opacity: 0.3});
}

function applyFreigth(amount) {
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/financial_management/apply_money_delivery_insert.html?delivery_order_id=<%=delivery_order_id%>&association_id=<%=delivery_order.getString("delivery_order_number").equals("")?"P"+purchase_id+"-"+number:delivery_order.getString("delivery_order_number")%>&amount="+amount+"&subject=1";
	$.artDialog.open(url, {title: '申请运费',width:'700px',height:'500px', lock: true,opacity: 0.3});
}
</script>
</head>

<body onload="onloadPrint(<%=isPrint%>)">

<div class="demo">
  <div align="center" id="tabs" width="95%">	
	<div id="master" align="left" style="padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">
			交货单信息
			&nbsp;&nbsp;<a href="#" onClick="showTransit()">明细修改</a>
			<%
			  	if(delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.READY)
			  	{
			  %>
			  	&nbsp;&nbsp;<a href="#" onClick="uploadDeliveryOrderDetail()">上传</a>
			  <%
			  	}
			  %>
			  	&nbsp;&nbsp;<a href="#" onClick="downloadDeliveryOrder()">下载</a>
			  	&nbsp;&nbsp;<a href="#" onClick="print()">打印</a>
			  <% 
				if(delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.INTRANSIT)
				{
			  %>
			  		&nbsp;&nbsp;<a href="#" onClick="showStockIn(<%=delivery_order_id%>)">交货单入库</a>
			  <%
				}
			  %>
			  		&nbsp;&nbsp;<a href="#" onClick="createWaybill(<%=delivery_order.get("delivery_order_id",0l)%>)">生成运单</a>
			  		&nbsp;&nbsp;<a href="#" onClick="uploadInvoice(<%=delivery_order.get("delivery_order_id",0l)%>)">上传发票</a>
				  		<%
				  			DBRow shippingCompany = expressMgr.getDetailCompany(delivery_order.get("sc_id",0l));
				  			if(shippingCompany !=null)
				  			{
				  		%>
				  		&nbsp;&nbsp;<input type="button" class="long-button" value="打印运单" onclick="printWayBill(<%=delivery_order.getString("delivery_order_id")%>,'<%=shippingCompany.getString("internal_print_page")%>')"/>
				  		<%	
				  			}
				  		%>
		</legend>
		<table width="70%" align="center" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="30%" align="right" nowrap="nowrap" height="25">采购单号：</td>
					<td width="30%" align="left" height="25"><a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/purchase_detail.html?purchase_id=<%=purchase_id%>"><%=purchase_id%></a></td>
					<td align="30%" height="25" valign="middle" nowrap="nowrap"><div width="100%" align="right">交货批次：</div></td>
					<td align="30%" height="25"><%=delivery_order.getString("delivery_order_number").equals("")?"P"+purchase_id+"-"+number:delivery_order.getString("delivery_order_number")%></td>
				</tr>
				<tr>
					<td valign="middle" align="right" height="25" nowrap="nowrap">联系人：</td>
					<td align="left" valign="middle" height="25"><%=delivery_order.getString("delivery_linkman") %></td>
					<td valign="middle" align="right" height="25" nowrap="nowrap">联系电话：</td>
					<td height="25"><%=delivery_order.getString("delivery_linkman_phone") %></td>
				</tr>
				<tr>
					<td valign="middle" align="right" height="25" nowrap="nowrap">交货仓库：</td>
					<td align="left" valign="middle" height="25"><%=catalogMgr.getDetailProductStorageCatalogById(delivery_order.get("delivery_psid",0l)).getString("title")%></td>
					<td valign="middle" align="right" height="25" nowrap="nowrap">预计到达日期：</td>
					<td height="25"><%=delivery_order.getString("arrival_eta") %></td>
				</tr>
				<tr>
					<td align="right" height="25" colsapn="3">供应商：</td>
					<td align="left" height="25" colspan="3"><%=supplier.getString("sup_name") %></td>
				</tr>
				<tr>
					<td align="right" valign="middle" nowrap="nowrap" height="25">交货地址：</td>
					<td align="left" valign="middle" height="25" colspan="7"><%=delivery_order.getString("delivery_address") %></td>
				</tr>
				<tr>
					<td valign="middle" align="right" height="25">备注：</td>
					<td height="25" colspan="7"><%=delivery_order.getString("remark") %></td>
				</tr>
					   
			</table>
		</fieldset>
		<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单运单信息&nbsp;&nbsp;<a href="#" onClick="showFreight()">运单修改</a></legend>
		<table style="padding-left: 15px;" border="0" align="center" cellpadding="0" cellspacing="0" width="70%">
			<tr>
				<td width="16%" align="right" nowrap="nowrap" height="25">货运公司：</td>
			  	<td width="200" align="left" height="25" colspan="3"><%=delivery_order.getString("waybill_name") %></td>
			  	<td width="20%" align="right" height="25" nowrap="nowrap">运单号：</td>
				<td width="20%" align="left" height="25" colspan="3"><%=delivery_order.getString("waybill_number") %></td>
			</tr>
			<tr>
				<td width="16%" align="right" nowrap="nowrap" height="25">运输方式：</td>
			  	<td width="200" align="left" height="25" colspan="3"><%=transportWayKey.getStatusById(delivery_order.get("transportby",0)) %></td>
			  	<td width="20%" align="right" height="25" nowrap="nowrap">承运公司：</td>
				<td width="20%" align="left" height="25" colspan="3"><%=delivery_order.getString("carriers") %></td>
			</tr>
			<tr>
				<td width="10%" valign="middle" align="right" height="25">发货国：</td>
				<td width="15%">&nbsp;
 <%
					DBRow transport_send_country_row = transportMgrLL.getCountyById(delivery_order.getString("begin_country"));
					out.println(transport_send_country_row==null?"":transport_send_country_row.getString("c_country"));
 %>
				</td>
				<td width="10%" valign="middle" align="right">发货港：</td>
				<td width="15%">&nbsp;<%=delivery_order.getString("begin_port")%></td>
				<td width="10%" valign="middle" align="right" height="25">目的国：</td>
				<td width="15%">&nbsp;
 <%
					DBRow transport_receive_country_row = transportMgrLL.getCountyById(delivery_order.getString("end_country"));
					out.println(transport_receive_country_row==null?"":transport_receive_country_row.getString("c_country"));
 %>
 
				</td>
				<td width="10%" valign="middle" align="right">目的港：</td>
				<td width="15%">&nbsp;<%=delivery_order.getString("end_port")%></td>
			</tr>
		</table>
		<%
			DBRow[] rows = freightMgrLL.getDeliveryFreightCostByDeliveryId(delivery_order.getString("delivery_order_id"));
			DBRow deliveryRow = deliveryMgrLL.getDeliveryOrderById(delivery_order.getString("delivery_order_id"));
		%>
		<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
		     <tr>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">小计</th>	
		     </tr>
		     <%
	    	 float sum_price = 0;
		     if(rows!=null){
			     for(int i = 0;i<rows.length;i++)
			     {
			    	 DBRow row = rows[i];
			    	 long dfc_id = row.get("dfc_id",0l);
		    		 double dfc_unit_count = row.get("dfc_unit_count",0d);
		    		 String dfc_project_name = "";
		    		 String dfc_way = "";
		    		 String dfc_unit = "";
		    		 String dfc_currency = "";
		    		 double dfc_unit_price = 0;
		    		 double dfc_exchange_rate = 0;
			    	 if(dfc_id != 0) {//以前有数据
			    		 dfc_project_name = row.getString("dfc_project_name");
			    		 dfc_way = row.getString("dfc_way");
			    		 dfc_unit = row.getString("dfc_unit");
			    		 dfc_unit_price = row.get("dfc_unit_price",0d);
			    		 dfc_currency = row.getString("dfc_currency");
			    		 dfc_exchange_rate = row.get("dfc_exchange_rate",0d);
			    	 }
		      %>
		     <tr height="30px" style="padding-top:3px;">
		        <td valign="middle" style="padding-top:3px;">
		        	<input type="hidden" name="dfc_id" id="dfc_id" value="<%=dfc_id %>"/>
		        	<input type="hidden" name="dfc_project_name" id="dfc_project_name" value="<%=dfc_project_name %>"/>
		        	<input type="hidden" name="dfc_way" id="dfc_way" value="<%=dfc_way %>"/>
		        	<input type="hidden" name="dfc_unit" id="dfc_unit" value="<%=dfc_unit %>"/>
					<%=dfc_project_name %>	
		        </td>
		        <td valign="middle" nowrap="nowrap" >       
		      		<%=dfc_way %>
		        </td>
		        <td valign="middle">
					<%=dfc_unit %>
		        </td>
		        <td >
					<%=dfc_unit_price %>
		        </td>
		         <td >
					<%=dfc_currency%>
		        </td>
		        <td >
					<%=dfc_exchange_rate %>
		        </td>
		        <td valign="middle" >       
					<%=dfc_unit_count %>
		        </td>
		        <td valign="middle" nowrap="nowrap">       
					<%= MoneyUtil.round(dfc_unit_price*dfc_exchange_rate*dfc_unit_count,2) %>
		        </td>
		     </tr>
		     
		     <%
		     		sum_price += dfc_unit_price*dfc_exchange_rate*dfc_unit_count;
		    	 }
			 %>
			 <tr height="30px">
			 
			 	<td colspan="2">
			 	<%
			 		for(int j=0; j<applyMoneys.length; j++){
			 			out.println("<a href=\"javascript:void(0)\" onClick=\"goApplyFundsPage('"+applyMoneys[j].get("apply_id",0)+"')\">F"+applyMoneys[j].get("apply_id",0)+"</a>申请资金"+applyMoneys[j].get("amount",0f)+applyMoneys[j].getString("currency")+"<br/>");
			 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByBusiness(Long.toString(delivery_order_id),5);
			 			if(applyTransferRows.length>0) {
			 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
			 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
					  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
					  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
					  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
					  				int moneyStatus = applyTransferRows[ii].get("status",0);
					  				if(moneyStatus != 0 )
					  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+"<br/>");
					  				else
					  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+"<br/>");
			 					}
				  			}
			 			}
			 		}
			 	%>
			 	</td>
			 	<td colspan="5" align="right">总计</td>
			 	<td nowrap="nowrap"><%=sum_price %></td>
			 </tr>
			 <%
		     }
		     %>
		     
		  </table>
		  <table width="100%" align="center"><tr><td align="right">
	     		&nbsp;&nbsp;<input name="button" type="button" value="运费修改" onClick="showFreightCost()" class="long-button"/>
	     	<%
	     		if(delivery_order.get("stock_in_set",1) == 2) {
	     	%>
	     			&nbsp;&nbsp;<input name="button" type="button" value="申请运费" onClick="applyFreigth('<%=sum_price %>')" class="long-button"/>
	     	<%
	     		}
	     	%>
		  </td></tr></table>
		  </fieldset>
		  <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单进出口信息</legend>
		  <table style="padding-left: 2px;" width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" colspan="4" height="25">
					&nbsp;&nbsp;
					<%=invoiceKey.getInvoiceById(String.valueOf(delivery_order.get("invoice",1)))%>
					&nbsp;&nbsp;
					<%=declarationKey.getStatusById(delivery_order.get("declaration",1))%>
					&nbsp;&nbsp;
					<%=clearanceKey.getStatusById(delivery_order.get("clearance",1))%>
					&nbsp;&nbsp;
					<%=drawbackKey.getDrawbackById(delivery_order.get("drawback",1))%>
					&nbsp;&nbsp;<input name="button" type="button" value="报关退税修改" onClick="showWayout()" class="long-button"/>
					&nbsp;&nbsp;<input name="button" type="button" value="退税申请" onClick="showDrawback(<%=delivery_order.get("delivery_order_id",0l) %>)" class="long-button"/>
				</td>
			</tr>
		</table>
		</fieldset>
		<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单明细口信息</legend>
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table id="gridtest"></table>
			<div id="pager2"></div> 
			
			<script type="text/javascript">
					var	lastsel;
					var mygrid = $("#gridtest").jqGrid({
							sortable:true,
							url:'dataDeliveryOrderDetails.html',//dataDeliveryOrderDetails.html
							datatype: "json",
							width:parseInt(document.body.scrollWidth*0.95),
							height:255,
							autowidth: false,
							shrinkToFit:true,
							postData:{delivery_order_id:<%=delivery_order_id%>,purchase_id:<%=purchase_id%>},
							jsonReader:{
						   			id:'delivery_order_detail_id',
			                        repeatitems : false
			                	},
						   	colNames:['delivery_order_detail_id','商品名','单位','采购单目前已交货','订购量','到达数量','本次发货','重量',<%=delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.FINISH?"'运费'":"'估算运费'"%>,'所在箱号','delivery_order_id'], 
						   	colModel:[ 
						   		{name:'delivery_order_detail_id',index:'delivery_order_detail_id',hidden:true,sortable:false},
						   		{name:'product_name',index:'product_name',editable:<%=edit%>,align:'left'},
						   		{name:'unitname',index:'unitname',align:'center',width:60},
						   		{name:'reapcount',index:'reapcount',sortable:false,align:'center'},
						   		{name:'purchasecount',index:'purchasecount',sortable:false,align:'center'}, 
						   		{name:'delivery_reap_count',index:'delivery_reap_count',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
						   		{name:'delivery_count',index:'delivery_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},editable:<%=edit%>,sortable:false,align:'center'},
						   		{name:'weight',index:'weight',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","}},
						   		{name:'freight_cost',index:'freight_cost',align:'center',width:60,formatter:'number',formatoptions:{decimalPlaces:2,thousandsSeparator: ","},sortable:false},
						   		{name:'delivery_box',index:'delivery_box',editable:<%=edit%>},
						   		{name:'delivery_order_id',index:'delivery_order_id',editable:<%=edit%>,editoptions:{readOnly:true,defaultValue:<%=delivery_order_id%>},hidden:true,sortable:false}	
						   		], 
						   	rowNum:-1,//-1显示全部
						   	pgtext:false,
						   	pgbuttons:false,
						   	mtype: "GET",
						   	gridview: true, 
							rownumbers:true,
						   	pager:'#pager2',
						   	sortname: 'delivery_order_detail_id', 
						   	viewrecords: true, 
						   	sortorder: "asc", 
						   	cellEdit: true, 
						   	cellsubmit: 'remote',
		
						   	afterEditCell:function(id,name,val,iRow,iCol)
						   				  { 
						   					if(name=='product_name') 
						   					{
						   						autoComplete(jQuery("#"+iRow+"_product_name","#gridtest"));
						   					}
						   				  }, 
						   	afterSaveCell:function(rowid,name,val,iRow,iCol)
						   				  { 
						   				  	if(name=='product_name') 
						   					{
						   						ajaxModProductName(jQuery("#gridtest"),rowid);
						   				  	}
						   				  }, 
						   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action',
						   	errorCell:function(serverresponse, status)
						   				{
						   					alert(errorMessage(serverresponse.responseText));
						   				},
						   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action'
						   	}); 		   	
					   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:<%=edit%>,del:<%=edit%>,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
					   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"",title:"自定义显示字段",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
					   
			</script>
				</td>
			</tr>
		</table>
		</fieldset>
		</div>
	</div>
</div>
</div>

<form name="download_form" method="post"></form>

<form action="" method="post" name="save_deliveryOrder_form">
	<input type="hidden" name="delivery_order_id" value="<%=delivery_order_id%>"/>
	<input type="hidden" name="waybill_number"/>
	<input type="hidden" name="waybill_name"/>
	<input type="hidden" name="arrival_eta"/>
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="filename" value="<%=filename%>"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>"/>
	<input type="hidden" name="isPrint" value="1"/>
</form>

<form action="administrator_delivery_order_detail.html" name="virtual_delivery_order_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="filename"/>
</form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="delivery_order_id"/>
</form>

<script type="text/javascript">
	$("#eta").date_input();
	
	function intransitDelivery(delivery_order_id,number)
	{
		if(confirm("你确定"+number+"的货物已发送了？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/intransitDelivery.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.href="administrator_delivery_order_detail.html?delivery_order_id="+delivery_order_id;
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(delivery_order_id,number)
	{
		if(confirm("确定申请"+number+"交货单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/applicationApproveDelivery.action";
			document.applicationApprove_form.delivery_order_id.value = delivery_order_id;
			document.applicationApprove_form.submit();
		}
	}
</script>

<script>
$("#tabs").tabs({
	cache: true,
	spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
	cookie: {expires: 30000},
	load: function(event, ui) {onLoadInitZebraTable();}	
});
</script>	
</body>
</html>
<%
	}
	catch(NoExistDeliveryOrderException e)
	{
		out.print("该交货单不存在，请查看其他交货单");
	}
%>
