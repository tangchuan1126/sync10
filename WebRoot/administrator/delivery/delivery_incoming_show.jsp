<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@ include file="../../include.jsp"%> 
<%
 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	String[] file=new String[2];
 	DBRow[] rows=null;
 	try
 	{
 		file = deliveryWarehouseMgrZJ.uploadIncoming(request);
 		rows = deliveryWarehouseMgrZJ.addCompareDeliveryWareHouse(file[0],Long.parseLong(file[1]));
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.csv文件');window.history.go(-1)</script>");
 	}
 	catch(FileException e)
 	{
 		out.print("<script>alert('上传失败，请重试');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../../common.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>

<table width="100%" border="0" height="100%">
	<tr>
		<td align="center" valign="top">
			<span style="font-family:'黑体'; font-size:25px;color:#000000 ">
			  采购单号：<%=file[1]%></span>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
			  <tr>
				<th width="10%" height="68" class="right-title " style="vertical-align: center;text-align: left;">商品条码</th>
				<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">到达数量</th>
			  </tr>
			  <%
				if(rows!=null)
				{
					for(int i=0;i<rows.length;i++)
					{							   
						DBRow product = productMgr.getDetailProductByPcode(rows[i].getString("dw_product_barcod"));
			  %>
				  <tr align="center" valign="middle" 
					<%
						boolean colorchange = false;
						if(product==null)
						{
							submit = false;
							msg = "入库文件内有未知商品";
							colorchange = true;
						}
						
						if(rows[i].getString("dw_count").equals("")||rows[i].getString("dw_count").startsWith("-")||!rows[i].getString("reap_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|\\d*$"))
						{
							submit = false;
							msg = "商品到货数量异常";
							colorchange = true;
						}
						
						if(colorchange)
						{
							out.print(" bgcolor='#FFC1C1'");
						}
					  %>>
					<td height="30" align="left" style="border-bottom: 1px solid #999999;padding-left:10px;"><%=rows[i].getString("dw_product_barcod")%></td>
					<td align="center" style="border-bottom: 1px solid #999999;padding-left:10px;"><%=rows[i].getString("dw_count") %></td>
				  </tr>
			  <%
					}
				}
			  %>
				
		  </table>
	  </td>
	</tr>
	<tr>
		<td align="right" valign="bottom">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#999999"><%=msg%></span>
					<%
						}
					%>	         
					</td>
					<td align="right" valign="middle" class="win-bottom-line">		
					<%
						if(submit)
						{
					%>
							<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSavePurchaseDetail()"/>
					<%
						}
					%>		
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>		</td>
				</tr>
			</table>
	  </td>
	</tr>
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/savePurchaseDetail.action" name="savePurchasedetail_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=file[0]%>"/>
	<input type="hidden" name="purchase_id" id="purchase_id" value="<%=file[1]%>"/>
</form>
<script type="text/javascript">
	
	function ajaxSavePurchaseDetail()
	{
		if(<%=submit%>)
		{
			var tempfilename = $("#tempfilename").val();
			var purchase_id = $("#purchase_id").val();	
			var machine_id = $("#machine_id").val();
			var para = "tempfilename="+tempfilename+"&purchase_id="+purchase_id+"&machine_id="+machine_id;
			
			
			$.blockUI.defaults = {
				css: { 
					padding:        '10px',
					margin:         0,
					width:          '200px', 
					top:            '45%', 
					left:           '40%', 
					textAlign:      'center', 
					color:          '#000', 
					border:         '3px solid #aaa',
					backgroundColor:'#fff'
				},
				
				// 设置遮罩层的样式
				overlayCSS:  { 
					backgroundColor:'#000', 
					opacity:        '0.8' 
				},
		
		    centerX: true,
		    centerY: true, 
			
				fadeOut:  2000
			};
			
			$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
			
			
			
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/incomingPurchase.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(db){
					alert("提交失败，请从新上传！");
				},
				
				success: function(date){
					if (date["close"])
					{
						
						$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
						parent.closeWin();
					}
					else
					{
						alert(date["error"]);
					}
				}
			});
		}
		else
		{
			alert("入库单有误，请检查");
		}
	}
</script>

</body>
</html>



