<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	long purchase_id = Long.parseLong(StringUtil.getString(request,"purchase_id","0"));
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
	String filename = StringUtil.getString(request,"filename");
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
	int number = 0;
	if(delivery_order == null)
	{
		DBRow[] delivery_orders = deliveryMgrZJ.getDeliveryOrders(purchase_id);
		number = delivery_orders.length+1;

		DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
		
		delivery_order = new DBRow();
		delivery_order.add("delivery_address",purchase.getString("delivery_address"));
		delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
		delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
		delivery_order.add("delivery_order_status",DeliveryOrderKey.READY);
		delivery_order.add("delivery_order_number","");
	}
	else
	{
		purchase_id = delivery_order.get("delivery_purchase_id",0l);
		
	}
	
	DBRow supplier; 
	try
	{
		supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id)).getString("supplier")));
	}
	catch(NumberFormatException e)
	{
		supplier = new DBRow();
	}
	
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单<%=delivery_order.getString("delivery_order_number")%></title>



<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/plugins/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.src.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />


<style>
.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}

<!--<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/getDeliveryOrderDetailsJSON.action -->
</style>
<script type="text/javascript">
	function autoComplete(obj)
	{
		obj.autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		{
			minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
		}); 
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	
	function ajaxModProductUnitName(p_name,obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="p_name="+p_name;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getDetailProductByPnameJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(date)
				{
					unit_name = date["unit_name"];
					obj.setCell(rowid,col,unit_name);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("(");
		var error2 = error1[1].split(")");	
		
		return error2[0];
	}
</script>

</head>

<body onload="">
<div align="center">

	<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					scroll: 1,
					sortable:true,
					url:'dataDeliveryOrderDetails.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					weight:1000,
					height:200,
					autowidth: false,
					postData:{delivery_order_id:<%=delivery_order_id%>,purchase_id:<%=purchase_id%>},
					jsonReader:{
				   			id:'delivery_order_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['delivery_order_detail_id','商品名','单位','采购单目前已交货','订购量','到达数量','本次发货','所在箱号','delivery_order_id'], 
				   	colModel:[ 
				   		{name:'delivery_order_detail_id',index:'delivery_order_detail_id',hidden:true,sortable:false},
				   		{name:'product_name',index:'product_name',editable:true,width:300},
				   		{name:'unitname',index:'unitname',width:50,align:'center'},
				   		{name:'reapcount',index:'reapcount',sortable:false,width:100},
				   		{name:'purchasecount',index:'purchasecount',sortable:false,width:55}, 
				   		{name:'delivery_reap_count',index:'delivery_reap_count',sortable:false,width:55},
				   		{name:'delivery_count',index:'delivery_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true,minValue:0.5},editable:true,sortable:false,width:70},
				   		{name:'delivery_box',index:'delivery_box',editable:true},
				   		{name:'delivery_order_id',index:'delivery_order_id',editable:true,editoptions:{readOnly:true,defaultValue:<%=delivery_order_id%>},hidden:true,sortable:false}	
				   		], 
				   	rowNum:15,//-1显示全部
				   	mtype: "POST",
				   	pager:'#pager2',
				   	sortname: 'delivery_order_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='product_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_product_name","#gridtest"));
				   					}
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	if(name=='product_name') 
				   					{
				   						ajaxModProductUnitName(val,jQuery("#gridtest"),rowid,"unitname");
				   				  	}
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:true,del:true,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"Columns",title:"Reorder Columns",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
	</script>
</div>
<form name="download_form" method="post"></form>

<form action="" method="post" name="save_deliveryOrder_form">
	<input type="hidden" name="delivery_order_id" value="<%=delivery_order_id%>"/>
	<input type="hidden" name="waybill_number"/>
	<input type="hidden" name="waybill_name"/>
	<input type="hidden" name="arrival_eta"/>
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="filename" value="<%=filename%>"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>"/>
</form>

<form action="administrator_delivery_order_detail.html" name="virtual_delivery_order_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="filename"/>
</form>

<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="delivery_order_id"/>
</form>
</body>
</html>

