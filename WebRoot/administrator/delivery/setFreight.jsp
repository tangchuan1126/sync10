<%@ page language="java"  pageEncoding="utf-8"%>

<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(20);
	String delivery_order_id = StringUtil.getString(request,"delivery_order_id");
	String association_id = StringUtil.getString(request,"association_id");
	String fr_id = StringUtil.getString(request,"fr_id");
	DBRow[] rows = freightMgrLL.getDeliveryFreightCostByDeliveryId(delivery_order_id);//更新数据
	DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(delivery_order_id,5);//
	DBRow deliveryRow = deliveryMgrLL.getDeliveryOrderById(delivery_order_id);
%>
<html>
<head>
<title>无标题文档</title>
	<link href="../comm.css" rel="stylesheet" type="text/css"/>
	<script language="javascript" src="../../common.js"></script>

	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	<script type="text/javascript" src="../js/select.js"></script>

	<script src="../js/zebra/zebra.js" type="text/javascript"></script>
	<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

	<link type="text/css" href="../js/jquery/ui/themes/base/ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/jquery/ui/ui.tabs.js"></script>
	<link type="text/css" href="../js/tabs/demos.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		init();
	});

	function setFreigth() {
		freight_form.submit();
	}
	
	function setFreightCost(fc_id,fc_project_name,fc_way,fc_unit) {
		addRow(fc_id,fc_project_name,fc_way,fc_unit);
		$('#changed').val('2');
	}
	
	function setRate(obj) {
		if(obj.value == 'RMB') {
			document.getElementsByName("dfc_exchange_rate")[$(obj).attr('index')].value = "1.0";
		}else {
			document.getElementsByName("dfc_exchange_rate")[$(obj).attr('index')].value = "0.0";
		}
	}
	
    function addRow(fc_id,fc_project_name,fc_way,fc_unit) {
    	var index = document.getElementById("tables").rows.length;
    	var row = document.getElementById("tables").insertRow(index);
    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='dfc_id' id='dfc_id'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='dfc_project_name' id='dfc_project_name' value='"+fc_project_name+"'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='dfc_way' id='dfc_way' value='"+fc_way+"'>";
    	add1=row.insertCell(0).innerHTML += "<input type='hidden' name='dfc_unit' id='dfc_unit' value='"+fc_unit+"'>";
    	add1=row.insertCell(0).innerHTML += fc_project_name;
		var add2=row.insertCell(1).innerHTML = fc_way;
		var add3=row.insertCell(2).innerHTML = fc_unit;
		var add4=row.insertCell(3).innerHTML = "<input type='text' size='8' name='dfc_unit_price' id='dfc_unit_price' value='0.0'>";
		var add5=row.insertCell(4).innerHTML = "<select id='dfc_currency' index='"+(index-2)+"' name='dfc_currency' onchange='setRate(this)'><option value='RMB'>RMB</option><option value='USD'>USD</option><option value='HKD'>HKD</option></select>";
		var add6=row.insertCell(5).innerHTML = "<input type='text' size='8' name='dfc_exchange_rate' id='dfc_exchange_rate' value='1.0'>";
		var add7=row.insertCell(6).innerHTML = "<input type='text' size='8' name='dfc_unit_count' id='dfc_unit_count' value='0.0'>";
		var add8=row.insertCell(7).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
    }

    function deleteRow(input){  
        var s=input.parentNode.parentNode.rowIndex;
        document.getElementById("tables").deleteRow(s); 
        $('#changed').val('2');
        //var num=document.getElementById("tables").rows.length;  
	}
    
	function selectFreigthCost() {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_cost/freight_cost_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function setFreigthSubmit() {
		$('#stock_in_set').val('2');
		freight_form.submit();
	}

	function cancelFreigthSubmit() {
		$('#stock_in_set').val('1');
		freight_form.submit();
	}
</script>
</head>

<body onload="onLoadInitZebraTable()">
<form name="freight_form" id="freight_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliverySetFreightAction.action">
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/setDeliveryFreight.html?delivery_order_id=<%=delivery_order_id%>&updated=1"/>
	<input type="hidden" name="delivery_id" id="delivery_id" value="<%=delivery_order_id %>">
	<input type="hidden" name="changed" id="changed" value="1">
	
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
			<legend style="font-size:15px;font-weight:normal;color:#999999;">货运费用信息</legend>
			<table id="tables" width="98%" border="0" align="center"  cellpadding="0" cellspacing="0" style="padding-top: 17px" class="zebraTable" id="stat_table">
		     <tr>
		     	<td align="right" colspan="8" height="30px">
		     		<input type="button" class="long-button" name="selectFreightCost" value="选择运费项目" onclick="selectFreigthCost()">
		     	</td>
		     </tr>
		     <tr>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">项目名称</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">计费方式</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单位</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">单价</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">币种</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">汇率</th>
		       <th width="15%" nowrap="nowrap" class="right-title"  style="text-align: center;">数量</th>	
		       <th width="25%" nowrap="nowrap" class="right-title"  style="text-align: center;"></th>	
		     </tr>
		     <%
		     if(rows!=null){
			     for(int i = 0;i<rows.length;i++)
			     {
			    	 DBRow row = rows[i];
			    	 long dfc_id = row.get("dfc_id",0l);
		    		 double dfc_unit_count = row.get("dfc_unit_count",0d);
		    		 String dfc_project_name = "";
		    		 String dfc_way = "";
		    		 String dfc_unit = "";
		    		 String dfc_currency = "";
		    		 double dfc_unit_price = 0;
		    		 double dfc_exchange_rate = 0;
			    	 if(dfc_id != 0) {//以前有数据
			    		 dfc_project_name = row.getString("dfc_project_name");
			    		 dfc_way = row.getString("dfc_way");
			    		 dfc_unit = row.getString("dfc_unit");
			    		 dfc_unit_price = row.get("dfc_unit_price",0d);
			    		 dfc_currency = row.getString("dfc_currency");
			    		 dfc_exchange_rate = row.get("dfc_exchange_rate",0d);
			    	 }
		      %>
		     <tr height="30px" style="padding-top:3px;">
		        <td valign="middle" style="padding-top:3px;">
		        	<input type="hidden" name="dfc_id" id="dfc_id" value="<%=dfc_id %>">
		        	<input type="hidden" name="dfc_project_name" id="dfc_project_name" value="<%=dfc_project_name %>">
		        	<input type="hidden" name="dfc_way" id="dfc_way" value="<%=dfc_way %>">
		        	<input type="hidden" name="dfc_unit" id="dfc_unit" value="<%=dfc_unit %>">
					<%=dfc_project_name %>	
		        </td>
		        <td valign="middle" nowrap="nowrap" >       
		      		<%=dfc_way %>
		        </td>
		        <td valign="middle">
					<%=dfc_unit %>
		        </td>
		        <td >
					<input type="text" size="8" name="dfc_unit_price" id="dfc_unit_price" value="<%=dfc_unit_price %>">
		        </td>
		         <td >
					<select id="dfc_currency" name="dfc_currency" index="<%=i %>" onclick="setRate(this)">
						<option value="RMB" <%=dfc_currency.equals("RMB")?"selected":""%>>RMB</option>
						<option value="USD" <%=dfc_currency.equals("USD")?"selected":""%>>USD</option>
						<option value="HKD" <%=dfc_currency.equals("HKD")?"selected":""%>>HKD</option>
					</select>
		        </td>
		        <td >
					<input type="text" size="8" name="dfc_exchange_rate" id="dfc_exchange_rate" value="<%=dfc_exchange_rate %>">
		        </td>
		        <td valign="middle" >       
					<input type="text" size="8" name="dfc_unit_count" id="dfc_unit_count" value="<%=dfc_unit_count %>">
		        </td>
				<td>
		     		<input type="button" value="删除" onclick="deleteRow(this)"/>
		     	</td>
		     </tr>
		     
		     <%
		    	 }
		     }
		     %>
		  </table>
		  <input type="hidden" name="stock_in_set" id="stock_in_set" value="1">
		</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
		<input name="freight" type="button" class="normal-green-long" onclick="setFreigthSubmit()" value="运费确认" >
		<input name="insert" type="button" class="normal-green-long" onclick="setFreigth()" value="保存" >

		<input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
</form>
<script type="text/javascript">
<!--
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
//-->
</script>
</body>
</html>

