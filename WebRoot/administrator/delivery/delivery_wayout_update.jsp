<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey,com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	String delivery_order_id = StringUtil.getString(request,"delivery_order_id").equals("")?"0":StringUtil.getString(request,"delivery_order_id");
	DBRow row = deliveryMgrLL.getDeliveryOrderById(delivery_order_id);
	String delivery_order_number = row.getValue("delivery_order_number")==null?"0":row.getValue("delivery_order_number").toString();
	String delivery_purchase_id = row.getValue("delivery_purchase_id")==null?"0":row.getValue("delivery_purchase_id").toString();
	String waybill_name = row.getValue("waybill_name")==null?"":row.getValue("waybill_name").toString();
	String carriers = row.getValue("carriers")==null?"":row.getValue("carriers").toString();
	String transportby = row.getValue("transportby")==null?"0":row.getValue("transportby").toString();
	String delivery_address = row.getValue("delivery_address")==null?"":row.getValue("delivery_address").toString();
	String begin_country = row.getValue("begin_country")==null?"0":row.getValue("begin_country").toString();
	String end_country = row.getValue("end_country")==null?"0":row.getValue("end_country").toString();
	String begin_port = row.getValue("begin_port")==null?"":row.getValue("begin_port").toString();
	String end_port = row.getValue("end_port")==null?"":row.getValue("end_port").toString();
	String delivery_linkman = row.getValue("delivery_linkman")==null?"":row.getValue("delivery_linkman").toString();
	String delivery_linkman_phone = row.getValue("delivery_linkman_phone")==null?"":row.getValue("delivery_linkman_phone").toString();
	String declaration = row.getValue("declaration")==null?"1":row.getValue("declaration").toString();
	String clearance = row.getValue("clearance")==null?"1":row.getValue("clearance").toString();
	String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
	String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String waybill_number = row.getValue("waybill_number")==null?"":row.getValue("waybill_number").toString();
	int delivery_order_status = row.get("delivery_order_status",01);
	int save_status = row.get("save_status",01);
	String arrival_eta = row.getValue("arrival_eta")==null?"":row.getValue("arrival_eta").toString();
	String fr_id = row.getValue("fr_id")==null?"":row.getValue("fr_id").toString();
	
	DBRow[] countyRows = deliveryMgrLL.getCounty();
	DBRow purchaseRow = purchaseMgrLL.getPurchaseById(row.getString("delivery_purchase_id"));
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}
	}
	
	$(document).ready(function(){
		if(<%=declaration%>==1||<%=declaration%>==2) {
			$("#declaration").val("<%=declaration%>");
		}else {
			$("#declarationTr").html("<%=declarationKey.getStatusById(Integer.parseInt(declaration))%>");
		}
		if(<%=clearance%>==1||<%=clearance%>==2) {
			$("#clearance").val("<%=clearance%>");
		}else {
			$("#clearanceTr").html("<%=clearanceKey.getStatusById(Integer.parseInt(clearance))%>");
		}
		if(<%=drawback%>==1||<%=drawback%>==2) {
			$("#drawback").val("<%=drawback%>");
		}else {
			$("#drawbackTr").html("<%=drawbackKey.getStatusById(Integer.parseInt(drawback))%>");
		}
		if(<%=invoice%>==1||<%=invoice%>==2) {
			$("#invoice").val("<%=invoice%>");
		}else {
			$("#invoiceTr").html("<%=invoiceKey.getStatusById(Integer.parseInt(invoice))%>");
		}

		init();
		$("#arrival_eta").date_input();
	});
	
	function openFreightSelect() {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliveryWayoutUpdateAction.action">
<input type="hidden" name="delivery_order.delivery_order_id" value="<%=delivery_order_id%>"/>
<input type="hidden" name="delivery_order.save_status" value="2"/>
<input type="hidden" name="delivery_order.delivery_purchase_id" value="<%=delivery_purchase_id %>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input type="hidden" name="finished" id="finished"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">

  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">交货单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=delivery_order_id%>
	          </td>
	          <td align="right" class="STYLE2">交货批次:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=delivery_order_number%>
	          </td>
	        </tr>
	         <tr>
	          <td align="right" class="STYLE2">发票:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" id="invoiceTr">
	          	<select name="delivery_order.invoice" id="invoice">
	          		<%
	          			ArrayList statuses = invoiceKey.getStatuses();
	          			for(int i=0;i<2;i++) {
	          				int statuse = Integer.parseInt(statuses.get(i).toString());
	          				String key = invoiceKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"'>"+key+"</option>");
	          			}
	          		%>
	          	</select>
	          </td>
	          <td align="right" class="STYLE2">报关:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" id="declarationTr">
	          	<select name="delivery_order.declaration" id="declaration">
	          		<%	
	          			ArrayList statuses2 = declarationKey.getStatuses();
	          			for(int i=0;i<2;i++) {
	          				int statuse = Integer.parseInt(statuses2.get(i).toString());
	          				String key = declarationKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"'>"+key+"</option>");
	          			}
	          		%>
	          	</select>
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">清关:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" id="clearanceTr">
	          	<select name="delivery_order.clearance" id="clearance">
	          		<%	
	          			ArrayList statuses3 = clearanceKey.getStatuses();
	          			for(int i=0;i<2;i++) {
	          				int statuse = Integer.parseInt(statuses3.get(i).toString());
	          				String key = clearanceKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"'>"+key+"</option>");
	          			}
	          		%>
	          	</select>
	          </td>
	          <td align="right" class="STYLE2">退税:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" id="drawbackTr">
	          	<select name="delivery_order.drawback" id="drawback">
	          		<%	
	          			ArrayList statuses4 = drawbackKey.getStatuses();
	          			for(int i=0;i<2;i++) {
	          				int statuse = Integer.parseInt(statuses4.get(i).toString());
	          				String key = drawbackKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"'>"+key+"</option>");
	          			}
	          		%>
	          	</select>
	          </td>
	        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
		<%
    		if(StringUtil.getInt(request,"finished",0)==1) {
    	%>
    		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="下一步" >
    	<%
    		} else {
    	%>
		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(0)" value="完成" >
		<%
    		}
		%>
		<input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply(finished)
	{		
		$('#finished').val(finished);
		document.apply_money_form.submit();
	}
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

