<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
	
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
	
	if(delivery_order ==null)
	{
		delivery_order = new DBRow();
	}
	
	float weight = deliveryMgrZJ.getDeliveryOrderWeight(delivery_order_id);
	
	int refresh = StringUtil.getInt(request,"refresh");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/select.js"></script>

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>	

<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}

.set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;font-weight:bold;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;}
</style>
<script type="text/javascript">
	function stateDiv(pro_id)
	{
		if(pro_id==-1)
		{
			$("#state_div").css("display","inline");
		}
		else
		{
			$("#state_div").css("display","none");
		}
	}
	
	//国家地区切换
function getStorageProvinceByCcid(ccid)
{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("手工输入","-1");
					}
				});
				
				var pro_id = <%=delivery_order.get("pro_id",0l)%>;
				if (pro_id!=""&&pro_id!=0)
				{
					$("#pro_id").setSelectedValue(pro_id);
					stateDiv(pro_id);
				}
}

	function selectStorage(ps_id)
	{
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:
				{
					ps_id:ps_id
				},
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data)
				{
					$("#send_address1").val(data.deliver_address1);
					$("#send_address2").val(data.deliver_address2);
					//$("#send_address3").val(data.deliver_address3);
					$("#send_zip_code").val(data.deliver_zip_code);
					$("#send_name").val(data.contact);
					$("#send_linkman_phone").val(data.phone);
					$("#send_city").val(data.city);
				}
			});
	}

$().ready(function() 
{
	getStorageProvinceByCcid(<%=delivery_order.get("ccid",0l)%>);//初始化地区	
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="<%=ConfigBean.getStringValue("systenFolder")%>administrator/order/print/internal_print_center.html">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            	  <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">
						交货单单号：<%=delivery_order.getString("delivery_order_number") %>
						<input type="hidden" name="type" value="D"/>
						<input type="hidden" name="id" value="<%=delivery_order_id%>"/>
						</td>
					  </tr>
				  </table>
				  <br/><br/>
					  <fieldset class="set" style="padding-bottom:4px;text-align: left;">
		  				<legend>
							<span style="" class="title">
								发货地址	
							</span>
						</legend>
					 	<table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr>
						 		<td align="right">计算运费仓库</td>
						 		<td>
						 				<select id="psid" name="ps_id" onchange="selectStorage(this.value)">
							     			<option value="0">选择仓库以计算运费</option>
							     			 <%
							     			 	DBRow[] storageRows = catalogMgr.getProductDevStorageCatalogTree();
												String qx;
												
												for ( int i=0; i<storageRows.length; i++ )
												{
													if ( storageRows[i].get("parentid",0) != 0 )
													 {
													 	qx = "├ ";
													 }
													 else
													 {
													 	qx = "";
													 }
											%>
						          			<option value="<%=storageRows[i].get("id",0l)%>"> 
												     <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",storageRows[i].get("level",0))%>
												     <%=qx%>
												     <%=storageRows[i].getString("title")%>
											</option>
											<%
												}
											%>
							     			 </select>
							    </td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="send_house_number" id="send_address1"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="send_street" id="send_address2"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货城市</td>
						 		<td><input style="width: 300px;" type="text" name="send_city" id="send_city"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="send_zip_code" id="send_zip_code"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="send_name" id="send_name"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">发货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="send_linkman_phone" id="send_linkman_phone"/></td>
						 	</tr>
					 </table>
					 </fieldset>
					 <fieldset class="set" style="padding-bottom:4px;text-align: left;">
	  				<legend>
						<span style="" class="title">
							目的地址	
						</span>
					</legend>
						 <table width="95%" align="center" cellpadding="2" cellspacing="0">
						 	<tr>
						 		<td align="right">目的国家</td>
						 		<td>
						 			<%
									DBRow countrycode[] = orderMgr.getAllCountryCode();
									String selectBg="#ffffff";
									String preLetter="";
									%>
							      <select name="deliver_ccid" id="ccid_hidden" onChange="getStorageProvinceByCcid(this.value);">
								  <option value="0">请选择...</option>
								  <%
								  for (int i=0; i<countrycode.length; i++)
								  {
								  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
									{
										if (selectBg.equals("#eeeeee"))
										{
											selectBg = "#ffffff";
										}
										else
										{
											selectBg = "#eeeeee";
										}
									}  	
									
									preLetter = countrycode[i].getString("c_country").substring(0,1);
								  %>
								    <option style="background:<%=selectBg%>;" <%=delivery_order.get("ccid",0l)==countrycode[i].get("ccid",0l)?"selected=\"selected\"":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
								  <%
								  }
								  %>
							      </select>		
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的省份</td>
						 		<td>
						 			<select name="deliver_pro_id" id="pro_id">
								    </select>		
								      	<div style="padding-top: 10px;display:none;" id="state_div">
											<input type="text" name="address_state" id="address_state"/>
										</div>
						 		</td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址街道</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_house_number" value="<%=delivery_order.getString("deliver_address1")%>"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址门牌号</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_street" value="<%=delivery_order.getString("deliver_address2")%>"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">目的城市</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_city" value="<%=delivery_order.getString("deliver_city")%>"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货地址邮编</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_zip_code" value="<%=delivery_order.getString("deliver_zip_code")%>"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_name" value="<%=delivery_order.getString("delivery_linkman")%>"/></td>
						 	</tr>
						 	<tr>
						 		<td align="right">交货联系人电话</td>
						 		<td><input style="width: 300px;" type="text" name="deliver_linkman_phone" value="<%=delivery_order.getString("delivery_linkman_phone")%>"/></td>
						 	</tr>
						 </table>
					 </fieldset>
					 <fieldset class="set" style="padding-bottom:4px;text-align:center;">
		  				<legend>
							<span style="" class="title">
								估算重量
							</span>
						</legend>
					 	<input style="width: 300px;" type="text" name="weight" id="weight" value="<%=weight%>"/>Kg
					 </fieldset>
				</td>
			 </tr>
             <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				 	<input type="submit" name="Submit2" value="下一步" class="normal-green">
			
			    	<input type="button" name="Submit2" value="取消" class="normal-white" onClick="$.artDialog.close();"></td>
             </tr>
            </table>
      </form>
</body>
</html>
