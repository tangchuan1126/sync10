<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.db.DBRow"%>
<%@page import="com.cwc.exception.JsonException"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="com.cwc.app.beans.jqgrid.FilterBean"%>
<%@page import="net.sf.json.JsonConfig"%>
<%@page import="com.cwc.app.beans.jqgrid.RuleBean"%>
<%@page import="com.cwc.json.JsonUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ include file="../../include.jsp"%> 
<%! private static int oldpages = -1; %>
<%
	
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");//100005;
	long purchase_id = StringUtil.getLong(request,"purchase_id");//100089;
			
	boolean search = Boolean.parseBoolean(StringUtil.getString(request,"_search"));//是否使用了搜索功能
	String sidx = StringUtil.getString(request,"sidx");//排序使用的字段
	String sord = StringUtil.getString(request,"sord");//排序顺序
	
	String filter = StringUtil.getString(request,"filters");//搜索条件
	
	FilterBean filterBean = null;//搜索条件bean
	
	if(search)//将搜索条件字符串转换成搜索条件bean，为方便后来搜索
	{
		Map<String, Object> map = new HashMap<String, Object>();  //String-->Property ; Object-->Clss 
		map.put("rules", RuleBean.class);
		JsonConfig configjson = JsonUtils.getJsonConfig();
		configjson.setClassMap(map);
		filterBean = (FilterBean) JsonUtils.toBean(JSONObject.fromObject(filter),FilterBean.class,configjson); 
	}
			
	String c = StringUtil.getString(request,"rows");//每页显示多少数据
	int pages = StringUtil.getInt(request,"page",1);//当前请求的是第几页
	
	PageCtrl pc = new PageCtrl();
	pc.setPageSize(Integer.parseInt(c));
	pc.setPageNo(pages);
	
	
	if(oldpages != pages||pages==1)
	{
		DBRow[] details = deliveryMgrZJ.getDeliveryOrderDetails(delivery_order_id, purchase_id,null,sidx,sord,filterBean);
		DBRow sum = computeProductMgrCCC.getSumDeliveryFreightCost(Long.toString(delivery_order_id));
		double sum_price = sum.get("sum_price", 0d);
		double sum_weight = 0;
		for(int j = 0;j<details.length; j++){
			double count = details[j].get("delivery_reap_count",0d)==0?details[j].get("delivery_count",0d):details[j].get("delivery_reap_count",0d);
			sum_weight += details[j].get("weight", 0d)*count;
		}
		for(int i = 0;i<details.length;i++)
		{
			if(details[i].getString("reapcount").equals("")||details[i].getString("purchasecount").equals(""))
			{
				details[i].add("reapcount","未定商品");
				details[i].add("purchasecount","未定商品");
			}
			
			if(details[i].getString("delivery_box").equals(""))
			{
				details[i].add("delivery_box"," ");
			}
			double count = details[i].get("delivery_reap_count",0d)==0?details[i].get("delivery_count",0d):details[i].get("delivery_reap_count",0d);
			details[i].add("freight_cost",sum_price * (details[i].get("weight", 0d)*count/sum_weight)/count);
		}
				
		DBRow data = new DBRow();
		data.add("page",pages);//page，当前是第几页
		data.add("total",pc.getPageCount());//total，总共页数
		
		data.add("rows",details);//rows，返回数据
		data.add("records",details.length);//records，总记录数

		out.println(new JsonObject(data).toString());
	}
%>