<%@ page contentType="text/html;charset=UTF-8"%>

<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderSaveKey"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<jsp:useBean id="fileWithTypeKey" class="com.cwc.app.key.FileWithTypeKey"/>
<jsp:useBean id="tDate" class="com.cwc.app.util.TDate"/>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long supplier_id = StringUtil.getLong(request,"supplier_id",0);
	long productline_id = StringUtil.getLong(request,"productline_id",0);
	String number = StringUtil.getString(request,"number");
	long ps_id = StringUtil.getLong(request,"ps_id",0);
	int status = StringUtil.getInt(request,"status",0);
	int declaration = StringUtil.getInt(request,"declarationStatus",0);
	int clearance = StringUtil.getInt(request,"clearanceStatus",0);
	int invoice = StringUtil.getInt(request,"invoiceStatus",0);
	int drawback = StringUtil.getInt(request,"drawbackStatus",0);
	int day = StringUtil.getInt(request,"day",3);
	String st = StringUtil.getString(request,"st");
	String en = StringUtil.getString(request,"en");
	int analysisType = StringUtil.getInt(request,"analysisType",0);
	int analysisStatus = StringUtil.getInt(request,"analysisStatus",0);
	int delivery_order_status = StringUtil.getInt(request,"delivery_order_status",0);
	int stock_in_set = StringUtil.getInt(request,"stock_in_set",0);
	long dept = StringUtil.getLong(request,"dept",0);
	long delivery_create_account_id = StringUtil.getLong(request,"delivery_create_account_id",0);
	long dept1 = StringUtil.getLong(request,"dept1",0);
	long delivery_create_account_id1 = StringUtil.getLong(request,"delivery_create_account_id1",0);
	DBRow[] suppliers = supplierMgrTJH.getAllSupplier(null);
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
	DBRow[] productline =  productLineMgrTJH.getAllProductLine();//供应商所在的产品线
	
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
	df.applyPattern("0.0");
	
	tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
	
	String input_st_date,input_en_date;
	if ( st.equals("") )
	{	
		input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		st = input_st_date;
	}
	else
	{	
		input_st_date = st;
	}
									
	if ( en.equals("") )
	{	
		input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		en = input_en_date;
	}
	else
	{	
		input_en_date = en;
	}
	
	String cmd = StringUtil.getString(request,"cmd");
	
	DBRow[] rows; 
	if(cmd!=null)
	{
		if(cmd.equals("filter")){
			rows = deliveryMgrZJ.filterDeliveryOrders(supplier_id,status,ps_id,pc,declaration, clearance,invoice, drawback, 0, productline_id,stock_in_set, delivery_create_account_id);
		}
		else if(cmd.equals("search")){
			rows = deliveryMgrZJ.searchDeliveryOrders(number,pc,0l);
		}
		else if(cmd.equals("followup")) {
			rows = deliveryMgrZJ.filterDeliveryOrders(0,delivery_order_status,ps_id,pc,declaration, clearance,invoice, drawback, day, 0,stock_in_set,delivery_create_account_id1);
		}
		else if(cmd.equals("analysis")) {
			rows = deliveryMgrLL.getAnalysis(st,en,analysisType,analysisStatus,day,pc);
		}
		else{
			rows = deliveryMgrZJ.getAllDeliveryOrder(pc);
		}
	}
	else{
		rows = deliveryMgrZJ.getAllDeliveryOrder(pc);
	}
	
	DeliveryOrderKey deliveryOrderKey = new DeliveryOrderKey();
	DBRow[] accounts = applyMoneyMgrLL.getAllByReadView("accountReadViewLL");
	DBRow[] accountCategorys = applyMoneyMgrLL.getAllByTable("accountCategoryBeanLL");
	DBRow[] adminGroups = applyMoneyMgrLL.getAllByTable("adminGroupBeanLL");
	DBRow[] productLineDefines = applyMoneyMgrLL.getAllByTable("productLineDefineBeanLL");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 0 0;
	width:408px; 
	height:36px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-2px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}
</style>
<script language="JavaScript1.2">
jQuery(function($document){
	/*
		var productline = "<%=productline_id %>";
		var supplier = "<%=supplier_id%>";
		if(productline != 0){
			getSupplierByProductline(productline,supplier);
		}
	*/
	$("#st").date_input();
	$("#en").date_input();

	// 初始化 下拉框
	 addAutoComplete($("#search_key"),
				"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/deliveryOrder/GetSearchDeliveryOrderJSONAction.action",
				"merge_field","delivery_order_id");
	})

function getSupplierByProductline(productlineid,supplierid){
		var para = "productline_id="+productlineid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/getSupplierJSONAction.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(e){
					alert("数据读取出错！");
				},
				
				success: function(data){
					$("#supplier").clearAll();
					$("#supplier").addOption("全部供应商","0");
					if(data != ""){
						$.each(data,function(i){
							$("#supplier").addOption(data[i].sup_name,data[i].id);
						});
					}if(supplierid){
							$("#supplier option[value = '"+supplierid+"']").attr("selected",true);
					}
				}
			});
	}

	$(document).ready(function(){
		getSupplierByProductline(<%=productline_id%>,<%=supplier_id%>);
	});

	function search()
	{
		if($("#search_key").val().length>0){
			document.search_form.number.value =	$("#search_key").val();
			document.search_form.submit();
		}
		else{
			alert("请输入采购单批号或交货批次");
		}
	}
	
	function filter(){
		document.filter_form.supplier_id.value = $("#supplier").getSelectedValue();
		document.filter_form.ps_id.value = $("#ps").getSelectedValue();
		document.filter_form.status.value = $("#delivery_order_status").getSelectedValue();
		document.filter_form.productline_id.value = $("#productline").getSelectedValue();
		document.filter_form.declarationStatus.value = $("#declarationStatus").getSelectedValue();
		document.filter_form.clearanceStatus.value = $("#clearanceStatus").getSelectedValue();
		document.filter_form.invoiceStatus.value = $("#invoiceStatus").getSelectedValue();
		document.filter_form.drawbackStatus.value = $("#drawbackStatus").getSelectedValue();
		document.filter_form.stock_in_set.value = $("#stock_in_set").getSelectedValue();
		document.filter_form.dept.value = $("#dept").getSelectedValue();
		document.filter_form.delivery_create_account_id.value = $("#delivery_create_account_id").getSelectedValue();
		document.filter_form.submit();
	}
	
	function intransitDelivery(delivery_order_id,number){
		if(confirm("你确定"+number+"供应商的货物已发送了？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/intransitDelivery.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function delDelivery(delivery_order_id,number)
	{
		if(confirm("确定交货单"+number+"？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
			$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/delDeliveryOrder.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.reload();
					}
					
				}
			});
		}							
	}
	
	function applicationApprove(delivery_order_id,number)
	{
		if(confirm("确定申请"+number+"交货单完成?"))
		{
			document.applicationApprove_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/applicationApproveDelivery.action";
			document.applicationApprove_form.delivery_order_id.value = delivery_order_id;
			document.applicationApprove_form.submit();
		}
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
		
	function closeWinNotRefresh()
	{
		tb_remove();
	}
	
	function addDeliveryOrder()
	{
		var url = "administrator_create_delivery.html";
		$.artDialog.open(url, {title: '交货单信息',width:'700px',height:'600px', lock: true,opacity: 0.3});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}
	
	
	function uploadFile(association_id,association_type)
	{
		var url = "delivery_upload_image.html?association_id="+association_id+"&association_type="+association_type;
		$.artDialog.open(url, {title: '交货单文件',width:'700px',height:'600px', lock: true,opacity: 0.3});
		//tb_show('创建交货单','administrator_create_delivery.html?TB_iframe=true&height=500&width=700',false);
	}

	function deliveryOrderLogs(delivery_order_id)
	{
		var url = 'delivery_order_logs.html?delivery_order_id='+delivery_order_id;
		$.artDialog.open(url, {title: '日志 交货单号:'+delivery_order_id,width:'800px',height:'500px', lock: true,opacity: 0.3});
	}

	function promptCheck(v,m,f)
	{
		if (v=="y")
		{
			 if(f.content == "")
			 {
					alert("请填写适当备注");
					return false;
			 }
			 
			 else
			 {
			 	if(f.content.length>300)
			 	{
			 		alert("内容太多，请简略些");
					return false;
			 	}
			 }
			 
			 return true;
		}
	}


	function changeType(obj) {
		$('#invoice').attr('style','display:none');
		$('#declaration').attr('style','display:none');
		$('#clearance').attr('style','display:none');
		$('#drawback').attr('style','display:none');
		if($(obj).val()==1) {
			$('#clearance').attr('style','');
		}else if($(obj).val()==2) {
			$('#declaration').attr('style','');
		}else if($(obj).val()==3) {
			$('#invoice').attr('style','');
		}else if($(obj).val()==4) {
			$('#drawback').attr('style','');
		}
	}

	function remark(delivery_order_id)
	{
		var text = "<div id='title'>跟进</div><br />";
		text += "备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
		$.prompt(text,
				{
					  submit:promptCheck,
			   		  loaded:
							function ()
							{
								
							}
					  ,
					  callback: 
					  
							function (v,m,f)
							{
								if (v=="y")
								{
										document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliveryRemarkAction.action"
										document.followup_form.delivery_order_id.value = delivery_order_id;
										document.followup_form.delivery_order_content.value = "备注:"+f.content;
										document.followup_form.type.value = 1;
										document.followup_form.submit();	
								}
							}
					  
					  ,
					  overlayspeed:"fast",
					  buttons: { 提交: "y", 取消: "n" }
				});
	}
	
	function followup(delivery_order_id,declaration,clearance,invoice,drawback,delivery_order_status)
	{
		
		if((delivery_order_status==1)||(delivery_order_status==2)||(declaration>1&&declaration!=4)||(clearance>1&&clearance!=4)||(invoice>1&&invoice!=4)||(drawback>1&&drawback!=4)) {
			var text = "<div id='title'>跟进</div><br />";
			text += "跟进范围:<select id='type' name='type' onchange='changeType(this)'>";
			text += delivery_order_status==1?"<option value='5'>启运</option>":"";
			text += delivery_order_status==2?"<option value='6'>取消启运</option>":"";
			text += declaration>1&&declaration!=4?"<option value='2'>出口报关</option>":"";
			text += clearance>1&&clearance!=4?"<option value='1'>进口清关</option>":"";
			text += invoice>1&&invoice!=4?"<option value='3'>发票</option>":"";
			text += drawback>1&&drawback!=4?"<option value='4'>退税</option>":"";
			text += "</select>&nbsp;&nbsp;";
			text += "跟进阶段:";

			var b = false;
			var b1 = delivery_order_status==1;
			b = b?b:b1;
			
			b1 = declaration>1&&declaration!=4;
	  		text += "<select name='declaration' id='declaration' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses2 = declarationKey.getStatuses();
	  			for(int i=2;i<statuses2.size();i++) {
	  				int statuse = Integer.parseInt(statuses2.get(i).toString());
	  				String key = declarationKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key%></option>"
			<%
	  			}
	  		%>
	  		text += "</select>";
			b = b?b:b1;  		

  			b1 = clearance>1&&clearance!=4;
	  		text += "<select name='clearance' id='clearance' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses3 = clearanceKey.getStatuses();
	  			for(int i=2;i<statuses3.size();i++) {
	  				int statuse = Integer.parseInt(statuses3.get(i).toString());
	  				String key = clearanceKey.getStatusById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key%></option>"
			<%
	  			}
	  		%>
	  		text += "</select>";
  			b = b?b:b1;
	  		

  			b1 = invoice>1&&invoice!=4;
			text += "<select id='invoice' name='invoice' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";		
	  		<%
	  			ArrayList statuses = invoiceKey.getInvoices();
	  			for(int i=2;i<statuses.size();i++) {
	  				int statuse = Integer.parseInt(statuses.get(i).toString());
	  				String key = invoiceKey.getInvoiceById(statuse);
	  		%>
	  		text += "<option value='<%=statuse%>'><%=key%></option>"
	  		<%
	  			}
	  		%>
	  		text += "</select>";
	  		b = b?b:b1;
	  		
  			b1 = drawback>1&&drawback!=4;
	  		text += "<select name='drawback' id='drawback' "+(!b?(b1?"":"style='display:none'"):"style='display:none'")+">";
	  		<%	
	  			ArrayList statuses4 = drawbackKey.getDrawbacks();
	  			for(int i=2;i<statuses4.size();i++) {
	  				int statuse = Integer.parseInt(statuses4.get(i).toString());
	  				String key = drawbackKey.getDrawbackById(statuse);
			%>
			text += "<option value='<%=statuse%>'><%=key%></option>"
			<%
	  			}
	  		%>
	  		text += "</select>";
	  		b = b?b:b1;
	  		
			text += "<br>备注:<br/><textarea rows='5' cols='60' id='content' name='content'/>";
			$.prompt(text,
			{
				  submit:promptCheck,
		   		  loaded:
						function ()
						{
							
						}
				  ,
				  callback: 
				  
						function (v,m,f)
						{
							if (v=="y")
							{
									document.followup_form.action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliveryLogsInsertAction.action"
									document.followup_form.delivery_order_id.value = delivery_order_id;
									document.followup_form.type.value = f.type;
									if(f.type==1) {
										document.followup_form.stage.value = f.clearance;
									}else if(f.type==2) {
										document.followup_form.stage.value = f.declaration;
									}else if(f.type==3) {
										document.followup_form.stage.value = f.invoice;
									}else if(f.type==4) {
										document.followup_form.stage.value = f.drawback;
									}
									document.followup_form.delivery_order_content.value = f.content;
									document.followup_form.delivery_order_type.value = 1;
									document.followup_form.submit();	
							}
						}
				  ,
				  overlayspeed:"fast",
				  buttons: { 提交: "y", 取消: "n" }
			});
		}else {
			alert("没有跟进项目");
		}
	}
	function goApplyFundsPage(id) {
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/apply_funds.html?cmd=search&apply_id1="+id;
	}
	function goFundsTransferListPage(id)
	{
		window.location.href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/financial_management/funds_transfer_list.html?cmd=search&transfer_id="+id;      
	}
	$(document).ready(function(){
		getLevelSelect(0, nameArray, widthArray, centerAccounts, null,vname,'<%=dept%>');
		getLevelSelect(1, nameArray, widthArray, centerAccounts, $("#dept"),vname,'<%=delivery_create_account_id%>');
		getLevelSelect(0, nameArray1, widthArray1, centerAccounts, null,vname1,'<%=dept1%>');
		getLevelSelect(1, nameArray1, widthArray1, centerAccounts, $("#dept1"),vname1,'<%=delivery_create_account_id1%>');
	});
	<%//level_id,value,name
		String str = "var centerAccounts = new Array(";
		str += "new Array('01',0,'选择部门')";
		str += ",new Array('01.0',0,'选择职员')";
		for(int i=0;i<adminGroups.length;i++) {//部门
			DBRow adminGroup = adminGroups[i];
			str += ",new Array('0"+(i+2)+"',"+adminGroup.get("adgid",0)+",'"+adminGroup.getString("name")+"')";
			str += ",new Array('0"+(i+2)+".0',0,'选择职员')";
			for(int ii=0;ii<accounts.length;ii++) {
				DBRow account = accounts[ii];
				if(account.get("account_parent_id",0)==adminGroup.get("adgid",0) && account.get("account_category_id",0)==2)//职员列表
					str += ",new Array('0"+(i+2)+"."+(ii+1)+"',"+account.get("acid",0)+",'"+account.getString("account_name")+"')";
			}
		}
		str+= ");";
		out.println(str);
	%>
	var nameArray = new Array('dept','delivery_create_account_id');
	var widthArray = new Array(120,120);
	var vname = new Array('nameArray','widthArray','centerAccounts','vname');
	var nameArray1 = new Array('dept1','delivery_create_account_id1');
	var widthArray1 = new Array(120,120);
	var vname1 = new Array('nameArray1','widthArray1','centerAccounts','vname1');
	function getLevelSelect(level,nameArray, widthArray, selectArray,o,vnames,value) {
			if(level<nameArray.length) {
				var name = nameArray[level];
				var width = widthArray[level];
				var levelId = o==null?"":$("option:selected",o).attr('levelId');
				var onchangeStr = "";
				if(level==nameArray.length-1)
					onchangeStr = "";
				else
					onchangeStr = "getLevelSelect("+(level+1)+","+vnames[0]+","+vnames[1]+","+vnames[2]+",this,"+vnames[3]+")";
				var selectHtml = "&nbsp;&nbsp;<select name='"+name+"' id='"+name+"' style='width:"+width+"px;' onchange="+onchangeStr+"> ";
				//alert(selectArray);
				for(var i=0;i<selectArray.length;i++) {
					if(levelId!="") {
						var levelIdChange = selectArray[i][0].replace(levelId+".");
						var levelIds = levelIdChange.split(".");	
						//alert(levelIdChange);
						if(levelIdChange!=selectArray[i][0]&&levelIds.length==1){
							//alert(levelId+",value="+selectArray[i][0]+",change='"+levelIdChange+"',"+levelIds.length);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
					else {
						var levelIdChange = selectArray[i][0];
						//alert(levelIdChange);
						var levelIds = levelIdChange.split(".");
						if(levelIds.length==1){
							//alert(levelId+","+selectArray[i][0]+levelId1);
							selectHtml += "<option value='"+selectArray[i][1]+"' levelId='"+selectArray[i][0]+"' displayName='"+selectArray[i][2]+"'>"+selectArray[i][2]+"</option> ";
						}
					}
				}
				selectHtml += "</select>";
				$('#'+name+'_div').html('');
				$('#'+name+'_div').append(selectHtml);
				//alert(selectHtml);
				$('#'+name).val(value);
				getLevelSelect(level+1,nameArray,widthArray,centerAccounts,$('#'+name),vnames);
			}
	} 
	
	
</script>

</head>

<body onload="onLoadInitZebraTable()">

<br/>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="56%" class="page-title" style="border-bottom:0px;" align="left"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle"/>&nbsp;&nbsp; 采购管理 »   交货单列表 </td>
    <td width="44%" align="right" ></td>
  </tr>
</table>
	<div class="demo" style="width:98%">
	<div id="tabs">
		<ul>
			<li><a href="#delivery_search">常用工具</a></li>
			<li><a href="#delivery_filter">高级搜索</a></li>
			<li><a href="#delivery_followup">需跟进</a></li>
			<li><a href="#delivery_analysis">交货单监控</a></li>
		</ul>
		<div id="delivery_search">
			 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
	            <tr>
	              <td nowrap="nowrap" width="30%" style="padding-top:3px;">
	              
		              <div id="easy_search_father">
								<div id="easy_search"><a href="javascript:search()"><img id="eso_search" src="../imgs/easy_search.gif" width="70" height="29" border="0"/></a></div>
					  </div>
						<table width="485" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="418">
									<div  class="search_shadow_bg">
									 <input name="search_key" id="search_key" value='<%=number %>' type="text" class="search_input" style="font-size:17px;font-family:  Arial;color:#333333" id="search_key" onkeydown="if(event.keyCode==13)search()"/>
									</div>
								</td>
								<td width="67">
								</td>
							</tr>
						</table>
				   </td>
	              <td width="45%"></td>
	              <td width="12%" align="right" valign="middle">
	              	<tst:authentication bindAction="com.cwc.app.api.zj.DeliveryMgrZJ.addDeliveryOrder">
						<a href="javascript:addDeliveryOrder();"><img src="../imgs/create_delivery_bg.jpg" width="129" height="51" border="0"/></a>
					</tst:authentication>
				  </td>
	            </tr>
	          </table>
		</div>
		
		<div id="delivery_filter">
			 <table width="100%" border="0">
				<tr>
					<td align="left" height="25">
					<div style="float:left;">

				    <select id="productline" id="productline" onChange="getSupplierByProductline(this.value)">
				     <option value="0">产品线选择...</option>
				     <%
						for(int i=0;i<productline.length;i++){
						   if(productline_id ==productline[i].get("id",0l)){
								//System.out.println("productline_id"+productline_id+"  "+productline[i].get("id",0l));
						   %>
						      <option value="<%=productline[i].getString("id") %>" selected="selected"><%=productline[i].getString("name") %></option>
						   <%
						   }else{
						   %>
						    <option value="<%=productline[i].getString("id") %>"><%=productline[i].getString("name") %></option>
						  <%
						    }
						      	}
						  %>
				      </select>


              	<select id="supplier" name="supplier">
		              		<option value="0">供应商选择</option>
              	</select>


						<select id="ps" name="ps">
							<option value="0">目的仓库选择</option>
							<%
								for(int j = 0;j<ps.length;j++)
								{
							%>
								<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
							<%
								}
							%>
						</select>
						
						<select id="stock_in_set" name="stock_in_set">
							<option value="0">运费设置</option>	
							<option value="1"<%=stock_in_set==1?"selected":"" %>>运费未完成</option>
							<option value="2"<%=stock_in_set==2?"selected":"" %>>运费已完成</option>
						</select>
						</div>
						<div style="float:left;" id="dept_div" name="dept_div">
						</div>
						<div id='delivery_create_account_id_div' name='delivery_create_account_id_div' style="float:left;">
						</div>
					<td>
					</td>
					</tr>
					<tr>
					<td align="left" height="25">

						<select id="delivery_order_status" name="delivery_order_status">
							<option value="0">流程状态</option>
							<option value="<%=DeliveryOrderKey.NOFINISH%>" <%=status==DeliveryOrderKey.NOFINISH?"selected":"" %>>未完成</option>
							<option value="<%=DeliveryOrderKey.READY%>" <%=status==DeliveryOrderKey.READY?"selected":""%>>准备中</option>
							<option value="<%=DeliveryOrderKey.INTRANSIT%>" <%=status==DeliveryOrderKey.INTRANSIT?"selected":""%>>运输中</option>
							<option value="<%=DeliveryOrderKey.APPROVEING%>" <%=status==DeliveryOrderKey.APPROVEING?"selected":""%>>审核中</option>
							<option value="<%=DeliveryOrderKey.FINISH%>" <%=status==DeliveryOrderKey.FINISH?"selected":""%>>完成</option>
						</select>

					&nbsp;&nbsp;
				 
				  <select name="declarationStatus" id="declarationStatus">
				  	<option value="0"> 出口报关状态</option>
	          		<%	
	          			ArrayList statuses21 = declarationKey.getStatuses();
	          			for(int i=0;i<statuses21.size();i++) {
	          				int statuse = Integer.parseInt(statuses21.get(i).toString());
	          				String key1 = declarationKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(declaration==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	
	          	<select name="clearanceStatus" id="clearanceStatus">
	          		<option value="0">进口清关状态</option>
	          		<%	
	          			ArrayList statuses31 = clearanceKey.getStatuses();
	          			for(int i=0;i<statuses31.size();i++) {
	          				int statuse = Integer.parseInt(statuses31.get(i).toString());
	          				String key1 = clearanceKey.getStatusById(statuse);
	          				out.println("<option value='"+statuse+"' "+(clearance==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	
	          	<select name="invoiceStatus" id="invoiceStatus">
	          		<option value="0">发票状态</option>
	          		<%
	          			ArrayList statuses11 = invoiceKey.getInvoices();
	          			for(int i=0;i<statuses11.size();i++) {
	          				int statuse = Integer.parseInt(statuses11.get(i).toString());
	          				String key1 = invoiceKey.getInvoiceById(statuse);
	          				out.println("<option value='"+statuse+"' "+(invoice==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
	          	&nbsp;&nbsp;
	          	
	          	<select name="drawbackStatus" id="drawbackStatus">
	          		<option value="0">退税状态</option>
	          		<%	
	          			ArrayList statuses41 = drawbackKey.getDrawbacks();
	          			for(int i=0;i<statuses41.size();i++) {
	          				int statuse = Integer.parseInt(statuses41.get(i).toString());
	          				String key1 = drawbackKey.getDrawbackById(statuse);
	          				out.println("<option value='"+statuse+"' "+(drawback==statuse?"selected":"")+">"+key1+"</option>");
	          			}
	          		%>
	          	</select>
					</td>
					<td align="center"><input type="button" class="button_long_refresh" value="过滤" onclick="filter()"/></td>
				</tr>
			</table>
		</div>
		<div id="delivery_followup">
			<form action="administrator_delivery_order_index.html">
			<input type="hidden" id="cmd" name="cmd" value="followup"/>
				<table width="100%">
					<tr>
						<td align="left" height="25">
						<div style="float:left;" id="dept_div" name="dept_div">
	
							<select id="ps_id" name="ps_id">
								<option value="0">目的仓库选择</option>
								<%
									for(int j = 0;j<ps.length;j++)
									{
								%>
									<option value="<%=ps[j].get("id",0l)%>" <%=ps_id==ps[j].get("id",0l)?"selected":"" %>><%=ps[j].getString("title")%></option>
								<%
									}
								%>
							</select>
							
							<select id="stock_in_set" name="stock_in_set">
								<option value="0">运费设置状态</option>	
								<option value="1"<%=stock_in_set==1?"selected":"" %>>运费未完成</option>
								<option value="2"<%=stock_in_set==2?"selected":"" %>>运费已完成</option>
							</select>
	
							<select id="delivery_order_status" name="delivery_order_status">
								<option value="0">流程状态</option>
								<option value="<%=DeliveryOrderKey.NOFINISH%>" <%=delivery_order_status==DeliveryOrderKey.NOFINISH||delivery_order_status==0?"selected":"" %>>未完成</option>
								<option value="<%=DeliveryOrderKey.READY%>" <%=delivery_order_status==DeliveryOrderKey.READY?"selected":""%>>准备中</option>
								<option value="<%=DeliveryOrderKey.INTRANSIT%>" <%=delivery_order_status==DeliveryOrderKey.INTRANSIT?"selected":""%>>运输中</option>
								<option value="<%=DeliveryOrderKey.APPROVEING%>" <%=delivery_order_status==DeliveryOrderKey.APPROVEING?"selected":""%>>审核中</option>
								<option value="<%=DeliveryOrderKey.FINISH%>" <%=delivery_order_status==DeliveryOrderKey.FINISH?"selected":""%>>完成</option>
							</select>
	
						&nbsp;&nbsp;
					  
					  <select name="declarationStatus" id="declarationStatus">
					  	<option value="0">出口报关状态</option>
		          		<%	
		          			ArrayList statuses211 = declarationKey.getStatuses();
		          			for(int i=0;i<statuses211.size();i++) {
		          				int statuse = Integer.parseInt(statuses211.get(i).toString());
		          				String key1 = declarationKey.getStatusById(statuse);
		          				out.println("<option value='"+statuse+"' "+(declaration==statuse?"selected":"")+">"+key1+"</option>");
		          			}
		          		%>
		          	</select>
		          	&nbsp;&nbsp;
		          	
		          	<select name="clearanceStatus" id="clearanceStatus">
		          		<option value="0">进口清关状态</option>
		          		<%	
		          			ArrayList statuses311 = clearanceKey.getStatuses();
		          			for(int i=0;i<statuses311.size();i++) {
		          				int statuse = Integer.parseInt(statuses311.get(i).toString());
		          				String key1 = clearanceKey.getStatusById(statuse);
		          				out.println("<option value='"+statuse+"' "+(clearance==statuse?"selected":"")+">"+key1+"</option>");
		          			}
		          		%>
		          	</select>
		          	&nbsp;&nbsp;
		          	
		          	<select name="invoiceStatus" id="invoiceStatus">
		          		<option value="0">发票状态</option>
		          		<%
		          			ArrayList statuses111 = invoiceKey.getInvoices();
		          			for(int i=0;i<statuses111.size();i++) {
		          				int statuse = Integer.parseInt(statuses111.get(i).toString());
		          				String key1 = invoiceKey.getInvoiceById(statuse);
		          				out.println("<option value='"+statuse+"' "+(invoice==statuse?"selected":"")+">"+key1+"</option>");
		          			}
		          		%>
		          	</select>
		          	&nbsp;&nbsp;
		          	
		          	<select name="drawbackStatus" id="drawbackStatus">
		          		<option value="0">退税状态</option>
		          		<%	
		          			ArrayList statuses411 = drawbackKey.getDrawbacks();
		          			for(int i=0;i<statuses411.size();i++) {
		          				int statuse = Integer.parseInt(statuses411.get(i).toString());
		          				String key1 = drawbackKey.getDrawbackById(statuse);
		          				out.println("<option value='"+statuse+"' "+(drawback==statuse?"selected":"")+">"+key1+"</option>");
		          			}
		          		%>
		          	</select>
		          	<input type="text" name="day" id="day" value="<%=day%>" size="2"/>天内未跟进
		          	</div>
		          		<div style="float:left;" id="dept1_div" name="dept1_div">
						</div>
						<div id='delivery_create_account_id1_div' name='delivery_create_account_id1_div' style="float:left;">
						</div>
						</td>
						<td align="center">
						<input type="submit"" class="button_long_refresh" value="跟进"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="delivery_analysis">
		<form action="administrator_delivery_order_index.html">
			<input type="hidden" id="cmd" name="cmd" value="analysis"/>
			<table width="100%">
				<tr>
					<td align="left">
						开始:<input name="st" type="text" id="st"  value="<%=input_st_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						截止:<input name="en" type="text" id="en" size="9" value="<%=input_en_date%>"  style="border:1px #CCCCCC solid;width:85px;" />
						<select id="analysisStatus" name="analysisStatus">
							<option value="1" <%=analysisStatus==1?"selected":"" %>>监控</option>
							<option value="2" <%=analysisStatus==2?"selected":"" %>>统计</option>
						</select>
						监控分类:
						<select id="analysisType" name="analysisType">
							<option value="1" <%=analysisType==1?"selected":"" %>>报关过程</option>
							<option value="2" <%=analysisType==2?"selected":"" %>>清关过程</option>
							<option value="3" <%=analysisType==3?"selected":"" %>>退税过程</option>
							<option value="4" <%=analysisType==4?"selected":"" %>>发票过程</option>
							<option value="5" <%=analysisType==5?"selected":"" %>>交货单完成</option>
						</select>
						大于
						<input type="text" id="day" name="day" value="<%=day %>" size="2"/>
						天
						
						
						<input type="submit" class="button_long_refresh" value="监控"/>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</div>
</div>
<script>
	$("#st").date_input();
	$("#en").date_input();
	$("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 } ,
	});
</script>
	<br/>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable" isNeed="true">
    <tr> 
        <th width="12%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">交货信息</th>
        <th width="8%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">目的仓库/状态</th>
        <th width="17%" nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">供应商</th>
        <th width="5%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">运单信息</th>
        <th width="7%" nowrap="nowrap"  class="right-title " style="vertical-align: center;text-align: center;">报关退税信息</th>
		<th width="7%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;">最后更新时间</th>
		<th width="10%" nowrap="nowrap" class="right-title " style="vertical-align: center;text-align: center;"></th>
  </tr>
  <%
  	for(int i=0;i<rows.length;i++)
  	{
  %>
  	<tr align="center" valign="middle">
	  	<td height="40" nowrap="nowrap">
	  		<%=rows[i].get("delivery_order_id",0l) %><br/>
	  		<a href="administrator_delivery_order_detail.html?delivery_order_id=<%=rows[i].get("delivery_order_id",0l) %>"><%=rows[i].getString("delivery_order_number")%></a><br/>
	  		<%=rows[i].getString("delivery_create_account").equals("vender")?"供应商":rows[i].getString("delivery_create_account")%><br/>
	  		<%=rows[i].getString("delivery_date")%>
	  	</td>
	  	<td nowrap="nowrap">
	  		<%=catalogMgr.getDetailProductStorageCatalogById(rows[i].get("delivery_psid",0l)).getString("title")%><br/>
	  		<%=deliveryOrderKey.getDeliveryOrderStatusById(rows[i].get("delivery_order_status",0))%><br/>
	  		<%=rows[i].getString("all_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("delivery_date")+" 00:00:00","dd"))+"天":"共用"+rows[i].getString("all_over")+"天" %>
	  	</td>
	  	<td nowrap="nowrap">
	  	<%
	  		DBRow supplier = supplierMgrTJH.getDetailSupplier(rows[i].get("delivery_supplier_id",0l));
	  		if(supplier==null)
	  		{
	  			supplier = new DBRow();
	  		}
	  		out.print(supplier.getString("sup_name"));
	  	%>
	  	</td>	  	
	  	<td align="left" nowrap="nowrap">
	  		运单号:<%=rows[i].getString("waybill_number")%><br/>
	  		货运公司:<%=rows[i].getString("waybill_name")%><br/>
	  		承运公司:<%=rows[i].getString("carriers")%><br/>
	  		<%
				long begin_country = rows[i].get("begin_country",0l);
				DBRow begin_country_row = transportMgrLL.getCountyById(Long.toString(begin_country));
				long end_country = rows[i].get("end_country",0l);
				DBRow end_country_row = transportMgrLL.getCountyById(Long.toString(end_country));					
			%>
			始发国/始发港:<%=begin_country_row==null?"无":begin_country_row.getString("c_country")%>/<%=rows[i].getString("begin_port").equals("")?"无":rows[i].getString("begin_port")%><br/>
			目的国/目的港:<%=end_country_row==null?"无":end_country_row.getString("c_country")%>/<%=rows[i].getString("end_port").equals("")?"无":rows[i].getString("end_port")%><br/>
			<%
				if(rows[i].get("stock_in_set",1)==1) {
					out.println("<font color='red'>");
				}
			%>
			<b>运费:<%=computeProductMgrCCC.getSumDeliveryFreightCost(rows[i].getString("delivery_order_id")).get("sum_price",0d) %>RMB<br/></b>
			<%
				if(rows[i].get("stock_in_set",1)==1) {
					out.println("</font>");
				}
			%>
			<%
				DBRow[] applyMoneys = applyMoneyMgrLL.getApplyMoneyByBusiness(rows[i].getString("delivery_order_id"),5);//
				for(int j=0; j<applyMoneys.length; j++){
		 			out.println("<a href=\"javascript:void(0)\" onClick=\"goApplyFundsPage('"+applyMoneys[j].get("apply_id",0)+"')\">F"+applyMoneys[j].get("apply_id",0)+"</a>申请资金"+applyMoneys[j].get("amount",0f)+"RMB<br/>");
		 			DBRow[] applyTransferRows = applyMoneyMgrLL.getApplyTransferByBusiness(rows[i].getString("delivery_order_id"),5);
		 			if(applyTransferRows.length>0) {
		 				for(int ii=0;applyTransferRows!=null && ii<applyTransferRows.length;ii++) {
		 					if(applyTransferRows[ii].get("apply_money_id",0) == applyMoneys[j].get("apply_id",0)) {
			  				String transfer_id = applyTransferRows[ii].getString("transfer_id")==null?"":applyTransferRows[ii].getString("transfer_id");
			  				String amount = applyTransferRows[ii].getString("amount")==null?"":applyTransferRows[ii].getString("amount");
			  				String currency = applyTransferRows[ii].getString("currency")==null?"":applyTransferRows[ii].getString("currency");
			  				int moneyStatus = applyTransferRows[ii].get("status",0);
			  				if(moneyStatus != 0 )
			  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>已转账"+amount+" "+currency+"<br/>");
			  				else
			  					out.println("<a href=\"javascript:void(0)\" onClick=\"goFundsTransferListPage('"+transfer_id+"')\">W"+transfer_id+"</a>申请转账"+amount+" "+currency+"<br/>");
		 					}
			  			}
		 			}
		 		}
		 	%>
	  	</td>
	  	<td nowrap="nowrap" >
	  		<%=declarationKey.getStatusById(rows[i].get("declaration",01)) %> <%=rows[i].get("declaration",01)==1?"":(rows[i].getString("declaration_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("delivery_date")+" 00:00:00","dd"))+"天":"共用"+rows[i].getString("declaration_over")+"天")%><br/>
	  			<%=clearanceKey.getStatusById(rows[i].get("clearance",01)) %><%=rows[i].get("clearance",01)==1?"":(rows[i].getString("clearance_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("delivery_date")+" 00:00:00","dd"))+"天":"共用"+rows[i].getString("clearance_over")+"天")%><br/>
	  			<%=invoiceKey.getInvoiceById(rows[i].get("invoice",01)) %><%=rows[i].get("invoice",01)==1?"":(rows[i].getString("invoice_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("delivery_date")+" 00:00:00","dd"))+"天":"共用"+rows[i].getString("invoice_over")+"天")%><br/>
	  			<%=drawbackKey.getDrawbackById(rows[i].get("drawback",01)) %><%=rows[i].get("drawback",01)==1?"":(rows[i].getString("drawback_over").equals("")?"已用"+df.format(tDate.getDiffDate(rows[i].getString("delivery_date")+" 00:00:00","dd"))+"天":"共用"+rows[i].getString("drawback_over")+"天")%>
	  	</td>
	  	<td><%=rows[i].getString("updatedate").equals("")?"":DateUtil.FormatDatetime("yy-MM-dd HH:mm",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").parse(rows[i].getString("updatedate")))%></td>
  	  	<td align="center" nowrap="nowrap">
  	  		<input class="long-button" type="button" onclick="followup(<%=rows[i].get("delivery_order_id",0l)%>,<%=rows[i].get("declaration",0l)%>,<%=rows[i].get("clearance",0l)%>,<%=rows[i].get("invoice",0l)%>,<%=rows[i].get("drawback",0l)%>,<%=rows[i].get("delivery_order_status",0l)%>)" value="跟进"/><br/>
  	  		<input class="long-button" type="button" onclick="remark(<%=rows[i].get("delivery_order_id",0l)%>)" value="备注"/><br/>
  	  		<input class="long-button" type="button" onclick="deliveryOrderLogs(<%=rows[i].get("delivery_order_id",0l)%>)" value="日志"/><br/>
  	  		<%
  	  			if(rows[i].get("delivery_order_status",0)== DeliveryOrderKey.READY)
  	  			{
  	  		%>
  	  			<input type="button" class="long-button" value="删除" onclick="delDelivery(<%=rows[i].get("delivery_order_id",0l)%>,'<%=rows[i].getString("delivery_order_number")%>')"/><br/>
  	  		<%
  	  			}
  	  		%>
  	  		<%
  	  			if(rows[i].get("delivery_order_status",0) == DeliveryOrderKey.INTRANSIT)
  	  			{
  	  		%>
  	  			<tst:authentication bindAction="com.cwc.app.api.zj.DeliveryApproveMgrZJ.addDeliveryApprove">
  	  			<input type="button" class="long-button" value="申请审核" onclick="applicationApprove(<%=rows[i].get("delivery_order_id",0l)%>,'<%=rows[i].getString("delivery_order_number")%>')"/><br/>
  	  			</tst:authentication>
  	  		<%
  	  			}
  	  		%>
  	  		<input type="button" class="long-button" value="相关文件" onclick="uploadFile(<%=rows[i].get("delivery_order_id",0) %>,<%=fileWithTypeKey.delivery %>)"/><br/>
  	  	</td>
  </tr>
  <%
  	}
   %>
</table>
<br />
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table">
<%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=pc.getPageNo()%>"/>
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onclick="javascript:go(document.getElementById('jump_p2').value)" value="GO"/>
    </td>
  </tr>
</table>

<form action="administrator_delivery_order_index.html" method="get" name="search_form">
	<input type="hidden" name="number"/>
	<input type="hidden" name="cmd" value="search"/>
</form>

<form action="administrator_delivery_order_index.html" method="post" name="filter_form">
	<input type="hidden" name="status"/>
	<input type="hidden" name="supplier_id"/>
	<input type="hidden" name="ps_id"/>
	<input type="hidden" name="cmd" value="filter"/>
	<input type="hidden" name="productline_id"/>
	<input type="hidden" name="declarationStatus"/>
	<input type="hidden" name="clearanceStatus"/>
	<input type="hidden" name="drawbackStatus"/>
	<input type="hidden" name="invoiceStatus"/>
	<input type="hidden" name="stock_in_set"/>
	<input type="hidden" name="dept"/>
	<input type="hidden" name="delivery_create_account_id"/>
</form>

<form name="dataForm" method="post">
        <input type="hidden" name="p"/>
        <input type="hidden" name="status" value="<%=status%>"/>
		<input type="hidden" name="supplier_id" value="<%=supplier_id%>"/>
		<input type="hidden" name="ps_id" value="<%=ps_id%>"/>
		<input type="hidden" name="cmd" value="<%=cmd%>"/>
		<input type="hidden" name="number" value="<%=number%>"/>
		<input type="hidden" name="productline_id" value="<%=productline_id%>"/>
		<input type="hidden" name="declarationStatus" value="<%=declaration %>"/>
		<input type="hidden" name="clearanceStatus" value="<%=clearance %>"/>
		<input type="hidden" name="drawbackStatus" value="<%=drawback %>"/>
		<input type="hidden" name="invoiceStatus" value="<%=invoice %>"/>
		<input type="hidden" name="st" value="<%=st%>"/>
	    <input type="hidden" name="en" value="<%=en%>"/>
	    <input type="hidden" name="analysisType" value="<%=analysisType%>"/>
	    <input type="hidden" name="analysisStatus" value="<%=analysisStatus%>"/>
	    <input type="hidden" name="day" value="<%=day%>"/>
	    <input type="hidden" name="stock_in_set" value="<%=stock_in_set %>"/>
	    <input type="hidden" name="dept" value="<%=dept%>"/>
		<input type="hidden" name="delivery_create_account_id" value="<%=delivery_create_account_id%>"/>
		<input type="hidden" name="dept1" value="<%=dept1%>"/>
		<input type="hidden" name="delivery_create_account_id1" value="<%=delivery_create_account_id1%>"/>
  </form>


<form action="" method="post" name="applicationApprove_form">
	<input type="hidden" name="delivery_order_id"/>
</form>

<form action="" method="post" name="followup_form">
	<input type="hidden" name="delivery_order_id"/>
	<input type="hidden" name="type" value="1"/>
	<input type="hidden" name="stage" value="1"/>
	<input type="hidden" name="delivery_order_type" value="1"/>
	<input type="hidden" name="delivery_order_content"/>
</form>
</body>
</html>



