<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<%
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单号：<%=delivery_order_id %></title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll; 
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />

<script type='text/javascript' src='../js/jquery.form.js'></script>
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>



<script src="../js/zebra/zebra.js" type="text/javascript"></script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<script language="javascript" src="../../common.js"></script>


<script type="text/javascript">
	function closeWindow(){
		parent.location.reload();
	}
</script>

<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
</style>
</head>

<body >
<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<td valign="top" style="padding:15px;"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center">
				<span style="font-family:'黑体'; font-size:25px;"></span><br/>
            </td>
          </tr>
        </table>
	    
	  <br>
  	  <table width="98%" height="100%"  border="0" cellpadding="0" cellspacing="0">
  	    <tr>
  	      <td  align="left" valign="top">
  	        <div class="demo">
  	          <div id="tabs">
  	            <ul>
	  	              <li><a href="delivery_order_logs_tright.html?delivery_order_id=<%=delivery_order_id %>">全部日志<span> </span></a></li>
					  <li><a href="delivery_order_logs_tright.html?delivery_order_id=<%=delivery_order_id %>&delivery_order_type=1">进度日志<span> </span></a></li>
					  <li><a href="delivery_order_logs_tright.html?delivery_order_id=<%=delivery_order_id %>&delivery_order_type=2">财务日志<span> </span></a></li>
					  <li><a href="delivery_order_logs_tright.html?delivery_order_id=<%=delivery_order_id %>&delivery_order_type=3">修改日志<span> </span></a></li>
			    </ul>
			  </div>
		    </div>
				  <script>
					$("#tabs").tabs({
						cache: true,
						spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
						cookie: { expires: 30000 } ,
						load: function(event, ui) {onLoadInitZebraTable();}	
					});
					</script>	</td>
	    </tr>
      </table>
  </td></tr>
 <tr>
 	<td align="right" class="win-bottom-line">
     <input type="button" name="Submit2" value="关闭" class="normal-white" onClick="closeWindow()">	</td>
 </tr>
</table>
</body>
</html>

