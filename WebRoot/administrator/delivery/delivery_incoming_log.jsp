<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>交货单入库</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

<script> 
function uploadPurchaseIncoming()
{
	 if($("#file").val() == "")
	{
		alert("请选择文件上传");
		
	}
	else
	{	
		var filename=$("#file").val().split(".");
		//filename[filename.length-1]=="csv"
		if(document.upload_form.file.value.indexOf("in")==-1)
		{
			alert("请选择入库日志文件上传");
		}
		else
		{
			document.upload_form.machine_id.value = $("#file").val();
			document.upload_form.submit();
			
		}	
	}
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE2 {
	font-size: medium;
	font-weight: bold;
	color: #666666;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<form action="delivery_incoming_show.html" name="upload_form" enctype="multipart/form-data" method="post">
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td colspan="2" align="left" valign="top">
			<br>
            <br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;"> 
						交货单号：<%=delivery_order.getString("delivery_order_number") %>
						</td>
					  </tr>
				  </table>
				  <br>
<br>

					<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" >
						 <tr>
							<td width="13%" align="left" valign="top" nowrap ><span class="STYLE2">上传入库文件</span></td>
							<td width="87%" align="left" valign="top" >
							  <label>
								<input name="file" type="file" id="file">
							  </label>
						   </td>
						</tr>
				  </table>
				</td>
			  </tr>
			 
			  <tr valign="bottom">
				<td colspan="2" align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="确定" class="normal-green" onClick="uploadPurchaseIncoming();">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();">				</td>
			  </tr>
</table> 
<input type="hidden" name="delivery_order_id" id="delivery_order_id" value="<%=delivery_order_id %>"/>
<input type="hidden" name="machine_id" id="machine_id"/>
</form>
</body>
</html>
