<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	DBRow[] noFinishPurchases = purchaseMgr.getNoFinishPurchase(0);
	
	int refresh = StringUtil.getInt(request,"refresh");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增采购单</title> 
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>
function uploadDeliveryOrderDetail()
{
	var purchase_id = $("#purchase_id").val();
	var para = "purchase_id="+purchase_id; 
	$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/supplier_login/supplier/PuchaseNoSaveDeliveryOrder.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:para,
			
			beforeSend:function(request){
			},
					
			error: function(data){
				alert("系统错误，请您稍后再试");
			},
					
			success: function(data){
				if(data["nosave"]=="true")
				{
					if(confirm("该采购单有未保存的交货单，你确定要创建新的交货单吗（我们将删除未保存的交货单）？"))
					{
						createDeliveryOrder(purchase_id);				
					}
				}
				else
				{
					createDeliveryOrder(purchase_id);
				}
			}
	});
}

function refresh()
{
	<%
		if(refresh==1)
		{
	%>
		parent.closeWin();
	<%
		}
		else
		{
	%>
		parent.closeWinNotRefresh();
	<%
		}
	%>
}

function closeWindow(){
	$.artDialog.close();
	//self.close();
}
function createDeliveryOrder(purchase_id)
{
	document.add_deliveryOrder_form.purchase_id.value = purchase_id;
	document.add_deliveryOrder_form.submit();
}
</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE3 {font-size: medium; font-weight: bold; color: #666666; }
-->
</style>

<style>
a:link {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}



a.hard:link {
	color:blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:visited {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:hover {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
a.hard:active {
	color: blue;
	text-decoration: underline;
	font-family: Arial;
	font-size:medium;
}
.STYLE12 {color: #666666; font-size: medium;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form action="administrator_delivery_order_detail_show.html" name="upload_form" enctype="multipart/form-data" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
              <tr>
                <td align="left" valign="top"><br>
            <table width="95%" align="center" cellpadding="0" cellspacing="0" >
					  <tr>
						<td align="center" style="font-family:'黑体'; font-size:25px; border-bottom:1px solid #cccccc;color:#000000;padding-bottom:15px;">创建交货单</td>
					  </tr>
				  </table>
					<br/>
					<br/>
					<table width="95%" align="center" cellpadding="5" cellspacing="0" >
						<tr  style=" padding-bottom:15px; padding-left:15px;">
						<td width="9%" align="left" valign="top" nowrap="nowrap" class="STYLE3" ><u>选择采购单</u></td>
						<td width="91"align="left" valign="top" >
							<select id="purchase_id">
								<%
									for(int i = 0;i<noFinishPurchases.length;i++)
									{
								%>
									<option value="<%=noFinishPurchases[i].get("purchase_id",0l)%>"><%="P"+noFinishPurchases[i].getString("purchase_id")%></option>
								<%
									}
								%>
							</select>
						</td>
					  </tr>
				  </table>
				</td></tr>
              <tr>
                <td  align="right" valign="middle" class="win-bottom-line">				
				  <input type="button" name="Submit2" value="下一步" class="normal-green" onClick="uploadDeliveryOrderDetail();">
			
			    <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();"></td>
              </tr>
            </table>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/creatDeliveryOrder.action" name="add_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="backurl" value="<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_transit_update.html"/>
</form>
</body>
</html>
