<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.ApplyTypeKey,com.cwc.app.api.ll.Utils,java.text.SimpleDateFormat"%>
<jsp:useBean id="invoiceKey" class="com.cwc.app.key.InvoiceKey"/>
<jsp:useBean id="drawbackKey" class="com.cwc.app.key.DrawbackKey"/>
<jsp:useBean id="clearanceKey" class="com.cwc.app.key.ClearanceKey"/>
<jsp:useBean id="declarationKey" class="com.cwc.app.key.DeclarationKey"/>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<jsp:useBean id="transportWayKey" class="com.cwc.app.key.TransportWayKey"/>
<%@ include file="../../include.jsp"%>
<%
	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
	String delivery_order_id = StringUtil.getString(request,"delivery_order_id").equals("")?"0":StringUtil.getString(request,"delivery_order_id");
	DBRow row = deliveryMgrLL.getDeliveryOrderById(delivery_order_id);
	if(row == null)
	{
		row = new DBRow();
	}
	String zip_code = row.getString("zip_code");
	String delivery_order_number = row.getValue("delivery_order_number")==null?"0":row.getValue("delivery_order_number").toString();
	String delivery_purchase_id = row.getValue("delivery_purchase_id")==null?"0":row.getValue("delivery_purchase_id").toString();
	String waybill_name = row.getValue("waybill_name")==null?"":row.getValue("waybill_name").toString();
	String carriers = row.getValue("carriers")==null?"":row.getValue("carriers").toString();
	String transportby = row.getValue("transportby")==null?"0":row.getValue("transportby").toString();
	String delivery_address = row.getValue("delivery_address")==null?"":row.getValue("delivery_address").toString();
	String begin_country = row.getValue("begin_country")==null?"0":row.getValue("begin_country").toString();
	String end_country = row.getValue("end_country")==null?"0":row.getValue("end_country").toString();
	String begin_port = row.getValue("begin_port")==null?"":row.getValue("begin_port").toString();
	String end_port = row.getValue("end_port")==null?"":row.getValue("end_port").toString();
	String delivery_linkman = row.getValue("delivery_linkman")==null?"":row.getValue("delivery_linkman").toString();
	String delivery_linkman_phone = row.getValue("delivery_linkman_phone")==null?"":row.getValue("delivery_linkman_phone").toString();
	String declaration = row.getValue("declaration")==null?"1":row.getValue("declaration").toString();
	String clearance = row.getValue("clearance")==null?"1":row.getValue("clearance").toString();
	String invoice = row.getValue("invoice")==null?"1":row.getValue("invoice").toString();
	String drawback = row.getValue("drawback")==null?"1":row.getValue("drawback").toString();
	String remark = row.getValue("remark")==null?"":row.getValue("remark").toString();
	String waybill_number = row.getValue("waybill_number")==null?"":row.getValue("waybill_number").toString();
	int delivery_order_status = row.get("delivery_order_status",01);
	int save_status = row.get("save_status",01);
	String arrival_eta = row.getValue("arrival_eta")==null?"":row.getValue("arrival_eta").toString();
	String fr_id = row.getValue("fr_id")==null?"":row.getValue("fr_id").toString();
	long delivery_psid = row.get("delivery_psid",0l);
	String deliver_address1 = row.getString("deliver_address1");
	String deliver_address2 = row.getString("deliver_address2");
	String deliver_address3 = row.getString("deliver_address3");
	String deliver_zip_code = row.getString("deliver_zip_code");
	String deliver_city = row.getString("deliver_city");
	
	DBRow[] countyRows = deliveryMgrLL.getCounty();
	DBRow purchaseRow = purchaseMgrLL.getPurchaseById(row.getString("delivery_purchase_id"));
	DBRow[] ps = catalogMgr.getProductStorageCatalogTree();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src='../js/jquery.form.js'></script>
<script type="text/javascript" src="../js/select.js"></script>
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
 <script type="text/javascript" src="../js/mcdropdown/lib/jquery.assets.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<style type="text/css">
<!--

.create_order_button
{
	background-attachment: fixed;
	background: url(../imgs/create_order.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	height: 51px;
	width: 129px;
	color: #000000;
	border: 0px;
	
}
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
-->
</style>
<script>
	var updated = false;
	<%
		//System.out.print(StringUtil.getString(request,"inserted"));
		if(StringUtil.getString(request,"updated").equals("1")) {
	%>
			updated = true;
	<%
		}
	%>
	function init() {
		if(updated) {
			parent.location.reload();
			closeWindow();
		}

	}
	
	$(document).ready(function(){
		if(<%=declaration%>==1||<%=declaration%>==2) {
			$("#declaration").val("<%=declaration%>");
		}else {
			$("#declarationTr").html("<%=declarationKey.getStatusById(Integer.parseInt(declaration))%>");
		}
		if(<%=clearance%>==1||<%=clearance%>==2) {
			$("#clearance").val("<%=clearance%>");
		}else {
			$("#clearanceTr").html("<%=clearanceKey.getStatusById(Integer.parseInt(clearance))%>");
		}
		if(<%=drawback%>==1||<%=drawback%>==2) {
			$("#drawback").val("<%=drawback%>");
		}else {
			$("#drawbackTr").html("<%=drawbackKey.getStatusById(Integer.parseInt(drawback))%>");
		}
		if(<%=invoice%>==1||<%=invoice%>==2) {
			$("#invoice").val("<%=invoice%>");
		}else {
			$("#invoiceTr").html("<%=invoiceKey.getStatusById(Integer.parseInt(invoice))%>");
		}

		init();
		$("#arrival_eta").date_input();
		getStorageProvinceByCcid(<%=row.get("ccid",0l)%>,<%=row.get("pro_id",0l)%>);
	});
	
	function openFreightSelect() {
		var url = "<%=ConfigBean.getStringValue("systenFolder")%>administrator/freight_resources/freight_resources_select.html";
		freightSelectWindow = $.artDialog.open(url, {title: '运输资源选择',width:'700px',height:'500px', lock: true,opacity: 0.3});
	}
	
	function getStorageProvinceByCcid(ccid,pro_id)
	{
		$.ajaxSettings.async = false;
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/GetStorageProvinceByCcidJSON.action",
				{ccid:ccid},
				function callback(data)
				{ 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("请选择......","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							$("#pro_id").addOption(data[i].pro_name,data[i].pro_id);
						});
					}
					
					if (ccid>0)
					{
						$("#pro_id").addOption("手工输入","-1");
					}
				});
				
				if (pro_id!=""&&pro_id!=0)
				{
					$("#pro_id").setSelectedValue(pro_id);
				}
	}
	
	function selectStorage()
	{
		var para = "ps_id="+$("#ps_id").val();
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getProductStorageCatalogJson.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){
				},
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				success: function(data){
					$("#deliver_address1").val(data.deliver_address1);
					$("#deliver_address2").val(data.deliver_address2);
					$("#deliver_address3").val(data.deliver_address3);
					$("#deliver_zip_code").val(data.deliver_zip_code);
					$("#deliver_city").val(data.city);
					$("#ccid").val(data["native"]);
					
					getStorageProvinceByCcid($("#ccid").val(),data.pro_id);
					
					$("#delivery_linkman").val(data.contact);
					$("#delivery_linkman_phone").val(data.phone);
				}
			});
	}
</script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="apply_money_form" id="apply_money_form" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliveryTransitUpdateAction.action">
<input type="hidden" name="delivery_order.delivery_order_id" value="<%=delivery_order_id%>"/>
<input type="hidden" name="delivery_order.save_status" value="2"/>
<input type="hidden" name="delivery_order.delivery_purchase_id" value="<%=delivery_purchase_id %>"/>
<input type="hidden" name="freight_changed" id="freight_changed" value="0"/>
<input type="hidden" name="finished" id="finished"/>
<input type="hidden" name="delivery_order.delivery_address" id="delivery_address"/>

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单信息</legend>
		<table width="100%" border="0" cellspacing="5" cellpadding="0">
	        <tr height="29">
	          <td align="right" class="STYLE2">交货单号:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=delivery_order_id%>
	          </td>
	          <td align="right" class="STYLE2">交货批次:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<%=delivery_order_number%>
	          
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">联系人:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="delivery_order.delivery_linkman" id="delivery_linkman" value="<%=delivery_linkman%>">
	          </td>
	          <td align="right" class="STYLE2">联系电话:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="delivery_order.delivery_linkman_phone" id="delivery_linkman_phone" value="<%=delivery_linkman_phone%>">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">是否起运:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          <%
	          	if(delivery_order_status==1||delivery_order_status==2){
	          %>
	          	<select id="ifTran" name="ifTran">
	          		<option value="1" <%=delivery_order_status==1?"selected":"" %>>否</option>
	          		<option value="2" <%=delivery_order_status==2?"selected":"" %>>是</option>
	          	</select>
	          <%
	          	} else {
	          %>
	          		是
	          <%
	          	}
	          %>
	          </td>
	          <td align="right" class="STYLE2">预计到达日期:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle">
	          	<input type="text" name="delivery_order.arrival_eta" id="arrival_eta" value="<%=arrival_eta.equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():arrival_eta%>">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">交货仓库:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          <%
	          	long ps_id = delivery_psid==0?purchaseRow.get("ps_id",0l):delivery_psid;
	          	if(row.get("delivery_order_status",0l)!=DeliveryOrderKey.FINISH) {
	          %>
	          	<select name="delivery_order.delivery_psid" id="delivery_psid" onchange="selectStorage()">
	          		<% 
	          			for(int i=0;i<ps.length;i++) {
	          		%>
	          				<option value="<%=ps[i].get("id",0l)%>" <%=ps_id==ps[i].get("id",0l)?"selected":"" %>><%=ps[i].getString("title")%></option>	          				
	          		<%
	          			}
	          		%>
	          	</select>
	          <%
	          	}
	          	else
	          	{
	          %>
	          		<% 
	          			for(int i=0;i<ps.length;i++) {
	          				if(ps[i].get("id",0l) == ps_id) {
	          		%>
	          				<input type="hidden" name="delivery_order.delivery_psid" id="delivery_psid" value="<%=ps_id %>">
	          		<%
	          				}
	          			}
	          		%>
	          <%
	          	}
	          %>
	          </td>
	        </tr>
	        <tr>
			    <td align="right" valign="middle" class="STYLE2" >所属国家:</td>
			    <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
				<%
				DBRow countrycode[] = orderMgr.getAllCountryCode();
				String selectBg="#ffffff";
				String preLetter="";
				%>
			      <select name="ccid" id="ccid" onchange="getStorageProvinceByCcid(this.value)">
				  <option value="0">选择国家...</option>
				  <%
				  for (int i=0; i<countrycode.length; i++)
				  {
				  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
					{
						if (selectBg.equals("#eeeeee"))
						{
							selectBg = "#ffffff";
						}
						else
						{
							selectBg = "#eeeeee";
						}
					}  	
					
					preLetter = countrycode[i].getString("c_country").substring(0,1);
					
			
				  %>
				    <option style="background:<%=selectBg%>;" <%=row.get("ccid",0l)==countrycode[i].get("ccid",0l)?"selected":""%> value="<%=countrycode[i].getString("ccid")%>"><%=countrycode[i].getString("c_country")%></option>
				  <%
				  }
				  %>
			      </select> 
				</td>
			  </tr>
			  <tr>
			  	<td align="right" valign="middle" class="STYLE2" >所属省份:</td>
			    <td>&nbsp;</td>
	          	<td align="left" valign="middle" colspan="4">
	          		<select name="pro_id" id="pro_id"></select>
	          	</td>
			  </tr>
			 <tr height="29">
	          <td align="right" class="STYLE2">所在城市:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="delivery_order.deliver_city" id="deliver_city" value="<%=deliver_city%>" style="width:510">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">交货地址开头:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="delivery_order.deliver_address1" id="deliver_address1" value="<%=deliver_address1%>" style="width:510">
	          </td>
	        </tr>
	         <tr height="29">
	          <td align="right" class="STYLE2">交货地址中间:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="delivery_order.deliver_address2" id="deliver_address2" value="<%=deliver_address2%>" style="width:510">
	          </td>
	        </tr>
	         <tr height="29">
	          <td align="right" class="STYLE2">交货地址结尾:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="delivery_order.deliver_address3" id="deliver_address3" value="<%=deliver_address3%>" style="width:510">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">邮政编码</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<input type="text" name="delivery_order.deliver_zip_code" id="deliver_zip_code" value="<%=deliver_zip_code%>" style="width:510">
	          </td>
	        </tr>
	        <tr height="29">
	          <td align="right" class="STYLE2">备注:</td>
	          <td>&nbsp;</td>
	          <td align="left" valign="middle" colspan="4">
	          	<textarea rows="2" cols="80" name="delivery_order.remark" id="remark"><%=remark %></textarea>
	          </td>
	        </tr>
		</table>
	</fieldset>	
	</td>
  </tr>
</table>

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
    <%
    		if(StringUtil.getInt(request,"finished",0)==1) {
    %>
    		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(1)" value="下一步" >
    <%
    	}else {
    %>
		<input name="insert" type="button" class="normal-green-long" onclick="submitApply(0)" value="完成" >
    <%
    	}
    %>
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>

</table>
  </form>
<script type="text/javascript">
<!--
	function submitApply(finished)
	{
		if($("#remark").val().trim().length>500)
		{
	    	 $("#remark").focus();
	     	alert("备注字数不能超过500！");
		}
		else 
		{
			$('#finished').val(finished);
			
			$("#delivery_address").val($("#deliver_address1").val()+$("#deliver_address2").val()+$("#deliver_address3").val());
			
			document.apply_money_form.submit();
		}
	}
	function closeWindow(){
		$.artDialog.close();
		//self.close();
	}
//-->
</script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript"> 

</script>
</body>
</html>

