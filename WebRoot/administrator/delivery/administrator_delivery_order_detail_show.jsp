<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.exception.purchase.FileTypeException"%>
<%@page import="com.cwc.app.exception.purchase.FileException"%>
<%@page import="java.util.HashMap"%>
<%@ include file="../../include.jsp"%> 
<%

 	PageCtrl pc = new PageCtrl();
 	pc.setPageNo(StringUtil.getInt(request,"p"));
 	pc.setPageSize(30);
 	String[] file=new String[2];
 	DBRow[] rows=null;
 	try
 	{
 		file = deliveryMgrZJ.uploadDeliveryOrderDetail(request);
 		rows = deliveryMgrZJ.excelShow(file[0]);
 	}
 	catch(FileTypeException e)
 	{
 		out.print("<script>alert('只可上传.xls文件');window.history.go(-1)</script>");
 	}
 	catch(FileException e)
 	{
 		out.print("<script>alert('上传失败，请重试');window.history.go(-1)</script>");
 	}
 	
 	boolean submit = true;
 	String msg = "";
 	HashMap barcode = new HashMap();
 	
 %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../common.js"></script>
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>

<link href="../comm.css" rel="stylesheet" type="text/css"/>
<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="100%" cellpadding="0" cellspacing="0" height="100%">
  <tr>
  	<td valign="top">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" style="padding-top:10px;">
		  <tr>
			<th width="9%"  class="left-title " style="vertical-align: center;text-align: left;">商品名</th>
			<th width="10%" class="right-title " style="vertical-align: center;text-align: left;">商品条码</th>
			<th width="10%" class="right-title " style="vertical-align: center;text-align: left;">单位</th>
			<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">已交数量</th>
			<th width="7%"  class="right-title " style="vertical-align: center;text-align: center;">订购数量</th>
			<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">本次发货量</th>
			<th width="9%"  class="left-title " style="vertical-align: center;text-align: center;">箱号</th>
		  </tr>
			 <%
				if(rows!=null)
				{	
					for(int i=0;i<rows.length;i++)
					{
						DBRow product = productMgr.getDetailProductByPname(rows[i].getString("product_name"));
						boolean colorchange = false;//增加有问题行变色
						if(barcode.containsKey(rows[i].getString("product_barcode")))
						{
							submit = false;
							msg = "交货单内有重复商品!";
							colorchange = true;
						}
						else
						{
							barcode.put(rows[i].getString("product_barcode"),"");
						}
			  %>
				  <tr align="center" valign="middle" 
					  <%if(product==null)
						{
							submit = false;
							msg = "交货单内有未知商品!";
							colorchange = true;
						}
						
						if(rows[i].getString("product_barcode").equals(""))
						{
							submit = false;
							msg = "交货单中有商品条码为空的，请检查上传的excel文件！";
							colorchange = true;
						}
						
						if(product!=null&&!rows[i].getString("product_barcode").equals(product.getString("p_code")))
						{
							submit = false;
							msg = "交货商品中有商品的商品名与条码对应错误，请检查上传的excel文件！";
							colorchange = true;
						}
						
						
						if(!rows[i].getString("delivery_count").matches("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d|\\d*$"))
						{
							submit = false;
							msg = "交货数量异常，请检查！";
							colorchange = true;
						}
						else if(rows[i].getString("delivery_count").equals("")||Float.valueOf(rows[i].getString("delivery_count"))==0)
						{
							submit = false;
							msg = "交货数量有为0或为空的，请检查上传的excel文件！";
							colorchange = true;
						}
						
						
						
						
						if(colorchange)
						{
							out.print(" bgcolor='#FFC1C1'");
						}
					  %>>
					<td height="40" align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("product_name") %>&nbsp;</td>
					<td align="left" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("product_barcode")%>&nbsp;</td>
					<td style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("unit_name") %>&nbsp;</td>
					<td style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("reap_count") %>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("purchase_count")%>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("delivery_count")%>&nbsp;</td>
					<td align="center" style="border-bottom: 1px solid #dddddd;padding-left:10px;"><%=rows[i].getString("delivery_box")%>&nbsp;</td>
				  </tr>
			  <%
					}
				}
			  %>
	  </table>
	</td>
  </tr>
 <tr>
 	<td valign="bottom" style="padding-top:3px;">
 		<table width="100%" cellpadding="0" cellspacing="0">
 			<tr>
 				<td align="left" valign="middle" class="win-bottom-line">
					<%
						if(!msg.equals(""))
						{
					%>
						<img src="../imgs/product/warring.gif" width="16" height="15" align="absmiddle"> <span style="color:#000000"><%=msg%></span>
					<%
						}
					%>			  &nbsp;</td>
 				<td align="right" valign="middle" class="win-bottom-line"> 
			      <%  
			      	if(submit)
			      	{
			      %>
			      	<input type="button" name="Submit2" value="确定" class="normal-green" onClick="ajaxSaveDeliveryDetail()"/>
			      <%
			      	}
			      %>
				  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="parent.closeWin();"/>
			  </td>
 			</tr>
 		</table>
 	</td>
 </tr>
   
</table>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/purchase/savePurchaseDetail.action" name="savePurchasedetail_form">
	<input type="hidden" name="tempfilename" id="tempfilename" value="<%=file[0]%>"/>
	<input type="hidden" name="delivery_order_id" id="delivery_order_id" value="<%=file[1]%>"/>
	<input type="hidden" name="purchaseid" id="purchaseid" value="<%=file[2]%>"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>delivery/delivery_order_detail.html" name="virtual_deliveryOrder_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="filename"/>
</form>

<script type="text/javascript">
	function savePurchaseDetail()
	{
		
			var tempfilename = $("#tempfilename").val();
			var purchase_id = $("#purchase_id").val();
		
	}
	
	function ajaxSaveDeliveryDetail()
	{
		if(<%=submit%>)
		{
			var filename = $("#tempfilename").val();
			var delivery_order_id = $("#delivery_order_id").val();	
			
			var purchase_id = $("#purchaseid").val();
			var para = "filename="+filename+"&delivery_order_id="+delivery_order_id;
			if(delivery_order_id =="0")
			{
				parent.virtualDeliveryOrder(filename,purchase_id);
			}
			else
			{
						$.blockUI.defaults = {
						css: { 
							padding:        '10px',
							margin:         0,
							width:          '200px', 
							top:            '45%', 
							left:           '40%', 
							textAlign:      'center', 
							color:          '#000', 
							border:         '3px solid #aaa',
							backgroundColor:'#fff'
						},
						
						// 设置遮罩层的样式
						overlayCSS:  { 
							backgroundColor:'#000', 
							opacity:        '0.8' 
						},
				
				    centerX: true,
				    centerY: true, 
					
						fadeOut:  2000
					};
					
					$.blockUI({ message: '<img src="../imgs/sending.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:#666666">保存中，请稍后......</span>'});
					
					
					$.ajax({
						url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrderDetails.action',
						type: 'post',
						dataType: 'json',
						timeout: 60000,
						cache:false,
						data:para,
						
						beforeSend:function(request){
						},
						
						error: function(){
							alert("提交失败，请重试！");
						},
						
						success: function(date){
		
							if (date["close"])
							{
								$.blockUI({ message: '<img src="../imgs/standard_msg_ok.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:green">保存成功！</span>' });
								$.unblockUI();
								parent.closeWin();
							}
							else
							{
								$.blockUI({ message: '<img src="../imgs/standard_msg_error.gif" align="absmiddle"/> &nbsp; <span style="font-size:13px;font-weight:bold;color:red">'+date["error"]+'</span>' });
							}
						}
					});
				}
		}
		else
		{
			alert("<%=msg%>");
		}
		
	}
</script>

</body>
</html>



