<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="com.cwc.app.key.PurchaseMoneyKey"%>
<%@page import="com.cwc.app.key.PurchaseArriveKey"%>
<%@page import="com.cwc.app.key.DeliveryOrderKey"%>
<%@page import="com.cwc.exception.RedirectException"%>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%> 
<% 
	try{
	long purchase_id = Long.parseLong(StringUtil.getString(request,"purchase_id","0"));
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
	String filename = StringUtil.getString(request,"filename");
	
	int isPrint = StringUtil.getInt(request,"isPrint",0);
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
	int number = 0;
	DBRow[] delivery_order_details;
	
	if(delivery_order == null)
	{
		DBRow[] delivery_orders = deliveryMgrZJ.getDeliveryOrders(purchase_id);
		number = delivery_orders.length+1;
		
		DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
		
		delivery_order = new DBRow();
		delivery_order.add("delivery_address",purchase.getString("delivery_address"));
		delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
		delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
		delivery_order.add("delivery_order_status",DeliveryOrderKey.READY);
		delivery_order.add("delivery_order_number","");
	}
	else
	{
		purchase_id = delivery_order.get("delivery_purchase_id",0l);
		
	}
	
	DBRow supplier; 
	try
	{
		supplier = supplierMgrTJH.getDetailSupplier(Long.parseLong(purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id)).getString("supplier")));
	}
	catch(NumberFormatException e)
	{
		supplier = new DBRow();
	}
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>交货单<%=delivery_order.getString("delivery_order_number")%></title>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-ui-1.8.14.custom.min.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/plugins/ui.multiselect.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.contextmenu.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.tablednd.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.layout.js"></script>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery.jqGrid.src.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js?v=1'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<link type="text/css" href="../js/jqGrid-4.1.1/js/jquery-ui-1.8.1.custom.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/jqGrid-4.1.1/js/ui.jqgrid.css" rel="stylesheet" />


<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />



<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
<script type="text/javascript" src="../js/popmenu/common.js"></script>
<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
	
<link href="../comm.css" rel="stylesheet" type="text/css"/>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>

<script type="text/javascript">
		function onloadPrint(isPrint)
		{
			if(isPrint>0)
			{
				visionariPrinter.PRINT_INIT("交货单");
				
				visionariPrinter.SET_PRINT_STYLEA(0,"HOrient",2);
				visionariPrinter.ADD_PRINT_TBURL(30,5,800,650,"../../delivery/print_delivery_order_detail.html?purchase_id=<%=purchase_id%>&delivery_order_id=<%=delivery_order_id%>");
				
				visionariPrinter.PREVIEW();
			}
			
		}
		
		function print()
		{			
				if(confirm("系统将自动为您保存交货单，您确定打印吗？"))
				{
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addPrintDeliveryOrder.action";
					document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
					document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
					document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
				
					document.save_deliveryOrder_form.submit();
				}
		}
		function searchPurchaseDetail()
		{
			if($("#search_key").val().trim()=="")
			{
				alert("请输入要关键字")
			}
			else
			{
				document.search_form.purchase_name.value=$("#search_key").val();
				document.search_form.submit();
			}	
		}
	
	function uploadDeliveryOrderDetail()
	{
		tb_show('上传交货单','delivery_upload_excel.html?delivery_order_id='+<%=delivery_order_id%>+'&purchase_id=<%=purchase_id%>&TB_iframe=true&height=500&width=800',false);
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function closeWinNotRefresh()
	{
		tb_remove();
	}
			 
	function clearPurchase()
	{
		if(confirm("确定清空该采购单内所有商品吗？"))
		{
			document.del_form.submit();
		}
	}
	
	function virtualDeliveryOrder(filename,purchase_id)
	{
		document.virtual_delivery_order_form.filename.value = filename;
		document.virtual_delivery_order_form.purchase_id.value = purchase_id;
		
		document.virtual_delivery_order_form.submit();
		
		tb_remove();
		
	}
	
	function logout()
	{
		if (confirm("确认退出系统吗？"))
		{
			document.logout_form.submit();
		}
	}
	
	function downloadDeliveryOrder()
	{
		var para = "delivery_order_id=<%=delivery_order_id%>&purchase_id=<%=purchase_id%>";
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/downloadDeliveryOrder.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(date){
						if(date["canexport"]=="true")
						{
							document.download_form.action=date["fileurl"];
							document.download_form.submit();
						}
						else
						{
							alert("无法下载！");
						}
					}
				});
	}
	
	function saveDeliveryOrder()
	{
		if(confirm("确定保存对此交货单的修改吗"))
		{
			<%
				if(delivery_order_id !=0)
				{
			%>
					document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/saveDeliveryOrder.action";
			<%	
				}
				else
				{
			%>
				document.save_deliveryOrder_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/addDeliveryOrder.action";
			<%	
				}
			%>
			
			document.save_deliveryOrder_form.waybill_number.value =  $("#waybill_id").val();
			document.save_deliveryOrder_form.waybill_name.value = $("#waybill").val();
			document.save_deliveryOrder_form.arrival_eta.value = $("#eta").val();
			
			document.save_deliveryOrder_form.submit();
		}
	}
	
	function autoComplete(obj)
	{
		obj.autocomplete("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
		{
			minChars: 0,
		width: 550,
		mustMatch: false,
		matchContains: true,
		autoFill: false,//自动填充匹配
		max:15,
		cacheLength:1,	//不缓存
		dataType: 'json', 
		updownSelect: true,
		scroll:false,
		selectFirst: false,
		updownSelect:true,
		highlight: function(match, keywords) {
			keywords = keywords.split(' ').join('|');
			return match.replace(new RegExp("("+keywords+")", "gi"),'<strong><font color="#FF3300">$1</font></strong>');
		},

				
		//加入对返回的json对象进行解析函数，函数返回一个数组       
		   parse: function(data) {   
			 var rows = [];    

			 for(var i=0; i<data.length; i++){   
			 		  
			  rows[rows.length] = {   
			  		data:data[i].p_name,
    				value:data[i].p_name+"["+data[i].p_code+"]["+data[i].unit_name+"]",
        			result:data[i].p_name
				};    
			  }   
			
		  	 return rows;   
			 },   
		
		formatItem: function(row, i, max) {
			return(row)
		},
		formatResult:function (row) {
			return row;
		}
		}); 
	}
	function beforeShowAdd(formid)
	{
		
	}
	
	function beforeShowEdit(formid)
	{
	
	}
	
	function afterShowAdd(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function afterShowEdit(formid)
	{
		autoComplete($("#product_name"));
	}
	
	function errorFormat(serverresponse,status)
	{
		return errorMessage(serverresponse.responseText);
	}
	
	
	
	function ajaxModProductName(obj,rowid,col)
	{
		var unit_name;//单字段修改商品名时更换单位专用
		var para ="delivery_order_detail_id="+rowid;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/getDeliveryOrderDetailJSON.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
				
				beforeSend:function(request){
				},
				
				error: function(){
					alert("提交失败，请重试！");
				},
				
				success: function(data)
				{
					obj.setRowData(rowid,data);
				}
			});
	}
	
	function errorMessage(val)
	{
		var error1 = val.split("(");
		var error2 = error1[1].split(")");	
		
		return error2[0];
	}
</script>

<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>


<style>
a.normal:link {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:visited {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:hover {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a.normal:active {
	color: #cccccc;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}

body
{
	font-size:12px;
}
.STYLE1 {color: #FFFFFF}
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
a.logout:link {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

a.logout:visited {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:hover {
font-size: 12px;
color: #FF0000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout_on.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}
a.logout:active {
font-size: 12px;
color: #000000;
text-decoration: none;
	background-attachment: fixed;
	background: url(../imgs/logout.gif);
	background-repeat: no-repeat;
	background-position: 1px center;
	height: 23px;
	width: 60px;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 28px;
}

.ui-datepicker {z-index:1200;}
.rotate
    {
        /* for Safari */
        -webkit-transform: rotate(-90deg);

        /* for Firefox */
        -moz-transform: rotate(-90deg);

        /* for Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }

.ui-jqgrid tr.jqgrow td 
	{
		white-space: normal !important;
		height:auto;
		vertical-align:text-top;
		padding-top:2px;
	}
</style>

</head>

<body onload="onloadPrint(<%=isPrint%>)">
<div align="center">
<div align="center">

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="30%">
			<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px; width:95%">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" align="right" nowrap="nowrap"><font style="font-family: 黑体; font-size: 16px;">采购批号：</font></td>
				  <td width="70%" align="left"><font style="font-family: 黑体; font-size: 16px;"><a href="<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/supplier_purchase_detail.html?purchase_id=<%=purchase_id%>">P<%=purchase_id%></a></font></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">交货单号：</font></td>
					<td align="left"><font style="font-family:黑体; font-size:16px"><%=delivery_order.getString("delivery_order_number").equals("")?"P"+purchase_id+"-"+number:delivery_order.getString("delivery_order_number")%></font></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">供应商：</font></td><td align="left"><%=supplier.getString("sup_name") %></td>
				</tr>
			</table>
			</div>
		</td>
		<td width="40%">
			<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px; width:95%">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="32%" align="right" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">货运公司：</font></td>
				  <td width="68%" align="left"><input id="waybill" name="waybill" type="text" style="width:100%;" value="<%=delivery_order.getString("waybill_name") %>"/></td>
				</tr>
				<tr>
					<td align="right"><font style="font-family:黑体; font-size:16px">运单号：</font></td>
					<td align="left"><input id="waybill_id" name="waybill_id" type="text" style="width:100%;" value="<%=delivery_order.getString("waybill_number") %>"/></td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">预计到达日期：</font></td>
					<td align="left"><input type="text" id="eta" name="eta" value="<%=delivery_order.getString("arrival_eta").equals("")?DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay():delivery_order.getString("arrival_eta")%>" style="width: 70px;"/></td>
				</tr>
			</table>
			</div>
		</td>
		<td align="right"><span style="padding-top:10px;padding-bottom:10px;">
		  <%
		  	if(delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.READY)
		  	{
		  %>
		  	<input class="long-button" name="button2" type="button" value="上传" onclick="uploadDeliveryOrderDetail()"/>
		  <%
		  	}
		  %>
		  
&nbsp;&nbsp;
<input name="button2" type="button" value="下载" onclick="downloadDeliveryOrder()" class="long-button-export"/>
		</span>		</td>
		</tr>
	  <tr>
			<td colspan="2" style=" padding-top:10px;">
				<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px; width:95%">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td width="19%" align="right" valign="middle" nowrap="nowrap"><font style="font-family:黑体; font-size:16px">交货地址：</font></td>
							<td width="81%" align="left" valign="middle"><%=delivery_order.getString("delivery_address") %></td>
						</tr>
						<tr>
							<td valign="middle" align="right"><font style="font-family:黑体; font-size:16px">联系人：</font></td>
							<td align="left" valign="middle"><%=delivery_order.getString("delivery_linkman") %></td>
						</tr>
						<tr>
							<td valign="middle" align="right"><font style="font-family:黑体; font-size:16px">联系电话：</font></td>
							<td><%=delivery_order.getString("delivery_linkman_phone") %></td>
						</tr>
					</table>
				</div>
			</td>
			<td align="right" nowrap="nowrap"><span style="padding-top:10px;padding-bottom:10px;">
			  <%
			  	if(delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.READY)
			  	{
			  %>
			  <input name="button" class="long-button-mod" type="button" value="保存" onclick="saveDeliveryOrder()"/>
&nbsp;&nbsp;
			   <%
			   	}
			   %>
<input name="button" type="button" value="打印" class="long-button-print" onclick="print()"/>
			  <%
			  	if(delivery_order.get("delivery_order_status",0)==DeliveryOrderKey.READY&&delivery_order_id!=0)
			  	{
			  %>
&nbsp;&nbsp;<input name="button" class="long-button" type="button" value="已起运" onclick="supplierIntransitDelivery(<%=delivery_order_id%>,'<%=delivery_order.getString("delivery_order_number")%>')"/>
			  <%
			  	}
			  %>
			</span></td>
	  </tr>
	</tr>
	
</table>
</div>
<br/>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table id="gridtest"></table>
	<div id="pager2"></div> 
	
	<script type="text/javascript">
			var	lastsel;
			var mygrid = $("#gridtest").jqGrid({
					scroll: 1,
					sortable:true,
					url:'dataDeliveryOrderDetails.html',//dataDeliveryOrderDetails.html
					datatype: "json",
					width:parseInt(document.body.scrollWidth*0.95),
					height:250,
					autowidth: false,
					shrinkToFit:true,
					postData:{delivery_order_id:<%=delivery_order_id%>,purchase_id:<%=purchase_id%>},
					jsonReader:{
				   			id:'delivery_order_detail_id',
	                        repeatitems : false
	                	},
				   	colNames:['delivery_order_detail_id','商品名','单位','采购单目前已交货','订购量','到达数量','本次发货','所在箱号','delivery_order_id'], 
				   	colModel:[ 
				   		{name:'delivery_order_detail_id',index:'delivery_order_detail_id',hidden:true,sortable:false},
				   		{name:'product_name',index:'product_name',editable:true,align:'left'},
				   		{name:'unitname',index:'unitname',align:'center',width:60},
				   		{name:'reapcount',index:'reapcount',sortable:false,align:'center'},
				   		{name:'purchasecount',index:'purchasecount',sortable:false,align:'center'}, 
				   		{name:'delivery_reap_count',index:'delivery_reap_count',sortable:false,align:'center',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","}},
				   		{name:'delivery_count',index:'delivery_count',formatter:'number',formatoptions:{decimalPlaces:1,thousandsSeparator: ","},editrules:{number:true},editable:true,sortable:false,align:'center'},
				   		{name:'delivery_box',index:'delivery_box',editable:true},
				   		{name:'delivery_order_id',index:'delivery_order_id',editable:true,editoptions:{readOnly:true,defaultValue:<%=delivery_order_id%>},hidden:true,sortable:false}	
				   		], 
				   	rowNum:20,//-1显示全部
				   	mtype: "POST",
				   	pager:'#pager2',
				   	sortname: 'delivery_order_detail_id', 
				   	viewrecords: true, 
				   	sortorder: "asc", 
				   	cellEdit: true, 
				   	cellsubmit: 'remote',
				   	afterEditCell:function(id,name,val,iRow,iCol)
				   				  { 
				   					if(name=='product_name') 
				   					{
				   						autoComplete(jQuery("#"+iRow+"_product_name","#gridtest"));
				   					}
				   				  }, 
				   	afterSaveCell:function(rowid,name,val,iRow,iCol)
				   				  { 
				   				  	if(name=='product_name') 
				   					{
				   						ajaxModProductName(jQuery("#gridtest"),rowid);
				   				  	}
				   				  }, 
				   	cellurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action',
				   	errorCell:function(serverresponse, status)
				   				{
				   					
				   					alert(errorMessage(serverresponse.responseText));
				   				},
				   	editurl:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/gridEditDeliveryOrderDetail.action'
				   	}); 		   	
			   	jQuery("#gridtest").jqGrid('navGrid',"#pager2",{edit:false,add:true,del:true,search:true,refresh:true},{closeAfterEdit:true,afterShowForm:afterShowEdit,beforeShowForm:beforeShowEdit},{closeAfterAdd:true,beforeShowForm:beforeShowAdd,afterShowForm:afterShowAdd,errorTextFormat:errorFormat},{},{multipleSearch:true,showQuery:true,sopt:['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc','nu','nn']});
			   	jQuery("#gridtest").jqGrid('navButtonAdd','#pager2',{caption:"Columns",title:"Reorder Columns",onClickButton:function (){jQuery("#gridtest").jqGrid('columnChooser');}});
			   
	</script>
		</td>
	</tr>
</table>
<br/>
<table width="95%" border="0">
	<tr>
		<td align="right"><input type="button" class="normal-white" value="返回" onclick="window.location.href='<%=ConfigBean.getStringValue("systenFolder")%>administrator/purchase/supplier_purchase_detail.html?purchase_id=<%=purchase_id%>'"/></td>
	</tr>
</table>
</div>
<form name="download_form" method="post"></form>

<form action="" method="post" name="save_deliveryOrder_form">
	<input type="hidden" name="delivery_order_id" value="<%=delivery_order_id%>"/>
	<input type="hidden" name="waybill_number"/>
	<input type="hidden" name="waybill_name"/>
	<input type="hidden" name="arrival_eta"/>
	<input type="hidden" name="purchase_id" value="<%=purchase_id%>"/>
	<input type="hidden" name="filename" value="<%=filename%>"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>"/>
</form>

<form action="delivery_order_detail.html" name="virtual_delivery_order_form">
	<input type="hidden" name="purchase_id"/>
	<input type="hidden" name="filename"/>
</form>
<script type="text/javascript">
	$("#eta").date_input();
	
	function supplierIntransitDelivery(delivery_order_id,number)
	{
		if(confirm(number+"的货物已发送了？"))
		{
			var para = "delivery_order_id="+delivery_order_id;
		$.ajax({
				url: '<%=ConfigBean.getStringValue("systenFolder")%>action/delivery/intransitDelivery.action',
				type: 'post',
				dataType: 'json',
				timeout: 60000,
				cache:false,
				data:para,
							
				beforeSend:function(request)
				{
				},
				error: function(e)
				{
					alert(e);
					alert("提交失败，请重试！");
				},
				success: function(data)
				{
					if(data.rel)
					{
						window.location.href="delivery_order_detail.html?delivery_order_id="+delivery_order_id;
					}
					
				}
			});
		}							
	}
</script>
</body>
</html>
<%
	}
	catch(Exception e)
	{
		response.sendRedirect("http://www.1you.com/");
	}
%>
