<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>

<%
	String delivery_order_id = StringUtil.getString(request,"delivery_order_id");
	String inserted = StringUtil.getString(request,"inserted");
	DBRow[] rows = deliveryMgrLL.getDeliveryOrderDetailByDeliveryOrderId(delivery_order_id);
	
%>
<html>
  <head>
    <title>到货</title>
		<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
		<link href="../comm.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
		<script type='text/javascript' src='../js/jquery.form.js'></script>
		<script type="text/javascript" src="../js/select.js"></script>
		<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
		<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
		
		<%-- Frank ****************** --%>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
		<%-- ***********************  --%>
		
		<%-- Frank ****************** --%>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
		<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
		<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
		<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
		<%-- ***********************  --%>
		

		<script type="text/javascript">
			$().ready(function() {	
				<%-- Frank ****************** --%>
				addAutoComplete($("#product_name"),
						"<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/getSearchProductsJSON.action",
						"p_name");
				<%-- ***********************  --%>	
			});

			function closeWindow(){
				$.artDialog.close();
			}

			function deleteRow(input){  
		          var s=input.parentNode.parentNode.rowIndex;
		          document.getElementById("tables").deleteRow(s); 
		          //var num=document.getElementById("tables").rows.length;  
		         
		    }

		    function addRow() {
			    if($('#product_name').val()=="") {
				    alert('请输入商品!');
				    return;
			    }
			    if($('#stockIn_count').val()==""||$('#stockIn_count').val()=="0") {
				    alert('请输入实到数量!');
				    return;
			    }
			    
		    	var row = document.getElementById("tables").insertRow(document.getElementById("tables").rows.length);
		    	var add1=row.insertCell(0).innerHTML = "<input type='hidden' name='product_name' value='"+document.getElementById("product_name").value+"'/>"+document.getElementById("product_name").value;
				var add2=row.insertCell(1).innerHTML = "0";
				var add3=row.insertCell(2).innerHTML = "<input type='text' name='stockIn_count' value='"+document.getElementById("stockIn_count").value+"'/>";
				var add4=row.insertCell(3).innerHTML = "<input type='button' value='删除' onclick='deleteRow(this)'/>";
				$('#product_name').val('');
				$('#stockIn_count').val('');

		    }

		    function submitStockTempIn() {
			    var table = document.getElementById("tables");
			    if(document.getElementById("tables").rows.length>0) {
			    	$('#stockInFrm').submit();
			    }
		    }

		    var inserted = false;
			<%
				if(inserted.equals("1")) {
			%>
					inserted = true;
			<%
				}
			%>
			
		    function init() {
				if(inserted) {
					parent.location.reload();
					closeWindow();
				}
			}

			$(document).ready(function(){
				init();
			});
		</script>
  </head>
  
  <body>
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
	<fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
		<legend style="font-size:15px;font-weight:normal;color:#999999;">交货单到货</legend>
		<form id="stockInFrm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/delivery/deliveryStockTempInAction.action">
		<input type="hidden" name="backurl" value='<%=ConfigBean.getStringValue("systenFolder")%>administrator/delivery/delivery_stock_temp_in.html?delivery_order_id=<%=delivery_order_id %>&inserted=1'/>
		<input type="hidden" id="delivery_order_id" name="delivery_order_id" value="<%=delivery_order_id %>">
		<table id="tables" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">商品名称</td>
				<td width="20%">应到数量</td>
				<td width="20%">实到数量</td>
				<td width="20%">&nbsp;</td>
			</tr>
			<%
				for(int i=0; rows!=null && i<rows.length; i++) {
					String product_id = rows[i].getString("product_id");
					DBRow productRow = deliveryMgrLL.getProductById(product_id);
					String p_name = productRow.getString("p_name");
					String delivery_count = rows[i].getString("delivery_count");
			%>
			<tr>
				<td>
					<input type="hidden" name="product_name" value="<%=p_name %>"/>
					<%=p_name %>
				</td>
				<td>
					<%=delivery_count %>
				</td>
				<td>
					<input type="text" name="stockIn_count" value="<%=delivery_count %>"/>
				</td>
				<td><input type="button" value="删除" onclick="deleteRow(this)"/></td>
			</tr>
			<%
				}
			%>
			</table>
			</form>
			<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">
					<input type="text" id="product_name" value=""/>
				</td>
				<td width="20%">
					0
				</td>
				<td width="20%">
					<input type="text" id="stockIn_count" value=""/>
				</td>
				<td width="20%"><input type="button" value="添加" onclick="addRow()"/></td>
			</tr>
			</table>
			
	</fieldset>	

	</td>
  </tr>
  <tr>
    <td align="right" width="100%" valign="middle" class="win-bottom-line">
      <input name="insert" type="button" class="normal-green-long" onclick="submitStockTempIn()" value="确定" >
      <input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="取消" >
    </td>
  </tr>
</table>
  </body>
</html>
