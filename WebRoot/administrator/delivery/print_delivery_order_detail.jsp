<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.beans.AdminLoginBean"%>
<%@ page import="java.util.HashMap"%>
<%@page import="com.cwc.app.key.PurchaseKey"%>
<%@page import="java.util.Date"%>
<%@ include file="../../include.jsp"%> 
<%
	long purchase_id = StringUtil.getLong(request,"purchase_id");
	long delivery_order_id = StringUtil.getLong(request,"delivery_order_id"); 
	
	DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrder(delivery_order_id);
	
	DBRow[] delivery_order_details; 
	
	delivery_order_details = deliveryMgrZJ.getDeliveryOrderDetails(delivery_order_id,purchase_id,null,null,null,null);
	
	DBRow purchase = purchaseMgr.getDetailPurchaseByPurchaseid(String.valueOf(purchase_id));
	
	DBRow supplier;
	long sid;
	try
	{
		sid = Long.parseLong(purchase.getString("supplier"));
	}
	catch(NumberFormatException e)
	{
		sid = 0;
	}
	
	if(sid ==0)
	{
		supplier = new DBRow();
		String name = "";
		if(session.getAttribute("supplier_name")!=null)
		{
			name = session.getAttribute("supplier_name").toString();
		}
		 
		supplier.add("sup_name",name);
	}
	else
	{
		supplier = supplierMgrTJH.getDetailSupplier(sid);
	}
	
	double sum = 0;
%>
<style type="text/css">
<!--
.STYLE1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>

<table width="720px" border="0" align="left" cellpadding="0" cellspacing="0">
<thead>
  <tr>
  	<td colspan="6">
		<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2" align="center" valign="middle" style="font-family: Arial Black;font-size: 19px; padding-bottom:10px;">Visionari LLC</td>
				<td width="45%" align="center" valign="middle" nowrap="nowrap"  style="font-family: 黑体;font-size: large;padding-bottom:10px;">微尘大业交货单</td>
				<td width="29%" colspan="2" align="left" style="font-family:黑体;font-size: 15px;padding-bottom:10px;">采购批号：P<%=purchase_id%>
				<br />
				交货单号：<%=delivery_order.getString("delivery_order_number")%><br/>
			  <span style="font-family: C39HrP48DmTt;font-size:40px;">*<%=delivery_order.getString("delivery_order_number")%>*</span></td>
		  </tr>
			   <tr>
			   	<td colspan="5" style=" padding-bottom:5px; padding-top:5px;">
					<hr/>
					<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
					<table width="100%">
						<tr>
							<td width="12%" align="right" nowrap="nowrap" style="font-family:黑体;font-size: 16px;">供应商：</td>
							<td width="49%" align="left"><%=supplier.getString("sup_name")%></td>
							<td width="13%" align="right" nowrap="nowrap" style="font-family:黑体;font-size: 16px;">交货日期：</td>
							<td width="26%" align="left"><%=delivery_order.getString("delivery_date")%></td>
						</tr>
						<tr>
							<td align="right" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">货运公司：</td>
							<td align="left"><%=delivery_order.getString("waybill_name") %></td>
							<td nowrap="nowrap" style="font-family:黑体;font-size: 16px;" align="right">运单号：</td>
							<td align="left"><%=delivery_order.getString("waybill_number")%></td>
						</tr>
					</table>
					</div>
					<hr/>
				</td>
			  </tr>
		 </table>
	</td>
  </tr>
    <tr>
		<th nowrap="nowrap"  align="left"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">商品名</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">单位</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">已交货</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">订购量</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">本次发货</th>
		<th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;border-left: 2px #000000 solid;border-top: 2px #000000 solid;border-bottom: 2px #000000 solid;border-right: 2px #000000 solid;font-family:黑体;font-size: 16px;background-color:#eeeeee">所在箱号</th>
	</tr>
  </tr>
  </thead>
  	<%
		for(int i = 0;i<delivery_order_details.length;i++)
		{
	%>
		<tr>
		<td align="left" height="30" style="font-family:黑体;font-size: 15px;border-left: 2px #000000 solid;border-bottom: 1px #000000 solid;padding-left:3px;"><%=delivery_order_details[i].getString("product_name")%></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;"><%=productMgr.getDetailProductByPcid(delivery_order_details[i].get("product_id",0l)).getString("unit_name")%></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid";><%=delivery_order_details[i].get("reapcount",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;"><%=delivery_order_details[i].get("purchasecount",0f) %></td>
		<td align="center" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;border-bottom: 1px #000000 solid;"><%=delivery_order_details[i].get("delivery_count",0f) %></td>
		<td align="left" style="font-family:黑体;font-size: 15px;border-left: 1px #000000 solid;;border-bottom: 1px #000000 solid;border-right: 2px #000000 solid;padding-left:3px;"><%=delivery_order_details[i].getString("delivery_box")%></td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="middle" style="border-left: 2px #000000 solid;border-bottom: 1px #000000 solid; padding-top:5px; padding-bottom:5px;">
			<span style="font-family: C39HrP48DmTt;font-size:40px;">*<%=delivery_order_details[i].getString("product_barcode")%>*</span>
			</td>
			<td colspan="4" style="border-right: 2px #000000 solid;border-bottom: 1px #000000 solid;"></td>
		</tr>
	<%
		}
	%>
	<tfoot>
		<tr>
			<td colspan="6" style="padding-top:15px;">
				<hr/>
				<div style="border:2px #dddddd solid;background:#eeeeee;-webkit-border-radius:7px;-moz-border-radius:7px;">
				<table width="100%">
				  <tr>
					<td width="10%" align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">交货地址：</td>
					<td width="90%" align="left"><%=delivery_order.getString("delivery_address")%></td>
				  </tr>
				  <tr>
					<td align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">收货人：</td>
					<td align="left"><%=delivery_order.getString("delivery_linkman")%></td>
				  </tr>
				  <tr>
					<td align="right" valign="middle" style="font-family:黑体;font-size: 16px;" nowrap="nowrap">收货电话：</td>
					<td align="left"><%=delivery_order.getString("delivery_linkman_phone")%></td>
				  </tr>
				</table>
				</div>
				<hr/>
			</td>
		</tr>
		
	</tfoot>
</table>



