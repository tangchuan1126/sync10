<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<jsp:useBean id="fileWithClassKey" class="com.cwc.app.key.FileWithClassKey"/>
<%@ taglib uri="/turboshop-tag" prefix="tst" %>
<%@ include file="../../include.jsp"%>
<%
	long association_id = StringUtil.getLong(request,"association_id",0l);
	int association_type = StringUtil.getInt(request,"association_type",0);
	String update = StringUtil.getString(request,"update");
	DBRow[] imageList = fileMgrZJ.getFileByWithIdAndType(association_id, association_type);
	request.getSession().setAttribute("imageList",imageList);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'upload_image.jsp' starting page</title>
    <style type="text/css">
	<!--
	.create_order_button
	{
		background-attachment: fixed;
		background: url(../imgs/create_order.jpg);
		background-repeat: no-repeat;
		background-position: center center;
		height: 51px;
		width: 129px;
		color: #000000;
		border: 0px;
		
	}
	.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}
	-->
	</style>
	<link href="../comm.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../common.js"></script>
	<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
	<script type='text/javascript' src='../js/jquery.form.js'></script>
	
	<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
	<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
	<script type="text/javascript">
	function uploadImage()
	{
		if($('#voucher').val() == ""){
			alert("请输入上传文件!");
		}else {
			$('#uploadImageForm').submit();
		}
	}

	function delImage(id) {
		$('delId').val(id);
		$('#delImageForm').attr('action',$('#delImageForm').attr('action')+'?file_id='+id);
		$('#delImageForm').submit();
	}
	</script>
  </head>
  
  <body>
  <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:18px;margin-top:10px;">
  <tr>
    <td>
		 <fieldset style="border:2px #cccccc solid;padding:10px;-webkit-border-radius:7px;-moz-border-radius:7px;">
<legend style="font-size:15px;font-weight:normal;color:#999999;">上传文件</legend>
<table width="90%" border="0">
<form name="uploadImageForm" id="uploadImageForm" value="uploadImageForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/fileUpload/addImageAction.action" enctype="multipart/form-data" method="post">
<input type="hidden" name="backUrl" id="backUrl" value="<%=ConfigBean.getStringValue("systenFolder")%>page/administrator/delivery/delivery_upload_image.action?association_id=<%=association_id %>&association_type=<%=association_type %>"/>
<input type="hidden" name="sn" id="sn" value="D"/>
<input type="hidden" name="path" id="path" value="delivery"/>
	<td height="25" align="right"  class="STYLE2" nowrap="nowrap">上传文件:</td>
	  <td>&nbsp;</td>
       <td >
        <input type="file" id="voucher" name="voucher" />
        <input type="hidden" name="association_id" id="association_id" value="<%=association_id %>">
        <input type="hidden" name="association_type" id="association_type" value="<%=association_type %>">
	   </td>
	   <td align="left" nowrap="nowrap"><font style="color: red">*文件格式为:jpg,gif,bmp,doc,docx,xls,xlsx</font></td>
		<td align="left" nowrap="nowrap">
			<input type="button" name="voucherSubmit" id="voucherSubmit" value="上传" onclick="uploadImage()">
		</td>
	 </tr>
	 <tr>
	 	<td class="STYLE2">文件分类:</td>
	 	<td>&nbsp;</td>
	 	<td colspan="2">
        <select name="file_with_class">
        	<option value="<%=fileWithClassKey.PACKING %>">包装箱</option>
        	<option value="<%=fileWithClassKey.OUTERPACKING %>">包装照片</option>
        	<option value="<%=fileWithClassKey.PRODUCTPHOTO %>">商品照片</option>
        	<option value="<%=fileWithClassKey.TRANSPORTPHOT %>">转运照片</option>
        </select>
	 	</td>
	 </tr>
</table>
</form>
<table border="0">
<form id="delImageForm" name="delImageForm" action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/fileUpload/delImageAction.action" method="post">
<input type="hidden" name="backUrl" id="backUrl" value="<%=ConfigBean.getStringValue("systenFolder")%>page/administrator/delivery/delivery_upload_image.action?association_id=<%=association_id %>&association_type=<%=association_type %>"/>
		<%
			for(int i=0; i<imageList.length; i++) {				
				out.println("<tr><td nowrap='nowrap'align='right'  class='STYLE2' >上传的凭证:</td><td align='left'>");
				out.println("<a target='_blank' href='"+ConfigBean.getStringValue("systenFolder")+"upload/delivery/"+imageList[i].getString("file_name")+"'>"+imageList[i].getString("file_name")+"</a>");
				out.println("</td><td>");
				out.println(fileWithClassKey.getFileWithTypeId(imageList[i].get("file_with_class",0)));
				out.println("</td>");
				out.println("<td><input type='button' name='del' id='del' value='删除' onclick=delImage("+ imageList[i].getString("file_id") +")>");
				out.println("</td></tr>");
			}
		%>
</form>
</table>
</fieldset>	
	</td>
  </tr>
  	<tr>
		<td style="padding-top: 20px;" align="right"><input name="cancel" type="button" class="normal-white" onclick="closeWindow()" value="退出" <%= update.equals("1")?"style='display:none'":"" %>></td>
	</tr>
</table>
<script type="text/javascript">
function closeWindow(){
	parent.location.reload();
	$.artDialog.close();
}
</script>
  </body>
</html>
