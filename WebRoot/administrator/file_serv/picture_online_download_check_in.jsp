<!DOCTYPE html>
<%@page import="com.cwc.app.key.CheckInEntryPriorityKey"%>
<%@page import="com.cwc.app.key.CheckInOrderComeStatusKey"%>
<%@page import="com.cwc.app.key.WaitingTypeKey"%>
<%@page import="com.cwc.app.api.zr.LoadBarUseMgrZr"%>
<%@page import="com.cwc.app.key.OccupyTypeKey"%>
<%@page import="java.util.List"%>
<%@page import="com.cwc.util.StringUtil"%>
<%@page import="com.cwc.app.util.ConfigBean"%>

<html>
<head>

<%
	long dlo_id = StringUtil.getLong(request, "file_with_id");
	String base_path = StringUtil.getString(request, "base_path");
%>
<title>Picture Online Show</title>
<link href="Styles/photo.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<!-- é®ç½© -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script language="javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/picture_online_show/jquery.smoothZoom.min.js"></script>

<script type="text/javascript" src="../js/picture_online_show/jquery.prettyPhoto.js"></script>
<link type="text/css" href="../js/picture_online_show/css/prettyPhoto.css" rel="stylesheet" />


<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<script type="text/javascript" src="../js/showMessage/showMessage.js"></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
 
 <link rel="stylesheet" type="text/css" href="../js/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="../js/easyui/themes/icon.css" />

 <!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 下拉框多选 -->
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.css" rel="stylesheet"/>
<link type="text/css" href="../js/fullcalendar/jquery.multiselect.filter.css" rel="stylesheet" />
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.min.js"></script>	 
<script type="text/javascript" src="../js/fullcalendar/jquery.multiselect.filter.min.js"></script>
<style>
.ui-datepicker{font-size: 0.9em;}
.set{padding:2px;width:95%;word-break:break-all;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 0px;border: 2px solid green;} 
p{text-align:left;}
span.stateName{width:32%;float:left;text-align:right;font-weight:normal;}
span.stateValue{width:68%;display:block;float:left;text-indent:4px;overflow:hidden;text-align:left;font-weight:normal;}
.zebraTable td { border-bottom: 0px solid #DDDDDD;padding-left: 10px;}
span.spanBold{font-weight:bold;}
span.fontGreen{color:green;}
span.fontRed{color:red;}
span.spanBlue{color:blue;}
.search_input
{
	background:url(../imgs/search_bg.jpg) repeat 0 0;
	width:400px; 
	height:24px;
	font-weight:bold;
	border:1px #bdbdbd solid;
	
}
.search_shadow_bg
{
	background:url(../imgs/search_shadow_bg2.jpg) no-repeat 1px 4px;
	width: 420px; 
	height:43px;
	padding-top:3px;
	padding-left:3px;
	margin-bottom:5px;
}
#easy_search_father {
	position:absolute;
	width:0px;
	height:0px;
	z-index:1;
}
#easy_search {
	position:absolute;
	left:318px;
	top:-18px;
	width:55px;
	height:30px;
	z-index:9999;
	visibility: visible;
}

/* 调整 refresh 位置 */
.adjustRefresh {
	background: url(../check_in/imgs/button_long_refresh_2.png) no-repeat 0 0;
	width: 130px;
	height: 42px;
}

/* legend_foot fieldset 右下角开口 */
.demoa {
	bottom: -8px;	
	display: inline-block;
	position: absolute;
	text-align: left;
	left: 27px;
	background: #FCFCFC;
	padding:0px 5px;
}
#containerType {
	bottom: -8px;	
	right: 16px;
	display: inline-block;
	text-align: right;
	position: absolute;
	background:#fff;
}
.purpose {
	top: -2px;	
	right: 30px;
	display: inline-block;
	text-align: right;
	position: absolute;
	background:#fcfcfc;
}
.loadBar{
	margin:5px 0px;
}
div.page-last:hover{
	background-position:-45px bottom;
	
}
div.page-last{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px bottom;
    cursor: pointer;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
    margin-right:10px;
}
div.page-last-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -91px bottom;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
    margin-right:10px;
}
div.page-frist:hover{
	background-position:-45px 47px;
	
}
div.page-frist{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px 47px;
    color: #aaa;
    display: inline-block;
    height: 23px;
    line-height: 21px;
    width: 44px;
}
div.page-first-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -91px 48px;
    display: inline-block;
    height: 24px;
    line-height: 24px;
    text-align: center;
    width: 44px;
}
div.page-next{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px -24px;
    color: #aaa;
    cursor: pointer;
    display: inline-block;
    height: 22px;
    line-height: 21px;
    width: 24px;
}
div.page-next-none{
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -54px -24px;
    color: #aaa;
    display: inline-block;
    height: 22px;
    line-height: 21px;
    width: 24px;
}
div.page-next:hover{
	 background-position: -27px -24px;
} 
div.page-up{
	cursor:pointer;
    background-image: url("./imgs/page-first-last-btn.png");
    background-position: -1px top;
    color: #aaa;
    cursor: pointer;
    display: inline-block;
    height: 22px;
    line-height: 22px;
    width: 24px;
}
div.page-up-none{
    background-image: url("./imgs/page-first-last-btn.png");
     background-position: -53px top;
    color: #aaa;
    display: inline-block; 
    height: 22px;
    line-height: 22px;
    width: 24px;
}

div.page-up:hover{
	 background-position: -27px top;
	
}
div.page-no{
	background: none repeat scroll 0 0 white;
    border-radius: 5px;
    display: inline-block;
    height: 21px;
    line-height: 22px;
    padding: 0 7px;
    white-space:nowrap; 
    box-shadow: 1px 1px 1px 0 #aaa inset;
}
div.page-no:hover{
	background: #0076ff;
	color:#FFF;
}
</style>

<style type="text/css">
	body {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
	td {font-size:8pt; font-family:Verdana, Geneva, sans-serif}
</style>
<style type="text/css">
		
			.smooth_zoom_preloader {background-image: url("../js/picture_online_show/images/preloader.gif");}	
			.smooth_zoom_icons {background-image: url("../js/picture_online_show/images/icons.png");}
			li a{
				position: relative;
			}
		.count{
			    background: none repeat scroll 0 0 #2894ff;
			    border-radius: 125px;
			    color: #d2e9ff;
			    font-weight: 700;
			    height: 20px;
			    line-height: 21px;
			    position: absolute;
			    right: 28px;
			    top: 7px;
			    width: 20px;
		}
	  
</style>
<script type="text/javascript">
	$(function(){
		var base_path = '<%=ConfigBean.getStringValue("systenFolder")%>';
		//遮罩
		$.blockUI.defaults = {
				 css: { 
				  padding:        '8px',
				  margin:         0,
				  width:          '170px', 
				  top:            '45%', 
				  left:           '40%', 
				  textAlign:      'center', 
				  color:          '#000', 
				  border:         '3px solid #999999',
				  backgroundColor:'#ffffff',
				  '-webkit-border-radius': '10px',
				  '-moz-border-radius':    '10px',
				  '-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
				  '-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
				 },
				 //设置遮罩层的样式
				 overlayCSS:  { 
				  backgroundColor:'#000', 
				  opacity:        '0.6' 
				 },
				 
				 baseZ: 99999, 
				 centerX: true,
				 centerY: true, 
				 fadeOut:  1000,
				 showOverlay: true
				};
		
	})
	
	function ajaxSearch(){


	}
	function setZoom (){
  		var imgObj = new Image();
  	    imgObj.src = $("#rotateImg").attr('src');
  	    var sizeImg = imgObj.width>imgObj.height?imgObj.width:imgObj.height;
  	    $('#fullResImage').css({
  	    "width":sizeImg+"px",
  	    "height":sizeImg+"px",
  	    });
  	    if(imgObj.width<imgObj.height){				//高图需要进行横向居中。正好相等则什么也不做 
  	    	var leftsize = Math.floor((1-imgObj.width/imgObj.height)/2*100);//计算应距离左边边距百分之多少，并给过高的图片加行left属性
  	    	 $("#rotateImg").css({
  				"position" : "absolute",
  				"left" : leftsize+"%"
  			});
  	    }else if(imgObj.width>imgObj.height){		//宽图需要进行纵向居中
  	    	var topsize = Math.floor((1-imgObj.height/imgObj.width)/2*100);//计算应距离上边距百分之多少，并给过宽的图片加行top属性
  		   $("#rotateImg").css({
  				"position" : "absolute",
  				"top" :""+topsize+"%",
  			});
  		}
		$('#fullResImage').smoothZoom('destroy').smoothZoom();
	}

	function closeZoom (){
		$('#fullResImage').smoothZoom('destroy');
	}
	//图片旋转
	var indexZ = 0;
	function turnLeft()
	{
		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ -= 90) + "deg)",
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});

	} 
	function turnRight()
	{
 		if (indexZ == 360 | indexZ == -360)
			indexZ = 0;
		$("#rotateImg").css({
			"-moz-transform" : "rotate(" + (indexZ += 90) + "deg)"
		});
		$("#rotateImg").css({
			"-webkit-transform" : "rotate(" + (indexZ) + "deg)"
		});
	}
	
</script>
</head>
<body>
	<div class="demo" style="width:100%" >
	  <div id="tabs">
		<ul>
			
			<li><a href="#checkin_filter">Photo Search</a></li>
		</ul>
		
	      <div id="checkin_filter" >
	      		<table width="100%" height="61" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left">
						<div style="margin-bottom: 2px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td  >
										<b>Enter ID</b> :&nbsp;
										<div style="display:inline"><input name="search_key" id="search_key" type="text"   style="width:300px;font-size:15px;font-family:Arial;color:#333333;" id="search_key" onkeydown="if(event.keyCode==13)search()" /></div>
										<input type="button" class="button_long_search" value="Search" onclick="search();"/> 
										
									</td>
									<td >
									
								</td>
								</tr>
								
							</table>
						</div>
					</td>
					<td width="67" align="center"></td>
				    <td width="3px"></td>
				    
				</tr>
			</table>
	      </div>
       
       </div>
      
    </div>

	<div class="content">
		
		<div class="module-toolbar">
			<span class="folder_show_toolbar">
				<input type="checkbox" id='chk-select' style="display:none;"/>All File
				&nbsp;&nbsp;<input type="button" id='btn-download' class="long-button" style="display:none;" value="Download"/>
			</span>

			
			<a title="Home" class="view" href="javascript:void(0)" >Home</a>
			<a title="List" class="list" href="javascript:void(0)" >List</a>
		</div>
		<div id="show_photo">
			<div id="nodata" style="display:none;font-size:25px;text-align:center;padding-top:50px;color:#bbbbbb;">No picture found!</div>
			<div class="photo_folder">
				
				<span style="float:right;margin-right:50px;" id="pic_count"></span>
				<ul class="ul_folder" id="picture_folder">
					
				</ul>
				<div style="clear:both;"></div>
			</div>
			<div id="photo_item">
				
			</div>
		</div>
  	</div>
</body>
<script>
	$("#tabs").tabs({
   		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>'
   });

   $(function(){
   		addAutoComplete($("#search_key"),
			"<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/checkin/GetSearchCheckInJSONAction.action",
			"merge_field","dlo_id");

   		$(document).delegate(".chk-folder","click",function(e){

   			var event=e||window.event,
        	o = (event.srcElement||event.target),
        	checked = $(o).prop("checked"),
        	file_with_class = $(o).attr("file_with_class"),
        	filelist = $("[parent_file_with_class='"+file_with_class+"']")||[];
        	for(var i = 0 ;i < filelist.length; i++){
        		$(filelist[i]).prop("checked",checked);
        	}
        	var cnt =0, folderlist = $(".chk-folder")||[];
        	for(var i = 0 ;i < folderlist.length; i++){
        		if($(folderlist[i]).prop("checked")==true){
        			cnt ++;
        		}
        	}
        	if(cnt ==0){
        		$("#chk-select").prop("checked",false);
        	}
        	else if(cnt == folderlist.length){
        		$("#chk-select").prop("checked",true);
        	}

        	countingChecked();

   		});


   		$(document).delegate(".chk-file","click",function(e){

   			var event=e||window.event,
        	o = (event.srcElement||event.target),
        	checked = $(o).prop("checked"),
        	fileId = $(o).attr("file-id"),

        	filelist = $("[file-id='"+fileId+"']")||[];
        	for(var i = 0 ;i < filelist.length; i++){
        		$(filelist[i]).prop("checked",checked);
        	}
        	var file_with_class = $(o).attr("parent_file_with_class");
        	filelist = $("[parent_file_with_class='"+file_with_class+"']")||[];
        	var cnt = 0;
        	for(var i = 0 ;i < filelist.length; i++){
        		if($(filelist[i]).prop("checked")==true){
        			cnt ++;
        		}
        	}
        	if(cnt==0){
        		
        		$("[file_with_class='"+file_with_class+"']").prop("checked",false);
        	}
        	else if(cnt == filelist.length ){
        		$("[file_with_class='"+file_with_class+"']").prop("checked",true);	
        	}
        	 countingChecked();

   		});

   	    $(document).delegate("#chk-select","click",function(e){
   			var event=e||window.event,
        	o = (event.srcElement||event.target),
        	checked = $(o).prop("checked");
        	
        	$(".chk-folder").prop("checked",checked);
        	$(".chk-file").prop("checked",checked);
        	countingChecked();
   		});

   		
   		$(document).delegate("#btn-download", "click", function (e) {
			var fileIds = "", exists = {}, 
			list = $(".chk-file");
			for (var i = 0; i < list.length; i++) {
				var o = list[i];
				if ($(o).prop("checked") == true) {
					var fileId = $(o).attr("file-id");
					if (typeof(exists[fileId]) == "undefined") {
						var spit = fileIds == "" ? "" : ",";
						exists[fileId] = fileId;
						fileIds += spit + fileId;
					}
				}
			}

			if (fileIds == "") {
				alert("Please select at least one folder!")
				return;
			}
			var entityId = $("#search_key").val().replace(/\'/g, ''),
			url = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/downloadPicturesByFileIds.action?file_with_id=" + entityId + "&file_ids=" + fileIds;
			window.open(url, "height=0,width=0");
		});

   });
	
   function countingChecked(){
   		var cnt =0, exists = {}, 
   		list = $(".chk-file");
		for (var i = 0; i < list.length; i++) {
			var o = list[i];
			if ($(o).prop("checked") == true) {
				var fileId = $(o).attr("file-id");
				if (typeof(exists[fileId]) == "undefined") {
					cnt ++;	
					exists[fileId] = fileId;
				}
				
			}
		}
		if(cnt>0){
    		$("#btn-download").attr("value","Download ("+(cnt)+")");
    	}
    	else {
    		$("#btn-download").attr("value","Download");
    	}

		return cnt;
   }
   function showAllFile(){
   	   $(".folder_show_toolbar").html("<input type='checkbox' id='chk-select'/>All File &nbsp;&nbsp;<input type='button' id='btn-download' class='long-button' style='display:none;'' value='DownLoad'/><span style='display:none;' id='area_choose_info' ></span>");
   	   if($(".folder").length==0){
   	   		$("#nodata").css("display","block");
   	   		$("#chk-select").css("display","none");
   	   }
   	   else{
   	   		$("#nodata").css("display","none");
   	   		$("#chk-select").css("display","inline");
   	   }

   	   var folders = $(".chk-folder")||[];
   	   if(folders.length>0){
   	   		$("#btn-download").css("display","inline");
   	   		for(var i=0;i<folders.length;i++){
   	   			 var o = folders[i];
   	   			 if($(o).prop("checked")==true){
   	   			 	$("#chk-select").prop("checked",true);
   	   			 	break;
   	   			 }
   	   		}
   	   }
   	   countingChecked();

   }


   function search(){
		var val = $("#search_key").val();
				
		if(val.trim()=="")
		{
			alert("Please enter key word for searching");
			$("#search_key").focus();
		}
		else
		{
			val = val.replace(/\'/g,'');
			var val_search = "\'"+val.toUpperCase()+"\'";
			$("#search_key").val(val_search);
			
			var uri = '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/check_in/getPicturesByMainId.action';
			$.ajax({
		  		type:'post',
		  		url:uri,
		  		dataType:'json',
		  		async:false,
		  		data:{"entry_id":val},
		  		beforeSend:function(request){
				      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
				},
			 	success:function(data){

			 		console.log(data)
			 		$.unblockUI();       //遮罩关闭
		 			var folder = "";
		 			var pictures = "";
		 			var totalCount =0;
			 		if(data!=null&&data.length>0){
			 			/*$("#chk-select").css("display","inline");
			 			$("#btn-download").css("display","inline");*/
			 			
			 			for(var i=0;i<data.length;i++){
			 			//	console.log("the "+i+ " folder"+data[i].pictures.length);
			 				var count =0;
			 				var rows = data[i].photos||[];
			 				for(var j=0;j<rows.length;j++){
			 					if(rows[j].FILE_PATH.indexOf(".jpg")>=0 || rows[j].FILE_PATH.indexOf(".JPG")>=0){
			 						count=count+1;
			 						totalCount = totalCount+1;	
			 					}
			 				}
			 				if(count==0) {
			 					continue;
			 				}
			 				//home 文件夹
			 				folder += "<li>";
			 				folder += "<a href='javascript:void(0)'><img class='folder' alt='"+data[i].dir_name+"' value="+data[i].file_with_class+" src='images/folder.PNG'>";
			 				folder += "<span class='text'>"+data[i].dir_name+"</span>";
			 				folder += " <div style='position:absolute;left:15px;top:28px'><input type='checkbox' class='chk-folder' file_with_class='"+data[i].file_with_class+"' ></div><div class='count'>"+count+"</div></a>";
			 				folder += "</li>";
			 				//图片
			 				pictures += "<div class='photo_item'>";
			 				pictures += "<span class='ttl'>"+data[i].dir_name+"</span>";
			 				
			 				
			 				pictures += "<ul>";
			 				
			 				if(data[i].photos.length>0){
			 					for(var j=0;j<data[i].photos.length;j++){
			 						if(data[i].photos[j].FILE_PATH.indexOf(".jpg")==-1 && data[i].photos[j].FILE_PATH.indexOf(".JPG")==-1){
			 							continue;
			 						}

					 				pictures += "<li class='photo_container'>";
					 				pictures += "<div  class='image_div'>";
					 				/*pictures += "<a class='image' href='<%=base_path%>"+data[i].photos[j].FILE_ID+"'  rel='prettyPhoto[gallery"+i+"]'  title='"+data[i].photos[j].FILE_NAME+"'>";
					 				pictures += "<img id='loadImage' src='<%=base_path%>"+data[i].photos[j].FILE_ID+"' >"; */
					 				pictures += "<a class='image' href='<%=ConfigBean.getStringValue("systenFolder")%>"+data[i].photos[j].URI+"'  rel='prettyPhoto[gallery"+i+"]'  href='javascript:void(0)' title='"+data[i].photos[j].FILE_NAME+"'>";
				 					pictures += "<img id='loadImage' src='<%=ConfigBean.getStringValue("systenFolder")%>"+data[i].photos[j].URI+"' >"; 
					 				pictures += "</a>";
					 				pictures += "</div>";
					 				
					 				//pictures += "<div  class='image_name_div'>";

					 				//pictures += "<input type='checkbox' class='chk-file' parent_file_with_class='"+data[i].file_with_class+"' file-id='"+data[i].photos[j].FILE_ID+"' /><a href='javascript:void(0)' title='"+data[i].photos[j].FILE_NAME+"'>"+data[i].photos[j].FILE_NAME+"</a>";
					 				pictures += "<div  class='image_div'>";
					 				pictures += "<input type='checkbox' class='chk-file' parent_file_with_class='"+data[i].file_with_class+"' file-id='"+data[i].photos[j].FILE_ID+"' /><a href='javascript:void(0)' title='"+data[i].photos[j].FILE_NAME+"' folder-id='"+data[i].file_with_class+"' file-id='"+data[i].photos[j].FILE_ID+"'>"+data[i].photos[j].FILE_NAME+"</a>";
					 				pictures += "</div>";
					 				pictures += "</li>";
			 				
			 					}
			 				}
			 				pictures += "</ul>";
			 				pictures += "</div>";
				 			
			 			}
			 			
						 		
					}
					
		 			$("#picture_folder").html(folder);
		 			
		 			$("#photo_item").html(pictures); 
		 			showAllFile();
		 			
		 			$("#show_photo .photo_folder").show().siblings().hide().end().find("ul").show().nextAll().remove();
		 			
		 			
		 			$(".module-toolbar .view").click(function(){
		 				showAllFile();
		 				$("#show_photo .photo_folder").show().siblings().hide().end().find("ul").show().nextAll().remove();
		 			});
		 			$(".module-toolbar .list").click(function(){
		 				showAllFile();
		 				$("#photo_item").show().siblings().hide();
		 			});
		 			$(".photo_folder ul li a img").click(function(){
		 				$(".folder_show_toolbar").html("<a href='javascript:void(0)'>Back</a> |  <a href='javascript:void(0)'>All File</a>  &nbsp;&nbsp;<input type='button' id='btn-download' class='long-button'  value='DownLoad'/> "+$(this).find(".text").text());
		 				var index = $(".photo_folder ul li a img").index(this);
		 				$(".photo_folder ul").hide().parent(".photo_folder").append($("#photo_item").children().eq(index).children("ul").clone(true));
		 				$("#pic_count").val($("#photo_item").children().eq(index).children("ul li").length);
		 				 countingChecked();
		 			});
		 			
		 			$(".folder_show_toolbar a").live('click',function(){
		 				showAllFile();
		 				
		 				$("#picture_folder").show().nextAll().remove();
		 				
		 				
		 				$("#pic_count").val("");
		 			});
		 			
		 			var canvasWidth =  806;
		 			var canvasHeight = 447;
		 			
		 			//查看图片
		 			if($(window).width()* 1 < 1000){
		 		 	   	canvasWidth = 600;
		 		 		canvasHeight = 347;
		 		 	}
		 			$(".image").prettyPhoto({
		 				default_width: canvasWidth,
		 				default_height: canvasHeight,	
		 				slideshow:false, /* false OR interval time in ms */
		 				autoplay_slideshow: false, /* true/false */
		 				opacity: 0.50, /* opacity of background black */
		 				theme: 'facebook', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
		 				modal: false, /* If set to true, only the close button will close the window */	
		 				overlay_gallery: false,
		 				changepicturecallback: setZoom,
		 				callback: closeZoom,
		 				social_tools: false,
		 				image_markup: '<div style="width:'+canvasWidth+'px; height:'+canvasHeight+'px;"><div id="fullResImage"  ><img id="rotateImg"  src="{path}" /></div></div>',
		 				fixed_size: true,
		 				
		 				/******************************************
		 				Enable Responsive settings below if needed.
		 				Max width and height values are optional.
		 				******************************************/
		 				responsive: false,
		 				responsive_maintain_ratio: true,
		 				max_WIDTH: '',
		 				max_HEIGHT: ''
		 			});		
					
		 	
			 	},
			 	error:function(){
			 		$.unblockUI();       //关闭遮罩
					alert("System error >_<!!!");			 		
			 	}
			 });
		}
	}


</script>
</html>