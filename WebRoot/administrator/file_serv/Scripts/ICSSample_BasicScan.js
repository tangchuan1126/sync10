﻿var _imageCaptureParam = {
'productKey': '272E8F005CF4B62E526245A387B8815E0D5EFC392B17B1839C7DAECC0DC0594210000000',
'containerID': 'dwtcontrolContainer',   //The ID of ImageCapture Suite control div in HTML.This value is required.
    'isTrial': 'false',  

    /*
    'isTrial': 'true',  
    isTrial is used to judge whether ImageCapture Suite control is trial or full. This value is optional.
    The default value is 'TRUE', which means the control is a trial one. The value of isTrial is 'TRUE' or 'FALSE'.
    */

    /*
    'version': '9,3',   
    The version of ImageCapture Suite control, which is used to judge the version when downloading CAB.
    This value is optional. The default value is '9,3'.
    */                      
    
    /*
    'resourcesPath': 'Resources',   
    The relative path of MSI, CAB and PKG.
    This value is optional. The default value is 'Resources'.
    */    
   
    /*
    'width': 580,       //The width of ImageCapture Suite control
     This value is optional. The default value is '580'.
     */  
       
    /*
    'height': 600       //The height of  ImageCapture Suite control
    This value is optional. The default value is '600'.
     */  
       
    /*  These are events. The name of 'OnPostAllTransfer' shouldn't be changed, but the name of 'Dynamsoft_OnPostAllTransfers' can be modified. 
        Please pay attention, the name of 'Dynamsoft_OnPostAllTransfers' and 'function Dynamsoft_OnPostAllTransfers()' event must be coincident.
        
        Events are as follows. You can choose one or many according to you needs.
        Once an event is added, you must make sure the corresponding function is defined in your code.
        
        'onPostTransfer':Dynamsoft_OnPostTransfer,
        'onPostAllTransfers':Dynamsoft_OnPostAllTransfers,  
        'onMouseClick':Dynamsoft_OnMouseClick,   
        'onPostLoad':Dynamsoft_OnPostLoadfunction,    
        'onImageAreaSelected':Dynamsoft_OnImageAreaSelected,   
        'onMouseDoubleClick':Dynamsoft_OnMouseDoubleClick,   
        'onMouseRightClick':Dynamsoft_OnMouseRightClick,   
        'onTopImageInTheViewChanged':Dynamsoft_OnTopImageInTheViewChanged,   
        'onImageAreaDeSelected':Dynamsoft_OnImageAreaDeselected,    
        'onGetFilePath':Dynamsoft_OnGetFilePath  
    */  
     'onTopImageInTheViewChanged':Dynamsoft_OnTopImageInTheViewChanged                          
};


var gImageCapture;
(function() {
	gImageCapture = new Dynamsoft.ImageCapture(_imageCaptureParam);
})();

var seed;
function onPageLoad() {
    initInfo();            //Add guide info
    seed = setInterval(initControl, 500);
 }

function initControl() {
     var DWObject = gImageCapture.getInstance();
     if (DWObject) {
    	   if (DWObject.ErrorCode == 0) {
               clearInterval(seed);
               DWObject.BrokerProcessType = 1;

               var vDWTSource = document.getElementById("source");
               if (vDWTSource) {
                   vDWTSource.options.length = 0;
                   // fill in the source items.
                   for (var i = 0; i < DWObject.SourceCount; i++) {
                       vDWTSource.options.add(new Option(DWObject.GetSourceNameItems(i), i));
                   }
                   if (DWObject.SourceCount > 0) {
                       source_onchange();
                   }
               }
               var vResolution = document.getElementById("Resolution");
               if (vResolution) {
                   vResolution.options.length = 0;
                   vResolution.options.add(new Option("100", 100));
                   vResolution.options.add(new Option("150", 150));
                   vResolution.options.add(new Option("200", 200));
                   vResolution.options.add(new Option("300", 300));
                }
               vResolution.selectedIndex = 3 ;
                notifyIsOpenCapture();
           }
     }
 }
function notifyIsOpenCapture(){
	//如果是只有一个Twain的那么就是直接弹开Twain的
	var source = $("#source");
	var options = $("#source option");
	var items = "" ;
	var hasTwainCount = 0 ;
	var hasWainIndex = [] ;
	if(options.length == 1){
 		acquireImage();
 		return ;
	}
/*
	for(var index = 0 , count = options.length ; index < count ; index++  ){
		var item = ($(options.get(index)).html()).trim().toUpperCase();
		if(item.indexOf("TWAIN") != -1){
			hasTwainCount ++ ;
			hasWainIndex.push(index);
 		}
	}
	if(hasTwainCount == 1){
		//可以选中唯一的一个
		var selectedIndex = hasWainIndex[0];
 		$(options.get(selectedIndex)).attr("selected",true);
 		acquireImage();
	}
*/
  }
function source_onchange() {
    var DWObject = gImageCapture.getInstance();
    if (DWObject) {
        var vDWTSource = document.getElementById("source");
        if (vDWTSource) {
            if (vDWTSource)
                DWObject.SelectSourceByIndex(vDWTSource.selectedIndex);
            else
                DWObject.SelectSource();
        }

        DWObject.CloseSource();
    }
}
function acquireImage() {
    var DWObject = gImageCapture.getInstance();
    if (DWObject) {
        if (DWObject.SourceCount > 0) {
        	var vDWTSource = document.getElementById("source");
            if (vDWTSource) {
                if (vDWTSource)
                    DWObject.SelectSourceByIndex(vDWTSource.selectedIndex);
                else
                    DWObject.SelectSource();
            }
        	var vResolution = document.getElementById("Resolution");

            DWObject.Resolution = vResolution.value;
            DWObject.IfShowUI = true ;
            DWObject.CloseSource();
            DWObject.OpenSource();
 			DWObject.SetViewMode(3,3);
			if(DWObject.GetDeviceType() == 7){
				DWObject.IfDisableSourceAfterAcquire = true;
			}else{
				DWObject.IfDisableSourceAfterAcquire = false;
			}
			 DWObject.IfFeederEnabled  = false ;
			 DWObject.IfDuplexEnabled  = true ;
			 DWObject.AcquireImage();
        }
        else
            alert("No webcams or TWAIN compatible drivers detected.");
    }
}

function Dynamsoft_OnTopImageInTheViewChanged(index) {
  var DWObject = gImageCapture.getInstance();
  if (DWObject) {
      DWObject.CurrentImageIndexInBuffer = index;
  }
}
//******************Instructions*******************//
function initInfo() {
    var MessageBody = document.getElementById("divInfo");
    if (MessageBody) {
        var ObjString = "<div>";
        ObjString += "This sample demonstrates how to scan documents using ImageCapture Suite.<br />";
        ObjString += "<br />";
        ObjString += "<b>Steps to try:</b>";
        ObjString += "<br />";
        ObjString += "1. Connect your scanner<br />";
        ObjString += "2. Click the \"Scan\" button";
        ObjString += "<br />";
        ObjString += "<br />";
        ObjString += "Any questions? <a target='blank' href='mailto:support@dynamsoft.com'>Please let us know!</a>";
        ObjString += "<br />";
        ObjString += "</div>";
        MessageBody.innerHTML = ObjString;
    }
}
