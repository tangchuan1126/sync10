﻿var _imageCaptureParam = {
'productKey': 'FE8C85A85CD0D2130CA8A89AD96408B1176B3441C47221263F7909021BE0FBAC10000000',
'containerID': 'dwtcontrolContainer',   //The ID of ImageCapture Suite control div in HTML.This value is required.
    'isTrial': 'false'

    /*
    'isTrial': 'true',  
    isTrial is used to judge whether ImageCapture Suite control is trial or full. This value is optional.
    The default value is 'TRUE', which means the control is a trial one. The value of isTrial is 'TRUE' or 'FALSE'.
    */

    /*
    'version': '9,3',   
    The version of ImageCapture Suite control, which is used to judge the version when downloading CAB.
    This value is optional. The default value is '9,3'.
    */                      
    
    /*
    'resourcesPath': 'Resources',   
    The relative path of MSI, CAB and PKG.
    This value is optional. The default value is 'Resources'.
    */    
   
    /*
    'width': 580,       //The width of ImageCapture Suite control
     This value is optional. The default value is '580'.
     */  
       
    /*
    'height': 600       //The height of  ImageCapture Suite control
    This value is optional. The default value is '600'.
     */  
       
    /*  These are events. The name of 'OnPostAllTransfer' shouldn't be changed, but the name of 'Dynamsoft_OnPostAllTransfers' can be modified. 
        Please pay attention, the name of 'Dynamsoft_OnPostAllTransfers' and 'function Dynamsoft_OnPostAllTransfers()' event must be coincident.
        
        Events are as follows. You can choose one or many according to you needs.
        Once an event is added, you must make sure the corresponding function is defined in your code.
        
        'onPostTransfer':Dynamsoft_OnPostTransfer,
        'onPostAllTransfers':Dynamsoft_OnPostAllTransfers,  
        'onMouseClick':Dynamsoft_OnMouseClick,   
        'onPostLoad':Dynamsoft_OnPostLoadfunction,    
        'onImageAreaSelected':Dynamsoft_OnImageAreaSelected,   
        'onMouseDoubleClick':Dynamsoft_OnMouseDoubleClick,   
        'onMouseRightClick':Dynamsoft_OnMouseRightClick,   
        'onTopImageInTheViewChanged':Dynamsoft_OnTopImageInTheViewChanged,   
        'onImageAreaDeSelected':Dynamsoft_OnImageAreaDeselected,    
        'onGetFilePath':Dynamsoft_OnGetFilePath  
    */  
 
};