/**
 * 
 */


(function($) {
    //扩展这个方法到jquery
    $.fn.extend({
        //插件名字
    	
        showPicture: function() {
        	var data = arguments[0];
        	var arg1 = arguments[1];
        	var _element = this;
        	
        	var fn = {
        		'appendPicture' : function(){	//////*********方法回调 添加图片
        			
        			//**************
        			return _element.each(function() {
                		var pictureUrl = "_fileserv/file";
               		 	var element = this;
        	        	var html = '';
        	        	var base_path = $(element).find("input[name='base_path']").val();
        	        	data = arg1;
        				if(data.length>0){
        					//***过滤相同的Id的图片 相同则不添加
        					var fileIds = $(element).find("input[name='file_names']").val().split(",");
        					
        					var _data = [];
        					for(var i=0;i<data.length;i++){
        						_data[i] = data[i].file_id;
        					}
        					if($.inArray(fileIds[0],_data)!=-1){
        						return;
        					}
        					
        					//************* 过滤  end
        					var count = $(element).find("#pictureWidth ul li").length;
        					$(element).find("#pictureWidth").css("width",(count+data.legnth)*67);
        					var fileId = "";
        					for(var i=0; i<data.length; i++){
        						fileId += data[i].file_id+","
        						html += '<li value="'+data[i].original_file_name+'" fileWithId="'+data[i].file_with_id+'" fileid="'+data[i].file_id+'">';
        						var imgType = data[i].original_file_name;
            					imgType = imgType.substring(imgType.lastIndexOf(".")+1);
            					if(imgType == "jpg" || imgType == "png" || imgType == "bmp" || imgType == "gif"){
            						html += ' <img value="'+data[i].file_id+'" src="'+base_path+pictureUrl+'/'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
            					}else if(imgType == "xls"){
            						html += ' <img value="'+data[i].file_id+'" src="../file/images/xls.png" title="'+data[i].original_file_name+'">';
            					}else if(imgType == "pdf"){
            						html += ' <img value="'+data[i].file_id+'" src="../file/images/pdf.png" title="'+data[i].original_file_name+'">';
            					}else{
            						html += ' <img value="'+data[i].file_id+'" src="../file/images/none.png" title="'+data[i].original_file_name+'">';
            					} 
        						html += '<span></span>';
        						html += '<div >local</div> </li>';
        					}
        					fileId = $(element).find("input[name='file_names']").val() + fileId;
        					$(element).find("input[name='file_names']").val(fileId);
        				}
        				$(element).find("div.showPicture_overflow ul").prepend(html);
        				$(element).find("li").unbind();
        				$(element).find("li a").unbind();
        				$(element).find("li").on("click",function(){	//绑定查看事件
        					var currentName = $(this).val();
        					var file_with_id = $(this).attr("fileWithId");
        					
        					 var obj = {
        						  		file_with_type:'',
        						  		file_with_id : file_with_id,
        						  		current_name : currentName ,
        						  		cmd:"multiFile",
        						  		table:'file',
        						  		base_path:base_path + "upload/check_in"
        							}
        						    if(window.top && window.top.openPictureOnlineShow){
        								window.top.openPictureOnlineShow(obj);
        							}else{
        							    openArtPictureOnlineShow(obj,base_path);
        							}
        				});
        				$(element).find("li span").on("click",function(){	//绑定删除事件
        					 var id = $(this).parent().attr("fileid");
                          	 var array = $(element).find("input[name='file_names']").val().split(",");
	                       	 var lastFile = "";
	                       	 for(var index = 0 ; index < array.length ; index++ ){
	                       			if(id==array[index]){
	                       				array[index] = "";
	                       				continue;
	                       			}
	                       		
	                       			lastFile += array[index];
	                       			if(index!=array.length-1 && array[index]!=""){
	                       				lastFile+=",";
	                       			}
	                       	 }
	                       	 console.log(lastFile);
	                       	 $(element).find("input[name='file_names']").val(lastFile);
	                       	 $(this).parent().remove();
        					
        				});	//删除 end
               	 });
               },
               'getUploadPicture':function(){
            	//   console.log(_element[0]);
            	   var value = "";
            	    _element.each(function() {
            	    	
            		  value =  $(this).find("input[name='file_names']").val().substring(0,$(this).find("input[name='file_names']").val().length-1);
            	   });
            	    return value;
               }
               
        	};
        	if(typeof data == 'string'){
        		//fn
        		
        		return fn[data](arg1);
        	}
        	
        	if(typeof data !== 'object'){
        		//
        		alert("arguments is error !!");
        		return;
        	}
        	return this.each(function() {
        		var element = this;
        		var _width = $(element).width();
        		
        		//*********查询条件
            	var file_with_id = data.file_with_id;
            	var file_with_class = data.file_with_class;
            	var file_with_type = data.file_with_type;
            	
            	//*********一些配置参数
            	var base_path = data.base_path;
            	var target = this.id;
            	var limitSize = data.limitSize;
            	var multipleBtn = data.multiple_button==null?true:data.multiple_button;
            	var singleBtn = data.single_button==null?true:data.single_button;
            	var uploadBtn = data.upload_button==null?true:data.upload_button;
            	var width = data.width&&data.width!=null?data.width:_width;
            	
            	
            	//**************根据配置参数生成页面
            	var div = '<div class="btn"></div>';
            	$(element).append(div);
            	if(multipleBtn){
            		var btn = '<span><input type="button" id="mutiple" class="multiple-button" /></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #mutiple").on("click",function(){
            			
	            		//'onlineScanner');
	            		var uri = base_path + "administrator/file/picture_online_scanner.html?target="+target; 
	            		$.artDialog.open(uri , {title: 'Online Mutiple',width:'950px',height:'530px', lock: true,opacity: 0.3,fixed: true});
            		});
            		
            	}
            	if(singleBtn){
            		var btn = '<span><input type="button" id="single" class="single-button" /></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #single").on("click",function(){
	            		//	onlineSingleScanner('onlineScanner');
            		    var uri = base_path + "administrator/file/picture_online_single_scanner.html?target="+target; 
            			$.artDialog.open(uri , {title: 'Online Single',width:'300px',height:'170px', lock: true,opacity: 0.3,fixed: true});
            		});
            		
            	}
            	if(uploadBtn){
            		var btn = '<span><input type="button" id="upload" class="upload-button" /></span>';
            		$(element).find(".btn").append(btn);
            		$(element).find(".btn #upload").on("click",function(){
            		//	uploadFile('jquery_file_up');
            			
        			    var fileNames = $("input[name='file_names']").val();
        			    var obj  = {
        				     reg:"picture_office",
        				     'limitSize':limitSize,
        				     'target':target
        				 }
        			    var uri = base_path + "administrator/file/jquery_file_up.html?"; 
        				uri += jQuery.param(obj);
        				 if(fileNames && fileNames.length > 0 ){
        					uri += "&file_names=" + fileNames;
        				}
        			//	 $.artDialog.data("files",fileMap.get(_target));
        				$.artDialog.open(uri , {id:'file_up',title: 'Upload Photo',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
        			    	 close:function(){
        						//调用弹出页面的方法,弹出页面的方法是执行父页面的方法
        						this.iframe.contentWindow.showFiles && this.iframe.contentWindow.showFiles();
        			   	}});
            			
            		});
            	}
            	$(element).append('<input type="hidden" name="file_names" id="file_names"/>');	//添加数据临时存放隐藏域
            	$(element).append('<input type="hidden" name="base_path" id="base_path"/>');	//添加数据临时存放隐藏域
            	$(element).find("input[name='base_path']").val(base_path);
            	//**************查询 将图片展示在页面
            	var _html = '<div class="showPicture_overflow" ><div id="pictureWidth" style=""> <ul ></ul></div></div>';
            	$(element).append(_html);
            	
            	width -= $(element).find(".showPicture_overflow").position().left+50;
            	$(element).find(".showPicture_overflow").css("width",width+'px');
            	if(file_with_id != null && file_with_id != undefind){
                	
                	var uri = "";
                	var pictureUrl = "_fileserv/file";
                	$.ajax({
                		url:uri,
                		data:{'file_with_class':file_with_class,'file_with_type':file_with_type,'file_with_id':file_with_id},
                		dataType:'json',
                		type:'post',
                		success:function(data){
                			var html = '';
                			var fileId = "";
                			if(data.length>0){
                				
                				for(var i=0; i<data.length; i++){
                					fileId += data[i].file_id+",";
                					html += ' <li value="'+data[i].original_file_name+'" fileWithId="'+data[i].file_with_id+'" >';
                					var imgType = data[i].file_path;
                					imgType = imgType.substring(imgType.lastIndexOf("."));
                					if(imgType == "jpg" || imgType == "png" || imgType == "bmp" || imgType == "gif"){ 
                						html += ' <img value="'+data[i].file_id+'" src="'+base_path+pictureUrl+'/'+data[i].file_id+'" title="'+data[i].original_file_name+'">';
                					}else if(imgType == "xls"){
                						html += ' <img value="'+data[i].file_id+'" src="../file/images/xls.png" title="'+data[i].original_file_name+'">';
                					}else if(imgType == "pdf"){
                						html += ' <img value="'+data[i].file_id+'" src="../file/images/pdf.png" title="'+data[i].original_file_name+'">';
                					}else{
                						html += ' <img value="'+data[i].file_id+'" src="../file/images/none.png" title="'+data[i].original_file_name+'">';
                					} 
                					if(data[i].expire_in_secs>0){
                						htlm += '<span></span>';
                					}
                					html += '<div >'+data[i].expire_in_secs>0?'local':'server'+' </div> </li>';
                				}
                				fileId.substring(0,fileId.length-1);
                				$(element).find("input[name='file_names']").val(fileId);
                			}
                			$(element).append(html);
                			$(element).find("li").on("click",function(){	//绑定查看事件
                				var currentName = $(this).val();
            					var file_with_id = $(this).attr("fileWithId");
            					 var obj = {
            						  		file_with_type:file_with_type,
            						  		file_with_id : file_with_id,
            						  		current_name : currentName ,
            						  		cmd:"multiFile",
            						  		table:'file',
            						  		base_path:base_path + "upload/check_in"
            							}
            						    if(window.top && window.top.openPictureOnlineShow){
            								window.top.openPictureOnlineShow(obj);
            							}else{
            							    openArtPictureOnlineShow(obj,base_path);
            							}
                			});
                			$(element).find("li a").on("click",function(){	//绑定删除事件
                				 var id = $(this).parent().attr("fileid");
                              	 var array = $(element).find("input[name='file_names']").val().split(",");
    	                       	 var lastFile = "";
    	                       	 for(var index = 0 ; index < array.length ; index++ ){
    	                       			if(id==array[index]){
    	                       				array[index] = "";
    	                       				continue;
    	                       			}
    	                       		
    	                       			lastFile += array[index];
    	                       			if(index!=array.length-1 && array[index]!=""){
    	                       				lastFile+=",";
    	                       			}
    	                       	 }
    	                       	 console.log(lastFile);
    	                       	 $(element).find("input[name='file_names']").val(lastFile);
    	                       	 $(this).parent().remove();
                            		
                			});	//删除 end
                		},
                		error:function(){
                			alert('System error');
                		}
                	});
            	}
            });
        }
    });
 //传递jQuery到方法中，这样我们可以使用任何javascript中的变量来代替"$"      
})(jQuery); 
