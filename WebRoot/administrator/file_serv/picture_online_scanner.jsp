<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="com.cwc.json.JsonObject"%>
<%@ include file="../../include.jsp"%> 
<html>
<head>
    <title>OnLine Scan</title>
    
     <script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.min.js"></script>
    <script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
	<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
 	 <link rel="stylesheet" href="Styles/simple_scan.css">
    <%
    	String target = StringUtil.getString(request,"target");
		String sessionId =  request.getSession().getId();
		
    %>
</head>
<body onload="onPageLoad();">
 		<div style="margin-bottom:4px;padding-left:5px;border:1px solid red;height:40px;line-height:40px;background-color: #F2DEDE;border-color: #EED3D7;border-radius:4px;">
			<input class="button" type="button" value="Remove Selected" onclick="removeSelecte();"/>
			<input class="button" type="button" value="RemoveAll" onclick="removeAll();">
		</div>
 			<div style="border:0px solid silver;width:580px;height:600px;float:left;">
 				   <div id="dwtcontrolContainer" class="DWTContainer"></div>
 			</div>
			<div style="border:0px solid green;height:600px;float:left;width:300px;margin-left:10px;" class="ScanWrapper">
			           <div class="divTableStyle" >
			           <label for="source" style="font-size:12px;font-family:arial,helvetica,sans-serif;line-height:20px;">
							<b>Select Source:</b>
							<select id="source"  onchange="source_onchange()" style="position:relative;width: 217px;margin-left:29px;" size="1"></select>
						</label>
						<div style="margin-top:5px;">
				  		 	<label for="Resolution" style="font-size:12px;font-family:arial,helvetica,sans-serif;line-height:20px;">
								<b>Resolution:</b>
								<select id="Resolution" size="1">
									 
								</select>
								DPI
							</label>
						</div>
				  		  <input class="DWTScanButton btn" type="button" value="Scan" onclick="acquireImage();" />
						  <!-- 
				  		  <input class="DWTScanButton btn" type="button" value="Single Scan " onclick="initSingleControl();" />
				  		  -->
						  <input class="DWTScanButton btn" type="button" value="Submit" onclick="uploadImages();" />
				    	</div>
				    	<div id="notify">
							<div>
								This sample demonstrates how to scan documents using Dynamic Web TWAIN.
								<br>
								<br>
								<b>Steps to try:</b>
								<br>
								1. Connect your scanner
									<br>
								2. Click the "Scan" button
								<br>
								3. Before close please click "Submit" button 
								<br >
 							</div>
						</div>
			</div>
	 <div style="clear:both;"></div>
     <script src="Scripts/dynamsoft.imagecapturesuite.initiate.js"></script>
	  <script src="Scripts/product_info.js"></script>

	 
     <script src="Scripts/ICSSample_BasicScan.js"></script>
      <script type="text/javascript">
      var hasImageSeed ;
      function hasImage(){
    	    var DWObject = gImageCapture.getInstance();
    		if(DWObject){
    			if(DWObject.HowManyImagesInBuffer > 0 ){
    				clearInterval(hasImageSeed);
    				uploadImages();
    			}
    		}
    	}
    	function initSingleControl() {
    	     var DWObject = gImageCapture.getInstance();
    	     if (DWObject) {
    	         if (DWObject.ErrorCode == 0) {
    	             clearInterval(seed);
    	             flag = true ;
    	             DWObject.BrokerProcessType = 1;
    	             DWObject.SetViewMode(3,3);
    	 			 DWObject.MaxImagesInBuffer = 1;
    	 			 
    	 			 var vDWTSource = document.getElementById("source");
    	             if (vDWTSource) {
    	                if (vDWTSource)
    	                    DWObject.SelectSourceByIndex(vDWTSource.selectedIndex);
    	                else
    	                    DWObject.SelectSource();
    	             }
    	             DWObject.IfShowUI = true ;
     	        	 var vResolution = document.getElementById("Resolution");
     	             DWObject.Resolution = vResolution.value;
    	             DWObject.AcquireImage();
    	             hasImageSeed = setInterval(hasImage, 500);
    	         }
    	     }
    	 }
       </script>
     <script type="text/javascript">
       function checkIfImagesInBuffer(DWObject) {
    	    if (DWObject.HowManyImagesInBuffer == 0) {
     	        return false;
    	    }
    	    else
    	        return true;
    	}
 	
     	function removeSelecte(){
     	    var DWObject = gImageCapture.getInstance();
			if(DWObject.HowManyImagesInBuffer != 0){
			    DWObject.RemoveAllSelectedImages();
			}
			 
        }
    	function removeAll(){
     	    var DWObject = gImageCapture.getInstance();
			if(DWObject.HowManyImagesInBuffer != 0){
			    DWObject.RemoveAllImages();
			}
        }
     	 function uploadImages(){
      	        var DWObject = gImageCapture.getInstance();
    	    	var ischeck = checkIfImagesInBuffer(DWObject);
    			if(ischeck){
    				//这里执行上传的文�?
    				//和jquery file up 一样都是走同样接口。和回调方法
    				//这里的上传是先上传到temp那个文件夹下。然后后面的操作执行回调函数�?需要返回file_names ,target)
    				 
    				var CurrentPathName = unescape(location.pathname);	// get current PathName in plain ASCII	
    				var CurrentPath = CurrentPathName.substring(0, CurrentPathName.lastIndexOf("/") + 1);
    				var  strActionPage = "/Sync10/_fileserv/upload";
    				var count =  DWObject.HowManyImagesInBuffer;
    				var successUploadCount = 0 ;
    				var filenames = [];
    				for(var index = 0 ; index < count ; index++ ){
    				    var Digital = new Date();
    				     
    						var CurrentTime = Digital.getTime();
    						strFileName = CurrentTime+".jpg";
    						var strHostIP = location.hostname;	
    						DWObject.HTTPPort = $.trim(location.port==""?80:location.port);	
    						
    						DWObject.SetCookie("JSESSIONID=<%= sessionId%>"+";"); //添加认证的
       					 
    				 		var uploadFlag = DWObject.HTTPUploadThroughPost($.trim(strHostIP),index,strActionPage,$.trim(strFileName));
    				 		//张睿2014-10-24。修改 不通过ErrorCode  来判断了，通过向东返回的json字符串来判断
    				 		
    				 		var upfileJson = (handUpFileJson(DWObject.HTTPPostResponseString));
    			 			if(upfileJson && upfileJson.flag * 1 > 0){
    			 				  successUploadCount++;
      						      filenames.push(upfileJson.file);
    			 			}else{
    			 				alert("UpLoad Failed.");
    			 			}
    			 			
    				 		/* DWObject.HTTPUploadThroughPost($.trim(strHostIP),index,strActionPage,$.trim(strFileName));
    				 		if (DWObject.ErrorCode != 0) {
    							alert(DWObject.ErrorString);
    							if (DWObject.ErrorString == "HTTP process error."){
     							}
    						}
    						else{
    						    successUploadCount++;
    						    filenames.push(strFileName);
    						}	   */
    				}
     				if(successUploadCount * 1 > 0){
    					//调用回调函数
    					<%-- var file_names = filenames.join(",");
    				    $.artDialog.opener.uploadFileCallBack  && $.artDialog.opener.uploadFileCallBack(file_names,'<%= target%>');
    				    $.artDialog && $.artDialog.close(); --%>
    				    $.artDialog.opener.uploadFileCallBack  && $.artDialog.opener.uploadFileCallBack(filenames,'<%= target%>');
    				    $.artDialog && $.artDialog.close();
    				}
   			    }else{
     			}
    		}
     	 //2014-10-27 添加张睿
    	 function handUpFileJson(strjson){
     		 
     		 var returnObj = {} ;
      		try{
     		 var jsonValue = eval("("+strjson+")");
     		 if(jsonValue && jsonValue.length == 1){
     			returnObj = { 
     				     flag:jsonValue[0].file_id * 1 ,
     				 	 file:jsonValue[0] 
     			};
     		 }
      		}catch (e) {
 			}finally{
 				return returnObj;
 			}
     
     	 }
      </script>
</body>
</html>
