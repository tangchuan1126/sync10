<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
    DBRow[] storageCatalogs = catalogMgr.getProductStorageCatalogTree();
	String AddUnloadLocationAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/storage_location/AddUnloadLocationAction.action";
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加装卸位置</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script>

function addLoadUnloadLocation()
{
		  
	var f = document.addLocationForm;

	if(f.location_name.value == ""){
		showMessage("请填写位置名称","alert");
		return false;
	}
	else if (f.psc_id.value == 0){
	 	showMessage("请选择仓库","alert");
	 	return false;
	 }
	else{
		ajaxAddUnloadLocation();
	}
}
function ajaxAddUnloadLocation(){

	$.ajax({
		url:'<%=AddUnloadLocationAction%>',
		data:$("#addLocationForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
			    setTimeout("windowClose()", 1000);
			}
		},
		error:function(){
			showMessage("系统错误","error");
			$.unblockUI();
		}
	});
}

function windowClose(){
	$.artDialog && $.artDialog.close();
	$.artDialog.opener.refreshWindow  && $.artDialog.opener.refreshWindow();
};
function closeWin()
{
	$.artDialog.close();
}

</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addLocationForm" id="addLocationForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:90%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">增加装卸位置</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
        <tr>
			  	 <td align="right" valign="middle" class="STYLE2" >位置名称:</td>
			    <td align="left" valign="middle" > 
			    	<input name="location_name" id="location_name" type="text" value=""  size="45px"/>
			    </td>
			</tr>
			  <tr>
			  	<td align="right" valign="middle" class="STYLE2" colspan="1">所属仓库:</td>
			    <td align="left" valign="middle" colspan="3">
			    <select id="psc_id" name="psc_id">
						<option value="0">请选择仓库...</option>
						<%
							for(int i =0 ;i<storageCatalogs.length;i++)
							{
						%>
							<option value="<%=storageCatalogs[i].get("id",0l) %>"><%=storageCatalogs[i].getString("title") %></option>
						<%
							}
						%>
					</select>
			    </td>
			  </tr>
    </table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addLoadUnloadLocation();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>	
</body>
</html>
