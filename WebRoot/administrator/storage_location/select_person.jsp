<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
String psId = StringUtil.getString(request,"psId");
String objId = StringUtil.getString(request,"el_id");
String title = StringUtil.getString(request,"title");
DBRow[] personsList = googleMapsMgrCc.getPersonByPsId(Long.parseLong(psId));
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
</style>
  </head>
  <body>
  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="width: 100%;">
	    	<tr style=" font-weight:bold; font-size:16px; color:#00F;">
	    		<td style="width: 49%;">&nbsp;Existence title</td>
	    		<td style="width: 2%;"></td>
	    		<td style="width: 49%;">&nbsp;Free title</td>
	    	</tr>
	    	<tr>
	    		<td>
					<div id="exitTitle" style="background-color:#FFF;width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
						
					</div>
				</td>
				<td></td>
	    		<td>
					<div id="notExitTitle" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
					<%
						if(personsList != null && personsList.length>0){
							for(int i=0; i<personsList.length; i++){
								DBRow t = (DBRow)personsList[i];
								%>
								<div title="<%=t.getString("employe_name") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addTitle(this.parentNode)">
										<%=t.getString("employe_name") %>
										<input type="hidden" id="title_id" value="<%=t.getString("adid") %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="addTitle(this.parentNode)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
					%>
					</div>
				</td>
	    	</tr>
	    	<tr>
				<td colspan="3" align="right" class="win-bottom-line">
					<input type="button" name="Submit1" class="short-short-button" value="Confirm" onClick="confirm();"> 
					<input type="button" name="Submit2" class="short-short-button" value="Cancel" onClick="closeWindow();">
				</td>
			</tr>
	    </table>
	</div>
	<!-- model -->
	<div id="title_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow"  style="width: 85%; float: left; " ondblclick="removeTitle(this.parentNode)">
			<input type="hidden" id="title_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/delete_grey.png'); margin: 1px; cursor: pointer;" 
				onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/delete_blue.png&quot;)'" 
				onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/delete_grey.png&quot;)'"
				onclick="removeTitle(this.parentNode)">
		</div>
	</div>
	<div id="title_free_model" style="display:none; width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
		<div class="text-overflow" style="width: 85%; float: left; " ondblclick="addTitle(this.parentNode)">
			<input type="hidden" id="title_id">
		</div>
		<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
					onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
					onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
					onclick="addTitle(this.parentNode)">
		</div>
	</div>
	<!-- model end-->
  </body>
<script type="text/javascript">
//删除员工
function removeTitle(ele){
	if($("#notExitTitle").find("div").length == 0){
		$("#notExitTitle").html("");
	}
	var je = $(ele);
	var id = je.find("#title_id").val();
	var title = je.attr("title");
	var node = creatTitleNode(title,id,true);
	$("#notExitTitle").append(node);
	je.remove();
}
//添加员工
function addTitle(ele){
	if($("#exitTitle").find("div").length == 0){
		$("#exitTitle").html("");
	}
	var je = $(ele);
	var id = je.find("#title_id").val();
	var title = je.attr("title");
	var node = creatTitleNode(title,id,false);
	$("#exitTitle").append(node);
	je.remove();
}
//创建员工标签
function creatTitleNode(title,id,isFree){
	var node = null;
	if(isFree){
		node = $("#title_free_model").clone();
	}else{
		node = $("#title_model").clone();
	}
	node.removeAttr("id");
	node.attr("title",title);
	node.find("#title_id").val(id);
	node.find("div").first().append(title);
	node.css("display","");
	return node;
}
function confirm(){
	var ids = "";
	var names = "";
	var ts = $("#exitTitle #title_id");
	for(var i=0; i<ts.length; i++){
		ids += $(ts[i]).val()+",";
		names +=  $(ts[i].parentNode.parentNode).attr("title")+",";
	}
	if(ids!=""){
		ids = ids.substr(0,ids.length-1);
		names = names.substr(0,names.length-1);
	}
	$.artDialog.opener.backFillPerson(names,ids);
	$.artDialog.close();
}
function closeWindow() {
	$.artDialog.close();
}
</script>
</html>
