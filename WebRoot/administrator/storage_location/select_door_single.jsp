<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="java.text.*" %>
<%@page import="com.cwc.util.*,com.cwc.app.util.*" %>
<%@ taglib uri="/turboshop-tag" prefix="tst"%>
<%@ include file="../../include.jsp"%>
<%
String objName = StringUtil.getString(request,"area_name");
String objId = StringUtil.getString(request,"el_id");
String dock = StringUtil.getString(request,"dock");
String psId = StringUtil.getString(request,"ps_id");
String[] docks = dock.split(",");
DBRow[] dockList = locationAreaXmlImportMgrCc.getDoorByPsId(psId);
%>
<html>
  <head>
<!--  基本样式和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />
<script type="text/javascript" src="../../common.js"></script>
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- table 斑马线 -->
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<!-- jquery UI 加上 autocomplete -->
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ui.autocomplete.html.js'></script>
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.autocomplete.css" />
<style type="text/css">
.text-overflow {
	display:block;
	word-break:keep-all;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
}
</style>
  </head>
  <body>
  <!-- 
  	<div align="left" style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="font-weight:bold; font-size:16px; color:#00F">
	    	<tr>
	    		<td style="text-align: right">ZONE NAME：</td>
	    		<td><%=objName %></td>
	    	</tr>
	    </table>
	</div>
	<br/>
	 -->
  	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;">
	    <table style="width: 100%;">
	    	<tr>
	    		<td>
					<div id="notExitDoor" style="background-color:#FFF; width: 100%; border:2px #dddddd solid; height:324px; overflow:auto" >
					<%
						if(dockList != null && dockList.length>0){
							for(int i=0; i<dockList.length; i++){
								DBRow d = (DBRow)dockList[i];
								%>
								<div title="<%=d.getString("doorid") %>" style="width: 110px; height: 15px; border: solid; border-color: #CCCCCC; border-width: 1px; margin: 5px; float: left; cursor: default;">
									<div class="text-overflow" style="width: 85%; float: left; " ondblclick="confirm(this.parentNode)">
										<%=d.getString("doorid") %>
										<input type="hidden" id="sd_id" value="<%=d.getString("sd_id") %>">
									</div>
									<div style="width: 12px; height: 12px; float: right; background-image: url('../imgs/maps/add_grey.png'); margin: 1px; cursor: pointer;" 
												onmousemove="this.style.backgroundImage='url(&quot;../imgs/maps/add_blue.png&quot;)'" 
												onmouseout="this.style.backgroundImage='url(&quot;../imgs/maps/add_grey.png&quot;)'"
												onclick="confirm(this.parentNode)">
									</div>
								</div>
								<%
							}
						}else{
							%>
							<center>
								no data
							</center>
							<%
						}
					%>
					</div>
				</td>
	    	</tr>
	    </table>
	</div>
  </body>
<script type="text/javascript">
function confirm(el){
	var name = $(el).attr("title");
	var id = $(el).find("#sd_id").val();
	$.artDialog.opener.backFillDoor(name,id,'<%=objId %>');
	$.artDialog.close();
}
function closeWindow() {
	$.artDialog.close();
}
</script>
</html>