<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	DBRow[] catalogs = catalogMgr.getProductStorageCatalogTree();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>仓库区域</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />		
		<script type="text/javascript" src="../js/select.js"></script>


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>
<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

	
	
		<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
	function addArea()
	{
		//tb_show('新增区域','add_location_area.html?TB_iframe=true&height=500&width=800',false);
		var uri ='<%=ConfigBean.getStringValue("systenFolder")%>'+ "administrator/storage_location/add_location_area.html";
		$.artDialog.open(uri,{title: "新增区域",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function modArea(area_id)
	{
		//tb_show('调整区域','mod_location_area.html?area_id='+area_id+'&TB_iframe=true&height=500&width=800',false);
		var uri='<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/storage_location/mod_location_area.html?area_id="+area_id;
		$.artDialog.open(uri,{title:"调整区域",width:'800px',height:'500px', lock: true,opacity: 0.3,fixed: true});
	}
	
	function closeWin()
	{
		tb_remove();
		window.location.reload();
	}
	
	function show(tdps_id)
	{
		$("div").html("");//清除展示div内容
		$(".img").attr("src","imgs/close.gif");//展开图片统一换成不展开的
		
		if($("#"+tdps_id).attr("class") == "opentitle")
		{
			$(".opentitle").attr("class","closetitle");//展开的仓库标题换成不展开样式
		}
		else
		{
			$(".opentitle").attr("class","closetitle");//展开的仓库标题换成不展开样式
			
			var inner = ""
			var ps_id = tdps_id.split("_");
			var para = "ps_id="+ps_id[ps_id.length-1];
			
			$.ajax({
				url: 'storage_location_area_list.html',
				type: 'post',
				dataType: 'html',
				timeout: 60000,
				cache:false,
				data:para,
				beforeSend:function(request){

				},
				
				error: function(e){
					alert(e);
					alert("请求失败")
				},
				
				success: function(html){
					$("#ps"+ps_id[ps_id.length-1]).html(html);
					$("#oc"+ps_id[ps_id.length-1]).attr("src","imgs/open.gif");
					$("#"+tdps_id).attr("class","opentitle");
				}
			});
		}
	}
	
	function seelocationStorage(area_id)
	{
		document.see_location_catalog.area_id.value = area_id;
		document.see_location_catalog.submit();
	}
</script>

<style type="text/css">
<!--
.line-black-1234 {
	border: 1px solid #000000;
}

.img{
	border: 0px;
}

.closetitle{
	font-weight:lighter; 
	font-size:larger;
}
.opentitle{
	font-weight: bold; 
	font-size:large;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   区域管理</td>
  </tr>
</table>
<br>
<form method="post" name="del_form">
<input type="hidden" name="id" >

</form>

<form method="post" name="mod_form">
<input type="hidden" name="id" >
<input type="hidden" name="title" >
</form>

<form method="post" name="add_form">
<input type="hidden" name="parentid">
<input type="hidden" name="title">
</form>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td align="left"> <!--  <input type="button" class="long-button-add" onClick="addArea()" value="增加区域"/> --></td>
</tr>
</table>
<br/>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0"   class="zebraTable">
      	<tr>
        	<th width="58%" class="left-title"  style="vertical-align: center;text-align: center;">仓库名称</th>
     	</tr>
     	<%
     		for(int i = 0;i<catalogs.length;i++)
     		{
     	%>
     		<tr > 
      			<td align="left" valign="middle" height="60">
      				<img id="oc<%=catalogs[i].getString("id")%>" class="img" src="imgs/close.gif">
      				<span class="closetitle" id="<%=catalogs[i].getString("title")+"_"+catalogs[i].getString("id")%>" onClick="show(this.id)"><a href="javascript:void(0)"><%=catalogs[i].getString("title")%></a></span>
      				<div id="ps<%=catalogs[i].getString("id")%>"></div>
      			</td>
    		</tr>
     	<%
     		}
     	%>
</table>
<form action="location_catalog_list.html" method="post" name="see_location_catalog">
	<input type="hidden" name="area_id"/>
</form>
</body>
</html>
