<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String backurl = StringUtil.getString(request,"backurl");

long psid = StringUtil.getLong(request,"psid");
DBRow storageCatalog = null ;
if(psid != 0l){
	storageCatalog = catalogMgr.getDetailProductStorageCatalogById(psid);
}else{
 storageCatalog = new DBRow();
}

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加区域</title>
<script src="<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp"></script>
<link href="../comm.css?v=1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>

<script type="text/javascript" src="../js/imgPreview/CJL.0.1.min.js"></script>
<script type="text/javascript" src="../js/imgPreview/QuickUpload.js"></script>
<script type="text/javascript" src="../js/imgPreview/ImagePreviewd.js"></script>
<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<script>

</script>
<style type="text/css">
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->

.perview {width:100%; height:98%;background:#fff;font-size:12px; border-collapse:collapse;}
.perview td, .perview th {padding:5px;border:1px solid #ccc;}
.perview th {background-color:#f0f0f0; height:20px;}
.perview a:link, .perview a:visited, .perview a:hover, .perview a:active {color:#00F;}
.perview table{ width:100%;border-collapse:collapse;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form method="post" enctype="multipart/form-data" name="add_location_area_form">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="2" align="center" valign="top">
    	<table width="95%" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="60" align="center" style="font-size:14px; font-weight:bolder">仓库：
				<input type="hidden" name="area_psid" id="area_psid" value="<%=storageCatalog.get("id",0l) %>">
					<select  disabled="disabled">
						<option value="<%=storageCatalog.get("id",0l) %>"><%=storageCatalog.getString("title") %></option>
					</select>
                                       区域名称:<input type="text" name="area_name" id="area_name"/>&nbsp;&nbsp;例：101（只可是三位数字）&nbsp;&nbsp;
                </td>
                <td>                       
                  <input type="button" class="short-button" name="storage_door" value=" 卸货门" onClick="addStorageDoor(<%=storageCatalog.get("id",0l) %>)">
                  <input type="button" class="short-button" name="unload_location" value=" 装卸位置" onClick="addUnloadLocation(<%=storageCatalog.get("id",0l) %>)">
                </td>
			</tr>
			<tr height="8%" id="showDoorOrLocation" style="display:none;">
			<td colspan="2">
				<table border="0" width="100%">
				<input type="hidden" name="attrStoragedoor" id="attrStoragedoor" />
				<input type="hidden" name="attrUnloadLocation" id="attrUnloadLocation" />
					<tr>
					<td width="12%" style="font-size:12px; font-weight:bolder;padding-left:24px;">卸货门：</td>
					<td width="36%" id="storageDoor" name="storageDoor"></td>
					 <td style="font-size:12px; font-weight:bolder;">装卸位置：</td>
				        <td id="unloadLocation" name="unloadLocation"></td>
					</tr>
				</table>
			</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<table border="0" class="perview" width="100%">
							<tr>
								<td align="center" height="5%" style="font-size:18px; font-weight:bolder">区域图片选择&nbsp;&nbsp;&nbsp;&nbsp;<input id="idFile" name="pic" type="file" /></td>
								
							</tr>
							<tr><th height="5%">预览图</th></tr>
							<tr>
								<td align="center"><img id="idImg" /></td>
							</tr>
					</table>
					<script>
					var ip = new ImagePreview( $$("idFile"), $$("idImg"), {
						maxWidth: 415, maxHeight: 311, action: "viewImg.jsp"
					});
					ip.img.src = ImagePreview.TRANSPARENT;
					ip.file.onchange = function(){ ip.preview(); };
					
					</script>
					<br/>
					<style>
					/*file样式*/
					#idPicFile {
						width:80px;height:20px;overflow:hidden;position:relative;
						background:url(http://www.cnblogs.com/images/cnblogs_com/cloudgamer/169629/o_addfile.jpg) center no-repeat;
					}
					#idPicFile input {
						font-size:20px;cursor:pointer;
						position:absolute;right:0;bottom:0;
						filter:alpha(opacity=0);opacity:0;
						outline:none;hide-focus:expression(this.hideFocus=true);
					}
					</style>
			  </td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">&nbsp;
    </td>
    <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
	 <input type="button" name="Submit2" value="保存" class="normal-green" onClick="addArea()" id="saveButton">
	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWindow();">
	</td>
  </tr>
</table>
</form>
<script type="text/javascript">
//添加卸货门
function addStorageDoor(psid){
	
	var uri ='<%=ConfigBean.getStringValue("systenFolder")%>'+ "administrator/storage_location/add_area_unload_door.html?psid="+psid;
	$.artDialog.open(uri,{title: "添加卸货门",width:'420px',height:'260px', lock: true,opacity: 0.3,fixed: true});
}
//显示添加的卸货门
function showDoorValue(doorId,sdId){
	$("#showDoorOrLocation").show();
	var divD = '<div style="float:left;" id="divDoor'+sdId+'" name="divDoor"><input type="hidden" id="door" name="door" value="'+sdId+'"/>[ '+doorId+' ]<img alt="删除" style="cursor:pointer;" src="../imgs/del.gif" onclick="deleteDoor('+sdId+')"/>&nbsp;&nbsp;</div>'; 
	$(divD).appendTo($("#storageDoor"));
}
//移除卸货门
function deleteDoor(sdId){
	$("#divDoor"+sdId).remove();
}
//添加装卸位置
function addUnloadLocation(psid){
	var uri ='<%=ConfigBean.getStringValue("systenFolder")%>'+ "administrator/storage_location/add_area_unload_location.html?psid="+psid;
	$.artDialog.open(uri,{title: "添加装卸位置",width:'420px',height:'260px', lock: true,opacity: 0.3,fixed: true});
}

//显示添加的装卸位置
function showUnloadLocation(locationName,id){
	$("#showDoorOrLocation").show();
	var divL = '<div style="float:left;" id="divLocation'+id+'" name="divLocation"><input type="hidden" id="locationName" name="locationName" value="'+id+'"/>[ '+locationName+' ]<img alt="删除" style="cursor:pointer;" src="../imgs/del.gif" onclick="deleteUnloadLocation('+id+')"/>&nbsp;&nbsp;</div>'; 
	$(divL).appendTo($("#unloadLocation"));
}
//移除卸货位置
function deleteUnloadLocation(id){
	$("#divLocation"+id).remove();
}

function addArea()
	{	
		var divDoor = $("div[name='divDoor']");			
		var divLocation= $("div[name='divLocation']")
		var doorId = "" ;
		var unloadLocationId = "";
		$(divDoor).each(
				function(j){
					var door = $(this).find("input[name='door']").val();
					doorId += ","+door;
				});
		 if(doorId.length > 1 ){doorId = doorId.substr(1);}
		 
		 $(divLocation).each(
					function(k){
						var locationId = $(this).find("input[name='locationName']").val();
						unloadLocationId += ","+locationId;
					});
			 if(unloadLocationId.length > 1 ){unloadLocationId = unloadLocationId.substr(1);}
		 
		var reg = /^\d{1,3}$/;
		
		var filetype = $("#idFile").val().split(".");
		if($("#area_psid").val() == 0)
		{
			alert($("#area_psid").val());
		}
		else if(!reg.test($("#area_name").val()))
		{
			alert("请按命名规则输入区域名");
		}
		
		else if(filetype[filetype.length-1].toLowerCase() != "jpg"&&filetype[filetype.length-1].toLowerCase() != "bmp"&&filetype[filetype.length-1].toLowerCase() != "gif")
		{
			alert("请上传正确的图片格式（支持jpg，bmp，gif）");
		}
		else
		{
			document.add_location_area_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/addLocationArea.action";
			document.add_location_area_form.attrStoragedoor.value =doorId;
			document.add_location_area_form.attrUnloadLocation.value =unloadLocationId;
			document.add_location_area_form.submit();
		}
		
	};
	function closeWindow()
	{
		$.artDialog.close();
	}
</script>
<div style="display:none" id="keep_alive"></div>
</body>
</html>
