<%@page import="java.math.RoundingMode"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	String backurl = StringUtil.getString(request,"backurl");
	
	long psid = StringUtil.getLong(request,"psid");
	DBRow storageCatalog = null ;
	if(psid != 0l){
		storageCatalog = catalogMgr.getDetailProductStorageCatalogById(psid);
	}else{
		storageCatalog = new DBRow();
	}
	String loadKmlAear =StringUtil.getString(request,"loadKmlAear");
	DBRow datas  =null;
	DBRow[] zones =null;
	DBRow[] locations =null;
	DBRow[] spots =null;
	DBRow[] docks =null;
	DBRow[] stagings =null;
	DBRow[] webcams =null;
	if(loadKmlAear.equals("true")){
		datas = locationAreaXmlImportMgrCc.getAllAreaInfoForStorage(storageCatalog);
		locations=(DBRow[])datas.getValue("location");
		zones=(DBRow[])datas.getValue("zones");
		docks=(DBRow[])datas.getValue("docks");
		stagings=(DBRow[])datas.getValue("staging");
		spots=(DBRow[])datas.getValue("parking");
		webcams=(DBRow[])datas.getValue("webcam");
		
	}
	String systenFolder = ConfigBean.getStringValue("systenFolder");
%>
<script type="text/javascript">
var systenFolder = <%=systenFolder %>;
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Map File</title>

<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />

<script type="text/javascript"
	src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript"
	src="../js/autocomplete/jquery.ui.tabs.js"></script>


<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css"
	href="../js/autocomplete/jquery.ui.tabs.css" />

<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="/Sync10-ui/lib/art/plugins/jquery.artDialog.source.js"
	type="text/javascript"></script>
<script src="/Sync10-ui/lib/art/plugins/iframeTools.source.js"
	type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet"
	href="../js/fullcalendar/chosen.css" />
<script type="text/javascript"
	src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css"
	type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css"
	rel="stylesheet" />
<style type="text/css" media="all">
@import "../js/thickbox/global.css";

@import "../js/thickbox/thickbox.css";
</style>

<!-- GoogleMap -->
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
<script src="../js/maps/GoogleMapsV3.js"></script>
<script src="../js/maps/jsmap.js"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<!-- 细滚动条 -->
<link rel="stylesheet"
	href="/Sync10-ui/lib/mCustomScrollbar/css/jquery.mCustomScrollbar.css" />
<script type="text/javascript"
	src="/Sync10-ui/lib/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css" />
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>

<style type="text/css">
#locationArea {
	display: none;
}

.set {
	border: 2px solid #999999;
	font-weight: normal;
	line-height: 18px;
	margin-bottom: 10px;
	margin-top: 10px;
	padding: 7px;
	width: 250px;
	word-break: break-all;
}

<!--
.input-line {
	width: 200px;
	font-size: 12px;
}

.text-line {
	font-size: 12px;
}

.STYLE1 {
	font-size: 12px;
	font-weight: bold;
}

.STYLE2 {
	color: #666666
}

.STYLE3 {
	font-size: 12px;
	font-weight: bold;
	color: #666666;
}

-->
.perview {
	width: 100%;
	height: 98%;
	background: #fff;
	font-size: 12px;
	border-collapse: collapse;
}

.perview td, .perview th {
	padding: 5px;
	border: 1px solid #ccc;
}

.perview th {
	background-color: #f0f0f0;
	height: 20px;
}

.perview a:link, .perview a:visited, .perview a:hover, .perview a:active
	{
	color: #00F;
}

.perview table {
	width: 100%;
	border-collapse: collapse;
}

.selectAllBtn {
	width: 30px;
	position: absolute;
	height: 20px;
	top: 3px;
	border-radius: 5px;
	background-color: rgb(206, 202, 202);
	cursor: pointer;
	left: 7px;
}

.selectAllCheckbox {
	float: left;
	margin-top: 4px;
	margin-left: 2px;
}

.selectAllBtn:hover {
	border: 1px solid rgb(245, 226, 80);
}

.showOp {
	background: url('imgs/showOp.png');
	height: 6px;
	width: 7px;
	display: block;
	float: right;
	margin-top: 8px;
	margin-right: 5px;
}

.selectOp {
	z-index: 1;
	font-size: 12px;
	width: 107px;
	text-align: left;
	left: 7px;
	top: 24px;
	border-radius: 3px;
	border: 1px solid #bbb;
	box-shadow: 0 1px 3px rgba(0, 0, 0, .2);
	background: #fff;
	line-height: 25px;
	padding: 7px 0;
	white-space: nowrap;
	position: absolute !important;
	font-weight: initial !important;
	display: none;
}

.selectOp>div:hover {
	background-color: #CDE3F3;
	cursor: pointer;
}
/* .selectAl:hover,.invertSelection:hover,.selectNone:hover{
	background-color: #CDE3F3;
	cursor: pointer;
} */
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div id="locationArea">
		<form id="locationAreaForm">
			<table width="100%" height="100%" border="0" cellpadding="0"
				cellspacing="0" align="center">
				<tr>
					<td colspan="2" align="center" valign="top">
						<table width="95%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="40" align="left"
									style="font-size: 14px; font-weight: bolder">Storage： <input
									type="hidden" name="area_psid" id="area_psid"
									value="<%=storageCatalog.get("id",0l) %>"> <input
									type="hidden" name="title" id="title"
									value="<%=storageCatalog.getString("title") %>"> <select
									disabled="disabled">
										<option value="<%=storageCatalog.get("id",0l) %>"><%=storageCatalog.getString("title") %></option>
								</select>
								</td>
								<input type="hidden" name="kml_name" id="kml_name"
									value="<%=StringUtil.getString(request,"source")%>">
								<td align="right"><input type="button"
									name="previewKmlButton" value="Map Preview"
									class="short-button" onClick="previewKml()"
									id="previewKmlButton" style="display: none;" /> <input
									type="button" name="previewKmlOffButton" value="Hide Map"
									class="short-button" onClick="previewKmlOff()"
									id="previewKmlOffButton" style="display: none;" />&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="button" name="previewKmlOffButton"
									value="Update Kml" class="short-button" onClick="updateKml()"
									id="updateKmlOffButton" />&nbsp;&nbsp;&nbsp;&nbsp; <input
									type="button" name="choiceFileButton" value="File Import"
									class="short-button" onClick="importKml('jquery_file')"
									id="choiceFileButton" /> <input type="button"
									name="exportFileButton" value="File Export"
									class="short-button"
									onClick="exportExcel(<%=storageCatalog.get("id",0l)%>)"
									id="exportFileButton" /> <input type="button"
									name="batchDeleteStorageLayer" id="batchDelete"
									value="Batch Delete" class="short-button"
									onClick="batchDelete_()" />
									<div style="display: none;" id="jquery_file">
										<input type="hidden" name="file_names" />
									</div></td>
							</tr>
							<tr>
								<td colspan="2" align="center" valign="top">
									<div id="previewKmlMap"
										style="width: 100%; height: 600px; display: none;">
								</td>
							</tr>
							<tr>
								<td id="storage_location_list" align="center" colspan="2"
									valign="top">
									<%  if(!loadKmlAear.equals("") && loadKmlAear.equals("true")){
				%>
									<div id="tabs">
										<ul>
											<li><a href="#area">Zone</a></li>
											<li><a href="#location">Location</a></li>
											<li><a href="#docks">Docks</a></li>
											<li><a href="#staging">Staging</a></li>
											<li><a href="#parking">Parking</a></li>
											<li><a href="#webcam">Webcam</a></li>
										</ul>
										<div id='area'>
											<table style="width: 100%;" border="0" cellpadding="0"
												cellspacing="0" class="zebraTable" id="areaTable">
												<tr>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 11%;'>Zone</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 12%;'>Titles</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 12%;''>Dock</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 11%;'>X
														Position</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 11%;'>Y
														Position</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 11%;'>X
														Length</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 11%;'>Y
														Length</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 10%;'>Angle</th>
													<th nowrap='nowrap' class='left-title '
														style='vertical-align: center; text-align: center; width: 6%;'>Status</th>
												</tr>
												<% if(zones!=null && zones.length>0){
	  								for(int i=0;i<zones.length;i++){
	  									DBRow row=zones[i];
	  									String zone=row.getString("area_name");
	  									String title=row.getString("title");
	  									String docks_=row.getString("docks");
	  									String xPosition=row.getString("x");
		  								String yPosition=row.getString("y");
	  									String xLength=row.getString("width");
	  									String yLength=row.getString("height");
	  									String angle=row.getString("angle")==null?"0":row.getString("angle");
	  									long area_id = row.get("area_id",0l);
	  									String stateStr=area_id>0?"Old":"New";
	  									String color = area_id>0?"black":"green";
	  									if(zone!=null && !zone.equals("")){
	  								%>
												<tr <%if(i%2==0){%> class="even" <%}else{%> class="odd"
													<%} %>>
													<td style="word-break: break-all; font-weight: bold"><%=zone %></td>
													<td style="word-break: break-all; font-weight: bold"><%=title %></td>
													<td style="word-break: break-all; font-weight: bold"><%=docks_ %></td>
													<td style="word-break: break-all; font-weight: bold"><%=xPosition %></td>
													<td style="word-break: break-all; font-weight: bold"><%=yPosition %></td>
													<td style="word-break: break-all; font-weight: bold"><%=xLength %></td>
													<td style="word-break: break-all; font-weight: bold"><%=yLength %></td>
													<td style="word-break: break-all; font-weight: bold"><%=angle %></td>
													<td style="word-break: break-all; font-weight: bold"><span
														style="color: <%=color %>;"><%=stateStr %></span></td>
												</tr>
												<% 	
	  									}
	  									
	  								}
	  							}else{
	  								out.print("<tr>colspan='9' align='center'>No data...</td></tr>");
	  							}%>
											</table>
										</div>
										<div id="location">
											<div id="tabs1">
												<ul id='locationTable'>

													<%
										if(locations!=null && locations.length>0){
											Map<String,String> htmlStr=new HashMap<String,String>();
											Map<String,Integer> groupCount=new HashMap<String,Integer>();
											for(int i=0;i<locations.length;i++){
												DBRow row=locations[i];
												String placemark_name=row.getString("placemark_name");
												String title=storageCatalog.getString("title");
												String area_name=row.getString("area_name");
												String is3D=row.getString("is3D").equals("0")?"2D":"3D";
												long slc_id=row.get("slc_id", 01);
												String stateStr=slc_id>0?"Old":"New";
			  									String color = slc_id>0?"black":"green";
												String tableBody="<td style='word-break:break-all;font-weight:bold;'>"+placemark_name+"</td>"+
							  							"<td style='word-break:break-all;font-weight:bold'>"+title+"</td>"+
							  							"<td style='word-break:break-all;font-weight:bold'>"+area_name+"</td>"+
							  							"<td style='word-break:break-all;font-weight:bold'>"+is3D+"</td>"+
							  							"<td style='word-break:break-all;font-weight:bold'><span style='color: "+color+";'>"+stateStr+"</td>";
												if(htmlStr.isEmpty()){
													String tableHead="<div id='location_"+row.getString("area_name")+"'><table width='100%' border='0' cellpadding='0' cellspacing='0' class='zebraTable' ><tr>"+
								  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Location</th>"+
								  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Storage</th>"+
								  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Zone</th>"+
								  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Dimensional</th>"+
								  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Status</th></tr>";
								  					groupCount.put(row.getString("area_name"), 0);
													htmlStr.put(row.getString("area_name"), tableHead+"<tr class='even'>"+tableBody+"</tr>");
													out.print("<li><a href='#location_"+row.get("area_name")+"'>location_"+row.get("area_name")+"</a></li>");
													
												}else{
													if(htmlStr.containsKey(row.getString("area_name"))){
														String html=htmlStr.get(row.getString("area_name"));
														htmlStr.remove(row.getString("area_name"));
														Integer count=groupCount.get(row.getString("area_name"));
														if((count+1)%2==0){
															htmlStr.put(row.getString("area_name"), html+"<tr class='even'>"+tableBody+"</tr>");
														}else{
															htmlStr.put(row.getString("area_name"), html+"<tr class='odd'>"+tableBody+"</tr>");
														}
														groupCount.remove(row.getString("area_name"));
														groupCount.put(row.getString("area_name"), count+1);
														
													}else{
														String tableHead="<div id='location_"+row.getString("area_name")+"'><table width='100%' border='0' cellpadding='0' cellspacing='0' class='zebraTable' ><tr>"+
									  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Location</th>"+
									  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Storage</th>"+
									  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Zone</th>"+
									  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Dimensional</th>"+
									  							"<th nowrap='nowrap'  class='left-title' style='vertical-align: center;text-align: center;'>Status</th></tr>";
									  							htmlStr.put(row.getString("area_name"), tableHead+"<tr class='even'>"+tableBody+"</tr>");
														groupCount.put(row.getString("area_name"), 0);
														//locationNames.add(row.getString("area_name"));
														out.print("<li><a href='#location_"+row.get("area_name")+"'>location_"+row.get("area_name")+"</a></li>");
													}
												}
											}
											StringBuffer htmlStr_=new StringBuffer();
											for (Map.Entry<String, String> entry : htmlStr.entrySet()) {
												htmlStr_.append(entry.getValue()).append("</table></div>");
											}
											out.print(htmlStr_.toString());
										}else{
									%>
													<li><a href="#location_">location</a></li>
													<div id="#location_">
														<table width="100%" border="0" cellpadding="0"
															cellspacing="0" class="zebraTable">
															<tr>
																<th nowrap="nowrap" class="left-title "
																	style="vertical-align: center; text-align: center;">Location</th>
																<th nowrap="nowrap" class="left-title "
																	style="vertical-align: center; text-align: center;">Storage</th>
																<th nowrap="nowrap" class="left-title "
																	style="vertical-align: center; text-align: center;">Zone</th>
																<!-- <th nowrap="nowrap"  class="left-title " style="vertical-align: center;text-align: center;">位置类型</th> -->
																<th nowrap="nowrap" class="left-title "
																	style="vertical-align: center; text-align: center;">Dimensional</th>
																<th nowrap="nowrap" class="left-title "
																	style="vertical-align: center; text-align: center;">Status</th>
															</tr>
															<tr>
																<td colspan='5' align='center'>No data...</td>
															</tr>
														</table>
													</div>
													<%
										}
									%>
												</ul>


											</div>
										</div>
										<div id="docks">
											<table style="width: 100%;" border="0" cellpadding="0"
												cellspacing="0" class="zebraTable" id="docksTable">
												<tr>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Dock</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Storage</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Zone</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Status</th>
												</tr>
												<%
								if(docks!=null && docks.length>0){
									for(int i=0;i<docks.length;i++){
									DBRow row=docks[i];
									long door_id = row.get("door_id",0l);
									String stateStr = door_id>0?"Old":"New";
									String color = door_id>0?"black":"green";
							%>
												<tr <%if(i%2==0){%> class="even" <%}else{%> class="odd"
													<%} %>>
													<td style="word-break: break-all; font-weight: bold">
														<%=row.get("placemark_name","-") %>
													</td>
													<td style="word-break: break-all; font-weight: bold"><%=storageCatalog.getString("title") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("area_name","-") %></td>
													<td style="word-break: break-all; font-weight: bold"><span
														style="color: <%=color %>;"><%=stateStr %></span></td>
												</tr>
												<%
									}
								}else{
									out.print("<tr><td colspan='8' align='center'>No data...</td></tr>");	
								}
							%>
											</table>
										</div>
										<div id="staging">
											<table style="width: 100%;" border="0" cellpadding="0"
												cellspacing="0" class="zebraTable" id="stagingTable">
												<tr>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Staging</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Storage</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Dock</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Status</th>
												</tr>
												<%
							
								if(stagings!=null && stagings.length>0){
									for(int i=0;i<stagings.length;i++){
										DBRow row=stagings[i];
										long staging_id = row.get("staging_id",0l);
										String stateStr = staging_id>0?"Old":"New";
										String color = staging_id>0?"black":"green";
							%>
												<tr <%if(i%2==0){%> class="even" <%}else{%> class="odd"
													<%} %>>
													<td style="word-break: break-all; font-weight: bold"
														id="location_name" name="location_name"
														value="<%=row.get("placemark_name","-") %>"><%=row.get("placemark_name","-") %>
													</td>
													<td style="word-break: break-all; font-weight: bold"><%=storageCatalog.getString("title") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("door_name","-") %></td>
													<td style="word-break: break-all; font-weight: bold"><span
														style="color: <%=color %>;"><%=stateStr %></span></td>
												</tr>
												<%
									}
								}else{
									out.print("<tr><td colspan='8' align='center'>No data...</td></tr>");
								}
							%>
											</table>
										</div>
										<div id="parking">
											<table style="width: 100%;" border="0" cellpadding="0"
												cellspacing="0" class="zebraTable" id="parkingTable">
												<tr>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Parking</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Storage</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Zone</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Status</th>
												</tr>
												<%
			  						if(spots!=null && spots.length>0){
			  							for(int i=0;i<spots.length;i++){
			  								DBRow row=spots[i];
											long parking_id = row.get("parking_id",0l);
											String stateStr = parking_id>0?"Old":"New";
											String color = parking_id>0?"black":"green";
								%>
												<tr <%if(i%2==0){%> class="even" <%}else{%> class="odd"
													<%} %>>
													<td style="word-break: break-all; font-weight: bold"
														id="parking_name" name="parking_name"
														value="<%=row.get("placemark_name","-") %>"><%=row.get("placemark_name","-") %>
													</td>
													<td style="word-break: break-all; font-weight: bold"><%=storageCatalog.getString("title") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("area_name","-") %>&nbsp;</td>
													<td style="word-break: break-all; font-weight: bold"><span
														style="color: <%=color %>;"><%=stateStr %></span></td>
												</tr>
												<%
			  							}
			  						}else{
			  							out.print("<tr><td colspan='8' align='center'>No data...</td></tr>");
			  						}
			  					%>
											</table>
										</div>
										<div id="webcam">
											<table style="width: 100%;" border="0" cellpadding="0"
												cellspacing="0" class="zebraTable" id="webcamTable">
												<tr>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Ip</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Port</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">User</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Password</th>
													<th nowrap="nowrap" class="left-title "
														style="vertical-align: center; text-align: center;">Status</th>
												</tr>
												<%
			  						if(webcams!=null && webcams.length>0){
			  							for(int i=0;i<webcams.length;i++){
			  								DBRow row=webcams[i];
											long id = row.get("id",0l);
											String stateStr = id>0?"Old":"New";
											String color = id>0?"black":"green";
								%>
												<tr <%if(i%2==0){%> class="even" <%}else{%> class="odd"
													<%} %>>
													<td style="word-break: break-all; font-weight: bold"><%=row.getString("ip") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("port","-") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("user","-") %></td>
													<td style="word-break: break-all; font-weight: bold"><%=row.get("password","-") %></td>
													<td style="word-break: break-all; font-weight: bold"><span
														style="color: <%=color %>;"><%=stateStr %></span></td>
												</tr>
												<%
			  							}
			  						}else{
			  							out.print("<tr><td colspan='8' align='center'>No data...</td></tr>");
			  						}
			  					%>
											</table>
										</div>
									</div> <% 
				} %>

								</td>
							</tr>
						</table>
					</td>
				</tr>
				<%--   <tr id="submit_button_tr">
    <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">&nbsp;
    </td>
    <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
    	<input type ="checkbox" name="checkbox" id="deleteKml" ps_id="<%=storageCatalog.get("id",0l) %>">Delete old data before save
	 	<input type="button" name="Submit2" value="Save" class="normal-green" onClick="saveArea()" id="saveButton">
	  	<input type="button" name="Submit2" value="Cancel" class="normal-white" onClick="closeWindow();">
	</td>
  </tr> --%>
			</table>
		</form>
		<form id="loadKmlForm">
			<input type="hidden" id="loadKmlAear" name="loadKmlAear"
				value="false"> <input type="hidden" id="source"
				name="source" value=""> <input type="hidden" id=psid
				name="psid" value="<%=storageCatalog.get("id",0l)%>">
		</form>
		<form action="" name="download_form" id="download_form"></form>
	</div>
	<script type="text/javascript">
$(function(){
	
	 $("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 }  
	 });
	 $("#tabs").tabs("select",0);
	 if("true"=='<%=loadKmlAear %>'){
		 $("#batchDelete").hide();
		 $("#tabs1").tabs('destroy');
		 $("#previewKmlButton").show();
		 $("#tabs1").tabs({
			cache: true,
			spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
			cookie: { expires: 180000 }  
		 });
		 $("#tabs1").tabs("select",0);
		 
		 var api = $.artDialog.open.api;	// 			$.artDialog.open扩展方法
		 api.button([
	 		{
	 			name: 'Save',
	 			callback: function () {
	 				saveArea(this);
	 				return false ;
	 			},
	 			focus:true
	 			
	 		},
	 		{
	 			name: 'Cancel' ,
	 			callback: function () {
	 				closeWindow();
	 			}
	 			
	 		}]
	 	);
		 
		 var ps_id='<%=storageCatalog.get("id",0l) %>';
		 if(api.DOM.buttons.find('input#deleteKml').length==0){
			 api.DOM.buttons.prepend(" <input type ='checkbox' name='checkbox' id='deleteKml' ps_id='"+ps_id+"' >Delete old data before save");
		 }else{
			 api.DOM.buttons.find('input#deleteKml').prop('checked',false);
		 } 
		 $.unblockUI();
		 
		 //$("body").show();
		 
	 }else{
		 var api = $.artDialog.open.api;
		 api.button([]);
		 //api.DOM.buttons.html('');
		 $("#submit_button_tr").hide();
		 $("#batchDelete").show();
		//加载storage_location_list.jsp
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/storageLocationList.html',
			type: 'post',
			dataType: 'html',
			timeout: 180000,
			cache:false,
			data:{psid:<%=psid%>,ps_title:'<%=storageCatalog.getString("title") %>'},
			beforeSend:function(request){
				$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			error: function(){
				$.unblockUI();       //遮罩关闭
			},
			success: function(html){
				$("#storage_location_list").html(html);
				$.unblockUI();       //遮罩关闭
				
				
			}
		});
	}
	 
$("#locationArea").show();
setTimeout(function(){
	 $("#locationArea").height($("#locationArea").parents().eq(0)[0].clientHeight);
	 $("#locationArea").mCustomScrollbar();	
},500)

	
	
})
function closeWindow()
{
	$.artDialog.close();
}
var addLocationIndex = 0;
//文件导入
function importKml(_target){
	if(_target=="jquery_file"){
	    var targetNode = $("#"+_target);
	    var fileNames = $("input[name='file_names']",targetNode).val();
	    var obj  = {
		     //reg:"kml",
		     reg:"xls",
		     limitSize:3,
		     target:_target,
		     limitNum:1
		 }
	    var uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/file/jquery_file_up.html?"; 
		uri += jQuery.param(obj);
		if(fileNames.length > 0 ){
			uri += "&file_names=" + fileNames;
		}
		$.artDialog.open(uri , {id:'file_up',title: 'File Import',width:'770px',height:'530px', lock: true,opacity: 0.3,fixed: true,
			 close:function(){
	 	}});
	}
}

function exportExcel(ps_id){
	$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});   //遮罩打开
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/exportFolderData.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:{"ps_id":ps_id},
		cache:false,
		success: function(data){
			$.unblockUI();       //遮罩关闭
			if(data["flag"]=="true"){
				document.download_form.action=data["fileurl"];
				document.download_form.submit();				
			}	
		},
		error: function(){
			 $.unblockUI();
			 showMessage("System Error!","error");
		}
	});
}

/* function deleteKml(ps_id){
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/deleteAllStorageLayer',
		type: 'get',
		dataType: 'json',
		timeout: 60000,
		data:{ps_id:ps_id},
		cache:false,
		beforeSend:function(request){
			 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success: function(data){
			 $.unblockUI();
			if(data["flag"]=="true"){
				//showMessage(data["result"],"succeed");
				showMessage("Succeed","succeed");
				setTimeout(function(){
				//  $.artDialog && $.artDialog.close();
					$("#loadKmlAear").val("false");
					$("#loadKmlForm").submit();
				},1000);
			}else{
				showMessage(data["result"],"error");	
			}	
		},
		error:function(){
			 $.unblockUI();
			 showMessage("System Error!","error");
		}
	});
} */

//jquery file up 回调函数
function uploadFileCallBack(fileNames){
	if($.trim(fileNames).length > 0 ){
		getKMLParseresult(fileNames);
	}
}
function getKMLParseresult(fileNames){
	$.ajax({
		url:'<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/LocationAreaXmlImportAction.action',
		data:'fileName='+fileNames+'&storageId=<%=storageCatalog.get("id",0l) %>&storageTitle=<%=storageCatalog.getString("title") %>',
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   
			if(data && data.flag == "true"){
				$("#loadKmlAear").val("true");
				$("#source").val(data.source);
				$("#loadKmlForm").submit();
			}else{
				alert(data.err);
				 $.unblockUI();
			}
		},
		error:function(){
			 $.unblockUI();
			 showMessage("System error","error");
		}
	});
}
//保存KML导入的数据
function saveArea(_this){
	var ps_id = _this.DOM.buttons.find('#deleteKml').attr('ps_id');
	var flag=false;
	if(_this.DOM.buttons.find('#deleteKml').prop('checked')){
		flag=true;
		//deleteKml(ps_id)
	}
	ajaxSubmit(flag);
}
function valiData(){
	var isSelected = $("#isThreeDimensional option[value='-1']");
	for(var i=0; i<isSelected.length; i++){
		if(isSelected[i].selected){
			return false;
		}
	}
	return true;
}
function ajaxSubmit(flag){
	$.ajax({
		url:'/Sync10/_gis/storageCotroller/saveExcelLayerData',
		data:$("#locationAreaForm").serialize()+"&flag="+flag,
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
				showMessage("Save Succeed","succeed");
				setTimeout(function(){
				   // $.artDialog && $.artDialog.close();
					$("#loadKmlAear").val("false");
					$("#loadKmlForm").submit();
				},1000);
			}else{
				showMessage("Save failed","alert");
			}
		},
		error:function(){
			$.unblockUI();
			showMessage("System error","error");
		}
	});
}

//地图预览
var map = null;
var kmlLayer = null;
var kmlLayer_loc = null;
function previewKml(){
	var fileName = "<%=StringUtil.getString(request,"source")%>";
	if(!fileName || fileName==null || fileName==""){
		return null;
	}
	 var href = window.location.href.split("/");
	var url = href[0]+"//"+href[2]+"/"+href[3]+"/upl_imags_tmp/"+fileName;
	var url_loc = href[0]+"//"+href[2]+"/"+href[3]+"/upl_imags_tmp/loc_"+fileName; 
	$("#previewKmlMap").show();
	$("#previewKmlButton").hide();
	$("#previewKmlOffButton").show();
	if(map == null){
		map = jsNewMapClear(document.getElementById("previewKmlMap"));
	}
	var fit = false;
	if(kmlLayer == null){
		kmlLayer = jsNewKmlLayer(url,null,map,true);
		fit = true;
	}
	if(kmlLayer_loc == null){
		kmlLayer_loc = jsNewKmlLayer(url_loc,null,map,!fit);
	}
}
function previewKmlOff(){
	$("#previewKmlMap").hide();
	$("#previewKmlOffButton").hide();
	$("#previewKmlButton").show();
}
//重新生成kml
function updateKml(){
	var ps_id =<%=psid%>;
	$.ajax({
		url:'/Sync10/_gis/storageCotroller/createKml',
		data:{"ps_id":ps_id},
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	   },
		success:function(data){
			if(data && data.flag == "true"){
				showMessage("Update Kml Succeed","succeed");
			}
		},
		error:function(){
			showMessage("System error","error");
		}
	});
}
//显示全选，反选，不选操作plane
function showOpPlane(event){
	$(".selectOp").show();
	stopPropagation(event);
	$(window).on('click',function(){
		$(".selectOp").hide();
		$(window).unbind('click');
	})
	 /* $(this.document).on('click',function(){
		$(".selectOp").hide();
		$(this.document).unbind('click');
	})  */
}
//全选，不选功能
function selectAllOrNoneOp(_this,event){
	stopPropagation(event);
	
	var tableEl=$(_this).parents().find('table').eq(0);
	var checkboxElArray= tableEl.find("input[type='checkbox']");
	var checked;
	if($(_this).prop('checked')){
		checked=true;
	}else{
		checked=false;
	}
	for(var i=0;i<checkboxElArray.length;i++){
		$(checkboxElArray[i]).prop('checked',checked);
	}
}
//全选
function selectAll(_this,event){
	stopPropagation(event);
	$(".selectOp").hide();
	var tableEl=$(_this).parents().find('table').eq(0);
	var checkboxElArray= tableEl.find("input[type='checkbox']");
	for(var i=0;i<checkboxElArray.length;i++){
		$(checkboxElArray[i]).prop('checked',true);
	}
}
//反选
function invertSelection(_this,event){
	stopPropagation(event);
	$(".selectOp").hide();
	var tableEl=$(_this).parents().find('table').eq(0);
	var checkboxElArray= tableEl.find("input[type='checkbox']");
	for(var i=0;i<checkboxElArray.length;i++){
		//$(checkboxElArray[i]).prop('checked',true);
		if($(checkboxElArray[i]).prop('checked')){
			$(checkboxElArray[i]).prop('checked',false);
		}else{
			$(checkboxElArray[i]).prop('checked',true);
		}
	}
}
//不选
function selectNone(_this,event){
	stopPropagation(event);
	$(".selectOp").hide();
	var tableEl=$(_this).parents().find('table').eq(0);
	var checkboxElArray= tableEl.find("input[type='checkbox']");
	for(var i=0;i<checkboxElArray.length;i++){
		$(checkboxElArray[i]).prop('checked',false);
	}
}
//阻止事件冒泡
function stopPropagation(event){
	var e=event || window.event;
	if (e && e.stopPropagation){
	    e.stopPropagation();    
	}
	else{
	    e.cancelBubble=true;
	}
}
//批量删除
function batchDelete_(){
	
	var ids="",type;
	var ps_id =<%=psid%>;
	var tabIndex=$("#tabs").tabs().tabs('option', 'selected');
	var tabHref=$("#tabs ul>li a").eq(tabIndex).attr('href');
	//var tableID=$(checkBoxEl[0]).parents().find('table').eq(0).attr('id');
	var trElArray=new Array();
	if(tabHref=='#area'){
		type=5;
	}else if(tabHref=='#webcam'){
		type=6;
	}else if(tabHref=='#parking'){
		type=4;
	}else if(tabHref=='#staging'){
		type=2;
	}else if(tabHref=='#docks'){
		type=3;
	}else{
		type=1;
		tabIndex=$("#tabs1").tabs().tabs('option', 'selected');
		tabHref=$("#tabs1 ul>li a").eq(tabIndex).attr('href');
	}
	var checkBoxEl;
	var checkBoxEl=$(tabHref).find("input[type='checkbox']");
	for(var i=1;i<checkBoxEl.length;i++){
		if($(checkBoxEl[i]).prop('checked')){
			
			var id=$(checkBoxEl[i]).val();
			if(id){
				var trEl=$(checkBoxEl[i]).parent().parent();
				trElArray.push(trEl);
				if(ids){
					ids+=','+id;
				}else{
					ids=id;
				}
			}
		}
	}
	if(ids===""){
		return;
	}
	 $.artDialog({
	 	title:'Notify',
	 	lock: true,
	 	opacity: 0.3,
	 	icon:'question',
	         content: "<span style='font-weight:bold;'>Delete selected row ?</span>",
	         button: [
	        {
	            name: 'YES',
	            callback: function () {
	            	deleteAjax();
	            },
	            focus: true
	        },
	        {
	            name: 'NO'
	        }
	    ]
		});
	 var deleteAjax=function(){
		 $.ajax({
				url:'/Sync10/_gis/storageCotroller/batchDeleteStorageLayer',
				data:{ps_id: ps_id, ids:ids, type:type},
				dataType:'json',
				type:'post',
				async: false,
				beforeSend:function(request){
					 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			    },
				success:function(data){
					 $.unblockUI();
					 if(data.flag== "true"){
						 $(".selectAllCheckbox").prop('checked',false);
						 for(var i=0;i<trElArray.length;i++){
							$(trElArray[i]).remove();
						}
					 }
				},
				error:function(){
					 $.unblockUI();
				}
			});
	 }
	
}
</script>
	<style type="text/css">
.ui-tabs .ui-tabs-panel {
	background: white;
}

table tr.odd {
	background: #f9f9f9;
}

table tr.even:hover, table tr.odd:hover {
	background: #e6f3c5
}

#locationArea .mCSB_inside .mCSB_container {
	margin-right: 0px;
}

#locationArea .mCSB_scrollTools {
	right: -6px
}
</style>
</body>
</html>
