<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyTypeKey"%>
<%@page import="com.cwc.app.key.LoadUnloadOccupancyStatusKey"%>
<%@page import="com.cwc.app.key.LoadUnloadRelationTypeKey"%>
<%@page import="com.cwc.app.key.TransportRegistrationTypeKey"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="../../include.jsp"%> 
<%
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(5);

String cmd = StringUtil.getString(request,"cmd");
String doorId = StringUtil.getString(request,"doorId");
long psId = StringUtil.getLong(request,"psId");
int occupancyType = StringUtil.getInt(request, "occupancy_type");
int rel_type = StringUtil.getInt(request, "rel_type");
long rel_id	= StringUtil.getLong(request, "rel_id");

DBRow storageDoorList[];
if(cmd.equals("search")){
	if(0 == rel_type && 0 == rel_id)
	{
		storageDoorList = storageDoorLocationMgrZYZ.getSearchStorageDoor(doorId,psId,pc);
	}
	else
	{
		storageDoorList = doorOrLocationOccupancyMgrZyj.getStorageDoorsByNamePsRel(doorId, psId, occupancyType, rel_type, rel_id, pc);
	}
}else{
	storageDoorList = storageDoorLocationMgrZYZ.getAllStorageDoor(pc);
}

	DBRow[] storageCatalogs = catalogMgr.getProductStorageCatalogTree();
	String DeleteStorageDoorAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/storage_location/DeleteStorageDoorAction.action";
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>装卸门占用信息</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css"; 
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(function(){
	if(0 != $("#rel_type").val())
	{
		$("#rel_id_span").css("display","");
	}
	else
	{
		$("#rel_id_span").css("display","none");
	}
});
function changeLoadUnloadType(obj)
{
	$("#rel_id").val("");
	if(0 != $(obj).val())
	{
		$("#rel_id_span").css("display","");
	}
	else
	{
		$("#rel_id_span").css("display","none");
	}
}
</script>
<style type="text/css">
#door_occupancy_table tr td{line-height: 20px;}
</style>
  </head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
  <br>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   装卸门管理 </td>
	  </tr>
	</table>
  <br/>
  <form action="" name="search_form" method="post"> 
	<table width="98%">
		<tr>
					<td>
					门牌号：<input style="width: 200px;" name="doorId" type="text" id="doorId"  value="<%=doorId %>">&nbsp;&nbsp;
					仓库：<select id="psId" name="psId">
						<option value="0">请选择仓库...</option>
						<%
							for(int i =0 ;i<storageCatalogs.length;i++)
							{
						%>
							<option value="<%=storageCatalogs[i].get("id",0l) %>" <%=storageCatalogs[i].get("id",0l)==psId?"selected":"" %>><%=storageCatalogs[i].getString("title") %></option>
						<%
							}
						%>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;
					占用单据:
					<select name="rel_type" id="rel_type" onchange="changeLoadUnloadType(this);">
						<option value="0">请选择单据类型</option>
						<%
							LoadUnloadRelationTypeKey loadUnloadRelationTypeKey = new LoadUnloadRelationTypeKey();
							ArrayList<String> loadUnloadRelationTypeKeys = loadUnloadRelationTypeKey.getLoadUnloadRelationTypes();
							for(int i = 0; i < loadUnloadRelationTypeKeys.size(); i ++)
							{
								boolean isSelected = (rel_type==Integer.parseInt(loadUnloadRelationTypeKeys.get(i)))?true:false;
						%>
							<option value='<%=loadUnloadRelationTypeKeys.get(i) %>' <%if(isSelected){out.print("selected='selected'");} %>><%=loadUnloadRelationTypeKey.getLoadUnloadRelationTypeName(loadUnloadRelationTypeKeys.get(i)) %></option>
						<%		
							}
						%>
					</select>
					<span id="rel_id_span"><input name="rel_id" id="rel_id" value='<%=0!=rel_id?rel_id:"" %>'/></span>
						<input type="hidden" name="occupancy_type" value='<%=LoadUnloadOccupancyTypeKey.DOOR %>'/>
						<input type="hidden" name="cmd" value="search">
						<input name="Submit21" type="submit" class="button_long_refresh" value="过滤" />&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
		</tr>
	</table>
	</form>
	<br/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="18%" class="right-title" style="vertical-align: center;text-align: center;">门牌号</th>
        <th width="18%" class="left-title" style="vertical-align: center;text-align: center;">所属仓库</th>
        <th width="74%" class="left-title" style="vertical-align: center;text-align: center;">占用信息</th>
    </tr>
  <%
  		if(storageDoorList != null){
  				
  			for(int i = 0 ;i<storageDoorList.length; i++){
  %>
    <tr>
      <td  width="18%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageDoorList[i].getString("doorId") %></td>
      <td  width="18%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageDoorList[i].getString("title") %></td>
      <td  width="74%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' >
			<table id="door_occupancy_table" style="border-collapse: collapse;">
      		
      				<%
      					DBRow[] locationOccupancys = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR,storageDoorList[i].get("sd_id",0l),
      							0,0 , 0, null, null, null, null,0,2,0);
      					if(locationOccupancys.length > 0)
      					{
      						for(int j = 0; j < locationOccupancys.length; j ++)
      						{
      				%>
      						<tr>
				      			<td>
				      				<%=locationOccupancys[j].getString("book_start_time") %>--
				      				<%=locationOccupancys[j].getString("book_end_time") %>
				      			</td>
				      			<td>
				      				<%=new LoadUnloadRelationTypeKey().getLoadUnloadRelationTypeName(locationOccupancys[j].get("rel_type",0)) %>
				      				:<%=locationOccupancys[j].get("rel_id",0) %>
				      			</td>
				      			<td>
				      				<%=new TransportRegistrationTypeKey().getTransportRegistrationTypeKeyValue(locationOccupancys[j].get("rel_occupancy_use",0)) %>
				      			</td>
				      			<td>
				      				<%=new LoadUnloadOccupancyStatusKey().getLoadUnloadOccupancyStatusName(locationOccupancys[j].get("occupancy_status",0)) %>
				      			</td>
				      			<td>
				      				<%=null==adminMgrLL.getAdminById(locationOccupancys[j].getString("creator"))?"":adminMgrLL.getAdminById(locationOccupancys[j].getString("creator")).getString("employe_name")%>
				      			</td>
				      			<td>
				      				<%
				      					SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				      					SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				      				%>
				      				<%=to.format(from.parse(locationOccupancys[j].getString("create_time"))) %>
				      			</td>
				      		</tr>
      				<%			
      						}
      					}
      					else
      					{
      						out.println("");
      					}
      				%>
      			
      	</table>
	  </td>
    </tr>
   <% }
  	}
   %>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="" method="post">
	          <input type="hidden" name="p">
	          <input type="hidden" name="cmd" value="<%=cmd %>"/>
	          <input type="hidden" name="doorId" value="<%=doorId %>" />	
	          <input type="hidden" name="psId" value="<%=psId %>" />          
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      跳转到 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table> 
  </body>
</html>
