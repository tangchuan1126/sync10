<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
int p = StringUtil.getInt(request,"p");
PageCtrl pc = new PageCtrl();
pc.setPageNo(p);
pc.setPageSize(30);

String cmd = StringUtil.getString(request,"cmd");
String locationName = StringUtil.getString(request,"locationName");
long pscId = StringUtil.getLong(request,"pscId");

DBRow loadUnloadLocation[];
if(cmd.equals("search")){
	loadUnloadLocation = storageDoorLocationMgrZYZ.getSearchLoadUnloadLocation(locationName,pscId,pc);
}else{
    loadUnloadLocation = storageDoorLocationMgrZYZ.getAllLoadUnloadLocation(pc);
}

DBRow[] storageCatalogs = catalogMgr.getProductStorageCatalogTree();
String DeleteLoadUnloadLocationAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/storage_location/DeleteloadUnloadLocationAction.action";
%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>装卸位置管理</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script> 

<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css"; 
</style>

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '170px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};

</script>
<script type="text/javascript">

	//添加装卸位置
	function addLoadUnloadLocation(){
		var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_location/add_load_unload_location.html"; 
	    $.artDialog.open(uri , {title: "增加装卸位置",width:'520px',height:'270px', lock: true,opacity: 0.3,fixed: true});
   }

	//修改装卸位置
	function modLoadUnloadLocation(locationId){
		var  uri = '<%=ConfigBean.getStringValue("systenFolder")%>' + "administrator/storage_location/mod_load_unload_location.html?locationId="+locationId; 
	    $.artDialog.open(uri , {title: "修改装卸位置",width:'520px',height:'270px', lock: true,opacity: 0.3,fixed: true});
   } 
	//删除 提示
	function deleteLoadUnloadLocation(id,locationName){
		if(id!=0 ){
			$.artDialog.confirm('确认删除  '+locationName+' 装卸位置吗？', function(){
				ajaxDelLoadUnloadLocation(id);
			}, function(){
			});
		}
	}
	//删除
	function ajaxDelLoadUnloadLocation(id){
		$.ajax({
			url:'<%=DeleteLoadUnloadLocationAction%>',
			data:'locationId='+id,
			dataType:'json',
			type:'post',
			beforeSend:function(){
			$.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
			},
			success:function(data){
				$.unblockUI();
				if(data && data.flag == "true"){
					refreshWindow();
				}else{
					showMessage("系统错误...","alert");
				}
			},
			error:function(){
				showMessage("系统错误","error");
				$.unblockUI();
			}
	
		});
	}
	
	//刷新
	function refreshWindow(){
		window.location.reload();
	}
</script>
  </head>
  
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
  <br>
	<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <tr>
	    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库管理 »   装卸位置管理 </td>
	  </tr>
	</table>
  <br/>
  <form action="" name="search_form"> 
	<table width="98%">
		<tr>
					<td>
					位置名称：<input style="width: 200px;" name="locationName" type="text" id="locationName"  value="<%=locationName %>">&nbsp;&nbsp;
					仓库：<select id="pscId" name="pscId">
						<option value="0">请选择仓库...</option>
						<%
							for(int i =0 ;i<storageCatalogs.length;i++)
							{
						%>
							<option value="<%=storageCatalogs[i].get("id",0l) %>" <%=storageCatalogs[i].get("id",0l)==pscId?"selected":"" %>><%=storageCatalogs[i].getString("title") %></option>
						<%
							}
						%>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="hidden" name="cmd" value="search">
						<input name="Submit21" type="submit" class="button_long_refresh" value="过滤" />&nbsp;&nbsp;&nbsp;&nbsp;
					    <input name="Submit3" type="button" class="long-button-add" value="增加" onClick="addLoadUnloadLocation();">
					</td>
		</tr>
	</table>
	</form>
	<br/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th width="33%" class="right-title" style="vertical-align: center;text-align: center;">位置编号</th>
        <th width="33%" class="left-title" style="vertical-align: center;text-align: center;">所属仓库</th>
        <th width="34%" style="vertical-align: center;text-align: center;" class="left-title">操作</th>
    </tr>
  <%
  		if(loadUnloadLocation != null){
  			for(int i=0; i<loadUnloadLocation.length; i++){
  %>
    <tr>
      <td  width="33%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=loadUnloadLocation[i].getString("location_name") %></td>
      <td  width="33%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=loadUnloadLocation[i].getString("title") %></td>
   	  <td  width="34%" height="60" align="center" valign="middle" style='word-break:break-all;font-weight:bold'>
       <input name="Submit1" type="button" class="short-short-button-mod" value="修改" onClick="modLoadUnloadLocation(<%=loadUnloadLocation[i].get("id",0l) %>)">&nbsp;&nbsp;
       <input name="Submit2" type="button" class="short-short-button-del" value="删除" onClick="deleteLoadUnloadLocation(<%=loadUnloadLocation[i].get("id",0l) %>,'<%=loadUnloadLocation[i].getString("location_name") %>')">
      </td>
    </tr>
   <%	}
  	} 
  	%>
    </table>
    <br/>
    <table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
	  <form name="dataForm" action="load_unload_location_list.html">
	          <input type="hidden" name="p">
	          <input type="hidden" name="cmd" value="<%=cmd %>"/>	     
	          <input type="hidden" name="locationName" value="<%=locationName %>" />  
	          <input type="hidden" name="pscId" value="<%=pscId %>"/>  
	  </form>
	        <tr> 
	          
	    <td height="28" align="right" valign="middle"> 
	      <%
	int pre = pc.getPageNo() - 1;
	int next = pc.getPageNo() + 1;
	out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
	out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
	out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
	out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
	out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
	%>
	      跳转到 
	      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
	      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
	    </td>
	        </tr>
</table> 
  </body>
</html>
