<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String backurl = StringUtil.getString(request,"backurl");
String  psid = StringUtil.getString(request,"psid");
String  psTitle = StringUtil.getString(request,"ps_title");
String loadKmlAear = StringUtil.getString(request,"loadKmlAear");
%>
<html>
<head>
<title>Storage location list</title>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table id="location_list"  border="0" cellpadding="0" cellspacing="0" align="center"  style="width: 100%;height: 100%;"  >
			<tr>
				<td align="center" colspan="2" valign="top">
					<div id="tabs">
						<ul>
							<li><a href="#area" onclick="loadData(5)">Zone</a></li>
							<li><a href="#location" onclick="loadData(1)">Location</a></li>
							<li><a href="#docks" onclick="loadData(3)">Docks</a></li>
							<li><a href="#staging" onclick="loadData(2)">Staging</a></li>
							<li><a href="#parking" onclick="loadData(4)">Parking</a></li>
							<li><a href="#webcam" onclick="loadData(6)">Webcam</a></li>
						</ul>
							<div id='area'>
								<table style="width: 100%;" border="0"cellpadding="0" cellspacing="0" class="zebraTable" id="areaTable" >
								</table>
							</div>
							<div id="location">
							<div id ="tabs1">
								<ul id ='locationTable'></ul>
							</div>
							</div>
						<div id="docks" >
							<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="docksTable" >
							</table>
						</div>
					 	<div id="staging">
							<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="stagingTable" >
							</table>
						</div>
					 	<div id="parking">
							<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="parkingTable" >
							</table>
						</div>
					 	<div id="webcam">
							<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="webcamTable" >
							</table>
						</div>
					</div>
 				</td>
			</tr>
</table>
	<input type="hidden" id="psid" name="psid" value="<%=psid%>">
</body>
<script type="text/javascript">
$(function(){

	 $("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 }  
	});
	 $("#tabs").tabs("select",0);
	 
	 setTimeout(function(){
		 $("#locationArea").height($("#locationArea").parents().eq(0)[0].clientHeight);
		 $("#locationArea").mCustomScrollbar();
	 },500)
});
var area = null;
function isNull(param){
	return param?param:"";
}
//加载相关数据
function loadData(type){
	var _type = type;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/loadPosition.action',
		type: 'post',
		data:{"ps_id":<%=psid %>,"type":_type},
		dataType: 'json',
		timeout: 60000,
		cache:false,
		async:false, 
		beforeSend:function(request){
		},
		success: function(data){
			if(data){
					var rows=data.rows;
					var locationArea=data.locationarea?data.locationarea:null;
					var html ="";
					var colspan = (_type==1 || _type==5)?13:12;
					var addHtml = "<tr><td colspan='"+colspan+"' align='center'><button type='button' style='width:60px; height:20px;' onclick='addLocation(this.parentNode.parentNode,"+_type+")'>+</button></td></tr>";
					if(_type==3){
						html="<tr >"+
							"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
						 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
						 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 10%;'>Dock</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 15%;'>Zone</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Angle</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>Operation</th>"+
	  					"</tr>";
						if(rows.length>0){
							for(var i=0;i<rows.length; i++){
								var r = rows[i];
								html+=  "<tr ondblclick='modifyLocation(this,3)'>"+
									    "<td><input class='selectCheckbox' type='checkbox' value='"+r.sd_id+"'></td>"+
									    "<td id='doorid' style='word-break:break-all;font-weight:bold;'>"+isNull(r["doorid"])+"&nbsp;</td>"+
										"<td id='area_name' style='word-break:break-all;font-weight:bold;'>"+isNull(r["area_name"])+"&nbsp;</td>"+
										"<td id='x' style='word-break:break-all;font-weight:bold;'>"+isNull(r["x"])+"&nbsp;</td>"+
										"<td id='y' style='word-break:break-all;font-weight:bold;'>"+isNull(r["y"])+"&nbsp;</td>"+
										"<td id='height' style='word-break:break-all;font-weight:bold;'>"+isNull(r["height"])+"&nbsp;</td>"+
										"<td id='width' style='word-break:break-all;font-weight:bold;'>"+isNull(r["width"])+"&nbsp;</td>"+
										"<td id='angle' style='word-break:break-all;font-weight:bold;'>"+isNull(r["angle"])+"&nbsp;</td>"+
										"<td align='center'>"+
											"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,3)'>&nbsp;&nbsp;"+
											"<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.sd_id+"&apos;,3)'>"+
											"<input type='hidden' id='sd_id' value='"+r["sd_id"]+"'>"+
											"<input type='hidden' id='area_id' value='"+r["area_id"]+"'>"+
										"</td>"+
										"</tr>";
									
							}
						}else{
							html+="<tr><td colspan='8' align='center'>No data...</td></tr>";
						}
						html += addHtml;
						$("#docksTable").html(html);
					}else if(_type==4){
						html="<tr>"+
							"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
						 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
						 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Parking</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Zone</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Angle</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>Operation</th>"+
	  					"</tr>";
						if(rows.length>0){
							for(var i=0;i<rows.length; i++){
								var r = rows[i];
								html+="<tr ondblclick='modifyLocation(this,4)'>"+
									"<td><input class='selectCheckbox' type='checkbox' value='"+r.yc_id+"'></td>"+
									"<td  id='yc_no' style='word-break:break-all;font-weight:bold' id='parking_name' name='parking_name' value='"+r.placemark_name+"'>"+isNull(r["yc_no"])+"&nbsp;</td>"+
									"<td id='area_name' style='word-break:break-all;font-weight:bold' >"+isNull(r.area_name)+"&nbsp;</td>"+
									"<td id='x' style='word-break:break-all;font-weight:bold' >"+isNull(r.x)+"&nbsp;</td>"+
									"<td id='y' style='word-break:break-all;font-weight:bold' >"+isNull(r.y)+"&nbsp;</td>"+
									"<td id='height' style='word-break:break-all;font-weight:bold' >"+isNull(r.height)+"&nbsp;</td>"+
									"<td id='width' style='word-break:break-all;font-weight:bold' >"+isNull(r.width)+"&nbsp;</td>"+
									"<td id='angle' style='word-break:break-all;font-weight:bold' >"+isNull(r.angle)+"&nbsp;</td>"+
									"<td align='center'>"+
										"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,4)'>&nbsp;&nbsp;"+
										"<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.yc_id+"&apos;,4)'>"+
										"<input type='hidden' id='yc_id' value='"+r.yc_id+"'>"+
										"<input type='hidden' id='area_id' value='"+r.area_id+"'>"+
									"</td>"+
								"</tr>";
							}
						}else{
							html+="<tr><td colspan='8' align='center'>No data...</td></tr>";
						}
						html += addHtml;
						$("#parkingTable").html(html);	
					}else if(_type==2){
						html="<tr>"+
							"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
						 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
						 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 10%;'>Staging</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Dock</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 14%;'>Y Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 12%;'>Angle</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>Operation</th>"+
	  					"</tr>";
						if(rows.length>0){
								for(var i=0;i<rows.length; i++){
									var r = rows[i];	
									html+="<tr ondblclick='modifyLocation(this,2)'>"+
										"<td><input class='selectCheckbox' type='checkbox' value='"+r.id+"'></td>"+
										"<td id='location_name' style='word-break:break-all;font-weight:bold' id='location_name' name='location_name' value='"+r.placemark_name+"'>"+isNull(r.location_name)+"&nbsp;</td>"+
										"<td id='doorid' style='word-break:break-all;font-weight:bold' >"+isNull(r.doorid)+"&nbsp;</td>"+
										"<td id='x' style='word-break:break-all;font-weight:bold' >"+isNull(r.x)+"&nbsp;</td>"+
										"<td id='y' style='word-break:break-all;font-weight:bold' >"+isNull(r.y)+"&nbsp;</td>"+
										"<td id='height' style='word-break:break-all;font-weight:bold' >"+isNull(r.height)+"&nbsp;</td>"+
										"<td id='width' style='word-break:break-all;font-weight:bold' >"+isNull(r.width)+"&nbsp;</td>"+
										"<td id='angle' style='word-break:break-all;font-weight:bold' >"+isNull(r.angle)+"&nbsp;</td>"+
										"<td align='center'>"+
											"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,2)'>&nbsp;&nbsp;"+
											"<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.id+"&apos;,2)'>"+
											"<input type='hidden' id='id' value='"+r.id+"'>"+
											"<input type='hidden' id='sd_id' value='"+r.sd_id+"'>"+
										"</td>"+
									"</tr>";
								}
						}else{
							html+="<tr><td colspan='8' align='center'>No data...</td></tr>";
						}
						html += addHtml;
						$("#stagingTable").html(html);
					}else if(_type==6){
						html="<tr>"+
						"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
					 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
					 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>IP</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>Port</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 9%;'>User</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 9%;'>Password</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>X</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>Y</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>Inner_Radius</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>Outer_Radius</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>S_Degree</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>E_Degree</th>"+
  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 7%;'>Operation</th>"+
  					"</tr>";
							if(rows.length>0){	
								for(var i=0;i<rows.length; i++){
									var r = rows[i];	
									html+="<tr ondblclick='modifyLocation(this,6)' value='"+r.id+"'>"+
										"<td><input class='selectCheckbox' type='checkbox' value='"+r.id+"'></td>"+
										"<td id='ip' style='word-break:break-all;font-weight:bold' id='ip' name='ip' value='"+r.ip+"'>"+isNull(r.ip)+"&nbsp;</td>"+
										"<td id='port' style='word-break:break-all;font-weight:bold' >"+isNull(r.port)+"&nbsp;</td>"+
										"<td id='user' style='word-break:break-all;font-weight:bold' >"+isNull(r.user)+"&nbsp;</td>"+
										"<td id='password' style='word-break:break-all;font-weight:bold' >"+isNull(r.password)+"&nbsp;</td>"+
										"<td id='x' style='word-break:break-all;font-weight:bold' >"+isNull(r.x)+"&nbsp;</td>"+
										"<td id='y' style='word-break:break-all;font-weight:bold' >"+isNull(r.y)+"&nbsp;</td>"+
										"<td id='inner_radius' style='word-break:break-all;font-weight:bold' >"+isNull(r.inner_radius)+"&nbsp;</td>"+
										"<td id='outer_radius' style='word-break:break-all;font-weight:bold' >"+isNull(r.outer_radius)+"&nbsp;</td>"+
										"<td id='s_degree' style='word-break:break-all;font-weight:bold' >"+isNull(r.s_degree)+"&nbsp;</td>"+
										"<td id='e_degree' style='word-break:break-all;font-weight:bold' >"+isNull(r.e_degree)+"&nbsp;</td>"+
										"<td align='center'>"+
											"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,6)'>&nbsp;&nbsp;"+
											"<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.id+"&apos;,6)'>"+
											"<input type='hidden' id='cam_id' value='"+r.id+"'>"+
										"</td>"+
									"</tr>";
								}
						}else{
							html+="<tr><td colspan='11' align='center'>No data...</td></tr>";
						}
						html += addHtml;
						$("#webcamTable").html(html);
							
					}else if(_type==1){
						$("#tabs1").tabs('destroy');
						var divTR="";
						if(locationArea.length>0){
							//html="<ul id ='locationTable'>";
							for(var i=0;i<locationArea.length;i++){
								var slc_area=locationArea[i].area_name;
								var area_id=locationArea[i].area_id;
								var folderName="Zone_"+slc_area;
								folderName=folderName.replace("\\.+|\\s+", "");
								html +="<li><a href='#"+area_id+"' onclick='loadLocation(&apos;"+slc_area+"&apos;,&apos;"+area_id+"&apos;)'>"+folderName+"</a></li>";
								var divTR="<div id='"+area_id+"' name='"+folderName+"'></div>";
							$("#tabs1").find("[name="+folderName+"]").length>0 || $("#tabs1").append(divTR);
							}
							$("#locationTable").html(html)
					 	}else{
							html+="  <table style='width: 100%;' border='0' cellpadding='0' cellspacing='0' class='zebraTable' >"+
								  	"<tr>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 14%;'>Location</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 15%;'>Zone</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Dimensional</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Position</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Position</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>X Length</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Y Length</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Angle</th>"+
										"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 6%;'>Operation</th>"+
									"</tr>";
							html+="<tr><td colspan='9' align='center'>No data...</td></tr>"
							html += addHtml;
							html+="</table>";
							$("#tabs1").html(html);
						}
						$("#tabs1").tabs({
							cache: true,
							spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
							cookie: { expires: 30000 }
						});
						$("#tabs1").tabs("select",0);
					}else if(_type==5){
						 html="	<tr>"+
						 	"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
						 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
						 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Zone</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 12%;'>Titles</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 12%;''>Dock</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>X Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Y Position</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>X Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Y Length</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 10%;'>Angle</th>"+
	  						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 6%;'>Operation</th>"+
	  					"</tr>";
						if(rows.length>0){
						for(var i=0;i<rows.length;i++){
							var r = rows[i];
							html+="<tr ondblclick='modifyLocation(this,5)'>"+
										"<td><input class='selectCheckbox' type='checkbox' value='"+r.area_id+"'></td>"+
										"<td id='area_name' style='word-break:break-all;font-weight:bold'>"+isNull(r.area_name)+"&nbsp;</td>"+
										"<td id='title_name' style='word-break:break-all;font-weight:bold'>"+isNull(r.title_name)+"&nbsp;</td>"+
										"<td id='doorid' style='word-break:break-all;font-weight:bold'>"+isNull(r.doorid)+"&nbsp;</td>"+
										"<td id='x' style='word-break:break-all;font-weight:bold'>"+isNull(r.x)+"&nbsp;</td>"+
										"<td id='y' style='word-break:break-all;font-weight:bold'>"+isNull(r.y)+"&nbsp;</td>"+
										"<td id='height' style='word-break:break-all;font-weight:bold'>"+isNull(r.height)+"&nbsp;</td>"+
										"<td id='width' style='word-break:break-all;font-weight:bold'>"+isNull(r.width)+"&nbsp;</td>"+
										"<td id='angle' style='word-break:break-all;font-weight:bold'>"+isNull(r.angle)+"&nbsp;</td>"+
										"<td align='center'>"+
											"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,5)'>&nbsp;&nbsp;"+
											"<img id='delete'  src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.area_id+"&apos;,5)'>"+

											"<input type='hidden' id='area_id' value='"+r.area_id+"'>"+
										"</td>"+
									"</tr>";
						  }
					   }else{
						   html+="<tr><td colspan='9' align='center'>No data...</td></tr>";
					   }
						html += addHtml;
						$("#areaTable").html(html);
					}
					onLoadInitZebraTable();
				}
		}
	});
}
 
function loadLocation(slc_area,area_id){
	var _slc_area =slc_area;
	var _area_id =area_id;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/loadLocaiton.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:{"slc_area":_slc_area,"ps_id":<%=psid%>},
		cache:false,
		async:false, 
		beforeSend:function(request){
		},
		success: function(data){
			if(data.length>0){
			var folderdivName="Zone_"+_slc_area;
			var html="";
			html+="<table style='width: 100%;' border='0' cellpadding='0' cellspacing='0' class='zebraTable' >";
					html+="<tr>"+
						"<th style='position: relative;width: 33px;background-color: #e5e5e5;border-top: 1px solid #bbbbbb;border-left: 1px solid #bbbbbb;border-bottom: 1px solid #bbbbbb;'>"+
					 	"<div class='selectAllBtn' onclick='showOpPlane(event)'><input class='selectAllCheckbox' type='checkbox' onclick='selectAllOrNoneOp(this,event)'><span class='showOp'></span></div>"+
					 	"<div class='selectOp'><div class='selectAll' onclick='selectAll(this,event)'>Select All</div><div class='invertSelection' onclick='invertSelection(this,event)'>Invert Selection</div><div class='selectNone' onclick='selectNone(this,event)'>Select None<div></div></th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Location</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 13%;'>Zone</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 10%;'>Dimensional</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 12%;'>X Position</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 12%;'>Y Position</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>X Length</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 11%;'>Y Length</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 8%;'>Angle</th>"+
						"<th nowrap='nowrap'  class='left-title ' style='vertical-align: center;text-align: center; width: 6%;'>Operation</th>"+
					"</tr>";
				for(var i=0;i<data.length;i++){
					var r=data[i];
					html+=  "<tr ondblclick='modifyLocation(this,1)'>"+
					"<td><input class='selectCheckbox' type='checkbox' value='"+r.slc_id+"'></td>"+
					"<td id='slc_position' style='word-break:break-all;font-weight:bold'>"+isNull(r.slc_position)+"&nbsp;</td>"+
					"<td id='area_name' style='word-break:break-all;font-weight:bold'>"+isNull(r.area_name)+"&nbsp;</td>"+
					"<td id='is_three_dimensional' style='word-break:break-all;font-weight:bold'>"+(r.is_three_dimensional==0?'2D':'3D')+"</td>"+	
					"<td id='x' style='word-break:break-all;font-weight:bold'>"+isNull(r.x)+"&nbsp;</td>"+
					"<td id='y' style='word-break:break-all;font-weight:bold'>"+isNull(r.y)+"&nbsp;</td>"+
					"<td id='height' style='word-break:break-all;font-weight:bold'>"+isNull(r.height)+"&nbsp;</td>"+
					"<td id='width' style='word-break:break-all;font-weight:bold'>"+isNull(r.width)+"&nbsp;</td>"+
					"<td id='angle' style='word-break:break-all;font-weight:bold'>"+isNull(r.angle)+"&nbsp;</td>"+
					"<td align='center'>"+
						"<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,1)'>&nbsp;&nbsp;"+
						"<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,&apos;"+r.slc_id+"&apos;,1)'>"+
						"<input type='hidden' id='slc_id' value='"+r.slc_id+"'>"+
						"<input type='hidden' id='slc_area' value='"+r.slc_area+"'>"+
					"</td>"+
				"</tr>";

				}
			}else{
			}
			html += "<tr><td colspan='10' align='center'><button type='button' style='width:60px; height:20px;' onclick='addLocation(this.parentNode.parentNode,1)'>+</button></td></tr>";

			html+="</table>";
			$("#"+_area_id).html(html);
			onLoadInitZebraTable();
		}
		
	});
}
function loadStorageData(){
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/GetStorageDataAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:{"ps_id":<%=psid %>},
		cache:false,
		success: function(data){
			if(data){
				title = data.title;
				dock = data.dock;
				area = data.area;
			}
		}
	});
}
//删除位置
function deleteLocation(el,id,type){
	var _el = el;
	var data = "location_id="+id+"&type="+type+"&ps_id=<%=psid %>";
	$.ajax({
		url: '/Sync10/_gis/storageCotroller/deleteStorageLayer',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data: data,
		cache:false,
		success: function(data){
			if(data && data.flag=="true"){
				$(_el).remove();
			}
		},
		error:function(){
			showMessage("Delete fail ","error");
		}
	});
}
//修改位置
function modifyLocation(el,type){
	//type: 1 location, 2 staging, 3 docks, 4 parking, 5 area
	var node = creatElement(el,type);
	if(node){
		$(el).hide();
		$(el).after(node);
	}
}
function creatElement(e,type){
	var style = "width: 100%; height: 18px; font-size: 12px;"
	var html = null;
	var trId = "";
	var trId_add = "";
	if(e == null){  //添加location
		trId_add = "add_" + type + "_" + addLocationIndex++;
	}
	if(type==6){
		trId = e==null ? trId_add : "modify_webcam_"+getVal(e,"cam_id");
		html = "<tr id='"+trId+"'  onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td><input id='ip' name='ip' type='text' style='"+style+"' value='"+getVal(e,"ip")+"'></b>";
		html += "<input id='cam_id' name='cam_id' type='hidden' value='"+getVal(e,"cam_id")+"'></td>";//保存id属性
		html += "<td><input id='port' name='port' type='text' style='"+style+"' value='"+getVal(e,"port")+"'></td>";
		html += "<td><input id='user' name='user' type='text' style='"+style+"' value='"+getVal(e,"user")+"'></td>";
		html += "<td><input id='password' name='password' type='text' style='"+style+"' value='"+getVal(e,"password")+"'></td>";
		html += "<td><input id='x' name='x' type='text' style='"+style+"' value='"+getVal(e,"x")+"'></td>";
		html += "<td><input id='y' name='y' type='text' style='"+style+"' value='"+getVal(e,"y")+"'></td>";
		html += "<td><input id='inner_radius' name='inner_radius' type='text' style='"+style+"' value='"+getVal(e,"inner_radius")+"'></td>";
		html += "<td><input id='outer_radius' name='outer_radius' type='text' style='"+style+"' value='"+getVal(e,"outer_radius")+"'></td>";
		html += "<td><input id='s_degree' name='s_degree' type='text' style='"+style+"' value='"+getVal(e,"s_degree")+"'></td>";
		html += "<td><input id='e_degree' name='e_degree' type='text' style='"+style+"' value='"+getVal(e,"e_degree")+"'></td>";
	}
	if(type==5){	//area
		var areaName = getVal(e,"area_name");
		var areaId = getVal(e,"area_id");
		trId = e==null ? trId_add : "modify_area_"+areaId;
		html = "<tr id='"+trId+"' onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td>"
				+"	<input id='area_name' name='area_name' type='text' style='"+style+"' value='"+areaName+"'>"
				+"	<input type='hidden' name='area_id' id='area_id' value='"+areaId+"'>"
				+"</td>";
		html += "<td>"
				+"	<input id='title_name' name='title_name' type='text' style='"+style+"' value='"+getVal(e,"title_name")+"' onclick='selectTitle(this,\""+areaName+"\",\""+trId+"\")'>"
				+"	<input id='title_id' name='title_id' type='hidden' value='0'>"
				+"</td>";
		html += "<td>"
				+"	<input id='doorid' name='doorid' type='text' style='"+style+"' value='"+getVal(e,"doorid")+"' onclick='selectDoor(this,\""+areaName+"\",\""+trId+"\")'>"
				+"	<input id='sd_id' name='sd_id' type='hidden' value='0'>"
				+"</td>";
	}
	if(type==4){	//parking
		var ycId = getVal(e,"yc_id");
		var ycNo = getVal(e,"yc_no");
		trId = e==null ? trId_add : "modify_parking_"+ycId;
		html = "<tr id='"+trId+"'  onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td>"
				+"	<input id='yc_no' name='yc_no' type='text' style='"+style+"' value='"+ycNo+"'>"
				+"	<input type='hidden' id='yc_id' name='yc_id' value='"+ycId+"'>"
				+"</td>";
		html += "<td>"
				+"	<input id='area_name' name='area_name' type='text' style='"+style+"' value='"+getVal(e,"area_name")+"' onclick='selectAreaSingle(3,\""+ycNo+"\",\""+trId+"\")'>"
				+"	<input id='area_id' name='area_id' type='hidden' value='"+getVal(e,"area_id")+"'>"
				+"</td>";
	}
	if(type==3){	//docks
		var sdId = getVal(e,"sd_id");
		var doorId = getVal(e,"doorid");
		trId = e==null ? trId_add : "modify_docks_"+sdId;
		html = "<tr id='"+trId+"' onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td>"
				+"	<input id='doorid' name='doorid' type='text' style='"+style+"' value='"+doorId+"'>"
				+"	<input type='hidden' name='sd_id' id='sd_id' value='"+sdId+"'>"
				+"</td>";
		html += "<td>"
				+"	<input id='area_name' name='area_name' type='text' style='"+style+"' value='"+getVal(e,"area_name")+"' onclick='selectAreaSingle(2,\""+doorId+"\",\""+trId+"\")'>"
				+"	<input id='area_id' name='area_id' type='hidden' value='"+getVal(e,"area_id")+"'>"
				+"</td>";
	}
	if(type==2){	//staging
		var staName = getVal(e,"location_name");
		var staId = getVal(e,"id");
		trId = e==null ? trId_add : "modify_staging_"+staId;
		html = "<tr id='"+trId+"' onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td>"
				+"	<input id='location_name' name='location_name' type='text' style='"+style+"' value='"+staName+"'>"
				+"	<input type='hidden' id='id' name='id' value='"+staId+"'>"
				+"</td>";
		html += "<td>"
				+"	<input id='doorid' name='doorid' type='text' style='"+style+"' value='"+getVal(e,"doorid")+"' onclick='selectDoorSingle(\""+staName+"\",\""+trId+"\")'>"
				+"	<input id='sd_id' name='sd_id' type='hidden' value='"+getVal(e,"sd_id")+"'>"
				+"</td>";
	}
	if(type==1){	//loction
		var slcName = getVal(e,"slc_position");
		var slcId = getVal(e,"slc_id");
		var dim = getVal(e,"is_three_dimensional");
		trId = e==null ? trId_add : "modify_location_"+slcId;
		html = "<tr id='"+trId+"'onkeydown='keydown(event,this,"+type+")'><td></td>";
		html += "<td>"
				+"	<input id='slc_position' name='slc_position' type='text' style='"+style+"' value='"+slcName+"'>"
				+"	<input type='hidden' id='slc_id' name='slc_id' value='"+slcId+"'>"
				+"</td>";
		html += "<td>"
				+"	<input id='area_name' name='area_name' type='text' style='"+style+"' value='"+getVal(e,"area_name")+"' onclick='selectAreaSingle(1,\""+slcName+"\",\""+trId+"\")'>"
				+"	<input id='slc_area' name='slc_area' type='hidden' value='"+getVal(e,"slc_area")+"'>"
				+"</td>";
		html += "<td>"
				+"	<select id='is_three_dimensional' name='is_three_dimensional' style='"+style+"'>"
				+"		<option value='0' ";
		if(dim == "2D"){
			html += "selected='selected'";
		}
		html += ">2D</option>"
				+"		<option value='1' "
		if(dim == "3D"){
			html += "selected='selected'";
		}
		html += ">3D</option>"
				+"	</select>"
				+"</td>";
	}
	if(type!=6){
		html += "<td><input id='x' name='x' type='text' style='"+style+"' value='"+getVal(e,"x")+"'></td>";
		html += "<td><input id='y' name='y' type='text' style='"+style+"' value='"+getVal(e,"y")+"'></td>";
		html += "<td><input id='height' name='height' type='text' style='"+style+"' value='"+getVal(e,"height")+"'></td>";
		html += "<td><input id='width' name='width' type='text' style='"+style+"' value='"+getVal(e,"width")+"'></td>";
		html += "<td><input id='angle' name='angle' type='text' style='"+style+"' value='"+getVal(e,"angle")+"'></td>";
	}
	html += "<td align='center'>"
			+"	<img src='../imgs/maps/confirm.jpg' alt='√' style='cursor: default;' onclick='saveLocation(this.parentNode.parentNode,"+type+")'>&nbsp;&nbsp;"
			+"	<img src='../imgs/maps/back_blue.png' alt='←' style='cursor: default;' onclick='cancelModify(\""+trId+"\")'>"
			+"	<input type='hidden' id='location_type' value='"+type+"'>"
			+"</td>";
	html += "</tr>";
	
	return html;
}
function getVal(ele,id){
	var val = "";
	if(ele && $(ele).find("#"+id)){
		val = $(ele).find("#"+id).text().trim();
	}
	if(val == ""){
		if(ele && $(ele).find("input#"+id).length != 0){
			val = $(ele).find("input#"+id).val();
		}
	}
	return val;
}
//选择title
function selectTitle(ele,name,elId){
	var title = $(ele).val();
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_title.html?area_name='+name+'&el_id='+elId+'&title='+title;
	$.artDialog.open(url, {title: "Select Title ["+name+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
}
//选择door
function selectDoor(ele,name,elId){
	var dock = $(ele).val();
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door.html?area_name='+name+'&el_id='+elId+'&dock='+dock+'&ps_id='+<%=psid %>;
	$.artDialog.open(url, {title: "Select Dock ["+name+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
}
//选择单个door
function selectDoorSingle(name,elId){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_door_single.html?el_id='+elId+'&ps_id=<%=psid %>';
	$.artDialog.open(url, {title: "Select Dock ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
}
//选择单个area
function selectAreaSingle(areaType,name,elId){
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/select_area_single.html?el_id='+elId+'&ps_id=<%=psid %>&area_type='+areaType;
	$.artDialog.open(url, {title: "Select Area ["+name+"]",width:'790px',height:'425px', lock: true,opacity: 0.3});
}
//选择多个zone
function selectZones(ele,name,elId){
	var area_name=$(ele).val();
	var area_id=$("#"+elId+" area_id").val();
	var url = '<%=ConfigBean.getStringValue("systenFolder")%>administrator/gis/select_zone.html?psId=<%=psid%>&area_name='+area_name+'&area_id='+area_id+'&objId='+elId;
	$.artDialog.open(url, {title:"Select Area ["+name+"]",width:'825px',height:'435px', lock: true,opacity: 0.3});
	
}
//回填title
function backFillTitle(names, ids, eleId){
	$("#"+eleId).find("#title_name").val(names);
	$("#"+eleId).find("#title_id").val(ids);
}
function backFillZone(names, ids, eleId){
	$("#"+eleId).find("#area_name").val(names);
	$("#"+eleId).find("#area_id").val(ids);
}
//回填door
function backFillDoor(names, ids, eleId){
	$("#"+eleId).find("#doorid").val(names);
	$("#"+eleId).find("#sd_id").val(ids);
}
//回填area
function backFillArea(name,id,eleId){
	$("#"+eleId).find("#area_name").val(name);
	if(eleId.indexOf("modify_location")==0){
		$("#"+eleId).find("#slc_area").val(id);
	}else{
		$("#"+eleId).find("#slc_area").val(id);
		$("#"+eleId).find("#area_id").val(id);
	}
}
//取消修改
function cancelModify(trId){
	var ele = $("#"+trId);
	$(ele).prev().show();
	$(ele).remove();
}
//保存修改
function saveLocation(node,type){
 	//位置坐标对比
	var ele = $(node);
	var change=false;
	if(type==6){
		change=iswebcamChange(ele);
	}else if(type==7){
		change=isprinterChange(ele);
	}else{
	    change = isPositionChange(ele);
	}
	var newLoc = false;
	var id = ele.attr("id");
	if(id.indexOf("add_")==0){  //添加新位置
		newLoc = true;
	    change =true;
	if(type==1){
		var name=$("#"+id+" input#slc_position").val();
		if(name.indexOf("_")>-1){
			showMessage("name not contain '_' !","error");
			return ;
		}
	}
	}
	var data = getSerializeData(ele)+"&ps_id=<%=psid %>&ps_title=<%=psTitle %>&type="+type+"&isChanged="+change;
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/StorageLocationModifyAction.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data: data,
		cache:false,
		success: function(data){
			if(data && data.flag=="true"){
				if(newLoc){
					newDataToTable(ele,data.position_id);
				}else{
					backDataToTable(ele);
				}
			}else if(data && data.flag=="false"){
				showMessage("update error","error");
				backDataToTable(ele);
			}
		},
		error:function(){
			showMessage("System error","error");
		}
	});
}
//获取提交json数据
function getJsonData(ele){
	var data = {};
	if(ele){
		var e = $(ele).find("input,select");
		for(var i=0; i<e.length; i++){
			if($(e[i]).attr("id")){
				data[$(e[i]).attr("id")] = $(e[i]).val();
			}
		}
	}
	return data;
}
//获取提交序列化数据
function getSerializeData(ele){
	var data = "";
	if(ele){
		var e = $(ele).find("input,select");
		for(var i=0; i<e.length; i++){
			if($(e[i]).attr("id")){
				data += $(e[i]).attr("id") +"="+ $(e[i]).val()+"&";
			}
		}
		if(data != ""){
			data = data.substring(0, data.length-1);
		}
	}
	return data;
}
//更新表格数据
function backDataToTable(el){
	var s = $(el);
	var d = $(el).prev();
	if(s && d){
		var e = s.find("input,select");
		for(var i=0; i<e.length; i++){
			var id = $(e[i]).attr("id");
			if(id && d.find("#"+id).length!=0){
				var tagName = d.find("#"+id)[0].tagName.toLowerCase();
				var tagName_e = e[i].tagName.toLowerCase();
				var val = $(e[i]).val();
				if(tagName == "td"){
					if(tagName_e == "select"){
						val = $(e[i]).find("option[value='"+val+"']").text();
						d.find("#"+id).html(val+"&nbsp;");
					}else{
						d.find("#"+id).html(val+"&nbsp;");
					}
				}else if(tagName == "input"){
					d.find("#"+id).val(val);
				}
			}
		}
	}
	d.show();
	s.remove();
}
//新增数据添加到表格
function newDataToTable(el,locId){
	var s = $(el);
	if(s){
		var type = s.find("#location_type").val();
		//操作
		var op = "<img src='../imgs/maps/edit.jpg' alt='I' style='cursor: default;' onclick='modifyLocation(this.parentNode.parentNode,"+type+")'>&nbsp;&nbsp;"+
				 "<img src='../imgs/maps/delete_red.jpg' alt='×' style='cursor: default;' onclick='deleteRow(this.parentNode.parentNode,"+locId+","+type+")'>";
		s.children().last().html(op);
		//隐藏属性
		var e = s.find("input[id][type='hidden']");
		e.first().val(locId);	//回填位置ID
		s.children().last().append(e);
		//显示属性
		e = s.find("input[id][type!='hidden'],select[id]");
		for(var i=0; i<e.length; i++){
			var id = $(e[i]).attr("id");
			var td = $(e[i].parentNode);
			td.attr("id",id);
			var tagName = e[i].tagName.toLowerCase();
			var val = $(e[i]).val();
			if(tagName == "select"){
				s.children().last().append("<input type='hidden' id='"+id+"' value='"+val+"'>"); //隐藏属性
				val = $(e[i]).find("option[value='"+val+"']").text();
			}
			td.html(val+"&nbsp;");
		}
		$(s.find('td')[0]).html('<input class="selectCheckbox" type="checkbox" value="'+locId+'">');
		
		s.attr("style","word-break:break-all;font-weight:bold");
		s.removeAttr("id");
	}

}	
	function deleteRow(el,id,type){
		 $.artDialog({
			 	title:'Notify',
			 	lock: true,
			 	opacity: 0.3,
			 	icon:'question',
			    content: "<span style='font-weight:bold;'>Delete this row ?</span>",
			    button: [
			        {
			            name: 'YES',
			            callback: function () {
			            	deleteLocation(el,id,type);
			            },
			            focus: true
			        },
			        {
			            name: 'NO'
			        }
			    ]
			});
	}
//校验坐标是否改变
function isPositionChange(el){
	var change = false;
	var s = $(el);
	var id = s.attr("id");
	if(id.substr(id.indexOf("add_"))==0){  //添加新位置
		return true;
	}
	var d = $(el).prev();
	var ids = ["#x","#y","#width","#height","#angle"];
	for(var i=0; i<ids.length; i++){
		if(s.find(ids[i]).val().trim() != d.find(ids[i]).text().trim()){
			change = true;
		}
	}
	return change;
}
//校验webcam坐标是否改变
function iswebcamChange(el){
	var change = false;
	var s = $(el);
	var id = s.attr("id");
	if(id.substr(id.indexOf("add_"))==0){  //添加新位置
		return true;
	}
	var d = $(el).prev();
	var ids = ["#x","#y","#inner_radius","#outer_radius","#s_degree","#e_degree"];
	for(var i=0; i<ids.length; i++){
		if(s.find(ids[i]).val().trim() != d.find(ids[i]).text().trim()){
			change = true;
		}
	}
	return change;
}
//校验printer坐标是否改变
function isprinterChange(el){
	var change = false;
	var s = $(el);
	var id = s.attr("id");
	if(id.substr(id.indexOf("add_"))==0){  //添加新位置
		return true;
	}
	var d = $(el).prev();
	var ids = ["#x","#y"];
	for(var i=0; i<ids.length; i++){
		if(s.find(ids[i]).val().trim() != d.find(ids[i]).text().trim()){
			change = true;
		}
	}
	return change;
}
//添加位置
function addLocation(node,type){
	var el = creatElement(null,type);
	$(node).before(el);
}
function keydown(evt,node,type){
	  var evt=evt?evt:(window.event?window.event:null);//兼容IE和FF
	  if (evt.keyCode==13){
		  saveLocation(node,type);
	}
}

</script>
</html>
