<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	String ps_id =StringUtil.getLong(request,"ps_id");
	DBRow storageCatalog = null ;
	if(psid != 0l){
		storageCatalog = catalogMgr.getDetailProductStorageCatalogById(psid);
	}else{
		storageCatalog = new DBRow();
	}
	DBRow 	datas = locationAreaXmlImportMgrCc.getAllAreaInfoForStorage(storageCatalog);
%>
<script type="text/javascript">
var systenFolder = <%=systenFolder %>;
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Map File</title>

<!-- 基本css 和javascript -->
<link type="text/css" href="../comm.css" rel="stylesheet" />

<script type="text/javascript" src="../js/jqGrid-4.1.1/js/jquery-1.7.2.source.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.core.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.position.js"></script>
<script type="text/javascript" src="../js/autocomplete/jquery.ui.tabs.js"></script>

   
<script type='text/javascript' src='../js/autocomplete/autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.core.css" />

<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.theme.css" />
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.ui.tabs.css" />
 
<script type="text/javascript" src="../js/jquery/jquery.cookie.js"></script>
<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<script src="../js/fullcalendar/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../js/fullcalendar/chosen.css" /> 
<script type="text/javascript" src="../js/datepicker/jquery.date_input.js"></script>
<link rel="stylesheet" href="../js/datepicker/date_input.css" type="text/css" />
<link type="text/css" href="../js/fullcalendar/stateBox.css" rel="stylesheet"/>
<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>

<!-- GoogleMap -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script>
<script src="../js/maps/GoogleMapsV3.js"></script> 
<script src="../js/maps/jsmap.js"></script> 

<!-- stateBox 信息提示框 -->
<script type="text/javascript" src='../js/showMessage/showMessage.js'></script>
<!-- 遮罩 -->
<script type="text/javascript" src="../js/blockui/jquery.blockUI.233.js"></script>
 
<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css"/>
<script>
$.blockUI.defaults = {
		css: { 
			padding:        '8px',
			margin:         0,
			width:          '230px', 
			top:            '45%', 
			left:           '40%', 
			textAlign:      'center', 
			color:          '#000', 
			border:         '3px solid #999999',
			backgroundColor:'#eeeeee',
			'-webkit-border-radius': '10px',
			'-moz-border-radius':    '10px',
			'-moz-box-shadow': '0 1px 12px rgba(0,0,0,0.5)',
			'-webkit-box-shadow':'0 1px 12px rgba(0,0,0,0.5)'
		},
		//设置遮罩层的样式
		overlayCSS:  { 
			backgroundColor:'#000', 
			opacity:        '0.6' 
		},
		baseZ: 99999, 
		centerX: true,
		centerY: true, 
		fadeOut:  1000,
		showOverlay: true
	};
</script>

<style type="text/css">
	.set {
	    border: 2px solid #999999;
	    font-weight: normal;
	    line-height: 18px;
	    margin-bottom: 10px;
	    margin-top: 10px;
	    padding: 7px;
	    width: 250px;
	    word-break: break-all;
	}
<!--
.input-line
{
	width:200px;
	font-size:12px;

}

.text-line
{
	font-size:12px;
}
.STYLE1 {font-size: 12px; font-weight: bold; }
.STYLE2 {color: #666666}
.STYLE3 {font-size: 12px; font-weight: bold; color: #666666; }
-->

.perview {width:100%; height:98%;background:#fff;font-size:12px; border-collapse:collapse;}
.perview td, .perview th {padding:5px;border:1px solid #ccc;}
.perview th {background-color:#f0f0f0; height:20px;}
.perview a:link, .perview a:visited, .perview a:hover, .perview a:active {color:#00F;}
.perview table{ width:100%;border-collapse:collapse;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form id="locationAreaForm">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="center" colspan="2" valign="top">
			<div id="tabs">
				<ul>
					<li><a href="#area" onclick="loadData(5)">Zone</a></li>
					<li><a href="#location" onclick="loadData(1)">Location</a></li>
					<li><a href="#docks" onclick="loadData(3)">Docks</a></li>
					<li><a href="#staging" onclick="loadData(2)">Staging</a></li>
					<li><a href="#parking" onclick="loadData(4)">Parking</a></li>
					<li><a href="#webcam" onclick="loadData(6)">Webcam</a></li>
				</ul>
				<div id='area'>
					<table style="width: 100%;" border="0"cellpadding="0" cellspacing="0" class="zebraTable" id="areaTable" >
					</table>
				</div>
				<div id="location">
				<div id ="tabs1">
					<ul id ='locationTable'></ul>
				</div>
				</div>
				<div id="docks" >
					<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="docksTable" >
					</table>
				</div>
			 	<div id="staging">
					<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="stagingTable" >
					</table>
				</div>
			 	<div id="parking">
					<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="parkingTable" >
					</table>
				</div>
			 	<div id="webcam">
					<table style="width: 100%; " border="0" cellpadding="0" cellspacing="0" class="zebraTable" id="webcamTable" >
					</table>
				</div>
			</div>
		</td>
	</tr>
  <tr id="submit_button_tr">
    <td width="51%" align="left" valign="middle" class="win-bottom-line" id="errorMessage">&nbsp;
    </td>
    <td width="49%" align="right" class="win-bottom-line" style="padding-right:48px;">
    	<input type ="checkbox" name="checkbox" id="deleteKml" ps_id="<%=storageCatalog.get("id",0l) %>">Delete old data before save
	 	<input type="button" name="Submit2" value="Save" class="normal-green" onClick="saveArea()" id="saveButton">
	  	<input type="button" name="Submit2" value="Cancel" class="normal-white" onClick="closeWindow();">
	</td>
  </tr>
</table>
</form>
<script type="text/javascript">
$(function(){
	 $("#tabs").tabs({
		cache: true,
		spinner: '<img src="../imgs/sending.gif" width="16" height="16" align="absmiddle" border="0"/>' ,
		cookie: { expires: 30000 }  
	});
	 $("#tabs").tabs("select",0);
});

function deleteKml(ps_id){
	$.ajax({
		url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/gis/deleteKml.action',
		type: 'post',
		dataType: 'json',
		timeout: 60000,
		data:$("#locationAreaForm").serialize(),
		cache:false,
		beforeSend:function(request){
			 $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
		},
		success: function(data){
			 $.unblockUI();
			if(data["flag"]=="true"){
				//showMessage(data["result"],"succeed");
				showMessage("Succeed","succeed");
				setTimeout(function(){
				//  $.artDialog && $.artDialog.close();
					$("#loadKmlAear").val("false");
					$("#loadKmlForm").submit();
				},1000);
			}else{
				showMessage(data["result"],"error");	
			}	
		},
		error:function(){
			 $.unblockUI();
			 showMessage("System Error!","error");
		}
	});
}

//保存KML导入的数据
function saveArea(){
	var ps_id = $("#deleteKml").attr("ps_id");
	if($("#deleteKml")[0].checked){
		deleteKml(ps_id);
	}
		ajaxSubmit();
	
}
function valiData(){
	var isSelected = $("#isThreeDimensional option[value='-1']");
	for(var i=0; i<isSelected.length; i++){
		if(isSelected[i].selected){
			return false;
		}
	}
	return true;
}
function ajaxSubmit(){
	$.ajax({
		url:'/Sync10/_gis/storageCotroller/saveExcelLayerData',
		data:$("#locationAreaForm").serialize(),
		dataType:'json',
		type:'post',
		beforeSend:function(request){
	      $.blockUI({message: '<img src="../js/thickbox/loadingAnimation6.gif" align="absmiddle"/> '});
	   },
		success:function(data){
		   $.unblockUI();
			if(data && data.flag == "true"){
				showMessage("Save Succeed","succeed");
				setTimeout(function(){
				   // $.artDialog && $.artDialog.close();
					$("#loadKmlAear").val("false");
					$("#loadKmlForm").submit();
				},1000);
			}else{
				showMessage("Save failed","alert");
			}
		},
		error:function(){
			$.unblockUI();
			showMessage("System error","error");
		}
	});
}

</script>

</body>
</html>
