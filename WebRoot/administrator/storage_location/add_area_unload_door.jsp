<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
	long psId = StringUtil.getLong(request,"psid");
	
    DBRow[] storagedoors = storageDoorLocationMgrZYZ.getStorageDoorByPsid(psId);
	String AddStorageDoorAction = ConfigBean.getStringValue("systenFolder")+"action/administrator/storage_location/AddStorageDoorAction.action";
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>增加区域卸货门</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>

<!-- 引入Art -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>		
<script>
//保存装卸门
function addAreaUnloadDoor() {
	var doorId = $("#doorId").find("option:selected").text();
	var sdId = $("#doorId").val();
	
	$.artDialog.opener.showDoorValue(doorId,sdId);
	 closeWin();
}
function closeWin()
{
	$.artDialog.close();
}
</script>
<style type="text/css">
.STYLE2 {color: #0066FF; font-weight: bold; font-size:12px;font-family: Arial, Helvetica, sans-serif;}

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="addStorageDoorForm" id="addStorageDoorForm" method="post" action="">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center" valign="top">	
<fieldset style="border:2px #cccccc solid;padding:15px;margin:10px;width:87%;">
  <legend style="font-size:15px;font-weight:normal;color:#999999;">增加区域装卸门</legend>
 	<table width="100%" border="0" cellspacing="3" cellpadding="2">
        <tr>
			  	 <td align="right" valign="middle" class="STYLE2" >门牌号:</td>
			    <td align="left" valign="middle" > 
			    	 <select id="doorId" name="doorId">
						<option value="0">请选择门牌号...</option>
						<%
							for(int i =0 ;i<storagedoors.length;i++)
							{
						%>
							<option value="<%=storagedoors[i].get("sd_id",0l) %>"><%=storagedoors[i].getString("doorId") %></option>
						<%
							}
						%>
					</select>
			    </td>
			</tr>
    </table>
 </fieldset>
</td>
</tr>
  <tr>
    <td width="51%" align="left" valign="middle" class="win-bottom-line">&nbsp;</td>
    <td width="49%" align="right" class="win-bottom-line">
	 <input type="button" name="Submit2" value="添加" class="normal-green" onClick="addAreaUnloadDoor();">

	  <input type="button" name="Submit2" value="取消" class="normal-white" onClick="closeWin();">
	</td>
  </tr>
</table>
</form>
</body>
</html>
