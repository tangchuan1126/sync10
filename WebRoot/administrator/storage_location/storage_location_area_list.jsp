<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%
	PageCtrl pc = new PageCtrl();
	pc.setPageNo(StringUtil.getInt(request,"p"));
	pc.setPageSize(30);
	
	long psid = StringUtil.getLong(request,"ps_id");
	DBRow[] areas = locationMgrZJ.getLocationAreaByPsid(psid);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>仓库区域</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/fullcalendar/jquery-1.7.1.min.js"></script>
<!-- dialog -->
<link rel="stylesheet" type="text/css" href="../js/art/skins/aero.css" />
<script src="../js/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.source.js" type="text/javascript"></script>
<script src="../js/art/plugins/iframeTools.js" type="text/javascript"></script>
<style type="text/css">
.tdstyle{border:1px #000000 solid; font-weight:bold;font-size:25px; background-color:#e5e5e5;}
.tdborderRight{border:1px #000000 solid; font-weight:bold;font-size:16px;border-right:none;border-top:none;border-bottom:none;}
.tdborderLeft{border:1px #000000 solid; font-weight:bold;font-size:16px;border-left:none;border-top:none;border-bottom:none;}
</style>
<script type="text/javascript">
//增加区域
function addArea(psid)
{
	var uri ='<%=ConfigBean.getStringValue("systenFolder")%>'+ "administrator/storage_location/add_location_area.html?psid="+psid;
	$.artDialog.open(uri,{title: "新增区域",width:'860px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}
//调整区域
function modArea(area_id)
{
	var uri='<%=ConfigBean.getStringValue("systenFolder")%>'+"administrator/storage_location/mod_location_area.html?area_id="+area_id;
	$.artDialog.open(uri,{title:"调整区域",width:'860px',height:'500px', lock: true,opacity: 0.3,fixed: true});
}

function windowRefresh()
{
	window.location.reload();
}
</script>
</head>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td align="left"><input type="button" class="long-button-add" onClick="addArea(<%=psid %>)" value="增加区域"/></td>
</tr>
</table>
<%
if(areas.length>0){
	for(int i =0;i<areas.length;i++)
	{
		DBRow[] areaDoor = locationMgrZJ.getAreaDoorOrLocationByAreaId(areas[i].get("area_id",0l),1);
		DBRow[] areaLocation = locationMgrZJ.getAreaDoorOrLocationByAreaId(areas[i].get("area_id",0l),2);
		DBRow doorId;
		DBRow locationId;
%>
	<table cellpadding='0' cellspacing='0'>
		<tr>
			<td align='center' valign='middle' class="tdstyle" width='50%'>区域名称&nbsp;&nbsp;&nbsp;<%=areas[i].getString("area_name") %></td>
			
			<td class="left-title" valign='middle' align='center' style='border:1px #000000 solid; font-weight:bold;font-size:25px;background-color:#e5e5e5;' width='50%'>所属仓库&nbsp;&nbsp;&nbsp;<%=areas[i].getString("title") %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='调整区域' class='long-button-mod' onclick='modArea(<%=areas[i].get("area_id",0l)%>)'/>&nbsp;&nbsp;<input type="button" class="short-short-button-del" onClick="delArea(<%=areas[i].get("area_id",0l)%>)" value="删除"/>
			</td>
		</tr>
		<%
			if(areaDoor.length>0 || areaLocation.length>0){
		%>
		<tr height="8%">
				<td colspan='2' align='center' valign='middle' style=' background-color:#e5e5e5;' width='50%'>
				<table border="0" width="100%" cellpadding='0' cellspacing='0'>
					 <tr>
					 <td width="12%" class="tdborderRight">&nbsp;对应装卸门：</td>
					 <td width='38%' class="tdborderLeft">
					  <%
					    for(int j = 0;j < areaDoor.length;j++){
					      doorId = storageDoorLocationMgrZYZ.getDetailStorageDoor(areaDoor[j].get("rel_dl_id",0l));
					 %>
					 	<div style="float:left;">[<%=doorId.getString("doorId") %>]&nbsp;&nbsp;</div>
					 <%} %>
					 </td>
					<td  width="10%" class="tdborderRight">&nbsp;装卸位置：</td>
					 <td width='40%' class="tdborderLeft">
					  <%
					     for(int k = 0;k < areaLocation.length;k++){
					    	 locationId = storageDoorLocationMgrZYZ.getDetailLoadUnloadLocation(areaLocation[k].get("rel_dl_id",0l));
					 %>
					 	<div style="float:left;">[<%=locationId.getString("location_name") %>]&nbsp;&nbsp;</div>
					 <%} %>
					 </td>
					 </tr>
				</table>
			</td>
		</tr>
		<%}%>
		<tr>
			<td colspan='2' align='center' style='border:1px #000000 solid; font-weight:bold'>
				<a href="javascript:void(0)" onclick="seelocationStorage(<%=areas[i].get("area_id",0l) %>)"><img width="960px" height="720px" src="<%=ConfigBean.getStringValue("systenFolder")%>administrator/storage_location/uploadImage/<%=areas[i].getString("area_img") %>"/></a>
			</td>
		</tr>
	</table>
	<br/>
	<br/>
<%
	}
}else{
%>
	<div align="center" style="height:30px;border:1px #000000 solid; font-weight:bold;font-size:18px; background-color:#e5e5e5;">该仓库暂时没有划分区域，请增加区域...</div>
<%} %>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>/action/administrator/storage_location/delLocationAreas.action" name="del_area_form" method="post">
	<input type="hidden" name="area_id"/>
</form>
<form action="location_catalog_list.html" method="post" name="see_location_catalog">
	<input type="hidden" name="area_id"/>
</form>
<script type="text/javascript">
	function delArea(area_id)
	{
		if(confirm("确定删除该区域吗？"))
		{
			document.del_area_form.area_id.value = area_id;
			document.del_area_form.submit();
		}
		
	};
	function seelocationStorage(area_id)
	{
		document.see_location_catalog.area_id.value = area_id;
		document.see_location_catalog.submit();
	}
</script>
</html>
