<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="com.cwc.app.key.LocationKey"%>
<%@ include file="../../include.jsp"%> 
<%
long area_id = StringUtil.getLong(request,"area_id");
String position = StringUtil.getString(request,"position");
String type = StringUtil.getString(request,"type","");

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);


DBRow rows[] = locationMgrZJ.getAllLocationCatalogByAreaId(area_id,pc,position,type);

DBRow area = locationMgrZJ.getDetailLocationAreaById(area_id);

LocationKey lk = new LocationKey();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>无标题文档</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../common.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery-1.3.2.min.js"></script>

		<script type="text/javascript" src="../js/popmenu/jquery.corner.js"></script>
		<script type="text/javascript" src="../js/popmenu/jquery-impromptu.2.7.min.js"></script>
		<script type="text/javascript" src="../js/popmenu/common.js"></script>
		<link href="../js/popmenu/menu.css" rel="stylesheet" type="text/css" />
		<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>		
		<script type="text/javascript" src="../js/select.js"></script>
		<script type="text/javascript" src="../js/actionform/jquery.actionform.js"></script>

<script type='text/javascript' src='../js/autocomplete/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../js/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="../js/autocomplete/jquery.autocomplete.css" />

<script src="../js/zebra/zebra.js" type="text/javascript"></script>
<link href="../js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="../js/mcdropdown/lib/jquery.mcdropdown.js"></script>
<!---// load the mcDropdown CSS stylesheet //--->
<link type="text/css" href="../js/mcdropdown/css/jquery.mcdropdown.css" rel="stylesheet" media="all" />


<style type="text/css" media="all">
@import "../js/thickbox/global.css";
@import "../js/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="../js/thickbox/1024.css" title="1024 x 768" />
<script src="../js/thickbox/thickbox.js" type="text/javascript"></script>
<script src="../js/thickbox/global.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/print/LodopFuncs.js"></script>
<script type="text/javascript" src="../js/print/m.js"></script>
		
<script>
document.write("<script src='<%=ConfigBean.getStringValue("systenFolder")%>alert_js.jsp?rnd="+Math.random()+"'></s"+"cript>")
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>

<script type="text/javascript">
	function addLocationCatalog()
	{
		$.prompt(
		
		"<div id='title'>增加位置</div><br/>位置类型：<br/><select id='proLocationCatalogType' name='proLocationCatalogType'><option value=''>请选择分类...</option><option value='R'>货架</option><option value='T'>通道</option><option value='P'>托盘</option><option value='N'>不规则</option></select><br/>位置编号<br/><input type='text' name='proLocationCatalogPosition' id='proLocationCatalogPosition'/><br/>X坐标<br/><input type='text' name='proslcx' id='proslcx' value='0'/><br/>Y坐标<br/><input type='text' name='proslcy' id='proslcy' value='0'/>",
		
		{
			  submit:
			  function(v,m,f)
			  {
			  	if(v=="y")
			  	{
			  		if(f.proLocationCatalogType == 0)
			  		{
			  			alert("请选择位置类型");
			  			return false;
			  		}
			  		if(f.proLocationCatalogPosition ==0)
			  		{
			  			alert("请填写位置编号");
			  			return false;
			  		}
			  		if(parseFloat(f.proslcx)!=f.proslcx)
			  		{
			  			alert("请输入正确的坐标格式");
			  			return false;
			  		}
			  		if(parseFloat(f.proslcy)!=f.proslcy)
			  		{
			  			alert("请输入正确的坐标格式");
			  			return false;
			  		}
			  		return true;
			  	}
			  },
			  callback: 
			  
					function (v,m,f)
					{
						if (v=="y")
						{
							document.add_location_catalog_form.position.value = f.proLocationCatalogPosition;
							document.add_location_catalog_form.type.value = f.proLocationCatalogType;
							document.add_location_catalog_form.slc_x.value = f.proslcx;
							document.add_location_catalog_form.slc_y.value = f.proslcy;
							
							document.add_location_catalog_form.submit();
						}
					}
			  ,
			  overlayspeed:"fast",
			  buttons: { 保存: "y", 取消: "n" }
		});
	}
	
	
	function modLocationCatalog(slc_id)
	{
		$.prompt(
		
		"<div id='title'>修改位置</div><br/>位置类型：<br/><select id='proLocationCatalogType' name='proLocationCatalogType'><option value=''>请选择分类...</option><option value='R'>货架</option><option value='T'>通道</option><option value='P'>托盘</option><option value='N'>不规则</option></select><br/>位置编号<br/><input type='text' name='proLocationCatalogPosition' id='proLocationCatalogPosition'/><br/>X坐标<br/><input type='text' name='proslcx' id='proslcx' value='0'/><br/>Y坐标<br/><input type='text' name='proslcy' id='proslcy' value='0'/>",
		{
			  submit:
			  function(v,m,f)
			  {
			  	if(v=="y")
			  	{
			  		if(f.proLocationCatalogType == 0)
			  		{
			  			alert("请选择位置类型");
			  			return false;
			  		}
			  		if(f.proLocationCatalogPosition ==0)
			  		{
			  			alert("请填写位置编号");
			  			return false;
			  		}
			  		if(parseFloat(f.proslcx)!=f.proslcx)
			  		{
			  			alert("请输入正确的坐标格式");
			  			return false;
			  		}
			  		if(parseFloat(f.proslcy)!=f.proslcy)
			  		{
			  			alert("请输入正确的坐标格式");
			  			return false;
			  		}
			  		return true;
			  	}
			  },
			  loaded:
				function ()
				{
					$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/getLocationCatalogJSON.action",
							{slc_id:slc_id},//{name:"test",age:20},
							function callback(data)
							{							
								$("#proLocationCatalogType").setSelectedValue(data.slc_type);
								$("#proLocationCatalogPosition").val(data.slc_position);
								
								$("#proslcx").val(data.slc_x);
								$("#proslcy").val(data.slc_y);
							}
					);
				},
			  callback: 
			  
					function (v,m,f)
					{
						if (v=="y")
						{
							document.mod_location_catalog_form.position.value = f.proLocationCatalogPosition;
							document.mod_location_catalog_form.type.value = f.proLocationCatalogType;
							document.mod_location_catalog_form.slc_x.value = f.proslcx;
							document.mod_location_catalog_form.slc_y.value = f.proslcy;
							document.mod_location_catalog_form.slc_id.value = slc_id;
							
							document.mod_location_catalog_form.submit();
						}
					}
			  ,
			  overlayspeed:"fast",
			  buttons: { 修改位置: "y", 取消: "n" }
		});
	}
	
	function delLocationCatalog(slc_id,slc_position_all)
	{
		if(confirm("您确定删除"+slc_position_all+"吗？"))
		{
			document.del_location_catalog_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/delLocationCatalog.action";
			document.del_location_catalog_form.slc_id.value = slc_id;
			document.del_location_catalog_form.submit();
		}
		
	}
	
	function search()
	{
		document.search_form.position.value = $("#search_position").val();
		document.search_form.type.value = $("#search_type").getSelectedValue();
		
		document.search_form.submit();
	}
	
	function switchCheckboxAction()
	{
		$('input[name=switchCheckboxSlcid]').inverse();
	}
	
	function getAllChecked()
	{
		return($('#switchCheckboxSlcid').checkbox());
	}
	
	function delLocationCatalogs()
	{
		if(confirm("确定删除所有选择的位置吗？"))
		{
			var slc_ids = getAllChecked();
			if(slc_ids =="")
			{
				alert("请选择至少一个位置!");
			}
			else
			{
				document.del_location_catalog_form.action = "<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/delLocationCatalogs.action";
				document.del_location_catalog_form.slc_id.value = slc_ids;	
				document.del_location_catalog_form.submit();		
			}
			
		}
		
	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title"><img src="../imgs/page_title.gif" alt="title" width="17" height="12" align="absmiddle">&nbsp;&nbsp; 仓库应用 »   位置管理</td>
  </tr>
</table>
<br/>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="65%">
	
	<div style="border:2px #dddddd solid;background:#eeeeee;padding:5px;-webkit-border-radius:7px;-moz-border-radius:7px;width:900px;">
	
	<table width="99%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td align="left" bgcolor="#eeeeee" style="padding-left:10px;">位置类型：<select name="search_type" id="search_type"><option value="" <%=type.equals("")?"selected":""%>>所有类型</option><option value="R" <%=type.equals("R")?"selected":""%>>货架</option><option value="T" <%=type.equals("T")?"selected":""%>>通道</option><option value="P" <%=type.equals("P")?"selected":""%>>托盘</option><option value="N" <%=type.equals("N")?"selected":""%>>不规则</option></select>&nbsp;&nbsp;&nbsp;&nbsp;位置编号：<input type="text" name="search_position" id="search_position" value="<%=position%>" style="width:200px;">&nbsp;&nbsp;&nbsp;<input name="Submit4" type="button" class="button_long_search" value="搜索" onClick="search()"></td>
    <td align="center" bgcolor="#eeeeee">
	<input type="button" value="批量删除" class="long-button" onclick="delLocationCatalogs()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="button" class="long-button-add" value="增加位置" onClick="addLocationCatalog()">	</td>
  </tr>
</table>
	
	</div>
	
	
	</td>
    <td width="35%" align="right">&nbsp;</td>
  </tr>
</table>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
  <tr>
    <td style="font-size: x-large;font-weight: bold;">所属仓库：<%=area.getString("title")%>&nbsp;|&nbsp;所在区域：<%=area.getString("area_name")%></td>
  </tr>
</table>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
  <form name="listForm" method="post">
    <tr> 
        <th width="36" align="center" valign="middle"  class="left-title"><input type="checkbox" name="switchCheckbox" id="switchCheckbox" value="checkbox" onClick="switchCheckboxAction()"></th>
        <th width="297"  class="left-title">位置条码</th>
        <th width="176" align="left"  class="right-title" style="vertical-align: center;text-align:center;">类别</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">位置编号</th>
        <th width="165"  class="right-title" style="vertical-align: center;text-align: center;">&nbsp;</th>
    </tr>

    <%
		for ( int i=0; i<rows.length; i++ )
		{
	%>
    <tr  > 
      <td height="80" align="center" valign="middle"   ><input type="checkbox" id="switchCheckboxSlcid" name="switchCheckboxSlcid" value="<%=rows[i].get("slc_id",0l)%>">      </td>
      <td height="80" valign="middle"  style='word-break:break-all;padding-top:10px;padding-bottom:10px;font-size: 20px;' ><%=rows[i].getString("slc_position_all")%></td>
      <td align="center" valign="middle" style="line-height:20px;font-size: 20px;"><%=lk.getLoationType(rows[i].getString("slc_type"))%></td>
      <td align="center" valign="middle" style="font-size: 20px;"><%=rows[i].getString("slc_position") %></td>
      <td align="center" valign="middle" nowrap="nowrap"><input name="Submit3" type="button" class="short-short-button-mod" value="修改" onClick="modLocationCatalog(<%=rows[i].get("slc_id",0l)%>)">
&nbsp;&nbsp;<input name="Submit32" type="button" class="short-short-button-del" value="删除" onClick="delLocationCatalog(<%=rows[i].get("slc_id",0l)%>,'<%=rows[i].getString("slc_position_all")%>')">&nbsp;&nbsp;&nbsp;<input type="button" class="short-short-button-print" onClick="printBarcode(<%=rows[i].get("slc_id",0l)%>)" value="打印"/>
	  </td>
    </tr>
    <%
		}
	%>
  </form>
</table>

<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" action="location_catalog_list.html">
    <input type="hidden" name="p">
	<input type="hidden" name="area_id" value="<%=area_id%>"/>
	<input type="hidden" name="position" value="<%=position%>"/>
	<input type="hidden" name="type" value="<%=type%>"/>
  </form>
  <tr>
    <td height="28" align="right" valign="middle" class="turn-page-table"><%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>">
        <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO">
    </td>
  </tr>
</table>

<form action="location_catalog_list.html" name="search_form" method="post">
	<input type="hidden" name="area_id" value="<%=area_id%>"/>
	<input type="hidden" name="position"/>
	<input type="hidden" name="type"/>
</form>
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/addLocationCatalog.action" name="add_location_catalog_form" method="post">
	<input type="hidden" name="area_id" value="<%=area_id%>"/>
	<input type="hidden" name="position"/>
	<input type="hidden" name="type"/>
	<input type="hidden" name="slc_x"/>
	<input type="hidden" name="slc_y"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>?area_id=<%=area_id%>"/>
</form>

<form name="del_location_catalog_form" method="post">
	<input type="hidden" name="slc_id"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>?area_id=<%=area_id%>"/>
</form>

<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/storage_location/modLocationCatalog.action" method="post" name="mod_location_catalog_form">
	<input type="hidden" name="area_id" value="<%=area_id%>"/>
	<input type="hidden" name="slc_id"/>
	<input type="hidden" name="position"/>
	<input type="hidden" name="type"/>
	<input type="hidden" name="slc_x"/>
	<input type="hidden" name="slc_y"/>
	<input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURI(request)%>?area_id=<%=area_id%>"/>
</form>

<form target="_blank" method="get" name="print_barcode">
	<input type="hidden" id="slc_id" name="slc_id"/>
	<input type="hidden" id="copies" name="copies" value="1"/>
	<input type="hidden" id="print_range_width" name="print_range_width"/>
	<input type="hidden" id="print_range_height" name="print_range_height"/>
	<input type="hidden" id="printer" name="printer"/>
	<input type="hidden" id="paper" name="paper"/>
</form>

<script type="text/javascript">
	function printBarcode(slc_id)
	{
		var lable_template_id = 100002;
		if(lable_template_id !=0)
		{ 	
			var printer;
			var paper;
			var print_range_width;
			var print_range_height;
			var template_path;
			
			var para = "lable_template_id="+lable_template_id;
			$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/lableTemplate/getDetailLableTemplateJSON.action',
					type: 'post',
					dataType: 'json',
					timeout: 60000,
					cache:false,
					data:para,
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(data){
	
						if (data["close"])
						{
							printer = data.printer;
							paper = data.paper;
							print_range_width = data.print_range_width;
							print_range_height = data.print_range_height;
							template_path = data.template_path;
													
			
							printLabel(slc_id,printer,paper,print_range_width,print_range_height,template_path);
						}
					}
				});
		}
		else
		{
			alert("请选择正确的标签模板！！");
		}
	}
	
	function printLabel(slc_id,printer,paper,print_range_width,print_range_height,template_path)
	{
		$.ajax({
					url: '<%=ConfigBean.getStringValue("systenFolder")%>administrator/'+template_path,
					type: 'post',
					dataType: 'html',
					timeout: 60000,
					cache:false,
					data:
					{
						slc_id:slc_id,
						printer:printer,
						paper:paper,
						print_range_width:print_range_width,
						print_range_height:print_range_height,
						template_path:template_path,
						copies:1
					},
					
					beforeSend:function(request){
					},
					
					error: function(e){
						alert(e);
						alert("提交失败，请重试！");
					},
					
					success: function(data)
					{
						var printer = visionariPrinter.SELECT_PRINTER();
						if(printer !=-1)
						{
							visionariPrinter.PRINT_INIT(0,0,print_range_width+"mm",print_range_height+"mm","位置标签");
							visionariPrinter.ADD_PRINT_HTM(0,0,"100%","100%","<body leftmargin='0' topmargin='0'>"+data+"</body>");
							visionariPrinter.SET_PRINT_COPIES(1);
							//visionariPrinter.PREVIEW();
							visionariPrinter.PRINT();
						}
					}
				});
	}
</script>
</body>
</html>
