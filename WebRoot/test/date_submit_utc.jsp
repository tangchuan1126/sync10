<!doctype html>
<%@ page contentType="text/html;charset=utf-8"%>
<html>
<head>
	<meta charset="UTF-8" />
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/sunny/jquery-ui.css" />
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
	<script>
		$(function(){
			$('input[name="myDate"]').datepicker({ dateFormat: "yy-mm-dd" });
			$('form:has(input[name="myDate"])').submit(function(evt){
				var myDate = $( 'input[name="myDate"]' ).datepicker( "getDate" );   //利用日期控件的方法获取日期对象
			        $(evt.currentTarget).append($('<input type=hidden name=myDateUTC  value='+myDate.getTime()+' />'));
			        return true;
			});
		});
	</script>
</head>
<body>
<h1>服务器时区是：<%= System.getProperty("user.timezone") %></h1>
<h1>浏览器时区是：
<script>
//时间偏移和时区是相反的符号
document.write((new Date().getTimezoneOffset() > 0 ? "-" : "+") +Math.abs(new Date().getTimezoneOffset() / 60) + " hours");
</script>
</h1>
<hr/>
<%  if(request.getMethod().equalsIgnoreCase("POST")){  %>
	您提交的参数是：<pre><% for(java.util.Map.Entry<String,String[]> me: request.getParameterMap().entrySet() ) {%>
<%= me.getKey() %>: <%= me.getValue()[0] %><% if(me.getValue()[0].matches("\\d+")) { %>
	此时间戳还原为服务器时间是：<%= new java.util.Date(Long.valueOf(me.getValue()[0])) %>
<% } } %>
</pre>
	
<%  } else { %>
	<form method="POST" >
		
		<input type="text" name="myDate" />
		
		<input type="submit" />
	
	</form>
<% } %>
</body>
</html>