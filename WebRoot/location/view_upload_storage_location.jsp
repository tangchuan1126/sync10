<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
String permitFile = "csv";
String filePath = "/location/storage_location_file/";

TUpload upload = new TUpload();
			
upload.useOrgName();
upload.setRootFolder(filePath);
upload.setPermitFile(permitFile);

int flag = upload.upload(request);
String msg;
boolean haveError = false;

if (flag==2)
{
	msg = "只能上传:"+permitFile;

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else if (flag==1)
{
	msg = "上传出错";

	out.println("<script>");
	out.println("alert('"+msg+"')");
	out.println("history.back();");
	out.println("</script>");
}
else
{
	msg = upload.getFileName();
	
	String csvRow[] = productMgr.getCsv(filePath+msg);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提交商品定位信息</title>
<link href="../comm.css" rel="stylesheet" type="text/css">
<style>
.form
{
	padding:0px;
	margin:0px;
}

</style>
<script>
function inp()
{
	document.product_form.submit();
	
}
</script>
</head>
<body >
<table width="70%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="63%" style="font-size:14px;font-weight:bold">当前文件:<span style="color:#FF0000"><%=msg%></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 记录数：<span style="color:#FF0000"><%=csvRow.length%></span></td>
    <td width="37%" align="right" valign="middle" style="font-size:14px;font-weight:bold">&nbsp;</td>
  </tr>
</table>
<br />
<table width="70%" height="22" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
<form action="<%=ConfigBean.getStringValue("systenFolder")%>action/administrator/product/addStorageLocation.action" method="post" name="product_form">
<input type="hidden" name="csv_name" value="<%=filePath+msg%>">
<input type="hidden" name="backurl" value="<%=upload.getRequestRow().getString("backurl")%>">
  <tr >
    <td width="47%" height="30" bgcolor="#eeeeee" style="font-size:13px;font-weight:bold">商品条码</td>
    <td width="14%" align="center" bgcolor="#eeeeee" style="font-size:13px;font-weight:bold">数 量</td>
    <td width="39%" align="center" bgcolor="#eeeeee" style="font-size:13px;font-weight:bold">存放位置</td>
  </tr>
<%
String bgcolor="";
String fontcolor  = "";

for (int i=0; i<csvRow.length; i++)
{
	if (csvRow[i].trim().equals("")||csvRow[i].trim().split(",").length!=3)
	{
		continue;
	}
	
	if ((i+1)%2==0)
	{
		bgcolor = "bgcolor='#f8f8f8'";
		fontcolor = "color:#000000";
	}
	else
	{
		bgcolor = "bgcolor='#ffffff'";
		fontcolor = "color:#000000";
	}

	String cols[] = csvRow[i].split(",");

%>
  <tr <%=bgcolor%>>
    <td width="47%" height="30" style="font-size:13px;<%=fontcolor%>"><%=cols[0]%></td>
    <td width="14%" align="center" style="font-size:13px;<%=fontcolor%>"><%=cols[1]%></td>
    <td width="39%" align="center" style="font-size:13px;<%=fontcolor%>"><%=cols[2]%></td>
  </tr>

<%
}

}
%>
</form>
</table>
<br />
<table width="70%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%" align="right" valign="middle">
	<%
	if (haveError)
	{
	%>
     
      <input name="Submit" type="button" class="short-button" onClick="history.back()" value="   返 回  " >
    <%
	}
	else
	{
	%>
	
	<input name="Submit" type="button" class="long-button" id="in" onClick="inp()" value="   确认提交  ">
	<%
	}
	%>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
