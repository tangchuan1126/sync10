<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%
long cid = StringUtil.getLong(request,"cid");
String cmd = StringUtil.getString(request,"cmd");

long ps_id = 100043l;

PageCtrl pc = new PageCtrl();
pc.setPageNo(StringUtil.getInt(request,"p"));
pc.setPageSize(30);

DBRow storageLocations[] ;

if (cmd.equals("compare"))
{
	storageLocations = productMgr.getIncorrectStorageByCidPsid( cid, ps_id, pc);
}
else
{
	storageLocations = productMgr.getAllStorageLocation(pc);
}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>美国仓库商品定位</title>
<link href="comm.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../common.js"></script>

<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>

<script src="js/zebra/zebra.js" type="text/javascript"></script>
<link href="js/zebra/zebra.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function upload()
{
	if (document.upload_form.file.value=="")
	{
		alert("请选择上传的商品定位信息");
	}
	else if ( checkFileName(document.upload_form.file.value) )
	{
		alert("文件名不能包含中文");
		
	}
	else
	{
		document.upload_form.submit();
	}
}

function includeChinese(str) 
{
	var rxp=/^[\u4e00-\u9fa5]+$/g;

	return rxp.test(str);
}

function checkFileName(name)
{
	var a = name.split("\\");
	var aa = a[a.length-1].split(".");
	
	return(includeChinese(aa[0]));
}
//-->
</script>

<style>
form
{
	padding:0px;
	margin:0px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="onLoadInitZebraTable()">
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td class="page-title">商品定位   </td>
  </tr>
</table>
<br>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="52%" height="30" bgcolor="#E3F2E3" style="padding-left:10px;">
	<form action="storage_location_la.html" method="post"  name="compare_form">
	<input type="hidden" name="cmd" value="compare">
	<strong>商品分类</strong>
     &nbsp;&nbsp;
<select name="cid" id="cid">
      <option value="0">全部分类</option>
      <%
String qx="";
Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
DBRow treeRows[] = catalogMgr.getProductCatalogTree();

for ( int i=0; i<treeRows.length; i++ )
{
	if ( treeRows[i].get("parentid",0) != 0 )
	 {
	 	qx = "├ ";
	 }
	 else
	 {
	 	qx = "";
	 }
%>
      <option value="<%=treeRows[i].getString("id")%>" <%=treeRows[i].get("id",0l)==cid?"selected":""%>> <%=Tree.makeSpace("&nbsp;&nbsp;&nbsp;",treeRows[i].get("level",0))%> <%=qx%> <%=treeRows[i].getString("title")%> </option>
      <%
}
%>
    </select>
	&nbsp;&nbsp;
      <input name="Submit" type="submit" class="long-button" value="对比库存">
	  </form>
   </td>
    <td width="48%" bgcolor="#E3F2E3">&nbsp;</td>
  </tr>
  <tr> 
   
      <td height="30" colspan="2" align="right" bgcolor="#CEDEFF"  style="padding-left:10px;padding-right:10px;">
	  	  <form action="view_upload_storage_location.html" method="post" enctype="multipart/form-data" name="upload_form">
	  <input type="hidden" name="backurl" value="<%=StringUtil.getCurrentURL(request)%>">

        <input type="file" name="file">
        &nbsp; &nbsp; &nbsp;
        <input name="Submit23" type="button" class="long-button-upload" onClick="upload()" value="  上传定位信息">
        </form>      </td>
  </tr>
</table>



<br>
<br>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">
    <tr> 
        <th style="vertical-align: center;text-align: center;" class="left-title">仓库</th>
        <th align="center" class="right-title" style="vertical-align: center;text-align: center;">商品条码</th>
        <th width="10%"  class="right-title" style="vertical-align: center;text-align: center;" >实际库存</th>
        <th width="12%" style="vertical-align: center;text-align: center;" class="right-title" >系统库存</th>
        <th width="26%" style="vertical-align: center;text-align: center;" class="right-title">存放位置</th>
    </tr>

<%
for (int i=0; i<storageLocations.length; i++)
{
%>

    <tr > 
      <td   width="15%" height="60" align="center" valign="middle" style='word-break:break-all;padding-top:10px;padding-bottom:10px;' ><%=storageLocations[i].getString("title")%></td>
      <td   width="37%" align="center" valign="middle" style='word-break:break-all;font-weight:bold' ><%=storageLocations[i].getString("barcode")%></td>
      <td   align="center" valign="middle" >
	  <%
	  if (storageLocations[i].getString("quantity").equals(""))
	  {
	  	out.println("<font color=red>无</font>");
	  }
	  else
	  {
	  	out.println(storageLocations[i].getString("quantity"));
	  }
	  %>
	  
	  &nbsp;</td>
      <td   align="center" valign="middle" ><%=storageLocations[i].getString("merge_count")%>&nbsp;</td>
      <td   align="center" valign="middle" >
	  <%
	  DBRow locations[] = productMgr.getStorageLocationByBarcode(ps_id,storageLocations[i].get("pc_id",0l),null);
	  for (int j=0; j<locations.length; j++)
	  {
	  %>
			<div style="padding:3px;color:#CC0000;font-weight:bold;font-size:13px;"><%=locations[j].getString("position")%> : <spab style="color:#0000FF"><%=locations[j].get("quantity",0f)%></span></div>

	  <%
	  }
	  %>
&nbsp;
      </td>
    </tr>
<%
}
%>

</table>
<br>
<table width="98%" border="0" align="center" cellpadding="3" cellspacing="0">
  <form name="dataForm" method="post">
          <input type="hidden" name="p">
<input type="hidden" name="cmd" value="<%=cmd%>">
<input type="hidden" name="cid" value="<%=cid%>">
  </form>
        <tr> 
          
    <td height="28" align="right" valign="middle"> 
      <%
int pre = pc.getPageNo() - 1;
int next = pc.getPageNo() + 1;
out.println("页数：" + pc.getPageNo() + "/" + pc.getPageCount() + " &nbsp;&nbsp;总数：" + pc.getAllCount() + " &nbsp;&nbsp;");
out.println(HtmlUtil.aStyleLink("gop","首页","javascript:go(1)",null,pc.isFirst()));
out.println(HtmlUtil.aStyleLink("gop","上一页","javascript:go(" + pre + ")",null,pc.isFornt()));
out.println(HtmlUtil.aStyleLink("gop","下一页","javascript:go(" + next + ")",null,pc.isNext()));
out.println(HtmlUtil.aStyleLink("gop","末页","javascript:go(" + pc.getPageCount() + ")",null,pc.isLast()));
%>
      跳转到 
      <input name="jump_p2" type="text" id="jump_p2" style="width:28px;" value="<%=StringUtil.getInt(request,"p")%>"> 
      <input name="Submit22" type="button" class="page-go" style="width:28px;padding-top:0px;" onClick="javascript:go(document.getElementById('jump_p2').value)" value="GO"> 
    </td>
        </tr>
</table> 
<br>
</body>
</html>
