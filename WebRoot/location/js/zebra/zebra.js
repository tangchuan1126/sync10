// 目前是所有的页面都采用新的样式
// 如果需要改成只在需要的页面的显示新的样式需要把
//	1.在新的页面上的table上添加isNeed='true'
//	2.然后把Js中的if(){}else{}中调换里面的内容;
//	3.if()中的条件改成(isNeed == 'true')
//	已经处理td中再次包含table的情况
function onLoadInitZebraTable(){
	
		var table = $(".zebraTable");
		var isNeed = table.attr("isNeed");
		if(isNeed == "true"){
			// 需要添加新的样式 table 添加属性isNeed == "true";
			var trs = $("tr",table);
			 
				trs.each(function(index){
					  if(index > 0){
					 	var _this = $(this);
					 	$("td",_this).css("border","none");
					 	var length = $("td",_this).length;
					 	if($("td",_this).attr("colspan")){length = $("td",_this).attr("colspan") * 1};
					 
						 $("<tr class='split'><td colspan='"+length+"'></td></tr>").insertAfter(_this);
					  }
					 
				})
			 
			$(".zebraTable tr:not(tr.split)").mouseover(function() {
						$(this).addClass("over");
				}
			).mouseout(function() {$(this).removeClass("over");});
 
		}else{
			// 按照以前的样式
			$(".zebraTable tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
			$(".zebraTable tr:even").addClass("alt");
		}
		
	}