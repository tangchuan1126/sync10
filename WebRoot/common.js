
function go(p){
	document.dataForm.p.value = p;
	document.dataForm.submit();
}

function checkEmail(email){
	var reg = /^([a-zA-Z0-9_-])+.*@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
	return (email.match(reg));
}

function isNumber(variable){
	var reg = /^\d+$/;
	return (reg.test(variable));
}

var unitConverter = function(){
	
	var WeightUOMKey = {"LBS":2,"KG":1};
	
	var LengthUOMKey = {"DM":3,"CM":2,"MM":1,"INCH":4};
	
	return {
		
		/**
		 * 暂时只适用于LBS和Kg
		 * 1KG:1千克=2.2046226218488磅
		 * 2LBS:1磅=0.45359237千克
		 */
		weightUnitConverter:function(weight,fromUom,toUom){
			
			weight = Number(weight);
			fromUom = Number(fromUom);
			toUom = Number(toUom);
			
			var result = 0;
			
			switch(fromUom){
				
				case WeightUOMKey.LBS : 
					result = this.widthLbsConverterOthers(weight, toUom);
					break;
				case WeightUOMKey.KG :
					result = this.widthKgConverterOthers(weight, toUom);
					break;
			}
			
			//保留三位
			return result.toFixed(3);
		},
		widthLbsConverterOthers:function(weight, toUom){
			
			var result = 0;
			 
			 switch (toUom) {
			 
				case WeightUOMKey.LBS :
					result = weight;
					break;
				case WeightUOMKey.KG :
					result = 0.4536 * weight;
					break;
			}
			return result;
		},
		widthKgConverterOthers:function(weight, toUom){
			
			var result = 0;
			 
			switch (toUom) {
			 
				case WeightUOMKey.KG :
					result = weight;
					break;
				case WeightUOMKey.LBS :
					result = 2.2046 * weight;
					break;
			}
			 
			return result;
		},
		lengthUnitConverter:function(length, fromUom, toUom){
			
			length = Number(length);
			fromUom = Number(fromUom);
			toUom = Number(toUom);
			
			var result = 0;
			 
			switch (fromUom){
		 
		 		case LengthUOMKey.DM:
		 			result = this.lengthDmConverterOthers(length, toUom);
		 			break;
		 		case LengthUOMKey.CM:
			 		result = this.lengthCmConverterOthers(length, toUom);
			 		break;
		 		case LengthUOMKey.MM:
			 		result = this.lengthMmConverterOthers(length, toUom);
			 		break;
		 		case LengthUOMKey.INCH:
			 		result = this.lengthInchConverterOthers(length, toUom);
			 		break;
		}
		 
		return result.toFixed(2);
		},
		lengthDmConverterOthers:function(length, toUom){
			
			 var result = 0;
			 
			 switch (toUom) {
			 	case LengthUOMKey.DM:
					result = length;
					break;
				case LengthUOMKey.CM:
					result = length * 10;
					break;
				case LengthUOMKey.MM:
					result = length * 100;
					break;
				case LengthUOMKey.INCH:
					result = length * 10 / 2.54;
					break;
				default:
					break;
			}
			return result;
		},
		lengthCmConverterOthers:function(length, toUom){
			 
			var result = 0;
			
			switch (toUom) {
			 	case LengthUOMKey.CM:
					result = length;
					break;
				case LengthUOMKey.DM:
					result = length / 10;
					break;
				case LengthUOMKey.MM:
					result = length * 10;
					break;
				case LengthUOMKey.INCH:
					result = length / 2.54;
					break;
			}
			
			return result;
		},
		lengthMmConverterOthers:function(length, toUom){
			
			 var result = 0;
			 
			 switch (toUom) {
			 	case LengthUOMKey.MM:
					result = length;
					break;
				case LengthUOMKey.DM:
					result = length / 100;
					break;
				case LengthUOMKey.CM:
					result = length / 10;
					break;
				case LengthUOMKey.INCH:
					result = length / 10 / 2.54;
					break;
			}
			return result;
		},
		lengthInchConverterOthers:function(length, toUom){
			
			 var result = 0;
			 
			 switch (toUom) {
			 	case LengthUOMKey.INCH:
					result = length;
					break;
				case LengthUOMKey.DM:
					result = length / 10 * 2.54;
					break;
				case LengthUOMKey.CM:
					result = length * 2.54;
					break;
				case LengthUOMKey.MM:
					result = length * 10 * 2.54;
					break;
			}
			 
			return result;
		}
	}
}();