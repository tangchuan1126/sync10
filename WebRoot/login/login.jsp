<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%>
<%
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);
adminMgr.isRightAdminPath(response,request);

String cookieAccount = StringUtil.getCookie(request,"account");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Login <%=systemConfig.getStringConfigValue("webtitle")%></title>

<link rel="Shortcut Icon" href="/Sync10/favicon.png"  type="image/x-icon" /> 
<link rel="Bookmark" href="/Sync10/favicon.png" type="image/x-icon" />  

<!-- Custom CSS -->
<link href="/Sync10-ui/pages/Examples/css/sb-admin-2.css" rel="stylesheet">
<link href="/Sync10-ui/lib/animate.css" rel="stylesheet"> 


<style>

.OkMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #00BE00;
	background:#E6FFE6 url(imgs/standard_msg_ok.gif) 5px 5px no-repeat;
	color:#000;
	height:18px;
} 

.WarningMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #00A8FF;
	background:#E2F5FF url(imgs/standard_msg_warning.gif) 5px 5px no-repeat;	
	color:#000;
	height:18px;
}

.ErrorMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #F60;
	background:#FFF2E9 url(imgs/standard_msg_error.gif) 5px 5px no-repeat;	
	color:#000;
	height:34px;
}

@font-face { 
	font-family: C39HrP48DmTt; 
	font-weight: bold; 
	src: url(code39p48.eot);
}


	
</style>
<link href="/Sync10-ui/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="/Sync10-ui/bower_components/jquery/dist/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="aui-artDialog/css/ui-dialog.css" />
<script src="aui-artDialog/dist/dialog.js" type="text/javascript"></script>


<link href="/Sync10-ui/bower_components/fullpage.js/jquery.fullPage.css" rel="stylesheet">
<script src="/Sync10-ui/bower_components/fullpage.js/jquery.fullPage.js"></script>

<script language="JavaScript" type="text/JavaScript">
function login(){
	
	theForm = document.login_form;
	
	if ( theForm.account.value=="" ){
		
		document.getElementById("logining_info").innerHTML = "Enter account";
		//document.getElementById("logining_info").className = "ErrorMsg";
		/* $("#logining_info").removeClass().addClass("form-control").addClass("ErrorMsg");
		$("#logining_info").show();
		$("#logining_info").attr("style", "letter-spacing: 2px;"); */
		theForm.account.focus();
		$("#account").addClass("warning");
		
	}else if ( theForm.pwd.value=="" ){
		
		/* document.getElementById("logining_info").innerHTML = "Enter password";
		//document.getElementById("logining_info").className = "ErrorMsg";
		$("#logining_info").removeClass().addClass("form-control").addClass("ErrorMsg");
		$("#logining_info").show();
		$("#logining_info").attr("style", "letter-spacing: 2px;"); */
		theForm.pwd.focus();
		$("#pwd").addClass("warning");
		
	}
	/*else if ( theForm.licence.value=="" ){
		
		document.getElementById("logining_info").innerHTML = "Enter verification code";
		document.getElementById("logining_info").className = "ErrorMsg";
		theForm.licence.focus();
		
	}*/else{
		
		var remeber = document.getElementById("remeber2").checked;
		
		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/admin/AccountLoginMgrAction.action',
			type: 'post',
			dataType: 'json',
			timeout: 60000,
			cache:false,
			data:"account="+theForm.account.value+"&pwd="+theForm.pwd.value+"&licence="+theForm.licence.value+"&remeber="+remeber,
			beforeSend:function(request){
				/* $("#logining_info").removeClass().addClass("form-control").addClass("WarningMsg");
				$("#logining_info").html("Login..."); */
			},
			error: function(){
				alert("Network error, try again!");
			},
			success: function(data){
				
				if(data.success){
					
					/* $("#logining_info").addClass("OkMsg");
					$("#logining_info").html("Login..."); */
					/* $("#logining_info").removeClass().addClass("form-control").addClass("OkMsg");
					$("#logining_info").html("Login..."); */
					window.location = "../administrator/index.html";
					
				} else{
					/* $("#logining_info").removeClass().addClass("form-control").addClass("ErrorMsg");
					$("#logining_info").html(data.errorInfo);
					$("#logining_info").show();
					$("#logining_info").attr("style", "letter-spacing: 2px;"); */
					
					if("Account or password error" == data.errorInfo){
						$("#account").addClass("warning");
						$("#pwd").addClass("warning");
					}else{
						var d = dialog({
						    title:'ERROR',
						    content: ''+data.errorInfo,
							width: 260,
						    height: 100
						});    
						d.showModal();
					}
					$("#licenceImg").attr("src","<%=ConfigBean.getStringValue("systenFolder")%>loginLicence");
				}
			}
		});
	}
	
	return false;
}


if (top.location !== self.location){
	
	top.location = self.location;
}
/* //连接状态改变的事件  
function onConnect(status) {   
    if (status == Strophe.Status.CONNFAIL) {  
        
    } else if (status == Strophe.Status.AUTHFAIL) {  
         
    } else if (status == Strophe.Status.DISCONNECTED) {  
         
    } else if (status == Strophe.Status.CONNECTED) {
    	$.cookie('jid',this.jid, { path: '/Sync10/' });
    	$.cookie('rid',this.rid,{ path: '/Sync10/' });
    	$.cookie('sid',this.sid,{ path: '/Sync10/' });
    	window.location = "../administrator/index.html";

    }  
} */
</script>
<style>
.l-bg {
	background-attachment: scroll;
	background-image: url(imgs/l_bg.jpg);
	background-repeat: repeat-x;
	background-position: center top;
}
</style>
<style type="text/css">
<!--
.lt {
	font-size: 13px;
	font-weight: bold;
	color: #2270D2;
	padding-top: 2px;
	padding-right: 12px;
	padding-bottom: 0px;
	padding-left: 0px;
}
td {
	font-size: 12px;
	color: #666666;
}
-->
</style>
<style type="text/css">

.text-input {
	background-color: #FFFFFF;
	height: 20px;
	width: 125px;
	border: 1px solid #b2b2b2;
	color: #DD0000;
}
.text-input2 {
	background-color: #FFFFFF;
	height: 20px;
	width: 70px;
	border: 1px solid #b2b2b2;
	color: #DD0000;
}
.login-b {
	background-attachment: fixed;
	background: url(imgs/login_b.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 70px;
	border: 0px solid;
}

.mb15 {
    margin-bottom: 15px;
}

.switch {
	float: right;
	z-index: 1;
	margin-top: -10;
	margin-right: -15;
	cursor: pointer;
}

.qrcode {
	margin-top: 11px;
	margin-bottom: 41px;
}

.info {
	display: inline-block;
	text-align: center;
	/* font-size: 12px;
	color: #161FC2; */
	background-color: #e5e5e5;
	border-color: #bbbbbb;
	border-style: solid;
	border: 1px;
	color: #333333;
	font-size: 12px;
	height: 25px;
	padding: 5px;
	min-width: 176px !important;
	max-width: 176px;
	border-radius: 20px;
}

ol#flowBg > li[class="active"] {
	background-color: #19F318;
}

.warning {
	border: 2px solid red;
}

</style>

</head>
<body bgcolor="999999" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><!-- onLoad="document.getElementById('account').focus();" -->


<script type="text/javascript">
	function hidePrompt(){
		setTimeout('$("#logining_info").hide();', 1500);
	}
	
	function switchWindow(obj){
		var status = $(obj).attr("data-status");
		console.log("status: "+status);
		if(status==1){
			$("#signWindow").hide();
			$("#qrcodeWindow").fadeIn("slow");
			$("#title").html("Please Scan");
			$(obj).attr("data-status", "2").attr("src", "imgs/a-1.png").attr("title", "Sign In").attr("alt", "Sign In");
		}
		
		if(status==2){
			$("#qrcodeWindow").hide();
			$("#signWindow").fadeIn("slow");
			$("#title").html("Please Sign In");
			$(obj).attr("data-status", "1").attr("src", "imgs/b.png").attr("title", "Download App").attr("alt", "Download App");
		}
	}
	
	$(document).remove("keydown").bind("keydown",function(e){
        var curKey = e.which; 
        if(curKey == 13){ 
        	login();
        }
	});
	
	var bgImgs = new Array();
	bgImgs[0] = "imgs/bg-0.jpg";
	bgImgs[1] = "imgs/bg-1.jpg";
	bgImgs[2] = "imgs/bg-2.jpg";
	
	function chgBg(index,url){
		if(url){
			$("body").attr("style", "background-image:url("+url+"); background-size: 100%;");
		}else{
			$("body").attr("style", "background-image:url("+bgImgs[index]+"); background-size: 100%;");
		}
		
		$("#flowBg li[class='active']").removeClass("active");
		$("#flowBg li").eq(index).addClass("active");
	}
	
	//chgBg(0,bgImgs[0]);
	
	var curIndex = 0;
	//setInterval("next()", 5000);
	
	function next(){
		curIndex = (curIndex+1) % bgImgs.length;
		chgBg(curIndex, bgImgs[curIndex]);
	}
	
	$(document).ready(function(){
		$("#flowBg li").each(function(index, obj){
			$(obj).unbind("click").bind("click", function(e){
				curIndex = index;
				chgBg(index);
			});
		});
	});
</script>



	

	<!-- <div id="switchCtn" class="slide" data-ride="carousel">
		<ol id="flowBg" class="carousel-indicators" style="position: fixed;bottom: 0px;">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		</ol>
	</div> -->

 <style>
 	
 	ul{margin:0;padding:0;}
 	li{  list-style: none;}
 	/*导航*/
 	.header{
 	
 		  position: fixed;
		  width: 100%;
		  top: 0;
		  left: 0;
		  z-index: 999;
		  /*border-bottom: 1px #eeeeee solid;*/
  		  background-color: rgba(255,255,255,.0); 
 	}
 	.mainNav{
 		width:1170px; 
 		margin:0 auto;
 		height:80px;
 		padding-right: 15px;
  		padding-left: 15px;
 		  
 	}
 	.osologo{
 		line-height: 80px;
	  height: inherit;
 	}
 	.mainMenu{
 		line-height: 80px;
	  height: inherit; 
 	}
 	
 	.mainMenu ul li{
 		float:left;
 		width: 120px;
  		font-size: 1.2em;
  		text-align: left;
 	}
 	
 	.mainMenu ul li a{
 		color:#000;
 		text-decoration: none;
 	}
 	
 	.mainMenu ul li.active,.mainMenu ul li a:hover{
 		color:#2489c5;
 		cursor: pointer;
 	}
 	
 	/*登陆*/
 	#loginCtn{
 		position: fixed;
		top: 120px;
		position:fixed;
		margin:auto;
		left:0;
		right:0;
		bottom:0;
		z-index: 999; 
 	}
 	
 	.panel-heading,.panel{
 		background: rgba(255,255,255,.7);  
 	}
 	
 	.panel-heading {background-color: #fff !important;} 
 	
 	/*快速选择*/
	#fp-nav ul li{height:20px;}
 	#fp-nav ul li span{ 
 		  border: 1px #cccccc solid !important;
		  width: 10px !important;
		  height: 10px !important;
		  background: rgba(255,255,255,.9) !important;
 	}
 	
 	#fp-nav ul li span:hover{
 		  margin:-2px 0 0 -2px !important;  
 	}
 	
 	#fp-nav ul li .active span{
 		background:#2489c5 !important;
 		border:#0cf !important; 
 		width: 12px !important;
		height: 12px !important;
 		margin:-2px 0 0 -3px !important;  
 	}
 	
 	#section0{
		background:url("imgs/bg03.jpg");
		background-size:100%;
	}
	
	#section0 .intro img{
		width:35%;  
		margin:100px 0 0;
	}
	
	#section1 .intro img{
		width:43%;
		margin: 30px 0 0 100px;
	}
	
	#section2 .intro img{
		
		width:50%;
  		margin: 30px 0 0 50px;
	}
	
	#section1{
		background:url("imgs/bg02.jpg");
		background-size:100%;
	}
	#section2{
		background:url("imgs/bg01.jpg");
		background-size:100%;
	}
 	
 	#login_form{
 		margin-top:15px;
 	}
 </style>
 
 <script type="text/javascript">
	$(document).ready(function() {
		
		$('#fullpage').fullpage({
			anchors: ['firstPage', 'secondPage', '3rdPage'],
			sectionsColor: ['#fff', '#1BBC9B', '#7E8F7C'],
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['', '', ''],
			responsive: 900,
			afterLoad: function(anchorLink, index){				
			 	if(index==1){
					$("#section0 .intro").show().removeClass("bounceOutDown").addClass("bounceInRight");
				} 
			 	if(index==2){
					$("#section1 .intro").show().removeClass("bounceOutDown").addClass("bounceInRight");
				} 
			 	if(index==3){
					$("#section2 .intro").show().removeClass("bounceOutDown").addClass("bounceInRight");
				}   
   			},
			onLeave: function(index, nextIndex, direction){
			 	if(index==1){
					$("#section0 .intro").removeClass("bounceInRight").addClass("bounceOutDown");
				} 
			 	if(index==2){
					$("#section1 .intro").removeClass("bounceInRight").addClass("bounceOutDown"); 
				} 
			 	if(index==3){
					$("#section2 .intro").removeClass("bounceInRight").addClass("bounceOutDown");
				}
			}
		});
		
		var hi = $("#section0").height();
		//解决无法获取当前页面高度的bug.
		if(hi >300){
			var h = $(window).height(); 
			var imgH = 500;
			$("#section0").height(h);
			$("#section1").height(h);
			$("#section2").height(h);
			
			$("#section0 .fp-tableCell").height(imgH);
			$("#section1 .fp-tableCell").height(imgH);
			$("#section2 .fp-tableCell").height(imgH);
			
		}
	});
</script>
 
 <div class="container">
	 <div class="header">
	 	<div class="mainNav">
	 		<div class="osologo pull-left">&nbsp;<!-- <img src="imgs/logo.png"> --></div> 
	 		<div class="mainMenu pull-right">
	 			<ul>
	 				<li class="active" style="width:126px;">Index</li>
	 				<li style="width:114px;"><a href="/Sync10/App.html" target="_blank">Phone</a></li> 
	 				<li style="width:120px;text-align:right"><a href="/Sync10/App.html" target="_blank">Large Screen</a></li>
	 			</ul>
	 		</div>
	 	</div>
	 </div>
</div>


<div id="loginCtn" class="container">
    <div class="row">
        <div class="col-md-4 pull-right">
            <div class="login-panel panel panel-default" style="margin-top: 20%;min-width: 358px;">
                <!-- <div class="panel-heading">
                    
                    <img src="imgs/logo.png" style="/* margin-top:-10px; margin-left: -15px; */">
                    <img src="imgs/b.png" class="switch" data-status="1" title="Download App" alt="Download App" onclick="switchWindow(this);">
                </div> --> 
                
                <div id="signWindow" class="panel-body" style="padding:15px 25px 5px 25px;min-height:280px;">
                	<img src="imgs/new_logo.png">
                	  
                    <form role="form" name="login_form" id="login_form" onSubmit="return login()">
                        <fieldset>
                            <span id="logining_info" style="letter-spacing: 2px; display:none" class="form-control"></span>
                            <div class="input-group mb15">
                            	<span class="input-group-addon">
						<i class="glyphicon glyphicon-user"></i>
					</span>
                                <input class="form-control" placeholder="Account" name="account" type="text" id="account" value="<%=cookieAccount==null?"":cookieAccount%>" autofocus="" onfocus="hidePrompt();" onchange="javascript: $(this).removeClass('warning'); $('#pwd').removeClass('warning');">
                            </div>
                            <div class="input-group mb15">
                             <span class="input-group-addon">
						<i class="glyphicon glyphicon-lock"></i>
					</span>
                                <input class="form-control" placeholder="Password" name="pwd" type="password" id="pwd" value="" onfocus="hidePrompt();" onchange="javascript: $(this).removeClass('warning'); $('#account').removeClass('warning');">
                            </div>
                            <div class="input-group mb15">
                                <span class="input-group-addon">
						<i class="glyphicon glyphicon-check"></i>
					</span>
                                <input class="form-control" placeholder="Verification" name="licence" type="text" id="licence5" style="float:left;width:70%" onfocus="hidePrompt();"> 
                                <img id="licenceImg" name="licenceImg" src="<%=ConfigBean.getStringValue("systenFolder")%>loginLicence" border="0" style="display:inline;height:34px;width:29%;margin-left:2px">
                            </div>
                            
                            <a href="javascript:void(0);" class="btn btn-lg btn-success btn-block" onclick="login();">Login</a>
                        </fieldset>
                    </form>
                    <label style="font-weight:normal;">
                        <input name="remember" type="checkbox" value="Remember Me" id="remeber2" <%=cookieAccount==null ? " ":"checked"%> onfocus="hidePrompt();" style="float:left;">Remember Me
                    </label>
                    <a href="http://download.firefox.com.cn/releases/webins/3.6/zh-CN/Firefox-3.6.17.exe" style="float: right;margin-bottom: 0px;padding-bottom: 0px;text-decoration: underline;color: #161FC2;">Please login for Firefox</a>
                </div>
                <div id="qrcodeWindow" class="panel-body" style="display:none;min-height:280px; vertical-align: middle; text-align: center;">
                	<img src="imgs/barcode.png" class="qrcode"/>
                	<span class="info">Use Mobile To Scan QRCode</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="fullpage">
	<div class="section " id="section0">
		<div class="intro Animated" style="display:none;" >
			<img alt="" src="imgs/img03.png">
		</div>
	</div>
	<div class="section" id="section1" >
		<div class="intro Animated" style="display:none;">
			<img alt="" src="imgs/img02.png">
		</div>
	</div>
	<div class="section" id="section2">
		<div class="intro Animated" style="display:none;">
			<img alt="" src="imgs/img01.png"> 
		</div>
	</div>
</div>
 
 
 
</body>
</html>
