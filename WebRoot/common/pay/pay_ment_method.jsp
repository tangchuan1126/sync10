<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<jsp:useBean id="currencyKey" class="com.cwc.app.api.CurrencyKey"/>
<%@page import="com.cwc.app.key.BillTypeKey"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormat"%>

 
<html>
<head>
<%
String ExecuteVertualTerminalByCustomerAction = ConfigBean.getStringValue("systenFolder")+"action/pay/ExecuteVertualTerminalByCustomerAction.action";
long billId = StringUtil.getLong(request,"bill_id");
String comeFrom = StringUtil.getString(request,"comefrom");
String clientId = StringUtil.getString(request,"client_id");
String selectIndex = StringUtil.getString(request,"select_index");
DBRow billRow = new DBRow();
DBRow[] items = null ;
DBRow userInfo = null ;
	if(comeFrom.equals("invoice")){
		billRow = billMgrZr.getBillItemByClientIdAndBillId(billId,clientId);
		items = billMgrZr.getItemsByBillId(billId);
		if(billRow.get("bill_type",0) == BillTypeKey.order ){
			DBRow forOrder = new DBRow();
			forOrder.add("name","Pay For Order ["+billRow.get("porder_id",0l)+"] Fee ");
			forOrder.add("quantity","1");
			forOrder.add("unit_price",billRow.get("order_fee",0.0d));
			forOrder.add("amount",billRow.get("order_fee",0.0d));
			items = new DBRow[1];
			items[0] = forOrder;
			
			
		}else if(billRow.get("bill_type",0) == BillTypeKey.shipping){
			if(items == null || (items != null && items.length < 1)){
				DBRow forShippingFee = new DBRow();
				forShippingFee.add("name","For Bill ["+billRow.get("bill_id",0l)+"] Shipping Fee");
				forShippingFee.add("quantity","1");
				forShippingFee.add("unit_price",0.0d);
				forShippingFee.add("amount",0.0d);
				items = new DBRow[1];
				items[0] = forShippingFee;
			}
		}
		 userInfo = waybillMgrZR.getUserNameById(billRow.get("create_adid",0l));
	}else if(comeFrom.equals("payforitem") || comeFrom.equals("payforebayitem")){
		int index = 1 ;
		String itemNameForItem = "item_name_" ;
		List<DBRow> arrayRow = new ArrayList<DBRow>();
		double totalForItem = 0.0d;
		 java.text.DecimalFormat   format   =new   java.text.DecimalFormat("#.00");  
		while(StringUtil.getString(request,itemNameForItem + index) != null && StringUtil.getString(request,itemNameForItem + index).length() > 0){
			DBRow temp = new DBRow();
			temp.add("name",StringUtil.getString(request,itemNameForItem + index));
			double quantity  = Double.parseDouble(StringUtil.getString(request,"quantity_" + index));
			double actual_price  = Double.parseDouble(StringUtil.getString(request,"amount_" + index));
			
			temp.add("quantity",quantity+"");
			temp.add("actual_price",Double.parseDouble(StringUtil.getString(request,"amount_" + index)));
			temp.add("amount",quantity * actual_price);
			arrayRow.add(temp);
			totalForItem += quantity * actual_price;
			index++;
		}
	 
		items =arrayRow.toArray(new DBRow[arrayRow.size()]);
		double total_discount = Double.parseDouble(format.format(Double.parseDouble(StringUtil.getString(request,"discount_amount_cart"))));
		double shipping_fee = Double.parseDouble(format.format(Double.parseDouble(StringUtil.getString(request,"shipping_1"))));
		
		billRow.add("rate_type",StringUtil.getString(request,"currency_code"));
		billRow.add("total_discount",total_discount);
		billRow.add("shipping_fee",shipping_fee);
		billRow.add("subtotal",Double.parseDouble(format.format(totalForItem)));
		billRow.add("save",Double.parseDouble(format.format(totalForItem-total_discount+shipping_fee)));
	 
	}else if(comeFrom.equals("payfororderfee")){
		//cmd=_cart&upload=1&business=zhanji_1331024784_biz%40vvme.com&custom=order_source%3AORDER&item_number_1=For+Order&item_name_1=For+Order%3A600838&amount_1=12&quantity_1=1&currency_code=USD
		items = new DBRow[1];
		DBRow item = new DBRow();
		item.add("name",StringUtil.getString(request,"item_name_1"));
		double amount = Double.parseDouble(StringUtil.getString(request,"amount_1"));
		item.add("actual_price",amount);
		item.add("quantity", 1);
		item.add("amount",amount);
		items[0] = item;
		billRow.add("rate_type",StringUtil.getString(request,"currency_code"));
		billRow.add("total_discount",0);
		billRow.add("shipping_fee",0);
		billRow.add("subtotal",amount);
		billRow.add("save",amount);
	}

%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice Detail</title>
<style type="text/css">
	table td.td_left{text-align:right;width:105px;height:30px;line-height:30px;padding-right:10px;}
	table td.td_right{text-align:left;width:500px;height:30px;line-height:30px;padding-left:10px;}
	input.txt{ border: 1px solid #ADC2D6;height:23px;width:200px;line-height:23px;color:black;};
 
	
	p.group{height:35px;height:35px;}
	p.group label{width:105px;display:block;clear:left;text-indent:right;}
	
	span.field{display:block;margin-left:30px;width:500px;}
   
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
	
</style>
<style type="text/css">
	 
	*{font-size:12px;color:black;font-family: Verdana,Arial,sans-serif;}
   table tr td{line-height:25px;height:25px;}
   
   .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	span.addNewSpan{display:block;width:70px;height:20px;float:right;}
	ul.addressList{list-style-type:none;margin-left:10px;}
	ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
	
	p.selectType{border:1px solid silver;background:#E6F3C5;height:35px;line-height:35px;}
	ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	span.button{-moz-transition: all 0.218s ease 0s; -moz-user-select: none;background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;color: #444444; cursor: pointer;font-size: 14px;font-weight: bold;height: 27px;line-height: 27px; min-width: 54px;
    outline: medium none;padding: 0 8px;text-align: center;display:block;margin-top:10px;
   }
   p.say {margin-top:8px;width:500px;}
   table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
   table.basic{border-collapse :collapse ;border:1px solid silver;}
   
   table.basic th{background:#e8f1fa;width:200px;font-size:12px; border:1px solid silver;}
   table.basic td{background:white;width:200px;text-indent:30px;border:1px solid silver;}
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
   ul.itemDetail{list-style-type:none;margin-left:20px;margin-top:-2px;margin-bottom:5px;}
   ul.itemDetail li{border:0px solid silver;height:14px;line-height:14px;}
   input.noborder{width:250px;border:none;border-bottom:1px solid silver;}
   ul.showUl{list-style-type:none;margin-left:20px;}
    ul.showUl li {line-height:25px;height:25px;}
</style>
<script type="text/javascript">
 
jQuery(function($){
	$("#whatisCvv").poshytip({
		className: 'tip-darkgray',
		content: "<p style='line-height:13pt;color:white;font-weight:normal;'>For MasterCard, Visa or Discover, it's the last three digits in the signature area on the back of your card.  For American Express, it's the four digits on the front of the card.</p>",
		bgImageFrameSize: 2
	});
	headProcess(2);
	var currency = '<%= billRow.getString("rate_type")%>';
	var state = '<%= billRow.getString("address_state")%>';
	if(currency.length > 0){
		 
		 $("#mc_currency option[value='"+currency+"']").attr("selected",true);
		//
		 
	}
	if('<%= comeFrom%>' === "payforitem"  || '<%= comeFrom%>' === "payfororderfee" || '<%= comeFrom%>' === "payforebayitem"){
		$("#payforItemNote").html("");
	}
 
	if('<%= comeFrom%>' != "invoice" ){
 		$("#invoiceDetails").css("display","none");
 		$("#sendto_p").css("display","none");
 		 
 		$("#payforItemNote").html("");
 		
	}else{
		 var countryCode = '<%= billRow.getString("address_country_code")%>';
		 var countryCodeNode = $("#countryCode");
		$("option[value='"+countryCode+"']",countryCodeNode).attr("selected",true);
		 getStorageProvinceByCountryCode(countryCode,state);
	}
	var addressInfo = '<%= billRow.getString("address_name")%>' + "," + '<%= billRow.getString("address_street")%>'
	+","+'<%= billRow.getString("address_state")%>' + "," + '<%= billRow.getString("address_state")%>'+"," +  '<%= billRow.getString("address_country")%>';

	$("#addressInfoStr").html(addressInfo);
})
 
function currencyCodeDiv(currency_code){
	if(currency_code==-1){
		$("#currencyCodeTd").append("<span id='currencyCodeSpan'>&nbsp;&nbsp;<input type='text' id='currencyCodeInput'/></span>");
	}else{
		$("#currencyCodeSpan").remove();
	}
}

function stateDiv(pro_id){
	if(pro_id==-1){
		$("#stateTd").append("<span id='stateSpan'>&nbsp;&nbsp;<input type='text' id='pro_id_input' /><span>");
	}else{
		$("#stateSpan").remove();
	}
}
function getStorageProvinceByCountryCode(country_code,state){ 
	$("#stateSpan").remove();
 
		$.getJSON("<%=ConfigBean.getStringValue("systenFolder")%>action/pay/GetProvinceByCountryCodeJsonNotLoginAction.action",
				{country_code:country_code},
				function callback(data)
				{ 
					 
					$("#pro_id").clearAll();
					$("#pro_id").addOption("Please select...","0");
					
					if (data!="")
					{
						$.each(data,function(i){
							if(data[i].pro_name!=undefined&&data[i].pro_name!=""&&data[i].p_code!=undefined&&data[i].p_code!="")
							{
								$("#pro_id").addOption(data[i].pro_name,data[i].p_code);
							}
						});
					}
					
					if (country_code.length>0)
					{
						$("#pro_id").addOption("Other","-1");
					}
					if(state && state.length > 0){
						var options = $("#pro_id option"); 
						for(var index = 0 , count = options.length ; index < count ; index++ ){
							var temp = $(options.get(index));
							if(temp.html() === state){
								temp.attr("selected",true);
								break;
							}

							
						}
				 }	
				}
		);
}

function validate(){
	var acct = $("#acct").val();
	var amount = $("#amount").val();
    var countryCode = $("#countryCode").val();
  
    var mc_currency = $("#mc_currency").val();
    var date_MM = $("#date_MM").val();
    var date_YYYY = $("#date_YYYY").val();
    var cvv2 = $("#cvv2").val();
    var firstName = $("#firstName").val();
    var lastName = $("#lastName").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var street = $("#street").val();
    var city = $("#city").val();
    var pro_id = $("#pro_id").val();
    var zip = $("#zip").val() ; //,lastName,email,phone,street,city,pro_id,zip
	
	if(countryCode * 1 == 0){
		showMessage("Please Select Country .","alert");
		$("#countryCode").focus();
		return false;
	}
	
	 
    
    if(isNull(acct)){
    	showMessage("Please Input Card number .","alert");
    	$("#acct").focus();
		return false;
    }
	if(isNull(amount,"amount")){
		showMessage("Please Input Amout . eg 14.45","alert");
		$("#amount").focus();
		return false;
	}
	
	if(!isNum(amount)){
		showMessage("Data Formate Error . eg 14.45","alert");
		$("#amount").focus();
		return false;
	}
	if(mc_currency * 1 == -1){
		var nodeInputValue  = $("#currencyCodeInput").val();
		if(isNull(nodeInputValue)){
			showMessage("Please Input CurrencyCode .eg USD","alert");	
			$("#currencyCodeInput").focus();
			return false;
		} 
	} 
	if(isNull(date_MM)){
		showMessage("Please Input Month .eg 08","alert");
		$("#date_MM").focus();
		return false;
	}
	if($.trim(date_MM).length != 2 || $.trim(date_MM) * 1 > 12){
		showMessage("Data Formate Error .Between 01~12","alert");
		$("#date_MM").focus();
		return false;
	}
	if(isNull(date_YYYY)){
		showMessage("Please Input Year .eg 2012","alert");
		$("#date_YYYY").focus();
		return false;
	}
	if($.trim(date_YYYY).length != 4 ){
		showMessage("Data Formate Error . eg 2012","alert");
		$("#date_YYYY").focus();
		return false;
	}
	if(isNull(cvv2)){
		showMessage("Please Input Cvv2 Value .","alert");
		$("#cvv2").focus();
		return false;
	}
	if($.trim(cvv2).length < 3){
		showMessage("The Cvv2 Value At Least 3 Length","alert");
		$("#cvv2").focus();
		return false;
	}
	//firstName,lastName,email,phone,street,city
	var varrayStr = ["firstName","lastName","email","phone","street","city","zip"];
	for(var index = 0 , count = varrayStr.length ; index < count ; index++ ){
		if(isNull($("#"+varrayStr[index]).val())){
			showMessage("Please Input "+varrayStr[index],"alert");
			$("#"+varrayStr[index]).focus();
			return false;
		}
	}
 	if(pro_id * 1 == 0){
 		showMessage("Please Select State","alert");
 		return false;
 	}
 	if(pro_id * 1 == -1 ){
		if(isNull($("#pro_id_input").val())){
			$("#pro_id_input").focus();
			showMessage("Please Input  State","alert");
			return false;
		}
 	}
 	return true;
	showMessage("success","alert");
}
function fixIntValue(_this){
 
	var node = $(_this);
	var value = node.val();
 
	var fixValue = value.replace(/[^0-9]/g,'');
	node.val(fixValue);
 
}
function isNull(value,node){
	 
	if(value){
		if( $.trim(value).length < 0 ) {
			return true;
		}else{
			return false;
		}
	}
	 
	return true;
	
}
//acct=22323232&amount=2323&cvv2=2323&firstName=323&lastName=232&email=23&phone=23&street=23&city=23&zip=23
//amount=12&currencyCode=AUD&acct=2323&cardType=Visa&cvv2=23232&expdate=082012&firstName=firstname&lastName=LastName&countryCode=US&state=CA&city=city&street=list street&zip=2323&email=329169773@qq.com&phone=32322313
function isNum(keyW)
{
	var reg=  /^(-[0-9]|[0-9]|(0[.])|(-(0[.])))[0-9]{0,}(([.]*\d{1,2})|[0-9]{0,})$/;
	return(reg.test(keyW) );
} 
function submitOn(){
	
	if(validate()){
		var form = $("#cark");
		var param = form.serialize();
		 
	 	//currencyCode,expdate,state
	 	var currencyCode = "";
	  
	 	var currencyCodeInput = $("#currencyCodeInput");
	 	if(currencyCodeInput.length > 0){
	 		currencyCode = currencyCodeInput.val();
		}else{
			currencyCode = $("#mc_currency").val();
		}
	 
		var expdate = $("#date_MM").val()+$("#date_YYYY").val();
		 
		var  state = "";
		var stateInput = $("#pro_id_input");
		if(stateInput.length > 0){
			state = stateInput.val();
		}else{
			state = $("#pro_id").val();
		}
	 
		 
		//param += "&currencyCode="+currencyCode+"&expdate="+expdate + "&state="+ state + "&cardType="+$(".paymentTypeOn").attr("target")+"&bill_id="+'<%= billId%>';
		param += "&currencyCode="+currencyCode+"&expdate="+expdate + "&state="+ state + "&bill_id="+'<%= billId%>';
		param += "&note=" + $("#note").val();
		if($("#amount").attr("disabled") + "" == "disabled"){
			param += "&amount="+$("#amount").val();
		}
		if('<%= comeFrom%>' === "payforebayitem"){
			param = (param + "&"+(payForEbayItemUrl) + "&come_from=payforebayitem");
		 }
		if('<%= comeFrom%>' === "payforitem"){
			param = (param + "&"+(payForItemUrl) + "&come_from=payforitem");
		}
 		 
		windowLock();
		$.ajax({
			url:'<%= ExecuteVertualTerminalByCustomerAction%>',
			data:param,
			dataType:'json',
			timeout: 60000,
			cache:false,
			success:function(data){
				windowUnLock();
				if(data.rs.indexOf("Failure")  != -1){
					showMessage(data.reasion,"error");
				}else{
					showMessage("Payment Success","succeed");
					window.setTimeout("showPayMentComplete()", 2000)
					 
				}
			},
			error:function(){
				windowUnLock();
				showMessage("System Error , Try Later","error")
			}
		})
	};
 
}
function changeSelected(whoSelect){
 
	 $(".paymentType").removeClass("paymentTypeOn");
	 $("."+ whoSelect).addClass("paymentTypeOn");
	 if(whoSelect == "visa"){
			$("#cardTypeShowInfo").html("You select VISA");
	 }else if(whoSelect == "mc"){ 
		 $("#cardTypeShowInfo").html("You select MASTER");
	 }else if(whoSelect == "discover"){
		 $("#cardTypeShowInfo").html("You select DISCOVER");
	 }else if(whoSelect == "amex"){
		 $("#cardTypeShowInfo").html("You select AMEX");
	 }
}
</script>
<style type="text/css">
	div.buttonShow{text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;padding-top:5px;}
 
	span.spanPayClick_fix{background:url('common/pay/pay_ment/btn_xpressCheckout.gif');width:145;height:42px;display:block;margin:0px auto;};
	
</style>
</head>
<body>
 
				 <h2 style="text-indent:5px;background:#ebebeb;height:35px;line-height:35px;"  >Step 3 Of 3 : Select Payment method.</h2>	
  				  
  				  <div class="payment_method">
					  <div id="tabs">
							<ul>
								<li><a href="#onlinePay"><span style="font-weight:bold;">Credit Card</span></a></li>
								<li><a href="#paypal"><span style="font-weight:bold;">Paypal</span></a></li>
								<li><a href="#western"><span style="font-weight:bold;">Western Union</span></a></li>
								<li><a href="#wire"><span style="font-weight:bold;">Wire Transfer</span></a></li>
								
							</ul>
							
						    <div id="onlinePay">	
							 	   <form id="cark">
							 	  
							 		<table>
							 			<tr>
							 				<td class="td_left">Country</td>
							 				<td class="td_right">
						 						<%
												DBRow countrycode[] = orderMgr.getAllCountryCode();
												String selectBg="#ffffff";
												String preLetter="";
												%>
							 					<select id="countryCode" name="countryCode" onChange="getStorageProvinceByCountryCode(this.value)" style="height:23px;line-height:23px;"> 
							 					<option value="0">Please select...</option>
							 					  <%
													  for (int i=0; i<countrycode.length; i++)
													  {
													  	if (!preLetter.equals(countrycode[i].getString("c_country").substring(0,1)))
														{
															if (selectBg.equals("#eeeeee"))
															{
																selectBg = "#ffffff";
															}
															else
															{
																selectBg = "#eeeeee";
															}
														}  	
														
														preLetter = countrycode[i].getString("c_country").substring(0,1);
													  %>
													    <option style="background:<%=selectBg%>;" value="<%=countrycode[i].getString("c_code")%>"><%=countrycode[i].getString("c_country")%></option>
													  <%
													  }
													%>
											  </select>
							 				</td>
							 			</tr>
							 		 	<tr>
							 				<td class="td_left">Card number</td>
							 				<td class="td_right"><input type="text" id="acct" name="acct" class="txt" onkeyup="fixIntValue(this);"/></td>
							 			</tr>
							 			<tr>
							 				<td class="td_left">Amount</td>
							 				<td class="td_right"><input type="text" id="amount" name="amount" class="txt" disabled value='<%= billRow.get("save",0.0d) %>'/></td>
							 			</tr>
							 			<!-- 
							 			<tr>
							 				<td class="td_left">Card Types</td>
							 				<td class="td_right" style="padding-top:8px;"> 
							 						<span class="paymentType visa paymentTypeOn" onclick="changeSelected('visa')" target="Visa">visa</span>
							 						<span class="paymentType mc" onclick="changeSelected('mc')" target="Master">mc</span>
							 						<span class="paymentType discover" onclick="changeSelected('discover')" target="discover">discover</span>
							 						<span class="paymentType amex" onclick="changeSelected('amex')" target="amex">amex</span>
							 						<span style="display:block;color: green; float: left; font-weight: bold; margin-left: 10px;width: 200px;" id="cardTypeShowInfo">You select VISA</span>
							 				</td> 
							 			</tr>
							 			 -->
							 			 <tr>
							 				<td class="td_left">Currency Code</td>
							 				<td class="td_right" id="currencyCodeTd">
							 				<select id="mc_currency" onchange="currencyCodeDiv(this.value)" disabled>
							 					<%
												ArrayList currency = currencyKey.getCurrency();
												for (int i=0; i<currency.size(); i++){
													%>
														 <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>"><%=currency.get(i)%></option>
													<%
												}
												%>
												<option value="-1">Other</option>
											</select>
											
							 				</td>
							 			</tr>
							 			
							 			<tr>
							 				<td class="td_left">Expiration date</td>
							 				<td class="td_right">
							 					<p class="date_p" style="float:left;">
							 						<span style="">MM</span> 
							 						<input id="date_MM" type="text" class="small" maxlength="2" onkeyup="fixIntValue(this);"/>  
							 					</p>
							 					<p class="date_p" style="float:left;margin-left:0px;">
							 						<span style="">YYYY</span> 
							 						<input id="date_YYYY" type="text" class="small" onkeyup="fixIntValue(this);" maxlength = "4"/>
							 					</p>
							 				</td>
							 			</tr>
							 		
							 			<tr>
							 				<td class="td_left">
							 					 CVV2 
							 				 	<span id="whatisCvv" style="color:#084482;cursor:pointer;">?</span>
							 				</td>
							 				<td class="td_right"><input type="text" id="cvv2" name="cvv2" class="small" maxlength = "4"/></td>
							 			</tr>
							 
							 			<tr>
							 				<td class="td_left">First name</td>
							 				<td class="td_right"><input type="text" id="firstName" name="firstName" class="txt" value='<%= billRow.getString("address_name") %>'/></td>
							 			</tr>
							 			<tr>
							 				<td class="td_left" >Last name</td>
							 				<td class="td_right"><input type="text" id="lastName" name="lastName"  class="txt"/></td>
							 			</tr>
							 			
							 		  <tr>
							 				<td class="td_left">Street</td>
							 				<td class="td_right"><input type="text" id="street" name="street" class="txt" value='<%= billRow.getString("address_street") %>'/></td>
							 			</tr>
							 			<tr>
							 				<td class="td_left">City</td>
							 				<td class="td_right"><input type="text" id = "city" name="city" class="txt" value='<%= billRow.getString("address_city") %>'/></td>
							 			</tr>
							 			<tr>
							 				<td class="td_left">State</td>
							 				<td class="td_right" id="stateTd">
							 					<select id="pro_id" onchange="stateDiv(this.value)">
							 						
							 					</select>
							 				</td>
							 			</tr>
							 			<tr>
							 				<td class="td_left">ZIP Code</td>
							 				<td class="td_right"><input type="text" id="zip" name="zip" class="small" maxlength="10" value='<%= billRow.getString("address_zip") %>'/></td>
							 			</tr>
							 			 <tr>
							 				<td class="td_left">Buyer Email</td>
							 				<td class="td_right"><input type="text" id="email" name="email" class="txt" value='<%= billRow.getString("client_id") %>'/></td>
							 			</tr>
							 			<tr>
							 				<td class="td_left">Buyer Phone</td>
							 				<td class="td_right"><input type="text" id="phone" name="phone" class="txt" value='<%= billRow.getString("tel") %>'/></td>
							 			</tr>
							 		 
							 			<tr>
							 				<td class="td_left">Note</td>
							 				<td class="td_right"> <input type="text" name="note" class="txt" id="note"/></td>
							 			</tr>
							 			
							 			</table>
							 		 
							 		</form>
							 		
							 
							 		<br />
							 		   <span class="spanPayClick" style="background:url('common/pay/pay_ment/comfi.gif');border:none;margin:0px auto ;display:block;height:28px;line-height:28px;width:59px;text-align:center;" onclick="submitOn();"> Confirm </span> 
							 	 	 	<!-- <input type="button" value="text" onclick="showPayMentComplete();"/> -->
							 </div>
							<div id="paypal">
								   
								   
								   
				  		 	 <p style="margin-top:10px;">
				  		 	 <div style="border:0px solid silver;width:700px;padding-top:20px;" class="">
	  		 	 					 
			 				<div style="width:40%;float:left;border:0px solid green;">
						  		 	<img src="common/pay/visionari.gif" />
								 	<p><span id="employe_name_" style="font-weight:bold;"> <%= (userInfo != null?userInfo.getString("account"):"")%></span><span style="margin-left:10px;" id="create_time_"><%= billRow.getString("create_date") %> </span></p>
								    <p style="" id="account_name_"><%= billRow.getString("account_name") %></p>
								    <p style="margin-top: 10px;" id="account_"><%= billRow.getString("account") %></p>
						  	</div>
						   <div style="width:55%;float:right;border:0px solid red;margin-bottom:30px;">
			  
				  			<h1 style="color: #CCCCCC;text-transform:uppercase;font-size:16px;text-align:right;margin-right:20px;margin-top:10px;">Invoice</h1>
					  		 	<table id="invoiceDetails" class="basic" summary="Invoice details" style="float:right;margin-top:20px; ">
									<tbody>
										<tr>
											<th>Invoice number</th>
											<td><%=  billRow.get("bill_id",0l)%> &nbsp;</td>
										</tr>
										<tr>
											<th>Invoice date</th>
											<td><%=billRow.getString("create_date") %> &nbsp;</td>
										</tr>
										<tr>
											<th>Payment terms</th>
											<td>Due on receipt</td>
										</tr>
										
										<tr>
											<th>Due date</th>
											<td><%=billRow.getString("create_date") %> &nbsp;</td>
										</tr>
									</tbody>
								</table>
				  		 </div>
				  		  <div style="clear:both;border:0px solid silver;margin-top:20px;margin-bottom:10px;" id="sendto">
			  		  
			  		 		<p id="sendto_p" class="ui-corner-all" style="width:98%;background:#e8f1fa;padding:5px;text-indent:10px;line-height:35px;height:35px;">
			  		 			 <span style="font-weight:bold;font-size:14px;">Send To : </span> <span id="addressInfoStr"></span>
			  		 		</p>
			  			 </div>
			  		 	 <table class="itemTable">
			  		 			<thead>
			  		 				<tr>
			  		 					<th width="60%">Description</th>
			  		 					<th width="16%">Quantity</th>
			  		 					<th width="16%">Unit price</th>
			  		 					<th>Amount</th>
			  		 				</tr>
			  		 			</thead>
			  		 			<tbody id="tbodyView">
			  		 				<%
			  		 					if(items != null && items.length > 0){
			  		 						for(int index = 0 , count = items.length ; index < count ; index++ ){
			  		 					 	String background  = (index%2 == 0)?"":"#e6f3c5";
			  		 							%>	
			  		 							<tr style="background:<%= background %>;">
			  		 								<td class="name">
			  		 									<%= items[index].getString("name") %>
			  		 								<%
									 			if (items[index].get("product_type",0)==CartQuote.UNION_CUSTOM ){
									 				 DBRow[] un = billMgrZr.getProductUnionByPcid(items[index].get("pc_id",0l)); 
									 				 if(un != null && un.length > 0){
										 				%>
										 				<ul class="itemDetail">
										 				
										 				<% 
									 					 for(DBRow u : un){
										 					%>
										 				<li>├ <%= u.getString("p_name") %> x <%= u.get("quantity",0.0) %>&nbsp;<%= u.getString("unit_name") %></li>	
										 					<%   
										 				 }
										 				%>
										 				 </ul>
										 				<% 
									 				 }
									 			}
									 			%>
			  		 								</td>
			  		 								<td class="t"><%= items[index].getString("quantity") %></td>
			  		 								<td class="t"><%= items[index].get("actual_price",0.0d) %></td>
			  		 								<td class="t"><%= items[index].getString("amount") %></td>
			  		 								
			  		 							</tr>
			  		 							<% 	
			  		 						}
			  		 					}
			  		 				%>
			  		 	 
			  		 			</tbody>
			  		 			<tfoot>
			  		 				<tr>
			  		 					<td style="text-align:left;text-indent:10px;" id="payforItemNote">
			  		 						Note :  
			  		 						<p style="border:1px solid silver;width:370px;margin-left:5px;line-height:13pt;margin-bottom:4px;">
			  		 							<%= billRow.getString("note") %>
			  		 						</p>
			  		 					</td>
			  		 					<td colspan="3" style="border-left:1px solid silver;">
			  		 						 <table style="width:100%;border-collapse:collapse;">
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Currency</td>
			  		 						 		<td id="rate_type_td"><%= billRow.getString("rate_type") %></td>
			  		 						 	</tr>
			  		 						 	<!--  
			  		 						 	<tr >
			  		 						 		<td style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" id="">Subtotal</td>
			  		 						 		<td id="subtotal_td"><%= billRow.get("subtotal",0.0d) %></td>
			  		 						 	</tr>
			  		 						 	-->
			  		 						 	<tr>
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Discount</td>
			  		 						 		<td id="total_discount_td"><%= billRow.get("total_discount",0.0d) %></td>
			  		 						 	</tr>
			  		 							<tr style="background:#e8f1fa">
			  		 						 		<td id="" style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" >Shipping Fee</td>
			  		 						 		<td id="shipping_fee_td"><%= billRow.get("shipping_fee",0.0d) %></td>
			  		 						 	</tr>
			  		 						 	<tr >
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Total</td>
			  		 						 		<td id="save_td"><%= billRow.get("save",0.0d) %></td>
			  		 						 	</tr>
			  		 						 </table>
			  		 					</td>
			  		 					
			  		 				</tr>
			  		 			</tfoot>
			  		 	 </table>
				  		 	  <div style="margin-top:20px;">
				  	 						<%if(comeFrom.equals("invoice")){
				  	 							if(billRow.getString("invoice_id").length() > 0){
				  	 						%>
				  	 					 		<span class="spanPayClick_fix"><a style="width:145px;height:42px;display:block" href= 'https://www.paypal.com/us/cgi-bin/?cmd=_pay-inv&id=<%=billRow.getString("invoice_id") %>' target="_blank"></a></span>
				  	 						<% 	
				  	 							}else{
				  	 						%>
				  	 					 		<span class="spanPayClick_fix"><a style="width:145px;height:42px;display:block" href= 'javascript:sendInvoiceNotCreate();' ></a></span>
				  	 						<% 
				  	 							}
				  	 						%>
					  		 	 			<%}else if(comeFrom.equals("payforitem")){ %>
					  		 	  				<span class="spanPayClick_fix"><a style="width:145px;height:42px;display:block" href= 'javascript:payForItemFormSubmit();'  ></a></span>
					  		 	 			<%}else if(comeFrom.equals("payfororderfee")){ %>
					  		 	 			  	<span class="spanPayClick_fix"><a style="width:145px;height:42px;display:block" href= 'javascript:payForOrderFeeSubmit();'  ></a></span>
					  		 			   <%} else if(comeFrom.equals("payforebayitem")){%>
					  		 			   		 <span class="spanPayClick_fix"><a style="width:145px;height:42px;display:block" href= 'javascript:payForEbayItemFormSubmit();'  ></a></span>
					  		 			   	
					  		 			   <%} %>
					  		  
					  		 	</div>
				  		 	</div> 
				  		 	 </p>
			  		 	
			  		 </div>	
							 
							 <div id="western" style="">
									<img src="http://www.vvme.com/images/westernunion.gif" style="margin-bottom:5px;"/>
									<p class="B">
										To complete payment by Western Union, go to your local Western Union branch, or go to
										<a href="http://www.westernunion.com/selectCountry.asp"> their website(http://www.westernunion.com/selectCountry.asp)</a>
										, and follow their instructions. The
										<strong> beneficiary details</strong>
										for VVME.COM are:
										</p>
										<ul class="itemUL">
											<li>First Name: FEI</li>
											<li> Last Name: HAN </li>
											<li> CITY:Bei Jing</li>
											<li>Country: CHINA </li>
											<li> Address: BeiYuanJiaYuan, ChaoYang District, Beijing, PR China, 100012 </li>
										</ul>
									</p>
									<p class="B">
									After successfully transferring the funds, Please send the payment details by email to
									<a href="mailto:sales@vvme.com" style="color:#f60;">sales@vvme.com </a>
									or
									<a href="mailto:service@vvme.com" style="color:#f60;">service@vvme.com</a>
									, fill in the details of followoing:
								</p>
									<ul class="itemUL">
									<li>Reference no.(MTCN)</li>
									<li>Amount of money </li>
									<li>Sender's First Name</li>
									<li>Sender's Last Name</li>
									<li> Country </li>
								</ul>
								<p class="B">Then, you're done!</p>
								<p style="text-algin:center;">
									<span class="spanPayClick" style="background:url('common/pay/pay_ment/comfi.gif');border:none;margin:0px auto ;display:block;height:28px;line-height:28px;width:59px;text-align:center;"> <a href="mailto:sales@vvme.com,service@vvme.com">Email To</a></span> 
								</p>
							 </div>
							 
							 
							 
							 
							 
							 
							<div id="wire" style="">
								<img src="http://www.vvme.com/images/wiretransfer.jpg" style="margin-bottom:5px;"/>
								<p class="B">
									To complete payment by Wire Transfer, contact your local bank or use internet banking to do the Wire (T/T). The
									<strong> beneficiary details</strong>
									for VVME.COM are:
								</p>
								<ul class="itemUL">
									<li> Beneficiary Bank: HSBC BANK</li>
									<li> Beneficiary Bank Address: 1350 GRANT ROAD, SUITE 3  MOUNTAIN VIEW, CA 94040</li>
									<li> Account Name(Recipient Name): Visionari E-Commerce LLC</li>
									<li> ACT # :  876001169 </li>
									<li> Swift Code: MRMDUS33</li>
									<li> ABA Number: 122240861</li>
									<li> Account Type: Business Checking </li>
									<li>
									Account No.: On Price List
									<br>
									</li>
								</ul>
								<p class="B">
									After successfully transferring the funds, Please send the payment details by email to
									<a href="mailto:sales@vvme.com" style="color:#f60;">sales@vvme.com </a>
									or
									<a href="mailto:service@vvme.com" style="color:#f60;">service@vvme.com</a>
									, fill in the details of followoing:
								</p>
								<ul class="itemUL">
									<li> Amount of money </li>
									<li>Sender Name </li>
									<li> Country </li>
								</ul>
								<p class="B">Then, you're done!</p>
								<p style="text-algin:center;">
									<span class="spanPayClick" style="background:url('common/pay/pay_ment/comfi.gif');border:none;margin:0px auto ;display:block;height:28px;line-height:28px;width:59px;text-align:center;"> <a href="mailto:sales@vvme.com,service@vvme.com">Email To</a></span> 
								</p>
							 </div>
					  </div>
		 </div>
			
				<div class="buttonShow"><input type="button"  class="backButton" onclick="doInputEmailAndInoviceNumber('true','<%= billRow.get("bill_id",0l) %>','<%= billRow.getString("client_id") %>','<%=comeFrom %>');" />&nbsp;&nbsp;</div>
			  <script type="text/javascript">
			 	 $("#tabs").tabs();
			 	 
			 	 $("#tabs").tabs( "select" , '<%=selectIndex%>' * 1 );
			  </script>
</body>
</html>
