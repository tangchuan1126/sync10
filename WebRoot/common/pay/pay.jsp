<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%> 
<%@page import="com.cwc.app.key.EbaySite"%>
<%@page import="com.cwc.json.JsonObject"%>
<%@page import="java.util.*"%>
<jsp:useBean id="currencyKey" class="com.cwc.app.api.CurrencyKey"/>
<%@page import="com.cwc.app.key.BillTypeKey"%>
 <html xmlns="http://www.w3.org/1999/xhtml">
 
	<head>
		<title>VVME Pay Ment Page</title>
		<script type="text/javascript" src="common/pay/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="common/pay/jquery-ui-1.7.3.custom.min.js"></script>
		<link rel="stylesheet" type="text/css" href="common/pay/jquery-ui-1.7.3.custom.css" />
		<link rel="stylesheet" type="text/css" href="common/pay/pay_ment/stateBox.css" />
		<link rel="stylesheet" type="text/css" href="common/pay/art/skins/aero.css" />
		<link rel="stylesheet" type="text/css" href="common/pay/tip/tip-darkgray/tip-darkgray.css" />
		<script src="common/pay/art/plugins/jquery.artDialog.source.js" type="text/javascript"></script>
		<script src="common/pay/art/plugins/iframeTools.source.js" type="text/javascript"></script>
		<script src="common/pay/art/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="common/pay/select.js" type="text/javascript"></script>
		<script src="common/pay/tip/jquery.poshytip.min.js" type="text/javascript"></script>
		<style type="text/css">
			*{margin:0px;padding:0px;font-size:12px;color:black;font-family: Verdana,Arial,sans-serif;}
			body{background:white;}
			strong{padding:3px;}
			a{text-decoration:none;}
			div.main{border:0px solid silver;background:white;width:755px;}	
			div.logo{border:0px solid red; margin-top:3px; height:58px;}
			div.payment_note{padding:5px;border:1px solid silver;margin-top:10px;}
			p{padding:2px;}
		 	a.link:hover{color:#f60;}
		 	a.show_a{text-decoration:underline;color:#f60;}
		 	
			div.payment_select{margin-top:10px;border:0px solid red; }
			div.payment_method{border:0px solid red;}
			ul.itemUL{margin-left:25px;font-style:italic ;}
			ul.itemUL li{line-height:15px; height:15px;}


			div.payment_select h1.showTitle{width:750px;height:21px;background:url("common/pay/pay_ment/step.png") no-repeat;background-position:0 0;}
			 span.step_one {cursor:pointer;display;block;height:25px; display:block;margin-left: 15px;float:left;line-height:25px;}
		  	 span.step_two {cursor:pointer;display;block;height:25px;display:block;margin-left: 110px;width: 146px;float:left;line-height:25px;}
			 span.step_three {cursor:pointer;display;block;height:25px;display:block;margin-left: 120px;width:150px;float:left;line-height:25px;}
			p.step_p_shuoming{line-height:25px;height:25px;text-indent:30px;}
			div.pay_for_invoice ,div.pay_for_order{border:1px solid silver;margin-left:50px;margin-top:10px;}
			div.pay_for_invoice  p,div.pay_for_order p{text-indent:10px;}
			input.buttonSearch{background:url("common/pay/pay_ment/short_search.gif");width:45px;height:20px;border:none;text-align:center;cursor:pointer;text-indent:10px;}
			td.td_right{width:120px;text-align:right;}
			div.cssDivschedule{
			width:100%;height:100%;position:absolute;left:0px;top:0px;z-index:10;display:none;
			 background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
			background: -moz-linear-gradient(top,  rgba(0,0,0,0.1) 0%, rgba(0,0,0,0) 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.1)), color-stop(100%,rgba(0,0,0,0)));
			background: -webkit-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -o-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: -ms-linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			background: linear-gradient(top,  rgba(0,0,0,0.1) 0%,rgba(0,0,0,0) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1a000000', endColorstr='#00000000',GradientType=0 );
			}
			div.innerDiv{margin-top:200px;margin-left:130px;text-align:center;border:1px solid silver;width:500px;height:30px;background:white;line-height:30px;font-size:14px;}
				
				
			
			div.bottom{border:1px solid silver; height:50px;margin-top:10px;}
			div.infoFromServer{border:1px solid silver;margin-top:10px;}
			input.nextButton{width:73px;height:27px;background:url("common/pay/pay_ment/next.gif");border:none;cursor:pointer;}
			input.backButton{width:73px;height:27px;background:url("common/pay/pay_ment/back.gif");border:none;cursor:pointer;}	
			/*pay for ebay*/
			table.shop{border:1px solid silver;border-collapse:collapse ;width:700px;}
			
			table.shop th {font-size:12px;border:1px solid silver;line-height:25px;height:25px;background:#E6F3C5;}
			table.shop td{border:1px solid silver;text-indent:10px;line-height:22px;height:22px;}
			input.invoiceInput {border: 1px solid #ADC2D6;height:23px;line-height: 23px; width: 200px;}
			input.txt{border:none;border-bottom:1px solid silver;width:130px;color:seagreen;}
			span.add_ebay , span.add , span.delete{margin-left:8px;width:16px;height:16px;display:block;float:left;border:0px solid silver;cursor:pointer;background:url("common/pay/pay_ment/add.gif")}
			span.delete{background:url("common/pay/pay_ment/delete.gif")}
			p.i_p{text-indent:10px;line-height:25px;height:25px;}
			.paymentType{background-image:url("common/pay/pay_ment/payment_icons_sprite.2D.png");border:0px solid silver;width:37px;height:23px;display:block;float:left;text-indent:-1000px;cursor:pointer;}
		 	.visa{background-position:0 -30px;}
		 	.paymentTypeOn{border:2px solid green;}
		 	.discover{background-position:0 -90px;margin-left:10px;}
		 	.amex{background-position:0 -120px;margin-left:10px;}
			.mc{background-position:0 -60px;margin-left:10px;}
			 p.date_p{border:0px solid silver;width:65px;height:50px;}
			 p.date_p span{display:block;line-height:20px;height:20px;}
			 input.small{height:23px;width:50px;}
			 
			 /*invoice message show*/
			 table.paypalTable{border-collapse:collapse;border-top:1px solid #eeeeee;border-left:1px solid #eeeeee;border-bottom:1px solid #eeeeee;width:100%;font:11px Verdana, Helvetica, sans-serif;color:#333;}
			table.paypalTable td.paypalTableLeft{background-color:#eeeeee;padding:5px 10px 0 0;}
			table.paypalTable td.paypalTableRight{padding:5px 0 5px 10px;}
			.spanPayClick{cursor:pointer;margin:0; padding:1px 10px; border:1px solid #bfbfbf; border-right-color:#908d8d; border-bottom-color:#908d8d;background:#ffa822};
			div.buttonShow{text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;}
			div.safetyPayment{border:0px solid silver;width:100%;padding-top:5px;margin-top:3px;}
			div.DivLeft{border:0px solid silver;width:150px;float:left;padding:5px;}
			p.pRight{border:0px solid red;width:500px;;float:left;text-indent:12px;color:#666666;}
			span.mcafee{display:block;width:94px;height:54px;background:url("common/pay/pay_ment/macfee.gif");}
			span.paypalVerified{display:block;width:70px;height:70px;background:no-repeat url("common/pay/pay_ment/safe.png");background-position:0px -151px;}
			span.vseal{display:block;width:100px;height:58px;background:url("common/pay/pay_ment/vseal.gif");}
			span.bank_transfer{margin-top:9px;float:left;display:block;width:112px;height:29px;border:0px solid silver;background:url("http://www.vvme.com/images/wiretransfer.jpg") no-repeat;}	
			span.west_union {margin-top:9px;margin-left:5px;float:left;display:block;width:112px;height:29px;border:0px solid silver;background:url("http://www.vvme.com/images/westernunion.gif") no-repeat;}	
			span.creditcar{margin-left:5px;float:left;display:block;width:112px;;height:38px;border:0px solid silver;}
			span.paypal{margin-left:5px;float:left;display:block;width:65px;height:38px;border:0px solid silver;background:url("common/pay/pay_ment/PayPal_mark_60x38.gif") no-repeat;}
		    span.paypal2{margin-top:8px;margin-left:5px;float:left;display:block;width:112px;;height:38px;border:0px solid silver;}
			
			/*付款过后的table*/
			 table.successtable{border-collapse :collapse ;border:1px solid silver;width:100%;}
			 table.successtable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
			 table.successtable td{line-height:30px;height:30px;border:1px solid silver;}
			 table.successtable td.name{text-indent:15px;width:440px;}
			 table.successtable td.t{text-indent:10px;}
			 table.successtable tfoot  td{height:25px;border:none;text-align:center;}
			 p.paymentSuccess{background:no-repeat url("common/pay/pay_ment/succeed.png");  height: 37px; line-height: 37px;margin-left: 104px; text-indent: 50px;width: 450px;  font-weight: bold;color:green;}
			 ul.paymentSuccessUl{list-style-type:none;border:0px solid red;margin-left:104px;width:500px;}
			 ul.paymentSuccessUl li{line-height:20px;height:20px;margin-top:5px;}
		</style>
		<% 
			String GetOrderAndInvoiceInfoAction = ConfigBean.getStringValue("systenFolder")+"action/pay/GetOrderAndInvoiceInfoAction.action";
			String selectedIndex = StringUtil.getString(request,"selected_index");
			String GetOrderNotLoginOnAction = ConfigBean.getStringValue("systenFolder")+"action/ebay/GetOrderNotLoginOnAction.action";
			String billIdComeFromUrl = StringUtil.getString(request,"bill_id");
			String payerEmailComeFromUrl = StringUtil.getString(request,"payer_email");
			String isInvoiceOpen = StringUtil.getString(request,"invoice_open_flag");
			String paymentState = StringUtil.getString(request,"payment_state");
			DBRow billRow = new DBRow();
			List<DBRow> items = new ArrayList<DBRow>();
			
			if(paymentState.equals("true")){
				String discount_amount_cart = StringUtil.getString(request,"discount_amount_cart");
				//shipping_1表示的是总共的运费
				String shipping = StringUtil.getString(request,"shipping_1");
				String currency_code = StringUtil.getString(request,"currency_code");
				//读取ItemList 中的值
				String item_name = StringUtil.getString(request,"item_name_1");
				billRow.add("shipping",shipping);
				billRow.add("discount_amount_cart",discount_amount_cart);
				billRow.add("currency_code",currency_code);
				int index = 1;
				while(item_name.length() > 0){
					DBRow temp = new DBRow();
					temp.add("item_name", StringUtil.getString(request,"item_name_"+index));
					temp.add("quantity",StringUtil.getString(request,"quantity_"+index));
					temp.add("amount",StringUtil.getString(request,"amount_"+index));
			 
					 
					index++ ;
					item_name = StringUtil.getString(request,"item_name_"+index);
					items.add(temp);
				}
			}
			DBRow[] itemArray = items.toArray(new DBRow[items.size()]);
			
		%>
		<script type="text/javascript">
	 		 
		var contentHtml  ;
 		var returnBillId = "";
 		var returnClientId = "";
 		var returnSelectIndex = "";
		var payForItemUrl = "";
		var payForOrderFeeUrl = "";
		var billRow = new Object();
		var arrayList =  [] ;
		var payForEbayItemUrl = "";
		var ebayArrayList = [];
		var orderId ;
		var orderFee;
		var payforOrderCurrencyCode;
		var paymentCompleteTagIndex ; 
		var invoiceArray = [];
		var billRowJson =  <%=new JsonObject(billRow).toString()%>;
		var itemRowJson =  <%=new JsonObject(itemArray).toString()%>;
	 
		function windowLock(){$(".cssDivschedule").css("display","block");}
		function windowUnLock(){$(".cssDivschedule").fadeOut();}
			
		function showDetail(){
			 
			var a = $("#show_detail");
		 
			if(a.attr("type") === "pay_for_invoice"){
				var billId = a.attr("targeted");
				var uri =  "pay_for_invoice.html?bill_id=" + billId; 
			 	$.dialog.open(uri , {title: 'Show Detail',width:'1100px',height:'530px', lock: true,opacity: 0.3,fixed: true});
			}
		}
	 	function showPayMentComplete(){
			// 填充billRow数据
			$("#paymentcomplete").css("display","block");
			$("#paymenting").css("display","none");
		 
			 var arrayInner ; 
			if(paymentCompleteTagIndex * 1 == 0){
				// ebay
				 arrayInner = ebayArrayList;
				 $("#shipping_payComplete").html(billRow["shippingFee_ebay"]);
				 $("#currency_payComplete").html(billRow["currency_code_ebay"]);
				 $("#total_payComplete").html(billRow["amount_ebay"] );
				 $("#discount_payComplete").html(billRow["discount_ebay"] );
			}
			if(paymentCompleteTagIndex * 1 == 1){
				// invoice
			 $("#shipping_payComplete").html(billRow["shippingFee_invoice"]);
			 $("#currency_payComplete").html(billRow["currency_code_invoice"]);
			 $("#total_payComplete").html(billRow["amount_invoice"] );
			 $("#discount_payComplete").html(billRow["discount_invoice"] );
			 arrayInner = invoiceArray ;
			}
			if(paymentCompleteTagIndex * 1 == 2){
				// with out invoice
				arrayInner = arrayList;
				 $("#shipping_payComplete").html(billRow["shippingFee"] );
				 $("#currency_payComplete").html(billRow["currency_code"]);
				 $("#total_payComplete").html(billRow["amount"] );
				 $("#discount_payComplete").html(billRow["discount"] );
			}
			//读取ItemList中的数据
			setPaymentCompleteItemList(arrayInner);
			
		}
		function setPaymentCompleteItemList(arrayInner){
			var tbody = $("#successTbody");
			 $("tr",tbody).remove();
			 var totalAoumt = 0 ;
			if(arrayInner != null && arrayInner.length > 0){
				for(var index =  0 , count = arrayInner.length ; index < count ; index++ ){
					var temp = arrayInner[index];
					var backgroundColor = ":white;";
					if(index % 2 == 1){
						backgroundColor = ":#e6f3c5;";
					}
					var sum = getDoubleValue(temp["quantity"],temp["amount"]);
			 
					var tr = "<tr style='background"+backgroundColor+"'><td class='name'>"+temp["item_name"] +"</td>";
					tr += "<td class='t'>"+temp["quantity"]+"</td>";
					tr += "<td class='t'>"+temp["amount"]+"</td>";
					tr += "<td class='t'>"+sum+"</td></tr>";
					totalAoumt += parseFloat(sum);
					 
					tbody.append($(tr));
				}

			}
			return totalAoumt ;
		 
		}
		function getDoubleValue(number1 , number2 ){
			var value = parseFloat(number1) * parseFloat(number2);
			return decimal(value,2);
		}
		function ajaxGetHtml(uri,node,fromData){

			windowLock();
			$.ajax({
				url:uri,
				dataType:'html',
				success:function(data){
					windowUnLock();
					node.html(data);
				},
				error:function(){
					showMessage("System Error , Please Try Later","error");
					windowUnLock();
				}
			})	
		}
		function showIntroduction(){
			window.location.reload();
		}
		
		function refreshWindow(){
			window.location.reload();
		}
		function selectPayMethod(fromData){
			windowLock();
			$.ajax({
				url:'payment_method.html',
				dataType:'html',
				success:function(data){
					windowUnLock();
					$("#content").html(data);
					initCredCarPayMent(fromData);
				},
				error:function(){
					showMessage("System Error , Please Try Later","error");
					windowUnLock();
				}
			})	
		}
		// 初始化如果是信用卡付款的信息
		function initCredCarPayMent(data){
			 // 初始化信息
			 
			 if(data){
				 
			 	 $("#amount").val(fromObjectStringValue(data.save));
				 $("#email").val(fromObjectStringValue(data.client_id));
				 $("#phone").val(fromObjectStringValue(data.tel));
				 $("#street").val(fromObjectStringValue(data.address_street));

				 $("#city").val(fromObjectStringValue(data.address_city));
				 $("#firstName").val(fromObjectStringValue(data.address_name));
				 $("#zip").val(fromObjectStringValue(data.address_zip));
			 }
		}
		function fromObjectStringValue(value){
			return value ? value:"";
		}
		function getReadMe(){
			 $("#content").html(contentHtml);
		}
		function parentGotoSelectPayMent(url){
			ajaxGetHtml("payment_method.html"+url,$("#content"));
		}
		function doPayForInvoice(billIdComeFromUrl,payerEmailComeFrmUrl){
			var invoice_id = billIdComeFromUrl || $("#invoice_id").val();
			var payer_email = payerEmailComeFrmUrl ||  $("#payer_email").val();
			if($.trim(invoice_id).length < 1 ){
				showMessage("Please Input Invoice Number","alert");
				return ;
			}
			if($.trim(payer_email).length < 1){
				showMessage("Input Payer Email","alert");
				return ;
			}
			windowLock();
			 
			$.ajax({
				url:'<%= GetOrderAndInvoiceInfoAction%>',
				dataType:'json',
				data:$("#pay_for_invoice").serialize(),
				success:function(data){
					canNext = false;
					 windowUnLock();
					 if(data.flag === "success"){
						if(data.message){
							showMessage(data.message,"alert");
							return ;
						}else{
							 billId = invoice_id;
							 returnBillId = invoice_id;
							 returnClientId = payer_email;
							 invoiceId = data.invoice_id;
							canNext = true;
							// 填充数据
							// 如果是改账单没有发送Invoice应该给出提示客户。改INvoice还没有创建叫他联系vvme.并且不能付款
							contentHtml = $("#content").html();
							 
							if(data.invoice_state * 1 >= 1){
								if(data.trade_id * 1 > 0){
									 showMessage("The Invoice Is Pay","alert");
									 canNext = false;
								} else{
									if(data.key_type * 1 == 0){
										returnSelectIndex = 1;
									}else{
										returnSelectIndex = 0;
									}
								}
							}else { 
								canNext = true;
								return ;
							}
							
						}
					 }else{
						canNext = false;	
						showMessage(data.message,"error");
					}
			 
					doInputEmailAndInoviceNumber(canNext,invoice_id,payer_email,"invoice");
				},
				error:function(e){
					canNext = false;
					showMessage("System Error , Plase Try Later","error");
					windowUnLock();
				}
				
			})
		}
		function setInvoiceNotCreateForm(){
			
		}
		function getBillDetail(){
			var tabSelectedIndex = ($("#tabs" ).tabs("option","selected"));
			paymentCompleteTagIndex = tabSelectedIndex;
			if(tabSelectedIndex * 1 == 0){
					doPayForEbayItem();
			}else if(tabSelectedIndex * 1 == 1){
				doPayForInvoice();
		   	}else if(tabSelectedIndex * 1 == 2){
		   		doPayForItemNext();
		  }else if(tabSelectedIndex * 1  == 3){
			  doPayForOrderFee();
		 }
		}
		function doPayForEbayItem(){
			var formValue = $("#ebayformValue");
			
			// 首先进行很严格的检查如果通过了 在到下一个页面
			// 建立一个Object存放fee,建立一个array 存放ItemList。然后缓存在页面上
			if(validatePayForItem_("_ebay")){
				//把Customer的值补充上
				var buyerId = $("#buyer_id").val();
			 
				var custom_ebay = "order_source=:EBAY" + ",auction_buyer_id="+buyerId;
				$("input[name='custom']",formValue).val(custom_ebay);
			 
				ebayArrayList = []; 
				billRow["discount_ebay"] =  $("#discount_ebay").val();
				billRow["shippingFee_ebay"] =$("#shippingFee_ebay").val();
				billRow["currency_code_ebay"] = $("#currency_code_ebay").val();
				billRow["amount_ebay"] = $("#amount_ebay").val();
				billRow["buyer_id"] = $("#buyer_id").val();
				// 读取itemList ;
				var index = 1;
				var itemNumberNode = $("input[name='item_number_"+index+"']",formValue);
				
				while(itemNumberNode.length > 0){
					var temp = new Object();
					temp["item_number"] = itemNumberNode.val();
				
					temp["item_name"] = $("input[name='item_name_"+index+"']",formValue).val();
					 
					temp["amount"] = $("input[name='amount_"+index+"']",formValue).val();
					temp["quantity"] = $("input[name='quantity_"+index+"']",formValue).val();
					ebayArrayList.push(temp);
					index++;
					itemNumberNode =  $("input[name='item_number_"+index+"']")
				}
				//设置Ebay_Item_return ;
				var strForm = (formValue.serialize());
				var returnUrlValue = "http://www.1you.com/paypal/pay.html?payment_state=true&"+strForm;
				$("input[name='return']",formValue).val(returnUrlValue);
			 
				payForEbayItemUrl =  (formValue.serialize());
				
				ajaxGetHtml("what_pay_for.html?come_from=payforebayitem&"+payForEbayItemUrl,$("#content"));
			}
			
		}
		function doPayForOrderFee(){
			// 要检查Order Id 是不是存在.如果是存在的就进行下一步。并且要读取Order上的客户的信息，这种只能通过信用卡付款
			var order_fee = $("#order_fee").val();
			var order_id = $("#order_id").val();
			if(order_id * 1 < 1){
				showMessage("Input Your Order Number","alert");
				return  ;
			} 
			if(parseFloat(order_fee) <= 0 ){
				showMessage("Input Your Order Fee","alert");
				return ;
			}
			windowLock();
			$.ajax({
				url:'<%= GetOrderNotLoginOnAction%>' + "?oid="+order_id,
				dataType:'json',
				success:function(data){
					windowUnLock();
					if(data.flag =="error"){
						showMessage("The Order Number Not Exist","alert");
					}else{
						// 这里还应该判断是不是发货了什么的,然后值就直接请求第二个页面
						orderId = order_id;
						orderFee = order_fee;
						var value = ",order_id:"+order_id+",order_fee:"+order_fee;
						payforOrderCurrencyCode = $("payfororder_currency_code").val();
						$("#amount_1").val(order_fee);
						$("#item_name_1").val($("#item_name_1").val() + order_id );
						var custom = $("#custom");
						var s = custom.val();
						custom.val(s + value);

						payForOrderFeeUrl = $("#payfororderfee").serialize();
					 	 
						ajaxGetHtml("what_pay_for.html?come_from=payfororderfee&oid="+order_id+"&"+payForOrderFeeUrl,$("#content"));
						
					}
				},
				error:function(){
					windowUnLock();
					showMessage("System Error ,Please Try Later","error");
				}
			})
		}
		function doPayForItemNext(){
			var formValue = $("#formValue");
			var returnUrl = "http://www.1you.com/paypal/pay.html?payment_state=true&";
		 	var strForm = formValue.serialize();
		 
		 	returnUrl += strForm;
		 
		 	$("input[name='return']",formValue).val(returnUrl);
			payForItemUrl = ($("#formValue").serialize());
		
			// 首先进行很严格的检查如果通过了 在到下一个页面
			// 建立一个Object存放fee,建立一个array 存放ItemList。然后缓存在页面上
			if(validatePayForItem_()){
				arrayList = []; 
				billRow["discount"] =  $("#discount").val();
				billRow["shippingFee"] =$("#shippingFee").val();
				billRow["currency_code"] = $("#currency_code").val();
				billRow["amount"] = $("#amount").val();
				// 读取itemList ;
				var index = 1;
				var itemNumberNode = $("input[name='item_number_"+index+"']",formValue);
				
				while(itemNumberNode.length > 0){
					var temp = new Object();
					temp["item_number"] = itemNumberNode.val();
					temp["item_name"] = $("input[name='item_name_"+index+"']",formValue).val();
				 
					temp["amount"] = $("input[name='amount_"+index+"']",formValue).val();
					temp["quantity"] = $("input[name='quantity_"+index+"']",formValue).val();
					arrayList.push(temp);
					index++;
					itemNumberNode =  $("input[name='item_number_"+index+"']")
				}
				ajaxGetHtml("what_pay_for.html?come_from=payforitem&"+payForItemUrl,$("#content"));
			}
	 
		}
		function validatePayForItem_(target){
			var trs ;
			var isEbay = (target === "_ebay");
		    if(target === "_ebay"){
			   trs = $("tr",$("#tbody_ebay"));
			   var buyerId = $("#buyer_id");
			   if( buyerId.val().length < 1){
				   showMessage("Input Your Ebay Buyer Id","alert");
				   return false;
			   }
			}else{
			  trs = $("tr",$("#tbody"));
			}
			 for(var index = 0 ; index < trs.length ; index++ ){
				 var _this = $(trs[index]);
				 var itemNumber = $("input[name='item_number_"+(index+1)+"']",_this);
		 
				 if(itemNumber && $.trim(itemNumber.val()).length > 0 && itemNumber.val().indexOf("*") == -1){ 
				 }else{
					 
					if(isEbay){
						showMessage("ebayItem Number Is InValidate","alert");
					}else{
						showMessage("Item Number Is InValidate","alert");
					}
					return false;
				 }

				 var itemName = $("input[name='item_name_"+(index+1)+"']",_this);
				 
				 if(itemName && $.trim(itemName.val()).length > 0 && itemName.val().indexOf("*") == -1){ 
				 }else{
					 if(isEbay){
						showMessage("ebayItem Title Is Invalidate","alert");
					 }else{
						 showMessage(" Item Name Is Invalidate","alert");
					}
					return false;
				 }
				 var countPrice = 0;
				 var countQuantity = 0;
				 if(isEbay){
					  countPrice = parseFloat($(".countPrice_ebay",_this).val());
					  countQuantity = parseFloat($(".countQuantity_ebay",_this).val());
				}else{
					  countPrice = parseFloat($(".countPrice",_this).val());
					  countQuantity = parseFloat($(".countQuantity",_this).val());
				}
				 
				 if(countPrice * countQuantity <= 0 ){
					showMessage("Price Or Quantity Invalidate","alert");
					 return false;
				 };
			 }
			 return true;
		}
		function doInputEmailAndInoviceNumber(flag , billId , clientId,comeFrom){
			if(flag){
				//表示的是可以进入Detail页面
				if(comeFrom == "payforitem"){
					 
					ajaxGetHtml("what_pay_for.html?come_from=payforitem&"+payForItemUrl,$("#content"))
				}else if(comeFrom == "invoice"){
				   ajaxGetHtml("what_pay_for.html?bill_id="+billId+"&client_id="+clientId+"&come_from=" + comeFrom ,$("#content"));
				}else if(comeFrom == "payfororderfee"){
					ajaxGetHtml("what_pay_for.html?come_from=payfororderfee&"+payForOrderFeeUrl,$("#content"))
				}else if(comeFrom == "payforebayitem"){
					ajaxGetHtml("what_pay_for.html?come_from=payforebayitem&"+payForEbayItemUrl,$("#content"))
					
				}
			}
		 
		}
			function returnToBill(selectedIndex){
				headProcess(0);
			$("#content").html(contentHtml);
			if(selectedIndex*1 == 0){
				if(billRow){
					for(var i in billRow){
						$("#"+i).val(billRow[i]);
					}
					$("#currency_code option[value='"+billRow["currency_code"]+"']").attr("selected",true);
				}
			 
				 if(ebayArrayList.length > 0){
					 $("#tbody_ebay tr").remove();
					for(var index = 0 ,count = ebayArrayList.length ; index < count ; index++ ){
						var temp = ebayArrayList[index];
						if(temp){
							addTr(temp["item_number"],temp["item_name"],temp["amount"],temp["quantity"],"_ebay");		
						}
					}
				}
				
			}else if(selectedIndex*1 == 1){

				$("#payer_email").val(returnClientId);
				$("#invoice_id").val(returnBillId);
  
			}else if(selectedIndex*1 == 2){
				 // payForItem ,  读取页面缓存的值然后添加到页面上
				if(billRow){
					for(var i in billRow){
						$("#"+i).val(billRow[i]);
					}
					$("#currency_code option[value='"+billRow["currency_code"]+"']").attr("selected",true);
				}
			 
				 if(arrayList.length > 0){
					 $("#tbody tr").remove();
					for(var index = 0 ,count = arrayList.length ; index < count ; index++ ){
						var temp = arrayList[index];
						if(temp){
							//		
							addTr(temp["item_number"],temp["item_name"],temp["amount"],temp["quantity"]);		
						}
					}
				}
				
				
			}  else if(selectedIndex*1 == 3){
				// payfororderfee
				$("#order_id").val(orderId);
				$("#order_fee").val(orderFee);
				
				$("#payfororder_currency_code option[value='"+payforOrderCurrencyCode+"']").attr("selected",true);
			}
			$("#tabs").tabs();
			$("#tabs").tabs( "select" , selectedIndex * 1);
			 
		}
		function showSelectPayMentMethod(keyType,invoiceId,comefrom){
			if(comefrom == "payforitem"){
				 
				//这里应该是 Form表单的提交到另外的一个界面 declare_r
				var notShow = 	$("#notShow");
				var action = notShow.attr("action");
		 		//payforitem不需要跳转的时候就打开Paypal
				//notShow.attr("action",action+"?"+payForItemUrl);
				//notShow.submit();
				ajaxGetHtml("payment_method.html?"+"comefrom="+comefrom + "&select_index=1&"+payForItemUrl,$("#content"));
			}else if(comefrom == "invoice"){
				if(invoiceId.length < 1){
					returnSelectIndex = 0;
				}
				ajaxGetHtml("payment_method.html?bill_id="+returnBillId+"&client_id="+returnClientId+"&select_index="+returnSelectIndex+"&comefrom="+comefrom ,$("#content"));
				if(keyType * 1 == 0 && invoiceId.length > 0){
					window.open("https://www.paypal.com/us/cgi-bin/?cmd=_pay-inv&id="+ invoiceId);
				}
			}else if(comefrom == "payfororderfee"){
				var notShow = 	$("#notShow");
				var action = notShow.attr("action");
		 
				notShow.attr("action",action+"?"+payForOrderFeeUrl);
	 
				notShow.submit();
				ajaxGetHtml("payment_method.html?"+"comefrom="+comefrom + "&select_index=1&"+payForOrderFeeUrl,$("#content"));
			}else if(comefrom == "payforebayitem"){
				var notShow = 	$("#notShow");
				var action = notShow.attr("action");
				//payforebayitem不需要跳转的时候就打开Paypal
				//notShow.attr("action",action+"?"+payForEbayItemUrl);
				//notShow.submit();
				ajaxGetHtml("payment_method.html?"+"comefrom="+comefrom + "&select_index=1&"+payForEbayItemUrl,$("#content"));
			}
		}
		
		//pay for item javascript
		function payForItemFormSubmit(){
			var notShow = 	$("#notShow");
			notShow.attr("action","https://www.paypal.com/cgi-bin/webscr"+"?"+payForItemUrl);
			 

			
			notShow.submit();
		}
		function payForEbayItemFormSubmit(){
			var notShow = 	$("#notShow");
	 		notShow.attr("action","https://www.paypal.com/cgi-bin/webscr"+"?"+payForEbayItemUrl);
			notShow.submit();
		}
		jQuery(function($){
			 
			//$(".showTitle").css("background-position","0 -21");
	 	 
 
		 $(".add").live("click",function(){
				 addTr();
		 })
		 countTotalPrice();
		 countEbayTotalPrice();
		 // 计算所有的total 然后相加
		 $(".countQuantity").live("keyup",function(){
			// 做输入的验证 
			var _this = $(this);
			fixInputValue(_this,"int");
			countTotalPrice();
		 })
		 $(".countPrice").live("keyup",function(){
			var _this = $(this);
			fixInputValue(_this);
			countTotalPrice();

		 })
		 //ebay 
		 $(".countQuantity_ebay").live("keyup",function(){
			// 做输入的验证 
			var _this = $(this);
			fixInputValue(_this,"int");
			countTotalPriceEbay();
		 })
		 $(".countPrice_ebay").live("keyup",function(){
			var _this = $(this);
			fixInputValue(_this);
			countTotalPriceEbay();

		 })
		 //tips
		 if('<%= isInvoiceOpen%>' === "true"){
			 doPayForInvoice('<%= billIdComeFromUrl%>' , '<%= payerEmailComeFromUrl%>');
		 }
		 if('<%= paymentState%>' === "true"){
			  initPayMentCompelte();
		 }
		
		})
		function initPayMentCompelte(){
			$("#paymentcomplete").css("display","block");
			$("#paymenting").css("display","none");
			 
			$("#shipping_payComplete").html(billRowJson["shipping"] );
			$("#currency_payComplete").html(billRowJson["currency_code"]);
			
			$("#discount_payComplete").html(billRowJson["discount_amount_cart"]);
			var sum = setPaymentCompleteItemList(itemRowJson);
		 
			$("#total_payComplete").html(parseFloat(sum) + parseFloat(billRowJson["shipping"])  - parseFloat(billRowJson["discount_amount_cart"]));
			
		}
		function  countTotalPriceEbay(_this){
		 
			 
			var shippingFee = $("#shippingFee_ebay") ;
			var shippingFeeValue =parseFloat( shippingFee.val());
			var trs = $("tr",$("#tbody_ebay"));
			var sum = 0 ;
			trs.each(function(){
				var tr = $(this);
				var countPrice = parseFloat($(".countPrice_ebay",tr).val());
				var countQuantity = parseFloat($(".countQuantity_ebay",tr).val());
			 
				sum += (countPrice * countQuantity);
			})
			if(_this){
				fixInputValue($(_this));
			}
			
		 
			 
			$("#amount_ebay").val(decimal(sum  + shippingFeeValue ,2));
		}
		function fixInputValue(_this,intFlag){
			_this = $(_this);
			var value = _this.val();
			var fixValue ;
			if(intFlag){
				fixValue = value.replace(/[^0-9]/g,'');
				_this.val(fixValue * 1);
			}else{
				
				 fixValue = value.replace(/[^0-9.]/g,'');
				if(fixValue.length < 1){
					fixValue = 0;
				}
				if( fixValue.length > 1 && (fixValue+"").indexOf("0") == 0&& (fixValue+"").indexOf(".") != 1){
					fixValue = fixValue.substr(1);
				}
			
				_this.val((fixValue));
			}
			
	}
	function addTr(itemNumber , itemName , amout , quantity,target){
		var tbody = $("#tbody");
		if(target){
			tbody = $("#tbody" + target);
			 
		}else{
			target = "";
		}
		
		tbody.append(createTr(itemNumber , itemName , amout , quantity,target));
		fixInputName(target);
	}
	 function deleteTr(_this,target){
		 var tbody  = $("#tbody");
		 if(target === "_ebay"){
			 tbody = $("#tbody" + "_ebay");
		 }
		 if($("tr",tbody).length <= 1){showMessage("Not Delete.","alert") ; return ;};
		var node = $(_this).parent().parent();
		node.remove();
		fixInputName(target);
		countTotalPrice();
	}
	function createTr(itemNumber , itemName , amout , quantity,target){
		var tr ="<tr>";
	 	// 如果是有值的时候那么就应该把值赋值上去， 如果没有的话就按照默认的设置
	 	 
	 
	 	var fixItemNumber = itemNumber?itemNumber:"*Product Number";
	 	var fixItemName = itemName?itemName:"*Product Name";
	 	
	 	var fixAmout = amout?amout:"0.0";
	 	var fixQuantity = quantity?quantity:"1";
	 	if(target === "_ebay"){
	 		tr += "<td><input type='text' class='txt' name='item_number' value='"+fixItemNumber+"' onfocus=inputFocusItemNumber(this,'_ebay'); onblur=inputBlurItemNumber(this,'_ebay');></td>"
			tr += "<td><input type='text' class='txt' name='item_name' value='"+fixItemName+"' onfocus=inputFocusItemTitle(this,'_ebay'); onblur=inputBlurItemTitle(this,'_ebay');></td>";
			tr += "<td><input type='text' class='txt countPrice_ebay' name='amount' value='"+fixAmout+"' onfocus='inputFocusAmount(this);' onblur='inputBlurItemAmount(this);'/></td>";
			tr += "<td><input type='text' class='txt countQuantity_ebay' name='quantity' value='"+fixQuantity+"' /></td>" ;
			tr += "<td><span class='delete' onclick=deleteTr(this,'_ebay')></span></td></tr>";
		}else{
	 	//onfocus='inputFocusAmount(this);' onblur='inputBlurItemAmount(this);'
	 		 
	 
			tr += "<td><input type='hidden' class='txt' name='item_number' value='"+fixItemNumber+"' onfocus='inputFocusItemNumber(this);' onblur='inputBlurItemNumber(this);'/><input type='text' class='txt' name='item_name' value='"+fixItemName+"' onfocus='inputFocusItemTitle(this);' onblur='inputBlurItemTitle(this);'/></td>";
			tr += "<td><input type='text' class='txt countPrice' name='amount' value='"+fixAmout+"' onfocus='inputFocusAmount(this);' onblur='inputBlurItemAmount(this);'/></td>";
			tr += "<td><input type='text' class='txt countQuantity' name='quantity' value='"+fixQuantity+"' /></td>" ;
			tr += "<td><span class='delete' onclick='deleteTr(this)'></span></td></tr>";
		}
		return $(tr);
	}
	function fixInputName(target){
		var array = ['item_number','item_name','amount','quantity'];
		var tbody = $("#tbody");
		if(target === "_ebay"){
			tbody = $("#tbody"+"_ebay");
		}
		 
		 var trs = $("tr",tbody);
		 var length = trs.length ;
		 trs.each(function(index){
			var tr = $(this);
			var inputs  = $("input",tr);
			for(var j = 0 , count = inputs.length ;j < count ;j++){
					var _input  = $(inputs[j]);
					_input.attr("name",array[j]+"_" + (index+1));
					if(target != "_ebay" && array[j] === "item_number"){
						_input.val(index+1);
					}
			}
			 
		 })
	}
	function fixAddAndRemoveButton(){
		
	}
	function countTotalPrice(_this){
	 
	 
		var shippingFee = $("#shippingFee") ;
		var shippingFeeValue =parseFloat( shippingFee.val());
		var trs = $("tr",$("#tbody"));
		var sum = 0 ;
		trs.each(function(){
			var tr = $(this);
			var countPrice = parseFloat($(".countPrice",tr).val());
			var countQuantity = parseFloat($(".countQuantity",tr).val());
		 
			sum += (countPrice * countQuantity);
		})
		if(_this){
			fixInputValue($(_this));
		}
		
	 
		 
		$("#amount").val(decimal(sum  + shippingFeeValue ,2));
	}
	function countDiscount(_this){
		if(_this){
			fixInputValue($(_this));
		}
		var trs = $("tr",$("#tbody"));
		 
		var sum = 0 ;
		trs.each(function(){
			var tr = $(this);
			var countPrice = parseFloat($(".countPrice",tr).val());
			var countQuantity = parseFloat($(".countQuantity",tr).val());
			sum += (countPrice * countQuantity);
		});
		 
		var  amount = $("#amount").val();
		if(parseFloat(amount) >  sum){showMessage("Total Amount > All Items","alert");countTotalPrice(); return; }
		var value = sum - amount;
		$("#discount").val(decimal(parseFloat(value) ,4));


	}
	function decimal(num,v){
		var vv = Math.pow(10,v);
		var value =  Math.round(num*vv)/vv + "";
		if(value === "NaN"){
			value = "N/A";
		}
		return value;
	} 
	// 检查是不是有包含0 的输入项目
		function volidatePayForItem(){
		 var trs = $("tr",$("#tbody"));
		 var flag = false;
		 var sellerId = $("#sellerId").val();
		 var buyerId = $("#buyerId").val();
		 var value = "";
 
		value += ",seller_id" + ":" +sellerId 	

		value +=",auction_buyer_id" +":" +  buyerId;
	 
		var custom = $("#custom");
		var s = custom.val();
		custom.val(s + value);


		 for(var index = 0 ; index < trs.length ; index++ ){
			 var _this = $(trs[index]);
			 var countPrice = parseFloat($(".countPrice",_this).val());
			 var countQuantity = parseFloat($(".countQuantity",_this).val());
			 if(countPrice * countQuantity <= 0 ){
				showMessage("Input Invalidate","alert");
				 return false;
			 };
		 }
		
		}
		function inputFocusItemNumber(_this,target){
			var node = $(_this);	

			
			var value = node.val();
			if(target === "_ebay"){
				//表示的是EbayItem
				if(value == "*ebayItem Number"){
					node.val("").css("color","black");
				} 
			}else{
				if(value == "*Item Number"){
					node.val("").css("color","black");
				} 
			}
		}
		function inputBlurItemNumber(_this,target){
			var node = $(_this);
			var value = node.val();
			if(target === "_ebay"){
				if(value.length < 1){
					node.val("*ebayItem Number");
					node.css("color","seagreen");
				}
			}else {
				if(value.length < 1){
					node.val("*Item Number");
					node.css("color","seagreen");
				}
			}
		}
		function inputFocusItemTitle(_this,target){
		 
			var node = $(_this);
			var value = node.val();
			 
			if(target === "_ebay"){
				if(value == "*ebayItem Title"){
					node.val("").css("color","black");
				} 
			}else{
				if(value == "*Product Name"){
					node.val("").css("color","black");
				} 
			}
		}
		function inputBlurItemTitle(_this,target){
			var node = $(_this);
			var value = node.val();
			if(target === "_ebay"){
				if(value.length < 1){
					node.val("*ebayItem Title").css("color","seagreen");
				}	
			}else{
				if(value.length < 1){
					node.val("*Product Name").css("color","seagreen");
				}
			}
		}

		function headProcess(index){
			index  = index * 1;
			$(".showTitle").css("background-position","0 "+(-21 * index ));
		}
		function inputFocusAmount(_this){
			var node = $(_this);
			if($.trim(node.val()).indexOf("*") != -1 || $.trim(node.val()) =="0.0" ){
				node.val("").css("color","black");
			}
		}
		function inputBlurItemAmount(_this){
			fixInputValue($(_this));
			var node = $(_this);
			if($.trim(node.val()) *1 == 0){
				node.val("0.0").css("color","seagreen");
			}
		}
		function payForOrderFeeSubmit(){
			var notShow = 	$("#notShow");
			var action = notShow.attr("action");
	 
			notShow.attr("action",action+"?"+payForOrderFeeUrl);
 
			notShow.submit();
		}


		function addEbayItem(){
			addTr("*ebayItem Number" , "*ebayItem Title" , "0.0" , "0","_ebay");
		}
		function countDiscount_ebay(_this){
			if(_this){
				fixInputValue($(_this));
			}
			var trs = $("tr",$("#tbody_ebay"));
			 
			var sum = 0 ;
			trs.each(function(){
				var tr = $(this);
				var countPrice = parseFloat($(".countPrice_ebay",tr).val());
				var countQuantity = parseFloat($(".countQuantity_ebay",tr).val());
				sum += (countPrice * countQuantity);
			});
			 
			var  amount = $("#amount_ebay").val();
			if(parseFloat(amount) >  sum){showMessage("Total Amount > All Items","alert");countEbayTotalPrice(); return; }
			var value = sum - amount;
			$("#discount_ebay").val(decimal(parseFloat(value) ,4));
		}
		function countEbayTotalPrice(_this){
			 
		 
		 
			var shippingFee = $("#shippingFee_ebay") ;
			var shippingFeeValue =parseFloat( shippingFee.val());
			var trs = $("tr",$("#tbody_ebay"));
			var sum = 0 ;
			trs.each(function(){
				var tr = $(this);
				var countPrice = parseFloat($(".countPrice_ebay",tr).val());
				var countQuantity = parseFloat($(".countQuantity_ebay",tr).val());
			 
				sum += (countPrice * countQuantity);
			})
			if(_this){
				fixInputValue($(_this));
			}
			
			 
			 
			$("#amount_ebay").val(decimal(sum  + shippingFeeValue ,2));
		}
		function createInvoiceFormNotCreate(name , amount , discount , shipping,custom ){
			var form = $("#invoiceNotCreate");
			$("input[name='item_name_1']",form).val(name);
			$("input[name='amount_1']",form).val(amount);
			$("input[name='discount_amount_cart']",form).val(discount);
			$("input[name='shipping_1']",form).val(shipping);
			$("input[name='custom']",form).val(custom);
		}
		function sendInvoiceNotCreate(){
			var form = $("#invoiceNotCreate");
		 
			form.submit();
		}
		</script>
	</head>
	<body>
	<div class="cssDivschedule" style="">
 		<div class="ui-corner-all innerDiv">
 			 Please Hold On.....
 		</div>
	</div>
		<div class="main">
			<form id="invoiceNotCreate" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
				<input type="hidden" name="cmd" value="_cart" /> 
				<input type="hidden" name="upload" value="1" /> 
				 <input type="hidden" name="business" value="vvmecom@gmail.com" />  
				<input type="hidden" name="custom" id="custom_ebay" value=""/>
				<input type="hidden"  name="item_number_1" value="1"/>
				<input type="hidden"  name="item_name_1"  />
				<input type="hidden"  name="amount_1" />
				<input type="hidden"  name="quantity_1" value="1" />
				<input type="hidden" name="discount_amount_cart" />
				<input type="hidden" name="shipping_1" />
				 
			 	<input type="hidden" name="return" value="http://www.1you.com/paypal/pay.html?payment_state=true" /> 
				
			</form>
		 	<form id="notShow" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank"></form>
			 <div class="payment_select">
		 
			 		<h3>Make Your Payment</h3> 
					<p  style="margin-top:5px;line-height:24px;">&nbsp;&nbsp;&nbsp;This payment page is ONLY used to make payment with/without an Invoice, or pay for an existing order.
					 If you are going to make payment for your shopping cart items, please <a href="http://www.vvme.com/shopping-cart" target="_blank" class="show_a"><img width="95" height="19" title=" Checkout " alt="Checkout" src="common/pay/pay_ment/button_checkout.gif" /></a>. We accept the following payment method:  
					 </p>
					 <p style="clear:both;line-height:50px;height:50px;margin-top:-9px;text-algin:center;padding-left:50px;">
					 	<span class="bank_transfer"></span>
						<span class="west_union"></span>   
						<span class="paypal2"><img src="common/pay/pay_ment/bnr_shopNowUsing_150x40.gif" width="112px" height="31px;"/></span>  
						<span class="creditcar">
							<img src="common/pay/pay_ment/directpaymentp6.gif" width="112px" height="38px;"/>
						</span>
						<span style="display:block;clear:both;"></span>
					 </p>
			 	<div style="display:block;" id="paymenting">
			 		<div style="border:0px solid silver;height:25px;">
			 			<span class="stepSpan step_one" >Select what you are paying for</span>
						<span class="stepSpan step_two">Review Payment Detail</span>
						<span class="stepSpan step_three">Select Payment Method</span>
			 		</div>
			 		
					<h1 class="showTitle">
					</h1>
					<div style="border:0px solid silver;padding:10px;" id="content">
						<h2 style="text-indent:5px;background:#ebebeb;height:35px;line-height:35px;"  >Step 1 Of 3 : Select what you are paying for.</h2>

					       <div id="tabs">
							 <ul>
							 	 <li><a href="#pay_for_ebay_item" ><span style="font-weight:bold;">Pay For Ebay Item</span></a></li>	
								 <li><a href="#pay_for_invoice_div"><span style="font-weight:bold;">Pay For Invoice</span></a></li>	
								 <li><a href="#pay_for_without_invoice"><span style="font-weight:bold;">Pay Without Invoice</span></a></li>	
								 <!--  <li><a href="#pay_for_order_fee">Pay For Existing Order</a></li>-->
								 
							 </ul>
							 	<div id="pay_for_ebay_item">
									 <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="ebayformValue" target="_blank">
											<input type="hidden" name="cmd" value="_cart" /> 
											<input type="hidden" name="upload" value="1" /> 
											
											<input type="hidden" name="custom" id="custom_ebay" value=""/>
											<input type="hidden" name="business" value="vvmecom@gmail.com" />  
											<input type="hidden" name="return" value="" /> 
										<table class="shop" >
											<thead>
											<tr >
												<th width="250px">ebayItem Number <span id="whatisitemnumber" style="color:#084482;cursor:pointer;">?</span></th>
									 
												<th width="250px">ebayItem Title</th>
												<th width="200px">Item Price</th>
												<th width="80px">Quantity</th>
												<th width="100px"><span class='add_ebay' onclick="addEbayItem();"></span></th>
											 </tr>
											 </thead>
											 <script type="text/javascript">
											 $('#whatisitemnumber').poshytip({
													className: 'tip-darkgray',
													content: "<img src='common/pay/pay_ment/whatIsItemNumber.jpg' width='277px' height='300px' />",
													bgImageFrameSize: 9
												});
											 </script>
											 <tbody id="tbody_ebay">
												 <tr>
													<td><input type="text" class="txt" name="item_number_1" value="*ebayItem Number" onfocus="inputFocusItemNumber(this,'_ebay');"  onblur="inputBlurItemNumber(this,'_ebay');"/></td>
													<td><input type="text" class="txt" name="item_name_1" value="*ebayItem Title" onfocus="inputFocusItemTitle(this,'_ebay');" onblur="inputBlurItemTitle(this,'_ebay');"/></td>
													<td><input type="text" class="txt countPrice_ebay" name="amount_1" value="0.0" onfocus="inputFocusAmount(this);" onblur="inputBlurItemAmount(this);"/></td>
													<td><input type="text" class="txt countQuantity_ebay" name="quantity_1" value="1" /></td>
													<td>
														<span class="delete" onclick="deleteTr(this,'_ebay')"></span>
													</td>
												 </tr> 
												 
						
											 </tbody>
											 <tfoot>
												<tr>
													 
													<td colspan="5" style="text-align:left;">
														 Buyer ebay Id:<input type="text" style="width:140px;" id="buyer_id" />
 														<input type="hidden" name="discount_amount_cart"  value="0.0" id="discount_ebay" />
 													</td>
												</tr>
												<tr>
													<td colspan="5" style="text-align:right;">
														Shipping Fee :  <input type="text" id="shippingFee_ebay" name="shipping_1"  onkeyup="countEbayTotalPrice(this)" value="0.0" class="" style="border:none;border-bottom:1px solid silver;width:100px;"/>
													</td>
												</tr>
												<tr>
													<td colspan="5" style="text-align:right;">
														Total Amount :  <input type="text" id="amount_ebay" disabled onkeyup="countDiscount_ebay()" value="0.0" class="" style="border:none;border-bottom:1px solid silver;width:100px;"/>
													</td>
												</tr>
											 
												<tr>
													<td colspan="5" style="text-align:right;">
													Currency:
														<select style="width:100px;" name="currency_code" id="currency_code_ebay">
															<%
																ArrayList currency = currencyKey.getCurrency();
																for (int i=0; i<currency.size(); i++){
																	%>
																		 <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>"><%=currency.get(i)%></option>
																	<%
																}
														 %>
														</select>
													</td>
													 
												</tr>
												 
											 </tfoot>
										</table>
									</form>
 
									<div  class="buttonShow" style="text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;padding-top:5px;">
										<input type="button"  class="nextButton" onclick="getBillDetail();"/> &nbsp;
									</div> 
							 </div>
							 <div id="pay_for_invoice_div" >
							 	
								<form id="pay_for_invoice">
									<input type="hidden" name="type" value="pay_for_invoice"/>
									<table>
										<tr>
											<td class="td_right" ><b>Invoice Number :</b></td>
											<td><input type="text" class="invoiceInput" id="invoice_id" name="bill_id"/></td>
										</tr>
										<tr>
											<td class="td_right"><b>Client Email:</b></td>
											<td><input type="text" class="invoiceInput" id="payer_email" name="client_id" /></td>
										</tr>
										 
									</table>
								</form>
								<div  class="buttonShow" style="text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;padding-top:5px;">
									<input type="button"  class="nextButton" onclick="getBillDetail();"/> &nbsp;
								</div> 
							 </div>
							 
							 <div id="pay_for_without_invoice"  >
									 <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="formValue" target="_blank">
											<input type="hidden" name="cmd" value="_cart" /> 
											<input type="hidden" name="upload" value="1" /> 
											 <input type="hidden" name="business" value="vvmecom@gmail.com" />  
											<input type="hidden" name="custom" id="custom" value=""/>
										 
											<input type="hidden" name="return" value="" /> 
										<table class="shop" >
											<thead>
											<tr >
												 
												<th width="250px">Product Name</th>
												<th width="200px">Product Price</th>
												<th width="80px">Quantity</th>
												<th width="100px"><span class='add'></span></th>
											 </tr>
											 </thead>
											 <tbody id="tbody">
												 <tr>
													 
													<td>
														<input type="hidden" class="txt" name="item_number_1" value="1" onfocus="inputFocusItemNumber(this);"  onblur="inputBlurItemNumber(this);"/>
														<input type="text" class="txt" name="item_name_1" value="*Product Name" onfocus="inputFocusItemTitle(this);" onblur="inputBlurItemTitle(this);"/>
													</td>
													<td><input type="text" class="txt countPrice" name="amount_1" value="0.0" onfocus="inputFocusAmount(this);" onblur="inputBlurItemAmount(this);"/></td>
													<td><input type="text" class="txt countQuantity" name="quantity_1" value="1" /></td>
													<td>
														<span class="delete" onclick="deleteTr(this)"></span>
													</td>
												 </tr> 
												 
						
											 </tbody>
											 <tfoot>
												 
												<tr>
													<td colspan="5" style="text-align:right;">
														<input type="hidden" id="discount" name="discount_amount_cart"  onkeyup="countTotalPrice(this)" value="0.0" class="" style="border:none;border-bottom:1px solid silver;width:100px;"/>
														Shipping Fee :  <input type="text" id="shippingFee" name="shipping_1"  onkeyup="countTotalPrice(this)" value="0.0" class="" style="border:none;border-bottom:1px solid silver;width:100px;"/>
													</td>
												</tr>
												<tr>
													<td colspan="5" style="text-align:right;">
														Total Amount :  <input type="text"  disabled id="amount" onkeyup="countDiscount()" value="0.0" class="" style="border:none;border-bottom:1px solid silver;width:100px;"/>
													</td>
												</tr>
											 
												<tr>
													<td colspan="5" style="text-align:right;">
													Currency:
														<select style="width:100px;" name="currency_code" id="currency_code">
															<%
																
																for (int i=0; i<currency.size(); i++){
																	%>
																		 <option value="<%=currencyKey.getCurrencyById( currency.get(i).toString() )%>"><%=currency.get(i)%></option>
																	<%
																}
														 %>
														</select>
													</td>
													 
												</tr>
											 </tfoot>
										</table>
									</form>
									<div  class="buttonShow" style="text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;padding-top:5px;">
										<input type="button"  class="nextButton" onclick="getBillDetail();"/> &nbsp;
									</div> 
							 </div>
							<!--  
							<div id="pay_for_order_fee">
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="payfororderfee" target="_blank">
								<input type="hidden" name="cmd" value="_cart" /> 
								<input type="hidden" name="upload" value="1" /> 
								<input type="hidden" name="business" value="vvmecom@gmail.com" /> 
								<input type="hidden" name="custom" id="custom" value="order_source:ORDER"/>
								<input type="hidden"   name="item_number_1" value="For Order"/>
								<input type="hidden" id="item_name_1" name="item_name_1" value="For Order:"/>
								<input type="hidden" id="amount_1" name="amount_1" value="0.0" />
								<input type="hidden" id="quantity_1" name="quantity_1" value="1" />
								<table class="shop" >
									<thead>
									<tr >
										<th width="" colspan="4" style="text-align:center;">Pay For Exsiting Order</th>
										 
									 </tr>
									 </thead> 
									 <tbody>
										<tr>
											<td>
												<span>For Order</span>
											</td>
											<td>
												Order ID : <input type="text" class="txt" id="order_id" onkeyup="fixInputValue(this,true)"/>
											</td>
											<td>
												Amount: <input type="text" class="txt" id="order_fee" value="0" onkeyup="fixInputValue(this);"/>
											</td>
											<td>
												<span>1</span>
											</td>
										</tr>
									 </tbody>
									 <tfoot>
										<tr>
											<td>
											Currency:go
												<select name="currency_code" id="payfororder_currency_code">
													<%
																ArrayList currencyTemp = currencyKey.getCurrency();
																for (int i=0; i<currency.size(); i++){
																	%>
																		 <option value="<%=currencyKey.getCurrencyById( currencyTemp.get(i).toString() )%>"><%=currencyTemp.get(i)%></option>
																	<%
																}
														 %>
												</select>
											</td>
											<td colspan="3"  >
												
											 
												 
												
											</td>
										</tr>
									 </tfoot>
								</table>
				
								</form>
								<div  class="buttonShow" style="text-align:right;text-indent:5px;background:#ebebeb;height:35px;line-height:35px;margin-top:4px;padding-top:5px;">
									 <input type="button"  class="nextButton" onclick="getBillDetail();"/> &nbsp;
								 </div> 
							</div> 	
							-->
						 </div>
						
						
						
					 
					</div>
				</div>	
				<div id="paymentcomplete" style="border:1px solid silver;padding-bottom: 10px;padding-left: 20px;display:none;">
					<div class="paymentcompleteInfo" style="border-bottom: 1px dashed  silver ;width:705px;padding-bottom:5px;">
						
						 <p class="paymentSuccess">PayMent Complete.</p>
						 <ul class="paymentSuccessUl">
							 <li>1.Your order will be deal with as soon as your payment go through.
							 <li>2.If you have any porblems when make the payment please contact us asap.</li>
							 <li>3.For more details about your order please check below information.</li>
						 </ul>
						 <div style="clear:both;"></div>
						 
					</div>	
					<div id="shippingInfoTable" style="width:709;border:0px solid silver;margin-top:10px;">
							<table class="successtable">
							<thead>
			  		 				<tr>
			  		 					<th width="60%">Description</th>
			  		 					<th width="16%">Quantity</th>
			  		 					<th width="16%">Unit price</th>
			  		 					<th>Amount</th>
			  		 				</tr>
			  		 			</thead>
								<tbody id="successTbody">
									 <tr>
									 	<td class="name">amen</td>
									 	<td class="t">amen</td>
									 	<td class="t">amen</td>
									 	<td class="t">amen</td>
									 	
									 </tr>

								</tbody>
							    <tfoot>
					  		 				<tr>
					  		 					<td style="text-align:left;text-indent:10px;" id="payforItemNote">
					  		 						 
					  		 					</td>
					  		 					<td colspan="3" style="border-left:1px solid silver;">
					  		 						 <table style="width:100%;border-collapse:collapse;">
					  		 						 	<tr style="background:#e8f1fa">
					  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Currency</td>
					  		 						 		<td id="currency_payComplete"></td>
					  		 						 	</tr>
					  		 						 	<tr>
					  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Discount</td>
					  		 						 		<td  id="discount_payComplete"></td>
					  		 						 	</tr>
					  		 							<tr  style="background:#e8f1fa">
					  		 						 		<td id="" style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" >Shipping Fee</td>
					  		 						 		<td  id="shipping_payComplete"></td>
					  		 						 	</tr>
					  		 						 	<tr>
					  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Total</td>
													 		<td  id="total_payComplete"></td>
					  		 						 	</tr>
					  		 						 </table>
					  		 					</td>
					  		 					
					  		 				</tr>
					  		 			</tfoot>
					  		</table>
			  		</div>
				</div>
					<div style="padding-left:26px;border:0px solid silver;width:717px;" id="bottomInfo">
 						<p style="height:25px;line-height:25px;">
						 	If you have any problems when you make the payment, do not hesitate contact at&nbsp;
						 	<a onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 && window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('http://www.mydepots.com/livehelp/client.php?locale=en&group=1&srcweb=vvme.com&url='+escape(document.location.href)+'&referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;" title="Live Help" target="_blank" href="http://www.mydepots.com/livehelp/client.php?locale=en&group=1">
								<img width="86" height="24" alt="" src="common/pay/pay_ment/livehelp.gif">
							</a>
						 </p>
						<div class="safetyPayment">
							<div class="DivLeft">
								<span class="mcafee"></span>
							</div>
							<p class="pRight"> Our website can secure your private information using a SSL Certificate. Information exchanged with any address beginning with https is encrypted using SSL before transmission.</p>
 							<div style="clear:both;"></div>
						</div>
						
						<div class="safetyPayment">
							<div class="DivLeft">
							
								<span class="paypalVerified"></span>
							</div>
							<p class="pRight">Visionari e-Commerce LLC has been verified as the owner or operator of the website located at WWW.VVME.COM. Official records confirm Visionari e-Commerce LLC as a valid business.</p>
							<div style="clear:both;"></div>
						</div>
					</div>

			 </div>
			  
			  <script type="text/javascript">
					$("#tabs").tabs();
					function showMessage(_content,_state){
						var o =  {
							state:_state || "succeed" ,
							content:_content,
							corner: true
						 };
						 var  _self = $("body"),
						_stateBox = $("<div style='font-size:14px;'/>").addClass("ui-stateBox").html(o.content);
						_self.append(_stateBox);	
						if(o.corner){
							_stateBox.addClass("ui-corner-all");
						}
						if(o.state === "succeed"){
							_stateBox.addClass("ui-stateBox-succeed");
							setTimeout(removeBox,1500);
						}else if(o.state === "alert"){
							_stateBox.addClass("ui-stateBox-alert");
							setTimeout(removeBox,2000);
						}else if(o.state === "error"){
							_stateBox.addClass("ui-stateBox-error");
							setTimeout(removeBox,2800);
						}
						_stateBox.fadeIn("fast");
						function removeBox(){
							_stateBox.fadeOut("fast").remove();
					 }
					}
					contentHtml = $("#content").html();
			  </script>
			 </div>
		</div>
	</body>
</html>