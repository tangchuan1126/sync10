<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../../include.jsp"%>
<%@page import="com.cwc.app.key.BillTypeKey"%>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice Detail</title>
	 


<style type="text/css">
	 
	*{font-size:12px;color:black;font-family: Verdana,Arial,sans-serif;}
   table tr td{line-height:25px;height:25px;}
   .td_left{border:1px solid red;width:100px;text-align:right;}
   .set{border:2px #999999 solid;padding:2px;width:90%;word-break:break-all;margin-top:10px;margin-top:5px;line-height:18px;-webkit-border-radius:5px;-moz-border-radius:5px; margin-bottom: 10px;} 
	span.addNewSpan{display:block;width:70px;height:20px;float:right;}
	ul.addressList{list-style-type:none;margin-left:10px;}
	ul.addressList li {text-indent:5px;line-height:25px;height:25px;margin-top:2px;border-bottom:1px dotted silver;cursor:pointer;}
	
	p.selectType{border:1px solid silver;background:#E6F3C5;height:35px;line-height:35px;}
	ul.type{border:1px solid silver;background:#E6F3C5;list-style-type:none;height:35px;line-height:35px;}
	ul.type li {margin-top:4px;float:left;border:1px solid silver;width:100px;line-height:25px;height:25px;margin-left:10px;text-align:center;cursor:pointer;}
	ul.type li.on{background:white;}
	span.button{-moz-transition: all 0.218s ease 0s; -moz-user-select: none;background: -moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5;border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 2px 2px 2px 2px;color: #444444; cursor: pointer;font-size: 14px;font-weight: bold;height: 27px;line-height: 27px; min-width: 54px;
    outline: medium none;padding: 0 8px;text-align: center;display:block;margin-top:10px;
   }
   p.say {margin-top:8px;width:500px;}
   .infoList table td{}
   table.addressInfo td.right{text-align:right;font-weight:bold;color:#0066FF;}
   table.basic{border-collapse :collapse ;border:1px solid silver;}
   
   table.basic th{background:#e8f1fa;width:170px;font-size:12px; border:1px solid silver;}
   table.basic td{background:white;width:200px;text-indent:30px;border:1px solid silver;}
   table.itemTable{border-collapse :collapse ;border:1px solid silver;width:100%;}
   table.itemTable th{background:#e8f1fa;font-size:12px;font-weight:normal;line-height:35px;height:35px;border:1px solid silver;}
   table.itemTable td{line-height:30px;height:30px;border:1px solid silver;}
   table.itemTable td.name{text-indent:15px;}
   table.itemTable td.t{text-indent:10px;}
   table.itemTable tfoot  td{height:25px;border:none;text-align:center;}
   ul.itemDetail{list-style-type:none;margin-left:20px;margin-top:-2px;margin-bottom:5px;}
   ul.itemDetail li{border:0px solid silver;height:14px;line-height:14px;}
   input.noborder{width:250px;border:none;border-bottom:1px solid silver;}
</style>
<%
	long billId = StringUtil.getLong(request,"bill_id");
	DBRow billRow = billMgrZr.getBillByBillId(billId);
	DBRow[] items = billMgrZr.getItemsByBillId(billId);
	if(billRow.get("bill_type",0) == BillTypeKey.order ){
		DBRow forOrder = new DBRow();
		forOrder.add("name","Pay For Order ["+billRow.get("porder_id",0l)+"] Fee ");
		forOrder.add("quantity","1");
		forOrder.add("unit_price",billRow.get("order_fee",0.0d));
		forOrder.add("amount",billRow.get("order_fee",0.0d));
		items = new DBRow[1];
		items[0] = forOrder;
		
		
	}else if(billRow.get("bill_type",0) == BillTypeKey.shipping){
		if(items == null || (items != null && items.length < 1)){
			DBRow forShippingFee = new DBRow();
			forShippingFee.add("name","For Bill ["+billRow.get("bill_id",0l)+"] Shipping Fee");
			forShippingFee.add("quantity","1");
			forShippingFee.add("unit_price",0.0d);
			forShippingFee.add("amount",0.0d);
			items = new DBRow[1];
			items[0] = forShippingFee;
		}
	}
	DBRow userInfo = waybillMgrZR.getUserNameById(billRow.get("create_adid",0l));

%>
<script type="text/javascript">
	jQuery(function($){
		init();
	})
	function init(){
		//address info
		var addressInfo = '<%= billRow.getString("address_name")%>' + "," + '<%= billRow.getString("address_street")%>'
			+","+'<%= billRow.getString("address_state")%>' + "," + '<%= billRow.getString("address_state")%>'+"," +  '<%= billRow.getString("address_country")%>';

		$("#addressInfoStr").html(addressInfo);
	}
	 
</script>
</head>
<body>
     <div style="margin:0px auto;padding:2px;border:0px solid red;">
			 <div style="width:40%;float:left;border:0px solid green;">
			  		 	<img src="common/pay/visionari.gif" />
					 	<h3 style="color: #333333;margin-top: 10px;">Visionari LLC</h3>
					 	<p><span id="employe_name_" style="font-weight:bold;"> <%= (userInfo != null?userInfo.getString("account"):"")%></span><span style="margin-left:10px;" id="create_time_"><%= billRow.getString("create_date") %> </span></p>
					    <p style="" id="account_name_"><%= billRow.getString("account_name") %></p>
					    <p style="margin-top: 10px;" id="account_"><%= billRow.getString("account") %></p>
					   
			  		 </div>
			  		 <div style="width:55%;float:right;border:0px solid red;margin-bottom:30px;">
			  		 <!--  invoice detail Show -->
			  			<h1 style="color: #CCCCCC;text-transform:uppercase;font-size:16px;text-align:right;margin-right:20px;margin-top:10px;">Invoice</h1>
			  		 	<table id="invoiceDetails" class="basic" summary="Invoice details" style="float:right;margin-top:20px; ">
							<tbody>
								<tr>
									<th>Invoice number</th>
									 
									<td><%=  billRow.get("bill_id",0l)%> &nbsp;</td>
								</tr>
								<tr>
									<th>Invoice date</th>
									<td><%=billRow.getString("create_date") %> &nbsp;</td>
								</tr>
								<tr>
									<th>Payment terms</th>
									<td>Due on receipt</td>
								</tr>
								
								<tr>
									<th>Due date</th>
									<td><%=billRow.getString("create_date") %> &nbsp;</td>
								</tr>
							</tbody>
						</table>
			  		 </div>
			  		 <div style="clear:both;border:0px solid silver;margin-top:20px; " id="sendto">
			  		  
			  		 	<p class="ui-corner-all" style="width:98%;background:#e8f1fa;padding:5px;text-indent:10px;line-height:35px;height:35px;">
			  		 		 <span style="font-weight:bold;font-size:14px;">Send To : </span> <span id="addressInfoStr"></span>
			  		 	</p>
			  		 </div>
			  		 <div style="border:0px solid silver;margin-top:20px;">
			  		 	 <table class="itemTable">
			  		 			<thead>
			  		 				<tr>
			  		 					<th width="60%">Description</th>
			  		 					<th width="16%">Quantity</th>
			  		 					<th width="16%">Unit price</th>
			  		 					<th>Amount</th>
			  		 				</tr>
			  		 			</thead>
			  		 			<tbody id="tbodyView">
			  		 				<%
			  		 					if(items != null && items.length > 0){
			  		 						for(int index = 0 , count = items.length ; index < count ; index++ ){
			  		 					 	String background  = (index%2 == 0)?"":"#e6f3c5";
			  		 							%>	
			  		 							<tr style="background:<%= background %>;">
			  		 								<td class="name">
			  		 									<%= items[index].getString("name") %>
			  		 								<%
									 			if (items[index].get("product_type",0)==CartQuote.UNION_CUSTOM ){
									 				 DBRow[] un = billMgrZr.getProductUnionByPcid(items[index].get("pc_id",0l)); 
									 				 if(un != null && un.length > 0){
										 				%>
										 				<ul class="itemDetail">
										 				
										 				<% 
									 					 for(DBRow u : un){
										 					%>
										 				<li>├ <%= u.getString("p_name") %> x <%= u.get("quantity",0.0) %>&nbsp;<%= u.getString("unit_name") %></li>	
										 					<%   
										 				 }
										 				%>
										 				 </ul>
										 				<% 
									 				 }
									 			}
									 			%>
			  		 								</td>
			  		 								<td class="t"><%= items[index].getString("quantity") %></td>
			  		 								<td class="t"><%= items[index].get("actual_price",0.0d) %></td>
			  		 								<td class="t"><%= items[index].getString("amount") %></td>
			  		 								
			  		 							</tr>
			  		 							<% 	
			  		 						}
			  		 					}
			  		 				%>
			  		 	 
			  		 			</tbody>
			  		 			<tfoot>
			  		 				<tr>
			  		 					<td style="text-align:left;text-indent:10px;">
			  		 						Note :  
			  		 						<div style="border:1px solid silver;width:300px;margin-left:5px;height:90px;">
			  		 							<%= billRow.getString("note") %>
			  		 						</div>
			  		 					</td>
			  		 					<td colspan="3" style="border-left:1px solid silver;">
			  		 						 <table style="width:100%;border-collapse:collapse;">
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Currency</td>
			  		 						 		<td id="rate_type_td"><%= billRow.getString("rate_type") %></td>
			  		 						 	</tr>
			  		 						 	<tr >
			  		 						 		<td style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" id="">Subtotal</td>
			  		 						 		<td id="subtotal_td"><%= billRow.get("subtotal",0.0d) %></td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Discount</td>
			  		 						 		<td id="total_discount_td"><%= billRow.get("total_discount",0.0d) %></td>
			  		 						 	</tr>
			  		 							<tr >
			  		 						 		<td id="" style="width:50%;font-weight:bold;text-align:left;text-indent:20px;" >Shipping Fee</td>
			  		 						 		<td id="shipping_fee_td"><%= billRow.get("shipping_fee",0.0d) %></td>
			  		 						 	</tr>
			  		 						 	<tr style="background:#e8f1fa">
			  		 						 		<td style="font-weight:bold;text-align:left;text-indent:20px;" >Total</td>
			  		 						 		<td id="save_td"><%= billRow.get("save",0.0d) %></td>
			  		 						 	</tr>
			  		 						 </table>
			  		 					</td>
			  		 					
			  		 				</tr>
			  		 			</tfoot>
			  		 	 </table>
			  		 	 </div>
			  		  		 
	 </div>
</body>
</html>
