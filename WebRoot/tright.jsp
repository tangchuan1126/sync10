<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.cwc.app.api.AdminLoggerBean"%>
<%@ include file="include.jsp"%> 
<%


TDate tDate = new TDate();
tDate.addDay(-20);

String input_st_date,input_en_date;

input_st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
input_en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();


long ps_id = StringUtil.getLong(request,"ps_id");


//用日期作为key，重新封装
DBRow wait4HandleOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileWait4HandleOrders(input_st_date,input_en_date));
DBRow lackingOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileLackingOrders(input_st_date,input_en_date,ps_id));
DBRow wait4PrintOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileWait4PrintOrders(input_st_date,input_en_date,ps_id));
DBRow doubtOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDoubtOrders(input_st_date,input_en_date,ps_id));
DBRow printedOrdersObject = orderMgr.getDataByDate(orderMgr.getProfilePrintedOrders(input_st_date,input_en_date,ps_id));
DBRow doubtAddressOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDoubtAddressOrders(input_st_date,input_en_date,ps_id));
DBRow doubtCostOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDoubtCostOrders(input_st_date,input_en_date,ps_id));
DBRow doubtPayOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDoubtPayOrders(input_st_date,input_en_date,ps_id));

DBRow disputeRefundOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDisputeRefundOrders(input_st_date,input_en_date,ps_id));
DBRow otherDisputingOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileOtherDisputingOrders(input_st_date,input_en_date,ps_id));
DBRow disputeWarrantyOrdersObject = orderMgr.getDataByDate(orderMgr.getProfileDisputeWarrantyOrders(input_st_date,input_en_date,ps_id));

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
.profile {
	background-attachment: scroll;
	background-image: url(imgs/login_profile.jpg);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
<script type="text/javascript" src="js/popmenu/jquery-1.3.2.min.js"></script>


<style>
a:link {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:visited {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:hover {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}
a:active {
	color: #999999;
	text-decoration: underline;
	font-family: Arial;
	font-size:12px;
}



a.hard:link {
	color:#FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:visited {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:hover {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
a.hard:active {
	color: #FF0066;
	text-decoration: underline;
	font-family: Arial;
	font-size:14px;
}
</style>

</head>

<body >

	
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="zebraTable">



    <tr> 
        <th  class="left-title" style="vertical-align: center;text-align: center;">日 期</th>
        <th  class="left-title" style="vertical-align: center;text-align: center;">待抄单</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">缺货</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">疑问订单</th>
        <th class="right-title" style="vertical-align: center;text-align: center;">疑问地址</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">成本审核</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">疑问付款</th>
		
		<th  class="right-title" style="vertical-align: center;text-align: center;">争议退款</th>
		<th  class="right-title" style="vertical-align: center;text-align: center;">争议质保</th>
		<th  class="right-title" style="vertical-align: center;text-align: center;">其他争议</th>
		
        <th  class="right-title" style="vertical-align: center;text-align: center;">待打印</th>
        <th  class="right-title" style="vertical-align: center;text-align: center;">已打印</th>
  </tr>
<%
DBRow date[] = orderMgr.getProfileDate(input_st_date,input_en_date);
for (int i=0; i<date.length; i++)
{

%>

    <tr > 
      <td height="30" align="center" valign="middle" ><%=date[i].getString("date")%></td>
      <td align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&handle=1&handle_status=0&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=0&search_field=0&cmd=" target="_blank" <%=getCount(wait4HandleOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(wait4HandleOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td align="center" valign="middle"><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&handle=0&handle_status=0&product_type_int=0&product_status=1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(lackingOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(lackingOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&handle=0&handle_status=1&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(doubtOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(doubtOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&amp;st=<%=date[i].getString("date")%>&amp;en=<%=date[i].getString("date")%>&amp;business=&amp;handle=0&amp;handle_status=5&amp;product_type_int=0&amp;product_status=-1&amp;ps_id=0&amp;filter_ps_id=<%=ps_id%>&amp;search_field=0&amp;cmd=" target="_blank" <%=getCount(doubtAddressOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(doubtAddressOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&amp;st=<%=date[i].getString("date")%>&amp;en=<%=date[i].getString("date")%>&amp;business=&amp;handle=5&amp;product_type_int=0&amp;product_status=-1&amp;ps_id=0&amp;filter_ps_id=<%=ps_id%>&amp;search_field=0&amp;cmd=" target="_blank" <%=getCount(doubtCostOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(doubtCostOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&amp;st=<%=date[i].getString("date")%>&amp;en=<%=date[i].getString("date")%>&amp;business=&amp;handle=0&amp;handle_status=7&amp;product_type_int=0&amp;product_status=-1&amp;ps_id=0&amp;filter_ps_id=<%=ps_id%>&amp;search_field=0&amp;cmd=" target="_blank" <%=getCount(doubtPayOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(doubtPayOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
	  
	  
	  
	  
	  
<td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&after_service_status=7&handle_status=0&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(disputeRefundOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(disputeRefundOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>

<td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&after_service_status=11&handle_status=0&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(disputeWarrantyOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(disputeWarrantyOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>

<td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&after_service_status=13&handle_status=0&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(otherDisputingOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(otherDisputingOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>

	  
	  
	  
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&handle=2&handle_status=0&product_type_int=0&product_status=2&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(wait4PrintOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(wait4PrintOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
	  
      <td   align="center" valign="middle" ><a href="order/ct_order_auto_reflush.html?val=&st=<%=date[i].getString("date")%>&en=<%=date[i].getString("date")%>&business=&handle=3&handle_status=0&product_type_int=0&product_status=-1&ps_id=0&filter_ps_id=<%=ps_id%>&search_field=0&cmd=" target="_blank" <%=getCount(printedOrdersObject.get(date[i].getString("date"),new Object()))>0?"class='hard'":"class='normal'"%>><%=getCount(printedOrdersObject.get(date[i].getString("date"),new Object()))%></a></td>
  </tr>
<%
}
%>
</table>

</body>
</html>
<%!
public int getCount(Object obj)
{
	if (obj instanceof DBRow)
	{
		return( ((DBRow)obj).get("c",0) );
	}
	else
	{
		return(0);
	}
}
%>


