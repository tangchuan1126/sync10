<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="../include.jsp"%>
<%
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登录 <%=systemConfig.getStringConfigValue("webtitle")%></title>
<style>
.OkMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #00BE00;
	background:#E6FFE6 url(imgs/standard_msg_ok.gif) 5px 5px no-repeat;
	color:#000;
	height:18px;
} 
.WarningMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #00A8FF;
	background:#E2F5FF url(imgs/standard_msg_warning.gif) 5px 5px no-repeat;	
	color:#000;
	height:18px;
}
.ErrorMsg{
	padding:5px 5px 5px 25px;
	border:1px solid #F60;
	background:#FFF2E9 url(imgs/standard_msg_error.gif) 5px 5px no-repeat;	
	color:#000;
	height:18px;
}
</style>
<script type="text/javascript" src="../jquery-1.3.2.js"></script>
<script language="JavaScript" type="text/JavaScript">
function login()
{

	theForm = document.login_form;
	if ( theForm.account.value=="" )
	{ 
		document.getElementById("logining_info").innerHTML = "帐号不能为空";
		document.getElementById("logining_info").className = "ErrorMsg";
		theForm.account.focus();
	}
	else if ( theForm.pwd.value=="" )
	{
		document.getElementById("logining_info").innerHTML = "订单号不能为空";
		document.getElementById("logining_info").className = "ErrorMsg";
		theForm.pwd.focus();
	}
	else if ( theForm.licence.value=="" )
	{
		document.getElementById("logining_info").innerHTML = "验证码不能为空";
		document.getElementById("logining_info").className = "ErrorMsg";
		theForm.licence.focus();
	}
	else
	{

		$.ajax({
			url: '<%=ConfigBean.getStringValue("systenFolder")%>action/supplier/supplierLogin.action',
			type: 'post',
			dataType: 'html',
			timeout: 60000,
			cache:false,
			data:"account="+theForm.account.value+"&password="+theForm.pwd.value+"&licence="+theForm.licence.value,
			
			beforeSend:function(request){
					$("#logining_info").addClass("WarningMsg");
					$("#logining_info").html("正在登录......");
			},
			
			error: function(){
					alert("网络错误，请重试！");
			},
			
			success: function(html)
			{

				if (html==1)
				{
					$("#logining_info").addClass("ErrorMsg");
					$("#logining_info").html("帐号或密码不正确");
					$("#licenceImg").attr("src","<%=ConfigBean.getStringValue("systenFolder")%>loginLicence");
				}
				else if (html==2)
				{
					$("#logining_info").addClass("ErrorMsg");
					$("#logining_info").html("验证码不正确");
					$("#licenceImg").attr("src","<%=ConfigBean.getStringValue("systenFolder")%>loginLicence");
				}
				else if (html==3)
				{
					$("#logining_info").addClass("ErrorMsg");
					$("#logining_info").html("帐号被屏蔽");
					$("#licenceImg").attr("src","<%=ConfigBean.getStringValue("systenFolder")%>loginLicence");
				}
				else
				{
					$("#logining_info").addClass("OkMsg");
					$("#logining_info").html("验证通过，正在登陆......");
					window.location = "../administrator/index.html";
				}
			}
		});
	}
	 
	return (false);
}


if (top.location !== self.location)
{ 
	top.location=self.location;
}
</script>
<style>

.l-bg {
	background-attachment: scroll;
	background-image: url(imgs/l_bg.jpg);
	background-repeat: repeat-x;
	background-position: center top;
}
</style>
<style type="text/css">
<!--
.lt {
	font-size: 13px;
	font-weight: bold;
	color: #2270D2;
	padding-top: 2px;
	padding-right: 12px;
	padding-bottom: 0px;
	padding-left: 0px;
}
td {
	font-size: 12px;
	color: #666666;
}
-->
</style>
<style type="text/css">
<!--
.text-input {
	background-color: #FFFFFF;
	height: 20px;
	width: 155px;
	border: 1px solid #b2b2b2;
	color: #DD0000;
}
.text-input2 {
	background-color: #FFFFFF;
	height: 20px;
	width: 70px;
	border: 1px solid #b2b2b2;
	color: #DD0000;
}
.login-b {
	background-attachment: fixed;
	background: url(imgs/login_b.gif);
	background-repeat: no-repeat;
	background-position: center center;
	height: 20px;
	width: 70px;
	border: 0px solid;
}
-->
</style>
</head>
<body bgcolor="999999" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="document.getElementById('account').focus();">
<br>
<br>
<br>
<br>
<table width="753" height="400" border="0" align="center" cellpadding="0" cellspacing="0"  class="l-bg">
  <tr> 
    <td width="35" >&nbsp;</td>
    <td width="476" align="left" valign="top"><br>
      <br>
      <br>
      <a href="http://download.firefox.com.cn/releases/webins/3.6/zh-CN/Firefox-3.6.17.exe">请使用Firefox登录</a><br>
      <br>
<form name="login_form" id="login_form" onSubmit="return login()" >
      <table width="225" height="120" border="0" cellpadding="3" cellspacing="0" background="imgs/login_bg.gif">
        <tr>
          <td width="413" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="42%" align="center" valign="middle" class="lt"> 
                  <table width="75" border="0" cellspacing="0" cellpadding="0">
                    <%
String cookieAccount = StringUtil.getCookie(request,"account");
%>
                    <tr> 
                      <td width="20">
<input name="remember" type="checkbox" id="remeber2" value="1" <%=cookieAccount==null?"":"checked"%>></td>
                      <td width="55" class="text">
<label for="remeber2">记住帐号</label></td>
                    </tr>
                  </table>
                </td>
                <td width="58%" align="right" valign="middle" class="lt">请登录</td>
              </tr>
              <tr> 
                <td colspan="2"><table width="100%" border="0" cellpadding="3" cellspacing="0">
                    <tr> 
                      <td width="21%" height="20" align="right" class="text">帐&nbsp;&nbsp;号:</td>
                      <td width="79%"> <input name="account" type="text" class="text-input" id="account" tabindex="1" value="<%=cookieAccount==null?"":cookieAccount%>"></td>
                    </tr>
                    <tr> 
                      <td height="20" align="right" class="text">密&nbsp;&nbsp;码:</td>
                      <td><input name="pwd" type="password" class="text-input" id="pwd3" tabindex="2"></td>
                    </tr>
                    <tr> 
                      <td align="right" class="text">验证码:</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="35%"><input name="licence" type="text" class="text-input2" id="licence5"  style="width:50px;" tabindex="3"> 
                            </td>
                            <td width="65%"><img id="licenceImg" name="licenceImg" src="<%=ConfigBean.getStringValue("systenFolder")%>loginLicence" border="0" ></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
      <table width="353" border="0" cellspacing="0" cellpadding="4">
        <tr> 
          <td width="85"><input name="Submit" type="submit" class="login-b" value=" " ></td>
          <td width="252"><span id="logining_info" style="letter-spacing: 2px;"><a href="/install_lodop.exe">打印控件安装</a>&nbsp;&nbsp;&nbsp;<a href="/1youbarcodes.exe">条码字体安装</a></span></td>
        </tr>
      </table>
      <input type="hidden" name="purchaseid"/>
      </form> </td>
    <td width="242" align="right" valign="top" style="padding-right:20px;padding-top:20px;"><img src="imgs/system_logo.gif" width="192" height="20"></td>
  </tr>
</table>
<table width="753" height="20" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td width="1043" align="right" valign="middle" bgcolor="#2270D2"><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">&copy;2010-3010 VVME.COM - All Rights Reserved.</font></td>
  </tr>
</table>
</body>
</html>