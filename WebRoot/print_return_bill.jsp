<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="include.jsp"%>
<%
String rc = StringUtil.getString(request,"rc");
long ps_id = StringUtil.getLong(request,"ps_id");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Print RMA Code</title>
<style>
.print-code
{
	font-family: C39HrP24DlTt;font-size:30px;
}
</style>

<style media=print>
.noPrint
{
	display:none;
}<!--用本样式在打印时隐藏非打印项目--
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<fieldset style="border:1px #000000 dashed;padding:4px;width:360px;">
<legend style="font-size:15px;font-weight:bold;color:#000000;font-family:Arial, Helvetica, sans-serif">Attach To Your Package</legend>
<table width="356" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td width="131" rowspan="2" align="left" valign="middle" style="font-size:20px;font-family:Verdana, Arial, Helvetica, sans-serif;font-weight:bold">RMA Code</td>
    <td width="213" align="left" valign="middle" style="font-size:20px;font-family:Arial, Helvetica, sans-serif"><%=rc%></td>
  </tr>
  <tr>
    <td height="27" align="left" valign="middle" class="print-code">*<%=rc%>*</td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top" style="border-top:1px #000000 solid;font-size:12px;font-family:Arial, Helvetica, sans-serif"><strong>Return Address:</strong><br>
	<%
	DBRow detail = catalogMgr.getDetailProductStorageCatalogById(ps_id);
	if (detail==null)
	{
		detail = new DBRow();
	}
	%>
	<%=detail.getString("contact")%><br><br>

	<%=StringUtil.ascii2Html(detail.getString("address"))%><br>
	<%=detail.getString("phone")%>

    </td> 
  </tr>
  <tr>
    <td colspan="2" align="right" valign="middle" style="border-top:1px #000000 solid;font-size:12px;font-family:Arial, Helvetica, sans-serif">Visionari e-Commerce LLC</td>
  </tr> 


</table>
</fieldset>
<br>
<table width="365" border="0" cellspacing="0" cellpadding="0">
  <tr class="noPrint">
    <td colspan="2" align="center" valign="middle"><input type="button" name="Submit" value="Print RMA Code" style="width:200px;height:50px;" onClick="window.print();"></td>
  </tr>
</table>

</body>
</html>
