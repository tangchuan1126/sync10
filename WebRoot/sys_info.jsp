<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="include.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系统信息</title>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%
Object sysTime = "";
Object server = "";
Object os = "";
Object jdk = "";

try
{
ServletContext context = application;

server = context.getServerInfo();
os = System.getProperty("os.name", "");
jdk = System.getProperty("java.vm.version", "");

sysTime = System.getProperties().get("user.country");
}
catch (Exception e)
{
}
%>
  <br>
  <table width="98%" border="0" align="center" cellpadding="4" cellspacing="0"  class="thetable">
    <thead>
      <tr> 
        <td height="25" colspan="4" valign="top" bgcolor="e5e5e5" class="line-0010-bbbbbb">系统信息</td>
      </tr>
    </thead>
    <tr align="left"> 
      <td height="26" valign="middle" >服务器时区：</td>
      <td valign="middle"> 
        <%=sysTime%>      </td>
      <td valign="middle" >应用服务器：</td>
      <td valign="middle" > 
        <%=server%>      </td>
    </tr>
    <tr align="left"> 
      <td width="23%" height="26" valign="middle" > 操作系统：</td>
      <td width="28%" valign="middle"> 
        <%=os%>      </td>
      <td width="26%" valign="middle" >JDK版本 ：</td>
      <td width="23%" valign="middle" > 
        <%=jdk%>      </td>
    </tr>
    <tr align="left"> 
      <td height="26" valign="middle" >MySQL 版本：</td>
      <td valign="middle">
              </td>
      <td colspan="2" valign="middle" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <%
Runtime runtime = Runtime.getRuntime();
long totalMemory = runtime.totalMemory();
long freeMemory  = runtime.freeMemory();
float totalKB = (float)totalMemory/1024f/1024f;
float useKB  = (float)(totalMemory-freeMemory)/1024f/1024f;
float percent = useKB/totalKB;
%>
        <tr>
          <td width="16%">内存占用率： </td>
          <td width="49%"><table width="230" border="0" cellpadding="0" cellspacing="1" bgcolor="#999999">
              <tr>
                <td width="244" bgcolor="#FFFFFF"><table width="230" border="0" cellpadding="0" cellspacing="1">
                    <tr>
                      <td width="230" bgcolor="#33CC00"><table width="<%=percent*230%>" border="0" cellpadding="0" cellspacing="0" bgcolor="#FF0000">
                          <tr>
                            <td><img src="../../imgs/blank.gif" alt="a" width="3" height="8"></td>
                          </tr>
                      </table></td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
          <td width="35%"><%=StringUtil.formatNumber("#.00",useKB)%> M / <%=StringUtil.formatNumber("#.00",totalKB)%> M</td>
        </tr>
      </table></td>
    </tr>
  </table>
</body>
</html>
