<!DOCTYPE html >
<%@page import="com.cwc.app.lucene.zyj.PlateInventaryIndexMgr"%>
<%@page import="com.cwc.app.lucene.zyj.AdminIndexMgr"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*,
				com.javaps.lucene.*,
				com.cwc.lucene.*,
				com.cwc.asynchronized.*,
				com.cwc.app.lucene.*,
				com.cwc.spring.util.*,
				com.cwc.db.DBUtilIFace" %>
<%@page import="com.cwc.app.lucene.zj.TradeIndexMgr"%>
<%@page import="com.cwc.app.lucene.zr.BillIndexMgr"%>
<%@page import="com.cwc.app.lucene.zr.WayBillIndexMgr"%>
<%@page import="com.cwc.lucene.BatchUpdateWayBillIndex"%>
<%@page import="com.cwc.app.lucene.zr.DeliveryOrderIndexMgr"%>
<%@page import="com.cwc.app.lucene.zr.TransportIndexMgr"%>
<%@page import="com.cwc.lucene.BatchUpdateApplyMoneyIndex"%>
<%@page import="com.cwc.app.lucene.zr.ApplyMoneyIndexMgr"%>
<%@page import="com.cwc.app.lucene.zr.ApplyTransferIndexMgr"%>
<%@page import="com.cwc.app.lucene.zr.PurchaseIndexMgr"%>
<%@page import="com.cwc.app.key.CodeTypeKey"%>
<%@page import="org.apache.lucene.analysis.core.WhitespaceAnalyzer"%>
<%@page import="org.apache.lucene.util.Version"%>
<%@page import="org.wltea.analyzer.lucene.IKAnalyzer"%>
<%@page import="com.cwc.app.lucene.zr.QuestionIndexMgr"%>
<%@page import="com.cwc.app.lucene.zyj.RepairOrderIndexMgr"%>
<%@page import="com.cwc.app.lucene.zyj.ServiceOrderIndexMgrZyj"%>
<%@page import="com.cwc.app.lucene.zyj.ReturnProductOrderIndexMgrZyj"%>
<%@page import="com.cwc.app.lucene.zyj.B2BOrderIndexMgr"%>
<%@page import="com.cwc.app.lucene.zyj.TitleIndexMgr"%>
<%@page import="com.cwc.app.lucene.zyj.CustomerIndexMgr"%>
<%@page import="com.cwc.app.lucene.zwb.CheckInIndexMgr"%>
<%@page import="com.cwc.app.lucene.zwb.CheckInCarrierIndexMgr"%>
<%@page import="com.cwc.app.lucene.ys.ShipToIndexMgr"%>
<%@page import="com.cwc.app.lucene.ys.ShipToStorageIndexMgr"%>
<% 
	try
	{
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>更新索引异步任务测试工具</title>
</head>
<body>
<% 

if(request.getMethod().equalsIgnoreCase("POST")) { 
	List<String> indexNames = Arrays.asList(request.getParameterValues("indexNames"));
	DBUtilIFace dbUtil = (DBUtilIFace)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
	ThreadManager threadManager = ThreadManager.getInstance();
	
	if(indexNames.contains("product")){
		IndexCore indexMgr = ProductIndexMgr.getInstance();
//		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,null,  //产品索引更新暂不使用新的merge方法
//														"product","orignal_pc_id=0", //注意条件，仅对非定制产品
//														"pc_id","p_name","p_code","catalog_id","unit_name","alive");

//		BatchUpdateIndexSQL threadAction = new BatchUpdateIndexSQL("select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p " 
//																  +"join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main+" "
//																  +"left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon+" "
//																  +"left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC+" "
//																  +"where p.orignal_pc_id = 0",//使用语句查询
//														dbUtil,indexMgr,"pc_id",  //产品索引更新暂不使用新的merge方法
//														"merge_info",false, //注意条件，仅对非定制产品
//														"pc_id,p_name,p_code,catalog_id,unit_name,alive","p_code,p_name");

		BatchUpdateProductIndex threadAction = new BatchUpdateProductIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40),0);
		//pageSize属性是决定一次从数据库中读取记录的单位，
		//如果使用新的merge方法，也就是一次批量更新索引的粒度，
		//由于批量更新索引会synchronize indexMgr，所以如果使用新merge方法，这个批量不宜过大
		//如果不使用新的merge方法，则pageSize可以适当加大，以提高数据读取效率
		threadAction.setPageSize(2000);
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}//if
	
	if(indexNames.contains("porder")){
		IndexCore indexMgr = OrderIndexMgr.getInstance();
		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,"oid","merge_field","porder","",true,"oid,address_name,client_id,txn_id,auction_buyer_id,item_number,ems_id,address_country","oid,address_name,client_id,txn_id,auction_buyer_id,item_number,ems_id,address_country",new WhitespaceAnalyzer(Version.LUCENE_40));
//		(dbUtil,indexMgr,"orderid=oid merge_field",
//														"porder","", //条件为空，对所有订单
//														"oid,address_name,client_id,txn_id,auction_buyer_id,item_number,ems_id,address_country");
		//pageSize属性是决定一次从数据库中读取记录的单位，
		//如果使用新的merge方法，也就是一次批量更新索引的粒度，
		//由于批量更新索引会synchronize indexMgr，所以如果使用新merge方法，这个批量不宜过大
		//如果不使用新的merge方法，则pageSize可以适当加大，以提高数据读取效率
		threadAction.setPageSize(500); 
		//如果不设置optThreshold属性，则默认是全部做完再优化索引，这样会加快速度，因为频繁优化索引会增加处理时间
		//可按需设置，例如：如想每更新5000条优化一次，可使用以下：
		//threadAction.setOptThreshold(5000);
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	
	if(indexNames.contains("trade")){
		IndexCore indexMgr = TradeIndexMgr.getInstance();
//		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,"oid","merge_field","porder","",true,"","oid,address_name,client_id,txn_id,auction_buyer_id,item_number,ems_id,address_country");
		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,"trade_id","merge_field","trade","",true,"trade_id,txn_id,client_id,relation_id,payer_email,delivery_address_name","trade_id,txn_id,client_id,relation_id,payer_email,delivery_address_name",new WhitespaceAnalyzer(Version.LUCENE_40));
		//pageSize属性是决定一次从数据库中读取记录的单位，
		//如果使用新的merge方法，也就是一次批量更新索引的粒度，
		//由于批量更新索引会synchronize indexMgr，所以如果使用新merge方法，这个批量不宜过大
		//如果不使用新的merge方法，则pageSize可以适当加大，以提高数据读取效率
		threadAction.setPageSize(500); 
		//如果不设置optThreshold属性，则默认是全部做完再优化索引，这样会加快速度，因为频繁优化索引会增加处理时间
		//可按需设置，例如：如想每更新5000条优化一次，可使用以下：
		//threadAction.setOptThreshold(5000);
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	
	if(indexNames.contains("bill")){
		IndexCore indexMgr = BillIndexMgr.getInstance();
//		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,"bill_id=bill_id merge_field",
//														"bill_order","", //条件为空，对所有订单
//														"bill_id","client_id","porder_id","payer_email","address_name");
		BatchUpdateIndex threadAction = new BatchUpdateIndex(dbUtil,indexMgr,"bill_id","merge_field","bill_order","",true,"bill_id,client_id,porder_id,payer_email,address_name","bill_id,client_id,porder_id,payer_email,address_name",new WhitespaceAnalyzer(Version.LUCENE_40));												
		
		threadAction.setPageSize(500); 
		 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("way"))
	{
		IndexCore indexMgr = WayBillIndexMgr.getInstance();
		BatchUpdateWayBillIndex threadAction = new BatchUpdateWayBillIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));
		
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}

	
	if(indexNames.contains("transport")){
		IndexCore indexMgr = TransportIndexMgr.getInstance();
		BatchUpdateTransportIndex threadAction = new BatchUpdateTransportIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	
	if(indexNames.contains("repair_order")){
		IndexCore indexMgr = RepairOrderIndexMgr.getInstance();
		BatchUpdateRepairOrderIndex threadAction = new BatchUpdateRepairOrderIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}

	if(indexNames.contains("apply_money")){
		IndexCore indexMgr = ApplyMoneyIndexMgr.getInstance();
		BatchUpdateApplyMoneyIndex threadAction = new BatchUpdateApplyMoneyIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("apply_transfer")){
		IndexCore indexMgr = ApplyTransferIndexMgr.getInstance();
		BatchUpdateApplyTransferIndex threadAction = new BatchUpdateApplyTransferIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	
	if(indexNames.contains("purchase"))
	{
		IndexCore indexMgr = PurchaseIndexMgr.getInstance();
		BatchUpdatePurchaseIndex threadAction = new BatchUpdatePurchaseIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("serviceOrder"))
	{
		IndexCore indexMgr = ServiceOrderIndexMgrZyj.getInstance();
		BatchUpdateServiceOrderIndex threadAction = new BatchUpdateServiceOrderIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("returnOrder"))
	{
		IndexCore indexMgr = ReturnProductOrderIndexMgrZyj.getInstance();
		BatchUpdateReturnOrderIndex threadAction = new BatchUpdateReturnOrderIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("shipTo"))
	{
		IndexCore indexMgr = ShipToIndexMgr.getInstance();
		BatchUpdateShipToIndex threadAction = new BatchUpdateShipToIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("shipToStorage"))
	{
		IndexCore indexMgr = ShipToStorageIndexMgr.getInstance();
		BatchUpdateShipToStorageIndex threadAction = new BatchUpdateShipToStorageIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("question"))
	{
		IndexCore indexMgr = QuestionIndexMgr.getInstance();
		BatchUpdateQuestionIndex threadAction = new BatchUpdateQuestionIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("b2BOrder"))
	{
		IndexCore indexMgr = B2BOrderIndexMgr.getInstance();
		BatchUpdateB2BOrderIndex threadAction = new BatchUpdateB2BOrderIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("titles"))
	{
		IndexCore indexMgr = TitleIndexMgr.getInstance();	//,new WhitespaceAnalyzer(Version.LUCENE_40)
		BatchUpdateTitleIndex threadAction = new BatchUpdateTitleIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));//new IKAnalyzer()
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("customer"))
	{
		IndexCore indexMgr = CustomerIndexMgr.getInstance();
		BatchUpdateCustomerIndex threadAction = new BatchUpdateCustomerIndex(dbUtil,indexMgr,new IKAnalyzer());
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("checkIn"))
	{
		IndexCore indexMgr = CheckInIndexMgr.getInstance();
		BatchUpdateCheckInIndex threadAction = new BatchUpdateCheckInIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("carrier"))
	{
		IndexCore indexMgr = CheckInCarrierIndexMgr.getInstance();
		BatchUpdateCheckInCarrierIndex threadAction = new BatchUpdateCheckInCarrierIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("admin"))
	{
		IndexCore indexMgr = AdminIndexMgr.getInstance();
		BatchUpdateAdminIndex threadAction = new BatchUpdateAdminIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("plateNo"))
	{
		IndexCore indexMgr = PlateInventaryIndexMgr.getInstance();
		BatchUpdatePlateInventaryIndex threadAction = new BatchUpdatePlateInventaryIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40));
		threadAction.setPageSize(500); 
		threadManager.executeInBackground(threadAction);
		out.println(String.format("update index using %s <br/>",indexMgr));
	}
	if(indexNames.contains("Receipt"))
	{
		BatchUpdateReceiptIndex threadAction = new BatchUpdateReceiptIndex(session.getId());
		threadManager.executeInBackground(threadAction);
		out.println("update index using ReceiptIndex <br/>");
	}
%>
<h1>后台任务已经启动完毕，请查看以下日志：</h1>
<h4><%= application.getRealPath("/logs/javaps_index.log") %></h4>
<% } else { %>
<h1>异步更新索引任务触发</h1>
<h4>在启动之前，请确认JAVAPS_INDEX日志配置已经在Log4J配置文件中存在，
异步任务将写日志到：<br/><%= application.getRealPath("/logs/javaps_index.log") %></h4>
<form method="POST">
	<ul>
		<li>
			<input type="checkbox" name="indexNames" value="product" />产品（product） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="porder" />订单（porder） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="trade" />交易（trade） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="bill" />账单（Bill） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="way" />运单（wayBill） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="deliveryOrder" />交货单（deliveryOrder） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="transport" />转运单（transport） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="repair_order" />返修单（transport） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="apply_money" />申请资金（applyMoney） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="apply_transfer" />转账申请（applyTransfer） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="purchase" />采购单（purchase） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="serviceOrder" />服务单（serviceOrder） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="returnOrder" />退货单（returnOrder） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="question" />问答（question） 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="b2BOrder" />B2B订单（b2BOrder） 
		</li>
		
		<li>
			<input type="checkbox" name="indexNames" value="titles" />Title
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="customer" />Customer
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="checkIn" />Check In 
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="carrier" />Carrier
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="admin" />Admin
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="shipTo" />ShipTo
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="shipToStorage" />ShipTo Storage Catalog
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="plateNo" />PlateNo
		</li>
		<li>
			<input type="checkbox" name="indexNames" value="Receipt" />Receipt
		</li>
	</ul>
	<input type="submit" value="启动任务" />
</form>
<% }//else %>
</body>
</html>
<%
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
%>
