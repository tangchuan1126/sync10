package sysu.yujian.containerloading;


public class ContainerLoading
{

    public ContainerLoading(Instance instance)
    {
        excutionTime = 0L;
        boxes = instance.boxes;
        width = instance.width;
        length = instance.length;
        height = instance.height;
        volume = length * width * height;
    }

    public ExecutionResult solve(long timeLimit, int blockType, int version)
    {
        excutionTime = System.currentTimeMillis();
        ExecutionResult solution = null;
        if(version == 0)
        {
//            //system.out.println("Algorithm: Greedy");
            solution = GreedyOnMaximalSpace.solve(blockType);
        } else
        if(version == 1)
        {
//            //system.out.println("Algorithm: Greedy with 2-step Look Ahead, (1, 0)");
            solution = LookAheadOnMaximalSpaceV1.solve(timeLimit, blockType);
        } else
        if(version == 2)
        {
//            //system.out.println("Algorithm: Greedy with 2-step Look Ahead, (1, -1)");
            solution = LookAheadOnMaximalSpaceV2.solve(timeLimit, blockType);
        } else
        if(version == 3)
        {
//            //system.out.println("Algorithm: Grasp");
            solution = GraspOnMaximalSpace.solve(timeLimit, blockType);
        }
        excutionTime = System.currentTimeMillis() - excutionTime;
        return solution;
    }

    public long excutionTime;
    public static int width = 0;
    public static int length = 0;
    public static int height = 0;
    public static int volume = 0;
    public static Box boxes[] = null;

}