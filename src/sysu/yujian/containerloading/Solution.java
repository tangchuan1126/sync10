// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2011-12-21 12:09:14
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Solution.java

package sysu.yujian.containerloading;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package sysu.yujian.containerloading:
//            ContainerLoading, PlacedCuboid, PlacedBlock, GeneralBlock, 
//            Box

public class Solution
{

    public Solution()
    {
        placedCuboid = new ArrayList();
        boxesVolume = 0;
    }

    public double getUtilization()
    {
        return ((double)boxesVolume * 100D) / (double)ContainerLoading.volume;
    }

    public void add(PlacedCuboid p)
    {
        placedCuboid.add(p);
        boxesVolume += p.getVolume();
    }

    public void add(PlacedBlock pb)
    {
        PlacedCuboid pc;
        for(Iterator iterator = pb.block.cuboid.iterator(); iterator.hasNext(); add(new PlacedCuboid(pb.x + pc.x, pb.y + pc.y, pb.z + pc.z, pc.length, pc.width, pc.height, pc.box)))
            pc = (PlacedCuboid)iterator.next();

    }

    public Solution clone()
    {
        Solution tmp = new Solution();
        PlacedCuboid p;
        for(Iterator iterator = placedCuboid.iterator(); iterator.hasNext(); tmp.add(p.clone()))
            p = (PlacedCuboid)iterator.next();

        tmp.boxesVolume = boxesVolume;
        return tmp;
    }

    public int size()
    {
        return placedCuboid.size();
    }

    public String toString()
    {
        String result = "";
        result = (new StringBuilder(String.valueOf(result))).append("Container length: ").append(ContainerLoading.length).append("\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Container width: ").append(ContainerLoading.width).append("\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Container height: ").append(ContainerLoading.height).append("\n\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Boxes Volume: ").append(boxesVolume).append("\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Container Volume: ").append(ContainerLoading.volume).append("\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Utilization: ").append(getUtilization()).append("\n\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("Packed Boxes: ").append(placedCuboid.size()).append("\n").toString();
        result = (new StringBuilder(String.valueOf(result))).append("format: boxId x y z length width height\n\n").toString();
        for(Iterator iterator = placedCuboid.iterator(); iterator.hasNext();)
        {
            PlacedCuboid p = (PlacedCuboid)iterator.next();
            result = (new StringBuilder(String.valueOf(result))).append(p.box.type).append(" ").append(p.x).append(" ").append(p.y).append(" ").append(p.z).append(" ").append(p.length).append(" ").append(p.width).append(" ").append(p.height).append("\n").toString();
        }

        return result;
    }

    public void draw(String path)
    {
        FileWriter fw = null;
        try
        {
            fw = new FileWriter(new File(path));
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        StringBuilder output = new StringBuilder("Graphics3D[{");
        for(int i = 0; i < placedCuboid.size(); i++)
        {
            if(i != 0)
                output.append(",");
            output.append("Cuboid[");
            output.append(((PlacedCuboid)placedCuboid.get(i)).toString());
            output.append("]");
        }

        output.append((new StringBuilder(",Opacity[0],Cuboid[{0, 0, 0},{")).append(ContainerLoading.length).append(", ").append(ContainerLoading.width).append(", ").append(ContainerLoading.height).append("}]").toString());
        output.append("}, Boxed->False]\n");
        try
        {
            fw.write(output.toString());
            fw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }


    private ArrayList placedCuboid;
    private int boxesVolume;
    
	public ArrayList getPlacedCuboid() {
		return placedCuboid;
	}
}