package sysu.yujian.containerloading;

import java.io.IOException;
import java.util.Scanner;

public class Instance
{
  public Box[] boxes = null;
  public int length;
  public int width;
  public int height;
  private int problemNumber;

  public int getID()
  {
    return this.problemNumber;
  }

  public void load(Scanner cin) throws IOException {
    this.problemNumber = cin.nextInt();
    this.length = cin.nextInt();

    this.length = cin.nextInt();
    this.width = cin.nextInt();
    this.height = cin.nextInt();

    int typeCount = cin.nextInt();

    this.boxes = new Box[typeCount];
    for (int i = 0; i < typeCount; ++i) {
      this.boxes[i] = new Box();
      this.boxes[i].initialize(cin, this.length, this.width, this.height);
    }
  }
}