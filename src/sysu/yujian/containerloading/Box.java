package sysu.yujian.containerloading;

import java.util.Scanner;

public class Box
{

    public Box()
    {
        variation = null;
    }

    public void initialize(Scanner cin, int containerLength, int containerWidth, int containerHeight)
    {
        int length[] = new int[DIMENSIONALITY];
        boolean indicator[] = new boolean[DIMENSIONALITY];
        type = cin.nextInt();
        int n = 0;
        for(int i = 0; i < DIMENSIONALITY; i++)
        {
            length[i] = cin.nextInt();
            if(cin.nextInt() == 1)
                indicator[i] = true;
            else
                indicator[i] = false;
        }

        for(int i = 0; i < DIMENSIONALITY; i++)
            if(indicator[i])
            {
                boolean isSquare = false;
                for(int j = 0; j < DIMENSIONALITY; j++)
                {
                    if(j == i)
                        continue;
                    if(length[j] == length[DIMENSIONALITY - i - j])
                        isSquare = true;
                    break;
                }

                if(isSquare)
                    n++;
                else
                    n += 2;
            }

        int tmp[][] = new int[n][DIMENSIONALITY];
        count = cin.nextInt();
        volume = length[0] * length[1] * length[2];
        n = 0;
        for(int i = 0; i < DIMENSIONALITY; i++)
            if(indicator[i])
            {
                for(int j = 0; j < DIMENSIONALITY; j++)
                {
                    if(j == i)
                        continue;
                    tmp[n][0] = length[j];
                    tmp[n][1] = length[DIMENSIONALITY - i - j];
                    tmp[n][2] = length[i];
                    if(tmp[n][0] > containerWidth || tmp[n][1] > containerLength || tmp[n][2] > containerHeight)
                        continue;
                    n++;
                    if(length[j] == length[DIMENSIONALITY - i - j])
                        break;
                }

            }

        variation = new int[n][DIMENSIONALITY];
        for(int i = 0; i < n; i++)
            variation[i] = tmp[i];

    }

    public static int DIMENSIONALITY = 3;
    public int type;
    public int count;
    public int volume;
    public int variation[][];

}
