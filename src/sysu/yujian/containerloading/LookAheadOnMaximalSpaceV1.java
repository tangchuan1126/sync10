// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2012-1-20 11:40:56
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   LookAheadOnMaximalSpaceV1.java

package sysu.yujian.containerloading;

import java.io.PrintStream;
import java.util.*;

// Referenced classes of package sysu.yujian.containerloading:
//            ExecutionResult, BlockGenerator, State, Utility, 
//            GeneralBlock, Space, SortedTriple, PlacedBlock, 
//            ContainerLoading, Box

public class LookAheadOnMaximalSpaceV1
{

    public LookAheadOnMaximalSpaceV1()
    {
    }

    public static ExecutionResult solve(long timeLimit, int blockType)
    {
        ExecutionResult exeResult = new ExecutionResult();
        long start = System.currentTimeMillis();
        finishTime = start + timeLimit;
        GeneralBlock backup[] = BlockGenerator.generateBlock(blockType);
        exeResult.blockGenerationTime = System.currentTimeMillis() - start;
        exeResult.blockCount = backup.length;
        State bestState = null;
        for(int LEAF_COUNT = 1; System.currentTimeMillis() - start < timeLimit && LEAF_COUNT <= 0x3fffffff; LEAF_COUNT <<= 1)
        {
            long startTime = System.currentTimeMillis();
            WIDTH = (int)(Math.pow(LEAF_COUNT, 0.5D) + 0.001D);
            GeneralBlock blockList[] = (GeneralBlock[])backup.clone();
            State state = new State();
            state.initialze();
            while(!state.spaceList.isEmpty()) 
            {
                blockSearch(state, blockList, 0);
                if(bestBlock != null)
                {
                    packBlock(state, firstSpace, bestBlock);
                    blockList = updateBlockList(state.freeBoxes, blockList);
                }
                if(System.currentTimeMillis() >= finishTime)
                    break;
            }
            if(bestState == null || bestState.volume < state.volume)
                bestState = state;
//            //system.out.println((new StringBuilder("LEAF_COUNT: ")).append(LEAF_COUNT).append(" Current Best: ").append(bestState.getUtilization()).append(" (").append(state.getUtilization()).append(") Time: ").append((double)(System.currentTimeMillis() - startTime) * 0.001D).toString());
        }

        exeResult.solution = Utility.stateToSolution(bestState);
        exeResult.totalExecutionTime = System.currentTimeMillis() - start;
        return exeResult;
    }

    private static GeneralBlock[] updateBlockList(int freeBoxes[], GeneralBlock blockList[])
    {
        boolean removed[] = new boolean[blockList.length];
        int count = blockList.length;
        for(int i = 0; i < blockList.length; i++)
        {
            GeneralBlock b = blockList[i];
            boolean ok = true;
            int ai[];
            int l = (ai = b.component).length;
            for(int k = 0; k < l; k++)
            {
                int j = ai[k];
                if(freeBoxes[j] >= b.typeCount[j])
                    continue;
                ok = false;
                break;
            }

            if(!ok)
            {
                removed[i] = true;
                count--;
            }
        }

        GeneralBlock newGeneralBlockList[] = new GeneralBlock[count];
        count = 0;
        for(int i = 0; i < blockList.length; i++)
            if(!removed[i])
                newGeneralBlockList[count++] = blockList[i];

        return newGeneralBlockList;
    }

    private static Space chooseMaximalSpace(State state)
    {
        ArrayList spaceList = state.spaceList;
        SortedTriple minimalDistance = null;
        Space optimal = null;
        for(Iterator iterator = spaceList.iterator(); iterator.hasNext();)
        {
            Space s = (Space)iterator.next();
            SortedTriple distance = s.cornerDistance();
            int compareResult = 0;
            if(minimalDistance != null)
                compareResult = distance.compareTo(minimalDistance);
            if(minimalDistance == null || compareResult < 0 || compareResult == 0 && s.getVolume() > optimal.getVolume())
            {
                minimalDistance = distance;
                optimal = s;
            }
        }

        return optimal;
    }

    private static State completeSolution(State initState, GeneralBlock blockList[])
    {
        State state;
        for(state = initState.clone(); !state.spaceList.isEmpty();)
        {
            Space space = chooseMaximalSpace(state);
            ArrayList bl = chooseBlock(state, blockList, space, 1);
            if(bl.isEmpty())
                state.spaceList.remove(space);
            else
                packBlock(state, space, (GeneralBlock)bl.get(0));
        }

        return state;
    }

    private static void blockSearch(State state, GeneralBlock blockList[], int depth)
    {
        if(System.currentTimeMillis() >= finishTime)
            return;
        if(depth == 2 || state.spaceList.isEmpty())
        {
            State completeState = completeSolution(state, blockList);
            if(completeState.volume > bestBlockScore)
            {
                bestBlockScore = completeState.volume;
                bestBlock = ((PlacedBlock)completeState.placedBlock.get(blockIndex)).block;
            }
            return;
        }
        Space space = chooseMaximalSpace(state);
        ArrayList candidate = chooseBlock(state, blockList, space, WIDTH);
        if(depth == 0)
        {
            bestBlockScore = 0;
            bestBlock = null;
            blockIndex = state.placedBlock.size();
            firstSpace = space;
            if(candidate.isEmpty())
            {
                state.spaceList.remove(space);
                return;
            }
        }
        if(candidate.isEmpty())
        {
            state.spaceList.remove(space);
            blockSearch(state, blockList, depth + 1);
            state.spaceList.add(space);
        } else
        {
            State newState;
            for(Iterator iterator = candidate.iterator(); iterator.hasNext(); blockSearch(newState, blockList, depth + 1))
            {
                GeneralBlock b = (GeneralBlock)iterator.next();
                newState = state.clone();
                packBlock(newState, space, b);
            }

        }
    }

    private static ArrayList chooseBlock(State state, GeneralBlock blockList[], Space s, int count)
    {
        selectedSpace = s;
        ArrayList candidate = new ArrayList();
        GeneralBlock ageneralblock[];
        int k = (ageneralblock = blockList).length;
        for(int j = 0; j < k; j++)
        {
            GeneralBlock b = ageneralblock[j];
            if(b.length > s.length() || b.width > s.width() || b.height > s.height())
                continue;
            boolean okay = true;
            int ai[];
            int i1 = (ai = b.component).length;
            for(int l = 0; l < i1; l++)
            {
                int i = ai[l];
                if(state.freeBoxes[i] >= b.typeCount[i])
                    continue;
                okay = false;
                break;
            }

            if(!okay)
                continue;
            if(count != 1)
            {
                candidate.add(b);
                if(candidate.size() == count)
                    return candidate;
                continue;
            }
            if(candidate.size() == 0 || largestVolume.compare(b, (GeneralBlock)candidate.get(0)) < 0)
            {
                candidate.clear();
                candidate.add(b);
                continue;
            }
            if(b.blockVolume < ((GeneralBlock)candidate.get(0)).blockVolume)
                break;
        }

        return candidate;
    }

    private static void packBlock(State state, Space s, GeneralBlock b)
    {
        int ai[];
        int dy = (ai = b.component).length;
        for(int j = 0; j < dy; j++)
        {
            int i = ai[j];
            state.freeBoxes[i] -= b.typeCount[i];
        }

        Space taken = new Space();
        int dx = b.length;
        dy = b.width;
        int dz = b.height;
        if(s.cornerID >= 4)
        {
            taken.z1 = s.z2;
            dz = -dz;
        } else
        {
            taken.z1 = s.z1;
        }
        int p = s.cornerID % 4;
        if(p == 0)
        {
            taken.x1 = s.x1;
            taken.y1 = s.y1;
        } else
        if(p == 1)
        {
            taken.x1 = s.x2;
            taken.y1 = s.y1;
            dx = -dx;
        } else
        if(p == 2)
        {
            taken.x1 = s.x2;
            taken.y1 = s.y2;
            dx = -dx;
            dy = -dy;
        } else
        {
            taken.x1 = s.x1;
            taken.y1 = s.y2;
            dy = -dy;
        }
        taken.x2 = taken.x1 + dx;
        taken.y2 = taken.y1 + dy;
        taken.z2 = taken.z1 + dz;
        taken.maintain();
        state.placedBlock.add(new PlacedBlock(taken.x1, taken.y1, taken.z1, b));
        state.volume += b.boxVolume;
        state.boxesCount += b.count;
        updateSpaceList(state, s, taken);
    }

    private static void updateSpaceList(State state, Space s, Space taken)
    {
        ArrayList spaceList = state.spaceList;
        if(taken.getVolume() == s.getVolume())
            spaceList.remove(s);
        ArrayList mList = new ArrayList();
        for(Iterator iterator = spaceList.iterator(); iterator.hasNext();)
        {
            Space space = (Space)iterator.next();
            Space is = space.intersect(taken);
            if(is == null)
                mList.add(space);
            else
                space.cut(is, mList);
        }

        Collections.sort(mList, yComparator);
        int set[] = new int[mList.size()];
        int count = 0;
        spaceList.clear();
        for(int i = 0; i < mList.size(); i++)
        {
            Space si = (Space)mList.get(i);
            boolean removed = false;
            boolean ok = false;
            for(int k = 0; k < state.freeBoxes.length && !ok; k++)
                if(state.freeBoxes[k] != 0)
                {
                    int ai[][];
                    int i1 = (ai = ContainerLoading.boxes[k].variation).length;
                    for(int l = 0; l < i1; l++)
                    {
                        int variation[] = ai[l];
                        if(si.width() < variation[0] || si.length() < variation[1] || si.height() < variation[2])
                            continue;
                        ok = true;
                        break;
                    }

                }

            if(!ok)
            {
                removed = true;
            } else
            {
                for(int j = 0; j < count;)
                    if(((Space)mList.get(set[j])).y2 <= si.y1)
                        set[j] = set[--count];
                    else
                        j++;

                for(int j = count - 1; j >= 0; j--)
                {
                    if(!((Space)mList.get(set[j])).contains(si))
                        continue;
                    removed = true;
                    break;
                }

                if(!removed)
                {
                    set[count++] = i;
                    spaceList.add((Space)mList.get(i));
                }
            }
        }

    }

    private static final int DEPTH = 2;
    private static int WIDTH;
    private static Space selectedSpace;
    private static int bestBlockScore;
    private static GeneralBlock bestBlock;
    private static int blockIndex;
    private static Space firstSpace;
    private static long finishTime = 0L;
    private static Comparator largestVolume = new Comparator() {

        public int compare(GeneralBlock o1, GeneralBlock o2)
        {
            if(o2.blockVolume == o1.blockVolume)
                return LookAheadOnMaximalSpaceV1.bestFit.compare(o1, o2);
            else
                return o2.blockVolume - o1.blockVolume >= 0 ? 1 : -1;
        }

        public int compare(Object obj, Object obj1)
        {
            return compare((GeneralBlock)obj, (GeneralBlock)obj1);
        }

    };
    private static Comparator bestFit = new Comparator() {

        public int compare(GeneralBlock o1, GeneralBlock o2)
        {
            SortedTriple s1 = new SortedTriple(LookAheadOnMaximalSpaceV1.selectedSpace.width() - o1.width, LookAheadOnMaximalSpaceV1.selectedSpace.length() - o1.length, LookAheadOnMaximalSpaceV1.selectedSpace.height() - o1.height);
            SortedTriple s2 = new SortedTriple(LookAheadOnMaximalSpaceV1.selectedSpace.width() - o2.width, LookAheadOnMaximalSpaceV1.selectedSpace.length() - o2.length, LookAheadOnMaximalSpaceV1.selectedSpace.height() - o2.height);
            int result = s1.compareTo(s2);
            if(result == 0)
                return o1.count - o2.count;
            else
                return result >= 0 ? 1 : -1;
        }

        public int compare(Object obj, Object obj1)
        {
            return compare((GeneralBlock)obj, (GeneralBlock)obj1);
        }

    };
    private static Comparator yComparator = new Comparator() {

        public int compare(Space a, Space b)
        {
            if(a.y1 != b.y1)
                return a.y1 - b.y1;
            if(a.y2 != b.y2)
                return b.y2 - a.y2;
            if(a.x1 != b.x1)
                return a.x1 - b.x1;
            if(a.x2 != b.x2)
                return b.x2 - a.x2;
            if(a.z1 != b.z1)
                return a.z1 - b.z1;
            else
                return b.z2 - a.z2;
        }

        public int compare(Object obj, Object obj1)
        {
            return compare((Space)obj, (Space)obj1);
        }

    };



}