// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2012-1-20 11:45:41
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GreedyOnMaximalSpace.java

package sysu.yujian.containerloading;

import java.util.*;

// Referenced classes of package sysu.yujian.containerloading:
//            ExecutionResult, BlockGenerator, State, Utility, 
//            Space, GeneralBlock, SortedTriple, PlacedBlock, 
//            ContainerLoading, Box

public class GreedyOnMaximalSpace
{
    public static class Node
        implements Comparable
    {

        public int compareTo(Node o)
        {
            return o.score - score;
        }

        public int compareTo(Object obj)
        {
            return compareTo((Node)obj);
        }

        GeneralBlock block;
        int score;

        public Node(GeneralBlock gb, int s)
        {
            block = gb;
            score = s;
        }
    }


    public GreedyOnMaximalSpace()
    {
    }

    public static ExecutionResult solve(int blockType)
    {
        ExecutionResult exeResult = new ExecutionResult();
        long start = System.currentTimeMillis();
        GeneralBlock blockList[] = BlockGenerator.generateBlock(blockType);
        exeResult.blockGenerationTime = System.currentTimeMillis() - start;
        exeResult.blockCount = blockList.length;
        State state = new State();
        state.initialze();
        while(!state.spaceList.isEmpty()) 
        {
            calculateUsableSize(state.freeBoxes);
            Space space = chooseMaximalSpace(state);
            GeneralBlock b = chooseBlock(state, blockList, space);
            if(b == null)
            {
                state.spaceList.remove(space);
            } else
            {
                packBoxes(state, space, b);
                blockList = updateBlockList(state.freeBoxes, blockList);
            }
        }
        exeResult.solution = Utility.stateToSolution(state);
        exeResult.totalExecutionTime = System.currentTimeMillis() - start;
        return exeResult;
    }

    private static GeneralBlock[] updateBlockList(int freeBoxes[], GeneralBlock blockList[])
    {
        boolean removed[] = new boolean[blockList.length];
        int count = blockList.length;
        for(int i = 0; i < blockList.length; i++)
        {
            GeneralBlock b = blockList[i];
            boolean ok = true;
            int ai[];
            int l = (ai = b.component).length;
            for(int k = 0; k < l; k++)
            {
                int j = ai[k];
                if(freeBoxes[j] >= b.typeCount[j])
                    continue;
                ok = false;
                break;
            }

            if(!ok)
            {
                removed[i] = true;
                count--;
            }
        }

        GeneralBlock newGeneralBlockList[] = new GeneralBlock[count];
        count = 0;
        for(int i = 0; i < blockList.length; i++)
            if(!removed[i])
                newGeneralBlockList[count++] = blockList[i];

        return newGeneralBlockList;
    }

    private static Space chooseMaximalSpace(State state)
    {
        ArrayList spaceList = state.spaceList;
        SortedTriple minimalDistance = null;
        Space optimal = null;
        for(Iterator iterator = spaceList.iterator(); iterator.hasNext();)
        {
            Space s = (Space)iterator.next();
            SortedTriple distance = s.cornerDistance();
            int compareResult = 0;
            if(minimalDistance != null)
                compareResult = distance.compareTo(minimalDistance);
            if(minimalDistance == null || compareResult < 0 || compareResult == 0 && s.getVolume() > optimal.getVolume())
            {
                minimalDistance = distance;
                optimal = s;
            }
        }

        return optimal;
    }

    private static GeneralBlock chooseBlock(State state, GeneralBlock blockList[], Space s)
    {
        GeneralBlock best = null;
        int score = 0x80000001;
        GeneralBlock ageneralblock[];
        int k = (ageneralblock = blockList).length;
        for(int j = 0; j < k; j++)
        {
            GeneralBlock b = ageneralblock[j];
            if(b.length <= s.length() && b.width <= s.width() && b.height <= s.height())
            {
                boolean okay = true;
                int ai[];
                int i1 = (ai = b.component).length;
                for(int l = 0; l < i1; l++)
                {
                    int i = ai[l];
                    if(state.freeBoxes[i] >= b.typeCount[i])
                        continue;
                    okay = false;
                    break;
                }

                if(okay)
                {
                    int lost = wasteSpace(s, b);
                    int value = b.boxVolume - lost;
                    if(value > score)
                    {
                        best = b;
                        score = value;
                    }
                }
            }
        }

        return best;
    }

    private static void packBoxes(State state, Space s, GeneralBlock b)
    {
        int ai[];
        int dy = (ai = b.component).length;
        for(int j = 0; j < dy; j++)
        {
            int i = ai[j];
            state.freeBoxes[i] -= b.typeCount[i];
        }

        Space taken = new Space();
        int dx = b.length;
        dy = b.width;
        int dz = b.height;
        if(s.cornerID >= 4)
        {
            taken.z1 = s.z2;
            dz = -dz;
        } else
        {
            taken.z1 = s.z1;
        }
        int p = s.cornerID % 4;
        if(p == 0)
        {
            taken.x1 = s.x1;
            taken.y1 = s.y1;
        } else
        if(p == 1)
        {
            taken.x1 = s.x2;
            taken.y1 = s.y1;
            dx = -dx;
        } else
        if(p == 2)
        {
            taken.x1 = s.x2;
            taken.y1 = s.y2;
            dx = -dx;
            dy = -dy;
        } else
        {
            taken.x1 = s.x1;
            taken.y1 = s.y2;
            dy = -dy;
        }
        taken.x2 = taken.x1 + dx;
        taken.y2 = taken.y1 + dy;
        taken.z2 = taken.z1 + dz;
        taken.maintain();
        state.placedBlock.add(new PlacedBlock(taken.x1, taken.y1, taken.z1, b));
        state.volume += b.boxVolume;
        updateSpaceList(state, s, taken);
    }

    private static void updateSpaceList(State state, Space s, Space taken)
    {
        ArrayList spaceList = state.spaceList;
        if(taken.getVolume() == s.getVolume())
            spaceList.remove(s);
        ArrayList mList = new ArrayList();
        for(Iterator iterator = spaceList.iterator(); iterator.hasNext();)
        {
            Space space = (Space)iterator.next();
            Space is = space.intersect(taken);
            if(is == null)
                mList.add(space);
            else
                space.cut(is, mList);
        }

        Collections.sort(mList, yComparator);
        int set[] = new int[mList.size()];
        int count = 0;
        spaceList.clear();
        for(int i = 0; i < mList.size(); i++)
        {
            Space si = (Space)mList.get(i);
            boolean removed = false;
            boolean ok = false;
            for(int k = 0; k < state.freeBoxes.length && !ok; k++)
                if(state.freeBoxes[k] != 0)
                {
                    int ai[][];
                    int i1 = (ai = ContainerLoading.boxes[k].variation).length;
                    for(int l = 0; l < i1; l++)
                    {
                        int variation[] = ai[l];
                        if(si.width() < variation[0] || si.length() < variation[1] || si.height() < variation[2])
                            continue;
                        ok = true;
                        break;
                    }

                }

            if(!ok)
            {
                removed = true;
            } else
            {
                for(int j = 0; j < count;)
                    if(((Space)mList.get(set[j])).y2 <= si.y1)
                        set[j] = set[--count];
                    else
                        j++;

                for(int j = count - 1; j >= 0; j--)
                {
                    if(!((Space)mList.get(set[j])).contains(si))
                        continue;
                    removed = true;
                    break;
                }

                if(!removed)
                {
                    set[count++] = i;
                    spaceList.add((Space)mList.get(i));
                }
            }
        }

    }

    private static int[] knapsack(int capacity, int value[], int count[])
    {
        int opt[] = new int[capacity + 1];
        opt[0] = 1;
        for(int j = 0; j < value.length; j++)
        {
            for(int i = capacity; i >= 0; i--)
                if(opt[i] != 0)
                {
                    int kValue = i + value[j];
                    for(int k = 1; k <= count[j] && kValue <= capacity; k++)
                    {
                        opt[kValue] = 1;
                        kValue += value[j];
                    }

                }

        }

        for(int i = 0; i <= capacity; i++)
            if(opt[i] == 1)
                opt[i] = i;
            else
                opt[i] = opt[i - 1];

        return opt;
    }

    private static int wasteSpace(Space s, GeneralBlock b)
    {
        int length = s.length() - b.length;
        int width = s.width() - b.width;
        int height = s.height() - b.height;
        return (length - xKnap[length]) * s.width() * s.height() + s.length() * (width - yKnap[width]) * s.height() + s.length() * s.width() * (height - zKnap[height]);
    }

    private static void calculateUsableSize(int freeBoxes[])
    {
        Box boxes[] = ContainerLoading.boxes;
        int length = ContainerLoading.length;
        int width = ContainerLoading.width;
        int height = ContainerLoading.height;
        int size = Math.max(length, width);
        if(size < height)
            size = height;
        int value[] = new int[size + 1];
        int count = 0;
        for(int i = 0; i < boxes.length; i++)
        {
            Box b = boxes[i];
            int ai[][];
            int k = (ai = b.variation).length;
            for(int j = 0; j < k; j++)
            {
                int variation[] = ai[j];
                int l = variation[1];
                if(value[l] == 0)
                    count++;
                value[l] += freeBoxes[i];
            }

        }

        int xValue[] = new int[count];
        int xCount[] = new int[count];
        count = 0;
        for(int i = 1; i <= length; i++)
            if(value[i] != 0)
            {
                xValue[count] = i;
                xCount[count++] = value[i];
            }

        xKnap = knapsack(length, xValue, xCount);
        Arrays.fill(value, 0);
        count = 0;
        for(int i = 0; i < boxes.length; i++)
        {
            Box b = boxes[i];
            int ai1[][];
            int j1 = (ai1 = b.variation).length;
            for(int i1 = 0; i1 < j1; i1++)
            {
                int variation[] = ai1[i1];
                int w = variation[0];
                if(value[w] == 0)
                    count++;
                value[w] += freeBoxes[i];
            }

        }

        int yValue[] = new int[count];
        int yCount[] = new int[count];
        count = 0;
        for(int i = 1; i <= width; i++)
            if(value[i] != 0)
            {
                yValue[count] = i;
                yCount[count++] = value[i];
            }

        yKnap = knapsack(width, yValue, yCount);
        Arrays.fill(value, 0);
        count = 0;
        for(int i = 0; i < boxes.length; i++)
        {
            Box b = boxes[i];
            int ai2[][];
            int l1 = (ai2 = b.variation).length;
            for(int k1 = 0; k1 < l1; k1++)
            {
                int variation[] = ai2[k1];
                int h = variation[2];
                if(value[h] == 0)
                    count++;
                value[h] += freeBoxes[i];
            }

        }

        int zType[] = new int[count];
        int zValue[] = new int[count];
        count = 0;
        for(int i = 1; i <= height; i++)
            if(value[i] != 0)
            {
                zType[count] = i;
                zValue[count++] = value[i];
            }

        zKnap = knapsack(height, zType, zValue);
    }

    private static Comparator yComparator = new Comparator() {

        public int compare(Space a, Space b)
        {
            if(a.y1 != b.y1)
                return a.y1 - b.y1;
            if(a.y2 != b.y2)
                return b.y2 - a.y2;
            if(a.x1 != b.x1)
                return a.x1 - b.x1;
            if(a.x2 != b.x2)
                return b.x2 - a.x2;
            if(a.z1 != b.z1)
                return a.z1 - b.z1;
            else
                return b.z2 - a.z2;
        }

        public int compare(Object obj, Object obj1)
        {
            return compare((Space)obj, (Space)obj1);
        }

    };
    private static int xKnap[];
    private static int yKnap[];
    private static int zKnap[];

}