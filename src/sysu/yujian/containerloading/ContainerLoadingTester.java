package sysu.yujian.containerloading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ContainerLoadingTester
{
  public static void main(String[] args)
    throws IOException
  {
    int pidStart = 0;
    int pidEnd = 0;
    int processedNumber = 1;
    int searchTime = 1000;
    int blockType = 2;
    int version = 2;

    switch (args.length) {
    case 6:
      version = Integer.parseInt(args[5]);
    case 5:
      blockType = Integer.parseInt(args[4]);
    case 4:
      searchTime = Integer.parseInt(args[3]);
      searchTime *= 1000;
    case 3:
      processedNumber = Integer.parseInt(args[2]);
    case 2:
      pidEnd = Integer.parseInt(args[1]);
    case 1:
      pidStart = Integer.parseInt(args[0]);
    }

//    //system.out.println("Args: pid = " + pidStart + " - " + pidEnd + ", processedNumber = " + processedNumber + ", searchTime = " + 
//      searchTime + ", blockType = " + blockType);
//
    File resultDir = new File("result");
    resultDir.mkdirs();
//    File summaryFile = new File(resultDir, "sumamry.csv");
//    PrintWriter pw = new PrintWriter(new FileWriter(summaryFile));
//    pw.println("test set, test case, utilization, time (ms), block gen time (ms), block count");

    for (int pid = pidStart; pid <= pidEnd; ++pid)
    {
      ProblemLoader problemLoader = new ProblemLoader(" 1 1 999 135 130 125 3 1 60 1 30 1 20 1 5 2 80 1 55 1 38 1 4 3 90 1 55 1 40 1 5");

      double totalUtilization = 0D;
      double totalTime = 0D;

      int processed = 0;
      for (processed = 0; (processed < processedNumber) && (problemLoader.hasNextInstance()); ++processed)
      {
        Instance instance = problemLoader.getNextInstance();

        ContainerLoading solver = new ContainerLoading(instance);
        ExecutionResult exeResult = solver.solve(searchTime, blockType, version);
        Solution solution = exeResult.solution;
        if (solution == null) continue;

        totalTime += solver.excutionTime * 0.001D;
        totalUtilization += solution.getUtilization();

        File dir = new File(resultDir, "BR" + pid + "/");
        dir.mkdirs();
        File nbFile = new File(dir, "instance" + instance.getID() + ".nb");
        solution.draw(nbFile.getCanonicalPath());
        
       // //system.out.println("Packed Boxes: "+solution.getPlacedCuboid().size());

        FileWriter fw = new FileWriter(new File(dir, "instance" + instance.getID() + ".txt"));
        fw.write(solution.toString());
        fw.close();
//
//        //system.out.println("Instance " + instance.getID() + ": " + solution.getUtilization());
//        //system.out.println("Time Used: " + (solver.excutionTime / 1000.0D));
//
//        pw.println("BR" + pid + "," + instance.getID() + "," + solution.getUtilization() + "," + exeResult.totalExecutionTime + "," + exeResult.blockGenerationTime + "," + exeResult.blockCount);
//        pw.flush();
      }

//      //system.out.println("Average Utilization: " + (totalUtilization / processed));
//      //system.out.println("Avg Time(sec): " + (totalTime / processed));
    }

//    pw.close();
  }
}