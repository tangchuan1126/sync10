package sysu.yujian.containerloading;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class ProblemLoader
{
  private Scanner cin = null;
  private int problemCount;
  private int processed;

  public ProblemLoader(String path)
    throws IOException
  {
	
//	ByteArrayInputStream bis =  new ByteArrayInputStream(path.getBytes());
//  this.cin = new Scanner(new File(path));
	this.cin = new Scanner(path);
	this.problemCount = this.cin.nextInt();
    this.processed = 0; }

  public int size() {
    return this.problemCount; }

  public boolean hasNextInstance() {
    return (this.processed < this.problemCount); }

  public Instance getNextInstance() throws IOException {
    Instance instance = new Instance();
    instance.load(this.cin);
    this.processed += 1;
    return instance;
  }
}