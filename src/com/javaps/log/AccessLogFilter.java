/**
 * ### 1 名称
 * AccessLogFilter
 * 
 * ### 2 用途
 * 向logserv系统记录Web请求的日志
 * 
 * ### 3 用法
 * 配置一个全局过滤器（指定拦截模式），在那个过滤器中，通过Spring容器找到这个Bean，然后调用这个Bean的方法来记录日志
 * ***注意：***过滤器在web.xml中的次序很关键，这个过滤器必须位于`UrlRewriteFilter`的后面，也就是说需要在进行URL重写之后！
 * 
 * ### 4 实现
 * 
 * ### 4.1 主要逻辑
 * 1. 需记录HTTP请求的各项参数（几乎是请求头所有内容），再加上在session中的key为`Config.adminSesion`所指定的对象。
 * 2. 需根据请求URI中的`/page/`或`/action/`前缀来区分请求的类型为page或action
 * 3. 需参照一个初始化好的URL忽略表来忽略不需要记录日志的请求，这个表初始化需求见下节
 * 
 * ### 4.2 初始化逻辑
 * 1. 可使用Spring配置文件来初始化，并注入有关参数，参数如下：
 * 		- 日志服务器REST服务请求参数
 *      - 应用发布的根目录绝对路径
 * 		- 一组XML文件的相对于应用发布根WEB-INF的路径，如`/conf/zj/actionController-entityReadMapping.xml`
 * 	
 * 2. 第二个配置参数可能无法在spring配置文件中指定，建议由Filter在初始化时为这个Bean设置
 * 
 * 3. 第三个配置参数set方法中，需要parse那些XML文件，找到以下上下文结构下的`prop`节点的key属性加入URL忽略表
 * 
 * 		<beans>
 *			<bean  class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
 *				<property name="mappings"> 
 *					<props>
 *						<prop key="..." >...</prop>
 *          		</props>
 *      		</property>
 * 			</bean>
 * 		</beans>   
 */
package com.javaps.log;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.AccessLogBean;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.key.LogServModelKey;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AccessLogFilter implements Filter {
	static Logger log = Logger.getLogger("PLATFORM");

	private FilterConfig config;
	private Set<String> ignoreUris = new HashSet<String>();

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		String requestUri = ((HttpServletRequest) req).getRequestURI();
		// log.info("AccessLogFilter requestUri:"+requestUri);
		String contextPath = config.getServletContext().getContextPath();
		if (contextPath.length() > 0) {
			requestUri = requestUri.substring(contextPath.length());
		}
		// 忽略掉未登录的请求
		HttpSession ses = StringUtil.getSession((HttpServletRequest) req);
		AdminLoginBean ab = (ses != null) ? new AdminMgr().getAdminLoginBean(ses) : null;
		if ( ab != null && ab.getAccount()!=null) {
			if (!ignoreUris.contains(requestUri)) {
				FloorLogMgrIFace logMgr = (FloorLogMgrIFace) MvcUtil.getBeanFromContainer("logMgr");
				try {
					JSONObject alb =MvcUtil.accessLog(requestUri,(HttpServletRequest) req);
					logMgr.addLog(LogServModelKey.ACCESS_LOG,alb, true);
				} catch (JSONException e) {
					e.printStackTrace();
					log.error("AccessLogFilter: logMgr.accessLog fail", e);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("AccessLogFilter: logMgr.accessLog fail", e);
				}
			} else {
				log.info("AccessLogFilter ignore:" + requestUri);
			}
		} else {
			log.info("AccessLogFilter unauth:" + requestUri);
		}
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;

		String confPath = config.getServletContext().getRealPath("/");
		String[] ignoreMappingConfig = config.getInitParameter(
				"ignoreMappingConfig").split("\\s+");
		try {
			// 分析这些XML文件，抽取数据，填充ignoreUris集合
			for (String fpath : ignoreMappingConfig) {
				if (fpath.length() == 0)
					continue; // 忽略分割以后可能产生的空串
				fpath = confPath + fpath;
				SAXReader reader = new SAXReader();
				Document document = reader.read(new File(fpath));
				List<Node> props = document
						.selectNodes("//beans/bean[@class='org.springframework.web.servlet.handler.SimpleUrlHandlerMapping']/property[@name='mappings']/props/prop");
				for (Node n : props) {
					ignoreUris.add(n.valueOf("@key"));
				}
			}

			// 启动Redis Queue监听器
			RedisMessageListenerContainer redisMessageListenerContainer = (RedisMessageListenerContainer) MvcUtil
					.getBeanFromContainer("redisMessageListenerContainer");
			MessageListenerAdapter redisMessageListenerAdapter = (MessageListenerAdapter) MvcUtil
					.getBeanFromContainer("redisMessageListenerAdapter");
			ChannelTopic eventTopic = new ChannelTopic("logserv:event");
			redisMessageListenerContainer.addMessageListener(
					redisMessageListenerAdapter, eventTopic);
		} catch (Exception e) {
			log.fatal("AccessLogFilter init failed", e);
			throw new RuntimeException(e);
		}
	}

	/* 原来用于截获请求流，并为过滤器链中后续的过滤器和处理器提供可重新读取的流，目前暂时无用
	class StringReaderHttpServletRequest extends HttpServletRequestWrapper {
		private String body;

		StringReaderHttpServletRequest(HttpServletRequest req, String s) {
			super(req);
			this.body = s;
		}

		@Override
		public BufferedReader getReader() throws IOException {
			return new BufferedReader(new StringReader(body));
		}

		@Override
		public ServletInputStream getInputStream() throws IOException {
			return new StringServletInputStream(this.body);
		}

	}

	public static class StringServletInputStream extends ServletInputStream {
		private ByteArrayInputStream bytes;

		public StringServletInputStream(String s) {
			try {
				this.bytes = new ByteArrayInputStream(s.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				log.error("AccessLogFilter body not UTF-8 encoded", e);
				this.bytes = new ByteArrayInputStream(s.getBytes());
			}
		}

		@Override
		public int available() throws IOException {
			return bytes.available();
		}

		@Override
		public void close() throws IOException {
			bytes.close();
		}

		@Override
		public synchronized void mark(int readlimit) {
			bytes.mark(readlimit);
		}

		@Override
		public boolean markSupported() {
			return bytes.markSupported();
		}

		@Override
		public int read() throws IOException {
			return bytes.read();
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return bytes.read(b, off, len);
		}

		@Override
		public int read(byte[] b) throws IOException {
			return bytes.read(b);
		}

		@Override
		public synchronized void reset() throws IOException {
			bytes.reset();
		}

		@Override
		public long skip(long n) throws IOException {
			return bytes.skip(n);
		}

	}
	*/
}
