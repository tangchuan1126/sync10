/**
 * 
 */
package com.javaps.test.log;

import static com.javaps.test.TestUtils.random;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.log.AccessLogBean;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.key.LogServModelKey;

/**
 * @author frank
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/com/javaps/test/log/applicationContext.xml" })
public class LogMgrTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private FloorLogMgrIFace logMgr;

	@Resource
	private RedisMessageListenerContainer redisMessageListenerContainer;

	@Resource
	private MessageListenerAdapter redisMessageListenerAdapter;

	@Resource
	private MessageListenerAdapter redisMessageListenerAdapter2;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Thread.sleep(1000);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		final ChannelTopic eventTopic = new ChannelTopic("logserv:event");
		redisMessageListenerContainer.addMessageListener(
				redisMessageListenerAdapter, eventTopic);

		// 增加另一个listener作为干扰
		if (redisMessageListenerAdapter2 != null) {
			new Thread() {
				@Override
				public void run() {
					redisMessageListenerContainer.addMessageListener(
							redisMessageListenerAdapter2, eventTopic);
				}
			}.start();
		}

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}
/*
	//@Test
	public void testAvailableProductStoreLog() {
		try {
			ProductStoreLogBean psLog = new ProductStoreLogBean();

			psLog.setOid(random(1000000, 100000));
			psLog.setCancel_psl_id(random(1000000, 100000));
			psLog.setAdid(random(10000, 1000));
			psLog.setBill_type(random(1, 50));
			psLog.setOperation(random(1, 50));
			psLog.setPc_id(random(100000, 10000));
			psLog.setPs_id(random(1000, 100));
			psLog.setQuantity(random(0, 2) > 0 ? 1 : -1);
			psLog.setAccount("hanlong");
			psLog.setTitle_id(random(10000, 1000));
			psLog.setProduct_line_id(random(10000, 1000));
			psLog.setQuantity_type(1);
			psLog.setLot_number(random(1000000, 100000) + "");
			psLog.setCatalogs(new long[] { random(10000, 1000) });

			String returnId = logMgr.availableProductStoreLog(psLog, false);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}

	}

	//@Test
	public void testAllocateProductStoreLog() {
		try {
			ProductStoreAllocateLogBean psab = new ProductStoreAllocateLogBean();
			psab.setAllocate_adid(random(10000, 1000));
			psab.setPc_id(random(100000, 10000));
			psab.setPs_id(random(1000, 100));
			psab.setSlc_id(random(10000, 1000));
			psab.setAllocate_container_count(-1);
			psab.setAllocate_piece_count(-1);
			psab.setTitle_id(random(10000, 1000));
			psab.setSystem_bill_id(random(1000000, 100000));
			psab.setSystem_bill_type(random(1, 50));
			psab.setCon_id(random(1000000, 100000));
			psab.setContainer_type_id(random(100000, 10000));
			psab.setContainer_type(random(1, 50));
			psab.setLot_number(random(1000000, 100000) + "");
			psab.setProduct_line_id(random(10000, 1000));
			psab.setCatalogs(new long[] { random(10000, 1000) });
			String returnId = logMgr.allocateProductStoreLog(psab, false);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}*/

	/*// @Test
	public void testPhysicalProductStoreLog() {
		try {
			ProductStorePhysicalLogBean pspb = new ProductStorePhysicalLogBean();
			pspb.setOperation_adid(random(10000, 1000));
			pspb.setOperation_type(random(1, 50));
			pspb.setPc_id(random(100000, 10000));
			pspb.setPs_id(random(1000, 100));
			pspb.setSystem_bill_type(random(1, 50));
			pspb.setSystem_bill_id(random(1000000, 100000));
			pspb.setSlc_id(random(10000, 1000));
			pspb.setLp_id(random(1000000, 100000));
			pspb.setMachine(random(1000000, 100000) + "");
			pspb.setTitle_id(random(10000, 1000));
			pspb.setQuantity(random(0, 2) > 0 ? 1 : -1);
			pspb.setProduct_line_id(random(10000, 1000));
			pspb.setCatalogs(new long[] { random(10000, 1000) });
			pspb.setSerial_number(random(1000000, 100000) + "");
			pspb.setLot_number(random(1000000, 100000) + "");
			String returnId = logMgr.physicalProductStoreLog(pspb, false);
			assertTrue(returnId != null && returnId.length() > 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}

	// @Test
	public void testSerialNumberLog() {
		try {
			SerialNumberLogBean snlb = new SerialNumberLogBean();
			snlb.setPc_id(10000);
			snlb.setSupplier_id(100001);
			snlb.setSerialNumber("797161901");
			snlb.setTitle_id(10000);
			String returnId = logMgr.serialNumberLog(snlb, false);
			assertTrue(returnId != null && returnId.length() > 0);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	/*// @Test
	public void testInboundLog() {
		try {
			InboundLogBean ilb = new InboundLogBean();
			ilb.setPc_id(10000);
			ilb.setCount(100);
			ilb.setSerial_number("797161901");
			ilb.setInbound_adid(100025);
			ilb.setTransport_id(100013);
			ilb.setP_code("B/H1/43K");
			ilb.setP_name("BULB/H1/43K");
			ilb.setMachine_id("machine_id");
			ilb.setContainer_id(1);
			String returnId = logMgr.inboundLog(100013, ilb, false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}

	// @Test
	public void testOutboundLog() {
		try {
			OutboundLogBean olb = new OutboundLogBean();
			olb.setPc_id(10000);
			olb.setCount(100);
			olb.setOutbound_adid(100025);
			olb.setSerial_number("797161901");
			olb.setTransport_id(100013);
			olb.setP_code("B/H1/43K");
			olb.setP_name("BULB/H1/43K");
			olb.setMachine_id("machine_id");
			olb.setContainer_id(1);
			String returnId = logMgr.outboundLog(100013, olb, false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	/*// @Test
	public void testBackUpProductStorage() {
		try {
			ProductStorageBackUpBean logBean = new ProductStorageBackUpBean();
			logBean.setCatalogs(new long[] { 100004, 100005 });
			logBean.setPc_id(10000);
			logBean.setPs_id(100000);
			logBean.setProduct_line_id(100001);
			String returnId = logMgr.backUpProductStorage(logBean, false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	/*// @Test
	public void testBackUpProductStorageTitle() {
		try {
			ProductStorageTitleBackUpBean logBean = new ProductStorageTitleBackUpBean();
			logBean.setCatalogs(new long[] { 100004, 100005 });
			logBean.setPc_id(10000);
			logBean.setPs_id(100000);
			logBean.setProduct_line_id(100001);
			logBean.setLot_number("999999");
			logBean.setTitle_id(1);
			String returnId = logMgr.backUpProductStorageTitle(logBean, false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}

	}*/
/*
	// @Test
	public void testBackUpProductStorageLocation() {
		try {
			ProductStorageLocationBackUpBean logBean = new ProductStorageLocationBackUpBean();
			logBean.setCatalogs(new long[] { 100004, 100005 });
			logBean.setPc_id(10000);
			logBean.setPs_id(100000);
			logBean.setProduct_line_id(100001);
			logBean.setP_code("B/H1/43K");
			logBean.setLot_number("999999");
			logBean.setTitle_id(1);
			logBean.setTime_number(1);
			logBean.setSlc_id(1);
			logBean.setSystem_bill_id(10000);
			logBean.setSystem_bill_type(3);
			logBean.setPosition("position");
			String returnId = logMgr.backUpProductStorageLocation(logBean,
					false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	/*// @Test
	public void testBackUpProductStorageContainer() {
		try {
			ProductStorageContainerBackUpBean logBean = new ProductStorageContainerBackUpBean();
			logBean.setCatalogs(new long[] { 100004, 100005 });
			logBean.setPc_id(10000);
			logBean.setProduct_line_id(100001);
			logBean.setLot_number("999999");
			logBean.setTitle_id(1);
			logBean.setSlc_id(1);
			logBean.setContainer_type_id(1);
			logBean.setContainer_type(1);
			logBean.setCon_id(1);
			String returnId = logMgr.backUpProductStorageContainer(logBean,
					false);
			System.out.println(returnId);
			assertTrue(returnId != null && returnId.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	/*// @Test
	public void testAllocateProductStoreLog_Async() {
		try {
			for (int i = 0; i < 1000; i++) {
				ProductStoreAllocateLogBean psab = new ProductStoreAllocateLogBean();
				psab.setAllocate_adid(random(10000, 1000));
				psab.setPc_id(random(100000, 10000));
				psab.setPs_id(random(1000, 100));
				psab.setSlc_id(random(10000, 1000));
				psab.setAllocate_container_count(-1);
				psab.setAllocate_piece_count(-1);
				psab.setTitle_id(random(10000, 1000));
				psab.setSystem_bill_id(random(1000000, 100000));
				psab.setSystem_bill_type(random(1, 50));
				psab.setCon_id(random(1000000, 100000));
				psab.setContainer_type_id(random(100000, 10000));
				psab.setContainer_type(random(1, 50));
				psab.setLot_number(random(1000000, 100000) + "#" + i);
				psab.setProduct_line_id(random(10000, 1000));
				psab.setCatalogs(new long[] { random(10000, 1000) });
				logMgr.allocateProductStoreLog(psab, true);
				Thread.sleep(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
*/
	//@Test
	public void testAccessLog_Async() {
		try {
			for (int i = 0; i < 1; i++) {
				AccessLogBean alb = new AccessLogBean();
				alb.setAccount("hanlong");
				alb.setAdgid(random(10000, 1000));
				alb.setAdid(random(10000, 1000));
				alb.setContent_type("application/x-www-form-urlencoded");
				alb.setEmploye_name("hanlong");
				alb.setLocal_addr("127.0.0.1");
				alb.setLocal_port(80);
				alb.setLocale("zh_CN");
				alb.setMethod("POST");
				alb.setMultipart(false);
				alb.setOperate_type(2);
				alb.setRemote_addr("192.168.1.38");
				alb.setRequest_uri("/action/TEST_BY_FRANK_JUNIT#" + i);
				alb.setSecure(random(0, 2) > 0);
				alb.setUser_agent("Mozilla/4.0 #" + i);

				List<Map<String, Object>> pl = new ArrayList<Map<String, Object>>();
				Map<String, Object> pm = new HashMap<String, Object>();
				pm.put("key", "singlevalue_param");
				pm.put("value", "简单参数#" + i);
				pl.add(pm);
				pm = new HashMap<String, Object>();
				pm.put("key", "multivalue_param");
				pm.put("value",
						new String[] { "值1#" + i, "值2#" + i,
								"值3#" + i });
				pl.add(pm);

				alb.setParameters(pl);
				
				JSONObject json = new JSONObject(alb);
				//json.remove("local_port");
				logMgr.addLog("AccessLog", json, false);
				//logMgr.accessLog(alb, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
	

	//@Test
	public void testAddLog() throws Exception{
		
		try {
			String name = LogServModelKey.TMS_TRACK;  //TmsTrack
			JSONObject json = new JSONObject();
			json.put("wh_id", 1000005)
			.put("module", 1)
			.put("operation", 1)
			.put("invoices_type", 2)
			.put("invoices_no", "21011101")
			.put("p_invoices_type", "1")
			.put("p_invoices_no", "1011101")
			.put("previous_state", "1")
			.put("after_state", "2")
			.put("content", "")
			.put("file_urls", new String[]{	"http://192.168.1.15/files/1.jpg",	"http://192.168.1.15/files/2.jpg"})
			.put("operator", "10000089");
			
			String id = logMgr.addLog(name, json, false);
			
			System.out.println("return value: "+id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testBasicDataSetupLog() throws Exception{
		
		try {
			String name = LogServModelKey.TMS_TRACK;  //TmsTrack
			JSONObject json = new JSONObject();
			json.put("event", "delete")
			.put("table", "product")
			.put("pri_key", 123)
			.put("ref_table", "")
			.put("ref_pri_key", 0)
			.put("content", new JSONObject[]{new JSONObject().put("name", "name1").put("age", 23),new JSONObject().put("name", "name2").put("age", 24)})
			.put("creat_by", new JSONObject().put("id", 245).put("name", "creat_name"))
			.put("device", new JSONObject().put("id", 1).put("name", "web"))
			.put("describe", "describe describe");
			
			
			String id = logMgr.addLog("BasicDataSetup", json, false);
			
			System.out.println("return value: "+id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
