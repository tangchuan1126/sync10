package com.javaps.test.log;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/com/javaps/test/log/applicationContext.xml" })

public class LogMgrTestSuite  {
	@Test 
	public void run() {
        JUnitCore.runClasses(LogMgrTest.class);
        JUnitCore.runClasses(LogFilterTest.class);
    }
}
