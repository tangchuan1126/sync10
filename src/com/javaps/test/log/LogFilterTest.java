/**
 * 
 */
package com.javaps.test.log;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertNotEquals;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.PropertyUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.log.FilterAccessLogBean;
import com.cwc.app.beans.log.FilterProductStoreAllocateLogBean;
import com.cwc.app.beans.log.FilterProductStoreLogBean;
import com.cwc.app.beans.log.FilterProductStorePhysicalLogBean;
import com.cwc.app.floor.api.FloorLogFilterIFace;
import com.cwc.app.key.LogServModelKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * @author frank
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/com/javaps/test/log/applicationContext.xml" })
public class LogFilterTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private FloorLogFilterIFace logFilter;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testQueryLogs()throws Exception{
		
		//System.out.println(System.currentTimeMillis());
		
		
		String modelName = LogServModelKey.TMS_TRACK;
		JSONObject queryJson = new JSONObject();
		PageCtrl pageCtrl = new PageCtrl();
		pageCtrl.setPageNo(1);
		pageCtrl.setPageSize(10);
		String start_date = "2015/5/6 1:27:38";
		String end_date = "2015/5/6 5:09:55";
		
		DBRow[] rows = logFilter.queryLogs(modelName, queryJson, pageCtrl, start_date, end_date);
		
		System.out.println(DBRowUtils.multipleDBRowArrayAsJSON(rows).toString(4));
	}
	
	
	//@Test
	public void testGetSearchProductStoreSysLogs_onGroup() {
		try {
			FilterProductStoreLogBean psLog = new FilterProductStoreLogBean();

//			psLog.setOid(1091676);
			psLog.setAdid(10132);
//			psLog.setOperation(45);
//			psLog.setPc_id(105424);
			psLog.setPs_id(1000);
			psLog.setTitle_id(10057);
//			psLog.setProduct_line_id(10596);
//			psLog.setLot_number("1094558");
//			psLog.setCatalogs(10366);
			//onGroup:加聚合条件
			//			0, 无聚合条件
			//			1, sum(quantity)聚合（pc_id,title_id,lot_number)
			
			//in_or_out: 加子条件
			//			1, quantity >0
			//			2, quantity <0
			int onGroup = 1;
			int in_or_out = 1;
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(100);
			pc.setPageNo(1);
			String start_date = "2013-08-01";
			String end_date = "2014-01-01";
			DBRow[] result = logFilter.getSearchProductStoreSysLogs(start_date,end_date,psLog, onGroup, in_or_out, pc);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(result).toString(4));
			assertTrue(result != null && result.length > 0);
			inspect(pc);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}

	}

	//@Test
	public void testGetSearchProductStoreSysLogs() {
		try {
			FilterProductStoreLogBean psLog = new FilterProductStoreLogBean();

//			psLog.setOid(1050562);
			psLog.setAdid(10132);
//			psLog.setOperation(45);
//			psLog.setPc_id(104037);
			psLog.setPs_id(1000);
			psLog.setTitle_id(10057);
//			psLog.setProduct_line_id(10401);
//			psLog.setLot_number("1070871");
//			psLog.setCatalogs(100602);
			int onGroup = 0;
			int in_or_out = 1;
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(100);
			pc.setPageNo(1);
			String start_date = "2013-07-01";
			String end_date = "2013-10-01";
			DBRow[] result = logFilter.getSearchProductStoreSysLogs(start_date,end_date,psLog, onGroup, in_or_out, pc);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(result).toString(4));
			inspect(pc);
			assertTrue(result != null && result.length > 0);
			assertResultMatchBean(result, psLog);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}

	}
	
//	@Test
	public void testGetSearchProductStoreAllocateLogs(){
		try{
			FilterProductStoreAllocateLogBean psLog = new FilterProductStoreAllocateLogBean();
//			psLog.setPs_id(473);
//			psLog.setLot_number("1031631");
//			psLog.setSlc_id(10393);
//			psLog.setProduct_line_id(10092);
//			psLog.setTitle_id(10538);
			psLog.setPc_id(103217);
//			psLog.setCatalogs(10058);
//			psLog.setAllocate_adid(10287);
			
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(100);
			pc.setPageNo(1);
			String start_date = "2014-01-06";
			String end_date = "2014-03-06";
			DBRow[] result = logFilter.getSearchProductStoreAllocateLogs(start_date, end_date, psLog, pc);
			assertTrue(result != null && result.length > 0);
			assertResultMatchBean(result, psLog);
			inspect(pc);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
		
	}
	
//	@Test
	public void testGetSearchProductStorePhysicalLogs(){
		try{
			FilterProductStorePhysicalLogBean psLog = new FilterProductStorePhysicalLogBean();
//			psLog.setPs_id(1076);
//			psLog.setLot_number("1047629");
//			psLog.setSerial_number("1079764");
//			psLog.setProduct_line_id(10799);
			psLog.setPc_id(103217);
			
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(100);
			pc.setPageNo(1);
			String start_date = "2013-11-01";
			String end_date = "2014-02-01";
			DBRow[] result = logFilter.getSearchProductStorePhysicalLogs(start_date, end_date, psLog, pc);
			assertTrue(result != null && result.length > 0);
			assertResultMatchBean(result, psLog);
			inspect(pc);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
	
//	@Test
	public void testGetSearchAccessLog(){
		try{
			FilterAccessLogBean alb = new FilterAccessLogBean();
			alb.setAdgid(10592);
			alb.setAdid(10892);
			alb.setOperate_type(2);
			
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(10);
			pc.setPageNo(1);
			String start_date = "2012-01-01";
			String end_date = "2014-03-01";
			
			DBRow[] result = logFilter.getSearchAccessLog(start_date, end_date, alb, pc);
			assertTrue(result != null && result.length > 0);
			assertResultMatchBean(result, alb);
			
			DBRow detail = logFilter.getSearchDetailAccessLog(result[0].getString("id"));
			assertResultMatchBean(detail, alb);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Exception");
		}
	}
	
	private void assertResultMatchBean(DBRow detail, Object bean) throws Exception {
		assertResultMatchBean(new DBRow[]{ detail }, bean);
	}
	private void assertResultMatchBean(DBRow[] result, Object bean) throws Exception {
		PropertyDescriptor[] pd = PropertyUtils.getPropertyDescriptors(bean);

		for(DBRow r: result){
			for(PropertyDescriptor p: pd){
				if(p.getReadMethod() == null || p.getWriteMethod() == null) continue;
				
				String pn = p.getName();
				
				Object pv = PropertyUtils.getProperty(bean, pn);
				if(pv==null || 
				(pv instanceof Long && ((Long)pv).intValue() == 0) || 
				(pv instanceof Integer && ((Integer)pv).intValue() == 0 ) )  continue;
				
				if(pv instanceof Integer) pv = ((Integer)pv).longValue();
				
				Object v = r.getValue(pn.toUpperCase());
				
				if(v instanceof Integer) v = ((Integer)v).longValue();
				
				if( v instanceof Object[]) {
					Object[] a = (Object[])v;
					boolean found=false;
					for(Object o: a){
						if( o instanceof Integer && pv instanceof Long){
							found = ((Long)pv).longValue() == ((Integer)o).longValue();
						}
						else if (o instanceof Long && pv instanceof Long)
						{
							found = ((Long)pv).longValue() == ((Long)o).longValue();
						}
						else {
							found = o.equals(pv);
						}
						if(found) break;
					}
					assertTrue(found);
				}
				else if (pv instanceof Long && v instanceof Long){
					assertEquals( ((Long)pv).longValue(), ((Long)v).longValue());
				}
				else assertEquals(pv,v);
			}
		}
	}
	
	private void inspect(PageCtrl pc){
		System.out.printf("allCount=%d pageCount=%d pageNo=%d pageSize=%d rowCount=%d\n\n",
						 pc.getAllCount(),
						 pc.getPageCount(),
						 pc.getPageNo(),
						 pc.getPageSize(),
						 pc.getRowCount()
		);
	}
}
