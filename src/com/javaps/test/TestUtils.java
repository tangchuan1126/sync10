package com.javaps.test;

import com.cwc.db.DBRow;

public class TestUtils {

	public static int random(int base, int diff) {
		return (int) (base + Math.floor(Math.random() * diff));
	}

	public static String dbRowAsString(DBRow para) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			sb.append(indent).append(field).append("=")
					.append(para.getValue(field.toString())).append("\n");
		}
		sb.append("}");
		return sb.toString();
	}
}
