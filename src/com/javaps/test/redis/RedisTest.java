package com.javaps.test.redis;

import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/com/javaps/test/applicationContext.xml" })
public class RedisTest extends AbstractJUnit4SpringContextTests {
	@Resource
	private StringRedisTemplate redisTemplate;

	@Resource
	private RedisMessageListenerContainer redisMessageListenerContainer;

	@Resource
	private MessageListenerAdapter redisMessageListenerAdapter;

	private int counter = 0;

	@Before
	public void setUp() throws Exception {
		redisMessageListenerContainer.addMessageListener(
				redisMessageListenerAdapter, new PatternTopic("logserv"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPublish() {
		try {
			for(int i=0;i<100; i++)
				redisTemplate.convertAndSend("logserv", "消息 " + (counter++));
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
}
