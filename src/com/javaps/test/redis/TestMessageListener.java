package com.javaps.test.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

public class TestMessageListener {
	@Autowired
	private StringRedisTemplate template;

	public void onMessage(String event) {
		System.out
				.printf("%s [%d]\n", event, Thread.currentThread().hashCode());

		String message = null;
		do {

			BoundListOperations<String, String> ops = template
					.boundListOps("logserv:queue");
			if ((message = ops.rightPop()) != null) {
				//system.out.printf("%s [%d]\n", message, Thread.currentThread()
						//.hashCode());
			} else
				break;
		} while (message != null);

	}

}
