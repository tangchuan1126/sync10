package com.javaps.test.redis;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import com.javaps.test.TestUtils;

@Configuration
public class AppConfig {
	@Bean
	JedisConnectionFactory connectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean
	RedisMessageListenerContainer container(
			final JedisConnectionFactory connectionFactory) {
		RedisMessageListenerContainer container = new RedisMessageListenerContainer() {
			{
				setConnectionFactory(connectionFactory);
			}
		};
		container.addMessageListener(
				listenerAdapter(),
				new ChannelTopic(
				"logserv:event"));
		return container;
	}
	
	@Bean
	TestMessageListener messageListener(){
		return new TestMessageListener();
	}

	@Bean
	MessageListenerAdapter listenerAdapter() {
		return new MessageListenerAdapter(messageListener(), "onMessage");
	}

	@Bean
	StringRedisTemplate template(JedisConnectionFactory connectionFactory) {
		return new StringRedisTemplate(connectionFactory);
	}

	public static void main(String[] args) throws InterruptedException {
		final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
				AppConfig.class);

		
		Thread t = new Thread(new Runnable() {
			public void run() {
				StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);
				//system.out.println("Sending message from another thread...");
				
				for(int i=0; i<1000; i++){
					template.boundListOps("logserv:queue").leftPush("***消息内容#"+i);
					template.convertAndSend("logserv:event", "==》消息通知#"+i);
					try {
						Thread.sleep(TestUtils.random(0, 1000));
					} catch (InterruptedException e) {
						e.printStackTrace();
						break;
					}
				}
			}
		});
		
		t.start();
		t.join();
		ctx.close();
	}
}