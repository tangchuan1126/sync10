package com.javaps.test.db;

import javax.annotation.Resource;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;
import com.javaps.test.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/com/javaps/test/db/applicationContext.xml" })
public class DemoTest extends AbstractJUnit4SpringContextTests {
	@Resource
	private FloorAdminMgr floorAdminMgr;
	
	@Resource
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println(IndexReader.class.getMethod("open", Directory.class));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test_getDetailAdmin(){
		long adid = 100025;
		String account = "hanlong";
		DBRow row=null;
		
		try {
			DBRow[] result = this.dbUtilAutoTranSQLServer.selectMutliple(
					"select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where Status = 'Open'"
					);
			for(DBRow r:result){
				System.out.println(TestUtils.dbRowAsString(r));
			}
			row = floorAdminMgr.getDetailAdmin(adid);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
		assertEquals(row.getString("account"),account);
	}
	
}
