package com.javaps.test.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;
import com.javaps.test.TestUtils;

public class MsaMockup extends ActionFatherController {
	// @Autowired
	private FloorAdminMgr floorAdminMgr;
	private TransactionTemplate txTemplate;

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public FloorAdminMgr getFloorAdminMgr() {
		return floorAdminMgr;
	}

	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PageCtrl pc = new PageCtrl();
		int pageNo = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageSize"), "1000"));
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		DBRow[] result = this.floorAdminMgr.getAllAdmin(pc);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject()
				.put("pageCtrl", new JSONObject(pc)).put("items",
						DBRowUtils.dbRowArrayAsJSON(result));

		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 新建管理员
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		//system.out.println(TestUtils.dbRowAsString(row));
		long adid = this.floorAdminMgr.addAdmin(row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject(DBRowUtils.dbRowAsMap(this.floorAdminMgr
						.getDetailAdmin(adid))).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 更新管理员
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		//system.out.println(TestUtils.dbRowAsString(row));
		long adid = row.get("ADID", 0L);
		this.floorAdminMgr.modifyAdmin(adid, row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		DBRow detail = this.floorAdminMgr.getDetailAdmin(adid);
		
		response.setStatus(detail==null?500:200);
		response.getWriter().print(
			detail ==  null ? new JSONObject().put("success", false).put("error","不存在此管理员："+adid) :
			new JSONObject(DBRowUtils.dbRowAsMap(detail))
		);
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		this.floorAdminMgr.delAdmin(Long.parseLong(request.getParameter("id")));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject().put("success", true).toString());
	}

	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		AdminLoginBean am = new AdminMgr().getAdminLoginBean(request.getSession());
		//system.out.println(DBRowUtils.dbRowArrayAsJSON(am.getTitles()));
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							doGet(request, response);
							break;
						case "POST":
							doPost(request, response);
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							doDelete(request, response);
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}

	}

}
