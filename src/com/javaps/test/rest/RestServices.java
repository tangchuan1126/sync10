package com.javaps.test.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;

public class RestServices extends ActionFatherController {
	
	private List<SimpleUrlHandlerMapping> restMapping;

	public List<SimpleUrlHandlerMapping> getRestMapping() {
		return restMapping;
	}

	public void setRestMapping(List<SimpleUrlHandlerMapping> restMapping) {
		this.restMapping = restMapping;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		switch (request.getMethod()) {
		case "GET":
			doGet(request, response);
			break;
		case "POST":
			doPost(request, response);
			break;
		case "PUT":
			doPut(request, response);
			break;
		case "DELETE":
			doDelete(request, response);
			break;
		}
	}

	private void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONArray result = new JSONArray();
		String cp = request.getServletContext().getContextPath();
		if(cp.equals("/")) cp="";
		
		for(SimpleUrlHandlerMapping m:restMapping){
			for(String p:m.getHandlerMap().keySet()){
				result.put(new JSONObject().put("uri", cp+p));			
			}
		}
		
		response.getWriter().print(result.toString());
	}

	private void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		throw new UnsupportedOperationException();
	}

	private void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		throw new UnsupportedOperationException();
	}

	private void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		throw new UnsupportedOperationException();
	}

}
