package com.javaps.test.rest;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.tjh.OfficeLocationMgrTJH;
import com.cwc.app.floor.api.tjh.FloorOfficeLocationMgrTJH;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.javaps.test.TestUtils;
import com.cwc.app.util.DBRowUtils;

public class OfficeTree extends ActionFatherController {
	// @Autowired
	private FloorOfficeLocationMgrTJH floorOfficeLocationMgrTJH;
	private TransactionTemplate txTemplate;
	private OfficeLocationMgrTJH officeLocationMgrTJH;
	

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		

		
		Map<String,DBRow[]> accountContext = officeLocationMgrTJH.getAllOfficeLocationTree();
		JSONObject output = new JSONObject();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Set<String> key = accountContext.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
         //   //system.out.println(oneKey);
            output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(accountContext.get(oneKey)));
        }

response.getWriter().print(output.toString());



	/*	JSONArray zNodes = officeLocationMgrTJH.getAllOfficeLocationTree();
		response.setContentType("application/json");

		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject().put("define",zNodes);
		response.getWriter().print(output.toString());*/
	}

	
	

	private void setJson(JSONObject output) {
		// TODO Auto-generated method stub
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 新建管理员
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
	
		//system.out.println(TestUtils.dbRowAsString(row));
		DBRow result =	floorOfficeLocationMgrTJH.addOfficeLocation(row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 更新办公地址
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		//system.out.println("jsonData=" + jsonData);
		
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		row.remove("name");
		row.remove("children");
		row.remove("pid");
		//system.out.println(TestUtils.dbRowAsString(row));
		long adid = row.get("id", 0L);
		//system.out.println(adid);
		this.floorOfficeLocationMgrTJH.modOfficeLocation(adid, row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		DBRow detail = this.floorOfficeLocationMgrTJH.getDetailOfficeLocation(adid);
		
		response.setStatus(detail==null?500:200);
		response.getWriter().print(
			detail ==  null ? new JSONObject().put("success", false).put("error","不存在此地点："+adid) :
			new JSONObject(DBRowUtils.dbRowAsMap(detail))
		);
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		this.floorOfficeLocationMgrTJH.delOfficeLocation(Long.parseLong(request.getParameter("id")));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject().put("success", true).toString());
	}

	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							doGet(request, response);
							break;
						case "POST":
							doPost(request, response);
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							doDelete(request, response);
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}

	}

	public FloorOfficeLocationMgrTJH getFloorOfficeLocationMgrTJH() {
		return floorOfficeLocationMgrTJH;
	}

	public void setFloorOfficeLocationMgrTJH(
			FloorOfficeLocationMgrTJH floorOfficeLocationMgrTJH) {
		this.floorOfficeLocationMgrTJH = floorOfficeLocationMgrTJH;
	}

	public OfficeLocationMgrTJH getOfficeLocationMgrTJH() {
		return officeLocationMgrTJH;
	}

	public void setOfficeLocationMgrTJH(OfficeLocationMgrTJH officeLocationMgrTJH) {
		this.officeLocationMgrTJH = officeLocationMgrTJH;
	}

}
