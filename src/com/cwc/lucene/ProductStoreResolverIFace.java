package com.cwc.lucene;

import java.util.List;

import org.apache.lucene.document.Document;

public interface ProductStoreResolverIFace {
	public List<Long> resolve(Document doc)
	throws Exception;
}
