package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
 
public class BatchUpdateTitleIndex extends ThreadAction{

	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");

	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
 
	public BatchUpdateTitleIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"titleName"};	//,"adminUsers","productLines","productCategorys","productNames"
		this.fieldNames = new String[]{"titleName"};	//,"adminUsers","productLines","productCategorys","productNames"
		this.documentKey = "title_id";
		this.mergeKey = "merge_field";
		this.isMergeKey = true;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() 
		throws Exception 
	{
		String sql = "select * from title";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;
		
			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					long title_id = rows[i].get("title_id", 0L);
					rows[i].add("titleId","T"+rows[i].getString("title_id"));
					rows[i].add("titleName",rows[i].getString("title_name"));
					
					ProprietaryMgrIFaceZyj proprietaryMgrZyj = (ProprietaryMgrIFaceZyj)MvcUtil.getBeanFromContainer("proxyProprietaryMgrZyj");
					String adminUsers = "";
					 
					 DBRow[] adminRows = proprietaryMgrZyj.findAdminsByTitleId(true, 0L, title_id, 0, 0, null, null);
					 for (int j = 0; j < adminRows.length; j++) 
					 {
						 adminUsers += " "+adminRows[j].getString("employe_name");
					 }
					 
					 String productLines = "";
					 DBRow[] pcLineRows = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", title_id+"", 0, 0, null, null);
					 for (int j = 0; j < pcLineRows.length; j++)
					 {
						 productLines += " "+pcLineRows[j].getString("name");
					 }
					 
					 String productCategorys = "";
					 DBRow[] pcCataRows = proprietaryMgrZyj.findProductCatagorysByTitleId(true, 0L, "","", "", title_id+"", 0, 0, null, null);
					 for (int j = 0; j < pcCataRows.length; j++)
					 {
						 productCategorys += " "+pcCataRows[j].getString("title");
					 }
					 
					 String productNames = "";
					 DBRow[] productRows = proprietaryMgrZyj.findProductsByTitleId(true, 0L, 0L,null, title_id, 0, 0, null, null);
					 for (int j = 0; j < productRows.length; j++)
					 {
						 productNames += " "+productRows[j].getString("p_name");
					 }
					
					rows[i].add("adminUsers",adminUsers);
					rows[i].add("productLines",productLines);
					rows[i].add("productCategorys",productCategorys);
					rows[i].add("productNames",productNames);
					
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}//for
					
				//indexMgr.updateIndexNoToLowerCase(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				indexMgr.updateIndex(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}
 
	

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
