package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zr.FloorWayBillOrderMgrZR;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

import com.cwc.app.floor.api.zyj.service.*;
import com.cwc.app.floor.api.zyj.model.*;


/**
 * 生成商品索引只可用于商品索引生成
 * @author Administrator
 *
 */
public class BatchUpdateProductIndex extends ThreadAction{
	
	
	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");
	
	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	private ProductCustomerSerivce productCustomerSerivce;
	private int productId = 0;
	
	//如果merge不空，则自动把索引字段merge在一起，然后调用IndexCore.updateIndex的merge版本（Frank新增）
	//merge字段格式： idKey mergeKey
	
 
	public BatchUpdateProductIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer,int pId) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"pc_id","p_name","all_code"};
		this.fieldNames = new String[]{"pc_id","p_name","catalog_id","unit_name","alive","all_code","p_name","all_customer","all_title"};
		this.documentKey = "pc_id";
		this.mergeKey = "merge_info";
		this.isMergeKey = false;
		this.analyzer = analyzer;
		this.productCustomerSerivce = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
		this.productId = pId;
	}

	@Override
	protected void perform() throws Exception {

		String sql = "select p.pc_id,p.p_name,p.catalog_id,p.unit_name,p.alive from product p where orignal_pc_id = 0";

		if(this.productId>0){
			
			sql += " and pc_id="+this.productId;
		}
		
		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;

			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			DBRow[] pcodeRows = null;
			StringBuffer all_code;
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			if(this.productId>0){
				iwc.setOpenMode(OpenMode.APPEND);
			}
			else{
				iwc.setOpenMode(OpenMode.CREATE);
			}
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					long pc_id = rows[i].get("pc_id",0l);
					
					
					String all_customer = "";
					String all_title = "";
					 
					List<Customer> list_customer = this.productCustomerSerivce.getCustomers(Integer.parseInt(pc_id+""));
					List<Title> list_title = this.productCustomerSerivce.getTitles(Integer.parseInt(pc_id+""));
					 
					 
					for(int j=0;j<list_customer.size();j++){
						all_customer += " " + list_customer.get(j).getId() ;
					}
					 
					for(int j=0;j<list_title.size();j++){
						all_title += " " + list_title.get(j).getId();
					}
					 
					rows[i].add("all_customer",all_customer);
					rows[i].add("all_title",all_title);
					
					
					pcodeRows = dbUtil.selectMutliple(String.format("select p_code from product_code where pc_id = %d",pc_id)); 
					
					all_code = new StringBuffer("");
					if(rows != null && rows.length > 0 )
					{
						for(DBRow temp : pcodeRows)
						{
							all_code.append(" "+temp.getString("p_code").trim());
						}
					}
					
					rows[i].add("all_code",all_code.toString());
					
					
					
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).replaceAll("/"," ").trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}//for
					
				indexMgr.updateIndex(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
//		catch (Throwable t) 
//		{
//			javapsLog.fatal(String.format("[%s] ERROR",sql), t);
//		}

	}

	

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public static Object defaultValue(Class<?> cp){
		if(cp.equals(String.class)) return "";
		
		if(cp.equals(int.class) ||
		   cp.equals(long.class) ||
		   cp.equals(short.class)  ||
		   cp.equals(double.class) ||
		   cp.equals(float.class)  ) {
			return 0;
		}
		if(cp.equals(boolean.class)) return false;
		
		return null;
	}
	public static boolean isPrimitiveOf(Class<?> cp, Class<?> cw){
		if(!cp.isPrimitive()) return false;
		if(cw.isPrimitive()) return cp.isAssignableFrom(cw);
		
		if(cp.equals(int.class) && cw.equals(Integer.class) ||
		   cp.equals(long.class) && cw.equals(Long.class)   ||
		   cp.equals(boolean.class) && cw.equals(Boolean.class) ||
		   cp.equals(short.class) && cw.equals(Short.class) ||
		   cp.equals(double.class) && cw.equals(Double.class) ||
		   cp.equals(float.class) && cw.equals(Float.class) ) {
			return true;
		}
		
		return false;
			
	}
	public static String dbRowAsString(DBRow para) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			try {
				sb.append(indent).append(field).append("=")
						.append(para.getValue(field.toString())).append("\n");
			} catch (Exception e) {
				javapsLog.fatal(String.format("ERROR when get field %s from DBRow",
						field),e);
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setProductCustomerSerivce(
			ProductCustomerSerivce productCustomerSerivce) {
		this.productCustomerSerivce = productCustomerSerivce;
	}
	
	
}
