package com.cwc.lucene;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.iface.zr.QuestionMgrIfaceZr;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

public class BatchUpdateQuestionIndex extends ThreadAction
{

	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");
	
	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private int pageSize=1000;
	private Analyzer analyzer;
	
	private QuestionMgrIfaceZr questionMgrZr = (QuestionMgrIfaceZr) MvcUtil.getBeanFromContainer("questionMgrZr");
	
	
	public BatchUpdateQuestionIndex(DBUtilIFace dbUtil, IndexCore indexMgr, Analyzer analyzer)
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.analyzer = analyzer;
	}




	@Override
	protected void perform() throws Exception
	{
		String sql = "select * from question_q";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;
		
			DBRow[] rows = null;
			 
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{ 
					String question_id = (rows[i].getString("question_id"));
					Term term = new Term("question_id", question_id);
					javapsLog.info("question " + question_id + "create.");
					writer.updateDocument(term, getDocumentByDBRow(rows[i]));
 				} 
				
				indexCount += rows.length;
								
				pc += pageSize;
			} 
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	private Document getDocumentByDBRow(DBRow row) throws Exception {
		 Document  document = new  Document();
		 document.add(new StringField("question_id", row.getString("question_id") + "",Store.YES));
		 document.add(new StringField("product_id",  row.getString("product_id") + "",Store.YES));
		 document.add(new StringField("product_catalog_id",row.getString("product_catalog_id")  + "",Store.YES));
		 document.add(new StringField("question_catalog_id", row.getString("question_catalog_id")  + "",Store.YES));
		 document.add(new TextField("question_title",row.getString("question_title") , Store.YES));
		 StringBuffer content = new StringBuffer(row.getString("contents"));
		 content.append( questionMgrZr.getQuestionFilesContent(row.get("question_id", 0l)));
		 document.add(new TextField("content", content.toString(), Store.YES));
 		 return(document);
	}

}
