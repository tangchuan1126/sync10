package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;

public class BatchUpdateIndex extends ThreadAction {
	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");
	
	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String tableName;
	private String condition;
	private String[] fieldNames;
	private int pageSize=1000;
	private int optThreshold=0; //索引优化阀值，0表示全部做完再优化，仅对merge方式有效
	private String documentKey;
	private String[] mergeField;
	private String mergeKey;
	private boolean isMergeKey;
	private Analyzer analyzer;
	
	 
	

	public BatchUpdateIndex(DBUtilIFace dbUtil, IndexCore indexMgr,String documentKey, String mergeKey,
			String tableName, String condition,boolean isMergeKey,String fieldNames,String mergeField,Analyzer analyzer) {
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.tableName = tableName;
		this.condition = condition;
		this.fieldNames = fieldNames.split(",");
		this.documentKey = documentKey;
		this.fieldNames = fieldNames.split(",");
		this.mergeField = mergeField.split(",");
		this.mergeKey = mergeKey;
		this.isMergeKey = isMergeKey;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() throws Exception {

		String sql = selectClause();
		
		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;

			DBRow[] rows=null;
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			String[] mergeValues = new String[pageSize];
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				
				for(int i=0; i<rows.length; i++)
				{
						
						//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f)).append(" ");
					}
						mergeValues[i] = sb.toString().trim();
				}//for
				
				indexMgr.updateIndex(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
				
				
				pc += pageSize;
			}// for
			
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
//		catch (Throwable t) 
//		{
//			javapsLog.fatal(String.format("[%s] ERROR",tableName.toUpperCase()), t);
//		}

	}

	protected String selectClause() {
		StringBuilder sb = new StringBuilder();
		for (String s : fieldNames) {
			sb.append(s).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		
		String whereClause = "";
		if(this.condition!=null && this.condition.length()>0){
			whereClause = "where "+this.condition;
		}
		return String.format("select %s from %s %s", sb.toString(),tableName,whereClause);
	}

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	protected Object[] indexFields(DBRow row,Class<?>[] paraTypes) throws Exception{
		
		Object[] fields = new Object[fieldNames.length];
		
		for (int i=0;i<fieldNames.length;i++) {
			Object v = row.getValue(fieldNames[i]);
			if(paraTypes[i].isInstance(v)){
				fields[i] = v;
			}
			else if(v==null){
				javapsLog.warn(String.format("[%s] NULL value for %s type=%s",
						tableName.toUpperCase(),fieldNames[i],paraTypes[i]));
				fields[i] = defaultValue(paraTypes[i]);
			}
			else if(isPrimitiveOf(paraTypes[i],v.getClass())){
				fields[i] = v;
			}
			else {
				javapsLog.warn(String.format("[%s] Hard cast %s to %s",
						tableName.toUpperCase(),v.getClass().getName(),paraTypes[i]));
				
				fields[i] = paraTypes[i].cast(v);
			}

		}
		return fields;

		
	}
	public static Object defaultValue(Class<?> cp){
		if(cp.equals(String.class)) return "";
		
		if(cp.equals(int.class) ||
		   cp.equals(long.class) ||
		   cp.equals(short.class)  ||
		   cp.equals(double.class) ||
		   cp.equals(float.class)  ) {
			return 0;
		}
		if(cp.equals(boolean.class)) return false;
		
		return null;
	}
	public static boolean isPrimitiveOf(Class<?> cp, Class<?> cw){
		if(!cp.isPrimitive()) return false;
		if(cw.isPrimitive()) return cp.isAssignableFrom(cw);
		
		if(cp.equals(int.class) && cw.equals(Integer.class) ||
		   cp.equals(long.class) && cw.equals(Long.class)   ||
		   cp.equals(boolean.class) && cw.equals(Boolean.class) ||
		   cp.equals(short.class) && cw.equals(Short.class) ||
		   cp.equals(double.class) && cw.equals(Double.class) ||
		   cp.equals(float.class) && cw.equals(Float.class) ) {
			return true;
		}
		
		return false;
			
	}
	public static String dbRowAsString(DBRow para) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			try {
				sb.append(indent).append(field).append("=")
						.append(para.getValue(field.toString())).append("\n");
			} catch (Exception e) {
				javapsLog.fatal(String.format("ERROR when get field %s from DBRow",
						field),e);
			}
		}
		sb.append("}");
		return sb.toString();
	}
	
	/*
	public static String pageCtrlAsString(PageCtrl pc){
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		sb.append(indent).append("allCount=").append(pc.getAllCount()).append("\n")
		  .append(indent).append("pageCount=").append(pc.getPageCount()).append("\n")
		  .append(indent).append("pageNo=").append(pc.getPageNo()).append("\n")
		  .append(indent).append("pageSize=").append(pc.getPageSize()).append("\n")
		  .append(indent).append("rowCount=").append(pc.getRowCount()).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
	*/

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOptThreshold() {
		return optThreshold;
	}

	public void setOptThreshold(int optThreshold) {
		this.optThreshold = optThreshold;
	}

}
