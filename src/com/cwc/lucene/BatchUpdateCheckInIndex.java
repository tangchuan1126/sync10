package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.key.ModuleKey;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;

/**
 * 生成商品索引只可用于商品索引生成
 * @author Administrator
 *
 */
public class BatchUpdateCheckInIndex extends ThreadAction{
	
	
	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");
	
	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
	//如果merge不空，则自动把索引字段merge在一起，然后调用IndexCore.updateIndex的merge版本（Frank新增）
	//merge字段格式： idKey mergeKey
	
 
	public BatchUpdateCheckInIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"mainId","dlo_id","gate_container_no" ,"gate_driver_liscense", "gate_liscense_plate", "company_name" ,"loadNo"};
		this.fieldNames = new String[]{"ps_id"};//"mainId","dlo_id","gate_container_no" ,"gate_driver_liscense", "gate_liscense_plate", "company_name" ,"loadNo"
		this.documentKey = "dlo_id";
		this.mergeKey = "merge_field";
		this.isMergeKey = false;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() throws Exception {

		String sql = "select * from door_or_location_occupancy_main ";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;

			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			DBRow[] pcodeRows = null;
			DBRow[] equipment = null;
			StringBuffer loadNo;
			StringBuffer containerNo;
			StringBuffer liscensePlate;
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					
					long dlo_id = rows[i].get("dlo_id",0l);
					
					rows[i].add("mainId","E"+dlo_id);
					pcodeRows = dbUtil.selectMutliple(String.format("select * from door_or_location_occupancy_details where dlo_id = %d",dlo_id)); 
					equipment = dbUtil.selectMutliple("select * FROM entry_equipment equipment WHERE equipment.check_in_entry_id = "+dlo_id+" or equipment.check_out_entry_id="+dlo_id);
					loadNo = new StringBuffer("");
					containerNo = new StringBuffer();
					liscensePlate = new StringBuffer();
					if(rows != null && rows.length > 0 )
					{
						for(DBRow temp : pcodeRows)
						{
//							if(temp.get("number_type",0l)==ModuleKey.CHECK_IN_LOAD){
								loadNo.append(" "+temp.getString("number"));
//							}
							
						}
						for(DBRow temp : equipment){
							int type = temp.get("equipment_type", 0);
							if(type==1){//车头
								liscensePlate.append(" "+temp.get("equipment_number", ""));
							}else if(type==2){ //车尾
								containerNo.append(" "+temp.get("equipment_number", ""));
							}
						}
					}
					
					rows[i].add("loadNo",loadNo.toString());
					rows[i].add("gate_container_no",containerNo.toString());
					rows[i].add("gate_liscense_plate",liscensePlate.toString());
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).replaceAll("/"," ").trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}//for
					
				indexMgr.updateIndex(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
//		catch (Throwable t) 
//		{
//			javapsLog.fatal(String.format("[%s] ERROR",sql), t);
//		}

	}

	

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public static Object defaultValue(Class<?> cp){
		if(cp.equals(String.class)) return "";
		
		if(cp.equals(int.class) ||
		   cp.equals(long.class) ||
		   cp.equals(short.class)  ||
		   cp.equals(double.class) ||
		   cp.equals(float.class)  ) {
			return 0;
		}
		if(cp.equals(boolean.class)) return false;
		
		return null;
	}
	public static boolean isPrimitiveOf(Class<?> cp, Class<?> cw){
		if(!cp.isPrimitive()) return false;
		if(cw.isPrimitive()) return cp.isAssignableFrom(cw);
		
		if(cp.equals(int.class) && cw.equals(Integer.class) ||
		   cp.equals(long.class) && cw.equals(Long.class)   ||
		   cp.equals(boolean.class) && cw.equals(Boolean.class) ||
		   cp.equals(short.class) && cw.equals(Short.class) ||
		   cp.equals(double.class) && cw.equals(Double.class) ||
		   cp.equals(float.class) && cw.equals(Float.class) ) {
			return true;
		}
		
		return false;
			
	}
	public static String dbRowAsString(DBRow para) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			try {
				sb.append(indent).append(field).append("=")
						.append(para.getValue(field.toString())).append("\n");
			} catch (Exception e) {
				javapsLog.fatal(String.format("ERROR when get field %s from DBRow",
						field),e);
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
