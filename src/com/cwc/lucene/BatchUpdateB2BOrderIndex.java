package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
 

public class BatchUpdateB2BOrderIndex extends ThreadAction{

	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");

	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
 
	public BatchUpdateB2BOrderIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"b2BOrder","pickUpStorage","receiveStorage","b2b_order_waybill_number","b2b_order_waybill_name","carriers","applyMoney","customer_dn","retail_po"};
		this.fieldNames = new String[]{"b2BOrder","pickUpStorage","receiveStorage","b2b_order_waybill_number","b2b_order_waybill_name","carriers","applyMoney","customer_dn","retail_po"};
		this.documentKey = "b2b_oid";
		this.mergeKey = "merge_field";
		this.isMergeKey = true;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() 
		throws Exception 
	{
		String sql = "select * from b2b_order";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;
		
			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			//DBRow[] applyMoneys = new DBRow[0];
		//	DBRow[] applyTransfers = new DBRow[0];
			DBRow pickUpStorage = null;
			DBRow receiveStorage = null;
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					//每个采购单创建索引前先清空一次供应商，资金申请，付款信息
					//applyMoneys = null;
					//applyTransfers = null;

					rows[i].add("b2BOrder","B"+rows[i].getString("b2b_oid"));
					
					pickUpStorage = dbUtil.selectSingle("select * from product_storage_catalog where id = "+rows[i].get("send_psid",0l));
					
					rows[i].add("pickUpStorage",pickUpStorage.get("title",""));
					
					receiveStorage = dbUtil.selectSingle("select * from product_storage_catalog where id = "+rows[i].get("receive_psid",0l));
					if (receiveStorage==null || receiveStorage.size()==0) {
						rows[i].add("receiveStorage","");
					} else {
						rows[i].add("receiveStorage",receiveStorage.get("title",""));
					}
					
					/*
					FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
					
					applyMoneys = floorApplyMoneyMgrZJ.getApplyMoneyByAssociationIdAndType(rows[i].get("b2b_oid",0l),11);
					applyTransfers  = new DBRow[0];
					String applyMoney = "";
					for (int j = 0; j < applyMoneys.length; j++) 
					{
						  applyMoney += "(";
						  
						  applyMoney +="F"+applyMoneys[j].get("apply_id",0l);
						  applyTransfers = floorApplyMoneyMgrZJ.getApplyTransferForApplyMoneyId(applyMoneys[j].get("apply_id",0l));
						  for (int h = 0; h < applyTransfers.length; h++) 
						  {
							applyMoney +=" W"+applyTransfers[h].get("transfer_id",0l);
						  }
						  applyMoney += ") ";
					}
					rows[i].add("applyMoney",applyMoney);
					*/
					
					rows[i].add("applyMoney","");
				
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField) {
						sb.append(rows[i].getString(f).trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}//for
					
				indexMgr.updateIndexNoToLowerCase(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
