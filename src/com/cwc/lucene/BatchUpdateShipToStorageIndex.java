package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;

public class BatchUpdateShipToStorageIndex extends ThreadAction{

	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");

	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
	public BatchUpdateShipToStorageIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"title","city","send_street","send_house_number","send_zip_code","send_contact","country","state"};
		this.fieldNames = new String[]{"title","city","send_street","send_house_number","send_zip_code","send_contact","country","state","storage_type_id","ship_to_name","storage_type"};
		this.documentKey = "id";
		this.mergeKey = "merge_field";
		this.isMergeKey = false;
		this.analyzer = analyzer;
	}
	
	
	
	@Override
	protected void perform() 
		throws Exception 
	{
		String sql = "select psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,"
				+ "psc.pro_id,psc.send_pro_input as state,psc.contact,psc.phone, psc.storage_type, psc.storage_type_id,cp.pro_name, cc.c_country as country, st.ship_to_name"
				+ " from "+ConfigBean.getStringValue("product_storage_catalog")+" psc LEFT JOIN  "+ConfigBean.getStringValue("ship_to")+" st "
						+ "ON psc.storage_type_id = st.ship_to_id LEFT JOIN "
				+ ConfigBean.getStringValue("country_province") + " cp ON psc.pro_id = cp.pro_id "
				+ "LEFT JOIN "+ConfigBean.getStringValue("country_code")+" cc ON psc.native = cc.ccid ";
						

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			int indexCount=0;
		
			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					rows[i].add("id",rows[i].getString("id"));
					
					
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}
					
				indexMgr.updateIndexNoToLowerCase(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}
	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
