package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zr.FloorWayBillOrderMgrZR;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

/**
 * 生成商品索引只可用于商品索引生成
 * @author Administrator
 *
 */
public class BatchUpdateCheckInCarrierIndex extends ThreadAction{
	
	
	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");
	
	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
	//如果merge不空，则自动把索引字段merge在一起，然后调用IndexCore.updateIndex的merge版本（Frank新增）
	//merge字段格式： idKey mergeKey
	
 
	public BatchUpdateCheckInCarrierIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"company_name"};
		this.fieldNames = new String[]{"company_name"};
		this.documentKey = "dlo_id";
		this.mergeKey = "merge_field";
		this.isMergeKey = false;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() throws Exception {

		String sql = "select * from door_or_location_occupancy_main group by company_name";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;

			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
//					pcodeRows = dbUtil.selectMutliple(String.format("select * from door_or_location_occupancy_details where dlo_id = %d",dlo_id)); 
//					
//					loadNo = new StringBuffer("");
//					if(rows != null && rows.length > 0 )
//					{
//						for(DBRow temp : pcodeRows)
//						{
//							if(!temp.getString("load_number").equals("")){
//								loadNo.append(" "+temp.getString("load_number"));
//							}
//							
//						}
//					}
//					
//					rows[i].add("loadNo",loadNo.toString());
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).replaceAll("/"," ")).append(" ");
					}	
					mergeValues[i] = sb.toString();
				}//for
					
				indexMgr.updateIndex(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
//		catch (Throwable t) 
//		{
//			javapsLog.fatal(String.format("[%s] ERROR",sql), t);
//		}

	}

	

	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public static Object defaultValue(Class<?> cp){
		if(cp.equals(String.class)) return "";
		
		if(cp.equals(int.class) ||
		   cp.equals(long.class) ||
		   cp.equals(short.class)  ||
		   cp.equals(double.class) ||
		   cp.equals(float.class)  ) {
			return 0;
		}
		if(cp.equals(boolean.class)) return false;
		
		return null;
	}
	public static boolean isPrimitiveOf(Class<?> cp, Class<?> cw){
		if(!cp.isPrimitive()) return false;
		if(cw.isPrimitive()) return cp.isAssignableFrom(cw);
		
		if(cp.equals(int.class) && cw.equals(Integer.class) ||
		   cp.equals(long.class) && cw.equals(Long.class)   ||
		   cp.equals(boolean.class) && cw.equals(Boolean.class) ||
		   cp.equals(short.class) && cw.equals(Short.class) ||
		   cp.equals(double.class) && cw.equals(Double.class) ||
		   cp.equals(float.class) && cw.equals(Float.class) ) {
			return true;
		}
		
		return false;
			
	}
	public static String dbRowAsString(DBRow para) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			try {
				sb.append(indent).append(field).append("=")
						.append(para.getValue(field.toString())).append("\n");
			} catch (Exception e) {
				javapsLog.fatal(String.format("ERROR when get field %s from DBRow",
						field),e);
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
