package com.cwc.lucene;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FloatField;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.wltea.analyzer.lucene.IKAnalyzer;








import com.cwc.app.util.PathUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

public abstract class IndexCore
{
	 static Logger log = Logger.getLogger("PLATFORM");
	 static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	
	 private String indexPath;
	 private static int ADD_INDEX_COUNT = 0;		//创建索引次数
	 private int optimizeThreshold = 500;		//索引每增加多少条，需要优化一次 
	 
	 
	 /**
	  * 子类创建索引前，必须调用设置索引文件存放路径
	  * 如果使用内存存储索引就无需设置
	  * @param indexPath
	  */
	 public void initIndexPath(String indexPath)
	 {
		 PathUtil pathUtil = (PathUtil) MvcUtil.getBeanFromContainer("pathUtil");
		 this.indexPath = pathUtil.getLucenePath()+File.separator+indexPath;
	 }

	 /**
	  * 获得索引文件目录
	  * 子类可覆盖，提供其他文件系统（比如：内存）
	  * @return
	  * @throws Exception
	  */
	 public Directory getDirectory()
	 	throws Exception
	 {
		 return( FSDirectory.open(new File(this.indexPath)) );
	 }
	 
	 private void addPsIdToDocuments(Document[] docs)
	 	throws Exception
	 {
		 LongField[] fields =null;
		 try{
			 ProductStoreResolverIFace resolver = (ProductStoreResolverIFace)MvcUtil.getBeanFromContainer("productStoreResolver");
			 List<Long> ps_ids = resolver.resolve(docs[0]);
			 if(ps_ids != null)  
			 {
				 fields = new LongField[ps_ids.size()];
				 for(int i=0; i<ps_ids.size(); i++)
				 {
					 fields[i] = new LongField("ps_id", ps_ids.get(i), Field.Store.YES);
				 }
			 }
		 }
		 catch(NoSuchBeanDefinitionException nsbde){
			 log.error("productStoreResolver not exists!");
		 }
		 
		 if(fields != null && fields.length > 0) {
			 for(Document doc: docs){
				 for(LongField lf: fields){
					 doc.add(lf);
				 }
			 }
		 }
	 }
	 
	 /**
	  * 创建索引，支持批量创建
	  * 不适合大批量创建，容易造成内存溢出
	  * @param analyzer
	  * @param docs
	  * @throws Exception
	  */
	 protected synchronized void addIndex(Analyzer analyzer,Document docs[])
	 	throws Exception
	 {
		 IndexWriter writer = null;
		 Directory dir = null;
		 
		 addPsIdToDocuments(docs);
		 
		 
		 try
		 {
			 dir = this.getDirectory();   
			 
		     IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40, analyzer); 
			 
		     writer = new IndexWriter(dir,iwc);//(dir,analyzer, false,IndexWriter.MaxFieldLength.UNLIMITED);
			 
			 //把所有字段加入所有
			 for (int i=0; i<docs.length; i++)
			 {
				 writer.addDocument(docs[i]);
				 ADD_INDEX_COUNT++;	//统计创建索引数
			 }
			 
			 /*******Bugfix :
			 Frank：原这段代码逻辑有问题！如果一次调用时，所增加的索引数目虽然大于optimizeThreshold(500)，但并不是此值的整数倍，则不会进行优化！
			        最坏情况－－若每次调用后，ADD_INDEX_COUNT恰好都不是optimizeThreshold的整数倍，则索引优化永远不会执行！
			 if (ADD_INDEX_COUNT%optimizeThreshold==0)
			 {
				 writer.optimize();
				 ADD_INDEX_COUNT = 0;
				 log.info(this.getClass().getName()+" optimize index.");
			 }
			 ****************/
			 if (ADD_INDEX_COUNT >= optimizeThreshold)  //改为只要ADD_INDEX_COUNT计数大于等于阀值，就进行优化
			 {
//				 writer.optimize();
				 ADD_INDEX_COUNT = 0;
				 log.info(this.getClass().getName()+" optimize index.");
			 }
			 
		 } 
		 catch (FileNotFoundException e)	//	第一次创建索引没有索引文件，会报错
		 {
//			 writer = new IndexWriter(dir,analyzer, true,IndexWriter.MaxFieldLength.UNLIMITED);
			 
			 IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, analyzer); 
			 
		     writer = new IndexWriter(dir,iwc);
			 for (int i=0; i<docs.length; i++)
			 {
				 writer.addDocument(docs[i]);
			 }
		 }
		 catch (Exception e)
		 {
			 e.printStackTrace();
			 throw new SystemException(e,"IndexCore.addIndex",log);
		 }
		 finally
		 {
			 writer.close();
		 }
	 }

	 /**
	  * 修改所有
	  * 修改的DOC字段必须是添加时候的所有字段
	  * @param analyzer
	  * @param field
	  * @param val
	  * @param doc
	  * @throws Exception
	  */
	 protected synchronized void updateIndex(Analyzer analyzer,String field,String val,Document doc)
	 	throws Exception
	 {
		IndexWriter writer = null;
		
		addPsIdToDocuments(new Document[] { doc });
		 
		try
		{
			Directory dir = this.getDirectory();   
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40, analyzer);
			
			try
			{
//				writer = new IndexWriter(dir,analyzer,false,IndexWriter.MaxFieldLength.UNLIMITED);
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe){ //容错，更新时按需创建，Frank
				log.warn("Empty index, re-init "+dir);
//				writer = new IndexWriter(dir,analyzer,true,IndexWriter.MaxFieldLength.UNLIMITED);
				writer = new IndexWriter(dir,iwc);
			}
			
			writer.updateDocument(new Term(field, val), doc);
			
			if ( ++ADD_INDEX_COUNT >= optimizeThreshold)  //只要ADD_INDEX_COUNT计数大于等于阀值，就进行优化
			 {
//				 writer.optimize();
				 ADD_INDEX_COUNT = 0;
				 log.info(this.getClass().getName()+" optimize index.");
			 }
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null) writer.close();
		}
	 }
	 
	 /**
		 * @author Frank
		 * 更新/增加索引，仅仅创建id和merge_field，本方法可一次添加多条记录，
		 * 注意：因此方法为synchronized，为防止锁定indexMgr对象太长时间，不要传入太大的数组！
		 * @param idKey  在索引库中添加文档的主键字段的名称
		 * @param mergeKey 在索引库中添加文档的全文检索字段的名称
		 * @param ids 要添加的文档数组的id，ids每个条目与fields每个条目按顺序一一对应，构成一组索引文档
		 * @param fields 要添加的文档数组的全文检索字段（应是已合并好的）
		 * @param optThreadhold 自定义的优化阀值，如果为0，则使用此类的optimizeThreshold值
		 * @throws Exception
		 */
		public synchronized void updateIndex(String idKey, String mergeKey, String[] ids, String[] fields, int optThreshold) 
				throws Exception{
			IndexWriter writer = null;
			//批量创建索引分词器
			Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);//new IKAnalyzer();new StopAnalyzer(Version.LUCENE_30);//new SimpleAnalyzer();//new WhitespaceAnalyzer(); 
			
			if(optThreshold<=0) {
				optThreshold = this.optimizeThreshold;
			}
			
			try
			{
				Directory dir = this.getDirectory();   
				IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40,analyzer);
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
				iwc.setRAMBufferSizeMB(256);
				iwc.setMaxThreadStates(50);
				iwc.setMaxBufferedDocs(1000);
				try
				{
//					writer = new IndexWriter(dir,analyzer,false,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				catch(java.io.FileNotFoundException fe){ //容错，更新时按需创建，Frank
					log.warn("Empty index, re-init "+dir);
//					writer = new IndexWriter(dir,analyzer,true,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				
				for(int i=0; i<ids.length; i++){
					Document doc = new Document();
					String docId = ids[i];
					doc.add(new StringField("index", "1", Store.NO));
					doc.add(new TextField(idKey,docId, Store.YES));
					doc.add(new TextField(mergeKey,fields[i].toLowerCase(),Store.YES));

					writer.updateDocument(new Term(idKey,docId ), doc);
//					javapsLog.info(idKey+":"+docId);
					ADD_INDEX_COUNT++;
				}//for
				
				if ( ADD_INDEX_COUNT >= optThreshold)  //只要ADD_INDEX_COUNT计数大于等于阀值，就进行优化
				 {
//					 writer.optimize();
					 ADD_INDEX_COUNT = 0;
				 }
			}
			catch (CorruptIndexException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (LockObtainFailedException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (IOException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			}   
			finally
			{
				if(writer!=null) writer.close();
			}
		}
	 
	 
	/**
	 * @author 詹洁
	 * 更新/增加索引，创建这
	 * 注意：因此方法为synchronized，为防止锁定indexMgr对象太长时间，不要传入太大的数组！
	 * @param idKey  在索引库中添加文档的主键字段的名称
	 * @param mergeKey 在索引库中添加文档的全文检索字段的名称
	 * @param ids 要添加的文档数组的id，ids每个条目与fields每个条目按顺序一一对应，构成一组索引文档
	 * @param values 要添加的文档数组的全文检索字段（应是已合并好的）
	 * @param fields 要添加的文档数组字段，不包含全文检索字段
	 * @param isMergeKey 是否只生成全文检索字段
	 * @param rows	要条件的文档数组字段的值
	 * @throws Exception
	 */
	public synchronized void updateIndex(String idKey, String mergeKey, String[] mergeValues,String[] fields,DBRow[] rows,boolean isMergeKey,IndexWriter writer) 
		throws Exception
	{
			for(int i=0; i<rows.length; i++)
			{
				Document doc = new Document();
				doc.add(new StringField("index", "1",Store.NO));
				long id = rows[i].get(idKey,0l);
				doc.add(new TextField(idKey,String.valueOf(id), Store.YES));
				if (isMergeKey)//是否只生成merger字段
				{
					doc.add(new TextField(mergeKey, mergeValues[i].toLowerCase(),Store.YES));
//					doc.add(new TextField(mergeKey, mergeValues[i],Store.YES));//不转成小写
				}
				else
				{						
					for (String field : fields) 
					{
						doc.add(new TextField(field,rows[i].getString(field),Store.YES));
					}
					
					doc.add(new TextField(mergeKey,mergeValues[i].toLowerCase(),Store.YES));
//					doc.add(new TextField(mergeKey,mergeValues[i],Store.YES));//不转成小写
					
				}
				
				writer.updateDocument(new Term(idKey,String.valueOf(id)), doc);
//				javapsLog.info(idKey+":"+id+"---"+mergeKey+":"+mergeValues[i]);
				ADD_INDEX_COUNT++;
			}//for
	}
	
	/**
	 * @author 詹洁
	 * 更新/增加索引，创建这
	 * 注意：因此方法为synchronized，为防止锁定indexMgr对象太长时间，不要传入太大的数组！
	 * @param idKey  在索引库中添加文档的主键字段的名称
	 * @param mergeKey 在索引库中添加文档的全文检索字段的名称
	 * @param ids 要添加的文档数组的id，ids每个条目与fields每个条目按顺序一一对应，构成一组索引文档
	 * @param values 要添加的文档数组的全文检索字段（应是已合并好的）
	 * @param fields 要添加的文档数组字段，不包含全文检索字段
	 * @param isMergeKey 是否只生成全文检索字段
	 * @param rows	要条件的文档数组字段的值
	 * @throws Exception
	 */
	public synchronized void updateIndexToUpperCase(String idKey, String mergeKey, String[] mergeValues,String[] fields,DBRow[] rows,boolean isMergeKey,IndexWriter writer) 
		throws Exception
	{
			for(int i=0; i<rows.length; i++)
			{
				Document doc = new Document();
				doc.add(new StringField("index", "1",Store.NO));
				long id = rows[i].get(idKey,0l);
				doc.add(new TextField(idKey,String.valueOf(id), Store.YES));
				if (isMergeKey)//是否只生成merger字段
				{
					doc.add(new TextField(mergeKey, mergeValues[i].toUpperCase(),Store.YES));
//					doc.add(new TextField(mergeKey, mergeValues[i],Store.YES));//不转成小写
				}
				else
				{						
					for (String field : fields) 
					{
						doc.add(new TextField(field,rows[i].getString(field),Store.YES));
					}
						
					doc.add(new TextField(mergeKey,mergeValues[i].toUpperCase(),Store.YES));
//					doc.add(new TextField(mergeKey,mergeValues[i],Store.YES));//不转成小写
					
				}
					
				writer.updateDocument(new Term(idKey,String.valueOf(id)), doc);
//				javapsLog.info(idKey+":"+id+"---"+mergeKey+":"+mergeValues[i]);
				ADD_INDEX_COUNT++;
			}//for
	}	
	
	public synchronized void updateIndexNoToLowerCase(String idKey, String mergeKey, String[] mergeValues,String[] fields,DBRow[] rows,boolean isMergeKey,IndexWriter writer) 
		throws Exception
	{
			for(int i=0; i<rows.length; i++)
			{
				Document doc = new Document();
				doc.add(new StringField("index", "1",Store.NO));
				String id = rows[i].getString(idKey);
				doc.add(new TextField(idKey,id, Store.YES));
				if (isMergeKey)//是否只生成merger字段
				{
					doc.add(new TextField(mergeKey, mergeValues[i],Store.YES));
				}
				else
				{						
					for (String field : fields) 
					{
						doc.add(new TextField(field,rows[i].getString(field),Store.YES));
					}
					
					doc.add(new TextField(mergeKey,mergeValues[i],Store.YES));
					
				}
				
				writer.updateDocument(new Term(idKey,String.valueOf(id)), doc);
//				javapsLog.info(idKey+":"+id+"---"+mergeKey+":"+mergeValues[i]+": "+fields[fields.length-1]);
				ADD_INDEX_COUNT++;
			}//for
	}
		
	 
	 /**
	  * 删除索引
	  * @param analyzer
	  * @param field
	  * @param val
	  * @throws Exception
	  */
	 protected synchronized void deleteIndex(Analyzer analyzer,String field,String val)
	 	throws Exception
	 {
		IndexWriter writer = null;
		IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40, analyzer);
		try
		{
			Directory dir = this.getDirectory();   
//			writer = new IndexWriter(dir,analyzer,IndexWriter.MaxFieldLength.UNLIMITED);
			writer = new IndexWriter(dir,iwc);
			Term term = new Term(field,val);
			writer.deleteDocuments(term);
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.deleteIndex",log);
		}
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.deleteIndex",log);
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.deleteIndex",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.deleteIndex",log);
		}  
		finally
		{
			writer.close();
		}
	 }
	 
	 /**
	  * 过滤掉特殊关键词，提供给子类使用
	  * 便于维护管理
	  * @param key
	  * @return
	  * @throws Exception
	  */
	 protected String filterWords(String key)
	 	throws Exception
	 {
		String tmpKey = key.trim().toLowerCase();
		
		//将@和.替换为空格 因为这个字符在邮件地址中有分隔意义
		tmpKey = tmpKey.replaceAll("\\.|@", " ");

		return tmpKey; 
	 }
	 
	 protected String processQueryTerms(String key) throws Exception{
			String tmpKey = filterWords(key);
			
			//先获取OR的组，即用/分隔的组
			List<List<String>> orGroups = new ArrayList<List<String>>();
			String[] splited = tmpKey.split("/+");
			for(int i=0; i<splited.length;i++){
				String s = splited[i].trim();
				List<String> andGroups = Arrays.asList(s.split("\\s+")); //OR组内获取AND组，即用空格分隔的
				if(!andGroups.isEmpty()) {
					orGroups.add(andGroups);
				}
			}
			
			StringBuilder sb=new StringBuilder();
			for(int i=0; i<orGroups.size(); i++){
				List<String> andGroups=orGroups.get(i);
				if(andGroups==null) continue;
				if(andGroups.size()>1) sb.append("(");
				for(int j=0; j<andGroups.size();j++){
					sb.append(andGroups.get(j));
					if(j<andGroups.size()-1) sb.append(" && ");
				}
				if(andGroups.size()>1) sb.append(")");
				if(i<orGroups.size()-1) sb.append(" || ");
			}
			

			return sb.toString(); 
	 }
	 
	 /**
	  * 把搜索结果分页封装成Document数组
	  * @param searcher
	  * @param hits
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 private Document[] packetDocumentsFromSearch(IndexSearcher searcher,TopDocs hits,PageCtrl pc)
	 	throws Exception
	 {
		try
		{
			ArrayList<Document> docAl = new ArrayList<Document>();
			
			if (pc==null)
			 {
				 for (int i = 0; i < hits.scoreDocs.length; i++) 
				 {   
					 ScoreDoc sDoc = hits.scoreDocs[i];   
					 float sorce = sDoc.score;
					 Document doc = searcher.doc(sDoc.doc);
					 doc.add(new FloatField("sorce",sorce, Store.YES));
					 docAl.add(doc);
				 }
			 }
			 else
			 {
			        int pageSize = pc.getPageSize();//每页显示的记录数
			        int pageNo = pc.getPageNo();//当前页码
			        int start = ( pageNo - 1) * pageSize;
			        int end = pageNo * pageSize;

					for (int i = start; i<end&&i<hits.scoreDocs.length; i++) 
					{
						 ScoreDoc sDoc = hits.scoreDocs[i];   
						 float sorce = sDoc.score;
						 Document doc = searcher.doc(sDoc.doc); 
						 doc.add(new FloatField("sorce",sorce, Store.YES));
						 docAl.add(doc);
					}
					
			        int pageCount = ((hits.scoreDocs.length + pageSize) - 1) / pageSize;//总页数
			        if( pageCount < pageNo)
			        {
			            pageNo = pageCount;
			        }
			        pc.setPageNo(pageNo);
			        pc.setPageSize(pageSize);
			        pc.setPageCount(pageCount);
			        pc.setAllCount(hits.scoreDocs.length);//记录总数
			 }
			
			return((Document[])docAl.toArray(new Document[0]));
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.packetDocumentsFromSearch",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.packetDocumentsFromSearch",log);
		}
	 }
	 
	 /**
	  * 普通分页查询搜索
	  * @param query
	  * @param maxResultCount
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 protected Document[] getDocumentsBySearch(Query query,int page_count,Sort sort,PageCtrl pc)
	 	throws FileNotFoundException,Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{
			 reader =  DirectoryReader.open(this.getDirectory());
			 
			 searcher = new IndexSearcher(reader);
			// searcher.setSimilarity(new IKSimilarity());
			 TopDocs hits = searcher.search (query,page_count*pc.getPageSize(),sort); //(query, null, maxResultCount, sort);
			 return(this.packetDocumentsFromSearch(searcher,hits,pc));
		} 
		catch (FileNotFoundException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getDocumentsBySearch",log);
		}  
		finally
		{
//			if (searcher!=null)
//			{
//				searcher.close();				
//			}
			
			if(reader!=null)
			{
				reader.close();
			}
		}
	 }
	 
	 /**
	  * 默认相关度排序
	  * @param query
	  * @param maxResultCount
	  * @param pc
	  * @return
	  * @throws FileNotFoundException
	  * @throws Exception
	  */
	 protected Document[] getDocumentsBySearch(Query query,int page_count,PageCtrl pc)
	 	throws FileNotFoundException,Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{
			 reader =  DirectoryReader.open(this.getDirectory());
			 
			 searcher = new IndexSearcher(reader);
			// searcher.setSimilarity(new IKSimilarity());
			 TopDocs hits = searcher.search (query, page_count*pc.getPageSize()); //(query, null, maxResultCount, sort);
			 return(this.packetDocumentsFromSearch(searcher, hits, pc));
		} 
		catch (FileNotFoundException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getDocumentsBySearch",log);
		}  
		finally
		{	
			if(reader!=null)
			{
				reader.close();
			}
		}
	 }
 
	 /**
	  * 带过滤器搜索
	  * @param query
	  * @param filter
	  * @param maxResultCount
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 protected Document[] getDocumentsBySearch(Query query,Filter filter,int maxResultCount,PageCtrl pc)
	 	throws FileNotFoundException,Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{	
			reader =  DirectoryReader.open(this.getDirectory());
//			 searcher = new IndexSearcher(this.getDirectory());
			// searcher.setSimilarity(new IKSimilarity());
			searcher = new IndexSearcher(reader);
			 TopDocs hits = searcher.search(query,filter,maxResultCount);
					 
			 return(this.packetDocumentsFromSearch(searcher, hits, pc));
		} 
		catch (FileNotFoundException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getDocumentsBySearch",log);
		}  
		finally
		{
//			searcher.close();
			reader.close();
		}
	 }	 
	 
	 /**
	  *         private List<Filter> filterList;
        public ezfilter(){
            filterList = new ArrayList<Filter>();
        }
        public void addFilter(String Field,String Value){
            Term term=new Term(Field,Value);//添加term
            QueryWrapperFilter filter=new QueryWrapperFilter(new TermQuery(term));//添加过滤器
            filterList.add(filter);//加入List，可以增加多個过滤
        }
        public Query getFilterQuery(Query query){
            for(int i=0;i<filterList.size();i++){
                //取出多個过滤器，在结果中再次定位结果
                query = new FilteredQuery(query, filterList.get(i));
            }
            return query;
			        }    
			}
			
			在查询时，调用方式如下：
			
			ezfilter filter = new ezfilter();
			filter.addFilter("id","1000");//过滤id=1000
			filter.addFilter("type","school");//过滤type=school
			.....
			query=filter.getFilterQuery(query);//结果过滤
			hits = searcher.search(query);
			
			
			以上代码只是简化说明，希望有碰到的朋友可以参考。

	  */
	 
	 
	 
	 
	 /**
	  * 带排序和过滤器搜索
	  * @param query
	  * @param filter
	  * @param maxResultCount
	  * @param sort
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 protected Document[] getDocumentsBySearch(Query query,Filter filter,int maxResultCount,Sort sort,PageCtrl pc)
	 	throws FileNotFoundException,Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{
			reader =  DirectoryReader.open(this.getDirectory());
//			 searcher = new IndexSearcher(this.getDirectory());
			searcher = new IndexSearcher(reader);
			// searcher.setSimilarity(new IKSimilarity());
			 TopDocs hits = searcher.search(query, filter,maxResultCount,sort);
				 
			 return(this.packetDocumentsFromSearch(searcher, hits, pc));
		} 
		catch (FileNotFoundException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getDocumentsBySearch",log);
		}  
		finally
		{
//			searcher.close();
			reader.close();
		}
	 }	 

	 /**
	  * 专门提供给大数据量从数据库转成索引
	  * @param rs
	  * @throws Exception
	  */
//	 protected synchronized void rebuildIndexFromDataBase(Analyzer analyzer,ResultSet rs)
//	 	throws Exception
//	 {
//		 try
//		 {
//			 long st = System.currentTimeMillis();	    
//			 
//			 Directory dir = this.getDirectory();   
//			 IndexWriter writer = new IndexWriter(dir,analyzer, true,IndexWriter.MaxFieldLength.UNLIMITED);
//			 
//			 //首先创建第一个索引
//			 if (rs != null && rs.next())
//			 {
//				 writer.addDocument( getDataBaseDocument(rs) );
//			 }
//			 writer.close();
//			 
//			 //追加后面的索引
//			 writer = new IndexWriter(dir,analyzer, false,IndexWriter.MaxFieldLength.UNLIMITED);
//			 while(rs != null && rs.next())
//			 {
//				 writer.addDocument( getDataBaseDocument(rs) );
//				 //system.out.print(".");
//			 }
//			 writer.optimize();
//			 writer.close();
//			 
//			 long en = System.currentTimeMillis();
//			 log.info(this.getClass().getName()+" rebuildIndexFromDataBase cost sec:"+(en-st));
//		 } 
//		 catch (Exception e)
//		 {
//			 throw new SystemException(e,"IndexCore.rebuildIndexFromDataBase",log);
//		 }
//	 }
	 
	 
	 protected Document[] getDocumentsBySearch(Query query,int page_count)
			 	throws FileNotFoundException,Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{
			 reader =  DirectoryReader.open(this.getDirectory());
			 
			 searcher = new IndexSearcher(reader);
			 TopDocs hits = searcher.search (query, page_count);
			 return(this.packetDocumentsFromSearch(searcher, hits, null));
		} 
		catch (FileNotFoundException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getDocumentsBySearch",log);
		}  
		finally
		{	
			if(reader!=null)
			{
				reader.close();
			}
		}
	 }
	 
	 protected void getAllIndex(String fields[])
	 	throws Exception
	 {
		IndexSearcher searcher = null;
		IndexReader reader = null;
		try
		{
//			 searcher = new IndexSearcher(this.getDirectory());
			 reader =  DirectoryReader.open(this.getDirectory());
			 searcher = new IndexSearcher(reader);
			 Term t = new Term("index", "1");
			 Query query = new TermQuery(t);
			 TopDocs hits = searcher.search(query, 1000);
			 
			 //system.out.println("Total Result count:"+hits.totalHits);
			 
			 for (int i = 0; i < hits.scoreDocs.length; i++) 
			 {   
				 ScoreDoc sDoc = hits.scoreDocs[i];   
				 Document doc = searcher.doc(sDoc.doc);   
				 
				 for (int j = 0; j < fields.length; j++) 
				 {
						 //system.out.print (doc.get(fields[j]));     
						 //system.out.print (" ");    
				 }
				 //system.out.print ("\n");            
			 }  
		} 
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.getAllIndex",log);
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.getAllIndex",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.getAllIndex",log);
		}
		finally
		{
//			searcher.close();
			reader.close();
		}
	 }
	 
	 /**
	  * 子类可设置优化阀值
	  * @param optimizeThreshold
	  */
	public void setOptimizeThreshold(int optimizeThreshold)
	{
		this.optimizeThreshold = optimizeThreshold;
	}
	
	public DBRow[] mergeSearchToUpperCase(String mergeKey, String q,PageCtrl pc)
			throws Exception
		{
			try
			{
				ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
				
//				javapsLog.info("[before processQueryTerms] q="+q);
				//q = processQueryTerms(q);
//				javapsLog.info("[after processQueryTerms] q="+q);
				
				
				QueryParser queryParser = new QueryParser(Version.LUCENE_40,mergeKey,new WhitespaceAnalyzer(Version.LUCENE_40));
				queryParser.setDefaultOperator(Operator.OR);
				Query query = queryParser.parse(q.toUpperCase());

				Document docs[] = getDocumentsBySearch(query,pc.getPageSize(),pc);
				
				
				for (int i=0; i<docs.length; i++)
				{
					DBRow row = new DBRow();
					for(IndexableField f: docs[i].getFields()){
						row.add(f.name(), f.stringValue());
					}//for
					
					resultAL.add(row);
				}
				
				return((DBRow[])resultAL.toArray(new DBRow[0]));
			}
			catch (FileNotFoundException e)
			{
				throw e;
			} 
			catch (ParseException e)
			{
				throw new SystemException(e,"IndexCore.mergeSearchToUpperCase("+q+")",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.mergeSearchToUpperCase("+q+")",log);
			}
		}
	
	public DBRow[] mergeSearch(String mergeKey, String q,PageCtrl pc) throws Exception {
		
		try {
			
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
				
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,mergeKey,new WhitespaceAnalyzer(Version.LUCENE_40));
			queryParser.setDefaultOperator(Operator.OR);
			Query query = queryParser.parse(q.toLowerCase());

			Document docs[] = getDocumentsBySearch(query,pc.getPageSize(),pc);
				
				for (int i=0; i<docs.length; i++)
				{
					DBRow row = new DBRow();
					for(IndexableField f: docs[i].getFields()){
						
						row.add(f.name(), f.stringValue());
					}
					
					resultAL.add(row);
				}
				
				return((DBRow[])resultAL.toArray(new DBRow[0]));
				
			} catch (FileNotFoundException e) {
				
				throw e;
			} catch (ParseException e) {
				
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			} catch (Exception e) {
				
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			}
		}
	
	public synchronized void updateIndexWithIK(String idKey, String mergeKey, String[] ids, String[] fields, int optThreshold) 
	throws Exception{
		IndexWriter writer = null;
		Analyzer analyzer = new IKAnalyzer();//new IKAnalyzer();new StopAnalyzer(Version.LUCENE_30);//new SimpleAnalyzer();//new WhitespaceAnalyzer(); 
		if(optThreshold<=0) {
			optThreshold = this.optimizeThreshold;
		}
			try
			{
				Directory dir = this.getDirectory();   
				IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40, analyzer); 
				try
				{
//					writer = new IndexWriter(dir,analyzer,false,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				catch(java.io.FileNotFoundException fe)
				{ //容错，更新时按需创建，Frank
					log.warn("Empty index, re-init "+dir);
//					writer = new IndexWriter(dir,analyzer,true,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				for(int i=0; i<ids.length; i++){
					Document doc = new Document();
					String docId = ids[i];
					doc.add(new StringField("index", "1", Store.NO));
					doc.add(new TextField(idKey,docId, Store.YES));
					doc.add(new StringField(mergeKey, fields[i], Store.YES));
					writer.updateDocument(new Term(idKey,docId ), doc);
					ADD_INDEX_COUNT++;
				}//for
				if ( ADD_INDEX_COUNT >= optThreshold)  //只要ADD_INDEX_COUNT计数大于等于阀值，就进行优化
				 {
//					 writer.optimize();
					 ADD_INDEX_COUNT = 0;
					 log.info(this.getClass().getName()+" optimize index.");
				 }
			}
			catch (CorruptIndexException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (LockObtainFailedException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (IOException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			}   
			finally
			{
				if(writer!=null) writer.close();
			}
		}
	public synchronized void updateIndexIK(String idKey, String mergeKey, String[] ids, String[] fields, int optThreshold) 
	throws Exception{
			IndexWriter writer = null;
			Analyzer analyzer = new IKAnalyzer();//new IKAnalyzer();new StopAnalyzer(Version.LUCENE_30);//new SimpleAnalyzer();//new WhitespaceAnalyzer(); 
			if(optThreshold<=0) {
				optThreshold = this.optimizeThreshold;
			}
			try
			{
				Directory dir = this.getDirectory();
				IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40, analyzer); 
				try{
//					writer = new IndexWriter(dir,analyzer,false,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				catch(java.io.FileNotFoundException fe){ //容错，更新时按需创建，Frank
					log.warn("Empty index, re-init "+dir);
//					writer = new IndexWriter(dir,analyzer,true,IndexWriter.MaxFieldLength.UNLIMITED);
					writer = new IndexWriter(dir,iwc);
				}
				for(int i=0; i<ids.length; i++)
				{
					Document doc = new Document();
					String docId = ids[i];
					doc.add(new StringField("index", "1", Store.NO));
					doc.add(new TextField(idKey,docId, Store.YES));
					doc.add(new StringField(mergeKey, fields[i].toLowerCase(), Store.YES));
					writer.updateDocument(new Term(idKey,docId), doc);
					ADD_INDEX_COUNT++;
				}//for
				if ( ADD_INDEX_COUNT >= optThreshold)  //只要ADD_INDEX_COUNT计数大于等于阀值，就进行优化
				 {
//					 writer.optimize();
					 ADD_INDEX_COUNT = 0;
					 log.info(this.getClass().getName()+" optimize index.");
				 }
			}
			catch (CorruptIndexException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (LockObtainFailedException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (IOException e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.updateIndex",log);
			}   
			finally
			{
				if(writer!=null) writer.close();
			}
		}
	public DBRow[] mergeSearchIK(String mergeKey, String q,PageCtrl pc)
		throws Exception
	{
			try
			{
				ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
//				Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
				Analyzer analyzer = new IKAnalyzer();
				QueryParser queryParser = new QueryParser(Version.LUCENE_40,mergeKey, analyzer);
				queryParser.setDefaultOperator(QueryParser.Operator.OR); //将默认断词之间的关系设为AND，默认是OR
				Query query = queryParser.parse(q);
				
				Document docs[] = getDocumentsBySearch(query,pc.getPageSize(),pc);
				SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter("","");
		    	Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));
				for (int i=0; i<docs.length; i++)
				{
					Document d = docs[i];
					TokenStream tokenStream = TokenSources.getTokenStream(d, "merge_field", analyzer);
					String v = highlighter.getBestFragment(tokenStream, d.get("merge_field"));
			     
					DBRow row = new DBRow();
					for(IndexableField f: docs[i].getFields())
					{
						if(f.name().equals("merge_field")){
							row.add(f.name(), v);
						}else{
							row.add(f.name(), f.stringValue());
						}
						
					}//for
					resultAL.add(row);
				}
				return((DBRow[])resultAL.toArray(new DBRow[0]));
			}
			catch (FileNotFoundException e)
			{
				throw e;
			} 
			catch (ParseException e)
			{
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			}
	}
	
	public static void displayTokes(TokenStream tokenStream) 
		throws IOException
	{  
        CharTermAttribute termAttribute = tokenStream.addAttribute(CharTermAttribute.class);  
        tokenStream.reset();  //此行，不能少，不然会报 java.lang.ArrayIndexOutOfBoundsException  
        while(tokenStream.incrementToken())
        {  
//            //system.out.print("["+termAttribute.toString()+"]");
//            javapsLog.info("["+termAttribute.toString()+"]");
        }
	}
	 
}
