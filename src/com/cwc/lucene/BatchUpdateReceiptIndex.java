package com.cwc.lucene;

import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.lucene.analysis.Analyzer;

import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBUtilIFace;

public class BatchUpdateReceiptIndex extends ThreadAction{
	
	private String url;
	private String sessionId;
	
	public BatchUpdateReceiptIndex(String sessionID ) 
	{	
		  this.sessionId=sessionID;
		  Properties props=new Properties();
		  InputStream in;
		  try{
			  in=getClass().getResourceAsStream("/env.properties"); 
			  props.load(in);
		  }catch(Exception e){
			   return ;
		  }
		  if(props.isEmpty()){
			  return;
		  }
		  url=props.get("receipt.index.url").toString();
	}

	@Override
	protected void perform() 
		throws Exception 
	{
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);
		
		httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
		HttpResponse response = httpClient.execute(httpget);
		StatusLine statusLine = response.getStatusLine();
		int httpStatus=statusLine.getStatusCode();
		if (httpStatus == 200) {
			
		}
		httpClient.getConnectionManager().shutdown();
	}

}
