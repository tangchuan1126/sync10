package com.cwc.lucene;

import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.db.DBRow;

public class TaskB2BOrderIndex extends Thread {
	DBRow[] rows;
	Long psid;
	public TaskB2BOrderIndex(DBRow[] rows, Long psid) {
		this.rows = rows;
		this.psid = psid;
	}
	public void run() {
		B2BOrderIndexMgr.getInstance().mageIndexs(rows, "b2bindex"+ psid);
	}
}