package com.cwc.lucene;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexableField;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.lucene.ProductStoreResolverIFace;

public class ProductStoreResolver implements ProductStoreResolverIFace {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	@Override
	public List<Long> resolve(Document doc)
	throws Exception
	{
		List<IndexableField> list = doc.getFields();
		
		ArrayList<Long> returnPSList = new ArrayList<Long>();

		for (int i = 0; i <list.size(); i++) 
		{
			String fieldName = list.get(i).name();
			
			switch (fieldName)
			{
				case "dlo_id":
					returnPSList = checkInPsid("dlo_id", doc);
					break;
					
				case "transport_id":
					returnPSList = transportPsid("transport_id", doc);
					break;
			}
		}
		
		return returnPSList;
	}
	
	private ArrayList<Long> checkInPsid(String fieldName,Document doc)
		throws Exception
	{
		long dlo_id = Long.parseLong(doc.get(fieldName));
		
		String sql = " select * from "+ConfigBean.getStringValue("door_or_location_occupancy_main")+" where dlo_id = ? ";
		
		DBRow para = new DBRow();
		para.add("dlo_id",dlo_id);
		
		DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
		
		long ps_id = row.get("ps_id",0l);
		
		ArrayList<Long> psIdList = new ArrayList<Long>();
		
		psIdList.add(ps_id);
		
		return psIdList;
	}
	
	private ArrayList<Long> transportPsid(String fieldName,Document doc)
		throws Exception
	{
		long transport_id = Long.parseLong(doc.get(fieldName));
		
		String sql = " select * from "+ConfigBean.getStringValue("transport")+" where transport_id = ? ";
		
		DBRow para = new DBRow();
		para.add("transport_id",transport_id);
		
		DBRow transport = dbUtilAutoTran.selectPreSingle(sql, para);
		long send_psid = transport.get("send_psid",0l);
		long receive_psid = transport.get("receive_psid",0l);
		
		ArrayList<Long> psidList = new ArrayList<Long>();
		psidList.add(send_psid);
		psidList.add(receive_psid);
		
		return psidList;
	}
	
	
	
	public static void main(String[] args) 
	throws Exception
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new StringField("pc_id", String.valueOf(10000), Store.YES));
		 doc.add(new StringField("p_name", "Pname", Store.YES));
		 doc.add(new StringField("p_code", "pcode", Store.YES));   
		 doc.add(new LongField("catalog_id",50000, Store.YES));   
		 doc.add(new StringField("unit_name","unit_name", Store.YES));
		 doc.add(new TextField("alive","zhanjie 123 wangyanqiu",Store.YES));
		 
		ProductStoreResolver pr = new ProductStoreResolver();
		pr.resolve(doc);
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
