package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

public class BatchUpdateServiceOrderIndex extends ThreadAction{

	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");

	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
	
	public BatchUpdateServiceOrderIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer) 
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"s_id","o_id","w_id","r_id","b_id"};
		this.fieldNames = new String[]{"sid","oid","wid","rid","bid"};
		this.documentKey = "sid";
		this.mergeKey = "merge_field";
		this.isMergeKey = true;
		this.analyzer = analyzer;
	}
	
	
	
	@Override
	protected void perform() 
		throws Exception 
	{
		String sql = "select * from service_order";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;
		
			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace serviceOrderMgrZyj = (com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyServiceOrderMgrZyj");
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					rows[i].add("s_id","S"+rows[i].getString("sid"));
					rows[i].add("o_id","O"+rows[i].getString("oid"));
					if(!"".equals(rows[i].getString("wid")) && !"0".equals(rows[i].getString("wid")))
					{
						rows[i].add("w_id", "W"+rows[i].getString("wid"));
					}
					 String rpIdStr		= "";
					 com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj = (com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyReturnProductOrderMgrZyj");
					 DBRow[] returnOrders = returnProductOrderMgrZyj.getReturnOrderBySid(rows[i].get("sid",0L));
					 if(returnOrders.length > 0)
					 {
						 rpIdStr		= "R"+returnOrders[0].get("rp_id", 0L);
						 rows[i].add("r_id", rpIdStr);
					 }
					DBRow[] billRows	= serviceOrderMgrZyj.getServiceBillOrdersBySid(rows[i].get("sid",0L));
					String billIdStr	= "";
					if(billRows.length > 0)
					{
						billIdStr		= "B"+billRows[0].get("bill_id", 0L);
						rows[i].add("b_id", billIdStr);
					}
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}
					
				indexMgr.updateIndexNoToLowerCase(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}
	protected Method updateMethod() {
		Method[] meths = indexMgr.getClass().getMethods();
		for (Method m : meths) {
			if (m.getName().equals("updateIndex"))
				return m;
		}
		return null;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}


