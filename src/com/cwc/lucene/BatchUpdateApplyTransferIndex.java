package com.cwc.lucene;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class BatchUpdateApplyTransferIndex extends ThreadAction {
	static Logger javapsLog = Logger.getLogger("JAVAPS_INDEX");
	static Logger log = Logger.getLogger("PLATFORM");

	private DBUtilIFace dbUtil;
	private IndexCore indexMgr;
	private String documentKey;
	private String mergeKey;
	private boolean isMergeKey;
	private String[] fieldNames;
	private String[] mergeField;
	private int pageSize=1000;
	private Analyzer analyzer;
 
	public BatchUpdateApplyTransferIndex(DBUtilIFace dbUtil, IndexCore indexMgr,Analyzer analyzer)
		throws Exception
	{
		this.dbUtil = dbUtil;
		this.indexMgr = indexMgr;
		this.mergeField = new String[]{"transfer","apply","association","receiver","receiver_information","receiver_account"};
		this.fieldNames = new String[]{"transfer","apply","association","receiver","receiver_information","receiver_account"};
		this.documentKey = "transfer_id";
		this.mergeKey = "merge_field";
		this.isMergeKey = true;
		this.analyzer = analyzer;
	}

	@Override
	protected void perform() 
		throws Exception 
	{
		String sql = "select * from apply_transfer";

		IndexWriter writer = null;
		try 
		{
			int pc = 0;
			
			int indexCount=0;
		
			DBRow[] rows = null;
			String[] mergeValues = new String[pageSize];
			
			DBRow applyMoney = null;
			
			Directory dir = indexMgr.getDirectory();
			IndexWriterConfig iwc=new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe)
			{ 
				//容错，更新时按需创建，Frank
				writer = new IndexWriter(dir,iwc);
			}
			
			while (pc == 0 || rows.length==pageSize) 
			{
				rows = dbUtil.selectMutliple(String.format("%s limit %d,%d",sql,pc,pageSize));
					
				for(int i=0; i<rows.length; i++)
				{
					
					rows[i].add("transfer","W"+rows[i].get("transfer_id",0l));
					rows[i].add("apply","|F"+rows[i].get("apply_money_id",0l)+"|");
					
					applyMoney = dbUtil.selectSingle(String.format("select * from apply_money where apply_id = %d",rows[i].get("apply_money_id",0l)));
					
					rows[i].add("association",this.getAssociation(applyMoney.get("association_id",0l),applyMoney.get("association_type_id",0)));
					
					rows[i].add("receiver_information",this.getPaymentInformation(rows[i].getString("receiver_information")));
					
					//merge fields
					StringBuilder sb=new StringBuilder();
					for(String f:mergeField)
					{
						sb.append(rows[i].getString(f).trim()).append(" ");
					}	
					mergeValues[i] = sb.toString().trim();
				}//for
					
				indexMgr.updateIndexNoToLowerCase(documentKey,mergeKey,mergeValues,fieldNames,rows,isMergeKey,writer);
				
				indexCount += rows.length;
								
				pc += pageSize;
			}// for
								
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"IndexCore.updateIndex",log);
		}   
		finally
		{
			if(writer!=null)
			{
				writer.close();
			}
				
		}
	}
	
	/**
	 * 获得关联
	 * @param association_id
	 * @param association_type_id
	 * @return
	 * @throws Exception
	 */
	private String getAssociation(long association_id,int association_type_id)
		throws Exception
	{
		String association = "";
		if(association_id !=0)
		{
			switch (association_type_id) 
			{
				case 2:
					association = "固定资产"+association_id;
					break;
				
				case 4:
					association = "P"+association_id;
					break;
				
				case 6:
					association = "T"+association_id;
					break;
				
				default:
					association = ""+association_id;
					break;
			}
		}
		return association;
	}
	
	//收款信息
	private String getPaymentInformation(String payment_information)
		throws Exception
	{
		StringBuffer paymentInformation = new StringBuffer("");
		
		 String[] arrayList = payment_information.split("\r\n");
		 if(arrayList != null && arrayList.length > 0)
		 {
			 for(int index = 0 , count = arrayList.length ; index < count ; index++ )
			 {
				String temp = arrayList[index]; 
     	 		temp = StringUtil.full2HalfChange(temp);
     	 		
     	 		int splitIndex = temp.indexOf(":");
     	 		
     	 		if(splitIndex != -1)
     	 		{
     	 			paymentInformation.append(" ").append(temp.substring(splitIndex+1).replaceAll(" ","")) ;
     	 		}
     	 		else
     	 		{
     	 			paymentInformation.append(" ").append(temp);
     	 		}
     	 			
				  
			 }
		 }
		 else
		 {
			 if(payment_information.length() > 0)
			 {
				 paymentInformation.append(" ").append(payment_information);
			 }
		 }
		 
		 return (paymentInformation.toString());
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
