package com.cwc.json;

import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cwc.db.DBRow;
import com.cwc.db.DBObjectType;

public class JsonObject
{
	private String ouString = null;

	/**
	 * 根据接收DBRow是单个对象还是数组，自动转换为对应JSON数据
	 * @param obj		DBRow或DBRow[]
	 * @throws Exception
	 */
	public JsonObject(Object obj)
		throws Exception
	{
		if (obj==null)
		{
			ouString = "[]";
		}
		else
		{
			//先判断是数组还是普通对象
			if (obj.getClass().isArray())
			{
				if ( obj instanceof DBRow[] )
				{
					DBRow rows[] = (DBRow[])obj;
					JSONArray jsa = new JSONArray();//json数组
					
					for(int i=0;i<rows.length;i++)
					{   
						ArrayList fieldNames = rows[i].getFieldNames();
						
						JSONObject jso = new JSONObject();//json数组中的元素
						for (int j=0; j<fieldNames.size(); j++)
						{
							String key = fieldNames.get(j).toString().toLowerCase();
							jso.put(key, rows[i].getString(key));   
						}
						jsa.add(i,jso);
//						jsa.put(i, jso);   
					}
					
					ouString = jsa.toString();
				}
				else
				{
					throw new Exception("new JsonObject error:Object[] must be DBRow[]");
				}
			}
			else
			{
				if ( obj instanceof DBRow )
				{
					DBRow row = (DBRow)obj;
					
					ArrayList fieldNames = row.getFieldNames();
					JSONObject jso = new JSONObject();
					for (int j=0; j<fieldNames.size(); j++)
					{
						String key = fieldNames.get(j).toString().toLowerCase();

						//如果字段值是DBROW[]，则需要再封装一次json
						if ( isDBRowArrayType(row.get(fieldNames.get(j).toString(), new Object())) )
						{
							DBRow subRows[] = (DBRow[])row.get(fieldNames.get(j).toString(), new Object());
							JSONArray subJsa = new JSONArray();
							
							for (int k=0; k<subRows.length;k++)
							{
								ArrayList subRowsFieldNames = subRows[k].getFieldNames();
								//获得字段名称
								JSONObject subJso = new JSONObject();
								for (int kk=0; kk<subRowsFieldNames.size();kk++)
								{
									String subKey = subRowsFieldNames.get(kk).toString().toLowerCase();
									subJso.put(subKey, subRows[k].getString(subKey));
								}
								
								subJsa.add(k,subJso);
//								subJsa.put(k, subJso);   
							}
							
							jso.put(key, subJsa);
						}
						else
						{
							jso.put(key, row.getString(key));
						}
					}
					
					ouString = jso.toString();
				}
				else
				{
					throw new Exception("new JsonObject error:Object must be DBRow");
				}
			}
		}
	}
	
	public String toString()
	{
		return(ouString);
	}
	
	/**
	 * 判断是否为dbrow[]类型
	 * @param obj
	 * @return
	 */
	public boolean isDBRowArrayType(Object obj)
	{
		if ( obj.getClass().isArray() )
		{
			if ( obj instanceof DBRow[] )
			{
				return(true);
			}
		}
		
		return(false);
	}

	/**
	 * 判断是否为dbrow类型
	 * @param obj
	 * @return
	 */
	private boolean isDBRowType(Object obj)
	{
		return( obj instanceof DBRow );
	}
	
	public static void main(String args[]) throws Exception
	{
		DBObjectType.ll1l11ll1l1l1ll11ll1111ll11l1l11l1l11111l1l111l1l1l1 = 1;
		
		ArrayList dataList = new ArrayList();
		
		for (int i=0; i<10; i++)
		{
			DBRow tmp = new DBRow();
			tmp.add("id", i);
			tmp.add("name", "name"+i);
			tmp.add("time", System.currentTimeMillis());
			dataList.add(tmp);
		}

		DBRow data = new DBRow();
		data.add("page", "2");
		data.add("total", "2");
		data.add("rows", (DBRow[])dataList.toArray(new DBRow[0]));
		data.add("records", "13");
		
		//system.out.println( new JsonObject(data).toString() );
		
		
	}
	
	
}






















