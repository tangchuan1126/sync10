package com.cwc.json;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import net.sf.json.util.JSONUtils;

/*
 * ���� java --> json ʱ���ڸ�ʽ��ת��
 * 
 *		JsonConfig jconfig = new JsonConfig(); 
 *		jconfig.registerJsonValueProcessor(java.util.Date.class,
 *			new DateJsonValueProcessor());
 *
 * json --> java ʱ���ڸ�ʽ��ת������
 * 
 * 		String[] dateFormats = new String[] {"yyyy-MM-dd hh:mm:ss"};   
 *  	JSONUtils.getMorpherRegistry().registerMorpher(new DateMorpher(dateFormats));
 */

public class DateJsonValueProcessor implements JsonValueProcessor {
	
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
	private SimpleDateFormat sdf;
	
	public DateJsonValueProcessor() {   
		JSONUtils.getMorpherRegistry().registerMorpher(
				new DateMorpher(new String[] {DEFAULT_DATE_PATTERN}));
		sdf = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
	}
	
	public DateJsonValueProcessor(String datePattern) {
		try {
			JSONUtils.getMorpherRegistry().registerMorpher(
					new DateMorpher(new String[] {datePattern}));
			sdf = new SimpleDateFormat(datePattern);
		} catch(RuntimeException e) {
			sdf = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
		}
	}
	
	public Object processArrayValue(Object value, JsonConfig cfg) {
		return process(value);
	}

	public Object processObjectValue(String key, Object value, JsonConfig cfg) {
		return process(value);
	}
	
	private Object process(Object value) {
		if(value == null) {
			return "";
		}
		if(value instanceof Date) {
			return sdf.format(value);
		}
		return value.toString();
	}
	
	

}
