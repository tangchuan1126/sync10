package com.cwc.json;

import java.util.ArrayList;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

public class JsonUtils {

	public static JsonConfig getJsonConfig() {	
		JsonConfig cfg = new JsonConfig();
		cfg.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		cfg.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor());
		return cfg;
	}
	
	public static JsonConfig getJsonConfig(String datePattern) {
		JsonConfig cfg = new JsonConfig();
		cfg.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		cfg.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor(datePattern));
		return cfg;
	}
	
	public static Object toBean(JSONObject jsonObject, Class<? extends Object> rootClass) {
		JsonConfig cfg = getJsonConfig();
		cfg.setRootClass(rootClass);
		return JSONObject.toBean(jsonObject,cfg);
	}
	
	public static Object toBean(JSONObject jsonObject, Class<? extends Object> rootClass, JsonConfig cfg) {
		cfg.setRootClass(rootClass);
		return JSONObject.toBean(jsonObject,cfg);
	}
	
	public static JSONObject toJsonObject(Object bean) {
		JsonConfig cfg = getJsonConfig();
		return JSONObject.fromObject(bean, cfg);
	}
	
	public static JSONArray toJsonArray(Object list) {
		JsonConfig cfg = getJsonConfig();
		return JSONArray.fromObject(list, cfg);
	}
	
	public static JSONArray toJsonArray(Object list, JsonConfig cfg) {
		return JSONArray.fromObject(list, cfg);
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<? extends Object> toList(JSONArray jsonArray, Class<? extends Object> rootClass) {
		
		JsonConfig cfg = getJsonConfig();
		cfg.setRootClass(rootClass);
		return  (ArrayList<? extends Object>) JSONArray.toCollection(jsonArray,cfg);
	}
}
