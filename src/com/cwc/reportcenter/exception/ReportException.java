package com.cwc.reportcenter.exception;

/**
 * 报表异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ReportException extends Exception 
{
	public ReportException() 
	{
		super();
	}
	
	public ReportException(String inMessage)
	{
		super(inMessage);
	}
}
