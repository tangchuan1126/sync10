package com.cwc.reportcenter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.reportcenter.exception.ReportException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class ReportCenter
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private Map<String, String> params;   
	private String reporViewTemplatetFilePath;   //报表模板文件路径
	private JRDataSource dataSource;    //报表数据源
	
	public static String REPORT_VIEW_ROOTPATH = Environment.getHome()+ "/WEB-INF/report_center_viewt/";
	
	public ReportCenter(String reporViewTemplatetFilePath, Map<String, String> params,DBRow dataRows[])
		throws ReportException
	{
		if (reporViewTemplatetFilePath==null)
		{
			throw new ReportException("reporViewTemplatetFilePath can't be NULL!");
		}
		else if (reporViewTemplatetFilePath.equals(""))
		{
			throw new ReportException("reporViewTemplatetFilePath can't be EMPTY!");
		}
		else if (!reporViewTemplatetFilePath.endsWith(".jasper"))
		{
			throw new ReportException("reporViewTemplatet request JASPER!");
		}
		else if (params==null)
		{
			throw new ReportException("params can't be NULL!");
		}
		
		this.reporViewTemplatetFilePath = reporViewTemplatetFilePath;
		this.params = params;
		this.dataSource = new ReportDataSource(dataRows);//通过DBROW[]伪装数据源
	}

	public JasperPrint getPrinter() 
		throws Exception 
	{
		File reportFile = new File(this.reporViewTemplatetFilePath);   
		if (!reportFile.exists())
		{
			throw new ReportException(reporViewTemplatetFilePath+" is't EXIST!");			
		}
   
		try
		{   
			// Load编译好的模板   
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());   
			// 进行数据填充   
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, this.params, this.dataSource);   
		    return(jasperPrint);   
		}
		catch (JRException e) 
		{   
			throw new SystemException(e,"getPrinter",log);
		 }   
		   
	}  

	
	public static void main(String args[])
		throws Exception 
	{
		HashMap<String, String> paras = new HashMap<String, String>();
		
		ArrayList<DBRow> dataAl = new ArrayList<DBRow>();
		
		for (int i=0; i<300; i++)
		{
			DBRow row1 = new DBRow();
			row1.add("unit_name", "PAIR");
			row1.add("unit_price", "41");
			row1.add("pc_id", "1");
			row1.add("p_name", "BULB/H11-8K/75W");
			row1.add("p_code", "BULB/H11-8K/75W");
			row1.add("catalog_id", "1");
			row1.add("gross_profit", "0.45");
			row1.add("weight", "0.2");
			row1.add("union_flag", "0");
			row1.add("alive", "alive");
			dataAl.add(row1);
		}
		
		String reporViewTemplatetFilePath = "D:/ireport_template/quote.jasper";
		DBRow dataRows[] = (DBRow[])dataAl.toArray(new DBRow[0]);
		
		ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows);
		JasperPrint printer = reportCenter.getPrinter();
		
		/// 生成PDF
		byte[] bReport = JasperExportManager.exportReportToPdf(printer);
		FileOutputStream fos = new FileOutputStream("D:/ireport_template/quote_"+System.currentTimeMillis()+".pdf",true); 
		fos.write(bReport);   
		fos.flush(); 
		fos.close();   
		//**/
		
		/** 生成xls
		JRXlsExporter exporter = new JRXlsExporter();

	    ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, printer);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, oStream);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE); // 删除记录最下面的空行
		exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);// 删除多余的ColumnHeader
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);// 显示边框
		exporter.exportReport();
				   
		byte[] bytes = oStream.toByteArray();
		FileOutputStream fos = new FileOutputStream("D:/ireport_template/quote_"+System.currentTimeMillis()+".xls",true); 
		fos.write(bytes);   
		fos.flush(); 
		fos.close();  
		**/
		

	}
	
	
}

















