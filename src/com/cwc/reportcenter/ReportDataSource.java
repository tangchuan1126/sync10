package com.cwc.reportcenter;

import com.cwc.db.DBRow;
import com.cwc.reportcenter.exception.ReportException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 * 伪装一个数据源，通过DBROW[]来提供数据给报表使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ReportDataSource implements JRDataSource
{
	private int pointerPos = -1;
	private DBRow dataRows[];
	
	public ReportDataSource(DBRow dataRows[])
		throws ReportException
	{
		if (dataRows==null)
		{
			throw new ReportException("ReportDataSource.DataRows can't be NULL!");
		}
		else if (dataRows.length==0)
		{
			throw new ReportException("The length of ReportDataSource.DataRows can't be ZERO!");
		}
		
		this.dataRows = dataRows;
	}
	
	/**
	 * 报表引擎根据报表模板，扫描每一个字段
	 * @param jrField
	 * @return
	 * @throws JRException
	 */
	public Object getFieldValue(JRField jrField) 
		throws JRException
	{
		String fieldName = jrField.getName();   
		////system.out.println(fieldName);
		return(dataRows[pointerPos].getString(fieldName));
	}

	/**
	 * 报表引擎扫描记录每一行
	 * 报表引擎会先调用一次，再去扫描字段数据
	 * @return
	 * @throws JRException
	 */
	public boolean next() 
		throws JRException
	{
		pointerPos++;
		return(pointerPos<dataRows.length);
	}

}
