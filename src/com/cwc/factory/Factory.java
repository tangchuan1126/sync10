package com.cwc.factory;

import java.util.HashMap;

import org.apache.log4j.Logger;


/**
 * 类工厂
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Factory 
{
	static Logger log = Logger.getLogger("PLATFORM");
	private HashMap objCache = new HashMap();
	private static Factory factory = null;

	
	private Factory(){}
	
	public synchronized static Factory getInstance()
	{
		if ( factory==null )
		{
			factory = new Factory();
			return(factory);
		}
		else
		{
			return(factory);
		}
	}
	
	/**
	 * 通过代理创建一个类
	 * @param className
	 * @return
	 * @throws Exception
	 */
	public Object getInstanceViaProxy(String className)
		throws Exception
	{
		try
		{
			return(this.createInstance(className,true,true));
		} 
		catch (Exception e) 
		{
			log.error("Factory.getInstanceViaProxy error:"+e);
			throw new Exception("Factory.getInstanceViaProxy error:"+e);
		}
	}

	/**
	 * 不通过代理创建一个类
	 * @param className
	 * @return
	 * @throws Exception
	 */
	public Object getInstanceNonProxy(String className)
		throws Exception
	{
		try
		{
				return(this.createInstance(className,false,true));
		} 
		catch (Exception e) 
		{
			log.error("Factory.getInstanceNonProxy error:"+e);
			throw new Exception("Factory.getInstanceNonProxy error:"+e);
		}
	}

	/**
	 * 通过代理不使用缓存创建一个类
	 * @param className
	 * @return
	 * @throws Exception
	 */
	public Object getInstanceViaProxyNonCache(String className)
		throws Exception
	{
		try
		{
			return(this.createInstance(className,true,false));
		} 
		catch (Exception e) 
		{
			log.error("Factory.getInstanceNonProxy error:"+e);
			throw new Exception("Factory.getInstanceNonProxy error:"+e);
		}
	}
	
	/**
	 * 不通过代理不使用缓存创建一个类
	 * @param className
	 * @return
	 * @throws Exception
	 */
	public Object getInstanceNonProxyNonCache(String className)
		throws Exception
	{
		try
		{
			return(this.createInstance(className,false,false));
		} 
		catch (Exception e) 
		{
			log.error("Factory.getInstanceNonProxy error:"+e);
			throw new Exception("Factory.getInstanceNonProxy error:"+e);
		}
	}

	/**
	 * 创建类
	 * @param className					类名
	 * @param boundProxyflag			是否绑定代理
	 * @param cacheflag					是否使用缓存
	 * @return
	 * @throws Exception
	 */
	public Object createInstance(String className,boolean boundProxyflag,boolean cacheflag)
		throws Exception
	{
		try
		{
			Object obj = null;
			//String key = className.concat("@").concat(String.valueOf(boundProxyflag));

			//obj = objCache.get(key);   不要cache，否则在多线程下有点问题

//			if( obj==null )
//			{

				obj = (Object)Class.forName(className).newInstance();

				
				////system.out.println("==> "+className);
				
				//只有需要cache才缓冲
//				if (cacheflag)
//				{
//					objCache.put(key,obj);	
//				}
//			}
			
			//绑定AOP代理
//			if (boundProxyflag)
//			{
//				AopProxyFactory aopProxyFactory = new AopProxyFactory();
//				obj = aopProxyFactory.getAopProxy().bind(obj);
//			}

			return(obj);
		} 
		catch (Exception e) 
		{
			log.error("Factory.createInstance("+className+") error:"+e);
			throw new Exception("Factory.createInstance("+className+") error:"+e);
		}
	}

	public static void main(String args[])
		throws Exception
	{

	}
}






