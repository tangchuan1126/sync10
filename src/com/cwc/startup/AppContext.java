package com.cwc.startup;



import javax.servlet.ServletContext;

import com.cwc.util.FileUtil;

/**
 * 写日志配置文件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AppContext
{
    private static AppContext _ctx = null;

    private String appRealPath = null;

    protected AppContext()
    {}

    public static AppContext getInstance()
    {
        if (_ctx == null)
        {
            _ctx = new AppContext();
        }
        return _ctx;
    }

    public void init(ServletContext context) throws RuntimeException
    {
        try
        {
            this.appRealPath = context.getRealPath("/");
            this.appRealPath = this.appRealPath.replace('\\','/');
            writeLog4jConfig(this.appRealPath);   //创建Log4J 的配置文件
            //system.out.println("com.cwc.startup.AppContext.java initialized.");
       
        }
        catch(Exception ex)
        {
            //system.out.println("Turboshop AppContext : error in init():" + ex.toString());
            throw new RuntimeException(ex.getMessage());
        }
    }

    /**
     * 写日志配置文件
     * @param path
     * @throws Exception
     */
    public void writeLog4jConfig(String path) throws Exception
    {
        StringBuffer str = new StringBuffer("");
    
        str.append("log4j.logger.com=ERROR\n");
        str.append("log4j.additivity.com=false\n");
        str.append("log4j.logger.PLATFORM=INFO,A1,A3,A4\n");
        str.append("log4j.logger.ACTION=INFO,A2,A3,A4\n");
        str.append("log4j.logger.DB=INFO,A5,A3,A4\n");
        str.append("log4j.logger.SENDMAIL=INFO,A6,A3,A4\n");
        str.append("log4j.logger.PAGE=INFO,A7,A3,A4\n");
        str.append("log4j.logger.PAYPAL=INFO,A8\n");
        str.append("log4j.logger.JMS=INFO,A9,A4\n");
    
        str.append("log4j.appender.A4=org.apache.log4j.ConsoleAppender\n");
        str.append("log4j.appender.A4.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A4.layout.ConversionPattern=[%c] [%-5p] %m%n\n");
    
        str.append("log4j.appender.A1=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A1.File="+path+"WEB-INF/logs/platform.log\n");
        str.append("log4j.appender.A1.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A1.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A1.layout.ConversionPattern=[%d] [%-5p] %m%n\n");
    
        str.append("log4j.appender.A2=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A2.File="+path+"WEB-INF/logs/action.log\n");
        str.append("log4j.appender.A2.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A2.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A2.layout.ConversionPattern=[%d] [%-5p] %m%n\n");
    
        str.append("log4j.appender.A3=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A3.File="+path+"WEB-INF/logs/flow.log\n");
        str.append("log4j.appender.A3.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A3.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A3.layout.ConversionPattern=[%d] [%c] [%-5p] %m%n\n");
    
        str.append("log4j.appender.A5=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A5.File="+path+"WEB-INF/logs/db.log\n");
        str.append("log4j.appender.A5.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A5.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A5.layout.ConversionPattern=[%d] [%-5p] %m%n\n"); 
     
        str.append("log4j.appender.A6=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A6.File="+path+"WEB-INF/logs/sendmail.log\n");
        str.append("log4j.appender.A6.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A6.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A6.layout.ConversionPattern=[%d] [%-5p] %m%n\n");
        
        str.append("log4j.appender.A7=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A7.File="+path+"WEB-INF/logs/page.log\n");
        str.append("log4j.appender.A7.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A7.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A7.layout.ConversionPattern=[%d] [%-5p] %m%n\n");
        
        str.append("log4j.appender.A8=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A8.File="+path+"WEB-INF/logs/paypal.log\n");
        str.append("log4j.appender.A8.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A8.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A8.layout.ConversionPattern=[%d] [%-5p] %m%n\n");
        
        str.append("log4j.appender.A9=org.apache.log4j.DailyRollingFileAppender\n");
        str.append("log4j.appender.A9.File="+path+"WEB-INF/logs/jms.log\n");
        str.append("log4j.appender.A9.DatePattern='_'yyyy-MM-dd\n");
        str.append("log4j.appender.A9.layout=org.apache.log4j.PatternLayout\n");
        str.append("log4j.appender.A9.layout.ConversionPattern=[%d] [%-5p] %m%n");

        String file = path+"WEB-INF/conf/log4j.properties";
        FileUtil.delFile(file);
        FileUtil.createFile(file,"UTF-8",str.toString());
    }
    
    
    public String getRealPath()
    {
        return appRealPath;
    }
}