package com.cwc.startup;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
//import java.util.Enumeration;
//import java.io.File;

import com.cwc.startup.AppContext;

/**
 * 上下文监听器
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ContextListener implements ServletContextListener
{
    /**
     * The servlet context with which we are associated.
     */
    private ServletContext context = null;

    public void contextDestroyed(ServletContextEvent event)
    {
        log("Turboshop context destroyed.");
        this.context = null;
    }

    /**
     * 初始化上下文
     * @param event
     */
    public void contextInitialized(ServletContextEvent event)  //Tomcat启动Servlet容器，马上触发该方法
    {
        this.context = event.getServletContext();
        AppContext.getInstance().init(context); //初始化应用程序的环境参数，Log4j配置文件等
    }

    /**
     * Log a message to the servlet context application log.
     *
     * @param message Message to be logged
     */
    private void log(String message)
    {
        if (context != null)
            context.log("ContextListener: " + message);
        //else
            //system.out.println("ContextListener: " + message);
    }

}