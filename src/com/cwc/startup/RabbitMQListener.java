package com.cwc.startup;

import java.util.Set;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.cwc.app.api.CheckinQueueConsumer;
import com.cwc.app.api.CheckoutQueueConsumer;


public class RabbitMQListener implements ServletContextListener {
	
	/*public static final String SERVER_ADDRESS = "54.201.95.71";
	public static final String USER_NAME = "wise";
	public static final String PASSWORD = "rb132qa";
	public final static String ROUTE_KEY_CHECKIN = "to-linc-checkin-queue"; 
	public final static String ROUTE_KEY_CHECKOUT = "to-linc-checkout-queue"; */
	
	private CheckinQueueConsumer checkinQueueConsumer;
	private CheckoutQueueConsumer checkoutQueueConsumer;
	private Thread thdConsumerCheckout;
	private Thread thdConsumerCheckin;
	
	@SuppressWarnings("deprecation")
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("======stop listenning rq=======");
		thdConsumerCheckout.stop();
		thdConsumerCheckin.stop();
	}
	
	 private void destroySpecifyThreads() {  
	        final Set<Thread> threads = Thread.getAllStackTraces().keySet();  
	        for (Thread thread : threads) {  
	        	 synchronized (this) {  
                    try {  
                        thread.stop();  
                    } catch (Exception e) {  
                    	e.printStackTrace();  
                    }  
                }  
	        }  
	    }  
	  

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try{
			System.out.println("======start listenning rq=======");    
			checkoutQueueConsumer = new CheckoutQueueConsumer();
			thdConsumerCheckout = new Thread(checkoutQueueConsumer);
			thdConsumerCheckout.start();
			
	        checkinQueueConsumer = new CheckinQueueConsumer();
	        thdConsumerCheckin = new Thread(checkinQueueConsumer);
	        thdConsumerCheckin.start();
	        
		}catch(Exception ex){
			
		}
		
       
		// TODO Auto-generated method stub

	}

}
