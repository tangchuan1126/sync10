package com.cwc.startup;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.Signature;
import java.util.ResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.cwc.app.util.Config;
import com.cwc.asynchronized.ThreadManager;
import com.cwc.initconf.Resource;

/**
 * 系统启动SERVLET
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class StartUpInitServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    
    private static Logger log = Logger.getLogger("PLATFORM");
	
	public void init(ServletConfig config)
		throws ServletException
	{
		//system.out.println("StartUpInitServlet init");
		try 
		{
			ThreadManager.getInstance();															//启动异步线程池
			//checkLicense();																			//检查licence

			log.info("Visionari Syncing V2.0 Starting.");
			log.info("(C)2010-3010 VVME.COM all rights reserved.");
		}
		catch (Exception e) 
		{
			log.error("StartUpInitServlet.init error:" + e);
			throw new ServletException("init error:" + e);
		}
	}

	/**
	 * 初始化语言资源文件
	 * @throws Exception
	 */
	public void initResource()
		throws Exception
	{
		new Resource();
	}
	
	public void destroy()
	{
	}

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
		doGet(request,response);
	}
	

}
