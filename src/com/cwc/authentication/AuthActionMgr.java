package com.cwc.authentication;

import org.apache.log4j.Logger;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;

/**
 * 鉴权事件管理
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AuthActionMgr
{
	static Logger log = Logger.getLogger("PLATFORM");
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获得所有需要鉴权事件
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAuthAction()
		throws Exception
	{
		try
		{
			//修改：lujintao  2015/6/8  因为表新加了字段auth（用auth表示是否需要权限控制），所以SQL中加 "auth=1"
			DBRow authRows[] = dbUtilAutoTran.selectMutliple("select action, action_uri, request_method from "+ConfigBean.getStringValue("authentication_action") + " where auth=1");
			
			return(authRows);
		}
		catch (Exception e)
		{
			log.error("AuthActionMgr.getAllAuthAction(request) error:" + e);
			throw new Exception("getAllAuthAction(request) error:" + e);
		}
	}

	/**
	 * 获得所有需要鉴权页面资源
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAuthPage()
		throws Exception
	{
		try
		{
			DBRow authRows[] = dbUtilAutoTran.selectMutliple("select link from "+ConfigBean.getStringValue("control_tree"));
			return(authRows);
		}
		catch (Exception e)
		{
			log.error("AuthActionMgr.getAllAuthPage(request) error:" + e);
			throw new Exception("getAllAuthPage(request) error:" + e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}








