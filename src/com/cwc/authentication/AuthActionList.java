package com.cwc.authentication;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

/**
 * 存放需要鉴权事件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AuthActionList
{
	static Logger log = Logger.getLogger("PLATFORM");
	private static ArrayList<String> authActionList = new ArrayList<String>();
	private AuthActionMgr authActionMgr;
	private static HashMap<String, DBRow> authUriMap = new HashMap<String, DBRow>();

	/**
	 * 系统启动时加载鉴权事件到内存
	 * @throws Exception
	 */
	public void init()
		throws Exception
	{
		try
		{
			DBRow authActionRow[] = authActionMgr.getAllAuthAction();
			for (int i=0; i<authActionRow.length; i++)
			{
				authActionList.add(authActionRow[i].getString("action"));
				if(!StringUtil.isBlank(authActionRow[i].getString("action_uri"))){
					authUriMap.put(authActionRow[i].getString("action_uri"), authActionRow[i]);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Create AuthActionList error:"+e);
			throw new Exception("Create AuthActionList error:"+e);
		}
		
		log.info("Load AuthActionList successful!");
	}

	public static boolean isNeed2Auth(String action)
	{
		return(authActionList.contains(action));
	}

	public static ArrayList<String> getAuthActionList()
	{
		return(authActionList);
	}
	
	public static boolean isNeed2AuthUri(String uri){
		return authUriMap.containsKey(uri);
	}
	
	public static HashMap<String, DBRow> getAuthUriMap(){
		return authUriMap;
	}
	
	public static void destroy()
	{
		authActionList.clear();
		authActionList = null;
		log.info("Destroy AuthActionList");
		
		authUriMap.clear();
		authUriMap = null;
		log.info("Destroy AuthUriMap");
	}

	public void setAuthActionMgr(AuthActionMgr authActionMgr)
	{
		this.authActionMgr = authActionMgr;
	}
}
