package com.cwc.authentication;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.cwc.db.DBRow;
import com.cwc.exception.PageMatchRegException;

/**
 * 存放需要鉴权页面资源
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AuthPageList
{
	static Logger log = Logger.getLogger("PLATFORM");
	private static ArrayList<String> authPageList = new ArrayList<String>();
	private AuthActionMgr authActionMgr;

	/**
	 * 系统启动时加载鉴权页面资源到内存
	 * @throws Exception
	 */
	public void init()
		throws Exception
	{
		try
		{
			DBRow authActionRow[] = authActionMgr.getAllAuthPage();
			for (int i=0; i<authActionRow.length; i++)
			{
				if (!authActionRow[i].getString("link").equals(""))
				{
					authPageList.add(authActionRow[i].getString("link"));					
				}
			}
		}
		catch (Exception e)
		{
			log.error("Create AuthPageList error:"+e);
			throw new Exception("Create AuthPageList error:"+e);
		}
		
		log.info("Load AuthPageList successful!");
	}

	/**
	 * 判断当前访问页面是否需要鉴权，支持正则表达式配置
	 * @param page
	 * @return
	 */
	public static boolean isNeed2Auth(String page)
		throws PageMatchRegException
	{
		//如果没有匹配页面，再检查下是否匹配正则表达式
		if (authPageList.contains(page)==false)
		{
			Pattern p;
			Matcher m;
			
			for (int i=0; i<authPageList.size(); i++)
			{
				//找出正则表达式配置的权限页面
				if ( authPageList.get(i).toString().indexOf("(")>=0&&authPageList.get(i).toString().indexOf(")")>=0 )
				{
					p = Pattern.compile(authPageList.get(i).toString(),2); //参数2表示大小写不区分
					m = p.matcher(page);
					//匹配上
					if (m.find())
					{
						throw new PageMatchRegException(authPageList.get(i).toString());//把匹配的权限配置正则表达式往外扔
					}
				}
			}
			return(false);
		}
		
		return(true);
	}

	public static ArrayList<String> getAuthPageList()
	{
		return(authPageList);
	}
	
	public static void destroy()
	{
		authPageList.clear();
		authPageList = null;
		log.info("Destroy AuthPageList");
	}

	public void setAuthActionMgr(AuthActionMgr authActionMgr)
	{
		this.authActionMgr = authActionMgr;
	}
	
	public static void main(String args[])
	{
		Pattern p = Pattern.compile("help_center/index-([0-9]+).html",2); //参数2表示大小写不区分
		Matcher m = p.matcher("help_center/index-12.html");
		boolean result = m.find();
		////system.out.println(result);
	}
	
	
}





