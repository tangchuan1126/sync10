package com.cwc.authentication;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.sbb.AccountMgrSbb;
import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.db.DBRow;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageMatchRegException;
import com.cwc.spring.util.MvcUtil;

/**
 * 后台管理员权限
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AdminAuthenCenter
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private static HashMap<String, ArrayList> permitPageHM = new HashMap<String, ArrayList>();			//页面资源对应角色id
	private static HashMap<String, ArrayList> permitActionHM = new HashMap<String, ArrayList>();		//action资源对一个角色id
	
	private static HashMap<String, ArrayList> permitPageExtendHM = new HashMap<String, ArrayList>();			//页面资源对应管理员id
	private static HashMap<String, ArrayList> permitActionExtendHM = new HashMap<String, ArrayList>();		//action资源对一个管理员id
	
	private static HashMap<Long, HashMap<Long, String>> rolePermitPageCache = new HashMap<Long, HashMap<Long, String>>(); //角色权限对应的页面资源
	private static HashMap<Long, HashMap<Long, String>> userExtendPageCache = new HashMap<Long, HashMap<Long, String>>(); //用户扩展权限对应的页面资源
	
	/**
	 * 设置页面资源对应角色ID，一对多
	 * @param page
	 * @param adgid
	 */
	private static void putPermitPageHM(String page, ArrayList<String> adgid)
	{
		permitPageHM.put(page, adgid);
	}
	
	/**
	 * 设置页面资源对应管理员ID，一对多
	 * @param page
	 * @param adid
	 */
	private static void putPermitPageExtendHM(String page, ArrayList<String> adid)
	{
		permitPageExtendHM.put(page, adid);
	}
	
	/**
	 * 设置action资源对应角色ID，一对多
	 * @param action
	 * @param adgid
	 */
	private static void putPermitActionHM(String action, ArrayList<String> adgid)
	{
		permitActionHM.put(action, adgid);
	}
	
	/**
	 * 设置action资源对应管理员ID，一对多
	 * @param action
	 * @param adid
	 */
	private static void putPermitActionExtendHM(String action, ArrayList<String> adid)
	{
		permitActionExtendHM.put(action, adid);
	}

	/**
	 * 通过page获得允许的角色id
	 * @param page
	 * @return
	 */
	private static ArrayList getPermitPageGroups(String page)
	{
		return((ArrayList)permitPageHM.get(page));
	}
	
	/**
	 * 通过page获得允许的管理员id
	 * @param page
	 * @return
	 */
	private static ArrayList getPermitPageGroupsExtend(String page)
	{
		return((ArrayList)permitPageExtendHM.get(page));
	}
	
	/**
	 * 通过action获得允许的角色id
	 * @param action
	 * @return
	 */
	private static ArrayList getPermitActionGroups(String action)
	{
		return((ArrayList)permitActionHM.get(action));
	}
	
	private static ArrayList getPermitActionGroupsExtend(String action)
	{
		return((ArrayList)permitActionExtendHM.get(action));
	}
	
	/**
	 * 判断某个角色是否有该页面资源权限
	 * 先判断角色权限，再判断帐号扩展权限
	 * @param page
	 * @param adgid
	 * @param adid
	 * @return 
	 * @return
	 */
	public static void isPermitPage(String page,long adgid,long adid)
		throws OperationNotPermitException
	{
		boolean isNeed2Auth = false;
		
		try
		{
			/**
			 * 不对URL做任何处理前，先判断是否需要鉴权
			 * 因为有可能某些URL就是直接带参数的
			 */
			if (!AuthPageList.isNeed2Auth(page))
			{
				//对带参数URL处理下
				if ( page.indexOf("String")>0 )
				{
					//把后面参数去掉
					page = page.substring(0,page.indexOf("String"));
					
					if (AuthPageList.isNeed2Auth(page))
					{
						isNeed2Auth = true;
					}
				}
			}
			else
			{
				isNeed2Auth = true;
			}
		}
		catch (PageMatchRegException e)
		{
			//把页面转换成配置权限的正则表达式
			page = e.getMessage();
		}
		
		//角色权限或者扩展权限授权
		if ( isNeed2Auth&&!havePageRoleRights( page, adgid)&&!havePageAdminRights(page, adid) )
		{
			throw new OperationNotPermitException(page);//把没有权限的页面抛出去
		}
	}
	
	/**
	 * 是否具有页面角色权限
	 * @param page
	 * @param adgid
	 * @return
	 */
	public static boolean havePageRoleRights(String page,long adgid)
	{
		return havePageRoleRights0(page, adgid);
	  /*	
		//角色权限
		ArrayList permitAdgid = getPermitPageGroups(page);

		if (permitAdgid==null)
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			
			try
			{
				permitAdgid = adminMgr.getPermitPageAdgid(page);
				putPermitPageHM(page, permitAdgid);
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.havePageRoleRights() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdgid = new ArrayList();
			}
		}
		
		return(permitAdgid.contains(String.valueOf(adgid)));
	  */	
	}
	
	/**
	 * 是否具有页面扩展权限
	 * @param page
	 * @param adid
	 * @return
	 */
	public static boolean havePageAdminRights(String page,long adid)
	{
		return havePageAdminRights0(page, adid);
	  /*	
		//管理员权限
		ArrayList permitAdid = getPermitPageGroupsExtend(page);

		if (permitAdid==null)
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			
			try
			{
				permitAdid = adminMgr.getPermitPageAdid(page);
				putPermitPageExtendHM(page, permitAdid);
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.havePageAdminRights() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdid = new ArrayList();
			}
		}
		
		return( permitAdid.contains(String.valueOf(adid)) );
	  */	
	}
	

	/**
	 * 判断某个角色是否有该action资源权限
	 * 先判断角色权限，再判断帐号扩展权限
	 * @param action
	 * @param adgid
	 * @param adid
	 * @return
	 */
	public static boolean isPermitAction(String action,long adgid,long adid)
	{
		if (action.equals(""))
		{
			return(false);
		}
		
		//先判断action是否需要鉴权
		if (!AuthActionList.isNeed2Auth(action))
		{
			return(true);
		}
		
		//角色权限或者扩展权限授权
		return( haveActionRoleRights( action, adgid)||haveActionAdminRights( action, adid) )
				|| ( getRoleNeed2AuthActions(adgid).contains(action)||getUserNeed2AuthActions(adid).contains(action) );
		
	}

	/**
	 * 是否具有action角色权限
	 * @param action
	 * @param adgid
	 * @return
	 */
	public static boolean haveActionRoleRights(String action,long adgid)
	{
		//角色权限
		ArrayList permitAdgid = getPermitActionGroups(action);
		
		if (permitAdgid==null)
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			
			try
			{
				permitAdgid = adminMgr.getPermitActionAdgid(action);
				putPermitActionHM(action, permitAdgid);
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.haveActionRoleRights() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdgid = new ArrayList();
			}
		}
		boolean qq = permitAdgid.contains(String.valueOf(adgid));
		return(qq);
	}
	
	/**
	 * 是否具有action扩展权限
	 * @param action
	 * @param adid
	 * @return
	 */
	public static boolean haveActionAdminRights(String action,long adid)
	{
		//管理员扩展权限
		ArrayList permitAdid = getPermitActionGroupsExtend(action);
		
		if (permitAdid==null)
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			
			try
			{
				permitAdid = adminMgr.getPermitActionAdid(action);
				putPermitActionExtendHM(action, permitAdid);
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.haveActionAdminRights() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdid = new ArrayList();
			}
		}
		boolean qq = permitAdid.contains(String.valueOf(adid)); 
		return(qq);
	}
	
	
	/**
	 * 清空页面资源权限缓存
	 */
	public static void clearPermitPageHM()
	{
		if (permitPageHM!=null)
		{
			permitPageHM.clear();
		}
		
		if (permitPageExtendHM!=null)
		{
			permitPageExtendHM.clear();
		}
		
		if(rolePermitPageCache!=null){
			rolePermitPageCache.clear();
		}
		
		if(userExtendPageCache!=null){
			userExtendPageCache.clear();
		}
	}
	
	/**
	 * 清空action资源权限缓存
	 */
	public static void clearPermitActionHM()
	{
		if (permitActionHM!=null)
		{
			permitActionHM.clear();
		}
		
		if (permitActionExtendHM!=null)
		{
			permitActionExtendHM.clear();
		}
	}
	
	/**
	 * 验证：此账号是否拥有菜单权限
	 * @param 账号,部门,按钮
	 * @return true/false
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static boolean menuAuthorityValidate(long adid,DBRow[] dept,String authority){
		//每次动态获取用户角色，防止修改用户角色后adminLoggerBean中取得的角色与修改后不符
		FloorAccountMgrSbb floorAccountMgr = (FloorAccountMgrSbb)MvcUtil.getBeanFromContainer("floorAccountMgr");
		
		try {
			dept = floorAccountMgr.getAccountDepartmentAndPost(adid);
		} catch (Exception e1) {
			dept = new DBRow[]{};
		}
		
		boolean result = false;
		
		if(!authority.equals("")){
			
			//处理带参数url
			if ( authority.indexOf("String")>0 ){
				//把后面参数去掉
				authority = authority.substring(0,authority.indexOf("String"));
			}
			
			//适应老模式 捕获异常
			boolean auth = false;
			
			try {
				
				auth = !AuthPageList.isNeed2Auth(authority);
				
			}catch (PageMatchRegException e) {
				//把页面转换成配置权限的正则表达式
				authority = e.getMessage();
			}	
			
			//判断权限
			if (auth){
				
				result = true;
			}else{
				
				boolean result1 = havePageAdminRights(authority,adid);
				
				boolean result2 = false;
				
				for(DBRow oneResult : dept ){
					
					if(havePageRoleRights(authority,Long.valueOf(oneResult.getString("DEPTID")))){

						result2 = true;
						break;
					}
				}
				
				if(result1 || result2){
					
					result = true;
				}
			} 
		}
		
		return result;
	}
	/**
	 * 验证：此账号是否拥有页面按钮权限
	 * @param 账号,部门,按钮
	 * @return true/false
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static boolean pageAuthorityValidate(long adid,DBRow[] dept,String authority){
		//每次动态获取用户角色，防止修改用户角色后adminLoggerBean中取得的角色与修改后不符
		FloorAccountMgrSbb floorAccountMgr = (FloorAccountMgrSbb)MvcUtil.getBeanFromContainer("floorAccountMgr");
		
		try {
			dept = floorAccountMgr.getAccountDepartmentAndPost(adid);
		} catch (Exception e1) {
			dept = new DBRow[]{};
		}
		
		boolean result = false;
		
		if(!authority.equals("")){
			
			if (!AuthActionList.isNeed2Auth(authority)){
				
				result = true;
			}else{
				
				boolean result1 = haveActionAdminRights(authority,adid) || getUserNeed2AuthActions(adid).contains(authority);
				
				boolean result2 = false;
				
				for(DBRow oneResult : dept ){
					
					if(haveActionRoleRights(authority,Long.valueOf(oneResult.getString("DEPTID"))) || getRoleNeed2AuthActions(oneResult.get("DEPTID", -1)).contains(authority)){

						result2 = true;
						break;
					}
				}
				
				if(result1 || result2){
					
					result = true;
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * 是否具有页面角色权限
	 * @param page
	 * @param adgid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static boolean havePageRoleRights0(String page,long adgid)
	{
		//角色权限
		HashMap<Long, String> permitAdgid = getPermitRolePage(adgid);

		if (permitAdgid==null)
		{
			try
			{
				cacheRolePermitPage(adgid);
				
				permitAdgid = getPermitRolePage(adgid);
				
				permitAdgid = (permitAdgid==null) ? new HashMap<Long, String>() : permitAdgid; 
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.havePageRoleRights0() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdgid = new HashMap<Long, String>();
			}
		}
		
		return(permitAdgid.values().contains(page));
	}
	
	
	/**
	 * 缓存角色权限对应页面资源
	 * @param adgid
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static void cacheRolePermitPage(long adgid) throws Exception
	{
		//角色权限
		HashMap<Long, String> permitAdgid = getPermitRolePage(adgid);

		if (permitAdgid==null)
		{
			AccountMgrSbb accountMgr = (AccountMgrSbb)MvcUtil.getBeanFromContainer("accountMgr");
			
			HashMap<String, DBRow> tileMenuTree = new HashMap<String, DBRow>();
			accountMgr.genTileMenuTreeFromMenuRecord(accountMgr.getRolePageRecord(adgid), tileMenuTree);
			
			permitAdgid = new HashMap<Long, String>();
			for(DBRow node: tileMenuTree.values()){
				permitAdgid.put(node.get("id", -1L), node.getString("link"));
			}
			
			putRolePermitPage(adgid, permitAdgid);
		}
	}
	
	/**
	 * 是否具有页面扩展权限
	 * @param page
	 * @param adid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static boolean havePageAdminRights0(String page,long adid)
	{
		//管理员权限
		HashMap<Long, String> permitAdid = getPermitUserExtendPage(adid);

		if (permitAdid==null)
		{
			try
			{
				cacheUserExtPermitPage(adid);
				
				permitAdid = getPermitUserExtendPage(adid);
				
				permitAdid = (permitAdid==null) ? new HashMap<Long, String>() : permitAdid; 
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.havePageAdminRights0() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdid = new HashMap<Long, String>();
			}
		}
		
		return( permitAdid.values().contains(page) );
	}
	
	/**
	 * 缓存用户扩展权限对应页面资源
	 * @param adid
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static void cacheUserExtPermitPage(long adid) throws Exception
	{
		//管理员权限
		HashMap<Long, String> permitAdid = getPermitUserExtendPage(adid);

		if (permitAdid==null)
		{
			AccountMgrSbb accountMgr = (AccountMgrSbb)MvcUtil.getBeanFromContainer("accountMgr");
			
			HashMap<String, DBRow> tileMenuTree = new HashMap<String, DBRow>();
			accountMgr.genTileMenuTreeFromMenuRecord(accountMgr.getUserExtendPageRecord(adid), tileMenuTree);
			
			permitAdid = new HashMap<Long, String>();
			for(DBRow node: tileMenuTree.values()){
				permitAdid.put(node.get("id", -1L), node.getString("link"));
			}
			
			putUserExtendPage(adid, permitAdid);
			
		}
	}
	
	/**
	 * 获取adgid授权的需验证的Action
	 * @param adgid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static ArrayList getRoleNeed2AuthActions(long adgid)
	{
		ArrayList authActionsList = new ArrayList();
		
		//角色权限
		HashMap<Long, String> permitAdgid = getPermitRolePage(adgid);

		if (permitAdgid==null)
		{
			try
			{
				cacheRolePermitPage(adgid);
				
				permitAdgid = getPermitRolePage(adgid);
				
				permitAdgid = (permitAdgid==null) ? new HashMap<Long, String>() : permitAdgid; 
			}
			catch (Exception e)
			{
				log.error("AdminAuthenCenter.havePageRoleRights0() error:"+e);
				
				//如果从数据库获取出错了，则付一个空值
				permitAdgid = new HashMap<Long, String>();
			}
		}
		
		StringBuffer sqlBuffer = new StringBuffer();
		if(permitAdgid.size() > 0){
			sqlBuffer.append("SELECT action FROM authentication_action aua WHERE (");
			
			int i=0;
			for(Long pageId : permitAdgid.keySet()){
				if(i > 0){
					sqlBuffer.append(" OR aua.page=").append(pageId);
				}else{
					sqlBuffer.append(" aua.page=").append(pageId);
				}
				
				i++;
			}
			
			sqlBuffer.append(" ) AND NOT EXISTS(SELECT 1 FROM admin_group_auth aga LEFT JOIN authentication_action aua1 ON aua1.ataid = aga.ataid WHERE aua1.page = aua.page AND  aga.adgid = "+ adgid +") ")
					 .append("UNION ALL ")
					 .append("SELECT action FROM admin_group_auth aga LEFT JOIN  authentication_action aua ON aua.ataid = aga.ataid WHERE aga.adgid = "+ adgid);
		}
		
		if(sqlBuffer.length() > 0){
			try
			{
				AccountMgrSbb accountMgr = (AccountMgrSbb)MvcUtil.getBeanFromContainer("accountMgr");
				
				DBRow authActionRow[] = accountMgr.getRoleOrUserNeed2AuthActions(sqlBuffer.toString());
				for (int i=0; i<authActionRow.length; i++)
				{
					if (!authActionRow[i].getString("action").equals(""))
					{
						authActionsList.add(authActionRow[i].getString("action"));					
					}
				}
			}
			catch (Exception e)
			{
				
			}
		}
		
		return authActionsList;
	}
	
	/**
	 * 获取adid授权的需验证的Action
	 * @param adid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static ArrayList getUserNeed2AuthActions(long adid)
	{
		ArrayList authActionsList = new ArrayList();
		
		//管理员权限
		HashMap<Long, String> permitAdid = getPermitUserExtendPage(adid);

		if (permitAdid==null)
		{
			try
			{
				cacheUserExtPermitPage(adid);
				
				permitAdid = getPermitUserExtendPage(adid);
				
				permitAdid = (permitAdid==null) ? new HashMap<Long, String>() : permitAdid; 
			}
			catch (Exception e)
			{
				//如果从数据库获取出错了，则付一个空值
				permitAdid = new HashMap<Long, String>();
			}
		}
		
		StringBuffer sqlBuffer = new StringBuffer();
		if(permitAdid.size() > 0){
			sqlBuffer.append("SELECT action FROM authentication_action aua WHERE (");
			
			int i=0;
			for(Long pageId : permitAdid.keySet()){
				if(i > 0){
					sqlBuffer.append(" OR aua.page=").append(pageId);
				}else{
					sqlBuffer.append(" aua.page=").append(pageId);
				}
				
				i++;
			}
			
			sqlBuffer.append(" ) AND NOT EXISTS(SELECT 1 FROM turboshop_admin_group_auth_extend agae LEFT JOIN authentication_action aua1 ON agae.ataid = aua1.ataid WHERE aua1.page = aua.page AND  agae.adid = "+ adid +")")
					 .append("UNION ALL ")
					 .append("SELECT aua.action action FROM turboshop_admin_group_auth_extend agae LEFT JOIN authentication_action aua ON agae.ataid = aua.ataid WHERE agae.adid ="+ adid);
		}
		
		if(sqlBuffer.length() > 0){
			try
			{
				AccountMgrSbb accountMgr = (AccountMgrSbb)MvcUtil.getBeanFromContainer("accountMgr");
				
				DBRow authActionRow[] = accountMgr.getRoleOrUserNeed2AuthActions(sqlBuffer.toString());
				for (int i=0; i<authActionRow.length; i++)
				{
					if (!authActionRow[i].getString("action").equals(""))
					{
						authActionsList.add(authActionRow[i].getString("action"));					
					}
				}
			}
			catch (Exception e)
			{
				
			}
		}
		
		return authActionsList;
	}
	
	/**
	 * 设置角色ID对应页面资源，一对多
	 * @param adgid
	 * @param pages
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static void putRolePermitPage(long adgid, HashMap<Long, String> pages)
	{
		rolePermitPageCache.put(adgid, pages);
	}
	
	/**
	 * 设置管理员ID对应扩展页面资源，一对多
	 * @param adid
	 * @param extPages
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static void putUserExtendPage(long adid, HashMap<Long, String> extPages)
	{
		userExtendPageCache.put(adid, extPages);
	}
	
	/**
	 * 通过角色id获得允许的page
	 * @param adgid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static HashMap<Long, String> getPermitRolePage(long adgid)
	{
		return(rolePermitPageCache.get(adgid));
	}
	
	/**
	 * 通过用户id获得用户扩展(定制)page
	 * @param adid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private static HashMap<Long, String> getPermitUserExtendPage(long adid)
	{
		return(userExtendPageCache.get(adid));
	}
}



