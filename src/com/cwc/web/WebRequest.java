package com.cwc.web;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

public class WebRequest 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private String requestURI = null;
	private String server = null;
	
	public static String HTTP_PROTOCOL = "http";
	public static String HTTPS_PROTOCOL = "https";
	int port = 80;
	
	private String protocol = HTTP_PROTOCOL;
	
	
	private TextPara textPara = null;
	private int serverStatus = 0;						//http返回状态
	private String serverResponse = null;				//返回response
	
	public HttpMethod outMethod = null;
	
	/**
	 * 设置协议
	 * @param protocol
	 */
	public void setProtocol(String protocol)
	{
		this.protocol = protocol;
	}
	
	/**
	 * 设置当前URL
	 * @param requestURI
	 */
	public void setRequestURI(String requestURI)
	{
		this.requestURI = requestURI;
	}
	
	/**
	 * 设置服务器
	 * @param server
	 */
	public void setServer(String server)
	{
		this.server = server;
	}
	
	/**
	 * 设置端口
	 * @param port
	 */
	public void setPort(int port)
	{
		this.port = port;
	}
	
	/**
	 * 设置文本参数
	 * @param textPara
	 */
	public void setPara(TextPara textPara)
	{
		this.textPara = textPara;
	}
	
	/**
	 * 
	 * @return
	 */
	private HttpMethod getGetMethod()
	{
		
		if (textPara==null)
		{
			return new GetMethod(requestURI);
		}
		else
		{
			return new GetMethod(requestURI+textPara.getGETPara());
		}
	}

	/**
	 * 
	 * @return
	 */
	private HttpMethod getPostMethod()
    {
		PostMethod post = new MyPostMethod(requestURI);
		////system.out.println("------------->2echo println:"+post);
		post.setRequestBody(textPara.getPOSTPara());
		return(post);
    }

	/**
	 * 提交GET
	 * @throws Exception
	 */
	public void commitGET()
		throws Exception
	{
		HttpMethod method = null;
		
		try
		{
			HttpClient client = new HttpClient();
			client.getHostConfiguration().setHost(server,port,protocol);
			
			method = getGetMethod();
			 
			client.executeMethod(method);
			
			serverStatus = method.getStatusLine().getStatusCode();
			serverResponse = method.getResponseBodyAsString();
			
			outMethod = method;
		}
		catch (HttpException e) 
		{
			log.error("commitGET HttpException error:"+e);
			throw new Exception("commitGET HttpException error:"+e);
		}
		catch (IOException e)
		{
			log.error("commitGET IOException error:"+e);
			throw new Exception("commitGET IOException error:"+e);
		}
		finally
		{
			method.releaseConnection();	
		}
	}
	
	/**
	 * 提交POST
	 * @throws Exception
	 */
	public void commitPOST()
		throws Exception
	{
		HttpMethod method = null;
		
		try
		{
			HttpClient client = new HttpClient();
//			client.getHostConfiguration().setHost(server,port,protocol);
//			////system.out.println("------------->1echo println:"+client);
//			method = getPostMethod();
			method=new GetMethod(server);
		      
			client.executeMethod(method);
			
			serverStatus = method.getStatusLine().getStatusCode();
			serverResponse = method.getResponseBodyAsStream().toString();
			
			outMethod = method;
		}
		catch (HttpException e) 
		{
			//system.out.println("commitPOST HttpException error:"+e);
			throw new Exception("commitGET HttpException error:"+e);
		}
		catch (IOException e)
		{
			//system.out.println("commitPOST IOException error:"+e);
			throw new Exception("commitGET IOException error:"+e);
		}
		finally
		{
			if (method!=null)
			{
				method.releaseConnection();	
			}
		}
	}
	
	/**
	 * 获得服务器返回状态
	 * @return
	 */
	public int getServerStatus()
	{
		return(serverStatus);
	}
	
	/**
	 * 获得返回的response
	 * @return
	 */
	public String getServerResponse()
	{
		return(serverResponse);
	}
	
	public static void main(String args[]) throws Exception
	{
//		String name = "echo";
//		String pwd = "";
//		String email = "33qa@q1213.com";
		
		WebRequest wr = new WebRequest();
 
//		wr.setPort(80);
//
//		wr.setProtocol(WebRequest.HTTP_PROTOCOL);
		String tranID = "5R662179881871A";
		String server = "http://www.vvme.com/webservice?txn_id=";
		wr.setServer(server+tranID);
		wr.commitPOST();
		
		//system.out.println(wr.getServerStatus()+"kkkkkkkkkkkkk");//页面正常输出 都是 200 
		//system.out.println(wr.getServerResponse());
	 
	 
    }   


}
