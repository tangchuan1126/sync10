package com.cwc.web;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.httpclient.NameValuePair;

/**
 * 文本参数
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TextPara 
{
	private HashMap paraA = new HashMap();
	
	/**
	 * 设置参数
	 * @param name
	 * @param val
	 */
	public void putPara(String name,String val)
	{
		paraA.put(name,val);
	}
	
	/**
	 * 获得POST参数
	 * @return
	 */
	public NameValuePair[] getPOSTPara()
	{
		Iterator it = paraA.keySet().iterator();
		NameValuePair nvp[] = new NameValuePair[paraA.size()];
		
		int i = 0;
		while (it.hasNext())
		{
            Object obj = it.next();
            nvp[i++] = new NameValuePair(obj.toString(),paraA.get( obj.toString() ).toString());
            ////system.out.println(obj.toString()+" - "+paraA.get( obj.toString() ).toString());
        }
		
		return(nvp);
	}

	/**
	 * 获得GET参数
	 * @return
	 */
	public String getGETPara()
	{
		Iterator it = paraA.keySet().iterator();
		StringBuffer sb = new StringBuffer("?");
		
		Object obj = null;
		int i = 0;
		while (true)
		{
			obj = it.next();
			
			sb.append(obj.toString());
			sb.append("=");
			sb.append(paraA.get( obj.toString() ).toString());
			sb.append("&");
			
			if (i++ == paraA.size()-2)
			{
				break;
			}
		}
		obj = it.next();
		sb.append(obj.toString());
		sb.append("=");
		sb.append(paraA.get( obj.toString() ).toString());
		
		return(sb.toString());
	}
	
	public static void main(String args[])
	{
		TextPara t = new TextPara();
		t.putPara("a","1");
		t.putPara("B","2");
		t.putPara("c","13");
		t.putPara("FF","哈哈");
		//system.out.println(t.getGETPara());
	}
}
