package com.cwc.email;

import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;

import com.cwc.app.api.SystemConfig;
import com.cwc.spring.util.MvcUtil;

/**
 * 发送邮件用户设置
 * @author Administrator
 *
 */
public class User  implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4786527349847487899L;
	private String userName;	//帐户
	private String passWord;	//密码
	private String host;		//smtp服务器
	private String from;		//发送人邮件
	private int port;			//smtp服务端口
	private String author;		//发送人（例：微尘大业）
	private String rePly;		//回复邮件

	/**
	 * 完整参数设置
	 * @param userName
	 * @param passWord
	 * @param host
	 * @param port
	 * @param from
	 * @param author	//发件人名称
	 * @param rePly
	 * @throws Exception
	 */
	public User(String userName,String passWord,String host,int port,String from,String author,String rePly)
		throws Exception
	{
		this.userName = userName;
		this.passWord = passWord;
		this.host = host;
		this.from = from;
		this.port = port;
		this.author = author;
		this.rePly = rePly;
	}
	
	/**
	 * 使用同系统默认帐号发送
	 * @throws Exception
	 */
	public User()
		throws Exception
	{
		SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
		
		this.userName = systemConfig.getStringConfigValue("sendMailAccount");
		this.passWord = systemConfig.getStringConfigValue("sendMailPwd");
		this.host = systemConfig.getStringConfigValue("sendMailSmtp");
		this.from = systemConfig.getStringConfigValue("mailAuthor");
		this.port = systemConfig.getIntConfigValue("sendMailSmtpPort");
		this.author = systemConfig.getStringConfigValue("sendMailSender");
		this.rePly = systemConfig.getStringConfigValue("sendMailReplyMail");
	}
	
	/**
	 * 只设置回复邮件，其他参数使用系统配置
	 * @param rePly
	 * @throws Exception
	 */
	public User(String rePly)
		throws Exception
	{
		SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
		
		this.userName = systemConfig.getStringConfigValue("sendMailAccount");
		this.passWord = systemConfig.getStringConfigValue("sendMailPwd");
		this.host = systemConfig.getStringConfigValue("sendMailSmtp");
		this.from = systemConfig.getStringConfigValue("mailAuthor");;
		this.port = systemConfig.getIntConfigValue("sendMailSmtpPort");
		this.author = systemConfig.getStringConfigValue("sendMailSender");
		this.rePly = rePly;
	}
	
	/**
	 * 获得帐号
	 * @return
	 */
	public String getUsername()
	{
		return(this.userName);
	}
	
	/**
	 * 获得密码
	 * @return
	 */
	public String getPassword()
	{
		return(this.passWord);
	}
	
	/**
	 * 获得smtp服务器
	 * @return
	 */
	public String getHost()
	{
		return(this.host);
	}
	
	/**
	 * 获得发送地址
	 * @return
	 */
	public String getFrom()
	{
		Base64 enc = new Base64();
		
		author = "=?UTF-8?B?"+new String(enc.encode(author.getBytes()))+"?=";
		from = "\""+author+"\" <"+from+">";
		
		return(this.from);
	}
	
	/**
	 * 判断是否需要SSL登录SMTP
	 * @return
	 */
	public boolean isSSL()
	{
		return(false);
	}

	/**
	 * 获得SMTP端口
	 * @return
	 */
	public int getPort()
	{
		return port;
	}
	
	/**
	 * 获得回复邮件地址
	 * @return
	 */
	public String getRePly()
	{
		return rePly;
	}
	
	public static void main(String args[])
	{
		
	}
	
	
}





