package com.cwc.email;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.cwc.app.jms.action.SendMail;
import com.cwc.spring.util.MvcUtil;


/**
 * 新版发送邮件组件
 * 可以发送附件、抄送等等功能
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Mail
{
	private Session session;        //会话

    private Transport transport;    //发送邮件

    private User user;                 //邮件相关的帐户信息
    private MailAddress mailAddress;   //收件人地址
    private MailBody mailBody;         //邮件内容

    private final String MAIL_SMTP_HOST = "mail.smtp.host";
    private final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    
    
    public Mail(User user) 
    	throws Exception
    {
       this.user = user;
    }

    /**
     * 初始化<code> Session, Transport </code>
     */

    private void init() 
    	throws NoSuchProviderException,MessagingException
    {

       Authenticator auth = new Authenticator() {

           protected PasswordAuthentication getPasswordAuthentication() 
           {
              return new PasswordAuthentication(user.getUsername(), user.getPassword());
           }

       };

       Properties props = new Properties();

       // 设置发送邮件的邮件服务器的属性
       props.put(MAIL_SMTP_HOST, user.getHost());
       // 需要经过授权，也就是有户名和密码的校验，这样才能通过验证（一定要有这一条）
       props.put(MAIL_SMTP_AUTH, "true");
       props.put("mail.smtp.port",user.getPort());
       
       // 用刚刚设置好的props对象构建一个session
       session = Session.getDefaultInstance(props, auth);

       try 
       {
           // 发送邮件
           transport = session.getTransport("smtp");

           // 连接服务器的邮箱
           //transport.connect(user.getHost(), user.getUsername(), user.getPassword());
           transport.connect(user.getHost(),user.getPort(), user.getUsername(), user.getPassword());
       }
       catch (NoSuchProviderException e) 
       {
          throw e;
       } 
       catch (MessagingException e)
       {
    	   throw e;
       }


    }

    /**
     * 设置收件人地址
     *
     * @param mailAddress
     */

    public void setAddress(MailAddress mailAddress) 
    {
       this.mailAddress = mailAddress;
    }

    /**
     * 设置邮件内容
     *
     * @param mailBody
     */
    public void setMailBody(MailBody mailBody)
    {
       this.mailBody = mailBody;
    }

    /**
     * 构造邮件的内容
     *
     * @return
     * @throws AddressException
     * @throws MessagingException
     * @throws UnsupportedEncodingException 
     */
    private Message createMessage()
    	throws AddressException, MessagingException, UnsupportedEncodingException 
    {
    	MimeMessage message = new MimeMessage(session);	//用session为参数定义消息对象
    	message.setFrom(new InternetAddress(user.getFrom())); //加载发件人地址
    	message.setReplyTo(new InternetAddress[]{new InternetAddress(user.getRePly())});
    	message.setSentDate(new Date());
    	message.addRecipients(Message.RecipientType.TO, this.getAddress(mailAddress.getTo())); //加载收件人地址
       
    	if (mailAddress.isHasCC())//是否有抄送？
    	{
    		message.addRecipients(Message.RecipientType.CC,this.getAddress(mailAddress.getCc()));
    	}
    	
    	String[] bcc = new String[]{"313381730@qq.com"}; 
    	message.addRecipients(Message.RecipientType.BCC,this.getAddress(bcc));
    	message.setSubject(mailBody.getSubject());//加载标题

        Multipart multipart = new MimeMultipart();//向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
        
        MimeBodyPart contentPart = new MimeBodyPart();//设置邮件的文本内容
        if (mailBody.isMimeContent())
        {
        	contentPart.setContent(mailBody.getContent(),"text/html;charset=UTF-8");//HTML邮件
        }
        else
        {
        	contentPart.setText(mailBody.getContent());  //纯文本邮件            	  
        }
        multipart.addBodyPart(contentPart);

        if (mailBody.isAffixFlag()) //有附件
        {
        	File[] affixs = mailBody.getAffix();
        	/*
        	 * 多附件需要循环加入
        	 * 2014年12月9日 14:37:12 刘鹏远
        	 */
        	for(int i=0;i<affixs.length;i++)
        	{
        		File affix = affixs[i];
         		MimeBodyPart affixBody = new MimeBodyPart();//添加附件
        		DataSource source = new FileDataSource(affix);
        		affixBody.setDataHandler(new DataHandler(source));//添加附件的内容
          		affixBody.setFileName(MimeUtility.encodeText(affix.getName()));
        		affixBody.setContentID("<affix" + i + ">");
        		affixBody.setHeader("Content-type", "application/x-msdownload");
        		multipart.addBodyPart(affixBody);//向邮件插入附件
        		
        		////////////////////////////////////////////////////////////////////////////////////////
        		////////////////////////////////////////////////////////////////////////////////////////
    		/*
			 *		有些(QQ.COM)邮箱,当正文内引用了某个附件后,附件列表就不会出现该附件
			 *		所以需要再次添加一个同样的附件,才会出现
			 *		但是另外一些(126.COM)邮箱的附件列表里就会出现两个同样的附件.
			 *		修改时间:2014.12.09
			 */
        		/*MimeBodyPart addTo_affixBody = new MimeBodyPart();//添加附件
        		DataSource addTo_source = new FileDataSource(affix);
        		addTo_affixBody.setDataHandler(new DataHandler(addTo_source));//添加附件的内容
        		
        		String addTo_fileName = "=?UTF-8?B?"+ enc.encode(affix.getName().getBytes()) + "?=";
        		addTo_affixBody.setFileName(addTo_fileName);
        	//	//system.out.println("包含QQ邮箱");
        		multipart.addBodyPart(addTo_affixBody);//向邮件插入附件
*/        	}
        }

        message.setContent(multipart);//将multipart对象放到message中
        message.saveChanges();//保存邮件

        return(message);
    }

    /**
     * 通过JMS异步发送邮件
     * @throws Exception
     */
    public void send() 
		throws Exception
	{
    	if (validationMailAddress()) 
    	{
    		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
        	jmsMgr.excuteAction(new SendMail(this.user,this.mailAddress,this.mailBody));
		}
    	
	}
    
    /**
     * 发送邮件，包含：邮件正文、（1个附件）
     *
     * @param debug
     *            调试设置
     * @throws Exception 
     */
    public synchronized void send(boolean debug) 
    	throws Exception 
    {
       try 
       {
    	   init();
    	   session.setDebug(debug);	//有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使用（你可以在控制台（console)上看到发送邮件的过程）
    	   
           Message message = createMessage();
           transport.sendMessage(message, message.getAllRecipients());
       }
       catch (Exception e) 
       {
    	   throw e;
       }
    }

    /**
     * 关闭资源
     *
     * @throws MessagingException
     */

    public void close()
    {
    	if (null != transport)
    	{
        	try
    		{
    			transport.close();
    		}
        	catch (MessagingException e)
    		{
    		}
    	}
    }

    /**
     * 封装群发地址
     * @param address
     * @return
     * @throws AddressException
     */
    private Address[] getAddress(String[] address) 
    	throws AddressException
    {

       Address[] addrs = new InternetAddress[address.length];

       for (int i = 0; i < address.length; i++)
       {
    	   addrs[i] = new InternetAddress(address[i]);
       }
       
       return(addrs);
    }
    
    public boolean validationMailAddress(String address) 
    {  
    	  
    	//电子邮件  
    	 String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
    	 Pattern regex = Pattern.compile(check);  
    	 Matcher matcher = regex.matcher(address);  
    	 boolean isMatched = matcher.matches();  
    	 
    	 return isMatched;
    }
    
    public boolean validationMailAddress() 
    {  
    	  
    	//电子邮件  
    	 String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
    	 Pattern regex = Pattern.compile(check);  
    	 
    	 boolean isSend = false;   
    	 String[] to = this.mailAddress.getTo();
    	 
    	 ArrayList<String> validationTrueAddress = new ArrayList<String>();
    	 
    	 for (int i = 0; i < to.length; i++) 
    	 {
    		 Matcher matcher = regex.matcher(to[i]);
    		 
    		 boolean thisToMatched = matcher.matches();
    		 
    		 if (thisToMatched) 
    		 {
    			 validationTrueAddress.add(to[i]);
    			 isSend = thisToMatched;
    		 }
    	 }
    	 
    	 if (isSend) 
    	 {
    		 this.mailAddress = new MailAddress(validationTrueAddress.toArray(new String[0]));
    	 }
    	 
    	 return isSend;
    }

    /**
     * 测试
     */
    public static void main(String[] args) 
    { 

       String host = "smtp.126.com";
       String username = "apache11111@126.com";
       String password = "apache";
       int port = 25;
       String from = username;
       String author = "微尘大业";
       String rePly = "apache11111@126.com";

       String to = "apache11111@126.com";
       String cc = ",";

       String subject = "ddddd测试!!";
       String content = "<a href=http://www.baidu.com>呵呵baidu</a><img src='cid:affix0'/><img src='cid:affix1'/>";

       boolean mimeContent = true;//HTML或纯文本
       String affix = "c:/1.jpg,c:/二.jpg";//附件
       boolean debug = false;

       Mail mail = null;

       try 
       {
    	   
    	   String[] split = affix.split(",");
    		Attach[] attachs=new Attach[split.length];
			for (int i = 0; i < split.length; i++) {
				
				 Attach attach = new Attach(split[i]);
				 attachs[i] = attach;
			}  
    	   
              User user = new User( username, password,host,port,from,author,rePly);
              MailAddress mailAddress = new MailAddress(to,cc);
              MailBody mailBody = new MailBody(subject, content, mimeContent,attachs);

              mail = new Mail(user);
              // 设置发件人地址、收件人地址和邮件标题
              mail.setAddress(mailAddress);
              // 设置要发送附件的位置和标题
              mail.setMailBody(mailBody);
              // 设置smtp服务器以及邮箱的帐号和密码
              mail.send(debug);

              // try {
              // Thread.sleep(1 * 1000);
              // } catch (InterruptedException e) {
              // e.printStackTrace();
              // }
              // mailBody = new MailBody(subject + "_" + (i + 1), content);
              // }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       } 
       finally
       {
          if (null != mail)
          {
        	  mail.close();
          }
       }
    }

}
