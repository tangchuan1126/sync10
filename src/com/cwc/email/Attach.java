package com.cwc.email;

import java.io.Serializable;

/**
 * 邮件附件
 * @author Administrator
 *
 */
public class Attach implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7229947353123935435L;
	private String affix;	//附件完整路径
	
	public Attach(String affix)
	{
		this.affix = affix;
	}
	
	/**
	 * 获得附件完整路径
	 * @return
	 */
	public String getAffix()
	{
		return affix;
	}
}
