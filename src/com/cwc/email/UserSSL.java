package com.cwc.email;

/**
 * 使用SSL验证用户
 * @author Administrator
 *
 */
public class UserSSL extends User
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserSSL(String userName, String passWord, String host,int port,String from,String author,String rePly)
		throws Exception
	{
		super(userName, passWord, host,port, from,author,rePly);
	}
	
	public UserSSL()
		throws Exception
	{
		super();
	}
	
	public UserSSL(String rePly)
		throws Exception
	{
		super( rePly);
	} 

	public boolean isSSL()
	{
		return(true);
	}
}
