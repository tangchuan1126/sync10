package com.cwc.email;

import java.io.File;
import java.io.Serializable;

import com.cwc.app.page.core.VelocityPageFileFather4Email;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.app.page.core.VelocityTemplateFile4Email;
import com.cwc.app.page.core.VelocityTemplateString;


public class MailBody  implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1116406485719381423L;
	private String subject;
	private String content;
	
	/**
	 * @log 改为数组 刘鹏远 2014.12.09
	 */
	private String[] affix;	// 该body下的所以附件URL
	
	private boolean mimeContent;
	private boolean isAffixFlag = false;
	
	/**
	 * @log 改为数组 刘鹏远 2014.12.09
	 */
	private File[] affixFile;//附件文件	//
	
	/**
	 * 普通邮件
	 * @param subject
	 * @param content
	 * @param mimeContent	true(HTML) 或 false(纯文本)
	 * @param affix			null 不需要附件
	 * @throws Exception
	 */
	//把最后一个参数改为动态  刘鹏远 2014.12.09
	public MailBody(String subject,String  content,boolean mimeContent,Attach... attachs)
		throws Exception
	{
		this.init( subject,  content, mimeContent, attachs);
	}
	//把最后一个参数改为数组  刘鹏远 2014.12.09
	private void init(String subject,String  content,boolean mimeContent,Attach[] attachs)
		throws Exception
	{
		this.subject = subject;
		this.content = content;
		
		this.mimeContent = mimeContent;
		
		if (attachs!=null && attachs.length>0)
		{
			affixFile = new File[attachs.length];
			this.affix = new String[attachs.length];
			/*
			 * 多附件循环操作new File
			 * 加for循环 刘鹏远 2014.12.09
			 */
			for(int i=0;i<attachs.length;i++){
				Attach attach = attachs[i];
				affix[i] = attach.getAffix();
				affixFile[i] = new File(attach.getAffix());
				if (affixFile[i].exists()==false)
				{
					//抛出有异常文件 刘鹏远 2014.12.09
					throw new Exception("Aaffix not found! <" + affixFile[i] +">");
				}
				else if (affixFile[i].isFile()==false)
				{
					//抛出有异常文件 刘鹏远 2014.12.09
					throw new Exception("Aaffix not a file!! <" + affixFile[i] + ">");
				}
				else
				{
					isAffixFlag = true;//有附件并正确
				}
			}
		}
	}
	
	/**
	 * 使用velocity模板做邮件
	 * @param velocityPage	velocity邮件模板
	 * @param mimeContent	true(HTML) 或 false(纯文本)
	 * @param affix			null 不需要附件
	 * @throws Exception
	 */
	public MailBody(VelocityPageFileFather4Email velocityPage,boolean mimeContent,Attach[] attach)
		throws Exception
	{
		String vBody = VelocityTemplateFile4Email.getInstance().getPageContent(velocityPage);
		
		String tmpSubject = "";
		
		if (vBody.indexOf("<title>")>=0&&vBody.indexOf("</title>")>=0)
		{
			tmpSubject = vBody.split("<title>")[1];
			tmpSubject = tmpSubject.split("</title>")[0];
		}
		
		this.init( tmpSubject,  vBody, mimeContent, attach);
	}

	/**
	 * 使用velocity字符串模板做邮件
	 * @param velocityPage
	 * @param mimeContent
	 * @param attach
	 * @throws Exception
	 */
	//把最后一个参数改为动态  刘鹏远 2014.12.09
	public MailBody(VelocityEmailPageStringFather velocityPage,boolean mimeContent,Attach... attach)
		throws Exception
	{
		this( velocityPage.getTemplateTitle(),  velocityPage.getBody(), mimeContent, attach);
	}
	
/*	public MailBody(VelocityEmailPageStringFather velocityPage,boolean mimeContent,Attach[] attachs)
			throws Exception
		{
			VelocityTemplateString.getInstance().getPageContent(velocityPage);
			this.init( velocityPage.getTemplateTitle(),  velocityPage.getBody(), mimeContent, attachs);
		}*/


	/**
	 * 获得邮件主题
	 * @return
	 */
	public String getSubject()
	{
		return(subject);
	}
	
	/**
	 * 获得邮件内容
	 * @return
	 */
	public String getContent()
	{
		return(content);
	}
	
	/**
	 * 判断是否多媒体邮件
	 * @return
	 */
	public boolean isMimeContent()
	{
		return(mimeContent);
	}
	
	/**
	 * 判断是否发送附件
	 * @return
	 */
	public boolean isAffixFlag()
	{
		return(isAffixFlag);
	}
	
	/**
	 * 获得附件文件
	 * @return
	 */
	public File[] getAffix()
	{
		return(affixFile);
	}
	
	/**
	 * 获得附件文件名数组
	 * 与附件数组下标对应
	 * @return
	 * @log 
	 */
	@Deprecated
	public String[] getAffixName()
	{
		String preFit;
		String[] affixNames = new String[this.affix.length];
		for(int i=0;i<this.affix.length;i++){
			String affix = this.affix[i];
			if (affix.indexOf("/")>=0)
			{
				preFit = "/";
			}
			else
			{
				preFit = "\\";
			}
			int pos = affix.lastIndexOf(preFit);
			;
			affixNames[i] = affix.substring(pos+1,affix.length());
		}
		
		return affixNames;
	}
	
	public static void main(String args[])
	{
		//String s = "d:/1wd/dd.pdf";
		
	}
	
}




