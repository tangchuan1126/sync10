package com.cwc.email;

import java.io.Serializable;

/**
 * 接受邮件的地址
 * 支持抄送
 * 多个收件人使用,隔开
 * @author Administrator
 *
 */
public class MailAddress  implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5979853667608124490L;
	private String to[];
	private String cc[];
	private boolean isHasCC = false;
	
	public MailAddress(String to)
	{
		this.to = to.split(","); 
	}
	
	public MailAddress(String[] tos)
	{
		this.to = tos;
	}
	
	public MailAddress(String to,String cc)
	{
		this.to = to.split(","); 
		this.cc = cc.split(","); 
		isHasCC = true;
	}
	
	/**
	 * 是否有抄送人
	 * @return
	 */
	public boolean isHasCC()
	{
		return(isHasCC);
	}
	
	/**
	 * 获得收件人
	 * @return
	 */
	public String[] getTo()
	{
		return(to);
	}
	
	/**
	 * 获得抄送人
	 * @return
	 */
	public String[] getCc()
	{
		return(cc);
	}
}
