package com.cwc.exception;

/**
 * 会员没有登录异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MemberNotLoginException extends Exception 
{
	public MemberNotLoginException() 
	{
		super();
	}
	
	public MemberNotLoginException(String inMessage)
	{
		super(inMessage);
	}
}
