package com.cwc.exception;

/**
 * 直接向请求输出信息向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class WriteOutResponseException extends Exception 
{
	public WriteOutResponseException() 
	{
		super();
	}
	
	public WriteOutResponseException(String inMessage)
	{
		super(inMessage);
	}
}
