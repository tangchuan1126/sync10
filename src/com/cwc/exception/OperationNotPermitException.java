package com.cwc.exception;

/**
 * 没有操作权限异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class OperationNotPermitException extends Exception   
{
	private static final long serialVersionUID = 1L;

	public OperationNotPermitException()
	{
		super();
	}
	
	public OperationNotPermitException(String msg) 
	{
		super(msg);
	}
	
	public OperationNotPermitException(String msg, Throwable cause) 
	{
		super(msg, cause);
	}
	
	public OperationNotPermitException(Throwable cause) 
	{
		super(cause);
	}
	
}
