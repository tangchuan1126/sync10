package com.cwc.exception;

import org.apache.log4j.Logger;

/**
 * 该异常用来把堆栈信息记录到日志中
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SystemException extends Exception 
{
	public SystemException() 
	{
		super();
	}
	
	public SystemException(String inMessage)
	{
		super(inMessage);
	}
	
	public SystemException(Exception e,String action,Logger log)
	{
		super(e);
		
		//把堆栈信息记录到日志
		StackTraceElement[] ste = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		sb.append(e.getMessage() + "\r\n");
		for (int i = 0;i < ste.length;i++)
		{
			sb.append(ste[i].toString() + "\r\n");
		}
		
		if (action!=null&&!action.equals(""))
		{
			log.error(action);			
		}
		log.error(sb.toString());
	}
}
