package com.cwc.exception;

/**
 * 网络异常，Jms再次将消息放入队列
 * @author Administrator
 *
 */
public class ConnectErrorJmsException extends Exception 
{
	public ConnectErrorJmsException() 
	{
		super();
	}
	
	public ConnectErrorJmsException(String inMessage)
	{
		super(inMessage);
	}
	
	public ConnectErrorJmsException(Exception e)
	{
		super(e);
	}
}
