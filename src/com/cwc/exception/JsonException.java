package com.cwc.exception;

/**
 * 返回JSON异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class JsonException extends Exception 
{
	public JsonException(Object obj)
	{
		super(obj.toString());
	}
}
