package com.cwc.exception;

/**
 * 外部接口异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class OutsideInterfaceException extends Exception 
{
	public OutsideInterfaceException() 
	{
		super();
	}
	
	public OutsideInterfaceException(String inMessage)
	{
		super(inMessage);
	}
}
