package com.cwc.exception;

/**
 * 重定向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class RedirectException extends Exception 
{
	public RedirectException() 
	{
		super();
	}
	
	public RedirectException(String inMessage)
	{
		super(inMessage);
	}
}
