package com.cwc.exception;

/**
 * 没有找到页面异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PageNotFoundException extends Exception 
{
	public PageNotFoundException() 
	{
		super();
	}
	
	public PageNotFoundException(String inMessage)
	{
		super(inMessage);
	}
}
