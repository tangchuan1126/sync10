package com.cwc.exception;

/**
 * 重定向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ForwardException extends Exception 
{
	public ForwardException() 
	{
		super();
	}
	
	public ForwardException(String inMessage)
	{
		super(inMessage);
	}
}
