package com.cwc.exception;

/**
 * 发送邮件异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SendMailException extends Exception 
{
	public SendMailException() 
	{
		super();
	}
	
	public SendMailException(String inMessage)
	{
		super(inMessage);
	}
}
