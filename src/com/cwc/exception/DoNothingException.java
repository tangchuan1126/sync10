package com.cwc.exception;

/**
 * 返回JSON异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DoNothingException extends Exception 
{
	public DoNothingException(Object obj)
	{
		super(obj.toString());
	}
}
