package com.cwc.exception;

/**
 * 重定向到backurl异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class RedirectBackUrlException extends Exception 
{
	public RedirectBackUrlException() 
	{
		super();
	}
	
	public RedirectBackUrlException(String inMessage)
	{
		super(inMessage);
	}
}
