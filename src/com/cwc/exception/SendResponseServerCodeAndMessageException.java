package com.cwc.exception;

/**
 * 直接向浏览器发送服务器代码
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class SendResponseServerCodeAndMessageException extends Exception 
{
	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public SendResponseServerCodeAndMessageException() 
	{
		super();
	}
	
	public SendResponseServerCodeAndMessageException(int ServerErrorCode,String msg)
	{
		super();
		
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
}
