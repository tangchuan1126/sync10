package com.cwc.exception;

/**
 * 重定向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class Forward2JspException extends Exception 
{
	public Forward2JspException() 
	{
		super();
	}
	
	public Forward2JspException(String inMessage)
	{
		super(inMessage);
	}
}
