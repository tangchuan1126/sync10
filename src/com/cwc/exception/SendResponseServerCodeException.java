package com.cwc.exception;

/**
 * 直接向浏览器发送服务器代码
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class SendResponseServerCodeException extends Exception 
{
	private int ServerErrorCode = 0;
	
	public SendResponseServerCodeException() 
	{
		super();
	}
	
	public SendResponseServerCodeException(int ServerErrorCode)
	{
		super();
		
		this.ServerErrorCode = ServerErrorCode;
	}
	
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
}
