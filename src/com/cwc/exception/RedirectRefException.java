package com.cwc.exception;

/**
 * 重定向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class RedirectRefException extends Exception 
{
	public RedirectRefException() 
	{
		super();
	}
	
	public RedirectRefException(String inMessage)
	{
		super(inMessage);
	}
}
