package com.cwc.exception;

/**
 * 当前访问页面，匹配正则表达式配置权限
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class PageMatchRegException extends Exception 
{
	public PageMatchRegException() 
	{
		super();
	}
	
	public PageMatchRegException(String inMessage)
	{
		super(inMessage);
	}
}
