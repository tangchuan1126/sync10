package com.cwc.cache;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MemoryCache
{
	private static HashMap cache;
	public static int WEAK = 1;
	public static int SOFT = 2;
	public static int STRONG = 3;
	
	static
	{
		cache = new HashMap();
	}

	public static void putObject(int model, Object key, Object value) 
	{
		Object reference = null;
		
		if ( model==WEAK )
		{
			reference = new WeakReference(value);
		}
		else if ( model==SOFT ) 
		{
			reference = new SoftReference(value);
		}
		else if ( model==STRONG ) 
		{
			reference = new StrongReference(value);
		}
		cache.put(key, reference);
	}
	
	public static Object getObject(Object key) 
	{
		Object value = null;
	    Object ref = cache.get(key);
	    
	    if (ref != null) 
	    {
	    	if (ref instanceof StrongReference) 
	    	{
	    		value = ((StrongReference) ref).get();
	    	}
		    else if (ref instanceof SoftReference) 
		    {
		    	value = ((SoftReference) ref).get();
		    } 
		    else if (ref instanceof WeakReference) 
		    {
		    	value = ((WeakReference) ref).get();
		    }
	    }
	    
	    return(value);
	}

	public static Object removeObject(Object key) 
	{
		Object value = null;
	    Object ref = cache.remove(key);
	    
	    if (ref != null) 
	    {
	    	if (ref instanceof StrongReference) 
	    	{
	    		value = ((StrongReference) ref).get();
	    	}
		    else if (ref instanceof SoftReference) 
		    {
		    	value = ((SoftReference) ref).get();
		    } 
		    else if (ref instanceof WeakReference) 
		    {
		    	value = ((WeakReference) ref).get();
		    }
	    }
	    return(value);
	}

	public static void flush() 
	{
		cache.clear();
	}
	  
	private static class StrongReference 
	{
	  	private Object object;

		public StrongReference(Object object) 
		{
			this.object = object;
	    }

	    public Object get() 
	    {
	    	return(object);
	    }
	}
}















