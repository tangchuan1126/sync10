package com.cwc.cache;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * 处理表跟URI的映射(多对多关系，以表名作为主键)
 * 这类主要用在记录静态文件生成标记，因为我们在URL REWRITE配置里面，一个URI是可以绑定多个表的
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TableKey  implements Externalizable
{
	private static HashMap tableMap = new HashMap();
	
	static Logger log = Logger.getLogger("PLATFORM");
	
	/**
	 * 设置表跟URI
	 * @param key				表名
	 * @param val				URI集合
	 * @throws Exception
	 */
	private static void put(Object key,ArrayList val)
		throws Exception
	{
		if (key!=null&&val!=null)
		{
			tableMap.put(key,val);
		}
		else
		{
			throw new Exception("TableKey put("+key+","+val+") error!");
		}
	}
	
	/**
	 * 通过表名获得对应URI集合
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static Object get(Object key)
		throws Exception
	{
		if (key!=null)
		{
			return(tableMap.get(key));
		}
		else
		{
			throw new Exception("TableKey get("+key+") error!");
		}
	}
	
	/**
	 * 通过表明删除URI
	 * @param key
	 * @throws Exception
	 */
	public static void remove(Object key)
		throws Exception
	{
		if (key!=null)
		{
			tableMap.remove(key);
		}
		else
		{
			throw new Exception("TableKey remove("+key+") error!");
		}
	}
	
	/**
	 * 通过表名增加URI
	 * @param key
	 * @param val
	 * @throws Exception
	 */
	public static void putSubElement(Object key,Object val)
		throws Exception
	{
		ArrayList valAl;
		
		//先判断是否已经存在这个表名的记录
		if ( tableMap.containsKey(key) )
		{
			valAl = (ArrayList)tableMap.get(key);
		}
		else
		{
			valAl = new ArrayList();
		}
		
		//如果跟该表名相关的URI集合不包含新URI，则追加进去
		if ( !valAl.contains(val) )
		{
			valAl.add(val);
			put(key,valAl);	
		}
	}
	
	/**
	 * 通过表名获得URI集合
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static ArrayList getSubElement(Object key)
		throws Exception
	{
		if ( tableMap.containsKey(key) )
		{
			return((ArrayList)tableMap.get(key));
		}
		else
		{
			return(new ArrayList());
		}
	}
	
	/**
	 * 通过表名删除一个URI
	 * @param key
	 * @param val
	 * @throws Exception
	 */
	public static void removeSubElement(Object key,Object val)
		throws Exception
	{
		if ( tableMap.containsKey(key) )
		{
			ArrayList valAl = (ArrayList)tableMap.get(key);
			valAl.remove(val);
			put(key,valAl);
		}
	}

	public static void removeSubElement(Object val)
		throws Exception
	{
	}
	
	public static void main(String args[])
		throws Exception
	{
		TableKey.putSubElement("info","select * from info");
		TableKey.putSubElement("info","select name.gae from info");
		TableKey.putSubElement("info","select name.gae from info,content where a.id=b.id");
		TableKey.putSubElement("content","select * from info");
		TableKey.putSubElement("info","select * from info");

		//removeSubElement("info","select * from info");
		
		ArrayList al = TableKey.getSubElement("info");
		for (int i=0; i<al.size(); i++)
		{
			////system.out.println(al.get(i));
		}
	}
	
	 /**
	  * 自行处理反序列化
	  */
	public void readExternal(ObjectInput oi)
		throws IOException, ClassNotFoundException
	{
		try 
		{
			//除了还原自己状态，还要还原TableKey状态
			
			HashMap record = (HashMap) oi.readObject();
			
			tableMap = (HashMap)record.get("tableMap");
		} 
		catch (IOException e)
		{
			//log.error("LruCacheArithmetic readExternal error:"+e);
		}	
		catch (ClassNotFoundException e)
		{
			log.error("TableKey readExternal ClassNotFoundException:"+e);
		}	
	}

	/**
	 * 自行处理序列化
	 * 把所有静态对象先存到一个HASHMAP里面，然后序列化
	 */
	public void writeExternal(ObjectOutput oi)
		throws IOException
	{
		try
		{
			HashMap record = new HashMap();
			
			record.put("tableMap", tableMap);	
			
			oi.writeObject(record);

		}
		catch (Exception e) 
		{
			log.error("TableKey writeExternal error:"+e);
		}		
	}
}







