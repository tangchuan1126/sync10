package com.cwc.cache;

import com.opensymphony.oscache.general.GeneralCacheAdministrator;

/**
 * 使用OSCACHE作为数据库内存缓存
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class OscacheArithmetic implements CacheArithmetic
{
	private GeneralCacheAdministrator admin;
	
	/**
	 * 增加SQL-数据
	 * @param table			表名
	 * @param key			SQL
	 * @param value			结果集
	 * @throws Exception
	 */
	public void putObject(String[] table, Object key, Object value)
		throws Exception 
	{
		admin.putInCache((String)key,value,table);
	}

	/**
	 * 通过SQL获得数据集
	 * @param key		SQL
	 * @return
	 * @throws Exception
	 */
	public Object getObject(Object key) 
		throws Exception 
	{
		try 
		{
			return(admin.getFromCache((String)key));
		}
		catch (Exception e) 
		{
			admin.cancelUpdate((String)key);
			return(null);
		}
	}

	/**
	 * 删除缓存
	 * @param key			SQL
	 * @return
	 * @throws Exception
	 */
	public Object removeObject(Object key)
		throws Exception 
	{
		return(null);
	}

	/**
	 * 用表明清空相关SQL键的数据集缓存
	 * @param table
	 * @throws Exception
	 */
	public void flush(String[] table)
		throws Exception
	{
		for (int i=0; i<table.length; i++)
		{
			admin.flushGroup(table[i]);
		}
	}

	/**
	 * 销毁缓存
	 */
	public void flushAll()
		throws Exception
	{
		admin.flushAll();
	}

	
	
	
	
	public void setAdmin(GeneralCacheAdministrator admin)
	{
		this.admin = admin;
	}

}
