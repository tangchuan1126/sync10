package com.cwc.cache;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.cwc.app.key.LocalKey;
import com.cwc.util.FileUtil;

public class _l1l1l1l1l1l1l1l1l extends LruCacheArithmetic
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	/**
	 * 生成静态文件
	 * @param realpath			文存放路径
	 * @param uri				当前访问URIL
	 * @param sysFolder			系统文件夹
	 * @param content			文件内容
	 */
	public String l1l1l1ll1l1l1l1(String realpath,String uri,String sysFolder,String content)
	{
		try 
		{
			String l1l1l1l1l1l1l1l = l1l1l11l11l1l1l1l1l1(sysFolder,uri);		//文件名
			String l1l1ll1l1ll1l1l1 = l1l1l11l11l1l1l1l1(sysFolder,uri);		//文件夹
			
			if (!l1l1ll1l1ll1l1l1.equals(""))
			{
				l1l1ll1l1ll1l1l1 += "/";
			}
			//log.info("start create html"+realpath+l1l1ll1l1ll1l1l1+l1l1l1l1l1l1l1l);
        	FileUtil.createFile(realpath+l1l1ll1l1ll1l1l1+l1l1l1l1l1l1l1l,"UTF-8",content);	//生成静态文件
        	//log.info("end create html");
        	
        	return(l1l1l1l1l1l1l1l);
		}
		catch (Exception e) 
		{
			log.error("HtmlCache.l1l1l1ll1l1l1l1 error:"+ e);
			return(null);
		}
	}

	/**
	 * 创建存放静态文件的文件夹
	 * @param realpath			文存放路径
	 * @param uri				当前访问URIL
	 * @param sysFolder			系统文件夹
	 */
	public String l1l1lll11l1l1l1l1(String realpath,String uri,String sysFolder)
	{
		try 
		{
			String l1l1l1l1l1l1l1l = l1l1l11l11l1l1l1l1(sysFolder,uri);	//文件夹
			
	       	if (!l1l1l1l1l1l1l1l.equals(""))
	       	{
		       	File file = new File(realpath+l1l1l1l1l1l1l1l);
		       	if (!file.exists())
		       	{
		       		file.mkdir();
		       	}
	       	}
	       	
	       	return(l1l1l1l1l1l1l1l);
		}
		catch (Exception e) 
		{
			log.error("HtmlCache.l1l1lll11l1l1l1l1 error:"+ e);
			return(null);
		}
	}

	/**
	 * 从URI中分析出文件夹
	 * @param sysFolder			系统文件夹
	 * @param uri				当前访问URIL
	 * @return					文件夹名称
	 */
	public String l1l1l11l11l1l1l1l1(String sysFolder,String uri)
	{
		String folder;
		sysFolder = sysFolder.substring(0,sysFolder.length()-1);

		//用正则表达式提取出当前URI中的文件夹
		String reg = sysFolder+"((/(\\S+))*/(\\S+))*/(\\S+\\.html)";
		Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher (uri);        
        if (m.find ())
        {
        	folder = m.group(4);
        	if (folder!=null)
        	{
    			//根据当前语言地区追加后序
        		return(LocalKey.getLocalKey(folder+"_shadow"));
        	}
        }

        return("");
	}

	/**
	 * 从URI中分析出文件名
	 * @param sysFolder			系统文件夹
	 * @param uri				当前访问URIL
	 * @return					文件名
	 */
	public String l1l1l11l11l1l1l1l1l1(String sysFolder,String uri)
	{
		String filename;
		String orgSysFolder = sysFolder;
		sysFolder = sysFolder.substring(0,sysFolder.length()-1);
		
		String folder = l1l1l11l11l1l1l1l1(orgSysFolder,uri);
		
		//用正则表达式提取出当前URI中的文件名
		String reg = sysFolder+"((/(\\S+))*/(\\S+))*/(.+=)*(\\S+\\.html)";
		Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher (uri);        
        if (m.find ())
        {
        	filename = m.group(6);
        	if (filename!=null)
        	{
        		//如果uri没有文件夹，则在文件名后追加地区语言标识
        		if (folder.equals(""))
        		{
            		return(LocalKey.getLocalKey(filename.substring(0,filename.indexOf(".")))+".html");
        		}
        		else
        		{
            		return(filename);        			
        		}
        	}
        }

        return("");
	}
	
	public static void main(String args[])
	{
	}
}
