package com.cwc.cache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

import com.cwc.app.util.ThreadContext;
import com.opensymphony.oscache.base.Cache;
import com.opensymphony.oscache.web.ServletCacheAdministrator;

/**
 * 内存缓存数据操作类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CacheMgr 
{
	static Logger log = Logger.getLogger("PLATFORM");
	private CacheArithmetic cache;
	

	/**
	 * 设置缓存
	 * @param table		表名
	 * @param key		SQL
	 * @param value		数据集
	 * @throws Exception
	 */
	public void putObject(String table[],Object key, Object value)
		throws Exception
	{
		try
		{
			//log.info("putObject("+table+","+key+","+value+")");
			//synchronized(key)
			//{
				cache.putObject(table,key,value);
			//}
		} 
		catch (Exception e) 
		{
			//在高并发的时候，oscache会有时出现一个小问题，可以把该异常忽略
			//throw new Exception("CacheMgr putObject error:"+e);
		}
	}

	/**
	 * 获得缓存数据集
	 * @param key			SQL
	 * @return
	 * @throws Exception
	 */
	public Object getObject(Object key)
		throws Exception
	{
		try
		{
			//log.info("getObject("+key+")");
			return(cache.getObject(key));
		} 
		catch (Exception e)
		{
			throw new Exception("CacheMgr getObject error:"+e);
		}
	}

	/**
	 * 删除内存缓存数据集
	 * @param key			SQL
	 * @return
	 * @throws Exception
	 */
	public Object removeObject(Object key)
		throws Exception
	{
		try 
		{
			return(cache.removeObject(key));
		} 
		catch (Exception e)
		{
			throw new Exception("CacheMgr removeObject error:"+e);
		}
	}

	/**
	 * 清空所有与表名相关的数据集缓存
	 * @param table
	 * @throws Exception
	 */
	public void flush(String table[]) 
		throws Exception
	{
		try
		{
			cache.flush(table);			//清空数据集内存缓存
			(new _l1l1l1l1l1l1l1l1l()).flush(table);		//清空静态文件生成标记
			
//			//system.out.print("flush：");
//			for (int i=0; i<table.length; i++)
//			{
//				//system.out.print(table[i]+" ");
//			}
//			//system.out.println("");
		}
		catch (Exception e) 
		{
			throw new Exception("CacheMgr flush error:"+e);
		}
	}

	/**
	 * 销毁内存缓存
	 * @throws Exception
	 */
	public void flushAll() 
		throws Exception
	{
		try 
		{
			cache.flushAll();
		}
		catch (Exception e) 
		{
			throw new Exception("CacheMgr flushAll error:"+e);
		}
	}
	
    public static void flushCacheGroup(String groups[]) 
    {
    	HttpServletRequest request = (HttpServletRequest)ThreadContext.get("request");
    	
    	Cache cache = ServletCacheAdministrator.getInstance(request.getSession().getServletContext()).getCache(request, PageContext.APPLICATION_SCOPE); 

    	for (int i=0; i<groups.length; i++)
    	{
            cache.flushGroup(groups[i]);
        }
    }

    
    
    
	public void setCache(CacheArithmetic cache)
	{
		this.cache = cache;
	}
	
}








