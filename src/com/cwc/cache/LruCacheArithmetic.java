package com.cwc.cache;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.HashMap;




import org.apache.log4j.Logger;




import com.cwc.app.util.ConfigBean;

/**
 * 记录HTML静态缓存标记
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class LruCacheArithmetic implements CacheArithmetic,Externalizable
{
	  private static int cacheSize;
	  private static HashMap cache;
	  private static ArrayList keyList;	//排序主线,控制算法先进，先出
	  
	  static Logger log = Logger.getLogger("PLATFORM");
	  
	  /**
	   * 取消使用同步方式，以免造成性能瓶颈
	   */
	  static
	  {
		  cacheSize = ConfigBean.getIntValue("htmlcache_size");
		  cache = new HashMap();
		  keyList = new ArrayList();
	  }

	  /**
	   * 根据表名，URI，记录静态缓存页面生成
	   * @param table			URI绑定的表名
	   * @param key				URI
	   * @param value			1
	   * @throws Exception
	   */
	  public void putObject(String table[],Object key, Object value)
	  	throws Exception
	  {
		  synchronized(key)
		  {
			  //判断该URI是否已经记录
			  if (!isContainKey(key))
			  {
				    cache.put(key, value);
				    keyList.add(key);
				    
				    //检查缓存池大小似乎否超标
				    if (keyList.size() > cacheSize) 
				    {
				    	try 
						{
				    		//超标了，把最先进入的KEY删除
				    		Object oldestKey = keyList.remove(0);
				    		cache.remove(oldestKey);
						}
				    	catch (IndexOutOfBoundsException e) 
						{
						}
				    }

				    try
					{
				    	//把该URI跟表名绑定
						for (int i=0; i<table.length; i++)
						{
							TableKey.putSubElement(table[i],key);
						}
					} 
				    catch (Exception e) 
					{
				    	throw new Exception("LruCacheArithmetic putObject putSubElement error!");
					}  
			  }
		  }
	  }

	  /**
	   * 根据URI获得缓存标记
	   * @param key				URI
	   * @return
	   */
	  public Object getObject(Object key)
	  {
		  synchronized(key)
		  {
			    Object result = cache.get(key);
			    
			    //这里很简单的通过ARRAYLIST实现了先进先出的算法
			    keyList.remove(key);
			    if (result != null) 
			    {
			      keyList.add(key);
			    }
			    
			    return result;
		  }	   
	  }

	  /**
	   * 删除KEY
	   * @param key
	   * @return
	   * @throws Exception
	   */
	  public Object removeObject(Object key)
	  	throws Exception
	  {
		    keyList.remove(key);
		    return cache.remove(key);
	  }

	  /**
	   * 删除跟表名绑定的所有URI缓存标记
	   * @param table
	   * @throws Exception
	   */
	  public void flush(String table[]) 
	  	throws Exception
	  {
	  		ArrayList al;
			for (int i=0; i<table.length; i++)
			{
				//得到与该表名绑定的所有URI
				al = TableKey.getSubElement(table[i]);
				
				for (int j=0; j<al.size(); j++)
				{
					removeObject(al.get(j));
				}
				
				TableKey.remove(table[i]);
			}
	  }

	  /**
	   * 销毁缓存
	   */
	  public void flushAll() 
	  	throws Exception
	  {
		    cache.clear();
		    keyList.clear();
	  }
	  
	  /**
	   * 判断该URI缓存是否存在
	   * @param key
	   * @return
	   */
	  public boolean isContainKey(Object key)
	  {
//		  //system.out.println("-- start keyList -----");
//		  for (int i=0; i<keyList.size(); i++)
//		  {
//			  //system.out.println(keyList.get(i));
//		  }
//		  //system.out.println("-- end keyList -----");
//		  
//		  //system.out.println("-- start cache -----");
//          Iterator iterator = cache.keySet().iterator();            
//          while (iterator.hasNext()) {
//           //system.out.println(iterator.next());
//          }
//		  //system.out.println("-- end cache -----");
//		  
//		  //system.out.println("");
//		  //system.out.println("");
		  
		  return(cache.containsKey(key));
	  }

	  public static void main(String args[])
	  	throws Exception
	  {

	  }

	 /**
	  * 自行处理反序列化
	  */
	public void readExternal(ObjectInput oi)
		throws IOException, ClassNotFoundException
	{
		try 
		{
			HashMap record = (HashMap) oi.readObject();
			
			cacheSize = Integer.parseInt(record.get("cacheSize").toString());
			cache = (HashMap) record.get("cache");
			keyList = (ArrayList) record.get("keyList");

		} 
		catch (IOException e)
		{
			//log.error("LruCacheArithmetic readExternal error:"+e);
		}	
		catch (ClassNotFoundException e)
		{
			log.error("LruCacheArithmetic readExternal ClassNotFoundException:"+e);
		}	
	}

	/**
	 * 自行处理序列化
	 * 把所有静态对象先存到一个HASHMAP里面，然后序列化
	 */
	public void writeExternal(ObjectOutput oi)
		throws IOException
	{
		try
		{
			HashMap record = new HashMap();
			
			record.put("cacheSize", cacheSize);
			record.put("cache", cache);
			record.put("keyList", keyList);
			
			oi.writeObject(record);
		}
		catch (Exception e) 
		{
			log.error("LruCacheArithmetic writeExternal error:"+e);
		}		
	}

}






