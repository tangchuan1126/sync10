package com.cwc.cache;

/**
 * 缓存策略需要实现的接口
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface CacheArithmetic 
{
	  public void putObject(String table[],Object key, Object value) throws Exception;

	  public Object getObject(Object key) throws Exception;

	  public Object removeObject(Object key) throws Exception;

	  public void flush(String table[]) throws Exception;
	  
	  public void flushAll() throws Exception;
}
