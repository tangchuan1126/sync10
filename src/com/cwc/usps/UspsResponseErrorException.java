package com.cwc.usps;

/**
 * usps打印服务器返回的错误
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class UspsResponseErrorException extends Exception 
{
	public UspsResponseErrorException() 
	{
		super();
	}
	
	public UspsResponseErrorException(String inMessage)
	{
		super(inMessage);
	}
}
