package com.cwc.usps;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.*;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.cwc.app.util.Environment;
import com.cwc.exception.SystemException;
import com.cwc.http.Client;
import com.sun.media.jai.codec.*;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class UspsClient  
{
	static Logger log = Logger.getLogger("ACTION");
	
	private String imgPath = Environment.getHome()+"administrator/order/UspsPrintLabel";
	private String server = "https://Secure.ShippingAPIs.com/ShippingAPI.dll";
	private String waybillNo;
	
	//收发件人信息
	private String fromName;
	private String fromAddress2;
	private String fromAddress1;
	private String fromCity;
	private String fromState;
	private String fromZip5;
	
	private String toName;
	private String toAddress2;
	private String toCity;
	private String toState;
	private String toZip5;
	private String weightOunces;
	
	public String getWaybillNo()
	{
		return waybillNo;
	}

	/**
	 * 通过XML字符串获得XML doc
	 * @param xmlStr
	 * @return
	 * @throws Exception
	 */
	public Document getXMLDoc(String xmlStr)
		throws Exception
	{
		 SAXReader saxReader = new SAXReader();
		 Document document = saxReader.read(new StringReader(xmlStr));  
		
		return(document);
	}
	
	/**
	 * 从XML DOC中获节点数据
	 * @param doc
	 * @param path
	 * @param name
	 * @return
	 */
	public String getValueByXMLPathName(Document doc,String path,String name)
	{
		Element element = (org.dom4j.Element)doc.selectSingleNode(path);
		Iterator iter=element.elementIterator();
	    while(iter.hasNext())
	    {
	    	Element subelement = (Element)iter.next();
	    	
	    	if (subelement.getName().equals(name))
	    	{
	    		return(subelement.getText());
	    	}
	    }
	    
		return(null);
	}
	
	/**
	 * 生成Usps打印Label图片
	 * @param base64Str
	 * @param name
	 * @throws Exception
	 */
	public void createPrintLabelBase64Img(String base64Str,String name)
		throws Exception
	{
		try
		{
			Base64 base64 =new Base64();
			
			
//			BASE64Decoder decoder = new BASE64Decoder();   
			 
			// Base64解码   
			byte[] buffer = base64.decode(base64Str);//decoder.decodeBuffer(base64Str);   
			for (int i = 0; i < buffer.length; ++i) 
			{   
				if (buffer[i] < 0)  // 调整异常数据   
				{
					buffer[i] += 256;   
				}
			}  
			
			File file = new File(imgPath+"/"+name+".tif");
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
			bos.write(buffer);
			bos.close();
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"createPrintLabelBase64Img",log);
		}
	}

	/**
	 * 把TIF图片转换成PNG
	 * @param tif
	 * @param png
	 * @throws Exception
	 */
	public void convert2PNG(String tif,String png)
		throws Exception
	{
		try
		{
			FileOutputStream out = new FileOutputStream(png);
			File file = new File(tif);
			ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",file,null);
			ImageEncoder encoder = ImageCodec.createImageEncoder("png",out,null);
			encoder.encode( decoder.decodeAsRenderedImage() );
			decoder.getInputStream().close();//不关闭，源文件删不掉
			encoder.getOutputStream().close();
			file.delete();
			out.close();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"convert2PNG",log);
		}
	}
	
	/**
	 * 剪裁图片
	 * @param src
	 * @param out
	 * @param sx
	 * @param sy
	 * @param width
	 * @param height
	 * @throws Exception
	 */
	public void cropImage(String src,String out,int   sx,   int   sy,   int   width,   int   height)
		throws Exception
    {
		try
		{
	        //1.获得原始图象Image对象
	        File   file=new   File(src);
	        BufferedImage baseImage   =ImageIO.read(file);
	        
            ImageFilter   filter   =   new   CropImageFilter(sx,   sy,   width,   height);//根据图像裁剪过滤器产生过滤器
            //下面根据过滤器产生图像生产者
            ImageProducer   producer   =   new   FilteredImageSource(baseImage.getSource(),   filter);
            Image img   =   Toolkit.getDefaultToolkit().createImage(producer);//根据图像生产者产生新图像
            
            //3.生成图片文件
            BufferedImage tag   =   new   BufferedImage(width,   height,   BufferedImage.TYPE_INT_RGB);
            tag.getGraphics().drawImage(img,   0,   0,   width,   height,   null);
            
            ImageIO.write(tag, "png", new File(out));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"cropImage",log);
		}   
    } 
	
	/**
	 * 缩放图片
	 * @param srcImageFile
	 * @param result
	 * @param out_width
	 * @throws Exception
	 */
	 public void scale(String srcImageFile, String result, int out_width) 
	 	throws Exception
	 {
		  try
		  {
			   BufferedImage src = ImageIO.read(new File(srcImageFile)); // 读入文件
			   float width = src.getWidth(); // 得到源图宽
			   float height = src.getHeight(); // 得到源图长
			   
			   int out_height = (int)((out_width/width)*height);
			   
			   Image image = src.getScaledInstance(out_width, out_height, Image.SCALE_DEFAULT);
		
			   BufferedImage tag = new BufferedImage(out_width, out_height,BufferedImage.TYPE_INT_RGB);
			   Graphics g = tag.getGraphics();
			   g.drawImage(image, 0, 0, null); // 绘制缩小后的图
			   g.dispose();
			   ImageIO.write(tag, "png", new File(result));// 输出到文件流
		  }
		  catch (IOException e)
		  {
			  throw new SystemException(e,"scale",log);
		  }
	 }
	
	/**
	 * 提交数据给USPS并生成打印LABEL
	 * @throws Exception
	 */
	public void commit()
		throws UspsResponseErrorException,Exception
	{
		try
		{
			String para = "API=DeliveryConfirmationV3&XML=<DeliveryConfirmationV3.0Request USERID='324VISIO2573'>";
			para += "<Option>1</Option>";
			para += "<ImageParameters />";
			para += "<FromName>"+this.fromName+"</FromName>";
			para += "<FromFirm />";
			para += "<FromAddress1>"+this.fromAddress1+"</FromAddress1>";
			para += "<FromAddress2>"+this.fromAddress2+"</FromAddress2>";
			para += "<FromCity>"+this.fromCity+"</FromCity>";
			para += "<FromState>"+this.fromState+"</FromState>";
			para += "<FromZip5>"+this.fromZip5+"</FromZip5>";
			para += "<FromZip4 />";
			
			para += "<ToName>"+this.toName+"</ToName>";
			para += "<ToFirm />";
			para += "<ToAddress1 />";
			para += "<ToAddress2>"+this.toAddress2+"</ToAddress2>";
			para += "<ToCity>"+this.toCity+"</ToCity>";
			para += "<ToState>"+this.toState+"</ToState>";
			para += "<ToZip5>"+this.toZip5+"</ToZip5>";
			para += "<ToZip4 />";
			para += "<WeightInOunces>"+this.weightOunces+"</WeightInOunces>";
			para += "<ServiceType>Priority</ServiceType>";
			para += "<POZipCode />";
			para += "<ImageType>TIF</ImageType>";
			para += "<LabelDate />";
			para += "</DeliveryConfirmationV3.0Request>";
			
			//
			
				//log.info(para);
			
			String responseXmlStr = Client.httpPost(server, para,10000);
			if (responseXmlStr.toLowerCase().indexOf("error")>=0)
			{
				Document doc = getXMLDoc(responseXmlStr);
				String description = getValueByXMLPathName(doc, "//Error", "Description");

				throw new UspsResponseErrorException(description);
			}

			//log.info(responseXmlStr);

			Document doc = getXMLDoc(responseXmlStr);
			String base64Img = getValueByXMLPathName(doc, "//DeliveryConfirmationV3.0Response", "DeliveryConfirmationLabel");
			String waybillNo = getValueByXMLPathName(doc, "//DeliveryConfirmationV3.0Response", "DeliveryConfirmationNumber");

			//long st = System.currentTimeMillis();
			
			createPrintLabelBase64Img(base64Img,waybillNo);//把XML转换成TIF图片
			convert2PNG(imgPath+"/"+waybillNo+".tif",imgPath+"/"+waybillNo+".png");//把TIF转换成PNG
			cropImage(imgPath+"/"+waybillNo+".png",imgPath+"/"+waybillNo+".png",130,   100,   1220, 830);//剪裁图片
			//scale(imgPath+"/"+waybillNo+".png", imgPath+"/"+waybillNo+".png", 800);//缩放图片
			
			//long en = System.currentTimeMillis();
			////system.out.println(en-st);
			this.waybillNo = waybillNo;
		} 
		catch (UspsResponseErrorException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	
	

	public void setFromName(String fromName)
	{
		this.fromName = fromName;
	}

	public void setFromAddress2(String fromAddress2)
	{
		this.fromAddress2 = fromAddress2;
	}

	public void setFromCity(String fromCity)
	{
		this.fromCity = fromCity;
	}

	public void setFromState(String fromState)
	{
		this.fromState = fromState;
	}

	public void setFromZip5(String fromZip5)
	{
		this.fromZip5 = fromZip5;
	}

	public void setToName(String toName)
	{
		this.toName = toName;
	}

	public void setToAddress2(String toAddress2)
	{
		this.toAddress2 = toAddress2;
	}

	public void setToCity(String toCity)
	{
		this.toCity = toCity;
	}

	public void setToState(String toState)
	{
		this.toState = toState;
	}

	public void setToZip5(String toZip5)
	{
		this.toZip5 = toZip5;
	}

	public void setWeightOunces(String weightOunces)
	{
		this.weightOunces = weightOunces;
	}
	
	public void setFromAddress1(String fromAddress1)
	{
		this.fromAddress1 = fromAddress1;
	}


	public static void main(String args[]) throws Exception
	{
		String server = "http://production.ShippingAPIs.com/ShippingAPITest.dll";
		String para = "API=Verify&XML=<AddressValidateRequest USERID='324VISIO2573'><Address ID='0'><Address1></Address1><Address2>263 Shuman Blvd</Address2><City>Naperville</City><State>IL</State><Zip5></Zip5><Zip4></Zip4></Address></AddressValidateRequest>";
		
		String responseXmlStr = Client.httpPost(server, para,10000);
		
		//system.out.println(responseXmlStr);
	}

	
}








