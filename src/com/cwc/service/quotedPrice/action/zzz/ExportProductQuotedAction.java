package com.cwc.service.quotedPrice.action.zzz;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.QuotePriceMgrIfaceZZZ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ExportProductQuotedAction extends ActionFatherController {

	private QuotePriceMgrIfaceZZZ quotePriceMgrZZZ;
	
	public void setQuotePriceMgrZZZ(QuotePriceMgrIfaceZZZ quotePriceMgrZZZ) {
		this.quotePriceMgrZZZ = quotePriceMgrZZZ;
	}

	@Override
	public void perform(HttpServletRequest arg0, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException,DoNothingException, Exception 
	{
		String filedownload = quotePriceMgrZZZ.createQuotedPDF(arg0);
		if(filedownload!=null)
		{
			quotePriceMgrZZZ.downLoadPDF(response, filedownload);
		}
		throw new DoNothingException("");
	}

}
