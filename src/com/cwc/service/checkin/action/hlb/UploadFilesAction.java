package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UploadFilesAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private CheckInMgrIFaceZyj checkInMgrZyj;

	private long windowCheckInMainId;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String mainId = StringUtil.getString(request, "dlo_id"); // 添加或修改的子单据数据
	    windowCheckInMainId = Long.parseLong(mainId);
		//获得创建人
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
	    long createManId = adminLoginBean.getAdid();
	    checkInMgrZwb.addFiles(request, windowCheckInMainId, createManId);
		if(!StringUtil.isBlankAndCanParseLong(mainId))
		{
			checkInMgrZyj.windowCheckInUpdateMainTimeEquipment(Long.parseLong(mainId), createManId);
		}
	}
	

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}

}
