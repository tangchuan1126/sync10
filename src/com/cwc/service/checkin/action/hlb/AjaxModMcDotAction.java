package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxModMcDotAction extends ActionFatherController {
	private CheckInMgrIfaceHlb checkInMgrHlb;
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String companyName = StringUtil.getString(request, "carrierName");
		String mc_dot =  StringUtil.getString(request, "mc_dot");
		String phone_carrier = StringUtil.convertStr2PhotoNumber(companyName);
		DBRow[] rows = checkInMgrHlb.findCarrierInfoByCompanyName(companyName);
		if (rows.length == 1) {
			rows[0].add("mc_dot", mc_dot);
			rows[0].add("phone_carrier", phone_carrier);
			long count = checkInMgrHlb.ModMcdotByCompanyName(companyName, rows[0]);
			if (count > 0) {
				rows[0].add("result", "success");
				throw new JsonException(new JsonObject(rows[0]));
			}
		}
	}
}
