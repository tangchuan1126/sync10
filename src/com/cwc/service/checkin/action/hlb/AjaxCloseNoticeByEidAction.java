package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * <p>ClassName：AjaxCloseNoticeByEidAction</p>
 * <p>Description：关闭主单据的所有 window schedule</p>
 * @author huanglianbin
 * @version 1.0 V
 * <p>createTime: 2015 年 2 月 27 日</p>
 */
public class AjaxCloseNoticeByEidAction extends ActionFatherController {
	private CheckInMgrIfaceHlb checkInMgrHlb;
	
	public void setCheckInMgrHlb( CheckInMgrIfaceHlb checkInMgrHlb ) {
		this.checkInMgrHlb = checkInMgrHlb;
	}

	/**
	 * MethodName： perform
	 * Description：通过 entryId 关闭主单据的所有 window schedule
	 * @author: huanglianbin
	 * @param request
	 * @param response
	 * @throws WriteOutResponseException
	 * @throws RedirectRefException
	 * @throws ForwardException
	 * @throws Forward2JspException
	 * @throws RedirectBackUrlException
	 * @throws RedirectException
	 * @throws OperationNotPermitException
	 * @throws PageNotFoundException
	 * @throws DoNothingException
	 * @throws Exception 
	 * @see com.cwc.action.core.ActionFatherController#perform( javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse )
	 */
	@Override
	public void perform( HttpServletRequest request, HttpServletResponse response )
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow row = checkInMgrHlb.CloseNoticeByEid( request );
		throw new JsonException( new JsonObject( row ) );
	}

}
