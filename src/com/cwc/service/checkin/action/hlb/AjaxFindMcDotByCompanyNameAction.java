package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxFindMcDotByCompanyNameAction extends ActionFatherController {
	private CheckInMgrIfaceHlb checkInMgrHlb;
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String carrierName = StringUtil.getString(request,"carrierName");
		String mc_dot =  StringUtil.getString(request, "mc_dot");
		DBRow[] rows = checkInMgrHlb.findMcDotByCompanyName(carrierName);
		DBRow[] rows2 = checkInMgrHlb.findCompanyNameByMcDot(mc_dot);
		DBRow row= new DBRow();
		if(rows.length == 0) {
			if (rows2.length == 0) {
				row.add("result", "new");
			}
		} else if(rows.length == 1) {
			// 如果 rows2.length 也等于 1，则表示通过客户端提供的 mc_dot 也可查询到一条记录
			if (rows2.length == 1) {
				row.add("result", "allMatch");
			} else {
				row = rows[0];
				row.add("result", "success");				
			}

		} else {
			row.add("result", "error");
		}
		throw new JsonException(new JsonObject(row));
		
	}
}
