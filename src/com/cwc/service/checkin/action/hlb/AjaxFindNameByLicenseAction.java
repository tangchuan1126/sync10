package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxFindNameByLicenseAction extends ActionFatherController {
	private CheckInMgrIfaceHlb checkInMgrHlb;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String licenseNo = StringUtil.getString(request, "licenseNo");
		DBRow[] rows = checkInMgrHlb.findNameByLicense(licenseNo);
		// 如果返回的驾驶证信息只有一条，将根据驾驶证查询到的驾驶员信息返回到页面
		if (rows.length == 1) {
			throw new JsonException(new JsonObject(rows[0]));
		}
		
	}
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}
}
