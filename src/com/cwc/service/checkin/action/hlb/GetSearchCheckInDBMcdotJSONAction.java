package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSearchCheckInDBMcdotJSONAction extends ActionFatherController {
	static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private CheckInMgrIfaceHlb checkInMgrHlb;
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		javapsLog.info("request: " + request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
				StringUtil.getString(request, "q") : 
				StringUtil.getString(request, "term");
		javapsLog.info("q=" + q);
		try {
			DBRow bills[] =  checkInMgrHlb.getSearchCheckInDBMcdotJSON(q);
			
			throw new JsonException(new JsonObject(bills));
		} catch (JsonException e) {
			throw e;
		} catch (Exception e) {
			// 加入搜索出错，返回空值
			throw new JsonException(new JsonObject(null));
		}
	}

}
