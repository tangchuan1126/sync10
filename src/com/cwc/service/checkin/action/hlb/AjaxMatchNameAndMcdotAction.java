package com.cwc.service.checkin.action.hlb;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxMatchNameAndMcdotAction extends ActionFatherController{
	private CheckInMgrIfaceHlb checkInMgrHlb;
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// csm：carrier_scac_mcdot 的简称
		// entry 主表：door_or_location_occupancy_main 的简称
		
		String carrierName = StringUtil.getString(request, "carrierName");
		String mc_dot =  StringUtil.getString(request, "mc_dot");
		// 同时用 carrierName 和 mc_dot 去 csm 进行查询
		DBRow rows_all = checkInMgrHlb.matchNameAndMcdot(carrierName, mc_dot);
		// 用 carrierName 去 csm 查询  mc_dot
		DBRow[] rows_carrier = checkInMgrHlb.findMcDotByCompanyName(carrierName);
		// 用 mc_dot 去 csm 查询 carrierName
		DBRow[] rows_mc_dot = checkInMgrHlb.findCompanyNameByMcDot(mc_dot);
		
		DBRow row = new DBRow();
		
		row = checkInMgrHlb.getMatchResult(rows_all, rows_mc_dot, rows_carrier);

			
		throw new JsonException(new JsonObject(row));		
	}		
}
