package com.cwc.service.checkin.action.hlb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.checkin.ResourceHasUsedException;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GateCheckInUpdateAction extends ActionFatherController{
	
	private CheckInMgrIfaceHlb checkInMgrHlb;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long main_id = StringUtil.getLong(request, "main_id");
		DBRow row = checkInMgrZwb.selectMainByEntryId(main_id);
		row = checkInMgrHlb.gateCheckInUpdate(row, request);
		throw new JsonException(new JsonObject(row));
	}
}
