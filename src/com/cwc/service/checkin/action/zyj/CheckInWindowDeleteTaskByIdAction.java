package com.cwc.service.checkin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckinTaskNotFoundException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentHasLoadedOrdersException;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;

public class CheckInWindowDeleteTaskByIdAction  extends ActionFatherController{

	private CheckInMgrIFaceZyj checkInMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		try
		{
			checkInMgrZyj.webWindowCheckinDeleteTask(request);
			//.windowCheckInDeleteTask(request);
		}
		catch(EquipmentHadOutException e){
			throw new SendResponseServerCodeAndMessageException(50000,"[ Equipment has left.]");
		}catch (CheckInAllTasksClosedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ All tasks closed, cannt wait.]");
		} catch (EquipmentHasLoadedOrdersException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ The task has loaded.]");
		}catch (CheckinTaskNotFoundException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ The task has been deleted.]");
		} catch (Exception e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
	}

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}

}
