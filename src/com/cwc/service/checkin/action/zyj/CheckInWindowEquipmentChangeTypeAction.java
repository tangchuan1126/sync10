package com.cwc.service.checkin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;

public class CheckInWindowEquipmentChangeTypeAction  extends ActionFatherController{

	private CheckInMgrIFaceZyj checkInMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		try
		{
			checkInMgrZyj.webWindowCheckInChangeEquipmentType(request);
			//.windowCheckInChangeEquipmentType(request);
		}
		catch(EquipmentNotFindException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[Equipment not found.]");
		}catch (Exception e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
	}

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}

}
