package com.cwc.service.checkin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustHasTasksException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.CheckinTaskNotFoundException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.DoorNotFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentHasLoadedOrdersException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.exception.checkin.SpotNotFindException;
import com.cwc.app.exception.checkin.TaskNoAssignWarehouseSupervisorException;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;

public class CheckInWindowTasksAction  extends ActionFatherController{

	private CheckInMgrIFaceZyj checkInMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		try
		{
			checkInMgrZyj.webCheckInWindowAddOrUpdateTask(request);
			//.windowCheckInMain(request);
		}catch(EquipmentHadOutException e){
			throw new SendResponseServerCodeAndMessageException(50000,"[Equipment has left!]");
		}catch(TaskNoAssignWarehouseSupervisorException e){
		//	//system.out.println(1);
			throw new SendResponseServerCodeAndMessageException(50000,"[All tasks must add supervisor!]");
		}catch(DoorHasUsedException e1){
			throw new SendResponseServerCodeAndMessageException(50000,"[The door "+e1.getMessage()+" has been used.]");
		}catch (SpotHasUsedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[The spot "+e.getMessage()+" has been used.]");
		}catch (CheckInTaskCanntDeleteException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" cannt delete.]");
		}catch (CheckInTaskRepeatToEntryException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ Task repeat.]");
		}catch (CheckInTaskRepeatToOthersException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ Task repeat.]");
		}catch (CheckInMustHasTasksException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[Must add tasks.]");
		}catch (CheckInAllTasksClosedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ All tasks closed, cannt wait.]");
		}catch (CheckInMustNotHaveTasksException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" cannt add tasks.]");
		}catch (CheckInTaskHaveDeletedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ Task is deleted.]");
		}catch (EquipmentHasLoadedOrdersException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ Task had loaded, \n please delete load details.]");
		}catch(EquipmentNotFindException e){
			throw new SendResponseServerCodeAndMessageException(50000,"[Equipment not found.]");
		}catch(DoorNotFindException e){
			throw new SendResponseServerCodeAndMessageException(50000,"[Door not found.]");
		}catch(SpotNotFindException e){
			throw new SendResponseServerCodeAndMessageException(50000,"[Spot not found.]");
		}catch(CheckinTaskNotFoundException e2){
			throw new SendResponseServerCodeAndMessageException(50000,"[Task not found.]");
		}catch(SystemException e2){
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		catch (Exception e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
	}
	

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}

}
