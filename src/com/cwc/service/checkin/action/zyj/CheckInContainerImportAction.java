package com.cwc.service.checkin.action.zyj;

import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class CheckInContainerImportAction extends ActionFatherController {
	
	private CheckInMgrIFaceZyj checkInMgrZyj;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		String result = checkInMgrZyj.checkInImportContainers(request);
		
		responseXML(result, response);
	}
	
	private void responseXML(String result,HttpServletResponse response)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<Response>");
		boxQtyXml.append("<Result>"+result+"</Result>");
		boxQtyXml.append("</Response>");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");

		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		}
	}

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}
	
}