package com.cwc.service.checkin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentHasDecidedPickedException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.exception.checkin.EquipmentOccupyResourcesException;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class CheckInWindowDeleteEquipmentByIdAction  extends ActionFatherController{

	private CheckInMgrIFaceZyj checkInMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		DBRow result = new DBRow();
		try
		{
			int a = checkInMgrZyj.webDeleteEquipment(request);
					//.deleteEquipmentById(request);
			result.add("flag", a);
		}
		catch(EquipmentNotFindException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Equipment not found.]");
		}
		catch(EquipmentHadOutException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[All Task Closed or Equipment "+e.getMessage()+" Left.]");
		}
		catch(EquipmentHasDecidedPickedException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Pick by E"+e.getMessage()+".]");
		}
		catch(EquipmentOccupyResourcesException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[It occupied resource.]");
		}
		catch (Exception e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		throw new JsonException(new JsonObject(result));
	}

	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}

}
