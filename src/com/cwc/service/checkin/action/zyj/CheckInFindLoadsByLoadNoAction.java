package com.cwc.service.checkin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CheckInFindLoadsByLoadNoAction extends ActionFatherController{
	
	private SQLServerMgrIFaceZJ sqlServerMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		
		String loadNo = StringUtil.getString(request, "loadNo");

		DBRow[] rows = sqlServerMgrZJ.findLoadCompanyIdCustomerIdByLoadNo(loadNo, 0L, request);
		if(0 == rows.length)
		{
			rows = sqlServerMgrZJ.findOrderCompanyIdCustomerIdByLoadNo(loadNo, 0L, request);
		}
        DBRow  result = new DBRow();
	    result.add("flag", rows.length);
		throw new JsonException(new JsonObject(result));
	}


	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

}
