package com.cwc.service.checkin.action.zwb;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ModCheckInAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		long id=checkInMgrZwb.modCheckIn(request);
		DBRow row =checkInMgrZwb.findGateCheckInById(id);
		if(row.get("yc_id", 0l)==0){
			long dlo_id=row.get("dlo_id", 0l);
			DBRow[] rows=this.checkInMgrZwb.findloadingByInfoId(dlo_id);
			if(rows!=null && rows.length>0){
				row.add("door_id",rows[0].get("rl_id",0l));
				//根据门id 查询门信息
				DBRow doorRow=this.checkInMgrZwb.findDoorById(rows[0].get("rl_id",0l));
				if(doorRow!=null){
					row.add("door_name",doorRow.getString("doorId"));
				}
			}
		}else{
			DBRow ycRow=this.checkInMgrZwb.findStorageYardControl(row.get("yc_id", 0l));
			row.add("yc_name",ycRow.getString("yc_no"));
		}
		throw new JsonException(new JsonObject(row));
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	

   
}
