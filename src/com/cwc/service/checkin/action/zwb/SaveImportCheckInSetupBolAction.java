package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public class SaveImportCheckInSetupBolAction extends ActionFatherController{
	
	private CheckInMgrIfaceZr  checkInMgrZr;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		String jsonStr=StringUtil.getString(request,"jsonStr");
//		//system.out.println(jsonStr);
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		JSONArray jsons=new JSONArray(jsonStr);
		for(int i=0;i<jsons.length();i++){
			JSONObject json = jsons.getJSONObject(i);
			String company_key=json.get("company_key").toString();
			String customer_key=json.get("customer_key").toString();
			String title_id=json.get("title_id").toString();
			String ship_to_id=json.get("ship_to_id").toString();
			String freight_term_id=json.get("freight_term_id").toString();
			String validate_type=json.get("validate_type").toString();
			String master_bol = json.get("master_bol").toString().trim().toLowerCase();
			String bol = json.get("bol").toString().trim().toLowerCase();
			long master_bol_key = 0;//yes或不填的时候，默认打印
			long bol_key = 0;//yes或不填的时候，默认打印
			if("no".equals(master_bol)){
				master_bol_key=1;//no，不打印
			}
			if("no".equals(bol)){
				bol_key=1;
			}
			
			DBRow row = new DBRow();
			row.add("company_key", company_key);
			row.add("customer_key", customer_key);
			row.add("title_id", title_id);
			row.add("ship_to_id", ship_to_id);
			row.add("freight_term_id", freight_term_id);
			row.add("master_bol", master_bol_key);
			row.add("bol", bol_key);
			row.add("validate_type", validate_type);
			list.add(row);
		}
		DBRow[] rows = list.toArray(new DBRow[list.size()]);
		//this.checkInMgrZr.addBolSetup(rows);
		DBRow  result = new DBRow();
	    result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
	}


	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}


   
}
