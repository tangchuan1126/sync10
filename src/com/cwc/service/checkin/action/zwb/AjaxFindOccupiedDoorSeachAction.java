package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zwb.TransportMgrIfaceZwb;
import com.cwc.app.iface.zwb.WaybillMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.thoughtworks.xstream.mapper.ArrayMapper;

public class AjaxFindOccupiedDoorSeachAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		String doorName=StringUtil.getString(request,"doorName");
		long ps_id=StringUtil.getLong(request,"ps_id");
		long area_id=StringUtil.getLong(request,"zone_id");
		long flag=StringUtil.getLong(request,"flag");
		if(flag==1){
			DBRow[] rows=this.checkInMgrZwb.getUnavailableDoorByDoorName(ps_id,area_id,doorName); //葛庆玲改  2014/12/2
			throw new JsonException(new JsonObject(rows));
		}else{
			DBRow[] rows=this.checkInMgrZwb.findUnavailableDoorByDoorName(ps_id,area_id,doorName); //葛庆玲改  2014/12/2
			throw new JsonException(new JsonObject(rows));
		}
        
//        //system.out.println(rows.length);        
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	

   
}
