package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zwb.TransportMgrIfaceZwb;
import com.cwc.app.iface.zwb.WaybillMgrIfaceZwb;
import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.thoughtworks.xstream.mapper.ArrayMapper;

public class AjaxPrintFindNoticeAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		long detail_id=StringUtil.getLong(request,"id");
        DBRow[] rows=this.checkInMgrZwb.windowFindSchedule(detail_id);
        String name="";
        String note="";
        if(rows.length>0 && rows!=null){
        	for(int i=0;i<rows.length;i++){
        		if(i==rows.length-1){
        			name+=rows[i].get("employe_name","");
        		}else{
        			name+=rows[i].get("employe_name","")+",";
        		}
        	}
        	note=rows[0].get("schedule_detail", "");
        }
        DBRow row=new DBRow();
        row.add("name",name);
  
        String[] str=note.split("Note：");
        if(str.length>1){
        	 row.add("note",str[1]);
        }else{
        	row.add("note","");
        }
		throw new JsonException(new JsonObject(row));
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	

   
}
