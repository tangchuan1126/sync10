package com.cwc.service.checkin.action.zwb;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

//wenti
public class CheckInExportAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		com.cwc.app.iface.AdminMgrIFace adminMgr = (com.cwc.app.iface.AdminMgrIFace)MvcUtil.getBeanFromContainer("proxyAdminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 		
		
		String path="../../"+this.checkInMgrZwb.ajaxDownEntryExport(adminLoggerBean.getAdid() ,request);	
		
		DBRow dbrow = new DBRow();
		dbrow.add("canexport", true);
		dbrow.add("fileurl", path);
		throw new JsonException(new JsonObject(dbrow));
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	

   
}
