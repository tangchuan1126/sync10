package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxFindDoorsByZoneIdAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		long zoneId=StringUtil.getLong(request,"zoneId");
		long mainId=StringUtil.getLong(request,"mainId");
		long ps_id=StringUtil.getLong(request,"ps_id");
		long flag=StringUtil.getLong(request,"flag");
		//DBRow[] rows=checkInMgrZwb.findDoorsByZoneId(zoneId,mainId,ps_id,flag);
//		DBRow[] rows=this.checkInMgrZwb.selectUseDoor(ps_id, zoneId);
		DBRow[] rows=this.checkInMgrZwb.getVacancyDoor(ps_id, zoneId, mainId);//根据zoneId、mianId、 ps_id 查询可用的door 葛庆玲  2014/12/02
		
		if(flag==0){
			throw new JsonException(new JsonObject(rows));
		}else{
			List<DBRow> singleList = new ArrayList<DBRow>(); 
			List<DBRow> doubleList = new ArrayList<DBRow>();
			
			//按照门的id计算优先单数  然后双数 没有弹开页面
			if(rows!=null&&rows.length>0){  
				for(int i=0;i<rows.length;i++){
					long doorid=rows[i].get("sd_id", 0l);//门的id
					if(doorid%2==1){
						singleList.add(rows[i]);
						break;
					}else if(doorid%2==0){
						doubleList.add(rows[i]);
					}
				}
			}
			if(singleList.size()>0){
				throw new JsonException(new JsonObject(singleList.toArray(new DBRow[singleList.size()])));
			}else if(doubleList.size()>0){
				throw new JsonException(new JsonObject(doubleList.toArray(new DBRow[doubleList.size()])));
			}else{
				throw new JsonException(new JsonObject(null));
			}
		}
		
		
		
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	

   
}
