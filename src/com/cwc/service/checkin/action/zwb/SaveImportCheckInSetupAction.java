package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public class SaveImportCheckInSetupAction extends ActionFatherController{
	
	private CheckInMgrIfaceZr  checkInMgrZr;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		String jsonStr=StringUtil.getString(request,"jsonStr");
		//system.out.println(jsonStr);
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		JSONArray jsons=new JSONArray(jsonStr);
		for(int i=0;i<jsons.length();i++){
			JSONObject json = jsons.getJSONObject(i);
			String company_key=json.get("company_key").toString();
			String customer_key=json.get("customer_key").toString();
			String title_id=json.get("title_id").toString();
			String ship_to_id=json.get("ship_to_id").toString();
			String freight_term_id=json.get("freight_term_id").toString();
			String load_bar_id=json.get("load_bar_id").toString();
			String validate_type=json.get("validate_type").toString();
			
			DBRow row = new DBRow();
			row.add("company_key", company_key);
			row.add("customer_key", customer_key);
			row.add("title_id", title_id);
			row.add("ship_to_id", ship_to_id);
			row.add("freight_term_id", freight_term_id);
			row.add("load_bar_id", load_bar_id);
			row.add("validate_type", validate_type);
			list.add(row);
		}
		DBRow[] rows = list.toArray(new DBRow[list.size()]);
		this.checkInMgrZr.addLoadBarSet(rows);
		DBRow  result = new DBRow();
	    result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
	}


	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}


   
}
