package com.cwc.service.checkin.action.zwb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zj.SQLServerMgrZJ;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zwb.TransportMgrIfaceZwb;
import com.cwc.app.iface.zwb.WaybillMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.thoughtworks.xstream.mapper.ArrayMapper;

public class AjaxCheckInWindowGetZoneAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private SQLServerMgrZJ sqlServerMgrZJ;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
       // DBRow[] row=this.checkInMgrZwb.getLoading(request);
		long type=StringUtil.getLong(request,"number_type");
        DBRow[] rows = sqlServerMgrZJ.getLoading(request);
 		if(rows!=null && rows.length > 0 ){
			for(int i=0;i<rows.length;i++){
				String area_name = rows[i].get("area_name", "");
				String title_name = rows[i].get("title_name", "");
				//DBRow row = checkInMgrZwb.getLoading(area_name,title_name,type);
//				if(row != null){
//					rows[i].add("area_id", row.get("area_id",0l));
//				}
			}
		}
        
        throw new JsonException(new JsonObject(rows));
      
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	public void setSqlServerMgrZJ(SQLServerMgrZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

   
}
