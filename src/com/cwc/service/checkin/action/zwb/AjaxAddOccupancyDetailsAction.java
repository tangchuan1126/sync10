package com.cwc.service.checkin.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustHasTasksException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;

//wenti
public class AjaxAddOccupancyDetailsAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private MessageAlerter messageAlert;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		DBRow  result = new DBRow();
		try
		{
			
			com.cwc.app.iface.AdminMgrIFace adminMgr = (com.cwc.app.iface.AdminMgrIFace)MvcUtil.getBeanFromContainer("proxyAdminMgr");
			AdminLoginBean loginBean = adminMgr.getAdminLoginBean(request.getSession());
			long entry_id = checkInMgrZwb.addOccupancyDetails(loginBean.getAdid() , request);
		    result.add("flag", entry_id);
		   
			
		}
		catch(DoorHasUsedException e1)
		{
			//throw new DoorHasUsedException();
			//messageAlert.setMessage(request,Resource.getStringValue("","DoorHasUsedException",""));
//			throw new DoorHasUsedException(50000,"[The door has been used.]");
			throw new SendResponseServerCodeAndMessageException(50000,"[The door "+e1.getMessage()+" has been used.]");
		}
		catch (SpotHasUsedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[The spot "+e.getMessage()+" has been used.]");
		}
		catch (CheckInTaskCanntDeleteException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" cannt delete.]");
		}
		catch (CheckInTaskRepeatToEntryException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" repeat.]");
		}
		catch (CheckInTaskRepeatToOthersException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" repeat.]");
		}
		catch (CheckInMustHasTasksException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" must add tasks.]");
		}
		catch (CheckInAllTasksClosedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ All tasks closed, cannt wait.]");
		}
		catch (CheckInMustNotHaveTasksException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ "+e.getMessage()+" cannt add tasks.]");
		}catch (CheckInTaskHaveDeletedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[ Task had deleted by others.]");
		}
		catch(SystemException e2)
		{
			//throw new DoorHasUsedException();
			//messageAlert.setMessage(request,Resource.getStringValue("","DoorHasUsedException",""));
//			throw new DoorHasUsedException(50000,"[The door has been used.]");
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		
		catch (Exception e) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		throw new JsonException(new JsonObject(result));
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}


	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}


	

   
}
