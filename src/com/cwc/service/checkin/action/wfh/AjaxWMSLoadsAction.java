package com.cwc.service.checkin.action.wfh;


import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;

import com.cwc.exception.JsonException;

import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

import com.cwc.json.JsonObject;

import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AjaxWMSLoadsAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		com.cwc.app.iface.AdminMgrIFace adminMgr = (com.cwc.app.iface.AdminMgrIFace)MvcUtil.getBeanFromContainer("proxyAdminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 	

		long adid = adminLoggerBean.getAdid();

		String startTime = StringUtil.getString(request, "startTime");
		String endTime = StringUtil.getString(request, "endTime");

		List<DBRow> data = checkInMgrZwb.getLoadNo(request, adid, startTime, endTime);
        
        DBRow[] datas = (DBRow[]) data.toArray(new DBRow[0]);
        throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(datas));

		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
}
