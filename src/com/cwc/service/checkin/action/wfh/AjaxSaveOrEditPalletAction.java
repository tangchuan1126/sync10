package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AjaxSaveOrEditPalletAction extends ActionFatherController{
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adid = adminLoginBean.getAdid();
		
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		long palletId = StringUtil.getInt(request,"palletId");
		int titleId = StringUtil.getInt(request,"titleId");
		long palletType = StringUtil.getLong(request,"palletType");
		float palletCount = StringUtil.getFloat(request,"palletCount");
		float damagedCount = StringUtil.getFloat(request,"damagedCount");
		int flag = StringUtil.getInt(request,"flag");
		String postDate = DateUtil.NowStr();
		
		DBRow para = new DBRow();
		para.add("pallet_id", palletId);
		para.add("title_id", titleId);
		para.add("pallet_type", palletType);
		para.add("pallet_count", palletCount);
		para.add("damaged_count", damagedCount);
		para.add("post_date", postDate);
		para.add("ps_id", ps_id);
		para.add("flag", flag);
		long count = checkInMgrWfh.saveOrEditPallet(para, adid);
		
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}

}
