package com.cwc.service.checkin.action.wfh;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.key.SealWithStatusKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class AjaxSealAction extends ActionFatherController {
	// @Autowired
	private FloorCheckInMgrWfh floorCheckInMgrWfh;
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
	private TransactionTemplate txTemplate;
	private AdminMgr adminMgr;
	private SealWithStatusKey sealWithStatusKey = new SealWithStatusKey();
	//查询sela
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
 		long psId=adminLoggerBean.getPs_id(); 
		int pageNo = Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "5"));
		long ps_id = StringUtil.getLong("ps_id");
		int seal_status = Integer.parseInt(StringUtils.defaultString(request.getParameter("import_status"),"0"));
		String import_start_time = StringUtils.defaultString(request.getParameter("import_start_time"), "");
		String import_end_time = StringUtils.defaultString(request.getParameter("import_end_time"), "");
		String used_start_time = StringUtils.defaultString(request.getParameter("used_start_time"), "");
		String used_end_time = StringUtils.defaultString(request.getParameter("used_end_time"), "");
		String return_start_time = StringUtils.defaultString(request.getParameter("return_start_time"), "");
		String return_end_time = StringUtils.defaultString(request.getParameter("return_end_time"), "");
		if(!StringUtil.isBlank(import_start_time)){
			import_start_time = DateUtil.showUTCTime(import_start_time, psId);
		}
		if(!StringUtil.isBlank(import_end_time)){
			import_end_time = DateUtil.showUTCTime(import_end_time, psId);
		}
		if(!StringUtil.isBlank(used_start_time)){
			used_start_time = DateUtil.showUTCTime(used_start_time, psId);
		}
		if(!StringUtil.isBlank(used_end_time)){
			used_end_time = DateUtil.showUTCTime(used_end_time, psId);
		}
		if(!StringUtil.isBlank(return_start_time)){
			return_start_time = DateUtil.showUTCTime(return_start_time, psId);
		}
		if(!StringUtil.isBlank(return_end_time)){
			return_end_time = DateUtil.showUTCTime(return_end_time, psId);
		}
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		DBRow para = new DBRow();
		para.add("seal_status", seal_status);
		para.add("ps_id", ps_id);
		para.add("import_start_time", import_start_time);
		para.add("import_end_time", import_end_time);
		para.add("used_start_time", used_start_time);
		para.add("used_end_time", used_end_time);
		para.add("return_start_time", return_start_time);
		para.add("return_end_time", return_end_time);
	//	para.add("key", key);
		
		DBRow adminBean =  new DBRow();
		adminBean.add("ps_id", adminLoggerBean.getPs_id());
		adminBean.add("adgid", adminLoggerBean.getAdgid());
		adminBean.add("adid", adminLoggerBean.getAdid());
//		adminBean.add("adid", adminLoggerBean.get);
		
		DBRow[] result = floorCheckInMgrWfh.getSealInventory(para ,pc);	//按照条件查出所有的seal
		for (DBRow row : result) {
			String import_time = row.get("import_time", "");
			String used_time = row.get("used_time", "");
			String return_time = row.get("return_time", "");
			if(!used_time.equals("")){
				used_time = used_time.substring(0, used_time.length()-2);
				used_time = DateUtil.showLocalparseDateTo24Hours(used_time, psId);
			}
			if(!import_time.equals("")){
				import_time = import_time.substring(0, import_time.length()-2);
				import_time = DateUtil.showLocalparseDateTo24Hours(import_time, psId);
			}
			if(!return_time.equals("")){
				return_time = return_time.substring(0, return_time.length()-2);
				return_time = DateUtil.showLocalparseDateTo24Hours(return_time, psId);
			}
			row.add("import_time", import_time);
			row.add("used_time", used_time );
			row.add("return_time", return_time);
		}
		DBRow[] ps = floorCheckInMgrZwb.findSelfStorage();	//查出所有的仓库
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(0,sealWithStatusKey.IMPORT);
		list.add(1,sealWithStatusKey.USED);
		list.add(2,sealWithStatusKey.RETURN);
		DBRow keys = new DBRow();
		DBRow kk = new DBRow();
		for (int i = 0;i<list.size() ;i++) {
			DBRow k = new DBRow();
			k.add("key", list.get(i));
			k.add("value", sealWithStatusKey.getSealWithStatusName(list.get(i)));
			kk.add(sealWithStatusKey.getSealWithStatusName(list.get(i)), DBRowUtils.dbRowAsMap(k));
		}
		keys.add("sealWithStatusKey", DBRowUtils.dbRowAsMap(kk));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject()
				.put("pageCtrl", new JSONObject(pc))
				.put("data", DBRowUtils.dbRowArrayAsJSON(result))
				.put("storage", DBRowUtils.dbRowArrayAsJSON(ps))
				.put("keys", DBRowUtils.dbRowAsMap(keys))
				.put("adminBean", DBRowUtils.dbRowAsMap(adminBean));
		

		response.getWriter().print(output.toString());
	}
	
	// 添加seal
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		////system.out.println(TestUtils.dbRowAsString(row));
		Date date=(Date)Calendar.getInstance().getTime();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String postDate = dateformat.format(date);	//获得当前系统时间
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
 		long psId=adminLoggerBean.getPs_id(); 
 		long adId=adminLoggerBean.getAdid();
		
		row.add("import_time", postDate);
		row.add("import_user", adId);
		row.add("seal_status", sealWithStatusKey.IMPORT);
		
		long sealId = floorCheckInMgrWfh.addSealInventory(row);
		Map<String,String> map = new HashMap<String, String>();
		if(sealId>0){
			map.put("data", "success");
		}else{
			map.put("data", "error");
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(map));
	}
	// 更新seal
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
//		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
//		//system.out.println(TestUtils.dbRowAsString(row));
		long sealId = row.get("seal_id", 0L);
		int seal_status = row.get("seal_status", 0);
		Date date=(Date)Calendar.getInstance().getTime();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String postDate = dateformat.format(date);	//获得当前系统时间
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
 		long psId=adminLoggerBean.getPs_id(); 
 		long adId=adminLoggerBean.getAdid();
 		DBRow _row = new DBRow();
 		_row.add("seal_no", row.get("seal_no","").trim());
 		_row.add("seal_status",seal_status);
 		_row.add("ps_id",row.get("ps_id",0));
		if(seal_status==2){  ///1----import  2----used  3-----return
			_row.add("used_time",postDate);
			_row.add("assign_time",postDate);
			_row.add("assign_user", adId);
		}else if(seal_status==3){
			_row.add("return_time", postDate);
			_row.add("return_user", adId);
		}
		
		long count = floorCheckInMgrWfh.updateSealInventory(sealId, _row);
		Map<String,Object> map = new HashMap<String, Object>();
		if(count>0){
			map.put("success", true);
		}else{
			map.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(map));
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	/*	this.floorAdminMgr.delAdmin(Long.parseLong(request.getParameter("id")));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject().put("success", true).toString());*/
	}

	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
	//	AdminLoginBean am = new AdminMgr().getAdminLoginBean(request.getSession());
	//	//system.out.println(DBRowUtils.dbRowArrayAsJSON(am.getTitles()));
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							doGet(request, response);
							break;
						case "POST":
							doPost(request, response);
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							doDelete(request, response);
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}

	}
	

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}
	
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public AdminMgr getAdminMgr() {
		return adminMgr;
	}

	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}
}
