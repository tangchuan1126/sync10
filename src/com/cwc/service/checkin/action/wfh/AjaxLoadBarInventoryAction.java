package com.cwc.service.checkin.action.wfh;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.key.SealWithStatusKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;
import com.jspsmart.upload.File;

public class AjaxLoadBarInventoryAction extends ActionFatherController {
	// @Autowired
	private FloorCheckInMgrWfh floorCheckInMgrWfh;
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
	private TransactionTemplate txTemplate;
	private AdminMgr adminMgr;
	//查询loadBarInventory
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		long psId = adminLoggerBean.getPs_id();
		int pageNo = Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "5"));
		long ps_id = StringUtil.getLong("ps_id");
		int customer_id = StringUtil.getInt(request, "customer_id");
		int load_bar_id = StringUtil.getInt(request, "load_bar_id");
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		DBRow para = new DBRow();
		para.add("customer_id", customer_id);
		para.add("load_bar_id", load_bar_id);
		para.add("ps_id", ps_id);
		
		
		DBRow adminBean =  new DBRow();
		adminBean.add("ps_id", adminLoggerBean.getPs_id());
		adminBean.add("adgid", adminLoggerBean.getAdgid());
		adminBean.add("adid", adminLoggerBean.getAdid());
		
		DBRow[] result = floorCheckInMgrWfh.getLoadBarInventory(para ,pc);	//按照条件查出所有的seal
		for (DBRow row : result) {
			String create_time = row.get("import_time", "");
			
			if(!create_time.equals("")){
				create_time = create_time.substring(0, create_time.length()-2);
				create_time = DateUtil.showLocalparseDateTo24Hours(create_time, psId);
			}
			row.add("create_time", create_time);
		}
		DBRow[] ps = floorCheckInMgrZwb.findSelfStorage();	//查出所有的仓库
		DBRow[] customer = floorCheckInMgrWfh.findAllCustomer();	//查出所有的customer
		DBRow[] loadBar = floorCheckInMgrZwb.selectLoadBar();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject()
				.put("pageCtrl", new JSONObject(pc))
				.put("data", DBRowUtils.dbRowArrayAsJSON(result))
				.put("storage", DBRowUtils.dbRowArrayAsJSON(ps))
				.put("customers", DBRowUtils.dbRowArrayAsJSON(customer))
				.put("loadBars", DBRowUtils.dbRowArrayAsJSON(loadBar))
				.put("adminBean", DBRowUtils.dbRowAsMap(adminBean));
		

		response.getWriter().print(output.toString());
	}
	
	// 添加loadBarInventory
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		////system.out.println(TestUtils.dbRowAsString(row));
		String postDate = DateUtil.NowStr();
		DBRow date = DBRowUtils.convertToDBRow(jsonData);
		long loadBarId ;
		DBRow loadBar = floorCheckInMgrWfh.findLoadBarByName(date.get("load_bar_name", ""));
		if(loadBar.get("load_bar_id", 0)==0){//判断是否已经有这种loadBar
			date = new DBRow();
			date.add("load_bar_name", row.get("load_bar_name", ""));
			loadBarId = floorCheckInMgrWfh.addLoadBar(date);
		}else{
			loadBarId = loadBar.get("load_bar_id", 0);
		}
		long loadBarInventoryId;
		row.add("load_bar_id", loadBarId);
		DBRow loadBarInventory = floorCheckInMgrWfh.findLoadBarInventoryByLoadBarId(Integer.parseInt(loadBarId+""),row.get("customer_id",0));  //查询是否已经有这种loadBarInventory
		if(loadBarInventory.get("load_bar_inventory_id", 0)>0){//判断是是否已经有这种loadBarInventory
			date = new DBRow();
			date.add("load_bar_inventory_count", loadBarInventory.get("load_bar_inventory_count",0f)+row.get("load_bar_inventory_count", 0f));
			date.add("load_bar_damaged_count", loadBarInventory.get("load_bar_damaged_count",0f)+row.get("load_bar_damaged_count", 0f));
			loadBarInventoryId = floorCheckInMgrWfh.updateLoadBarInventory(loadBarInventory.get("load_bar_inventory_id", 0),date);
		}else{
			date = new DBRow();
			date.add("load_bar_id", loadBarId);
			date.add("create_time", postDate);
			date.add("customer_id", row.get("customer_id", 0));
			date.add("load_bar_inventory_count", row.get("load_bar_inventory_count", 0f));
			date.add("load_bar_damaged_count", row.get("load_bar_damaged_count", 0f));
			date.add("ps_id", row.get("ps_id", 0L));
			loadBarInventoryId = floorCheckInMgrWfh.addLoadBarInventory(date);
		}
		Map<String,String> map = new HashMap<String, String>();
		if(loadBarInventoryId>0){
			map.put("data", "success");
		}else{
			map.put("data", "error");
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(map));
	}
	// 更新loadBarInventory
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
//		//system.out.println("jsonData=" + jsonData);
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
//		//system.out.println(TestUtils.dbRowAsString(row));
		long loadBarName = row.get("load_bar_name", 0L);
		int loadBarInventoryId = row.get("load_bar_inventory_id", 0);
 		DBRow _row = new DBRow();
 		_row.add("customer_id",row.get("customer_id",0));
 		_row.add("load_bar_inventory_count",row.get("load_bar_inventory_count",0F));
 		_row.add("load_bar_damaged_count",row.get("load_bar_damaged_count",0F));
		
		long count = floorCheckInMgrWfh.updateLoadBarInventory(loadBarInventoryId, _row);
		Map<String,Object> map = new HashMap<String, Object>();
		if(count>0){
			map.put("success", true);
		}else{
			map.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(map));
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
	}

	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
	//	AdminLoginBean am = new AdminMgr().getAdminLoginBean(request.getSession());
	//	//system.out.println(DBRowUtils.dbRowArrayAsJSON(am.getTitles()));
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							doGet(request, response);
							break;
						case "POST":
							doPost(request, response);
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							doDelete(request, response);
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}

	}
	

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}
	
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public AdminMgr getAdminMgr() {
		return adminMgr;
	}

	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}
}
