package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AjaxFindSNAction extends ActionFatherController{
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		long con_id = StringUtil.getLong(request,"con_id");
		int pageSize = StringUtil.getInt(request,"pageSize");
		int pageNo = StringUtil.getInt(request,"pageNo");
		long ps_id = StringUtil.getLong(request,"ps_id");
		DBRow[] data = checkInMgrWfh.findSNByConID(con_id, pageNo, pageSize,ps_id);
		
		throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(data));
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
