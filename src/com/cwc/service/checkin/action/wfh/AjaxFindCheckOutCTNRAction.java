package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AjaxFindCheckOutCTNRAction extends ActionFatherController{
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		String liscense_plate = StringUtil.getString(request, "liscense_plate");
		String gate_container_no = StringUtil.getString(request, "gate_container_no");
		long dlo_id = StringUtil.getLong(request, "dlo_id");
		long ps_id = StringUtil.getLong(request,"ps_id");
		DBRow[] data = checkInMgrWfh.findForgetCheckOutEquipmentByCtnr(gate_container_no,liscense_plate,dlo_id,ps_id,"");
		
		throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(data));
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
