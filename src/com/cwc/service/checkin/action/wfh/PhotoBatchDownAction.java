package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.tc.FileMgrTc;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class PhotoBatchDownAction extends ActionFatherController {
	private FileMgrTc fileMgrTc;
	private FileWithCheckInClassKey fileWithCheckInClassKey = new FileWithCheckInClassKey();
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		
		fileMgrTc.createPackageByFileIds(request,response);
		
		
		
		//throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(picturs));
	}
	
	public void setFileMgrTc(FileMgrTc fileMgrTc) {
		this.fileMgrTc = fileMgrTc;
	}

}
