package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zr.LoadBarUseMgrIfaceZr;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AjaxSaveLoadBarAction extends ActionFatherController{
	
	private LoadBarUseMgrIfaceZr loadBarUseMgrZr;
	private AdminMgr adminMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
 		long psId=adminLoggerBean.getPs_id(); 
 		long adid=adminLoggerBean.getAdid();
		String time = DateUtil.FormatDatetime("yyy-MM-dd hh:mm:ss");
		DBRow para = new DBRow();
		para.add("load_bar_id", StringUtil.getString(request, "load_bar_id"));
		para.add("dlo_id", StringUtil.getString(request, "main_id"));
		para.add("equipment_id", StringUtil.getString(request, "equipment_id"));
		para.add("last_opertion_time", time);
		para.add("count", StringUtil.getInt(request,"count"));
		para.add("last_opertion_adid", adid);
		DBRow row = new DBRow();
		long id = loadBarUseMgrZr.addLoadBarUse(para);
		row.add("data", id>0?1:0);
		throw new WriteOutResponseException(new JSONObject(row).toString());
	}



	public void setLoadBarUseMgrZr(LoadBarUseMgrIfaceZr loadBarUseMgrZr) {
		this.loadBarUseMgrZr = loadBarUseMgrZr;
	}


	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}
	

	
}
