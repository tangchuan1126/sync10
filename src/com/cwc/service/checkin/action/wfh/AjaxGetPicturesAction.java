package com.cwc.service.checkin.action.wfh;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxGetPicturesAction extends ActionFatherController{
	
	private FileMgrIfaceZr fileMgrZr;
	private FileWithCheckInClassKey fileWithCheckInClassKey = new FileWithCheckInClassKey();
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		
		long entry_id = StringUtil.getLong(request, "entry_id");
		DBRow[] picturs = fileMgrZr.getCheckInModelPhotos(entry_id);
        
		throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(picturs));
		
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}
		
	
	
}
