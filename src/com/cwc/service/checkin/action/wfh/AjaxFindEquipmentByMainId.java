package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AjaxFindEquipmentByMainId extends ActionFatherController{
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	//private AdminMgr adminMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		long dloId = StringUtil.getLong(request, "entry_id");
		int equipmentType = StringUtil.getInt(request, "equipment_type");
		DBRow[] rows = checkInMgrWfh.findTractorByEntryId(dloId, equipmentType);
		throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(rows));
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
