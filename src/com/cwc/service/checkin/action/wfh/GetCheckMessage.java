package com.cwc.service.checkin.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetCheckMessage extends ActionFatherController{
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		DBRow data = new DBRow();
		String ctnr = StringUtil.getString(request, "ctnr");
		int type = StringUtil.getInt(request, "type");
		String time = StringUtil.getString(request, "time");
		String checkInCode = StringUtil.getString(request, "check_code");
		if(ctnr.equals("") || (type!=1 && type!=2) || time.equals("") || checkInCode.equals("")){
			data.add("success", false);
			data.add("data", "Parameter error!");
			throw new WriteOutResponseException(new JSONObject(data).toString());
		}
		try{
			data = checkInMgrWfh.getCheckMessage(ctnr,type,time,checkInCode);
			data.add("success", true);
		}catch(Exception e){
			data.add("success", false);
			data.add("data", "system error!");
		}
		throw new WriteOutResponseException(new JSONObject(data).toString());
	}


	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
