package com.cwc.service.waybillB2B.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.CartWaybillB2BMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class RemoveToWayBillB2BCartAction extends ActionFatherController {

	private CartWaybillB2BMgrIFaceZJ cartWaybillB2BMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		try
		{
			cartWaybillB2BMgrZJ.removeProduct(request);
		}
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		
		throw new WriteOutResponseException("ok");
	}
	public void setCartWaybillB2BMgrZJ(CartWaybillB2BMgrIFaceZJ cartWaybillB2BMgrZJ) {
		this.cartWaybillB2BMgrZJ = cartWaybillB2BMgrZJ;
	}
}
