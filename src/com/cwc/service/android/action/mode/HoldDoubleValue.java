package com.cwc.service.android.action.mode;

import java.io.Serializable;

/**
 * 如果A,B 都是相同类型的参数的时候请你在命名的时候写好前面的参数
 *  比如 TaskIdAndDoorId
 * @author win7zr
 *
 * @param <A>
 * @param <B>
 */
public class HoldDoubleValue<A,B> implements Serializable {

	public final A a ;
	public final B b ;
	
 
	public HoldDoubleValue(A a, B b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof HoldDoubleValue){
			HoldDoubleValue o =	(HoldDoubleValue) obj;
			return o.a.equals(a) && o.b.equals(b);
		}
 		return  false ;
	}
	
	@Override
	public int hashCode() {
 		return a.hashCode() + b.hashCode();
	}
	
}

