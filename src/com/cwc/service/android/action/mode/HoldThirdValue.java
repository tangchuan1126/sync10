package com.cwc.service.android.action.mode;

public class HoldThirdValue<A,B,C> extends HoldDoubleValue<A,B> {
	
	public final C c ; 
	public HoldThirdValue(A a, B b , C c) {
		super(a, b);
		this.c = c ;
 	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof HoldThirdValue){
			HoldThirdValue o =	(HoldThirdValue) obj;
			return o.a.equals(a) && o.b.equals(b) && o.c.equals(c);
		}
 		return  false ;
	}
	
	@Override
	public int hashCode() {
 		return a.hashCode() + b.hashCode() + c.hashCode();
	}
}
