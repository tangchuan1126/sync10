
package com.cwc.service.android.action.mode;

/**
 * android 需要改的接口太多,现在处理
 * @author ZHANJIE
 *
 */

 
 
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

/**
 * 用于在android 查询Entry的时候，当前操作人是否有权限看这个Entry
 * @author zhangrui
 *
 */
public class CheckInEntryPermissionUtil {

	private static boolean isLoginUserIsAdmin(AdminLoginBean adminLoggerBean){
		if(adminLoggerBean != null){
			DBRow[] rows = adminLoggerBean.getDepartment();
			if(rows != null && rows.length>0){
				for (DBRow r : rows) {
					if(r.get("deptid", 0l) == 10000l){
						return true ;
					}
				}
			}
		}
		return false ;
	}
	/**
	 * 
	 * @param checkIn checkIn的主记录
	 * 如果当前的登录不是admin权限的人，那么他只能查看本仓库的数据。admin 是可以查看所有的数据的
	 * @param adminLoggerBean
	 * @throws NoPermiessionEntryIdException
	 */
	public static void checkEntry(DBRow checkIn ,  AdminLoginBean adminLoggerBean) throws NoPermiessionEntryIdException,CheckInNotFoundException,SystemException{
		 if(checkIn == null){
			 throw new CheckInNotFoundException();
		 }
		 if(adminLoggerBean == null){
			 throw new SystemException() ;
		 }
		 if(isLoginUserIsAdmin(adminLoggerBean)){
			 return ;
		 }else{
			 if(checkIn.get("ps_id", 0l) != adminLoggerBean.getPs_id()){
				 throw new NoPermiessionEntryIdException();
			 }
		 }
	}
	public static void checkEntryDetailNeedCheckIn(DBRow entryDetail) throws DockCheckInFirstException {
		if(entryDetail.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.UNPROCESS){
			throw new DockCheckInFirstException();
		} 
	}
	public static boolean isEntryLeft(DBRow entryRow) throws Exception {
		
		if(entryRow.get("status", 0)== CheckInMainDocumentsStatusTypeKey.LEFT){
			return true ;
		}
 		return false;
	}
}
 