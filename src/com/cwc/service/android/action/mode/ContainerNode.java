package com.cwc.service.android.action.mode;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.exception.android.InitContainerException;
import com.cwc.db.DBRow;

/**
 * 用于处理给定一个List<Lp>
 * 生成container_loading的平铺表的功能.在数据库中插入
 * 生成Container_child_list
 * @author ZHANJIE
 *
 */
public class ContainerNode {

	private long containerId ;
	
	private long parentContainerId ;
	
	private DBRow containerRow ; 
	
	private ContainerNode parentNode ;
	
	private List<ContainerNode> subContainer ;
	
	private static String tem = "%s  %s  %s  %s";
	
	public ContainerNode() {
		super();
	}

	public ContainerNode(DBRow containerRow) {
		super();
		this.containerRow =  containerRow ;
		this.containerId = containerRow.get("con_id", 0l);
		this.parentContainerId = containerRow.get("parent_con_id", 0l);
 		this.subContainer = new ArrayList<ContainerNode>();
	} 
	
	public void addContainer(ContainerNode addContainerNode ){
		subContainer.add(addContainerNode);
		/*if(containerId != 0l && parentNode != null){
			parentNode.addContainer(addContainerNode);
		}*/
	}

	
	private static  void findNextNodes(List<ContainerNode> roots , List<ContainerNode> arrayList){
		
		for(ContainerNode n: roots){
			List<ContainerNode> willAddToRoots = new ArrayList<ContainerNode>();
			for(ContainerNode temp :  arrayList){
  				if( temp.getParentContainerId() == n.getContainerId()){
					temp.setParentNode(n);
					n.addContainer(temp);
					willAddToRoots.add(temp);
 				}
			}
			if(willAddToRoots.size() > 0 ){
				//arrayList.removeAll(willAddToRoots);
				findNextNodes(willAddToRoots,arrayList);
			}
		}
	}
	
	
	public static void initContainerLoading(ContainerNode root , List<ContainerNode> arrayList){
		List<ContainerNode> willAddToRoots = new ArrayList<ContainerNode>();
		
		willAddToRoots.add(root);
		ContainerNode.findNextNodes(willAddToRoots, arrayList);
		 
	}
	
	public static void main(String[] args) {
		
		DBRow rootDBRow = new DBRow();
		rootDBRow.add("con_id", 1);
		rootDBRow.add("parent_con_id",0);
		
		ContainerNode clp = new ContainerNode(rootDBRow);
		
		DBRow clpDBRow = new DBRow();
		clpDBRow.add("con_id", 2);
		clpDBRow.add("parent_con_id",1);
		ContainerNode blp = new ContainerNode(clpDBRow);
		
		DBRow ilpDBRow = new DBRow();
		ilpDBRow.add("con_id", 3);
		ilpDBRow.add("parent_con_id",1);
		ContainerNode ilp = new ContainerNode(ilpDBRow);

		DBRow ilpDBRow1 = new DBRow();
		ilpDBRow1.add("con_id", 4);
		ilpDBRow1.add("parent_con_id",3);
		ContainerNode ilp1 = new ContainerNode(ilpDBRow1);
		
		 
		List<ContainerNode> arrayList = new ArrayList<ContainerNode>();
		arrayList.add(clp);
		arrayList.add(blp);
		arrayList.add(ilp);
		arrayList.add(ilp1);
		
		ContainerNode.initContainerLoading(clp, arrayList);
	//	ContainerNode.initSubList(arrayList);
		ContainerNode.printList(ContainerNode.initChildList(arrayList));
  	}
	public static List<DBRow> initChildList(List<ContainerNode> arrayList){
		List<DBRow>  datas = new ArrayList<DBRow>();
		for(ContainerNode node : arrayList){
 			create(node.getContainerId(),node,0,datas);
		}
		return datas;
	}
	private static void printList(List<DBRow>  arrayList){
		for(DBRow temp : arrayList){
			 String value = String.format(tem, temp.get("search_root_con_id", 0l), temp.get("cont_id", 0l),temp.get("levv", 0),
					 temp.get("parent_con_id", 0l));
			 //system.out.println(value);
		}
	}
	private static void create(long search_id,ContainerNode node, int lev,List<DBRow>  datas){
		 /*String value = String.format(tem, search_id, node.getContainerId(),lev,node.getParentContainerId());
		 //system.out.println(value);*/
		 datas.add(createDBRow(node, lev, search_id));
		 if(node.getSubContainer() != null && node.getSubContainer().size() > 0 ){
			 	for(ContainerNode temp : node.getSubContainer()){
			 		create(search_id,temp , lev+1,datas);
			 	} 
		 } 
	}
	private static DBRow createDBRow(ContainerNode node , int lev , long search_id){
		DBRow row = new DBRow();
		row.add("search_root_con_id", search_id);
		row.add("cont_id", node.getContainerId());
		row.add("parent_con_id", node.getParentContainerId());
		row.add("levv", lev);
		return row ;
	}
	public long getContainerId() {
		return containerId;
	}

	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}

	public long getParentContainerId() {
		return parentContainerId;
	}

	public void setParentContainerId(long parentContainerId) {
		this.parentContainerId = parentContainerId;
	}

	public ContainerNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(ContainerNode parentNode) {
		this.parentNode = parentNode;
	}

	public List<ContainerNode> getSubContainer() {
		return subContainer;
	}

	public void setSubContainer(List<ContainerNode> subContainer) {
		this.subContainer = subContainer;
	}

	public DBRow getContainerRow() {
		return containerRow;
	}

	public void setContainerRow(DBRow containerRow) {
		this.containerRow = containerRow;
	}
	
	
	
	
	
	
	
	
}
