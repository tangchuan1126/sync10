package com.cwc.service.android.action.zwb;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;



public class AndroidSeachProductLineAction extends ActionFatherController{
	
	private ProductLineMgrIfaceZwb productLineMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		String xml = "";
		response.setContentType("application/xml");	
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		xml = getPost(inputStream);
		String conditions= StringUtil.getSampleNode(xml,"Conditions");
		if(conditions.equals("productLine")){   //判断是否查询产品线 或分类
			DBRow[] row=productLineMgrZwb.getAllProductLine();
			responseProductLineXML(row,response);
		}else if(conditions.equals("productCatalogOne")){   //查询第一级产品分类
			String seachId= StringUtil.getSampleNode(xml,"SeachId");
			DBRow[] row=productLineMgrZwb.getProductCatalogByProductLine(Long.parseLong(seachId));
			responseProductCatalogOneXML(row,response);
		}else if(conditions.equals("productCatalogTwo")){
			String seachId= StringUtil.getSampleNode(xml,"SeachId");
			DBRow[] row=productLineMgrZwb.getProductCatalogByParentId(Long.parseLong(seachId));
			responseProductCatalogOneXML(row,response);
		}

		
	}
	
	
	//数据组成产分类xml 返回到手机
	private void responseProductCatalogOneXML(DBRow[] row,HttpServletResponse response) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		for(int i=0;i<row.length;i++){
			sb.append("<ProductCatalog>");
				sb.append("<CatalogId>"+row[i].get("id",0l)+"</CatalogId>");
				sb.append("<CatalogName>"+row[i].getString("title")+"</CatalogName>");
			sb.append("</ProductCatalog>");
		}				
//		//system.out.println(sb.toString());  //打印xml		
		try {
		   response.setCharacterEncoding("UTF-8");
		   OutputStream out = response.getOutputStream();
		   response.setContentLength(sb.toString().getBytes().length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
	
	
	//数据组成产品线xml 返回到手机
	private void responseProductLineXML(DBRow[] row,HttpServletResponse response) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		for(int i=0;i<row.length;i++){
			sb.append("<ProductLine>");
				sb.append("<LineId>"+row[i].get("id",0l)+"</LineId>");
				sb.append("<LineName>"+row[i].getString("name")+"</LineName>");
			sb.append("</ProductLine>");
		}				
//		//system.out.println(sb.toString());		
		try {
		   response.setCharacterEncoding("UTF-8");
		   OutputStream out = response.getOutputStream();
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
	
	//接收手机传过来的xml
	private String getPost(InputStream inputStream)throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null; 
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try{
			    	input.close();
			    }catch (Exception f){
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData,"UTF-8"));
	}

	public void setProductLineMgrZwb(ProductLineMgrIfaceZwb productLineMgrZwb) {
		this.productLineMgrZwb = productLineMgrZwb;
	}

}
