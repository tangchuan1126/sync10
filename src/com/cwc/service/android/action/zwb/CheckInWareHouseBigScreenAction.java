package com.cwc.service.android.action.zwb;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.SerchLoadingNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.util.DateUtil;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

public class CheckInWareHouseBigScreenAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private AdminMgrIFaceZJ adminMgrZJ;
	private CatalogMgrIFace catalogMgr;
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;
	private CheckInMgrIfaceZr checkInMgrZr;
	
	static Logger log = Logger.getLogger("PLATFORM");

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		DBRow fromAndroid = getJsonObjectFromRequest(request);
		DBRow result = new DBRow();
		
		if(!fromAndroid.getString("Version").equals(AndroidUtil.getScreenVersion())){
			//throw new AppVersionException();
			result.add("ret", BCSKey.FAIL);
			result.add("err", BCSKey.AppVersionException);
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		}
		
		int ps_id = StringUtil.getInt(request, "ps_id");
		String method = fromAndroid.getString("Method");
		/**
		 * 大屏幕内的小屏幕合并请求
		 */
		if(method.equals("multipleScreen")){
			String data = StringUtil.getString(request, "data");
			result = doMultipleMethod(data, ps_id);
		}else{
			PageCtrl pc = new PageCtrl();
			int pageSize = StringUtil.getInt(request, "pageSize");
			int pageNo = StringUtil.getInt(request, "pageNo");
			pc.setPageSize(pageSize);
			pc.setPageNo(pageNo);
			
			result = doMethod(method, ps_id, pc);
		}
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	
	private DBRow doMultipleMethod(String data, int ps_id){
		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		JSONObject jo = StringUtil.getJsonObject(data);
		try {
			JSONArray ja = jo.getJSONArray("data");
			int len = ja.length();
			for (int i = 0; i < len; i++) {
				JSONObject screen = ja.getJSONObject(i);
				String method = screen.getString("method");
				
				PageCtrl pc = new PageCtrl();
 				int pageSize = screen.getInt("pageSize");
 				int pageNo = screen.getInt("pageNo");
 				pc.setPageSize(pageSize);
 				pc.setPageNo(pageNo);
 				
 				DBRow row = doMethod(method, ps_id, pc);
 				result.add(method, row);
			}
		} catch (JSONException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		return result;
	}
 
 	private DBRow doMethod(String method, int ps_id, PageCtrl pc){
 		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
   		try{
   			
			if(method.equals("selectGateCheckIn")){
				DBRow[] datas= checkInMgrZwb.selectGateCheckIn(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("selectWareHouseCheckIn")){
				DBRow[] datas= checkInMgrZwb.selectWareHouseCheckIn(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("selectWareHouseNumberProcessing")){
				DBRow[] datas= checkInMgrZwb.selectWareHouseNumberProcessing(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
				 
			}
			if(method.equals("selectSpotSituation")){
				DBRow[] datas= checkInMgrZwb.selectSpotSituation(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("selectDockSituation")){
				DBRow[] datas= checkInMgrZwb.selectDockSituation(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("getProductStorageCatalogTree")){
 
//				log.error("---------------------------------zhangrui----------------------------" + StringUtil.getString(request, "screen"));
 				DBRow[] datas= checkInMgrZwb.findSelfStorage();
				result.add("data", datas);
			}
			if(method.equals("selectSpotSituation")){
				DBRow[] datas= checkInMgrZwb.selectSpotSituation(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("selectDockSituation")){
				DBRow[] datas= checkInMgrZwb.selectDockSituation(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equals("selectDockCloseNoCheckOut")){
				DBRow[] datas= checkInMgrZwb.selectDockCloseNoCheckOut(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equalsIgnoreCase("goingToWareHouse")){
				DBRow[] datas= checkInMgrZr.goingToWareHouse(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equalsIgnoreCase("remainJobs")){
				DBRow[] datas= checkInMgrZr.remainJobs(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equalsIgnoreCase("loadProcessing")){
				DBRow[] datas= checkInMgrZr.workingOnWarehouse(ps_id,2,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}
			if(method.equalsIgnoreCase("receiveProcessing")){
				DBRow[] datas= checkInMgrZr.workingOnWarehouse(ps_id,1,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}
			if(method.equalsIgnoreCase("cargoOnSpot")){
				DBRow[] datas= checkInMgrZr.cargoOnSpot(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(method.equalsIgnoreCase("cargoOnDoor")){
				DBRow[] datas= checkInMgrZr.cargoOnDoor(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("emptyCTNR")){
				DBRow[] datas= checkInMgrZr.emptyCTNR(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		 
		    if(method.equalsIgnoreCase("goingToWindow")){
				DBRow[] datas= checkInMgrZr.goingToWindow(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}		 
		    if(method.equalsIgnoreCase("parking3rdPartyNone")){
				DBRow[] datas= checkInMgrZr.parking3rdPartyNone(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("waitingList")){
				DBRow[] datas= checkInMgrZr.waitingList(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("forgetCloseTask")){
				DBRow[] datas= checkInMgrZr.forgetCloseTask(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("leaving")){
				DBRow[] datas= checkInMgrZr.leaving(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("forgetCheckOut")){
				DBRow[] datas= checkInMgrZr.forgetCheckOut(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("remainJobsLabor")){
					DBRow[] datas= checkInMgrZr.remainJobsLabor(ps_id, pc) ;
					result.add("data", datas);
					result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("noTaskLabor")){
					DBRow[] datas= checkInMgrZr.noTaskLabor(ps_id, new long[]{1000002},new long[]{0}, pc);
					result.add("data", datas);
					result.add("total", pc.getPageCount());
			}
		    if(method.equalsIgnoreCase("noTaskEntry")){
				DBRow[] datas= checkInMgrZr.noTaskEntry(ps_id, pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
		    }
		    if(method.equals("LoadReceiveCloseToday")){
				DBRow[] datas = checkInMgrZr.loadReceiveCloseToday(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}
			if(method.equals("LoadReceiveNotFinish")){
				DBRow[] datas = checkInMgrZr.loadReceiveNotFinish(ps_id,pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}
			if(method.equals("ghostCTNR")){
				DBRow[] datas = checkInMgrZr.ghostCTNR(ps_id, pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
 			}
			//LoadReceive 流程 查询IncomingToWindow,Load/Receive ,Waiting Assing Labor,Remain Jobs,Under Processing,Closed Today
			if(method.equals("LoadReceiveProcess")){
				DBRow datas = checkInMgrZr.getLoadReceiveProcess(ps_id);
				result.add("data", datas);
			}
		    
			/**
			 * 查看那些打印机是可用的
			 */
			if(method.equals("PrinterResource")){
				DBRow[] datas = androidPrintMgrZr.getPrintServerByBigScreen(ps_id, pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
 			result.add("total_num", pc.getAllCount());
 			
			/**
			 * 返回当前wareHouse的时间，printer，服务器的时间
			 * {
			 *  person:
			 *  person_total:
			 *  printer:
			 *  printer_total:
			 *  server_time: 09/11 12:12
			 * }
			 */
			if(method.equals("ServerPrintAndPersonTime")){
				String server_time = DateUtil.showLocalparseDateToNoYear24Hours(DateUtil.NowStr(), ps_id);
				HoldDoubleValue<Integer, Integer> availableAndAll = androidPrintMgrZr.getAvailableAndAll(ps_id);
				
				//printer
				//printer_total ;
				
				//person 
				//person_total ;
				
				
				//DBRow[] datas= checkInMgrZr.noTaskLabor(ps_id, new long[]{1000002},new long[]{0}, null);
				String onlineusers = checkInMgrZr.getOnlineUserString();  //现在是显示了，所有仓库的人员 整个在线得,到时候改
				result.add("free_person", onlineusers != null ? onlineusers.split(",").length : 0 );
				
				result.add("printer", availableAndAll.a);
				result.add("printer_total", availableAndAll.b);
				result.add("server_time", server_time);
				
				
				result.remove("total_num");
				result.remove("data");
				result.remove("total");
			}
			
		}catch (AppVersionException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.AppVersionException;		
		}catch (NoPermiessionEntryIdException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoPermiessionEntryIdException;	
 		}catch (CheckInNotFoundException  e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInNotFound;
		}catch (SerchLoadingNotFoundException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SerchLoadingNotFoundException;
 		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		
		return result;
 	}
	
 
	
	
	
	
 
	 
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
 		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("Version", StringUtil.getString(request, "Version"));
 		return result ;
	}
	 
 
	
	
	
	
	
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
}
