package com.cwc.service.android.action.zwb;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public class CheckInPatrolAction extends ActionFatherController {
	
	private AdminMgrIFaceZJ adminMgrZJ;
	private CheckInMgrIfaceXj checkInMgrXj ;
	private YMSMgrAPI ymsMgrAPI;
	
	@SuppressWarnings("unused")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
//			boolean isMutiArray = false ; //是否是多层的数组嵌套
	  		try{
				DBRow fromAndroid = getJsonObjectFromRequest(request);
				if(!fromAndroid.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				//模拟登陆
				AdminLoginBean adminLoggerBean = AndroidPermissionUtil.androidSetSession(login, request);
				long ps_id = adminLoggerBean.getPs_id();
				long adid = adminLoggerBean.getAdid();
				String method = fromAndroid.getString("Method") ;
				/**
				 * 下载当前登录人的SpotArea
				 */
				if(method.equals("LoadLoginUserSpotArea")){
					DBRow[] rows = this.ymsMgrAPI.occupySpotForAreaView(ps_id, 0);
					long needPatrolArea = this.checkInMgrXj.findStoragePatrolTimeByPsId(ps_id);
					if(rows.length>0){
						fixResourcesArea(rows);
					}
					
					result.add("data", rows);
					result.add("needPatrolArea",needPatrolArea );
				}

				if(method.equals("LoadLoginUserDockZone")){
					DBRow[] rows = this.ymsMgrAPI.occupyDoorForAreaView(ps_id, 0);
					long needPatrolArea = this.checkInMgrXj.findStoragePatrolTimeByPsId(ps_id);
					if(rows.length>0){
						fixResourcesArea(rows);
					}
					result.add("data", rows);
					result.add("needPatrolArea",needPatrolArea );
					
				}
				/**
				 * spot列表
				 */
				if(method.equals("SpotAndCheckInMain")){
//					isMutiArray  = true ;
					long area_id = StringUtil.getLong(request, "AreaId");
					int is_finish = StringUtil.getInt(request, "is_finish");
			//		int status = StringUtil.getInt(request, "Status");
					DBRow[] needPatrol =	checkInMgrXj.returnSpot(ps_id, area_id, 0,1, null);
					DBRow[] finishPatrol =	checkInMgrXj.returnSpot(ps_id, area_id, 0,2, null);
					
			//		fixRtOccupiedSpot(datas) ;
					result.add("needPatrol",needPatrol );
					result.add("finishPatrol",finishPatrol );
					
				}
				/**
				 * 门的列表
				 * 
				 */
				 
				if(method.equals("ZoneAndCheckInDetail")){
//					isMutiArray  = true ;
					long area_id = StringUtil.getLong(request, "ZoneId");
					int is_finish = StringUtil.getInt(request, "is_finish");
	//				int status = StringUtil.getInt(request, "Status");	//0:ALL,1:Occupied,2:UnOccupied 
	//				DBRow[] datas = checkInPatrolMgrZr.getDockCheckDetailsInfo(ZoneId,login.get("ps_id", 0l) , status);
					DBRow[] needPatrol = checkInMgrXj.returnDoor(ps_id, area_id, 0,1, null);
					DBRow[] finishPatrol = checkInMgrXj.returnDoor(ps_id, area_id, 0,2, null);
					
					result.add("needPatrol", needPatrol);
					result.add("finishPatrol", finishPatrol);
					
				}
				
				
				/**
				 * 通过货柜号，车牌号查询
				 *
				 */
				if(method.equals("SearchResourceByEquipment")){
//					isMutiArray = true ;
					int type = StringUtil.getInt(request, "Type");
					String licensePlate = "";
					String trailerNo = "" ;
					String equipment_number="";
					
					equipment_number = StringUtil.getString(request, "searchValue");
					DBRow[] data=this.checkInMgrXj.getInYardEquipmentByEquipmentNumberLast(ps_id,equipment_number);
					result.add("data", data);
					
					

					
 				
				}
				/**
				 * 修改一个停车位( update | add ) entryId
				 */
				if(method.equals("ModifyResourceEquipment")){
					long equipment_id = 0l;
					long resource_id = 0l ;
					int resource_type = 0 ;
					long area_id = 0l ;
					long check_in_entry_id = 0l ;
					int search_resource_type = 0 ;
					equipment_id = StringUtil.getLong(request, "equipment_id");
					resource_id = StringUtil.getLong(request, "resource_id");
					resource_type = StringUtil.getInt(request, "resource_type");
					area_id = StringUtil.getLong(request, "area_id");
					check_in_entry_id = StringUtil.getLong(request, "check_in_entry_id");
					search_resource_type = StringUtil.getInt(request, "search_resource_type");
					//要改变的的停车位 和 停车位上面的entryID不能为空
					if(equipment_id == 0l || resource_id == 0l){
						throw new SystemException();
					}

 					checkInMgrXj.modifySpotEquipment(adid,equipment_id,resource_type, resource_id);

 					
				}
				if(method.equals("BatchModifyResourceEquipment")){
 					String value = StringUtil.getString(request, "datas");
		  			JSONObject jsons = new JSONObject(value);
		  			long resource_id=StringUtil.getJsonLong(jsons, "resource_id");
		  			int resource_type=StringUtil.getJsonInt(jsons, "resource_type");
		  			JSONArray jsonArray =StringUtil.getJsonArrayFromJson(jsons,"equipments");
		  			if(jsonArray!=null){
		  				for(int i = 0; i < jsonArray.length(); i++) {
		  					JSONObject equipmentObject = jsonArray.optJSONObject(i); 
		  					long equipment_id=StringUtil.getJsonLong(equipmentObject, "equipment_id");
		  					int search_resource_type = StringUtil.getJsonInt(equipmentObject, "search_resource_type");
		  					long check_in_entry_id=StringUtil.getJsonLong(equipmentObject, "check_in_entry_id");
		  					checkInMgrXj.modifySpotEquipment(adid,equipment_id,resource_type, resource_id);	
						}
		  				
		  			}
 					
				}
				/**
				 * 车牌号自动完成
				 */
				if(method.equals("getSearchCheckInCTNRJSON")){
					String trailerNo="" ;
				
					trailerNo = StringUtil.getString(request, "trailerNo");
 					DBRow[] data=checkInMgrXj.getSearchCheckInCTNRJSON(request, trailerNo);
 					if(data == null){
						throw new CheckInNotFoundException();
					}
 					result.add("data", data);
				}
				/**
				 * 货柜号自动完成
				 */
				if(method.equals("getSearchCheckInLicensePlateJSON")){
					String license_plate="";
					
					license_plate = StringUtil.getString(request, "license_plate");
					DBRow[] data=checkInMgrXj.getSearchCheckInLicensePlateJSON(request, license_plate);
					if(data == null){
						throw new CheckInNotFoundException();
					}
 					result.add("data", data);
				}
				/**
				 * 提交审核列表
				 */
				if(method.equals("patrolApproveList")){
					DBRow[] data=checkInMgrXj.verifySpot(ps_id, null);
					long count = checkInMgrXj.findStoragePatrolTimeByPsId(ps_id);
					if(data == null){
						throw new CheckInNotFoundException();
					}
					result.add("count", count);
 					result.add("resources", data);
				}
				
				/**
				 * 提交审核
				 */
				if(method.equals("submitPatrolApprove")){
					long id = checkInMgrXj.AndroidcheckInPatrolApprove(request);
 					result.add("data", id);
				}
				/**
				 * createEntryId(有货柜，系统无记录)
				 */
				if(method.equals("createEquipment")){
					long id=checkInMgrXj.createEntryId(request);
 					result.add("data", id);
				}
				
				/**
				 * clear设备
				 */
				if(method.equals("clearEquipment")){
					long equipment_id = StringUtil.getLong(request, "equipment_id");
					this.checkInMgrXj.delSpaceResourcesRelation(SpaceRelationTypeKey.Equipment, equipment_id, adid);
 					result.add("data", true);
				}
				/**
				 * confirm 停车位
				 */
				if(method.equals("confirmPatrolResource")){

//					long yc_id=StringUtil.getLong(request, "yc_id");
					long resource_id=StringUtil.getLong(request, "resource_id");
					long area_id=StringUtil.getLong(request, "area_id");
  					int resource_type=StringUtil.getInt(request, "resource_type");;
					
					result = checkInMgrXj.confirmPatrolResource(resource_id,resource_type,area_id,adminLoggerBean);
  					
				}
			
				/**
				 * againPatrolSpot
				 */
				if(method.equals("againPatrolSpot")){
					long area_id=StringUtil.getLong(request, "areaId");
					result = checkInMgrXj.AgainPatrolSpot(ps_id, area_id);
					
				}
				/**
				 * againPatrolDock
				 */
				if(method.equals("againPatrolDock")){
					long area_id=StringUtil.getLong(request, "areaId");
					result = checkInMgrXj.AgainPatrolDock(ps_id, area_id);
					
				}
				/**
				 * againPatrolAll
				 */
				if(method.equals("againPatrolAll")){
					//long area_id=StringUtil.getLong(request, "areaId");
					checkInMgrXj.AgainPatrolDock(ps_id, 0);
					checkInMgrXj.AgainPatrolSpot(ps_id, 0);
					
					
				}
				/**
				 *查询需要checkout的设备
				 */
				if(method.equals("equipmentNeedCheckOutList")){
//					isMutiArray = true ;
					DBRow[] datas =	checkInMgrXj.findCheckInPatrolApproveDetails(ps_id, 0, null);
 					result.add("result", datas);
				}
				/**
				 *单个checkout
				 */
				if(method.equals("singleCheckOut")){
//					isMutiArray = true ;
					long equipment_id = StringUtil.getLong(request,"equipment_id");
					String noteSingle = StringUtil.getString(request,"note");
					checkInMgrXj.verifyCheckOutSingle(equipment_id, noteSingle, adminLoggerBean);
					DBRow[] datas =	checkInMgrXj.verifySpot(ps_id, null);
 					result.add("resources", datas);
				}
				/**
				 *批量checkout
				 */
				if(method.equals("multipleCheckOut")){
//					isMutiArray = true ;
					String value = StringUtil.getString(request, "datas");
					JSONObject jsons = new JSONObject(value);
					JSONArray jsonArray =StringUtil.getJsonArrayFromJson(jsons,"equipment_ids");
					if(jsonArray!=null){
		  				for(int i = 0; i < jsonArray.length(); i++) {
		  					JSONObject spotObject = jsonArray.optJSONObject(i); 
		  					long equipment_id=StringUtil.getJsonLong(spotObject, "equipment_id");
		  					String noteSingle=StringUtil.getJsonString(spotObject, "note");
		  					checkInMgrXj.verifyCheckOutSingle(equipment_id,  noteSingle, adminLoggerBean);
		  				}
					}
					DBRow[] datas =	checkInMgrXj.verifySpot(ps_id, null);
 					result.add("resources", datas);
				}
				if(method.equals("updateEquipment")){
//					isMutiArray = true ;
					this.checkInMgrXj.updateEquipment(request);
				}
				if(method.equals("againPatrolResource")){
					long area_id = StringUtil.getLong(request,"area_id");
					long resource_id = StringUtil.getLong(request,"resource_id");
					int resource_type = StringUtil.getInt(request,"resource_type");
					result = this.checkInMgrXj.againPatrolResource(resource_id, resource_type, area_id,adminLoggerBean);
					
				}

	  		}catch (AppVersionException e) {
	  			ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
 			}catch (CheckInNotFoundException e) {
	  			ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
//			if(!isMutiArray){
//				throw new JsonException(new JsonObject(result));
//			}else{
				throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
//			}
	}
	
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
 		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("Version", StringUtil.getString(request, "Version"));
 		return result ;
	}
	private DBRow[] fixResourcesArea(DBRow[] rows) throws IOException{
		String patrolDone ="patrolDone";
		 if(rows.length>0){
			for (int i = 0; i < rows.length; i++) {
				if(rows[i].get("patrol_time", "").equals("") && rows[i].get("area_id", 0)>0){
					patrolDone="";
				}
				if(rows[i].get("area_id", 0)==0){
					rows[i].add("patrol_time", patrolDone);
				}
			 }
		 }
		return rows;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setCheckInMgrXj(CheckInMgrIfaceXj checkInMgrXj) {
		this.checkInMgrXj = checkInMgrXj;
	}
	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}
	
}
