package com.cwc.service.android.action.zwb;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;


public class AndroidSeachProductAction extends ActionFatherController{
	
	private ProductLineMgrIfaceZwb productLineMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	     
		String xml = "";
		response.setContentType("application/xml");		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		xml = getPost(inputStream);
		////system.out.println("这是xml:"+xml);
		String prodcutLine	= getSampleNode(xml,"ProductLine");
		String catalogOne	= getSampleNode(xml,"CatalogOne");
		String catalogTwo	= getSampleNode(xml,"CatalogTwo");
		String catalogThree	= getSampleNode(xml,"CatalogThree");
		String seachType	= getSampleNode(xml,"SeachType");
		String seachValue	= getSampleNode(xml,"SeachValue");
		DBRow[] row=null;
		if(!catalogThree.equals("")){
			row=this.productLineMgrZwb.seachProductByCatalogIdType(Long.parseLong(catalogThree),seachType,seachValue);
		}else if(!catalogTwo.equals("")){
			row=this.productLineMgrZwb.seachProductByCatalogIdType(Long.parseLong(catalogTwo),seachType,seachValue);
		}else if(!catalogOne.equals("")){
			row=this.productLineMgrZwb.seachProductByCatalogIdType(Long.parseLong(catalogOne),seachType,seachValue);
		}else if(!prodcutLine.equals("")){
			row=this.productLineMgrZwb.selectProductByLineId(Long.parseLong(prodcutLine),seachType,seachValue);
		}else{
			//只按 条件查询
			row=this.productLineMgrZwb.seachProductBySeachValue(seachType, seachValue);
		}
		
//		//system.out.println(prodcutLine);
//		//system.out.println(catalogOne);
//		//system.out.println(catalogTwo);
//		//system.out.println(catalogThree);
//		//system.out.println(seachType);
//		//system.out.println(seachValue);

		responseXML(row,response);
		
	}

	//接收手机传过来的xml
	private String getPost(InputStream inputStream)throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null; 
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try{
			    	input.close();
			    }catch (Exception f){
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData,"UTF-8"));
	}
	
	//数据组成xml 返回到手机
	private void responseXML(DBRow[] row,HttpServletResponse response) throws Exception{
//		//system.out.println("返回的总条目数："+row.length);
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		for(int i=0;i<row.length;i++){
			sb.append("<Product>");
				sb.append("<Pcid>"+row[i].get("pc_id",0l)+"</Pcid>");
				sb.append("<Pname>"+row[i].getString("p_name")+"</Pname>");
				sb.append("<CatalogId>"+row[i].get("catalog_id",0l)+"</CatalogId>");
				sb.append("<CatalogName>"+row[i].getString("title")+"</CatalogName>");
			sb.append("</Product>");
		}				
		////system.out.println(sb.toString());		
		try {
		   response.setCharacterEncoding("UTF-8");
		   OutputStream out = response.getOutputStream();
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
	
	public static String getSampleNode(String xml,String name) throws Exception{
			String xmlSplit1[] = xml.split("<"+name+">");
			if(xmlSplit1 != null && xmlSplit1.length > 0){
				String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");
				if(xmlSplit2 != null && xmlSplit2.length > 0){
					return(xmlSplit2[0]);
				}
			}
			return "" ;	
	}


	
	
	public void setProductLineMgrZwb(ProductLineMgrIfaceZwb productLineMgrZwb) {
		this.productLineMgrZwb = productLineMgrZwb;
	}

}
