package com.cwc.service.android.action.zwb;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;



public class AndroidSeachProductByIdAction extends ActionFatherController{
	
	private ProductLineMgrIfaceZwb productLineMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		String xml = "";
		response.setContentType("application/xml");	
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		xml = getPost(inputStream);
		String productId = StringUtil.getSampleNode(xml,"ProductId");
		DBRow row = new DBRow();
		  row = productLineMgrZwb.selectProductByProductId(Long.parseLong(productId));
		////system.out.println(productId);
		responseXML(row,response);
		
	}
	
	
	//数据组成产品线xml 返回到手机
	private void responseXML(DBRow row,HttpServletResponse response) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.append("<Product>");
			sb.append("<Name>"+row.getString("p_name")+"</Name>");
			sb.append("<Pcode>"+row.getString("p_code")+"</Pcode>");
			sb.append("<Length>"+row.get("length",0d)+"</Length>");
			sb.append("<Height>"+row.get("heigth",0d)+"</Height>");
			sb.append("<Width>"+row.get("width",0d)+"</Width>");
			sb.append("<Weight>"+row.get("weight",0d)+"</Weight>");
			sb.append("<UnitPrice>"+row.get("unit_price",0d)+"</UnitPrice>");
			sb.append("<UnitName>"+row.getString("unit_name")+"</UnitName>");
			sb.append("<CatalogTitle>"+row.getString("title")+"</CatalogTitle>");
			sb.append("<Volume>"+row.get("volume",0d)+"</Volume>");
			sb.append("<Pid>"+row.get("pc_id",0l)+"</Pid>");
			sb.append("</Product>");
			////system.out.println(sb);
		try {
		   response.setCharacterEncoding("UTF-8");
		   OutputStream out = response.getOutputStream();
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
	
	//接收手机传过来的xml
	private String getPost(InputStream inputStream)throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null; 
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try{
			    	input.close();
			    }catch (Exception f){
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData,"UTF-8"));
	}

	public void setProductLineMgrZwb(ProductLineMgrIfaceZwb productLineMgrZwb) {
		this.productLineMgrZwb = productLineMgrZwb;
	}

}
