package com.cwc.service.android.action.zwb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.android.SerchLoadingNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;

public class CheckInAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private AdminMgrIFaceZJ adminMgrZJ;
	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
   		try{
 			DBRow fromAndroid = getJsonObjectFromRequest(request);
			if(!fromAndroid.getString("Version").equals(AndroidUtil.getVersion())){
				throw new AppVersionException();
			}
			//首先进行登录
			DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
			if(login == null){
				throw new MachineUnderFindException();
			}
			//模拟登陆
			AdminLoginBean adminLoggerBean = AndroidPermissionUtil.androidSetSession(login, request);
			long ps_id = adminLoggerBean.getPs_id() ;
			
			//根据一个Loading# 查询是否有数据
			if(fromAndroid.getString("Method").equals("SearchLoading")){
 				DBRow queryData = getLoadingSearchFromRequest(request,adminLoggerBean) ;
				DBRow returnData =	checkInMgrZwb.androidGetLoading(queryData,request);
				if(returnData != null){
					result.add("data", new DBRow[]{returnData});
				}else{
					throw new SerchLoadingNotFoundException();
				}
				
 			}
			if(fromAndroid.getString("Method").equals("GetLoading")){
 				String area_name=StringUtil.getString(request,"area_name");
				String title_name=StringUtil.getString(request,"title_name");
				long type=StringUtil.getLong(request,"type");
				String search_number=StringUtil.getString(request,"search_number");
				int mark=StringUtil.getInt(request,"mark");
 				DBRow[] returnData =checkInMgrZwb.getLoading(area_name, title_name, type, search_number, 0, mark,ps_id);
				if(returnData != null){
					result.add("data", returnData);
				}else{
					throw new SerchLoadingNotFoundException();
				}
				
 			}
			if(fromAndroid.getString("Method").equals("GetZoneByTitle")){
 				String title_name=StringUtil.getString(request,"title_name");
				DBRow[] returnData =checkInMgrZwb.findZoneByTitleName(title_name,ps_id);
				if(returnData != null){
					result.add("data", returnData);
				}else{
					throw new SerchLoadingNotFoundException();
				}
				
 			}
			//得到具体的数据(包含有windowCheckIn item) 占用 停车位  和占用门
			if(fromAndroid.getString("Method").equals("GetSpotArea")){
 			    long entry_id=StringUtil.getLong(request,"entry_id");
			    int request_type = StringUtil.getInt(request, "request_type");
  				DBRow data = checkInMgrZwb.androidGetSpotArea(ps_id,entry_id,request_type);
				result.add("data", new DBRow[]{data});
			}
		 
			 
			//index.html页面
			if(fromAndroid.getString("Method").equals("CheckInMainList")){
				DBRow pageData = getPageDataFromRequest(request) ;
				PageCtrl pageCtrl =  new PageCtrl();
				pageCtrl.setPageSize(pageData.get("pageSize", 10));
				pageCtrl.setPageNo(pageData.get("pageNo", 0));
				DBRow[] datas = checkInMgrZwb.selectAllMain(0l,pageCtrl);
				result.add("data", datas);
			}
			//根据一个ID判断是否有这条记录
			if(fromAndroid.getString("Method").equals("CheckInHasRecode")){
 				DBRow ids = getDloId(request) ;
  				DBRow row = checkInMgrZwb.countDloId(ids.get("dlo_id", 0l),adminLoggerBean);
  				result.add("data", row);
			}
			//得到具体的数据(包含有windowCheckIn item)
			if(fromAndroid.getString("Method").equals("CheckInGetDetail")){
 				DBRow ids = getDloId(request) ;
  				DBRow data = checkInMgrZwb.getDoorOrLocationOccupancyMainById(ids.get("dlo_id", 0l) , request );
				result.add("data", data);
			}
			//根据主单据号查询 没有释放的门或停车位,shuttle的时候 入口
			
			if(fromAndroid.getString("Method").equals("GetDoorOrlocationByMainId")){
 				long dlo_id=StringUtil.getLong(request, "dlo_id");
 				String ctnr=StringUtil.getString(request, "ctnr");//login.get("ctnr", "");
				DBRow[] row = checkInMgrZwb.androidGetDoorOrlocationByMainId(dlo_id,ps_id,adminLoggerBean,ctnr);
				 
				result.add("data", row);      //仓库可用门
				
			}
			//获得checkout数据 yuanxinyu
			if(fromAndroid.getString("Method").equals("GetCheckOutData")){
  				long entryId = StringUtil.getLong(request, "entry_id");
 				checkInMgrZwb.checkEntryPermission(entryId, adminLoggerBean);
  				checkInMgrZr.checkGateCheckOutEntryHasLeft(entryId);
    			DBRow[] rows = checkInMgrZwb.getCheckOutEntryUseResource(entryId);
   				 
				result.add("datas", rows);    
				result.add("files", checkInMgrZr.getCheckInFile(entryId, FileWithCheckInClassKey.PhotoGateCheckOut));
 			}
			//查询不存在，选择新建一条
			if(fromAndroid.getString("Method").equals("creatNewEquipmentOnCheckOut")){
				DBRow data = checkInMgrWfh.creatNewEquipmentOnCheckOut(request);
				result.add("data", data);
			}
		 
			//获得checkout search数据 yuanxinyu
			if(fromAndroid.getString("Method").equals("GetCheckOutDataSearch")){
 				String trailerNo = StringUtil.getString(request, "trailerNo");
   				DBRow row = checkInMgrZwb.findResourceByEquipment(request,trailerNo);
				result.add("datas", row);      //仓库可用门
			}
			if(fromAndroid.getString("Method").equalsIgnoreCase("GetCheckOutDataSearchByEntry")){
				long entry_id = StringUtil.getLong(request, "entry_id");
				List<DBRow> arrayList = checkInMgrZr.searchGateCheckOutEquipmentByEntryId(entry_id, adminLoggerBean);
				result.add("datas", arrayList.size() > 0 ? arrayList.get(0): new DBRow());      //仓库可用门

			}
			//获得checkout save方法 yuanxinyu
			if(fromAndroid.getString("Method").equals("GetCheckOutDataSave")){
				/*
				isMutiArray = true ;
				String jsonString = StringUtil.getString(request, "jsonString");
				com.fr.base.core.json.JSONArray jsonArray = new com.fr.base.core.json.JSONArray(jsonString);
				long adid=login.get("adid",0l);
				String doorNames = "";
				String spotNames = "";
				String equipmentNos = "";
				String seals = "";
				int entry_id = 0;
				for(int i = 0 ; i < jsonArray.length() ; i++){
					com.fr.base.core.json.JSONObject json = jsonArray.getJSONObject(i);
					entry_id = json.getInt("entry_id");
					int equipment_id = json.getInt("equipment_id"); //设备ID
					String seal = json.getString("seal");
					String equipment_no = json.getString("equipment_no");
					equipmentNos += equipment_no + ",";
					seals = seal;
				 
					if(json.getInt("resources_type") == 1)
					{
						doorNames = json.getString("equipment_type_msg") + "-" + json.getString("door_name") + ",";
					}
					else
					{
						spotNames = json.getString("equipment_type_msg") + "-" + json.getString("spot_name") + ",";
					}
					
					doorNames = doorNames.substring(0,doorNames.length()-1);
					spotNames = spotNames.substring(0,spotNames.length()-1);
					// 调用接口 更新占用资源信息。
					this.checkInMgrZwb.checkOutTheResource(equipment_id, adid, entry_id, request);
					
				}
				equipmentNos = equipmentNos.substring(0,equipmentNos.length() - 1);
				this.checkInMgrZwb.checkOutTheLog(doorNames, spotNames, true, adid, equipmentNos, seals, entry_id, request);
				
//				result.add("datas", row);      //仓库可用门
			*/
			}
			
			//验证gps
			if(fromAndroid.getString("Method").equals("VerificationGps")){
 				String gps_tracker=StringUtil.getString(request, "gps_tracker");
				DBRow row=this.checkInMgrZwb.androidVerificationGps(gps_tracker);
				result.add("data",row);	 
 			}
			
			//添加CheckWindow detail 
			if(fromAndroid.getString("Method").equals("AddCheckWindowDetails")){
				DBRow data = getCheckWindowDetails(request);
				checkInMgrZwb.addWindowCheckInByAndroid(data, request);
  			}
			
		 
			 //android 扫门 选择停车位
			if(fromAndroid.getString("Method").equals("AndroidCheckInWindowStop")){
			    long id=this.checkInMgrZwb.androidCheckInWindowStop(request);
				result.add("data", id);
			}
			
		 
			if(fromAndroid.getString("Method").equals("EntryTaskEquipment")){
 				long entry_id = StringUtil.getLong(request, "dlo_id") ;
				
			}
			//check out 扫描entry_id 查询信息
			if(fromAndroid.getString("Method").equals("CheckOutSearchEntryId")){
 				DBRow datas=checkInMgrZwb.androidCheckOutSearchEntryId(request);
				if(datas == null || datas.getFieldNames().size() < 1 ){
   					throw new CheckInNotFoundException();
   				}
				result.add("data", datas);
			}
			
			
			//根据货柜号查询占用的门和停车位
			if(fromAndroid.getString("Method").equals("GetUseDoorOrSpot")){
 			    String container_no=StringUtil.getString(request, "container_no");
				DBRow datas= checkInMgrZwb.androidGetUseDoorOrSpot(container_no,adminLoggerBean.getPs_id());	
				result.add("data", datas);
			}
			
			//pick up 搜索 ctnr方法  得到 门或 停车位方法
			if(fromAndroid.getString("Method").equals("PickUpSearchNumberGetDoorOrLocation")){
 			    String container_no=StringUtil.getString(request, "container_no");
				DBRow datas= checkInMgrZwb.androidFindSpotOrDoorByCtnr(container_no,adminLoggerBean.getPs_id());
				result.add("data", datas);
			}
			
			//********************************巡逻人员用 释放门  和停车位
			if(fromAndroid.getString("Method").equals("MoveToSpot")){
 				long info_id=StringUtil.getLong(request,"info_id");
	 			long spot_id=StringUtil.getLong(request,"spot_id");
	 			long equipment_id = StringUtil.getLong(request, "equipment_id");
				String spot_name = StringUtil.getString(request, "spot_name");
				long adid = adminLoggerBean.getAdgid();	
				DBRow datas= checkInMgrZwb.androidMoveToSpot(info_id, equipment_id, spot_id, adid, spot_name);// (info_id,spot_id,adid,spot_name);
				if(datas != null && datas.get("flag",0) == 1){ //1表示Spot已经被占用了
					 throw new SpotHasUsedException();
				}
			}
			
			//巡逻人员释放停车位 占用门
			if(fromAndroid.getString("Method").equals("MoveToDoor")){
 				long adid= adminLoggerBean.getAdid();
				DBRow datas= checkInMgrZwb.androidMoveToDoor(request, adid);
				if(datas != null && datas.get("flag",0) == 1){ //1表示门已经被保留了
					 throw new DoorHasUsedException();
				}
			}
			
			if(fromAndroid.getString("Method").equals("selectGateCheckIn")){
 				PageCtrl pc = new PageCtrl();
				int pageSize = StringUtil.getInt(request, "pageSize");
				int pageNo = StringUtil.getInt(request, "pageNo");
				pc.setPageSize(pageSize);
				pc.setPageNo(pageNo);
				DBRow[] datas= checkInMgrZwb.selectGateCheckIn(adminLoggerBean.getPs_id(),pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(fromAndroid.getString("Method").equals("selectWareHouseCheckIn")){
 				PageCtrl pc = new PageCtrl();
				int pageSize = StringUtil.getInt(request, "pageSize");
				int pageNo = StringUtil.getInt(request, "pageNo");
				pc.setPageSize(pageSize);
				pc.setPageNo(pageNo);
				DBRow[] datas= checkInMgrZwb.selectWareHouseCheckIn(adminLoggerBean.getPs_id(),pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			if(fromAndroid.getString("Method").equals("selectWareHouseNumberProcessing")){
 				PageCtrl pc = new PageCtrl();
				int pageSize = StringUtil.getInt(request, "pageSize");
				int pageNo = StringUtil.getInt(request, "pageNo");
				pc.setPageSize(pageSize);
				pc.setPageNo(pageNo);
				DBRow[] datas= checkInMgrZwb.selectWareHouseNumberProcessing(adminLoggerBean.getPs_id(),pc);
				result.add("data", datas);
				result.add("total", pc.getPageCount());
			}
			//**************************************************
			
			/**
			 * 装货的时候某个门下最后，最后一个单据提示relaseDoor
			 */
			if(fromAndroid.getString("Method").equals("CloseLoadRelaseDoor")){
				DBRow data = new DBRow();
				data.add("dlo_detail_id", StringUtil.getLong(request, "dlo_detail_id"));
				data.add("entry_id", StringUtil.getLong(request, "entry_id"));
				data.add("equipment_id", StringUtil.getLong(request, "equipment_id"));
				checkInMgrZwb.loadEntryLastReleaseDoor(data, adminLoggerBean);
			}
			/**
			 * 装货的时候 提示停留或者离开 选择后提交的接口
			 */
			if(fromAndroid.getString("Method").equals("CloseLoadStayOrLeave")){
				DBRow data = new DBRow();
				data.add("entry_id", StringUtil.getLong(request, "entry_id"));
				data.add("is_leave", StringUtil.getInt(request, "is_leave"));
				data.add("dlo_detail_id", StringUtil.getInt(request, "dlo_detail_id"));
				data.add("equipment_id", StringUtil.getLong(request, "equipment_id"));
				checkInMgrZwb.loadEntryLastStayOrLeave(data, adminLoggerBean);
			}
			if(fromAndroid.getString("Method").equals("GetEntryFreightTerm")){
				long entryId =  StringUtil.getLong(request, "entry_id");
				DBRow[] details =  checkInMgrZwb.getEntryDetailsByEntryId(entryId);
				fixEntryFreightTermEntryDetails(details);
				result.add("datas",details);
			}
		}catch(CheckInEntryIsLeftException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInEntryIsLeftException;
		}catch(EquipmentHadOutException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.EquipmentHadOutException;
		}catch(NoRecordsException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.NoRecordsException;
		}catch(SpotHasUsedException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.SpotHasUsedException;
		}catch(DoorHasUsedException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.DoorHasUsedException;
		}catch (AppVersionException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.AppVersionException;		
		}catch (NoPermiessionEntryIdException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoPermiessionEntryIdException;	
 		}catch (CheckInNotFoundException  e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInNotFound;
		}catch (SerchLoadingNotFoundException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SerchLoadingNotFoundException;
 		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		////system.out.println(StringUtil.convertDBRowsToJsonString(result));
 		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));

	}
	/**
	 * checkout接口验证
	 * @author Yuanxinyu
	 * @param rows
	 */
	private DBRow[] fixCheckOut(DBRow[] rows)
	{
		if(rows != null && rows.length >0)
		{
			for(int i = 0 ; i < rows.length ; i++)
			{
				rows[i].remove("check_in_time");
				rows[i].remove("check_in_entry_id");
				rows[i].remove("check_out_entry_id");
				rows[i].remove("equipment_purpose");
				rows[i].remove("equipment_status");
				rows[i].remove("seal_delivery");
				rows[i].remove("phone_equipment_number");
				rows[i].remove("srr_id");
				rows[i].remove("relation_type");
				rows[i].remove("relation_id");
				rows[i].remove("occupy_status");
				rows[i].remove("module_type");
			}
			return rows;
		}
		return rows;
	}
	private void fixEntryFreightTermEntryDetails(DBRow[] details){
		if(details != null && details.length > 0){
			for(int index = 0 , count = details.length ; index < count; index++ ){
				DBRow detail = details[index];
				DBRow temp = new DBRow();
				temp.add("freight_term", detail.getString("freight_term"));
				temp.add("staging_area_id", detail.getString("staging_area_id"));
				temp.add("number", detail.getString("number"));
				temp.add("number_type", detail.getString("number_type"));
				temp.add("customer_id", detail.getString("customer_id"));
				details[index] = temp ;
			}
		}
	}
 
	 
	
 
 	
	public static String getJSONObject(Object data) throws Exception{
		String value = "";
		if(data instanceof DBRow){
			DBRow dbrowData = (DBRow) data ;
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData.getFieldNames();
			for(String fileName : filedNames){
				Object inner = dbrowData.getValue(fileName);
					if(inner instanceof DBRow[]){
						json.put(fileName.toLowerCase(), getJSON(inner));
					}else if(inner instanceof DBRow){
						json.put(fileName.toLowerCase(), getJSON(inner));
	 				}else{
	 					
	 					json.put(fileName.toLowerCase(), inner);
	 				}
				 
				
			}
			value = json.toString();
		}
		if(data instanceof DBRow[]){
			DBRow[] dbrows = (DBRow[]) data ;
			JSONArray array = new JSONArray();
			for(DBRow temp : dbrows){
				array.add(getJSON(temp));
			}
			value = array.toString();
 		}
		
		return value ;
  	}
	 
	public static Object getJSON(Object data) throws Exception{
		Object obj = new Object();
		if(data instanceof DBRow[]){
			DBRow[] datas = (DBRow[]) data ;
			JSONArray  jas = new JSONArray();
			for(DBRow temp : datas){
				jas.add(getJSON(temp));
			}
			obj = jas ;
		}else if(data instanceof DBRow){
			DBRow dbrowData = (DBRow)data ;
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData.getFieldNames();
			for(String fileName : filedNames){
				json.put(fileName.toLowerCase(), getJSON(dbrowData.getValue(fileName)));
			}
			obj = json ;
		}else{
			obj  = data ;
		}
		 
		return obj ;
	}
	
 
	
	
	
	private DBRow getCheckWindowDetails(HttpServletRequest request) throws Exception{
		DBRow result = new DBRow();
		String value = StringUtil.getString(request, "value");
		//{"datas":[{"pager":1,"inform":"100263","email":1,"door":"Dock3","load":"1212","comment":"sadsadasd","type":"Load#:","sns":0},{"pager":0,"inform":"100263","email":1,"door":"Dock3","load":"3121","comment":"sdsd","type":"CTN#:","sns":1}],"check_in_id":"100015"}
		JSONObject json = JSONObject.fromObject(value);
 		JSONArray details = json.getJSONArray("datas");

		long dlo_id = json.getLong("check_in_id");
		if(details != null && details.size() > 0 ){
			List<DBRow> detailDatas = new ArrayList<DBRow>();
			for(int index = 0 , count = details.size() ; index < count ; index++ ){
				JSONObject detail = details.getJSONObject(index);
				int pager = detail.getInt("pager");
				int email = detail.getInt("email");
				int sns = detail.getInt("sns");
				String userIds = detail.getString("inform");
				//String beizu = detail.getString("comment");
				String type  = detail.getString("type");
				//String comment = detail.getString("comment");
				long doorId = detail.getLong("door");
				long loadId = detail.getLong("load");
				
				DBRow data = new DBRow();
				data.add("pager", pager);
				data.add("email", email);
				data.add("sns", sns);
				data.add("userIds", userIds);
			
			//	data.add("beizu", beizu);
				data.add("doorId", doorId);
				data.add("loadId", loadId);
				data.add("type", type);
			//	data.add("comment", comment);
				detailDatas.add(data);
			}
			result.add("details", detailDatas);
		}
		result.add("dlo_id", dlo_id);
		return result ;
	}
	
	private DBRow getDloId(HttpServletRequest request) throws Exception{
		DBRow returnRow = new DBRow();
		returnRow.add("dlo_id", StringUtil.getLong(request, "dlo_id"));
		returnRow.add("zone_id", StringUtil.getLong(request, "zone_id"));
 		return returnRow;
	}
	private DBRow getPageDataFromRequest(HttpServletRequest request ) throws Exception {
		DBRow returnRow = new DBRow();
		returnRow.add("pageSize", StringUtil.getInt(request, "pageSize"));
		returnRow.add("pageNo", StringUtil.getInt(request, "pageNo"));
		return returnRow;
	}
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
 		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("Version", StringUtil.getString(request, "Version"));
 		return result ;
	}
	private DBRow getLoadingSearchFromRequest(HttpServletRequest request,AdminLoginBean adminLoggerBean) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("number_type", StringUtil.getString(request, "number_type"));
		result.add("search_number", StringUtil.getString(request, "search_number"));
		result.add("ps_id",  adminLoggerBean.getPs_id());
 		return result ;
	}
	private DBRow addGateCheckInFromRequest(HttpServletRequest request,DBRow loginBean) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("gate_driver_first_name", StringUtil.getString(request, "gate_driver_first_name"));
		result.add("gate_driver_last_name", StringUtil.getString(request, "gate_driver_last_name"));
		result.add("liscense_plate", StringUtil.getString(request, "liscense_plate"));
		result.add("appointment_time", StringUtil.getString(request, "appointment_time"));
		result.add("driver_liscense", StringUtil.getString(request, "driver_liscense"));
		result.add("gate_container_no", StringUtil.getString(request, "gate_container_no"));
		result.add("company_name", StringUtil.getString(request, "company_name"));
		result.add("gps_tracker", StringUtil.getLong(request, "gps_tracker"));
		result.add("yc_id", StringUtil.getLong(request, "yc_id"));
		result.add("ps_id",  loginBean.get("ps_id", 0l));
		result.add("adid", StringUtil.getString(request, "adid"));
		result.add("rel_type", StringUtil.getString(request, "rel_type"));
		result.add("status", StringUtil.getString(request, "status"));
		
		result.add("type", StringUtil.getString(request, "type"));
		result.add("bill_id", StringUtil.getString(request, "bill_id"));
		result.add("door_id", StringUtil.getString(request, "door_id"));
		
 		return result ;
	}
	
	
	
	
	
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
