package com.cwc.service.android.action.zwb;


import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.UserIsOnLineCantLoginException;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.AdminMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;


public class AndroidLoginUserAction extends ActionFatherController{
	
	private static final int CHECKONLINE = 1 ;		//需要检查是否在线
	private static final int UNCHECKONLINE = 0 ;	//不需要检查是否在线

	private AdminMgrIfaceZwb adminMgrZwb;
	private CheckInMgrIfaceZr checkInMgrZr ;
	private AdminMgrIFaceZJ adminMgrZJ;
	private AccountMgrIFaceSbb accountMgr;

    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
 	    DBRow returnRow = new DBRow();
	    returnRow.add("ret", 0);
	    returnRow.add("err", 0);
	    try{
	    	////system.out.println("Login ... in.... ");
	    	String userName	= StringUtil.getString(request, "LoginAccount");
			String userPwd= StringUtil.getString(request,"Password");
			int ischeckonline = StringUtil.getInt(request, "ischeckonline");
			String appVersion = StringUtil.getString(request, "version");
			String serverVersion = AndroidUtil.getVersion()	;
			String method = StringUtil.getString(request, "Method");
			if(method.equalsIgnoreCase("Login")){
				if(!StringUtil.isBlank(serverVersion) && !StringUtil.isBlank(appVersion) && !appVersion.equalsIgnoreCase(serverVersion)){
					throw new AppVersionException();
				}
				DBRow loginRow = adminMgrZJ.MachineLogin(userName, userPwd);
 				if(loginRow != null ){
					Long user_id= loginRow.get("adid", 0l);	
					String account = loginRow.getString("account");
					if(ischeckonline == CHECKONLINE){
						boolean isOnline = checkInMgrZr.checkAndroidLoginIsOnLine(account);
						if(isOnline){
							throw new UserIsOnLineCantLoginException();
						}
					}
					AndroidPermissionUtil.androidSetSession(loginRow, request);
					//checkInMgrZr.refreshLastRefreshTime(user_id);
					returnRow.add("ret", 1);
					returnRow.add("err", 0);
					DBRow data = new DBRow();
 
					data.add("departments", (DBRow[])loginRow.get("departments", new Object()));
					data.add("warehouses", (DBRow[])loginRow.get("warehouses", new Object()));
					data.add("userid", user_id);
 					data.add("username", loginRow.getString("employe_name"));
 					data.add("portraits", (DBRow[])loginRow.get("portraits", new DBRow[0]));
 					data.add("perPortraits", (DBRow[])loginRow.get("perPortraits", new DBRow[0]));
 					returnRow.add("data",new DBRow[] {data});
 					DBRow[] permissions = accountMgr.getUserMobileMenuRecordByAdid(user_id);
 					returnRow.add("permissions", permissions);

				}
			}
			if(method.equalsIgnoreCase("RefreshCookie")){
 				DBRow data = getRequestData(request);
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);
			}
		 
			
	    }catch(UserIsOnLineCantLoginException e){
	    	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.UserIsOnLineCantLoginException);
	    }catch (AccountOrPwdIncorrectException e) {
	    	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.LOGINERROR );
	    }catch (AppVersionException e) {
	    	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.AppVersionException);
 		}catch (AccountNotPermitLoginException e) {
 		   	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.AccountNotPermitLoginException);
 		}
	    catch (Exception e) {
 			 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.FAIL);
		}
	     ////system.out.println(StringUtil.convertDBRowsToJsonString(returnRow));
		throw new JsonException(StringUtil.convertDBRowsToJsonString(returnRow));
	}

	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
		 }
 	    return returnRow;
	}
	
	public void setAdminMgrZwb(AdminMgrIfaceZwb adminMgrZwb) {
		this.adminMgrZwb = adminMgrZwb;
	}


	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	

}
