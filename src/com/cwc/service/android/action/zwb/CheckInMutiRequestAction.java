package com.cwc.service.android.action.zwb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.CheckInMgrZr;
import com.cwc.app.api.zr.LoadPaperWorkMgrZr;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.LoadNumberNotExistException;
import com.cwc.app.exception.android.SerchLoadingNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.VerifyShippingCTNRException;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.LoadPaperWorkMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * checkin 提交的数据带有文件借口
 * @author zhangrui
 *
 */
public class CheckInMutiRequestAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	private AdminMgrIFaceZJ adminMgrZJ;
	private LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr ;
	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow row = getJsonObjectMutiRequest(request);							// 基本的数据
   				if(!row.getString("Version").equals(AndroidUtil.getVersion())){
   					throw new AppVersionException();
   				}
				String method = row.getString("Method");
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(row.getString("LoginAccount"), row.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				//模拟登陆
				AdminLoginBean adminLoggerBean = AndroidPermissionUtil.androidSetSession(login, request);

				//添加
				if(method.equals("AddGateCheckIn")){
					//files 就是文件的数据
					String filePath = row.getString("filePath");
					DBRow[] spot=checkInMgrZwb.getUseSpot(adminLoggerBean.getPs_id());
					long yc_id=spot[0].get("yc_id", 0l);
					row.add("yc_id", yc_id);
					long  id = checkInMgrZwb.gateAdnroidCheckInAdd(row, filePath,  adminLoggerBean);
// 					DBRow returnDBRow = new DBRow();
//					returnDBRow.add("entry_id", id);
					result.add("entry_id", id);
					result.add("yc_no", spot[0].get("yc_no",""));

				}
				if(method.equals("GateCheckIn")){
					DBRow data = checkInMgrZwb.gateCheckInAdd(request,row,adminLoggerBean,true);
					//****checkout其它相同的设备 并且给新添加的entry加日志
					String equipmentids = row.getString("equipmentids");  //如果有传过来 ids 则代表要checkout其他相同的设备
					if(!"".equals(equipmentids)){
						DBRow para = new DBRow();
						para.add("gate_container_no", StringUtil.getString(request, "gate_container_no"));
						para.add("liscense_plate", StringUtil.getString(request, "liscense_plate"));
						para.add("adid",  adminLoggerBean.getAdid());
						para.add("ps_id", adminLoggerBean.getPs_id());
						para.add("equipmentIds", equipmentids);
						//给新添加的entry添加checkOut其他设备的日志
						checkInMgrWfh.addCheckOutLog(para, data.get("entry_id", 0L));
						//checkOut  其他相同的设备
						checkInMgrWfh.checkOutEquipment(data.get("entry_id", 0L),para);
					}
					//**   ENG     wangfeihu  add
					if(data != null){
						if(data.get("flag", 0) == CheckInMgrZwb.GateCheckInAddSuccess){
							result.add("resource_name",data.get("resource_name",""));
							result.add("resource_type",data.get("resource_type",""));
							long entry_id = data.get("entry_id",0l);
							result.add("entry_id",entry_id);
							result.add("prompt",data.get("prompt",""));
							DBRow[] equipments = checkInMgrZr.getEntryEquipmentsWithResources(entry_id);
							result.add("equipments", equipments);
						}
						if(data.get("flag", 0) == CheckInMgrZwb.GateCheckInNeedCheckOutSameEquipment){
							result  = data ;
						}
					}
				}
				//checkin out
				if(method.equals("CheckOut")){
					
 	/*				long infoId  	   = row.get("DLO_ID",0l);
					String ctnr_no 	   = row.getString("CTNRVAL");
					String isLive	   = row.getString("ISLIVE");
					String seal		   = row.getString("SEAL");
					long adid = login.get("adid", 0l);
					long ps_id = login.get("ps_id",0l);
					String filePath = row.getString("filePath");
					checkInMgrZwb.androidCheckInOut(request, ps_id, infoId, isLive, seal, adid, ctnr_no, filePath);*/
				 	
					DBRow result1 = checkInMgrZwb.checkOut(true,request);
					result.add("data", result1);

				}
				//close  dock close 的接口
				if(method.equals("CloseBill")){
					DBRow returnRow = checkInMgrZwb.androidUpdateDetailsNumberStatus(row, adminLoggerBean);
					result.add("data", returnRow); 		
				}
				//closeEntryDetail
				if(method.equals("CloseEntryDetail")){
					int flag = checkInMgrZwb.closeLoad(row,adminLoggerBean);
					if(flag == CheckInMgrZwb.LoadCloseNotifyInputSeal){
 						//result.add("load_bars", checkInMgrZwb.selectLoadBar()); 
						//因为在load的时候 seal 和loadBar 已经在close的时候填写了所以如果在返回的是InputSeal
						//那么就应该 当成是LoadCloseNotifyStayOrLeave，直接走下一步
						//张睿2014-10-13
						 flag = CheckInMgrZwb.LoadCloseNotifyStayOrLeave;
					}
					result.add("data", flag); 		
				}
				//Load 在exception的时候执行的操作
				if(method.equals("LoadException")){
					int flag = checkInMgrZwb.exceptionLoad(row, adminLoggerBean);
					if(flag == CheckInMgrZwb.LoadCloseNotifyInputSeal){
 						//因为在load的时候 seal 和loadBar 已经在close的时候填写了所以如果在返回的是InputSeal
						//那么就应该 当成是LoadCloseNotifyStayOrLeave，直接走下一步
						//张睿2014-10-13
						flag = CheckInMgrZwb.LoadCloseNotifyStayOrLeave;
					}
					result.add("data", flag); 
				}
				//预 填 pickup seal
				if(method.equals("findPickUpSealByEntryId")){
				    long mainId=StringUtil.getLong(request, "mainId");
				    DBRow mainRow= checkInMgrZwb.findGateCheckInById(mainId);
				    String out_seal = mainRow.get("out_seal", "");
					result.add("data",out_seal ) ;
				}
				
				//修改pickup seal
				if(method.equals("modPickUpSeal")){
					long id= checkInMgrZwb.modPickUpSeal(request);
					result.add("data", id);  
				}
				//查询load bar
				if(method.equals("selectLoadBar")){
					DBRow[] rows= checkInMgrZwb.selectLoadBar();
					result.add("data", rows);  
				}
 			}catch(OperationNotPermitException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.OperationNotPermitException;	
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;	
 			}catch (LoadNumberNotExistException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.LoadNumberNotExistException;	
 			}catch (SerchLoadingNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SerchLoadingNotFoundException;
	 		}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
	 		}catch(VerifyShippingCTNRException e){
	 			ret = BCSKey.SUCCESS ;
				err = BCSKey.FAIL;
//				result.add("flag", e.getMessage());
				result.add("err_info", e.getMessage());
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
			////system.out.println(StringUtil.convertDBRowsToJsonString(result));
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
 
	}

	/**
	 * 不管是不是带有文件的inputStream 。都正确返回数据
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private DBRow getJsonObjectMutiRequest(HttpServletRequest request) throws Exception{
		DBRow returnRow = new DBRow();
		String sessionId=request.getRequestedSessionId();
		returnRow.add("SESSION_ID", sessionId);
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}


	public void setLoadPaperWorkMgrZr(LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr) {
		this.loadPaperWorkMgrZr = loadPaperWorkMgrZr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}


	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
