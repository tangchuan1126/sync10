package com.cwc.service.android.action.zwb;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.special.SpecialAddException;
import com.cwc.app.exception.special.SpecialDelException;
import com.cwc.app.exception.special.SpecialUpdateException;
import com.cwc.app.iface.gql.TaskAndInvoiceMgrIfaceGql;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.TaskStatusKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;



public class AndroidInvoiceAndTaskAction extends ActionFatherController{
	
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgrIFaceZJ adminMgrZJ;
	private TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
			 DBRow returnRow = new DBRow();
			 returnRow.add("ret", 1);
		try {
			 DBRow request_data=this.getParamFromRequest(request);
			 String key=request_data.get("key", null);
			 if(!StringUtil.isBlank(key)){
			    	switch (key.trim()) {
					case "getAllProjectByAdid":
						DBRow[] projects=taskAndInvoiceMgrIfaceGql.androidGetProjectByAdid(request,response);
						returnRow.add("projects", projects);
						break;
//					case "getProjectInfomationByScheduleId":
//						DBRow maintasks=taskAndInvoiceMgrIfaceGql.androidMainTaskByScheduleId(request, response);
//						returnRow.add("maintasks", maintasks);
//						break;
					case "getMainTaskGroupByTime":
						DBRow maintasks=taskAndInvoiceMgrIfaceGql.androidGetMainTaskGroupByTime(request, response);
						returnRow.add("project_detail", maintasks);
						break;	
					case "getSubTaskInformationByMainId":
						DBRow subtasks=taskAndInvoiceMgrIfaceGql.androidGetSubTaskInformationByMainId(request, response);
						returnRow.add("main_detail", subtasks);
						break;	
					case "assignSubTask":
						taskAndInvoiceMgrIfaceGql.androidAssignSubTask(request, response);
						break;	
					case "getALLSpecialTaskNotice":
						DBRow[] notices=taskAndInvoiceMgrIfaceGql.androidGetNoticeSpecialTask(request, response);
						returnRow.add("notices", notices);
						break;	
					case "closeSubTask":
						DBRow row=taskAndInvoiceMgrIfaceGql.androidCloseSubTask(request, response);
						returnRow.add("mainTaskStatus", row!=null&&!row.isEmpty()?row.get("main_status", 0):TaskStatusKey.OPEN);
						break;		
					case "delSubTask":
						taskAndInvoiceMgrIfaceGql.androidBatchDelTask(request, response);
						break;	
					case "insertSubTask":
						DBRow[] subaddtasks=taskAndInvoiceMgrIfaceGql.androidBatchAddTask(request, response);
						returnRow.add("sub_tasks", subaddtasks);
						break;	
					case "updateSubTask":
						taskAndInvoiceMgrIfaceGql.androidUpdateTask(request, response);
						break;		
					default: 
						   returnRow.add("ret", 0);
						   returnRow.add("err", "key:"+key+"不存在");
					       break;	
					}
			    }
		  }
		  catch (SpecialDelException e) {
		    	 returnRow.add("ret", BCSKey.SUCCESS);
		 	     returnRow.add("err", 90);
		 	     returnRow.add("data", "Task is already started!");
		 	     returnRow.add("task_status", e.getMessage());
	 	  } 
		  catch (SpecialAddException e) {
		    	 returnRow.add("ret", BCSKey.FAIL);
		 	     returnRow.add("err", 90);
		 	     returnRow.add("data", "Unable to add, task is already closed.");
	 	  } 
		  catch (SpecialUpdateException e) {
	    	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", 90);
	 	     returnRow.add("data", "Unable to edit, task is already accepted.");
 		  } 
		  catch (AppVersionException e) {
	    	 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.AppVersionException);
 		  }catch (Exception e) {
 			 returnRow.add("ret", BCSKey.FAIL);
	 	     returnRow.add("err", BCSKey.FAIL);
		}
		throw new JsonException(StringUtil.convertDBRowsToJsonString(returnRow));
	}
	
	/**
	 * 从request中取出参数值
	 * @param request
	 * @throws IOException 
	 * @throws JSONException 
	 */
	private DBRow getParamFromRequest(HttpServletRequest request) throws Exception{
		DBRow dbRow=new DBRow();
		//Step 1
		 String result=IOUtils.toString(request.getReader());
		 if(!StringUtil.isBlank(result)){
			 JSONObject jsonData = new JSONObject(result);
			 dbRow=DBRowUtils.convertToDBRow(jsonData);
		 }
		//Step 2
		Enumeration<String> paramEnum=request.getParameterNames();
		while (paramEnum != null &&paramEnum.hasMoreElements()) {
			String key = (String) paramEnum.nextElement();
			dbRow.add(key, StringUtil.getString(request, key));
		}
		return dbRow;
	}


	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}


	public void setTaskAndInvoiceMgrIfaceGql(
			TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql) {
		this.taskAndInvoiceMgrIfaceGql = taskAndInvoiceMgrIfaceGql;
	}

}
