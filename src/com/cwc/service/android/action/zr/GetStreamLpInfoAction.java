package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 
 * 返回一个result.xml文件
 * 里面的信息主要包括
 * 	(总共已经包含的件数.还剩多少的体积)
 * @author Administrator
 *
 *	<flag>success</flag>
 *	
 */
public class GetStreamLpInfoAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	private static String preXmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data>";
	private static String aftXmlString = "</data>";
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			try{
				String lp_name = StringUtil.getString(request, "lp");
				result = androidMgrZr.getLpInfo(lp_name) ; //androidMgrZr.getLpInfo(cp_lp_id) ;
			}catch (Exception e) {
				result.add("flag", "error");
 			}
			sendResultXml(response, result);
	}
	
	public void sendResultXml(HttpServletResponse response ,DBRow result ) throws IOException {
		response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("lpInfo.xml","utf-8"));  
		StringBuffer xml = new StringBuffer(preXmlString);
		xml.append("<Flag>").append(result.getString("flag")).append("</Flag>");
		DecimalFormat format = new DecimalFormat("####.00");
		String s = format.format(result.get("left_volume",0.0d));  
		xml.append("<TypeName>").append(result.getString("type_name")).append("</TypeName>");
		xml.append("<Lp>").append(result.get("lp", 0l)).append("</Lp>");
		xml.append("<LpName>").append(result.getString("lp_name")).append("</LpName>");
		xml.append("<LeftWeight>").append(result.get("left_weight",0.0d)).append("</LeftWeight>");
		xml.append("<LeftVolume>").append(s).append("</LeftVolume>");
		xml.append(aftXmlString);
		
		OutputStream outputStream = response.getOutputStream() ;
		outputStream.write(xml.toString().getBytes());
		outputStream.flush();
		outputStream.close();
	}
	
	
}
