package com.cwc.service.android.action.zr;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.CheckTaskProcessingChangeTaskToDockException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.EntryTaskHasFinishException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.LoadBarUseMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

/**
 * Assign Task 代替原来的AndroidDockCheckIn
 * @author win7zr
 *
 */
public class AndroidGisAction extends ActionFatherController{


	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	private AdminMgrIFaceZJ adminMgrZJ;
	private LoadBarUseMgrIfaceZr loadBarUseMgrZr ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
 				DBRow data = getRequestData(request);
 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
			 
				
				//模拟登陆
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAccount(loginRow.getString("LoginAccount"));
				adminLoggerBean.setAdgid(loginRow.get("adgid",0l));
				adminLoggerBean.setAdid(loginRow.get("adid",0l));
				adminLoggerBean.setPs_id(loginRow.get("ps_id",0l));
				adminLoggerBean.setEmploye_name(loginRow.getString("employe_name"));
				HttpSession session = request.getSession(true);
				session.setAttribute(Config.adminSesion,adminLoggerBean);
				
 				String method = data.getString("Method") ;
 				//包含所有的自有仓库
 				if(method.equalsIgnoreCase("GisBaseData")){
 					
 					
 				}
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
			
		 ////system.out.println(StringUtil.convertDBRowsToJsonString(result) + "...");
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
			
	}
	/**
	 * append Tasks
	 * @param result
	 * @param entry_id
	 * @param equipment_id
	 * @param resources_id
	 * @param resources_type
	 * @param adminLoggerBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月10日
	 */
	private void appendTasks(DBRow result, long entry_id , long equipment_id , long  resources_id, int resources_type , AdminLoginBean adminLoggerBean) throws Exception{
			DBRow[] datas =checkInMgrZr.getTaskProcessingDetailByEntryAndEquipmentAndResouces(entry_id, equipment_id, resources_type, resources_id, adminLoggerBean);
			result.add("datas", datas);
 			DBRow[] loadBarUse = loadBarUseMgrZr.androidGetLoadBarUse(equipment_id, entry_id) ;
			result.add("load_use", loadBarUse == null || loadBarUse.length < 1 ? new DBRow[]{} : loadBarUse );
			result.add("load_bars",checkInMgrZwb.selectLoadBar());
			result.add("equiment", checkInMgrZr.getEquipmentSeals(equipment_id));

	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	private List<HoldDoubleValue<Long,String>> getTaskProcessingStartValues(String value) throws Exception{
		JSONArray jsonArray =  new JSONArray(value);
		List<HoldDoubleValue<Long, String>> returnList = new ArrayList<HoldDoubleValue<Long,String>>();
		for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
			 JSONObject  json = jsonArray.getJSONObject(index);
			 returnList.add(new HoldDoubleValue<Long, String>
			 	(StringUtil.getJsonLong(json, "dlo_detail_id"),
			 	 StringUtil.getJsonString(json, "number")));
			
		}
		return returnList ;
	}
	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setLoadBarUseMgrZr(LoadBarUseMgrIfaceZr loadBarUseMgrZr) {
		this.loadBarUseMgrZr = loadBarUseMgrZr;
	}

}
