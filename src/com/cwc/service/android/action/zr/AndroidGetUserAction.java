package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class AndroidGetUserAction extends ActionFatherController{

	@Override
	public void perform(HttpServletRequest requset, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 			//读取一个xml文件然后返回
			File xml = new File("d:/zr/Users.xml");
			FileInputStream fileInputStream = new FileInputStream(xml);
			byte[] buffer = new byte[512];
			int length = -1 ;
			response.setContentType("application/xml;charset=UTF-8");
 			response.setCharacterEncoding("UTF-8");
			OutputStream outputStream =	response.getOutputStream();
			while((length = fileInputStream.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			outputStream.flush();
			outputStream.close();
			fileInputStream.close();
			
			
	}

}
