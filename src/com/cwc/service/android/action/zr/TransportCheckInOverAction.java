package com.cwc.service.android.action.zr;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.util.DateUtil;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class TransportCheckInOverAction extends ActionFatherController {

	private AndroidMgrIfaceZr androidMgrZr;

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

			InputStream inputStream = request.getInputStream();
			// 获取验证的信息。
			String requestXml = getPost(inputStream);
			////system.out.println(requestXml + "<<<<<<<<<<<<<");
			String fc = StringUtil.getSampleNode(requestXml, "fc");
			String transportId = StringUtil.getSampleNode(requestXml, "Transport");
			////system.out.println( transportId + "<<<<<<<<<<<");
			
			if(fc.equals("TransportReceiveCompare")){
				String details = "<RSN>1234567890|1234567890|987654321|1|2|3|4|5|6|7|8|9|123|222</RSN>" ; 
				responseXML(fc,0, 20, response, details, 0);
				
			}
			
			if(fc.equals("TransportReceiveComplete")){
				String details = "<RSN>1234567890|1234567890|987654321|1|2|3|4|5|6|7|8|9|123|222</RSN>" ; 
				responseXML(fc,0, 20, response, details, 0);
			/*	String details = "<ReceiveResult>1</ReceiveResult>" ; 
				responseXML(fc,1, 0, response, details, 0);*/
			}
			if(fc.equals("TransportSubmitComplete")){
				String details = "";
				responseXML(fc, 1, 0, response, details, 0);
			}

	}
	
	//读取一个xml文件然后返回
	
	
	
	 
	private String getPost(InputStream inputStream) throws Exception {
		BufferedInputStream input = null; 	
		byte[] buffer = new byte[1024]; 	
		int count = 0; 	
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream(); // 请求数据存放对象
		byte[] iXMLData = null;
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1) {
				streamXML.write(buffer, 0, count);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (Exception f) {
					f.printStackTrace();
				}
			}
		}
		iXMLData = streamXML.toByteArray();
		return (new String(iXMLData));
	}

	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)throws Exception{
		response.setContentType("text/html; charset=UTF-8");		
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		boxQtyXml.append("<err>"+err+"</err>");
		boxQtyXml.append("<Detail>");
		if(details.length() > 1 )
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		////system.out.println(boxQtyXml + "response . <<<<<<<<<<<<<<<<<<<,");
	 	try {
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
}
