package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.drools.lang.DRLParser.unary_constr_return;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.api.zwb.UploadAndDownloadPDF;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.SerchLoadingNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.RelativeFileMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * android 上传签字文件
 * 将文件上传到signature文件夹下面
 * @author zhangrui
 *
 */
public class AndroidSignatureFileUpAction extends ActionFatherController{

	private AdminMgrIFaceZJ adminMgrZJ;
	
	private RelativeFileMgrIfaceZr relativeFileMgrZr;
	
    private UploadAndDownloadPDF uploadAndDownloadPDF;
	
    private String upLoadFileUrl="";
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow row = getJsonObjectMutiRequest(request);							// 基本的数据
   				if(!row.getString("Version").equals(AndroidUtil.getVersion())){
   					throw new AppVersionException();
   				}
				String method = row.getString("Method");
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(row.getString("LoginAccount"), row.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				AndroidPermissionUtil.androidSetSession(login, request);

//				
//				if(method.equals("BillOfLoadingSignature")){
//					String filePath = row.getString("filePath");
//					String relativeValue = row.getString("relative_value");
//					int type = row.get("relative_type", 0);
//					DBRow insertDBRow = new DBRow();
//					insertDBRow.add("relative_value", relativeValue);
//					insertDBRow.add("relative_type", type);
//					insertDBRow.add("relative_file_path", filePath);
//					long id = relativeFileMgrZr.addRelativeFile(insertDBRow);
//					if(id == 0l){
//						throw new SystemException();
//					}
//					result.add("fileName", row.getString("fileName"));
// 				}
				//2015-06-01 修改签字图片上传到文件服务器  
				if(method.equals("BillOfLoadingSignature")){
				    String sessionId=request.getRequestedSessionId();
					  
					String filePath = row.getString("filePath");
					String relativeValue = row.getString("relative_value");
					int type = row.get("relative_type", 0);
					
					//step 1 上传文件服务器
					String file_id=uploadAndDownloadPDF.upLoadPDF(upLoadFileUrl, Environment.getHome().replace("\\", "/")+filePath, sessionId);
					//step 2 更新file表
					relativeFileMgrZr.updateFileWithType(file_id);
					//step 2 插入relative_file表中
					DBRow insertDBRow = new DBRow();
					insertDBRow.add("relative_value", relativeValue);
					insertDBRow.add("relative_type", type);
					insertDBRow.add("relative_file_path", file_id);
					long id = relativeFileMgrZr.addRelativeFile(insertDBRow);
					if(id == 0l){
						throw new SystemException();
					}
					result.add("fileName", file_id);
 				}
				
				
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;	
 			}catch (SerchLoadingNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SerchLoadingNotFoundException;
	 		}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
			throw new JsonException(new JsonObject(result));
	}
	
	private DBRow getJsonObjectMutiRequest(HttpServletRequest request) throws IOException, Exception{
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    }
		    if(files.size() == 1){
	    		String filePath = Environment.getHome()+"upload/signature/";
	    		File dir = new File(filePath);
	    		if(!dir.exists()){dir.mkdirs();}
	    		String fileName = new Date().getTime()+".png"; 
	    		FileUtils.copyInputStreamToFile(files.get(0).getInputStream(), new File(dir,fileName));
	    		returnRow.add("filePath",  "upload/signature/"+fileName);
	    		returnRow.add("fileName", fileName);
 	    	}else{
	    		throw new RuntimeException("Only One File is Ok.");
	    	}
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
		return returnRow ;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setRelativeFileMgrZr(RelativeFileMgrIfaceZr relativeFileMgrZr) {
		this.relativeFileMgrZr = relativeFileMgrZr;
	}

	public UploadAndDownloadPDF getUploadAndDownloadPDF() {
		return uploadAndDownloadPDF;
	}

	public void setUploadAndDownloadPDF(UploadAndDownloadPDF uploadAndDownloadPDF) {
		this.uploadAndDownloadPDF = uploadAndDownloadPDF;
	}

	public void setUpLoadFileUrl(String upLoadFileUrl) {
		this.upLoadFileUrl = upLoadFileUrl;
	}
	
	
	
}
