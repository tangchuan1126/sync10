package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.SerchLoadingNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;

/**
 * 用于手持设备自动补全功能的使用
 * @author zhangrui
 *
 */
public class AndroidNumberAutoCompleteAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private CheckInMgrIfaceZr checkInMgrZr ;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow row = getDataFromRequest(request);							// 基本的数据
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				if(!row.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
				String method = row.getString("Method");
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(row.getString("LoginAccount"), row.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				AdminLoginBean adminLoginBean =	AndroidPermissionUtil.androidSetSession(login, request);
				long ps_id = adminLoginBean.getPs_id() ;
				
				if(method.equals("SearchTrailer")){	 //查询车尾
					String value = checkInMgrZwb.searchEquipmentByEquipmentType(ps_id,row.getString("SEARCHVALUE"),CheckInTractorOrTrailerTypeKey.TRAILER);
					if(!StringUtil.isNull(value)){
						result.add("datas", value);
					}else{
						result.add("datas", "");
					}
					result.add("type", "string");
				}
				if(method.equals("SearchTractor")){	 //查询车尾
					String value = checkInMgrZwb.searchEquipmentByEquipmentType(ps_id,row.getString("SEARCHVALUE"),CheckInTractorOrTrailerTypeKey.TRACTOR);
					if(!StringUtil.isNull(value)){
						result.add("datas", value);
					}else{
						result.add("datas", "");
					}
					result.add("type", "string");
				}
				if(method.equals("SearchEquipment")){ //查询设备
					String value = checkInMgrZwb.searchEquipment(ps_id,row.getString("SEARCHVALUE"));
					if(!StringUtil.isNull(value)){
						result.add("datas", value);
					}else{
						result.add("datas", "");
					}
					result.add("type", "string");
				}
				if(method.equalsIgnoreCase("SearchCarrier")){
					DBRow[] datas = checkInMgrZr.getCarrierByPhoneNumber(row.getString("SEARCHVALUE"));
 					result.add("datas", datas);
					result.add("type", "json");

				}
				if(method.equalsIgnoreCase("SearchMcDot")){
					DBRow[] datas = checkInMgrZr.getSearchMcDot(row.getString("SEARCHVALUE"));
 					result.add("datas", datas);
					result.add("type", "json");
				}
				//查看gateDriverLiscense
				if(method.equalsIgnoreCase("SearchGateDriverLiscense")){
					DBRow[] datas = checkInMgrZr.searchGateDriverLiscense(row.getString("SEARCHVALUE"));
 					result.add("datas", datas);
					result.add("type", "json");
				}
			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;		
			}catch (NoPermiessionEntryIdException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;	
	 		}catch (CheckInNotFoundException  e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound;
			}catch (SerchLoadingNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SerchLoadingNotFoundException;
	 		}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (IOException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
 			result.add("ret", ret);
			result.add("err", err);
 	 		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	private DBRow getDataFromRequest(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow ;
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
	
}
