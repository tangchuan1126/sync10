package com.cwc.service.android.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.AndroidPrintMgr;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.PrintServerNoPrintException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.printTask.AndroidPrintServerUnLineException;
import com.cwc.app.iface.gql.PrintLabelMgrIfaceGql;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInAndroidPrintMgrIfaceZwb;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.PrintTaskKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.service.admin.action.AdminLoginAction;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONObject;
import com.fr.data.core.DataUtils;
import com.fr.base.core.json.JSONArray;
/**
 * android通过web去打印
 * @author zhangrui
 *
 */
public class AndroidPrintByWebAction extends ActionFatherController {
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private AdminMgrIFaceZJ adminMgrZJ;
	private CheckInAndroidPrintMgrIfaceZwb checkInAndroidPrintMgrIfaceZwb;
	private PrintLabelMgrIfaceGql printLabelMgrIfaceGql;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			//
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow data = getRequestData(request);
				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
				AdminLoginBean adminLoginBean = AndroidPermissionUtil.androidSetSession(loginRow, request);

				long ps_id = adminLoginBean.getPs_id() ;
				long area_id = loginRow.get("AreaId", 0l);
			    long adid = adminLoginBean.getAdid();
				
				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
				String method = data.getString("Method") ;
				if(method.equals("BillOfLoadingPrintTask")){
					//组织数据
					androidPrintMgrZr.addBillOfLoadingPrintTask(data, loginRow,request);
				}
				if(method.equals("LoadingTickPrintTask")){
					androidPrintMgrZr.loadingTickPrintTask(data, loginRow);
				}
				if(method.equals("CountingSheetPrintTask")){
					androidPrintMgrZr.countingSheetPrintTask(data, loginRow);
				}
				if(method.equals("ReceiptsTicketPrintTask")){
					androidPrintMgrZr.receiptsTicketPrintTask(data, loginRow);
				}
				if(method.equals("ShippingLabelPrintTask")){
					androidPrintMgrZr.addShippingLabelPrintTask(data, loginRow);
				}
				if(method.equals("PackingListTask")){
					androidPrintMgrZr.addPackingList(data, loginRow);
				}
				if(method.equals("GetPrintServer")){
					result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));		//添加printServer
				}
				if(method.equals("GetLetterPrintServer")){
					result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Letter));		//添加printServer
				}
				//收货的时候根据entry打印Label
				if(method.equals("EntryCtnrPrintPlateLabel")){
					DBRow dataRow = new DBRow();
					dataRow.add("entry_id", StringUtil.getLong(request, "entry_id"));
					dataRow.add("count", StringUtil.getInt(request, "count"));
					dataRow.add("print_server_id", StringUtil.getInt(request, "print_server_id"));
					dataRow.add("dlo_detail_id", StringUtil.getLong(request, "dlo_detail_id"));

					androidPrintMgrZr.entryCtnrPrintPlateLabel(dataRow, loginRow);
				}
				if(data.getString("method").equals("AndroidLinkWebPrint")){    //dock close d 
					//组织数据
					String bill_number=data.getString("number");
 					long entry_id=data.get("entry_id",0l);
					String companyId = data.getString("company_id");
					String customer_id = data.getString("customer_id");
					int number_type = data.get("number_type", 0);
					long order_no = data.get("order_no", 0l);
					
					DBRow[] datas = checkInMgrZwb.androidLinkWebPrint(bill_number,entry_id, companyId , customer_id,number_type,order_no,  adid,  request);
					result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.LabelOrletter));		//添加printServer
					result.add("datas", datas);
  				}
				if(data.getString("method").equals("AndroidLinkWebPrintGetPath")){    //dock check in
					//组织数据
					String number=data.getString("number");
					String companyId=data.getString("company_id");
					String customerId=data.getString("customer_id");
					long order_no=data.get("order_no",0l);
					long entry_id=data.get("entry_id",0l);
					int number_type=data.get("number_type", 0);
					DBRow rows =  checkInMgrZwb.androidLinkWebPrintGetPath(number, companyId, customerId, entry_id, number_type, order_no, adid, request); // checkInMgrZwb.androidLinkWebPrintGetPath(number, companyId, customerId,entry_id,number_type,order_no);
					rows.add("printServer",  androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.LabelOrletter));		//添加printServer
					result = rows;
 				}
				if(data.getString("method").equals("LoadGetBillOfLadingInfo")){    //装货的时候，打印BillOfLading 的时候需要的参数
					//组织数据
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					
					DBRow detailRow = checkInMgrZwb.selectDetailByDetailId(dlo_detail_id);
 					int  number_type = detailRow.get("number_type", 0);
 					long order_no = 0l;
 					if(number_type == ModuleKey.CHECK_IN_ORDER){order_no = detailRow.get("number", 0l);}
 					if(number_type == ModuleKey.CHECK_IN_PONO){order_no = detailRow.get("order_no", 0l);}
					DBRow rows =  checkInMgrZwb.androidLinkWebPrintGetPath( detailRow.getString("number")
 							 		,detailRow.getString("company_id"), detailRow.getString("customer_id"),
 							 		 detailRow.get("dlo_id", 0l),detailRow.get("number_type", 0)  ,
 							 		 order_no, adid, request);  
					rows.add("printServer",  androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.LabelOrletter));		//添加printServer
					
					//DBRow cnRow=checkInMgrZwb.androidGetCtnr(detailRow.get("dlo_id", 0l), detailRow.getString("number"), number_type);?
					DBRow cnRow=checkInMgrZwb.getEquipmentByEquipmentId(detailRow.get("equipment_id",0l));
					//equipment_id
					if(cnRow!=null){
						rows.add("container_no",cnRow.getString("equipment_number"));
					}
					
					if(number_type!=ModuleKey.CHECK_IN_CTN&&number_type!=ModuleKey.CHECK_IN_BOL){
						long entry_id=data.get("entry_id",0l);
						String loadNo=data.getString("load_no");
						if(!StringUtil.isBlank(loadNo)){
							DBRow bean=printLabelMgrIfaceGql.findLabelTempDetail(String.valueOf(entry_id), loadNo)==null?new DBRow():printLabelMgrIfaceGql.findLabelTempDetail(String.valueOf(entry_id),  loadNo);
							rows.add("trailer_loader", bean.get("trailer_loader",0));
							rows.add("freight_counted", bean.get("freight_counted",0));
						}
					}
					
					result = rows;
 				}
				
				if(data.getString("method").equals("AndroidCtnrWebPrint")){    //根据load 获取打印模版列表  load 唯一的情况
					//组织数据
					long entry_id=data.get("entry_id",0l);
					String ctnr_no=data.getString("ctnr_no");
					String bol_no=data.getString("bol_no");
					DBRow rows = checkInMgrZwb.androidCtnrWebPrint(ctnr_no,bol_no,entry_id, adid, request);
					rows.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));		//添加printServer
					result = rows;
 				}
				
				if(data.getString("method").equals("AndroidSelectLabel")){    //根据load 获取打印模版列表  load 唯一的情况
					//组织数据
					long entry_id=data.get("entry_id",0l);
					long detail_id=data.get("dlo_detail_id", 0l);
					DBRow rows = checkInAndroidPrintMgrIfaceZwb.androidSelectLabel(adid, entry_id, detail_id, request);
					
					rows.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.LabelOrletter));		//添加printServer
					
					int number_type=data.get("number_type",0);
					if(number_type!=ModuleKey.CHECK_IN_CTN&&number_type!=ModuleKey.CHECK_IN_BOL){
						String loadNo=data.getString("load_no");
						if(!StringUtil.isBlank(loadNo)){
							DBRow bean=printLabelMgrIfaceGql.findLabelTempDetail(String.valueOf(entry_id), loadNo)==null?new DBRow():printLabelMgrIfaceGql.findLabelTempDetail(String.valueOf(entry_id), loadNo);
							rows.add("trailer_loader", bean.get("trailer_loader",0));
							rows.add("freight_counted", bean.get("freight_counted",0));
						}
					}
					
					result = rows;
 				}
				if(data.getString("method").equals("AndroidUpdateLabel")){    
					//组织数据
					DBRow rows=new DBRow();
					Long insertId=printLabelMgrIfaceGql.editLabelTempDetail(request);
					rows.add("insertId", insertId);
					result = rows;
 				}
				if(data.getString("method").equals("GateLabelPrint")){
					 androidPrintMgrZr.addGatePrintLabelTask(data, loginRow);
				}
				if(data.getString("method").equals("TLPLabelPrint")){
					//print_server_id,entry_id,palletIds,date
					 androidPrintMgrZr.addTLPPrintLabelTask(data, loginRow);
				}
				if(data.getString("method").equals("RNLabelPrint")){
					//print_server_id,entry_id,palletIds,date
					 androidPrintMgrZr.addRNPrintLabelTask(data, loginRow);
				}
				if(data.getString("method").equals("BasicLabelPrint")){
					
					androidPrintMgrZr.addBasicLabelPrintTask(data, loginRow);
				}
			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
 			}catch (PrintServerNoPrintException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.PrintServerNoPrintException;
 			}catch (AndroidPrintServerUnLineException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AndroidPrintServerUnEable;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}
			result.add("ret", ret);
			result.add("err", err);
			////system.out.println(StringUtil.convertDBRowsToJsonString(result));
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	 
 
 
	 
	public DBRow getRequestData(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow ;
	}
	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}




	public void setCheckInAndroidPrintMgrIfaceZwb(
			CheckInAndroidPrintMgrIfaceZwb checkInAndroidPrintMgrIfaceZwb) {
		this.checkInAndroidPrintMgrIfaceZwb = checkInAndroidPrintMgrIfaceZwb;
	}




	public void setPrintLabelMgrIfaceGql(PrintLabelMgrIfaceGql printLabelMgrIfaceGql) {
		this.printLabelMgrIfaceGql = printLabelMgrIfaceGql;
	}
	
	
	
	
	
}
