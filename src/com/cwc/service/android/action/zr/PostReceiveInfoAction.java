package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 返回一个zip文件
 * 	这个lp 上已经有了商品的时候,那么就是要放回一个zip。文件。包含有lp上包含商品的基础信息。
 * @author Administrator
 *
 */
public class PostReceiveInfoAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			byte[] buffered = new byte[1024];
			int length = -1 ;
			InputStream inputStream = request.getInputStream();
			FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/post.xml"));
			while((length = inputStream.read(buffered)) != -1){
				fileOutputStream.write(buffered, 0, length);
			}
			inputStream.close();
			fileOutputStream.close();
			sendXml(response);
			
	}
	
	private void sendXml(HttpServletResponse response ) throws Exception {
		response.setContentType("text/html; charset=UTF-8");
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<data><fc>PrintLicensePlate</fc>");
		sb.append("<ret>1</ret>");
		sb.append("<err>0</err>");
		sb.append("<Processingcount>10</Processingcount >"); 
		sb.append("<ServiceTime>2011-4-13 11:06:45</ServiceTime></data>");
		    
		response.setCharacterEncoding("UTF-8");
			
	   OutputStream out = response.getOutputStream();
	   out.write(sb.toString().getBytes());
	   ////system.out.println(sb.toString());
	   out.flush();
	   out.close();
	 
	}

}
