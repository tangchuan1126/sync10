package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
 
public class LoadProductMainPictrueAction extends ActionFatherController{
	
	private AndroidMgrIfaceZr androidMgrZr;
	
  	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	/**
	 * 根据一个PC_ID的返回一个缩略图片。
	 * 首先根据这个ID去查询Main(图片本身的一张图片)
	 * 如果没有商品本身的图片那么 按照图片的在数据库的顺序去选择一张
	 * 方法主要是android给显示图片用的
	 */
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		
			long pc_id = StringUtil.getLong(request, "pc_id");
			File file =	androidMgrZr.getProductMainPicture(pc_id) ;
			if(file != null){
				FileInputStream is = new FileInputStream(file);
				byte[] buffer = new byte[1024];
				int length = -1 ;
				response.setContentType("image/jpeg");
				ServletOutputStream out = 	response.getOutputStream();
				while((length = is.read(buffer)) != -1){
					out.write(buffer,0,length);
				}
				out.flush();
				is.close();
				out.close();
			}
			
 	}

}
