package com.cwc.service.android.action.zr;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetTransportOutboundAction extends ActionFatherController{
	
	private AndroidMgrIfaceZr androidMgrZr;
	
	
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			try{
				//查询transport_outbound表中的数据根据transport_id ;
				//返回的data的数据类型{flag:'success',data:[{to_id:'',sku:'',qty:'',transport_id:''}]}
				//如果是后台报错了。flag == error 
				//接口改了。android测试需要改
				long transport_id = StringUtil.getLong(request, "transport_id");
				result.add("flag", "success");
				DBRow[] data = androidMgrZr.queryTransportOutBoundByTransportId(transport_id);
				List<DBRow> arrayList = new ArrayList<DBRow>();
				for(int index = 0 , count = data.length ; index < count ; index++ ){
					DBRow temp = new DBRow();
					temp.add("to_id", data[index].get("to_id", 0l));
					temp.add("sku", data[index].get("to_pc_id", 0l));
					temp.add("to_p_code",data[index].get("to_p_code", 0l));
					temp.add("to_p_name",data[index].getString("to_p_name"));
					temp.add("qty",  data[index].get("to_count", 0.0));
					////system.out.println( data[index].get("to_count", 0) + "<<<<<<<<<<<<<<<<");
					temp.add("transport_id", data[index].get("to_transport_id", 0l));
					arrayList.add(temp) ;
				}
				result.add("data", arrayList.toArray(new DBRow[arrayList.size()]));
				
			}catch (Exception e) {
				result.add("flag", "error");
	 		}
			throw new JsonException(new JsonObject(result));
	}

}
