package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sound.midi.SysexMessage;
import javax.transaction.SystemException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.AndroidPrintMgr;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.DeletePalletNoFailedException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.android.UpdatePalletTypeFailedException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.zr.LoadPaperWorkMgrIfaceZr;
import com.cwc.app.iface.zr.ReceiveWorkMgrIfaceZr;
import com.cwc.app.iface.zr.WmsLoadMgrZrIface;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.WmsOrderTypeKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class LoadPaperWorkAction extends ActionFatherController{
	
	private CheckInMgrIfaceZwb checkInMgrZwb;

	private SQLServerMgrIFaceZJ serverMgrZJ;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;

	private LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr ; 
	
	private ReceiveWorkMgrIfaceZr receiveWorkMgrZr ;
	
	private WmsLoadMgrZrIface wmsLoadMgrZr ;
 
	public static final int VAILDATIONERROR = 90;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow data = getRequestData(request);
 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
				
				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}

				//模拟登陆
				AdminLoginBean adminLoggerBean = AndroidPermissionUtil.androidSetSession(loginRow, request);
				
				long ps_id = loginRow.get("ps_id", 0l);
				long area_id = loginRow.get("AreaId", 0l);
				long adid = loginRow.get("adid", 0l);
				String method = data.getString("Method") ;
				
				 
				if(method.equalsIgnoreCase("SearchLoadByEntry")){
					DBRow[] details = checkInMgrZwb.getEntryDetailsByEntryId(data.get("entry_id", 0l));
					result.add("datas", fixLoadDatas(details));
 				}
				
			 
			
			 
				//查询load：从哪些托盘上拿了哪些货
				if(method.equalsIgnoreCase("FindLoadOrderItems")){
					String customer_id = data.getString("customer_id");
					String company_id = data.getString("company_id");
					String  order_no = data.getString("order_no");
					int order_type = data.get("order_type", 0);
//					DBRow[] datas = serverMgrZJ.findLoadedMasterAndOrders(load_no, company_id, customer_id);
					DBRow[] datas = serverMgrZJ.findOrdersInfoForConsolidate(order_type, order_no, company_id, customer_id, adid, request);
					result.add("datas", datas);
					result.add("printServer",androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));		//添加printServer
				}
				//查询load：从哪些托盘上拿了哪些货
				if(method.equalsIgnoreCase("ChangeOrderItemPallets")){
					String customer_id = data.getString("customer_id");
					String company_id = data.getString("company_id");
					long  order_no = data.get("order_no", 0L);
					String item_id = data.getString("item_id");
					int line_no = data.get("line_no", 0);
					int change_qty = data.get("change_qty", 0);
					int changedQty = serverMgrZJ.updateOrderItemPalletQty(order_no, item_id, change_qty, line_no, company_id, customer_id, adid, request);
					result.add("changed_qty", changedQty);
				}
				
				if(method.equalsIgnoreCase("GetPalletTypeByCompanyId")){
					String company_id = data.getString("company_id");
					DBRow[] datas = serverMgrZJ.findPalletTypesByCompanyID(company_id);
					fixPalletType(datas);
					result.add("datas", datas);
				}
				if(method.equalsIgnoreCase("ConlidateChangePalletsNumber")){	//conlidate 改变Pallets的数量
					String order_no = data.getString("order_no");
					long orderNoLong = Long.parseLong(order_no);
					String customer_id = data.getString("customer_id");
					String company_id = data.getString("company_id");
					int original_pallets = data.get("original_pallets", 0);
					int current_pallets = data.get("current_pallets", 0);
 					int order_type = data.get("order_type", 0);
					serverMgrZJ.updateOrderPalletsByOrderNo(orderNoLong, customer_id, company_id, original_pallets, current_pallets, adid, request);
					DBRow[] datas = serverMgrZJ.findOrdersInfoForConsolidate(order_type, order_no, company_id, customer_id, adid, request);
 					result.add("datas", datas);
				}
				//查询load：从哪些托盘上拿了哪些货
				if(method.equalsIgnoreCase("ChangeOrderItemPallets")){
					String customer_id = data.getString("customer_id");
					String company_id = data.getString("company_id");
					long  order_no = data.get("order_no", 0L);
					String item_id = data.getString("item_id");
					int line_no = data.get("line_no", 0);
					int change_qty = data.get("change_qty", 0);
					int changedQty = serverMgrZJ.updateOrderItemPalletQty(order_no, item_id, change_qty, line_no, company_id, customer_id, adid, request);
					result.add("changed_qty", changedQty);
				}
				
				
				
				//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>8-28<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
				if(method.equalsIgnoreCase("LoadSearchEntryDetail")){
					long entry_id = data.get("entry_id", 0l);
					long equipment_id = data.get("equipment_id", 0l);
 					loadPaperWorkMgrZr.addSearchEntryValue(result, entry_id,equipment_id ,adminLoggerBean);
 					DBRow entry =(DBRow)result.get("entry", new DBRow());
 					if(entry != null ){
 						DBRow[] details =(DBRow[]) entry.get("details",new DBRow[]{});
 						if(details == null || details.length < 1){
 							throw new NoRecordsException();
 						}
 					}
				}
				
				
				if(method.equalsIgnoreCase("SearchOrderPalletByLoadNo")){
					
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					long equipment_id = data.get("equipment_id", 0l);
					DBRow row = loadPaperWorkMgrZr.findOrderPalletByLoadNo(dlo_detail_id, adid);
					if(row == null){
						throw new NoRecordsException();
					}
  					result.add("datas", row);
  					result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));
  					result.add("isLastDetail", checkInMgrZwb.isDetailLastOfEntry(dlo_detail_id,equipment_id)?1:0);
  					result.add("loadBars", checkInMgrZwb.selectLoadBar());
  					result.add("palletTypes", getPalletTypes());
  					
  					//没有扫描Pallet时，更新handle_time
  					if(!isTaskScanPallet(dlo_detail_id)){
  						DBRow updateRow = new DBRow();
  						updateRow.add("handle_time", DateUtil.NowStr());
  						checkInMgrZwb.updateDetailByIsExist(dlo_detail_id, updateRow);
  					}
						
					
				
  				 
				}
				if(method.equalsIgnoreCase("getOrderInfosByMasterBol")){
					
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					long master_bol_no = data.get("master_bol_no", 0l);
					long equipment_id = data.get("equipment_id", 0l);
					DBRow row = loadPaperWorkMgrZr.getOrderInfosByMasterBol(dlo_detail_id, master_bol_no, adid);
					if(row == null){
						throw new NoRecordsException();
					}
  					result.add("load_info", row);
  					result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));
  					result.add("isLastDetail", checkInMgrZwb.isDetailLastOfEntry(dlo_detail_id,equipment_id)?1:0);
  					result.add("loadBars", checkInMgrZwb.selectLoadBar());
  					result.add("palletTypes", getPalletTypes());
						
					
				
				}
				/**
				 * 上一个方法前台选择过后执行的方法
				 */
				if(method.equalsIgnoreCase("RefreshOrderInfos")){
					String master_bol = StringUtil.getString(request, "master_bol");
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					int isClearData = data.get("isClearData", 0);
					long lr_id = data.get("lr_id", 0l);
					
					DBRow row = loadPaperWorkMgrZr.refreshOrderInfos(dlo_detail_id, adid, master_bol,isClearData,lr_id);
					result.add("printServer",androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));		//添加printServer
					result.add("datas", row);
				}
				
				/**
				 * 这个方法处理图片信息，提交装货的图片
				 */
				if(method.equalsIgnoreCase("SubmitLoadPalletInfo")){
					loadPaperWorkMgrZr.loadConfirmSubmitPhoto(data, adminLoggerBean);
   				}
				/**
				 * 装货的时候的时候扫描一个Pallet 就提交数据
				 * 和修改Pallet type 
				 * 和修改pro no .
				 */
				if(method.equalsIgnoreCase("LoadOnePallet")){
					loadPaperWorkMgrZr.loadScanAndPalletTypeChange(data,adminLoggerBean);
				}
				if(method.equalsIgnoreCase("LoadConsolidatePallets")){ //主要是From pallet to pallet 
					String order_no = data.getString("order_no");
					long orderNoLong = Long.parseLong(order_no);
  					int original_pallets = data.get("original_pallets", 0);
					int current_pallets = data.get("current_pallets", 0);
 					long dlo_detail_id = data.get("dlo_detail_id", 0l);
 					int reason = data.get("reason", 0);
 					String from_pallet = data.getString("from_pallet");
 					String to_pallet = data.getString("to_pallet");
 					long LR_ID =  data.get("LR_ID", 0l);
 					int order_type= data.get("order_type", 0);
 					int order_system= data.get("system_type", 0);

  					loadPaperWorkMgrZr.loadConsolidate(from_pallet, to_pallet, reason, dlo_detail_id, orderNoLong, adid, current_pallets, original_pallets,LR_ID,order_type,order_system , data , adminLoggerBean);
					//serverMgrZJ.updateOrderPalletsByOrderNo(orderNoLong, customer_id, company_id, original_pallets, current_pallets, adid, request);
					//添加Consolidate的时候把对应的Pallety_number 记录到系统当中
				}
				if(method.equalsIgnoreCase("CloseSyncBillStatus")){	//关闭系统中的Order,Load,Master_bol,同时也会关闭Wms系统中的数据
					String master_bol_no = data.getString("master_bol_no");
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					String pro_no = data.getString("pro_no");
					loadPaperWorkMgrZr.closeSyncBill(dlo_detail_id, master_bol_no,pro_no,loginRow.get("adid", 0l));
				}
				/**
				 * 查询在Load Close页面Close 按钮是否可用
				 */
				if(method.equalsIgnoreCase("IsOrderClose")){  
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					String master_bol_no = data.getString("master_bol_no");
					boolean flag = loadPaperWorkMgrZr.isCloseMasteOrOrder(dlo_detail_id, master_bol_no);
					result.add("closeFlag", flag ? "1":"0");
				}
				//10-11 李君浩 print 功能不需要有door,不需要已经gate checkout
				if(method.equalsIgnoreCase("EntryPrint")){
					long entry_id =  data.get("entry_id", 0l);
					loadPaperWorkMgrZr.getEntryPrint(result,entry_id,adminLoggerBean);
					DBRow entry =(DBRow)result.get("entry", new DBRow());
 					if(entry != null ){
 						DBRow[] details =(DBRow[]) entry.get("details",new DBRow[]{});
 						if(details == null || details.length < 1){
 							throw new NoRecordsException();
 						}
 					}
				}
				if(method.equalsIgnoreCase("NeedHelp")){
					int flag_help = data.get("flag_help", 0);
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
 					loadPaperWorkMgrZr.updateDetailNeedHelp(dlo_detail_id, flag_help);
				}
				if(method.equalsIgnoreCase("DeleteScanPalletNo")){
					long dlo_detail_id = data.get("dlo_detail_id", 0l);
					String order_number = data.getString("order_number");
					String pallet_no = data.getString("pallet_no");
					int count = loadPaperWorkMgrZr.deletePalletNo(dlo_detail_id, order_number, pallet_no);
					if(count <= 0){
						throw  new DeletePalletNoFailedException();
					}
				}
				//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Common Load<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
				if(method.equalsIgnoreCase("CommonLoadRefresh")){  // 普通的收货的界面 , PalletTypes返回
					long lr_id = data.get("lr_id", 0l);
					result.add("datas",  wmsLoadMgrZr.commonLoadRefresh(lr_id));
	 				result.add("palletTypes", getPalletTypes());
	 				result.add("orderSystem", receiveWorkMgrZr.getOrderSystemInfo(lr_id));
				}
				if(method.equals("CommonLoadDelete")){
					long wms_order_type_id = data.get("wms_order_type_id", 0l);
					int count = wmsLoadMgrZr.commonLoadDelete(wms_order_type_id);
					if(count <= 0){
						throw new DeletePalletNoFailedException() ;
					}
				}
				
				if(method.equalsIgnoreCase("CommonLoadConsolidatePallet")){
					
					int system_type = data.get("system_type", 0);
					int order_type = data.get("order_type", 0);
					long detail_id = data.get("detail_id", 0l);
					long lr_id = data.get("lr_id", 0l);
					String from_pallet_number =  data.getString("from_pallet_number");
					String to_pallet_number =  data.getString("to_pallet_number");
					String wms_consolidate_reason =  data.getString("reason");
 					int order_live =  data.get("order_level",0);
							
 					long resources_id = data.get("resources_id", 0l);
 					int resources_type = data.get("resources_type", 0);

 					
					DBRow loadPallet = new DBRow();
 					loadPallet.add("wms_pallet_type_count", 0);
					loadPallet.add("wms_pallet_number", from_pallet_number);
					loadPallet.add("wms_consolidate_pallet_number", to_pallet_number);
					loadPallet.add("wms_consolidate_reason", wms_consolidate_reason);
					loadPallet.add("order_number", "");
					loadPallet.add("order_type", order_type);
					loadPallet.add("order_system", system_type);
					loadPallet.add("dlo_detail_id", detail_id);
					loadPallet.add("order_live", order_live);
					loadPallet.add("delivery_or_pick_up", CheckInMainDocumentsRelTypeKey.PICK_UP);
					loadPallet.add("lr_id", lr_id);
					
					loadPallet.add("resources_type", resources_type);
					loadPallet.add("resources_id", resources_id);
					loadPallet.add("scan_adid", adminLoggerBean.getAdid());
					
					long id = wmsLoadMgrZr.commonLoadPallet(loadPallet);
					if(id <= 0l){
						throw new SystemException();
					}
				}
				if(method.equalsIgnoreCase("CommonLoadPallet")){
					int system_type = data.get("system_type", 0);
					int order_type = data.get("order_type", 0);
					long detail_id = data.get("detail_id", 0l);
					long lr_id = data.get("lr_id", 0l);
					String pallet_number =  data.getString("pallet_number");
					int count =  data.get("count",0);
					String pallet_type =  data.getString("pallet_type");
					int order_live =  data.get("order_level",0);
					
					long resources_id = data.get("resources_id", 0l);
					int resources_type = data.get("resources_type", 0);

					
					
					DBRow loadPallet = new DBRow();
					loadPallet.add("wms_pallet_type", pallet_type);
					loadPallet.add("wms_pallet_type_count", count);
					loadPallet.add("wms_pallet_number", pallet_number);
					loadPallet.add("order_number", "");
					loadPallet.add("order_type", order_type);
					loadPallet.add("order_system", system_type);
					loadPallet.add("dlo_detail_id", detail_id);
					loadPallet.add("order_live", order_live);
					loadPallet.add("delivery_or_pick_up", CheckInMainDocumentsRelTypeKey.PICK_UP);
					loadPallet.add("lr_id", lr_id);
					
					loadPallet.add("resources_type", resources_type);
					loadPallet.add("resources_id", resources_id);
					loadPallet.add("scan_adid", adminLoggerBean.getAdid());
					
					long id = wmsLoadMgrZr.commonLoadPallet(loadPallet);
					if(id <= 0l){
						throw new SystemException();
					}
					result.add("wms_order_type_id", id);
				}
				if(method.equalsIgnoreCase("CommonLoadModify")){
					long wms_order_type_id = data.get("wms_order_type_id", 0l);
					int count =  data.get("count",0);
					String pallet_type =  data.getString("pallet_type");
					DBRow updateRow = new DBRow();
					updateRow.add("wms_pallet_type_count",count);
					updateRow.add("wms_pallet_type",pallet_type);
					int modifyCount = wmsLoadMgrZr.commonLoadModify(wms_order_type_id, updateRow);
					if(modifyCount <= 0l){
						throw new SystemException();
					}
				}
				if(method.equalsIgnoreCase("CommonReload")){
					long lr_id = data.get("lr_id", 0l);
					long detail_id = data.get("detail_id", 0l);
					wmsLoadMgrZr.commonLoadReload(lr_id, detail_id);
					result.add("datas",  wmsLoadMgrZr.commonLoadRefresh(lr_id));
				}
			}catch(UpdatePalletTypeFailedException e ){
				ret = BCSKey.FAIL ;
				err = BCSKey.UpdatePalletTypeFailedException ;
			}catch(DeletePalletNoFailedException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DeletePalletNoFailedException ;
			}catch(CheckInEntryIsLeftException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInEntryIsLeftException ;
			}catch(DockCheckInFirstException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DockCheckInFirstException ;
			}catch (NoPermiessionEntryIdException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;	
	 		}catch (DataFormatException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DataFormateException;
 			}catch (LoadIsCloseException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LoadIsCloseException;
			}catch (NoRecordsException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoRecordsException;
 			}
			catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	private DBRow[] getPalletTypes(){

		String[] palletTypeId = new String[]{"6548","5842","5844","6844","5440","5245","4842","4848","4840","5640","6247","6340","7245","7840","5544","5048","85x17.5","85x52","66x48","60x48","56x48","45x42","41x41","58x34","78x48","64x42","65x9","68x9","72x10","78x10","68x41","50x45","CHEP","FL","PLT1","PLT2","SP","PECO","For VZO","For Sharp","36x85","60*40"} ;
        String[] palletTypeName = new String[]{"65x48","58x42","58x44","68x44","54x40","52x45","48x42","48x48","48X40","56x40","62x47","63x40","72x45","78x40","55x44","50x48","85x17.5","85x52","66x48","60x48","56x48","45x42","41x41","58x34","78x48","64x42","65x9","68x9","72x10","78x10","68x41","50x45","CHEP","FloorLoad","PalletType1","PalletType2","Slipsheet","PECO","For VZO","For Sharp","36x85","60*40"};
		if(palletTypeId != null && palletTypeId.length > 0 
				&& palletTypeName != null && palletTypeName.length > 0
				&& palletTypeName.length == palletTypeId.length){
			DBRow[] returnArrays = new DBRow[palletTypeId.length];
			for(int index = 0 , count = palletTypeId.length ; index < count ; index++ ){
				DBRow temp = new DBRow();
				temp.add("pallettypeid", palletTypeId[index]);
				temp.add("pallettypename", palletTypeName[index]);
				returnArrays[index] = temp ;
			}
			return returnArrays ;
		}
		return new  DBRow[0];
	}
	
	private DBRow getSubmitLoadPalletInfoRow(DBRow data) throws DataFormatException {
		try{
			  DBRow returnRow = new DBRow();
			  String jsonInfo = data.getString("jsoninfo");
			  JSONObject jsonObject = StringUtil.getJsonObject(jsonInfo);
			  if(jsonObject != null){
				  String company_id = StringUtil.getJsonString(jsonObject, "company_id");
				  String customer_id = StringUtil.getJsonString(jsonObject, "customer_id");
				  String master_bol_no = StringUtil.getJsonString(jsonObject, "master_bol_no");
 				  returnRow.add("company_id", company_id);
				  returnRow.add("customer_id", customer_id);
				  returnRow.add("master_bol_no", master_bol_no);
				  returnRow.add("dlo_detail_id", data.get("dlo_detail_id", 0l));
				  
				  JSONArray arrays =  StringUtil.getJsonArrayFromJson(jsonObject, "order_infos");
				  if(arrays != null && arrays.length() > 0 ){
					  DBRow[] orderInfos = new DBRow[arrays.length()];
					  for(int index = 0 , count = arrays.length() ; index < count ; index++ ){
						  DBRow order = new DBRow();
						  JSONObject orderJson = StringUtil.getJsonObjectFromArray(arrays, index);
						  order.add("orderno",StringUtil.getJsonString(orderJson, "orderno") );
						  JSONArray palletInfos = StringUtil.getJsonArrayFromJson(orderJson, "pallet_info");
						  if(palletInfos != null && palletInfos.length() > 0){
							  DBRow[] palletInfoDBRows = new DBRow[palletInfos.length()];
							  for(int palletIndex = 0  , palletCount = palletInfos.length() ;   
									  palletIndex < palletCount ; palletIndex++){
								  DBRow palletType = new DBRow();
								  palletType.add("wms_pallet_type", StringUtil.getJsonString(palletInfos.getJSONObject(palletIndex), "pallet_type"));
								  palletType.add("wms_pallet_type_count", StringUtil.getJsonInt(palletInfos.getJSONObject(palletIndex), "count"));
								  palletInfoDBRows[palletIndex] = palletType ;
							  }	
							  order.add("pallet_info",palletInfoDBRows);
						  }
						  orderInfos[index] = order ;
					  }
					  returnRow.add("order_infos", orderInfos);
				}
			  }
			  return returnRow ;
		}catch (Exception e) {
			throw new DataFormatException() ;
 		}
	}
	private void fixPalletType(DBRow[] datas){
		if(datas != null && datas.length > 0 ){
			for(DBRow row : datas){
				row.remove("DateCreated");
				row.remove("UserCreated");
				row.remove("UserUpdated");
				row.remove("DateUpdated");
				row.remove("DateUpdated");
			}
		}
	}
	private boolean isLoadClose(DBRow row) throws Exception{
		boolean flag = false ;
		DBRow[] dbrowArray =(DBRow[]) row.get("order_info", new DBRow[]{}) ;
		if(dbrowArray != null && dbrowArray.length > 0 ){
			if(dbrowArray[0].getString("ml_status").equalsIgnoreCase("Closed")){
				flag = true;
			}
		}
		return flag ;
	}
	
	private DBRow[] fixLoadDatas(DBRow[] arrays){
		List<DBRow> arrayList = new ArrayList<DBRow>();
		if(arrays != null && arrays.length > 0 ){
			for(DBRow row : arrays){
				String loadNumber = row.getString("load_number");
				if(!StringUtil.isNull(loadNumber)){
					DBRow insertRow = new DBRow();
					insertRow.add("load_number", loadNumber);
					arrayList.add(insertRow);
				}
			}
		}
		return arrayList.toArray(new DBRow[0]);
	}
	public DBRow getRequestData(HttpServletRequest request) throws IOException, Exception{
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	
	public boolean isTaskScanPallet(Long detail_id) throws Exception {
		return checkInMgrZwb.isTaskScanPallet(detail_id);
	}


	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setServerMgrZJ(SQLServerMgrIFaceZJ serverMgrZJ) {
		this.serverMgrZJ = serverMgrZJ;
	}

	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}

	public void setLoadPaperWorkMgrZr(LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr) {
		this.loadPaperWorkMgrZr = loadPaperWorkMgrZr;
	}
	public void setReceiveWorkMgrZr(ReceiveWorkMgrIfaceZr receiveWorkMgrZr) {
		this.receiveWorkMgrZr = receiveWorkMgrZr;
	}
	public void setWmsLoadMgrZr(WmsLoadMgrZrIface wmsLoadMgrZr) {
		this.wmsLoadMgrZr = wmsLoadMgrZr;
	}
	
}
