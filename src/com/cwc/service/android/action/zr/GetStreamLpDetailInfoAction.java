package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 返回一个zip文件
 * 	这个lp 上已经有了商品的时候,那么就是要放回一个zip。文件。包含有lp上包含商品的基础信息。
 * @author Administrator
 *
 */
public class GetStreamLpDetailInfoAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			byte[] buffered = new byte[1024];
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("bj0001LicensePlate"+".zip","utf-8"));  

			OutputStream outputStream =	response.getOutputStream();
			FileInputStream zip = new FileInputStream(new File("d:/bj0001LicensePlate.zip"));
			 
			int length = -1 ;
			while((length = zip.read(buffered)) != -1){
				outputStream.write(buffered, 0, length);
			}
			zip.close();
			outputStream.flush();
			outputStream.close();
			
			
	}

}
