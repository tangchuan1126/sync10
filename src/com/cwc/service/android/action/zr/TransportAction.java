package com.cwc.service.android.action.zr;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 处理Transport获取相关的数据的Action 
 * 传递的都是是JSON格式
 * @author zhangrui
 *
 */
public class TransportAction extends ActionFatherController{

	private   AndroidMgrIfaceZr androidMgrZr = null;
	
	private AdminMgrIFaceZJ adminMgrZJ;

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow fromAndroid = getJsonObjectFromRequest(request);
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				String method = fromAndroid.getString("Method");
				
				//处理Inbound Transport List
				if(method.equals("CheckInTransports")){
					DBRow[] datas = 	androidMgrZr.getInboundTransportList(login);
					result.add("data", datas); 
				}			
				//释放门 (收货的时候)
				if(method.equals("ClearInboundDoor")){
					long transportId = getTransportId(request);
					androidMgrZr.clearDoorAndLocation(transportId, TransportRegistrationTypeKey.DELEIVER);
				}
				//Oubound 的时候(释放门)
				if(method.equals("ClearOutboundDoor")){
					long transportId = getTransportId(request);
					androidMgrZr.clearDoorAndLocation(transportId, TransportRegistrationTypeKey.SEND);
				}
				
				/*
				 *  1.在Inbound的时候根据TransportId 去下载基础数据
				 *  2.{ret:1;err:2;productCode:[{},{},{}] ; productInfo:[{},{}];datas[{},{},{}]}
				 *  3.productCode表示的是所收商品的p_code(每种商品都加上 了PCID--PCID)
				 *  4.productInfo 表示商品的基本信息比如length,width,hight,weight
				 *  5.datas所要收的商品的总和
				 */
				
				if(method.equals("InboundTransportBaseData")){
					long transport_id = getTransportId(request);
					result = androidMgrZr.getInboundBaseInfo(transport_id);
				}
				//处理Position Transport List
				if(method.equals("PositionReceiveTransportList")){
 					DBRow[] datas = androidMgrZr.getPositionTransportList(login);
 					result.add("data", datas);
				}
				if(method.equals("PositionReceiveBaseData")){
					long transport_id = getTransportId(request);
					result = androidMgrZr.getPositionBaseInfo(transport_id);
				}
				if(method.equals("TransportInboundImage")){		//表示的是查询File表中的数据
	  				DBRow queryRow = getTransportPictureJsonRow(request);
					DBRow[] rows = androidMgrZr.getFilesAndCreateSmall(queryRow.get("FileWithId", 0l), queryRow.get("FileWithType",0), queryRow.getString("SysFileDir"));
					result.add("data", rows);
	  			}
				if(method.equals("DeleteTransportFile")){
					long file_id = StringUtil.getLong(request, "FileId");
					androidMgrZr.deleteFileTableFile(file_id);
				}
				 
				
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}
			result.add("ret", ret);
			result.add("err", err);
			throw new JsonException(new JsonObject(result));
	}

	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
 		return result ;
	}
	private DBRow getTransportPictureJsonRow(HttpServletRequest request){
		DBRow queryRow = new DBRow();
		queryRow.add("FileWithId", StringUtil.getString(request, "FileWithId"));
		queryRow.add("FileWithType", StringUtil.getString(request, "FileWithType"));
		queryRow.add("SysFileDir", StringUtil.getString(request, "SysFileDir"));
		 
		
		return queryRow ;
	}
	private long getTransportId(HttpServletRequest request) {
		return StringUtil.getLong(request, "TransportId");
	}
	 
	

}
