package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * android 请求一个文件
 * @author zhangrui
 *
 */
public class AndroidLoadFileAction extends ActionFatherController{
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 			
			String method = StringUtil.getString(request, "Method");
			FileInputStream  inputStream = null ;
			
			if(method.equals("LoadFilePinterTCP")){	//下载TCP printer 
				inputStream  = new FileInputStream(new File(Environment.getHome()+"/android/android_tcp_printer.txt"));
				response.setContentType("html/text;charset=UTF-8");
				response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("android_tcp_printer","utf-8"));  
			}
			if(inputStream != null){
				byte[] buffer = new byte[1024 * 1024];
				int length = -1 ;
				OutputStream outputStream =  response.getOutputStream();
				while((length = inputStream.read(buffer)) != -1){
					outputStream.write(buffer, 0, length);
				}
				inputStream.close();
 				outputStream.close(); 
			}
			
	}
	
	

}
