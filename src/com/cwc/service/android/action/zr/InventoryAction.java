package com.cwc.service.android.action.zr;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.TitleNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.storageApprove.HasWaitApproveAreaException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.AndroidMgrIFaceZJ;
import com.cwc.app.iface.zr.InventoryMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
 
/**
 * 处理android(Inventory)
 * @author zhangrui
 *
 */
public class InventoryAction extends ActionFatherController{

	private InventoryMgrIfaceZr inventoryMgrZr;
 	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private AndroidMgrIFaceZJ androidMgrZJ;
	 
 	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
  				DBRow fromAndroid = getJsonObjectFromRequest(request);
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				if(login == null)
				{
					throw new MachineUnderFindException();
				}
				
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAccount(fromAndroid.getString("LoginAccount"));
				adminLoggerBean.setAdgid(login.get("adgid",0l));
				adminLoggerBean.setAdid(login.get("adid",0l));
				adminLoggerBean.setPs_id(login.get("ps_id",0l));
				adminLoggerBean.setEmploye_name(login.getString("employe_name"));
				
				if(fromAndroid.getString("Method").equals("OperatorArea"))//OperatorArea
				{
					DBRow[] data = inventoryMgrZr.getOperatorArea(login.get("ps_id", 0l));
					result.add("data", data);
 				}
				if(fromAndroid.getString("Method").equals("FindTitle"))
				{		//查询某个title
					String title = StringUtil.getString(request, "title");
					DBRow  data = inventoryMgrZr.getTitleByName(title);
					if(data == null){throw new TitleNotFoundException();}
					result.add("data", new DBRow[]{data});
 				}
				//在盘点 在位置上提交动作(提交所有的接地容器 ， location_id ,location ,area_id , loginDBRow)
				if(fromAndroid.getString("Method").equals("TakeStock"))//
				{
					long location_id = StringUtil.getLong(request, "LocationId");
					String location = StringUtil.getString(request, "Location");
					long area_id = StringUtil.getLong(request, "AreaId") ;
					
					String floorContainers = StringUtil.getString(request, "FloorContainers");	//100023,1000012
//					//system.out.println(floorContainers);
					androidMgrZJ.takeStoreStorageLocation(floorContainers, location_id);
					
 				}
				
				if (fromAndroid.getString("Method").equals("TakeStockOver"))
				{
					long area_id = StringUtil.getLong(request, "AreaId") ;
					DBRow[] differents = androidMgrZJ.storageTakeStockDifferents(area_id);
					
					result.add("data",differents);
				}
				
				//盘点的时候整个区域盘点完成(area_id)
				if(fromAndroid.getString("Method").equals("TakeStockApprove"))//
				{
 					long area_id = StringUtil.getLong(request, "AreaId") ;
 					int different_count = androidMgrZJ.storageTakeStockApprove(area_id, adminLoggerBean);
 					
 					DBRow data = new DBRow();
 					data.add("different_count",different_count);
 					result.add("data", new DBRow[]{data});
 				}
				
			}
			catch (HasWaitApproveAreaException e)
			{
				
			}
			catch (TitleNotFoundException e) {
					ret = BCSKey.FAIL ;
					err = BCSKey.TitleNotFoundException;
	 		}catch (ContainerNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LPNotFound;
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (IOException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}
			result.add("ret", ret);
			result.add("err", err);
			throw new JsonException(new JsonObject(result));

	 
	}

	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
 		return result ;
	}
	 

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setInventoryMgrZr(InventoryMgrIfaceZr inventoryMgrZr) {
		this.inventoryMgrZr = inventoryMgrZr;
	}

	public void setAndroidMgrZJ(AndroidMgrIFaceZJ androidMgrZJ) {
		this.androidMgrZJ = androidMgrZJ;
	}
	 


}
