package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.DeleteFileException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.FileWithClassKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * 1.android 处理图片(文件上传)
 * @author Administrator
 *
 */
public class AndroidUpZipFileAction extends ActionFatherController {

 	
	private FileMgrIfaceZr fileMgrZr ;

	private AdminMgrIFaceZJ adminMgrZJ;
	 
	 
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception { 
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				HttpSession session = 	request.getSession() ;
				Object obj =  session.getAttribute(Config.adminSesion);
 				DBRow row = getJsonObjectMutiRequest(request);							// 基本的数据
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(row.getString("LoginAccount"), row.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(login, request);

				String method = row.getString("Method");
				if(method.equalsIgnoreCase("UpFile")){
	 				DBRow[] uris = fileMgrZr.androidUpZipPictrue(row, adminLoggerBean);
	 				result.add("datas", uris);
				} 
				if(method.equalsIgnoreCase("GetFileUri")){
					 int fileWithClass = row.get("file_with_class", 0);
					 long fileWithId = row.get("file_with_id", 0l);
					 int fileWithType = row.get("file_with_type", 0);
					 long fileWithDetailId = row.get("file_with_detail_id", 0l);
					 String option_file_param = row.getString("option_file_param");
					 //只有CountingSheet才带detail_id
					 if(fileWithClass != FileWithCheckInClassKey.PhotoCountingSheet){
						 fileWithDetailId = 0l;
					 }
					 DBRow[] uris = fileMgrZr.androidGetFileUri(fileWithId, fileWithType, fileWithClass,fileWithDetailId,option_file_param);
					 //PhotoTaskProcessing 的时候把 receive 的图也带进去
					 if(fileWithClass == FileWithCheckInClassKey.PhotoTaskProcessing){
						 fileWithClass = FileWithCheckInClassKey.PhotoReceive;
						 DBRow[] rec = fileMgrZr.androidGetFileUri(fileWithId, fileWithType, fileWithClass,fileWithDetailId,option_file_param);
						 
						 
						 if(uris!=null && uris.length>0){
							 if(rec!=null && rec.length>0){
								 DBRow[] concat = new DBRow[uris.length + rec.length];
								 System.arraycopy(uris, 0, concat, 0, uris.length);
								 System.arraycopy(rec, 0, concat, uris.length, rec.length);
								 uris = concat;
							 }
						 }else{
							 uris = rec;
						 }
					 }
					 result.add("datas", uris);
				 }
				if(method.equalsIgnoreCase("DeleteFile")){
					long file_id = row.get("file_id", 0l);
					//int count = fileMgrZr.deleteFileBy(file_id);
					int count = fileMgrZr.deleteFromFileServ(new long[]{file_id}, row.getString("session_id"));
					if(count <= 0){
						throw new DeleteFileException();
					}
				}
			}catch(DeleteFileException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DeleteFileException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}

	private DBRow getJsonObjectMutiRequest(HttpServletRequest request) throws Exception{
		DBRow returnRow = new DBRow();
		String sessionId=request.getRequestedSessionId();
		returnRow.add("SESSION_ID", sessionId);
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkout", "_checkout.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}
}
