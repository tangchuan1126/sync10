package com.cwc.service.android.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.json.types.JsonArray;

import com.cwc.action.core.ActionFatherController;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class SubmitJsonAction  extends ActionFatherController {

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
		 
			
			String value = StringUtil.getString(request, "json");
			JSONArray data =  JSONArray.fromObject(value);
			Thread.sleep(5000);
			////system.out.println(value);
			for(int index  = 0 , count = data.size() ; index < count ; index++ ){
					JSONObject s = data.getJSONObject(index);
					/*//system.out.println(" sn : " +s.getString("sn") 
							+ " sku : " + s.getString("sku") 
							+ " quantity : " + s.getInt("quantity")
							+ " palletNum : " + s.getString("palletNum"));*/
			}
			 
			DBRow result = new DBRow();
			result.add("flag", "success");
		 
			throw new JsonException(new JsonObject(result));
	}

}
