package com.cwc.service.android.action.zr;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.TempContainerCreateFailedException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zl.ContainerMgrIFaceZYZ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zr.ContainerMgrIfaceZr;
import com.cwc.app.iface.zr.TempContainerMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
 
public class ContainerAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	
	private ContainerMgrIfaceZr containerMgrZr;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	 
	private TempContainerMgrIfaceZr tempContainerMgrZr ;
	
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	
	private ContainerMgrIFaceZYZ containerMgrZYZ ;
 			
	@SuppressWarnings("finally")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
  				DBRow fromAndroid = getJsonObjectFromRequest(request);
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				if(fromAndroid.getString("Method").equals("InnerContainerLoad")){
					result = androidMgrZr.containerLoadProduct(fromAndroid.getString("Container"));
					/**
					 * 1.根据Container的编码去，查询数据，如果有返回的Container，在根据ContainerId去查询图数据库
					 */
					
					if(result != null){
						DBRow[] containers = (DBRow[]) result.get("container", new DBRow[]{});
						if(containers != null && containers.length > 0){
							long containerId = containers[0].get("con_id", 0l);
 							String re = productStoreMgrZJ.containerLoadProduct(login.get("ps_id",0l),containerId);
							result.add("result",re);
						}
					}
				}
				if(fromAndroid.getString("Method").equals("CreateContainer")){
					long titleId = StringUtil.getLong(request, "title_id");
					int number = StringUtil.getInt(request, "create_number");
					long containerTypeId = StringUtil.getLong(request, "container_type_id");
					int containerType = StringUtil.getInt(request, "container_type");
					String lotnumber = StringUtil.getString(request, "lotnumber");
					DBRow[] datas = containerMgrZr.createContainer(containerTypeId, containerType, number,titleId,lotnumber);
					result.add("data", datas);
				}
				if(fromAndroid.getString("Method").equals("BaseTLPContainerTypes")){//查询TLP的ContainerTypes
					DBRow[] containerTypeRow = containerMgrZr.getContainerBaseType(ContainerTypeKey.TLP);
 					result.add("data", containerTypeRow);
				}
				if(fromAndroid.getString("Method").equals("CreateTLPOnTemp")){
					String container =  fromAndroid.getString("Container");
					long title_id = StringUtil.getLong(request, "title_id");
					long containerId = tempContainerMgrZr.addTempTLPContainer(container, title_id);
					if(containerId != 0l){
						result.add("containerId", containerId);	
						result.add("container", container);
						result.add("containerHasSn", ContainerHasSnTypeKey.NOSN);
						result.add("containerType", "TLP");
						result.add("subContainerType", "ALL");
						result.add("subContainerTypeId", -1);
						result.add("subCount", -1);
						result.add("containerPcId", -1);
						result.add("containerPcName", "");
						result.add("containerPcCount", -1);
						result.add("is_full", ContainerProductState.FULL);
 					}else{
						//创建Container失败
 						throw new TempContainerCreateFailedException();
					}
					 
				}
  			}catch (TempContainerCreateFailedException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.TempContainerCreateFailedException;
			}catch (ContainerNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LPNotFound;
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (IOException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}  			
			result.add("ret", ret);
			result.add("err", err);
			throw new JsonException(new JsonObject(result));
 			 
	}

	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("Container", StringUtil.getString(request, "Container"));
		return result ;
	}
	 

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	public void setTempContainerMgrZr(TempContainerMgrIfaceZr tempContainerMgrZr) {
		this.tempContainerMgrZr = tempContainerMgrZr;
	}

	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}

	public void setContainerMgrZr(ContainerMgrIfaceZr containerMgrZr) {
		this.containerMgrZr = containerMgrZr;
	}

	public void setContainerMgrZYZ(ContainerMgrIFaceZYZ containerMgrZYZ) {
		this.containerMgrZYZ = containerMgrZYZ;
	}
	

}
