package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * android 需要下载的基础的信息
 * @author zhangrui
 *
 */
public class AndroidSysBaseDataAction extends ActionFatherController{
	
	private AdminMgrIFaceZJ adminMgrZJ;
	private TransportMgrIfaceZr transportMgrZr;
	private CheckInMgrIfaceZr checkInMgrZr;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
		
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		boolean isMutiArray = false ; //是否是多层的数组嵌套
		DBRow result  =  new DBRow();
  		try{
			DBRow fromAndroid = getJsonObjectFromRequest(request);
			//首先进行登录
			DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
			if(login == null){
				throw new MachineUnderFindException();
			}
			String method = fromAndroid.getString("Method") ;
			
			AndroidPermissionUtil.androidSetSession(login, request);

			
			//获取系统人员
			if(method.equals("SysUser")){
				DBRow[]	datas = transportMgrZr.getAllUserAndDept(0, 0);
				fixDBRow(datas) ;
				result.add("data", datas);
			}
			if(method.equals("Test")){
				DBRow[]	datas = transportMgrZr.getAllUserAndDept(0, 0);
				fixDBRow(datas) ;
				List<DBRow> user = new ArrayList<DBRow>();
				List<DBRow> mag = new ArrayList<DBRow>();
				for(DBRow temp :datas){
					if(temp.get("proJsId", 0) > 0){
						mag.add(temp);
					}else{
						user.add(temp);
					}
				}
				result.add("User", user.toArray(new DBRow[]{}));
				result.add("manager", mag.toArray(new DBRow[]{}));
			}
			
			//统计成员名下未完成的任务数
			if(method.equals("CountUncompletedTask")){
				int ps_id = StringUtil.getInt(request, "ps_id") ;// .get("",0l);
				int proJsId = StringUtil.getInt(request, "proJsId");
				int adgid = StringUtil.getInt(request, "adgid");
				
				// // 15 == ProcessKey.GateNotifyWareHouse , ProcessKey.CHECK_IN_WINDOW
				 //  0  == ProcessKey.CHECK_IN_WAREHOUSE 
 				int[] processKeys = new int[]{ProcessKey.CHECK_IN_WAREHOUSE};
				if(15 == proJsId){
					processKeys = new int[]{ProcessKey.GateNotifyWareHouse,ProcessKey.CHECK_IN_WINDOW,ProcessKey.CHECK_IN_WAREHOUSE}; //表示的是Supervisor的任务，现在他有两种
				}
				if(0 == proJsId){
					processKeys = new int[]{ProcessKey.CHECK_IN_WAREHOUSE};  	// labor 的任务53
				}
				
 				
				DBRow[]	datas = checkInMgrZr.countMemberUncompletedTask(ps_id, proJsId, adgid, processKeys);
				result.add("data", datas);
			}
			if(method.equals("Presence")){
				long ps_id = login.get("ps_id",0l);
				//DBRow[]	datas = checkInMgrZr.getPresenceOfSuperior(ps_id);
				//result.add("data", datas);
			}
			
  		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (IOException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.DATATYPEINCRRECTLY;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		
		////system.out.println(StringUtil.convertDBRowsToJsonString(result));
		if(!isMutiArray){
			throw new JsonException(new JsonObject(result));
		}else{
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		}
	}
	
	
	private void fixDBRow(DBRow[] rows){
		for(DBRow row : rows){
			row.remove("pwd");
			row.remove("last_login_date");
			row.remove("post_date");
			row.remove("llock");
			row.remove("employe_summary");
			row.remove("file_path");
			row.remove("register_token");
			row.remove("description");
			row.remove("index_page");
			row.remove("entrytime");
			int proJsId = row.get("proJsId", 0);
			row.add("proJsId", proJsId);
		}
	}
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		return result ;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
	
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
	
}	
 
