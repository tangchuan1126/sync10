package com.cwc.service.android.action.zr;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
public class GetThumbnailAction extends ActionFatherController{
	
	/**
	 * android 根据机器的ID去查询 这台手机所创建的商品
	 * 这些商品 选择一张商品本身的图片去生成缩略图 存放在Thumbnail目录下面
	 * 然后返回一个zip文件(包含一个xml 和id的缩略图文件)
	 */
	private String fileFolderOr ;
	private String fileFolderTb ;
	private AndroidMgrIfaceZr androidMgrZr;
	private AdminMgrIFaceZJ adminMgrZJ;
	private String pictureXml = "pictureXml.xml";
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			fileFolderOr= Environment.getHome().replace("\\", "/")+"."+ "/upload/product/";
			fileFolderTb = Environment.getHome().replace("\\", "/")+"."+ "/upload/thumbnail/";
			DBRow[] rows = null;
			long countMaxPcid = 0l;
			int limit = 0 ;
			int ret = BCSKey.SUCCESS;//默认成功
			int err = BCSKey.FAIL;
			String fc = "";
			String machine = "";
			String password  = "";
			String xml = "";
			String loginAccount = "";
			
			try{
				xml = getPost(request.getInputStream());
				 
				fc = StringUtil.getSampleNode(xml,"fc");
				machine = StringUtil.getSampleNode(xml,"Machine");
				loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
				limit = Integer.parseInt(StringUtil.getSampleNode(xml,"Limit")) ;
				password = StringUtil.getSampleNode(xml,"Password");
				countMaxPcid = Long.parseLong(StringUtil.getSampleNode(xml,"MaxPcId"));
				DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				
				rows = androidMgrZr.getProductFileBy( countMaxPcid, limit);
				if(rows != null && rows.length > 0){
					for(DBRow temp : rows){
						//生成XML.生成缩略图。
						 
						if(temp.getString("file_name").length() > 0){
							String fileName = temp.getString("file_name");
							File tb = new File(fileFolderTb + fileName);
							if(!tb.exists()){
								File or = new File(fileFolderOr+fileName);
								if(or.exists()){
									int flag = makeSmallImage(or, fileFolderTb+fileName);
									if(flag == 1){
										//生成成功
										temp.add("thumbnail", fileName);
									}
								}
							}else{
								temp.add("thumbnail", fileName);
							}
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
				ret = BCSKey.FAIL ;
 			}finally{
 				//返回zip文件.如果是报错了也是应该返回的
 				createFile(rows, machine, fc, ret, err);
 			}
 			
 			byte[] buffer = new byte[1024 * 1024];
			int length = -1 ;
			FileInputStream  inputStream = new FileInputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+"_product_picture.zip"));
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode(machine+"_product_picture.zip","utf-8"));  

			
			OutputStream outputStream =  response.getOutputStream();
			while((length = inputStream.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close(); 
			
			
 		
	}
	
 
	
	private StringBuffer getDetails(DBRow[] rows){
		StringBuffer details = new StringBuffer("");
	 
//			/weight,length,width,heigth 
			for(DBRow temp : rows ){
				if(temp != null){
					details.append("<Details>");
					details.append("<Pcid>").append(temp.get("pc_id", 0l)).append("</Pcid>");
					details.append("<Width>").append(temp.get("width", 0.0d)).append("</Width>");
					details.append("<Pname>").append(temp.getString("p_name")).append("</Pname>");
					details.append("<Weight>").append(temp.get("weight", 0.0d)).append("</Weight>");
					details.append("<Length>").append(temp.get("length", 0.0d)).append("</Length>");
					details.append("<Heigth>").append(temp.get("heigth", 0.0d)).append("</Heigth>");
					details.append("<UnitName>").append(temp.getString("unit_name")).append("</UnitName>");
					details.append("<UnitPrice>").append(temp.getString("unit_price")).append("</UnitPrice>");
					details.append("<Thumbnail>").append(temp.getString("thumbnail")).append("</Thumbnail>");
					details.append("<Volume>").append(temp.get("volume", 0.0f)).append("</Volume>");
					details.append("<Pcode>").append(temp.getString("p_code")).append("</Pcode>");
					details.append("<CatalogId>").append(temp.get("catalog_id", 0L)).append("</CatalogId>");
					details.append("<SNLength>").append(temp.get("sn_size", 0)).append("</SNLength>");
					details.append("<CatalogTitle>").append(temp.getString("catalog_title")).append("</CatalogTitle>");
					details.append("<UnionFlag>").append(temp.get("union_flag", 0l)).append("</UnionFlag>");
					details.append("</Details>");
				}
			}
	 
		return details ;
	}
	
	public  int makeSmallImage(File srcImageFile,String dstImageFileName) throws Exception {
		int flag = 0 ;
        FileOutputStream fileOutputStream = null;
        JPEGImageEncoder encoder = null;
        BufferedImage tagImage = null;
        Image srcImage = null;
        try{
            srcImage = ImageIO.read(srcImageFile);
            int srcWidth = srcImage.getWidth(null);//原图片宽度
            int srcHeight = srcImage.getHeight(null);//原图片高度
            int dstMaxSize = 150;//目标缩略图的最大宽度/高度，宽度与高度将按比例缩写
            int dstWidth = srcWidth;//缩略图宽度
            int dstHeight = srcHeight;//缩略图高度
            float scale = 0;
            //计算缩略图的宽和高
            if(srcWidth>dstMaxSize){
                dstWidth = dstMaxSize;
                scale = (float)srcWidth/(float)dstMaxSize;
                dstHeight = Math.round((float)srcHeight/scale);
            }
            srcHeight = dstHeight;
            if(srcHeight>dstMaxSize){
                dstHeight = dstMaxSize;
                scale = (float)srcHeight/(float)dstMaxSize;
                dstWidth = Math.round((float)dstWidth/scale);
            }
            //生成缩略图
            tagImage = new BufferedImage(dstWidth,dstHeight,BufferedImage.TYPE_INT_RGB);
            tagImage.getGraphics().drawImage(srcImage,0,0,dstWidth,dstHeight,null);
            fileOutputStream = new FileOutputStream(dstImageFileName);
            encoder = JPEGCodec.createJPEGEncoder(fileOutputStream);
            encoder.encode(tagImage);
            fileOutputStream.close();
            fileOutputStream = null;
            flag =  1 ;
        }finally{
            if(fileOutputStream!=null){
                try{
                    fileOutputStream.close();
                }catch(Exception e){
                }
                fileOutputStream = null;
            }
            encoder = null;
            tagImage = null;
            srcImage = null;
          
        }
        return flag ;
    }
	/**
	 * 生成zip文件
	 * 	1.首先是生成.xml文件
	 * 	2.然后把图片放入进来
	 * @param machine
	 */
	private void createFile( DBRow[] rows , String machine ,String fc , int ret , int err) throws Exception {
		 
		 
		ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+"_product_picture.zip")));	 
		//xml 的文件
		putXmlInZip(zipOutputStream, rows, machine, fc, ret, err);
		if(rows != null && rows.length > 0){
			for(DBRow row : rows){
				String thumbnail = row.getString("thumbnail");
				if(thumbnail != null && thumbnail.length() > 0){
					putPictureInZip( zipOutputStream ,new File(fileFolderTb+thumbnail),thumbnail);
				}
			}
		}
 		zipOutputStream.flush();
 		zipOutputStream.close();
	}
	 
	private void putXmlInZip(ZipOutputStream zipOutputStream , DBRow[] rows , String machine ,String fc , int ret , int err) throws Exception {
		String values = "" ;
		if(rows != null && rows.length > 0){
			values = createXml(fc, ret, err, getDetails(rows).toString(), rows.length);
		}else{
			values = createXml(fc, ret, err, "", 0);
		}
		ZipEntry entry = new ZipEntry(pictureXml);
 		zipOutputStream.putNextEntry(entry);
 		zipOutputStream.write(values.getBytes());
	}
	private void putPictureInZip(ZipOutputStream zipOutputStream ,File file,String fileName){
		 FileInputStream fileInputStream = null; 
		try{
			ZipEntry entry = new ZipEntry(fileName);
			zipOutputStream.putNextEntry(entry);
		 
	 		
			fileInputStream = new FileInputStream(file); 
			 byte[] buffer = new byte[1024 * 20] ;
			 int length = -1 ;
			 while((length =  fileInputStream.read(buffer)) != -1){
				 zipOutputStream.write(buffer, 0, length);
			 }
		}catch (Exception e) { 
 		}finally{
 			try {
 				if(fileInputStream != null)
 					fileInputStream.close();
			} catch (IOException e) {
 				e.printStackTrace();
			}
 		}
	}
	private String createXml(String fc,int ret,int err,String details,int postcount){
 		StringBuffer boxQtyXml = new StringBuffer();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		boxQtyXml.append("<err>"+err+"</err>");
		boxQtyXml.append("<Detail>");
		if(details.length() > 1 )
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		return boxQtyXml.toString();
	}
	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try {
			     input.close();
			    } catch (Exception f) {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData));
	}
}
