package com.cwc.service.android.action.zr;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.CheckInMgrZr;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

/**
 * 给郭春阳 重新写DockCheckIn的接口
 * @author win7zr
 *
 */
public class AndroidDockCheckInAction extends ActionFatherController{

	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	private AdminMgrIFaceZJ adminMgrZJ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
				DBRow data = getRequestData(request);

 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);

				
 				String method = data.getString("Method") ;
 				
 				/**
 				 * 过滤出Task下面的设备
 				 */
 				if(method.equalsIgnoreCase("EntryTaskEquipment")){
  	 				long entry_id = data.get("entry_id", 0l);
 					DBRow[] datas=  checkInMgrZr.getEntryTaskEquipment(entry_id);	
 					if(datas == null || datas.length < 1){
 						throw new NoRecordsException() ;
 					}
 					result.add("data", datas);
 				}
 				if(method.equalsIgnoreCase("GetDoorAndSpot")){
 					long entry_id = data.get("entry_id", 0l);
 					int resources_type = data.get("occupy_type", -1);
 					long ps_id = adminLoggerBean.getPs_id();
 					int request_type =  data.get("request_type", 0);
 					if(request_type == 0){
 						request_type = CheckInMgrZr.RequestAll;
 					}
 					
 					HoldDoubleValue<DBRow[], DBRow[]> doorAndSpots = checkInMgrZr.androidGetResources(ps_id, entry_id, request_type,resources_type);
 					HoldDoubleValue<DBRow[], DBRow[]> notFreeDoorAndSpots = checkInMgrZr.androidGetNotFreeResources(ps_id,resources_type);

 					result.add("door", doorAndSpots.a);
 					result.add("spot", doorAndSpots.b);
 					result.add("notfreedoor", notFreeDoorAndSpots.a);
 					result.add("notfreespot", notFreeDoorAndSpots.b);
 				}
 				//加载warehouse 数据 dock check in 输入Entry调用的接口
 				/*if(method.equalsIgnoreCase("DockCheckInSearchEntryDetail")){
  	 				long equipment_id = data.get("equipment_id", 0l); //(request, "equipment_id");
  	 				long entry_id = data.get("entry_id", 0l);
 					DBRow datas=  checkInMgrZr.dockCheckInSearchEntryDetail(entry_id,equipment_id,adminLoggerBean);		
 					result.add("data", datas);
 				}*/
 				//删除warehouse 通知
 				if(method.equalsIgnoreCase("DockCheckInDeleteNotice")){
 	 			/*	DBRow datas =  checkInMgrZr.dockCheckInDeleteNotice(request,adminLoggerBean,true);
 					result.add("data", datas);*/
 				}
 				if(method.equals("DockCheckInSubmit")){
					long adid= adminLoggerBean.getAdid();
					long entry_id = data.get("entry_id", 0l);
					String delivery_seal = data.getString("delivery_seal");
					String pickup_seal = data.getString("pickup_seal");
					String filePath = data.getString("filePath");
					checkInMgrZr.dockCheckInSubmit(entry_id, adid, delivery_seal, pickup_seal, filePath,true,request);  //checkInMgrZwb.androidWahouseCheckIn(infoId, adid, delivery_seal,pickup_seal,filePath);
					
 				}
 				//dock check AssignTask
 				if(method.equals("DockCheckInAssignTask")){
  					/*DBRow datas=checkInMgrZr.dockCheckInAssignTask(request,adminLoggerBean);
 					result.add("data", datas);*/
 	  			}
 				
 				//把任务移动到指定的门上
 			/*	if(method.equalsIgnoreCase("DockCheckInTaskMoveToDoor")){
 					
 					//checkInMgrZr.mutiDockClose(data, adminLoggerBean)
 					long entry_id = data.get("entry_id", 0l);
 					long equipment_id = data.get("equipment_id", 0l);
 					long sd_id = data.get("sd_id", 0l); //移动到那个门
 					int moved_resource_type = data.get("moved_resource_type", 0); //移动的资源类型
 					long move_resouce_id = data.get("moved_resouce_id", 0l);		  //移动的资源Id
 					
 					checkInMgrZr.dockCheckInTaskMoveToDoor(entry_id, equipment_id, sd_id, 
 							moved_resource_type, move_resouce_id, adminLoggerBean);
 					
 					DBRow datas =  checkInMgrZr.dockCheckInSearchEntryDetail(entry_id,equipment_id,adminLoggerBean);		
 					result.add("data", datas);
 				}*/
 
 			}catch(CheckInNotFoundException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound ;
 			}catch(CheckInEntryIsLeftException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInEntryIsLeftException ;
			}catch(DockCheckInFirstException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DockCheckInFirstException ;
			}catch (NoPermiessionEntryIdException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;	
	 		}catch (DataFormatException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DataFormateException;
 			}catch (LoadIsCloseException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LoadIsCloseException;
			}catch (NoRecordsException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoRecordsException;
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
			////system.out.println(StringUtil.convertDBRowsToJsonString(result) + "....");
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
 		
			
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
}
