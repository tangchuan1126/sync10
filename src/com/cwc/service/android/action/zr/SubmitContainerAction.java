package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
 






import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.ContainerNotFullException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zr.TempContainerMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.service.android.action.mode.ContainerNode;
import com.cwc.util.AndroidUtil;
  

public class SubmitContainerAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private TempContainerMgrIfaceZr tempContainerMgrZr ;
	
	
	@SuppressWarnings("finally")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				//DBRow fromAndroid = getJsonObjectFromRequest(request);
				Map<String,Object> data = AndroidUtil.getDataFromAndroidXml(request.getInputStream());
				String account = (String)data.get("LoginAccount");
				String password = (String)data.get("Password");
				String method = (String)data.get("Method");
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(account, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				if(method.equals("SubmitContainer"))
				{
					//result = androidMgrZr.submitContainer(fromAndroid);
				}
				if(method.equals("SubmitTempContainer"))
				{
//					result = tempContainerMgrZr.submitContainer(fromAndroid);启用图数据库方式
 					
					tempContainerMgrZr.submitContainerTemp(data,login.get("ps_id",0l));
				}
				if(method.equals("CopyContainer"))
				{
					/*result =  new DBRow();
					String containerIdString = fromAndroid.getString("ContainerId");
					long containerId = Long.parseLong(containerIdString);
					//system.out.println(containerId);*/
 				}
 				 
			}catch (NullPointerException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
			}catch (ContainerNotFullException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
 			}catch (ContainerNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LPNotFound;
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (IOException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}
			finally
			{
				result.add("ret", ret);
				result.add("err", err);
 				throw new JsonException(new JsonObject(result));
 			}
	}
	/**
	 * 从request 对象中获取XMl文件。
	 * 然后解析出来生成DBRow 
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws SystemException 
	 * 
	 * 
	 * dbrow 
	 * 	loginAccount
	 * 	Password
	 * 	Machine
	 * 	data:{List<DBRow[]>}
	 *  	  
	 * 
	 */
	@SuppressWarnings("unchecked")
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException, SystemException{
		
		DBRow result = new DBRow();
		 
		InputStream inputStream =  request.getInputStream();
        SAXReader reader = new SAXReader();  
		
        try 
        {
			Document document = reader.read(inputStream);
			
			Element root = document.getRootElement();
			
			
	 
			List<Node> listLoginAccount = 	root.selectNodes("LoginAccount");
			List<Node> listPassword = 	root.selectNodes("Password");
			List<Node> listMachine = 	root.selectNodes("Machine");
			List<Node> listMethod = 	root.selectNodes("fc");
			List<Node> listContainer = 	root.selectNodes("ContainerId");

			result.add("LoginAccount", listLoginAccount.get(0).getText());
			result.add("Password", listPassword.get(0).getText());
			result.add("Machine", listMachine.get(0).getText());
			result.add("Method", listMethod.get(0).getText()) ;
			if(listContainer != null && listContainer.size() > 0 ){
				result.add("ContainerId", listContainer.get(0).getText()) ;
			}
			List<DBRow> datas = new ArrayList<DBRow>();
			List<ContainerNode> listContainerNode = new ArrayList<ContainerNode>();
			ContainerNode rootNode = null ;
			 Element details =	root.element("Detail").element("Details");
			 if(details != null){
				 List<Element> containers =  details.elements("Container");
				 if(containers != null && containers.size() > 0 ){
					 for(int index = 0 , count = containers.size() ; index < count ; index++ ){
						 Element container = containers.get(index);
						 long con_id = 	Long.parseLong((container.attributeValue("con_id")));
						 int container_type = Integer.parseInt((container.attributeValue("container_type")));
						 long container_type_id = Long.parseLong((container.attributeValue("container_type_id")));
						 long parent_con_id = Long.parseLong((container.attributeValue("parent_con_id")));
						 int parent_container_type = Integer.parseInt((container.attributeValue("parent_container_type")));
						 long parent_container_type_id = Long.parseLong((container.attributeValue("parent_container_type_id")));
						 	
						 long titleId = Long.parseLong((container.attributeValue("title_id")));
						 String lotNumber = container.attributeValue("lotNumber");
						 
						 
						 
						 DBRow containerProduct = new DBRow();
						 
						 containerProduct.add("con_id", con_id);
						 containerProduct.add("container_type",container_type);
						 containerProduct.add("container_type_id",container_type_id);
						 containerProduct.add("parent_con_id",parent_con_id);
						 containerProduct.add("parent_container_type",parent_container_type);
						 containerProduct.add("parent_container_type_id",parent_container_type_id);
						 containerProduct.add("title_id", titleId);
						 containerProduct.add("lot_number", lotNumber);

						 
						 //添加到ContainerNode中
						 ContainerNode containerNode = new ContainerNode(containerProduct);
						 listContainerNode.add(containerNode);
						 if(parent_con_id == 0l){
							 rootNode = containerNode;
						 }
						 
						 
						 List<Element> products = container.elements("Product");
						 if(products != null && products.size() > 0){
							 List<DBRow> innerProducts = new ArrayList<DBRow>();
							 for(Element product : products){
								 //<Product sn='' qty='2.0' pcid='1000030'></Product>
								 String sn = product.attributeValue("sn");
								 double cp_quantity = Double.parseDouble(product.attributeValue("qty"));
								 long cp_pc_id = Long.parseLong(product.attributeValue("pcid"));
								 String cp_lp_name =  product.attributeValue("cp_lp_name");
								 
								 ////system.out.println("cp_pc_id : "+cp_pc_id + " sn : " + sn + "cp_quantity : " + cp_quantity);
 								 DBRow productInContainer = new DBRow();
								 productInContainer.add("cp_sn", sn);
								 productInContainer.add("cp_quantity", cp_quantity);
								 productInContainer.add("cp_pc_id", cp_pc_id);
								 productInContainer.add("cp_lp_id", con_id);
								 productInContainer.add("cp_lp_name", cp_lp_name);
								 innerProducts.add(productInContainer);
						 
							 }
							 containerProduct.add("products", innerProducts.toArray(new DBRow[innerProducts.size()]));
						 }
						 datas.add(containerProduct);
					 }
				
					 
				 }
			 }
			 result.add("datas", datas);
			 if(rootNode != null && listContainerNode.size() > 0){
				 ContainerNode.initContainerLoading(rootNode, listContainerNode);
				 result.add("rootNode", rootNode);
				 result.add("listContainerNode", listContainerNode);
				 result.add("childList", ContainerNode.initChildList(listContainerNode));
			 }
	 
		} catch (DocumentException e) {
 			e.printStackTrace();
		}  
	 
		return result ;
	}
	
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	public void setTempContainerMgrZr(TempContainerMgrIfaceZr tempContainerMgrZr) {
		this.tempContainerMgrZr = tempContainerMgrZr;
	}
	
	 
}
