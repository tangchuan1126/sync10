package com.cwc.service.android.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPushMgrIfaceZr;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;

/**
 * @author 	Zhengziqi
 * 2014年10月24日
 *
 */
public class PushMessageByGCMAction extends ActionFatherController
{
	private AndroidPushMgrIfaceZr androidPushMgrZr;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private ScheduleMgrIfaceZR scheduleMgrZr ;
 	
	
	
	
	private CheckInMgrIfaceZr checkInMgrZr ;
	
	static Logger logger = Logger.getLogger("ACTION");
	
	 /**
	  * 	machine : 表示机器的型号
	  * 	transport_id : 表示的是transport_id
	  */
	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		
		try {
			DBRow fromAndroid = getRequestData(request);
  			if(!fromAndroid.getString("Version").equals(AndroidUtil.getVersion())){
				throw new AppVersionException();
			}
  			//首先进行登录
			DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
			if(login == null){
				throw new MachineUnderFindException();
			}
			AndroidPermissionUtil.androidSetSession(login, request);
			long adid = login.get("adid", 0l);
			 
			 // 15 == ProcessKey.GateNotifyWareHouse , ProcessKey.CHECK_IN_WINDOW
			 //  0  == ProcessKey.CHECK_IN_WAREHOUSE
			String method = fromAndroid.getString("Method");
 
			if(StringUtils.equalsIgnoreCase("Register", method)){
				
 				String regId = fromAndroid.getString("regId");
				androidPushMgrZr.updateRegisterReflection(adid, regId);
			}
			 
			if(method.equalsIgnoreCase("MessageQuery")){
				/*	long schedule_id = fromAndroid.get("schedule_id", 0l);
				DBRow[] datas = scheduleMgrZr.getAllNotFinishScheduleByAdid(adid, 1, schedule_id, 10);
				result.add("datas", datas);*/
				long message_id = fromAndroid.get("message_id", 0l);
			    DBRow[] datas = androidPushMgrZr.queryMessage(message_id, 20, adid, 0);
				result.add("datas", datas);
			}
		 
			
			if(method.equalsIgnoreCase("ScheduleType")){		//得到不同类型的Task的数量
				DBRow[] scheduleTypes = scheduleMgrZr.getNotFinishScheduleType(adid);
				if(scheduleTypes == null){
 					throw new NoRecordsException();
 				}
				result.add("datas", scheduleTypes);
			}
			if(method.equalsIgnoreCase("AssignTaskList")){
				DBRow[] assignTasks = checkInMgrZr.getAssignTaskList(adid);
				if(assignTasks == null){
 					throw new NoRecordsException();
 				}
 				result.add("datas", assignTasks);
			}
			if(method.equalsIgnoreCase("ScheduleUnFinishAndNoAssociate")){ //当前的Schedule没有和任何的业务关联
				DBRow[] datas =  scheduleMgrZr.queryScheduleUnFinishAndNoAssociate(login);
 				result.add("datas", datas);
 				if(datas == null){
 					throw new NoRecordsException();
 				}
			}
			//getAssignTaskDetail
			/*if(method.equalsIgnoreCase("AssignTaskDetail")){
				long entry_id = fromAndroid.get("entry_id",0l);
				long equipment_id = fromAndroid.get("equipment_id",0l);
				DBRow data = checkInMgrZr.getAssignTaskDetail(entry_id, equipment_id, login);
				//DBRow data = checkInMgrZr.assginTaskDetaillByEntryAndEquipment(entry_id, equipment_id);
				result.add("data", data);
			}
			if(method.equalsIgnoreCase("AssignTask")){		
				String execute_user_ids = fromAndroid.getString("execute_user_ids");
				String schedule_ids = fromAndroid.getString("schedule_ids");
				long entry_id = fromAndroid.get("entry_id", 0l);
				long equipment_id = fromAndroid.get("equipment_id", 0l);
				checkInMgrZr.singleWareHouseManagerAssignTask(execute_user_ids, schedule_ids, login);
				DBRow data = checkInMgrZr.getAssignTaskDetail(entry_id, equipment_id, login); //在回传数据
				result.add("data", data);
			}*/
			if(method.equalsIgnoreCase("AssignTaskMoveToDoor")){	//分配任务的时候移动任务到其他门上
				long entry_id = fromAndroid.get("entry_id", 0l);
				long equipment_id = fromAndroid.get("equipment_id", 0l);
				long sd_id = fromAndroid.get("sd_id", 0l); //移动到那个门
				int moved_resource_type = fromAndroid.get("moved_resource_type", 0); //移动的资源类型
				long move_resouce_id = fromAndroid.get("moved_resource_id", 0l);	 //移动的资源Id
				String from = fromAndroid.getString("from");
				
				checkInMgrZr.assignTaskMoveToDoor(entry_id, equipment_id, sd_id, moved_resource_type,
						move_resouce_id, login.get("adid", 0l));
				
				DBRow datas = checkInMgrZr.assginTaskDetaillByEntryAndEquipment(entry_id, equipment_id,true);
 					result.add("data", datas);
		 
				
			}
			//Loader在Task界面选择他的Task进入出现的列表
			if(method.equalsIgnoreCase("LoaderExecuteTaskList")){
				DBRow[] datas = checkInMgrZr.executeTaskList(adid);
				/*if(datas == null){
 					throw new NoRecordsException();
 				}*/
				result.add("datas", datas == null ?  new DBRow[0] : datas);
			}
			if(method.equalsIgnoreCase("IsNotifyPushAndroidMessage")){	
				//1 表示接受通知 ,0 表示不接受通知信息
				int notifyFlag = fromAndroid.get("message_notify_flag", 0);
				DBRow updateRow = new DBRow();
				updateRow.add("message_notify_flag", notifyFlag);
				androidPushMgrZr.updateAdmin(adid, updateRow);
			}
			//schedule count 某个人有多少Task
			if(method.equalsIgnoreCase("MessageQueryCount")){
				int countMessage = scheduleMgrZr.countNotFinishSchedule(adid);
 				result.add("countMessage", countMessage);
			}
			/*if(method.equalsIgnoreCase("NewSchedule")){
				long schedule_id = fromAndroid.get("schedule_id", 0l);
			//checkInMgrZr.refreshLastRefreshTime(adid);
 				DBRow schedule = scheduleMgrZr.newSchedule(adid);
				if(schedule != null && schedule.get("schedule_id", 0l) > schedule_id){
					AndroidPushMessge androidPushMessage = AndroidPushMessge.getPushMessage(schedule);
 				//	Map<String, String>   map =  androidPushMessage.getMessage().getData();
					DBRow appendDate =  DBRowUtils.convertToDBRow( new JSONObject(map.get("data")));
					appendDate.add("new_message", 1);
					appendDate.add("schedule_id", schedule.get("schedule_id", 0l));
					//result.add("datas", androidPushMessage.getMessage().getData());  
					result.add("datas", appendDate);
				}
				int countMessage = scheduleMgrZr.countNotFinishSchedule(adid);
 				result.add("count_message", countMessage);
			}*/
 		}catch(EquipmentHadOutException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.EquipmentHadOutException ;
		}catch(CheckInEntryIsLeftException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInEntryIsLeftException ;
		}catch (NoPermiessionEntryIdException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoPermiessionEntryIdException;	
 		}catch (LoadIsCloseException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LoadIsCloseException;
		}catch (NoRecordsException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoRecordsException;
		}catch (AppVersionException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.AppVersionException;
		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		// //system.out.println(StringUtil.convertDBRowsToJsonString(result));

		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getRequestData(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow ;
	}
	
	public AdminMgrIFaceZJ getAdminMgrZJ() {
		return adminMgrZJ;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public AndroidPushMgrIfaceZr getAndroidPushMgrZr() {
		return androidPushMgrZr;
	}

	public void setAndroidPushMgrZr(AndroidPushMgrIfaceZr androidPushMgrZr) {
		this.androidPushMgrZr = androidPushMgrZr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
}
