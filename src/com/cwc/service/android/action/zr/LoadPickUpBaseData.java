package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class LoadPickUpBaseData  extends ActionFatherController {

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			//读取zip文件然后下载
		String fileFolderOr= Environment.getHome().replace("\\", "/")+"."+ "/upload/";

			File zip = new File(fileFolderOr+"GZ-01OutList.zip");
			FileInputStream in = new FileInputStream(zip);
			ServletOutputStream  out =  response.getOutputStream() ; 
			int length = -1 ;
			byte[] buffer = new byte[1024];
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("zf"+"_product_picture.zip","utf-8"));  

			while(( length =  in.read(buffer)) != -1){
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();
			out.close();
			
	}

}
