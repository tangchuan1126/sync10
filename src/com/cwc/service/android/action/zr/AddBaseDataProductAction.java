package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
/**
 * 处理一个zip 文件
 * @author Administrator
 *
 */
public class AddBaseDataProductAction extends ActionFatherController {
	
 	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private String zipFile = "_product.zip";
	private String zipfolder =  "_product";
	private AndroidMgrIfaceZr androidMgrZr;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
 	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 			//
		    int  ret = BCSKey.SUCCESS;
		    int  err = BCSKey.FAIL;
			String account = "" ;
			String password = "" ;
			long pcid = 0l ;
			String parentDir = "" ;
			try{
				ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
				List<FileItem> items = uploadHandler.parseRequest(request);
			    for (FileItem item : items) {
	            	if(item.isFormField()){
	            		if( item.getFieldName().equals("account")){
	            			account = item.getString();
	            		}
	            		if(item.getFieldName().equals("password")){
	            			password = item.getString();
	            		}
	            	}
	            }
			 
				DBRow login = adminMgrZJ.MachineLogin(account, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAccount(account);
				adminLoggerBean.setAdgid(login.get("adgid",0l));
				adminLoggerBean.setAdid(login.get("adid",0l));
				adminLoggerBean.setPs_id(login.get("ps_id",0l));
				adminLoggerBean.setEmploye_name(login.getString("employe_name"));
				adminLoggerBean.setTitles(androidMgrZr.getLoginTitles(login.get("adid",0l)));
				HttpSession session = request.getSession(true); 
				session.setAttribute(Config.adminSesion,adminLoggerBean);
			    for(FileItem item : items){
			    	if(!item.isFormField()){
			    		InputStream inputStream = item.getInputStream() ;
			    		parentDir = saveZipFile(inputStream);
			    	}
			    }
			     pcid = androidMgrZr.addBaseProductByzip(parentDir, adminLoggerBean);
			    
			    
			}catch (ProductNameIsExistException e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductNameIsExist;//商品名称已存在
			}
			catch (ProductCodeIsExistException e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductCodeIsExist;//商品条码已存在
			}
			catch(ProductNotExistException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.NOTEXITSPRODUCT; //商品不存在
			}
			catch(ProductUnionSetCanBeProductException	e)//已经设置了的组合套装不能做子商品
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductUnionCannotDoChild;
			}
			catch(ProductUnionIsInSetException	e)//同一个组合套装下，商品不能重复
			{
				ret = BCSKey.FAIL;
				err = BCSKey.WithProductUnionNotRepeat;
			}
			catch(ProductCantBeSetException e)//已经作为组合的商品，不能变成组合，在其下创建商品
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductUnionNoCreateProduct;
			}
			catch (XMLDataErrorException e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
			}
			catch(MachineUnderFindException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.LOGINERROR;//登录失败，账号密码错误
			}
			catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
			{
				ret = BCSKey.FAIL;
				err = BCSKey.DATATYPEINCRRECTLY;
			}
			catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
			{
				ret = BCSKey.FAIL;
				err = BCSKey.NETWORKEXCEPTION;
			}
			catch(OperationNotPermitException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
			}
			catch (ProductInUnionException e)//商品有组合关系，不能删除
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductInUnionNotDel;
			}
			catch (ProductHasRelationStorageException e)//商品已建跟库存关系，不能删除
			{
				ret = BCSKey.FAIL;
				err = BCSKey.ProductHasRelationStorage;
			}
			catch (Exception e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.SYSTEMERROR;
				throw new SystemException(e,"bcsrequest:",log);
			}finally{
 				responseXML(ret, err, response, pcid);
 			}
		
			
	}
	//保存到文件
	private String saveZipFile(InputStream inputStream) throws Exception{
	 
		long time =  System.currentTimeMillis();
		FileOutputStream fileOutputStream = null;	 
		try{
			String parentDir = Environment.getHome()+"/upl_excel_tmp/"+time+zipfolder;
			File file = new File(Environment.getHome()+"/upl_excel_tmp/"+time+zipFile);
			fileOutputStream = new FileOutputStream(file);
			byte[] buffer = new byte[1024 * 50];
			int length = -1 ;
			while((length =  inputStream.read(buffer)) != -1){
				fileOutputStream.write(buffer, 0, length);
			}
			//解压出zip文件
			
			createZipFile(file,parentDir);
			return parentDir;
		}catch (Exception e) {
			throw e ;
 		}finally{
 			inputStream.close();
 			fileOutputStream.close();
 		}
	}
	//解压出zip文件
	private void createZipFile(File file,String  parentDir)throws Exception {
		FileInputStream fileInputStream = null;
		ZipFile zipFile = new ZipFile(file);
		ZipEntry entry = null; 
 		ZipInputStream zipInputStream = null;
 		File parentDirs = new File(parentDir);
 		if(!parentDirs.exists()){parentDirs.mkdirs();}
 		try{
 			fileInputStream = new FileInputStream(file);
 			zipInputStream = new ZipInputStream(fileInputStream);
			byte[] bufferd = new byte[1024 * 50];
			int length = -1 ;
			List<String> files  = new ArrayList<String>();
			while((entry = zipInputStream.getNextEntry()) != null){
				String name = entry.getName();
				
				File tempFile = new File(parentDir +File.separator+name);
				tempFile.createNewFile();
				InputStream inputStream = zipFile.getInputStream(entry);
				OutputStream outputStream = new FileOutputStream(tempFile);
				
				while((length = inputStream.read(bufferd)) != -1){
					outputStream.write(bufferd, 0, length);
				}
				files.add(name);
				inputStream.close();
				outputStream.close();
			}
 		}catch (Exception e) {
 			throw e ;
 		}finally{
			if(zipInputStream != null){zipInputStream.close();}
			if(fileInputStream != null){fileInputStream.close();}
		}
	}
	private void responseXML( int ret,int err,HttpServletResponse response,long id) //添加商品的时候返回一个Id
	throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
 		boxQtyXml.append("<ret>"+ret+"</ret>");
 
		boxQtyXml.append("<err>"+err+"</err>");
		boxQtyXml.append("<Pid>"+id+"</Pid>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
	//	logBcs.info("response:"+boxQtyXml.toString());
		////system.out.println(boxQtyXml.toString()+"----返回值");
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

}
