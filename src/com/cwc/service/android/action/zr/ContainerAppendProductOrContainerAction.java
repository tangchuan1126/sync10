package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * workContainer 添加商品 或者是添加Container
 * @author ZHANJIE
 *
 */
public class ContainerAppendProductOrContainerAction  extends ActionFatherController{
	
	private AndroidMgrIfaceZr androidMgrZr;

	private AdminMgrIFaceZJ adminMgrZJ;
 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	//读取
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				JSONObject json = getJsonObjectFromRequest(request);
				//{"LoginAccount":"xxf","ContainerId":100105,"subContainers":[{"container_id":100109}],"Machine":"LA-01","products":[{"container_id":0,"qty":1,"sn":"","pc_id":1000030},{"container_id":0,"qty":1,"sn":"","pc_id":1000031},{"container_id":0,"qty":2,"sn":"","pc_id":1000031},{"container_id":0,"qty":2,"sn":"","pc_id":1000030}],"Password":"xxf"}
				String loginAccount = json.getString("LoginAccount");
				String password = json.getString("Password");
				long container_id = json.getLong("ContainerId");
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				List<DBRow> subContainerRows = new ArrayList<DBRow>();
				List<DBRow> containerProducts = new ArrayList<DBRow>();
				
				convertContainerProducts(json, containerProducts);
				convertSubContainer(json, subContainerRows);
			
				androidMgrZr.appendContainer(container_id, subContainerRows, containerProducts);
 			}catch (ContainerNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LPNotFound;
 			}catch (IOException e) {
				//数据格式错误
				ret = BCSKey.FAIL ;
				err = BCSKey.DATATYPEINCRRECTLY;
 			}catch (MachineUnderFindException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}finally{
 				result.add("ret", ret);
				result.add("err", err);
 			}
			throw new JsonException(new JsonObject(result));

 	}
	//"container_id":0,"qty":1,"sn":"","pc_id":1000030,container_name
	private void convertContainerProducts(JSONObject json  , List<DBRow> arrayList){
		JSONArray productJsonArrays =	json.getJSONArray("products") ;

		if(productJsonArrays != null && productJsonArrays.size() > 0 ){
			for(int index = 0 , count = productJsonArrays.size() ; index < count ; index++ ){
				JSONObject container =	(JSONObject)productJsonArrays.get(index);
				DBRow temp = new DBRow();
 				temp.add("cp_pc_id", container.getLong("pc_id"));
				temp.add("cp_sn", container.getString("sn"));
				temp.add("cp_quantity", container.getDouble("qty"));
				temp.add("cp_lp_id", container.getLong("container_id"));
				temp.add("cp_lp_name", container.getString("container_name"));
				arrayList.add(temp);
			}
		}
	}
	private void convertSubContainer(JSONObject json  , List<DBRow> arrayList){
		
		JSONArray subContainerJsonArrays =	json.getJSONArray("subContainers") ;
		if(subContainerJsonArrays != null && subContainerJsonArrays.size() > 0 ){
			for(int index = 0 , count = subContainerJsonArrays.size() ; index < count ; index++ ){
				JSONObject container =	(JSONObject)subContainerJsonArrays.get(index);
				DBRow temp = new DBRow();
				temp.add("container_id", container.getLong("container_id"));
				arrayList.add(temp);
			}
		}
		
	}
	public JSONObject getJsonObjectFromRequest(HttpServletRequest request) throws IOException   {
		 	ServletInputStream inputStream = request.getInputStream() ;
		 	JSONObject json = null;
		 	try{
		 		String value = IOUtils.toString(inputStream);
		 		json = JSONObject.fromObject(value);
  		 	}finally{
		 		IOUtils.closeQuietly(inputStream);
		 	}
 		 	return json ;
	}

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	

}
