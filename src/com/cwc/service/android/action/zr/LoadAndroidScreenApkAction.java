package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class LoadAndroidScreenApkAction extends ActionFatherController{

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			byte[] buffer = new byte[1024 * 1];
			int length = -1 ;
			//读取apk .
			File updateApk = new File(Environment.getHome()+"/android/LargeScreen.apk");
			FileInputStream  inputStream = new FileInputStream(updateApk);
			response.setContentType("application/vnd.android.package-archive;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("update_screen.apk","utf-8"));  
			response.setContentLength((int)updateApk.length());
 			OutputStream outputStream =  response.getOutputStream();
			while((length = inputStream.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close(); 
		
		
	}

}
