package com.cwc.service.android.action.zr;

import java.io.IOException;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.ProductPictureDeleteException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.qa.ProductNotFindException;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.DataOrCountKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 处理商品基础信息的Action
 * @author zhangrui
 *
 */
public class BaseProductAction extends ActionFatherController {

	private AndroidMgrIfaceZr androidMgrZr;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj ;
	
	private ProductMgrIFace productMgr;
	
	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	
	@SuppressWarnings("unused")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			boolean isMutil = false ;
			try{
				DBRow fromAndroid = getJsonObjectFromRequest(request);
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				//模拟登陆
				//模拟登陆
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAccount(fromAndroid.getString("LoginAccount"));
				adminLoggerBean.setAdgid(login.get("adgid",0l));
				adminLoggerBean.setAdid(login.get("adid",0l));
				adminLoggerBean.setPs_id(login.get("ps_id",0l));
				adminLoggerBean.setEmploye_name(login.getString("employe_name"));
				HttpSession session = request.getSession(true);
				session.setAttribute(Config.adminSesion,adminLoggerBean);
				
				if(login == null){throw new MachineUnderFindException();}
				if(fromAndroid.getString("Method").equals("GetBaseProductInfo")){		//获取商品的基础信息
					result = androidMgrZr.getBaseProductInfo(fromAndroid.get("PId", 0l));
				}
				if(fromAndroid.getString("Method").equals("DeleteProductFile")){		//删除图片
					long pf_id = getProductPictureFileId(request);
					androidMgrZr.deleteProductFile(pf_id);
				}
				if(fromAndroid.getString("Method").equals("AddProductCode")){			//添加ProductCode
					DBRow productCode = getProductCode(request);
					DBRow[] datas = 	androidMgrZr.addProductCode(productCode, login);
					result.add("data", datas);
				}
				/*if(fromAndroid.getString("Method").equals("SimpleUpdateProduct")){			//simpleUpdateProduct
					DBRow productSimpleInfo = getSimpleProductInfo(request);
					androidMgrZr.addProductCode(productSimpleInfo, login);
				}*/
				if(fromAndroid.getString("Method").equals("LoadProductAndPicture")){
					int p = StringUtil.getInt(request, "page");				//第几页
					int pageSize = StringUtil.getInt(request, "pageSize");		//每页多少条数据
					PageCtrl pc = new PageCtrl();
					pc.setPageNo(p);
					pc.setPageSize(pageSize);
					DBRow[] datas = androidMgrZr.getBaseProductBy(pc);
					result.add("data", datas);
					if(datas != null){
						////system.out.println(datas.length);
					}
				}
				if(fromAndroid.getString("Method").equals("RefreshProduct")){
					long maxPcId = StringUtil.getLong(request, "MaxPcId");
					int length = StringUtil.getInt(request, "Length");
					String flag = StringUtil.getString(request,"Flag");
				 
					DBRow[] datas = androidMgrZr.getBaseProductBy(maxPcId, length,flag);
					 
					result.add("data", datas);
				}
				if(fromAndroid.getString("Method").equals("GetLoginTitle")){ // 
 					DBRow[] datas =	proprietaryMgrZyj.findProprietaryByAdminProduct(true, login.get("adid",0l), 0l, 0, 0, null, DataOrCountKey.DATA, request);
 					if(datas != null && datas.length > 0 ){
						DBRow[] fixResult = new DBRow[datas.length] ;
						for(int index =0 , count = datas.length ; index < count ; index++ ){
							DBRow row =  datas[index];
							row.remove("title_admin_id");
							row.remove("title_admin_adid");
							row.remove("title_admin_title_id");
							fixResult[index]  = row ;
						}
						result.add("data", fixResult);

					}
				}
				if(fromAndroid.getString("Method").equals("GetAllTitle")){
					long pc_id = fromAndroid.get("Pid", 0l) ;
					DBRow productRow = productMgr.getAllDetailProduct(pc_id);
					long pc_line_id = 0;
					long pc_catagory_id = 0;
  					
					if(null != productRow)
					{
						pc_line_id = productRow.get("product_line_id", 0L);
						pc_catagory_id = productRow.get("catalog_id", 0L);
  					}
					DBRow[] datas =	proprietaryMgrZyj.findProprietaryByAdminProduct(true, login.get("adid",0l), 0l, 0, 0, null,DataOrCountKey.DATA, request);
  					DBRow[] productLine = proprietaryMgrZyj.findProprietaryByAdidAndProductLineId(login.get("adid",0l), pc_line_id, pc_id, null, request);
   					DBRow[] productCatagory = proprietaryMgrZyj.findProprietaryByAdidAndProductCatagoryId(login.get("adid",0l), pc_catagory_id, pc_id, null, request);
   					DBRow[] selected = proprietaryMgrZyj.findProprietaryByPcSelf(login.get("adid",0l), pc_line_id,pc_catagory_id, pc_id, null, request);
  					 
					result.add("LoginTitle", fixTitles(datas));	//当前登录的人的title
					result.add("SelectTitle", fixTitles(selected))	;		//当前商品选择的title
					result.add("ProductLine", fixTitles(productLine))	;		//ProductLine title
					result.add("Catagory", fixTitles(productCatagory))	;		//商品分类的 title
				}
				/**
				 * 根据一个product_id 去获取这个商品的容器类型所有的配置
				 */
				if(fromAndroid.getString("Method").equals("ContainerInfo")){	//根据PC_ID去查询所有的关于这个商品的系统配置的Containertype的信息
					isMutil = true ;
					long pid = StringUtil.getLong(request,"pc_id");
					DBRow[] datas = androidMgrZr.getContainerInfoByPcId(pid);
					result.add("data", datas);
				}
				if(fromAndroid.getString("Method").equals("LoginTitle")){	//获取当前登录的人的title
					DBRow[] datas =	proprietaryMgrZyj.findProprietaryByAdminProduct(true, login.get("adid",0l), 0l, 0, 0, null,DataOrCountKey.DATA, request);
					result.add("data", datas);
				}
				if(fromAndroid.getString("Method").equals("SearchProductByDim")){
					//SearchProductByStr : str 有可能是pcid,有可能是barcode , 有可能是p_name
					//support like productName , barCode , pcid 
					String keyWord = StringUtil.getString(request, "keyword");
					DBRow[] datas = productMgr.getProductByKeyWord(keyWord);
					result.add("data", datas);
				}
				if(fromAndroid.getString("Method").equals("SearhProductByCondition")){
					String catalogId = "0";
					String productLineId = "0";
					int productType = 0 ;
					String title_id = StringUtil.getString(request, "title_id") ;
					PageCtrl pc = new PageCtrl();
					pc.setPageNo(StringUtil.getInt(request, "pageNo"));
					pc.setPageSize(StringUtil.getInt(request, "pageSize"));
					DBRow[] rows = proprietaryMgrZyj.filterProductAndRelateInfoByPcLineCategoryTitleId(true,catalogId,productLineId,productType, 0, 0,title_id, 0l, 0, 5,0,0, pc, request);
 					result.add("data", rows);
 					result.add("pageCount", pc.getPageCount());
				}
				//创建Container的时候需要的部分参数
				if(fromAndroid.getString("Method").equals("ContainerParam")){
					//客户仓库的信息
					DBRow[] pss = storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.CUSTOMER,null); 
					if(pss != null){
						DBRow[] fixPss = new DBRow[pss.length];
						for(int index = 0 , count = pss.length ; index < count ; index++ ){
							DBRow newRow = new DBRow();
							newRow.add("title", pss[index].getString("title"));
							newRow.add("id", pss[index].get("id",0l));

							fixPss[index] = newRow;
						}
						result.add("pss", fixPss);
					}else{
						result.add("pss", new DBRow[0]);
					}
				}

			}catch (ProductCodeIsExistException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.ProductCodeIsExist ;		
			}catch (ProductPictureDeleteException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.ProductPictureDeleteFailed ;
 			}catch (ProductNotFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.ProductNotFound ;
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (Exception e) {
			  ret = BCSKey.FAIL ;
			  err = BCSKey.SYSTEMERROR;
			} 
			result.add("ret", ret);
			result.add("err", err);
			if(isMutil){
				throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
			}else{throw new JsonException(new JsonObject(result));}
	}
	
	
	private DBRow[] fixTitles(DBRow[] datas){
		if(datas == null ){
			return new DBRow[]{};
		}
		DBRow[] fixResult = new DBRow[datas.length] ;
		for(int index =0 , count = datas.length ; index < count ; index++ ){
			DBRow row =  datas[index];
			row.remove("title_admin_id");
			row.remove("title_admin_adid");
			row.remove("title_admin_title_id");
			fixResult[index]  = row ;
		}
		return fixResult ;
	}
	 
	
	private DBRow getSimpleProductInfo(HttpServletRequest request) throws NumberFormatException, Exception  {
		DBRow result = new DBRow();
		result.add("p_name", StringUtil.getString(request, "p_name").trim().toUpperCase());
		result.add("unit_name", StringUtil.getString(request, "unit_name"));
		result.add("unit_price", StringUtil.getDouble(request, "unit_price"));
		result.add("weight", StringUtil.getDouble(request, "weight"));
		result.add("union_flag", StringUtil.getInt(request, "union_flag"));
		result.add("length", StringUtil.getDouble(request, "length"));
		result.add("width", StringUtil.getDouble(request, "width"));
		result.add("heigth", StringUtil.getDouble(request, "heigth"));
		result.add("sn_size", StringUtil.getInt(request, "sn_size"));
		result.add("pc_id", StringUtil.getLong(request, "pc_id"));
		return result;
	}
	/**
	 * product_code
	 * @param request
	 * @return
	 */
	private DBRow getProductCode(HttpServletRequest request){
		
		DBRow result = new DBRow();
		 
		result.add("p_code", StringUtil.getString(request, "p_code").toUpperCase());
		result.add("pc_id", StringUtil.getString(request, "pc_id"));
		result.add("code_type", StringUtil.getString(request, "code_type"));
	  
		return result ;
	}
	private long getProductPictureFileId(HttpServletRequest request){
		long pf_id = StringUtil.getLong(request, "pf_id");
		return pf_id;
	}

	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException{
		
		DBRow result = new DBRow();
		 
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("PId", StringUtil.getLong(request, "PId"));
		return result ;
	}

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
 
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}


	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}


	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj) {
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}

}
