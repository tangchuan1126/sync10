package com.cwc.service.android.action.zr;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddNewLpInfoAction extends ActionFatherController {
	
	static Logger logBcs = Logger.getLogger("BCS");
	
	private AdminMgrIFaceZJ adminMgrZJ;
	 
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	private AndroidMgrIfaceZr androidMgrZr;
	 
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			InputStream inputStream = request.getInputStream();
			int ret = BCSKey.FAIL;//默认 
			int err = BCSKey.DATASIZEINCORRECTLY;
			String fc = "";
			String machine = "";
			String password  = "";
			String xml = "";
			String loginAccount = "";
			long transportId = 0l;
			StringBuffer returnDetails = new StringBuffer();
			int postcount = 0;
			int billType = 0 ;
			String LpName = "" ;
			try{
				xml = getPost(inputStream);
				logBcs.info("request"+xml);
				fc = StringUtil.getSampleNode(xml,"fc");
				machine = StringUtil.getSampleNode(xml,"Machine");
				loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
				password = StringUtil.getSampleNode(xml,"Password");
				transportId = Long.parseLong(StringUtil.getSampleNode(xml, "BillId"));
				
				LpName = StringUtil.getSampleNode(xml, "LpName");
				
				DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				//返回一个ID值.
			 
				long returnId = androidMgrZr.addNewLp(LpName, transportId, billType);
				returnDetails.append(returnId +"");
				ret = BCSKey.SUCCESS ;
			}catch(MachineUnderFindException e){
				ret = BCSKey.FAIL;
				err = BCSKey.LOGINERROR;	//登录失败，账号密码错误
			}finally{
				responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
			}
	}
	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)throws Exception{
		response.setContentType("text/html; charset=UTF-8");		
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		boxQtyXml.append("<err>"+err+"</err>");
		boxQtyXml.append("<Detail>");
		if(details.length() > 1 )
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		logBcs.info("response:"+boxQtyXml.toString());
	 	try {
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}
	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try {
			     input.close();
			    } catch (Exception f) {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData));
	}
}

