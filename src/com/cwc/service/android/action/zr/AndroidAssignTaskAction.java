package com.cwc.service.android.action.zr;

 
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.EntryTaskHasFinishException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

/**
 * Assign Task 代替原来的AndroidDockCheckIn
 * @author win7zr
 *
 */
public class AndroidAssignTaskAction extends ActionFatherController{


	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	private AdminMgrIFaceZJ adminMgrZJ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
				DBRow data = getRequestData(request);

 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
				//模拟登陆
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);
 				String method = data.getString("Method") ;
 				
 				/**
 				 * 通过Entry Id 去获取 分配任务的列表
 				 * {
 				 * 	 entry:	
 				 *   equipment_type:
 				 *   equipment_id:
 				 *   equipment_number:
  				 * 	 number:3
 				 * }
 				 */
 				if(method.equals("AssginTaskListByEntry")){
 					long entry_id = data.get("entry_id", 0l);
 					checkInMgrZr.checkEntryIsLeft(entry_id);
 					DBRow[] datas =checkInMgrZr.assginTaskListByEntry(entry_id,adminLoggerBean);
 					result.add("data", datas);
 				}
 				if(method.equals("AssginTaskDetailByEntryAndEquipment")){
 					long entry_id = data.get("entry_id", 0l);
 					long equiment_id = data.get("EQUIPMENT_ID", 0l);
 					
 					DBRow datas = checkInMgrZr.assginTaskDetaillByEntryAndEquipment(entry_id, equiment_id,true);
 					result.add("data", datas);
 				}
 				//AndroidAssignTaskAction-AssginTaskDetailByEntryAndEquipment,
 				if(method.equals("AssginTaskByEntry")){
 					long entry_id = data.get("entry_id", 0l);
 					long equiment_id = data.get("EQUIPMENT_ID", 0l);
 					String execute_user_ids = data.getString("execute_user_ids");
 					String values = data.getString("values");
 					List<HoldDoubleValue<Long, Integer>> listArrays =  getAssignTaskByEntryData(values);
 					checkInMgrZr.assignTaskByEntry(execute_user_ids, listArrays, adminLoggerBean);
 					//重新查询返回结果
 					DBRow datas = checkInMgrZr.assginTaskDetaillByEntryAndEquipment(entry_id, equiment_id,true);
 					result.add("data", datas);
 				}
 				/**
 				 * 分配任务的时候改门的 通过Details
 				 */
 				if(method.equalsIgnoreCase("AssignTaskChangeDoorByDetail")){
 					String detail_ids = data.getString("detail_ids");
 					long resouces_id = data.get("resouces_id", 0l);
 					int resouces_type = data.get("resouces_type", 0);
 					long equipment_id = data.get("equipment_id", 0l);
 					long entry_id = data.get("entry_id", 0l); 
 					checkInMgrZr.assignTaskChangeDoorByDetails(detail_ids, resouces_id, resouces_type, equipment_id, adminLoggerBean, entry_id);
 					DBRow datas = checkInMgrZr.assginTaskDetaillByEntryAndEquipment(entry_id, equipment_id,true);
 					result.add("data", datas);
 				}if(method.equals("AssignTaskList")){
 					DBRow[] datas = checkInMgrZr.countMemberTaskRelEqptTask(adminLoggerBean.getAdid());
 					result.add("data", datas);
 				}
 			}catch(EntryTaskHasFinishException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.EntryTaskHasFinishException ;
 			}catch(CheckInNotFoundException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound ;
 			}catch(CheckInEntryIsLeftException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInEntryIsLeftException ;
			}catch(DockCheckInFirstException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DockCheckInFirstException ;
			}catch (NoPermiessionEntryIdException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;	
	 		}catch (DataFormatException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DataFormateException;
 			}catch (LoadIsCloseException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LoadIsCloseException;
			}catch (NoRecordsException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoRecordsException;
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
		 //	//system.out.println(StringUtil.convertDBRowsToJsonString(result) + "....");
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
 		
			
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
	private List<HoldDoubleValue<Long, Integer>> getAssignTaskByEntryData(String values) throws JSONException{
		JSONArray jsonArray = new JSONArray(values);
		if(jsonArray != null && jsonArray.length() > 0){
			List<HoldDoubleValue<Long, Integer>> returnList = new ArrayList<HoldDoubleValue<Long, Integer>>();
			for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
				JSONObject  json = jsonArray.getJSONObject(index);
				returnList.add(new HoldDoubleValue<Long, Integer>(
						StringUtil.getJsonLong(json, "detail_id"),
						StringUtil.getJsonInt(json, "isupdate")));
			}
			return returnList ;
		}
		return null ;
	}
	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	

}
