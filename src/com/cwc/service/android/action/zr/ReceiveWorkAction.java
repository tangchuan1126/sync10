package com.cwc.service.android.action.zr;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.AndroidPrintMgr;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.android.SumsangPalletNoNotFoundException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.zr.ReceiveWorkMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.DocLevelKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

public class ReceiveWorkAction extends ActionFatherController{

	private ReceiveWorkMgrIfaceZr receiveWorkMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	private AdminMgrIFaceZJ adminMgrZJ;
	private CheckInMgrIFaceZyj checkInMgrZyj ;
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		try{
			DBRow data = getJsonObjectMutiRequest(request);
				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
				throw new AppVersionException();
			}
			DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
			if(loginRow == null){
				throw new MachineUnderFindException();
			}
			//上传文件到文件服务器需要用到
			loginRow.add("SESSION_ID", data.getString("SESSION_ID"));
			//模拟登陆
			AdminLoginBean adminLoggerBean = AndroidPermissionUtil.androidSetSession(loginRow, request);

			long ps_id = loginRow.get("ps_id", 0l);
			long area_id = loginRow.get("AreaId", 0l);	
			String method = data.getString("Method");
			if(method.equalsIgnoreCase("EntryReceive")){
				long entry_id = data.get("entry_id", 0l);
				result = receiveWorkMgrZr.getSumSangCtnrDetails(entry_id, adminLoggerBean);
				if(result == null || result.get("account", 0) < 1){
					throw new NoRecordsException();
				}
				result.add("printServer", androidPrintMgrZr.getPrintServerByLoginUser(ps_id, area_id, AndroidPrintMgr.Label));
			}
			if(method.equalsIgnoreCase("SubmitSumSang")){
				DBRow receiveRows = getReciveRows(data);
				if(receiveRows != null){
					long ic_id = receiveRows.get("ic_id", 0l);
					long dlo_detail_id = receiveRows.get("dlo_detail_id", 0l);
					long entry_id = receiveRows.get("entry_id", 0l);
					String filePath = data.getString("filePath");
					DBRow[] pallets =(DBRow[]) receiveRows.get("pallets",new DBRow[0]);
					receiveWorkMgrZr.addReceiveInfo(ic_id, dlo_detail_id, entry_id, pallets, loginRow, filePath);
					//提交成功过后返回已经收货的数据
					DBRow[] datas = receiveWorkMgrZr.getReceivedSumSangDetails(dlo_detail_id, ic_id);
					fixDatas(datas);
					result.add("datas", datas);
					result.add("staging", receiveWorkMgrZr.getStatingByPsId(loginRow.get("ps_id", 0l)));
				}else{
					throw new DataFormatException();
				}
  			}
			if(method.equalsIgnoreCase("ReceivedSumSangDetail")){
				long dlo_detail_id = data.get("dlo_detail_id", 0l);
				long ic_id = data.get("ic_id", 0l);
				DBRow[] datas = receiveWorkMgrZr.getReceivedSumSangDetails(dlo_detail_id, ic_id);
				DBRow containerInfo = receiveWorkMgrZr.getContainerInfo(ic_id);
				if(containerInfo == null){
					throw new ContainerNotFoundException();
				}
				fixDatas(datas);
				result.add("datas", datas);
				result.add("staging", receiveWorkMgrZr.getStatingByPsId(loginRow.get("ps_id", 0l)));
				result.add("totalbox", containerInfo.get("qty", 0l));
				
			}
			if(method.equalsIgnoreCase("ReceivedFinish")){
				
				receiveWorkMgrZr.finishReceived(data, loginRow);
			}
		 
			// 三星收货的时候异常
			if(method.equalsIgnoreCase("ReceivedException")){
				receiveWorkMgrZr.receivedException(data, loginRow.get("adid", 0l));
			}
			
			//下面是三星装货的 操作 更新dlo_detail_id到表中
			if(method.equalsIgnoreCase("LoadPallet")){ //其实就是Update storage_load_unload_location表中的数据
				long icp_id = data.get("icp_id", 0l);
				long dlo_detail_id = data.get("dlo_detail_id", 0l);
				checkInMgrZyj.updateContainerPlateShipByIcpid(icp_id, dlo_detail_id,loginRow);
			}
			
			
			//根据 ，有dlo_detail_id  表示就是已经装货的列表
			if(method.equalsIgnoreCase("GetHasLoadPallet")){
 				long ic_id = data.get("ic_id", 0l);
 				long dlo_detail_id = data.get("dlo_detail_id", 0l);	
 				 
 				DBRow containerInfo = receiveWorkMgrZr.getContainerInfo(ic_id);
 				if(containerInfo == null){
 					throw new SystemException() ;
 				}
				DBRow[] datas = receiveWorkMgrZr.getReceivedSumSangDetails(dlo_detail_id, ic_id);
				result.add("datas", datas);
				result.add("totalbox", containerInfo.get("recevie_qty", 0l));
			}
			if(method.equalsIgnoreCase("ReLoadSumSang")){ //把SumSang的ship_entryd_id 更新到0l
				long ic_id = data.get("ic_id", 0l);
 				long dlo_detail_id = data.get("dlo_detail_id", 0l);	
 				receiveWorkMgrZr.reloadSumsang( ic_id ,  dlo_detail_id);
				DBRow[] datas = receiveWorkMgrZr.getReceivedSumSangDetails(dlo_detail_id, ic_id);
				result.add("datas", datas);
			}
			//三星装货完成
			if(method.equalsIgnoreCase("SumsangLoadFinish")){	//三星装货的时候提交的接口
				receiveWorkMgrZr.sumsangLoadFinish(data, loginRow);
			}
			//三星Load 的时候下面的提交提交的图片
			if(method.equalsIgnoreCase("SumsangLoadPhoto")){
				receiveWorkMgrZr.SumsangLoadPhoto(data, loginRow);
			}
			//三星装货的时候异常
			/*if(method.equalsIgnoreCase("LoadSumsangException")){
				receiveWorkMgrZr.sumsangLoadException(data, loginRow);
			}*/
			//move pallet
			if(method.equalsIgnoreCase("QueryPallet")){
				String palletNumber = data.getString("pallet");
				DBRow returnRow = receiveWorkMgrZr.getPalletInfo(palletNumber);
				result.add("data", returnRow);
			}
			//修改Pallet 的number 以及Pallet area
			if(method.equalsIgnoreCase("MovePallet")){
				DBRow[] updateRows = getMovePallet(data);
				if(updateRows != null && updateRows.length > 0){
					receiveWorkMgrZr.updatePallet(updateRows);
				}
			}
			if(method.equalsIgnoreCase("GetStaging")){
				result.add("staging", receiveWorkMgrZr.getStatingByPsId(loginRow.get("ps_id", 0l)));
			}
			if(method.equalsIgnoreCase("UpdatePallet")){
				int box_number = data.get("box_number", 0);
				long icp_id = data.get("icp_id", 0l);
				long area = data.get("area", 0l);
				double weight = data.get("weight", 0.0d);
				if(box_number <= 0 || area <= 0l){
					throw new SystemException() ;
				}
				DBRow updateRow = new DBRow();
				updateRow.add("box_number", box_number);
				updateRow.add("staging_area", area);
				updateRow.add("weight", weight);
				receiveWorkMgrZr.updatePallet(icp_id, updateRow);
 			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>2015/1/3 receive <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			if(method.equals("CommonReceiveRefresh")){  // 普通的收货的界面 , PalletTypes返回
				long lr_id = data.get("lr_id", 0l);
				result.add("datas", receiveWorkMgrZr.getCommonReceived(lr_id));
 				result.add("palletTypes", getPalletTypes());
 				result.add("orderSystem", receiveWorkMgrZr.getOrderSystemInfo(lr_id));
			}
			
			if(method.equals("CommonReceivePallet")){
  				
				int system_type = data.get("system_type", 0);
				int order_type = data.get("order_type", 0);
				long detail_id = data.get("detail_id", 0l);
				long lr_id = data.get("lr_id", 0l);
				String pallet_number =  data.getString("pallet_number");
				int count =  data.get("count",0);
				String pallet_type =  data.getString("pallet_type");
				int order_live =  data.get("order_level",0);
				long resources_id = data.get("resources_id", 0l);
				int resources_type = data.get("resources_type", 0);

				
				DBRow insertRow = new DBRow();
				insertRow.add("pallet_type", pallet_type);
				insertRow.add("pallet_type_count", count);
				insertRow.add("pallet_number", pallet_number);
				insertRow.add("order_type", order_type);
				insertRow.add("order_system", system_type);
				insertRow.add("dlo_detail_id", detail_id);
				insertRow.add("order_live", order_live);
				insertRow.add("lr_id", lr_id);
				insertRow.add("delivery_or_pick_up", CheckInMainDocumentsRelTypeKey.DELIVERY);
				
				insertRow.add("resources_type", resources_type);
				insertRow.add("resources_id", resources_id);
				insertRow.add("scan_adid", adminLoggerBean.getAdid());

				long receive_id = receiveWorkMgrZr.addCommonReceived(insertRow);
				if(receive_id <= 0l){
					throw new SystemException();
				}
				result.add("receive_pallet_id", receive_id);
			}
			/**
			 * 删除
			 */
			if(method.equalsIgnoreCase("CommonReceiveDelete")){
				long receive_pallet_id = data.get("receive_pallet_id", 0l);
				int count = receiveWorkMgrZr.commonReceiveDelete(receive_pallet_id);
				if(count <= 0){
					throw new SystemException();
				}
			}
			/*
			 * 修改Pallet type 
			 */
			if(method.equalsIgnoreCase("CommonReceiveModify")){
				int count = data.get("count", 0); 	//
				long receive_pallet_id = data.get("receive_pallet_id", 0l);
				String pallet_type = data.getString("pallet_type");
				DBRow updateRow = new DBRow();
				updateRow.add("pallet_type", pallet_type);
				updateRow.add("pallet_type_count", count);
				receiveWorkMgrZr.updateCommonReceive(receive_pallet_id, updateRow);
			}
			 
		}catch(DockCheckInFirstException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.DockCheckInFirstException;	
		}catch(ContainerNotFoundException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.ContainerNotFoundException;	
		}catch(SumsangPalletNoNotFoundException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.SumsangPalletNotFound;	
		}catch(CheckInNotFoundException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInNotFound;	
		}catch (NoPermiessionEntryIdException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoPermiessionEntryIdException;	
 		}catch (DataFormatException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.DataFormateException;
			}catch (LoadIsCloseException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LoadIsCloseException;
		}catch (NoRecordsException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoRecordsException;
			}catch (AppVersionException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.AppVersionException;
		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
	//	//system.out.println(StringUtil.convertDBRowsToJsonString(result) + "....");
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
 
	private DBRow[] getPalletTypes(){
		String[] palletTypeId = new String[]{"4840","5640","6247","6340","7245","7840","CHEP","FL","PLT1","PLT2","SP"} ;
		String[] palletTypeName = new String[]{"48X40","56x40","62x47","63x40","72x45","78x40","CHEP","FloorLoad","PalletType1","PalletType2","Slipsheet"};
		if(palletTypeId != null && palletTypeId.length > 0 
				&& palletTypeName != null && palletTypeName.length > 0
				&& palletTypeName.length == palletTypeId.length){
			DBRow[] returnArrays = new DBRow[palletTypeId.length];
			for(int index = 0 , count = palletTypeId.length ; index < count ; index++ ){
				DBRow temp = new DBRow();
				temp.add("pallettypeid", palletTypeId[index]);
				temp.add("pallettypename", palletTypeName[index]);
				returnArrays[index] = temp ;
			}
			return returnArrays ;
		}
		return new  DBRow[0];
	
	}
	
	private void fixDatas(DBRow[] datas){
		for(DBRow data : datas){
			data.add("pallet_number", data.getString("plate_no"));
			data.add("area", data.getString("staging_area"));
			data.remove("plate_no");
			data.remove("staging_area");
		}
	}
	/**
	 *  
	 * 	jsonInfo:[
	 * 		{
	 * 			icp_id:1212
	 * 			area:1212
	 *      }
	 *   ]
	 *   
	 * @param data
	 * @return
	 * @author zhangrui
	 * @throws JSONException 
	 * @Date   2014年9月25日
	 */
	private DBRow[] getMovePallet(DBRow data) throws JSONException , SystemException{
	/*	String jsonInfo = data.getString("jsonInfo");
		JSONArray jsonArray = new JSONArray(jsonInfo);
		if(jsonArray != null && jsonArray.length() > 0){
			DBRow[] returnArray = new DBRow[jsonArray.length()];
			for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
				DBRow temp = new DBRow();
				long areaId = StringUtil.getJsonLong(jsonArray.getJSONObject(index), "area") ;
				if(areaId <= 0l){
					throw new SystemException() ;
				}
				temp.add("staging_area", areaId);
				returnArray[index] = temp ;
			}
			return returnArray ;
		}
		return null ;*/
		
		String icp_ids = data.getString("icp_ids");
		long area  = data.get("area", 0l);
		if(icp_ids != null){
			String[] icpIdArray = icp_ids.split(",");
			if(icpIdArray != null && icpIdArray.length > 0){
				DBRow[] returnArray = new DBRow[icpIdArray.length];
				for(int index =  0 , count = icpIdArray.length ; index < count ; index++ ){
					DBRow temp = new DBRow();
					temp.add("staging_area", area);
					temp.add("icp_id", Long.parseLong(icpIdArray[index]));
					if(Long.parseLong(icpIdArray[index]) <= 0l || area <= 0l){
						throw new SystemException();
					}
					returnArray[index] = temp ;
				}
				return returnArray ;
			}
 		}
		return null ;
	}
	/**
	 * 得到提交的三星收货的数据
	 * @param datas
	 * @return
	 * @author zhangrui
	 * @Date   2014年9月13日
	 */
	private DBRow getReciveRows(DBRow data){
		String jsonInfo = data.getString("jsonInfo");
		  JSONObject jsonObject = StringUtil.getJsonObject(jsonInfo);
		  DBRow returnRow = null ;
		  if(jsonObject != null){
			  returnRow = new DBRow();
			  long dlo_detail_id = StringUtil.getJsonLong(jsonObject, "dlo_detail_id");
			  long ic_id = StringUtil.getJsonLong(jsonObject, "ic_id");
			  long entry_id = StringUtil.getJsonLong(jsonObject, "entry_id");
			  JSONArray jsonArray = StringUtil.getJsonArrayFromJson(jsonObject, "pallets");
			  if(jsonArray != null && jsonArray.length() > 0){
				  DBRow[] pallets = new DBRow[jsonArray.length()];
				  for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
					  JSONObject palletJson =	StringUtil.getJsonObjectFromArray(jsonArray, index);
					  if(palletJson != null){
						  DBRow row = new DBRow();
						  row.add("pallet_number", StringUtil.getJsonString(palletJson, "pallet_number"));
						  row.add("area", StringUtil.getJsonString(palletJson, "area"));
						  row.add("box_number", StringUtil.getJsonInt(palletJson, "box_number"));
						  row.add("weight", StringUtil.getJsonDouble(palletJson, "weight"));

						  
 						  pallets[index] = row ;
					  }
				  }
				  returnRow.add("pallets", pallets);
			  }
			  returnRow.add("ic_id", ic_id);
			  returnRow.add("dlo_detail_id", dlo_detail_id);
			  returnRow.add("entry_id", entry_id);
 		  }
		 return returnRow ;
			  
	}
	/**
	 * 不管是不是带有文件的inputStream 。都正确返回数据
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private DBRow getJsonObjectMutiRequest(HttpServletRequest request) throws Exception{
		DBRow returnRow = new DBRow();
		String sessionId=request.getRequestedSessionId();
		returnRow.add("SESSION_ID", sessionId);
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}

	public void setReceiveWorkMgrZr(ReceiveWorkMgrIfaceZr receiveWorkMgrZr) {
		this.receiveWorkMgrZr = receiveWorkMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}
	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
	
}
