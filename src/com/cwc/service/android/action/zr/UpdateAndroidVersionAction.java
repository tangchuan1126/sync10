package com.cwc.service.android.action.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 这个类用于android 在登录界面的时候去请求当前服务器版本,以及版本的更新的信息details { version:, details:[
 * 1.更新XXXXX, 2.修改XXXXX ] }
 * 
 * @author zhangrui
 */
public class UpdateAndroidVersionAction extends ActionFatherController {

	@SuppressWarnings("deprecation")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// 读取一个文件的xml。然后返回
 		DBRow returnDBRow = new DBRow();
		int ret = BCSKey.SUCCESS;
		int err = 0;
		try {
			String fileUploadPath = Environment.getHome().replace("\\", "/")+ "." + "/android/androidVerion.xml";
			DBRow data = paraXml(new FileInputStream(new File(fileUploadPath)));
			returnDBRow.add("version",data.getString("version"));
			returnDBRow.add("descripts",(DBRow[])data.get("descript",new DBRow[]{}));
			 
		} catch (Exception e) {
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
		}
		returnDBRow.add("ret", ret);
		returnDBRow.add("err", err);
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(returnDBRow));
	}
	
	private DBRow paraXml(InputStream inputStream) throws Exception{
		DBRow returnDBRow = new DBRow();
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
	        DocumentBuilder builder = factory.newDocumentBuilder();  
	        Document document = builder.parse(inputStream);  
	        Element element = document.getDocumentElement(); 
	        
	        NodeList versionNodes = element.getElementsByTagName("Value");
	        if(versionNodes != null && versionNodes.getLength() > 0 ){
	        	returnDBRow.add("version", versionNodes.item(0).getTextContent());
	        }
	        NodeList descriptNodes = element.getElementsByTagName("Descript");
	        if(descriptNodes != null && descriptNodes.getLength() > 0 ){
	        	DBRow[]  dess = new DBRow[descriptNodes.getLength()] ;
	        	for(int index = 0 , count = descriptNodes.getLength() ; index < count ; index++ ){
	        		DBRow temp  = new DBRow();
	        		temp.add("value", descriptNodes.item(index).getTextContent());
	        		dess[index] = temp ;
	        	}
	        	returnDBRow.add("descript", dess);
	        }
		}catch (Exception e) {
			throw new SystemException("Parse xml failed.");
 		}finally{
 			if(inputStream != null){
 				try {
 					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
 			}
 		}
		return returnDBRow ;
		
	}
}
