package com.cwc.service.android.action.zr;

 
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;

/**
 * 
 * @author wfh
 *
 */
public class AndroidSmallParcelAction extends ActionFatherController{


	private CheckInMgrIfaceWfh checkInMgrWfh ;
	private AdminMgrIFaceZJ adminMgrZJ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
				DBRow data = getRequestData(request);

 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
				//模拟登陆
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);
 				String method = data.getString("Method") ;
 				//得到所有的carrier
 				if(method.equals("getSmallParcelCarrier")){
 					DBRow[] datas = checkInMgrWfh.getSmallParcelCarrier();
 					result.add("data", datas);
 				}
 				//load all 所有的order
 				else if(method.equals("scanAllPalletOrTackNO")){
 					long detail_id = data.get("detail_id", 0L);
 					long adid = adminLoggerBean.getAdid();
 					DBRow datas = checkInMgrWfh.scanAllPalletOrTackNO(detail_id, adid);
 					result.add("data", datas);
 				}
 				//reload order 
 				else if(method.equals("reloadOrder")){
 					long detail_id = data.get("detail_id", 0L);
 					int is_keep_order = data.get("is_keep_order", 0);
 					DBRow datas = checkInMgrWfh.reloadOrder(detail_id, is_keep_order);
 					result.add("data", datas.get("data", new DBRow[0]));
 					result.add("is_order", datas.get("is_order", 0));
 				}
 				//filterSmallParcel
 				else if(method.equals("filterSmallParcel")){
// 					long customer_id = data.get("customer_id", 0L);
 					long carrier_id = data.get("carrier_id", 0L);
 					long detail_id = data.get("detail_id", 0L);
 					DBRow datas = checkInMgrWfh.filterSmallParcel(carrier_id, 0L,detail_id);
 					result.add("data", datas.get("data", new DBRow[0]));
 					result.add("is_order", datas.get("is_order", 0));
 				}
 				else if(method.equals("scanPalletOrTrackNo")){
 					long adid = adminLoggerBean.getAdid();
 					String scan_numbers = data.get("scan_numer", "");
 					long detail_id = data.get("detail_id", 0L);
  					DBRow datas = checkInMgrWfh.scanPalletOrTrackNo(scan_numbers, detail_id, adid);
//  					if(datas.get("status", 0)==-1){
//  						ret = BCSKey.FAIL;
//  						err = 90;
//  						datas = new DBRow();
//  						datas.add("data", "");
//  					}
 					result.add("data", datas);
 				}
 				//去除扫描状态
 				else if(method.equals("removeScanState")){
 					int wms_scan_number_type = data.get("wms_scan_number_type", 0);
 					long wms_order_type_id = data.get("wms_order_type_id", 0L);
 					long detail_id = data.get("detail_id", 0L);
 					long order_id = data.get("wms_order_id", 0L);
 					DBRow datas = checkInMgrWfh.removeScanState(wms_order_type_id, detail_id, wms_scan_number_type);
 					result.add("data", datas);
 				}
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
		 //	System.out.println(StringUtil.convertDBRowsToJsonString(result) + "....");
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
 		
			
	}
	
	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow ;
	}
	
	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	

}
