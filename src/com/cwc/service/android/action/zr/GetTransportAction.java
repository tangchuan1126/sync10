
package com.cwc.service.android.action.zr;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetTransportAction extends ActionFatherController {
	static Logger logBcs = Logger.getLogger("BCS");
	
	private AdminMgrIFaceZJ adminMgrZJ;
	 
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	private AndroidMgrIfaceZr androidMgrZr;
	 
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}




	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
			//transport_id 
			//fromWarehouse
			//toWarehouse
			//area 
			//state 
			InputStream inputStream = request.getInputStream();
			int ret = BCSKey.FAIL;//默认成功
			int err = BCSKey.DATASIZEINCORRECTLY;
			String fc = "";
			String machine = "";
			String password  = "";
			String xml = "";
			String loginAccount = "";
			StringBuffer returnDetails = new StringBuffer();
			int postcount = 0;
		 
			try{
				xml = getPost(inputStream);
				logBcs.info("request"+xml);
				fc = StringUtil.getSampleNode(xml,"fc");
				machine = StringUtil.getSampleNode(xml,"Machine");
				loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
				password = StringUtil.getSampleNode(xml,"Password");
				DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
				if(login == null){
					throw new MachineUnderFindException();
				}
				if(fc.equals("CheckInTransports")){
					 
					ArrayList<DBRow> list = new ArrayList<DBRow>();
					DBRow[] data = androidMgrZr.getCheckInTransport(login.get("ps_id", 0l));
					if(data!=null && data.length > 0){
						for(DBRow row : data){
							DBRow temp = new DBRow();
							temp.add("transport_id", row.get("transport_id", 0l));
							temp.add("fromwarehouse", row.getString("fromwarehouse"));
							temp.add("towarehouse", row.getString("towarehouse"));
							//查询装运单 所在门 和区域 
							DBRow[] door = androidMgrZr.getDoorNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.DELEIVER );
							DBRow[] location = androidMgrZr.getLocationNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.DELEIVER);
							//door,door - location,location 
							String doorStr = "" ;
							String locationStr = "";
							if(door != null){
								for(DBRow tempDoor : door){
									doorStr += ","+tempDoor.getString("doorId");
								}
							}
							if(location != null){
								for(DBRow tempLocation : location){
									locationStr+= "," + tempLocation.getString("location_name");
								}
							}
							temp.add("area", (doorStr.length() > 0 ? doorStr.substring(1) : "") + "|"+(locationStr.length() > 0 ? locationStr.substring(1) : ""));	// 在那个门卸货门
							temp.add("state", "CHECKIN");
							temp.add("title_id", row.get("title_id", 0l));
							temp.add("title_name", row.getString("title_name"));
							temp.add("purchase_id",row.get("purchase_id", 0l));
							list.add(temp);
						}
						returnDetails = getCheckInTransportDetails(list.toArray(new DBRow[list.size()]));
						 
					}
					
					ret = BCSKey.SUCCESS;
				}
				// Transport checkInOver 
				if(fc.equals("TransportCheckInOver")){
					// 改transportId 收货完成。
					long transportId = Long.parseLong(StringUtil.getSampleNode(xml,"TransportId"));
 					ret = BCSKey.SUCCESS;
					postcount = 1;
				}
				//outbound 
				if(fc.equals("OutBoundList")){
					ArrayList<DBRow> list = new ArrayList<DBRow>();
					DBRow[] data = androidMgrZr.getOutboundTransport(login.get("ps_id", 0l));
					if(data!=null && data.length > 0){
						for(DBRow row : data){
							DBRow temp = new DBRow();
							temp.add("transport_id", row.get("transport_id", 0l));
							temp.add("fromwarehouse", row.getString("fromwarehouse"));
							temp.add("towarehouse", row.getString("towarehouse"));
							temp.add("area", row.getString("areaname"));
							temp.add("state", "OUTBOUND");
							temp.add("reserve_type",row.get("reserve_type",0));
							
							DBRow[] door = androidMgrZr.getDoorNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.SEND );
							DBRow[] location = androidMgrZr.getLocationNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.SEND);
							//door,door - location,location 
							String doorStr = "" ;
							String locationStr = "";
							if(door != null){
								for(DBRow tempDoor : door){
									doorStr += ","+tempDoor.getString("doorId");
								}
							}
							if(location != null){
								for(DBRow tempLocation : location){
									locationStr+= "," + tempLocation.getString("location_name");
								}
							}
							temp.add("area", (doorStr.length() > 0 ? doorStr.substring(1) : "") + "|"+(locationStr.length() > 0 ? locationStr.substring(1) : ""));	// 在那个门装货货门

							list.add(temp);
						}
						returnDetails = getCheckInTransportDetails(list.toArray(new DBRow[list.size()]));
					}
					ret = BCSKey.SUCCESS;
					postcount = data.length;
				}
				// 收货的时候下载的transport_outbound上面的数据
				if(fc.equals("TransportOutbound")){
					long transportId = Long.parseLong(StringUtil.getSampleNode(xml,"Transport"));
					DBRow[] data = androidMgrZr.queryTransportOutBoundByTransportId(transportId);
					if(data!=null && data.length > 0){
						returnDetails = getTransportOutbound(data);
					}
					ret = BCSKey.SUCCESS;
					postcount = data == null ? 0 : data.length;
				}
				if(fc.equals("TransportPositionBaseInfo")){
					long transportId = Long.parseLong(StringUtil.getSampleNode(xml,"Transport"));
					DBRow[] data = androidMgrZr.getTransportWarehouseProduct(transportId);
					if(data!=null && data.length > 0){
						returnDetails = getTransportWareHouse(data);
					}
					ret = BCSKey.SUCCESS;
					postcount = data == null ? 0 : data.length;
				}
				//发货的时候下载的transport_detail 上面的数据
				if(fc.equals("TransportDetail")){
					long transportId = Long.parseLong(StringUtil.getSampleNode(xml,"Transport"));
					DBRow[] data = androidMgrZr.getTransportDetail(transportId);
 					if(data!=null && data.length > 0){
						returnDetails = getTransportDetail(data);
					}
					ret = BCSKey.SUCCESS;
					postcount = data == null ? 0 : data.length;
				}
				if(fc.equals("PositionReceiveTransportList")){ //收货放货时候查询的transport
					ArrayList<DBRow> list = new ArrayList<DBRow>();
					DBRow[] data = androidMgrZr.getTransportReceived(login.get("ps_id", 0l));
					if(data!=null && data.length > 0){
						for(DBRow row : data){
							DBRow temp = new DBRow();
							temp.add("transport_id", row.get("transport_id", 0l));
							temp.add("fromwarehouse", row.getString("fromwarehouse"));
							temp.add("towarehouse", row.getString("towarehouse"));
							temp.add("area", "");	 	
							temp.add("state", "RECEIVED");
							temp.add("title_id", row.get("title_id", 0l));
							temp.add("title_name", row.getString("title_name"));
							temp.add("purchase_id",row.get("purchase_id", 0l));
							 
							list.add(temp);
						}
						returnDetails = getCheckInTransportDetails(list.toArray(new DBRow[list.size()]));
					}
					
					ret = BCSKey.SUCCESS;
					
				}
				if(fc.equals("GetTransportByMachine")){
					//应该是这个仓库下面的
					ArrayList<DBRow> list = new ArrayList<DBRow>();
					DBRow[] data = androidMgrZr.getTransportByMachine(login.get("ps_id", 0l));
					if(data!=null && data.length > 0){
						for(DBRow row : data){
							DBRow temp = new DBRow();
							temp.add("transport_id", row.get("transport_id", 0l));
							temp.add("fromwarehouse", row.getString("fromwarehouse"));
							temp.add("towarehouse", row.getString("towarehouse"));
							temp.add("area", "");	 	
							temp.add("state", "RECEIVED");
							
							list.add(temp);
						}
						returnDetails = getCheckInTransportDetails(list.toArray(new DBRow[list.size()]));
					}
					
					ret = BCSKey.SUCCESS;
				}
			}catch(MachineUnderFindException e){
				ret = BCSKey.FAIL;
				err = BCSKey.LOGINERROR;	//登录失败，账号密码错误
			}finally{
				responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
			}
	}
	private StringBuffer getTransportDetail(DBRow[] rows){
		StringBuffer details = new StringBuffer("");
		if(rows != null && rows.length > 0){
			for(DBRow temp : rows ){
				if(temp != null){
					details.append("<Details>");
					details.append("<TransportId>").append(temp.get("transport_id", 0l)).append("</TransportId>");
					details.append("<Qty>").append(temp.get("transport_count", 0.0)).append("</Qty>");
					details.append("<ToPName>").append(temp.getString("transport_p_name")).append("</ToPName>");
					details.append("<ToPCode>").append(temp.getString("transport_p_code")).append("</ToPCode>");
					details.append("<Sn>").append(temp.getString("transport_product_serial_number")).append("</Sn>");
					details.append("<Sku>").append(temp.get("transport_pc_id", 0l)).append("</Sku>");
					details.append("<ToId>").append(temp.get("transport_detail_id", 0l)).append("</ToId>");
					details.append("</Details>");
				}
			}
		}
		return details ;
	}
	
	private StringBuffer getTransportOutbound(DBRow[] rows){
		StringBuffer details = new StringBuffer("");
		if(rows != null && rows.length > 0){
			for(DBRow temp : rows ){
				if(temp != null){
					details.append("<Details>");
					details.append("<TransportId>").append(temp.get("to_transport_id", 0l)).append("</TransportId>");
					details.append("<Qty>").append(temp.get("to_count", 0.0)).append("</Qty>");
					details.append("<ToPName>").append(temp.getString("to_p_name")).append("</ToPName>");
					details.append("<ToPCode>").append(temp.getString("to_p_code")).append("</ToPCode>");
					details.append("<Sn>").append(temp.getString("to_serial_number")).append("</Sn>");
					details.append("<Sku>").append(temp.get("to_pc_id", 0l)).append("</Sku>");
					details.append("<ToId>").append(temp.get("to_id", 0l)).append("</ToId>");
					details.append("<LotNumber>").append(temp.getString("to_lot_number")).append("</LotNumber>");

					details.append("</Details>");
				}
			}
		}
		return details ;
	}
	private StringBuffer getTransportWareHouse(DBRow[] rows){
		StringBuffer details = new StringBuffer("");
		if(rows != null && rows.length > 0){
			for(DBRow temp : rows ){
				if(temp != null){
					details.append("<Details>");
					details.append("<TransportId>").append(temp.get("tw_transport_id", 0l)).append("</TransportId>");
					details.append("<Qty>").append(temp.get("tw_count", 0.0)).append("</Qty>");
					details.append("<ToPName>").append(temp.getString("tw_product_name")).append("</ToPName>");
					details.append("<ToPCode>").append(temp.getString("tw_product_barcode")).append("</ToPCode>");
					details.append("<Sn>").append(temp.getString("tw_serial_number")).append("</Sn>");
					details.append("<Sku>").append(temp.get("tw_product_id", 0l)).append("</Sku>");
					details.append("<ToId>").append(temp.get("tw_id", 0l)).append("</ToId>");
					details.append("<LotNumber>").append(temp.getString("tw_lot_number")).append("</LotNumber>");
					details.append("</Details>");
				}
			}
		}
		return details ;
		
	}
	
//	private StringBuffer getCheckInTransportDetailsAddDoors(DBRow[] rows){
//		StringBuffer details = new StringBuffer("");
//		if(rows != null && rows.length > 0){
//			for(DBRow temp : rows ){
//				if(temp != null){
//					details.append("<Details>");
//					details.append("<TransportId>").append(temp.get("rel_id", 0l)).append("</TransportId>");
//					details.append("<RelType>").append(temp.getString("occupancy_type")).append("</RelType>");
//					details.append("<StartTime>").append(temp.getString("book_start_time")).append("</StartTime>");
//					details.append("<EndTime>").append(temp.getString("book_end_time")).append("</EndTime>");
//					details.append("</Details>");
//				}
//			}
//		}
//		return details ;
//	}
	
	private StringBuffer getCheckInTransportDetails(DBRow[] rows){
		StringBuffer details = new StringBuffer("");
		if(rows != null && rows.length > 0){
			for(DBRow temp : rows ){
				if(temp != null){
					details.append("<Details>");
					details.append("<TransportId>").append(temp.get("transport_id", 0l)).append("</TransportId>");
 					if(temp.getString("purchase_id").equals(0)){
						details.append("<Fromwarehouse>").append(temp.getString("fromwarehouse")).append("</Fromwarehouse>");
					}else{
						long purchase_id=temp.get("purchase_id", 0l);
						DBRow row = null;
						try {
							row = this.androidMgrZr.getSupplier(purchase_id);
							if(row == null){row = new DBRow();}
							details.append("<Fromwarehouse>").append(row.getString("sup_name")).append("</Fromwarehouse>");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					details.append("<Towarehouse>").append(temp.getString("towarehouse")).append("</Towarehouse>");
					details.append("<Area>").append(temp.getString("area")).append("</Area>");
					details.append("<State>").append(temp.getString("state")).append("</State>");	
					details.append("<Reserve>").append(temp.get("reserve_type",0)).append("</Reserve>");	
					details.append("<TitleId>").append(temp.get("title_id", 0l)).append("</TitleId>");
					details.append("<TitleName>").append(temp.getString("title_name")).append("</TitleName>");
					
					details.append("</Details>");
				}
			}
		}
		return details ;
	}
	
	
	
	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try {
			     input.close();
			    } catch (Exception f) {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData));
	}
	
	
	
	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)throws Exception{
		response.setContentType("text/html; charset=UTF-8");		
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		boxQtyXml.append("<err>"+err+"</err>");
		boxQtyXml.append("<Detail>");
		if(details.length() > 1 )
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		logBcs.info("response:"+boxQtyXml.toString());
	 	try {
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
	 
		   response.setContentLength(boxQtyXml.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   String value = new  String(boxQtyXml.toString().getBytes("UTF-8"),"UTF-8");
	 
		   out.write(value.getBytes());
		   out.flush();
		   out.close();
		   
		}catch (Exception e){
		   e.printStackTrace();
		} 
	}

}
