package com.cwc.service.android.action.zr;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 



import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 这个类以后要删除一些东西
 * @author zhangrui
 *
 */
public class GetStreamSerialProductAction   extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	
	private static String preXmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data>";
	private static String aftXmlString = "</data>";
	
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	private static int buffered_size = 1024 * 50 ;
	
	 /**
	  * 	machine : 表示机器的型号
	  * 	transport_id : 表示的是transport_id
	  */
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			InputStream inputRequestStream = request.getInputStream();
			int ret = BCSKey.FAIL;//默认成功
			int err = BCSKey.DATASIZEINCORRECTLY;
			String fc = "";
			String machine = "";
			String password  = "";
			String xml = "";
			String loginAccount = "";
			StringBuffer returnDetails = new StringBuffer();
			int postcount = 0;
			 
			xml = getPost(inputRequestStream);
			fc = StringUtil.getSampleNode(xml,"fc");
			machine = StringUtil.getSampleNode(xml,"Machine");
			loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			password = StringUtil.getSampleNode(xml,"Password");
			
		  
			long transport_id = Long.parseLong(StringUtil.getSampleNode(xml,"Transport")) ;
			
			createFile(machine,transport_id , fc);
	  
			byte[] buffer = new byte[1024 * 1024];
			int length = -1 ;
			FileInputStream  inputStream = new FileInputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+".zip"));
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode(machine+".zip","utf-8"));  

			
			OutputStream outputStream =  response.getOutputStream();
			while((length = inputStream.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close(); 
	}
	
	/**
	 * 生成product.xml
	 * @param transport_id
	 * <Details>
			<Name>WEDDINGDRESS/W92/WHITE/US12</Name>
			<PID>176098</PID>
			<Length>430.0</Length>
			<Width>380.0</Width>
			<Heigth>120.0</Heigth>
			<Weight>2.0</Weight>
		</Details>
	 */
	private String createProductXml(long transport_id, String fc) {
		StringBuffer productXml = new StringBuffer(preXmlString);
		try{
			DBRow[] result = null ;
			if(fc.equals("OutBoundBaseInfo")){
				result = androidMgrZr.getOutboundProductInfo(transport_id);
			}
			if(fc.equals("InboundBaseInfo")){
				result = androidMgrZr.getProductForTransport(transport_id);
			}
			if(result != null && result.length > 0){
				for(DBRow  temp : result){
					productXml.append(createProductXmlItem(temp));
				}
			}
		}catch (Exception e) {
		}
		productXml.append(aftXmlString);
		return productXml.toString();
 
	}
	private String createProductXmlItem(DBRow row ){
		StringBuffer sb = new StringBuffer("");
		sb.append("<Details>");
		sb.append("<Name>").append(row.getString("p_name")).append("</Name>");
		sb.append("<PID>").append(row.get("pc_id", 0l)).append("</PID>");
		sb.append("<Length>").append(row.get("length", 0.0f)).append("</Length>");
		sb.append("<Width>").append(row.get("width", 0.0f)).append("</Width>");
		sb.append("<Heigth>").append(row.get("heigth", 0.0f)).append("</Heigth>");
		sb.append("<Weight>").append(row.get("weight", 0.0f)).append("</Weight>");
		sb.append("</Details>");
		return sb.toString();
	}
	
	/**
	 * 生成productCode.xml
	 * <Details>
			<PCID>10000</PCID>
			<Barcode>B/H1/43K</Barcode>
			<Show>1</Show>
		</Details>
		一个PCID 要对应创建一个Barcode的PCID
	 * @param transport_id
	 */
	private String createProductCodeXml(long transport_id , String fc) {
		StringBuffer productCodeXml = new StringBuffer(preXmlString);
		try{
			
			DBRow[] result = null ;
			if(fc.equals("OutBoundBaseInfo")){
				result = androidMgrZr.getProductCodeForOutboundTransport(transport_id);
			}
			if(fc.equals("InboundBaseInfo")){
				result = androidMgrZr.getProductCodeForTransport(transport_id);
			}
 			if(result != null && result.length > 0){
				Set<Long> pcids = new HashSet<Long>();
				String pName = "" ;
				for(DBRow row : result){
					pcids.add(row.get("pc_id", 0l));
					if(pName.length() < 1){
						pName = row.getString("p_name");
					}
					 
					productCodeXml.append(createProductCodeXmlItem(row));
				}
				//处理一个PCID 要对应创建一个Barcode的PCID情况
				if(pcids.size() > 0 ){
					Iterator<Long> it = pcids.iterator();
					while(it.hasNext()){
						long pc_id = it.next();
						if(pc_id != 0l){
							DBRow row = new DBRow();
							row.add("pc_id", pc_id);
							row.add("p_name", pName);
							row.add("p_code", pc_id);
							row.add("code_type", 0); //表示BarCode 就是PCID
							productCodeXml.append(createProductCodeXmlItem(row));
						}
					}
				}
			}
		}catch (Exception e) {
 		}
		productCodeXml.append(aftXmlString);
 		return productCodeXml.toString();
	}
	private String createProductCodeXmlItem(DBRow row) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<Details>");
		sb.append("<PCID>").append(row.get("pc_id", 0l)).append("</PCID>");
		sb.append("<Barcode>").append(row.getString("p_code")).append("</Barcode>");
		sb.append("<CodeType>").append(row.get("code_type",0)).append("</CodeType>");
		sb.append("<PName>").append(row.getString("p_name")).append("</PName>");
		sb.append("</Details>");
		return sb.toString();
	}
	
	
	/**
	 * 生成sku-sn对应的serialProduct.xml
	 * <sp  product_serial_id="10002" sn="164895" sku="3232323" supplierId="232323" inTime="" outTime=""></sp>
	 * @param transport_id
	 */
	public String createSerialProductXml(long transport_id,String fc) {
		StringBuffer serialProductXml = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?><sps>");
		try{
			DBRow[] result = null ;
			if(fc.equals("OutBoundBaseInfo")){
				result = androidMgrZr.getSerialProductForOutboundTransport(transport_id);
			}
			if(fc.equals("InboundBaseInfo")){
				result = androidMgrZr.getSerialProductForTransport(transport_id);
			}
 			for(DBRow row : result){
				serialProductXml.append(createSerialProductItem(row));
			}
		}catch (Exception e) {
 		}
		serialProductXml.append("</sps>");
		return serialProductXml.toString();
	}
	
	public String createSerialProductItem(DBRow row){
		StringBuffer sb = new StringBuffer("");
		sb.append("<sp").append(" product_serial_id=\"").append(row.get("product_serial_id", 0l) + "\"");
		sb.append(" sn=\"").append(row.getString("serial_number") + "\"");
		sb.append(" sku=\"").append(row.get("pc_id", 0l) + "\"");
		sb.append(" supplierId=\"").append(row.get("supplier_id", 0l) + "\"");
		sb.append(" inTime=\"").append(row.getString("in_time") + "\"");
		sb.append(" outTime=\"").append(row.getString("out_time") + "\">");
		sb.append("</sp>");
		return sb.toString();
	}
	/**
	 * 生成zip文件
	 * 	1.首先是生成.xml文件
	 * 	2.然后把.xml文件压缩成.zip文件
	 * @param machine
	 */
	private void createFile(String machine , long transport_id , String fc ) throws Exception {
		String productXml =	createProductXml(transport_id , fc );
		String productCodeXml =	createProductCodeXml(transport_id , fc );
		String serialProductXml = createSerialProductXml(transport_id , fc);
		 
		ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+".zip")));	 
 		
		ZipEntry entry = new ZipEntry("serialProduct.xml");
 		zipOutputStream.putNextEntry(entry);
 		zipOutputStream.write(serialProductXml.getBytes());
 		
 		ZipEntry productCodeXmlEntry = new ZipEntry("productCode.xml");
 		zipOutputStream.putNextEntry(productCodeXmlEntry);
 		zipOutputStream.write(productCodeXml.getBytes());
 		
 		ZipEntry productXmlEntry = new ZipEntry("product.xml");
 		zipOutputStream.putNextEntry(productXmlEntry);
 		zipOutputStream.write(productXml.getBytes());
 	 
 		zipOutputStream.flush();
 		zipOutputStream.close();
	}
	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try {
			     input.close();
			    } catch (Exception f) {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData));
	}

}
