package com.cwc.service.android.action.zr;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.CheckTaskProcessingChangeTaskToDockException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.EntryTaskHasFinishException;
import com.cwc.app.exception.android.LoadIsCloseException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.exception.checkin.SmallParcelNotCloseOrderException;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.LoadBarUseMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

/**
 * Assign Task 代替原来的AndroidDockCheckIn
 * @author win7zr
 *
 */
public class AndroidTaskProcessingAction extends ActionFatherController{


	private CheckInMgrIfaceZr checkInMgrZr ;
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	private CheckInMgrIfaceWfh checkInMgrWfh ;
	private AdminMgrIFaceZJ adminMgrZJ;
	private LoadBarUseMgrIfaceZr loadBarUseMgrZr ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
 				DBRow data = getRequestData(request);
 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
			 
				AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);

				
 				String method = data.getString("Method") ;
 				
 				
 				if(method.equalsIgnoreCase("TaskProcessingListByEntry")){
 					long entry_id = data.get("entry_id", 0l);
 					DBRow[] datas =checkInMgrZr.taskProcessingListByEntry(entry_id,adminLoggerBean);
 					DBRow[] equipments = checkInMgrZr.entryEquipmentWithResouces(entry_id);
 					result.add("data", datas);
 					result.add("equipments", equipments);
 					if((datas == null || datas.length < 1) && (equipments == null || equipments.length < 1)){
 						throw new NoRecordsException() ;
 					}
 				}
 				if(method.equalsIgnoreCase("TaskProcessingDetailByEntryEquipmentAndResouces")){
 					long entry_id = data.get("entry_id", 0l);
 					long equipment_id = data.get("equipment_id", 0l);
 					long resources_id = data.get("resources_id", 0l);
 					int resources_type = data.get("resources_type", 0);
 					appendTasks(result, entry_id, equipment_id, resources_id, resources_type, adminLoggerBean);
 				}
 				/**
 				 * 选择几个Detail 开始 
 				 */
 				if(method.equalsIgnoreCase("TaskProcessingStartDetails")){
 					long equipment_id = data.get("equipment_id", 0l);
 					long entry_id = data.get("entry_id", 0l);
 					long resources_id = data.get("resources_id", 0l);
 					int resources_type = data.get("resources_type", 0);
 				/*	if(resources_type == OccupyTypeKey.SPOT){
 						throw  new CheckTaskProcessingChangeTaskToDockException();
 					}*/
 					List<HoldDoubleValue<Long, String>> details =	getTaskProcessingStartValues(data.getString("values"));
 					details = this.checkInMgrWfh.findTaskIsStart(details);
 					if(details.size()>0){
 						checkInMgrZr.taskProcessingStartDetails(equipment_id, entry_id, details, resources_id, adminLoggerBean.getAdid(),resources_type);
 					}
 					appendTasks(result, entry_id, equipment_id, resources_id, resources_type, adminLoggerBean);
 				}
 				//关闭某个单据
 				if(method.equalsIgnoreCase("TaskProcessingCloseDetail")){
 					 long entry_id = data.get("entry_id", 0l);
 					 long equipment_id = data.get("equipment_id", 0l);
 					 long resources_id = data.get("resources_id", 0l);
 					 int resources_type = data.get("resources_type", 0);
  					 String request_type = data.getString("request_type");
 					 int returnFlag = checkInMgrZr.taskProcessingCloseDetail(data, adminLoggerBean);
 					 if(returnFlag == CheckInMgrZwb.LoadCloseNotifyInputSeal){
 						 returnFlag = CheckInMgrZwb.LoadCloseNotifyStayOrLeave ;
 					 }
 					 result.add("returnFlag", returnFlag);
 					 if(!request_type.equalsIgnoreCase("notaskinfos")){
 						 appendTasks(result, entry_id, equipment_id, resources_id, resources_type, adminLoggerBean);
 					 }
   				}
 				if(method.equalsIgnoreCase("TaskProcessingMoveToDoor")){
 					long entry_id = data.get("entry_id", 0l);
 					long equipment_id = data.get("equipment_id", 0l);
 					long sd_id = data.get("sd_id", 0l); //移动到那个门
 					int moved_resource_type = data.get("moved_resource_type", 0); //移动的资源类型
 					long move_resouce_id = data.get("moved_resource_id", 0l);	 //移动的资源Id
 					checkInMgrZr.taskProcessingMoveTaskToDoor(entry_id, equipment_id, sd_id, moved_resource_type, move_resouce_id, adminLoggerBean);
 					data.add("resources_type", OccupyTypeKey.DOOR);
 					data.add("resources_id", sd_id);
 				}
 				/**
 				 * 1.提交数据
 				 * 2.realse 设备的资源 + realse Task 的占用情况
 				 */
 				if(method.equalsIgnoreCase("TaskProcessingFinishSubmit")){
 					//处理DockClose的图片
  					int returnFlag = checkInMgrZr.taskProcessingFinishSubmit(data, adminLoggerBean); 
  					result.add("returnflag", returnFlag);
 				}
 				if(method.equalsIgnoreCase("AddLoadBarUse")){
 					long equipment_id = data.get("equipment_id", 0l);
 					long entry_id = data.get("entry_id", 0l);
 					if(data.get("isUpdate", 0) == 1){
 						loadBarUseMgrZr.androidUpdateLoadBarUse(data, adminLoggerBean);
 					}else{
 	 					checkInMgrZr.addLoadBarUse(data, adminLoggerBean);
 					}
 					DBRow[] datas = loadBarUseMgrZr.androidGetLoadBarUse(equipment_id, entry_id);
 					result.add("load_use", datas);
 					//loadBarUseMgrZr.addLoadBarUse(inserRow);
 				}
 				if(method.equalsIgnoreCase("UpDateSeal")){
 					checkInMgrZr.updateEquipmentSeal(data, adminLoggerBean);
 				}
 				//把当前的任务分配给自己 || 或者是把当前的任务执行人换成自己
 				if(method.equalsIgnoreCase("TaskProcessingTakeOver")){
 					long entry_id = data.get("entry_id", 0l);
 					long equipment_id = data.get("equipment_id", 0l);
 					long resources_id = data.get("resources_id", 0l);
 					int resources_type = data.get("resources_type", 0);
 					//detail_id
 					checkInMgrZr.taskProcessingTakeOver(data, adminLoggerBean);
 					appendTasks(result, entry_id, equipment_id, resources_id, resources_type, adminLoggerBean);
 				}
 				/**
 				 * ReleaseDoor
 				 */
 				if(method.equalsIgnoreCase("TaskProcessingReleaseDoor")){
 					checkInMgrZr.taskProcessingReleaseDoor(data, adminLoggerBean);
 				}
 				/**
 				 * StayOrLeave
 				 */
 				if(method.equalsIgnoreCase("TaskProcessingStayOrLeave")){
 					checkInMgrZr.taskProcessingEquipmentLeavingInyard(data, adminLoggerBean);
 				}
 				//LoadReceive 流程 查询IncomingToWindow,Load/Receive ,Waiting Assing Labor,Remain Jobs,Under Processing,Closed Today
 				if(method.equals("LoadReceiveProcess")){
 					DBRow datas = checkInMgrZr.getLoadReceiveProcess(adminLoggerBean.getPs_id());
 					result.add("data", datas);
 				}
 				
 				if(method.equals("TaskProcessingList")){
 					DBRow[] datas = checkInMgrZr.getTaskProcessingByAdid(adminLoggerBean.getAdid());
 					result.add("data", datas);
 				}
 				
 			}catch(SmallParcelNotCloseOrderException e){
 				ret = BCSKey.FAIL ;
				err = 90;
				result.add("data", e.getMessage());
 			}catch(CheckInEntryIsLeftException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInEntryIsLeftException;
 			}catch(EntryTaskHasFinishException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.EntryTaskHasFinishException;
 			}catch(EquipmentHadOutException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentHadOutException;
 			}catch(AddLoadBarUseFoundException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.AddLoadBarUseFoundException;
 			}catch(EquipmentNotFindException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentNotFindException;
 			}catch(CheckTaskProcessingChangeTaskToDockException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckTaskProcessingChangeTaskToDockException;
 			}catch(CheckInNotFoundException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound ;
 			}catch(DockCheckInFirstException e){
				ret = BCSKey.FAIL ;
				err = BCSKey.DockCheckInFirstException ;
			}catch (NoPermiessionEntryIdException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;	
	 		}catch (DataFormatException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.DataFormateException;
 			}catch (LoadIsCloseException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LoadIsCloseException;
			}catch (NoRecordsException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.NoRecordsException;
 			}catch (AppVersionException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.AppVersionException;
			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
			}catch (Exception e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
			}
			result.add("ret", ret);
			result.add("err", err);
			
		// //system.out.println(StringUtil.convertDBRowsToJsonString(result) + "...");
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
			
	}
	/**
	 * append Tasks
	 * @param result
	 * @param entry_id
	 * @param equipment_id
	 * @param resources_id
	 * @param resources_type
	 * @param adminLoggerBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月10日
	 */
	private void appendTasks(DBRow result, long entry_id , long equipment_id , long  resources_id, int resources_type , AdminLoginBean adminLoggerBean) throws Exception{
			DBRow[] datas =checkInMgrZr.getTaskProcessingDetailByEntryAndEquipmentAndResouces(entry_id, equipment_id, resources_type, resources_id, adminLoggerBean);
			result.add("datas", datas);
 			DBRow[] loadBarUse = loadBarUseMgrZr.androidGetLoadBarUse(equipment_id, entry_id) ;
			result.add("load_use", loadBarUse == null || loadBarUse.length < 1 ? new DBRow[]{} : loadBarUse );
			result.add("load_bars",checkInMgrZwb.selectLoadBar());
			result.add("equiment", checkInMgrZr.getEquipmentSeals(equipment_id));
			result.add("showfinish", checkInMgrZr.isEquipmentHasResouces(resources_id, resources_type, equipment_id) ? "1":"0");

	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	private List<HoldDoubleValue<Long,String>> getTaskProcessingStartValues(String value) throws Exception{
		JSONArray jsonArray =  new JSONArray(value);
		List<HoldDoubleValue<Long, String>> returnList = new ArrayList<HoldDoubleValue<Long,String>>();
		for(int index = 0 , count = jsonArray.length() ; index < count ; index++ ){
			 JSONObject  json = jsonArray.getJSONObject(index);
			 returnList.add(new HoldDoubleValue<Long, String>
			 	(StringUtil.getJsonLong(json, "dlo_detail_id"),
			 	 StringUtil.getJsonString(json, "number")));
			
		}
		return returnList ;
	}
	public DBRow getRequestData(HttpServletRequest request)  throws Exception{
		DBRow returnRow = new DBRow();
		String sessionId=request.getRequestedSessionId();
		returnRow.add("SESSION_ID", sessionId);
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.add("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setLoadBarUseMgrZr(LoadBarUseMgrIfaceZr loadBarUseMgrZr) {
		this.loadBarUseMgrZr = loadBarUseMgrZr;
	}
	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
}
