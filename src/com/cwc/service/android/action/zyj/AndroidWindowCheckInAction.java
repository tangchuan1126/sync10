package com.cwc.service.android.action.zyj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustHasTasksException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.CheckinTaskNotFoundException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.DoorNotFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentHasDecidedPickedException;
import com.cwc.app.exception.checkin.EquipmentHasLoadedOrdersException;
import com.cwc.app.exception.checkin.EquipmentInYardException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.exception.checkin.EquipmentOccupyResourcesException;
import com.cwc.app.exception.checkin.EquipmentSameToEntryException;
import com.cwc.app.exception.checkin.ResourceHasUsedException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.exception.checkin.SpotNotFindException;
import com.cwc.app.exception.checkin.TaskNoAssignWarehouseSupervisorException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.AndroidUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

/**
 * android window checkin
 * @author win7zr
 *
 */
public class AndroidWindowCheckInAction extends ActionFatherController{

	private CheckInMgrIFaceZyj checkInMgrZyj;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private AdminMgrIFaceZJ adminMgrZJ;
	private CheckInMgrIfaceZr checkInMgrZr ;
	

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow result = new DBRow();
			try{
 				JSONObject data = getRequestData(request);
 				String method = data.getString("Method") ;

 				if(method.equalsIgnoreCase("CovertCarrier")){
 					checkInMgrZr.convertCarrierPhoneNumber();
 				}
 				if(method.equalsIgnoreCase("CovertGateDriverLiscense")){
 					checkInMgrZr.convertGateDriverLiscense();
 				}
 				if(!data.getString("Version").equals(AndroidUtil.getVersion())){
					throw new AppVersionException();
				}
 				DBRow loginRow = adminMgrZJ.MachineLogin(data.getString("LoginAccount"), data.getString("Password"));
				if(loginRow == null){
					throw new MachineUnderFindException();
				}
				//模拟登陆
				AdminLoginBean adminLoginBean =  AndroidPermissionUtil.androidSetSession(loginRow, request);
				
 				
 				
 				/**
 				 * 刚开始进入界面的查询
 				 */
 				if(method.equalsIgnoreCase("WindowCheckInList")){ //获取WindowCheckInList
 					long entry_id =  StringUtil.getJsonLong(data, "entry_id");
 					//going to window
 					PageCtrl pc = new PageCtrl();
 					pc.setPageNo(1);
 					pc.setPageSize(100);
 					DBRow[] datas =  checkInMgrZr.windowCheckInList(adminLoginBean.getPs_id(),pc);
 					result.add("datas", datas);
 				}
 				/**
 				 * window Check in 的搜索界面
 				 */
 				if(method.contentEquals("WindowCheckInSearchEntry")){
 					String searchValue = StringUtil.getJsonString(data, "searchvalue");
  					int searchType  = StringUtil.getJsonInt(data, "searchtype");
 					DBRow[] entrys = checkInMgrZr.getWindowCheckInSearchValue(searchValue,searchType,adminLoginBean);
 					if(entrys == null || entrys.length < 1){
 						throw new NoRecordsException();
 					}
 					result.add("datas", entrys);
 				}
 				/**
 				 * 通过一个EntryId ，去查询 他所有的 Equipments （包含自己的 + PickUp人家的）
 				 */
 				if(method.equalsIgnoreCase("WindowCheckInEntryEquipmentList")){
 					long entry_id = StringUtil.getJsonLong(data, "entry_id");
 					DBRow[] datas = checkInMgrZr.windowCheckInEntryEquipmentList(entry_id);
 					result.add("datas", datas);
 				}
 				/**
 				 * 获取 equipment Infos
 				 */
 				if(method.equalsIgnoreCase("WindowCheckInGetEquipmentInfos")){
 					long equipment_id = StringUtil.getJsonLong(data, "equipment_id");
 					long entry_id = StringUtil.getJsonLong(data, "entry_id");
 					result =  checkInMgrZr.windowCheckInGetEquipmentInfos(equipment_id, entry_id);
 					sortWindowEquipmentTaskResoucesInfos(result);
 					 
  				}
 				/**
 				 * window check 修改Driver info 的信息
 				 */
 				if(method.equalsIgnoreCase("WindowCheckInEntryInfos")){
 					long entry_id = StringUtil.getJsonLong(data, "entry_id");
 					DBRow  temp = checkInMgrZr.getWindowCheckInEntryInfos(entry_id);
 					temp.add("prioritys", "1,2,3,0");
 					result.add("data", temp);
 				}
 				/**
 				 * update driver info
 				 */
 				if(method.equalsIgnoreCase("UpdateEntryDriverInfo")){
 					  DBRow updateRow = getWindowCheckInUpdateDriverInfo(data);
 					  long  entry_id = updateRow.get("entry_id", 0l);
 					  updateRow.remove("entry_id");
 					  result =  checkInMgrZr.windowCheckInUpdateEntryDriverInfo(updateRow, entry_id,adminLoginBean);

  				}
 				
 				if(method.equalsIgnoreCase("HandMcDotAndCarrier")){
 					 DBRow dbrowData = new DBRow();
 					 dbrowData.add("flag", StringUtil.getJsonInt(data, "flag"));
 					 dbrowData.add("mc_dot", StringUtil.getJsonString(data, "mc_dot"));
 					 dbrowData.add("carrier", StringUtil.getJsonString(data, "carrier"));
 					 dbrowData.add("id", StringUtil.getJsonString(data, "id"));
  					 checkInMgrZr.handMcDotAndCarrier(dbrowData);
 				}
 				
 				//添加或者更新设备
 				if(method.equalsIgnoreCase("AddOrUpdateEquipment"))
 				{
 					result = checkInMgrZyj.addOrUpdateEquipment(data, adminLoginBean);
 				}
 				else if(method.equalsIgnoreCase("DeleteEquipment"))
 				{
 					int isCheckSuccess = checkInMgrZyj.deleteEquipment(data, adminLoginBean);
 					result.add("is_check_success", isCheckSuccess);
 				}
 				else if(method.equalsIgnoreCase("AddOrUpdateTask"))
 				{
 					String itemstr		= StringUtil.getJsonString(data, "items");
 					JSONArray items = new JSONArray(itemstr);
 					data.remove("items");
 					data.put("items", items);
 					checkInMgrZyj.checkInWindowAddOrUpdateTask(data, adminLoginBean);
 				}
 				else if(method.equalsIgnoreCase("DeleteTask"))
 				{
 					checkInMgrZyj.windowCheckInDeleteTask(data, adminLoginBean);
 				}
 				else if(method.equalsIgnoreCase("CheckOrderTypeSystem"))
 				{
 					long type = StringUtil.getJsonInt(data, "type");
 					String search_number = StringUtil.getJsonString(data, "search_number");
 					long mainId = StringUtil.getJsonLong(data, "mainId");
 					long ps_id = adminLoginBean.getPs_id();
 					long adid = adminLoginBean.getAdid();
 					result = checkInMgrZwb.checkOrderTypeSystem(type, search_number, mainId, ps_id, adid);
 					
 				}
 				//暂时不用
 				else if(method.equalsIgnoreCase("AutoAsignDoorByTitleZone"))
 				{
 					String title_name = StringUtil.getJsonString(data, "title_name");
 					long ps_id = StringUtil.getJsonLong(data, "ps_id");
 					DBRow[] rows = checkInMgrZwb.findZoneByTitleName(title_name, ps_id);
 					result.add("zones", rows);
 				}
 				else if(method.equalsIgnoreCase("InYardDropEquipmentsByNumber"))
 				{
 					String number = StringUtil.getJsonString(data, "equipment_number");
 					DBRow[] rows = checkInMgrZyj.findInYardDropEquipmentsByNumber(number, adminLoginBean.getPs_id(), CheckInTractorOrTrailerTypeKey.TRAILER);
 					result.add("equipments_in_yard", rows);
 				}else if(method.equalsIgnoreCase("EquipmentInfoAndEquipmentTasks"))
 				{
 					long equipment_id = StringUtil.getJsonLong(data, "equipment_id");
 					result = checkInMgrZyj.findEquipmentInfoAndEquipmentTasks(equipment_id);
 				}
 				else if(method.equalsIgnoreCase("EquipmentInfosById"))
 				{
 					long equipment_id = StringUtil.getJsonLong(data, "equipment_id");
 					result = checkInMgrZyj.findEquipmentReOccupyById(equipment_id);
 				}
 				else if(method.equalsIgnoreCase("TaskInfosByTaskId"))
 				{
 					long dlo_detail_id = StringUtil.getJsonLong(data, "detail_id");
 					result = checkInMgrZyj.findTaskEquipmentResourceByDetailIdSameCheck(dlo_detail_id);
 				}
 				else if(method.equalsIgnoreCase("FindSupervisors"))
 				{
 					long ps_id = adminLoginBean.getPs_id();
 					DBRow supervisors = checkInMgrZyj.findSupervisors(ps_id, 0L);
 					result.add("notice_names", supervisors.getString("adnames"));
 					result.add("notice_ids", supervisors.getString("adids"));
 				}
 				else if(method.equalsIgnoreCase("FindScaners"))
 				{
 					long ps_id = adminLoginBean.getPs_id();
 					DBRow scaners = checkInMgrZyj.findSupervisors(ps_id, 1000013L);
 					result.add("notice_names", scaners.getString("adnames"));
 					result.add("notice_ids", scaners.getString("adids"));
 				}
 				else if(method.equalsIgnoreCase("TaskChangeResourceOrEquipment"))
 				{
 					checkInMgrZyj.windowCheckInTaskChangeResources(data, adminLoginBean);
 				}
 				else if(method.equalsIgnoreCase("GetNotLeftEquipmentByEntryAndTotalTask"))
 				{
 					long entry_id = StringUtil.getJsonLong(data, "entry_id");
 					result.add("datas", checkInMgrZyj.getNotLeftEquipmentByEntryAndTotalTask(entry_id));
 				}
 			}catch(NoPermiessionEntryIdException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.NoPermiessionEntryIdException;
 			}catch(CheckInEntryIsLeftException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInEntryIsLeftException;
 			}catch(NoRecordsException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.NoRecordsException;
 			}catch(CheckInNotFoundException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInNotFound;
  			}catch(EquipmentNotFindException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentNotFindException;
 			} catch(EquipmentHadOutException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentHadOutException;
 			} catch(EquipmentHasDecidedPickedException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentHasDecidedPickedException;
 			} catch(EquipmentOccupyResourcesException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentOccupyResourcesException;
 			}catch (ResourceHasUsedException e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.ResourceHasUsedException;
 			}catch(DoorNotFindException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.DoorNotFindException;
 			}catch(SpotNotFindException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.SpotNotFindException;
 			}catch(EquipmentInYardException e){
 				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentInYardException;
 			}catch(DoorHasUsedException e1){
 				ret = BCSKey.FAIL ;
				err = BCSKey.DoorHasUsedException;
			}catch (SpotHasUsedException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.SpotHasUsedException;
			}catch (CheckInTaskCanntDeleteException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInTaskCanntDeleteException;
			}catch (CheckInTaskRepeatToEntryException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInTaskRepeatToEntryException;
			}catch (CheckInTaskRepeatToOthersException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInTaskRepeatToOthersException;
			}catch (CheckInMustHasTasksException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInMustHasTasksException;
			}catch (CheckInAllTasksClosedException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInAllTasksClosedException;
			}catch (CheckInMustNotHaveTasksException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInMustNotHaveTasksException;
			}catch (CheckInTaskHaveDeletedException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckInTaskHaveDeletedException;
			}catch (EquipmentHasLoadedOrdersException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentHasLoadedOrdersException;
			}catch(CheckinTaskNotFoundException e2){
				ret = BCSKey.FAIL ;
				err = BCSKey.CheckinTaskNotFoundException;
			}catch(EquipmentSameToEntryException e2){
				ret = BCSKey.FAIL ;
				err = BCSKey.EquipmentSameToEntryException;
			}catch(TaskNoAssignWarehouseSupervisorException e2){
				ret = BCSKey.FAIL ;
				err = BCSKey.TaskNoAssignWarehouseSupervisorException;
			}
			 
			result.add("ret", ret);
			result.add("err", err);
 			// System.out.println(StringUtil.convertDBRowsToJsonString(result) + "....");
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	/**
	 * 给姜振的返回的数据排序
	 * @author zhangrui
	 * @throws Exception 
	 * @Date   2015年2月10日
	 */
	private void sortWindowEquipmentTaskResoucesInfos(DBRow result) throws Exception{
		DBRow[] tasks = (DBRow[]) result.get("tasks", new DBRow[]{});
		if(tasks != null && tasks.length > 0){
			Arrays.sort(tasks, new Comparator<DBRow>() {
				@Override
				public int compare(DBRow o1, DBRow o2) { 
					//对于没有资源 或者是Spot的那么排在前面 
					int resources_type_o1 = o1.get("resources_type", -1);  //3 表示没有资源的情况
					int resources_type_o2 = o2.get("resources_type", -1);  //3表示没有资源的情况
					resources_type_o1 = resources_type_o1 == 0 ? 3 : resources_type_o1 ;
					resources_type_o2 = resources_type_o2 == 0 ? 3 : resources_type_o2 ;

 					try {
						sortWindowEquipmentTasks((DBRow[])o1.get("load_list", new DBRow[]{}));
						sortWindowEquipmentTasks((DBRow[])o2.get("load_list", new DBRow[]{}));
					} catch (Exception e) {
 						e.printStackTrace();
					}
					if(resources_type_o1 > resources_type_o2){
						return -1 ;
					}else if(resources_type_o1 < resources_type_o2){
						return 1 ;
					}else if(resources_type_o1 == resources_type_o2){
						//都是相同的资源类型,那么在按照名字排序
						return o1.getString("resources_type_value").compareTo(o2.getString("resources_type_value"));
					}
					return 0 ;
				}
			});
		}
	}
	
	private void sortWindowEquipmentTasks(DBRow[] load_list){
		if(load_list != null && load_list.length > 0){
			Arrays.sort(load_list,  new Comparator<DBRow>(){
				@Override
				public int compare(DBRow o1, DBRow o2) {
					return o1.getString("number_status").compareTo(o2.getString("number_status"));
				}
			});
		}
	}
	//{"entry_id":"120965","gate_liscense_plate":"ZHANGRUIPICK","Mac":"08:00:27:66:ec:09","Machine":"",
	//"Method":"UpdateEntryDriverInfo","company_name":"CONTINENTAL COURIER LIMITED","priority":"NA","Password":"admin","LoginAccount":"admin","mc_dot":"388178","Version":"1.0.35","gate_driver_name":"3434"}
	private DBRow getWindowCheckInUpdateDriverInfo(JSONObject data){
		DBRow temp = new DBRow();
		 temp.add("company_name", StringUtil.getJsonString(data, "company_name").toUpperCase());
		 temp.add("priority", StringUtil.getJsonString(data, "priority"));
		 temp.add("mc_dot", StringUtil.getJsonString(data, "mc_dot"));
		 temp.add("gate_driver_name", StringUtil.getJsonString(data, "gate_driver_name").toUpperCase());
		 temp.add("gate_driver_liscense", StringUtil.getJsonString(data, "gate_driver_liscense").toUpperCase());
		 temp.add("entry_id", StringUtil.getJsonString(data, "entry_id"));
		 temp.add("file_path", StringUtil.getJsonString(data, "filePath"));
		 temp.add("session_id", StringUtil.getJsonString(data, "SESSION_ID"));
		return temp ;
		
	}
	
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	//将传过来的值转成json
	public JSONObject getRequestData(HttpServletRequest request)  throws Exception{
		JSONObject returnRow = new JSONObject();
		String sessionId=request.getRequestedSessionId();
		returnRow.put("SESSION_ID", sessionId);
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		if(isMutilRequest){
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.put(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.put("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_checkin", "_checkIn.zip"));
		    	}
		    }
		    returnRow.put("files", files);
		}else{
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				String key = enumer.nextElement();
				returnRow.put(key, StringUtil.getString(request, key));
			}
		}
 	    return returnRow;
	}
	
	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}
	
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}


}
