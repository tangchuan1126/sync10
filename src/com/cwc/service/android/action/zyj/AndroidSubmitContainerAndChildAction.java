package com.cwc.service.android.action.zyj;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.ContainerNotFullException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zyj.AndroidMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.service.android.action.mode.ContainerNode;
import com.cwc.util.AndroidUtil;
  

public class AndroidSubmitContainerAndChildAction extends ActionFatherController{

	private AndroidMgrIFaceZyj androidMgrZyj;
	
	private AdminMgrIFaceZJ adminMgrZJ;
	 
	
	@SuppressWarnings("finally")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			try{
				DBRow fromAndroid = getJsonObjectFromRequest(request);
				//首先进行登录
				DBRow login = adminMgrZJ.MachineLogin(fromAndroid.getString("LoginAccount"), fromAndroid.getString("Password"));
				////system.out.println("login:"+(null==login));
				if(login == null)
				{
					throw new MachineUnderFindException();
				}
				result = androidMgrZyj.submitContainer(AndroidUtil.getDataFromAndroidXml(request.getInputStream()));
 				 
			}catch (ContainerNotFoundException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LPNotFound;
 			}catch (MachineUnderFindException e) {
				ret = BCSKey.FAIL ;
				err = BCSKey.LOGINERROR;
 			}catch (Exception e) {
 				ret = BCSKey.FAIL ;
				err = BCSKey.SYSTEMERROR;
 			}finally{
				result.add("ret", ret);
				result.add("err", err);
 				throw new JsonException(new JsonObject(result));
 			}
	}
	/**
	 * 从request 对象中获取XMl文件。
	 * 然后解析出来生成DBRow 
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws SystemException 
	 * 
	 * 
	 * dbrow 
	 * 	loginAccount
	 * 	Password
	 * 	Machine
	 * 	data:{List<DBRow[]>}
	 *  	  
	 * 
	 */
	@SuppressWarnings("unchecked")
	private DBRow getJsonObjectFromRequest(HttpServletRequest request) throws IOException, SystemException{
		
		DBRow result = new DBRow();
		 
		InputStream inputStream =  request.getInputStream();
        SAXReader reader = new SAXReader();  
		
        try {
			Document document = reader.read(inputStream);
			
			Element root = document.getRootElement();
			
			
	 
			List<Node> listLoginAccount = 	root.selectNodes("LoginAccount");
			List<Node> listPassword = 	root.selectNodes("Password");
			List<Node> listMachine = 	root.selectNodes("Machine");
			
			result.add("LoginAccount", listLoginAccount.get(0).getText());
			result.add("Password", listPassword.get(0).getText());
			result.add("Machine", listMachine.get(0).getText());
		 
			List<DBRow> datas = new ArrayList<DBRow>();
			List<ContainerNode> listContainerNode = new ArrayList<ContainerNode>();
			ContainerNode rootNode = null ;
			 Element details =	root.element("Detail").element("Details");
			 if(details != null){
				 List<Element> containers =  details.elements("Container");
				 if(containers != null && containers.size() > 0 ){
					 for(int index = 0 , count = containers.size() ; index < count ; index++ ){
						 Element container = containers.get(index);
						
						 DBRow containerProduct = handleXmlToObject(container);
						 
						 //添加到ContainerNode中
						 ContainerNode containerNode = new ContainerNode(containerProduct);
						 listContainerNode.add(containerNode);
						 if(containerProduct.get("parent_con_id", 0L) == 0l)
						 {
							 rootNode = containerNode;
						 }
						 
						 //容器中的容器
						 List<Element> subContainers = container.elements("SubContainer");
						 if(null != subContainers && subContainers.size() > 0)
						 {
							 for (Element element : subContainers)
							 {
								DBRow innerContainer = handleXmlToObject(element);
								ContainerNode innerContainerNode = new ContainerNode(innerContainer);
								listContainerNode.add(innerContainerNode);
								List<Element> products = element.elements("SubProduct");
								if(products != null && products.size() > 0)
								{
									List<DBRow> subContProducts = new ArrayList<DBRow>();
									for(Element product : products)
									{
		 								DBRow productInContainer = this.handleXmlToDBRow(product, innerContainer.get("con_id", 0L));
		 								subContProducts.add(productInContainer);
									}
									innerContainer.add("products", subContProducts.toArray(new DBRow[subContProducts.size()]));
								}
							 }
						 }
						 
						 //容器中的商品
						 List<Element> products = container.elements("Product");
						 if(products != null && products.size() > 0){
							 List<DBRow> innerProducts = new ArrayList<DBRow>();
							 for(Element product : products){
 								 DBRow productInContainer = this.handleXmlToDBRow(product, containerProduct.get("con_id", 0L));
								 innerProducts.add(productInContainer);
							 }
							 containerProduct.add("products", innerProducts.toArray(new DBRow[innerProducts.size()]));
						 }
						 datas.add(containerProduct);
					 }
				 }
			 }
			 result.add("datas", datas);
			 if(rootNode != null && listContainerNode.size() > 0)
			 {
				 ContainerNode.initContainerLoading(rootNode, listContainerNode);
				 result.add("rootNode", rootNode);
				 result.add("listContainerNode", listContainerNode);
			 }
	 
		} catch (DocumentException e) {
 			e.printStackTrace();
		}  
	 
		return result ;
	}
	 
	
	private DBRow handleXmlToObject(Element container)
	{
		 long con_id = 	Long.parseLong((container.attributeValue("con_id")));
		 int container_type = Integer.parseInt((container.attributeValue("container_type")));
		 long container_type_id = Long.parseLong((container.attributeValue("container_type_id")));
		 long parent_con_id = Long.parseLong((container.attributeValue("parent_con_id")));
		 int parent_container_type = Integer.parseInt((container.attributeValue("parent_container_type")));
		 long parent_container_type_id = Long.parseLong((container.attributeValue("parent_container_type_id")));
		 long titleId = Long.parseLong((container.attributeValue("title_id")));
		 String lotNumber = container.attributeValue("lotNumber");
		 
		 DBRow containerProduct = new DBRow();
		 containerProduct.add("con_id", con_id);
		 containerProduct.add("container_type",container_type);
		 containerProduct.add("container_type_id",container_type_id);
		 containerProduct.add("parent_con_id",parent_con_id);
		 containerProduct.add("parent_container_type",parent_container_type);
		 containerProduct.add("parent_container_type_id",parent_container_type_id);
		 containerProduct.add("title_id", titleId);
		 containerProduct.add("lot_number", lotNumber);
//		 //system.out.println("subContainer:"+con_id+","+container_type+","+container_type_id+","+parent_con_id+","+parent_container_type+","+parent_container_type_id+","+titleId+","+lotNumber);
		 
		 return containerProduct;
	}
	
	private DBRow handleXmlToDBRow(Element product, long con_id)
	{
		 String sn = product.attributeValue("sn");
		 if(null == sn || "NULL".equals(sn.toUpperCase()))
		 {
			 sn = "";
		 }
		 double cp_quantity = Double.parseDouble(product.attributeValue("qty"));
		 long cp_pc_id = Long.parseLong(product.attributeValue("pcid"));
		 String cp_lp_name =  product.attributeValue("cp_lp_name");
		 
		 DBRow productInContainer = new DBRow();
		 productInContainer.add("cp_sn", sn);
		 productInContainer.add("cp_quantity", cp_quantity);
		 productInContainer.add("cp_pc_id", cp_pc_id);
		 productInContainer.add("cp_lp_id", con_id);
		 productInContainer.add("cp_lp_name", cp_lp_name);
		 
//		 //system.out.println("Product:"+sn+","+cp_quantity+","+cp_pc_id+","+con_id+","+cp_lp_name);
		 
		 return productInContainer;
	}
 
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setAndroidMgrZyj(AndroidMgrIFaceZyj androidMgrZyj) {
		this.androidMgrZyj = androidMgrZyj;
	}

	 
}
