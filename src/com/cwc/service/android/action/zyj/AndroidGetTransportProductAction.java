package com.cwc.service.android.action.zyj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 这个类以后要删除一些东西
 * @author zhangrui
 *
 */
public class AndroidGetTransportProductAction extends ActionFatherController{

	private AndroidMgrIfaceZr androidMgrZr;
	private AdminMgrIFaceZJ adminMgrZJ;
	 /**
	  * 	machine : 表示机器的型号
	  * 	transport_id : 表示的是transport_id
	  */
	@SuppressWarnings("finally")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		
		
		////system.out.println("AndroidGetTransportProductAction");
		
			DBRow requestParam = getJsonObjectFromRequest(request);
			
			DBRow login = adminMgrZJ.MachineLogin(requestParam.getString("LoginAccount"), requestParam.getString("Password"));
			if(login == null){
				throw new MachineUnderFindException();
			}
		  
			long transport_id = Long.parseLong(requestParam.getString("transport_id")) ;
			DBRow result = new DBRow();
			result.add("ret",BCSKey.SUCCESS+"");
			result.add("product", getProductRow(transport_id, requestParam.getString("fc")));
			result.add("productcode", getProductCodeRow(transport_id, requestParam.getString("fc")));
			result.add("serialproduct", getSerialProductRow(transport_id, requestParam.getString("fc")));
			result.add("transportdetails", getTransportDetails(transport_id, requestParam.getString("fc")));
			////system.out.println(new JsonObject(result).toString());
			
			throw new JsonException(new JsonObject(result));
			
	}
	
	private DBRow[] getTransportDetails(long transport_id, String fc)
	{
		DBRow[] result = null;
		try
		{
			result = androidMgrZr.getTransportDetail(transport_id);
		}
		catch (Exception e) { }
		return result;
	}

	
	private DBRow[] getProductRow(long transport_id, String fc) {
		DBRow[] result = null ;
		try
		{
			if(fc.equals("OutBoundBaseInfo")){
				result = androidMgrZr.getOutboundProductInfo(transport_id);
			}
			if(fc.equals("InboundBaseInfo")){
				result = androidMgrZr.getProductForTransport(transport_id);
			}
			
			////system.out.println("product:"+result.length);
		}catch (Exception e) {
		}
		return result;
 
	}
	
	/**
	 * 生成productCode.xml
	 * <Details>
			<PCID>10000</PCID>
			<Barcode>B/H1/43K</Barcode>
			<Show>1</Show>
		</Details>
		一个PCID 要对应创建一个Barcode的PCID
	 * @param transport_id
	 */
	private DBRow[] getProductCodeRow(long transport_id , String fc) {
		List<DBRow> rows = new ArrayList<DBRow>();
		try{
			
			DBRow[] result = null ;
			if(fc.equals("OutBoundBaseInfo")){
				result = androidMgrZr.getProductCodeForOutboundTransport(transport_id);
			}
			if(fc.equals("InboundBaseInfo")){
				result = androidMgrZr.getProductCodeForTransport(transport_id);
			}
			
			//将商品的条码和商品的ID都作为条码来处理
 			if(result != null && result.length > 0){
				Set<Long> pcids = new HashSet<Long>();
				String pName = "" ;
				for(DBRow row : result){
					pcids.add(row.get("pc_id", 0l));
					if(pName.length() < 1){
						pName = row.getString("p_name");
					} 
					rows.add(row);
				}
				//处理一个PCID 要对应创建一个Barcode的PCID情况
				if(pcids.size() > 0 ){
					Iterator<Long> it = pcids.iterator();
					while(it.hasNext()){
						long pc_id = it.next();
						if(pc_id != 0l){
							DBRow row = new DBRow();
							row.add("pc_id", pc_id);
							row.add("p_name", pName);		//这里有错
							row.add("p_code", pc_id);
							row.add("code_type", 0); //表示BarCode 就是PCID
							rows.add(row);
						}
					}
				}
			}
		}catch (Exception e) {
 		}
		////system.out.println("productCode:"+rows.size());
 		return rows.toArray(new DBRow[0]);
	}
	
	
	/**
	 * 生成sku-sn对应的serialProduct.xml
	 * <sp  product_serial_id="10002" sn="164895" sku="3232323" supplierId="232323" inTime="" outTime=""></sp>
	 * @param transport_id
	 */
	public DBRow[] getSerialProductRow(long transport_id,String fc) {
		DBRow[] result = null ;
		try
		{
			if(fc.equals("OutBoundBaseInfo"))
			{
				result = androidMgrZr.getSerialProductForOutboundTransport(transport_id);
			}
			if(fc.equals("InboundBaseInfo"))
			{
				result = androidMgrZr.getSerialProductForTransport(transport_id);
			}
			////system.out.println("se:"+result.length);
		}
		catch (Exception e)
		{
			
 		}
		return result;
	}
	
	private DBRow getJsonObjectFromRequest(HttpServletRequest request)
	{
		DBRow result = new DBRow();
		result.add("LoginAccount", StringUtil.getString(request, "LoginAccount"));
		result.add("Password", StringUtil.getString(request, "Password"));
		result.add("Machine", StringUtil.getString(request, "Machine"));
		result.add("Method", StringUtil.getString(request, "Method"));
		result.add("transport_id", StringUtil.getString(request, "transport_id"));
		result.add("fc", StringUtil.getString(request, "fc"));
		
		/*//system.out.println("account:"+StringUtil.getString(request, "LoginAccount"));
		//system.out.println("Password:"+StringUtil.getString(request, "Password"));
		//system.out.println("fc"+StringUtil.getString(request, "fc"));
		//system.out.println("transport_id:"+StringUtil.getString(request, "transport_id"));*/
		return result;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}
	
	
	
}
