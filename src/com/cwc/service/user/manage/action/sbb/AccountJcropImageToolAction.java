package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;

public class AccountJcropImageToolAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/upl_imags_tmp/";
		String outImagePath = Environment.getHome().replace("\\", "/")+"."+ "/upload/account/";
		
		Map<String,Object> parameter = new HashMap<String, Object>();
		parameter.put("x", request.getParameter("x"));
		parameter.put("y", request.getParameter("y"));
		parameter.put("w", request.getParameter("w"));
		parameter.put("h", request.getParameter("h"));
		parameter.put("inImagePath", fileUploadPath + request.getParameter("bigImage"));
		parameter.put("outImagePath", outImagePath);
		
		parameter.put("file_name", request.getParameter("bigImage"));
		parameter.put("adid", request.getParameter("adid"));
		parameter.put("flag", request.getParameter("flag"));
		
		String url = accountMgr.croppedImage(parameter);
		
		DBRow result = new DBRow();
		result.add("url",url);
		
		throw new JsonException(new JsonObject(result));
	}
}
