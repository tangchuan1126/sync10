package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;

public class PostValidatorMgrAction extends ActionFatherController {
	
	private RoleMgrIFaceSbb roleMgr;
	
	public void setRoleMgr(RoleMgrIFaceSbb roleMgr) {
		this.roleMgr = roleMgr;
	}
	
	/**
	 * 账号管理 >> 验证
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		
		try {
			
			Map<String,String> parameter = new HashMap<String,String>();
			parameter.put("postName", request.getParameter("postName"));
			parameter.put("adpid",StringUtils.defaultString(request.getParameter("id"),"-1"));
			
			boolean result = roleMgr.postValidator(parameter);
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(new JSONObject().put("success", result).toString());
			
		}catch(Exception e){
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}