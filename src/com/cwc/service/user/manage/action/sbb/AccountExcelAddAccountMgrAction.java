package com.cwc.service.user.manage.action.sbb;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.Element;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.util.XmlUtil;

public class AccountExcelAddAccountMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	private String getPost(InputStream inputStream) throws Exception {
		
		BufferedInputStream input = null;
		byte[] buffer = new byte[1024];                                     
		int count = 0;                                                          
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		}catch (Exception e){
			
			e.printStackTrace();
		}finally{
			
			if(input != null){
			    try {
			    	input.close();
			    }catch (Exception f){
			    	f.printStackTrace();
			    }
			}
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private DBRow initDataXmlStringToDBRow(String xmlString,String[] nodeNames) throws Exception{
    	
		Document document = XmlUtil.getXMLDocForStringXML(xmlString);
    	
    	DBRow row = new DBRow();
    	for (int i = 0; i < nodeNames.length; i++){
    		
    		String nodeName = "//"+nodeNames[i];
    		Element element = (Element)document.selectSingleNode(nodeName);
    		String value = element.getStringValue().trim();
    		row.add(nodeNames[i].toLowerCase(),value);
		}
    	
    	return row;
    }
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		InputStream inputStream = request.getInputStream();
		String xml = getPost(inputStream);
		
		String[] params = new String[]{"account","password","employeName","telephone","email","warehouse","area","department","post"};
		DBRow row = this.initDataXmlStringToDBRow(xml,params);
		
		JSONObject returnVal = new JSONObject();
		
		//excel插入账号
		Map<String,String> result = accountMgr.addAccountExcel(row);
		
		Set<String> key = result.keySet();
		for (Iterator<String> it = key.iterator(); it.hasNext();) {
	        
			String oneKey = (String) it.next();
	     	returnVal.put(oneKey,result.get(oneKey));
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnVal.toString());
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "POST":
					doPost(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}