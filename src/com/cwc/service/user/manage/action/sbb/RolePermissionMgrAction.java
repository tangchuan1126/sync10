package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.cwc.app.util.DBRowUtils;

public class RolePermissionMgrAction extends ActionFatherController {
	
	private RoleMgrIFaceSbb roleMgr;
	
	public void setRoleMgr(RoleMgrIFaceSbb roleMgr) {
		this.roleMgr = roleMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String id = request.getParameter("id")!=null?request.getParameter("id"):"-1";
		
		//获取权限列表
		DBRow[] result = roleMgr.getPermissionContextList(StringUtil.getLongType(id));
		
		JSONObject output = new JSONObject();
		output.put("permission",DBRowUtils.multipleDBRowArrayAsJSON(result));
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		
		DBRow parameterRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		DBRow result = roleMgr.modifyRolePermission(parameterRow);
		
		JSONObject returnResult;
		
		if(result==null){
			returnResult = new JSONObject().put("success", false).put("error","此角色已删除!");
		}else{
			returnResult = new JSONObject(DBRowUtils.dbRowAsMap(result));
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}