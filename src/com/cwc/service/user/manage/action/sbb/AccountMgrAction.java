package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;

public class AccountMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1")));
		pc.setPageSize(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "10")));
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("PageCtrl", pc);
		
		parameter.put("searchConditions", StringUtils.defaultString(request.getParameter("searchConditions"),""));
		parameter.put("searchDeptment", StringUtils.defaultString(request.getParameter("searchDeptment"),""));
		parameter.put("searchPost", StringUtils.defaultString(request.getParameter("searchPost"),""));
		parameter.put("searchWarehouse", StringUtils.defaultString(request.getParameter("searchWarehouse"),""));
		parameter.put("searchArea", StringUtils.defaultString(request.getParameter("searchArea"),""));
		parameter.put("searchLocation", StringUtils.defaultString(request.getParameter("searchLocation"),""));
		
		parameter.put("searchFingerprint", StringUtils.defaultString(request.getParameter("searchFingerprint"),"false"));
		parameter.put("searchToken", StringUtils.defaultString(request.getParameter("searchToken"),"false"));
		parameter.put("searchPermission", StringUtils.defaultString(request.getParameter("searchPermission"),"false"));
		parameter.put("searchTitle", StringUtils.defaultString(request.getParameter("searchTitle"),"false"));
		parameter.put("searchUploadPhoto", StringUtils.defaultString(request.getParameter("searchUploadPhoto"),"false"));
		parameter.put("searchShield", StringUtils.defaultString(request.getParameter("searchShield"),"false"));
		parameter.put("cmd", StringUtils.defaultString(request.getParameter("cmd"),""));
		//查询账户
		Map<String,DBRow[]> accountList = accountMgr.getAccountList(parameter);
		
		JSONObject output = new JSONObject();
		output.put("pageCtrl", new JSONObject(pc));
		
		Set<String> key = accountList.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
            output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(accountList.get(oneKey)));
        }
        
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		
		DBRow accountRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		//只获取AdminLoginBean
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminBean = DBRowUtils.getAdminLoginBean(session.getAttribute(Config.adminSesion));
		//记录创建人 时间
		accountRow.put("created_by", adminBean.getAdid());
		accountRow.put("create_date", DateUtil.NowStr());
		
		//添加账户
		DBRow result = accountMgr.addAccount(accountRow);
		
		try {
			//刷新openfire人员
			if((boolean)result.get("SUCCESS")){
				long adid = result.get("adid", 0l);
				String account = accountRow.getString("ACCOUNT");
				accountMgr.refreshOpenfireMember(adid, account);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String flag = request.getParameter("flag");
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow accountRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		//修改账户
		DBRow result = accountMgr.modifyAccount(accountRow,flag);
		
		JSONObject returnResult;
		if(result==null){
			returnResult = new JSONObject().put("success", false).put("error","此账户已删除!");
		}else{
			returnResult = new JSONObject(DBRowUtils.dbRowAsMap(result));
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		//删除账户
		accountMgr.dropAccount(Long.parseLong(request.getParameter("id")));
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}