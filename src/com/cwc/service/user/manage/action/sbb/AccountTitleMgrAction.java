package com.cwc.service.user.manage.action.sbb;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class AccountTitleMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String id = StringUtils.defaultString(request.getParameter("id"), "-1");
		String associatedTitle = StringUtils.defaultString(request.getParameter("associatedTitle"), "");
		String noCorrelationTitle = StringUtils.defaultString(request.getParameter("noCorrelationTitle"), "");
		
		Map<String,DBRow[]> accountTitle = accountMgr.getAccountTitle(Long.parseLong(id),associatedTitle,noCorrelationTitle);
		
		JSONObject output = new JSONObject();
		
		Set<String> key = accountTitle.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
            output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(accountTitle.get(oneKey)));
        }
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		
		DBRow accountTitleRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		DBRow[] hasTitle = accountMgr.swapAccountTitle(accountTitleRow);
		
		//只获取AdminLoginBean
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminBean = DBRowUtils.getAdminLoginBean(session.getAttribute(Config.adminSesion));
		
		if(adminBean.getAdid() == accountTitleRow.get("adid", -1)){
			
			//修改后的title加到session里
			adminBean.setTitles(hasTitle);
			session.setAttribute(Config.adminSesion, adminBean);
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		
		DBRow titleRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		accountMgr.modifyAccountTitleSort(titleRow);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}