package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;

public class AndroidAccountPhotoMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
		String[] filePath = accountMgr.changeTofficialFile(param);
		
		String url = "";
		long id = -1L;
		
		if(filePath!=null && filePath.length>0){
			
			DBRow row = new DBRow();
			row.put("adid", Long.valueOf(param.get("adid","-1")));
			row.put("file_flag", param.get("file_flag","portrait"));
			
			long[] ids = new long[filePath.length];
			
			for(int i=0;i<filePath.length;i++){
				
				row.put("file_path", "/Sync10/upload/account/"+filePath[i]);
				ids[i] = accountMgr.addAccountImage(row);
			}
			
			//返回一个URL
			url = "upload/account/"+filePath[0];
			id =  ids[0];
		}
		
		
        response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("ret", "1").put("success", true).put("url", url).put("id", id).toString());
	}
	
	public void doPut(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
		//修改图像
		int result = accountMgr.modifyAccountPhoto(param);
		
		JSONObject returnResult;
		
		if(result==0){
			returnResult = new JSONObject().put("ret", "1").put("success", false).put("error","1");
		}else{
			returnResult = new JSONObject().put("ret", "1").put("success", true);
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
		long id = param.get("id", -1L);
		
		//删除账户图像
		accountMgr.dropAccountPhoto(id);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("ret", "1").put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		
		try {
			
			DBRow param = accountMgr.getRequestData(request);
			
			String android = param.get("android", "-1");
			
			switch (android) {
			
				case "GET":
					doGet(request, response, param);
					break;
				case "POST":
					doPost(request, response, param);
					break;
				case "PUT":
					doPut(request, response, param);
					break;
				case "DELETE":
					doDelete(request, response, param);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
