package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.UserTitleMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;

/**
 * 新框架实施后废除此类
 * 
 * */
public class UserTitleManageSortTitleAction extends ActionFatherController  {
	
	private UserTitleMgrIFaceSbb userTitleMgr;
	
	public void perform(HttpServletRequest requset, HttpServletResponse response) throws Exception {
		
		userTitleMgr.changeUserTitlePriority(requset);
		
		DBRow result = new DBRow();
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
	}
	
	public void setUserTitleMgr(UserTitleMgrIFaceSbb userTitleMgr) {
		this.userTitleMgr = userTitleMgr;
	}
}
