package com.cwc.service.user.manage.action.sbb;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.XmlUtil;

public class AccountExcelLoginMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	private String getPost(InputStream inputStream) throws Exception {
		
		BufferedInputStream input = null;
		byte[] buffer = new byte[1024];                                     
		int count = 0;                                                          
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		}catch (Exception e){
			
			e.printStackTrace();
		}finally{
			
			if(input != null){
			    try {
			    	input.close();
			    }catch (Exception f){
			    	f.printStackTrace();
			    }
			}
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		InputStream inputStream = request.getInputStream();
		String xml = getPost(inputStream);
		DBRow row = XmlUtil.initDataXmlStringToDBRow(xml,new String[]{"Account","Password"});
		String account = row.getString("account");
		String password = row.getString("password");
		
		//验证是否登陆
		boolean login = accountMgr.accountLoginValidator(account, password);
		
		JSONObject returnVal = new JSONObject();
		
		if(login){
			
			//登录后返回数据
			Map<String,DBRow[]> result = accountMgr.getAccountDeptPostAndWareArea();
			
			Set<String> key = result.keySet();
	        for (Iterator<String> it = key.iterator(); it.hasNext();) {
	        	
	            String oneKey = (String) it.next();
	            returnVal.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(result.get(oneKey)));
	        }
		}else{
			returnVal.put("error", "Login failed. Please try a different username/password.");
			//returnVal.put("error", "用户名或密码错误！");
		}
        
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnVal.toString());
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "POST":
					doPost(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}