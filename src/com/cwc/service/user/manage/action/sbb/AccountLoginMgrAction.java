package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.util.StringUtil;
import com.cwc.app.util.Config;

public class AccountLoginMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}

	public void perform(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		try {
			
			String loginType = StringUtil.getString(request, "loginType");
			String flag = StringUtil.getString(request, "isThirdWitness");
			
			//只获取AdminLoginBean
			HttpSession session = StringUtil.getSession(request);
			Object adminBean = session.getAttribute(Config.adminSesion);
			
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("adminBean", adminBean);
				
			//内部登录 - 非第三方登录
			params.put("userName", StringUtil.getString(request, "userName"));
			params.put("tokenSn", StringUtil.getString(request, "tokenSn"));
			
			//内部登录 - 第三方登录 
			//params.put("userName", StringUtil.getString(request, "userName"));
			params.put("pwd_user", StringUtil.getString(request, "pwd_user"));
			params.put("witnessUserName", StringUtil.getString(request, "witnessUserName"));
			params.put("witnessTokenSn", StringUtil.getString(request, "witnessTokenSn"));
			
			//供应商登录
			params.put("account", StringUtil.getString(request,"account"));
			params.put("pwd", StringUtil.getString(request,"pwd"));
			
			//正常登录
			//params.put("account", StringUtil.getString(request,"account"));
			//params.put("pwd", StringUtil.getString(request,"pwd"));
			params.put("licence", StringUtil.getString(request,"licence"));
			
			//3
			params.put("loginType", loginType);
			
			Map<String,Object> result = null;
			//内部登录
			if(loginType.equals("3")){
				
				if(flag.equals("1")){
					
					result = accountMgr.accountInternaLoginThird(params);
					
				} else if(flag.equals("2")){
					
					result = accountMgr.accountInternaLoginNoThird(params);
					
				}
			//供应商登录
			}else if(loginType.equals("2")){
				
				result = accountMgr.accountSupplierLogin(params);
			
			//正常登录
			} else {
				
				result = accountMgr.accountLogin(params);
			}
			
			//登录成功
			if(result != null && (boolean)result.get("success")){
				
				//记录到session
				session.setAttribute(Config.adminSesion,result.get("adminBean"));
				
				//设置记住账号
				if(StringUtil.getString(request,"remeber") != null && StringUtil.getString(request,"remeber").equals("true")){
					
					StringUtil.setCookie(response,"account",StringUtil.getString(request,"account"),60*60*24*365);
				}else{
					
					StringUtil.setCookie(response,"account",StringUtil.getString(request,"account"),0);
				}
			}
			
			//返回成功或失败信息
			JSONObject output = new JSONObject();
			output.put("success", result.get("success"));
			output.put("errorInfo", result.get("errorInfo"));
	        
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(output.toString());
			
		} catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}