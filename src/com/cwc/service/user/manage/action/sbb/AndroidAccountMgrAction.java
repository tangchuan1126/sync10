package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class AndroidAccountMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
	}
	
	public void doPut(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
		//修改账户
		DBRow accountRow = new DBRow();
		accountRow.put("adid", param.get("adid", -1L));
		accountRow.put("pwd", param.get("pwd", ""));
		accountRow.put("employe_name", param.get("name", ""));
		
		DBRow result = accountMgr.modifyAccount(accountRow,param.get("flag", "-1"));
		
		JSONObject returnResult;
		if(result==null){
			returnResult = new JSONObject().put("ret", "1").put("success", false).put("error","1");
		}else{
			returnResult = new JSONObject(DBRowUtils.dbRowAsMap(result)).put("ret", "1");
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}
	
	public void doDelete(HttpServletRequest request,HttpServletResponse response,DBRow param)throws Exception {
		
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		
		try {
			
			DBRow param = accountMgr.getRequestData(request);
			
			String android = param.get("android", "-1");
			
			switch (android) {
			
				case "GET":
					doGet(request, response, param);
					break;
				case "POST":
					doPost(request, response, param);
					break;
				case "PUT":
					doPut(request, response, param);
					break;
				case "DELETE":
					doDelete(request, response, param);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
