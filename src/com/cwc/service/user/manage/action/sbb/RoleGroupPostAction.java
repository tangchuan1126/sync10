package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

/**
 * 建立部门和职务的关系
 * @author Yuanxinyu
 *
 */
public class RoleGroupPostAction extends ActionFatherController {
	
	private RoleMgrIFaceSbb roleMgr;
	
	public void setRoleMgr(RoleMgrIFaceSbb roleMgr) {
		this.roleMgr = roleMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String id = request.getParameter("id")!=null?request.getParameter("id"):"-1";
		
		DBRow[] lefts = roleMgr.getLefts(StringUtil.getLongType(id));
		DBRow[] rights = roleMgr.getRights(StringUtil.getLongType(id));
		DBRow role = roleMgr.getRoleById(StringUtil.getLongType(id));
		
		JSONObject output = new JSONObject();
		output.put("lefts",DBRowUtils.multipleDBRowArrayAsJSON(lefts));
		output.put("rights",DBRowUtils.multipleDBRowArrayAsJSON(rights));
		output.put("role", role);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		
		DBRow returnValue = roleMgr.addGroupPost(jsonData);
		
		JSONObject returnResult;
		
		if(returnValue==null){
			returnResult = new JSONObject().put("success", false).put("error","System Error!");
		}else{
			JSONObject output = roleMgr.getGpDetail(jsonData);
			returnResult = output;
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		String adg_id = StringUtil.getString(request, "adg_id");
		String hasTitle = StringUtil.getString(request, "hasTitle");
		DBRow returnValue = roleMgr.delGroupPost(adg_id,hasTitle);
		
		JSONObject returnResult;
		
		if(returnValue==null){
			returnResult = new JSONObject().put("success", false).put("error","System Error!");
		}else{
			JSONObject output = roleMgr.getGpDetailById(Long.parseLong(adg_id));
			returnResult = output;
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}