package com.cwc.service.user.manage.action.sbb;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.fr.data.core.db.DBUtils;

public class AccountContextMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		long id = Long.parseLong(StringUtils.defaultString(request.getParameter("id"), "-1"));
		String style = StringUtil.getString(request, "style");
		
		if("customer_address".equals(style)){
			long customer_key = StringUtil.getLong(request, "customer_key");
			DBRow[] customerAddressByCustomerKey = accountMgr.getCustomerAddressByCustomerKey(customer_key);
			JSONArray multipleDBRowArrayAsJSON = DBRowUtils.multipleDBRowArrayAsJSON(customerAddressByCustomerKey);
			JSONObject jso = new JSONObject();
			jso.put("address", multipleDBRowArrayAsJSON);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(jso.toString());
			
			return;
		}
		//查询账户
		Map<String,DBRow[]> accountContext = accountMgr.getAccountContext(id);
		
		JSONObject output = new JSONObject();
		Set<String> key = accountContext.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
            output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(accountContext.get(oneKey)));
        }
        
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}