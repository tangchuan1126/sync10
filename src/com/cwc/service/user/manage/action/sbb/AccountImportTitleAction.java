package com.cwc.service.user.manage.action.sbb;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class AccountImportTitleAction extends ActionFatherController{
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	private Workbook getRemoteFile(String url,String sessionId){
		
		InputStream stream = null;
		URLConnection conn = null;
		Workbook workbook = null;
		
		try {
			
			URL realUrl = new URL(url);
			conn = realUrl.openConnection();
			conn.setRequestProperty("JSESSIONID", sessionId);
			conn.setConnectTimeout(30000);
			conn.connect();
			
			stream = conn.getInputStream();
			
			workbook = Workbook.getWorkbook(stream);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return workbook;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		String sessionId = request.getRequestedSessionId();
		String path = "http://"+request.getServerName()+request.getParameter("url");
		
		
		request.setCharacterEncoding("UTF-8");
		
		Workbook workbook = this.getRemoteFile(path,sessionId);
		
		Map<String,Object> resultMap = accountMgr.validateImportTitle(workbook);
		
		JSONObject output = new JSONObject();
		Set<String> key = resultMap.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
            
            if(resultMap.get(oneKey) instanceof DBRow[]){
            	output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON((DBRow[])resultMap.get(oneKey)));
            }else{
            	output.put(oneKey,resultMap.get(oneKey));
            }
        }
        
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		String sessionId = request.getRequestedSessionId();
		String path = "http://"+request.getServerName()+request.getParameter("url");
		
		Workbook workbook = this.getRemoteFile(path,sessionId);
		
		accountMgr.updateImportTitle(workbook);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				/*case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;*/
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}