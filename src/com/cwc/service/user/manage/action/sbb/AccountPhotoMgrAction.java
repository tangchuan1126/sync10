package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.app.util.DBRowUtils;

public class AccountPhotoMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		long adid = Integer.parseInt(request.getParameter("adid"));
		
		DBRow[] accountPhotoList = accountMgr.getAccountPhotoList(adid);
		
		JSONObject output = new JSONObject();
		
		output.put("accountPhoto",DBRowUtils.multipleDBRowArrayAsJSON(accountPhotoList));
        
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		
        response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}
	
	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow photoRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		//修改图像
		int result = accountMgr.modifyAccountPhoto(photoRow);
		
		JSONObject returnResult;
		if(result==1){
			returnResult = new JSONObject().put("success", false).put("error","此图像已删除");
		}else{
			returnResult = new JSONObject().put("success", true);
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(returnResult);
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		long id = Integer.parseInt(request.getParameter("id"));
		//删除账户图像
		accountMgr.dropAccountPhoto(id);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
			case "GET":
				doGet(request, response);
				break;
			case "POST":
				doPost(request, response);
				break;
			case "PUT":
				doPut(request, response);
				break;
			case "DELETE":
				doDelete(request, response);
				break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
