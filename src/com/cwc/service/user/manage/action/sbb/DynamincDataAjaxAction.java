package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.DynamincDataAjaxMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;

public class DynamincDataAjaxAction extends ActionFatherController  {
	
	private DynamincDataAjaxMgrIFaceSbb dynamincDataAjaxMgr;
	
	public void setDynamincDataAjaxMgr(DynamincDataAjaxMgrIFaceSbb dynamincDataAjaxMgr) {
		this.dynamincDataAjaxMgr = dynamincDataAjaxMgr;
	}

	public void perform(HttpServletRequest requset, HttpServletResponse response) throws Exception {
		
		DBRow[] result = dynamincDataAjaxMgr.getAreaByPsId(requset);
		
		throw new JsonException(new JsonObject(result));
	}
}
