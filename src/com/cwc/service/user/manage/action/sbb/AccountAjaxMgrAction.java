package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import us.monoid.json.JSONObject;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class AccountAjaxMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doDeptAjaxPost(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		long deptId = Long.valueOf(StringUtils.defaultString(request.getParameter("deptId"),"-1"));
		
		DBRow[] result = accountMgr.getPostByDeptId(deptId);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("post",DBRowUtils.dbRowArrayAsJSON(result)).toString());
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			
			String style = request.getParameter("style");
			
			switch (style) {
			
				case "deptAjaxPost":
					doDeptAjaxPost(request, response);
					break;
			}
			
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}