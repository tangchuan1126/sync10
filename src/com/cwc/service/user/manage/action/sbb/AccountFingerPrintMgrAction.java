package com.cwc.service.user.manage.action.sbb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class AccountFingerPrintMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			
			String fingerCode = request.getParameter("fingerCode");
			String adid = request.getParameter("userName");
			
			/*String fingerBase64 = request.getParameter("fingerBase64");
			
			//system.out.println(fingerBase64);
			
			String fingerCode = request.getParameter("fingerCode");
			long adid = Long.parseLong(request.getParameter("adid"));
			
			DBRow result = accountMgr.registrationFingerprint(adid,fingerCode,fingerBase64);*/
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			//response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
			
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}