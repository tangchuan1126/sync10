package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.DBRowUtils;

public class CustomerTitleRelationMgrAction extends ActionFatherController {
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1")));
		pc.setPageSize(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "10")));
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		
		//查询账户
		DBRow[] result = accountMgr.getCustomerTitleRelation(parameter);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(DBRowUtils.multipleDBRowArrayAsJSON(result).toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}