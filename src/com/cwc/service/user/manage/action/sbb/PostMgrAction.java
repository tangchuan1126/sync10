package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class PostMgrAction extends ActionFatherController {
	
	private RoleMgrIFaceSbb roleMgr;
	
	public void setRoleMgr(RoleMgrIFaceSbb roleMgr) {
		this.roleMgr = roleMgr;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1")));
		pc.setPageSize(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "1000")));
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("PageCtrl", pc);
		parameter.put("searchConditions", StringUtils.defaultString(request.getParameter("searchConditions"),""));
		
		DBRow[] postRow = roleMgr.getSearchPostList(parameter);
		
		JSONObject output = new JSONObject();
		output.put("pageCtrl", new JSONObject(pc));
		output.put("posts", DBRowUtils.multipleDBRowArrayAsJSON(postRow));
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow postRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminBean = DBRowUtils.getAdminLoginBean(session.getAttribute(Config.adminSesion));
		postRow.add("creator", adminBean.getAdid());
		postRow.add("create_time", DateUtil.NowStr());
		
		DBRow result = roleMgr.addPost(postRow);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow postRow = DBRowUtils.convertToMultipleDBRow(jsonData);
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminBean = DBRowUtils.getAdminLoginBean(session.getAttribute(Config.adminSesion));
		postRow.add("creator", adminBean.getAdid());
		postRow.add("create_time", DateUtil.NowStr());
		
		DBRow result = roleMgr.modifyPost(postRow);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		//正常应该返回修改后的model,返回其他参数会自动加到model里 ,暂时用collection.reset()解决.
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		//Map<String,String> result = roleMgr.dropRole(Long.parseLong(request.getParameter("id")));
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		//response.getWriter().print(new JSONObject(result));
		response.getWriter().print(new JSONObject());
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
