package com.cwc.service.user.manage.action.sbb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;

public class AccountExportTitleAction extends ActionFatherController{
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		Map<String,String> parameter = new HashMap<String,String>();
		parameter.put("searchConditions",StringUtils.defaultString(request.getParameter("searchConditions"),""));
		parameter.put("searchDeptment",StringUtils.defaultString(request.getParameter("searchDeptment"),""));
		parameter.put("searchPost",StringUtils.defaultString(request.getParameter("searchPost"),""));
		parameter.put("searchWarehouse",StringUtils.defaultString(request.getParameter("searchWarehouse"),""));
		parameter.put("searchArea",StringUtils.defaultString(request.getParameter("searchArea"),""));
		parameter.put("searchLocation",StringUtils.defaultString(request.getParameter("searchLocation"),""));
		parameter.put("searchFingerprint",StringUtils.defaultString(request.getParameter("searchFingerprint"),""));
		parameter.put("searchToken",StringUtils.defaultString(request.getParameter("searchToken"),""));
		parameter.put("searchPermission",StringUtils.defaultString(request.getParameter("searchPermission"),""));
		parameter.put("searchTitle",StringUtils.defaultString(request.getParameter("searchTitle"),""));
		parameter.put("searchUploadPhoto",StringUtils.defaultString(request.getParameter("searchUploadPhoto"),""));
		parameter.put("searchShield",StringUtils.defaultString(request.getParameter("searchShield"),""));
		
		String path = accountMgr.exportAccountTitleBySearchResult(parameter);
		
		DBRow result = new DBRow();
		if(!path.equals("0")){
			result.add("flag", true);
		}else{
			result.add("flag", false);
		}
		result.add("fileurl", path);
		
		throw new JsonException(new JsonObject(result));
	}
}