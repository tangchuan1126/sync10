package com.cwc.service.productLine.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class UpdateProductLineJSONAction extends ActionFatherController{
	
	private CatalogMgrIFace catalogMgr;
	
	private ProductLineMgrIFaceTJH productLineMgr;
	
	public void setProductLineMgr(ProductLineMgrIFaceTJH productLineMgr) {
		this.productLineMgr = productLineMgr;
	}
	
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception {
		
		long id = StringUtil.getLong(request, "id");
		String name = StringUtil.getString(request, "name");
		
		DBRow line = productLineMgr.getProductLineByName(name);
		
		DBRow category = catalogMgr.getDetailProductCatalogByName(name);
		
		DBRow result = new DBRow();
		
		if(category == null && line == null){
			
			result.add("availability", "available");
			
		} else if (line != null && id == line.get("id", 0)){
			
			result.add("availability", "available");
			
		} else{
			
			result.add("availability", "exists");
		}
		
		throw new JsonException(new JsonObject(result));
	}
}
