package com.cwc.service.productLine.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ModProductLineAction extends ActionFatherController{

	private ProductLineMgrIFaceTJH productLineMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		productLineMgr.modProductLine(request);
		throw new RedirectRefException();
		
	}
	public void setProductLineMgr(ProductLineMgrIFaceTJH productLineMgr) {
		this.productLineMgr = productLineMgr;
	}

	
}
