/**
 * 
 */
package com.cwc.service.customer.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.floor.api.fa.FloorCustomerMgr;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.lucene.zyj.CustomerIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class CustomerMgrAction extends ActionFatherController {
		
	private SystemConfigIFace systemConfig;
	
	private FloorCustomerMgr customerMgr;
	
	public void setCustomerMgr(FloorCustomerMgr customerMgr) {
		this.customerMgr = customerMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
//	customer_key customer_id customer_name house_address state
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {

		PageCtrl pc = new PageCtrl();
		pc.setPageNo(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1")));
		pc.setPageSize(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "10")));
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("searchConditions", StringUtils.defaultString(request.getParameter("searchConditions"),""));
		
//		parameter.put("searchCustomerId", StringUtils.defaultString(request.getParameter("searchCustomerId"),""));
//		parameter.put("searchCustomerName", StringUtils.defaultString(request.getParameter("searchCustomerName"),""));
//		parameter.put("searchStateValue", StringUtils.defaultString(request.getParameter("searchStateValue"),""));
//		parameter.put("searchCountryValue", StringUtils.defaultString(request.getParameter("searchCountryValue"),""));
		
		parameter.put("cmd", StringUtils.defaultString(request.getParameter("cmd"),""));
		
		if("search".equals(parameter.get("cmd"))
				&& !StringUtil.isBlank(parameter.get("searchConditions").toString()))
		{	
			CustomerIndexMgr customerIndexMgr = new CustomerIndexMgr();
			int page_count = systemConfig.getIntConfigValue("page_count");
			DBRow rows[] = customerIndexMgr.getSearchResults(parameter.get("searchConditions").toString(), 2, page_count, pc);
			
			for(DBRow one : rows){
				
				DBRow[] titles = customerMgr.getTitleProductByCustomer(one.get("customer_key",0));
				
				one.put("titles", titles);
				one.put("titleCnt", titles.length);
			}
			
			JSONObject output = new JSONObject();
			output.put("pageCtrl", new JSONObject(pc));
			output.put("customers", DBRowUtils.dbRowArrayAsJSON(rows));
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(output.toString());
		}
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		this.editAdminIndex("ADD", request);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		this.editAdminIndex("UPDATE", request);
	}

	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
			}
		}catch(Exception e){
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
	
private void editAdminIndex(String type, HttpServletRequest request) throws Exception{
		
		try{
			// customer_key customer_id customer_name house_address state
			JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
			DBRow customer = DBRowUtils.convertToMultipleDBRow(jsonData);
			type = type.toUpperCase();
			long customerKey = customer.get("customer_key",0l);
			String customer_id = customer.getString("customer_id");
			String customer_name = customer.getString("customer_name");
			String house_address = customer.getString("house_address");
			long state_dd = customer.get("state_dd",0l);
			String text_state = customer.getString("is_text_state");
			String state = customer.getString("state");  
			if (!text_state.equalsIgnoreCase("true")){
				DBRow dbRow3 = customerMgr.getProvinceByProId(state_dd);
				String provName = dbRow3.getString("pro_name");
				state = provName;
			}
			CustomerIndexMgr indexMgr = new CustomerIndexMgr();
			if("ADD".equals(type)){
				indexMgr.addIndex(customerKey, customer_id, customer_name, house_address, state);
			}else if("UPDATE".equals(type)){
				indexMgr.updateIndex(customerKey, customer_id, customer_name, house_address, state);
			}
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.editAdminIndex() error:"+ e);
		}
	}

}
