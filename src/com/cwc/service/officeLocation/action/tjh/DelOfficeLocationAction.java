package com.cwc.service.officeLocation.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.OfficeLocationMgrIFaceTJH;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class DelOfficeLocationAction extends ActionFatherController {

	private OfficeLocationMgrIFaceTJH officeLocationMgr;
	private MessageAlerter messageAlert;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		try 
		{
			officeLocationMgr.delOfficeLocation(request);
		} 
		catch (Exception e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","DelOfficeLocationException",""));
		}
		throw new RedirectRefException();

	}
	public void setOfficeLocationMgr(OfficeLocationMgrIFaceTJH officeLocationMgr) {
		this.officeLocationMgr = officeLocationMgr;
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

	
}
