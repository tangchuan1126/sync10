package com.cwc.service.file.action.zr;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.MalformedURLException;

import javax.imageio.ImageIO;

import org.drools.lang.DRLParser.init_key_return;
import org.springframework.expression.spel.ast.NullLiteral;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.cwc.app.util.Environment;
import com.fr.base.core.UUID;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class ImgToPdfMgr {
	public static final int height = 1054;// 一张标签图片的高度为1054
	public static final int width = 794;// 一张标签图片的宽度为794
	public static final int a4Height = 842;// 一张A4的高度为842
	public static final int a4Width = 595;// 一张A4的宽度为595
	public static final int heightofserver = 1123;
	private final String logo_path = Environment.getHome().replace("\\", "/")
			+ "WEB-INF/classes/osoLogo.jpg";;

	// private final String logo_path = "";

	/**
	 * 第一种解决方案 在不改变图片形状的同时，判断，如果h>w，则按h压缩，否则在w>h或w=h的情况下，按宽度压缩
	 *
	 * @param h
	 * @param w
	 * @return
	 */
	public int getPercent1(float h, float w) {
		// 21cm = 595磅 29.7cm = 842磅
		int p = 0;
		float p2 = 0.0f;
		if (h / w > 802f / 555f) {
			p2 = 802 / h * 100;
		} else {
			p2 = 555 / w * 100;
		}
		p = Math.round(p2);
		return p;
	}

	/**
	 * 第二种解决方案，统一按照宽度压缩 这样来的效果是，所有图片的宽度是相等的，自我认为给客户的效果是最好的
	 *
	 * @param args
	 */
	private int getPercent2(float h, float w) {
		int p = 0;
		float p2 = 0.0f;
		p2 = 530 / w * 100;
		p = Math.round(p2);
		return p;
	}

	/**
	 * 处理图片转成PDF
	 * 
	 * @param srcImages
	 *            图片数组
	 * @param OutputPdfFileName
	 * @return
	 */
	public File handleImage(BufferedImage[] srcimgs, String OutputPdfFileName) {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		if (srcimgs != null && srcimgs.length > 0) {
			try {
				PdfWriter pdfWriter = PdfWriter.getInstance(doc,
						new FileOutputStream(OutputPdfFileName));
				pdfWriter.setViewerPreferences(PdfWriter.PageLayoutOneColumn);
				doc.open();
				// BufferedImage srcimg=ImageIO.read(imageFile);
				for (int i = 0; i < srcimgs.length; i++) {
					int srcheight = srcimgs[i].getHeight();// 像素
					if (srcheight > height) {// 需要把图片分割为几张
						int result = srcheight / height;
						for (int j = 0; j < result; j++) {
							createPDFDoc(doc, srcimgs[i], j);
						}
					} else {// 直接生成pdf
						createPDFDoc(doc, srcimgs[i], 0);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				doc.close();
			}
		}
		File mOutputPdfFile = null;
		try {
			mOutputPdfFile = new File(OutputPdfFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	/**
	 * 处理图片转成PDF
	 * 
	 * @param srcPah
	 * @param OutputPdfFileName
	 * @return
	 */
	@SuppressWarnings("static-access")
	public Document handleImage(Document doc, PdfCopy copy,
			BufferedImage srcimg, PdfReader reader, boolean is,
			boolean isCountingSheet) {
		doc.open();
		try {
			PdfImportedPage page;
			PdfCopy.PageStamp stamp;
			int srcheight = srcimg.getHeight();// 像素
			int result = srcheight / height; // 要添加的页数

			// 先复制
			int oldPageNumber = reader.getNumberOfPages();
			int totalPageNumber = result + oldPageNumber;
			for (int i = 1; i <= oldPageNumber; i++) {
				page = copy.getImportedPage(reader, i);
				if (page.getPdfWriter() != null) {
					page.getPdfWriter().setFullCompression();
				}
				//
				if (is) {
					stamp = copy.createPageStamp(page);
					// add page numbers
					PdfContentByte content = stamp.getOverContent();
					ColumnText ct = new ColumnText(content);

					ct.showTextAligned(
							stamp.getOverContent(),
							Element.ALIGN_CENTER,
							new Phrase(new Chunk(String.format("page %d of %d",
									i, totalPageNumber))), 297.5f, 28, 0);
					stamp.alterContents();
				}
				copy.addPage(page);
			}
			// 再添加内容
			if (srcimg != null) {
				Document imageDocument = new Document(PageSize.A4, 20, 30, 50,
						30);
				ByteArrayOutputStream imageDocumentOutputStream = new ByteArrayOutputStream();
				PdfWriter imageDocumentWriter = null;

				try {
					if (srcheight > height) {// 需要把图片分割为几张
						for (int j = 0; j < result; j++) {
							imageDocumentWriter = PdfWriter.getInstance(
									imageDocument, imageDocumentOutputStream);
							imageDocument.open();

							BufferedImage outImg = srcimg.getSubimage(0, height
									* j, width, height);
							Image img = Image.getInstance(outImg, Color.white);
							// img.setCompressionLevel(9);
							img.setAbsolutePosition(20f, 50f);
							float heigth = img.getHeight();
							float width = img.getWidth();
							int percent = this.getPercent2(heigth, width);
							img.setAlignment(Image.MIDDLE);
							img.setAlignment(Image.TEXTWRAP);
							img.scalePercent(percent + 3);

							if (!imageDocument.add(img)) {
								throw new Exception(
										"Unable to add image to page!");
							}
							imageDocumentWriter.close();

							PdfReader imageDocumentReader = new PdfReader(
									imageDocumentOutputStream.toByteArray());

							page = copy.getImportedPage(imageDocumentReader, 1);
							if (page.getPdfWriter() != null) {
								page.getPdfWriter().setFullCompression();
							}

							stamp = copy.createPageStamp(page);
							if (is) {
								// add page numbers
								PdfContentByte content = stamp.getOverContent();
								ColumnText ct = new ColumnText(content);
								ct.showTextAligned(
										stamp.getOverContent(),
										Element.ALIGN_CENTER,
										new Phrase(new Chunk(String.format(
												"page %d of %d",
												++oldPageNumber,
												totalPageNumber))), 297.5f, 28,
										0);
								stamp.alterContents();
							}

							copy.addPage(page);

							imageDocumentReader.close();
						}
					} else {// 直接生成pdf
						imageDocumentWriter = PdfWriter.getInstance(
								imageDocument, imageDocumentOutputStream);
						imageDocument.open();
						if (imageDocument.newPage()) {

							BufferedImage outImg = srcimg.getSubimage(0, 0,
									width, height);
							Image img = Image.getInstance(outImg, Color.white);
							// img.setCompressionLevel(9);
							float heigth = img.getHeight();
							float width = img.getWidth();
							int percent = this.getPercent2(heigth, width);
							img.setAlignment(Image.MIDDLE);
							img.setAlignment(Image.TEXTWRAP);
							img.scalePercent(percent + 3);
							if (!imageDocument.add(img)) {
								throw new Exception(
										"Unable to add image to page!");
							}
							imageDocumentWriter.close();
							PdfReader imageDocumentReader = new PdfReader(
									imageDocumentOutputStream.toByteArray());

							page = copy.getImportedPage(imageDocumentReader, 1);
							if (page.getPdfWriter() != null) {
								page.getPdfWriter().setFullCompression();
							}

							stamp = copy.createPageStamp(page);

							// add page numbers
							PdfContentByte content = stamp.getOverContent();
							ColumnText ct = new ColumnText(content);
							Font font = new Font();
							font.setStyle(Font.BOLD);

							if (isCountingSheet) {
								ct.showTextAligned(stamp.getOverContent(),
										Element.ALIGN_CENTER, new Phrase(
												new Chunk("Counting Sheet",
														font)), 297.5f,
										a4Height - 32, 0);
							}
							if (is) {
								ct.showTextAligned(
										stamp.getOverContent(),
										Element.ALIGN_CENTER,
										new Phrase(new Chunk(String.format(
												"page %d of %d",
												++oldPageNumber,
												totalPageNumber))), 297.5f, 28,
										0);
							}
							stamp.alterContents();

							copy.addPage(page);

							imageDocumentReader.close();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					imageDocument.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			doc.close();
		}
		return doc;
	}

	/**
	 * 处理图片转成PDF
	 * 
	 * @param srcPah
	 * @param OutputPdfFileName
	 * @return
	 */
	public File handleImage(BufferedImage srcimg, String entryId,
			String OutputPdfFileName) {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		if (srcimg != null) {
			try {
				PdfWriter pdfWriter = PdfWriter.getInstance(doc,
						new FileOutputStream(OutputPdfFileName));
				pdfWriter.setViewerPreferences(PdfWriter.PageLayoutOneColumn);
				pdfWriter.setFullCompression();
				// pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));
				// pdfWriter.setPageEvent(new HeaderFooter());
				doc.open();
				// 创建pdf欢迎页
				// createWelcomePageOnFirst(doc,entryId,DateUtil.DateStr());

				// BufferedImage srcimg=ImageIO.read(imageFile);
				int srcheight = srcimg.getHeight();// 像素
				if (srcheight > height) {// 需要把图片分割为几张
					int result = srcheight / height;
					for (int j = 0; j < result; j++) {
						createPDFDoc(doc, srcimg, j);
					}
				} else {// 直接生成pdf
					createPDFDoc(doc, srcimg, 0);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return mOutputPdfFile;
			} finally {
				doc.close();
			}
		}
		try {
			mOutputPdfFile = new File(OutputPdfFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	public File handleHtml(String contentUrl, String OutputPdfFileName) {

		int tryTime = 0;
		File mOutputPdfFile = null;
		while (mOutputPdfFile == null && tryTime <= 5) {
			mOutputPdfFile = this.handleHtml(contentUrl, OutputPdfFileName,
					tryTime);
			tryTime++;
		}

		return mOutputPdfFile;
	}

	public File handleHtml(String contentUrl, String OutputPdfFileName,
			int tryTimes) {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		try {
			this.htmlToPdf(doc, contentUrl, OutputPdfFileName);
		} catch (Exception e) {
			System.out.println("---------------Try " + tryTimes
					+ " times-----------------");
			if (tryTimes == 5) {
				e.printStackTrace();
				System.out.println("*******************" + contentUrl
						+ "*******************");
			}
			return mOutputPdfFile;
		} finally {
			doc.close();
		}
		try {
			mOutputPdfFile = new File(OutputPdfFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	/**
	 * 处理图片转成PDF
	 * 
	 * @param srcPah
	 * @param OutputPdfFileName
	 * @return
	 */
	public File handleImageOfPdfServer(BufferedImage srcimg, String entryId,
			String OutputPdfFileName) {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		if (srcimg != null) {
			try {
				PdfWriter pdfWriter = PdfWriter.getInstance(doc,
						new FileOutputStream(OutputPdfFileName));
				pdfWriter.setViewerPreferences(PdfWriter.PageLayoutOneColumn);
				pdfWriter.setFullCompression();
				// pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));
				// pdfWriter.setPageEvent(new HeaderFooter());
				doc.open();
				// 创建pdf欢迎页
				// createWelcomePageOnFirst(doc,entryId,DateUtil.DateStr());

				// BufferedImage srcimg=ImageIO.read(imageFile);
				int srcheight = srcimg.getHeight();// 像素
				if (srcheight > heightofserver) {// 需要把图片分割为几张
					int result = srcheight / heightofserver;
					for (int j = 0; j < result; j++) {
						createPDFDocOfServer(doc, srcimg, j);
					}
				} else {// 直接生成pdf
					createPDFDocOfServer(doc, srcimg, 0);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return mOutputPdfFile;
			} finally {
				doc.close();
			}
		}
		try {
			mOutputPdfFile = new File(OutputPdfFileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	/**
	 * 画PDF
	 * 
	 * @param doc
	 * @param srcimg
	 * @param j
	 * @throws BadElementException
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void createPDFDocOfServer(Document doc, BufferedImage srcimg, int j)
			throws BadElementException, IOException, DocumentException {
		doc.newPage();
		BufferedImage outImg = srcimg.getSubimage(0, heightofserver * j, width,
				heightofserver);
		Image img = Image.getInstance(outImg, Color.white);
		float heigth = img.getHeight();
		float width = img.getWidth();
		int percent = this.getPercent2(heigth, width);
		// img.setCompressionLevel(9);
		img.setAlignment(Image.MIDDLE);
		img.setAlignment(Image.TEXTWRAP);
		img.scalePercent(percent + 3);
		doc.add(img);
	}

	/**
	 * 压缩图片
	 * 
	 * @throws Exception
	 */
	public File compressImg(String PDFPath, BufferedImage srcimg,
			float quality, boolean forceBaseline) throws Exception {
		// File jpgFile = new File(PDFPath + "temp.jpg");
		String tempFileName = UUID.randomUUID().toString() + ".jpg";
		File jpgFile = new File(PDFPath + tempFileName);
		FileOutputStream out = new FileOutputStream(jpgFile); // 输出到文件流

		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);

		JPEGEncodeParam param = JPEGCodec.getDefaultJPEGEncodeParam(srcimg);
		param.setQuality(quality, forceBaseline);

		encoder.setJPEGEncodeParam(param);
		encoder.encode(srcimg);

		out.close();

		return jpgFile;
	}

	/**
	 * 画PDF
	 * 
	 * @param doc
	 * @param srcimg
	 * @param j
	 * @throws BadElementException
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void createPDFDoc(Document doc, BufferedImage srcimg, int j)
			throws BadElementException, IOException, DocumentException {
		doc.newPage();
		BufferedImage outImg = srcimg.getSubimage(0, height * j, width, height);
		Image img = Image.getInstance(outImg, Color.white);
		float heigth = img.getHeight();
		float width = img.getWidth();
		int percent = this.getPercent2(heigth, width);
		// img.setCompressionLevel(9);
		img.setAlignment(Image.MIDDLE);
		img.setAlignment(Image.TEXTWRAP);
		img.scalePercent(percent + 3);
		doc.add(img);
	}

	/**
	 * 更新PDF
	 * 
	 * @param args
	 * @throws IOException
	 * @throws DocumentException
	 */
	public File addContentToPDF(String path, BufferedImage srcimg, boolean is,
			boolean isCountingSheet) throws DocumentException, IOException {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		// 先对文件重命名.bak
		try {
			File file = new File(path);
			file.renameTo(new File(path + ".bak"));
			File newfile = new File(path + ".bak");
			if (newfile != null && newfile.exists()) {
				PdfReader pdfReader = new PdfReader(newfile.getAbsolutePath());
				PdfCopy copy = new PdfCopy(doc, new FileOutputStream(path));
				handleImage(doc, copy, srcimg, pdfReader, is, isCountingSheet);
				pdfReader.close();
				copy.close();
				newfile.delete();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mOutputPdfFile;
		}
		try {
			mOutputPdfFile = new File(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	public File addContentToPDF(String path, String pdf2addPath, boolean is,
			boolean isCountingSheet) throws DocumentException, IOException {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		// 先对文件重命名.bak
		try {
			File file = new File(path);
			file.renameTo(new File(path + ".bak"));
			File newfile = new File(path + ".bak");
			if (newfile != null && newfile.exists()) {
				PdfImportedPage page = null;
				PdfReader pdfReader = new PdfReader(newfile.getAbsolutePath());
				PdfCopy copy = new PdfCopy(doc, new FileOutputStream(path));
				int pageNumber = pdfReader.getNumberOfPages();
				doc.open();
				for (int i = 1; i <= pageNumber; i++) {
					page = copy.getImportedPage(pdfReader, i);
					if (page.getPdfWriter() != null) {
						page.getPdfWriter().setFullCompression();
					}
					//
					copy.addPage(page);
				}
				// handleImage(doc, copy, srcimg, pdfReader, is,
				// isCountingSheet);
				pdfReader = new PdfReader(pdf2addPath);
				pageNumber = pdfReader.getNumberOfPages();
				for (int i = 1; i <= pageNumber; i++) {
					page = copy.getImportedPage(pdfReader, i);
					if (page.getPdfWriter() != null) {
						page.getPdfWriter().setFullCompression();
					}
					//
					copy.addPage(page);
				}
				pdfReader.close();
				copy.close();
				newfile.delete();
				new File(pdf2addPath).delete();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mOutputPdfFile;
		}
		try {
			mOutputPdfFile = new File(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	/**
	 * 处理图片转成PDF
	 * 
	 * @param srcPah
	 * @param OutputPdfFileName
	 * @return
	 */
	@SuppressWarnings("static-access")
	public Document handleImage(Document doc, PdfCopy copy,
			BufferedImage[] srcimgs, PdfReader reader, boolean is,
			int subHeight, int subWidth) {
		doc.open();
		try {
			PdfImportedPage page;
			PdfCopy.PageStamp stamp;
			int result = 1; // 要添加的页数

			// 先复制
			int oldPageNumber = reader.getNumberOfPages();
			int totalPageNumber = result + oldPageNumber;
			for (int i = 0; i < oldPageNumber;) {
				page = copy.getImportedPage(reader, ++i);
				if (page.getPdfWriter() != null) {
					page.getPdfWriter().setFullCompression();
				}

				if (is) {
					stamp = copy.createPageStamp(page);
					// add page numbers
					PdfContentByte content = stamp.getOverContent();
					ColumnText ct = new ColumnText(content);

					ct.showTextAligned(
							stamp.getOverContent(),
							Element.ALIGN_CENTER,
							new Phrase(new Chunk(String.format("page %d of %d",
									i, totalPageNumber))), 297.5f, 28, 0);
					stamp.alterContents();
				}
				copy.addPage(page);
			}
			// 再添加内容
			if (srcimgs != null && srcimgs.length > 0) {
				Document imageDocument = new Document(PageSize.A4, 20, 30, 50,
						30);
				ByteArrayOutputStream imageDocumentOutputStream = new ByteArrayOutputStream();
				PdfWriter imageDocumentWriter = PdfWriter.getInstance(
						imageDocument, imageDocumentOutputStream);

				imageDocument.open();
				try {
					// 直接生成一页pdf 并添加布局添加所有的图片
					if (imageDocument.newPage()) {
						for (int i = 0; i < srcimgs.length; i++) {
							Image img = Image.getInstance(srcimgs[i],
									Color.white);
							// img.setCompressionLevel(9);
							// 按照奇数 偶数 设置位置
							if (i % 2 == 0) {
								img.setAbsolutePosition(20, (i == 0 ? 1 : 0)
										* subHeight + 10);
							} else {
								img.setAbsolutePosition(20 + subWidth,
										(i == 1 ? 1 : 0) * subHeight + 10);
							}

							if (!imageDocument.add(img)) {
								throw new Exception(
										"Unable to add image to page!");
							}
						}
						imageDocument.close();
						imageDocumentWriter.close();
						PdfReader imageDocumentReader = new PdfReader(
								imageDocumentOutputStream.toByteArray());

						page = copy.getImportedPage(imageDocumentReader, 1);
						if (page.getPdfWriter() != null) {
							page.getPdfWriter().setFullCompression();
						}

						stamp = copy.createPageStamp(page);

						// add page numbers
						PdfContentByte content = stamp.getOverContent();
						ColumnText ct = new ColumnText(content);

						Font font = new Font();
						font.setStyle(Font.BOLD);

						ct.showTextAligned(stamp.getOverContent(),
								Element.ALIGN_CENTER, new Phrase(new Chunk(
										"TaskProcessing", font)), 297.5f,
								a4Height - 22, 0);

						if (is) {
							ct.showTextAligned(
									stamp.getOverContent(),
									Element.ALIGN_CENTER,
									new Phrase(new Chunk(String.format(
											"page %d of %d", ++oldPageNumber,
											totalPageNumber))), 297.5f, 28, 0);
						}
						stamp.alterContents();

						copy.addPage(page);

						imageDocumentReader.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					imageDocument.close();
					imageDocumentWriter.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			doc.close();
		}
		return doc;
	}

	/**
	 * 将task的图片追加到一张pdf页上
	 * 
	 * @param args
	 * @throws IOException
	 * @throws DocumentException
	 */
	public File addContentToPDF(String path, BufferedImage[] srcimgs,
			boolean is, int subHeight, int subWidth) throws DocumentException,
			IOException {
		Document doc = new Document(PageSize.A4, 20, 30, 50, 30);
		File mOutputPdfFile = null;
		// 先对文件重命名.bak
		try {
			File file = new File(path);
			file.renameTo(new File(path + ".bak"));
			File newfile = new File(path + ".bak");
			if (newfile != null && newfile.exists()) {
				PdfReader pdfReader = new PdfReader(newfile.getAbsolutePath());
				PdfCopy copy = new PdfCopy(doc, new FileOutputStream(path));
				handleImage(doc, copy, srcimgs, pdfReader, is, subHeight,
						subWidth);
				pdfReader.close();
				copy.close();
				newfile.delete();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return mOutputPdfFile;
		}
		try {
			mOutputPdfFile = new File(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mOutputPdfFile;
	}

	/**
	 * 创建pdf欢迎页
	 * 
	 * @param args
	 * @throws IOException
	 * @throws DocumentException
	 * @throws MalformedURLException
	 */
	public void createWelcomePageOnFirst(Document doc, String entry_id,
			String date) throws DocumentException, MalformedURLException,
			IOException {
		// HeaderFooter header=new HeaderFooter(new Phrase(), false);
		// header.setBorder(2);
		// doc.setHeader(header);

		// a table with three columns
		PdfPTable table = new PdfPTable(3);
		// we add the four remaining cells with addCell()
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.setWidths(new int[] { 83, 7, 10 });
		PdfPCell cell;

		// 1 1
		cell = new PdfPCell(new Phrase());
		cell.setBorder(0);
		table.addCell(cell);

		Chunk chunk = null;
		Font font = null;
		font = new Font();
		font.setSize(10);
		font.setStyle(Font.BOLD);
		font.setFamily(BaseFont.TIMES_ROMAN);

		// 1 2
		chunk = new Chunk("Entry:");
		chunk.setFont(font);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderColor(Color.gray);
		cell.setMinimumHeight(20);
		table.addCell(cell);

		// 1 3
		chunk = new Chunk(entry_id);
		chunk.setFont(font);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setBorderColor(Color.gray);
		table.addCell(cell);

		// 2 1
		cell = new PdfPCell(new Phrase());
		cell.setBorder(0);
		table.addCell(cell);

		// 2 2
		chunk = new Chunk("Date:");
		chunk.setFont(font);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderColor(Color.gray);
		cell.setMinimumHeight(20);
		table.addCell(cell);

		// 2 3
		chunk = new Chunk(date);
		chunk.setFont(font);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setBorderColor(Color.gray);
		table.addCell(cell);

		// 3 *
		cell = new PdfPCell(new Phrase());
		cell.setColspan(3);
		cell.setBorder(2);
		cell.setMinimumHeight(10);
		table.addCell(cell);

		Image img = Image.getInstance(logo_path);
		float heigth = img.getHeight();
		float width = img.getWidth();
		int percent = this.getPercent2(heigth, width);
		img.setAlignment(Image.MIDDLE);
		img.setAlignment(Image.TEXTWRAP);
		img.scalePercent(percent + 0);

		// 4 *
		cell = new PdfPCell(new Phrase());
		cell.setColspan(3);
		cell.setBorder(2);
		cell.addElement(img);
		table.addCell(cell);

		// 5 *
		chunk = new Chunk("www.vvme.com");
		chunk.setFont(font);
		cell = new PdfPCell(new Phrase(chunk));
		cell.setColspan(3);
		cell.setBorder(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		doc.add(table);

	}

	/**
	 * 将html转成pdf
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws DocumentException
	 */
	public void handleHtmlToPdf(String html, String invoiceTempPath)
			throws Exception {
		try {
			Document doc = new Document(PageSize.A4, 20, 30, 50, 30);

			PdfWriter.getInstance(doc, new FileOutputStream(invoiceTempPath));
			doc.open();

			HTMLWorker htmlWorker = new HTMLWorker(doc);

			htmlWorker.parse(new StringReader(html));
			doc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 把URL转换为PDF
	 * 
	 * @param outputFile
	 *            ， 示例：/data/fs/inspector/BJ20150522001.pdf
	 * @param url
	 *            ，示例：http :xxxx
	 * @return
	 * @throws Exception
	 */
	public File htmlToPdf(Document doc, String url, String outputFile)
			throws Exception {
		File outFile = new File(outputFile);
		if (!outFile.exists()) {
			outFile.getParentFile().mkdirs();
		}
		OutputStream os = new FileOutputStream(outputFile);
		ITextRenderer renderer = new ITextRenderer();

		renderer.setDocument(url);
		// renderer.setDocumentFromString(url);
		// String fontPath = PdfUtil.class.getClassLoader().getResource("/")
		// .getPath();
		// String fontPath =
		// ImgToPdfMgr.class.getClassLoader().getResource("").toString().replaceAll("file:/",
		// "")+ "simsun.ttc";

		// 解决中文支持问题
		// ITextFontResolver fontResolver = renderer.getFontResolver();
		// fontResolver.addFont(fontPath,
		// BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);

		renderer.layout();

		renderer.createPDF(os);
		os.flush();
		os.close();
		return outFile;
	}

	/*
	 * 测试
	 */
	public static void main(String[] args) throws IOException,
			DocumentException {
		ImgToPdfMgr gp = new ImgToPdfMgr();
		String pdfUrl = "c:\\test1.pdf";
		String src = "c:\\bol.jpg";
		BufferedImage srcimg = ImageIO.read(new File(src));

		BufferedImage target = new BufferedImage(a4Width / 2 - 40,
				a4Height / 2 - 20, BufferedImage.TYPE_INT_BGR);
		target.getGraphics().drawImage(srcimg, 0, 0, a4Width / 2 - 40,
				a4Height / 2 - 20, null);
		File jpgFile = new File("c:/" + "result.jpg");

		JPEGEncodeParam param = JPEGCodec.getDefaultJPEGEncodeParam(target);
		param.setQuality(1, false);

		FileOutputStream out = new FileOutputStream(jpgFile); // 输出到文件流
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(target);
		out.close();
		jpgFile.delete();

		// BufferedImage[] srcimgs=new BufferedImage[]{target};
		// gp.addContentToPDF(pdfUrl, srcimgs,false, a4Height/2,a4Width/2);

	}
}
