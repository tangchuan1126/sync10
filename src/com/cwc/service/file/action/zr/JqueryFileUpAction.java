package com.cwc.service.file.action.zr;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.imgscalr.Scalr;
 
 





import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class JqueryFileUpAction extends ActionFatherController 
{
	
	private static String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/upl_imags_tmp/";
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
			{
			//处理文件的上传.上传到临时的目录下
			//处理文件的格式,文件大小限制等等
			//文件先上传到临时文件目录  
			String upload =  ConfigBean.getStringValue("systenFolder")+"action/administrator/HandleJqueryFileAction.action";
	        ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
	        response.setContentType("application/json");
	        List<DBRow> jsons = new ArrayList<DBRow>();
	        try {
	            List<FileItem> items = uploadHandler.parseRequest(request);
	            for (FileItem item : items) {
	                if (!item.isFormField()) { 
	                		File uploadDirectory = new File(fileUploadPath);
	                		if(!uploadDirectory.exists()){
	                			uploadDirectory.mkdirs();
	                		}
	                		String fileName = item.getName().replaceAll("[\\,,&,\\=,\\?]", "");
	                        File file = new File(fileUploadPath, fileName);
	                        item.write(file);
	                        DBRow jsono = new DBRow();
	                       
	                        jsono.add("name", fileName);
	                        jsono.add("size", item.getSize());
	                        jsono.add("url", upload+"?getfile=" + fileName);
	                        jsono.add("thumbnail_url", upload+"?getthumb=" + fileName);
	                        jsono.add("delete_url", upload+"?delfile=" + fileName);
	                        jsono.add("delete_type", "GET");
	                        jsons.add(jsono);
	                }
	            }
	        } catch (FileUploadException e) {
	                throw new RuntimeException(e);
	        } catch (Exception e) {
	                throw new RuntimeException(e);
	        } finally {
	        	throw new JsonException(new JsonObject(jsons.toArray(new DBRow[jsons.size()])));
	        }
		
	}
	
	public static void main(String[] args){
		System.out.println(	"?aa&bb,cc=".replaceAll("[\\,,&,\\=,\\?]", ""));
	}
 
}
