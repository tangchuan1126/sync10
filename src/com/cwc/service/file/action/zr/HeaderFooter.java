package com.cwc.service.file.action.zr;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

public class HeaderFooter extends PdfPageEventHelper{
	String header;
    PdfTemplate total;
    @Override
	public void onCloseDocument(PdfWriter pdfwriter, Document document) {
		// TODO Auto-generated method stub
    	 ColumnText.showTextAligned(total,Element.ALIGN_LEFT,new Phrase(String.valueOf(pdfwriter.getPageNumber()-1)),2,2,0);
	}

    /**
     * Initialize one of the headers.
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(
     *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(30,16);
    }


    /**
     * Adds the header and the footer.
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
     *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onEndPage(PdfWriter writer, Document document) {
    	PdfPTable table = new PdfPTable(3);
        try{
        table.setWidths(new int[]{24,24,2});
        table.setTotalWidth(527);
        table.setLockedWidth(true);
        table.getDefaultCell().setFixedHeight(2000);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        table.addCell(String.valueOf(writer.getPageNumber()));
        table.getDefaultCell().setHorizontalAlignment(
        Element.ALIGN_RIGHT);
        table.addCell(String.format("page %d of",writer.getPageNumber()));
        PdfPCell cell = new PdfPCell(Image.getInstance(total));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        table.writeSelectedRows(0,-1,34,20,writer.getDirectContent());

        }
        catch(DocumentException de){
        throw new ExceptionConverter(de);
        }
    }

}
