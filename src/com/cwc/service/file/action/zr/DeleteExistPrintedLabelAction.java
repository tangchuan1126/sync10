package com.cwc.service.file.action.zr;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.json.types.JsonArray;

import antlr.collections.List;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.checkin.PdfUtilIFace;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DeleteExistPrintedLabelAction extends ActionFatherController {

	private PdfUtilIFace pdfUtils ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String message ="false";
		String manual = StringUtil.getString(request,"manual");
		String batch = StringUtil.getString(request, "batch");
		int flag = 0;
		
		if("true".equals(batch)){
			String data = StringUtil.getString(request, "data");
			String generateOnlyWithSign = StringUtil.getString(request, "generateIncludeWithoutSign");
			HashMap<String,String> existKey = new HashMap<String,String>();
			if(!"".equals(data)){
				JSONObject  json = JSONObject.fromObject(data);
				JSONArray arr = json.getJSONArray("list");
				//pdfUtils.batchGeneratePdf(arr, request);
				for(int i=0;i<arr.size();i++){
					JSONObject item= arr.getJSONObject(i);
					String key = item.getString("entry_id")+"-"+item.getString("detail_id");
					if(existKey.containsKey(key)){
						System.out.println("=============["+key+"] has existed================");
						continue;
					}
					
					
					if("true".equals(generateOnlyWithSign)){
						pdfUtils.createPdf(item.getString("entry_id"), item.getString("detail_id"), request, true);
					}
					else{
						pdfUtils.createPdf(item.getString("entry_id"), item.getString("detail_id"), request, false);
					}
					existKey.put(key, key);
				}
				
			}
		}
		else{
			
			//如果是手动生成则先删除原有文件
			String entryId = StringUtil.getString(request, "entry_id");
			String detailId = StringUtil.getString(request, "detail_id");
			
			
			if("true".equals(manual)){
				flag = pdfUtils.createPdf(entryId, detailId, request,true);	
			}
			else{
				
				flag = pdfUtils.createPdf(entryId, detailId, request,false);
			}
			
		}
		if(flag==1){
			message = "true";
		}
		
		
		
		DBRow result=new DBRow();
		result.add("message", message);
		throw new JsonException(new JsonObject(result));
		
		// TODO Auto-generated method stub

	}
	
	public void setPdfUtils(PdfUtilIFace pdfUtils) {
		this.pdfUtils = pdfUtils;
	}
	
}
