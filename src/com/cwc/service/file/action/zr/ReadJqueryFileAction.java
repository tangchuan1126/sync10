package com.cwc.service.file.action.zr;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.cwc.db.DBRow;


public class ReadJqueryFileAction extends ActionFatherController
{
	private static String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/upl_imags_tmp/";
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
			String upload =  ConfigBean.getStringValue("systenFolder")+"action/administrator/HandleJqueryFileAction.action";

			//根据传递回来的fileName读取临时文件夹下的file
			List<DBRow> rowsList = new ArrayList<DBRow>();
			String fileName = StringUtil.getString(request, "file_names");
			String[] fileArray = null ;
			if(fileName.trim().length() > 0 ){
				fileArray = fileName.split(",");
			}
			if(fileArray != null){
				for(String tempFileName : fileArray){
					//判断文件是不是存在。存在添加到List中
					File file = new File(fileUploadPath+tempFileName);
					if(file.exists()){
						DBRow tempRow = new DBRow();
						tempRow.add("name", tempFileName);
						tempRow.add("size", file.length());
						tempRow.add("url", upload+"?getfile=" + tempFileName);  
						tempRow.add("thumbnail_url", upload+"?getthumb=" + tempFileName);
						tempRow.add("delete_url", upload+"?delfile=" + tempFileName);
						tempRow.add("delete_type", "GET");
						rowsList.add(tempRow);
					}
				}
			}
        	throw new JsonException(new JsonObject(rowsList.toArray(new DBRow[rowsList.size()])));
 
	}

}
