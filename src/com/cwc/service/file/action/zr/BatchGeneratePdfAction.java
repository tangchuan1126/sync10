package com.cwc.service.file.action.zr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.tc.PdfGenertor;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class BatchGeneratePdfAction extends ActionFatherController {
	private static final int MAX_THREAD_NUM = 2;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		int dealNumPerThread =10;
		String data = StringUtil.getString(request, "data");
		String generateIncludeWithoutSign = StringUtil.getString(request, "generateIncludeWithoutSign");
		
		if(!"".equals(data)){
			JSONObject  json = JSONObject.fromObject(data);
			
			JSONArray arr = json.getJSONArray("list");
			HashMap<String,String> existKey = new HashMap<String,String>();
			ArrayList<JSONObject> jsonList = new ArrayList<JSONObject>();
			 for(int i=0;i<arr.size();i++){
				 JSONObject item = arr.getJSONObject(i);
				 String key = item.getString("entry_id")+"-"+item.getString("detail_id");
				 if(existKey.containsKey(key)){
					 continue;
				 }
				 existKey.put(key, key);
				 jsonList.add(item);
			 }
			 int threadNum = Math.max(1,Math.min(jsonList.size()/dealNumPerThread,MAX_THREAD_NUM));
			 dealNumPerThread = jsonList.size()/ threadNum;
			 
			 /*if(jsonList.size()%dealNumPerThread>0 ){
				 threadNum = threadNum+1;
			 }*/
			 if(threadNum * dealNumPerThread <jsonList.size()){
				 ++ dealNumPerThread; 
			 }
			 System.out.println("==========Thread number is :"+threadNum +"=============");
			 
			 ExecutorService pool = Executors.newFixedThreadPool(threadNum); 
			 List<Future<PdfGenertor>> list = new ArrayList<>();
			 List<PdfGenertor> list2 = new ArrayList<>();
			 for(int i=0;i<threadNum;i++){
				 PdfGenertor callable = new PdfGenertor(i*dealNumPerThread,dealNumPerThread,jsonList,request,generateIncludeWithoutSign);
				 list2.add(callable);
			 }
			 for(int i=0;i<list2.size();i++){
				 @SuppressWarnings("unchecked")
				Future<PdfGenertor> f = pool.submit(list2.get(i));
				list.add(f);
			 }
			 pool.shutdown(); 
			 
			 for (Future<PdfGenertor> f : list) {  
				 try{
					 f.get(); 
				 }catch(Exception ex){
					 ex.printStackTrace();
				 }
			 }
			
		}
		// TODO Auto-generated method stub

	}

}
