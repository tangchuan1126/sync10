package com.cwc.service.tranInfo.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.TranInfoMgrIFaceQLL;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddTranInfoAction extends ActionFatherController {

	private TranInfoMgrIFaceQLL tranInfoMgrQLL;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		 	
			tranInfoMgrQLL.addTranInfo(StringUtil.getLong(request, "tranID"));
			 
			throw new RedirectRefException();
	}

	public void setTranInfoMgrQLL(TranInfoMgrIFaceQLL tranInfoMgrQLL) {
		this.tranInfoMgrQLL = tranInfoMgrQLL;
	}
}
