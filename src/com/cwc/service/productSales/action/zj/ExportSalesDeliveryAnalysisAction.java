package com.cwc.service.productSales.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ExportSalesDeliveryAnalysisAction extends ActionFatherController {

	private ProductSalesMgrIFaceTJH productSalesMgrTJH;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, DoNothingException, Exception 
	{
		String path = productSalesMgrTJH.exportSalesDeliveryAnalysis(request);
		DBRow row = new DBRow();
		String basePath = "";
		if(!path.equals("0"))
		{
			row.add("canexport",true);
			basePath = "../../"+ path;
		}
		else
		{
			row.add("canexport", false);
		}
		
		row.add("fileurl",basePath);
		
		throw new JsonException(new JsonObject(row)); 
	}

	public void setProductSalesMgrTJH(ProductSalesMgrIFaceTJH productSalesMgrTJH) 
	{
		this.productSalesMgrTJH = productSalesMgrTJH;
	}

}
