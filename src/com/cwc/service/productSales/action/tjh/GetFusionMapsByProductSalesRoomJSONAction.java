package com.cwc.service.productSales.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetFusionMapsByProductSalesRoomJSONAction extends ActionFatherController{

	private ProductSalesMgrIFaceTJH productSalesMgr;
	private OrderProcessMgrIFaceTJH orderProcessMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String fusion_id = StringUtil.getString(request, "fusion_id");
		long catalog_id = StringUtil.getLong(request, "catalog_id");
		String cmd = StringUtil.getString(request, "cmd");
		long p_line_id = StringUtil.getLong(request, "p_line_id");
		long product_id = StringUtil.getLong(request, "product_id");
		String order_source = StringUtil.getString(request, "order_source");
		String monetary_unit = StringUtil.getString(request,"monetary");
		
		String start_date = StringUtil.getString(request, "start_date");
		String end_date = StringUtil.getString(request,"end_date");
		long cid = StringUtil.getLong(request,"cid");
		
		productSalesMgr.getProvinceProductSalesRommByFusionId(fusion_id,catalog_id,cmd,p_line_id,product_id,order_source,monetary_unit,start_date,end_date,cid);
		
		DBRow row = orderProcessMgr.getCountryMapFlashByFusionId(fusion_id);
		throw new JsonException(new JsonObject(row));
		
	}
	public void setProductSalesMgr(ProductSalesMgrIFaceTJH productSalesMgr) {
		this.productSalesMgr = productSalesMgr;
	}
	public void setOrderProcessMgr(OrderProcessMgrIFaceTJH orderProcessMgr) {
		this.orderProcessMgr = orderProcessMgr;
	}
	

}
