package com.cwc.service.productSales.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class StatsProductSalesAction extends ActionFatherController{

	private ProductSalesMgrIFaceTJH productSalesMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String start_date = StringUtil.getString(request, "start_date_sales");
		String end_date = StringUtil.getString(request, "end_date_sales");
		productSalesMgr.statsProductSalesByDelivery(start_date,end_date);
		DBRow cc = new DBRow();
		cc.add("result",true);
		//DBRow [] rows = productSalesMgr.getAllProductSalesByDate(start_date,end_date);
		throw new JsonException(new JsonObject(cc));
	}
	
	public void setProductSalesMgr(ProductSalesMgrIFaceTJH productSalesMgr) {
		this.productSalesMgr = productSalesMgr;
	}
	
}
