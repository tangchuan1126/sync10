package com.cwc.service.productChange.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.productChange.SameProductAndTypeException;
import com.cwc.app.iface.xj.ProductChangeMgrIFaceXJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 登记残损件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GridEditProductChangeDetailAction extends ActionFatherController 
{
	private ProductChangeMgrIFaceXJ productChangeMgrXJ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
		try 
		{  
		    String oper = StringUtil.getString(request,"oper");//grid操作
	
			if(oper.equals("edit"))
			{
				productChangeMgrXJ.modProductChangeDetail(request);
			}
			else if(oper.equals("add"))
			{
				productChangeMgrXJ.addProductChangeDetail(request);
			}
			else if(oper.equals("del"))
			{
				productChangeMgrXJ.delProductChangeDetail(request);
			}
		}
		catch(ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
		catch(SameProductAndTypeException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[同一商品和类型已存在]");
		}
		
		
	}

	public void setProductChangeMgrXJ(ProductChangeMgrIFaceXJ productChangeMgrXJ) {
		this.productChangeMgrXJ = productChangeMgrXJ;
	}

	
	
	


}
