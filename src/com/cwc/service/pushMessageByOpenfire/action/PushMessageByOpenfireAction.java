package com.cwc.service.pushMessageByOpenfire.action;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.XMPPException;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AppVersionException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zzq.PushMessageByOpenfireMgrIfaceZzq;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * @author 	Zhengziqi
 * 2014年12月18日
 *
 */
public class PushMessageByOpenfireAction extends ActionFatherController{
	
	private PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq;
	
	private static Map<String, Object> connectionStack = new HashMap<String, Object>();
	private static Map<String, Object> conversationStack = new HashMap<String, Object>();
	private static Map<String, Object> tempStack = new HashMap<String, Object>();
	private static Map<String, Object> historyStack = new HashMap<String, Object>();
	
	public static final String CONVERSATION_TYPE = "conversation";
	public static final String CONVERSATION_TYPE_NO = "1";
	public static final String PRESENCE_TYPE = "presence";
	public static final String NOTICE_TYPE = "notice";
	
	static Logger logger = Logger.getLogger("ACTION");

	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		
		try {
			com.cwc.app.iface.AdminMgrIFace adminMgr = (com.cwc.app.iface.AdminMgrIFace)MvcUtil.getBeanFromContainer("proxyAdminMgr");
			AdminLoginBean loginBean = adminMgr.getAdminLoginBean(request.getSession());
			 
			DBRow data = getRequestData(request);
			String method = data.getString("Method");
			String account = (loginBean != null && StringUtils.isNotBlank(loginBean.getAccount()))?String.valueOf(loginBean.getAccount()):data.getString("adid");
			account = account.toLowerCase();
			long adid = loginBean.getAdid();
			//TODO 
			//aquire params from property file 
			//log in, create connection.
			if(StringUtils.equals("Login", method)){
				Boolean isSuccess = this.getPushMessageByOpenfireMgrZzq().login(account, "admin", false, true, false);
				result.add("isSuccess", isSuccess);
			}
			//log out, close connection
			if(StringUtils.equals("Logout", method)){
				Boolean isSuccess = this.getPushMessageByOpenfireMgrZzq().logout(account, false);
				result.add("isSuccess", isSuccess);
			}
			//send message
			if(StringUtils.equals("SendMsg", method)){
				String chatId = data.getString("chatId");
				String msg = data.getString("msg");
				Boolean issuccess = this.getPushMessageByOpenfireMgrZzq().sendMsg(account, chatId, msg);
				result.add("issuccess", issuccess);
			}
			//aquire contacts
			if(StringUtils.equals("AquireContacts", method)){
				JSONObject dataJSONObject = this.getPushMessageByOpenfireMgrZzq().aquireContacts(account);
				result.add("data", dataJSONObject);
			}
			//receive message by round robin
			if(StringUtils.equals("RoundRobin", method)){
				String delTemp = data.getString("delTemp");
				String delHistroy = data.getString("delHistroy");
				String chatId = data.getString("chatId");
				String msgType = data.getString("msgType");
				JSONArray reply = this.getPushMessageByOpenfireMgrZzq().roundRobin(account, delTemp, delHistroy, chatId, msgType);
				result.add("data", reply);
				result.add("hasMessage", reply.size() > 0);
			}
			if(StringUtils.equals("Notice", method)){
				String chatIds = data.getString("chatIds");
				String msg = data.getString("msg");
				String msgType = data.getString("msgType");
				Boolean issuccess = this.getPushMessageByOpenfireMgrZzq().sendNotice(adid, chatIds, msg, msgType);
				//Boolean issuccess = this.getPushMessageByOpenfireMgrZzq().sysSendNotice(chatIds, msg, msgType);
				result.add("issuccess", issuccess);
			}
		} catch(CheckInEntryIsLeftException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.CheckInEntryIsLeftException ;
		}catch(DockCheckInFirstException e){
			ret = BCSKey.FAIL ;
			err = BCSKey.DockCheckInFirstException ;
		}catch (NoRecordsException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.NoRecordsException;
			}catch (AppVersionException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.AppVersionException;
		}catch (MachineUnderFindException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.LOGINERROR;
		}catch (XMPPException e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}catch (Exception e) {
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		result.add("ret", ret);
		result.add("err", err);
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getRequestData(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow ;
	}
	
	public static Map<String, Object> getConnectionStack() {
		return connectionStack;
	}

	public static void setConnectionStack(Map<String, Object> connectionStack) {
		PushMessageByOpenfireAction.connectionStack = connectionStack;
	}

	public PushMessageByOpenfireMgrIfaceZzq getPushMessageByOpenfireMgrZzq() {
		return pushMessageByOpenfireMgrZzq;
	}

	public void setPushMessageByOpenfireMgrZzq(
			PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq) {
		this.pushMessageByOpenfireMgrZzq = pushMessageByOpenfireMgrZzq;
	}

	public static Map<String, Object> getConversationStack() {
		return conversationStack;
	}

	public static void setConversationStack(Map<String, Object> conversationStack) {
		PushMessageByOpenfireAction.conversationStack = conversationStack;
	}

	public static Map<String, Object> getTempStack() {
		return tempStack;
	}

	public static void setTempStack(Map<String, Object> tempStack) {
		PushMessageByOpenfireAction.tempStack = tempStack;
	}

	public static Map<String, Object> getHistoryStack() {
		return historyStack;
	}

	public static void setHistoryStack(Map<String, Object> historyStack) {
		PushMessageByOpenfireAction.historyStack = historyStack;
	}
	
}
