package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.key.B2BOrderProductFileKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class B2BOrderProductFileFollowUpAction extends ActionFatherController
{
	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		
		int productFile = StringUtil.getInt(request, "product_file");
		String returnStr = "";
		if(B2BOrderProductFileKey.FINISH == productFile)
		{
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			returnStr = b2BOrderMgrZyj.checkB2BOrderProductFileFinish(b2b_oid);
		}
		DBRow result = new DBRow();
		if("".equals(returnStr))
		{
			result.add("flag", "success");
			b2BOrderMgrZyj.b2BOrderProductFileFollowUp(request);
		}
		else
		{
			result.add("flag", returnStr);
		}
		throw new JsonException(new JsonObject(result));
	}

	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}


}
