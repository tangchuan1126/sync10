package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class B2BOrderDownloadAction extends ActionFatherController {

	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException,
	RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db = new DBRow();;
		try 
		{
			String path = b2BOrderMgrZyj.downloadB2BOrder(request);
			String basePath = "";
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
				basePath = "../../" + path;	
			}
			else 
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",basePath);
		} 
		catch(Exception e) 
		{
			db.add("canexport",false);
		}
		
		throw new JsonException(new JsonObject(db));
	}

	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}
}
