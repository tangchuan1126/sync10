package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetB2BOrderSumVWPJsonAction extends ActionFatherController {

	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long b2b_oid = StringUtil.getLong(request,"b2b_oid");
		
		float volume  = b2BOrderMgrZyj.getB2BOrderVolume(b2b_oid);
		float weight = b2BOrderMgrZyj.getB2BOrderWeight(b2b_oid);
		double send_price = b2BOrderMgrZyj.getB2BOrderSendPrice(b2b_oid);
		
		DBRow vwp = new DBRow();
		vwp.add("volume",volume);
		vwp.add("weight",weight);
		vwp.add("send_price",send_price);
		
		throw new JsonException(new JsonObject(vwp));
	}

	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}



}
