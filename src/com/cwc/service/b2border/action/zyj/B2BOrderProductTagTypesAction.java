package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class B2BOrderProductTagTypesAction extends ActionFatherController{
	
	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		
		b2BOrderMgrZyj.saveB2BOrderProductTagFile(request);
	  	throw  new RedirectBackUrlException(); 
	}

	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}
	

}
