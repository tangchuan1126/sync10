package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class B2BOrderDetailGetJSONAction extends ActionFatherController {

	private LPTypeMgrIFaceZJ LPTypeMgrZJ;
	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow b2BOrderItem = b2BOrderMgrZyj.getB2BOrderItemById(request);
	 	
		String clpTypeIdStr = b2BOrderItem.getString("clp_type_id");
 		String CLPName = "";
 		if("0".equals(clpTypeIdStr))
 		{
 			CLPName = "无";
 		}
 		else
 		{
 			if(b2BOrderItem.get("clp_type_id",0l)!=0l)
		 	{
 				CLPName = LPTypeMgrZJ.getCLPName(b2BOrderItem.get("clp_type_id",0l));
		 	}
 		}
 		b2BOrderItem.add("clp_type",CLPName);
 	
 	
 		String blpTypeIdStr = b2BOrderItem.getString("blp_type_id");
 		String BLPName = "";
 		if("0".equals(blpTypeIdStr))
 		{
 			BLPName = "无";
 		}
 		else
 		{
 			if(b2BOrderItem.get("blp_type_id",0l)!=0l)
		 	{
 				BLPName = LPTypeMgrZJ.getBLPName(b2BOrderItem.get("blp_type_id",0l));
		 	}
 		}
	 	b2BOrderItem.add("blp_type",BLPName);
		
		throw new JsonException(new JsonObject(b2BOrderItem));
	}
	
	public void setLPTypeMgrZJ(LPTypeMgrIFaceZJ typeMgrZJ) {
		LPTypeMgrZJ = typeMgrZJ;
	}

	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}

}
