package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class B2BOrderAllProduresUpdateAction extends ActionFatherController{

	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		b2BOrderMgrZyj.updateB2BOrderProdures(request);
		
		String b2b_oid = StringUtil.getString(request,"b2b_oid");
		int isOutter = StringUtil.getInt(request, "isOutter");
		if(isOutter == 2)
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/b2b_order/b2b_order_wayout_update.html?b2b_oid="+b2b_oid+"&isOutter=2&isSubmitSuccess=2");
		else
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/b2b_order/b2b_order_wayout_update.html?b2b_oid="+b2b_oid);
		
	}


	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}
}
