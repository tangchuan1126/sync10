package com.cwc.service.b2border.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.b2b.B2BOrderDetailRepeatException;
import com.cwc.app.exception.b2b.B2BOrderDetailSamePcDiffLotNumberException;
import com.cwc.app.exception.b2b.B2BOrderSerialNumberRepeatException;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.transport.CanNotFillSerialNumberException;
import com.cwc.app.exception.transport.HadSerialNumberException;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditB2BOrderAction extends ActionFatherController {

	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作

			if(oper.equals("edit"))
			{
				b2BOrderMgrZyj.modB2BOrderItem(request);
			}
			else if(oper.equals("add"))
			{
				b2BOrderMgrZyj.addB2BOrderItem(request);
			}
			else if(oper.equals("del"))
			{
				b2BOrderMgrZyj.delB2BOrderItem(request);
			}
			
			throw new WriteOutResponseException("0");
		} 
		catch(B2BOrderDetailRepeatException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[已有此商品]");
		}
		catch(ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
		catch(B2BOrderSerialNumberRepeatException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号重复]");
		}
		catch(RepeatProductException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[交货单内已有这个商品]");
		}
		catch (HadSerialNumberException e) 
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号只能标记一个商品]");
		}
		catch (CanNotFillSerialNumberException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号只能标记一个商品]");
		}
		catch (B2BOrderDetailSamePcDiffLotNumberException e) 
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[同一商品在同一容器上（或散件）批次号应该相同]");
		}
	}
	
	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj orderMgrZyj) {
		b2BOrderMgrZyj = orderMgrZyj;
	}

}
