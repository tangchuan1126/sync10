package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class ShipTo extends ActionFatherController {
	
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		switch (request.getMethod()) {
		case "GET":
			doGet(request, response);
			break;
		case "POST":
			doPost(request, response);
			break;
		case "PUT":
			doPut(request, response);
			break;
		case "DELETE":
			doDelete(request, response);
			break;
		}
		
		
	}
	private void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow shipTo = shipToMgrZJ.updateShipTo(request);
		if(shipTo == null){
			throw new SendResponseServerCodeAndMessageException(50000,"Error while updating Ship To");
			
		}else{
			response.getWriter().print(new JsonObject(shipTo).toString());
		}
		
	}
	
	private void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		if(shipToMgrZJ.deleteShipTo(request)){
			
			response.getWriter().print(new JsonObject(new DBRow()).toString());
			
		}else{
			throw new SendResponseServerCodeAndMessageException(50000,"Error deleting ship to");
		}
		
	}
	private void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow shipTo = shipToMgrZJ.addShipTo(request);
		if(shipTo == null){
			throw new SendResponseServerCodeAndMessageException(50000,"Ship To Title already exists");
			
		}else{
			response.getWriter().print(new JsonObject(shipTo).toString());
		}
		
	}
	private void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JSONObject output = new JSONObject();
		String search_type = StringUtil.getString(request, "search_type");
		if("".equals(search_type))
		{
			PageCtrl pc = new PageCtrl();
			pc.setPageNo(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageNo"), "1")));
			pc.setPageSize(Integer.parseInt(StringUtils.defaultString(request.getParameter("pageSize"), "10")));
			
			DBRow[] shipTos = shipToMgrZJ.getAllShipToOrderByName(pc);
			DBRow[] allShipTo = shipToMgrZJ.getAllShipTo();
			for(int i=0; i< shipTos.length; i++){
				shipTos[i].add("ship_to_amount", allShipTo.length);
				int storage_type = StorageTypeKey.RETAILER;
				DBRow[] scOfShipTo = shipToMgrZJ.getStorageCatalogsOfShipTo(Long.parseLong(shipTos[i].getString("ship_to_id")), storage_type);
				shipTos[i].add("current_amount", scOfShipTo.length);
			}
			
			output = new JSONObject().put("pageCtrl", new JSONObject(pc)).put("items",
					DBRowUtils.dbRowArrayAsJSON(shipTos));
		}
		else
		{
			String path = shipToMgrZJ.exportProduct(request);
			String basePath = "";
			DBRow db = new DBRow();
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
				//basePath = "../../" + path;
			}
			else
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",path);
			output = new JSONObject(db);
		}
		response.getWriter().print(output.toString());
	}
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
}
