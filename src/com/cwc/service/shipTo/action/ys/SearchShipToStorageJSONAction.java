package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 搜索获得JSON格式的商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SearchShipToStorageJSONAction extends ActionFatherController 
{
	static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private SystemConfigIFace systemConfig;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		javapsLog.info("request: "+request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
		StringUtil.getString(request, "q") : 
		StringUtil.getString(request,"term");
		javapsLog.info("q="+q);
		String storageType = StringUtil.getString(request, "storage_type");
		String pageSize = StringUtil.getString(request, "page_size");
		try
		{
			//int suggestNum = systemConfig.getIntConfigValue("page_size");
			
			PageCtrl pc = new PageCtrl();
			if(pageSize!=""){
				pc.setPageSize(Integer.parseInt(pageSize));
			}else{
				pc.setPageSize(10000);
			}
			

			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("\\*","");
				q +="*";
				
			}
			
				
			
			DBRow orders[] = ShipToStorageIndexMgr.getInstance().getSearchResults(q,pc.getPageCount(),pc,storageType,null,0);
					//ShipToStorageIndexMgr.getInstance().mergeSearch("merge_field", q, pc);
			
			throw new JsonException(new JsonObject(orders));
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
		
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	
}
