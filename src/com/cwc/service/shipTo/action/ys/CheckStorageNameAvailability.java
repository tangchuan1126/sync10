package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class CheckStorageNameAvailability extends ActionFatherController {
	
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		DBRow[] exists = shipToMgrZJ.getStorageCatalogOfShipToByTitle(request);
		JSONObject output = new JSONObject();
		String sign = StringUtil.getString(request, "sign");
		
		
		if("edit".equals(sign)){
			if(exists.length > 0){
				long storage_cat_id = StringUtil.getLong(request, "storage_cat_id");
				long id = Long.parseLong(exists[0].getString("id"));
				if(id == storage_cat_id){
					output.put("available", "true");
				}else{
					output.put("available", "false");
				}
				
			}else{
				output.put("available", "true");
			}
		}else{
			if(exists.length == 0){
				output.put("available", "true");
			}else{
				output.put("available", "false");
			}
		}
		response.getWriter().print(output.toString());
		
	}
	
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
}
