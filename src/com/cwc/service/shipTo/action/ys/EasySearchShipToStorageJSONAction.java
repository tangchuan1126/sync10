package com.cwc.service.shipTo.action.ys;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;










import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 搜索获得JSON格式的商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class EasySearchShipToStorageJSONAction extends ActionFatherController 
{
	static Logger javapsLog = Logger.getLogger("JAVAPS");
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		
		javapsLog.info("request: "+request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
		StringUtil.getString(request, "q") : 
		StringUtil.getString(request,"term");
		javapsLog.info("q="+q);
		long shipToId = StringUtil.getLong(request,"ship_to_id");
		
		try
		{
			PageCtrl pc = new PageCtrl();
			
			pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
			pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));

			PageCtrl internalPc = new PageCtrl();
			internalPc.setPageNo(1);
			internalPc.setPageSize(10000);
			
			
			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("\\*","");
				q +="*";
				
			}
			DBRow storages[] = ShipToStorageIndexMgr.getInstance().mergeSearch("title", q, internalPc);
			if (storages.length > 0){
				if(shipToId==0){
					List<String> ids = new ArrayList<String>();
					StringBuilder sb = new StringBuilder();
					for(DBRow row : storages){
						if(!ids.contains(row.getString("storage_type_id"))){
							ids.add(row.getString("storage_type_id"));
							
							if(!"".equals(row.getString("storage_type_id"))){
								sb.append(row.getString("storage_type_id"));
								sb.append(",");
							}
						}
					}
					
					String idsList = 	storages.length>0 ? sb.substring(0, sb.toString().length()-1) : sb.toString();
					
					DBRow[] shipTos  = shipToMgrZJ.getShipToListByIds(idsList,pc);
					for(int i=0; i< shipTos.length; i++){
						int storage_type = StorageTypeKey.RETAILER;
						DBRow[] scOfShipTo = shipToMgrZJ.getStorageCatalogsOfShipTo(Long.parseLong(shipTos[i].getString("ship_to_id")), storage_type);
						shipTos[i].add("current_amount", scOfShipTo.length);
					}
					JSONObject output = new JSONObject()
					.put("pageCtrl", new JSONObject(pc)).put("items",
							DBRowUtils.dbRowArrayAsJSON(shipTos));
					response.getWriter().print(output.toString());
					
					
				}else{
					StringBuilder sb = new StringBuilder();
					boolean removeEnding=false;
					for(DBRow row : storages){
						//lujintao 修改 ：  添加了针对storage_type_id 的 非空判断
						if(row.getString("storage_type_id") != null && row.getString("storage_type_id").trim().length() > 0 && Long.parseLong(row.getString("storage_type_id"))==shipToId){
							sb.append(row.getString("id"));
							sb.append(",");
							removeEnding=true;
						}
					}
					
					String idsList = 	removeEnding ? sb.substring(0, sb.toString().length()-1) : sb.toString();
					
					DBRow[] result  = shipToMgrZJ.getShipToStorageListByIds(idsList,internalPc);
					
					response.getWriter().print(new JsonObject(result).toString());
					
					
				}
			}else{
				response.getWriter().print(new JSONObject().put("pageCtrl", new JSONObject(pc)).put("items",new DBRow[]{}).toString());
			}
			
			/*throw new JsonException(new JsonObject(orders));*/
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
		
	}
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}

//	public static void main(String[] args) {
//		String a = "60.200";
//		boolean matches = a.matches("[0-9]\\d*\\.?00*");
//		//system.out.println(matches);
//	}

	
}
