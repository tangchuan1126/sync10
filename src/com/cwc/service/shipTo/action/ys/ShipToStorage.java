package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class ShipToStorage extends ActionFatherController {
	
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		switch (request.getMethod()) {
		case "GET":
			doGet(request, response);
			break;
		case "POST":
			doPost(request, response);
			break;
		case "PUT":
			doPut(request, response);
			break;
		case "DELETE":
			doDelete(request, response);
			break;
		}
		
		
	}
	private void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String style = StringUtil.getString(request, "style");
		DBRow storageCat = new DBRow();
		if(style.equals("pause")){
			storageCat = shipToMgrZJ.updateActivePause(request,0);
		}else if(style.equals("active")){
			storageCat = shipToMgrZJ.updateActivePause(request,1);
		}else{
			storageCat = shipToMgrZJ.updateStorageCatalog(request);
		}
		
		if(storageCat == null){
			throw new SendResponseServerCodeAndMessageException(50000,"error while updating storage");
		}else{
			response.getWriter().print(new JsonObject(storageCat).toString());
		}
	}
	
	private void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if(shipToMgrZJ.deleteStorageCatalog(request)){
			response.getWriter().print(new JsonObject(new DBRow()).toString());
		}else{
			throw new SendResponseServerCodeAndMessageException(50000,"Error deleting ship to");
		}
	}
	
	private void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow shipTo = shipToMgrZJ.addStorageCatalog(request);
		if(shipTo == null){
			throw new SendResponseServerCodeAndMessageException(50000,"Storage Title already exists");
		}else{
			response.getWriter().print(new JsonObject(shipTo).toString());
		}
	}
	
	
	private void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow[] storageCatalog = shipToMgrZJ.getStorageCatalogsOfShipTo(request);
		response.getWriter().print(new JsonObject(storageCatalog).toString());
	}
	
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
}
