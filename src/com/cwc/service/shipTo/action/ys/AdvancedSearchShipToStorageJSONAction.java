package com.cwc.service.shipTo.action.ys;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;












import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 搜索获得JSON格式的商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AdvancedSearchShipToStorageJSONAction extends ActionFatherController 
{
	static Logger javapsLog = Logger.getLogger("JAVAPS");
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		
		javapsLog.info("request: "+request.getParameterMap());
		String country = StringUtil.getString(request, "country");
		String state = StringUtil.getString(request, "state");
		String shipTo = StringUtil.getString(request, "ship_to");
		String storage = StringUtil.getString(request, "storage");
		String storageType = StringUtil.getString(request, "storage_type")=="" ? String.valueOf(StorageTypeKey.RETAILER) :StringUtil.getString(request, "storage_type");
		
		long shipToId = StringUtil.getLong(request,"ship_to_id");
		
		try
		{
			PageCtrl pc = new PageCtrl();
			
			pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
			pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));
			
			DBRow filter = new DBRow();
			if(!country.equals("")&&!country.equals("\""))
			{
				filter.add("country", country);
			}
			if(!state.equals("")&&!state.equals("\""))
			{
				filter.add("state", state);
			}
			if(!shipTo.equals("")&&!shipTo.equals("\""))
			{
				filter.add("ship_to_name", shipTo);
			}
			if(!storage.equals("")&&!storage.equals("\""))
			{
				filter.add("title", storage);
			}
			if(shipToId > 0)
			{
				filter.add("ship_to_id", shipToId);
			}
			if(shipToId > 0){
				pc = null;
				filter.add("group", "");
			}else{
				filter.add("group", "GROUP BY st.ship_to_name");
			}
			DBRow[] shipToAdvanceSearch = shipToMgrZJ.ShipToAdvanceSearch(filter,pc);
			for(int i=0; i< shipToAdvanceSearch.length; i++){
				int storage_type = StorageTypeKey.RETAILER;
				DBRow[] scOfShipTo = shipToMgrZJ.getAddressBook(Long.parseLong(shipToAdvanceSearch[i].getString("ship_to_id")), storage_type, null);
				shipToAdvanceSearch[i].add("current_amount", scOfShipTo.length);
			}
			if(shipToId == 0){
				
				JSONObject output = new JSONObject();
				output = new JSONObject().put("pageCtrl", new JSONObject(pc)).put("items",
						DBRowUtils.dbRowArrayAsJSON(shipToAdvanceSearch));
				response.getWriter().print(output.toString());
			}else{
				response.getWriter().print(new JsonObject(shipToAdvanceSearch).toString());
			}
			
				
			
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
		
	}
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}

	

	
}
