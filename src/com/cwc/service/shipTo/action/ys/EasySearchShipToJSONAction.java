package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 搜索获得JSON格式的商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class EasySearchShipToJSONAction extends ActionFatherController 
{
	private ShipToMgrIFaceZJ shipToMgrZJ;
	
	static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private SystemConfigIFace systemConfig;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		
		javapsLog.info("request: "+request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
		StringUtil.getString(request, "q") : 
		StringUtil.getString(request,"term");
		javapsLog.info("q="+q);
		
		try
		{
			PageCtrl pc = new PageCtrl();
			
			pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
			pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));

			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("\\*","");
				q +="*";
				
			}
			DBRow shipTos[] = ShipToIndexMgr.getInstance().mergeSearch("ship_to_name", q, pc);
			for(int i=0; i< shipTos.length; i++){
				int storage_type = StorageTypeKey.RETAILER;
				DBRow[] scOfShipTo = shipToMgrZJ.getStorageCatalogsOfShipTo(Long.parseLong(shipTos[i].getString("ship_to_id")), storage_type);
				shipTos[i].add("current_amount", scOfShipTo.length);
			}
			JSONObject output = new JSONObject()
			.put("pageCtrl", new JSONObject(pc)).put("items",
					DBRowUtils.dbRowArrayAsJSON(shipTos));
			response.getWriter().print(output.toString());
			/*throw new JsonException(new JsonObject(orders));*/
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
		
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
}
