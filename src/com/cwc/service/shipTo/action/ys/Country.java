package com.cwc.service.shipTo.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;


public class Country extends ActionFatherController {
	
	private OrderMgrIFace orderMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		switch (request.getMethod()) {
		case "GET":
			doGet(request, response);
			break;
		}
	}
	
	private void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		DBRow[] countries = orderMgr.getAllCountryCode();
		JSONObject output = new JSONObject().put("countries",
				DBRowUtils.dbRowArrayAsJSON(countries));
		response.getWriter().print(output.toString());
	}
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	
}
