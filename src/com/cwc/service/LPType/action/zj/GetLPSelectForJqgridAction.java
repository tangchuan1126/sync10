package com.cwc.service.LPType.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zj.LPTypeMgrZJ;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetLPSelectForJqgridAction extends ActionFatherController {

	private LPTypeMgrIFaceZJ LPTypeMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long title_id = StringUtil.getLong(request,"title_id");
		String p_name = StringUtil.getString(request,"p_name");
		int lp_type = StringUtil.getInt(request,"lp_type");
		
		DBRow[] result = new DBRow[0];
		if (lp_type == ContainerTypeKey.CLP)
		{
			result = LPTypeMgrZJ.getCLPNameSelectForJqgridByPname(title_id, p_name);
		}
		/*
		else if (lp_type == ContainerTypeKey.BLP)
		{
			result = LPTypeMgrZJ.getBLPNameSelectForJqgridByPname(p_name);
		}
		else if (lp_type == ContainerTypeKey.ILP)
		{
			result = LPTypeMgrZJ.getILPNameSelectForJqgridByPname(p_name);
		}*///去掉ILP和BLP
		
		throw new JsonException(new JsonObject(result));

	}
	public void setLPTypeMgrZJ(LPTypeMgrIFaceZJ typeMgrZJ) {
		LPTypeMgrZJ = typeMgrZJ;
	}	
}
