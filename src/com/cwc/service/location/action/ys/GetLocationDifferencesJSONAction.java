package com.cwc.service.location.action.ys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetLocationDifferencesJSONAction extends ActionFatherController {

	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		
		long saa_id = StringUtil.getLong(request, "saa_id");

		 
		DBRow[] differences = storageApproveMgrZJ.getStorageApproveLocationBySaaId(saa_id);
		
		throw new JsonException(new JsonObject(differences));
	}
	public void setStorageApproveMgrZJ(StorageApproveMgrIFaceZJ storageApproveMgrZJ) {
		this.storageApproveMgrZJ = storageApproveMgrZJ;
	}



}
