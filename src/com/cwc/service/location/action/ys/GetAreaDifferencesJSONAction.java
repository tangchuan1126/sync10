package com.cwc.service.location.action.ys;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetAreaDifferencesJSONAction extends ActionFatherController {

	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		
		long ps_id = StringUtil.getLong(request, "ps_id");
		long area_id = StringUtil.getLong(request, "area_id");
		int approve_status = StringUtil.getInt(request, "approve_status");
		String sortBy = StringUtil.getString(request,"sortby","post_date");
		boolean sort = Boolean.valueOf(StringUtil.getString(request,"sort","true"));
		int p = StringUtil.getInt(request,"pageNo");
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(p);
		pc.setPageSize(10);
		
		
		DBRow[] storageApproveArea = storageApproveMgrZJ.filterStorageApproveArea(ps_id,area_id,approve_status,sortBy,sort,pc);

		JSONObject output = new JSONObject()
		.put("pageCtrl", new JSONObject(pc)).put("items",
				DBRowUtils.dbRowArrayAsJSON(storageApproveArea));
		response.getWriter().print(output.toString());
		
	}
	public void setStorageApproveMgrZJ(StorageApproveMgrIFaceZJ storageApproveMgrZJ) {
		this.storageApproveMgrZJ = storageApproveMgrZJ;
	}



}
