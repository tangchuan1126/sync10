package com.cwc.service.location.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.wp.CreateKmlMgrWp;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ModifyAreaAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	private AdminMgrIFace adminMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow data = new DBRow();
		  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		  long adgId = adminLoggerBean.getAdgid();
		  if(adgId!=10000){
			  data.add("flag", "authError");
			  throw new JsonException(new JsonObject(data));
		  }
		String psId = StringUtil.getString(request, "ps_id");
		String isChanged = StringUtil.getString(request, "isChanged");
		String name = StringUtil.getString(request, "zone_name");
		long areaId = StringUtil.getLong(request, "zone_id");
		String x = StringUtil.getString(request, "x");
		String y = StringUtil.getString(request, "y");
		String x_position = StringUtil.getString(request, "height");
		String y_position = StringUtil.getString(request, "width");
		String angle = StringUtil.getString(request, "angle");
		String latlng = "";
		try {
			//修改area
			DBRow row = new DBRow();
			row.add("area_name", name);
			if(isChanged.equals("y")){
				latlng = googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y, x_position, y_position, angle);
				row.add("x", x);
				row.add("y", y);
				row.add("height", x_position);
				row.add("width", y_position);
				row.add("angle", angle);
				row.add("latlng", latlng);
			}
			googleMapsMgrCc.updateStorageArea(areaId, row);
			data.add("flag", "true");
			data.add("latlng", latlng);
		} catch (Exception e) {
			e.printStackTrace();
			data.add("flag", "false");
			throw new JsonException(new JsonObject(data));
		}
		throw new JsonException(new JsonObject(data));
		
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
}
