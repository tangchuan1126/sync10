package com.cwc.service.location.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class UpdateKmlAction extends ActionFatherController {
	
	private CreateKmlMgrIfaceWp createKmlMgrWp;
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String psId = StringUtil.getString(request,"ps_id");
		//重新生成kml
		createKmlMgrWp.createKmlNew(psId);
		//删除无效kml
		queryKmlInfoMgrWp.deleteInvalidKml(Environment.getHome() + "upload/kml");
		DBRow data = new DBRow();
		data.add("flag", "true");
		throw new JsonException(new JsonObject(data));
	}

	public void setCreateKmlMgrWp(CreateKmlMgrIfaceWp createKmlMgrWp) {
		this.createKmlMgrWp = createKmlMgrWp;
	}

	public void setQueryKmlInfoMgrWp(QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp) {
		this.queryKmlInfoMgrWp = queryKmlInfoMgrWp;
	}

}
