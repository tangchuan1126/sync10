package com.cwc.service.location.action.cc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.lucene.util.automaton.RegExp;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.cc.GoogleMapsMgrCc;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.mongodb.util.Hash;

public class LocationAreaXmlImportAction extends ActionFatherController {
	
	private LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String fileName = StringUtil.getString(request,"fileName");
		String storageId = StringUtil.getString(request,"storageId");
		String storageTitle = StringUtil.getString(request,"storageTitle");
		String newFileName = renameFile("kml");
		List<DBRow> rows = parseExcel(fileName,newFileName,storageId,storageTitle);
		DBRow data = new DBRow();
		if(rows.size() == 1){
			String err = rows.get(0).getString("err","-");
			if(!"-".equals(err)){
				data.add("flag", "false");
				data.add("err", err);
				throw new JsonException(new JsonObject(data));
			}
		}
		//保存到 folder_from_kml表之前先做删除操作。
		locationAreaXmlImportMgrCc.truncateTableFolderFromKml();
		//存到内存表
		locationAreaXmlImportMgrCc.savePraseRusult(rows);
		 
		data.add("flag", "true");
		data.add("source", newFileName);
		throw new JsonException(new JsonObject(data));
	}
	
	/**
	 * 解析kml
	 * @param fileName
	 * @return
	 */
	public List<DBRow> parseXML(String fileName){
		List<DBRow> result = new ArrayList<DBRow>();
		try {
			String temp_url = Environment.getHome()+"upload/kml/"+fileName;
			List<DBRow> location = new ArrayList<DBRow>();
			List<DBRow> docks = new ArrayList<DBRow>();
			List<DBRow> staging = new ArrayList<DBRow>();
			
			Document doc = new SAXReader().read(new FileInputStream(new File(temp_url)));
			Element root = (Element) doc.getRootElement().elements().get(0);
			
			List<Element> folders=root.elements("Folder");
			for (Element folder : folders) {
				String folderName = folder.element("name").getTextTrim();
				//只需要Location Docks Staging
				if(folderName.indexOf("Location")!=0 && folderName.indexOf("Docks")!=0 && folderName.indexOf("Staging")!=0) continue;
				String fName = folderName.split("-")[0];//folder名称
				String area_name = folderName.split("-").length>1?folderName.split("-")[1]:"0";//area_name
				List<Element> placemarks = folder.elements("Placemark");
				
				for (Element placemark : placemarks) {
					String placemarkName = placemark.element("name").getTextTrim();
					
					DBRow row = new DBRow();
					row.add("folder_name",fName);
					row.add("area_name",area_name);
					row.add("placemark_name",placemarkName);
					row.add("source",fileName);
					result.add(row);
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		return result;
	}
	//另存KML文件
	public String saveKmlFile(String file,int source){
		try {
			InputStream input = null;
			if(source == 1){ //从Google服务器获取kml文件
				URL url = new URL(file);
				HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
				httpConnection.connect();
				input = httpConnection.getInputStream();
			}else if(source == 2){  //从上传的临时文件夹获取KML文件
				String url = Environment.getHome()+"upl_imags_tmp/"+file;
				input = new FileInputStream(new File(url));
			}else{
				return null;
			}
			String fileName = renameFile("kml");
			OutputStream out = new FileOutputStream( Environment.getHome()+"upload/kml/"+fileName);
			byte[] b = new byte[1024*1024];
			int len = -1;
			while((len = input.read(b))>-1){
				out.write(b, 0, len);
			}
			input.close();
			out.flush();
			out.close();
			return fileName;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	//获取当前时间生成的文件名
	public String renameFile(String ext){
		long time = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssS");
		return sdf.format(new Date(time))+"."+ext;
	}
	//解析xls文件
	public  List<DBRow> parseExcel(String filename,String newFileName,String storageId,String storageTitle) throws Exception{
		List<DBRow> rows = new ArrayList<DBRow>();  //返回页面显示的数据
		int currLine = 0;
		String currSheet = "";
		String path = Environment.getHome() + "upl_imags_tmp/" + filename;	
		InputStream is = new FileInputStream(path);
		try {
			Workbook rwb = Workbook.getWorkbook(is);
			Sheet rs = null;
			
			Sheet[] sheets = rwb.getSheets();
			
			String[] sheetNames = rwb.getSheetNames();
			for (String s : sheetNames) {
				if("base".equals(s.toLowerCase())){
					rs = rwb.getSheet(s);
					break;
				}
				DBRow errInfo = new DBRow();
				errInfo.add("err", "Not found base data");
				rows.clear();
				rows.add(errInfo);
				return rows; //warehouse基础数据缺少，无法计算经纬度
			}
			Document doc = DocumentHelper.createDocument();
			Element kml = doc.addElement("kml");
			kml.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
			Element document = kml.addElement("Document");  //生成kml文件
			document.addElement("name").setText(filename.substring(0,filename.lastIndexOf(".")));
			Element warehouse = document.addElement("Folder");
			warehouse.addElement("name").setText("WarehouseBase");
			
			//location单独生成一个kml
			Document doc_loc = DocumentHelper.createDocument();
			Element kml_loc = doc_loc.addElement("kml");
			kml_loc.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
			Element document_loc = kml_loc.addElement("Document");  //生成kml文件
			document_loc.addElement("name").setText(filename.substring(0,filename.lastIndexOf(".")));
			Element warehouse_loc = document_loc.addElement("Folder");
			warehouse_loc.addElement("name").setText("WarehouseBase");

			double maxX = 0;
			double maxY = 0;
			int pCount = 0;
			DBRow whRow = new DBRow();
			char[] whChar = new char[26];
			for(int i=0;i<rs.getRows();i++){  //先读出warehouse基础数据   用于计算经纬度
				currLine = i;
				currSheet = "Base";
				String type = rs.getCell(0, i).getContents().trim().toLowerCase();
				DBRow row = new DBRow();
				if("warehouse".equals(type)){
					char pName = rs.getCell(1, i).getContents().trim().toUpperCase().toCharArray()[0];
					row.add("folder_name","base");
					row.add("area_name", "warehouse");
					row.add("placemark_name",pName+"");
					row.add("ps_id",storageId);
					if(pName>='A'&&pName<='Z' || pName>='a'&&pName<='z'){
						whRow.add(pName+"",((NumberCell) (rs.getCell(2, i))).getValue()+","+((NumberCell) (rs.getCell(3, i))).getValue());
						whChar[pCount] = pName;
						row.add("x",((NumberCell) rs.getCell(2, i)).getValue());
						row.add("y",((NumberCell) rs.getCell(3, i)).getValue());
					}
					rows.add(row);
					pCount++;
				}else if("maxx".equals(type)){
					row.add("folder_name","base");
					row.add("area_name", "maxX");
					maxX = ((NumberCell) (rs.getCell(1, i))).getValue();
					row.add("placemark_name", maxX);
					row.add("ps_id",storageId);
					rows.add(row);
				}else if("maxy".equals(type)){
					row.add("folder_name","base");
					row.add("area_name", "maxY");
					maxY = ((NumberCell) (rs.getCell(1, i))).getValue();
					row.add("placemark_name", maxY);
					row.add("ps_id",storageId);
					rows.add(row);
					//break;
				}
				
			}
			if(pCount>=4 && maxX>0 && maxY>0){
				//按字母编号排序
				whChar = Arrays.copyOfRange(whChar, 0, pCount);
				Arrays.sort(whChar);
			}else{
				DBRow errInfo = new DBRow();
				errInfo.add("err", "Warehouse base data error");
				rows.clear();
				rows.add(errInfo);
				return rows; //warehouse基础数据缺少，无法计算经纬度
			}
			//将warehouse写入kml
			StringBuffer coordinates = new StringBuffer();
			for(int i=0;i<pCount;i++){
				coordinates.append(whRow.get(whChar[i]+"", "") + ",0.0 ");
			}
			coordinates.append(whRow.get(whChar[0]+"", "") + ",0.0");
			Element placemark = warehouse.addElement("Placemark");
			placemark.addElement("styleUrl").setText("#style-warehouse");
			placemark.addElement("name").setText("base");
			Element lineString = placemark.addElement("LineString");
			lineString.addElement("tessellate").setText("0");
			lineString.addElement("coordinates").setText(coordinates.toString());
			//计算缩放比例和旋转角度
			String pointA = whRow.get(whChar[0]+"", "");
			String pointB = whRow.get(whChar[1]+"", "");
			String pointD = whRow.get(whChar[pCount-1]+"", "");
			double aLng = Double.parseDouble(pointA.split(",")[0]);
			double aLat = Double.parseDouble(pointA.split(",")[1]);
			double bLng = Double.parseDouble(pointB.split(",")[0]);
			double bLat = Double.parseDouble(pointB.split(",")[1]);
			double dLng = Double.parseDouble(pointD.split(",")[0]);
			double dLat = Double.parseDouble(pointD.split(",")[1]);
			
			double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
			double radX = Math.atan((aLat-bLat)/(aLng-bLng));
			double radY = Math.atan((aLat-dLat)/(aLng-dLng));
			if(radY <= 0){
				radY += Math.PI;
			}
			radY -= Math.PI/2;
			double radN = radX - radY;
			double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
			for (Sheet s : sheets) {
				rs = s;
				currSheet = rs.getName().toLowerCase();
				for(int i=0;i<rs.getRows();i++){
					currLine = i;
					String sheetName = rs.getName().toLowerCase();
					if("base".equals(sheetName)){
						continue;
					}else if("webcam".equals(sheetName)){
						String type = rs.getCell(0, i).getContents().trim().toLowerCase();
						if("webcam".equals(type)){
							DBRow row = new DBRow();
							String camIp = rs.getCell(1,i).getContents().trim();
							String camPort = rs.getCell(2,i).getContents().trim();
							String camUser = rs.getCell(3,i).getContents().trim();
							String camPass = rs.getCell(4,i).getContents().trim();
							String x = rs.getCell(5,i).getContents().trim();
							String y = rs.getCell(6,i).getContents().trim();
							String inner_radius = rs.getCell(7,i).getContents().trim();
							String outer_radius = rs.getCell(8,i).getContents().trim();
							String s_degree = rs.getCell(9,i).getContents().trim();
							String e_degree = rs.getCell(10,i).getContents().trim();
							String latlng =googleMapsMgrCc.convertCoordinateToLatlng(storageId, x, y);
							row.add("latlng",latlng);
							row.add("ip",camIp);
							row.add("ps_id",storageId);
							row.add("port",camPort);
							row.add("user",camUser);
							row.add("password",camPass);
							row.add("x",x);
							row.add("y",y);
							row.add("area_name",inner_radius);
							row.add("placemark_name",outer_radius);
							row.add("width",s_degree);
							row.add("height",e_degree);
							row.add("folder_name","webcam");
							row.add("source",newFileName);
							rows.add(row);
						}
					}else{
						DBRow row = new DBRow();
						row.add("ps_id",storageId);
						String type = rs.getCell(0, i).getContents().trim().toLowerCase();
						String areaName = rs.getCell(1, i).getContents().trim();
						String areaNext = rs.getCell(2, i).getContents().trim();
						if("zone".equals(type)){  //原表格中area改名为zone，此处处理方法保留area写法，将area、zone统一处理为area
							type = "area";
						}
						
						if("area".equals(type) || "docks".equals(type) || "staging".equals(type) || "location".equals(type) || "parking".equals(type)){
							
							String reg = "\\d+";
							Pattern pat = Pattern.compile(reg);
							//门和停车位NAME只能用数字表示
					/*		if((("docks".equals(type) || "parking".equals(type)) && !pat.matcher(areaNext).matches())
									|| ("staging".equals(type) && !pat.matcher(areaName).matches())){
								DBRow errInfo = new DBRow();
								errInfo.add("err", "Sheet["+sheetName+"]: Line "+(i+1)+"\nDock and parking name must be number");
								rows.clear();
								rows.add(errInfo);
								return rows;
							}*/
							String dimensional = rs.getCell(3, i).getContents().trim().toUpperCase();
							int is3D = "3D".equals(dimensional) ? 1 : 0;
							
							if(rs.getCell(4, i).getType() != CellType.EMPTY && 
									rs.getCell(5, i).getType() != CellType.EMPTY && 
									rs.getCell(6, i).getType() != CellType.EMPTY && 
									rs.getCell(7, i).getType() != CellType.EMPTY
									){
								double X = Math.rint(((NumberCell) (rs.getCell(4, i))).getValue()*100)/100;
								double Y = Math.rint(((NumberCell) (rs.getCell(5, i))).getValue()*100)/100;
								double L = Math.rint(((NumberCell) (rs.getCell(6, i))).getValue()*100)/100;
								double W = Math.rint(((NumberCell) (rs.getCell(7, i))).getValue()*100)/100;
								row.add("x",X);
								row.add("y",Y);
								row.add("width",W);
								row.add("height",L);
								double angle = 0; //位置相对仓库角度
								if(rs.getColumns()>=9 && rs.getCell(8,i).getType() != CellType.EMPTY){
									angle =  Math.rint(((NumberCell) (rs.getCell(8, i))).getValue()*100)/100;
								}
								
								double[] XS = new double[]{X,X+L,X+L,X};
								double[] YS = new double[]{Y,Y,Y+W,Y+W};
								//位置相对仓库旋转处理
								if(angle != 0){
									for(int j=1; j<4; j++){
										XS[j] -= XS[0];
										YS[j] -= YS[0];
										double r = Math.toRadians(angle) + Math.atan(YS[j]/XS[j]);
										double l = Math.hypot(XS[j],YS[j]); 
										XS[j] = l*Math.cos(r) + XS[0];
										YS[j] = l*Math.sin(r) + YS[0];
										if(Double.isNaN(XS[j])){
											XS[j] = XS[0];
										}
										if(Double.isNaN(YS[j])){
											YS[j] = YS[0];
										}
									}
								}
								row.add("angle",angle);
								String cds = "";
								//仓库相对地图旋转、缩放、平移等处理
								for(int j=0; j<4; j++){
									YS[j] /= maxY/maxY_map;  //Y轴拉伸
									
									XS[j] = XS[j] + YS[j]*Math.sin(radN)*Math.cos(radN);
									YS[j] = YS[j]*Math.cos(radN);
									
									int sign = XS[j]>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
									double rad = radX + Math.atan(YS[j]/XS[j]);
									double len = Math.hypot(XS[j],YS[j]); 
									double lat = sign*len*ratio*Math.sin(rad) + aLat;
									double lng = sign*len*ratio*Math.cos(rad) + aLng;
									if(Double.isNaN(lat)){
										lat = aLat;
									}
									if(Double.isNaN(lng)){
										lng = aLng;
									}
									cds += lng +","+ lat + ",0.0 ";
								}
								cds += cds.split(" ")[0];
								
								row.add("latLng", cds);
								String pName = null;
								if("location".equals(type)){
									placemark = warehouse_loc.addElement("Placemark");
									areaNext = areaNext.replace("_", "-");
									pName = type + "_" + storageTitle + areaName + areaNext + "_" + areaNext;   //方便商品库存位置查询
								}else{
									placemark = warehouse.addElement("Placemark");
									pName = type + "_" + ("area".equals(type)?areaName:areaNext);
								}
								placemark.addElement("styleUrl").setText("#style-" + type);
								placemark.addElement("description").setText(storageId+"_"+newFileName+"_"+areaName);
								placemark.addElement("name").setText(pName);
								if("area".equals(type)){
									//渲染为线
									lineString = placemark.addElement("LineString");
									lineString.addElement("tessellate").setText("0");
									lineString.addElement("coordinates").setText(cds);
								}else{
									//渲染为多边形
									Element linearRing = placemark.addElement("Polygon").addElement("outerBoundaryIs").addElement("LinearRing");
									linearRing.addElement("tessellate").setText("0");
									linearRing.addElement("coordinates").setText(cds);
								}
							}
							
							row.add("folder_name",type);
							row.add("area_name",areaName.toUpperCase());   //area_name
							row.add("placemark_name",areaNext.toUpperCase());
							row.add("source",newFileName);
							if("area".equals(type)){
								row.add("is_three_dimensional",dimensional);   //zone对应docks
							}else{
								row.add("is_three_dimensional",is3D);
							}
					
						rows.add(row);
						}
					}
				}
			}
			//kml样式
			Map<String, String> styleMap = new HashMap<String, String>();
			//                         线，线宽，填充
			styleMap.put("warehouse", "AAAAAA,3,00AAAAAA");
			styleMap.put("area", "AAAAAA,2,0096FB3D");
			styleMap.put("location", "465079,1,FF7DFAB8");
			styleMap.put("docks", "6ED469,2,FFBFFB93");
			styleMap.put("staging", "0F6E96,1,FFB4FFFF");
			styleMap.put("parking", "6ED469,2,FFBFFB93");
			Set<String> styleKey = styleMap.keySet();
			for (String s : styleKey) {
				Element style = document.addElement("Style");
				style.addAttribute("id", "style-" + s);
				Element lineStyle = style.addElement("LineStyle");
				lineStyle.addElement("color").setText("FF" + styleMap.get(s).split(",")[0]);
				lineStyle.addElement("width").setText(styleMap.get(s).split(",")[1]+"");
				Element polyStyle = style.addElement("PolyStyle");
				polyStyle.addElement("color").setText(styleMap.get(s).split(",")[2]+"");
				polyStyle.addElement("fill").setText("1");
				polyStyle.addElement("outline").setText("1");
				style = document_loc.addElement("Style");
				style.addAttribute("id", "style-" + s);
				lineStyle = style.addElement("LineStyle");
				lineStyle.addElement("color").setText("FF" + styleMap.get(s).split(",")[0]);
				lineStyle.addElement("width").setText(styleMap.get(s).split(",")[1]+"");
				polyStyle = style.addElement("PolyStyle");
				polyStyle.addElement("color").setText(styleMap.get(s).split(",")[2]+"");
				polyStyle.addElement("fill").setText("1");
				polyStyle.addElement("outline").setText("1");
			}
			
			//保存为kml文件
			OutputFormat format = OutputFormat.createPrettyPrint();
			//location
			XMLWriter writer = new XMLWriter(new FileOutputStream(Environment.getHome() + "upl_imags_tmp/loc_" + newFileName));
			writer.write(doc_loc);
			writer.flush();
			//非location
			writer = new XMLWriter(new FileOutputStream(Environment.getHome() + "upl_imags_tmp/" + newFileName),format);
			writer.write(doc);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			DBRow errInfo = new DBRow();
			errInfo.add("err", "Can't parse data at sheet["+currSheet+"] line "+(currLine+1));
			rows.clear();
			rows.add(errInfo);
			is.close();
			return rows;
		}
		is.close();
		return rows;
	}


}


