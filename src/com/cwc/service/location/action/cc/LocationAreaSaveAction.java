package com.cwc.service.location.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class LocationAreaSaveAction extends ActionFatherController {
	
	private LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		locationAreaXmlImportMgrCc.saveLocationArea(request);
		DBRow result = new DBRow();
		result.add("flag", "true");
		throw new JsonException(new JsonObject(result));
	}
	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}
	public void setQueryKmlInfoMgrWp(QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp) {
		this.queryKmlInfoMgrWp = queryKmlInfoMgrWp;
	}
}
