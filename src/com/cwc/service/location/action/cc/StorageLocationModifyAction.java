package com.cwc.service.location.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wp.EditStorageInfoMgrIfaceWp;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class StorageLocationModifyAction extends ActionFatherController {
	
	private EditStorageInfoMgrIfaceWp editStorageInfoMgrWp;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
				DBRow data  = new DBRow();
				String positionId = editStorageInfoMgrWp.editStorageKmlLayer(request);
				if(positionId.equals("error")||positionId.equals("")){
					data.add("flag", "false");
				}else{
					data.add("flag", "true");
					data.add("position_id", positionId);
				}
			throw new JsonException(new JsonObject(data));
	}
	public void setEditStorageInfoMgrWp(
			EditStorageInfoMgrIfaceWp editStorageInfoMgrWp) {
		this.editStorageInfoMgrWp = editStorageInfoMgrWp;
	}

}
