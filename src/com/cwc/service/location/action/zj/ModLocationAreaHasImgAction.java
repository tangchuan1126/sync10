package com.cwc.service.location.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.location.AreaHasExitsException;
import com.cwc.app.exception.location.AreaNotExitsException;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

public class ModLocationAreaHasImgAction extends ActionFatherController {

	private LocationMgrIFaceZJ locationMgrZJ;
	private MessageAlerter messageAlert; 
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException,RedirectRefException,ForwardException,Forward2JspException,RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,AreaHasExitsException,AreaNotExitsException,Exception 
	{
		try 
		{
			locationMgrZJ.modLocationAreaWithImg(request);
		}
		catch (AreaHasExitsException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","AreaHasExitsException",""));
		}
		catch(AreaNotExitsException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","AreaNotExitsException",""));
		}
		throw new WriteOutResponseException("<script type=\"text/javascript\">parent.refreshWindow();</script>");
//		DBRow result = new DBRow() ;
//		result.add("flag", true);
//		throw new JsonException(new JsonObject(result));
	}
	
	public void setLocationMgrZJ(LocationMgrIFaceZJ locationMgrZJ) {
		this.locationMgrZJ = locationMgrZJ;
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
}
