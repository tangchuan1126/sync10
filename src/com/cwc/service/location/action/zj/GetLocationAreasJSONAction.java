package com.cwc.service.location.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetLocationAreasJSONAction extends ActionFatherController {

	private LocationMgrIFaceZJ locationMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		long ps_id = StringUtil.getLong(request, "ps_id");
		long title_id = StringUtil.getLong(request, "title_id");
		DBRow[] areas;
		if(title_id==0){
			areas = locationMgrZJ.getLocationAreaByPsid(ps_id);
		}else{
			areas = locationMgrZJ.getLocationAreaByPsidAndTitleId(ps_id,title_id);
		}
		
		JsonObject js = new JsonObject(areas);
		throw new JsonException(new JsonObject(areas));
	}
	public void setLocationMgrZJ(LocationMgrIFaceZJ locationMgrZJ) {
		this.locationMgrZJ = locationMgrZJ;
	}

}
