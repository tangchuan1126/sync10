package com.cwc.service.location.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.location.LocationCatalogHasExitsException;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class ModLocationCatalogAction extends ActionFatherController {

	private LocationMgrIFaceZJ locationMgrZJ;
	private MessageAlerter messageAlert; 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,LocationCatalogHasExitsException,Exception 
	{		
		try 
		{
			locationMgrZJ.modLocationCatalog(request);
		}
		catch (LocationCatalogHasExitsException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","LocationCatalogHasExitsException",""));
		}
		throw new RedirectBackUrlException();
	}
	public void setLocationMgrZJ(LocationMgrIFaceZJ locationMgrZJ) {
		this.locationMgrZJ = locationMgrZJ;
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
