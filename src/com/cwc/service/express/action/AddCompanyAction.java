package com.cwc.service.express.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 增加快递公司
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddCompanyAction extends ActionFatherController 
{
	private ExpressMgrIFace expressMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		expressMgr.addCompany(request);
		throw new RedirectRefException();
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}



}
