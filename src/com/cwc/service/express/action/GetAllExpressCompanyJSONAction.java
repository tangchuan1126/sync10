package com.cwc.service.express.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetAllExpressCompanyJSONAction extends ActionFatherController 
{
	private ExpressMgrIFace expressMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		int domestic = StringUtil.getInt(request, "domestic");
		long sc_id = StringUtil.getLong(request, "cur_sc_id");
		
		//getAllDomesticExpressCompanyByScId
		if (domestic==1)//国内
		{
			throw new JsonException(new JsonObject(expressMgr.getAllDomesticExpressCompanyByScId(sc_id,null)));
		}
		else
		{
			throw new JsonException(new JsonObject(expressMgr.getAllInternationalExpressCompany(null)));
		}
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}


}
