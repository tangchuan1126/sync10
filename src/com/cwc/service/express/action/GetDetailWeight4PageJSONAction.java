package com.cwc.service.express.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailWeight4PageJSONAction extends ActionFatherController 
{
	private ExpressMgrIFace expressMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		DBRow detailWeight = expressMgr.getDetailWeight(StringUtil.getLong(request, "sw_id"));
		DBRow detailCompany = expressMgr.getDetailCompany(detailWeight.get("sc_id", 0l));
		
		detailWeight.add("st_weight", MoneyUtil.round(detailWeight.get("st_weight",0f)*detailCompany.get("weight_rate",0f), 2));
		detailWeight.add("en_weight", MoneyUtil.round(detailWeight.get("en_weight",0f)*detailCompany.get("weight_rate",0f), 2));
		detailWeight.add("weight_step", MoneyUtil.round(detailWeight.get("weight_step",0f)*detailCompany.get("weight_rate",0f), 2));
		
		throw new JsonException(new JsonObject(detailWeight));
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}


}
