package com.cwc.service.express.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.util.MoneyUtil;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetShippingFeeAction extends ActionFatherController 
{
	private ExpressMgrIFace expressMgr;
	private SystemConfigIFace systemConfig;


	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long sc_id = StringUtil.getLong(request, "sc_id");
		float weight = StringUtil.getFloat(request, "weight");
		long ccid = StringUtil.getLong(request, "ccid");
		long pro_id = StringUtil.getLong(request, "pro_id");

		double shipping_fee;
		double shipping_feed;
		String zone,name;
		float weight_actual;

		try
		{
			//该计算只获取重量
			ShippingInfoBean shippingInfoBean = expressMgr.getShippingFee(sc_id, weight, ccid,pro_id);
			weight_actual = shippingInfoBean.getWeight();
			//
			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
			weight_actual = weight*weight_cost_coefficient;//成本重量
			
			shippingInfoBean = expressMgr.getShippingFee(sc_id, weight_actual, ccid,pro_id);
			shipping_fee = shippingInfoBean.getShippingFee();
			zone = shippingInfoBean.getZoneName();
			name = shippingInfoBean.getCompanyName();
//			weight_actual = shippingInfoBean.getWeight();

			double shipping_cost_coefficient = systemConfig.getDoubleConfigValue( "shipping_cost_coefficient" );
			shipping_feed = MoneyUtil.round(shipping_fee*shipping_cost_coefficient, 2);
		}
		catch (CountryOutSizeException e)
		{
			throw new WriteOutResponseException("CountryOutSizeException");
		}
		catch (WeightCrossException e)
		{
			throw new WriteOutResponseException("WeightCrossException");
		}
		catch (WeightOutSizeException e)
		{
			throw new WriteOutResponseException("WeightOutSizeException");
		}
		catch (ProvinceOutSizeException e)
		{
			throw new WriteOutResponseException("ProvinceOutSizeException");
		}
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		
		throw new WriteOutResponseException(String.valueOf(shipping_feed)+"|"+zone+"|"+name+"|"+weight_actual+"|"+weight+"|"+shipping_fee);
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}


}
