package com.cwc.service.maps.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetStorageRoadAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		long psId = StringUtil.getLong(request,"ps_id");
		long m = StringUtil.getLong(request,"main",0l);
		long e = StringUtil.getLong(request,"entery",0l);
		long p = StringUtil.getLong(request,"point",0l);
		DBRow road = new DBRow();
		if(m != 0){
			DBRow[] mainRoad = googleMapsMgrCc.getStorageRoadByPsid(psId);
			road.add("main", mainRoad);
		}
		if(e != 0){
			DBRow[] locRoad = googleMapsMgrCc.getLocationEnteryRoadByPsid(psId);
			road.add("loc_entery", locRoad);
		}
		if(p != 0){
			DBRow[] point = googleMapsMgrCc.getStorageRoadPointByPsid(psId);
			road.add("point", point);
		}
		
		DBRow data = new DBRow();
		data.add("flag", "true");
		data.add("road", road);
		//throw new JsonException(new JsonObject(data));
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(data));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
