package com.cwc.service.maps.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.StorageCatalogLocationMgrIfaceCc;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class QueryStorageCatalogAction extends ActionFatherController {
	
	StorageCatalogLocationMgrIfaceCc storageCatalogLocationMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String ps_id = StringUtil.getString(request,"pro_psid");
		String pro_type = StringUtil.getString(request,"pro_type");
		String pro_title = StringUtil.getString(request,"pro_title");
		String pro_lot_num = StringUtil.getString(request,"pro_lot_num");
		String pro_line = StringUtil.getString(request,"pro_line");
		String catalog_id_1 = StringUtil.getString(request,"pro_category_1");
		String catalog_id_2 = StringUtil.getString(request,"pro_category_2");
		String catalog_id_3 = StringUtil.getString(request,"pro_category_3");
		
		String pc_line_id = filterValue(pro_line); 
		String catalog_id = null;
		String code = null;
		int union_flag = Integer.parseInt(pro_type);
		String title_id = filterValue(pro_title);
		int type = ProductStatusKey.IN_STORE;
		String lot_number_id = filterValue(pro_lot_num);
		if(!"-1".equals(catalog_id_1)){
			catalog_id = catalog_id_1;
		}
		if(!"-1".equals(catalog_id_2)){
			catalog_id = catalog_id_2;
		}
		if(!"-1".equals(catalog_id_3)){
			catalog_id = catalog_id_3;
		}
		
		DBRow[] row = storageCatalogLocationMgrCc.findStorageCatalogLocation(true, pc_line_id, catalog_id, ps_id, code, union_flag, 0l, title_id, type, lot_number_id, request);
		throw new JsonException(new JsonObject(row));
	}
	
	private String filterValue(String str){
		String s = null;
		if(!"-1".equals(str)){
			s = str;
		}
		return s;
	}
	
	public void setStorageCatalogLocationMgrCc(
			StorageCatalogLocationMgrIfaceCc storageCatalogLocationMgrCc) {
		this.storageCatalogLocationMgrCc = storageCatalogLocationMgrCc;
	}

}
