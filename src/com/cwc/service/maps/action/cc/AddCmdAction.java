package com.cwc.service.maps.action.cc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.groovy.reflection.handlegen;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.xj.CarsCommandsMgrIFaceXJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddCmdAction extends ActionFatherController {
	CarsCommandsMgrIFaceXJ carsCommandsMgrXJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String interval = StringUtil.getString(request,"interval");
		String batch = StringUtil.getString(request,"batch");
		String aid = StringUtil.getString(request,"aid");
		String call = StringUtil.getString(request,"call");
		String name = StringUtil.getString(request,"name");
		Map params = new HashMap();
		params.put("interval", interval);
		params.put("batch", batch);
		carsCommandsMgrXJ.insertCmd(aid, name, call, "3", 2, params);
		DBRow data = new DBRow();
		data.add("flag","true");
		throw new JsonException(new JsonObject(data));
	}
	public void setCarsCommandsMgrXJ(CarsCommandsMgrIFaceXJ carsCommandsMgrXJ) {
		this.carsCommandsMgrXJ = carsCommandsMgrXJ;
	}
	
}
