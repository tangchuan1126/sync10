package com.cwc.service.maps.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetGeoAlarmLabelAction extends ActionFatherController {
	
	GoogleMapsMgrIfaceCc googleMapsMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		
		
		String type = StringUtil.getString(request,"type");
		int pageNo = StringUtil.getInt(request,"pageNo");
		int pageSize = StringUtil.getInt(request,"pageSize");
		String key = StringUtil.getString(request,"key");
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		
		
		DBRow[] row = null;
		DBRow condition = new DBRow();
		if("count".equals(type)){
			row = googleMapsMgrCc.getGeoAlarmLabelCount();
		}else if("geoFencing".equals(type)){
			condition.add("geotype", "3,4,5");
			row = googleMapsMgrCc.getGeoAlarmLabel(condition, pc);
		}else if("geoLine".equals(type)){
			condition.add("geotype", "2");
			row = googleMapsMgrCc.getGeoAlarmLabel(condition, pc);
		}else if("geoPoint".equals(type)){
			condition.add("geotype", "1");
			row = googleMapsMgrCc.getGeoAlarmLabel(condition, pc);
		}
		DBRow data = new DBRow();
		if(row != null){
			data.add("flag", "true");
			data.add("type", type);
			data.add("geo", row);
		}else{
			data.add("flag", "false");
		}
		throw new JsonException(new JsonObject(data));
	}
	
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
}
