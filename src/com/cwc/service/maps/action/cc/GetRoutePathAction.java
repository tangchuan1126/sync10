package com.cwc.service.maps.action.cc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.cc.GoogleMapsMgrCc;
import com.cwc.app.api.wp.QueryKmlInfoMgrWp;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetRoutePathAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long psId = StringUtil.getLong(request,"ps_id");
		int fromType = StringUtil.getInt(request,"from_type");
		String fromPosition = StringUtil.getString(request,"from_position");
		String fromLatlng = StringUtil.getString(request,"from_latlng");
		int toType = StringUtil.getInt(request,"to_type");
		String toPosition = StringUtil.getString(request,"to_position");
		String toLatlng = StringUtil.getString(request,"to_latlng");
		
		DBRow[] data = googleMapsMgrCc.getRoutePath(psId, fromType, fromPosition, fromLatlng, toType, toPosition, toLatlng);
		//throw new JsonException(new JsonObject(data));
		throw new WriteOutResponseException(StringUtil.convertDBRowArrayToJsonString(data));
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
}
