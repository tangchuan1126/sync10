package com.cwc.service.storageLocation.action.zyz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zl.StorageDoorLocationIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DeleteLoadUnloadLocationAction extends ActionFatherController{

	private StorageDoorLocationIFaceZYZ storageDoorLocationMgrZYZ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

			long locationId = StringUtil.getLong(request,"locationId");
			storageDoorLocationMgrZYZ.deleteLoadUnloadLocation(locationId);
		    
		    DBRow result = new DBRow();
		    result.add("flag", true);
		    throw new JsonException(new JsonObject(result));
		
	}
	public void setStorageDoorLocationMgrZYZ(
			StorageDoorLocationIFaceZYZ storageDoorLocationMgrZYZ) {
		this.storageDoorLocationMgrZYZ = storageDoorLocationMgrZYZ;
	}
	
	
	
}
