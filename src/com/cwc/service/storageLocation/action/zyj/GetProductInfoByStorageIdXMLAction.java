package com.cwc.service.storageLocation.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetProductInfoByStorageIdXMLAction extends ActionFatherController
{

	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	private AdminMgrIFaceZJ adminMgrZJ;
	
	private static String preXmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	private static int buffered_size = 1024 * 50 ;
	
	 /**
	  * 	machine : 表示机器的型号
	  * 	transport_id : 表示的是transport_id
	  */
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			InputStream inputRequestStream = request.getInputStream();
			int ret = BCSKey.SUCCESS;//默认成功
			int err = BCSKey.DATASIZEINCORRECTLY;
			String fc = "";
			String machine = "";
			String password  = "";
			String xml = "";
			String loginAccount = "";
			StringBuffer returnDetails = new StringBuffer();
			int postcount = 0;
			 
			xml = getPost(inputRequestStream);
			fc = StringUtil.getSampleNode(xml,"fc");
			machine = StringUtil.getSampleNode(xml,"Machine");
			loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			password = StringUtil.getSampleNode(xml,"Password");
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			createFile(machine);
	  
			byte[] buffer = new byte[1024 * 1024];
			int length = -1 ;
			FileInputStream  inputStream = new FileInputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+".zip"));
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode(machine+".zip","utf-8"));  

			
			OutputStream outputStream =  response.getOutputStream();
			while((length = inputStream.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			inputStream.close();
			outputStream.flush();
			outputStream.close(); 
	}
	
	/**
	 * 生成product.xml
	 * @param storage_id
	 * <Details>
			<Name>WEDDINGDRESS/W92/WHITE/US12</Name>
			<PID>176098</PID>
			<Length>430.0</Length>
			<Width>380.0</Width>
			<Heigth>120.0</Heigth>
			<Weight>2.0</Weight>
		</Details>
	 */
	private String createProductXml() throws Exception
	{
		StringBuffer productXml = new StringBuffer(preXmlString);
		try
		{
			DBRow[] result = storageCatalogMgrZyj.getProductInfoByStorageId();
			
			if (result != null && result.length > 0)
			{
				for (DBRow temp : result)
				{
					productXml.append(createProductXmlItem(temp));
				}
			}
			//system.out.println("productCount:"+result.length);
			return productXml.toString();
		}
		catch (Exception e)
		{
			throw new Exception(""+e);
		}
 
	}
	private String createProductXmlItem(DBRow row ){
		StringBuffer sb = new StringBuffer("");
		sb.append("<Details>");
		sb.append("<Name>").append(row.getString("p_name")).append("</Name>");
		sb.append("<PID>").append(row.get("pc_id", 0l)).append("</PID>");
		sb.append("<Length>").append(row.get("length", 0.0f)).append("</Length>");
		sb.append("<Width>").append(row.get("width", 0.0f)).append("</Width>");
		sb.append("<Heigth>").append(row.get("heigth", 0.0f)).append("</Heigth>");
		sb.append("<Weight>").append(row.get("weight", 0.0f)).append("</Weight>");
		sb.append("</Details>");
		return sb.toString();
	}
	
	/**
	 * 生成productCode.xml
	 * <Details>
			<PCID>10000</PCID>
			<Barcode>B/H1/43K</Barcode>
			<Show>1</Show>
		</Details>
		一个PCID 要对应创建一个Barcode的PCID
	 * @param transport_id
	 */
	private String createProductCodeXml() {
		StringBuffer productCodeXml = new StringBuffer(preXmlString);
		try
		{
			
			DBRow[] result = storageCatalogMgrZyj.getProductCodeInfoByStorageId();
			
 			if(result != null && result.length > 0){
				for(DBRow row : result){
					productCodeXml.append(createProductCodeXmlItem(row));
				}
				//处理一个PCID 要对应创建一个Barcode的PCID情况
			}
// 			//system.out.println("productCodeCount:"+result.length);
		}
		catch (Exception e) 
		{
			
 		}
 		return productCodeXml.toString();
	}
	private String createProductCodeXmlItem(DBRow row) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<Details>");
		sb.append("<PCID>").append(row.get("pc_id", 0l)).append("</PCID>");
		sb.append("<Barcode>").append(row.getString("p_code")).append("</Barcode>");
		sb.append("<CodeType>").append(row.get("code_type",0)).append("</CodeType>");
		sb.append("<PcName>").append(row.getString("p_name")).append("</PcName>");
		sb.append("</Details>");
		return sb.toString();
	}
	
	
	/**
	 * 生成zip文件
	 * 	1.首先是生成.xml文件
	 * 	2.然后把.xml文件压缩成.zip文件
	 * @param machine
	 */
	private void createFile(String machine) throws Exception {
//		String productXml =	createProductXml();
		String productCodeXml =	createProductCodeXml();
		
		ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+".zip")));	 
 		
//		ZipEntry productXmlEntry = new ZipEntry("storageProducts.xml");
// 		zipOutputStream.putNextEntry(productXmlEntry);
// 		zipOutputStream.write(productXml.getBytes());
 		
 		ZipEntry productCodeXmlEntry = new ZipEntry("storageProductCodes.xml");
 		zipOutputStream.putNextEntry(productCodeXmlEntry);
 		zipOutputStream.write(productCodeXml.getBytes());
 		
 		zipOutputStream.flush();
 		zipOutputStream.close();
	}
	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1){
			    streamXML.write(buffer, 0, count);
			} 
		 }catch (Exception e){
			e.printStackTrace();
		 }finally{
			if(input != null){
			    try {
			     input.close();
			    } catch (Exception f) {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		return (new String(iXMLData));
	}

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj)
	{
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ)
	{
		this.adminMgrZJ = adminMgrZJ;
	}

	
	
}
