package com.cwc.service.storageLocation.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.StorageDoorLocationIFaceZYZ;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.LoadUnloadOccupancyStatusKey;
import com.cwc.app.key.LoadUnloadOccupancyTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetDoorCanChooseOrAllByStorageToXMLAction extends ActionFatherController
{
	static Logger log = Logger.getLogger("ACTION");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj;
	private AdminMgrIFaceZJ adminMgrZJ;
	private StorageDoorLocationIFaceZYZ storageDoorLocationMgrZYZ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
		String loginAccount = "";
		String password = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow[] doorOccupancys = new DBRow[0];
		long ps_id = 0;
		String start_time = "";
		String end_time = "";
		
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
			password		= StringUtil.getSampleNode(xml,"Password");
			start_time		= StringUtil.getSampleNode(xml, "start_time");
			end_time		= StringUtil.getSampleNode(xml, "end_time");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			ps_id = login.get("ps_id",0l);
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			//system.out.println("start_time:"+start_time+",end_time:"+end_time);
			//system.out.println("--------------------------------------------");
			
			if(isStrNullOrNull(start_time) || isStrNullOrNull(end_time))
			{
				doorOccupancys = storageDoorLocationMgrZYZ.getSearchStorageDoor(null,ps_id,null);
			}
			else
			{
				doorOccupancys = doorOrLocationOccupancyMgrZyj.getBookDoorInfoCanChoose(start_time, end_time, ps_id, null);
			}
			//system.out.println("doorOccupancys:"+doorOccupancys.length);
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(doorOccupancys, response, ret, err);
		}
		
	}
	
	
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow[] doorOccupancys, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
//		sb.append("<doorOccupancys>");
		sb.append(getdoorOccupancysToString(doorOccupancys));
//		sb.append("</doorOccupancys>");
//		//system.out.println(sb.toString());
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
	private String getdoorOccupancysToString(DBRow[] doorOccupancys)throws Exception{
		
		StringBuffer sb = new StringBuffer();
		try
		{
			if(null != doorOccupancys)
			{
				for (int k = 0; k < doorOccupancys.length; k++)
				{
					DBRow doorOccupancy = doorOccupancys[k];
					sb.append("<doorOccupancy>");
					sb.append("<sd_id>"+doorOccupancy.get("sd_id", 0L)+"</sd_id>");
					sb.append("<doorId>"+doorOccupancy.getString("doorId")+"</doorId>");
					sb.append("<occupancy_type>"+LoadUnloadOccupancyTypeKey.DOOR+"</occupancy_type>");
					
					DBRow[] locationOccupancys = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR,doorOccupancy.get("sd_id",0L),
  							ProductStoreBillKey.TRANSPORT_ORDER,0 , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,0,1,0);
  					if(locationOccupancys.length > 0)
  					{
  						sb.append("<occupancyInfos>");
  						String sbSub = "";
  						for(int j = 0; j < locationOccupancys.length; j ++)
  						{
  							
  							sbSub += locationOccupancys[j].getString("book_start_time")+","
  									+locationOccupancys[j].getString("book_end_time")+"|";
  						}
  						sb.append(sbSub);
  						sb.append("</occupancyInfos>");
  					}
					sb.append("</doorOccupancy>");
				}
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	private boolean isStrNullOrNull(String str)
	{
		if(null == str || "null".equals(str) || "".equals(str))
		{
			return true;
		}
		return false;
	}

	public void setDoorOrLocationOccupancyMgrZyj(
			DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj)
	{
		this.doorOrLocationOccupancyMgrZyj = doorOrLocationOccupancyMgrZyj;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ)
	{
		this.adminMgrZJ = adminMgrZJ;
	}


	public void setStorageDoorLocationMgrZYZ(
			StorageDoorLocationIFaceZYZ storageDoorLocationMgrZYZ)
	{
		this.storageDoorLocationMgrZYZ = storageDoorLocationMgrZYZ;
	}



}
