package com.cwc.service.storageLocation.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetStroageByTypeXMLAction extends ActionFatherController{

	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	private AdminMgrIFaceZJ adminMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {


		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
		String loginAccount = "";
		String password = "";
		String stroageTypeId = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow[] storages = new DBRow[0];
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
			password		= StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			stroageTypeId	= StringUtil.getSampleNode(xml, "stroageTypeId");
			int typeId		= "".equals(stroageTypeId)?-1:Integer.parseInt(stroageTypeId);
			
			storages = storageCatalogMgrZyj.getProductStorageCatalogByType(typeId, null);
			
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(storages, response, ret, err);
		}
	}
	
	
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private void responseXML(DBRow[] storages, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<product_storage_catalogs>");
		sb.append(storagesArrayToString(storages));
		sb.append("</product_storage_catalogs>");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

	private String storagesArrayToString(DBRow[] storages) throws Exception
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			for (int i = 0; i < storages.length; i++)
			{
				sb.append("<product_storage_catalog id=\""+storages[i].get("id", 0L)+"\">");
				sb.append("<title>"+storages[i].getString("title")+" </title>");
				sb.append("<contact>"+storages[i].getString("contact")+" </contact>");
				sb.append("<phone>"+storages[i].getString("phone")+"</phone>");
				sb.append("<native>"+storages[i].get("native", 0L)+"</native>");
				sb.append("<storage_type>"+storages[i].get("storage_type", 0)+"</storage_type>");
				sb.append("<pro_id>"+storages[i].get("pro_id", 0L)+"</pro_id>");
				sb.append("<city>"+storages[i].getString("city")+" </city>");
				sb.append("<freight_coefficient>"+storages[i].get("freight_coefficient", 0)+"</freight_coefficient>");
				
				sb.append("<address>"+storages[i].getString("address")+" </address>");
				sb.append("<send_nation>"+storages[i].get("send_nation", 0L)+"</send_nation>");
				sb.append("<send_pro_id>"+storages[i].get("send_pro_id", 0L)+"</send_pro_id>");
				sb.append("<send_city>"+storages[i].getString("send_city")+" </send_city>");
				sb.append("<send_house_number>"+storages[i].getString("send_house_number")+" </send_house_number>");
				sb.append("<send_street>"+storages[i].getString("send_street")+" </send_street>");
				sb.append("<send_zip_code>"+storages[i].getString("send_zip_code")+"</send_zip_code>");
				sb.append("<send_contact>"+storages[i].getString("send_contact")+" </send_contact>");
				sb.append("<send_phone>"+storages[i].getString("send_phone")+"</send_phone>");
				sb.append("<send_pro_input>"+storages[i].getString("send_pro_input")+" </send_pro_input>");
				   
				sb.append("<deliver_address>"+storages[i].getString("deliver_address")+" </deliver_address>");
				sb.append("<deliver_nation>"+storages[i].get("deliver_nation", 0L)+"</deliver_nation>");
				sb.append("<deliver_pro_id>"+storages[i].get("deliver_pro_id", 0L)+"</deliver_pro_id>");
				sb.append("<deliver_city>"+storages[i].getString("deliver_city")+" </deliver_city>");
				sb.append("<deliver_house_number>"+storages[i].getString("deliver_house_number")+" </deliver_house_number>");
				sb.append("<deliver_street>"+storages[i].getString("deliver_street")+" </deliver_street>");
				sb.append("<deliver_zip_code>"+storages[i].getString("deliver_zip_code")+"</deliver_zip_code>");
				sb.append("<deliver_contact>"+storages[i].getString("deliver_contact")+" </deliver_contact>");
				sb.append("<deliver_phone>"+storages[i].getString("deliver_phone")+"</deliver_phone>");
				sb.append("<deliver_pro_input>"+storages[i].getString("deliver_pro_input")+" </deliver_pro_input>");
				
				sb.append("</product_storage_catalog>");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}
	

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj) {
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}


	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

}
