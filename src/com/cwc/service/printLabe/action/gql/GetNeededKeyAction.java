package com.cwc.service.printLabe.action.gql;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gql.TaskAndInvoiceMgrIfaceGql;
import com.cwc.app.key.PaymentMethodKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;
/***
 * 取key值的通用类
 * @author Administrator
 *
 */
public class GetNeededKeyAction extends ActionFatherController{
	
	private TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql;
	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		    String key=StringUtil.getString(request, "key");
		    this.setResponse(response);
		    if(!StringUtil.isBlank(key)){
		    	DBRow[] list=null;
		    	switch (key) {
				case "PaymentMethodKey":
					PaymentMethodKey paymentMethodKey=new PaymentMethodKey();
					ArrayList<String> payMentList=  paymentMethodKey.getStatus();
					String value=null;
					list=new DBRow[payMentList.size()];int i=0;
					DBRow row=null;
					for (String name : payMentList) {
						value=paymentMethodKey.getStatusById(Integer.parseInt(name));
						row=new DBRow();
						row.add("ID", name);
						row.add("TEXT", value);
						list[i++]=row;
					}
					
					response.getWriter().print(new JSONArray(list));
					break;
				case "GetAccountInfoList":
					taskAndInvoiceMgrIfaceGql.getAccountInfoList(request, response);
					break;
				case "GetAllCountryCode":
					taskAndInvoiceMgrIfaceGql.getAllCountryCode(request, response);
					break;
				case "GetStorageProvinceByCcid":
					taskAndInvoiceMgrIfaceGql.getStorageProvinceByCcid(request, response);
					break;	
				case "getAminInformation":
					taskAndInvoiceMgrIfaceGql.getAdminInformation(request, response);
					break;
				case "getConfigValue":
					taskAndInvoiceMgrIfaceGql.getConfigValue(request, response);
					break;
				case "getAddressInfoList":
					taskAndInvoiceMgrIfaceGql.getAllCustomerId(request, response);
					break;	
				case "getAddressInfoListByClientId":
					taskAndInvoiceMgrIfaceGql.getAddressInfoListByClientId(request, response);
					break;
				case "getScheduleResultByIndex":
					taskAndInvoiceMgrIfaceGql.getSearchResultByIndex(request, response, true);
					break;	
				case "getInvoiceResultByIndex":
					taskAndInvoiceMgrIfaceGql.getSearchResultByIndex(request, response, false);
					break;	
			    case "getAllUserInformation":
					taskAndInvoiceMgrIfaceGql.getAllUserInformationAction(request, response);
					break;	
			    case "exportPdf":
					taskAndInvoiceMgrIfaceGql.exportPdf(request, response);
					break;	
			    case "sortSubScheduleNumber":
					taskAndInvoiceMgrIfaceGql.sortSubScheduleNumber(request, response);
					break;
			    case "closeTask":
					taskAndInvoiceMgrIfaceGql.closeTask(request, response);
					break;	
			    case "closeInvoice":
					taskAndInvoiceMgrIfaceGql.closeInvoice(request, response);
					break;	
			    case "addLog":
					taskAndInvoiceMgrIfaceGql.addLogToSchedule(request, response);
					break;	
			    case "getSubTaskByMainId":
					taskAndInvoiceMgrIfaceGql.getSubTaskByMainId(request, response);
					break;	
			    case "getUserByParam":
					taskAndInvoiceMgrIfaceGql.getUserByWarehouseIdAndGroupId(request, response);
					break;
			    case "assignMainTask":
					taskAndInvoiceMgrIfaceGql.assignMainTask(request, response);
					break;	
			    case "getProjectBySearchParam":
					taskAndInvoiceMgrIfaceGql.getProjectBySearchParam(request, response);
					break;	
			    case "assignInvoice":
					taskAndInvoiceMgrIfaceGql.assignInvoice(request, response);
					break;	
			    default: 
			       this.setResponse(response);
			       response.getWriter().print(new JSONObject().put("message", "key:"+key+"不存在"));
			       break;
				}
		    }
		    

	}
	
	/**
	 * response 设置
	 * @response
	 */
	private void setResponse(HttpServletResponse response){
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	public void setTaskAndInvoiceMgrIfaceGql(
			TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql) {
		this.taskAndInvoiceMgrIfaceGql = taskAndInvoiceMgrIfaceGql;
	}
	
	

	

	
	
    
	
}
