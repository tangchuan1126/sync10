package com.cwc.service.printLabe.action.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gql.TaskAndInvoiceMgrIfaceGql;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class HandleScheduleAction extends ActionFatherController{
	private TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql;
	private TransactionTemplate txTemplate;
	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		try{
			txTemplate.execute(new TransactionCallbackWithoutResult() {
				
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus txStat) {
//					System.out.println(request.getMethod());
					try {
						switch (request.getMethod()) {
						case "GET":
							//get schedule 
							taskAndInvoiceMgrIfaceGql.getTaskAndDetail(request, response);
							break;
						case "POST":
							//add schedule
							taskAndInvoiceMgrIfaceGql.addAndUpdateSchedule(request, response);
							break;
						case "PUT":
							//update  schedule
							taskAndInvoiceMgrIfaceGql.addAndUpdateSchedule(request, response);
							break;
						case "DELETE":
							//delete schedule
							taskAndInvoiceMgrIfaceGql.deleteScheduleByScheduleId(request, response);
							
							break;
						default:
							
							
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
					
				}
			});
			
			
		}catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}


	public void setTaskAndInvoiceMgrIfaceGql(
			TaskAndInvoiceMgrIfaceGql taskAndInvoiceMgrIfaceGql) {
		this.taskAndInvoiceMgrIfaceGql = taskAndInvoiceMgrIfaceGql;
	}


	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}
    
	
}
