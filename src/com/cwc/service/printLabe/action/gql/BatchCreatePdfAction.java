package com.cwc.service.printLabe.action.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gql.PrintLabelMgrIfaceGql;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
/***
 * 取key值的通用类
 * @author Administrator
 *
 */
public class BatchCreatePdfAction extends ActionFatherController{
	
    private PrintLabelMgrIfaceGql printLabelMgrIfaceGql;
	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		    printLabelMgrIfaceGql.downLoadPdfZip(request, response);
	}
	

	public void setPrintLabelMgrIfaceGql(PrintLabelMgrIfaceGql printLabelMgrIfaceGql) {
		this.printLabelMgrIfaceGql = printLabelMgrIfaceGql;
	}	
	
	
    
	
}
