package com.cwc.service.helpCenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
/**
 * 根据ID获得JSON格式的帮助中心分类
 * @author Administrator
 *
 */
public class GetHelpCenterCatalogDetailJOSNAction extends
		ActionFatherController {
	private HelpCenterIFace helpMgr;
	public void setHelpMgr(HelpCenterIFace helpMgr) {
		this.helpMgr = helpMgr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		// TODO Auto-generated method stub
		DBRow detail=helpMgr.getDetailCatalog(request);
		if(detail==null){
			detail=new DBRow();
			detail.add("title","/");
		}
		throw new JsonException(new JsonObject(detail));
	}

}
