package com.cwc.service.helpCenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONObject;
/**
 * 根据ID获得文章的JOSN
 * @author Administrator
 *
 */
public class GetHelpCenterTopicDetailJOSNAction extends ActionFatherController {

	private MessageAlerter messageAlert;
	private HelpCenterIFace helpMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		long hct_id=StringUtil.getLong(request,"hct_id");
		DBRow detail=helpMgr.getDetailTopicsByHTCID(hct_id);
		throw new JsonException(new JSONObject(detail));
		
	}
	
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	public void setHelpMgr(HelpCenterIFace helpMgr) {
		this.helpMgr = helpMgr;
	}
}
