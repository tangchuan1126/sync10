package com.cwc.service.helpCenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.helpCenter.HaveTopicCatalogException;
import com.cwc.app.exception.helpCenter.ParentCatalogException;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.sun.mail.iap.Response;
/**
 * 删除分类
 * @author Administrator
 *
 */
public class DelHelpCenterCatalogAction extends ActionFatherController {
	
	private MessageAlerter messageAlert;
	private HelpCenterIFace helpMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			helpMgr.delCatalog(request);
		} 
		catch (ParentCatalogException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ParentCatalogException",""));
		}
		catch (HaveTopicCatalogException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","HaveTopicCatalogException",""));
		}
		throw new RedirectRefException();
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

	public void setHelpMgr(HelpCenterIFace helpMgr) {
		this.helpMgr = helpMgr;
	}

}
