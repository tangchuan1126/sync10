package com.cwc.service.helpCenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.helpCenter.ModHelpCenterTopicIssueNotOwnerException;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.util.StringUtil;

public class ModHelpCenterTopicIssueAction extends ActionFatherController {

	private MessageAlerter messageAlert;
	private HelpCenterIFace helpMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 			
	{
			try 
			{
				String issue=StringUtil.getString(request,"issue");
				String ids=StringUtil.getString(request,"hct_id");
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				
				if(issue.equals("use"))
				{
					helpMgr.modTopicIssue(1,ids,adminLoggerBean);
				}
				if(issue.equals("cancel"))
				{
					helpMgr.modTopicCancelIssue(0,ids,adminLoggerBean);
				}
			}
			catch (ModHelpCenterTopicIssueNotOwnerException e) 
			{
				messageAlert.setMessage(request, Resource.getStringValue("","ModHelpCenterTopicIssueNotOwnerException",""));
			}
			throw new RedirectRefException();
	}
	
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	public void setHelpMgr(HelpCenterIFace helpMgr) {
		this.helpMgr = helpMgr;
	}
}
