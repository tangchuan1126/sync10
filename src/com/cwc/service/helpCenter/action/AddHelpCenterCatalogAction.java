package com.cwc.service.helpCenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.helpCenter.DoubleCatalogNameException;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
/**
 * 添加帮助中心分类
 * @author Administrator
 *
 */
public class AddHelpCenterCatalogAction extends ActionFatherController {
	
	private MessageAlerter messageAlert;
	private HelpCenterIFace helpMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			helpMgr.addCatalog(request);
		}
		catch (DoubleCatalogNameException e)
		{
			messageAlert.setMessage(request, Resource.getStringValue("","DoubleCatalogNameException",""));
		}
		
		
		throw new RedirectRefException();
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	public void setHelpMgr(HelpCenterIFace helpMgr) {
		this.helpMgr = helpMgr;
	}

}
