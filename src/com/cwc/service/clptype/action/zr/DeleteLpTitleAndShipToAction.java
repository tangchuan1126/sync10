package com.cwc.service.clptype.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ClpTypeMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DeleteLpTitleAndShipToAction extends ActionFatherController{

	private ClpTypeMgrIfaceZr clpTypeMgrZr ;
	
	public void setClpTypeMgrZr(ClpTypeMgrIfaceZr clpTypeMgrZr) {
		this.clpTypeMgrZr = clpTypeMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			 
			long lp_title_ship_id = StringUtil.getLong(request, "lp_title_ship_id");
			clpTypeMgrZr.deleteLpTitleAndShipTo(lp_title_ship_id);
			DBRow result = new DBRow();
			result.add("flag", "success");
			throw new JsonException(new JsonObject(result));
	
 		
	}

}
