package com.cwc.service.purchase.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zwb.PreparePurchaseMgrIfaceZwb;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class TransportApplyMoneyAction extends ActionFatherController
{

	private PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb;
 
	
 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		DBRow result = preparePurchaseMgrZwb.transportApplyMoney(request);
		throw new JsonException(new JsonObject(result));
		
	}
	
	
	public void setPreparePurchaseMgrZwb(
			PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb)
	{
		this.preparePurchaseMgrZwb = preparePurchaseMgrZwb;
	}

}
