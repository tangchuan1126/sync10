package com.cwc.service.purchase.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zwb.PreparePurchaseMgrIfaceZwb;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateApplyMoneyAction extends ActionFatherController {

	private PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
			
	  
		long apply_id=StringUtil.getLong(request, "apply_id",0l);
		String adid=StringUtil.getString(request, "adid");
		long categoryId=StringUtil.getLong(request, "categoryId",0l);
		String association_id=StringUtil.getString(request, "associationId");
		String association_type_id=StringUtil.getString(request, "association_type_id");
		double amount=StringUtil.getDouble(request, "amount");
		String payee=StringUtil.getString(request, "payee");
		String paymentInfo=StringUtil.getString(request, "paymentInfo");
		String remark=StringUtil.getString(request, "remark");
		long center_account_id=StringUtil.getLong(request, "center_account_id",0l);
		long center_account_type_id=StringUtil.getLong(request, "center_account_type_id",0l);
		long center_account_type1_id=StringUtil.getLong(request, "center_account_type1_id",0l);
		long product_line_id=StringUtil.getLong(request, "product_line_id",0l);
		int flag=StringUtil.getInt(request, "flag");
		int add=StringUtil.getInt(request, "add");
		long payee_type_id = StringUtil.getLong(request,"payee_type_id",0);
		long payee_type1_id = StringUtil.getLong(request,"payee_type1_id",0);
		long payee_id = StringUtil.getLong(request,"payee_id",0);
		String currency=StringUtil.getString(request, "currency");
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		preparePurchaseMgrZwb.updateApplyMoney(apply_id,adid,categoryId, association_id, amount, payee, paymentInfo, remark, center_account_id,product_line_id,association_type_id, center_account_type_id, center_account_type1_id,payee_type_id,payee_type1_id,payee_id,currency,adminLoggerBean);
		
		if(flag==1)
		{
			throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");
		}
		else if(add==1)
		{
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/assets_apply_funds.html?add=1");
		}else if(add == 0) {
			throw new RedirectRefException();
		}
		
	}

	public void setPreparePurchaseMgrZwb(
			PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb) {
		this.preparePurchaseMgrZwb = preparePurchaseMgrZwb;
	}

}
