package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class DownLoadPurchaseAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,DoNothingException,Exception 
	{
		DBRow db = new DBRow();;
		try 
		{
			String path = purchaseMgr.downloadPurchase(request);
			String basePath = "";
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
				basePath = ConfigBean.getStringValue("systenFolder") + path;	
			}
			else
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",basePath);
		} 
		catch(NoProductInPurchaseException e)
		{
			db.add("result","采购单内没有商品");
			db.add("canexport",false);
		}
		catch (Exception e) 
		{
			db.add("canexport",false);
		}
		
		throw new JsonException(new JsonObject(db));
	}
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

}
