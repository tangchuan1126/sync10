package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.PurchaseApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 采购差异审核
 * @author Administrator
 *
 */
public class ApprovePurchaseAction extends ActionFatherController {

	private PurchaseApproveMgrIFaceZJ purchaseApproveMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long pa_id = StringUtil.getLong(request,"pa_id");
		String[] spdd_ids = request.getParameterValues("pdd_ids");
		long[] pdd_ids = new long[spdd_ids.length];
		String[] notes = new String[spdd_ids.length];
		
		for (int i = 0; i < spdd_ids.length; i++)
		{
			pdd_ids[i] = Long.parseLong(spdd_ids[i]);
			notes[i] = StringUtil.getString(request,"note_"+pdd_ids[i]);
		}
		
		purchaseApproveMgrZJ.approvePurchaseDifferent(pa_id, pdd_ids, notes, adminLoggerBean);
		
		throw new RedirectBackUrlException();
	}
	
	public void setPurchaseApproveMgrZJ(PurchaseApproveMgrIFaceZJ purchaseApproveMgrZJ) {
		this.purchaseApproveMgrZJ = purchaseApproveMgrZJ;
	}

}
