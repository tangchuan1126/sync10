package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得采购单的总体积，总重量，总货款
 * @return Json格式返回
 * @author Administrator
 *
 */
public class GetPurchaseSumVWPJsonAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		double all_price = purchaseMgr.getPurchasePrice(purchase_id);
		float all_volume = purchaseMgr.getPurchaseVolume(purchase_id);
		float all_weight = purchaseMgr.getPurchaseWeight(purchase_id);
		
		DBRow result = new DBRow();
		result.add("all_price",all_price);
		result.add("all_volume",all_volume);
		result.add("all_weight",all_weight);
		
		throw new JsonException(new JsonObject(result));
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

}
