package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseApproveMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 采购单申请审核
 * @author Administrator
 *
 */
public class ApplicationApprovePurchaseAction extends ActionFatherController {

	private  PurchaseApproveMgrIFaceZJ pruchaseApproveMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		pruchaseApproveMgrZJ.addPuchaseApprove(request);
		throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/purchase/purchase.html");
	}
	public void setPruchaseApproveMgrZJ(
			PurchaseApproveMgrIFaceZJ pruchaseApproveMgrZJ) {
		this.pruchaseApproveMgrZJ = pruchaseApproveMgrZJ;
	}

}
