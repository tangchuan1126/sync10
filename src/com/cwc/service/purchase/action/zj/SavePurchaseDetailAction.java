package com.cwc.service.purchase.action.zj;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 上传采购单详细保存
 * @author Administrator
 *
 */
public class SavePurchaseDetailAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	private ShortMessageMgrZyjIFace shortMessageMgrZyj;
	private StorageCatalogMgrZyjIFace storageCatalogMgrZyjIFace;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db=new DBRow();
		try 
		{
			
			//得到登录者的人员名
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			String emp_name		= adminLoggerBean.getEmploye_name();
			
			long purchase_id	= StringUtil.getLong(request, "purchase_id");//采购单Id
			DBRow purchase		= purchaseMgr.getDetailPurchaseByPurchaseid(purchase_id+"");//采购单信息
			long module_id		= ModuleKey.PURCHASE_ORDER;//采购单标识
			long storageId		= 0L;//仓库Id
			String storageName	= "";//仓库名称
			if(null != purchase){
				storageId		= purchase.get("ps_id",0L);
			}
			DBRow storageInfo	= storageCatalogMgrZyjIFace.getStorageCatalogById(storageId);//仓库信息
			if(null != storageInfo){
				storageName		= storageInfo.getString("title");
			}
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime 	= sdf.format(new Date());
			
			String createUpdate	= "";
			DBRow[] purchaseDetails =  purchaseMgr.getPurchaseDetails(request);
			if(0 == purchaseDetails.length){
				createUpdate	= "创建";
			}else{
				createUpdate	= "修改";
			}
			
			String content		= emp_name + "于" + sendTime + createUpdate + "了采购单：" + purchase_id +"，收货仓库为：" + storageName;
			
			long adgId1			= 100015L;//调试部门
			long adgId2			= 100019L;//采购部门
			long[] roleId		= {5, 10};//主管和副主管
			DBRow[] receivers1	= shortMessageMgrZyj.getUserListByAdgIdRoleId(adgId1, roleId);
			DBRow[] receivers2	= shortMessageMgrZyj.getUserListByAdgId(adgId2);
			int receivers1Len	= 0;
			int receivers2Len	= 0;
			if(null != receivers1){
				receivers1Len	= receivers1.length;
			}
			if(null != receivers2){
				receivers2Len	= receivers2.length;
			}
			int len				= receivers1Len+receivers2Len;
			DBRow[] receivers3	= new DBRow[len];
			System.arraycopy(receivers1, 0, receivers3, 0, receivers1Len);
			System.arraycopy(receivers2, 0, receivers3, receivers1Len, receivers2Len);
			
			shortMessageMgrZyj.addShortMessage(module_id, purchase_id, content, receivers3, adminLoggerBean.getAdid()); // addShortMessage(module_id, purchase_id, content, receivers3, adid);
			
			purchaseMgr.savePurchaseDetail(request);
			
			db.add("close",true);
		}
		catch (FileException e) 
		{
			db.add("error","上传文件内商品数量与原采购单内数量不匹配");
		}

		String result="<script type=\"text/javascript\">"+"parent.window.location.reload()</script>";
		
		throw new WriteOutResponseException(result);
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}

	public void setStorageCatalogMgrZyjIFace(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyjIFace) {
		this.storageCatalogMgrZyjIFace = storageCatalogMgrZyjIFace;
	}
	
	

}
