package com.cwc.service.purchase.action.zj;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AffirmTransferPurchaseAction extends ActionFatherController {
	
	private PurchaseIFace purchaseMgr;
	private ShortMessageMgrZyjIFace shortMessageMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 		
	{
		purchaseMgr.affirmTransferPurchase(request);
		//得到登录者的人员名
		AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		String emp_name		= adminLoggerBean.getEmploye_name();
		long module_id		= ModuleKey.PURCHASE_ORDER;//采购单标识
		long purchase_id	= StringUtil.getLong(request, "purchase_id");//采购单Id
		
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sendTime 	= sdf.format(new Date());
		
		double amount = StringUtil.getDouble(request,"amount");
		
		String content		= emp_name + "于" + sendTime + "申请采购单：" + purchase_id + "，金额为：" + amount;
		
		long adgId			= 100020L;//调试部门
		long[] roleId		= {5, 10};//主管和副主管
		DBRow[] receivers	= shortMessageMgrZyj.getUserListByAdgIdRoleId(adgId, roleId);
		
		shortMessageMgrZyj.addShortMessage(module_id, purchase_id, content, receivers, adminLoggerBean.getAdid()); //(module_id, purchase_id, content, receivers, request);
		
		throw new RedirectRefException();
	}
	
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}
	
}
