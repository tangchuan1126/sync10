package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.purchase.TransferException;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
/**
 * 采购单转账
 * @author Administrator
 *
 */
public class PurchasetransferAction extends ActionFatherController {
	
	private PurchaseIFace purchaseMgr;
	private MessageAlerter messageAlert;
	
	

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			purchaseMgr.purchaseTransfer(request);
		} 
		catch (TransferException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","TransferException",""));
		}
		throw new RedirectRefException();
	}
	
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
	
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
}
