package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class SupplierLoginAction extends ActionFatherController {

	private PurchaseIFace purchaseMgrZJ;
	private AdminMgrIFace adminMgr;
	private String verderaccount;
	private String verderpassword;
	private SupplierMgrIFaceTJH supplierMgrTJH;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			DBRow purchase = purchaseMgrZJ.getDetailPurchaseByIdAndSupplierId(request);
			
			int remember = StringUtil.getInt(request,"remember");
			int loginType = 1;
			String licence = StringUtil.getString(request,"licence");
			
			if(purchase==null)
			{
				throw new WriteOutResponseException("1");
			}
			else
			{
				//adminMgr.adminLogin(request,response,verderaccount,verderpassword, licence, remember, loginType);
				DBRow supplier = supplierMgrTJH.getDetailSupplier(StringUtil.getLong(request,"account"));
				StringUtil.getSession(request).setAttribute("supplier_name",supplier.getString("sup_name"));
			}
		}
		catch(AccountNotPermitLoginException e)
		{
			throw new WriteOutResponseException("1");
		}
		catch (VerifyCodeIncorrectException e)
		{
			throw new WriteOutResponseException("2");
		}
		throw new WriteOutResponseException("0");
	}
	public void setPurchaseMgrZJ(PurchaseIFace purchaseMgrZJ) {
		this.purchaseMgrZJ = purchaseMgrZJ;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public void setVerderaccount(String verderaccount) {
		this.verderaccount = verderaccount;
	}
	public void setVerderpassword(String verderpassword) {
		this.verderpassword = verderpassword;
	}
	public void setSupplierMgrTJH(SupplierMgrIFaceTJH supplierMgrTJH) {
		this.supplierMgrTJH = supplierMgrTJH;
	}

}
