package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

/**
 * 采购单商品入库
 * @author Administrator
 *
 */
public class IncomingAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,CanNotInbondScanException ,Exception 
	{
			DBRow db = new DBRow(); 
			try 
			{
				purchaseMgr.incoming(request);
				db.add("close",true);
			}
			catch (CanNotInbondScanException e)
			{
				db.add("error",Resource.getStringValue("","CanNotInbondScanException",""));
			}
		
			throw new JsonException(new JsonObject(db));
	}
	
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
}
