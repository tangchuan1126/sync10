package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class UpdatePurchaseVWAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		purchaseMgr.updatePurchaseVW(purchase_id);
		
		DBRow result = new DBRow();
		result.add("result","ok");
		throw new JsonException(new JsonObject(result));
	}
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

}
