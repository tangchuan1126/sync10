package com.cwc.service.purchase.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.PurchaseDetailRepartException;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditPurchaseDetailAction extends ActionFatherController {

	private PurchaseIFace purchaseMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		try
		{
			String oper = StringUtil.getString(request,"oper");//grid操作
			
			if(oper.equals("edit"))
			{
				purchaseMgr.modPurchaseDetailGrid(request);
			}
			else if(oper.equals("add"))
			{
				purchaseMgr.addPurchaseDetailGrid(request);
			}
			else if(oper.equals("del"))
			{
				purchaseMgr.delPurchaseDetailGrid(request);
			}
			
			throw new WriteOutResponseException("0");
		} 
		catch (PurchaseDetailRepartException e) 
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[已有此商品]");
		}
		catch (ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
	}
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

}
