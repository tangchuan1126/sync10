package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddPurchaseTermsAction extends ActionFatherController{

	private PurchaseIFace purchaseMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		purchaseMgr.updatePurchaseTerms(request);
		int isOutterUpdate = StringUtil.getInt(request, "isOutterUpdate");
		if(2 == isOutterUpdate){
			DBRow result = new DBRow();
			result.add("flag", true);
			throw new JsonException(new JsonObject(result));
		}else{
			long purchase_id	= StringUtil.getLong(request, "purchase_id");
			String expectArrTime= StringUtil.getString(request, "expectArrTime");
			int refresh = 1;//刷新
			String tempfilename = StringUtil.getString(request, "tempfilename");
			String url = StringUtil.getString(request,"backurl")+"?purchase_id="+purchase_id+"&refresh="+refresh+"&tempfilename="+tempfilename+"&expectArrTime="+expectArrTime;
			throw new RedirectException(url);
		}
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
	

}
