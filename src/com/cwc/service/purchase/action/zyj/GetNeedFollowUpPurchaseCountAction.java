package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetNeedFollowUpPurchaseCountAction extends ActionFatherController {

	private PurchaseMgrZyjIFace purchaseMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		int cmdType = StringUtil.getInt(request, "cmdType");
		long productLineId = StringUtil.getLong(request, "productLineId");
		
		if(1 == cmdType)
		{
			DBRow result = new DBRow();
			result = purchaseMgrZyj.getPurchaseNeedFollowUpCount(productLineId);
			throw new JsonException(new JsonObject(result));
		}
		else if(2 == cmdType)
		{
			DBRow[] result = purchaseMgrZyj.getProductLineNeedFollowUpPurchaseCount();
			throw new JsonException(new JsonObject(result));
		}
		else if(3 == cmdType)
		{
			DBRow result = new DBRow();
			result = purchaseMgrZyj.getPurchaseNeedFollowUpCount(productLineId);
			throw new JsonException(new JsonObject(result));
		}
		
	}

	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}

}
