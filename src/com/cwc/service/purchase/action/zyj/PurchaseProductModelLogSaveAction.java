package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.key.PurchaseProductModelKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class PurchaseProductModelLogSaveAction  extends ActionFatherController{
	
	private PurchaseMgrZyjIFace purchaseMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		int product_model = StringUtil.getInt(request, "product_model");
		String returnStr = "";
		if(PurchaseProductModelKey.FINISH == product_model)
		{
			long purchase_id = StringUtil.getLong(request, "purchase_id");
			returnStr = purchaseMgrZyj.checkPurchaseProductFileFinish(purchase_id);
		}
		DBRow result = new DBRow();
		if("".equals(returnStr))
		{
			result.add("result", "success");
			purchaseMgrZyj.handlePurchaseProductModelAjax(request);
		}
		else
		{
			result.add("result", returnStr);
		}
		throw new JsonException(new JsonObject(result));
		
	 		  
	}

	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}
}