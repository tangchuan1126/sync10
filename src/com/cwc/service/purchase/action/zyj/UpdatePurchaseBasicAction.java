package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdatePurchaseBasicAction extends ActionFatherController{

	private PurchaseIFace purchaseMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		purchaseMgr.updatePurchaseBasic(request);
		long purchase_id	= StringUtil.getLong(request, "purchase_id");
		int refresh = 1;//刷新
		String tempfilename = StringUtil.getString(request, "tempfilename");
		String expectArrTime = StringUtil.getString(request, "expectArrTime");
		String url = StringUtil.getString(request,"backurl")+"?purchase_id="+purchase_id+"&refresh="+refresh+"&tempfilename="+tempfilename+"&expectArrTime="+expectArrTime;
		throw new RedirectException(url);
		
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
	
	

}
