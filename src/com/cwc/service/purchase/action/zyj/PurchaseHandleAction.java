package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class PurchaseHandleAction extends ActionFatherController{

	private PurchaseMgrZyjIFace purchaseMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			try {
				switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
	 		 
	}

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PageCtrl pc = new PageCtrl();
		int pageNo = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageSize"), "1000"));
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		JSONArray result = purchaseMgrZyj.findPurchaseFillter(pc);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject()
				.put("pageCtrl", new JSONObject(pc)).put("items",
						result);

		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 新建管理员
//		JSONObject jsonData = new JSONObject(IOUtils.toString(request
//				.getReader()));
//		//system.out.println("jsonData=" + jsonData);
//		DBRow row = DBRowUtils.convertToDBRow(jsonData);
//		//system.out.println(TestUtils.dbRowAsString(row));
//		long adid = this.floorAdminMgr.addAdmin(row);
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().print(
//				new JSONObject(DBRowUtils.dbRowAsMap(this.floorAdminMgr
//						.getDetailAdmin(adid))).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 更新管理员
//		JSONObject jsonData = new JSONObject(IOUtils.toString(request
//				.getReader()));
//		//system.out.println("jsonData=" + jsonData);
//		DBRow row = DBRowUtils.convertToDBRow(jsonData);
//		//system.out.println(TestUtils.dbRowAsString(row));
//		long adid = row.get("ADID", 0L);
//		this.floorAdminMgr.modifyAdmin(adid, row);
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		DBRow detail = this.floorAdminMgr.getDetailAdmin(adid);
//		
//		response.setStatus(detail==null?500:200);
//		response.getWriter().print(
//			detail ==  null ? new JSONObject().put("success", false).put("error","不存在此管理员："+adid) :
//			new JSONObject(DBRowUtils.dbRowAsMap(detail))
//		);
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
//		this.floorAdminMgr.delAdmin(Long.parseLong(request.getParameter("id")));
//		response.setContentType("application/json");
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().print(
//				new JSONObject().put("success", true).toString());
	}
	
	
	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}


}
