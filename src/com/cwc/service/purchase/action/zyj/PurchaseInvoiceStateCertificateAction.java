package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.key.PurchaseLogTypeKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class PurchaseInvoiceStateCertificateAction extends ActionFatherController{

	private PurchaseMgrZyjIFace purchaseMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

			int file_with_type = StringUtil.getInt(request, "follow_up_type");
			int file_with_class = StringUtil.getInt(request,"invoice");  
			String returnStr = "";
			if(PurchaseLogTypeKey.THIRD_TAG == file_with_type && TransportTagKey.FINISH == file_with_class)
			{
				returnStr = purchaseMgrZyj.checkPurchaseProductTagFileFinish(request);
			}
			DBRow bean = new DBRow();
			if("".equals(returnStr))
			{
				purchaseMgrZyj.handlePurchaseInvoiceCertificationState(request);
				bean.add("flag", "success");
			}
			else
			{
				bean.add("flag", returnStr);
			}
			throw new JsonException(new JsonObject(bean));
		
	}

	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}

	
	
}
