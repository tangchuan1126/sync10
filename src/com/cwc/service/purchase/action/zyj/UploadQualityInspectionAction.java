package com.cwc.service.purchase.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class UploadQualityInspectionAction  extends ActionFatherController{

	private PurchaseMgrZyjIFace purchaseMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		long purchase_id = StringUtil.getLong(request, "purchase_id");
		int quality_inspection = StringUtil.getInt(request, "quality_inspection");
		String backurl = StringUtil.getString(request, "backurl");
		purchaseMgrZyj.uploadPurchaseQualityInspection(request, purchase_id, quality_inspection);
		throw new RedirectBackUrlException(backurl);
		
	}

	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}
	
	

}
