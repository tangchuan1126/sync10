package com.cwc.service.shortMessage.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddShortMessageAction extends ActionFatherController{

	private ShortMessageMgrZyjIFace shortMessageMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		String cmd				= StringUtil.getString(request, "cmd");
		
		
		//通过页面动态添加人员及电话来发送短信
		if("receivers".equals(cmd)){
			
			DBRow[] receivers		= null;
			String[] receiverNames	= request.getParameterValues("receiver_name");
			String[] receiverPhones	= request.getParameterValues("receiver_phone");
			if(null != receiverPhones && receiverPhones.length > 0){
				receivers = new DBRow[receiverPhones.length];
				for(int i = 0; i < receiverPhones.length; i ++){
					DBRow receiver	= shortMessageMgrZyj.generateReceiver(receiverNames[i], receiverPhones[i]);
					receivers[i]	= receiver;
				}
			}
			//shortMessageMgrZyj.addShortMessage(1, 1, "测试动态添加多个收信人", receivers, request);
			//通过群组ID,得到此群组下的所有人员，给这些人员发送短信
		}else if("adgId".equals(cmd)){
			
			String proName							= StringUtil.getString(request, "proAdgidName");
			int proAdgid						= Integer.parseInt(proName);
			
			shortMessageMgrZyj.addShortMessage(1, 2, "测试通过群组Id，发送短信", proAdgid, request);
		
		//通过群组ID,角色ID,给符合这些条件的人员发送短信
		}else if("adgIdRoleId".equals(cmd)){
			
			String name							= StringUtil.getString(request, "proAdgidName");
			int proAdgid						= Integer.parseInt(name);
			String proJsIdName					= StringUtil.getString(request, "proJsIdName");
			int proJsId							= Integer.parseInt(proJsIdName);
			
			shortMessageMgrZyj.addShortMessage(1, 2, "测试通过群组ID,角色Id,发送短信", proAdgid, proJsId, request);
		}
		DBRow result = new DBRow();
		result.add("flag", true);
		throw new JsonException(new JsonObject(result));
		
		
	}

	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}

	
	
	

}
