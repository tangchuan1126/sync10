package com.cwc.service.fileUpload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.FileMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddImageAction extends ActionFatherController {
	private FileMgrIFaceZJ fileMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception
	{
		fileMgrZJ.addFile(request);
		String backUrl = StringUtil.getString(request,"backUrl");
		throw new RedirectRefException(backUrl);
	}

	public FileMgrIFaceZJ getFileMgrZJ() {
		return fileMgrZJ;
	}

	public void setFileMgrZJ(FileMgrIFaceZJ fileMgrZJ) {
		this.fileMgrZJ = fileMgrZJ;
	}

}
