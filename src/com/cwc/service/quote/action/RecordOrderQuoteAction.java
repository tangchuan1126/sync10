package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.quote.CartQuoteErrorException;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 抄单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RecordOrderQuoteAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;
	private MessageAlerter messageAlert;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
//		try 
//		{
			int flag = quoteMgr.saveQuoteProduct(request);
//		}
//		catch (CartQuoteErrorException e) 
//		{
//			messageAlert.setMessage(request,Resource.getStringValue("","CartQuoteErrorException",""));
//		}
		
		throw new RedirectBackUrlException();
	}


	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}


	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}
	
}
