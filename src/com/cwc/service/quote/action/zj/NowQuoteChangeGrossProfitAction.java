package com.cwc.service.quote.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.QuoteMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class NowQuoteChangeGrossProfitAction extends ActionFatherController {

	private QuoteMgrIFaceZJ quoteMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		quoteMgrZJ.qutoPriceChangeGrossProfit(request);
		
		DBRow dbrow = new DBRow();
		dbrow.add("close",true);
		
		throw new JsonException(new JsonObject(dbrow));
	}
	
	public void setQuoteMgrZJ(QuoteMgrIFaceZJ quoteMgrZJ) {
		this.quoteMgrZJ = quoteMgrZJ;
	}

}
