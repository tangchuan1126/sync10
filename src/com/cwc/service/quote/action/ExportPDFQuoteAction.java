package com.cwc.service.quote.action;

import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.reportcenter.ReportCenter;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ExportPDFQuoteAction extends ActionFatherController 
{
	private QuoteIFace quote;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long qoid = StringUtil.getLong(request, "qoid");
		DBRow result = quote.getQuotePDFDatas(qoid);
		
		String reporViewTemplatetFilePath = result.getString("view_path");
		DBRow dataRows[] = (DBRow[])result.get("items",new Object());
		HashMap<String, String> paras = (HashMap<String, String>)result.get("paras",new Object());

		ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows);
		JasperPrint printer = reportCenter.getPrinter();

//		JRHtmlExporter exporter = new JRHtmlExporter();
//		StrUtil.getSession(request).setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, printer);
//        exporter.setParameter(JRExporterParameter.JASPER_PRINT, printer);
//        exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, null);
//        exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
//        exporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, out);
//        exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
//        exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "../servlets/image?image=");
//        exporter.exportReport();

		//pdf
		ServletOutputStream out = response.getOutputStream();
		byte[] pdfByte = JasperExportManager.exportReportToPdf(printer);
		response.setContentType("application/pdf");
	    response.setContentLength(pdfByte.length);
	    response.setHeader("Content-Disposition", "attachment; filename=\"VisionariQuote"+String.valueOf(qoid)+".pdf\"");
	    out.write(pdfByte, 0, pdfByte.length);
        out.flush();
        out.close();
	}

	
	
	public void setQuote(QuoteIFace quote)
	{
		this.quote = quote;
	}
	
}







