package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.quote.DuplicateOrderException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.exception.quote.OrderNotWait4RecordException;
import com.cwc.app.exception.quote.OrderSourceErrorException;
import com.cwc.app.exception.quote.TotalMcGrossErrorException;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 成单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FinishQuoteAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;
	private MessageAlerter messageAlert;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			quoteMgr.finishQuote(request);
		}
		catch (OrderSourceErrorException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","OrderSourceErrorException",""));
		}
		catch (DuplicateOrderException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","DuplicateOrderException","").replace("$1", e.getMessage()));
		}
		catch (TotalMcGrossErrorException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","TotalMcGrossErrorException",""));
		}
		catch (OrderNotFoundException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","OrderNotFoundException",""));
		}
		catch (OrderNotWait4RecordException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","OrderNotWait4RecordException",""));
		}
		
		throw new RedirectRefException();
	}

	
	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}
}
