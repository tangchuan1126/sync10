package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CreateQuoteAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long qoid = 0;
		
		try
		{
			qoid = quoteMgr.createQuote(request);
		}
		catch(Exception e)
		{
		}
		
		throw new WriteOutResponseException(String.valueOf(qoid));
	}

	
	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}



}
