package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ValidateProductsAndDeliveryAction extends ActionFatherController 
{
	private ProductMgrIFace productMgr;
	private CartQuoteIFace cart;
	private QuoteIFace quoteMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			long ps_id = StringUtil.getLong(request, "ps_id");
			long qoid = StringUtil.getLong(request, "qoid");
			long sc_id = StringUtil.getLong(request, "sc_id");
			long ccid = StringUtil.getLong(request, "ccid");
			               
			DBRow products[] = cart.getSimpleProducts(StringUtil.getSession(request));
			productMgr.validateProductsCreateStorage(ps_id,products);
			quoteMgr.getQuoteOrderCost(qoid, sc_id, ccid);//验证发货快递
		}
		catch (ProductNotCreateStorageException e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		catch (WeightOutSizeException e)
		{
			throw new WriteOutResponseException("WeightOutSizeException");
		}
		catch (WeightCrossException e)
		{
			throw new WriteOutResponseException("WeightCrossException");
		}
		catch (CountryOutSizeException e)
		{
			throw new WriteOutResponseException("CountryOutSizeException");
		}
		catch (ProvinceOutSizeException e)
		{
			throw new WriteOutResponseException("ProvinceOutSizeException");
		}
		
		throw new WriteOutResponseException("ok");
	}

	
	
	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}
	
	public void setCart(CartQuoteIFace cart)
	{
		this.cart = cart;
	}

	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}

}
