package com.cwc.service.quote.action;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddRetailPriceChangeAjaxAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;


	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			long rp_id = quoteMgr.addRetailPriceChange(request);
			
//			AdminMgr am = new AdminMgr();
//			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
//			JbpmMgrIFace jbpmMgr = (JbpmMgrIFace)MvcUtil.getBeanFromContainer("proxyJbpmMgr");
			
			String id = String.valueOf(rp_id);//调价申请单ID
			String key = "change_retail_price";

//			HashMap para = new HashMap();
//			para.put("id", id);
//			ArrayList pl = new ArrayList();
//			pl.add(adminInfo.getAdid());
//			para.put(JbpmMgr.JOINT_TASK_PEOPLE, pl);//创建任务人
//			jbpmMgr.startPIwithFormByKey(key,para,"调整零售报价");
		}
		catch(Exception e)
		{
			throw new WriteOutResponseException("false");
		}
		throw new WriteOutResponseException("true");
	}



	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}
}
