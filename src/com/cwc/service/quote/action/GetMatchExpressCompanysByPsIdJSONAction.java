package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 报价单获得发货仓库相对应的快递
 * 注意：使用跟打印订单相同的策略和算法
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetMatchExpressCompanysByPsIdJSONAction extends ActionFatherController 
{
	private ExpressMgrIFace expressMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long ps_id = StringUtil.getLong(request, "ps_id");
		long ccid = StringUtil.getLong(request, "ccid");
		long pro_id = StringUtil.getLong(request, "pro_id");
		float weight = StringUtil.getFloat(request, "weight");
		DBRow express[] = new DBRow[0];

		if (ps_id>0&&ccid>0)
		{
			//先判断是国内发货还是国际发货
			boolean isDomesticFlag = expressMgr.isDomesticShipping(ccid, ps_id);

			if (isDomesticFlag)//国内发货
			{
				express = expressMgr.getDomesticExpressCompanyByPsIdProId( ps_id, pro_id, weight, null);
			}
			else
			{
				express = expressMgr.getInternationalExpressCompanyByPsIdProId(ps_id,ccid,weight,null);
			}
		}
		
		throw new JsonException(new JsonObject(express));
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}


}
