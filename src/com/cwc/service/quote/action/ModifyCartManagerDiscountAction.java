package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ModifyCartManagerDiscountAction extends ActionFatherController 
{
	private CartQuoteIFace cartQuote;

	public void setCartQuote(CartQuoteIFace cartQuote)
	{
		this.cartQuote = cartQuote;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		float manager_discount = StringUtil.getFloat(request, "manager_discount");
		
		cartQuote.modifyCartManagerDiscount(StringUtil.getSession(request), manager_discount) ;
		throw new WriteOutResponseException("ok");
	}

	




}
