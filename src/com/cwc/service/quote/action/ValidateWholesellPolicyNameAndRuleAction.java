package com.cwc.service.quote.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.quote.DuplicateWholeSellPolicyNameException;
import com.cwc.app.exception.quote.ErrorWholeSellDiscountRuleException;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 验证重名和实现类名
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ValidateWholesellPolicyNameAndRuleAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			String org_name = "";
			
			String name = StringUtil.getString(request, "name");
			long wsd_id = StringUtil.getLong(request, "wsd_id");
			String discount_policy = StringUtil.getString(request, "discount_policy");

			if (wsd_id>0)
			{
				DBRow orgDetail = quoteMgr.getDetailWholeSellDiscountByWsdId(wsd_id);
				org_name = orgDetail.getString("name");
			}

			if (!org_name.equals(name)&&quoteMgr.getDetailWholeSellDiscountByName(name)!=null)
			{
				throw new DuplicateWholeSellPolicyNameException();
			}
			
			quoteMgr.validateDiscountRule( quoteMgr.getWholeSellRule2(discount_policy) );
			
		}
		catch (DuplicateWholeSellPolicyNameException e)
		{
			throw new WriteOutResponseException("DuplicateWholeSellPolicyNameException");
		}
		catch (ErrorWholeSellDiscountRuleException e)
		{
			throw new WriteOutResponseException("ErrorWholeSellDiscountRuleException:"+e.getMessage());
		}
		
		throw new WriteOutResponseException("ok");
	}


	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}

}
