package com.cwc.service.productbasedata.action.zyz;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.Tree;

public class GetProductUnionXMLAction extends ActionFatherController {
	
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private AdminMgrIFaceZJ adminMgrZJ;
	private ProductBaseDataMgrIFaceZYZ ProductBaseDataMgr;
	private ProductMgrIFace productMgr;
	private CatalogMgrIFace catalogMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
//		String xml ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?><data><Password>renweidong</Password><LoginAccount>renweidong</LoginAccount><union_flag>1</union_flag><pc_id>155001</pc_id></data>";
		String loginAccount = "";
		String password = "";
		String union_flag = "";
		String pc_id = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow[] productUnion = null;
		
		Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
				
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= ProductBaseDataMgr.getSampleNode(xml,"LoginAccount");
			password		= ProductBaseDataMgr.getSampleNode(xml,"Password");
			union_flag			= ProductBaseDataMgr.getSampleNode(xml, "union_flag");
			int union_flag_int	= "".equals(union_flag)?0:Integer.parseInt(union_flag);
			pc_id	= ProductBaseDataMgr.getSampleNode(xml, "pc_id");
			long pcIdInt	= "".equals(pc_id)?0:Long.parseLong(pc_id);
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			//套装关系
//			if(union_flag_int==1){
//				productUnion = productMgr.getProductsInSetBySetPid(pcIdInt);
				 productUnion = productMgr.getProductUnionsBySetPid(pcIdInt);
				for(int j = 0;j<productUnion.length;j++)
				{
				  	String catalogText = "";
				  		  
					DBRow allFather_union[] = tree.getAllFather(productUnion[j].get("catalog_id",0l));
					for (int jj=0; jj<allFather_union.length-1; jj++)
					{
						catalogText += allFather_union[jj].getString("title")+",";
					}
					  
					 
					DBRow catalog_union = catalogMgr.getDetailProductCatalogById(productUnion[j].get("catalog_id",0l));
					if (catalog_union!=null)
					{
						catalogText+=catalog_union.getString("title");
					}
					
					productUnion[j].add("catalog_text",catalogText);
					productUnion[j].add("gross_profit",(productUnion[j].get("gross_profit",0d)*100));
				}
//			}

		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(productUnion, response, ret, err);
		}
		
	}
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow[] productUnion, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<productUnions>");
		sb.append(productUnionToString(productUnion));
		sb.append("</productUnions>");
		
//		logBcs.info("response:"+sb.toString());
		////system.out.println(boxQtyXml.toString()+"----返回值");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
private String productUnionToString(DBRow[] productUnions)throws Exception{
		
		StringBuffer sb = new StringBuffer();
		try
		{
			if(productUnions != null){
				for (int k = 0; k < productUnions.length; k++)
				{
					sb.append("<productUnion id=\""+productUnions[k].get("pid", 0L)+"\">");
					
					sb.append("<set_pid>"+productUnions[k].get("set_pid",0l)+"</set_pid>");
					sb.append("<p_name>"+productUnions[k].getString("p_name")+"</p_name>");
					sb.append("<unit_name>"+productUnions[k].getString("unit_name")+"</unit_name>");
					sb.append("<quantity>"+productUnions[k].get("quantity", 0f)+"</quantity>");
					sb.append("<p_code>").append(productUnions[k].getString("p_code")).append("</p_code>");
					sb.append("<catalog_id>").append(productUnions[k].get("catalog_id", 0L)).append("</catalog_id>");
					sb.append("<catalog_text>").append(productUnions[k].getString("catalog_text")).append("</catalog_text>");
					sb.append("<weight>").append(productUnions[k].get("weight", 0f)).append("</weight>");
					sb.append("<unit_price>").append(productUnions[k].get("unit_price", 0d)).append("</unit_price>");
					sb.append("<volume>").append(productUnions[k].get("volume", 0f)).append("</volume>");
					sb.append("<gross_profit>").append(productUnions[k].get("gross_profit", 0d)).append("</gross_profit>");
					
					sb.append("</productUnion>");
				}
			}else{
				sb.append("0");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}

public AdminMgrIFaceZJ getAdminMgrZJ() {
	return adminMgrZJ;
}
public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
	this.adminMgrZJ = adminMgrZJ;
}
public ProductBaseDataMgrIFaceZYZ getProductBaseDataMgr() {
	return ProductBaseDataMgr;
}
public void setProductBaseDataMgr(ProductBaseDataMgrIFaceZYZ productBaseDataMgr) {
	ProductBaseDataMgr = productBaseDataMgr;
}
public ProductMgrIFace getProductMgr() {
	return productMgr;
}
public void setProductMgr(ProductMgrIFace productMgr) {
	this.productMgr = productMgr;
}
public CatalogMgrIFace getCatalogMgr() {
	return catalogMgr;
}
public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
	this.catalogMgr = catalogMgr;
}

	
	

}
