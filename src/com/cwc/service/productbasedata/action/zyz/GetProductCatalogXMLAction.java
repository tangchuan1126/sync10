package com.cwc.service.productbasedata.action.zyz;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.Tree;

public class GetProductCatalogXMLAction extends ActionFatherController {
	
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private AdminMgrIFaceZJ adminMgrZJ;
	private ProductBaseDataMgrIFaceZYZ ProductBaseDataMgr;
	private CatalogMgrIFace catalogMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
//		String xml ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?><data><Password>renweidong</Password><LoginAccount>renweidong</LoginAccount><parentid>0</parentid></data>";
		String loginAccount = "";
		String password = "";
		String parentid = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow[] productCatalogs = null;
		
				
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= ProductBaseDataMgr.getSampleNode(xml,"LoginAccount");
			password		= ProductBaseDataMgr.getSampleNode(xml,"Password");
			parentid			= ProductBaseDataMgr.getSampleNode(xml, "parentid");
			long parentidInt	= "".equals(parentid)?0:Long.parseLong(parentid);
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			productCatalogs = catalogMgr.getProductCatalogByParentId(parentidInt,null);

		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(productCatalogs, response, ret, err);
		}
		
	}
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow[] productCatalogs, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<productCatalogs>");
		sb.append(getProductCatalogToString(productCatalogs));
		sb.append("</productCatalogs>");
		
//		logBcs.info("response:"+sb.toString());
		////system.out.println(boxQtyXml.toString()+"----返回值");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
	private String getProductCatalogToString(DBRow[] productCatalogs)throws Exception{
		
		StringBuffer sb = new StringBuffer();
		try
		{
			if(null != productCatalogs)
			{
				for (int k = 0; k < productCatalogs.length; k++)
				{
					sb.append("<productCatalog>");
					sb.append("<id>").append(productCatalogs[k].get("id",0l)).append("</id>");
					sb.append("<catalog_title>"+productCatalogs[k].getString("title")+"</catalog_title>");
					sb.append("</productCatalog>");
				}
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setProductBaseDataMgr(ProductBaseDataMgrIFaceZYZ productBaseDataMgr) {
		ProductBaseDataMgr = productBaseDataMgr;
	}
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
	
	

}

