package com.cwc.service.productbasedata.action.zyz;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;











import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;

public class ProductBaseDataXMLAction extends ActionFatherController {
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private ProductBaseDataMgrIFaceZYZ productBaseDataMgr;
	private AdminMgrIFaceZJ adminMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException,Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream();      //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		
		String fc = "";
		String loginAccount = "";
		String password  = "";
		String xml = "";
		long pid = 0l ;
		int flag = 0;
		try 
		{
			
			xml = getPost(inputStream);
			
			fc = productBaseDataMgr.getSampleNode(xml,"fc");
			loginAccount = productBaseDataMgr.getSampleNode(xml,"LoginAccount");
			password = productBaseDataMgr.getSampleNode(xml,"Password");
			
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true); 
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
		
			if(fc.equals("addProduct")){
				  pid = productBaseDataMgr.addProductXML(xml, adminLoggerBean);
 				 
				 
		    } 
			if(fc.equals("addProductUnion")){
				productBaseDataMgr.addProductUnionXML(xml, adminLoggerBean);
			}
			if(fc.equals("delProduct")){
				productBaseDataMgr.delProductXML(xml);
			}
			if(fc.equals("delProductUnion")){
				productBaseDataMgr.delProductUnionXML(xml,adminLoggerBean);
			}
			if(fc.equals("modProduct")){
				productBaseDataMgr.modProductXML(xml,adminLoggerBean);
			}
			if(fc.equals("modProductUnion")){
				productBaseDataMgr.modProductUnionXML(xml,adminLoggerBean);
			}
			if(fc.equals("modProductCatalog")){
				productBaseDataMgr.modProductCatalogXML(xml,adminLoggerBean);
			}
			if(fc.equals("addProductCode")){
				flag = productBaseDataMgr.addProductCodeXML(xml,adminLoggerBean);
			}
			if(fc.equals("addProductCodeForCheck")){
				flag = productBaseDataMgr.addProductCodeXMLForCheck(xml,adminLoggerBean);
			}
			if(fc.equals("delProductFile")){
				productBaseDataMgr.delProductFile(xml);
			}
		}
		catch (ProductNameIsExistException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductNameIsExist;//商品名称已存在
		}
		catch (ProductCodeIsExistException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductCodeIsExist;//商品条码已存在
		}
		catch(ProductNotExistException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOTEXITSPRODUCT; //商品不存在
		}
		catch(ProductUnionSetCanBeProductException	e)//已经设置了的组合套装不能做子商品
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductUnionCannotDoChild;
		}
		catch(ProductUnionIsInSetException	e)//同一个组合套装下，商品不能重复
		{
			ret = BCSKey.FAIL;
			err = BCSKey.WithProductUnionNotRepeat;
		}
		catch(ProductCantBeSetException e)//已经作为组合的商品，不能变成组合，在其下创建商品
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductUnionNoCreateProduct;
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (ProductInUnionException e)//商品有组合关系，不能删除
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductInUnionNotDel;
		}
		catch (ProductHasRelationStorageException e)//商品已建跟库存关系，不能删除
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ProductHasRelationStorage;
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally{
			 responseXML(flag, fc, ret, err, response,pid);
		}
	}
	/**
	 * 获得请求过来的数据 
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	private void responseXML(int flag, String fc,int ret,int err,HttpServletResponse response,long... id) //添加商品的时候返回一个Id
	throws Exception
{
	StringBuilder boxQtyXml = new StringBuilder();
	boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	boxQtyXml.append("<data>");
	boxQtyXml.append("<fc>"+fc+"</fc>");
	boxQtyXml.append("<ret>"+ret+"</ret>");
	if(id != null && id.length > 0){
		boxQtyXml.append("<Pid>"+id[0]+"</Pid>");
	}
	boxQtyXml.append("<err>"+err+"</err>");
	boxQtyXml.append("<flag>"+flag+"</flag>");
	boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
	boxQtyXml.append("</data>");
	
//	logBcs.info("response:"+boxQtyXml.toString());
	////system.out.println(boxQtyXml.toString()+"----返回值");
	try 
	{
	   response.setCharacterEncoding("UTF-8");

	   OutputStream out = response.getOutputStream();
	   
	   response.setContentLength(boxQtyXml.toString().getBytes("UTF-8").length);                      //添加响应包长度
	   out.write(boxQtyXml.toString().getBytes("UTF-8"));
	   out.flush();
	   out.close();
	   
	}
	catch (Exception e)
	{
	   e.printStackTrace();
	} 
}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setProductBaseDataMgr(ProductBaseDataMgrIFaceZYZ productBaseDataMgr) {
		this.productBaseDataMgr = productBaseDataMgr;
	}

	
}
