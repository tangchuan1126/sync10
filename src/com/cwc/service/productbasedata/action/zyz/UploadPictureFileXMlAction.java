package com.cwc.service.productbasedata.action.zyz;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class UploadPictureFileXMlAction extends ActionFatherController{

	private ProductBaseDataMgrIFaceZYZ productBaseDataMgr;
	private AdminMgrIFaceZJ adminMgrZJ;
	
		@Override
		public void perform(HttpServletRequest request, HttpServletResponse response)
				throws WriteOutResponseException, RedirectRefException,
				ForwardException, Forward2JspException, RedirectBackUrlException,
				RedirectException, OperationNotPermitException,
				PageNotFoundException, DoNothingException, Exception {
				int flag = 0 ;
				String file_with_id = null ;
				String file_with_type = null ;
				String file_with_class = null ;
				String upload_adid = null ;	// machine_id
				String upload_time = DateUtil.NowStr();
				String password  = null ;
				String account = null ;
				String path = null  ;
	          	ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
		          try {
		         
		            List<FileItem> items = uploadHandler.parseRequest(request);
		            
		            for (FileItem item : items) {
		            	if(item.isFormField()){
		            		//必须先等formField解析完成,才能处理File
		            		if( item.getFieldName().equals("file_with_id")){
		            			file_with_id = item.getString();
		            		}
		            		if( item.getFieldName().equals("file_with_type")){
		            			file_with_type = item.getString();
		            		}
		            		if( item.getFieldName().equals("file_with_class")){
		            			file_with_class = item.getString();
		            		}
		            		if( item.getFieldName().equals("upload_adid")){
		            			upload_adid = item.getString(); //machine_id;
		            		}
		            		if(item.getFieldName().equals("path")){
		            			path = item.getString();
		            		}
		            		if(item.getFieldName().equals("account")){
		            			account = item.getString();
		            		}
		            		if(item.getFieldName().equals("password")){
		            			password = item.getString();
		            		}
		            		
		            		flag = 1 ;
		            	}
		            }
		      
					DBRow login = adminMgrZJ.MachineLogin(account, password);
					if(login == null){
						throw new MachineUnderFindException();
					}
					productBaseDataMgr.handleUploadProductFile(file_with_id, file_with_type, file_with_class, login.get("adid", 0l)+"", upload_time, path, items);
		          
		        }catch (MachineUnderFindException e) {
		        	flag = 0 ;
	 			} catch (Exception e) {
		        	 flag = 0 ;
		            throw new RuntimeException(e); 
		           
		            
		        } finally {
		    		response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode("result.xml","utf-8"));  

	 	        	response.setCharacterEncoding("UTF-8");
		        		
		  		    OutputStream out = response.getOutputStream();
		  		   
		  		    response.setContentLength((flag+"").toString().getBytes("UTF-8").length);                      //添加响应包长度
		  		    out.write((flag+"").toString().getBytes("UTF-8"));
		  		    out.flush();
		  		    out.close();
		        	 
		        	
		        }
		}
		
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setProductBaseDataMgr(ProductBaseDataMgrIFaceZYZ productBaseDataMgr) {
		this.productBaseDataMgr = productBaseDataMgr;
	}
	
}
