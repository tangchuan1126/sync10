package com.cwc.service.productbasedata.action.zyz;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.Tree;


public class GetProductDetailXMLAction extends ActionFatherController {
	
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private AdminMgrIFaceZJ adminMgrZJ;
	private ProductBaseDataMgrIFaceZYZ ProductBaseDataMgr;
	private ProductMgrIFace productMgr;
	private CatalogMgrIFace catalogMgr;
	private TransportMgrIfaceZr transportMgrZr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
//		String xml ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?><data><Password>renweidong</Password><LoginAccount>renweidong</LoginAccount><p_code>333</p_code></data>";
		String loginAccount = "";
		String password = "";
		String search_key = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow products = null;
		
		Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
		
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= ProductBaseDataMgr.getSampleNode(xml,"LoginAccount");
			password		= ProductBaseDataMgr.getSampleNode(xml,"Password");
			search_key			= ProductBaseDataMgr.getSampleNode(xml, "search_key");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			//根据商品条码获取商品信息
			if(!search_key.matches("[+-]?[1-9]+[0-9]*(\\.[0-9]+)?"))
			{
				String p_code = search_key;
				products = new DBRow();
						//商品条码可不惟一 zyj
						//productMgr.getDetailProductByPcode(p_code);//(ZJ)
			}
			else
			{
				long pc_id = Long.parseLong(search_key);
				products = productMgr.getDetailProductByPcid(pc_id);
				if(null == products)
				{
					products = new DBRow();
							//商品条码可不惟一 zyj
							//productMgr.getDetailProductByPcode(String.valueOf(pc_id));
				}

			}
			if(products != null){
				DBRow allFather[] = tree.getAllFather(products.get("catalog_id",0l));
				String catalog_title="";
				for (int i=0; i<allFather.length-1; i++)
			  	{
					catalog_title += allFather[i].getString("title")+",";
			  	}
				
				DBRow catalog = catalogMgr.getDetailProductCatalogById(products.get("catalog_id",0l));
				if(catalog!=null)
			  	{
					catalog_title +=catalog.getString("title");
					
			  	}
				products.add("catalog_title", catalog_title);
			}
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(products, response, ret, err);
		}
		
	}
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow products, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<products>");
		sb.append(productsDetailToString(products));
		sb.append("</products>");
		
//		logBcs.info("response:"+sb.toString());
		////system.out.println(boxQtyXml.toString()+"----返回值");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

	private String productsDetailToString(DBRow products) throws Exception
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			if(products != null){
				sb.append("<product>");
				sb.append("<pc_id>").append(products.get("pc_id", 0l)).append("</pc_id>");
				sb.append("<p_name>").append(products.getString("p_name")).append("</p_name>");
				sb.append("<catalog_id>").append(products.get("catalog_id", 0L)).append("</catalog_id>");
				sb.append("<p_code>").append(products.getString("p_code")).append("</p_code>");
				sb.append("<unit_name>").append(products.getString("unit_name")).append("</unit_name>");
				sb.append("<unit_price>").append(products.get("unit_price", 0d)).append("</unit_price>");
				sb.append("<length>").append(products.get("length",0f)).append("</length>");
				sb.append("<width>").append(products.get("width", 0f)).append("</width>");
				sb.append("<heigth>").append(products.get("heigth", 0f)).append("</heigth>");
				sb.append("<weight>").append(products.get("weight", 0f)).append("</weight>");
				sb.append("<volume>").append(products.get("volume", 0f)).append("</volume>");
				sb.append("<catalog_title>").append(products.getString("catalog_title")).append("</catalog_title>");
				sb.append("<union_flag>").append(products.get("union_flag", 0l)).append("</union_flag>");
				sb.append("</product>");
			}else{
				sb.append("0");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	public void setProductBaseDataMgr(ProductBaseDataMgrIFaceZYZ productBaseDataMgr) {
		ProductBaseDataMgr = productBaseDataMgr;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
	
	

}
