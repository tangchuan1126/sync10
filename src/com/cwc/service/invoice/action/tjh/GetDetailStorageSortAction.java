package com.cwc.service.invoice.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.StorageSortMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetDetailStorageSortAction extends ActionFatherController{
   private StorageSortMgrIFace storageSortMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		int di_id=StringUtil.getInt(request,"di_id");
        DBRow row[] = storageSortMgr.getDetailStorageSortById(di_id);
	}

	public void setStorageSortMgr(StorageSortMgrIFace storageSortMgr) {
		this.storageSortMgr = storageSortMgr;
	}
	
	

}
