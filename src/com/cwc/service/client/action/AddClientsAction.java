package com.cwc.service.client.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.client.ClientIsExistException;
import com.cwc.app.iface.ClientMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 创建客户
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddClientsAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private ClientMgrIFace clientMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			clientMgr.addClients(request);
		}
		catch (ClientIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ClientIsExistException",""));
		}

		throw new RedirectRefException();
	}

	public void setClientMgr(ClientMgrIFace clientMgr)
	{
		this.clientMgr = clientMgr;
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

}
