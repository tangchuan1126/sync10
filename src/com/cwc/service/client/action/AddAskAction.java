package com.cwc.service.client.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ClientMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 创建咨询
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddAskAction extends ActionFatherController 
{
	private ClientMgrIFace clientMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		clientMgr.addAsk(request);
		throw new RedirectRefException();
	}

	public void setClientMgr(ClientMgrIFace clientMgr)
	{
		this.clientMgr = clientMgr;
	}

}
