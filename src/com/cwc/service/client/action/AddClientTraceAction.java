package com.cwc.service.client.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.client.MarkOrderIsExistException;
import com.cwc.app.exception.client.NotDirectPayException;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.iface.ClientMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 客服服务
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddClientTraceAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private ClientMgrIFace clientMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			clientMgr.addClientTrace(request);
		}
		catch (OrderNotExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","OrderNotExistException",""));
		}
		catch (NotDirectPayException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","NotDirectPayException",""));
		}
		catch (MarkOrderIsExistException e)
		{
			DBRow result = e.getResult();
			
			String para[] = new String[]{
					result.getString("oid"),
					result.getString("account"),
			};
			
			messageAlert.setMessage(request,Resource.getStringValue("","MarkOrderIsExistException",""),para);
		}

		throw new RedirectBackUrlException();
	}

	public void setClientMgr(ClientMgrIFace clientMgr)
	{
		this.clientMgr = clientMgr;
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

}
