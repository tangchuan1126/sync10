package com.cwc.service.returnOrder.action.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.iface.gql.ReturnOrderMgrIfaceGql;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class ReturnOrderEditAction extends ActionFatherController{
	
	private ReturnOrderMgrIfaceGql returnOrderMgrGql;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String editType = StringUtil.getString(request,"editType");
		DBRow result = new DBRow();
		if("add".equals(editType)){//新增操作
			result = returnOrderMgrGql.addReturnOrder(request);
		}else if("edit".equals(editType)){//修改操作
			returnOrderMgrGql.updateReturnOrderById(request);
		}
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
		
	}


	public void setReturnOrderMgrGql(ReturnOrderMgrIfaceGql returnOrderMgrGql) {
		this.returnOrderMgrGql = returnOrderMgrGql;
	}


}
