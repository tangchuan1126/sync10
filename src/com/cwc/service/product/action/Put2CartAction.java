package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.CartIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;


/**
 * 把商品添加到购物车(ajax异步调用)
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Put2CartAction extends ActionFatherController 
{
	private CartIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			cart.put2Cart(request);
		}
		catch (ProductNotExistException e)
		{
			throw new WriteOutResponseException("ProductNotExistException");
		}
		catch (ProductNotCreateStorageException e)
		{
			throw new WriteOutResponseException("ProductNotCreateStorageException");
		}
		catch(ProductDataErrorException e)
		{
			throw new WriteOutResponseException("ProductDataErrorException");
		}
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		
		throw new WriteOutResponseException("ok");
	}

	public void setCart(CartIFace cart)
	{
		this.cart = cart;
	}
}
