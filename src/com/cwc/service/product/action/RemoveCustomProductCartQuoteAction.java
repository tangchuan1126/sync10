package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CustomCartQuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RemoveCustomProductCartQuoteAction extends ActionFatherController 
{
	private CustomCartQuoteIFace customCart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			long pid = StringUtil.getLong(request, "pid");
			long set_pid = StringUtil.getLong(request, "set_pid");
			customCart.removeProduct(StringUtil.getSession(request),set_pid,pid);
		} 
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		throw new WriteOutResponseException("ok");
	}

	public void setCustomCart(CustomCartQuoteIFace customCart)
	{
		this.customCart = customCart;
	}

}
