package com.cwc.service.product.action.zyj;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ProductMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class GetProductTraceCountByFileAndProductLineAction extends ActionFatherController {

	private ProductMgrZyjIFace productMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, 
		RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		DBRow[] results 	= productMgrZyj.getProductCountByProductLineGroupByAndFile();
		throw new JsonException(new JsonObject(results));
	}
	
	
	public void setProductMgrZyj(ProductMgrZyjIFace productMgrZyj) {
		this.productMgrZyj = productMgrZyj;
	}

}
