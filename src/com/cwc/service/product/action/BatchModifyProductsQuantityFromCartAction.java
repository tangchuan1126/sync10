package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


/**
 * 批量修改购物车商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class BatchModifyProductsQuantityFromCartAction extends ActionFatherController 
{
	private CartIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		String pids[] = request.getParameterValues("pids");
		String quantitys[] = request.getParameterValues("quantitys");
		String product_type[] = request.getParameterValues("product_type");
		
		cart.modQuantity(StringUtil.getSession(request), pids, quantitys,product_type);
		
		DBRow result = new DBRow();
		result.add("result","ok");
		throw new JsonException(new JsonObject(result));
	}

	public void setCart(CartIFace cart)
	{
		this.cart = cart;
	}


}
