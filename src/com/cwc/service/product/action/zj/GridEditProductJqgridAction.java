package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.UPCExistException;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditProductJqgridAction extends ActionFatherController {

	private ProductMgrIFaceZJ productMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException,OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作

			if(oper.equals("edit"))
			{
				productMgrZJ.modProductWithJqgrid(request);
			}
			else if(oper.equals("add"))
			{
				productMgrZJ.gridAddProduct(request);
			}
			else if(oper.equals("del"))
			{
				productMgrZJ.gridDelProduct(request);
			}
			throw new WriteOutResponseException("0");
			
			
		}
		catch (ProductCodeIsExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[This main code is exist]");
		}
		catch(ProductNameIsExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[This product name is exist]");
		}
		catch (ProductInUnionException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[This is suit, cann't be deleted]");
		}
		catch(UPCExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[UPC repeat]");
		}
		catch (ProductHasRelationStorageException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[This product is in storage, cann't be deleted]");
		}
		catch(OperationNotPermitException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Have no right]");
		}
	}
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

}
