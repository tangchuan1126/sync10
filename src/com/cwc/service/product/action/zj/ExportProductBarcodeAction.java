package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.CatalogNotExistProductException;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

public class ExportProductBarcodeAction extends ActionFatherController {

	private ProductMgrIFaceZJ productMgrZJ;
	private MessageAlerter messageAlert;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,CatalogNotExistProductException,Exception 
	{
		try 
		{
			String path = productMgrZJ.exportProductBarcode(request);
			String basePath = "";
			DBRow db = new DBRow();
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
				basePath = "../../"+ path;
			}
			else
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",basePath);
			
			throw new JsonException(new JsonObject(db));
		} 
		catch (CatalogNotExistProductException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","CatalogNotExistProductException",""));
		}
	}

	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
