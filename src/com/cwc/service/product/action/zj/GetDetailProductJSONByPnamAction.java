package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetDetailProductJSONByPnamAction extends ActionFatherController {
	
	private ProductMgrIFace productMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String pname = StringUtil.getString(request,"p_name");
		DBRow dbjson = new DBRow();
		
		DBRow product = productMgr.getDetailProductByPname(pname);
		long product_id = 0;
		String unit_name = "";
		if (product!=null) 
		{
			product_id = product.get("pc_id", 0l);
			unit_name = product.getString("unit_name");
		}
		
		dbjson.add("product_id",product_id);
		dbjson.add("unit_name",unit_name);
		
		throw new JsonException(new JsonObject(dbjson));
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
