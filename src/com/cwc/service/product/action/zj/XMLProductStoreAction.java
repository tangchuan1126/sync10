package com.cwc.service.product.action.zj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class XMLProductStoreAction extends ActionFatherController {

	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private ProductStorageMgrIFaceZJ productStorageMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream();
		String xml = "";
		
		xml = getPost(inputStream);
		long product_line_id = Long.parseLong(StringUtil.getSampleNode(xml,"id"));
		
		DBRow[] productStores = productStorageMgrZJ.xmlProductStore(product_line_id);
		
		responseXML(productStores,response);
	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private void responseXML(DBRow[] row,HttpServletResponse response)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<items>");
		
		for (int i = 0; i < row.length; i++) 
		{
			boxQtyXml.append("<item>");
			boxQtyXml.append("<sku>"+row[i].getString("sku")+"</sku>");
			boxQtyXml.append("<inv_all>"+row[i].get("store_all",0f)+"</inv_all>");
			boxQtyXml.append("<inv_bj>"+row[i].get("bj",0f)+"</inv_bj>");
			boxQtyXml.append("<inv_gz>"+row[i].get("gz",0f)+"</inv_gz>");
			boxQtyXml.append("<inv_la>"+row[i].get("la",0f)+"</inv_la>");
			boxQtyXml.append("<inv_pa>"+row[i].get("pa",0f)+"</inv_pa>");
			boxQtyXml.append("<price>"+row[i].get("price",0f)+"</price>");
			boxQtyXml.append("<weight>"+row[i].get("weight",0f)+"</weight>");
			boxQtyXml.append("<volume>"+row[i].get("volume",0f)+"</volume>");
			boxQtyXml.append("</item>");
		}
		boxQtyXml.append("</items>");
		
		//system.out.println(boxQtyXml.toString()+"----返回值");
		try 
		{
		   response.setCharacterEncoding("UTF-8");

		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

	public void setProductStorageMgrZJ(ProductStorageMgrIFaceZJ productStorageMgrZJ) {
		this.productStorageMgrZJ = productStorageMgrZJ;
	}
}
