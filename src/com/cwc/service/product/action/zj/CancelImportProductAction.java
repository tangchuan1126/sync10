package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
/**
 * 上传商品文件修改商品
 * @author Administrator
 *
 */
public class CancelImportProductAction extends ActionFatherController {

	private ProductMgrIFaceZJ productMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception
	{
		String filename = StringUtil.getString(request,"filename");
		
		productMgrZJ.delImportDB(filename);
		
		DBRow db = new DBRow();
		db.add("close",true);
		
		throw new JsonException(new JsonObject(db));
	}

	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

}
