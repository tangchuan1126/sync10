package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class GetDetailProductGridJSONByPnameAction extends ActionFatherController {

	private ProductMgrIFace productMgr;
	private CatalogMgrIFace catalogMgr;
	private ProductLineMgrIFaceTJH productLineMgrTJH;
	private ProductMgrIFaceZJ productMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String pname = StringUtil.getString(request,"p_name");
		DBRow product = productMgr.getDetailProductByPname(pname);
		
		if(product.get("union_flag",0)==0)
		{
			product.add("unionimg","<img src='../imgs/product.png'/>");
		}
		else
		{
			int unionCount = productMgrZJ.getProductUnionCountByPid(product.get("pc_id",0L));
			product.add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/><span class='badge'>"+unionCount+"</span></a>");
		}
	  	StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
	  
	  	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	  	DBRow allFather[] = tree.getAllFather(product.get("catalog_id",0l));
	  	for (int jj=0; jj<allFather.length-1; jj++)
	  	{
	  		//catalogText.append("<img src='img/folderopen.gif'/><a class='nine4' href='javascript:afilter("+allFather[jj].getString("id")+")'>"+allFather[jj].getString("title")+"</a><br/>");
	  		catalogText.append("<img src='img/folderopen.gif'/>"+allFather[jj].getString("title")+"</a><br/>");
	  		String s = (jj==0) ? "<img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
	  		catalogText.append(s);
	  	}
	  
	  	catalogText.append("</span>");
	  
	  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(product.getString("catalog_id")));
	  	if (catalog!=null)
	  	{
	  		//catalogText.append("&nbsp;&nbsp;<a href='javascript:afilter("+product.getString("catalog_id")+")'>"+catalog.getString("title")+"</a>");
	  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title"));
	  	}
	  	
	  	String alive_text;
	  	if (product.get("alive",0)==1)
		{
	      alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value=' Inactive' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-minus-sign icon-red'></i>&nbsp;Inactive</a><hidden></hidden><br/><br/>";
		}
		else
		{
	       alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value='Active' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-ok-sign icon-green'></i>&nbsp;Active</a><hidden></hidden><br/>";
		}		
	  	alive_text += "<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+product.getString("pc_id")+")\" ><i class='icon-tags'></i>&nbsp;Label</a>";
//	  	alive_text += "<input type='button' class='short-short-button' value='title' onClick=\"manage_pro_title('"+product.getString("pc_id")+"','"+product.getString("p_name")+"')\"  />";
		//?cmd=filter&pcid="+product.getString("catalog_id")+"&key=&union_flag=0
		product.add("catalog_text",catalogText.toString());
		product.add("alive_text",alive_text);
		product.remove("p_img");
		product.add("gross_profit",(product.get("gross_profit",0d)*100));
		String amozonCode = product.getString("p_code2");
		String upcCode = product.getString("upc");
		String p_codes_str = "";//"<input name='Submit43' type='button' class='short-short-button' value='条码' onclick='addProductCode("+rows[i].get("pc_id",0l)+")' title='"+pcodes+"'/>";
		if(!StringUtil.isBlank(amozonCode))
		{
			p_codes_str += "Amazon:"+amozonCode+"<br/>";
		}
		if(!StringUtil.isBlank(upcCode))
		{
			p_codes_str += "UPC:"+upcCode;
		}
		product.add("p_codes",p_codes_str);
		/*
		product.add("length", (int)product.get("length",0d)+"("+product.getString("length_uom")+")");
		product.add("width", (int)product.get("width",0d)+"("+product.getString("length_uom")+")");
		product.add("heigth", (int)product.get("heigth",0d)+"("+product.getString("length_uom")+")");
		product.add("weight", (int)product.get("weight",0d)+"("+product.getString("weight_uom")+")");
		product.add("unit_price", (int)product.get("unit_price",0d)+"("+product.getString("price_uom")+")");
		*/
		LengthUOMKey lengthUOMKey = new LengthUOMKey();
		WeightUOMKey weightUOMKey = new WeightUOMKey();
		PriceUOMKey priceUOMKey = new PriceUOMKey();
		product.add("length_uom_name", lengthUOMKey.getLengthUOMKey(product.get("length_uom", 0)));
		product.add("weight_uom_name", weightUOMKey.getWeightUOMKey(product.get("weight_uom", 0)));
		product.add("price_uom_name", priceUOMKey.getMoneyUOMKey(product.get("price_uom", 0)));
		throw new JsonException(new JsonObject(product));
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	
	public void setProductLineMgrTJH(ProductLineMgrIFaceTJH productLineMgrTJH) {
		this.productLineMgrTJH = productLineMgrTJH;
	}
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}
}
