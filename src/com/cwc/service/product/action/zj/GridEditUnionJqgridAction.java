package com.cwc.service.product.action.zj;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditUnionJqgridAction extends ActionFatherController {

	private ProductMgrIFaceZJ productMgrZJ;
	private ProductMgrIFace productMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作
			
			if(oper.equals("edit"))
			{
				productMgrZJ.modUnionWithJqgrid(request);
			}
			else if(oper.equals("add"))
			{
				productMgr.addProductUnion(request);
			}
			else if(oper.equals("del"))
			{
				productMgrZJ.delProductUnion(request);
				
			}else if(oper.equals("validate")){
				
				productMgr.validateProductSuit(request);
			}
			
			throw new WriteOutResponseException("0");
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Unit can't be a accessory.]");
		}
		catch (ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Product not found.]");
		}
		catch (ProductUnionIsInSetException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Product has been in this suit.]");
		}
		catch (ProductCantBeSetException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Product in suit can't be a suit.]");
		}
		catch (ProductCodeIsExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[This code is exist.]");
		}
		catch(OperationNotPermitException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[Have no right.]");
		}
	}
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
