package com.cwc.service.product.action.zj;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.api.tjh.ProductLineMgrTJH;
import com.cwc.app.api.zr.TransportMgrZr;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zj.ProductCodeMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductFileTypeKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class GetDetailProductGridJSONByIdAction extends ActionFatherController {

	private ProductMgrIFace productMgr;
	private CatalogMgrIFace catalogMgr;
	private ProductCodeMgrIFaceZJ productCodeMgrZJ;
	private SystemConfig systemConfig;
	private TransportMgrIfaceZr transportMgrZr;
	private ProductLineMgrIFaceTJH productLineMgrTJH;
	private ProductMgrIFaceZJ productMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		long pc_id = StringUtil.getLong(request,"pc_id");
		DBRow product = productMgr.getDetailProductByPcid(pc_id);
		
		if(product.get("union_flag",0)==0)
		{
			product.add("unionimg","<img src='../imgs/product.png'/>");
		}
		else
		{
			int unionCount = productMgrZJ.getProductUnionCountByPid(pc_id);
			product.add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/><span class='badge'>"+unionCount+"</span></a>");
		}
		StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
	  	String product_line_name = "";
	  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(product.getString("catalog_id")));
	  /*	if(null != catalog)
	  	{
	  		long product_line_id = catalog.get("product_line_id", 0L);
	  		DBRow productLine = productLineMgrTJH.getProductLineById(product_line_id);
	  		if(null != productLine)
	  		{
	  			product_line_name = productLine.getString("name");
	  			catalogText.append("<img src='img/folderopen.gif'/> <a class='nine4' href='javascript:void(0)'>"+product_line_name+"</a><br/>");
		  		String s = "<img src='img/joinbottom.gif'/>";
		  		catalogText.append(s);
	  		}
	  	}*/
	  	Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
	  	DBRow allFather[] = tree.getAllFather(product.get("catalog_id",0l));
	  	for (int jj=0; jj<allFather.length-1; jj++)
	  	{
	  		catalogText.append("<img src='img/folderopen.gif'/> <a class='nine4' href='javascript:afilter("+allFather[jj].getString("id")+")'>"+allFather[jj].getString("title")+"</a><br/>");
	  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
	  		catalogText.append(s);
	  	}
	  	
	  	if (catalog!=null)
	  	{
	  		catalogText.append("<img  src='img/page.gif'/><a href='javascript:afilter("+product.getString("catalog_id")+")'>"+catalog.getString("title")+"</a>");
	  	}
	  	
	  	String alive_text;
	  	if (product.get("alive",0)==1)
		{
	      alive_text = "<a name='Submit43' style='width:57px;'  class='buttons' value=' Inactive' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-minus-sign icon-red'></i>&nbsp;Inactive</a><hidden></hidden><br/></br/>";
		}
		else
		{
	       alive_text = "<a name='Submit43' style='width:57px;'  class='buttons' value='Active' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-ok-sign icon-green'></i>&nbsp;Active</a><hidden></hidden><br/></br/>";
		}		
		alive_text += "<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+product.getString("pc_id")+")\"  ><i class='icon-tags'></i>&nbsp;Label</a><hidden></hidden><br/></br/>"+
		       		  "<a class='buttons' style='width:57px;' value='Manage Codes' onClick=\"manageCodes(this);\"><i class='icon-barcode'></i>&nbsp;Codes</a>";
	  	//alive_text += "<input type='button' class='short-short-button' value='title' onClick=\"manage_pro_title('"+product.getString("pc_id")+"','"+product.getString("p_name")+"')\"  />";
		//?cmd=filter&pcid="+product.getString("catalog_id")+"&key=&union_flag=0
		product.add("catalog_text",catalogText.toString());
		product.add("alive_text",alive_text);
		product.remove("p_img");
		StringBuffer html =  new StringBuffer();
		html.append("<ul class='myul'>");
		// 显示出来商品文件的个数 和 商品标签的个数
		/*
		String value = systemConfig.getStringConfigValue("transport_product_file");
  		String[] arraySelected = value.split("\n");
  		ArrayList<String> selectedList= new ArrayList<String>();
  		for(String tempSelect : arraySelected){
  				if(tempSelect.indexOf("=") != -1){
  					String[] tempArray = tempSelect.split("=");
  					String tempHtml = tempArray[1];
  					selectedList.add(tempHtml);
  				}
  		}*/
//		ArrayList<String> selectedList= new ProductFileTypeKey().getProductFileTypeValue();
//	 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(pc_id,pc_id,FileWithTypeKey.PRODUCT_SELF_FILE);
//	 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
//	 		DBRow tempCount = productFileMap.get(indexOfList+1);
//	 		int tempCountNumber = 0 ;
// 			tempCountNumber = null != tempCount?tempCount.get("count",0):0;
// 				html.append("<li style='width:50px;'>");
//				html.append(selectedList.get(indexOfList).trim());
//				html.append(":");
//				html.append(tempCountNumber);
//				html.append("</li>");
//				/*
//				if(1 == indexOfList%2)
//				{
//					html.append("<br/>");
//				}
//				else
//				{
//					html.append(" ");
//				}*/
//	 	}
		ProductFileTypeKey productFileTypeKey = new ProductFileTypeKey();
  		ArrayList<String> selectedList= productFileTypeKey.getProductFileTypesKeys();
	 	Map<Integer,DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(pc_id,pc_id,FileWithTypeKey.PRODUCT_SELF_FILE);
	 	for(int indexOfList = 0 , countOfList = selectedList.size() ; indexOfList < countOfList ;indexOfList++ ){
	 		DBRow tempCount = productFileMap.get(Integer.valueOf(selectedList.get(indexOfList)));//indexOfList+1
	 		int tempCountNumber = 0 ;
 			tempCountNumber = null != tempCount?tempCount.get("count",0):0;
 			html.append("<li style='width:50px;'>");
				html.append(productFileTypeKey.getProductFileTypesKeyValue(selectedList.get(indexOfList)));
				html.append(":");
				html.append(tempCountNumber);
				html.append("</li>");

	 	}
		html.append("</ul>");
	 	product.add("img",html.toString());
		
		
		product.add("gross_profit",(product.get("gross_profit",0d)*100));
		
		DBRow[] productCodes = productCodeMgrZJ.getUseProductCodes(pc_id);
		String pcodes = "";
		for(int j=0;j<productCodes.length;j++)
		{
			pcodes += productCodes[j].getString("p_code");
			
			if(j<productCodes.length-1)
			{
				pcodes +="\n";
			}
		}
		String amozonCode = product.getString("p_code2");
		String upcCode = product.getString("upc");
		String p_codes_str = "";//"<input name='Submit43' type='button' class='short-short-button' value='条码' onclick='addProductCode("+rows[i].get("pc_id",0l)+")' title='"+pcodes+"'/>";
		if(!StringUtil.isBlank(amozonCode))
		{
			p_codes_str += "Amazon:"+amozonCode+"<br/>";
		}
		if(!StringUtil.isBlank(upcCode))
		{
			p_codes_str += "UPC:"+upcCode;
		}
		product.add("p_codes",p_codes_str);
		/*
		product.add("length", (int)product.get("length",0d)+"("+product.getString("length_uom")+")");
		product.add("width", (int)product.get("width",0d)+"("+product.getString("length_uom")+")");
		product.add("heigth", (int)product.get("heigth",0d)+"("+product.getString("length_uom")+")");
		product.add("weight", (int)product.get("weight",0d)+"("+product.getString("weight_uom")+")");
		product.add("unit_price", (int)product.get("unit_price",0d)+"("+product.getString("price_uom")+")");
		*/
		LengthUOMKey lengthUOMKey = new LengthUOMKey();
		WeightUOMKey weightUOMKey = new WeightUOMKey();
		PriceUOMKey priceUOMKey = new PriceUOMKey();
		product.add("length_uom_name", lengthUOMKey.getLengthUOMKey(product.get("length_uom", 0)));
		product.add("weight_uom_name", weightUOMKey.getWeightUOMKey(product.get("weight_uom", 0)));
		product.add("price_uom_name", priceUOMKey.getMoneyUOMKey(product.get("price_uom", 0)));
		
		String length = product.getString("length");
		String width = product.getString("width");
		String heigth = product.getString("heigth");
		String weight = product.getString("weight");
		String unit_price = product.getString("unit_price");
		String volume = product.getString("volume");
		
		if(!"".equals(length) && length != null && length.matches("[0-9]\\d*\\.?00*")){
			product.put("length",length.substring(0,length.indexOf(".")));
		}
		if(!"".equals(width) && width != null && width.matches("[0-9]\\d*\\.?00*")){
			product.put("width",width.substring(0,width.indexOf(".")));
		}
		if(!"".equals(heigth) && heigth != null && heigth.matches("[0-9]\\d*\\.?00*")){
			product.put("heigth",heigth.substring(0,heigth.indexOf(".")));
		}
		if(!"".equals(weight) && weight != null && weight.matches("[0-9]\\d*\\.?00*")){
			product.put("weight",weight.substring(0,weight.indexOf(".")));
		}
		if(!"".equals(unit_price) && unit_price != null && unit_price.matches("[0-9]\\d*\\.?00*")){
			product.put("unit_price",unit_price.substring(0,unit_price.indexOf(".")));
		}
		if(!"".equals(volume) && volume != null && volume.matches("[0-9]\\d*\\.?00*")){
			product.put("volume",volume.substring(0,volume.indexOf(".")));
		}
		
		
		throw new JsonException(new JsonObject(product));
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
	public void setProductCodeMgrZJ(ProductCodeMgrIFaceZJ productCodeMgrZJ) {
		this.productCodeMgrZJ = productCodeMgrZJ;
	}
	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
	public void setProductLineMgrTJH(ProductLineMgrIFaceTJH productLineMgrTJH) {
		this.productLineMgrTJH = productLineMgrTJH;
	}
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}
}
