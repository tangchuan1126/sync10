package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.product.LEVEL2ToLEVEL1ParentCanNotDefineException;
import com.cwc.app.iface.zj.ProductCatalogMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class MoveProductCatalogAction extends ActionFatherController {

	private ProductCatalogMgrIFaceZJ productCatalogMgrZJ;
	private MessageAlerter messageAlert;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		try 
		{
			productCatalogMgrZJ.moveCatalog(request);
		}
		catch(LEVEL2ToLEVEL1ParentCanNotDefineException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","CatalogCanNotMoveTheSonCatalogException",""));
		}
		catch(SuperAdminException e)
		{
			messageAlert.setMessage(request,"Cannot move to this category.");
		}
		
		throw new RedirectRefException();
	}

	public void setProductCatalogMgrZJ(ProductCatalogMgrIFaceZJ productCatalogMgrZJ) {
		this.productCatalogMgrZJ = productCatalogMgrZJ;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
