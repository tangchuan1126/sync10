package com.cwc.service.product.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class GetUnionProductDetailsGridJSONByPidAction extends
		ActionFatherController {
	
	private ProductMgrIFaceZJ productMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow[] unionProduct = productMgrZJ.getUnionProduct(request);
		for (int i = 0; i < unionProduct.length; i++)
		{
			unionProduct[i].add("length", (int)unionProduct[i].get("length",0d)+"("+unionProduct[i].getString("length_uom")+")");
			unionProduct[i].add("width", (int)unionProduct[i].get("width",0d)+"("+unionProduct[i].getString("length_uom")+")");
			unionProduct[i].add("heigth", (int)unionProduct[i].get("heigth",0d)+"("+unionProduct[i].getString("length_uom")+")");
			unionProduct[i].add("weight", (int)unionProduct[i].get("weight",0d)+"("+unionProduct[i].getString("weight_uom")+")");
			unionProduct[i].add("unit_price", (int)unionProduct[i].get("unit_price",0d)+"("+unionProduct[i].getString("price_uom")+")");
			unionProduct[i].add("gross_profit",unionProduct[i].get("gross_profit",0d)*100);
//			//system.out.println(unionProduct[i].getString("weight"));
//			//system.out.println(unionProduct[i].getString("unit_price"));
		}
		
		throw new JsonException(new JsonObject(unionProduct));

	}
	
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

}
