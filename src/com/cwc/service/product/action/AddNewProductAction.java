package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

/**
 * 增加品名条码
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddNewProductAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private ProductMgrIFace productMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			DBRow product = productMgr.addProduct(request);
			product.add("status", "success");
			throw new JsonException(new JsonObject(product));
		}
		catch (ProductNameIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ProductNameIsExistException",""));
		}
		catch (ProductCodeIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ProductCodeIsExistException",""));
		}

		//throw new RedirectRefException();
		
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}


}
