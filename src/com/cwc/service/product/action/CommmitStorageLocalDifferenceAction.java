package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.StorageApproveIsExistException;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 提交库存差异
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CommmitStorageLocalDifferenceAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private ProductMgrIFace productMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			productMgr.commmitStorageLocalDifference(request);
		}
		catch (StorageApproveIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","StorageApproveIsExistException",""));
		}

		throw new RedirectBackUrlException();
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}


}
