package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 通过货架ID获得JSON格式的商品数据
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetProductsByCidJSONAction extends ActionFatherController 
{
	private ProductMgrIFace productMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long product_catalog_id = StringUtil.getLong(request, "product_catalog_id");
		DBRow products[] = productMgr.getProductByCid(product_catalog_id,null);
		throw new JsonException(new JsonObject(products));
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}


}
