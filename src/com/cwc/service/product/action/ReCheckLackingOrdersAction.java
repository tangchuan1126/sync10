package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 对现有缺货订单进行重新抄单，重新计算库存
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ReCheckLackingOrdersAction extends ActionFatherController 
{
	private ProductMgrIFace productMgr;
	private WayBillMgrIFaceZJ wayBillMgrZJ;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			//productMgr.reCheckLackingOrders(request);
			long ps_id = StringUtil.getLong(request,"ps_id");
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			wayBillMgrZJ.reCheckLackingWayBillOrdersSub(adminLoggerBean,ps_id);
			
		}
		catch (Exception e)
		{
			throw new WriteOutResponseException("error");
		}
		
		throw new WriteOutResponseException("ok");
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}


}
