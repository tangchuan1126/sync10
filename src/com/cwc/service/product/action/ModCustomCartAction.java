package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CustomCartIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;


/**
 * 修改定制购物车商品数量
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ModCustomCartAction extends ActionFatherController 
{
	private CustomCartIFace customCart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			customCart.modQuantity(request);
		} 
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		throw new WriteOutResponseException("ok");
	}

	public void setCustomCart(CustomCartIFace customCart)
	{
		this.customCart = customCart;
	}

}
