package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartReturnIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class BatchModifyProductsQuantityFromCarReturnAction extends ActionFatherController 
{
	private CartReturnIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		String pids[] = request.getParameterValues("pids");
		String quantitys[] = request.getParameterValues("quantitys");
		String product_type[] = request.getParameterValues("product_type");
		
		cart.modQuantity(StringUtil.getSession(request), pids, quantitys,product_type);
		
		throw new WriteOutResponseException("ok");
	}

	public void setCart(CartReturnIFace cart)
	{
		this.cart = cart;
	}


}
