package com.cwc.service.product.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class SnLengthAction extends ActionFatherController {
	
	private ProductMgrIFace productMgr;
	
	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		DBRow ret = productMgr.updateProductSn(request);
		JSONObject output = new JSONObject(ret);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
		
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
