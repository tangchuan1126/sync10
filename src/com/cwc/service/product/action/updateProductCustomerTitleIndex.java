package com.cwc.service.product.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.util.Version;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.asynchronized.ThreadManager;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilIFace;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.lucene.BatchUpdateProductIndex;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class updateProductCustomerTitleIndex extends ActionFatherController {
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		String q = StringUtil.getString(request, "pid");
		
		try
		{
			ThreadManager threadManager = ThreadManager.getInstance();
			DBUtilIFace dbUtil = (DBUtilIFace)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
			
			IndexCore indexMgr = ProductIndexMgr.getInstance();

			BatchUpdateProductIndex threadAction = new BatchUpdateProductIndex(dbUtil,indexMgr,new WhitespaceAnalyzer(Version.LUCENE_40),Integer.parseInt(q));
			//pageSize属性是决定一次从数据库中读取记录的单位，
			//如果使用新的merge方法，也就是一次批量更新索引的粒度，
			//由于批量更新索引会synchronize indexMgr，所以如果使用新merge方法，这个批量不宜过大
			//如果不使用新的merge方法，则pageSize可以适当加大，以提高数据读取效率
			threadAction.setPageSize(2000);
			threadManager.executeInBackground(threadAction);
			
			DBRow data = new DBRow();
			data.add("flag", "true");
			data.add("result", "Successful!");
			
			throw new JsonException(new JsonObject(data));
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}	
	}
}
