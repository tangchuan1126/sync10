package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得JSON格式的组合关系详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailProductUnionJSONAction extends ActionFatherController 
{
	private ProductMgrIFace productMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long set_pid = StringUtil.getLong(request, "set_pid");
		long pid = StringUtil.getLong(request, "pid");
		
		DBRow detail = productMgr.getDetailProductInSet( set_pid, pid);
		throw new JsonException(new JsonObject(detail));
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}


}
