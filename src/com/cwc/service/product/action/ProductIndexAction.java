package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class ProductIndexAction extends ActionFatherController {
	
	private ProductMgrIFace productMgr;
	
	private AccountMgrIFaceSbb accountMgr;
	
	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}
	
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		long id = Long.parseLong(StringUtils.defaultString(request.getParameter("id"), "0"));
		
		DBRow result = productMgr.updateProductIndex(id);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws Exception {
		
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow row = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		//EDI 登录验证
		String account = "EDI001";
		
		DBRow admin = accountMgr.getAccountByAccount(account);
		
		DBRow result = new DBRow();
		
		if(admin != null && row.get("token") != null && admin.get("pwd","").equals(StringUtil.getMD5(row.get("token").toString()))){
			
			result = productMgr.updateProductIndex(row);
			
		}else{
			
			result.put("error", "Login failed");
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)throws Exception {
		
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
	}

	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
}
