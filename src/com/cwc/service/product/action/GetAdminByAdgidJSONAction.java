package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获取用户组下的用户 <b>Application describing:</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 <br>
 * <b>Company:vvme</b><br>
 * <b>Date:</b>2014-12-18下午6:02:07<br>
 * 
 * @author cuicong
 */
public class GetAdminByAdgidJSONAction extends ActionFatherController {
	private AdminMgrIFace adminMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws JsonException,
			WriteOutResponseException, RedirectRefException, ForwardException, Forward2JspException,
			RedirectBackUrlException, RedirectException, OperationNotPermitException, Exception {
		long adgid = StringUtil.getLong(request, "adgid");
		DBRow[] dbRows = adminMgr.getAdminByAdgid(adgid, new PageCtrl());
		throw new JsonException(new JsonObject(dbRows));
	}
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
}
