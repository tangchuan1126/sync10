package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.iface.CustomCartWarrantyIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;


/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PutCustomProduct2CartWarrantyAction extends ActionFatherController 
{
	private CustomCartWarrantyIFace customCart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			customCart.put2Cart(request);
		} 
		catch (ProductNotCreateStorageException e)
		{
			throw new WriteOutResponseException("ProductNotCreateStorageException");
		}
		catch (ProductNotExistException e)
		{
			throw new WriteOutResponseException("ProductNotExistException");
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw new WriteOutResponseException("ProductUnionSetCanBeProductException");
		}

		throw new WriteOutResponseException("ok");
	}

	public void setCustomCart(CustomCartWarrantyIFace customCart)
	{
		this.customCart = customCart;
	}

}
