package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 从购物车删除商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RemoveProductFromCartQuoteAction extends ActionFatherController 
{
	private CartQuoteIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long pid = StringUtil.getLong(request,"pid");
		int product_type = StringUtil.getInt(request, "product_type");
		
		cart.removeProduct(StringUtil.getSession(request), pid,product_type);
		
		throw new WriteOutResponseException("ok");
	}

	public void setCart(CartQuoteIFace cart)
	{
		this.cart = cart;
	}
}
