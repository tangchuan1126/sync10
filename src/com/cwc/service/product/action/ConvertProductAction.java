package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ConvertNotBeSelfException;
import com.cwc.app.exception.product.ConvertNotSameTypeException;
import com.cwc.app.exception.product.ConvertQuantityIncorrectException;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 转换商品库存
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ConvertProductAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private ProductMgrIFace productMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			productMgr.convertProduct(request);
		}
		catch (ProductNotCreateStorageException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ProductNotCreateStorageException",""));
		}
		catch (ConvertNotSameTypeException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ConvertNotSameTypeException",""));
		}
		catch (ConvertNotBeSelfException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ConvertNotBeSelfException",""));
		}
		catch (ProductNotExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ProductNotExistException",""));
		}
		catch (ConvertQuantityIncorrectException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ConvertQuantityIncorrectException",""));
		}
		
		throw new RedirectRefException();
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}


}
