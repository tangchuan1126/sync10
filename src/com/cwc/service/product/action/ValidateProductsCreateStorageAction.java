package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.iface.CartIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 验证购物车商品是否已经建库
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ValidateProductsCreateStorageAction extends ActionFatherController 
{
	private ProductMgrIFace productMgr;
	private CartIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			String p_name = StringUtil.getString(request, "p_name");
			long ps_id = StringUtil.getLong(request, "ps_id");
			DBRow products[] = null;
			               
			if (p_name.equals(""))//直接提交一个商品名验证
			{
					products = cart.getSimpleProducts(StringUtil.getSession(request));
			}
			else
			{
				//如果输入不存在的商品，让其通过，留到保存定制的时候检查
				DBRow detailP = productMgr.getDetailProductByPname(p_name);
				if (detailP==null)
				{
					throw new WriteOutResponseException("ok");
				}
				
				DBRow product = new DBRow();
				product.add("cart_pid", detailP.get("pc_id", 0l));
				product.add("cart_product_type", 0);
				products = new DBRow[] {product} ;
			}

			productMgr.validateProductsCreateStorage(ps_id,products);
		}
		catch (ProductNotCreateStorageException e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		
		throw new WriteOutResponseException("ok");
	}

	public void setProductMgr(ProductMgrIFace productMgr)
	{
		this.productMgr = productMgr;
	}
	
	public void setCart(CartIFace cart)
	{
		this.cart = cart;
	}

}
