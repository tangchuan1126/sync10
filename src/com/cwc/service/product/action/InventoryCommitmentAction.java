package com.cwc.service.product.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import us.monoid.json.JSONArray;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.InventoryCommitmentMgrIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**  
 * @author Liang Jie
 * Created on 2015年1月4日
 * 
 */
public class InventoryCommitmentAction extends ActionFatherController {

	private InventoryCommitmentMgrIFace inventoryCommitmentMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {		
		String companyId = request.getParameter("companyId");
		String customerId = request.getParameter("customerId");
		String[] dnList = request.getParameterValues("dnList[]");
		if(companyId==null||customerId==null||dnList==null||dnList.length<1){
			throw new SendResponseServerCodeAndMessageException(50000,"Wrong parameter offered!");
		}
		String[] resultArr = inventoryCommitmentMgr.commitInventory(request);
		if(resultArr == null || StringUtil.isBlank(resultArr[0])){
			throw new SendResponseServerCodeAndMessageException(50000,"No excel report generated!");
			
		}else{
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(new JSONArray(resultArr).toString());
		}
	}
	
	/**
	 * @param inventoryCommitmentMgr the inventoryCommitmentMgr to set
	 */
	public void setInventoryCommitmentMgr(
			InventoryCommitmentMgrIFace inventoryCommitmentMgr) {
		this.inventoryCommitmentMgr = inventoryCommitmentMgr;
	}

}
