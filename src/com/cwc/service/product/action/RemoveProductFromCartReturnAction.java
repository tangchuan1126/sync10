package com.cwc.service.product.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartReturnIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


/**
 * 从购物车删除商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RemoveProductFromCartReturnAction extends ActionFatherController 
{
	private CartReturnIFace cart;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long pid = StringUtil.getLong(request,"pid");
		int product_type = StringUtil.getInt(request,"product_type",-1);
		
		if(product_type<0)
		{
			cart.removeProductSelect(StringUtil.getSession(request), pid);
		}
		else
		{
			cart.removeProduct(StringUtil.getSession(request), pid,product_type);
		}
		throw new WriteOutResponseException("ok");
	}

	public void setCart(CartReturnIFace cart)
	{
		this.cart = cart;
	}
}
