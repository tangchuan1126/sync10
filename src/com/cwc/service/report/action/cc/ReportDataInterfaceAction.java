package com.cwc.service.report.action.cc;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.ReportDataInterfaceMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.cwc.util.XmlUtil;

public class ReportDataInterfaceAction extends ActionFatherController {
	
	private ReportDataInterfaceMgrIfaceCc reportDataInterfaceMgrCc;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		DBRow result = new DBRow();
		result.add("success", true);
		result.add("error", "");
		result.add("totle", 0);
		
		
		boolean stringResult = false;	//此标记为true时，返回resultStr
		String resultStr = null;
		
		try {
			String method = StringUtil.getString(request, "method");
			if("getItemsByCustomerId".equalsIgnoreCase(method)){
				String customerId = StringUtil.getString(request, "customer_id");
				if(!StringUtil.isBlank(customerId)){
					DBRow[] data = reportDataInterfaceMgrCc.getItemsByCustomerId(customerId);
					result.add("data", data);
					result.add("totle", data.length);
				}
			}
			if("getIntradayReceiveByCustomerId".equalsIgnoreCase(method)){
				String customerId = StringUtil.getString(request, "customer_id");
				String companyId = StringUtil.getString(request, "company_id");
				String dateStr = StringUtil.getString(request, "date");
				
				if(!StringUtil.isBlank(customerId)){
					DBRow[] data = reportDataInterfaceMgrCc.getIntradayReceiveByCustomerId(customerId, companyId, dateStr);
					result.add("data", data);
					result.add("totle", data.length);
				}
			}
			if("xmlRequest".equalsIgnoreCase(method)){
				stringResult = true;
				resultStr = doXmlReq(request);
			}
		} catch (Exception e) {
			result.add("success", false);
			result.add("error", "Query Failed");
		}
		
		if(stringResult){
			throw new JsonException(resultStr);
		}else {
			throw new JsonException(new JsonObject(result));
		}
	}
	
	/**
	 * 处理xml参数的报表
	 * @param request
	 * @return	返回jsonString
	 * @throws Exception
	 */
	private String doXmlReq(HttpServletRequest request) throws Exception{
		String result = null;
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = getPost(inputStream);
		DBRow row = XmlUtil.initDataXmlStringToDBRow(xml,new String[]{"Sheet","Content"});
		String sheet = row.getString("sheet");
		if("AppointmentCount".equalsIgnoreCase(sheet)){
			Map<String, String[]> map = new HashMap<String, String[]>();
			String content = row.getString("content");
			String[] ps = content.split(";");
			for (String s : ps) {
				String[] kv = s.split(":");
				map.put(kv[0], kv[1].split(","));
			}
			Map data = reportDataInterfaceMgrCc.appointmentCount(map);
			if(!data.isEmpty()){
				result = new JSONObject(data).toString();
			}
		}
		
		if(result == null){
			Map m = new HashMap<>();
			m.put("result", "no data");
			result = new JSONObject(m).toString();
		}
		return result;
	}
	
	private String getPost(InputStream inputStream) throws Exception {
		BufferedInputStream input = null; // 输入流,用于接收请求的数据
		byte[] buffer = new byte[1024]; // 数据缓冲区
		int count = 0; // 每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream(); // 请求数据存放对象
		byte[] iXMLData = null;
		try {
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1) {
				streamXML.write(buffer, 0, count);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (Exception f) {
					f.printStackTrace();
				}
			}
		}
		iXMLData = streamXML.toByteArray();

		return (new String(iXMLData));
	}

	public void setReportDataInterfaceMgrCc(
			ReportDataInterfaceMgrIfaceCc reportDataInterfaceMgrCc) {
		this.reportDataInterfaceMgrCc = reportDataInterfaceMgrCc;
	}


}
