package com.cwc.service.order.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class ModOrderZipAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		long order_id = StringUtil.getLong(request,"order_id");
		String zip = StringUtil.getString(request,"address_zip");
		int refresh = StringUtil.getInt(request,"refresh");
		
		orderMgr.modAddressZip(order_id,zip);
		
		String url = StringUtil.getString(request,"backurl")+"&refresh="+refresh;
		throw new RedirectException(url);
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
}
