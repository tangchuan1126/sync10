package com.cwc.service.order.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.OrderMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxBadReviewCountAction extends ActionFatherController {

	private OrderMgrIFaceZJ orderMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String client_id = StringUtil.getString(request,"client_id");
		
		DBRow data = orderMgrZJ.getOrderBadReview(client_id);
		
		throw new JsonException(new JsonObject(data));
		
	}

	public void setOrderMgrZJ(OrderMgrIFaceZJ orderMgrZJ) {
		this.orderMgrZJ = orderMgrZJ;
	}

}
