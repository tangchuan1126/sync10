package com.cwc.service.order.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 新的抄单 <b>Application describing:</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 <br>
 * <b>Company:vvme</b><br>
 * <b>Date:</b>2014-12-8下午3:05:54<br>
 * 
 * @author cuicong
 */
public class NewRecordOrderAction1 extends ActionFatherController {
	private OrderMgrIFace orderMgr;
	private MessageAlerter messageAlert;
	
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, Exception {
		orderMgr.recordOrderTrue(request);
		long oid = StringUtil.getLong(request, "oid");
		orderMgr.initCartFromOrder(request, oid);
		
		throw new WriteOutResponseException("OK");
	}
	
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	
}
