package com.cwc.service.order.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.DoubtGoodsMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.app.iface.zyj.ProductMgrZyjIFace;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetMonitorTraceCountJSonAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private TransportMgrIFaceZJ transportMgrZJ;
	private PurchaseMgrZyjIFace purchaseMgrZyj;
	private ProductMgrZyjIFace productMgrZyj;
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
	private DoubtGoodsMgrIFaceZJ doubtGoodsMgrZJ;
	private ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		DBRow result = orderMgr.getMonitorTraceCount(request);
		DBRow result2 = transportMgrZJ.trackDeliveryAndTransportCountFirstMenu();
		DBRow result3 = purchaseMgrZyj.getPurchaseNeedFollowUpCount(-1);
		DBRow result4 = productMgrZyj.getProductCountTotalByFileType();
		DBRow resultQuestionCount=questionCatalogMgrZwb.getQuestionCountByStatus();
		int resultNeedAnswerDoubtCount = doubtGoodsMgrZJ.waitAnswerDoubtGoodCount();
		DBRow returnOrderCountRow = returnProductOrderMgrZyj.statReturnProductOrderNeedHandleCount();
		
		
		ArrayList<String> list = result2.getFieldNames();
		for (int i = 0; i < list.size(); i++) 
		{
			result.add(list.get(i),result2.getString(list.get(i)));
		}
		
		ArrayList<String> list2 = result3.getFieldNames();
		for (int i = 0; i < list2.size(); i++) 
		{
			result.add(list2.get(i),result3.getString(list2.get(i)));
		}
		
		ArrayList<String> list3 = result4.getFieldNames();
		for (int i = 0; i < list3.size(); i++) 
		{
			result.add(list3.get(i), result4.getString(list3.get(i)));
		}
		result.add("questionCount", resultQuestionCount.get("count",0l));
		result.add("need_answer_doubt",resultNeedAnswerDoubtCount);
		
		ArrayList<String> returnOrderCountList = returnOrderCountRow.getFieldNames();
		for (int i = 0; i < returnOrderCountList.size(); i++) 
		{
			result.add(returnOrderCountList.get(i), returnOrderCountRow.getString(returnOrderCountList.get(i)));
		}
		throw new JsonException(new JsonObject(result));
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}

	public void setProductMgrZyj(ProductMgrZyjIFace productMgrZyj) {
		this.productMgrZyj = productMgrZyj;
	}

	public void setQuestionCatalogMgrZwb(
			QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}

	public void setDoubtGoodsMgrZJ(DoubtGoodsMgrIFaceZJ doubtGoodsMgrZJ) {
		this.doubtGoodsMgrZJ = doubtGoodsMgrZJ;
	}

	public void setReturnProductOrderMgrZyj(
			ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj) {
		this.returnProductOrderMgrZyj = returnProductOrderMgrZyj;
	}

    
}
