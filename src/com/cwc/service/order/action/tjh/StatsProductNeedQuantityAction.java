package com.cwc.service.order.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class StatsProductNeedQuantityAction extends ActionFatherController{

	private OrderProcessMgrIFaceTJH orderProcessMgr;
	private PageCtrl pc;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
			String start_date = StringUtil.getString(request, "start_date_need");
			String end_date = StringUtil.getString(request, "end_date_need");
			long cid = StringUtil.getLong(request, "ps_id");
			orderProcessMgr.statcProcuctNeedQuantityZJ(start_date, end_date, cid);
			
			DBRow cc = new DBRow();
			cc.add("result",true);
			throw new JsonException(new JsonObject(cc));
		
	}
	public void setOrderProcessMgr(OrderProcessMgrIFaceTJH orderProcessMgr) {
		this.orderProcessMgr = orderProcessMgr;
	}
	public void setPc(PageCtrl pc) {
		this.pc = pc;
	}

	
}
