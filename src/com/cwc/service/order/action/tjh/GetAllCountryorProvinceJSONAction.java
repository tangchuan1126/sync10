package com.cwc.service.order.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetAllCountryorProvinceJSONAction extends ActionFatherController{

	private OrderProcessMgrIFaceTJH orderProcessMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		
		long catalog_id = StringUtil.getLong(request, "catalog_id");
		long p_line_id = StringUtil.getLong(request, "pro_line_id");
		String p_name = StringUtil.getString(request, "product_name");
		String st_date = StringUtil.getString(request, "start_date");
		String end_date = StringUtil.getString(request, "end_date");
		
		orderProcessMgr.statsProductSpreadByMaps(catalog_id,p_line_id,p_name,st_date,end_date);
		throw new RedirectRefException();
	}
	public void setOrderProcessMgr(OrderProcessMgrIFaceTJH orderProcessMgr) {
		this.orderProcessMgr = orderProcessMgr;
	}

	
}
