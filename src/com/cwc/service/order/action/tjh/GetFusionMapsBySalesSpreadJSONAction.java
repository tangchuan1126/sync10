package com.cwc.service.order.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetFusionMapsBySalesSpreadJSONAction extends ActionFatherController{

	private OrderProcessMgrIFaceTJH orderProcessMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String fusion_id = StringUtil.getString(request, "fusion_id");
		long  catalog = StringUtil.getLong(request, "catalog_id");
		long p_line_id = StringUtil.getLong(request, "p_line_id");
		long product_id = StringUtil.getLong(request, "product_id");
		String start_date = StringUtil.getString(request, "start_date");
		String end_date = StringUtil.getString(request, "end_date");
		
		orderProcessMgr.getProvinceSpreadbyMap(fusion_id,catalog,p_line_id,product_id,start_date,end_date);
		
		DBRow row = orderProcessMgr.getCountryMapFlashByFusionId(fusion_id);
		throw new JsonException(new JsonObject(row));
		
	}
	public void setOrderProcessMgr(OrderProcessMgrIFaceTJH orderProcessMgr) {
		this.orderProcessMgr = orderProcessMgr;
	}

	
}
