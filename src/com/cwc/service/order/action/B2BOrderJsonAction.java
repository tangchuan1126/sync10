package com.cwc.service.order.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.WMSOrderMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorCheckInMgrZr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.WMSOrderMgrIFace;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.key.B2BOrderLogTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.key.WmsOrderUpdateTypeKey;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Attach;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.lucene.TaskB2BOrderIndex;
import com.cwc.util.StringUtil;
import com.vvme.order.util.CommonUtils;
import com.vvme.wms.Receipt;
import com.vvme.wms.ReceiptLine;
import com.vvme.wms.ScheduledTasks;

/**
 * 服务接口 ，B2b订单，以json格式 <b>Application describing:</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 <br>
 * <b>Company:vvme</b><br>
 * <b>Date:</b>2014-12-23下午8:28:04<br>
 * 
 * @author cuicong
 */

public class B2BOrderJsonAction extends ActionFatherController {
	static Logger log = Logger.getLogger("PLATFORM");
	
	private B2BOrderMgrIFaceZyj b2BOrderMgrZyj;
	private AdminMgrIFace adminMgr;// admin数据
	private ProductStorageMgrIFaceZJ productStorageMgrIFaceZJ;// 仓库数据
	private OrderMgrIFace orderMgrIFace;
	private TransportMgrIFaceZJ transportMgrIFaceZJ;
	private ProductMgrIFace productMgrIFace;
	private ShipToMgrIFaceZJ shipToMgrZJ;
	private FloorCheckInMgrZr floorCheckInMgrZr;
	private WMSOrderMgr wmsOrderMgr;
	private BoxTypeMgrIfaceZr boxTypeMgrZr;
	private WMSOrderMgrIFace proxyWMSOrderMgr;
	private ScheduledTasks b2bScheduledTasks;

	public void setB2bScheduledTasks(ScheduledTasks b2bScheduledTasks) {
		this.b2bScheduledTasks = b2bScheduledTasks;
	}

	public void setProxyWMSOrderMgr(WMSOrderMgrIFace proxyWMSOrderMgr) {
		this.proxyWMSOrderMgr = proxyWMSOrderMgr;
	}

	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}
	
	public void setFloorCheckInMgrZr(FloorCheckInMgrZr floorCheckInMgrZr) {
		this.floorCheckInMgrZr = floorCheckInMgrZr;
	}

	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
	public void setProductMgrIFace(ProductMgrIFace productMgrIFace) {
		this.productMgrIFace = productMgrIFace;
	}
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
	public void setB2BOrderMgrZyj(B2BOrderMgrIFaceZyj b2bOrderMgrZyj) {
		b2BOrderMgrZyj = b2bOrderMgrZyj;
	}
	
	public void setProductStorageMgrIFaceZJ(ProductStorageMgrIFaceZJ productStorageMgrIFaceZJ) {
		this.productStorageMgrIFaceZJ = productStorageMgrIFaceZJ;
	}
	
	public void setOrderMgrIFace(OrderMgrIFace orderMgrIFace) {
		this.orderMgrIFace = orderMgrIFace;
	}

	public void setWmsOrderMgr(WMSOrderMgr wmsOrderMgr) {
		this.wmsOrderMgr = wmsOrderMgr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException,
			RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException,
			OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		String action = StringUtil.getString(request, "action");
		String model = null;
		if (action.equals("addB2BOrder")) {
			model = addB2BOrder(request);
		} else if (action.equals("addB2BOrderItem")) {
			String s = StringUtil.getString(request, "data");
			model = addB2BOrderItem(request,s);
		} else if (action.equals("deleteB2BOrderItem")) {
			String b2b_detal_id = StringUtil.getString(request, "data");
			model = deleteB2BOrderItem(request,b2b_detal_id);
		} else if (action.equals("getB2BOrder")) {
			model = getB2BOrder(request);
		} else if (action.equals("getB2BOrderItem")) {
			model = getB2BOrderItem(request);
		} else if (action.equals("getB2BOrderItems")) {
			model = getB2BOrderItems(request);
		} else if (action.equals("updateB2BOrder")) {
			model = updateB2BOrder(request);
		} else if (action.equals("updateB2BOrderItem")) {
			String json = StringUtil.getString(request, "data");
			model = updateB2BOrderItem(request,json);
		} else if (action.equals("fillterB2BOrder")) {// 过滤b2b 数据
			DBRow dbRow = new DBRow();
			PageCtrl pc = new PageCtrl();// 分页数据
			pc.setPageNo(StringUtil.getInt(request, "pageNo"));
			pc.setPageSize(StringUtil.getInt(request, "pageSize"));
			int psize = StringUtil.getInt(request, "pageSize");
			if (psize==0) {
				pc = null;
			}
			dbRow = DBRowUtils.mapConvertToDBRow(B2BOrderJsonActionUtil.getParameterMap(request));// request
			model = fillterB2BOrder(dbRow,pc);
		} else if (action.equals("getAllCountry")) {
			model = getAllCountry(request);
		} else if (action.equals("getAllCustomerId")) {
			model = getAllCustomerId(request);
		} else if (action.equals("addTransport")) {
			model = addTransport(request);
		} else if (action.equals("getStorageType")) {// 获取仓库
			model = getStorageType(request);
		} else if (action.equals("getStorageType2")) {// 获取仓库
			model = getStorageType2(request);
		} else if (action.equals("getDetailProductByPname")) {// 根据名字获取商品明细
			model = getDetailProductByPname(request);
		} else if (action.equals("deleteB2Border")) {
			model = deleteB2BOrder(request);
		} else if (action.equals("cancelB2Border")) {// 取消B2bOrder,将状态置为9
			model = cancelB2Border(request);
		} else if (action.equals("dnCount")) {// 查询dn数量
			model = dnCount(request);
		} else if (action.equals("sumCount")) {// 追加商品数量
			model = sumCount(request);
		} else if (action.equals("getAdmin")) {// 获取用户数行结构
			model = getAdmin(request);
		} else if (action.equals("fastSearch")) {
			model = fastSearch(request);
		} else if (action.equals("getAllFreight")) {
			model = getAllFreight(request);
		} else if (action.equals("getB2BOrderLogs")) {
			model = B2BOrderJsonActionUtil.getB2BOrderLogs(request, b2BOrderMgrZyj, 1000);
		} else if (action.equals("getRetailers")) {
			model = getRetailers(request);
		} else if (action.equals("modLines")){
			model = modLines(request);
		} else if (action.equals("getClpTypes")){
			model = getClpTypes(request);
		} else if (action.equals("synWmsOrder")){
			model = synWmsOrder(request);
		} else if (action.equals("synWmsReceipt")){
			model = synWmsReceipt(request);
		} else if (action.equals("reCommit")) {
			model = this.reCommit(request);//回退库存确认
		} else if (action.equals("getWmsOrder")){
			DBRow dbRow = new DBRow();
			PageCtrl pc = new PageCtrl();// 分页数据
			pc.setPageNo(StringUtil.getInt(request, "pageNo"));
			pc.setPageSize(StringUtil.getInt(request, "pageSize"));
			int psize = StringUtil.getInt(request, "pageSize");
			if (psize==0) {
				pc = null;
			}
			dbRow = DBRowUtils.mapConvertToDBRow(B2BOrderJsonActionUtil.getParameterMap(request));// request
			model = fillterB2BOrder2(dbRow,pc);
			
		} else if (action.equals("sendmail")){
			model = sendmail(request);
		}
		
		throw new JsonException(model);
		
	}
	
	/**
	 * 发送邮件
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String sendmail(HttpServletRequest request) throws Exception{
		ReturnModel returnModel = new ReturnModel();
		try {
			User user = new User("reply@oso.com");
			Mail mail = new Mail(user);
			String email = request.getParameter("email");
			String html = request.getParameter("html");
			mail.setAddress(new MailAddress(email));
			mail.setMailBody(new MailBody("AppointmentInfo",html, true, new Attach[0]));
			mail.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	/**
	 * 同步WMS Order
	 * @param request
	 * @throws Exception 
	 * @throws NumberFormatException
	 * @return 1:更新成功 0:失败
	 */
	public String synWmsOrder(HttpServletRequest request) throws NumberFormatException, Exception{
		String [] oids=request.getParameterValues("oid");
		int type=Integer.parseInt(request.getParameter("type"));
		String carriers = request.getParameter("carriers");
		String pickup_appointment = request.getParameter("pickup_appointment");
		String loadno = request.getParameter("loadno");
		DBRow data = new DBRow();
		if (carriers!=null) data.add("carriers", carriers);
		if (pickup_appointment!=null) data.add("pickup_appointment", pickup_appointment);
		if (loadno!=null) data.add("load_no", loadno);
		try{
			switch(type){
				case WmsOrderUpdateTypeKey.LOADAPPT:
					for(String oid:oids){
						proxyWMSOrderMgr.update(Long.parseLong(oid), WmsOrderUpdateTypeKey.LOADAPPT, data);
					}
					return "1";
				default:
					for(String oid:oids){
						proxyWMSOrderMgr.update(Long.parseLong(oid), type);
					}
					return "1";
			}
		}catch(Exception e){
			log.info("同步WMSOrder异常-->"+e.getMessage());
			return "0";
		}
	}
	
	/**
	 * 同步Receipt
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String synWmsReceipt( HttpServletRequest request) throws Exception {
		ReturnModel rt = new ReturnModel();
		
		try {
			int otype=Integer.parseInt(request.getParameter("otype"));
			int type=Integer.parseInt(request.getParameter("type"));
			if (1==otype) {
				String para = StringUtil.getString(request, "data");
				String line = StringUtil.getString(request, "line");
				net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(para);
				Receipt main = (Receipt) net.sf.json.JSONObject.toBean(jsonObject, Receipt.class);
				net.sf.json.JSONArray linesj =  net.sf.json.JSONArray.fromObject(line);
				Set<ReceiptLine> lines = new HashSet<ReceiptLine>();
				for ( Object item: linesj) {
					net.sf.json.JSONObject obj = (net.sf.json.JSONObject) item;
					lines.add((ReceiptLine)net.sf.json.JSONObject.toBean(obj, ReceiptLine.class));
				}
				//TODO
				if (1==type) {
					//create
					Date now = new Date();
					main.setDateCreated(now);
					main.setDateUpdated(now);
					long rn = proxyWMSOrderMgr.saveRN(main, lines);
					rt.setDATA(rn);
				} else if (2==type) {
					//update
					long rn = proxyWMSOrderMgr.updateRN(main, lines);
					rt.setDATA(rn);
				}
			} else if (2==otype) {
				String CompanyID = StringUtil.getString(request, "CompanyID");
				String RNNo = StringUtil.getString(request, "ReceiptNo");
				if (1==type) {
					//cancel
					boolean r = proxyWMSOrderMgr.cancelRN(CompanyID, Long.parseLong(RNNo));
					if (r) {
						rt.setDATA(1);
					} else {
						rt.setCODE("500");
					}
				} else if (2==type) {
					//delete
					boolean r = proxyWMSOrderMgr.deleteRN(CompanyID, Long.parseLong(RNNo));
					if (r) {
						rt.setDATA(1);
					} else {
						rt.setCODE("500");
					}
				}
			}
		} catch(Exception e) {
			//e.printStackTrace();
			log.info("同步WMSReceipt异常-->"+e.getMessage());
			rt.setCODE("500");
			rt.setINFO(e.getMessage());
		}
		return new JSONObject(rt).toString();
	}

	/**
	 * 获取包装形式
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String getClpTypes(HttpServletRequest request) throws Exception {
		String resp=getDetailProductByPname(request);
		JSONObject json=new JSONObject(resp);
		if(json.getInt("CODE")!=200){
			return resp;
		}
		String para = StringUtil.getString(request, "data");
		JSONObject jsonObject = new JSONObject(para);
		int title_id= Integer.parseInt(jsonObject.getString("TITLE_ID"));
		String pname = jsonObject.getString("B2B_P_NAME");
		int b2b_oid = jsonObject.getInt("B2B_OID");
		DBRow order = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
		int send_psid=0;
		int customerKey=0;
		if(order!=null){
			send_psid=Integer.parseInt(order.getString("receive_psid","0"));
			customerKey=order.get("customer_key", 0);
		}
		DBRow row = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(send_psid);
		String ship_to_id="0";
		if(row!=null)
			ship_to_id=row.getString("ship_to_id","0");
		DBRow product = productMgrIFace.getDetailProductByPname(pname);
		if (product == null) {
			throw new Exception("不存在此商品" + pname);
		}
		long pc_id=product.get("pc_id", 0l);
		//修改clptype获取接口，原先无定义ship的clp无法查询出默认的包装形式
		// wangcr 06/10/2015
		DBRow [] rows=boxTypeMgrZr.getBoxTypes(pc_id, 0, title_id, Integer.parseInt(ship_to_id), customerKey,0);
		json=new JSONObject();
		json.put("product", product);
		json.put("clptypes", rows);
		
		return  json.toString();
	}
	
	/**
	 * 修改Lines
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String modLines(HttpServletRequest request) throws Exception {
		JSONObject resp=new JSONObject();
		resp.put("code", 200);
		
		try{
			@SuppressWarnings("rawtypes")
			Map map=B2BOrderJsonActionUtil.getParameterMap(request);
			String data=(String)map.get("data");
			
			JSONObject json = new JSONObject(data);
			String b2b_oid=json.getString("b2b_oid");
			DBRow orderRow=b2BOrderMgrZyj.getB2BOrder(Long.parseLong(b2b_oid));
			String cid=orderRow.getString("customer_id");
			
			JSONArray mod=(JSONArray)json.get("updates");
			JSONArray adds=(JSONArray)json.get("adds");
			
			//校验Element的SO值是否一致
			String _so="";
			if(CommonUtils.isElementCustomer(cid)){
				for(int i=0;i<mod.length();i++){
					String so=mod.getJSONObject(i).getString("SO");
					if(i==0)
						_so=so;
					
					if(!_so.equals(so)){
						resp.put("code", 500);
						resp.put("msg","Inconsistent SO found by Element ");
						return resp.toString();
					}
				}
				
				for(int i=0;i<adds.length();i++){
					String so=adds.getJSONObject(i).getString("SO");
					if(i==0)
						_so=so;
					
					if(!_so.equals(so)){
						resp.put("code", 500);
						resp.put("msg","Inconsistent SO found by Element!");
						return resp.toString();
					}
				}
			}
			
			String dels=(String)json.get("dels");

			String addData=adds.toString();
			addB2BOrderItem(request,addData);
			
			if(mod!=null){
				for(int i=0;i<mod.length();i++){
					String modData=mod.get(i).toString();
					updateB2BOrderItem(request,modData);
				}
			}
			
			if(!StringUtils.isEmpty(dels)){
				String [] ids=dels.split(",");
				for(String id:ids)
					deleteB2BOrderItem(request,id);
			}

			if(CommonUtils.isElementCustomer(cid)){
				DBRow row=new DBRow();
				row.put("so", _so);
				this.b2BOrderMgrZyj.updateB2BOrder(Long.parseLong(b2b_oid), row);
				SyncOrderLine(b2b_oid, request);
			}
		}catch(Exception e){
			e.printStackTrace();
			resp.put("code", 500);
		}
		return resp.toString();
	}
	/**
	 * 同步line，同步order及line
	 * 
	 * @param sb2b_oid
	 * @param request
	 */
	private void SyncOrderLine(String sb2b_oid, HttpServletRequest request) {
		try {
			if(sb2b_oid==null || "".equals(sb2b_oid)) return;
			long b2b_oid = Long.parseLong(sb2b_oid);
			DBRow Order = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
			//判断是否同步
			long wid = Order.get("b2b_order_number", 0L);
			if (wid>0) {
				//修改内容
				//System.out.println("update line");
				try{
					wmsOrderMgr.saveOrderLine(b2b_oid);
				} catch(RuntimeException e) {
					insertLog(b2b_oid,"update WMS line fail",B2BOrderLogTypeKey.Update, request);
				}
			} else {
				//执行save
				DBRow[] orderlines = b2BOrderMgrZyj.getB2BOrderItems(b2b_oid);
				try{
					this.wmsOrderMgr.save(Order, orderlines);
				} catch(RuntimeException e) {
					e.printStackTrace();
					insertLog(b2b_oid,"import WMS fail",B2BOrderLogTypeKey.Update, request);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 过滤表单数据
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-26上午11:16:55<br>
	 * @author: cuicong
	 */
	public String fastSearch(HttpServletRequest request) throws Exception {
		
		ReturnModel returnModel = new ReturnModel();
		try {
			PageCtrl pc = new PageCtrl();// 分页数据
			pc.setPageNo(StringUtil.getInt(request, "pageNo"));
			pc.setPageSize(StringUtil.getInt(request, "pageSize"));
			
			String searchKey = StringUtil.getString(request, "search_key");
			int serachModle = StringUtil.getInt(request, "search_mode");
			DBRow[] d = this.b2BOrderMgrZyj.searchB2BOrderByNumber(searchKey, serachModle, pc);
			
			DBRow row=new DBRow();
			if(d==null || d.length==0){
				returnModel.setCODE("200");
				returnModel.setDATA(new ArrayList<>());
				returnModel.setINFO("没有符合条件的结果");
				returnModel.setPAGECTRL(pc);
				
				return new JSONObject(returnModel).toString();
			}
			
			Vector<String> vec=new Vector<String>();
			if(d.length>0){
				for(DBRow r:d){
					if(r==null)
						continue;
					
					vec.add(r.getString("b2b_oid"));
				}
				
				row.put("b2b_oid", StringUtils.join(vec,","));
			}
			return fillterB2BOrder(row,pc);
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(new ArrayList<>());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	private String getAdmin(HttpServletRequest request) throws Exception {
		
		String type = StringUtil.getString(request, "type");
		String value = StringUtil.getString(request, "value");
		String key = StringUtil.getString(request, "key");
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (!value.equals("")) {
			
			//根据账号ID获得一个         账号
			DBRow dbRow = adminMgr.getDetailAdmin(Long.valueOf(value));
			if (dbRow == null) {
				dbRow = new DBRow();
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("data", dbRow.getString("adid"));
			map.put("id", dbRow.getString("adgid") + "_" + dbRow.getString("adid"));
			map.put("pId", dbRow.getString("adgid"));
			map.put("name", dbRow.getString("employe_name"));
			map.put("isParent", false);
			map.put("isSubitme", false);
			list.add(map);
			
		} else if (type.equals("") && key.equals("")) {
			
			//获得所有          部门
			DBRow[] d = adminMgr.getAllAdminGroup(null);
			for (DBRow dbRow : d) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("data", dbRow.getString("adgid"));
				map.put("id", dbRow.getString("adgid"));
				map.put("pId", "");
				map.put("name", dbRow.getString("name"));
				map.put("isParent", true);
				map.put("isSubitme", true);
				list.add(map);
			}
		} else if (!type.equals("")) {
			
			//根据部门ID获得      账号
			//DBRow[] d = adminMgr.getAdminByAdgid(Long.valueOf(type), null);
			DBRow[] d = adminMgr.getAccountByDeptId(Long.valueOf(type));
			
			for (DBRow dbRow : d) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("data", dbRow.getString("adid"));
				map.put("id", dbRow.getString("adgid") + "_" + dbRow.getString("adid"));
				map.put("pId", dbRow.getString("adgid"));
				map.put("name", dbRow.getString("employe_name"));
				map.put("isParent", false);
				map.put("isSubitme", false);
				list.add(map);
			}
			
		} else if (!key.equals("")) {
			
			//根据员工名模糊查询    账号
			DBRow[] d = adminMgr.getAdminsEmployeName(key);
			for (DBRow dbRow : d) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("data", dbRow.getString("adid"));
				map.put("id", dbRow.getString("adgid") + "_" + dbRow.getString("adid"));
				map.put("pId", dbRow.getString("adgid"));
				map.put("name", dbRow.getString("employe_name"));
				map.put("isParent", false);
				map.put("isSubitme", false);
				list.add(map);
			}
		}
		
		return new JSONArray(list).toString();
		
	}
	
	/**
	 * 追加item数量
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	
	private String sumCount(HttpServletRequest request) throws Exception {
		String json = StringUtil.getString(request, "data");
		DBRow re = DBRowUtils.convertToDBRow(new JSONObject(json));
		ReturnModel returnModel = new ReturnModel();
		int b2b_oid = re.get("B2B_OID", 0);
		String pname = re.get("B2B_P_NAME", "");
		DBRow[] d = this.b2BOrderMgrZyj.fillterB2BOrderItem(b2b_oid, pname,"");
		if (d != null && d.length == 1) {
			DBRow dbRow = d[0];
			dbRow.add("B2B_COUNT", dbRow.get("b2b_count", 0d) + re.get("B2B_COUNT", 0d));
			dbRow.add("PALLET_SPACES", dbRow.get("PALLET_SPACES", 0d) + re.get("PALLET_SPACES", 0d));
			dbRow.add("TOTAL_WEIGHT", dbRow.get("TOTAL_WEIGHT", 0d) + re.get("TOTAL_WEIGHT", 0d));
			dbRow.add("BOXES", dbRow.get("BOXES", 0d) + re.get("BOXES", 0d));
			this.b2BOrderMgrZyj.updateB2BOrderItem(dbRow.get("b2b_detail_id", 0l), dbRow);
			returnModel.setINFO("更新数量成功");
			returnModel.setDATA(dbRow);
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 查询dn数量，表示dn是否存在
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2015年1月19日上午11:30:21<br>
	 * @author: cuicong
	 */
	
	private String dnCount(HttpServletRequest request) throws Exception {
		int dncount = 0;
		ReturnModel returnModel = new ReturnModel();
		String json = StringUtil.getString(request, "data");
		
		if (!json.equals("")) {
			DBRowUtils.convertToDBRow(new JSONObject());
			DBRow dbRow = DBRowUtils.convertToDBRow(new JSONObject(json));
			DBRow[] d = this.b2BOrderMgrZyj.fillterB2BOrder(dbRow, null);
			if (d != null) {
				dncount = d.length;
			}
			returnModel.setINFO("客户：" + dbRow.get("customer_id", "") + "下的DN：" + dbRow.get("customer_dn", "") + "存在"
					+ dncount + "条");
		}
		
		returnModel.setDATA(Integer.valueOf(dncount));
		
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 取消订单
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2015年1月19日上午11:27:35<br>
	 * @author: cuicong
	 */
	
	private String cancelB2Border(HttpServletRequest request) throws Exception {
		long b2b_oid = StringUtil.getLong(request, "data");
		ReturnModel returnModel = new ReturnModel();
		String [] ids=b2BOrderMgrZyj.getLoadAndApp(b2b_oid);
		JSONObject json=new JSONObject();
		if(ids!=null){
			String loadno=ids[0];
			String aid=ids[1];
			json.put("LoadNo", loadno);
			json.put("AppId", aid);
			
			returnModel.setCODE("500");
			returnModel.setDATA(json);
			
			if(!StringUtils.isEmpty(loadno) && !StringUtils.isEmpty(aid)){
				returnModel.setINFO("订单已经生成load并约车,无法取消!");
				return new JSONObject(returnModel).toString();
			}
			
			if(!StringUtils.isEmpty(loadno) && StringUtils.isEmpty(aid)){
				returnModel.setINFO("订单已经生成load,无法取消!");
				return new JSONObject(returnModel).toString();
			}
			
			if(StringUtils.isEmpty(loadno) && !StringUtils.isEmpty(aid)){
				returnModel.setINFO("订单已经生成约车,无法取消!");
				return new JSONObject(returnModel).toString();
			}
		}
		
		DBRow row = new DBRow();
		row.add("b2b_order_status", 9);
		this.b2BOrderMgrZyj.updateB2BOrder(b2b_oid, row);
		// -----------插入日志----------
		
		this.insertLog(b2b_oid, "cancel order:" + b2b_oid, B2BOrderLogTypeKey.Update, request);
		//取消 load&appt 关联 
		
		row=this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
		//回写WMS关闭订单
		if(CommonUtils.isElementCustomer(String.valueOf(row.get("customer_id", 0L))))
			wmsOrderMgr.update(b2b_oid, WmsOrderUpdateTypeKey.CANCEL);
		
		returnModel.setCODE("200");
		returnModel.setINFO("取消订单成功，订单id为：" + b2b_oid);
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 
	 * 根据名称获取仓库属性
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2015-1-14上午9:30:01<br>
	 * @author: cuicong
	 */
	private String getDetailProductByPname(HttpServletRequest request) throws Exception {
		String json = StringUtil.getString(request, "data");
		JSONObject jsonObject = new JSONObject(json);
		ReturnModel returnModel = new ReturnModel();
		String s = "500:不存在此商品;200:订单下此商品不存在重复的item;201：订单下此商品存在重复的item";
		returnModel.setDATA(s);
		int b2b_id = jsonObject.getInt("B2B_OID");
		String pname = jsonObject.getString("B2B_P_NAME");
		String title = jsonObject.getString("TITLE");
		String isVali = jsonObject.getString("isVali");
		DBRow d = productMgrIFace.getDetailProductByPname(pname);
		if (d != null) {
			if(!"1".equals(isVali)){
				DBRow[] dbRows = b2BOrderMgrZyj.fillterB2BOrderItem(b2b_id, pname,title);
				if (dbRows.length > 0) {
					returnModel.setCODE("201");
					returnModel.setINFO("订单：" + b2b_id + "下此商品" + pname + "存在重复的" + dbRows.length + "条item");
				} else {
					returnModel.setCODE("200");
					returnModel.setINFO("订单：" + b2b_id + "下此商品" + pname + "不存在重复的item");
				}
			}else{
				returnModel.setCODE("200");
				returnModel.setINFO("");
			}
		} else {
			returnModel.setCODE("500");
			returnModel.setINFO("不存在此" + pname);
		}
		return new JSONObject(returnModel).toString();
	}
	
	private String getRetailers(HttpServletRequest request) throws Exception {
		DBRow[] reList = shipToMgrZJ.getAllShipTo();
		return new JSONArray(reList).toString();
	}
	
	/**
	 * 
	 * 获取所有的仓库
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2015-1-13上午9:37:22<br>
	 * @author: cuicong
	 */
	private String getStorageType(HttpServletRequest request) throws Exception {
		
		StorageTypeKey storageTypeKey = new StorageTypeKey();
		
		List<Map<String, Object>> reList = new ArrayList<Map<String, Object>>();
		String key = StringUtil.getString(request, "key");
		String type = StringUtil.getString(request, "type");
		String value = StringUtil.getString(request, "value");
		
		if (!value.equals("")) {// 默认选中的某个值
			DBRow row = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(Integer.valueOf(value));
			if (row == null) {
				row = new DBRow();
				
			}
			Map<String, Object> storage = new HashMap<String, Object>();
			storage.put("data", row.get("id", 0));
			storage.put("id", row.getString("id"));
			storage.put("pId", "");
			storage.put("name", row.get("title", ""));
			storage.put("deliver_contact", row.get("deliver_contact",""));
			storage.put("deliver_phone", row.getString("deliver_phone",""));
			storage.put("send_contact", row.get("send_contact",""));
			storage.put("send_phone", row.getString("send_phone",""));
			if (row.get("storage_type", 0) == 5) {
				storage.put("id", "5_" + row.getString("ship_to_id") + "_" + row.getString("id"));
				storage.put("pId", row.getString("ship_to_id"));
				
			} else {
				storage.put("pId", row.get("storage_type", 0));
				storage.put("id", row.get("storage_type", 0) + "_" + row.getString("id"));
				
			}
			
			reList.add(storage);
			
		} else if (!key.equals("")) {// 关键字查询
			DBRow filter = new DBRow();
			filter.add("title", key);
			DBRow[] dbRows = ShipToStorageIndexMgr.getInstance().getSearchResults(key);
			for (DBRow dbRow2 : dbRows) {
				Map<String, Object> storage = new HashMap<String, Object>();
				storage.put("data", dbRow2.get("id", 0));
				storage.put("id", dbRow2.getString("id"));
				storage.put("pId", "");
				storage.put("name", dbRow2.get("title", ""));
				if (dbRow2.get("storage_type", 0) == 5) {
					storage.put("id", "5_" + dbRow2.getString("ship_to_id") + "_" + dbRow2.getString("id"));
					storage.put("pId", dbRow2.getString("ship_to_id"));
					
				} else {
					storage.put("pId", dbRow2.get("storage_type", 0));
					storage.put("id", dbRow2.get("storage_type", 0) + "_" + dbRow2.getString("id"));
					
				}
				
				reList.add(storage);
			}
		} else {
			
			if (type.equals("")) {// 查询所有的仓库类型
				@SuppressWarnings("unchecked")
				List<String> storageTypes = (List<String>) storageTypeKey.getStorageTypeKeys();
				for (String data : storageTypes) {
					Map<String, Object> storage = new HashMap<String, Object>();
					storage.put("data", Integer.valueOf(data));
					storage.put("id", data);
					storage.put("pId", "");
					storage.put("name", storageTypeKey.getStorageTypeKeyName(data));
					storage.put("isParent", true);
					storage.put("isSubitme", false);
					reList.add(storage);
					
				}
			} else if (type.startsWith("5")) {// 客户仓库
				String types[] = type.split("_");
				
				if (types.length == 1) {// 存在两级树
					DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
					for (DBRow s : shipTos) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("ship_to_id", 0));
						storage.put("id", type + "_" + s.getString("ship_to_id"));
						storage.put("pId", 5);
						storage.put("name", s.get("ship_to_name", ""));
						storage.put("isParent", true);
						storage.put("isSubitme", true);
						reList.add(storage);
					}
					
				} else if (types.length == 2) {// 存在三级树
					DBRow[] shipTos = shipToMgrZJ.GetAllShipToCustomer(5, Long.valueOf(types[1]));
					for (DBRow s : shipTos) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("id", 0));
						storage.put("id", type + "_" + s.getString("id"));
						storage.put("pId", types[1]);
						storage.put("name", s.get("title", ""));
						reList.add(storage);
					}
					
				}
				
			} else {// 不是客户仓库
				String types[] = type.split(",");
				
				if (types.length == 1) {
					DBRow[] storageCatalogs = productStorageMgrIFaceZJ.getProductStorageCatalogsByTypeShipTo(
							Integer.valueOf(types[0]), 0);
					for (DBRow s : storageCatalogs) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("id", 0));
						storage.put("id", type + "_" + s.getString("id"));
						storage.put("pId", types[0]);
						storage.put("name", s.get("title", ""));
						storage.put("deliver_contact", s.get("deliver_contact",""));
						storage.put("deliver_phone", s.getString("deliver_phone",""));
						storage.put("send_contact", s.get("send_contact",""));
						storage.put("send_phone", s.getString("send_phone",""));
						reList.add(storage);
					}
					
				}
				
			}
		}
		
		return new JSONArray(reList).toString();
	}
	
	/**
	 * 
	 * 获取所有的仓库
	 * 
	 * @param request
	 * @return
	 */
	private String getStorageType2(HttpServletRequest request) throws Exception {
		StorageTypeKey storageTypeKey = new StorageTypeKey();
		
		List<Map<String, Object>> reList = new ArrayList<Map<String, Object>>();
		String key = StringUtil.getString(request, "key");
		String type = StringUtil.getString(request, "type");
		String value = StringUtil.getString(request, "value");
		
		if (!value.equals("")) {// 默认选中的某个值
			DBRow row = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(Integer.valueOf(value));
			if (row == null) {
				row = new DBRow();
			}
			Map<String, Object> storage = new HashMap<String, Object>();
			storage.put("data", row.get("id", 0));
			storage.put("id", row.getString("id"));
			storage.put("pId", "");
			storage.put("name", row.get("title", ""));
			storage.put("deliver_contact", row.get("deliver_contact",""));
			storage.put("deliver_phone", row.getString("deliver_phone",""));
			storage.put("send_contact", row.get("send_contact",""));
			storage.put("send_phone", row.getString("send_phone",""));
			if (row.get("storage_type", 0) == 5) {
				storage.put("id", "5_" + row.getString("ship_to_id") + "_" + row.getString("id"));
				storage.put("pId", row.getString("ship_to_id"));
				
			} else {
				storage.put("pId", row.get("storage_type", 0));
				storage.put("id", row.get("storage_type", 0) + "_" + row.getString("id"));
				
			}
			
			reList.add(storage);
			
		} else if (!key.equals("")) {// 关键字查询
			DBRow filter = new DBRow();
			filter.add("title", key);
			DBRow[] dbRows = ShipToStorageIndexMgr.getInstance().getSearchResults(key);
			for (DBRow dbRow2 : dbRows) {
				Map<String, Object> storage = new HashMap<String, Object>();
				storage.put("data", dbRow2.get("id", 0));
				storage.put("id", dbRow2.getString("id"));
				storage.put("pId", "");
				storage.put("name", dbRow2.get("title", ""));
				if (dbRow2.get("storage_type", 0) == 5) {
					storage.put("id", "5_" + dbRow2.getString("ship_to_id") + "_" + dbRow2.getString("id"));
					storage.put("pId", dbRow2.getString("ship_to_id"));
					
				} else {
					storage.put("pId", dbRow2.get("storage_type", 0));
					storage.put("id", dbRow2.get("storage_type", 0) + "_" + dbRow2.getString("id"));
					
				}
				
				reList.add(storage);
			}
		} else {
			
			if (type.equals("")) {// 查询所有的仓库类型
				@SuppressWarnings("unchecked")
				List<String> storageTypes = (List<String>) storageTypeKey.getStorageTypeKeys();
				for (String data : storageTypes) {
					Map<String, Object> storage = new HashMap<String, Object>();
					storage.put("data", Integer.valueOf(data));
					storage.put("id", data);
					storage.put("pId", "");
					storage.put("name", storageTypeKey.getStorageTypeKeyName(data));
					storage.put("isParent", true);
					storage.put("isSubitme", false);
					reList.add(storage);
					
				}
			} else if (type.startsWith("5")) {// 客户仓库
				String types[] = type.split("_");
				
				if (types.length == 1) {// 存在两级树
					DBRow[] shipTos = shipToMgrZJ.getAllShipTo();
					for (DBRow s : shipTos) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("ship_to_id", 0));
						storage.put("id", type + "_" + s.getString("ship_to_id"));
						storage.put("pId", 5);
						storage.put("name", s.get("ship_to_name", ""));
						storage.put("isParent", true);
						storage.put("isSubitme", true);
						reList.add(storage);
					}
					
				} else if (types.length == 2) {// 存在三级树
					DBRow[] shipTos = shipToMgrZJ.GetAllShipToCustomer0(0, Long.valueOf(types[1]));
					for (DBRow s : shipTos) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("id", 0));
						storage.put("id", type + "_" + s.getString("id"));
						storage.put("pId", types[1]);
						storage.put("name", s.get("title", ""));
						reList.add(storage);
					}
					
				}
				
			} else {// 不是客户仓库
				String types[] = type.split(",");
				
				if (types.length == 1) {
					DBRow[] storageCatalogs = productStorageMgrIFaceZJ.getProductStorageCatalogsByTypeShipTo0(
							Integer.valueOf(types[0]), 0);
					for (DBRow s : storageCatalogs) {
						Map<String, Object> storage = new HashMap<String, Object>();
						storage.put("data", s.get("id", 0));
						storage.put("id", type + "_" + s.getString("id"));
						storage.put("pId", types[0]);
						storage.put("name", s.get("title", ""));
						storage.put("deliver_contact", s.get("deliver_contact",""));
						storage.put("deliver_phone", s.getString("deliver_phone",""));
						storage.put("send_contact", s.get("send_contact",""));
						storage.put("send_phone", s.getString("send_phone",""));
						reList.add(storage);
					}
					
				}
				
			}
		}
		
		return new JSONArray(reList).toString();
	}
	
	/**
	 * 
	 * 获取所有的国家
	 * 
	 * @param request
	 * @return <b>Date:</b>2014-12-30下午4:53:31<br>
	 * @author: cuicong
	 * @throws Exception
	 */
	private String getAllCountry(HttpServletRequest request) throws Exception {
		DBRow d[] = orderMgrIFace.getAllCountryCode();
		return new JSONArray(d).toString();
	}
	
	/**
	 * 
	 * 获取所有的customerId
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2015-1-5下午4:36:21<br>
	 * @author: cuicong
	 */
	private String getAllCustomerId(HttpServletRequest request) throws Exception {
		DBRow d[] = b2BOrderMgrZyj.fillterCustomerBrand(new DBRow(), null);
		for (DBRow dbRow : d) {
			dbRow.remove("customer_key");
			dbRow.remove("brand_number");
			dbRow.add("value", dbRow.get("cb_id"));
			dbRow.remove("cb_id");
			dbRow.add("text", dbRow.get("brand_name"));
			dbRow.remove("brand_name");
		}
		return new JSONArray(d).toString();
	}
	
	/**
	 * 
	 * 添加B2Border
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24上午11:07:35<br>
	 * @author: cuicong
	 */
	public String addB2BOrder(HttpServletRequest request) throws Exception {
		String s = StringUtil.getString(request, "data");
		
		ReturnModel returnModel = new ReturnModel();
		// 收货人信息
		AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		
		//fix 
		DBRow send_psidDBRow = null;//productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(new Long(adminLoggerBean.getPs_id()).intValue());// 发送仓库
		
		try {
			JSONObject jsonObject = new JSONObject(s);
			
			
			DBRow dbRow = DBRowUtils.convertToDBRow(jsonObject);
			long sid =  dbRow.get("send_psid", adminLoggerBean.getPs_id());
			send_psidDBRow = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(Long.valueOf(sid).intValue());
					
			dbRow.add("send_psid", send_psidDBRow.get("id"));
			dbRow.add("send_zip_code", send_psidDBRow.get("send_zip_code"));
			dbRow.add("send_house_number", send_psidDBRow.get("send_house_number"));
			dbRow.add("send_street", send_psidDBRow.get("send_street"));
			dbRow.add("send_address3", send_psidDBRow.get("send_address3"));
			dbRow.add("send_name", send_psidDBRow.get("send_name"));
			dbRow.add("send_linkman_phone", send_psidDBRow.get("send_phone"));
			dbRow.add("send_city", send_psidDBRow.get("send_city"));
			dbRow.add("send_pro_id", send_psidDBRow.get("send_pro_id", 0));
			dbRow.add("b2b_order_date", new Timestamp(new Date().getTime()));
			dbRow.add("create_account", adminLoggerBean.getAccount());
			dbRow.add("create_account_id", adminLoggerBean.getAdid());
			dbRow.add("updatedate", new Timestamp(new Date().getTime()));
			dbRow.add("updateby", adminLoggerBean.getAdgid());
			dbRow.add("updatename", adminLoggerBean.getEmploye_name());
			int ps_id = dbRow.get("send_psid", 0);
			/**
			 * 对日期格式特殊处理
			 */
			B2BOrderJsonActionUtil.EditDate(dbRow, ps_id);
			long b2b_oid = b2BOrderMgrZyj.addB2BOrder(dbRow);
			if (b2b_oid > 0) {
				this.editB2BOrderIndex(b2b_oid, "add");
				returnModel.setINFO("插入成功,id为" + b2b_oid);
				returnModel.setDATA(b2b_oid);
				// -----------插入日志----------
				this.insertLog(b2b_oid, "create order", B2BOrderLogTypeKey.Create, request);
			}
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		
		return new JSONObject(returnModel).toString();
		
	}
	
	/**
	 * 
	 * 添加B2Border明细
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24上午11:21:16<br>
	 * @author: cuicong
	 */
	public String addB2BOrderItem(HttpServletRequest request,String data) {
		String s = data;
		ReturnModel returnModel = new ReturnModel();
		AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		try {
			JSONArray jsonObject = new JSONArray(s);
			DBRow dbRow[];
			dbRow = DBRowUtils.jsonArrayAsDBRowArray(jsonObject);
			
			long ids[] = new long[dbRow.length];
			int i = 0;
			int b2b_oid = 0;
			String cid="";
			String b2bOrderNumber="";
			DBRow orderRow=null;
			for (DBRow d : dbRow) {
				b2b_oid = d.get("b2b_oid", 0);
				
				if(StringUtils.isEmpty(cid)){
					orderRow=b2BOrderMgrZyj.getB2BOrder(b2b_oid);
					cid=orderRow.getString("customer_id");
					b2bOrderNumber=orderRow.getString("b2b_order_number");
				}
				
				d.add("b2b_delivery_count", 0f);// '订交货数量（交货型订为交货数，订为转换数）'
				d.add("b2b_backup_count", 0f);// '备件数量（交货型订单的备件数量，内部订单默认为0）'
				d.add("b2b_union_count", 0f);
				d.add("b2b_normal_count", 0f);
				d.add("b2b_wait_count", 0f);
				d.add("b2b_volume", 0f);// '商品体积（基础数据内商品体积）',
				d.add("b2b_weight", 0f);// 商品重量（基础数据内商品重量）',
				if (d.get("trailer_feet", "").equals("")) {
					d.remove("trailer_feet");
				}
				DBRow product = productMgrIFace.getDetailProductByPname(d.get("b2b_p_name", ""));
				if (product == null) {
					throw new Exception("不存在此商品" + d.get("b2b_p_name", ""));
				}
				
				d.add("b2b_pc_id", product.get("pc_id", 0l));
				d.add("b2b_p_code", product.get("lot_number", 0l));
				d.add("item_id", product.get("sku", ""));
				ids[i++] = b2BOrderMgrZyj.addB2BOrderItem(d);
			}
			
			if (ids.length > 0) {
				returnModel.setINFO("插入成功");
				returnModel.setDATA(ids);
				// -----------插入日志----------
				this.insertLog(b2b_oid, "add  orderItem # b2b_oid:" + b2b_oid, B2BOrderLogTypeKey.Update, request);
			}
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(null);
			returnModel.setINFO(e.getMessage());
			e.printStackTrace();
		}
		
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * getB2B订
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String getB2BOrder(HttpServletRequest request) throws Exception {
		long b2b_oid = StringUtil.getLong(request, "data");
		ReturnModel returnModel = new ReturnModel();
		try {
			DBRow row = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
			if (row == null) {
				row = new DBRow();
			}
			DBRow searchDbRow = new DBRow();
			searchDbRow.add("cb_id", row.get("customer_id", ""));
			DBRow[] customerDbrow = b2BOrderMgrZyj.fillterCustomerBrand(searchDbRow, null);
			if (customerDbrow.length == 1) {
				row.add("customer_name", customerDbrow[0].get("brand_name"));
			}
			
			DBRow send_psidDBRow = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(row.get("send_psid", 0));// 发送仓库
			if (send_psidDBRow == null) {
				send_psidDBRow = new DBRow();
			}
			DBRow receive_psidDBRow = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(row.get("receive_psid", 0));// 接收仓库
			if (send_psidDBRow != null) {
				row.add("send_psname", send_psidDBRow.getString("title"));
			}
			
			if (receive_psidDBRow != null) {
				row.add("receive_psname", receive_psidDBRow.get("title", ""));
			}
			// 时间处理
			int jet_lag = send_psidDBRow.get("jet_lag", 0);//
			DBRow[] logs = b2BOrderMgrZyj.getB2BOrderLogs(row.get("b2b_oid", 0l), 4);
			B2BOrderJsonActionUtil.getB2BOrderDate(row, jet_lag, row.get("send_psid", 0), logs);
			
			DBRow [] lines=b2BOrderMgrZyj.fillterB2BOrderItem(row.get("b2b_oid", 0l),"","");
			B2BOrderJsonActionUtil.getB2bOrderLines(row, lines);
			
			B2BOrderJsonActionUtil.getB2bOrderSOLines(row, lines);
			B2BOrderJsonActionUtil.getB2bOrderPOLines(row, lines);
			
			// 设置-------权限
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			list.add(B2BOrderJsonActionUtil.setAUTHS(row));
			returnModel.setAUTHS(list);
			returnModel.setDATA(row);
			returnModel.setINFO("操作成功");
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA("");
			returnModel.setINFO(e.toString());
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * getB2B订单明细
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String getB2BOrderItem(HttpServletRequest request) throws Exception {
		long item_id = StringUtil.getLong(request, "data");
		ReturnModel returnModel = new ReturnModel();
		try {
			DBRow dbRow = this.b2BOrderMgrZyj.getB2BOrderItem(item_id);// 明细数据
			// --------------------------------添加权限
			DBRow b2bOrder = this.b2BOrderMgrZyj.getB2BOrder(dbRow.get("b2b_oid", 0));// 主表数据
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			int i = 0;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("INDEX", i++);
			map.put("ID", dbRow.get("b2b_detail_id", 0));
			map.put("BUTTON_APP", true);
			
			if (b2bOrder.get("b2b_order_status", 0) == 1) {
				map.put("BUTTON_DELETE", true);
				map.put("BUTTON_CANCEL", false);
			} else if (b2bOrder.get("b2b_order_status", 0) >= 8 ) {
				map.put("BUTTON_CANCEL", false);
				map.put("BUTTON_DELETE", false);
			} else {
				map.put("BUTTON_CANCEL", true);
				map.put("BUTTON_DELETE", false);
			}
			
			list.add(map);
			returnModel.setAUTHS(list);
			// --------------------------------
			int warehousinger_id = dbRow.get("WAREHOUSINGER_ID", 0);// 入库人
			if (warehousinger_id != 0) {
				DBRow admim = adminMgr.getDetailAdmin(warehousinger_id);
				dbRow.add("WAREHOUSINGER_NAME", admim.getString("employe_name"));
			}
			int deliveryer_id = dbRow.get("DELIVERYER_ID", 0);// 收货人
			if (deliveryer_id != 0) {
				DBRow admim = adminMgr.getDetailAdmin(warehousinger_id);
				dbRow.add("DELIVERYER_NAME", admim.getString("employe_name"));
			}
			
			returnModel.setINFO("操作成功");
			returnModel.setDATA(dbRow);
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(new DBRow());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * etB2B订单明细列表
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String getB2BOrderItems(HttpServletRequest request) throws Exception {
		long b2b_oid = StringUtil.getLong(request, "data");
		ReturnModel returnModel = new ReturnModel();
		DBRow b2bOrder = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
		
		try {
			DBRow[] d = this.b2BOrderMgrZyj.getB2BOrderItems(b2b_oid);
			if (d.length > 0) {
				returnModel.setINFO("操作成功");
				returnModel.setDATA(d);
				returnModel.setPAGECTRL(new PageCtrl());
				// 设置-------权限
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				int i = 0;
				for (DBRow items : d) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("INDEX", i++);
					map.put("ID", items.get("b2b_detail_id", 0));
					map.put("BUTTON_APP", true);
					if (b2bOrder.get("b2b_order_status", 0) == 1) {
						map.put("BUTTON_DELETE", true);
						map.put("BUTTON_CANCEL", false);
					} else if (b2bOrder.get("b2b_order_status", 0) >= 8 ) {
						map.put("BUTTON_CANCEL", false);
						map.put("BUTTON_DELETE", false);
					} else {
						map.put("BUTTON_CANCEL", true);
						map.put("BUTTON_DELETE", false);
					}
					list.add(map);
				}
				returnModel.setAUTHS(list);
				
			} else {
				returnModel.setINFO("没有数据");
				returnModel.setDATA(d);
			}
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(new ArrayList<>());
			returnModel.setINFO(e.toString());
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 
	 * 删除B2B订单item
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String deleteB2BOrderItem(HttpServletRequest request,String b2b_detal_id) throws Exception {
//		
		DBRow dbRow = this.b2BOrderMgrZyj.getB2BOrderItem(Long.valueOf(b2b_detal_id));
		ReturnModel returnModel = new ReturnModel();
		try {
			for (String s : b2b_detal_id.split(",")) {
				this.b2BOrderMgrZyj.deleteB2BOrderItem(Long.valueOf(s));
			}
			// -----------插入日志----------
			
			int b2b_oid = dbRow.get("b2b_oid", 0);
			this.insertLog(b2b_oid, "delete  order line ", B2BOrderLogTypeKey.Update, request);
			
			returnModel.setINFO("删除成功");
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 删除B2B订单主单
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String deleteB2BOrder(HttpServletRequest request) throws Exception {
		long b2b_oid = StringUtil.getLong(request, "data");
		ReturnModel returnModel = new ReturnModel();
		try {
			DBRow row = b2BOrderMgrZyj.getB2BOrder(b2b_oid);
			this.b2BOrderMgrZyj.deleteB2BOrder(b2b_oid);
			
			this.insertLog(b2b_oid, "delete  order # b2b_oid:" + b2b_oid, B2BOrderLogTypeKey.Update, request);
			//this.editB2BOrderIndex(b2b_oid, "del");
			try {
				B2BOrderIndexMgr.getInstance().deleteIndex(b2b_oid);
			} catch (Exception e) {}
			//回写WMS关闭订单
			try {
				//wmsOrderMgr.update(b2b_oid, WmsOrderUpdateTypeKey.CANCEL);
				if(CommonUtils.isElementCustomer(String.valueOf(row.get("customer_id", 0L))))
						wmsOrderMgr.delete(row.getString("company_id", ""), row.get("customer_id", 0L), row.get("b2b_order_number",""), row.get("send_psid", 0L));
			} catch (Exception e) {}
			
			returnModel.setINFO("删除成功");
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 更新B2B订单主单
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String updateB2BOrder(HttpServletRequest request) throws Exception {
		String json = StringUtil.getString(request, "data");
		ReturnModel returnModel = new ReturnModel();
		
		try {
			JSONObject jo = new JSONObject(json);
			DBRow dbRow = DBRowUtils.convertToDBRow(jo);
			dbRow.put("updatedate", new Date());
			Long b2b_oid = dbRow.get("b2b_oid", 0l);
			DBRow obj = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
			int send_psid = obj.get("send_psid", 0);
			B2BOrderJsonActionUtil.EditDate(dbRow, send_psid);
			if (b2b_oid != 0) {
				this.b2BOrderMgrZyj.updateB2BOrder(b2b_oid, dbRow);
				this.editB2BOrderIndex(b2b_oid, "update");
				returnModel.setINFO("更新成功");
			}
			// -----------插入日志----------
			String log = "";
			
			if (dbRow.getFieldNames().contains("MABD")) {
				log += " Modify Order Info.";
			}
			if (dbRow.getFieldNames().contains("SEND_PSID")) {
				log += " Modify Ship Info.";
			}
			if (dbRow.getFieldNames().contains("FREIGHT_TERM")) {
				log += " Modify Bill Info.";
			}
			if (dbRow.getFieldNames().contains("CARRIERS") || dbRow.getFieldNames().contains("LOAD_NO") || dbRow.getFieldNames().contains("PICKUP_APPOINTMENT")) {
				log += " Modify Transport Info.";
			}
			//
			this.insertLog(b2b_oid, "" + log , B2BOrderLogTypeKey.Update, request);
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 更新B2B订单明细
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午12:17:13<br>
	 * @author: cuicong
	 */
	public String updateB2BOrderItem(HttpServletRequest request,String json) throws Exception {
		ReturnModel returnModel = new ReturnModel();
		
		try {
			JSONObject jo = new JSONObject(json);
			DBRow dbRow = DBRowUtils.convertToDBRow(jo);
			dbRow.remove("_index");
			Long b2b_detail_id = dbRow.get("b2b_detail_id", 0l);
			int b2b_oid=0;
			if (b2b_detail_id != 0) {
				
				DBRow product = productMgrIFace.getDetailProductByPname(dbRow.get("b2b_p_name", ""));
				if (product == null) {
					throw new Exception("不存在此商品" + dbRow.get("b2b_p_name", ""));
				}
				dbRow.add("b2b_pc_id", product.get("pc_id", 0l));
				dbRow.add("b2b_p_code", product.get("lot_number", 0l));
				dbRow.add("b2b_wait_count", dbRow.get("b2b_count"));
				
				this.b2BOrderMgrZyj.updateB2BOrderItem(b2b_detail_id, dbRow);
				returnModel.setINFO("更新成功");
				// -----------插入日志----------
				b2b_oid = dbRow.get("b2b_oid", 0);
				this.insertLog(b2b_oid, "update  orderItem :"+String.valueOf(dbRow.getFieldNames()) , B2BOrderLogTypeKey.Update, request);
				
			}
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO(e.getMessage());
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	public String fillterB2BOrder2(DBRow dbRow,PageCtrl pc) throws Exception {
		ReturnModel returnModel = new ReturnModel();
		try {
			//判断是否是VIZIO，VZB
			if ("4".equalsIgnoreCase(dbRow.get("customer_id", "")) ||"5".equalsIgnoreCase(dbRow.get("customer_id", ""))) {
				//同步wms数据
				//SingleTask.getinstanse().setTask(b2bScheduledTasks);
				List<Long> ids = b2bScheduledTasks.run(("0000"+dbRow.get("send_psid", "")).intern());
				if (ids.size()==0) {
					returnModel.setCODE("200");
					returnModel.setDATA(new ArrayList<>());
					returnModel.setINFO("No Order");
					returnModel.setPAGECTRL(pc);
					return new JSONObject(returnModel).toString();
				}
				this.margeB2BOrdersIndex(ids, dbRow.get("send_psid", 0l));
			}
			DBRow[] d = this.b2BOrderMgrZyj.fillterB2BOrder(dbRow, pc);
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			if (d.length > 0) {// 有数据
				//log.info(d.length);
				for (DBRow row : d) {
					// 时间处理
					int jet_lag = row.get("jet_lag", 0);//
					DBRow[] logs = new DBRow[0];
					//DBRow [] lines=b2BOrderMgrZyj.fillterB2BOrderItem(row.get("b2b_oid", 0l),"","");
					B2BOrderJsonActionUtil.getB2BOrderDate(row, jet_lag, row.get("send_psid", 0),  new DBRow[0]);
					B2BOrderJsonActionUtil.getB2bOrderLines(row,  new DBRow[0]);
					list.add(B2BOrderJsonActionUtil.setAUTHS(row));
				}
				//log.info("fillterB2BOrderf");
				returnModel.setAUTHS(list);
				returnModel.setCODE("200");
				returnModel.setDATA(d);
				returnModel.setINFO("过滤成功");
				
				returnModel.setPAGECTRL(pc);
			} else {
				returnModel.setCODE("200");
				returnModel.setDATA(new ArrayList<>());
				returnModel.setINFO("没有符合条件的结果");
				returnModel.setPAGECTRL(pc);
			}
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(new ArrayList<>());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 
	 * 过滤表单数据
	 * 
	 * @param request
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-26上午11:16:55<br>
	 * @author: cuicong
	 */
	public String fillterB2BOrder(DBRow dbRow,PageCtrl pc) throws Exception {
		ReturnModel returnModel = new ReturnModel();
		try {
			DBRow[] d = this.b2BOrderMgrZyj.fillterB2BOrder(dbRow, pc);
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			if (d.length > 0) {// 有数据
				for (DBRow row : d) {
					// 时间处理
					int jet_lag = row.get("jet_lag", 0);//
					DBRow[] logs = b2BOrderMgrZyj.getB2BOrderLogs(row.get("b2b_oid", 0l), 4);
					DBRow [] lines=b2BOrderMgrZyj.fillterB2BOrderItem(row.get("b2b_oid", 0l),"","");
					//if (logs!=null && logs.length>0)
					B2BOrderJsonActionUtil.getB2BOrderDate(row, jet_lag, row.get("send_psid", 0), logs);
					B2BOrderJsonActionUtil.getB2bOrderLines(row, lines);
					B2BOrderJsonActionUtil.getB2bOrderSOLines(row, lines);
					B2BOrderJsonActionUtil.getB2bOrderPOLines(row, lines);
					list.add(B2BOrderJsonActionUtil.setAUTHS(row));
				}
				returnModel.setAUTHS(list);
				returnModel.setCODE("200");
				returnModel.setDATA(d);
				returnModel.setINFO("过滤成功");
				
				returnModel.setPAGECTRL(pc);
			} else {
				returnModel.setCODE("200");
				returnModel.setDATA(new ArrayList<>());
				returnModel.setINFO("没有符合条件的结果");
				returnModel.setPAGECTRL(pc);
			}
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(new ArrayList<>());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	public String addTransport(HttpServletRequest request) throws Exception {
		String json = StringUtil.getString(request, "data");
		
		ReturnModel returnModel = new ReturnModel();
		try {
			JSONArray ja = new JSONArray(json);
			for (int i = 0; i < ja.length(); i++) {
				 DBRow  order = b2BOrderMgrZyj.getB2BOrder(ja.getLong(i));
				 if (order.get("b2b_order_status", 0)==2) {
					 //仅确认库存的order生成运单 2015/3/12 by wangcr
					 transportMgrIFaceZJ.addB2BOrderTransport(ja.getLong(i), request);
				 }
				//DBRow dbRow = new DBRow();
				//dbRow.add("b2b_order_status", 2);
				//this.b2BOrderMgrZyj.updateB2BOrder(ja.getLong(i), dbRow);
			}
			returnModel.setINFO("添加运单成功");
			
		} catch (Exception e) {
			returnModel.setCODE("500");
			returnModel.setDATA(e.toString());
			returnModel.setINFO("Exception");
			e.printStackTrace();
		}
		return new JSONObject(returnModel).toString();
	}
	
	public void setTransportMgrIFaceZJ(TransportMgrIFaceZJ transportMgrIFaceZJ) {
		this.transportMgrIFaceZJ = transportMgrIFaceZJ;
	}
	
	/**
	 * 回退确认。
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String reCommit(HttpServletRequest request) throws Exception {
		ReturnModel returnModel = new ReturnModel();
		long b2b_oid = StringUtil.getLong(request, "data");
		DBRow obj = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
		if (obj!=null && obj.get("b2b_oid", 0)>0 && obj.get("b2b_order_status", 0)==2) {
			try {
				DBRow row = new DBRow();
				row.add("b2b_order_status", 10);
				this.b2BOrderMgrZyj.updateB2BOrder(b2b_oid, row);
				this.insertLog(b2b_oid, "Revoke Commitment" , B2BOrderLogTypeKey.Update, request);
			} catch (Exception e) {
				returnModel.setCODE("500");
				returnModel.setINFO(e.getMessage());
			}
		} else {
			returnModel.setCODE("500");
			returnModel.setINFO("No find data");
		}
		return new JSONObject(returnModel).toString();
	}
	
	/**
	 * 修改b2b文本索引，加入 DN， PO
	 * 
	 * @param b2b_oid
	 * @param type ('add','update','del')
	 * @throws Exception
	 */
	public void editB2BOrderIndex(long b2b_oid, String type) throws Exception {
		// DBRow oldB2BOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
		DBRow oldB2BOrder = this.b2BOrderMgrZyj.getB2BOrder(b2b_oid);
		
		String sendStorage = "";
		// DBRow sendProductStorageCatalog =
		// floorCatalogMgr.getDetailProductStorageCatalogById(oldB2BOrder.get("send_psid", 0l));
		DBRow sendProductStorageCatalog = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(oldB2BOrder.get(
				"send_psid", 0));
		if (null != sendProductStorageCatalog) {
			sendStorage = sendProductStorageCatalog.getString("title");
		}
		
		// DBRow receiveProductStorage =
		// floorCatalogMgr.getDetailProductStorageCatalogById(oldB2BOrder.get("receive_psid", 0l));
		DBRow receiveProductStorage = productStorageMgrIFaceZJ.getProductStorageCatalogByPsid(oldB2BOrder.get(
				"receive_psid", 0));
		
		if (type.equals("add")) {
			B2BOrderIndexMgr.getInstance().addIndex(b2b_oid, sendStorage, receiveProductStorage.getString("title"),
					oldB2BOrder.getString("b2b_order_waybill_number"), oldB2BOrder.getString("b2b_order_waybill_name"),
					oldB2BOrder.getString("carriers"), oldB2BOrder.getString("customer_dn"),
					oldB2BOrder.getString("po"));
		} else if (type.equals("update")) {
			B2BOrderIndexMgr.getInstance().updateIndex(b2b_oid, sendStorage,
					receiveProductStorage != null ? receiveProductStorage.getString("title") : "",
					oldB2BOrder.getString("b2b_order_waybill_number"), oldB2BOrder.getString("b2b_order_waybill_name"),
					oldB2BOrder.getString("carriers"), oldB2BOrder.getString("customer_dn"),
					oldB2BOrder.getString("po"));
		} else if (type.equals("del")) {
			B2BOrderIndexMgr.getInstance().deleteIndex(b2b_oid);
		}
	}
	
	public void insertLog(long b2b_oid, String content, int key, HttpServletRequest request) throws Exception {
		// -----------插入日志----------
		AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		DBRow log = new DBRow();
		log.add("b2b_oid", b2b_oid);
		log.add("b2b_content", content);
		log.add("b2ber_id", adminLoginBean.getAdid());
		log.add("b2ber", adminLoginBean.getEmploye_name());
		log.add("b2b_type", key);
		log.add("activity_id", 1);
		log.add("b2b_date", new Timestamp(new Date().getTime()));
		
		b2BOrderMgrZyj.addB2BOrderLog(log);
		
	}
	public void margeB2BOrdersIndex(List<Long> ids, Long psid) {
		try {
			DBRow[] rows = this.b2BOrderMgrZyj.getB2BOrders(ids);
			//后台运行建索引
			new TaskB2BOrderIndex(rows, psid).start();
			//B2BOrderIndexMgr.getInstance().mageIndexs(rows);
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public String getAllFreight(HttpServletRequest request) throws Exception {
		List<Map<String,Object>> rt = new ArrayList<Map<String,Object>>();
		for (DBRow row : floorCheckInMgrZr.getAllCarrier()) {
			Map<String,Object> item = new HashMap<String,Object>();
			item.put("text", row.get("carrier",""));
			item.put("value", row.get("scac",""));
			rt.add(item);
		}
		return new JSONArray(rt).toString();
	}
}
