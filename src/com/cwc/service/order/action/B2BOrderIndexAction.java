package com.cwc.service.order.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class B2BOrderIndexAction extends ActionFatherController {
	static Logger log = Logger.getLogger("PLATFORM");
	/**
	 * 添加文本索引
	 * @param b2b_oid
	 * @param pickUpStorage
	 * @param receiveStorage
	 * @param trackingNumber
	 * @param shippingCompany
	 * @param carriers
	 * @param dn
	 * @param po
	 * @throws Exception
	 */
	public void addIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers, String dn, String po) throws Exception {
		B2BOrderIndexMgr.getInstance().addIndex(b2b_oid, pickUpStorage, receiveStorage, trackingNumber, shippingCompany, carriers, dn, po);
	}
	
	/**
	 * 更新文本索引
	 * @param b2b_oid
	 * @param pickUpStorage
	 * @param receiveStorage
	 * @param trackingNumber
	 * @param shippingCompany
	 * @param carriers
	 * @param dn
	 * @param po
	 * @throws Exception
	 */
	public void updateIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers, String dn, String po) throws Exception {
		B2BOrderIndexMgr.getInstance().updateIndex(b2b_oid, pickUpStorage, receiveStorage, trackingNumber, shippingCompany, carriers, dn, po);
	}
	
	/**
	 * 清除文本索引
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void deleteIndex(long  b2b_oid)throws Exception {
		B2BOrderIndexMgr.getInstance().deleteIndex(b2b_oid);
	}
	
	/**
	 * 查询文本索引
	 * @param key
	 * @param search_mode
	 * @param page_count
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc) throws Exception {
		return B2BOrderIndexMgr.getInstance().getSearchResults(key, search_mode, page_count, pc);
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String action = StringUtil.getString(request, "action");
		try {
			Map<String,Object> rt = new HashMap<String,Object>();
			if ("add".equals(action)) {
				long b2b_oid = StringUtil.getLong(request, "oid");
				if (b2b_oid==0) throw new Exception("Required long parameter 'oid' is not present");
				String pickUpStorage = StringUtil.getString(request, "pickUpStorage");
				String receiveStorage = StringUtil.getString(request, "receiveStorage");
				String trackingNumber = StringUtil.getString(request, "trackingNumber");
				String shippingCompany = StringUtil.getString(request, "shippingCompany");
				String carriers = StringUtil.getString(request, "carriers");
				String dn = StringUtil.getString(request, "dn");
				String po = StringUtil.getString(request, "po");
				//String so = StringUtil.getString(request, "so");
				this.addIndex(b2b_oid, pickUpStorage, receiveStorage, trackingNumber, shippingCompany, carriers, dn, po);
			} else if ("update".equals(action)) {
				long b2b_oid = StringUtil.getLong(request, "oid");
				if (b2b_oid==0) throw new Exception("Required long parameter 'oid' is not present");
				String pickUpStorage = StringUtil.getString(request, "pickUpStorage");
				String receiveStorage = StringUtil.getString(request, "receiveStorage");
				String trackingNumber = StringUtil.getString(request, "trackingNumber");
				String shippingCompany = StringUtil.getString(request, "shippingCompany");
				String carriers = StringUtil.getString(request, "carriers");
				String dn = StringUtil.getString(request, "dn");
				String po = StringUtil.getString(request, "po");
				//String so = StringUtil.getString(request, "so");
				this.updateIndex(b2b_oid, pickUpStorage, receiveStorage, trackingNumber, shippingCompany, carriers, dn, po);
			} else if ("delete".equals(action)) {
				long b2b_oid = StringUtil.getLong(request, "oid");
				if (b2b_oid==0) throw new Exception("Required long parameter 'oid' is not present");
				this.deleteIndex(b2b_oid);
			} else if ("search".equals(action)) {
				String key = StringUtil.getString(request, "key");
				if (key==null || "".equals(key)) throw new Exception("Required string parameter 'key' is not present");
				int search_mode = StringUtil.getInt(request, "search_mode");
				PageCtrl pc = new PageCtrl();// 分页数据
				pc.setPageNo(StringUtil.getInt(request, "pageNo"));
				pc.setPageSize(StringUtil.getInt(request, "pageSize"));
				rt.put("data", getSearchResults(key, search_mode, pc.getPageSize(), pc));
			} else {
				throw new Exception("No Action");
			}
			rt.put("code","200");
			rt.put("info", "");
			this.writerObject(rt, response, null);
		} catch (Exception e) {
			Map<String,Object> rt = new HashMap<String,Object>();
			rt.put("code","400");
			rt.put("error", e.getMessage());
			rt.put("info", e.getMessage());
			writerObject(rt, response, 400);
		}
	}
	
	@SuppressWarnings("deprecation")
	private void writerObject(Map<String,?> obj,HttpServletResponse response, Integer error) {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control","no-cache");
		if (error!=null) response.setStatus(error);
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
			new JSONObject(obj).write(pw);
			pw.flush();
		} catch (JSONException e) {
			response.setStatus(400, e.getMessage());
			log.error(e);
		} catch (IOException e) {
			log.error(e);
			response.setStatus(400, e.getMessage());
		} finally {
			if (pw!=null) pw.close();
		}
	}
}