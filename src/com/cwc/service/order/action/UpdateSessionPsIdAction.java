package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateSessionPsIdAction extends ActionFatherController{
	
	private  CartQuoteIFace cart;
	
	public void setCart(CartQuoteIFace cart) {
		this.cart = cart;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		  
			long ps_id = StringUtil.getLong(request, "ps_id");
			HttpSession session = request.getSession();
			cart.updateSessionPsId(session, ps_id);
			throw new JsonException("success");	
	}

}
