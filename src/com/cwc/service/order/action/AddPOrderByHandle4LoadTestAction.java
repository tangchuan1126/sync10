package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 手工添加订单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddPOrderByHandle4LoadTestAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long oid = 0;
		
		try
		{
			oid = orderMgr.addPOrderByHandle4LoadTest(request);
		}
		catch(Exception e)
		{
		}
		
		throw new WriteOutResponseException(String.valueOf(oid));
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}


}
