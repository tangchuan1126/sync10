package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 检查订单打印状态
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CheckOrderPrintedAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		boolean flag;
		
		try
		{
			flag = orderMgr.checkOrderPrinted(request);
		}
		catch(Exception e)
		{
			flag = false;
		}
		
		if (flag)
		{
			throw new WriteOutResponseException("1");//打印成功
		}
		else
		{
			throw new WriteOutResponseException("0");
		}
		
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}


}
