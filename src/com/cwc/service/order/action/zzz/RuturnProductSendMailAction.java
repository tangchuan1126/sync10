package com.cwc.service.order.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class RuturnProductSendMailAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
/**
 * 正常质保退货后发送邮件给客户
 * @param arg0
 * @param arg1
 * @throws WriteOutResponseException
 * @throws RedirectRefException
 * @throws ForwardException
 * @throws Forward2JspException
 * @throws RedirectBackUrlException
 * @throws RedirectException
 * @throws OperationNotPermitException
 * @throws PageNotFoundException
 * @throws DoNothingException
 * @throws Exception
 */
	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		long oid = StringUtil.getLong(arg0, "oid");
		long rp_id = StringUtil.getLong(arg0, "rp_id");
		orderMgr.sendMail(oid, rp_id);
		throw new WriteOutResponseException("<script>parent.closeTBWinRefrech();</script>");	
	}

}
