package com.cwc.service.order.action;

public class OrderCostBean
{
	private double productCost = 0;
	private double shippingCost = 0;
	private float weight = 0;

	private double mc_gross = 0;//订单金额
	private String mc_currency = "";//货币类型
	private String expressName = "";
	private double totalCost = 0;
	private double rmb_mc_gross = 0;

	public double getProductCost()
	{
		return productCost;
	}
	public void setProductCost(double productCost)
	{
		this.productCost = productCost;
	}
	
	public double getShippingCost()
	{
		return shippingCost;
	}
	
	public void setShippingCost(double shippingCost)
	{
		this.shippingCost = shippingCost;
	}
	
	public String getMcCurrency()
	{
		return mc_currency;
	}
	
	public void setMcCurrency(String mc_currency)
	{
		this.mc_currency = mc_currency;
	}

	public float getWeight()
	{
		return weight;
	}
	
	public void setWeight(float weight)
	{
		this.weight = weight;
	}
	
	public String getExpressName()
	{
		return expressName;
	}
	
	public void setExpressName(String expressName)
	{
		this.expressName = expressName;
	}
	
	public double getMcGross()
	{
		return mc_gross;
	}
	
	public void setMcGross(double mc_gross)
	{
		this.mc_gross = mc_gross;
	}
	
	public double getTotalCost()
	{
		return totalCost;
	}
	
	public void setTotalCost(double totalCost)
	{
		this.totalCost = totalCost;
	}
	
	public double getRmbMcGross()
	{
		return rmb_mc_gross;
	}
	
	public void setRmbMcGross(double rmb_mc_gross)
	{
		this.rmb_mc_gross = rmb_mc_gross;
	}
	
}
