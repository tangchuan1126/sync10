package com.cwc.service.order.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import us.monoid.json.JSONArray;

import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class B2BOrderJsonActionUtil {
	/**
	 * 
	 * 查询的时候，将时间的utc转变
	 * 
	 * @param row
	 * @param jet_lag
	 * @param send_psid
	 * @param logs
	 * @throws Exception <b>Date:</b>2015年2月3日下午5:01:23<br>
	 * @author: cuicong
	 */
	public static void getB2BOrderDate(DBRow row, int jet_lag, long send_psid, DBRow[] logs) throws Exception {
		SimpleDateFormat sdf = null;
		SimpleDateFormat sb = null;
		SimpleDateFormat s = null;
		SimpleDateFormat mdy =new SimpleDateFormat("MM/dd/yyyy");
//		if (jet_lag == 28800000) {// 时间格式显示方式，中文显示方式//yyyy-MM-dd HH:mm:ss
//			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//			sb = new SimpleDateFormat("MM-dd HH:mm");//
//		} else {// 英文显示方式
			sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			s = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			sb = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			
//		}
		if (!(row.get("updatedate", "").equals(""))) {
			row.add("updatedate",
					sdf.format(DateUtil.locationZoneTime(row.get("updatedate", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("customer_order_date", "").equals(""))) {
			row.add("customer_order_date",
					mdy.format(DateUtil.locationZoneTime(row.get("customer_order_date", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("eta", "").equals(""))) {
			row.add("eta", s.format(DateUtil.locationZoneTime(row.get("eta", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("etd", "").equals(""))) {
			row.add("etd", s.format(DateUtil.locationZoneTime(row.get("etd", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("mabd", "").equals(""))) {
			String mabd=s.format(DateUtil.locationZoneTime(row.get("mabd", ""), row.get("send_psid", 0)));
			if(mabd.endsWith("00:00"))
				row.add("mabd", mabd.substring(0, mabd.length()-6));
			else
				row.add("mabd", mabd);
		}
		if (!(row.get("pickup_appointment", "").equals(""))) {
			row.add("pickup_appointment",
					s.format(DateUtil.locationZoneTime(row.get("pickup_appointment", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("b2b_order_date", "").equals(""))) {
			row.add("b2b_order_date",
					sdf.format(DateUtil.locationZoneTime(row.get("b2b_order_date", ""), row.get("send_psid", 0))));
		}
		if (!(row.get("requested_date", "").equals(""))) {
			String reqDate=s.format(DateUtil.locationZoneTime(row.get("requested_date", ""), row.get("send_psid", 0)));
			
			if(reqDate.endsWith("00:00"))
				row.add("requested_date",
						reqDate.substring(0,reqDate.length()-6));
			else
				row.add("requested_date",
						reqDate);
		}
		
		if (!(row.get("ship_not_before", "").equals(""))) {
			String shipDate=s.format(DateUtil.locationZoneTime(row.get("ship_not_before", ""), row.get("send_psid", 0)));
			
			if(shipDate.endsWith("00:00"))
				row.add("ship_not_before",
						shipDate.substring(0,shipDate.length()-6));
			else
				row.add("ship_not_before",
						shipDate);
		}
		
		if (!(row.get("ship_not_later", "").equals(""))) {
			String shipNoDate=s.format(DateUtil.locationZoneTime(row.get("ship_not_later", ""), row.get("send_psid", 0)));
			
			if(shipNoDate.endsWith("00:00"))
				row.add("ship_not_later",
						shipNoDate.substring(0,shipNoDate.length()-6));
			else
				row.add("ship_not_later",
						shipNoDate);
		}
		
		row.add("LOGS", logs);
		for (DBRow b2bOrderLog : logs) {
			if (!b2bOrderLog.get("b2b_date", "").equals("")) {// 日志时间类型为
																// MM/dd
																// HH:mm
				b2bOrderLog.add("b2b_date",
						sb.format(DateUtil.locationZoneTime(b2bOrderLog.get("b2b_date", ""), row.get("send_psid", 0))));
			}
			
		}
		
	}
	
	public static void getB2bOrderLines(DBRow row, DBRow[] lines) throws Exception {
		row.add("LINES", lines);
	}
	
	public static void getB2bOrderPOLines(DBRow row, DBRow[] lines) throws Exception {
		Set<String> list=new HashSet<String>();
		for(DBRow line:lines){
			list.add(line.getString("retail_po"));
		}
		row.add("PO", list);
	}
	
	public static void getB2bOrderSOLines(DBRow row, DBRow[] lines) throws Exception {
		Set<String> list=new HashSet<String>();
		for(DBRow line:lines){
			list.add(line.getString("so"));
		}
		row.add("SO", list);
	}
	
	/**
	 * 
	 * request 转map
	 * 
	 * @param request
	 * @return <b>Date:</b>2015年2月3日下午3:36:21<br>
	 * @author: cuicong
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map getParameterMap(HttpServletRequest request) {
		// 参数Map
		Map properties = request.getParameterMap();
		// 返回值Map
		Map returnMap = new HashMap();
		Iterator entries = properties.entrySet().iterator();
		Map.Entry entry;
		String name = "";
		String value = "";
		while (entries.hasNext()) {
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();
			if (null == valueObj) {
				value = "";
			} else if (valueObj instanceof String[]) {
				String[] values = (String[]) valueObj;
				for (int i = 0; i < values.length; i++) {
					value = values[i] + ",";
				}
				value = value.substring(0, value.length() - 1);
			} else {
				value = valueObj.toString();
			}
			returnMap.put(name, value);
		}
		return returnMap;
	}
	
	/**
	 * 
	 * 设置权限
	 * 
	 * @param dbRow
	 * @return <b>Date:</b>2015年2月3日下午3:39:50<br>
	 * @author: cuicong
	 */
	public static List<Map<String, Object>> setAUTHS(DBRow[] dbRows) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		int i = 0;
		for (DBRow row : dbRows) {
			if(row==null)
				continue;
			// 设置-------权限
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("INDEX", i++);
			if (row.get("b2b_order_status", 0) == 1) {
				map.put("BUTTON_DELETE", true);
			} else {
				map.put("BUTTON_DELETE", false);
			}
			map.put("ID", row.get("b2b_oid", 0));
			map.put("BUTTON_APP", true);
			if (row.get("b2b_order_status", 0) == 2) {
				map.put("BUTTON_CANCEL", true);
			} else {
				map.put("BUTTON_CANCEL", false);
			}
			list.add(map);
		}
		return list;
	}
	
	/**
	 * 
	 * 设置权限
	 * 
	 * @param dbRow
	 * @return <b>Date:</b>2015年2月3日下午3:39:50<br>
	 * @author: cuicong
	 */
	public static Map<String, Object> setAUTHS(DBRow row) {
		int i = 0;
		// 设置-------权限
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("INDEX", i++);
		map.put("ID", row.get("b2b_oid", 0));
		map.put("BUTTON_APP", true);
		
		if (row.get("b2b_order_status", 0) == 8 || row.get("b2b_order_status", 0) == 9) {
			map.put("BUTTON_DELETE", false);
			map.put("BUTTON_CANCEL", false);
		} else if (row.get("b2b_order_status", 0) == 1) {
			map.put("BUTTON_DELETE", true);
			map.put("BUTTON_CANCEL", false);
		} else if (row.get("b2b_order_status", 0) == 2) {
			map.put("BUTTON_DELETE", false);
			map.put("BUTTON_CANCEL", true);
		} else {
			map.put("BUTTON_DELETE", false);
			map.put("BUTTON_CANCEL", true);
		}
		
		return map;
	}
	
	/**
	 * 跟新或者插入的时候对日期的处理
	 * 
	 * 
	 * @param dbRow
	 * @return <b>Date:</b>2015年2月3日下午4:10:59<br>
	 * @author: cuicong
	 * @throws Exception
	 */
	public static DBRow EditDate(DBRow dbRow, long send_psid) throws Exception {
		if (dbRow.get("eta", "").equals("")) {
			dbRow.remove("eta");
		} else {
			dbRow.add("eta",
					DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("pickup_appointment", "")), send_psid));
		}
		if (dbRow.get("pickup_appointment", "").equals("")) {
			dbRow.remove("pickup_appointment");
		} else {
			dbRow.add("pickup_appointment",
					DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("pickup_appointment", "")), send_psid));
		}
		if (dbRow.get("etd", "").equals("")) {
			dbRow.remove("etd");
		} else {
			dbRow.add("etd", DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("etd", "")), send_psid));
		}
		
		if (!dbRow.get("mabd", "").equals("")) {
			dbRow.add("mabd", DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("mabd", "")), send_psid));
		} else {
			dbRow.remove("mabd");
		}
		
		if (!dbRow.get("customer_order_date", "").equals("")) {
			dbRow.add("customer_order_date", DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("customer_order_date", "")), send_psid));
		} else {
			dbRow.remove("customer_order_date");
		}
		
		if (!dbRow.get("requested_date", "").equals("")) {
			dbRow.add("requested_date",
					DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("requested_date", "")), send_psid));
		} else {
			dbRow.remove("requested_date");
		}
		if (!dbRow.get("ship_not_before", "").equals("")) {
			dbRow.add("ship_not_before",
					DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("ship_not_before", "")), send_psid));
		} else {
			dbRow.remove("ship_not_before");
		}
		if (!dbRow.get("ship_not_later", "").equals("")) {
			dbRow.add("ship_not_later",
					DateUtil.showUTCTime(DateUtil.StringToDate(dbRow.get("ship_not_later", "")), send_psid));
		} else {
			dbRow.remove("ship_not_later");
		}
		return dbRow;
	}
	
	public static String getB2BOrderLogs(HttpServletRequest request, B2BOrderMgrIFaceZyj b2BOrderMgrZyj, int num)
			throws Exception {
		int b2b_oid = StringUtil.getInt(request, "b2b_oid");
		return new JSONArray(b2BOrderMgrZyj.getB2BOrderLogs(b2b_oid, num)).toString();
	}
}
