package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.ProductReturnOrderMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 质保申请
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MarkNormalWarrantyingAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private ProductReturnOrderMgrIFaceZJ productReturnOrderMgrZJ;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
//		long rp_id = orderMgr.markNormalWarrantying(request);
		
		long rp_id = productReturnOrderMgrZJ.markNormalWarrantying(request);
		int return_product_flag = StringUtil.getInt(request, "return_product_flag");
		long oid = StringUtil.getLong(request, "oid");
		if(return_product_flag==1)
		{
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/order/return_sendMail.html?rp_id="+rp_id+"&oid="+oid);
		}
		else
		{
			DBRow returnProduct = productReturnOrderMgrZJ.getDetailReturnProduct(rp_id);
			oid = returnProduct.get("oid",0l);
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/order/add_bill.html?rp_id="+rp_id+"&oid="+oid);
		}
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setProductReturnOrderMgrZJ(
			ProductReturnOrderMgrIFaceZJ productReturnOrderMgrZJ) {
		this.productReturnOrderMgrZJ = productReturnOrderMgrZJ;
	}
}
