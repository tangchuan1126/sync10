package com.cwc.service.order.action;

import java.util.List;
import java.util.Map;

import com.cwc.db.PageCtrl;

/**
 * b2borderjson 返回json对象 <b>Application describing:</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2015 <br>
 * <b>Company:vvme</b><br>
 * <b>Date:</b>2015年2月3日下午3:10:14<br>
 * 
 * @author cuicong
 */
public class ReturnModel {
	public String CODE = "200";
	public String INFO = "";
	public Object DATA = null;
	public PageCtrl PAGECTRL = null;
	public List<Map<String, Object>> AUTHS = null;
	
	public List<Map<String, Object>> getAUTHS() {
		return AUTHS;
	}
	
	public void setAUTHS(List<Map<String, Object>> aUTHS) {
		AUTHS = aUTHS;
	}
	
	public PageCtrl getPAGECTRL() {
		return PAGECTRL;
	}
	
	public void setPAGECTRL(PageCtrl pAGECTRL) {
		PAGECTRL = pAGECTRL;
	}
	
	public String getCODE() {
		return CODE;
	}
	
	public void setCODE(String cODE) {
		CODE = cODE;
	}
	
	public String getINFO() {
		return INFO;
	}
	
	public void setINFO(String iNFO) {
		INFO = iNFO;
	}
	
	public Object getDATA() {
		return DATA;
	}
	
	public void setDATA(Object dATA) {
		DATA = dATA;
	}
	
}