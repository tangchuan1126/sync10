package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 抄单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RecordOrderAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private MessageAlerter messageAlert;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		orderMgr.recordOrderTrue(request);
//		int flag = orderMgr.recordOrder(request);
		
		//缺货不再提示
//		if (flag==ProductStatusKey.STORE_OUT)
//		{
//			messageAlert.setMessage(request,Resource.getStringValue("","LackingRecordOrderException",""));
//		}
		
		throw new RedirectBackUrlException();
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}
	
}
