package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetWait4PrintOrdersCountAjaxAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		int c = 0;

		try
		{
			String st = StringUtil.getString(request,"st");
			String en = StringUtil.getString(request,"en");
			long ps_id = StringUtil.getLong(request, "ps_id");
			
			c = orderMgr.getWait4PrintOrdersCount( ps_id,st,en);
		}
		catch(Exception e)
		{
			throw new WriteOutResponseException("error");
		}

		throw new WriteOutResponseException(String.valueOf(c));
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

}
