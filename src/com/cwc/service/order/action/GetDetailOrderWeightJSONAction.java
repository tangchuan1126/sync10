package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.ExpressMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得JSON格式的递送人详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailOrderWeightJSONAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private SystemConfigIFace systemConfig;
	private ExpressMgrIFace expressMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		double lb_rate = 2.2d;
		long oid = StringUtil.getLong(request, "oid");
		
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		DBRow deliveryInfo = orderMgr.getDetailShipping(oid);
		
		float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
		
		float ship_weight = detailOrder.get("total_weight", 0f);
		double org_weight = MoneyUtil.round(ship_weight/weight_cost_coefficient, 2);
		
		DBRow weight = new DBRow();
		
		weight.add("ship_weight", ship_weight);
		weight.add("ship_weight_lb", MoneyUtil.round(ship_weight*lb_rate, 2));
		
		weight.add("org_weight", org_weight);
		weight.add("org_weight_lb", MoneyUtil.round(org_weight*lb_rate,2));
		
		if (detailOrder.get("sc_id", 0l)==0)
		{
			weight.add("print_weight", "?");
		}
		else
		{
			float print_weight_coefficient = expressMgr.getDetailCompany(detailOrder.get("sc_id", 0l)).get("print_weight_discount", 0f);
			double print_weight =  MoneyUtil.round(org_weight*print_weight_coefficient,2);
			weight.add("print_weight", print_weight);
			weight.add("print_weight_lb", MoneyUtil.round(print_weight*lb_rate,2));
		}
		
		if (deliveryInfo==null)
		{
			weight.add("actual_weight", "?");
		}
		else
		{
			weight.add("actual_weight", deliveryInfo.get("product_weight", 0f));
			weight.add("actual_weight_lb", MoneyUtil.round(deliveryInfo.get("product_weight", 0d)*lb_rate,2));
		}
		
		throw new JsonException(new JsonObject(weight));
	}

	
	
	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
	public void setExpressMgr(ExpressMgrIFace expressMgr)
	{
		this.expressMgr = expressMgr;
	}
	
}
