package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.InvoiceMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得JSON格式的递送人详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailInvoiceTemplateJSONAction extends ActionFatherController 
{
//	private OrderMgrIFace orderMgr;
	private InvoiceMgrIFaceZJ invoiceMgrZJ;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long invoice_id = StringUtil.getLong(request,"invoice_id");
		DBRow detail = invoiceMgrZJ.getFullInvoiceByInvoiceId(invoice_id);
		
		throw new JsonException(new JsonObject(detail));
	}

	public void setInvoiceMgrZJ(InvoiceMgrIFaceZJ invoiceMgrZJ) {
		this.invoiceMgrZJ = invoiceMgrZJ;
	}

//	public void setOrderMgr(OrderMgrIFace orderMgr)
//	{
//		this.orderMgr = orderMgr;
//	}

}
