package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得JSON格式的递送人详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailOrderCostJSONAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private SystemConfigIFace systemConfig;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long oid = StringUtil.getLong(request, "oid");
		
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		
		double product_cost_coefficient = systemConfig.getDoubleConfigValue( "product_cost_coefficient" );
		double shipping_cost_coefficient = systemConfig.getDoubleConfigValue( "shipping_cost_coefficient" );
		
		double product_cost = detailOrder.get("product_cost", 0d);
		double shipping_cost = detailOrder.get("shipping_cost", 0d);
		
		DBRow cost = new DBRow();
		cost.add("product_cost", product_cost);
		cost.add("shipping_cost", shipping_cost);
		
		product_cost = MoneyUtil.round(product_cost/product_cost_coefficient, 2);
		shipping_cost = MoneyUtil.round(shipping_cost/shipping_cost_coefficient, 2);
		
		cost.add("product_cost_cul", product_cost);
		cost.add("shipping_cost_cul", shipping_cost);
		
		throw new JsonException(new JsonObject(cost));
	}

	
	
	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
	
}
