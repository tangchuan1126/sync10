package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 获得JSON格式的递送人详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetAllCountrysJSONAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		throw new JsonException(new JsonObject( orderMgr.getAllCountryCode() ));
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

}
