package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

import com.cwc.util.StringUtil;

public class VolidateTxnIdAction extends ActionFatherController {
	private OrderMgrIfaceZR orderMgrZr;
	 
	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr) {
		this.orderMgrZr = orderMgrZr;
	}
	 

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			String txn_id = StringUtil.getString(request,"txn_id");
			DBRow row = orderMgrZr.getOrderByTxnId(txn_id);
			String returnString = (row != null ? "isexit":"notexit");
			 
			throw new JsonException(returnString);
			 
		
	}

}
