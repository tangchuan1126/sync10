package com.cwc.service.order.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.order.print.ExpressPolicyCore;
import com.cwc.app.order.print.ExpressPrintBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class GetShippingFeeAction extends ActionFatherController{
	//根据重量来计算一个仓库下的运费
	private  ExpressMgrIFace expressMgr;

	public void setExpressMgr(ExpressMgrIFace expressMgr) {
		this.expressMgr = expressMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		 
		String cart_product_types = StringUtil.getString(request, "cart_product_types");
		String cart_pids = StringUtil.getString(request, "cart_pids");
		String cart_quantitys = StringUtil.getString(request,"cart_quantitys");
		long pro_id = StringUtil.getLong(request,"pro_id"); //省份
		long ps_id = StringUtil.getLong(request,"ps_id");	//仓库
		long ccid= StringUtil.getLong(request,"ccid");		//国家
		
		DBRow[] results = null;
		ArrayList<DBRow> allList = new ArrayList<DBRow>();
		if(cart_product_types.trim().length() > 0 && cart_pids.trim().length() > 0 && cart_quantitys.trim().length() > 0)
		{
			 String[] arrayType = cart_product_types.split(",");
			 String[] arrayPid = cart_pids.split(",");
			 String[] arrayQuantity = cart_quantitys.split(",");
			 
			 DBRow[] rows = null;
			 if(arrayType.length == arrayPid.length && arrayPid.length == arrayQuantity.length){
				 
				 // 构造出购物车 cart_pid ,cart_quantity,cart_product_type
				 rows = new DBRow[arrayType.length];
				
				 for(int index = 0 , count = arrayType.length ; index < count ; index++ ){
					 DBRow row = new DBRow();
					 row.add("cart_pid", arrayPid[index]);
					 row.add("cart_quantity", arrayQuantity[index]);
					 row.add("cart_product_type", arrayType[index]);
					 rows[index] = row;
				 }
					ExpressPolicyCore expressPolicy = (ExpressPolicyCore)MvcUtil.getBeanFromContainer("NotLimitWeightExpressPolicyImp");//catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("express_policy_class");
					expressPolicy.setCcid(ccid);
					expressPolicy.setProId(pro_id);
					expressPolicy.setPsId(ps_id);
					ExpressPrintBean expressPrintBean[]   = expressPolicy.getNewExpressPrintPage(rows);
					
					ArrayList<ExpressPrintBean> selectExpressPrintBean = new ArrayList<ExpressPrintBean>();
					ArrayList<ExpressPrintBean> noSelectExpressPrintBean = new ArrayList<ExpressPrintBean>();
					
					ExpressPrintBean tmpShippingInfoBean;
					for (int sorti=0; sorti<expressPrintBean.length; sorti++)
					{
						for (int sortj=sorti+1; sortj<expressPrintBean.length; sortj++)
						{
							if (expressPrintBean[sorti].getShippingFee()>expressPrintBean[sortj].getShippingFee())
							{
								tmpShippingInfoBean = expressPrintBean[sorti];
								expressPrintBean[sorti] = expressPrintBean[sortj];
								expressPrintBean[sortj] = tmpShippingInfoBean;
							}
						}
					}
					
					for (int i = 0; i < expressPrintBean.length; i++) 
					{
						if(expressPrintBean[i].isSelectAuto())
						{
							selectExpressPrintBean.add(expressPrintBean[i]);
						}
						else
						{
							noSelectExpressPrintBean.add(expressPrintBean[i]);
						}
					}
					
					
					for (int i = 0; i < selectExpressPrintBean.size(); i++)
					{
						DBRow resultRow = new DBRow();
						resultRow.add("name",selectExpressPrintBean.get(i).getName());
						resultRow.add("shipping_fee",selectExpressPrintBean.get(i).getShippingFee());
						resultRow.add("use_type",selectExpressPrintBean.get(i).getUseType());
						resultRow.add("sc_id",selectExpressPrintBean.get(i).getScID());
						resultRow.add("select_auto",selectExpressPrintBean.get(i).isSelectAuto());
						
						
						allList.add(resultRow);
					}
					
					for (int i = 0; i < noSelectExpressPrintBean.size(); i++) 
					{
						DBRow resultRow = new DBRow();
						resultRow.add("name",noSelectExpressPrintBean.get(i).getName());
						resultRow.add("shipping_fee",noSelectExpressPrintBean.get(i).getShippingFee());
						resultRow.add("use_type",noSelectExpressPrintBean.get(i).getUseType());
						resultRow.add("sc_id",noSelectExpressPrintBean.get(i).getScID());
						resultRow.add("select_auto",noSelectExpressPrintBean.get(i).isSelectAuto());
						
						
						allList.add(resultRow);
					}
					 
//					results = new DBRow[expressPrintBean.length] ;
//					for(int index = 0 , count = expressPrintBean.length ;index < count ; index++ ){
//						DBRow resultRow = new DBRow();
//						resultRow.add("name", 	expressPrintBean[index].getName());
//						resultRow.add("shipping_fee", expressPrintBean[index].getShippingFee());
//						resultRow.add("use_type", expressPrintBean[index].getUseType());
//						resultRow.add("sc_id", expressPrintBean[index].getScID());
//						results[index] = resultRow;
//					}
			 }

		} 
		throw new JsonException(new JsonObject(allList.toArray(new DBRow[0])));
		 
	}
	
}

