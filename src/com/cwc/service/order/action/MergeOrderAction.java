package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.order.LackingOrderCanBeMergePrintedOrderException;
import com.cwc.app.exception.order.MergeOrderNotExistException;
import com.cwc.app.exception.order.MergeOrderNotSelfException;
import com.cwc.app.exception.order.MergeOrderStatusConflictException;
import com.cwc.app.exception.order.MergeOrderStatusIncorrectException;
import com.cwc.app.exception.order.MergeOrderStorageNotMatchException;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 合并订单 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MergeOrderAction extends ActionFatherController 
{
	private OrderMgrIFace orderMgr;
	private MessageAlerter messageAlert;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			orderMgr.mergeOrder(request);
		}
		catch (MergeOrderNotSelfException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","MergeOrderNotSelfException",""));
		}
		catch (MergeOrderNotExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","MergeOrderNotExistException",""));
		}
		catch (MergeOrderStatusIncorrectException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","MergeOrderStatusIncorrectException",""));
		}
		catch (MergeOrderStatusConflictException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","MergeOrderStatusConflictException",""));
		}
		catch (LackingOrderCanBeMergePrintedOrderException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","LackingOrderCanBeMergePrintedOrderException",""));
		}
		catch (MergeOrderStorageNotMatchException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","MergeOrderStorageNotMatchException",""));
		}
		
		throw new RedirectBackUrlException();
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}
}
