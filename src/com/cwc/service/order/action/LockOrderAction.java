package com.cwc.service.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

import com.cwc.app.api.OrderLock;
import com.cwc.util.StringUtil;

/**
 * 锁住订单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class LockOrderAction extends ActionFatherController 
{
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long oid = StringUtil.getLong(request, "oid");
		int operate_type = StringUtil.getInt(request, "operate_type");
		
		DBRow locker = OrderLock.lockOrder(StringUtil.getSession(request),oid, operate_type);
		
		if (locker==null)
		{
			throw new WriteOutResponseException("");		
		}
		else
		{
			//锁定人|操作内容
			throw new WriteOutResponseException(locker.getString("operator")+"|"+locker.getString("operate_type"));		
		}
	}

}
