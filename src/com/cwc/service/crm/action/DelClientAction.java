package com.cwc.service.crm.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CrmMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DelClientAction extends ActionFatherController 
{
	private CrmMgrIFace crmMgr;

	public void setCrmMgr(CrmMgrIFace crmMgr) 
	{
		this.crmMgr = crmMgr;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		crmMgr.delClientByCcId(request);
		
		throw new RedirectRefException();
	}



}
