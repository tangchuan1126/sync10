package com.cwc.service.title.action.zwb;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxLoadThirdProductCatalogAction extends ActionFatherController{
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	    
		String product_catalog_id=StringUtil.getString(request,"id");
		long number=StringUtil.getLong(request,"atomicBomb");
		
	
		String title_id=StringUtil.getString(request,"title_id");		
		
		DBRow[] row = proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(true, 0, "",product_catalog_id,"", title_id, 0, 0, null, request);
			
		throw new JsonException(new JsonObject(row));
		
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	
	

	
}
