package com.cwc.service.title.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateTitleProductLineAction extends ActionFatherController{
	
	private ProductLineMgrIfaceZwb productLineMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	     
		long productLineId=StringUtil.getLong(request,"productLineId");
		long titleId=StringUtil.getLong(request,"titleId");
		long toTitleId=StringUtil.getLong(request,"toTitleId");
		this.productLineMgrZwb.updateTitleByProductLine(productLineId,titleId,toTitleId);
		
	}

	public void setProductLineMgrZwb(ProductLineMgrIfaceZwb productLineMgrZwb) {
		this.productLineMgrZwb = productLineMgrZwb;
	}
	
}
