package com.cwc.service.title.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxLotNumberByTitleAction extends ActionFatherController{
	
	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	    
		String titleId=StringUtil.getString("titleId");
		DBRow[] row=storageCatalogMgrZyj.findProductLotNumberIdAndNameByTitlePcIdAdmin(true,0,titleId,0,null,request);
		throw new JsonException(new JsonObject(row));
		
	}
	

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj) {
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}
	
}
