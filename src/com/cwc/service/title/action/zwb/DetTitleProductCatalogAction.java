package com.cwc.service.title.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DetTitleProductCatalogAction extends ActionFatherController{
	
	private ProductLineMgrIfaceZwb productLineMgrZwb;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	     
		long productCatalogId=StringUtil.getLong(request,"productCatalogId");
		String titleId=StringUtil.getString(request,"titleId");
		String[] titleIds = titleId.split(",");
		boolean flag=false;
		int num =0;
		for(int i=0;i<titleIds.length; i++){
			num=this.productLineMgrZwb.detTitleByProductCatalog(productCatalogId, Integer.parseInt(titleIds[i]));
			if(num==1){
				//error
				flag = true;
			}
		}
		
        DBRow dbrow = new DBRow();
        if(flag){
        	dbrow.add("pd","error");
        }else{
        	dbrow.add("pd","ok");
        }
		throw new JsonException(new JsonObject(dbrow));
		
	}

	public void setProductLineMgrZwb(ProductLineMgrIfaceZwb productLineMgrZwb) {
		this.productLineMgrZwb = productLineMgrZwb;
	}
	
}
