package com.cwc.service.container.action.zyz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zl.ContainerMgrIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ExportContainerAction extends ActionFatherController{

	private ContainerMgrIFaceZYZ containerMgrZYZ;
	
	public void setContainerMgrZYZ(ContainerMgrIFaceZYZ containerMgrZYZ) {
		this.containerMgrZYZ = containerMgrZYZ;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception {
	
		String path = containerMgrZYZ.exportContainer(request);
		
		String basePath = "";
		DBRow db = new DBRow();
				
		if (!path.equals("0")){
			
			db.add("flag",true);
			basePath = "../../" + path;
		} else {
			
			db.add("flag",false);
		}
				
		db.add("fileurl",basePath);
				
		throw new JsonException(new JsonObject(db));
	}
}
