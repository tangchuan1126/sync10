package com.cwc.service.container.action.zyz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zl.ContainerMgrIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DeleteContainerTypeAction extends ActionFatherController{

	private ContainerMgrIFaceZYZ containerMgrZYZ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

			long typeId = StringUtil.getLong(request,"typeId");
			int container_type = StringUtil.getInt(request, "container_type");
			long delContainer = containerMgrZYZ.deleteContainerType(typeId, container_type);
			
			 DBRow result = new DBRow();
		    if(delContainer>0){
		    	 result.add("flag", true);
		    }else{
		    	result.add("flag", false);
		    }
		    
		    throw new JsonException(new JsonObject(result));
		
	}
	
	public void setContainerMgrZYZ(ContainerMgrIFaceZYZ containerMgrZYZ) {
		this.containerMgrZYZ = containerMgrZYZ;
	}

	
}
