package com.cwc.service.container.action.zyz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.kaha.ContainerId;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zl.ContainerMgrIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class SaveImportContainersAction extends ActionFatherController{

	private ContainerMgrIFaceZYZ containerMgrZYZ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		containerMgrZYZ.uploadContainers(request);
		DBRow db = new DBRow();
		db.add("flag",true);
		
		throw new JsonException(new JsonObject(db));
		
	}
	public void setContainerMgrZYZ(ContainerMgrIFaceZYZ containerMgrZYZ) {
		this.containerMgrZYZ = containerMgrZYZ;
	}
	
	
}
