package com.cwc.service.waybill.action.zr;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
 

public class UpdateWayBillStatusAction extends  ActionFatherController{

	private WayBillMgrIfaceZR wayBillMgrZr;
	
	 
	public void setWayBillMgrZr(WayBillMgrIfaceZR wayBillMgrZr) {
		this.wayBillMgrZr = wayBillMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
	 
	 
		String waybill_id = StringUtil.getString(request,"waybill_id");
		String adid = StringUtil.getString(request, "adid");
		int state = Integer.parseInt(StringUtil.getString(request, "status"));
		DBRow row = new DBRow();
		row.add("status", state);
		row.add("print_account", adid);
		row.add("print_date", DateUtil.NowStr());
		wayBillMgrZr.updateWayBillStatus(Long.parseLong(waybill_id),row);
		throw new JsonException("success");	
		 
	}

}
