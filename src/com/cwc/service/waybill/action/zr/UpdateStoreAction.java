package com.cwc.service.waybill.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateStoreAction  extends  ActionFatherController{

	private WayBillMgrIfaceZR wayBillMgrZr;
	
	 
	public void setWayBillMgrZr(WayBillMgrIfaceZR wayBillMgrZr) {
		this.wayBillMgrZr = wayBillMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,	ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
			DBRow updateRow =  new DBRow();
			updateRow.add("out_id", StringUtil.getString(request,"out_id"));
			updateRow.add("state",StringUtil.getString(request,"state"));
			wayBillMgrZr.updateOutBoundByRow(updateRow);
			throw new JsonException("success");
		
	}	
}
