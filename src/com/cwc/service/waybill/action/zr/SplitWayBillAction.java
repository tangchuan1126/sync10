package com.cwc.service.waybill.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class SplitWayBillAction extends  ActionFatherController{

	private WayBillMgrIfaceZR wayBillMgrZr;
	
	 
	public void setWayBillMgrZr(WayBillMgrIfaceZR wayBillMgrZr) {
		this.wayBillMgrZr = wayBillMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
			String ids = StringUtil.getString(request,"ids");
			String numbers = StringUtil.getString(request,"numbers");
			 
			long wayId = Long.parseLong(StringUtil.getString(request,"way_bill_id"));
			DBRow result = wayBillMgrZr.splitWayBill(ids, numbers, wayId);
			result.add("flag", "success");
			throw new JsonException(new JsonObject(result));	
		
	}

}
