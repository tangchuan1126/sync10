package com.cwc.service.waybill.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class DeleteSplitWayBillAction extends ActionFatherController{

	private WayBillMgrIfaceZR wayBillMgrZr;
	
	 
	public void setWayBillMgrZr(WayBillMgrIfaceZR wayBillMgrZr) {
		this.wayBillMgrZr = wayBillMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
	 
			long wayBillId = Long.parseLong(StringUtil.getString(request, "way_bill_id"));
			long parentBillId = Long.parseLong(StringUtil.getString(request,"parent_bill_id"));
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			wayBillMgrZr.deleteSplitWayBillBy(wayBillId,parentBillId,adminLoggerBean);
			throw new JsonException("success");
		
		
	}

}
