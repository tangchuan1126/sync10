package com.cwc.service.waybill.action.zj;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.fedex.FedexIEClient;
import com.cwc.fedex.FedexClient;
import com.cwc.fedex.FedexIEGZClient;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.usps.UspsResponseErrorException;
import com.cwc.util.StringUtil;

public class FEDEXIEGZInternalAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private ProductMgrIFace productMgr;
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	private TransportMgrIFaceZJ transportMgrZJ;
	private CatalogMgrIFace catalogMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		response.setHeader("Pragma","No-cache");   
		response.setHeader("Cache-Control","no-cache");   
		response.setDateHeader("Expires", 0); 
		
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long id = StringUtil.getLong(request,"id");
		String type = StringUtil.getString(request,"type");
		
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		float weight = StringUtil.getFloat(request,"weight");
		int pkcount = StringUtil.getInt(request,"pkcount",1);//一票几件
		
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");
		
		DBRow deliveryCountry = orderMgr.getDetailCountryCodeByCcid(ccid);		//目的国家
		DBRow deliveryProvince = productMgr.getDetailProvinceByProId(pro_id);	//目的省份

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人
		
		
		String send_name = StringUtil.getString(request,"send_name");
		String send_zip_code = StringUtil.getString(request,"send_zip_code");
		String send_address1 = StringUtil.getString(request,"send_address1");
		String send_address2 = StringUtil.getString(request,"send_address2");
		String send_address3 = StringUtil.getString(request,"send_address3");
		String send_linkman_phone = StringUtil.getString(request,"send_linkman_phone");
		String send_city = StringUtil.getString(request,"send_city");
		String deliver_name = StringUtil.getString(request,"deliver_name");
		String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
		String deliver_address1 = StringUtil.getString(request,"deliver_address1");
		String deliver_address2 = StringUtil.getString(request,"deliver_address2");
		String deliver_address3 = StringUtil.getString(request,"deliver_address3");
		String deliver_linkman_phone = StringUtil.getString(request,"deliver_linkman_phone");
		String deliver_city = StringUtil.getString(request,"deliver_city");
		
		String english_note = StringUtil.getString(request,"english_note");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		
		DBRow detailInvoice = new DBRow();
		
		//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		
		
		
		DBRow[] deliveryProducts = null;
		
		float quantity = 0; 
		double allPrice = 0;
		float allWeight = 0;
		
		ArrayList<DBRow> productList = new ArrayList<DBRow>();
		
		
		if(type.toUpperCase().equals("D"))
		{
			deliveryProducts = deliveryMgrZJ.getDeliveryOrderDetailsById(id);
			
			for (int i = 0; i < deliveryProducts.length; i++) 
			{
				DBRow product = productMgr.getDetailProductByPcid(deliveryProducts[i].get("product_id",0l));
				product.add("cart_quantity",deliveryProducts[i].get("delivery_count",0f));
				
				productList.add(product);
				
				allPrice += deliveryProducts[i].get("delivery_count",0f)*product.get("unit_price",0d);
				allWeight += deliveryProducts[i].get("delivery_count",0f)*product.get("weight",0d);
				
				quantity += deliveryProducts[i].get("delivery_count",0f);
			}
		}
		else if(type.toUpperCase().equals("T"))
		{
			deliveryProducts = transportMgrZJ.getTransportDetailByTransportId(id, null, null, null, null);
			
			for (int i = 0; i < deliveryProducts.length; i++) 
			{
				DBRow product = productMgr.getDetailProductByPcid(deliveryProducts[i].get("transport_pc_id",0l));
				product.add("cart_quantity",deliveryProducts[i].get("transport_send_count",0f));
				
				productList.add(product);
				
				allPrice += deliveryProducts[i].get("transport_send_count",0f)*product.get("unit_price",0d);
				allWeight += deliveryProducts[i].get("transport_send_count",0f)*product.get("weight",0d);
				
				quantity += deliveryProducts[i].get("transport_send_count",0f);
			}
		}
		
		double dutiableDeclared = 0;//申报价值
		if(inv_tv.trim().equals(""))
		{
			dutiableDeclared = Double.parseDouble(inv_uv)*quantity;
		}
		else
		{
			dutiableDeclared = Double.parseDouble(inv_tv);
		}		
		
		String wayBillNo = "";
		String destination = "";
		boolean uspsResponseError = false;
		String uspsResponseErrorDescription = "";
		
		FedexIEGZClient fedexIEGZClient = new FedexIEGZClient();
		
		String toTel = "0";
		if ( deliver_linkman_phone.equals("")==false )
		{
		 toTel = deliver_linkman_phone;
		}
		
		fedexIEGZClient.setFromName(send_name);
		fedexIEGZClient.setFromAddress(send_address1+send_address2+send_address3);
		fedexIEGZClient.setFromCity(send_city);
		
		DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(ps_id);
		
		DBRow sendCountry = orderMgr.getDetailCountryCodeByCcid(storageCatalog.get("native",0l));		//目的国家
		DBRow sendProvince = productMgr.getDetailProvinceByProId(storageCatalog.get("pro_id",0l));	//目的省份
		
		fedexIEGZClient.setFromState(sendProvince.getString("p_code"));
		fedexIEGZClient.setFromZip(send_zip_code);
		fedexIEGZClient.setFromTel(send_linkman_phone);
		fedexIEGZClient.setFromCountry(sendCountry.getString("c_code"));
		
		
		fedexIEGZClient.setToName(deliver_name);
		fedexIEGZClient.setToAddress(deliver_address1+deliver_address2+deliver_address3);
		fedexIEGZClient.setToCity(deliver_city);
		fedexIEGZClient.setToCountry(deliveryCountry.getString("c_code"));
		fedexIEGZClient.setCustomsValue(dutiableDeclared);
		
		
		fedexIEGZClient.setDescription(inv_dog);
		fedexIEGZClient.setCurrency("USD");
		fedexIEGZClient.setCountryOfManufacture("CN");
		fedexIEGZClient.setInvoiceProducts(productList.toArray(new DBRow[0]),dutiableDeclared,allPrice);
		fedexIEGZClient.setHscode(hs_code);
		fedexIEGZClient.setDutiesPayment(dtp);
		
		float privilege = weight/allWeight;
		fedexIEGZClient.setPrivilege(privilege);
		
		fedexIEGZClient.setToState(deliveryProvince.getString("p_code"));
		fedexIEGZClient.setToZip(deliver_zip_code);
		fedexIEGZClient.setToTel(toTel);
		fedexIEGZClient.setWeightLB(MoneyUtil.round(weight,2));
		
		fedexIEGZClient.setOids(type+id);
		fedexIEGZClient.setNote(english_note);
		fedexIEGZClient.setCreater(adminLoggerBean.getAccount());

		try
		{
//			fedexClient.commit();
//			fedexClient.commitMPS();
			fedexIEGZClient.commitMPS2(pkcount);
		}
		catch (UspsResponseErrorException e)//地址错误
		{
			uspsResponseError = true;
			uspsResponseErrorDescription = e.getMessage();
		}


		wayBillNo = fedexIEGZClient.getWaybillNo();
		
		
		
		
		if(uspsResponseError!=true)
		{
			if(type.toUpperCase().equals("D"))
			{
				deliveryMgrZJ.printDeliveryWayBill(wayBillNo,ccid,pro_id,sc_id,id,send_name, send_zip_code, send_address1,send_address2, send_address3, send_linkman_phone, send_city, deliver_name, deliver_zip_code, deliver_address1, deliver_address2, deliver_address3, deliver_linkman_phone, deliver_city,weight,"FEDEX",hs_code,pkcount,adminLoggerBean);
			}
			else if(type.toUpperCase().equals("T"))
			{
				transportMgrZJ.printTransportWayBill(wayBillNo, ccid, pro_id, sc_id,id, send_name, send_zip_code, send_address1, send_address2, send_address3, send_linkman_phone, send_city, deliver_name, deliver_zip_code, deliver_address1, deliver_address2, deliver_address3, deliver_linkman_phone, deliver_city,weight,"FEDEX",hs_code,pkcount,adminLoggerBean);
			}
			
			throw new WriteOutResponseException("close");
		}
		else
		{
			throw new WriteOutResponseException(uspsResponseErrorDescription);
		}
		
		
		
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
}
