package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.WaybillLogMgrIFaceZJ;
import com.cwc.app.jms.action.TrackingWaybillActionJMS;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class TrackingWaybillJMSAction extends ActionFatherController {

	static Logger log = Logger.getLogger("ACTION");
	private WaybillLogMgrIFaceZJ waybillLogMgrZJ;
	private SystemConfigIFace systemConfig;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		int before = StringUtil.getInt(request,"before",-1);
		
		if(before==-1)
		{
			before = systemConfig.getIntConfigValue("listorder_date_interval");
		}
		TDate tDate = new TDate();
		
		tDate.addDay(-before);
		
		String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
		
		
		DBRow[] waybillLogs = waybillLogMgrZJ.needTrackingWayBillLog(st, en);
		
		for (int  i= 0;  i< waybillLogs.length; i++) 
		{
			com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsTrackingMgr");
			jmsMgr.excuteAction(new TrackingWaybillActionJMS(waybillLogs[i].get("waybill_log_id",0l)));
		}
		
		throw new RedirectRefException();
	}

	public void setWaybillLogMgrZJ(WaybillLogMgrIFaceZJ waybillLogMgrZJ) {
		this.waybillLogMgrZJ = waybillLogMgrZJ;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

}
