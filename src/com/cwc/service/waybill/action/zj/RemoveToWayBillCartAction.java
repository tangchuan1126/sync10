package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class RemoveToWayBillCartAction extends ActionFatherController {

	private CartWaybillIFace cartWaybill;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		try
		{
			cartWaybill.removeProduct(request);
		}
		catch (Exception e)
		{
			throw new WriteOutResponseException(e.getMessage());
		}
		
		throw new WriteOutResponseException("ok");
	}
	public void setCartWaybill(CartWaybillIFace cartWaybill) {
		this.cartWaybill = cartWaybill;
	}

}
