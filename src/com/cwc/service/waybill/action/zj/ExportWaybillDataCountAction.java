package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ExportWaybillDataCountAction extends ActionFatherController{
	
	 private WayBillMgrIFaceZJ wayBillMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
     {

			try
			{
				String path =wayBillMgrZJ.exportWayBillCount(request);
				throw new WriteOutResponseException(path);
			}
			catch (WareHouseErrorException e)
			{
				throw new WriteOutResponseException("WareHouseErrorException");
			}

     }

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

	
}
