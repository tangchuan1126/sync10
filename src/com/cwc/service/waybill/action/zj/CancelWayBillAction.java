package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.waybill.WayBillOrderPrintedException;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CancelWayBillAction extends ActionFatherController {

	private WayBillMgrIFaceZJ wayBillMgrZJ;
	private OrderMgrIFace orderMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, DoNothingException, Exception 
	{
		DBRow dbrow = new DBRow();
		dbrow.add("result","ok");
		
		try 
		{
			wayBillMgrZJ.cancelWayBillOrder(request);
		}
		catch (WayBillOrderPrintedException e) 
		{
			dbrow.add("result","WayBillOrderPrintedException");
		}
		
		long oid = StringUtil.getLong(request,"oid");
		orderMgr.initCartFromOrder(request,oid);
		
		throw new JsonException(new JsonObject(dbrow));
		
	}
	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

}
