package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class SendWaybillDownLoad extends ActionFatherController {

	private WayBillMgrIFaceZJ wayBillMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		String path = wayBillMgrZJ.waybillSendDownload(request);
		
		String basePath = "";
		DBRow db = new DBRow();
		
		if (!path.equals("0")) 
		{
			db.add("canexport",true);
			basePath = "../../"+ path;
		}
		else
		{
			db.add("canexport",false);
		}
		
		
		db.add("fileurl",basePath);
		
		throw new JsonException(new JsonObject(db));
	}

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

}
