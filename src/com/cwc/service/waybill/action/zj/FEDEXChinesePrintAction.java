package com.cwc.service.waybill.action.zj;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.WayBillFromKey;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.ConnectErrorJmsException;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.fedex.FedexIEClient;
import com.cwc.fedex.FedexClient;
import com.cwc.json.JsonObject;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.usps.UspsResponseErrorException;
import com.cwc.util.StringUtil;

public class FEDEXChinesePrintAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private CartWaybillIFace cartWaybill;
	private ExpressMgrIFace expressMgr;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	private ProductMgrIFace productMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		DBRow returnResult = new DBRow();//给页面的返回值，原先的msg放入dbrow里，字段为msg，传递运单号回前台
		
		response.setHeader("Pragma","No-cache");   
		response.setHeader("Cache-Control","no-cache");   
		response.setDateHeader("Expires", 0); 
		
		HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();
		
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		String delivery_note = StringUtil.getString(request,"delivery_note");
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		
		long oid = StringUtil.getLong(request,"oid");
		long old_waybill_id = StringUtil.getLong(request,"old_waybill_id");
		
		long handle = StringUtil.getLong(request,"handle");
		long handle_status = StringUtil.getLong(request,"handle_status");
		long invoice_id = StringUtil.getLong(request,"invoice_id");
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		
		String inv_rfe_chinese = StringUtil.getString(request,"inv_rfe_chinese");
		String inv_dog_chinese = StringUtil.getString(request,"inv_dog_chinese");
		String material_chinese = StringUtil.getString(request,"material_chinese");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");//材质
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人

		int waybill_from_type = StringUtil.getInt(request,"waybill_from_type");
		int pkcount = StringUtil.getInt(request,"pkcount",1);
		
		String english_note = StringUtil.getString(request,"english_note");
		
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		DBRow order = null;
		if(oid!=0&&old_waybill_id==0)
		{
			order = orderMgr.getDetailPOrderByOid(oid);
		}
		
		if(oid==0&&old_waybill_id>0)
		{
			order = wayBillMgrZJ.getDetailInfoWayBillById(old_waybill_id);
		}
		
		DBRow detailInvoice = new DBRow();
		
		//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		
		
		
		cartWaybill.flush(StringUtil.getSession(request));
		
		float all_weight = cartWaybill.getCartWeight(StringUtil.getSession(request));
		
		float print_weight = orderMgr.calculateWayBillWeight(sc_id, all_weight);
		
		DBRow[] cartWaybill_products = cartWaybill.getDetailProduct();
		
		
		
//		float count = 0;
//		
		float quantity = 0; 
		double allPrice = 0;
		
		String oids = "";
		
		Map<String,String> oidMaps = new HashMap<String,String>();
		
		for (int i = 0; i < cartWaybill_products.length; i++)
		{
			try 
			{
				DBRow orderItem = orderMgr.getDetailOrderItemByIid(cartWaybill_products[i].get("order_item_id",0l));
				
				if(orderItem.get("wait_quantity",0f)<cartWaybill_products[i].get("cart_quantity",0f)&&cartWaybill_products[i].get("wait_quantity",0f)==0&&waybill_from_type==WayBillFromKey.Record)
				{
					throw new OrderItemWaitErrorException();
				}
				
				oidMaps.put(orderItem.getString("oid"),orderItem.getString("oid"));
			} 
			catch (Exception e) 
			{
				returnResult.add("msg","订单待发货数异常，请关闭抄单页面后重新抄单");
				throw new JsonException(new JsonObject(returnResult));
			}
			
			allPrice +=  cartWaybill_products[i].get("cart_quantity",0f)*cartWaybill_products[i].get("unit_price",0d);
				
			quantity += cartWaybill_products[i].get("cart_quantity",0f);
		}
		
		Set<String> oidList = oidMaps.keySet();
		for (String oidString:oidList) 
		{
			oids += oidString+",";
		}
		
		
		double dutiableDeclared = 0;//申报价值
		if(inv_tv.trim().equals(""))
		{
			dutiableDeclared = Double.parseDouble(inv_uv)*quantity;
		}
		else
		{
			dutiableDeclared = Double.parseDouble(inv_tv);
		}
		
//		
//		int NumberOfPieces = (int)((count-1.0)/10.0)+1;
//		ShippingInfoBean shippingInfoBean = expressMgr.getNewShippingFee(sc_id,ccid,pro_id,cartWaybill_products);
		
		DBRow detailDelivererInfo;
		if (detailInvoice.get("di_id",0l)==0)
		{
			detailDelivererInfo = orderMgr.getRandomDelivererInfo(ps_id);
			
			inv_di_id = detailDelivererInfo.get("di_id",0l);
		}
		else
		{
			detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
		}
		
		String wayBillNo = "";
		String destination = "";
		double shippment_rate;
		boolean uspsResponseError = false;
		String uspsResponseErrorDescription = "";
		
		FedexIEClient fedexClient = new FedexIEClient();
		
		String toTel = "0";
		if ( order.getString("tel").equals("")==false )
		{
		 toTel = order.getString("tel");
		}
		
		fedexClient.setFromName(detailDelivererInfo.getString("CompanyName"));
		fedexClient.setFromAddress(detailDelivererInfo.getString("AddressLine1")+detailDelivererInfo.getString("AddressLine2")+detailDelivererInfo.getString("AddressLine3"));
		fedexClient.setFromCity(detailDelivererInfo.getString("City"));
		fedexClient.setFromState(detailDelivererInfo.getString("DivisionCode"));
		fedexClient.setFromZip(detailDelivererInfo.getString("PostalCode"));
		fedexClient.setFromTel(detailDelivererInfo.getString("PhoneNumber"));
		fedexClient.setFromCountry(detailDelivererInfo.getString("CountryCode"));
		
		
		fedexClient.setToName(order.getString("address_name"));
		fedexClient.setToAddress(order.getString("address_street"));
		fedexClient.setToCity(order.getString("address_city"));
		fedexClient.setToCountry((String)countryCodeHM.get(order.getString("address_country").toLowerCase()));
		fedexClient.setCustomsValue(dutiableDeclared);
		
		fedexClient.setDescription(inv_dog);
		fedexClient.setCurrency("USD");
		fedexClient.setCountryOfManufacture("CN");
		fedexClient.setInvoiceProducts(cartWaybill_products,dutiableDeclared,allPrice);
		fedexClient.setHscode(hs_code);
		fedexClient.setDutiesPayment(dtp);
		
		float privilege = expressMgr.getDetailCompany(sc_id).get("print_weight_discount", 0f);
		fedexClient.setPrivilege(privilege);
		
		String address_state = order.getString("address_state");
		if(pro_id!=-1&&pro_id!=0)
		{
			DBRow province = productMgr.getDetailProvinceByProId(pro_id);
			
			
			address_state = province.getString("p_code");
		}
		
		fedexClient.setToState(address_state);
		fedexClient.setToZip(order.getString("address_zip"));
		fedexClient.setToTel(toTel);
		fedexClient.setWeightLB(MoneyUtil.round(print_weight,2));
		fedexClient.setNote(english_note);
		fedexClient.setOids(oids);
		fedexClient.setCreater(adminLoggerBean.getAccount());

		try
		{
//			fedexClient.commit();
//			fedexClient.commitMPS();
			fedexClient.commitMPS2(pkcount);
		}
		catch (UspsResponseErrorException e)//地址错误
		{
			uspsResponseError = true;
			uspsResponseErrorDescription = e.getMessage();
		}
		catch (Exception e)
		{
			if(e.toString().contains("java.net.")||e.toString().contains("timeout"))
			{
				uspsResponseError = true;
				uspsResponseErrorDescription = "网络不给力，稍后重试";
			}
		}


		wayBillNo = fedexClient.getWaybillNo();
		shippment_rate = fedexClient.getShippmentRate();
		
		
		
		if(uspsResponseError!=true)
		{
			
			
			
			long waybill_id = wayBillMgrZJ.addWayBillOrderSub(order.getString("address_name"),order.getString("address_country"),order.getString("address_city"),order.getString("address_zip"),order.getString("address_street"),order.getString("address_state"),ccid,pro_id,order.getString("client_id"),order.getString("tel"),sc_id, wayBillNo,shipping_cost,ps_id, destination,StringUtil.getSession(request),inv_di_id,inv_uv,inv_rfe,inv_dog,inv_tv,mail_piece_shape,delivery_note,adminLoggerBean,waybill_from_type,hs_code,cartWaybill_products,pkcount,material,dtp,inv_rfe_chinese,inv_dog_chinese,material_chinese,shippment_rate);
			
			//上传TrackingNumber给Ebay
			wayBillMgrZJ.upLoadTrackingNumberToEbay(waybill_id);
			
			DBRow[] orders = wayBillMgrZJ.returnOrdersByWayBillId(waybill_id);
			for (int i = 0; i < orders.length; i++)//检查这张运单的所有订单是否都上运单了
			{
				orderMgr.changeOrderHandle(orders[i].get("oid",0l));
			}
			
			orderMgr.initCartFromOrder(request,oid);
				

			cartWaybill.clearCart(StringUtil.getSession(request));
				
			DBRow waybill = wayBillMgrZJ.getWayBillOrderByWayBillId(waybill_id);
			
			if(waybill.get("product_status",0)==ProductStatusKey.IN_STORE)
			{
				
				DBRow[] wait_order_items = orderMgr.waitQuantityNotZero(oid);
				if(wait_order_items==null||wait_order_items.length==0)
				{
					returnResult.add("msg","close");
				}
				else
				{
					returnResult.add("msg","ok");
				}
			}
			else
			{
				returnResult.add("waybill_id",waybill_id);
				returnResult.add("msg","stockout");
			}
			
			throw new JsonException(new JsonObject(returnResult));
		}
		else
		{
			returnResult.add("msg",uspsResponseErrorDescription);
			throw new JsonException(new JsonObject(returnResult));
		}
		
		
		
	}
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	public void setCartWaybill(CartWaybillIFace cartWaybill) {
		this.cartWaybill = cartWaybill;
	}
	public void setExpressMgr(ExpressMgrIFace expressMgr) {
		this.expressMgr = expressMgr;
	}
	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
