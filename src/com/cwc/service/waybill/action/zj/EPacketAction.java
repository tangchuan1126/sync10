package com.cwc.service.waybill.action.zj;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.dhl.BarcodeDecoder;
import com.cwc.app.dhl.DHLClient;
import com.cwc.app.epacket.EPacketClient;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.key.WayBillFromKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class EPacketAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private CartWaybillIFace cartWaybill;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	private ProductMgrIFace productMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		response.setHeader("Pragma","No-cache");   
		response.setHeader("Cache-Control","no-cache");   
		response.setDateHeader("Expires", 0); 
		
		 
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		String delivery_note = StringUtil.getString(request,"delivery_note");
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		
		long oid = StringUtil.getLong(request,"oid");
		long old_waybill_id = StringUtil.getLong(request,"old_waybill_id");
		
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		
		String inv_rfe_chinese = StringUtil.getString(request,"inv_rfe_chinese");
		String inv_dog_chinese = StringUtil.getString(request,"inv_dog_chinese");
		String material_chinese = StringUtil.getString(request,"material_chinese");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人
		
		int waybill_from_type = StringUtil.getInt(request,"waybill_from_type");
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		DBRow order = null; 
		if(oid!=0&&old_waybill_id==0)
		{
			order = orderMgr.getDetailPOrderByOid(oid);
		}
		
		if(oid==0&&old_waybill_id>0)
		{
			order = wayBillMgrZJ.getDetailInfoWayBillById(old_waybill_id);
		}
		DBRow detailInvoice = new DBRow();
		
		//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		

		cartWaybill.flush(StringUtil.getSession(request));
		
		DBRow[] cartWaybill_products = cartWaybill.getDetailProduct();
		
		
		
		float quantity = 0; 
		for (int i = 0; i < cartWaybill_products.length; i++) 
		{
			try 
			{
				DBRow orderItem = orderMgr.getDetailOrderItemByIid(cartWaybill_products[i].get("order_item_id",0l));
				
				if(orderItem.get("wait_quantity",0f)<cartWaybill_products[i].get("cart_quantity",0f)&&cartWaybill_products[i].get("wait_quantity",0f)==0&&waybill_from_type==WayBillFromKey.Record)
				{
					throw new OrderItemWaitErrorException();
				}
			} 
			catch (Exception e) 
			{
				throw new WriteOutResponseException("订单待发货数异常，请关闭抄单页面后重新抄单");
			}
			
			quantity += cartWaybill_products[i].get("cart_quantity",0f);
		}
		
		double dutiableDeclared = 0;//申报价值
		if(inv_tv.trim().equals(""))
		{
			dutiableDeclared = Double.parseDouble(inv_uv)*quantity;
		}
		else
		{
			dutiableDeclared = Double.parseDouble(inv_tv)/quantity;
		}
		
		DBRow detailDelivererInfo;
		if (detailInvoice.get("di_id",0l)==0)
		{
			detailDelivererInfo = orderMgr.getRandomDelivererInfoByPsId(ps_id);
		}
		else
		{
			detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
		}
		
		String destination = "";
		
		
		
			//提交DHL运单信息
			String ConsigneeContactPhoneNumber;
			
			if (order.getString("tel").equals(""))
			{
				ConsigneeContactPhoneNumber = "";
			}
			else
			{
				ConsigneeContactPhoneNumber = order.getString("tel");
			}
			

			////system.out.println("---------------"+countryCodeHM.get(order.getString("address_country").toLowerCase()));
			
			EPacketClient ePacketClient = new EPacketClient();
			
			ePacketClient.setStartdate(DateUtil.NowStrEPacket());
			ePacketClient.setEnddate(DateUtil.NowStrEPacket());
			ePacketClient.setVolweight(0);
			ePacketClient.setOrderid(oid);
					
			ePacketClient.setReceiverName(order.getString("address_name"));
			ePacketClient.setReceiverStreet(order.getString("address_street"));
			ePacketClient.setReceiverCity(order.getString("address_city"));
			ePacketClient.setReceiverPostCode(order.getString("address_zip"));
			ePacketClient.setReceiverPhone(ConsigneeContactPhoneNumber);
			
			DBRow province = productMgr.getDetailProvinceByProId(pro_id);
			
			ePacketClient.setReceiverProvince(province.getString("p_code"));

			
			//设置发件人信息
			ePacketClient.setSendName("Visionari");
			ePacketClient.setSendPostCode(detailDelivererInfo.getString("PostalCode"));
			ePacketClient.setSendPhone(detailDelivererInfo.getString("PhoneNumber"));
			
			if(ps_id == 100000)
			{
				ePacketClient.setSendProvince("110000");
				ePacketClient.setSendCity("110100");
				ePacketClient.setSendCounty("111149");
			}
			else if(ps_id == 100006)
			{
				ePacketClient.setSendProvince("440000");
				ePacketClient.setSendCity("440100");
				ePacketClient.setSendCounty("440111");
			}
			
			ePacketClient.setSendStreet(detailDelivererInfo.getString("AddressLine1"));
			ePacketClient.setSendCompany(detailDelivererInfo.getString("CompanyName"));
			
			String trackingNumber = ePacketClient.comit(cartWaybill_products,dutiableDeclared);
			
			if(!trackingNumber.equals("0"))
			{
				AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				
				long waybill_id = wayBillMgrZJ.addWayBillOrderSub(order.getString("address_name"),order.getString("address_country"),order.getString("address_city"),order.getString("address_zip"),order.getString("address_street"),order.getString("address_state"),ccid,pro_id,order.getString("client_id"),order.getString("tel"),sc_id, trackingNumber,shipping_cost,ps_id, destination,StringUtil.getSession(request),inv_di_id,inv_uv,inv_rfe,inv_dog,inv_tv,mail_piece_shape,delivery_note,adminLoggerBean,waybill_from_type,hs_code,cartWaybill_products,1,material,dtp,inv_rfe_chinese,inv_dog_chinese,material_chinese,0);
				//上传TrackingNumber给Ebay
				wayBillMgrZJ.upLoadTrackingNumberToEbay(waybill_id);
				
				DBRow[] orders = wayBillMgrZJ.returnOrdersByWayBillId(waybill_id);
				for (int i = 0; i < orders.length; i++)//检查这张运单的所有订单是否都上运单了
				{
					orderMgr.changeOrderHandle(orders[i].get("oid",0l));
				}
				
				orderMgr.initCartFromOrder(request,oid);
					

				DBRow[] wait_order_items = orderMgr.waitQuantityNotZero(oid);
				if(wait_order_items==null||wait_order_items.length==0)
				{
					throw new WriteOutResponseException("close");
				}
				
				cartWaybill.clearCart(StringUtil.getSession(request));
				
				throw new WriteOutResponseException("ok");
			}
			else
			{
				throw new WriteOutResponseException(ePacketClient.getErrorMessage());
			}
			
		
	}
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	public void setCartWaybill(CartWaybillIFace cartWaybill) {
		this.cartWaybill = cartWaybill;
	}
	
	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
