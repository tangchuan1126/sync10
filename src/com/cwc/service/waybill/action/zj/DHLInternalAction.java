package com.cwc.service.waybill.action.zj;

import java.net.SocketException;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.dhl.BarcodeDecoder;
import com.cwc.app.dhl.DHLClient;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class DHLInternalAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private ProductMgrIFace productMgr;
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	private TransportMgrIFaceZJ transportMgrZJ;
	private CatalogMgrIFace catalogMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		response.setHeader("Pragma","No-cache");   
		response.setHeader("Cache-Control","no-cache");   
		response.setDateHeader("Expires", 0); 
		
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long id = StringUtil.getLong(request,"id");
		String type = StringUtil.getString(request,"type");
		
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		float weight = StringUtil.getFloat(request,"weight");
		
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");
		
		DBRow deliveryCountry = orderMgr.getDetailCountryCodeByCcid(ccid);		//目的国家
		DBRow deliveryProvince = productMgr.getDetailProvinceByProId(pro_id);	//目的省份

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人
		
		
		String send_name = StringUtil.getString(request,"send_name");
		String send_zip_code = StringUtil.getString(request,"send_zip_code");
		String send_address1 = StringUtil.getString(request,"send_address1");
		String send_address2 = StringUtil.getString(request,"send_address2");
		String send_address3 = StringUtil.getString(request,"send_address3");
		String send_linkman_phone = StringUtil.getString(request,"send_linkman_phone");
		String send_city = StringUtil.getString(request,"send_city");
		String deliver_name = StringUtil.getString(request,"deliver_name");
		String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
		String deliver_address1 = StringUtil.getString(request,"deliver_address1");
		String deliver_address2 = StringUtil.getString(request,"deliver_address2");
		String deliver_address3 = StringUtil.getString(request,"deliver_address3");
		String deliver_linkman_phone = StringUtil.getString(request,"deliver_linkman_phone");
		String deliver_city = StringUtil.getString(request,"deliver_city");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		
		DBRow detailInvoice = new DBRow();
		
		//发票信息由页面传递
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		

		
		DBRow[] products = null;
		
		float quantity = 0; 
		
		if(type.toUpperCase().equals("D"))
		{
			products = deliveryMgrZJ.getDeliveryOrderDetailsById(id);
			
			for (int i = 0; i < products.length; i++) 
			{
				quantity += products[i].get("delivery_count",0f);
			}
		}
		else if(type.toUpperCase().equals("T"))
		{
			products = transportMgrZJ.getTransportDetailByTransportId(id, null, null, null, null);
			
			for (int i = 0; i < products.length; i++) 
			{
				quantity += products[i].get("transport_send_count",0f);
			}
		}
		
		
		
		
		
		
		double dutiableDeclared = 0;//申报价值
		if(inv_tv.trim().equals(""))
		{
			dutiableDeclared = Double.parseDouble(inv_uv)*quantity;
		}
		else
		{
			dutiableDeclared = Double.parseDouble(inv_tv);
		}
		
		String airWayBillNumber = "";
		String destination = "";
		
		String errorMsg = "";
		try 
		{
			//提交DHL运单信息
			String ConsigneeContactPhoneNumber;
			
			if (deliver_linkman_phone.equals(""))
			{
				ConsigneeContactPhoneNumber = "Not Provided";
			}
			else
			{
				ConsigneeContactPhoneNumber = deliver_linkman_phone;
			}
			

			////system.out.println("---------------"+countryCodeHM.get(order.getString("address_country").toLowerCase()));
			
			DHLClient dhlClient = new DHLClient();
					
			dhlClient.setConsigneeCompanyName(deliver_name);
			dhlClient.setConsigneeAddressLine(deliver_address1+deliver_address2+deliver_address3);
			dhlClient.setConsigneeCity(deliver_city);
			dhlClient.setConsigneeDivisionCode(deliveryProvince.getString("p_code"));
			dhlClient.setConsigneePostalCode(deliver_zip_code);
			dhlClient.setConsigneeCountryCode(deliveryCountry.getString("c_code"));

			
			
			dhlClient.setConsigneeCountryName(deliveryCountry.getString("c_country"));
			dhlClient.setConsigneeContactPersonName(deliver_name);
			dhlClient.setConsigneeContactPhoneNumber( ConsigneeContactPhoneNumber);
			dhlClient.setConsigneeContactEmailFrom("services@vvme.com");
			dhlClient.setConsigneeContactEmailTo("services@vvme.com");
			dhlClient.setDutiableDeclaredValue(StringUtil.formatNumber("0.00",dutiableDeclared));
			dhlClient.setShipmentDetailsNumberOfPieces("1");//String.valueOf(NumberOfPieces)
			dhlClient.setShipmentDetailsWeight(0.5f);
			
			//设置发件人信息
			
			DBRow storageCatalog = catalogMgr.getDetailProductStorageCatalogById(ps_id);
			
			DBRow sendCountry = orderMgr.getDetailCountryCodeByCcid(storageCatalog.get("native",0l));		//目的国家
			DBRow sendProvince = productMgr.getDetailProvinceByProId(storageCatalog.get("pro_id",0l));	//目的省份
			
			dhlClient.setDev_CompanyName(send_name);
			dhlClient.setDev_City(send_city);
			dhlClient.setDev_DivisionCode(sendProvince.getString("p_code"));
			dhlClient.setDev_PostalCode(send_zip_code);
			dhlClient.setDev_CountryCode(sendCountry.getString("c_code"));
			dhlClient.setDev_CountryName(sendCountry.getString("c_country"));
			
			dhlClient.setDev_AddressLine1(send_address1);
			dhlClient.setDev_AddressLine2(send_address2);
			dhlClient.setDev_AddressLine3(send_address3);
			dhlClient.setDev_PhoneNumber(send_linkman_phone);
			
			//设置关税支付方
			dhlClient.setDutyPaymentType(dtp);

			//dhlClient.setShipmentDetailsWeight(1.5f*order.get("quantity",0f));
			
			//weight 1.5 8 n
			
			dhlClient.setOid(String.valueOf(id));
			
			airWayBillNumber = dhlClient.commit();
			destination = dhlClient.getDestination();
			
			if(airWayBillNumber.trim().equals(""))
			{
				throw new Exception();
			}
			
			errorMsg = dhlClient.getErrorPage();
			
		}
		catch(SocketException e)
		{
			throw new WriteOutResponseException("错误信息：链接失败");
		}
		catch (Exception e) 
		{
			if(e.getMessage().indexOf("Server returned HTTP response code: 502 for URL: http://xmlpi.dhl-usa.com/XMLShippingServlet")>=0)
			{
				throw new WriteOutResponseException("错误信息："+e.getMessage()+"<br/>DHL服务器异常，请与DHL联系！");
			}
		}
		
		if (airWayBillNumber.equals("0000000000"))
		{
			throw new WriteOutResponseException("../TransformXMLtoHTML/ResponseXMLS/"+errorMsg);
		}
		else
		{				
			new BarcodeDecoder(airWayBillNumber);
			
			if(type.toUpperCase().equals("D"))
			{
				deliveryMgrZJ.printDeliveryWayBill(airWayBillNumber,ccid,pro_id,sc_id,id,send_name, send_zip_code, send_address1,send_address2, send_address3, send_linkman_phone, send_city, deliver_name, deliver_zip_code, deliver_address1, deliver_address2, deliver_address3, deliver_linkman_phone, deliver_city,weight,"DHL",hs_code,1,adminLoggerBean);
			}
			else if(type.toUpperCase().equals("T"))
			{
				transportMgrZJ.printTransportWayBill(airWayBillNumber, ccid, pro_id, sc_id, id, send_name, send_zip_code, send_address1, send_address2, send_address3, send_linkman_phone, send_city, deliver_name, deliver_zip_code, deliver_address1, deliver_address2, deliver_address3, deliver_linkman_phone, deliver_city, weight, "DHL", hs_code,1,adminLoggerBean);
			}
			
			throw new WriteOutResponseException("close");
		}
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}
}
