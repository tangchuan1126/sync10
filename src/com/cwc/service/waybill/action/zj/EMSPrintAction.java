package com.cwc.service.waybill.action.zj;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.WayBillFromKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class EMSPrintAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private CartWaybillIFace cartWaybill;
	private ExpressMgrIFace expressMgr;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		DBRow returnResult = new DBRow();//给页面的返回值，原先的msg放入dbrow里，字段为msg，传递运单号回前台
		
		HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();
		 
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		String delivery_note = StringUtil.getString(request,"delivery_note");
		
		long oid = StringUtil.getLong(request,"oid");
		long old_waybill_id = StringUtil.getLong(request,"old_waybill_id");
		
		
		long handle = StringUtil.getLong(request,"handle");
		long handle_status = StringUtil.getLong(request,"handle_status");
		long invoice_id = StringUtil.getLong(request,"invoice_id");
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		
		String inv_rfe_chinese = StringUtil.getString(request,"inv_rfe_chinese");
		String inv_dog_chinese = StringUtil.getString(request,"inv_dog_chinese");
		String material_chinese = StringUtil.getString(request,"material_chinese");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人
		
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		
		int waybill_from_type = StringUtil.getInt(request,"waybill_from_type");
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		DBRow order = null;
		if(oid!=0&&old_waybill_id==0)
		{
			order = orderMgr.getDetailPOrderByOid(oid);
		}
		
		if(oid==0&&old_waybill_id>0)
		{
			order = wayBillMgrZJ.getDetailInfoWayBillById(old_waybill_id);
		}
		
		DBRow detailInvoice = new DBRow();
		
		//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		

		cartWaybill.flush(StringUtil.getSession(request));
		
		DBRow[] cartWaybill_products = cartWaybill.getDetailProduct();
		
//		float count = 0;
//		
		for (int i = 0; i < cartWaybill_products.length; i++)
		{
			try 
			{
				DBRow orderItem = orderMgr.getDetailOrderItemByIid(cartWaybill_products[i].get("order_item_id",0l));
				
				if(orderItem.get("wait_quantity",0f)<cartWaybill_products[i].get("cart_quantity",0f)&&cartWaybill_products[i].get("wait_quantity",0f)==0&&waybill_from_type==WayBillFromKey.Record)
				{
					throw new OrderItemWaitErrorException();
				}
			} 
			catch (Exception e) 
			{
				returnResult.add("msg","订单待发货数异常，请关闭抄单页面后重新抄单");
				throw new JsonException(new JsonObject(returnResult));
			}
		}
//		
//		int NumberOfPieces = (int)((count-1.0)/10.0)+1;
		//ShippingInfoBean shippingInfoBean = expressMgr.getNewShippingFee(sc_id,ccid,pro_id,cartWaybill_products);
		
		String airWayBillNumber = "";
		String destination = "";
		
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long waybill_id = wayBillMgrZJ.addWayBillOrderSub(order.getString("address_name"),order.getString("address_country"),order.getString("address_city"),order.getString("address_zip"),order.getString("address_street"),order.getString("address_state"),ccid,pro_id,order.getString("client_id"),order.getString("tel"),sc_id, airWayBillNumber,shipping_cost,ps_id, destination,StringUtil.getSession(request),inv_di_id,inv_uv,inv_rfe,inv_dog,inv_tv,mail_piece_shape,delivery_note,adminLoggerBean,waybill_from_type,hs_code,cartWaybill_products,1,material,dtp,inv_rfe_chinese,inv_dog_chinese,material_chinese,0);
		
		wayBillMgrZJ.upLoadTrackingNumberToEbay(waybill_id);
		
		DBRow[] orders = wayBillMgrZJ.returnOrdersByWayBillId(waybill_id);
		for (int i = 0; i < orders.length; i++)//检查这张运单的所有订单是否都上运单了
		{
			orderMgr.changeOrderHandle(orders[i].get("oid",0l));
		}
		
		orderMgr.initCartFromOrder(request,oid);
		
		
		
		cartWaybill.clearCart(StringUtil.getSession(request));
		
		DBRow waybill = wayBillMgrZJ.getWayBillOrderByWayBillId(waybill_id);
		
		if(waybill.get("product_status",0)==ProductStatusKey.IN_STORE)
		{
			
			DBRow[] wait_order_items = orderMgr.waitQuantityNotZero(oid);
			if(wait_order_items==null||wait_order_items.length==0)
			{
				returnResult.add("msg","close");
			}
			else
			{
				returnResult.add("msg","ok");
			}
		}
		else
		{
			returnResult.add("waybill_id",waybill_id);
			returnResult.add("msg","stockout");
		}
		
		throw new JsonException(new JsonObject(returnResult));
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setCartWaybill(CartWaybillIFace cartWaybill) {
		this.cartWaybill = cartWaybill;
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr) {
		this.expressMgr = expressMgr;
	}

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

}
