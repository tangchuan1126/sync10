package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.http.Client;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class EPacketDownLoadAction extends ActionFatherController {

	private String saveFilePath = Environment.getHome()+"administrator/order/EPacket/";
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{	
		String trackingNumber = StringUtil.getString(request,"trackingNumber");
		
		String md5url = StringUtil.getMD5("vvme_f8bb3c37f7f03335919b010bc3c5d5e4"+trackingNumber).toLowerCase();
		
		String url = "http://labels.ems.com.cn/partner/api/public/p/static/label/download/"+md5url+"/"+trackingNumber+".pdf";
		
		
		Client.httpGetDownLoad(url,saveFilePath+trackingNumber+".pdf");
		
		DBRow para = new DBRow();
		para.add("result","ok");
		throw new JsonException(new JsonObject(para));
	}

}
