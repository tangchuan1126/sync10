package com.cwc.service.waybill.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class WayBillResultAction extends ActionFatherController {

	private WayBillMgrIFaceZJ wayBillMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		wayBillMgrZJ.wayBillResult(request);
		
		throw new WriteOutResponseException
		(
			 "<script type=\"text/javascript\" src=\"../../../administrator/js/jqGrid-4.1.1/js/jquery-1.7.2.source.js\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/jquery.artDialog.source.js\" type=\"text/javascript\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/iframeTools.source.js\" type=\"text/javascript\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/iframeTools.js\" type=\"text/javascript\"></script>"	
			+"<script type=\"text/javascript\">" 
			+"$.artDialog.close();" 
			+"</script>");

	}
	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

}
