package com.cwc.service.waybill.action.zj;

import java.net.SocketException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.dhl.BarcodeDecoder;
import com.cwc.app.dhl.DHLClient;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.WayBillFromKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class DHLPrintAction extends ActionFatherController {

	private OrderMgrIFace orderMgr;
	private CartWaybillIFace cartWaybill;
	private ProductMgrIFace productMgr;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		DBRow returnResult = new DBRow();//给页面的返回值，原先的msg放入dbrow里，字段为msg，传递运单号回前台
		
		response.setHeader("Pragma","No-cache");   
		response.setHeader("Cache-Control","no-cache");   
		response.setDateHeader("Expires", 0); 
		
		HashMap countryCodeHM = orderMgr.getAllCountryCodeHM();
		 
		String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
		String delivery_note = StringUtil.getString(request,"delivery_note");
		double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
		
		long oid = StringUtil.getLong(request,"oid");
		long old_waybill_id = StringUtil.getLong(request,"old_waybill_id");
		
		long handle = StringUtil.getLong(request,"handle");
		long handle_status = StringUtil.getLong(request,"handle_status");
		long invoice_id = StringUtil.getLong(request,"invoice_id");
		long ccid = StringUtil.getLong(request,"ccid");
		long sc_id = StringUtil.getLong(request,"sc_id");
		long pro_id = StringUtil.getLong(request,"pro_id");

		long inv_di_id = StringUtil.getLong(request,"inv_di_id");
		String inv_uv = StringUtil.getString(request,"inv_uv");
		String inv_rfe = StringUtil.getString(request,"inv_rfe");
		String inv_dog = StringUtil.getString(request,"inv_dog");
		String inv_tv = StringUtil.getString(request,"inv_tv");
		String hs_code = StringUtil.getString(request,"hs_code");
		String material = StringUtil.getString(request,"material");
		String dtp = StringUtil.getString(request,"dtp","recipient");//支付关税人
		
		
		String inv_rfe_chinese = StringUtil.getString(request,"inv_rfe_chinese");
		String inv_dog_chinese = StringUtil.getString(request,"inv_dog_chinese");
		String material_chinese = StringUtil.getString(request,"material_chinese");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		int waybill_from_type = StringUtil.getInt(request,"waybill_from_type");
		
		if (sc_id==0)
		{
			sc_id = Long.parseLong( request.getAttribute("sc_id").toString() );
		}
		
		DBRow order = null; 
		if(oid!=0&&old_waybill_id==0)
		{
			order = orderMgr.getDetailPOrderByOid(oid);
		}
		
		if(oid==0&&old_waybill_id>0)
		{
			order = wayBillMgrZJ.getDetailInfoWayBillById(old_waybill_id);
		}
		DBRow detailInvoice = new DBRow();
		
		//新订单的发票信息都是记录在订单上的，所以需要直接使用订单上的数据
		detailInvoice.add("dog",inv_dog);
		detailInvoice.add("rfe",inv_rfe);
		detailInvoice.add("uv",inv_uv);
		detailInvoice.add("tv",inv_tv);
		detailInvoice.add("di_id",inv_di_id);//递送地址模板ID，在抄单的时候就被记录到订单里
		

		cartWaybill.flush(StringUtil.getSession(request));
		
		DBRow[] cartWaybill_products = cartWaybill.getDetailProduct();
		
		float quantity = 0; 
		
		boolean isHaveHID = false;
		for (int i = 0; i < cartWaybill_products.length; i++) 
		{
			try 
			{
				DBRow orderItem = orderMgr.getDetailOrderItemByIid(cartWaybill_products[i].get("order_item_id",0l));
				
				DBRow product = productMgr.getAllDetailProduct(orderItem.get("pid",0l));
				
				if(product!=null)
				{
					if(product.getString("product_line_name").toLowerCase().equals("hid"))
					{
						isHaveHID = true;
					}
				}
				
				if(orderItem.get("wait_quantity",0f)<cartWaybill_products[i].get("cart_quantity",0f)&&cartWaybill_products[i].get("wait_quantity",0f)==0&&waybill_from_type==WayBillFromKey.Record)
				{
					throw new OrderItemWaitErrorException();
				}
			} 
			catch (Exception e) 
			{
				returnResult.add("msg","订单待发货数异常，请关闭抄单页面后重新抄单");
				throw new JsonException(new JsonObject(returnResult));
			}
			
			quantity += cartWaybill_products[i].get("cart_quantity",0f);
		}
		
		double dutiableDeclared = 0;//申报价值
		if(inv_tv.trim().equals(""))
		{
			dutiableDeclared = Double.parseDouble(inv_uv)*quantity;
		}
		else
		{
			dutiableDeclared = Double.parseDouble(inv_tv);
		}
		
		DBRow detailDelivererInfo;
		if (detailInvoice.get("di_id",0l)==0)
		{
			detailDelivererInfo = orderMgr.getRandomDelivererInfoByPsId(ps_id);
			
			inv_di_id = detailDelivererInfo.get("di_id",0l);
		}
		else
		{
			detailDelivererInfo = orderMgr.getDetailDelivererInfo(detailInvoice.get("di_id",0l));
		}
		
		String airWayBillNumber = "";
		String destination = "";
		double shippment_rate = 0;
		String errorMsg = "";
		try {
			//提交DHL运单信息
			String ConsigneeContactPhoneNumber;
			
			if (order.getString("tel").equals(""))
			{
				ConsigneeContactPhoneNumber = order.getString("client_id");
			}
			else
			{
				ConsigneeContactPhoneNumber = order.getString("tel");
			}
			

			////system.out.println("---------------"+countryCodeHM.get(order.getString("address_country").toLowerCase()));
			
			DHLClient dhlClient = new DHLClient();
					
			dhlClient.setConsigneeCompanyName(order.getString("address_name"));
			dhlClient.setConsigneeAddressLine(order.getString("address_street"));
			dhlClient.setConsigneeCity(order.getString("address_city"));
			dhlClient.setConsigneeDivisionCode(order.getString("address_state"));
			dhlClient.setConsigneePostalCode( order.getString("address_zip"));
			dhlClient.setConsigneeCountryCode( (String)countryCodeHM.get(order.getString("address_country").toLowerCase()));

			
			
			dhlClient.setConsigneeCountryName(order.getString("address_country"));
			dhlClient.setConsigneeContactPersonName(order.getString("address_name"));
			dhlClient.setConsigneeContactPhoneNumber( ConsigneeContactPhoneNumber);
			dhlClient.setConsigneeContactEmailFrom(order.getString("business"));
			dhlClient.setConsigneeContactEmailTo(order.getString("client_id"));
			dhlClient.setDutiableDeclaredValue(StringUtil.formatNumber("0.00",dutiableDeclared));
			dhlClient.setShipmentDetailsNumberOfPieces("1");//String.valueOf(NumberOfPieces)
			dhlClient.setShipmentDetailsWeight(0.5f);
			
			//设置发票信息
			dhlClient.setContents(inv_dog);
			
			//设置发件人信息
			dhlClient.setDev_CompanyName(detailDelivererInfo.getString("CompanyName"));
			dhlClient.setDev_City(detailDelivererInfo.getString("City"));
			dhlClient.setDev_DivisionCode(detailDelivererInfo.getString("DivisionCode"));
			dhlClient.setDev_PostalCode(detailDelivererInfo.getString("PostalCode"));
			dhlClient.setDev_CountryCode(detailDelivererInfo.getString("CountryCode"));
			dhlClient.setDev_CountryName(detailDelivererInfo.getString("CountryName"));
			
			dhlClient.setDev_AddressLine1(detailDelivererInfo.getString("AddressLine1"));
			dhlClient.setDev_AddressLine2(detailDelivererInfo.getString("AddressLine2"));
			dhlClient.setDev_AddressLine3(detailDelivererInfo.getString("AddressLine3"));
			dhlClient.setDev_PhoneNumber(detailDelivererInfo.getString("PhoneNumber"));
			
			//设置关税支付方
			dhlClient.setDutyPaymentType(dtp);

			//dhlClient.setShipmentDetailsWeight(1.5f*order.get("quantity",0f));
			
			//weight 1.5 8 n
			
			dhlClient.setOid( String.valueOf(oid) );
			
			airWayBillNumber = dhlClient.commit();
			destination = dhlClient.getDestination();
			
			
			if(airWayBillNumber.trim().equals(""))
			{
				throw new Exception();
			}
			
			errorMsg = dhlClient.getErrorPage();
			
		}
		catch(SocketException e)
		{
			returnResult.add("msg","错误信息：链接失败");
			throw new JsonException(new JsonObject(returnResult));
		}
		catch (Exception e) 
		{
			if(e.getMessage().indexOf("Server returned HTTP response code: 502 for URL: http://xmlpi.dhl-usa.com/XMLShippingServlet")>=0)
			{
				returnResult.add("msg","错误信息："+e.getMessage()+"<br/>DHL服务器异常，请与DHL联系！");
				throw new JsonException(new JsonObject(returnResult));
			}
		}
		
		if (airWayBillNumber.equals("0000000000"))
		{
			returnResult.add("msg","../TransformXMLtoHTML/ResponseXMLS/"+errorMsg);
			throw new JsonException(new JsonObject(returnResult));
		}
		else
		{
			
			if(destination.equals("CLE")&&isHaveHID)
			{
				returnResult.add("msg","请确定此运单并非HID产品，否则有可能货物被扣！");
				throw new JsonException(new JsonObject(returnResult));
			}
			
			new BarcodeDecoder(airWayBillNumber);
			
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			long waybill_id = wayBillMgrZJ.addWayBillOrderSub(order.getString("address_name"),order.getString("address_country"),order.getString("address_city"),order.getString("address_zip"),order.getString("address_street"),order.getString("address_state"),ccid,pro_id,order.getString("client_id"),order.getString("tel"),sc_id, airWayBillNumber,shipping_cost,ps_id, destination,StringUtil.getSession(request),inv_di_id,inv_uv,inv_rfe,inv_dog,inv_tv,mail_piece_shape,delivery_note,adminLoggerBean,waybill_from_type,hs_code,cartWaybill_products,1,material,dtp,inv_rfe_chinese,inv_dog_chinese,material_chinese,shippment_rate);
			
			//上传TrackingNumber给Ebay
			wayBillMgrZJ.upLoadTrackingNumberToEbay(waybill_id);
			
			DBRow[] orders = wayBillMgrZJ.returnOrdersByWayBillId(waybill_id);
			for (int i = 0; i < orders.length; i++)//检查这张运单的所有订单是否都上运单了
			{
				orderMgr.changeOrderHandle(orders[i].get("oid",0l));
			}
			
			orderMgr.initCartFromOrder(request,oid);

			cartWaybill.clearCart(StringUtil.getSession(request));
			
			DBRow waybill = wayBillMgrZJ.getWayBillOrderByWayBillId(waybill_id);
			
			if(waybill.get("product_status",0)==ProductStatusKey.IN_STORE)
			{
				DBRow[] wait_order_items = orderMgr.waitQuantityNotZero(oid);
				if(wait_order_items==null||wait_order_items.length==0)
				{
					returnResult.add("msg","close");
				}
				else
				{
					returnResult.add("msg","ok");
				}
			}
			else
			{
				returnResult.add("waybill_id",waybill_id);
				returnResult.add("msg","stockout");
			}
			
			throw new JsonException(new JsonObject(returnResult));
		}
		
		
	}
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	public void setCartWaybill(CartWaybillIFace cartWaybill) {
		this.cartWaybill = cartWaybill;
	}
	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
