package com.cwc.service.waybill.action.zwb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zwb.WaybillMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.thoughtworks.xstream.mapper.ArrayMapper;

public class AjaxWaybillPkcountConditionAction extends ActionFatherController{
	
	private WaybillMgrIfaceZwb waybillMgrZwb;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		
        //循环所有产品线
		List<DBRow> arrayDBRow = new ArrayList<DBRow>();
		for(int i=1;i<4;i++){
			DBRow row=this.waybillMgrZwb.seachPkcount(i);
			 DBRow temp =new DBRow();
			 temp.add("name", i);
			 temp.add("id", i);
			 temp.add("count", row.get("count", 0l));
			 arrayDBRow.add(temp);
		 }
		 DBRow row=this.waybillMgrZwb.seachPkcountMuch(4);
			 DBRow temp =new DBRow();
			 temp.add("name", 4);
			 temp.add("id", 4);
			 temp.add("count", row.get("count", 0l));
			 arrayDBRow.add(temp);
		throw new JsonException(new JsonObject(arrayDBRow.toArray(new DBRow[arrayDBRow.size()])));
	}

	public void setWaybillMgrZwb(WaybillMgrIfaceZwb waybillMgrZwb) {
		this.waybillMgrZwb = waybillMgrZwb;
	}

	
   
}
