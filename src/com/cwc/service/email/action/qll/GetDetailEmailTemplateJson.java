package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.EmailTemplateIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

 
public class GetDetailEmailTemplateJson extends ActionFatherController {

	private EmailTemplateIFace emailTemplateMgr; 
	
	 public void setEmailTemplateMgr(EmailTemplateIFace emailTemplateMgr) {
		this.emailTemplateMgr = emailTemplateMgr;
	}

 
		public void perform(HttpServletRequest request, HttpServletResponse arg1)
		throws WriteOutResponseException, RedirectRefException,
		ForwardException, Forward2JspException, RedirectBackUrlException,
		RedirectException, OperationNotPermitException,
		PageNotFoundException, Exception {
	
		long template_id = StringUtil.getLong(request,"template_id");
		DBRow template = 	emailTemplateMgr.getDetailTemplate(template_id);
		if (template == null) {
			template = new DBRow();
			template.add("template_name", "/");
			template.add("template_file_name",null);
	}

	throw new RedirectRefException();
	
}

}
