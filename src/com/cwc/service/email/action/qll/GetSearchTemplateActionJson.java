package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.EmailTemplateIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSearchTemplateActionJson extends ActionFatherController {

	private EmailTemplateIFace emailTemplateMgr; 
	private PageCtrl pc;
	public void setEmailTemplateMgr(EmailTemplateIFace emailTemplateMgr) {
		this.emailTemplateMgr = emailTemplateMgr;
	}

	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		 
		
		String key = StringUtil.getString(request, "key");
		DBRow row [] = emailTemplateMgr.getSearchTemplate(key,pc);
		throw new JsonException(new JsonObject(row));
		
	}

}
