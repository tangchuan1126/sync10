package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.email.SameEmailTemplateException;
import com.cwc.app.iface.qll.EmailTemplateIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class ModEmailTemplateAction extends ActionFatherController 
{
	
	private EmailTemplateIFace emailTemplateMgr;
	private MessageAlerter messageAlert;
	 
	public void setEmailTemplateMgr(EmailTemplateIFace emailTemplateMgr)
	{
		this.emailTemplateMgr = emailTemplateMgr;
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		try
		{
			emailTemplateMgr.modTemplate(request);
		}
		catch (SameEmailTemplateException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","SameEmailTemplateException",""));
		}
		
		throw new RedirectRefException();

	}

}
