package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.EmailTemplateIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class DelEmailTemplateAction extends ActionFatherController{

	private EmailTemplateIFace emailTemplateMgr; 
	
	 
	public void setEmailTemplateMgr(EmailTemplateIFace emailTemplateMgr) {
		this.emailTemplateMgr = emailTemplateMgr;
	}


	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		 
		emailTemplateMgr.delTemplate(request);
		throw new RedirectRefException();
		
	}

	
}
