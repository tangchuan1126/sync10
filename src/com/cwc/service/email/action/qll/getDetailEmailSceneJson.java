package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.EmailSceneMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class getDetailEmailSceneJson extends ActionFatherController{

	private EmailSceneMgrIFace  emailSceneMgr;
	
 

	public void setEmailSceneMgr(EmailSceneMgrIFace emailSceneMgr) {
		this.emailSceneMgr = emailSceneMgr;
	}



	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		long id = StringUtil.getLong(request,"id");
		DBRow category = emailSceneMgr.getDetailScene(id);
		if (category == null) {
			category = new DBRow();
			category.add("parentname", "/");
			category.add("sname",null);
		}
//		JsonObject object = new JsonObject(category);
//		//system.out.println(object);
		throw new JsonException(new JsonObject(category));
	}

}
