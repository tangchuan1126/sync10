package com.cwc.service.email.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.email.HaveEmailSceneException;
import com.cwc.app.exception.email.HaveEmailTemplateException;
import com.cwc.app.iface.qll.EmailSceneMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class DelEmailSceneAction extends ActionFatherController {

	private EmailSceneMgrIFace  emailSceneMgr;
	private MessageAlerter messageAlert;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		try
		{
			emailSceneMgr.delScene(request);
		}
		catch (HaveEmailSceneException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","HaveEmailSceneException",""));
		}
		catch (HaveEmailTemplateException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","HaveEmailTemplateException",""));
		}
		
		throw new RedirectRefException();
		
	}

	public void setEmailSceneMgr(EmailSceneMgrIFace emailSceneMgr) {
		this.emailSceneMgr = emailSceneMgr;
	}
	
	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}


}
