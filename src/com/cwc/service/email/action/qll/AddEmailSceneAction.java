package com.cwc.service.email.action.qll;

 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.EmailSceneMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;


public class AddEmailSceneAction extends ActionFatherController {

	private EmailSceneMgrIFace emailSceneMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception 
	{
		emailSceneMgr.addScene(request);
		throw new RedirectRefException();

	}

	public void setEmailSceneMgr(EmailSceneMgrIFace emailSceneMgr) {
		this.emailSceneMgr = emailSceneMgr;
	}
	 
}
