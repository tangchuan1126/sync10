package com.cwc.service.set.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateReportScreenSetAction extends ActionFatherController{

	private CheckInMgrIfaceZr checkInMgrZr ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		DBRow dbRow=new DBRow();
		String ps_id = StringUtil.getString(request, "ps_id");
		String handle_time = StringUtil.getString(request, "handle_time");
		String red = StringUtil.getString(request, "red");
		String screen_type_key = StringUtil.getString(request, "screen_type_key");
		String yellow = StringUtil.getString(request, "yellow");
		dbRow.add("ps_id", ps_id);
		dbRow.add("handle_time", handle_time);
		dbRow.add("red", red);
		dbRow.add("screen_type_key", screen_type_key);
		dbRow.add("yellow", yellow);
		checkInMgrZr.saveOrUpdateSetLoadReceiveScreemParams(dbRow);

	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
}
