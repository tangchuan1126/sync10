package com.cwc.service.admin.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.app.lucene.zyj.AdminIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 搜索获得JSON格式的商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetSearchAdminsJSONAction extends ActionFatherController 
{
	private SystemConfigIFace systemConfig;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		String q = StringUtil.getString(request, "q");
		int search_mode = StringUtil.getInt(request, "search_mode");
		try
		{
			int suggestNum = systemConfig.getIntConfigValue("page_size");
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(suggestNum);
			
			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("/"," ");
				q = q.replaceAll("\\*","");
				q = q.trim()+"*";
				
				if(!(q.length()>1))
				{
					q = "";
				}
			}
			
			DBRow[] admins = AdminIndexMgr.getInstance().mergeSearch("merge_field", q.toLowerCase(), pc);
			throw new JsonException(new JsonObject(admins));
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
		
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
}
