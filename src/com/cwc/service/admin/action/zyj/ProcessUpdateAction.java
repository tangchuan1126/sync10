package com.cwc.service.admin.action.zyj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ProcessUpdateAction extends ActionFatherController{

	private ProduresMgrZyjIFace produresMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		DBRow errorRow = produresMgrZyj.updateProdure(request);
		String error = "";
		ArrayList<String> list = errorRow.getFieldNames();
		for (int i = 0; i < list.size(); i++) 
		{
			if(!"flag_check_produres".toUpperCase().equals(list.get(i)) && !"flag_check_produres_activity".toUpperCase().equals(list.get(i)) 
					&& !"flag_check_produres_notice".toUpperCase().equals(list.get(i)))
			{
				String str = errorRow.getString(list.get(i));
				if(!"".equals(str) && !"0".equals(str) && !"1".equals(str))
				{
					error += (str + "<br/>");
				}
			}
		}
		errorRow.add("error", error);
		throw new JsonException(new JsonObject(errorRow));
		
	}


	public void setProduresMgrZyj(ProduresMgrZyjIFace produresMgrZyj) {
		this.produresMgrZyj = produresMgrZyj;
	}

}
