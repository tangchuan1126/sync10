package com.cwc.service.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.admin.TokenCheckNotPassException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zzq.PushMessageByOpenfireMgrIfaceZzq;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 管理员登录
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AdminLoginAction extends ActionFatherController 
{
	private AdminMgrIFace adminMgr;
	
	private PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException,TokenCheckNotPassException, AdminIsExistException,Exception
	{
		try
		{
			adminMgr.adminLogin(request, response);
			//pushMessageByOpenfireMgrZzq.login(StringUtil.getString(request,"account"), StringUtil.getString(request,"pwd"), false, false, false);
		}
		catch (AccountOrPwdIncorrectException e)
		{
			throw new WriteOutResponseException("1");
		}
		catch (VerifyCodeIncorrectException e)
		{
			throw new WriteOutResponseException("2");
		}
		catch (AccountNotPermitLoginException e)
		{
			throw new WriteOutResponseException("3");
		}
		catch (TokenCheckNotPassException e)
		{
			throw new WriteOutResponseException("7");
		}
		catch (AdminIsExistException e)
		{
			throw new WriteOutResponseException("8");
		}
		throw new WriteOutResponseException("0");
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	public void setPushMessageByOpenfireMgrZzq(
			PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq) {
		this.pushMessageByOpenfireMgrZzq = pushMessageByOpenfireMgrZzq;
	}

}
