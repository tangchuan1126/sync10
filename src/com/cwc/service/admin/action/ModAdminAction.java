package com.cwc.service.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 修改管理员
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ModAdminAction extends ActionFatherController 
{
	private AdminMgrIFace adminMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		adminMgr.modAdmin(request);
		throw new RedirectRefException();
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}

}
