package com.cwc.service.admin.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.key.ControlTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class LoginFunctionMenuAction extends ActionFatherController {

	private AdminMgrIFace adminMgr;
	
	private AccountMgrIFaceSbb accountMgr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, DoNothingException, Exception 
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		//response.getWriter().append(adminMgr.getLeftTree().toString());
		
		AdminLoginBean alb = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		DBRow[] menus = accountMgr.getAccountNavigationBar(alb.getAdid());
		
		JSONArray menuJson = new JSONArray();
		
		for (int i = 0; menus!=null && i<menus.length; i++) {
			String title = menus[i].getString("title");
			String url = menus[i].getString("link");
			String ico = menus[i].getString("ico");
			long id = menus[i].get("id", 0l);
			
			JSONArray childrens = new JSONArray();
			
			DBRow[] nodes = (DBRow[]) menus[i].get("submenu");
			
			for (int j = 0; nodes!=null && j<nodes.length; j++) {
				String nodeTitle = nodes[j].getString("title");
				String nodeUrl = nodes[j].getString("link");
				String nodeIco = nodes[j].getString("ico");
				long nodeId = nodes[j].get("id", 0l);
				
				JSONObject children = new JSONObject();
				children.put("title", nodeTitle);
				children.put("url", nodeUrl);
				children.put("ico", nodeIco);
				children.put("id", nodeId);
				
				childrens.put(children);
			}
			
			JSONObject menu = new JSONObject();
			menu.put("title", title);
			menu.put("url", url);
			menu.put("ico", ico);
			menu.put("children", childrens);
			menu.put("id", id);
			
			menuJson.put(menu);
		}
		
		response.getWriter().append(menuJson.toString());

	}
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setAccountMgr(AccountMgrIFaceSbb accountMgr) {
		this.accountMgr = accountMgr;
	}

}
