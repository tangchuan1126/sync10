package com.cwc.service.admin.action.gzy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gzy.PurcharseIFaceGZY;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSupplierActionJSONAction extends ActionFatherController{

	private PurcharseIFaceGZY purchase;
	public void perform(HttpServletRequest request, HttpServletResponse response)throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception {
		long productLineId = StringUtil.getLong(request, "productline_id");
		DBRow rows [] = purchase.getSupplierByProductline(productLineId);
		throw new JsonException(new JsonObject(rows));
	}
	public void setPurchase(PurcharseIFaceGZY purchase) {
		this.purchase = purchase;
	}
	
}
