package com.cwc.service.admin.action.gzy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.iface.gzy.AdminMgrIFaceGZY;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 增加管理员
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddAdminActionGZY extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private AdminMgrIFaceGZY adminMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			adminMgr.addAdmin(request);
		}
		catch (AdminIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","AdminIsExistException",""));
		}

		throw new RedirectRefException();
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setAdminMgr(AdminMgrIFaceGZY adminMgr)
	{
		this.adminMgr = adminMgr;
	}


}
