package com.cwc.service.admin.action.gzy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gzy.SupplierIFaceGZY;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ModSupplierActionGZY extends ActionFatherController{
	private SupplierIFaceGZY supplier;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException, Exception {
		supplier.modSupplier(request);
		throw new RedirectRefException();
	}

	public void setSupplier(SupplierIFaceGZY supplier) {
		this.supplier = supplier;
	}
}
