package com.cwc.service.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.RoleIsExistException;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 增加角色
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddRoleAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private AdminMgrIFace adminMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{

		try
		{
			adminMgr.addRole(request);
			messageAlert.setMessage(request,Resource.getStringValue("","addRole",""));
		}
		catch (RoleIsExistException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","RoleIsExistException",""));
		}

		throw new RedirectRefException();
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}


}
