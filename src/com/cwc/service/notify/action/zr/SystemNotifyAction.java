package com.cwc.service.notify.action.zr;

import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zr.SysNotifyIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.outface.core.ParameterException;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

 
/**
 * 
 * @author win7zr
 */
public class SystemNotifyAction extends ActionFatherController  {

	
	private SysNotifyIfaceZr sysNotifyMgrZr ;
	/**
	 *  sender: string(adid require)
	 *	receivers:string(adid,adid,require)
	 *	subtitle:(简介，用于在系统提示的时候显示出来,或者在邮件显示的时候的Title,require)
	 *	messageType:1:简单的通知,2：任务类型 ,require
	 *	needEmail:1:Y,2:0(require)
	 *	needAppNotify:1:Y,0:N
	 *	emailContent:email 的内容
	 *	attachments:附件
	 */
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			int ret = BCSKey.SUCCESS ;
 			String errCode = "";
			DBRow result = new DBRow();
			try{
				DBRow data =   initData(request);
				/*data.add("sender", 100198);
				data.add("receivers", "329169773@qq.com,zhangrui@oso.com");
				data.add("ccreceivers", "zhangrui@oso.com");
				data.add("subtitle", "简介，用于在系统提示的时候显示出来,或者在邮件显示的时候的Title,require");
				data.add("messageType", "1");
				data.add("needEmail", "1");
				data.add("needAppNotify", "1");
				data.add("emailContent", "简介，用于在系统提示的时候显示出来,或者在邮件显");
			//	data.add("fileids", "427325,427320");
				data.add("emailContent", "简介，用于在系统提示的时候显示出来,或者在邮件显");
				*/
 				sysNotifyMgrZr.handleSystemNotify(data);
			}catch(ParameterException e){
				ret = BCSKey.FAIL;
				errCode = e.getMessage();
			}catch(Exception e){
				ret = BCSKey.FAIL;
	 			errCode = "System Error.";
 			}
			result.add("ret", ret);
			result.add("errCode", errCode);
 			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		
	}

 
	public SysNotifyIfaceZr getSysNotifyMgrZr() {
		return sysNotifyMgrZr;
	}

	public void setSysNotifyMgrZr(SysNotifyIfaceZr sysNotifyMgrZr) {
		this.sysNotifyMgrZr = sysNotifyMgrZr;
	}

	
	private DBRow initData(HttpServletRequest request){
		//AdminMgr adminMgr = ( AdminMgr )MvcUtil.getBeanFromContainer( "adminMgr" );
		//AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean( StringUtil.getSession( request ) );
		DBRow data = new DBRow();
		
	 
	/*	Enumeration<String>  d = request.getParameterNames();
		while(d.hasMoreElements()){
			System.out.println(d.nextElement());;
		}*/
 		data.add("sender",StringUtil.getString(request, "sender"));
		 data.add("receivers", StringUtil.getString(request, "receivers"));
		data.add("ccreceivers", StringUtil.getString(request, "ccreceivers"));
		data.add("subtitle", StringUtil.getString(request, "subtitle"));
		data.add("messageType", "1");
		data.add("needEmail", "1");
		data.add("needAppNotify", "1");
		data.add("emailContent", StringUtil.getString(request, "emailContent"));
	//	data.add("fileids", "427325,427320");
//		data.add("emailContent", "简介，用于在系统提示的时候显示出来,或者在邮件显");
		return data;
	}
	
}
