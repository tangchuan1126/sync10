package com.cwc.service.workflow.changeRetailPrice.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.app.util.Environment;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class BossAgreeAction extends ActionFatherController 
{
	private QuoteIFace quoteMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long rp_id = StringUtil.getLong(request, "rp_id");
		String task_id = StringUtil.getString(request, "task_id");
		AdminLoginBean adminInfo = (new AdminMgr()).getAdminLoginBean( request.getSession(true) );
//		jbpmMgr.addJoinTaskProple(task_id, String.valueOf(adminInfo.getAdid()));
//		jbpmMgr.completeTaskViaOutCome(task_id, "to end1");
		
		quoteMgr.updateProductProfit(StringUtil.getSession(request), rp_id);
		
		
		//总经理审批通过后，把商品毛利更新
		
		
		throw new RedirectRefException();
	}

	public void setQuoteMgr(QuoteIFace quoteMgr) 
	{
		this.quoteMgr = quoteMgr;
	}
}
