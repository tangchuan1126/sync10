package com.cwc.service.workflow.refund.action.qll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.OrdersMgrIFaceQLL;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
/**
 * 当流程被驳回时，申请人修改了信息后继续流转的action
 * @author Administrator
 *
 */
public class ReStartRefundProcessAction extends ActionFatherController{

	private OrdersMgrIFaceQLL ordersMgrQLL;
			
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
			 
			ordersMgrQLL.reStartRefundProcess(request);
			
		    throw new RedirectRefException();
	}
	
	public void setOrdersMgrQLL(OrdersMgrIFaceQLL ordersMgrQLL) {
		this.ordersMgrQLL = ordersMgrQLL;
	}

	 
}
