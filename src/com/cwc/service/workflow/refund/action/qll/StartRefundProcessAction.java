package com.cwc.service.workflow.refund.action.qll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.OrdersMgrIFaceQLL;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
/**
 * 开始流程的action接收任务
 * @author Administrator
 *
 */
public class StartRefundProcessAction extends ActionFatherController {
	
	private String manager;
	private OrdersMgrIFaceQLL ordersMgrQLL; 
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		    
		 	ordersMgrQLL.startRefundProcess(request, manager);
//			jbpmMgr.completeTask(taskId, mapInfo);
//			orderMgr.addPOrderNotePrivate(StrUtil.getLong(request, "oid"), orderNote,TracingOrderKey.OTHERS, ses,0);
	     	throw new RedirectRefException();
	}
 
	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setOrdersMgrQLL(OrdersMgrIFaceQLL ordersMgrQLL) {
		this.ordersMgrQLL = ordersMgrQLL;
	}
	 
}
