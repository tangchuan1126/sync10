package com.cwc.service.workflow.refund.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.PaypalPaymentMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
/**
 * 进行退款不负责流程流转的action
 * @author Administrator
 *
 */
public class ExecuteRefundProcessAction extends ActionFatherController {

	private PaypalPaymentMgrIFace paypalPaymentMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		 	//添加执行退款的接口
			String rs = paypalPaymentMgr.rufundTransaction(StringUtil.getString(request,"refundType"), StringUtil.getString(request, "tranID"), StringUtil.getDouble(request, "amount"), StringUtil.getString(request, "note"),StringUtil.getString(request, "currencyDoce"));
		    String result[] = rs.split(",");
			DBRow row = new DBRow();
			row.add("rs",result[0]);
			if(result[0].equals("Failure"))
			{
				row.add("reasion", result[2]);
			}else
			{
				row.add("reasion", "网络异常，稍后请重试 ...");
			}
			throw new JsonException(new JsonObject(row));
	}
	
	public void setPaypalPaymentMgr(PaypalPaymentMgrIFace paypalPaymentMgr) {
		this.paypalPaymentMgr = paypalPaymentMgr;
	}

}
