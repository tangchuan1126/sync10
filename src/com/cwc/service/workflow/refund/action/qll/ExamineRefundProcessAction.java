package com.cwc.service.workflow.refund.action.qll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.OrdersMgrIFaceQLL;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
/**
 * 在审批环节审批了结果时向下流转的action
 * @author Administrator
 *
 */
public class ExamineRefundProcessAction extends ActionFatherController{

	 
	private OrdersMgrIFaceQLL ordersMgrQLL;
	
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		 
			 ordersMgrQLL.examineRefundProcess(request);
			  
			 throw new RedirectRefException();
	}
	
	public void setOrdersMgrQLL(OrdersMgrIFaceQLL ordersMgrQLL) {
		this.ordersMgrQLL = ordersMgrQLL;
	}
	

}
