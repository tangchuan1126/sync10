package com.cwc.service.account.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class UpdateAccountAction extends ActionFatherController 
{

	private AccountMgrIfaceZr accountMgrZr;
	
	public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{		
			long account_id = StringUtil.getLong(request, "account_id");
			DBRow updateRow = new DBRow();
			String account_name= StringUtil.getString(request, "account_name");
			String account_number = StringUtil.getString(request, "account_number");
			String account_blank = StringUtil.getString(request, "account_blank");
			String account_swift = StringUtil.getString(request, "account_swift");
			String account_phone = StringUtil.getString(request, "account_phone");
			String account_address = StringUtil.getString(request, "account_address");
			String account_blank_address = StringUtil.getString(request, "account_blank_address");
		  
			updateRow.add("account_name", account_name);
			updateRow.add("account_number", account_number);
			updateRow.add("account_blank", account_blank);
			updateRow.add("account_swift", account_swift);
			
			updateRow.add("account_phone", account_phone);
			updateRow.add("account_address", account_address);
			updateRow.add("account_blank_address", account_blank_address);
			
		 	accountMgrZr.updateAccount(account_id, updateRow);
		 	DBRow result = new DBRow();
		 	result.add("flag", "success");
		 	throw new JsonException(new JsonObject(result));
		
	}

}
