package com.cwc.service.account.action.zr;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.sun.star.lang.NullPointerException;

public class GetAccountInfoAction extends ActionFatherController
{
	private AccountMgrIFaceSbb accountMgrSbb;
	
	public void setAccountMgrSbb(AccountMgrIFaceSbb accountMgrSbb) {
		this.accountMgrSbb = accountMgrSbb;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{       
			int ret = BCSKey.SUCCESS ;
			int err = BCSKey.FAIL ;
			DBRow row  =new DBRow();
			
			try {
				DBRow datas  =new DBRow();
				String account =StringUtil.getString(request,"account");
				if(account.equals("")||account==null){
					
					throw new NullPointerException("1");
				}
				DBRow userInfo = accountMgrSbb.GetUserInfoByAccount(account);
				if(userInfo==null||userInfo.isEmpty()){
					
					throw new NullPointerException("2");
				}
				long adid = userInfo.get("adid", 0l);
				if(adid!=0l){
					//获取部门和职务信息。
					DBRow[] departInfo= accountMgrSbb.findAdminDepartmentPosts(adid);
					//获取仓库和zone信息。
					DBRow[] storageInfo=accountMgrSbb.findAdminWarehouseAreas(adid);
					//通过账号ID查询账号所有图像信息
					DBRow[] imgInfo=accountMgrSbb.findAccountPortrait(adid);
					
					datas.add("departInfo", departInfo);
					datas.add("storageInfo", storageInfo);
					datas.add("imgInfo", imgInfo);
					row.add("ret",ret);
					row.add("err",err);
					row.add("datas", datas);
				}else{
					row.add("ret",ret);
					row.add("err",err);
					throw new NullPointerException("3");
				}
			} catch (NullPointerException e) {
			   if(e.getMessage()=="1"){
				   	ret=BCSKey.FAIL;
					err=BCSKey.AccountNullpointException;
					row.add("ret",ret);
					row.add("err",err);
			   }else if(e.getMessage()=="2"){
				   	ret=BCSKey.FAIL;
					err=BCSKey.NoUserByaccountException;
					row.add("ret",ret);
					row.add("err",err);
			   }else if(e.getMessage()=="3"){
					ret=BCSKey.FAIL;
					err=BCSKey.GetAdidException;
				   	row.add("ret",ret);
					row.add("err",err);
			   }
			   
			}
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(row));
	}

}
