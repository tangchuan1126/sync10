package com.cwc.service.configChange.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.zj.ConfigChangeMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditConfigChangeItemAction extends ActionFatherController {

	private ConfigChangeMgrIFaceZJ configChangeMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, DoNothingException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作
			
			if(oper.equals("edit"))
			{
				configChangeMgrZJ.modConfigChangeItemJqgrid(request);
			}
			else if(oper.equals("add"))
			{
				configChangeMgrZJ.addConfigChangeItemJqgrid(request);
			}
			else if(oper.equals("del"))
			{
				configChangeMgrZJ.delConfigChangeItemJqgrid(request);
			}
			
			throw new WriteOutResponseException("0");
		}
		catch(ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
	}
	public void setConfigChangeMgrZJ(ConfigChangeMgrIFaceZJ configChangeMgrZJ) {
		this.configChangeMgrZJ = configChangeMgrZJ;
	}

}
