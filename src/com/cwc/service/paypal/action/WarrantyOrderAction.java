package com.cwc.service.paypal.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class WarrantyOrderAction  extends ActionFatherController{

	private OrderMgrIfaceZR orderMgrZr;
	 
	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr) {
		this.orderMgrZr = orderMgrZr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse reponse)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
		DBRow[] rows ;
	 	long warrantyOrderId = Long.parseLong(StringUtil.getString(request, "id"));
		rows = orderMgrZr.getWarrantyOrderAndItemsById(warrantyOrderId);
		throw new JsonException(new JsonObject(rows));
		
	}

}
