package com.cwc.service.paypal.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeException;
import com.cwc.exception.WriteOutResponseException;

/**
 * paypal触发
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class NotifyAction extends ActionFatherController {

	
	private OrderMgrIfaceZR orderMgrZr;
	 
	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr) {
		this.orderMgrZr = orderMgrZr;
	}
 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		try{
			long orderId = orderMgrZr.notifyIPN(request);
			
			if(orderId != 0l)
			{
				orderMgrZr.orderPreHandle(orderId);
			}
		} 
		catch (Exception e){
			throw new SendResponseServerCodeException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		throw new SendResponseServerCodeException(HttpServletResponse.SC_OK);
		
	}
	 

}
