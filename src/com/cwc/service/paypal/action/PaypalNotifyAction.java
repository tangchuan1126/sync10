package com.cwc.service.paypal.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.PaypalMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeException;
import com.cwc.exception.WriteOutResponseException;

/**
 * paypal触发
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PaypalNotifyAction extends ActionFatherController 
{
	private PaypalMgrIFace paypalMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws SendResponseServerCodeException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		/**
		 * paypal触发接口特殊处理，触发订单过程发生任何错误
		 * 都直接返回500错误代码，正常返回200代码
		 */
		
		try
		{
			paypalMgr.paypalNotify(request);
		} 
		catch (Exception e)
		{
			throw new SendResponseServerCodeException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		
		throw new SendResponseServerCodeException(HttpServletResponse.SC_OK);
	}

	public void setPaypalMgr(PaypalMgrIFace paypalMgr)
	{
		this.paypalMgr = paypalMgr;
	}

}
