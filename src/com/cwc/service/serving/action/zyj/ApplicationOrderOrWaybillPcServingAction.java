package com.cwc.service.serving.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ApplicationOrderOrWaybillPcServingAction extends ActionFatherController{

	private ServiceOrderMgrZyjIFace serviceOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		long sid = serviceOrderMgrZyj.applicationOrderOrWaybillPcServing(request);
		throw new WriteOutResponseException(String.valueOf(sid));
		
	}

	public void setServiceOrderMgrZyj(ServiceOrderMgrZyjIFace serviceOrderMgrZyj) {
		this.serviceOrderMgrZyj = serviceOrderMgrZyj;
	}
	
	

}
