package com.cwc.service.catalog.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.catalog.HaveProductException;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

/**
 * 删除商品分类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DelProductCatalogAction extends ActionFatherController 
{
	private CatalogMgrIFace catalogMgr;
	//private MessageAlerter messageAlert;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
//		try
//		{
			DBRow row = catalogMgr.delProductCatalog(request);
			throw new JsonException(new JsonObject(row));
		//}
//		catch (HaveProductException e)
//		{
//			messageAlert.setMessage(request,Resource.getStringValue("","HaveProductException",""));
//		}
		
		//throw new RedirectRefException();
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr)
	{
		this.catalogMgr = catalogMgr;
	}

//	public void setMessageAlert(MessageAlerter messageAlert)
//	{
//		this.messageAlert = messageAlert;
//	}
	
}
