package com.cwc.service.catalog.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获得JSON格式的商品分类详细信息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetDetailProductCatalogJSONAction extends ActionFatherController 
{
	private CatalogMgrIFace catalogMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		long parentid = StringUtil.getLong(request,"parentid");
		DBRow catalog = catalogMgr.getDetailProductCatalogById(parentid);
		if ( catalog==null )
		{
			catalog = new DBRow();
			catalog.add("title","/");
		}
		
		throw new JsonException(new JsonObject(catalog));
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr)
	{
		this.catalogMgr = catalogMgr;
	}


}
