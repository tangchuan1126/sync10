package com.cwc.service.vetualterminal.action.qll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.qll.VertualTerminalMgrQLL;
import com.cwc.app.exception.task.NotTaskOwnerException;
import com.cwc.app.iface.qll.PaypalPaymentMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddVertualTerminalAction extends ActionFatherController {

	private VertualTerminalMgrQLL vertualTerminalMgrQLL;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception
	{
			vertualTerminalMgrQLL.addVertualTerminal(request);
			 
			throw new RedirectRefException();
	}

	public void setVertualTerminalMgrQLL(VertualTerminalMgrQLL vertualTerminalMgrQLL) {
		this.vertualTerminalMgrQLL = vertualTerminalMgrQLL;
	}


	 

}
