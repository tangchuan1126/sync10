package com.cwc.service.vetualterminal.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.qll.PaypalPaymentMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ExecuteVertualTerminalByCustomerAction extends ActionFatherController{

	private PaypalPaymentMgrIFace paypalPaymentMgr;
	
	public void setPaypalPaymentMgr(PaypalPaymentMgrIFace paypalPaymentMgr) {
		this.paypalPaymentMgr = paypalPaymentMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 
			String rs=paypalPaymentMgr.ExceuteVertualTerminalByPayMentPage(request);
			String result[] = rs.split(",");
			DBRow row = new DBRow();
			row.add("rs",result[0]);
			if(result[0].contains("Failure")||result[0].contains("SuccessWithWarning")){
				row.add("reasion", result[2]);
			}else{
				row.add("reasion", "System Error , Plase Try Later ...");
			}
			throw new JsonException(new JsonObject(row));
	}

}
