package com.cwc.service.vetualterminal.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetOrderNotLoginOnAction   extends ActionFatherController{

	private OrderMgrIFace orderMgr;
	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			long oid = StringUtil.getLong(request, "oid");
			DBRow order  ;
			if(oid != 0l){
				order = orderMgr.getDetailPOrderByOid(oid);
				if(order == null){
					order = new DBRow();
					order.add("flag", "error");
				}
			}else{
				order = new DBRow();
				order.add("flag", "success");
			}
			  
		   throw new JsonException(new JsonObject(order));
		
	}

}
