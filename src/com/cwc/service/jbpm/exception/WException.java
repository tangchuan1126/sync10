package com.cwc.service.jbpm.exception;

/**
 * 汇率不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class WException extends Exception 
{
	public WException() 
	{
		super();
	}
	
	public WException(String inMessage)
	{
		super(inMessage);
	}
}
