package com.cwc.service.task.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.task.NotTaskOwnerException;
import com.cwc.app.iface.TaskIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 不能完成任务
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class UnCompleteOrderTaskAction extends ActionFatherController 
{
	private MessageAlerter messageAlert;
	private TaskIFace taskMgr;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		try
		{
			taskMgr.unCompleteOrderTask(request);
		}
		catch(NotTaskOwnerException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","NotTaskOwnerException",""));
		}

		throw new RedirectRefException();
	}

	public void setMessageAlert(MessageAlerter messageAlert)
	{
		this.messageAlert = messageAlert;
	}

	public void setTaskMgr(TaskIFace taskMgr)
	{
		this.taskMgr = taskMgr;
	}


}
