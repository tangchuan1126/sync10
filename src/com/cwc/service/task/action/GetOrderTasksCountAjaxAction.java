package com.cwc.service.task.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.TaskIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GetOrderTasksCountAjaxAction extends ActionFatherController 
{
	private TaskIFace taskMgr;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		int c = 0;

		try
		{
//			c = taskMgr.getOrderTasksCount();
		}
		catch(Exception e)
		{
			throw new WriteOutResponseException("error");
		}

		throw new WriteOutResponseException(String.valueOf(c));
	}

	public void setTaskMgr(TaskIFace taskMgr)
	{
		this.taskMgr = taskMgr;
	}

}
