package com.cwc.service.bill.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.InvoiceStateKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AddBillByWeightAction extends ActionFatherController{

	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow billRow = new DBRow();
		long bill_id = StringUtil.getLong(request, "bill_id");
		boolean isUpdate = bill_id !=0l ? true:false;
		
		// 读取billRow
	 
		String[] arrayAddress = new String[]{"address_name","address_country","address_city","address_zip","address_street","address_country_code","address_state","note","memo_note"};
		for(String name : arrayAddress){	
			billRow.add(name, StringUtil.getString(request, name));
		}
		billRow.add("invoice_state", InvoiceStateKey.NewAdd);
		// product_cost,
		billRow.add("subtotal", StringUtil.getDouble(request, "subtotal"));
		billRow.add("total_discount", StringUtil.getDouble(request, "total_discount"));
		billRow.add("shipping_fee", StringUtil.getDouble(request, "shipping_fee"));
		billRow.add("save", StringUtil.getDouble(request, "save"));
		billRow.add("rate", StringUtil.getDouble(request, "rate"));
		billRow.add("product_cost", StringUtil.getDouble(request, "total_cost"));
		billRow.add("rate_type", StringUtil.getString(request, "rate_type"));
		
	
		billRow.add("client_id", StringUtil.getString(request,"client_id"));
		billRow.add("payer_email", StringUtil.getString(request, "payer_email"));
		billRow.add("tel", StringUtil.getString(request, "tel"));
		if(!isUpdate){
			billRow.add("create_date", DateUtil.NowStr());
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			
		 	long adid = adminLoggerBean.getAdid(); 
		 	billRow.add("create_adid",adid);
		}else{
			billRow.add("invoice_state", StringUtil.getInt(request, "invoice_state"));
		}
		//ccid,pro_id,ps_id
		billRow.add("ccid", StringUtil.getLong(request, "ccid"));
		billRow.add("pro_id", StringUtil.getLong(request, "pro_id"));
		billRow.add("ps_id", StringUtil.getLong(request, "ps_id"));
		billRow.add("sc_id", StringUtil.getLong(request, "sc_id"));
		billRow.add("bill_type", StringUtil.getInt(request,"bill_type"));
		billRow.add("total_weight", StringUtil.getFloat(request, "total_weight"));
		billRow.add("total_quantity",StringUtil.getFloat(request, "total_quantity"));
		billRow.add("account_payee",StringUtil.getLong(request,"account_payee"));
		billRow.add("weight_type", StringUtil.getString(request, "weight_type"));
		billRow.add("account", StringUtil.getString(request, "account"));
		billRow.add("account_name",StringUtil.getString(request,"account_name"));
		billRow.add("key_type", StringUtil.getInt(request, "key_type"));
		billRow.add("bill_status", BillStateKey.quote);
	 	if(isUpdate){
	 		billMgrZr.updateBill(bill_id, billRow, null);
	 	}else{
	 		billMgrZr.addBill(billRow, null);
	 	}
	 	DBRow result = new DBRow();
	 	result.add("result", "success");
	 	result.add("oid", 0);
		throw new JsonException(new JsonObject(result));
		
	}

}
