package com.cwc.service.bill.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.InvoiceMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class SendInvoiceAction extends ActionFatherController {
	
	private InvoiceMgrIfaceZR invoiceMgrZr;

 
	public void setInvoiceMgrZr(InvoiceMgrIfaceZR invoiceMgrZr) {
		this.invoiceMgrZr = invoiceMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
			long billId = StringUtil.getLong(request, "bill_id");
			DBRow result = invoiceMgrZr.sendInvoiceByInvoiceId(billId);
			throw new JsonException(new JsonObject(result));
			
			
		
	}

}
