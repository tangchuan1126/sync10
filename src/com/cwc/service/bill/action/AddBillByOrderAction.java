package com.cwc.service.bill.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.InvoiceStateKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AddBillByOrderAction extends ActionFatherController{

	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow billRow = new DBRow();
		long bill_id = StringUtil.getLong(request, "bill_id");
		boolean isUpdate = bill_id !=0l ? true:false;
		
		// 读取billRow
	 
		String[] arrayAddress = new String[]{"address_name","address_country","address_city","address_zip","address_street","address_country_code","address_state","note","memo_note"};
		for(String name : arrayAddress){	
			billRow.add(name, StringUtil.getString(request, name));
		}
	 
		// product_cost,
		billRow.add("rate", StringUtil.getDouble(request, "rate"));
 		billRow.add("rate_type", StringUtil.getString(request, "rate_type"));
		
 		billRow.add("invoice_state", InvoiceStateKey.NewAdd);
 		billRow.add("bill_status", BillStateKey.quote);
		billRow.add("client_id", StringUtil.getString(request,"client_id"));
		billRow.add("payer_email", StringUtil.getString(request, "payer_email"));
		billRow.add("tel", StringUtil.getString(request, "tel"));
		if(!isUpdate){
			billRow.add("create_date", DateUtil.NowStr());
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			
		 	long adid = adminLoggerBean.getAdid(); 
		 	billRow.add("create_adid",adid);
		 	billRow.add("porder_id", StringUtil.getLong(request, "porder_id"));
		}else{
			billRow.add("invoice_state", StringUtil.getInt(request, "invoice_state"));
		}
		billRow.add("ccid", StringUtil.getLong(request, "ccid"));
		billRow.add("pro_id", StringUtil.getLong(request, "pro_id"));
		billRow.add("bill_type", StringUtil.getInt(request,"bill_type"));
		billRow.add("account_payee",StringUtil.getLong(request,"account_payee"));
		billRow.add("account", StringUtil.getString(request, "account"));
		billRow.add("account_name",StringUtil.getString(request,"account_name"));
		billRow.add("key_type", StringUtil.getInt(request, "key_type"));
		billRow.add("order_fee", StringUtil.getDouble(request,"order_fee"));
		billRow.add("subtotal",StringUtil.getDouble(request,"order_fee"));
		billRow.add("save",StringUtil.getDouble(request,"order_fee"));	 
	 	if(isUpdate){
	 		billMgrZr.updateBill(bill_id, billRow, null);
	 	}else{
	 		billMgrZr.addBillByOrder(billRow, null);
	 	}
		throw new JsonException("success");
		
	}

}
/*
 * 	bill_type=5&key_type=0&account_payee=-2&note=3333%0D%0A&payer_email=buyer%40paypalsandbox.com&
 * address_name=John+Smith&address_street=123%2C+any+street&address_city=San+Jose&address_zip=95131&
 * tel=444444&address_country_code=US&rate=6.31&order_fee=33333&memo_note=4444%0D%0A&account=2222&
 * account_name=111111&rate_type=USD
 * */
