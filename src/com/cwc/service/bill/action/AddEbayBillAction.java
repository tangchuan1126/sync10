package com.cwc.service.bill.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.InvoiceStateKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AddEbayBillAction extends ActionFatherController {

	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		// 添加Bill 和BillItem
			long billId = StringUtil.getLong(request, "bill_id");
			boolean isUpdate =(billId != 0l? true:false);
			DBRow billRow = new DBRow();
			List<DBRow> itemList = new ArrayList<DBRow>();
		
			// 读取billRow
		 
			String[] arrayAddress = new String[]{"address_name","address_country","address_city","address_zip","address_street","address_country_code","address_state","note","memo_note"};
			for(String name : arrayAddress){	
				billRow.add(name, StringUtil.getString(request, name));
			}
			billRow.add("invoice_state", InvoiceStateKey.NewAdd);
			// product_cost,
			billRow.add("subtotal", StringUtil.getDouble(request, "subtotal"));
			billRow.add("total_discount", StringUtil.getDouble(request, "total_discount"));
			billRow.add("shipping_fee", StringUtil.getDouble(request, "shipping_fee"));
			billRow.add("save", StringUtil.getDouble(request, "save"));
			billRow.add("rate", StringUtil.getDouble(request, "rate"));
			 
			billRow.add("rate_type", StringUtil.getString(request, "rate_type"));
			
		
			billRow.add("client_id", StringUtil.getString(request,"client_id"));
			billRow.add("payer_email", StringUtil.getString(request, "payer_email"));
			billRow.add("tel", StringUtil.getString(request, "tel"));
			
			//ccid,pro_id,ps_id
 			 
			billRow.add("bill_type", StringUtil.getInt(request,"bill_type"));
			billRow.add("ccid", StringUtil.getInt(request,"ccid"));
			billRow.add("pro_id", StringUtil.getInt(request,"pro_id"));
			 
			billRow.add("total_quantity",StringUtil.getFloat(request, "total_quantity"));
			billRow.add("account_payee",StringUtil.getLong(request,"account_payee"));
 
			billRow.add("account", StringUtil.getString(request, "account"));
			billRow.add("account_name",StringUtil.getString(request,"account_name"));
			billRow.add("key_type", StringUtil.getInt(request, "key_type"));
			billRow.add("bill_status", BillStateKey.quote);
			if(!isUpdate){
				AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
				
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
				
			 	long adid = adminLoggerBean.getAdid(); 
			 	billRow.add("create_adid",adid);
			 	billRow.add("create_date", DateUtil.NowStr());
			 	
			}else{
				billRow.add("invoice_state", StringUtil.getInt(request, "invoice_state"));
			}
			
			//读取Items 
			int index = 1 ;
			String itemNumber = StringUtil.getString(request, "item_number_"+index);
 			while(itemNumber != null && itemNumber.length() > 0){
 				DBRow item = new DBRow();
 				item.add("item_number", itemNumber)	;
 				item.add("name", StringUtil.getString(request,"name_"+index));
				item.add("actual_price", StringUtil.getDouble(request,"actual_price_"+index));
				item.add("quantity", StringUtil.getFloat(request,"quantity_"+index));
				item.add("amount", StringUtil.getDouble(request,"amount_"+index));
				if(isUpdate){
					item.add("bill_item_id", StringUtil.getLong(request, "item_id_"+index));
				}
				itemList.add(item);
				index++ ;
				itemNumber =  StringUtil.getString(request, "item_number_"+index);
			}
 			if(isUpdate){
 				billMgrZr.updateBill(billId, billRow, itemList);
 			}else{
 				billMgrZr.addBill(billRow, itemList);
 			}
			// 清除 session
			CartQuoteIFace cartQuote = (CartQuoteIFace)MvcUtil.getBeanFromContainer("cartQuote");
			DBRow result = new DBRow();
		 	result.add("result", "success");
		 	result.add("oid", 0);
			throw new JsonException(new JsonObject(result));
	}

}
