package com.cwc.service.bill.action;

import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.reportcenter.ReportCenter;
import com.cwc.util.StringUtil;

// 导出为PDF
public class ExportPDFBillAction extends ActionFatherController {

	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		long billId = StringUtil.getLong(request, "bill_id");
		DBRow result = billMgrZr.getPDFDatas(billId);
		
		String reporViewTemplatetFilePath = result.getString("view_path");
		DBRow dataRows[] = (DBRow[])result.get("items",new Object());
		HashMap<String, String> paras = (HashMap<String, String>)result.get("paras",new Object());

		ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows);
		JasperPrint printer = reportCenter.getPrinter();
 

		//pdf
		ServletOutputStream out = response.getOutputStream();
		byte[] pdfByte = JasperExportManager.exportReportToPdf(printer);
		response.setContentType("application/pdf");
	    response.setContentLength(pdfByte.length);
	    response.setHeader("Content-Disposition", "attachment; filename=\"VisionariInvoice"+String.valueOf(billId)+".pdf\"");
	    out.write(pdfByte, 0, pdfByte.length);
        out.flush();
        out.close();
		
	}

}
