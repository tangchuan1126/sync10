package com.cwc.service.bill.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class UpdateBillAction extends ActionFatherController {

	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		// 添加Bill 和BillItem
			DBRow billRow = new DBRow();
			List<DBRow> itemList = new ArrayList<DBRow>();
		
			// 读取billRow
		 
			String[] arrayAddress = new String[]{"address_name","address_country","address_city","address_zip","address_street","address_country_code","address_state","note","memo_note"};
			for(String name : arrayAddress){	
				billRow.add(name, StringUtil.getString(request, name));
			}
			// product_cost,
			billRow.add("subtotal", StringUtil.getDouble(request, "subtotal"));
			billRow.add("total_discount", StringUtil.getDouble(request, "total_discount"));
			billRow.add("shipping_fee", StringUtil.getDouble(request, "shipping_fee"));
			billRow.add("save", StringUtil.getDouble(request, "save"));
			billRow.add("rate", StringUtil.getDouble(request, "rate"));
			billRow.add("product_cost", StringUtil.getDouble(request, "total_cost"));
			billRow.add("rate_type", StringUtil.getString(request, "rate_type"));
			
			
			billRow.add("client_id", StringUtil.getString(request,"client_id"));
			billRow.add("tel", StringUtil.getString(request, "tel"));
			billRow.add("create_date", DateUtil.NowStr());
			//ccid,pro_id,ps_id
			billRow.add("ccid", StringUtil.getLong(request, "ccid"));
			billRow.add("pro_id", StringUtil.getLong(request, "pro_id"));
			billRow.add("ps_id", StringUtil.getLong(request, "ps_id"));
			billRow.add("sc_id", StringUtil.getLong(request, "sc_id"));
			billRow.add("bill_type", StringUtil.getInt(request,"bill_type"));
			billRow.add("total_weight", StringUtil.getFloat(request, "total_weight"));
			billRow.add("total_quantity",StringUtil.getFloat(request, "total_quantity"));
			billRow.add("account_payee",StringUtil.getLong(request,"account_payee"));
			billRow.add("weight_type", StringUtil.getString(request, "weight_type"));
			billRow.add("account", StringUtil.getString(request, "account"));
			billRow.add("account_name",StringUtil.getString(request,"account_name"));
			billRow.add("key_type", StringUtil.getInt(request, "key_type"));
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			
		 	long adid = adminLoggerBean.getAdid(); 
		 	billRow.add("create_adid",adid);
			//读取Items 
			int index = 1 ;
			String type = StringUtil.getString(request, "product_type_"+index);
			//pc_id,quantity,unit_price,gross_profit,weight,unit_name,name,product_type,amount,actual_price
			while(type != null && type.length() > 0){
				//product_type_1=1&pc_id_1=10188&weight_1=0.18&unit_price_1=25.0&unit_name_1=PAIR&name_1=BULB%2F9006%2FPINK&actual_price_1=21.45&quantity_1=5&amount_1=107.27&
				DBRow item = new DBRow();
				item.add("product_type", StringUtil.getInt(request,"product_type_"+index));
				item.add("pc_id", StringUtil.getLong(request,"pc_id_"+index));
				item.add("weight", StringUtil.getFloat(request,"weight_"+index));
				item.add("unit_price", StringUtil.getDouble(request,"unit_price_"+index));
				item.add("unit_name", StringUtil.getString(request,"unit_name_"+index));
				item.add("name", StringUtil.getString(request,"name_"+index));
				item.add("actual_price", StringUtil.getDouble(request,"actual_price_"+index));
				item.add("quantity", StringUtil.getFloat(request,"quantity_"+index));
				item.add("amount", StringUtil.getDouble(request,"amount_"+index));
				itemList.add(item);
				index++ ;
				type =  StringUtil.getString(request, "product_type_"+index);
			}
			billMgrZr.addBill(billRow, itemList);
			// 清除 session
			CartQuoteIFace cartQuote = (CartQuoteIFace)MvcUtil.getBeanFromContainer("cartQuote");
			cartQuote.clearCart(request.getSession());
			throw new JsonException("success");
	}

}
