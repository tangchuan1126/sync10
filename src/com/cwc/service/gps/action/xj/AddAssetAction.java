package com.cwc.service.gps.action.xj;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.xj.CarsCommandsMgrIFaceXJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


/**
 * 添加GPS设备
 * @author Administrator
 *
 */
public class AddAssetAction extends ActionFatherController {
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{

		String gps_tracker = StringUtil.getString(request,"gps_tracker");
		String gate_liscense_plate = StringUtil.getString(request,"gate_liscense_plate");
		long groupId = StringUtil.getLong(request,"groupId");
		String groupName = StringUtil.getString(request,"groupName");
		String assetName = StringUtil.getString(request,"assetName");
		String callnum = StringUtil.getString(request,"callnum");
		long id = checkInMgrZwb.addAsset(gps_tracker, gate_liscense_plate,groupId,groupName,assetName,callnum);
		DBRow result = new DBRow();
		result.add("id", id);
		throw new JsonException(new JsonObject(result));
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
	

}
