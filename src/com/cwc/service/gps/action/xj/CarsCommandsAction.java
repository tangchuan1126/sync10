package com.cwc.service.gps.action.xj;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.xj.CarsCommandsMgrIFaceXJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


/**
 * 车辆命令
 * @author Administrator
 *
 */
public class CarsCommandsAction extends ActionFatherController {
	
	private CarsCommandsMgrIFaceXJ carsCommandsMgrIFaceXJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{

		int option = StringUtil.getInt(request,"option");    
		String  result = null;	  
		switch(option){
		  case 1:  //车辆定位
			  String ids= StringUtil.getString(request, "ids");
			  result = carsCommandsMgrIFaceXJ.getLastPos(ids);
			 
			break;
		  case 4:  //历史轨迹
			  result = carsCommandsMgrIFaceXJ.queryHistory(request);
			 
			break;
		  
		}    
		   throw new JsonException(result);
	}
	public void setCarsCommandsMgrIFaceXJ(
			CarsCommandsMgrIFaceXJ carsCommandsMgrIFaceXJ) {
		this.carsCommandsMgrIFaceXJ = carsCommandsMgrIFaceXJ;
	}
	
	

}
