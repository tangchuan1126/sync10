package com.cwc.service.gps.action.tw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CheckPrinterNameIsExistAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		String name=StringUtil.getString(request, "name");
		long psId=StringUtil.getLong(request, "psId");
		DBRow printerCount=googleMapsMgrCc.checkPrinterNameIsExist(name, psId);
		DBRow dbRow=new DBRow();
		if(Integer.parseInt(printerCount.get(printerCount.getFieldNames().get(0)).toString())>0){
	    	  dbRow.add("exist", true);
	      }else{
	    	  dbRow.add("exist", false);
	      }
		throw new JsonException(new JsonObject(dbRow));

	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
