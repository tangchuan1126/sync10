package com.cwc.service.gps.action.tw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.cc.LocationAreaXmlImportMgrCc;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ZoneDockHaldeAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	private LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		String  position_id=StringUtil.getString(request, "position_id");
		String sdIds=StringUtil.getString(request, "sdIds");
		DBRow row=new DBRow();
		row.add("flag", true);
		if(position_id!=null && !position_id.equals("")){
				try {
					googleMapsMgrCc.deleteAreaDoorByAreaId(Long.parseLong(position_id));
					row=new DBRow();
					row.add("flag", true);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					row.add("flag", false);
					e.printStackTrace();
				}
			if((boolean)row.getValue("flag") && sdIds!=null && !sdIds.equals("")){
				String[] sdIds_=sdIds.split(",");
				for(int i=0;i<sdIds_.length;i++){
					row=new DBRow();
					try {
						locationAreaXmlImportMgrCc.ddStorageAreaDoor(Long.parseLong(position_id),Long.parseLong(sdIds_[i]));
						row.add("flag", true);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						row.add("flag", false);
						e.printStackTrace();
					}
				}
			}
		}
		throw new JsonException(new  JsonObject(row));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}
	

}
