package com.cwc.service.gps.action.tw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ZonePersonHaldeAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String adids=StringUtil.getString(request, "adids");
		int areaId=StringUtil.getInt(request, "areaId");
		String oldAdids=StringUtil.getString(request, "oldAdids");
		DBRow data = new DBRow();
		data.add("flag", true);
		if(oldAdids!=null && !oldAdids.equals("")){
			String[] oldAdidStr_s=oldAdids.split(",");
			for(int i=0;i<oldAdidStr_s.length;i++){
				data=new DBRow();
				long OldAdid=Long.parseLong(oldAdidStr_s[i]);
				try {
					googleMapsMgrCc.deletePersonAreaByAdid(OldAdid);
					data.add("flag", true);
				} catch (Exception e) {
					data.add("flag", false);
					e.printStackTrace();
				}
			}
		}
		if(adids!=null && !adids.equals("") && (boolean)data.getValue("flag")){
			String[] adidStr_s=adids.split(",");
			for(int i=0;i<adidStr_s.length;i++){
				data=new DBRow();
				long adid=Long.parseLong(adidStr_s[i]);
				try {
					googleMapsMgrCc.updatePersonAreaByAdid(adid, areaId);
					data.add("flag", true);
				} catch (Exception e) {
					data.add("flag", false);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		throw new JsonException(new JsonObject(data)); 

	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
