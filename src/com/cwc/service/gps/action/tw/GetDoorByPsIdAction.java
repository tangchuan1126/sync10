package com.cwc.service.gps.action.tw;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONObject;

public class GetDoorByPsIdAction extends ActionFatherController {
	private LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		String psId=StringUtil.getString(request, "psId");
		DBRow[] dbrow= locationAreaXmlImportMgrCc.getDoorByPsId(psId);
		DBRow row=new DBRow();
		row.add("data", dbrow);
		throw new JsonException(new JSONObject(row));
	}
	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}

}
