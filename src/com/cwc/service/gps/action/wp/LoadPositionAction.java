package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONException;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class LoadPositionAction extends ActionFatherController{
	
	private  LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		String  psId = StringUtil.getString(request,"ps_id");
		int type=StringUtil.getInt(request,"type");
		DBRow data=new DBRow();
		DBRow[] rows=null;
		switch (type){
		case WebcamPositionTypeKey.LOCATION:
			//rows= locationAreaXmlImportMgrCc.getLocationData(psId);
			DBRow[]	locationArea=locationAreaXmlImportMgrCc.getAreaByPsidAndType(psId, "1");
			data.add("locationArea", locationArea);
	     	break;
		case WebcamPositionTypeKey.AREA:
			rows=locationAreaXmlImportMgrCc.getAreaData(psId);
			break;
		case WebcamPositionTypeKey.STAGING:
			rows=locationAreaXmlImportMgrCc.getStagingData(psId);
			break;
		case WebcamPositionTypeKey.PARKING:
			rows=locationAreaXmlImportMgrCc.getParkingData(psId);
			break;
		case WebcamPositionTypeKey.DOCKS:
			rows=locationAreaXmlImportMgrCc.getDocksData(psId);
			break;
		case WebcamPositionTypeKey.WEBCAM:
			rows=locationAreaXmlImportMgrCc.getWebcamData(psId);
			break;
		case WebcamPositionTypeKey.PRINTER:
			rows=locationAreaXmlImportMgrCc.getPrinterData(psId);
			break;
		}
		data.add("rows", rows);
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(data));
		//throw new JsonException(new JsonObject(data));
		
	}

	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}
	

}
