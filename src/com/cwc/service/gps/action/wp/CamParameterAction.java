package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CamParameterAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	private AdminMgrIFace adminMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		DBRow result = new DBRow();
		  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		  long adgId = adminLoggerBean.getAdgid();
		  if(adgId!=10000){
			  result.add("flag", "authError");
			  throw new JsonException(new JsonObject(result));
		  }
		String pageType =StringUtil.getString(request,"pageType");
		long psId =StringUtil.getLong(request,"ps_id");
		String id = StringUtil.getString(request, "id");
		String ip = StringUtil.getString(request, "ip");
		String port = StringUtil.getString(request, "port");
		String username = StringUtil.getString(request, "username");
		String password = StringUtil.getString(request, "password");
		String x = StringUtil.getString(request, "x");
		String y = StringUtil.getString(request, "y");
		String inner_radius = StringUtil.getString(request, "inner_radius");
		String outer_radius = StringUtil.getString(request, "outer_radius");
		String s_degree = StringUtil.getString(request, "s_degree");
		String e_degree = StringUtil.getString(request, "e_degree");
		int ischanged =StringUtil.getInt(request,"ischanged");
		DBRow data = new DBRow();
		/*if(ischanged==1){*/
			String latlng =googleMapsMgrCc.convertCoordinateToLatlng(Long.toString(psId), x, y);	
			data.add("latlng", latlng);
		/*}*/
		data.add("ps_id", psId);
		data.add("ip", ip);
		data.add("id", id);
		data.add("port",port);
		data.add("user", username);
		data.add("password", password);
		data.add("inner_radius", inner_radius);
		data.add("outer_radius", outer_radius);
		data.add("x", x);
		data.add("y", y);
		data.add("s_degree", s_degree);
		data.add("e_degree", e_degree);
		try {
			if(pageType.equals("0")){
			long  id_= googleMapsMgrCc.addWebcam(data);
			result.add("id", id_);
			}else {
			googleMapsMgrCc.updateWebcamInfo(data);
			}
			result.add("latlng", latlng);
			result.add("flag", "true");
		} catch (Exception e) {
			e.printStackTrace();
			result.add("flag", "false");
		}
		throw new JsonException(new JsonObject(result));
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

}
