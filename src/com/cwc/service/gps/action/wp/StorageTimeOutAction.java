package com.cwc.service.gps.action.wp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.cc.GoogleMapsMgrCc;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class StorageTimeOutAction extends ActionFatherController{
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		long ps_id =StringUtil.getLong(request,"ps_id");
		long timeout_one =StringUtil.getLong(request,"timeout_one");
		long timeout_two =StringUtil.getLong(request,"timeout_two");
		long timeout_three =StringUtil.getLong(request,"timeout_three");
		String flag =StringUtil.getString(request,"flag");
		DBRow row =new DBRow();
		row.add("ps_id",ps_id);
		row.add("timeout_one",timeout_one);
		row.add("timeout_two",timeout_two);
		row.add("timeout_three",timeout_three);
		long returnId=0l;
		if(flag.equals("add")){
			returnId=googleMapsMgrCc.addStorageTimeOut(row);
		}else if(flag.equals("update")){
			returnId=googleMapsMgrCc.updateStorageTimeOutByPsId(row);
		}
		DBRow result =new DBRow();
		result.add("id",returnId);
		throw new JsonException(new JsonObject(result));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

	
}
