package com.cwc.service.gps.action.wp;

import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ConvertCoordinateAction extends ActionFatherController{
        
	
	private GoogleMapsMgrIfaceCc googleMapsMgrCc; 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			long psId =StringUtil.getLong(request,"psId");
			double lat =StringUtil.getDouble(request,"lat");
			double lng =StringUtil.getDouble(request,"lng");
			DBRow row =new DBRow();
			String coordinate;
			try {
				coordinate = googleMapsMgrCc.convertLatlngToCoordinate(psId, lat, lng);
				if(!coordinate.equals("")){
					String x =coordinate.split(",")[0];
					String y =coordinate.split(",")[1];
					DecimalFormat df = new DecimalFormat("0.00");
					row.add("flag", "true");
					row.add("x", df.format(Double.parseDouble(x)));
					row.add("y", df.format(Double.parseDouble(y)));
				}
			} catch (Exception e) {
				row.add("flag", "flase");
				e.printStackTrace();
			}
			throw new JsonException(new JsonObject(row));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	

}
