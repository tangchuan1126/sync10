package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ModifyLightAction extends ActionFatherController{
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	private AdminMgrIFace adminMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		  DBRow  result =new DBRow();
		  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		  long adgId = adminLoggerBean.getAdgid();
		  if(adgId!=10000){
			  result.add("flag", "authError");
			  throw new JsonException(new JsonObject(result));
		  }
		String name =StringUtil.getString(request,"name");
		String x =StringUtil.getString(request,"x");
		String y =StringUtil.getString(request,"y");
		String status =StringUtil.getString(request,"status");
		String ischanged =StringUtil.getString(request,"isChanged");
        long psId =StringUtil.getLong(request,"ps_id");
        long id =StringUtil.getLong(request,"id");
        DBRow  row =new DBRow();
        String latlng="";
        /*if(ischanged.equals("y")){*/
        	latlng=googleMapsMgrCc.convertCoordinateToLatlng(Long.toString(psId), x, y);
        	row.add("latlng", latlng);
       /* }*/
        row .add("name",name);
        row .add("status",status);
        row .add("x",x);
        row .add("y",y);
        row .add("id",id);
        row .add("ps_id",psId);
        try {
			if(id==0l){
				DBRow rows=googleMapsMgrCc.queryLightSingle(psId, name.trim());
			    if(rows==null){
				id= googleMapsMgrCc.addLight(row);
				result.add("id", id);
				result.add("flag","true");
			    }else{
			        result.add("flag","repeat");
			    }
			}else {
				googleMapsMgrCc.updateLight(row);
				result.add("flag","true");
			}
			result.add("latlng", latlng);
		} catch (Exception e) {
			result.add("flag","false");
			e.printStackTrace();
		}
        
        
     	throw new JsonException(new JsonObject(result));
        
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}


}
