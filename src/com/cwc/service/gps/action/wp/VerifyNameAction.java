package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class VerifyNameAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		int type = StringUtil.getInt(request,"type");
		long psId = StringUtil.getLong(request,"ps_id");		
		String name = StringUtil.getString(request,"name");		
		DBRow data =new DBRow();
		try {
			DBRow[] dbrow =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, name, type);
			if(dbrow.length>0){
				data.add("flag", "false");
			}else{
				data.add("flag", "true");
			}
		} catch (Exception e) {
			e.printStackTrace();
			data.add("flag", "false");
		}
		throw new JsonException(new JsonObject(data));
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}


}
