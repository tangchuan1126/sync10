package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.iface.wp.EditStorageInfoMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class ModifyLocationAction extends ActionFatherController{
	 public EditStorageInfoMgrIfaceWp editStorageInfoMgrWp;
	 private GoogleMapsMgrIfaceCc googleMapsMgrCc;
		private CreateKmlMgrIfaceWp createKmlMgrWp;
		private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String psId =StringUtil.getString(request,"ps_id");
		Long slc_id =StringUtil.getLong(request,"slc_id");
		String slc_position =StringUtil.getString(request,"slc_position");
		String x =StringUtil.getString(request,"x");
		String y =StringUtil.getString(request,"y");
		String angle =StringUtil.getString(request,"angle");
		String width =StringUtil.getString(request,"w");
		String isChanged =StringUtil.getString(request,"isChanged");
		String height =StringUtil.getString(request,"l");
		DBRow result =new DBRow();
		DBRow data =new DBRow();
		String 	cds="";
		try {
		if(isChanged.equals("y")){
				cds = googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y,
					width, height, angle);
				data.add("latlng", cds);
				data.add("x", x);
				data.add("y", y);
				data.add("angle", angle);
				data.add("width", width);
				data.add("height", height);
		}
				data.add("slc_position", slc_position);
			    googleMapsMgrCc.updateStorageLocation(slc_id, data);
				createKmlMgrWp.createKmlNew(psId);
				queryKmlInfoMgrWp.deleteInvalidKml(Environment.getHome() + "upload/kml");
				result.add("latlng", cds);
				result.add("flag","true");
	
		} catch (Exception e) {
				result.add("flag","false");
				e.printStackTrace();
		}
	   throw new JsonException(new JsonObject(result));
		
	}

	public void setEditStorageInfoMgrWp(
			EditStorageInfoMgrIfaceWp editStorageInfoMgrWp) {
		this.editStorageInfoMgrWp = editStorageInfoMgrWp;
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
}
