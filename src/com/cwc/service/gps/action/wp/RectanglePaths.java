package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class RectanglePaths extends ActionFatherController{
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String psId=StringUtil.getString(request, "psId");
		String x=StringUtil.getString(request, "x");
		String y=StringUtil.getString(request, "y");
		String xPosition=StringUtil.getString(request, "xPosition");
		String yPosition=StringUtil.getString(request, "yPosition");
		String angle=StringUtil.getString(request, "angle");
		DBRow data=new DBRow();
		try{
			String paths=googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y, xPosition, yPosition, angle);
			data.add("data", paths);
			data.add("flag", "true");
		} catch (Exception e) {
			data.add("flag", "false");
			e.printStackTrace();
		}
		throw new JsonException(new JsonObject(data));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

	

}
