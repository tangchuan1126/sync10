package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ShowContainerCountsAction extends ActionFatherController{
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long ic_id =StringUtil.getLong(request,"ic_id");
		long psId =StringUtil.getLong(request,"ps_id");
		DBRow data =new DBRow();
		try {
			DBRow[] rows =googleMapsMgrCc.queryContainerCounts(ic_id,psId);
			if(rows.length>0){
			data.add("rows", rows);
			data.add("flag", "true");
			}else{
			data.add("flag", "nodata");
			}
		} catch (Exception e) {
			data.add("flag", "false");
			e.printStackTrace();
		}
		throw new JsonException(new JsonObject(data));
		
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
