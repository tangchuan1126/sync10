package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CalculatePointToLineAction extends ActionFatherController {
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		double x1=StringUtil.getDouble(request,"x1");
		double y1=StringUtil.getDouble(request,"y1");
		double x2=StringUtil.getDouble(request,"x2");
		double y2=StringUtil.getDouble(request,"y2");
		double m=StringUtil.getDouble(request,"m");
		double n=StringUtil.getDouble(request,"n");
		DBRow data=new DBRow ();
		try {
			String point =googleMapsMgrCc.calculatePointtoLine(x1, y1, x2, y2, m, n);
			data.add("flag","true");
			data.add("data",point);
		} catch (Exception e) {
			data.add("flag","false");
			e.printStackTrace();
		}
		throw new JsonException(new JsonObject(data));
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
