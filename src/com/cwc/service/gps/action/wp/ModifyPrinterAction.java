package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ModifyPrinterAction extends ActionFatherController{
	
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	AdminMgrIFace adminMgr;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow data=new DBRow();
		  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		  long adgId = adminLoggerBean.getAdgid();
		  if(adgId!=10000){
			  data.add("flag", "authError");
			  throw new JsonException(new JsonObject(data));
		  }
	      long p_id =StringUtil.getLong(request,"p_id");
	      long psId =StringUtil.getLong(request,"psId");
	      long p_type =StringUtil.getLong(request,"p_type");
	     // String servers =StrUtil.getString(request,"servers").equals("-1")?"":StrUtil.getString(request,"servers");
	      String type =StringUtil.getString(request,"type");
	      String servers_name =StringUtil.getString(request,"servers_name");
	      String x =StringUtil.getString(request,"x");
	      String y =StringUtil.getString(request,"y");
	      String size =StringUtil.getString(request,"size");
	      String name =StringUtil.getString(request,"name");
	      String ischanged =StringUtil.getString(request,"ischanged");
		  String pageType =StringUtil.getString(request,"pageType");
		  String area_id =StringUtil.getString(request,"area_id");
		  DBRow row =new DBRow();
		  String latlng="";
		 /* if(ischanged.equals("1")){*/
		    	 latlng=  googleMapsMgrCc.convertCoordinateToLatlng(Long.toString(psId), x, y);
		    	 row.add("latlng",latlng);
		     /* }*/
		  if(p_type==0l){
			   String port =StringUtil.getString(request,"port");
			   String ip =StringUtil.getString(request,"ip");
			    row.add("ip",ip);
			    row.add("port",port);
			    row.add("servers","");
			    row.add("servers_name","");
		  }else if(p_type==1l){
			  String servers =StringUtil.getString(request,"servers");
			  row.add("servers",servers);
			  row.add("servers_name",servers_name);
			  row.add("ip","");
			  row.add("port","");
		  }
		      row.add("p_id",p_id);
		      row.add("ps_id",psId);
		      row.add("name",name);
		      row.add("type",type);
		      row.add("size",size);
		      row.add("x",x);
		      row.add("y",y);
		      row.add("p_type",p_type);
		      row.add("physical_area",area_id);
		    	  try {
			    	  long insertId=0l;
			    	  if(pageType.equals("0")||p_id==0l){
			    		  insertId=googleMapsMgrCc.addPrinter(row);
		    		    data.add("id",insertId);
			    	  }else{
			    		  int update_id=googleMapsMgrCc.updatePrinter(row);
			    		  //data.add("id",update_id);
			    	  }
					data.add("flag","true");
					data.add("latlng", latlng);
				} catch (Exception e) {
					e.printStackTrace();
					data.add("flag", "false");
				} 
		      
		  	throw new JsonException(new JsonObject(data));
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	

}
