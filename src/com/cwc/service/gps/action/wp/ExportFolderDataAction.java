package com.cwc.service.gps.action.wp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wp.ExportFolderDataMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ExportFolderDataAction extends ActionFatherController {

	private ExportFolderDataMgrIfaceWp exportFolderDataMgrWp;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String ps_id = StringUtil.getString(request, "ps_id");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String dateString = sdf.format(new Date());
		String path = Environment.getHome()+"export/";
		File file = new File(path);
		// 如果文件夹不存在则创建
		if (!file.exists() && !file.isDirectory()) {
			file.mkdir();
		} else {
			file.delete();
			file.mkdir();
		}
		String fileName =ps_id +"_"+dateString + ".xls";
		
		String realPath =path+fileName;
		boolean flag = exportFolderDataMgrWp.createExcelNew(ps_id, realPath);
		String fileurl ="../../"+"export/"+fileName;
		DBRow data = new DBRow();
		data.add("flag", flag);
		data.add("fileurl", fileurl);
		throw new JsonException(new JsonObject(data));
	}

	public void setExportFolderDataMgrWp(
			ExportFolderDataMgrIfaceWp exportFolderDataMgrWp) {
		this.exportFolderDataMgrWp = exportFolderDataMgrWp;
	}

}
