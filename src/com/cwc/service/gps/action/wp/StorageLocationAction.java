package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class StorageLocationAction extends ActionFatherController {
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgr;
    private ProductStoreMgrIFaceZJ  productStoreMgr;
    private GoogleMapsMgrIfaceCc googleMapsMgrCc;
    
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		   	String position_all=StringUtil.getString(request,"position_all","--");
		   	String ps_id =StringUtil.getString(request,"ps_id","0");
		   	String title =StringUtil.getString(request,"title", "");
		   	String lot_num =StringUtil.getString(request,"lot_num","");
		   	long   line=Long.parseLong(StringUtil.getString(request,"line","0")) ;
		   	long slc_id =queryKmlInfoMgr.getPositionId(Long.parseLong(ps_id), position_all, WebcamPositionTypeKey.LOCATION);
		   	//DBRow[] webcam = googleMapsMgrCc.getWebcamByPosition(slc_id, WebcamPositionTypeKey.LOCATION);
		   	DBRow dbrow =new DBRow() ;
		   	//dbrow.add("webcam", webcam);
		   	//  本地没有连接查询商品信息的库 报错特此修改
			/*DBRow[] result = productStoreMgr.getProductStoreCount(ps_id, 0, slc_id, title, 0, lot_num, "", line);
			dbrow.add("result",result);
			if (result.length==0){
				dbrow.add("flag", "false");
				}else{
					dbrow.add("flag","true");
				}
				*/
		   	dbrow.add("flag","true");
				throw new JsonException(new JsonObject(dbrow));
		  
	}

	public void setQueryKmlInfoMgr(QueryKmlInfoMgrIfaceWp queryKmlInfoMgr) {
		this.queryKmlInfoMgr = queryKmlInfoMgr;
	}

	public void setProductStoreMgr(ProductStoreMgrIFaceZJ productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

}
