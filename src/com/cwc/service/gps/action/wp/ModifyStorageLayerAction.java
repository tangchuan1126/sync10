package com.cwc.service.gps.action.wp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wp.EditStorageInfoMgrIfaceWp;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ModifyStorageLayerAction extends ActionFatherController{
	private EditStorageInfoMgrIfaceWp editStorageInfoMgrWp;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		//注释提交
		DBRow positionId;
		DBRow data = new DBRow();
		try {
			positionId = editStorageInfoMgrWp.saveStorageLayer(request);
	       long  result =positionId.get("returnValue",0l);
			if(result!=0&&result!=-1&&result!=-2){
			data.add("flag", "true");
			data.add("position_id", result);
			data.add("repeat", "false");
			}else if(result==0){
			data.add("flag", "false");
			data.add("repeat", "true");
			}else if(result==-2){
			data.add("flag", "authError");	
			}
		} catch (Exception e) {
			if(e.getMessage().equals("-1")){
				data.add("repeat", "AotuTrue");
			}else{
				e.printStackTrace();
				
			}
			data.add("flag", "false");
		}
		throw new JsonException(new JsonObject(data));

	}
	public void setEditStorageInfoMgrWp(
			EditStorageInfoMgrIfaceWp editStorageInfoMgrWp) {
		this.editStorageInfoMgrWp = editStorageInfoMgrWp;
	}
}
