package com.cwc.service.pay.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetOrderAndInvoiceInfoAction extends ActionFatherController{

	
	private BillMgrIfaceZR billMgrZr;
	
	public void setBillMgrZr(BillMgrIfaceZR billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 
			DBRow result = null;
		 
			 long billId = StringUtil.getLong(request, "bill_id");
			 String clientId = StringUtil.getString(request, "client_id");
			 result = billMgrZr.getBillItemByClientIdAndBillId(billId, clientId);
			 if(result == null){
				 result = new DBRow();
				 result.add("message", "Please Check The Invoice Number And Email"); 
			 }
			 result.add("flag", "success");
			 
			if(result == null){
				result = new DBRow();
				result.add("flag", "error");
				result.add("message", "System Error, Plase Try Later");
			}
			throw new JsonException(new JsonObject(result));
		
			
		
	}

}
