package com.cwc.service.qa.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.qa.ModQAQuestionNotQuestionerException;
import com.cwc.app.iface.zj.QAMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.util.StringUtil;

public class ModQuestionAction extends ActionFatherController {
	
	private QAMgrIFaceZJ qaMgrZJ;
	private MessageAlerter messageAlerter;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			int modType = StringUtil.getInt(request,"modType");
			if(modType ==1)
			{
				qaMgrZJ.modQuestionHasAnswer(request);
			}
			else
			{
				qaMgrZJ.modQuestionById(request);
			}
		}
		catch (ModQAQuestionNotQuestionerException e) 
		{
			messageAlerter.setMessage(request,Resource.getStringValue("","ModQAQuestionNotQuestionerException",""));
		}
		throw new RedirectBackUrlException();
	}
	public void setQaMgrZJ(QAMgrIFaceZJ qaMgrZJ) {
		this.qaMgrZJ = qaMgrZJ;
	}
	public void setMessageAlerter(MessageAlerter messageAlerter) {
		this.messageAlerter = messageAlerter;
	}

}
