package com.cwc.service.qa.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.qa.ProductNotFindException;
import com.cwc.app.iface.zj.QAMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.util.StringUtil;

public class AddQuestionAction extends ActionFatherController {

	private QAMgrIFaceZJ qaMgrZJ;
	private MessageAlerter messageAlert;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,ProductNotFindException,Exception 
	{
			try 
			{
				qaMgrZJ.addQuestion(request);
			}
			catch (ProductNotFindException e) 
			{
				messageAlert.setMessage(request,Resource.getStringValue("","ProductNotFindException",""));
			}
			
			if(!StringUtil.getString(request,"backurl").equals(""))
			{
				throw new RedirectBackUrlException();
			}
			else
			{
				throw new RedirectRefException();
			}
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	public void setQaMgrZJ(QAMgrIFaceZJ qaMgrZJ) {
		this.qaMgrZJ = qaMgrZJ;
	}

}
