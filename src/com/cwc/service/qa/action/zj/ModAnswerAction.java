package com.cwc.service.qa.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.QAMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ModAnswerAction extends ActionFatherController {
	
	private QAMgrIFaceZJ qaMgrZJ;
	private MessageAlerter messageAlerter;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		qaMgrZJ.modAnswer(request);
		
		throw new RedirectBackUrlException();
	}
	public void setQaMgrZJ(QAMgrIFaceZJ qaMgrZJ) {
		this.qaMgrZJ = qaMgrZJ;
	}
	public void setMessageAlerter(MessageAlerter messageAlerter) {
		this.messageAlerter = messageAlerter;
	}

}
