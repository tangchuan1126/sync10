package com.cwc.service.supplier.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.supplier.SupplierLoginTimeOutException;
import com.cwc.app.iface.zj.SupplierMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class ModSupplierPwdAction extends ActionFatherController {

	private SupplierMgrIFaceZJ supplierMgrZJ;
	private MessageAlerter messageAlert;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException,SupplierLoginTimeOutException,OldPwdIncorrectException,Exception 
	{
		try 
		{
			supplierMgrZJ.modSupplierPwd(request);
			throw new RedirectBackUrlException();
		}
		catch(SupplierLoginTimeOutException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","SupplierLoginTimeOutException",""));
		}
		catch(OldPwdIncorrectException e)
		{
			messageAlert.setMessage(request,Resource.getStringValue("","OldPwdIncorrectException",""));
		}
		
		throw new RedirectRefException();
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}
	public void setSupplierMgrZJ(SupplierMgrIFaceZJ supplierMgrZJ) {
		this.supplierMgrZJ = supplierMgrZJ;
	}

}
