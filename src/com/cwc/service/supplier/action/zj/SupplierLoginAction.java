package com.cwc.service.supplier.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.SupplierMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
import com.cwc.verifycode.LoginLicence;

public class SupplierLoginAction extends ActionFatherController {
	
	private AdminMgrIFace adminMgr;
	private String verderaccount;
	private String verderpassword;
	private SupplierMgrIFaceZJ supplierMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
//			String licence = StrUtil.getString(request,"licence");
//			if(!licence.equals(LoginLicence.getTheLicence(request)))
//			{
//				throw new VerifyCodeIncorrectException();
//			}
			
			DBRow supplier = supplierMgrZJ.supplierLogin(request);
			
			int remember = StringUtil.getInt(request,"remember");
			int loginType = 1;
			
			if(supplier==null)
			{
				throw new WriteOutResponseException("1");
			}
			else
			{
				adminMgr.adminLogin(request,response,verderaccount,verderpassword, null, remember, loginType,supplier);
			}
		}
		catch(AccountNotPermitLoginException e)
		{
			throw new WriteOutResponseException("1");
		}
		catch (VerifyCodeIncorrectException e)
		{
			throw new WriteOutResponseException("2");
		}
		throw new WriteOutResponseException("0");
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setVerderaccount(String verderaccount) {
		this.verderaccount = verderaccount;
	}

	public void setVerderpassword(String verderpassword) {
		this.verderpassword = verderpassword;
	}

	public void setSupplierMgrZJ(SupplierMgrIFaceZJ supplierMgrZJ) {
		this.supplierMgrZJ = supplierMgrZJ;
	}

}
