package com.cwc.service.supplier.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSupplierInfoAjaxAction extends ActionFatherController{

	private SupplierMgrIFaceTJH SupplierMgrTJH;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long supplierId	= StringUtil.getLong(request, "supplier_id");
		DBRow result = SupplierMgrTJH.getDetailSupplier(supplierId);
		throw new JsonException(new JsonObject(result));
	}

	public void setSupplierMgrTJH(SupplierMgrIFaceTJH supplierMgrTJH) {
		SupplierMgrTJH = supplierMgrTJH;
	}
	
	

}
