package com.cwc.service.supplier.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gzy.SupplierIFaceGZY;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ModSupplierActionGZY extends ActionFatherController{
	private SupplierIFaceGZY supplier;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException, Exception {
		
		supplier.modSupplier(request);
		DBRow result = new DBRow();
		result.add("flag", true);
		throw new JsonException(new JsonObject(result));
//		throw new RedirectRefException();
	}

	public void setSupplier(SupplierIFaceGZY supplier) {
		this.supplier = supplier;
	}
}
