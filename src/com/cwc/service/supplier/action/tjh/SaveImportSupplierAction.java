package com.cwc.service.supplier.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.supplier.HaveUploadSupplierException;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class SaveImportSupplierAction extends ActionFatherController {

	private SupplierMgrIFaceTJH supplierMgr;
	private MessageAlerter messageAlert;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			supplierMgr.uploadQualification(request);
		} 
		catch (HaveUploadSupplierException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","UploadSupplierException",""));
		}
		throw new RedirectRefException();

	}
	
	
	public void setSupplierMgr(SupplierMgrIFaceTJH supplierMgr) {
		this.supplierMgr = supplierMgr;
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

	
}
