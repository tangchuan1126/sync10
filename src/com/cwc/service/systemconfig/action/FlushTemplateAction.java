package com.cwc.service.systemconfig.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 刷新模板
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FlushTemplateAction extends ActionFatherController 
{
	private SystemConfigIFace systemConfig;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		systemConfig.flushTemplate();

		throw new RedirectRefException();
	}

	public void setSystemConfig(SystemConfigIFace systemConfig)
	{
		this.systemConfig = systemConfig;
	}

}
