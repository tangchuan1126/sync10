package com.cwc.service.storageCatalog.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ExportStorageAddressAction extends ActionFatherController{

	private StorageCatalogMgrZyjIFace storageCatalogMgrZyjIFace;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		String path = storageCatalogMgrZyjIFace.exportStorageAddressAction(request);
		
		DBRow result = new DBRow();
		if(!path.equals("0")){
			result.add("flag", true);
		}else{
			result.add("flag", false);
		}
		result.add("fileurl", path);
		
		throw new JsonException(new JsonObject(result));
		
	}


	public void setStorageCatalogMgrZyjIFace(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyjIFace) {
		this.storageCatalogMgrZyjIFace = storageCatalogMgrZyjIFace;
	}

	

	
}
