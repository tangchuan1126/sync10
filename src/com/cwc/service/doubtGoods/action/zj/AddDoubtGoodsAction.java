package com.cwc.service.doubtGoods.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DoubtGoodsMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class AddDoubtGoodsAction extends ActionFatherController {

	private DoubtGoodsMgrIFaceZJ doubtGoodsMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long dg_id = doubtGoodsMgrZJ.addDoubtProduct(request);
		
		//String result="<script type=\"text/javascript\">window.parent.ssMenu("+dg_id+",0);window.parent.location.reload();</script>";
		String result="<script type=\"text/javascript\">window.parent.afterAddPrint("+dg_id+",0);</script>";
		
		throw new WriteOutResponseException(result);
	}
	public void setDoubtGoodsMgrZJ(DoubtGoodsMgrIFaceZJ doubtGoodsMgrZJ) {
		this.doubtGoodsMgrZJ = doubtGoodsMgrZJ;
	}

}
