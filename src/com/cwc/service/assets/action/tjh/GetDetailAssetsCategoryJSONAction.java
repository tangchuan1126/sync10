package com.cwc.service.assets.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.AssetsCategoryMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 根据id查询资产类别的详细信息
 * @author Administrator
 *
 */
public class GetDetailAssetsCategoryJSONAction extends ActionFatherController {

	private AssetsCategoryMgrIFace assetsCategoryMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception 
	{
		long id = StringUtil.getLong(request,"id");
		DBRow category = assetsCategoryMgr.getDetailAssetsCategory(id);
		if (category == null) {
			category = new DBRow();
			category.add("chName", "/");
			category.add("enName",null);
		}
//		JsonObject object = new JsonObject(category);
//		//system.out.println(object);
		throw new JsonException(new JsonObject(category));
	}
	public void setAssetsCategoryMgr(AssetsCategoryMgrIFace assetsCategoryMgr) {
		this.assetsCategoryMgr = assetsCategoryMgr;
	}

}
