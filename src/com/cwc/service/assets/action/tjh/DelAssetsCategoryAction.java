package com.cwc.service.assets.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.AssetsCategoryMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

/**
 * 删除资产类别信息
 * @author Administrator
 *
 */
public class DelAssetsCategoryAction extends ActionFatherController {

	private AssetsCategoryMgrIFace assetsCategory;
	private MessageAlerter messageAlert;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception 
	{
		try 
		{
			assetsCategory.delAssetsCategory(request);
		} 
		catch (Exception e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","DelAssetsCategoryException","")); 
		}
		throw new RedirectRefException();

	}
	public void setAssetsCategory(AssetsCategoryMgrIFace assetsCategory) {
		this.assetsCategory = assetsCategory;
	}
	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

	
}
