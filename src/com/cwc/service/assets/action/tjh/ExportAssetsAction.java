package com.cwc.service.assets.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.AssetsMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ExportAssetsAction extends ActionFatherController {

	private AssetsMgrIFace assetsMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String path = assetsMgr.exportAssets(request);
		DBRow row = new DBRow();
		if(!path.equals("0"))
		{
			row.add("canexport",true);
		}
		else
		{
			row.add("canexport", false);
		}
		row.add("fileurl",path);
		
		throw new JsonException(new JsonObject(row));

	}
	public void setAssetsMgr(AssetsMgrIFace assetsMgr) {
		this.assetsMgr = assetsMgr;
	}
	
	

}
