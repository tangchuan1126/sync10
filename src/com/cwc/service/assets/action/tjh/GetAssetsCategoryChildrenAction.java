package com.cwc.service.assets.action.tjh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.tjh.AssetsCategoryMgrIFace;
import com.cwc.db.*;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetAssetsCategoryChildrenAction extends ActionFatherController {

	private AssetsCategoryMgrIFace assetsCategoryMgr;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception 
	{
		long parentId = StringUtil.getLong(request,"parentId");
		DBRow row [] = assetsCategoryMgr.getAssetsCategoryChildren(parentId);

	}
	public void setAssetsCategoryMgr(AssetsCategoryMgrIFace assetsCategoryMgr) {
		this.assetsCategoryMgr = assetsCategoryMgr;
	}

	
}
