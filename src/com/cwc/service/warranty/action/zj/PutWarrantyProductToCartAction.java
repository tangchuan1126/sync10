package com.cwc.service.warranty.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.CartReturnProductReasonIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class PutWarrantyProductToCartAction extends ActionFatherController {
	
	private CartReturnProductReasonIFace cartReturnProductReason;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		String type = StringUtil.getString(request,"type");
		if(type.equals("add"))
		{
			cartReturnProductReason.putToReturnProductReasonCart(request);
		}
		else if(type.equals("select"))
		{
			cartReturnProductReason.putToReturnProductReasonCartForSelect(request);
		}
		
		throw new WriteOutResponseException("ok");
	}

	public void setCartReturnProductReason(
			CartReturnProductReasonIFace cartReturnProductReason) {
		this.cartReturnProductReason = cartReturnProductReason;
	}

}
