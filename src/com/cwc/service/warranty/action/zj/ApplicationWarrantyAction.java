package com.cwc.service.warranty.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductReturnOrderMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ApplicationWarrantyAction extends ActionFatherController {

	private ProductReturnOrderMgrIFaceZJ productReturnOrderMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		long rp_id = productReturnOrderMgrZJ.applicationWarranty(request);
		
		throw new WriteOutResponseException(String.valueOf(rp_id));
	}

	public void setProductReturnOrderMgrZJ(
			ProductReturnOrderMgrIFaceZJ productReturnOrderMgrZJ) {
		this.productReturnOrderMgrZJ = productReturnOrderMgrZJ;
	}

}
