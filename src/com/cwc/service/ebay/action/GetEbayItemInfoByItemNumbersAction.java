package com.cwc.service.ebay.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetEbayItemInfoByItemNumbersAction extends ActionFatherController{
	
	private EbayMgrZrIFace ebaMgrZr;
	
	
	public void setEbaMgrZr(EbayMgrZrIFace ebaMgrZr) {
		this.ebaMgrZr = ebaMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
			// 返回一个文件的地址
			String showFileName = StringUtil.getString(request, "file_name");
			String timeString = StringUtil.getString(request,"time");
			String filePath = Environment.getHome()+"upl_excel_tmp/"+showFileName;
			String loginSellerId = StringUtil.getString(request, "login_on_seller_id");
			String outPutFile =  new StringBuffer(Environment.getHome()+"upl_excel_tmp/").append("moreInfo_").append(timeString).append(".xls").toString();
			ebaMgrZr.getMoreInfoByXML(filePath, timeString + " More Info ", outPutFile,loginSellerId);
			throw new JsonException("moreInfo_"+timeString+".xls");
			
	}

}
