package com.cwc.service.ebay.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetEbayItemInfoBySingleItemAction extends ActionFatherController{
	
	private EbayMgrZrIFace ebaMgrZr;
	
	
	public void setEbaMgrZr(EbayMgrZrIFace ebaMgrZr) {
		this.ebaMgrZr = ebaMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
			// 返回一个文件的地址
		 	String itemNumber = StringUtil.getString(request, "item_number");
		 	String[] array = new String[1];
		 	array[0] = itemNumber;
		 	
		 	String timeString =  new Date().getTime()+"";
 
			String loginSellerId = StringUtil.getString(request, "login_on_seller_id");
			String outPutFile =  new StringBuffer(Environment.getHome()+"upl_excel_tmp/").append("moreInfo_").append(timeString).append(".xls").toString();
			DBRow row = ebaMgrZr.getEbayTokenBySellerId(loginSellerId);
			ebaMgrZr.getMoreInfoByItemNumbers(array, outPutFile, itemNumber + "  More Info ", row.getString("user_token"));
			throw new JsonException("moreInfo_"+timeString+".xls");
			
	}

}
