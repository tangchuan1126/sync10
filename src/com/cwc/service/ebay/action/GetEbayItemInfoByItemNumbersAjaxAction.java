package com.cwc.service.ebay.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetEbayItemInfoByItemNumbersAjaxAction extends ActionFatherController{
	
	private EbayMgrZrIFace ebaMgrZr;
	
	
	public void setEbaMgrZr(EbayMgrZrIFace ebaMgrZr) {
		this.ebaMgrZr = ebaMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 	
			 /**
			  * 读取一个ItemNumber的Type然后放到一个Session当中。
			  * 当读取到这个是否是结束的时候就把这个session中的值清除。然后返回页面一个XLS文件
			  */
			
			
	}

}
