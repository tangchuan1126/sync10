package com.cwc.service.ebay.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.zr.OrderMgrZr;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.jms.action.UpdateTradeItemSkuFvfMpnJMS;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class UpdateMpnAction   extends ActionFatherController {
	
	private EbayMgrZrIFace ebayMgr ;
	
	private OrderMgrIFace orderMgr ;
	
	private OrderMgrIfaceZR orderMgrZr ;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 		
		long oid = StringUtil.getLong(request, "oid");
		DBRow detailOrder = null;
		
			try
			{
				 
   				detailOrder = orderMgr.getDetailPOrderByOid(oid);
				
				if(detailOrder!= null && detailOrder.getString("order_source").equals(OrderMgrZr.ORDER_SOURCE_EBAY)){
					//然后查询COI表把itemnumber 都查询出来
					
					DBRow[] rows = orderMgrZr.getTradeItemByOid(oid);
					if(rows != null && rows.length > 0){
						
						for(DBRow row : rows)
						{
							String itemNumber = row.getString("item_number");
							String ebay_txn_id = row.getString("ebay_txn_id");
							if((itemNumber!=null&&itemNumber.length()>0)&&(ebay_txn_id.length()>0))
							{
								Map<String,String> map =  ebayMgr.getSellerId(itemNumber,row.get("trade_item_id",0l));
								
								String sellerId = map.get("sellerId");
								orderMgr.modSellerId(oid, sellerId);
								DBRow temp = new DBRow();
								String mpn = map.get("MPN");
								temp.add("seller_id", sellerId);
								temp.add("mpn", mpn);
								ebayMgr.updateTradeItem(row.get("trade_item_id", 0), temp);
								try 
								{
									com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
									jmsMgr.excuteAction(new UpdateTradeItemSkuFvfMpnJMS(oid));
									//ebayMgr.updateTransactionsFeeSKU(sellerId, row.getString("item_number"), row.getString("ebay_txn_id") , row.get("trade_item_id",0l),mpn);
								} 
								catch (Exception e) 
								{
									
								}
								
							}
						}
					}
				}
			}
			catch (Exception e)
			{
	 			throw e;
			}
			
	
	}


	public void setEbayMgr(EbayMgrZrIFace ebayMgr) {
		this.ebayMgr = ebayMgr;
	}


	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}


	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr) {
		this.orderMgrZr = orderMgrZr;
	}
	

}
