package com.cwc.service.ebay.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.EbayMgrZr;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.key.EbaySite;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class FindItemsAdvancedAction extends ActionFatherController{
	
	private EbayMgrZrIFace ebaMgrZr;
	
	
	public void setEbaMgrZr(EbayMgrZrIFace ebaMgrZr) {
		this.ebaMgrZr = ebaMgrZr;
	}

	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
			String keywords = StringUtil.getString(request, "keywords");
			String sellerId = StringUtil.getString(request, "seller_id");
			String loginOnSellerId = StringUtil.getString(request, "login_on_seller_id");
			int endCount = StringUtil.getInt(request, "end");
			int startCount = StringUtil.getInt(request, "start");
			String ebaySite = StringUtil.getString(request,"site");
			String siteString = new EbaySite().getEbaySiteById(Integer.valueOf(ebaySite)).split(",")[0];
			EbayMgrZr.baseFilePath =  Environment.getHome().replace("\\", "/")+"."+"/upl_excel_tmp/";
			String fileName = "moreinfo_"+new Date().getTime()+".xls";
//			DBRow row =	ebaMgrZr.findItemsAdvanced(sellerId,java.net.URLEncoder.encode(keywords),startCount, endCount, siteString, fileName,loginOnSellerId);
			DBRow row =	ebaMgrZr.findItemsAdvanced(sellerId,java.net.URLEncoder.encode(keywords,"UTF-8"),startCount, endCount, siteString, fileName,loginOnSellerId);
			throw new JsonException(new JsonObject(row));
	}

}
