package com.cwc.service.ebay.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.ebay.EbayPoiIfaceZr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetEbayItemNumbersByXlsAction  extends ActionFatherController {

	private EbayPoiIfaceZr ebayPoiMgrZr;
	public void setEbayPoiMgrZr(EbayPoiIfaceZr ebayPoiMgrZr) {
		this.ebayPoiMgrZr = ebayPoiMgrZr;
	}



	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			/**
		 	 * 1.读取这个XML文件的itemNumber 
		 	 * 2.返回到前台
		 	 * 3.前台在不断的Ajax请求。读取数据,把这些数据先放在Session当中。这样可以不用重复的打开读取xls文件
		 	 */
		String showFileName = StringUtil.getString(request, "file_name");
	 
		String filePath = Environment.getHome()+"upl_excel_tmp/"+showFileName;
		String[] array = ebayPoiMgrZr.readXLSItemNumbers(filePath);
		DBRow result = new DBRow();
		result.add("array", array);
		throw new JsonException(new JsonObject(result));
	}
}
