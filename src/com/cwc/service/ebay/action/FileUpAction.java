package com.cwc.service.ebay.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.CommonFileMgrIfaceZr;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class FileUpAction extends ActionFatherController {
	
	private CommonFileMgrIfaceZr commonFileMgrZr;
	
	public void setCommonFileMgrZr(CommonFileMgrIfaceZr commonFileMgrZr) {
		this.commonFileMgrZr = commonFileMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			String filePath =  Environment.getHome()+"upl_excel_tmp/";
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);  
			List<String> ids = new ArrayList<String>();
			long id = 0 ;
			if (isMultipart) {  
				AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
				long userId = adminLoggerBean.getAdgid();
			    DiskFileItemFactory factory = new DiskFileItemFactory(1024 * 4, new File(filePath));  
			    ServletFileUpload upload = new ServletFileUpload(factory);  
			    upload.setHeaderEncoding("UTF-8");  
			      
			    List<FileItem> fileItems = upload.parseRequest(request);  
			  
			    
			    // 依次处理请求  
			    Iterator<FileItem> iter = fileItems.iterator();  
			    while (iter.hasNext()) {  
			        FileItem item = (FileItem) iter.next(); 
			     
			        if (!item.isFormField()) {  
			            String fileName = item.getName();  
			            if (fileName != null) {  
			                // 如果文件存在则上传  3145728,8632275
			            	
			            	 File fullFile = new File(item.getName()); 
			                if (!fullFile.exists()) {  
			                	long size = item.getSize();
				            	// 这个是页面 input  name 的值  String fullFileName = item.getFieldName();
			                	String fullfilePath = filePath + fullFile.getName();
			                	DBRow fileRow = new DBRow();
			                	int index = fullFile.getName().indexOf(".");
			                	fileRow.add("file_type", fullFile.getName().substring(index+1));
			                	fileRow.add("file_path", fullfilePath);
			                	fileRow.add("file_name", fullFile.getName());
			                	fileRow.add("file_size", size);
			                	fileRow.add("file_creater", userId);
			                	fileRow.add("file_create_time", new TDate().getCurrentTime().replaceAll("\"", ""));
				             
				            	
				            	id = commonFileMgrZr.addCommonFile(fileRow);
				            	ids.add(id+"");
			                    File fileOnServer = new File(fullfilePath);  
			                    item.write(fileOnServer); 
			                }  
			            }  
			        }  
			    }  
			}  
			String returnUrl = request.getContextPath()+"/administrator/ebay/fileUp.html";
			if(ids.size() > 0 ){
				returnUrl += "?ids="+StringUtil.convertStringArrayToString(ids.toArray(new String[ids.size()]))+"&show=true";
			}
			response.sendRedirect(returnUrl);
		}

}
