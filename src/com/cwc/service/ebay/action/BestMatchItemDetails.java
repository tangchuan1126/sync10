package com.cwc.service.ebay.action;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.ebay.BestMatchItemDetailsMgrZr;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.iface.zr.ebay.BestMatchItemDetailIfaceZr;
import com.cwc.app.key.EbaySite;
 
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class BestMatchItemDetails extends ActionFatherController {
	
	
	
	private BestMatchItemDetailIfaceZr bestMatchItemDetailsMgrZr;
	
	 
  


 
 

	public void setBestMatchItemDetailsMgrZr(
			BestMatchItemDetailIfaceZr bestMatchItemDetailsMgrZr) {
		this.bestMatchItemDetailsMgrZr = bestMatchItemDetailsMgrZr;
	}
	





	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			 
			BestMatchItemDetailsMgrZr.filePath  =   Environment.getHome().replace("\\", "/")+"."+"/upl_excel_tmp"; 
			String sellerId = StringUtil.getString(request, "seller_id");
			String name = StringUtil.getString(request, "name");
			String keyWord = StringUtil.getString(request, "keywords");
			String searchSellerIds = StringUtil.getString(request, "search_seller");
			String ebaySite = StringUtil.getString(request,"site");
			String siteString = new EbaySite().getEbaySiteById(Integer.valueOf(ebaySite)).split(",")[0];
			int topNumber = StringUtil.getInt(request, "top");
			String filePath = bestMatchItemDetailsMgrZr.bestMatch(sellerId,name, keyWord, topNumber,searchSellerIds.split(","),siteString);
			DBRow result = new DBRow();
			 
			throw new JsonException(filePath);
	 
			
	}
	   

}
