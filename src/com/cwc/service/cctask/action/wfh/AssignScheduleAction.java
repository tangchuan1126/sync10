package com.cwc.service.cctask.action.wfh;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 
 * @ProjectName: [Sync10]
 * @Package: [com.cwc.service.cctask.action.wfh.AssignScheduleAction.java]
 * @ClassName: [AssignScheduleAction]
 * @Description: [推送任务]
 * @Author: [王飞虎]
 * @CreateDate: [2015年5月13日 上午9:42:40]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年5月13日 上午9:42:40]
 * @UpdateRemark: [整合到Sync10上]
 * @Version: [v1.0]
 * 
 */
public class AssignScheduleAction extends ActionFatherController {
	private CheckInMgrIfaceWfh checkInMgrWfh;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException, Exception {
		checkInMgrWfh.assignSchedule(request);
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}

}