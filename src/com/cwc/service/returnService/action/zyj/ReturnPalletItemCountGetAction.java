package com.cwc.service.returnService.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ReturnMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ReturnPalletItemCountGetAction extends ActionFatherController{

	private ReturnMgrIFaceZyj returnMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long pallet_id = StringUtil.getLong(request, "pallet_id");
		int count = returnMgrZyj.countPalletItemCountByPalletId(pallet_id);
		DBRow palletItemNum = new DBRow();
		palletItemNum.add("flag", count);
		throw new JsonException(new JsonObject(palletItemNum));
	}
	
	public void setReturnMgrZyj(ReturnMgrIFaceZyj returnMgrZyj) {
		this.returnMgrZyj = returnMgrZyj;
	}

}
