package com.cwc.service.refundlog.action.zj;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.RefundLogMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 申请退款
 * @author Administrator
 *
 */
public class ApplyForRefundAction extends ActionFatherController {

	private RefundLogMgrIFaceZJ refundLogMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		refundLogMgrZJ.applyForRefundLog(request);
		DBRow result = new DBRow();
		result.add("close",true);
		
		throw new JsonException(new JsonObject(result));
	}

	public void setRefundLogMgrZJ(RefundLogMgrIFaceZJ refundLogMgrZJ) {
		this.refundLogMgrZJ = refundLogMgrZJ;
	}

}
