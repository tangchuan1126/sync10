package com.cwc.service.returnProdcut.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ReturnProductMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CheckReturnProductAction extends ActionFatherController
{

	private ReturnProductMgrIfaceZr returnProductMgrZr; 
  
	
	public void setReturnProductMgrZr(ReturnProductMgrIfaceZr returnProductMgrZr)
	{
		this.returnProductMgrZr = returnProductMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			 
			DBRow row = returnProductMgrZr.checkReturnProductIsCreateByServices(request);
			DBRow result = new DBRow();
			if(row != null && row.get("rp_id", 0l) != 0l){
				result.add("flag", "success");
				result.add("rp_id",row.get("rp_id", 0l));
			}else{
				result.add("flag", "failed");
			}
			throw new JsonException(new JsonObject(result));
	}

}
