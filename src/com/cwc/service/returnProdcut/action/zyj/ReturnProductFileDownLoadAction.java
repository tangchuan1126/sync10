package com.cwc.service.returnProdcut.action.zyj;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.util.Environment;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class ReturnProductFileDownLoadAction extends ActionFatherController{

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		String showFileName = StringUtil.getString(request, "file_name");
		String folder = StringUtil.getString(request, "folder");
		String realPath = Environment.getHome()+ folder+"/"+showFileName;
		File file = new File(realPath);
		if(file.exists()){
			
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode(showFileName,"utf-8"));  
			ServletOutputStream outputStream = response.getOutputStream();  
			FileInputStream fis = new FileInputStream(file);  
			int len = 0;  
			byte[] b = new byte[1024];  
			while((len = fis.read(b))!= -1)
			{  
				outputStream.write(b, 0, len);  
			}  
			outputStream.close();
			fis.close();  
		}
		
		
	}

}
