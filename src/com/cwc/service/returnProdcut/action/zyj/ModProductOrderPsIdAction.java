package com.cwc.service.returnProdcut.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ModProductOrderPsIdAction extends ActionFatherController{

	private ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		DBRow result = new DBRow();
		returnProductOrderMgrZyj.modProductOrderPsId(request);
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
		
	}

	public void setReturnProductOrderMgrZyj(
			ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj) {
		this.returnProductOrderMgrZyj = returnProductOrderMgrZyj;
	}

}
