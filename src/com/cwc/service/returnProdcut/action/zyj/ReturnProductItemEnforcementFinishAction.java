package com.cwc.service.returnProdcut.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.ReturnProductMgrIfaceZr;
import com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductLogTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ReturnProductItemEnforcementFinishAction extends ActionFatherController{

	private ReturnProductMgrIfaceZr returnProductMgrZr;
	private ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj;
	private AdminMgrIFace adminMgr ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long rp_id	= StringUtil.getLong(request, "rp_id");
		int status	= StringUtil.getInt(request, "status");
		DBRow returnRow = new DBRow();
		returnRow.add("status", status);
		returnProductMgrZr.updateReturnProduct(returnRow, rp_id);
		//增加退货单日志
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		returnProductOrderMgrZyj.addReturnProductLog(rp_id,0,0,0, status, "退货单["+rp_id+"]"+new ReturnProductKey().getReturnProductStatusById(status), ReturnProductLogTypeKey.UPDATE, adminLoggerBean.getAdid(), DateUtil.NowStr());
		
		DBRow result = new DBRow();
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
		
	}

	public void setReturnProductMgrZr(ReturnProductMgrIfaceZr returnProductMgrZr) {
		this.returnProductMgrZr = returnProductMgrZr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setReturnProductOrderMgrZyj(
			ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj) {
		this.returnProductOrderMgrZyj = returnProductOrderMgrZyj;
	}


}
