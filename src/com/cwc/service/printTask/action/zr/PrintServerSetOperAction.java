package com.cwc.service.printTask.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.printTask.AndroidPrintServerNameExitsException;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class PrintServerSetOperAction extends ActionFatherController{
	
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow row = getDBRowFromRequest(request);
			DBRow result = new DBRow();
			try{
				result.add("ret", BCSKey.FAIL);
				if(row.getString("Method").equals("Add")){
					DBRow fixAddRow = new DBRow();
					fixAddRow.add("adid", row.get("adid", 0l));
					fixAddRow.add("printer_server_name", row.getString("printer_server_name"));
					fixAddRow.add("employe_name", row.getString("employe_name"));
					fixAddRow.add("ps_id", row.getString("ps_id"));
					fixAddRow.add("title", row.getString("title"));
					DBRow addRow=new DBRow();
					addRow.add("area_id", row.getString("area_id"));
					androidPrintMgrZr.addAndroidPrintServer(fixAddRow,addRow);
					result.add("ret", BCSKey.SUCCESS);
				}
				if(row.getString("Method").equals("Delete")){
					androidPrintMgrZr.deleteAndroidPrintServer(row.get("printer_server_id", 0l));
					result.add("ret", BCSKey.SUCCESS);
				}
			}catch (AndroidPrintServerNameExitsException e) {
				result.add("ret", BCSKey.FAIL);
				result.add("err", BCSKey.AndroidPrintServerNameExitsException);
 			}catch (Exception e) {
 				result.add("ret", BCSKey.FAIL);
				result.add("err", BCSKey.SYSTEMERROR);
 			}
 			throw new JsonException(new JsonObject(result));

	 }
	private DBRow getDBRowFromRequest(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow;
	}
	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
	
}
