package com.cwc.service.printTask.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.sun.star.uno.RuntimeException;

public class PrintTaskAction extends ActionFatherController {
	
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow row = getDBRowFromRequest(request);
			DBRow resultDBRow = new DBRow();
			try{
				if(row.getString("Method").equals("QueryWillPrintAndPrintFailed")){
					long print_server_id = StringUtil.getLong(request, "print_id");
					resultDBRow = androidPrintMgrZr.queryWillPrintAndPrintFailTask(print_server_id);
					resultDBRow.add("ret", BCSKey.SUCCESS);
 				}
			}catch (Exception e) {
				resultDBRow.add("ret", BCSKey.FAIL);
 			}
 			throw new JsonException(new JsonObject(resultDBRow));
	}
	
	private DBRow getDBRowFromRequest(HttpServletRequest request){
		DBRow returnRow = new DBRow();
		Enumeration<String>  enumer = request.getParameterNames();
		while(enumer != null && enumer.hasMoreElements()){
			String key = enumer.nextElement();
			returnRow.add(key, StringUtil.getString(request, key));
		}
		return returnRow;
	}
	
	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
	
	
}
