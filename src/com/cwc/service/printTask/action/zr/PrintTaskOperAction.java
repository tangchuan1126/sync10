package com.cwc.service.printTask.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.PrintTaskKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class PrintTaskOperAction extends ActionFatherController {
	
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			result.add("ret", BCSKey.FAIL);
			
			Thread.sleep(1000 * 2);
 			long printTaskId = StringUtil.getLong(request, "task_id");
 			String method = StringUtil.getString(request, "Method");
 			if(method.equalsIgnoreCase("cancel")){
 				androidPrintMgrZr.updatePrintTaskState(printTaskId, PrintTaskKey.Cancel);
 				result.add("ret", BCSKey.SUCCESS);
 			}
 			if(method.equalsIgnoreCase("print")){
 				int successFlag = StringUtil.getInt(request, "isSuccess");
 				long task_id = StringUtil.getLong(request, "task_id");
   				androidPrintMgrZr.updatePrintTaskState(task_id, successFlag == 1 ? PrintTaskKey.Success :PrintTaskKey.Failed );
 				result.add("ret", BCSKey.SUCCESS);
 			}
 			if(method.equalsIgnoreCase("reprint")){
  				long task_id = StringUtil.getLong(request, "task_id");
  				DBRow updateRow = new DBRow();
  				updateRow.add("state",  PrintTaskKey.ToPrint );
  				updateRow.add("date",  DateUtil.NowStr());
  				androidPrintMgrZr.updatePrintTask(task_id, updateRow);
 				result.add("ret", BCSKey.SUCCESS);
 			}
 			
  			throw new JsonException(new JsonObject(result));
 			
	}

	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
}
