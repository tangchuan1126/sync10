package com.cwc.service.returnStep.action.zj;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ReturnStepMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class ReturnStepSpiltUnion extends ActionFatherController {

	private ReturnStepMgrIFaceZJ returnStepMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,	ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		
		returnStepMgrZJ.spiltReturnStepUnion(request);
		throw new WriteOutResponseException("<script type=\"text/javascript\">parent.closeWin();</script>");
	}

	public void setReturnStepMgrZJ(ReturnStepMgrIFaceZJ returnStepMgrZJ) {
		this.returnStepMgrZJ = returnStepMgrZJ;
	}

}
