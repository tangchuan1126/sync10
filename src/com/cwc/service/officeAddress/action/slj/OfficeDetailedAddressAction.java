package com.cwc.service.officeAddress.action.slj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.tjh.OfficeLocationMgrTJH;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.tjh.FloorOfficeLocationMgrTJH;
import com.cwc.app.iface.tjh.OfficeLocationMgrIFaceTJH;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class OfficeDetailedAddressAction extends ActionFatherController {
	// @Autowired
	
//private OfficeLocationMgrTJH officeLocationMgrTJH;
	//private FloorOfficeLocationMgrTJH floorOfficeLocationMgrTJH;

	private OfficeLocationMgrIFaceTJH officeLocationMgr;



	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String str=request.getParameter("id"); 
		long	id=Long.parseLong(str); 
	//	long id= 100000;
	//	//system.out.println(id);
		DBRow result = this.officeLocationMgr.getDetailOfficeLocation(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject().put("items",DBRowUtils.multipleDBRowAsMap(result));
		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 更新
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		////system.out.println("jsonData=" + jsonData);
		
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		row.remove("name");
		row.remove("children");
		row.remove("pid");
	
		long adid = row.get("id", 0L);
		
		DBRow result = officeLocationMgr.updateOfficeaddressDetailed(row, adid);	
	//	//system.out.println((DBRowUtils.dbRowAsMap(result)).toString());
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
		}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	
	}

	@Override
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	

	}
	
	



	


	
	public OfficeLocationMgrIFaceTJH getOfficeLocationMgr() {
		return officeLocationMgr;
	}

	public void setOfficeLocationMgr(OfficeLocationMgrIFaceTJH officeLocationMgr) {
		this.officeLocationMgr = officeLocationMgr;
	}

}
