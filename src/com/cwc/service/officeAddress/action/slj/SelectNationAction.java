package com.cwc.service.officeAddress.action.slj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class SelectNationAction extends ActionFatherController {
	// @Autowired
	private OrderMgrIFace orderMgr ;

	public OrderMgrIFace getOrderMgr() {
		return orderMgr;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		DBRow[] result = this.orderMgr.getAllCountryCode();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject output = new JSONObject().put("items",
						DBRowUtils.dbRowArrayAsJSON(result));

		response.getWriter().print(output.toString());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
	}

	@Override
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	

	}

}
