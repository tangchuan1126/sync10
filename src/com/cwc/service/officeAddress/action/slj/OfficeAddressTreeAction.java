package com.cwc.service.officeAddress.action.slj;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.tjh.OfficeLocationMgrTJH;
import com.cwc.app.floor.api.tjh.FloorOfficeLocationMgrTJH;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.iface.tjh.OfficeLocationMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.DBRowUtils;

public class OfficeAddressTreeAction extends ActionFatherController {
	// @Autowired
//	private FloorOfficeLocationMgrTJH floorOfficeLocationMgrTJH;
//	private TransactionTemplate txTemplate;
//	private OfficeLocationMgrTJH officeLocationMgrTJH;
	private OfficeLocationMgrIFaceTJH officeLocationMgr;
	



	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		

		
		Map<String,DBRow[]> accountContext = officeLocationMgr.getAllOfficeLocationTree();
		JSONObject output = new JSONObject();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Set<String> key = accountContext.keySet();
        for (Iterator<String> it = key.iterator(); it.hasNext();) {
        	
            String oneKey = (String) it.next();
         //   //system.out.println(oneKey);
            output.put(oneKey,DBRowUtils.multipleDBRowArrayAsJSON(accountContext.get(oneKey)));
        }

        response.getWriter().print(output.toString());


	}

	
	

	private void setJson(JSONObject output) {
		// TODO Auto-generated method stub
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 新建
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow row = DBRowUtils.convertToDBRow(jsonData);	
		DBRow result =	officeLocationMgr.addOfficeAddress(row,request);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
		
		
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 更新
		JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		row.remove("name");
		row.remove("children");
		row.remove("pid");
		long adid = row.get("id", 0L);
		DBRow result =officeLocationMgr.updateOfficeaddress(row, adid);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
		
	}

	public void doDelete(HttpServletRequest request,HttpServletResponse response) throws Exception {
				
		DBRow result =	officeLocationMgr.delOfficeAddress(Long.parseLong(request.getParameter("id")));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject(DBRowUtils.dbRowAsMap(result)).toString());
	}

	@Override
	public void perform(final HttpServletRequest request,final HttpServletResponse response) throws Exception {
		try {
			switch (request.getMethod()) {
				case "GET":
					doGet(request, response);
					break;
				case "POST":
					doPost(request, response);
					break;
				case "PUT":
					doPut(request, response);
					break;
				case "DELETE":
					doDelete(request, response);
					break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	

	}





	public OfficeLocationMgrIFaceTJH getOfficeLocationMgr() {
		return officeLocationMgr;
	}




	public void setOfficeLocationMgr(OfficeLocationMgrIFaceTJH officeLocationMgr) {
		this.officeLocationMgr = officeLocationMgr;
	}






}
