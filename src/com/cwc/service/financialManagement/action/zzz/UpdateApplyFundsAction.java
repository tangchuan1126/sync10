package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateApplyFundsAction extends ActionFatherController {

	private ApplyMoneyMgrIFace applyMoneyMgr;
	
	public void setApplyMoneyMgr(ApplyMoneyMgrIFace applyMoneyMgr) {
		this.applyMoneyMgr = applyMoneyMgr;
	}

	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
			applyMoneyMgr.updateApplyMoney(request);
			//applyMoneyMgr.updateAssetsState(association_id,3);
			int add=StringUtil.getInt(request, "add");
			int flag=StringUtil.getInt(request, "flag");
			if(flag==1)
			{
				throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");
			}
			else if(add==1)
			{
				throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/assets_apply_funds.html?add=1");
			}else if(add == 0) {
				throw new RedirectRefException();
			}
	}

}
