package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddAssetsAction extends ActionFatherController {

	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) 
	{
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}
/**
 * 固定资产新增跳转到资金申请页面
 * @param arg0
 * @param arg1
 * @throws WriteOutResponseException
 * @throws RedirectRefException
 * @throws ForwardException
 * @throws Forward2JspException
 * @throws RedirectBackUrlException
 * @throws RedirectException
 * @throws OperationNotPermitException
 * @throws PageNotFoundException
 * @throws Exception
 */
	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
			String submit = StringUtil.getString(arg0,"cmd");
			if(submit.equals("noapply")){
				applyMoneyMgrZZZ.addAssets(arg0);
				throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");				
			}else{
			long assets_id=applyMoneyMgrZZZ.addAssets(arg0);
			String type = StringUtil.getString(arg0,"type");
			String currency = StringUtil.getString(arg0,"currency");
			String center_account_id = StringUtil.getString(arg0,"center_account_id");
			String product_line_id = StringUtil.getString(arg0,"product_line_id");
			String center_account_type_id = StringUtil.getString(arg0,"center_account_type_id");
			String center_account_type1_id = StringUtil.getString(arg0,"center_account_type1_id");
			String payee_type_id = StringUtil.getString(arg0,"payee_type_id");
			String payee_type1_id = StringUtil.getString(arg0,"payee_type1_id");
			String payee_id = StringUtil.getString(arg0,"payee_id");
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/assets_apply_funds.html?assets_id="+assets_id+"&type="+type+"&center_account_id="+center_account_id+"&product_line_id="+product_line_id+"&center_account_type_id="+center_account_type_id+"&center_account_type1_id="+center_account_type1_id+"&payee_type_id="+payee_type_id+"&payee_type1_id="+payee_type1_id+"&payee_id="+payee_id+"&currency="+currency);
			}
		}
}
