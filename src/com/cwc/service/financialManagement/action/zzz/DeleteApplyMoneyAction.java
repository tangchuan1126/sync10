package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class DeleteApplyMoneyAction extends ActionFatherController {

	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	
	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) {
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		
		applyMoneyMgrZZZ.deleteApplyMoneyByApplyId(request);
		String url = ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/apply_funds.html";
		throw new RedirectException(url);
	}

}
