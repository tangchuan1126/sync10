package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class DeleteFundsTransferAction extends ActionFatherController {

	private ApplyTransferMgrIFace applyTransferMgrZZZ;
	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	
	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}

	public void setApplyTransferMgrZZZ(ApplyTransferMgrIFace applyTransferMgrZZZ) {
		this.applyTransferMgrZZZ = applyTransferMgrZZZ;
	}

	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
		long transferId = StringUtil.getLong(arg0, "transferId");
		long applyId = StringUtil.getLong(arg0, "applyId");
		long userName_id = StringUtil.getLong(arg0, "userName_id");
		String name = StringUtil.getString(arg0, "userName");
		
		String context = "转账申请单删除:单号"+transferId+",删除原因:"+StringUtil.getString(arg0, "reason");
		
		applyTransferMgrZZZ.deleteApplyTransferAndHandleApplyMoneyState(applyId, transferId, arg0);
//		applyTransferMgrZZZ.deleteApplyTransferByTransferId(transferId);
		applyMoneyLogsMgrZZZ.addApplyMoneyLogs(applyId, userName_id, name, context);
		
		throw new RedirectRefException();
	}

}
