package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyFundsCategoryMgrIfaceZZZ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class DeleteApplyFundsCategoryAction extends ActionFatherController {

private ApplyFundsCategoryMgrIfaceZZZ applyFundsCategoryMgrZZZ;
	
	public void setApplyFundsCategoryMgrZZZ(
			ApplyFundsCategoryMgrIfaceZZZ applyFundsCategoryMgrZZZ) {
		this.applyFundsCategoryMgrZZZ = applyFundsCategoryMgrZZZ;
	}
	
	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception {
		// TODO Auto-generated method stub
		applyFundsCategoryMgrZZZ.deleteApplyFundsCategoryByCategoryId(arg0);
		throw new RedirectRefException();
	}

}
