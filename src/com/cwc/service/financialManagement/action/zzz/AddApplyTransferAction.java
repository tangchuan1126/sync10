package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;


public class AddApplyTransferAction extends ActionFatherController {

	private ApplyTransferMgrIFace applyTransferMgrIFace;
	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	private AdminMgrIFace adminMgr;
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}


	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}


	public void setApplyTransferMgrIFace(ApplyTransferMgrIFace applyTransferMgrIFace) {
		this.applyTransferMgrIFace = applyTransferMgrIFace;
	}


	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
		    DBRow row = applyTransferMgrIFace.addApplyTransfer(request);
		    long apply_money_id = StringUtil.getLong(request,"apply_money_id");
		    String context = "新建转账申请:单号"+row.get("transferId", 0l);
		    HttpSession session = request.getSession();
		    AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
		    
		    applyMoneyLogsMgrZZZ.addApplyMoneyLogs(apply_money_id,adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), context);
		    DBRow result = new DBRow();
			result.add("flag", true);
			throw new JsonException(new JsonObject(result));
		   //throw new RedirectRefException();		    
	}

}
