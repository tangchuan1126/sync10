package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.*;
import com.cwc.util.StringUtil;

public class UploadFileAction extends ActionFatherController {

	private ApplyTransferMgrIFace applyTransferMgrIFace;
	
	public void setApplyTransferMgrIFace(ApplyTransferMgrIFace applyTransferMgrIFace) {
		this.applyTransferMgrIFace = applyTransferMgrIFace;
	}

	/**
	 * 上传转账凭证
	 * @param arg0
	 * @param arg1
	 * @throws WriteOutResponseException
	 * @throws RedirectRefException
	 * @throws ForwardException
	 * @throws Forward2JspException
	 * @throws RedirectBackUrlException
	 * @throws RedirectException
	 * @throws OperationNotPermitException
	 * @throws PageNotFoundException
	 * @throws Exception
	 */
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
		long id=applyTransferMgrIFace.confirmTransferUploadVoucher(request);
		throw new WriteOutResponseException("<script type=\"text/javascript\">parent.tb_remove();parent.reloadFundsTransfer("+id+");</script>");
	}

}
