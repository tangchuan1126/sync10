package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyFundsCategoryMgrIfaceZZZ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class GetDetailApplyFundsCategoryJSONAction extends
		ActionFatherController {

	private ApplyFundsCategoryMgrIfaceZZZ applyFundsCategoryMgrZZZ;
	
	public void setApplyFundsCategoryMgrZZZ(
			ApplyFundsCategoryMgrIfaceZZZ applyFundsCategoryMgrZZZ) {
		this.applyFundsCategoryMgrZZZ = applyFundsCategoryMgrZZZ;
	}

	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
			DBRow category=applyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(arg0);
			if (category == null) {
				category = new DBRow();
				category.add("category_name", "/");				
			}
			throw new JsonException(new JsonObject(category));
	}

}
