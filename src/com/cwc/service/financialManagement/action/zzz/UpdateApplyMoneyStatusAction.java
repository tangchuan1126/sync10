package com.cwc.service.financialManagement.action.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateApplyMoneyStatusAction extends ActionFatherController {

	private ApplyMoneyMgrIFace applyMoneyMgr;
	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	private AdminMgrIFace adminMgr;
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}


	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}
	
	public void setApplyMoneyMgr(ApplyMoneyMgrIFace applyMoneyMgr) {
		this.applyMoneyMgr = applyMoneyMgr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
		
		applyMoneyMgr.updateApplyMoneyStatusByApplyId(request);
		long id = StringUtil.getLong(request, "applyId");
		String context = "确认并操作资金申请单“"+id+"”已完成付款";
		HttpSession session = request.getSession();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
		applyMoneyLogsMgrZZZ.addApplyMoneyLogs(id,adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), context);
		throw new RedirectRefException();
	}

}
