package com.cwc.service.financialManagement.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ll.ApplyMoneyMgrIFaceLL;
import com.cwc.app.iface.zyj.RepairOrderMgrZyjIFace;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class AssociationTypeCheckJSONAction extends ActionFatherController{
	
	private ApplyMoneyMgrIFaceLL applyMoneyMgrLL;
	private RepairOrderMgrZyjIFace repairOrderMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws JsonException,WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception 
	{
		String className = StringUtil.getString(request,"className");
		String id = StringUtil.getString(request,"id");
		int type = StringUtil.getInt(request,"type");
		DBRow row = new DBRow();
		Boolean b = false;
		if("repairOrderBeanLL".equals(className) && FinanceApplyTypeKey.REPAIR_ORDER == type)
		{
			b = repairOrderMgrZyj.isRepairOrderExist(Long.parseLong(id));
		}
		else
		{
			b = applyMoneyMgrLL.checkApplyMoneyAssociationById(className, id, type);
		}
		row.add("isok", b);
		throw new JsonException(new JsonObject(row));
	}
	
	public void setApplyMoneyMgrLL(ApplyMoneyMgrIFaceLL applyMoneyMgrLL) {
		this.applyMoneyMgrLL = applyMoneyMgrLL;
	}

	public void setRepairOrderMgrZyj(RepairOrderMgrZyjIFace repairOrderMgrZyj) {
		this.repairOrderMgrZyj = repairOrderMgrZyj;
	}
}
