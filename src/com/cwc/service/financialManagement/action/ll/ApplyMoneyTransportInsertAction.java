package com.cwc.service.financialManagement.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ll.ApplyMoneyMgrIFaceLL;
import com.cwc.app.iface.zyj.ApplyFundsMgrZyjIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ApplyMoneyTransportInsertAction extends ActionFatherController {

	private ApplyFundsMgrZyjIFace applyFundsMgrZyj;
	
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception
	{
		applyFundsMgrZyj.applyTransportFreightMoney(request);

		DBRow result = new DBRow();
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
	}
	
	public void setApplyFundsMgrZyj(ApplyFundsMgrZyjIFace applyFundsMgrZyj) {
		this.applyFundsMgrZyj = applyFundsMgrZyj;
	}
}
