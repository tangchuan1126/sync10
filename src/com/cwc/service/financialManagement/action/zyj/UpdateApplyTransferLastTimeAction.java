package com.cwc.service.financialManagement.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class UpdateApplyTransferLastTimeAction   extends ActionFatherController {

	private ApplyTransferMgrIFace applyTransferMgr;

	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
		applyTransferMgr.updateTransferLastTime(request);
		DBRow result = new DBRow();
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
	}

	public void setApplyTransferMgr(ApplyTransferMgrIFace applyTransferMgr) {
		this.applyTransferMgr = applyTransferMgr;
	}


	
	
}
