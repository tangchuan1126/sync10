package com.cwc.service.financialManagement.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddApplyFundsNoAssociateAction  extends ActionFatherController {

	private ApplyMoneyMgrIFace applyMoneyMgr;
	
	public void setApplyMoneyMgr(ApplyMoneyMgrIFace applyMoneyMgr) {
		this.applyMoneyMgr = applyMoneyMgr;
	}


	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception 
	{
//		String adid=StrUtil.getString(request, "adid");
//		long categoryId=StrUtil.getLong(request, "categoryId",0l);
		long association_id=StringUtil.getLong(request, "associationId",0l);
//		double amount=StrUtil.getDouble(request, "amount");
//		String payee=StrUtil.getString(request, "payee");
//		String paymentInfo=StrUtil.getString(request, "paymentInfo");
//		String remark=StrUtil.getString(request, "remark");
//		long center_account_id=StrUtil.getLong(request, "center_account_id",01);
//		long center_account_type_id=StrUtil.getLong(request, "center_account_type_id",01);
//		long center_account_type1_id=StrUtil.getLong(request, "center_account_type1_id",01);
//		long product_line_id=StrUtil.getLong(request, "product_line_id",01);
		int flag=StringUtil.getInt(request, "flag");
//		int association_type_id=StrUtil.getInt(request, "association_type_id");
		int add=StringUtil.getInt(request, "add");
//		long payee_type_id = StrUtil.getLong(request,"payee_type_id",0);
//		long payee_type1_id = StrUtil.getLong(request,"payee_type1_id",0);
//		long payee_id = StrUtil.getLong(request,"payee_id",0);
//		String currency=StrUtil.getString(request, "currency");
//		String lastTime = StrUtil.getString(request, "last_time");
//		
//		AdminMgr adminMgr = new AdminMgr();
//		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StrUtil.getSession(request));
		DBRow row = applyMoneyMgr.addApplyMoney(request);
		applyMoneyMgr.updateAssetsState(association_id,3);
		
//		if(flag==1)
//		{
			throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");
//		}
//		else if(add==1)
//		{
//			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/apply_noassociate_money_upload_image.html?association_id="+row.getString("applyId")+"&association_type=1");
//		}else if(add == 0) {
//			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/financial_management/apply_noassociate_money_upload_image.html?association_id="+row.getString("applyId")+"&association_type=1");
//		}
	}

}
