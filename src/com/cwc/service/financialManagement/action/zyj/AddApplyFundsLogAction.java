package com.cwc.service.financialManagement.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddApplyFundsLogAction extends ActionFatherController{

	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		applyMoneyLogsMgrZZZ.addApplyMoneyLogAndSchedule(request);
	}

	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}

}
