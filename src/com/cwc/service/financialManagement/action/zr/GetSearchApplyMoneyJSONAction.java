package com.cwc.service.financialManagement.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.lucene.zr.ApplyMoneyIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSearchApplyMoneyJSONAction extends ActionFatherController{
static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private SystemConfigIFace systemConfig;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		javapsLog.info("request: "+request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
				StringUtil.getString(request, "q") : 
				StringUtil.getString(request,"term");
		javapsLog.info("q="+q);
		
		try
		{
			int suggestNum = systemConfig.getIntConfigValue("page_size");
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(suggestNum);
			if(!q.equals("")&&!q.equals("\""))
			{
//				q = q.replaceAll("\"","");
//				q = q.replace("\\*","");
//				if(	 q.indexOf("p") == 0 || q.indexOf("P") == 0 
//				  || q.indexOf("t") == 0 || q.indexOf("T") == 0
//				  || q.indexOf("w") == 0 || q.indexOf("W") == 0
//				  || q.indexOf("f") == 0 || q.indexOf("F") == 0){
//				  
//				  q = q.substring(1);
//				}
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("\\*","");
				q = q.replaceAll("\'","");
			}
			DBRow bills[] =  ApplyMoneyIndexMgr.getInstance().mergeSearchIK("merge_field", q, pc);
			
			throw new JsonException(new JsonObject(bills));
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
}
