package com.cwc.service.bolCustomer.action.zwb;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gql.TaskAndInvoiceMgrIfaceGql;
import com.cwc.app.iface.zj.LableTemplateMgrIFaceZJ;
import com.cwc.app.iface.zwb.BolCustomerIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class BolListAction extends ActionFatherController{
	
	
	private BolCustomerIfaceZwb bolCustomerIfaceZwb;
	private TransactionTemplate txTemplate;
	
	@Override
	public void perform(final HttpServletRequest request, final HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		try{
			txTemplate.execute(new TransactionCallbackWithoutResult() {
				
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus txStat) {
//					System.out.println(request.getMethod());
					try {
						switch (request.getMethod()) {
						case "GET":
							bolCustomerIfaceZwb.getBolList(request, response, null);						
							break;
						case "POST":
							bolCustomerIfaceZwb.addBol(request, response);	
							break;
						case "PUT":
							bolCustomerIfaceZwb.editBol(request, response);
							break;
						case "DELETE":
							bolCustomerIfaceZwb.detBol(request, response);
							break;
						default:
							
							
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
					
				}
			});
			
			
		}catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}

	

	public void setBolCustomerIfaceZwb(BolCustomerIfaceZwb bolCustomerIfaceZwb) {
		this.bolCustomerIfaceZwb = bolCustomerIfaceZwb;
	}



	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}
	
}
