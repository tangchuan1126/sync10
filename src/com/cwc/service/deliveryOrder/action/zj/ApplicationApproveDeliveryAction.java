package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DeliveryApproveMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 交货申请审核
 * @author Administrator
 *
 */
public class ApplicationApproveDeliveryAction extends ActionFatherController {

	private  DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ;

	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		deliveryApproveMgrZJ.addDeliveryApprove(request);
		throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/delivery/administrator_delivery_order_index.html");
	}


	public void setDeliveryApproveMgrZJ(DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ) {
		this.deliveryApproveMgrZJ = deliveryApproveMgrZJ;
	}
}
