package com.cwc.service.deliveryOrder.action.zj;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditDeliveryOrderDetailAction extends ActionFatherController {

	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		
		{
//			 Enumeration enumOne = request.getParameterNames();
//			 while(enumOne.hasMoreElements())
//			 { 
//			    String s=(String)enumOne.nextElement();
//			    //system.out.println(StrUtil.getString(request,s)+"-------"+s);
//			 } 
			    
			String oper = StringUtil.getString(request,"oper");//grid操作

			if(oper.equals("edit"))
			{
				deliveryMgrZJ.modDeliveryOrderDetail(request);
			}
			else if(oper.equals("add"))
			{
				deliveryMgrZJ.addDeliveryOrderDetail(request);
			}
			else if(oper.equals("del"))
			{
				deliveryMgrZJ.delDelvieryOrderDetail(request);
			}
			throw new WriteOutResponseException("0");
		} 
		catch (ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
		
		catch(RepeatProductException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[交货单内已有这个商品]");
		}
	}
	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
