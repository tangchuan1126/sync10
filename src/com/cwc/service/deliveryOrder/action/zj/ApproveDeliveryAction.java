package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.DeliveryApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 交货单差异审核
 * @author Administrator
 *
 */
public class ApproveDeliveryAction extends ActionFatherController {

	private DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long da_id = StringUtil.getLong(request,"da_id");
		String[] sdad_ids = request.getParameterValues("dad_ids");
		long[] dad_ids = new long[sdad_ids.length];
		String[] notes = new String[sdad_ids.length];
		
		for (int i = 0; i < sdad_ids.length; i++)
		{
			dad_ids[i] = Long.parseLong(sdad_ids[i]);
			notes[i] = StringUtil.getString(request,"note_"+dad_ids[i]);
		}
		deliveryApproveMgrZJ.approveDeliveryDifferent(da_id, dad_ids, notes, adminLoggerBean);
		
		throw new RedirectBackUrlException();
	}

	public void setDeliveryApproveMgrZJ(DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ) 
	{
		this.deliveryApproveMgrZJ = deliveryApproveMgrZJ;
	}
}
