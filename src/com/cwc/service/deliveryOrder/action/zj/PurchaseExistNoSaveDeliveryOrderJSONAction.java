package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 检查该采购单是否有未保存交货单
 * @author Administrator
 *
 */
public class PurchaseExistNoSaveDeliveryOrderJSONAction extends ActionFatherController 
{
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow[] rows = deliveryMgrZJ.getNoSaveDeliveryOrderByPurchaseID(request);
		
		DBRow jsonResult = new DBRow();
		boolean result;
		if(rows!=null&&rows.length>0)
		{
			result = true;
		}
		else
		{
			result = false;
		}
		
		jsonResult.add("nosave",result);
		
		throw new JsonException(new JsonObject(jsonResult));
	}
	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
