package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 上传采购单详细保存
 * @author Administrator
 *
 */
public class SaveDeliveryOrderDetailsAction extends ActionFatherController {

	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db=new DBRow();
		try 
		{
			deliveryMgrZJ.saveDeliveryOrderDetails(request);
			db.add("close",true);
		}
		catch (FileException e) 
		{
			db.add("error","上传文件内商品数量与原采购单内数量不匹配");
		}

		throw new JsonException(new JsonObject(db));
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}
}
