package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddPrintDeliveryOrderAction extends ActionFatherController {

	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String backurl = StringUtil.getString(request,"backurl");
		
		long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
		if(delivery_order_id>0)
		{
			deliveryMgrZJ.saveDeliveryOrder(request);
		}
		else
		{
			delivery_order_id = deliveryMgrZJ.addDeliveryOrder(request);
		}
		
		backurl = backurl+"?delivery_order_id="+delivery_order_id+"&isPrint=1";
		
		throw new RedirectException(backurl);
	}
	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
