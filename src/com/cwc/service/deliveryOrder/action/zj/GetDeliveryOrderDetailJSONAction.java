package com.cwc.service.deliveryOrder.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * jqgrid修改交货单商品名后处理
 * @author Administrator
 *
 */
public class GetDeliveryOrderDetailJSONAction extends ActionFatherController {

	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow deliveryOrderDetail = deliveryMgrZJ.getDeliveryOrderDetail(request);
		
		if(deliveryOrderDetail.getString("reapcount").equals("")||deliveryOrderDetail.getString("purchasecount").equals(""))
		{
			deliveryOrderDetail.add("reapcount","未定商品");
			deliveryOrderDetail.add("purchasecount","未定商品");
		}
		
		if(deliveryOrderDetail.getString("delivery_box").equals(""))
		{
			deliveryOrderDetail.add("delivery_box"," ");
		}
		
		throw new JsonException(new JsonObject(deliveryOrderDetail));
		

	}
	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
