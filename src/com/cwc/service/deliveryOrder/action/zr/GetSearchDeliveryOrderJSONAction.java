package com.cwc.service.deliveryOrder.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.lucene.zr.DeliveryOrderIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetSearchDeliveryOrderJSONAction extends ActionFatherController{

	static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private int suggestNum = 10;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		javapsLog.info("request: "+request.getParameterMap());
		String q = request.getParameterMap().containsKey("q") ? 
				StringUtil.getString(request, "q") : 
				StringUtil.getString(request,"term");
		javapsLog.info("q="+q);
		
		try
		{
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(suggestNum);
			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q +="*";
				
			}
			DBRow bills[] =  DeliveryOrderIndexMgr.getInstance().mergeSearch("merge_field", q, pc);
			
			throw new JsonException(new JsonObject(bills));
		} 
		catch (JsonException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new JsonException(new JsonObject(null));//加入搜索出错，返回空值
		}
		
	}
	public int getSuggestNum() {
		return suggestNum;
	}

	public void setSuggestNum(int suggestNum) {
		this.suggestNum = suggestNum;
	}
}
