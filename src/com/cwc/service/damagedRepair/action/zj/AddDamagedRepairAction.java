package com.cwc.service.damagedRepair.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddDamagedRepairAction extends ActionFatherController {

	private DamagedRepairMgrIFaceZJ damagedRepairMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, Exception 
	{
		damagedRepairMgrZJ.addDamagedRepair(request);
		
		String backurl = StringUtil.getString(request,"backurl");
		
		//backurl +="?transport_id="+transport_id;
		
		String result="<script type=\"text/javascript\">"+"window.parent.location.href='"+backurl+"'</script>";
		throw new WriteOutResponseException(result);
	}

	public void setDamagedRepairMgrZJ(DamagedRepairMgrIFaceZJ damagedRepairMgrZJ) {
		this.damagedRepairMgrZJ = damagedRepairMgrZJ;
	}

}
