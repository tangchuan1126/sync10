package com.cwc.service.damagedRepair.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
/**
 * 上传文件保存返修明细
 * @author Administrator
 *
 */
public class SaveDamagedRepairDetailAction extends ActionFatherController {

	private DamagedRepairMgrIFaceZJ damagedRepairMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db = new DBRow();
		try 
		{
			damagedRepairMgrZJ.saveDamagedRepairDetail(request);
			
			db.add("close",true);
		} 
		catch (ProductNotExistException e) 
		{
			db.add("error","有无法识别的商品");
		}
		
		throw new JsonException(new JsonObject(db));

	}
	
	public void setDamagedRepairMgrZJ(DamagedRepairMgrIFaceZJ damagedRepairMgrZJ) {
		this.damagedRepairMgrZJ = damagedRepairMgrZJ;
	}

}
