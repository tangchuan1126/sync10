package com.cwc.service.damagedRepair.action.zj;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditDamagedRepairDetailAction extends ActionFatherController {

	private DamagedRepairMgrIFaceZJ damagedRepairMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作

			if(oper.equals("edit"))
			{
				damagedRepairMgrZJ.modDamagedRepairDetail(request);
			}
			else if(oper.equals("add"))
			{
				damagedRepairMgrZJ.addDamagedRepairDetail(request);
			}
			else if(oper.equals("del"))
			{
				damagedRepairMgrZJ.delDamagedRepairDetail(request);
			}
			
			throw new WriteOutResponseException("0");
		} 
		catch (ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
		
		catch(RepeatProductException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[返修单内已有这个商品]");
		}
	}
	public void setDamagedRepairMgrZJ(DamagedRepairMgrIFaceZJ damagedRepairMgrZJ) {
		this.damagedRepairMgrZJ = damagedRepairMgrZJ;
	}
	
}