package com.cwc.service.damagedRepair.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.DamagedRepairOutboundApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class ApproveDamagedRepairOutbound extends ActionFatherController {

	private DamagedRepairOutboundApproveMgrIFaceZJ damagedRepairOutboundApproveMgrZJ; 
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long rsa_id = StringUtil.getLong(request,"rsa_id");
		String[] srsad_ids = request.getParameterValues("rsad_ids");
		long[] rsad_ids = new long[srsad_ids.length];
		String[] notes = new String[srsad_ids.length];
		
		for (int i = 0; i < srsad_ids.length; i++)
		{
			rsad_ids[i] = Long.parseLong(srsad_ids[i]);
			notes[i] = StringUtil.getString(request,"note_"+rsad_ids[i]);
		}
		
		damagedRepairOutboundApproveMgrZJ.approveDamagedRepairOutboundDifferent(rsa_id, rsad_ids, notes, adminLoggerBean);
		
		throw new RedirectBackUrlException();
	}

	public void setDamagedRepairOutboundApproveMgrZJ(DamagedRepairOutboundApproveMgrIFaceZJ damagedRepairOutboundApproveMgrZJ) 
	{
		this.damagedRepairOutboundApproveMgrZJ = damagedRepairOutboundApproveMgrZJ;
	}

}
