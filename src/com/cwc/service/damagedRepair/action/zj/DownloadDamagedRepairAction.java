package com.cwc.service.damagedRepair.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class DownloadDamagedRepairAction extends ActionFatherController {

	private  DamagedRepairMgrIFaceZJ damagedRepairMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db = new DBRow();;
		try 
		{
			String path = damagedRepairMgrZJ.downloadDamagedRepairOrder(request);
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
			}
			else
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",path);
		} 
		catch (Exception e) 
		{
			db.add("canexport",false);
		}
		
		throw new JsonException(new JsonObject(db));
	}

	public void setDamagedRepairMgrZJ(DamagedRepairMgrIFaceZJ damagedRepairMgrZJ) {
		this.damagedRepairMgrZJ = damagedRepairMgrZJ;
	}
}
