package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.transport.CanNotFillSerialNumberException;
import com.cwc.app.exception.transport.HadSerialNumberException;
import com.cwc.app.exception.transport.TransportOrderDetailRepeatException;
import com.cwc.app.exception.transport.TransportSerialNumberRepeatException;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GridEditTransportAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		try 
		{
			String oper = StringUtil.getString(request,"oper");//grid操作

			if(oper.equals("edit"))
			{
				transportMgrZJ.modTransportDetail(request);
			}
			else if(oper.equals("add"))
			{
				transportMgrZJ.addTransportDetail(request);
			}
			else if(oper.equals("del"))
			{
				transportMgrZJ.delTransportDetail(request);
			}
			
			throw new WriteOutResponseException("0");
		} 
		catch(TransportOrderDetailRepeatException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[已有此商品]");
		}
		catch(ProductNotExistException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[我们无法识别这个商品]");
		}
		catch(TransportSerialNumberRepeatException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号重复]");
		}
		catch(RepeatProductException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[交货单内已有这个商品]");
		}
		catch (HadSerialNumberException e) 
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号只能标记一个商品]");
		}
		catch (CanNotFillSerialNumberException e)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[序列号只能标记一个商品]");
		}
	}
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
