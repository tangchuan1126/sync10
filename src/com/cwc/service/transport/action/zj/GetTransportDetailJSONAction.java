package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class GetTransportDetailJSONAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	private LPTypeMgrIFaceZJ LPTypeMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow transportDetail = transportMgrZJ.getTransportDetailById(request);
		
	 	
		String CLPName = "";
	 	if(transportDetail.get("clp_type_id",0l)!=0l)
	 	{
	 		CLPName = LPTypeMgrZJ.getCLPName(transportDetail.get("clp_type_id",0l));
	 	}
	 	transportDetail.add("CLP",CLPName);
	 	
	 	String BLPName = "";
	 	if(transportDetail.get("blp_type_id",0l)!=0l)
	 	{
	 		BLPName = LPTypeMgrZJ.getBLPName(transportDetail.get("blp_type_id",0l));
	 	}
	 	
	 	transportDetail.add("blp_type",BLPName);
		
		throw new JsonException(new JsonObject(transportDetail));
	}
	
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ)
	{
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setLPTypeMgrZJ(LPTypeMgrIFaceZJ typeMgrZJ) {
		LPTypeMgrZJ = typeMgrZJ;
	}

}
