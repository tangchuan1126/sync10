package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;
/**
 * 创建采购交货转运单
 * @author Administrator
 *
 */
public class AddPurchaseTransportAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		long transport_id = transportMgrZJ.addTransportForPurchase(request);
		
		String backurl = StringUtil.getString(request,"backurl");
		
		backurl +="?transport_id="+transport_id+"&finished=1";
		
		
//		throw new RedirectException(backurl);
		String result="<script type=\"text/javascript\">"+"parent.window.location.href='"+backurl+"'</script>";
		throw new WriteOutResponseException(result);
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
