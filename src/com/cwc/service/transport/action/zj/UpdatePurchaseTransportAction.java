package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdatePurchaseTransportAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		transportMgrZJ.updateTransportForPurchaseBasic(request);
		
		long transport_id = StringUtil.getLong(request, "transport_id");
		int isOutter = StringUtil.getInt(request, "isOutter");
		if(2 == isOutter)
		{
			throw new WriteOutResponseException("<script>" +
					"parent.location.reload()" +
					"</script>");
		}
		else
		{
			String backurl = StringUtil.getString(request,"backurl");
			backurl +="?transport_id="+transport_id+"&finished=1";
			throw new RedirectException(backurl);
		}
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
