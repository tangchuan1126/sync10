package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.TransportOutboundApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 交货单差异审核
 * @author Administrator
 *
 */
public class ApproveTransportOutboundAction extends ActionFatherController {

	private TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long tsa_id = StringUtil.getLong(request,"tsa_id");
		String[] stsad_ids = request.getParameterValues("tsad_ids");
		String[] stsads_ids = request.getParameterValues("tsads_ids");
		
		long[] tsad_ids = new long[stsad_ids.length];
		long[] tsads_ids = new long[stsads_ids.length];
		
		String[] notes = new String[stsad_ids.length];
		String[] noteSNs = new String[stsads_ids.length];
		
		for (int i = 0; i < stsad_ids.length; i++)
		{
			tsad_ids[i] = Long.parseLong(stsad_ids[i]);
			notes[i] = StringUtil.getString(request,"note_"+tsad_ids[i]);
		}
		
		for (int i = 0; i < stsads_ids.length; i++) 
		{
			tsads_ids[i] = Long.parseLong(stsads_ids[i]);
			noteSNs[i] = StringUtil.getString(request,"noteSN_"+tsads_ids[i]);
		}
		
		transportOutboundApproveMgrZJ.approveTransportOutboundDifferent(tsa_id,tsad_ids,notes,tsads_ids,noteSNs,adminLoggerBean);
		
		throw new RedirectBackUrlException();
	}

	public void setTransportOutboundApproveMgrZJ(
			TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ) {
		this.transportOutboundApproveMgrZJ = transportOutboundApproveMgrZJ;
	}

}
