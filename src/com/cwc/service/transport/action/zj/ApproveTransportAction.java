package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.DeliveryApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 交货单差异审核
 * @author Administrator
 *
 */
public class ApproveTransportAction extends ActionFatherController {

	private TransportApproveMgrIFaceZJ transportApproveMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		long ta_id = StringUtil.getLong(request,"ta_id");
		String[] stad_ids = request.getParameterValues("tad_ids");
		String[] stads_ids = request.getParameterValues("tads_ids");
		
		if(stad_ids==null)
		{
			stad_ids = new String[0];
		}
		if(stads_ids==null)
		{
			stads_ids = new String[0];
		}
		
		long[] tad_ids = new long[stad_ids.length];
		long[] tads_ids = new long[stads_ids.length];
		String[] notes = new String[stad_ids.length];
		String[] noteSNs = new String[stads_ids.length];
		
		for (int i = 0; i < stad_ids.length; i++)
		{
			tad_ids[i] = Long.parseLong(stad_ids[i]);
			notes[i] = StringUtil.getString(request,"note_"+tad_ids[i]);
		}
		
		for (int i = 0; i < stads_ids.length; i++) 
		{
			tads_ids[i] = Long.parseLong(stads_ids[i]);
			noteSNs[i] = StringUtil.getString(request,"noteSN_"+tads_ids[i]);
		}
		
		transportApproveMgrZJ.approveTransportDifferent(ta_id,tad_ids,notes,tads_ids,noteSNs,adminLoggerBean);
		
		throw new RedirectBackUrlException();
	}

	public void setTransportApproveMgrZJ(TransportApproveMgrIFaceZJ transportApproveMgrZJ) 
	{
		this.transportApproveMgrZJ = transportApproveMgrZJ;
	}

}
