package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class UploadTransportInvoiceAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		transportMgrZJ.uploadTransportInvoice(request);
		
		throw new WriteOutResponseException
		(
			 "<script type=\"text/javascript\" src=\"../../../administrator/js/jqGrid-4.1.1/js/jquery-1.7.2.source.js\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/jquery.artDialog.source.js\" type=\"text/javascript\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/iframeTools.source.js\" type=\"text/javascript\"></script>"
			+"<script src=\"../../../administrator/js/art/plugins/iframeTools.js\" type=\"text/javascript\"></script>"	
			+"<script type=\"text/javascript\">" 
			+"parent.window.location.reload();"
			+"$.artDialog.close();" 
			+"</script>"
		);
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	
	
}
