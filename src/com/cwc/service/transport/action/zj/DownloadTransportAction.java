package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class DownloadTransportAction extends ActionFatherController {

	private  TransportMgrIFaceZJ transportMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow db = new DBRow();;
		try 
		{
			String path = transportMgrZJ.downloadTransportOrder(request);
			String basePath = "";
			
			if (!path.equals("0")) 
			{
				db.add("canexport",true);
				basePath = "../../" + path;	
			}
			else
			{
				db.add("canexport",false);
			}
			
			
			db.add("fileurl",basePath);
		} 
		catch(Exception e) 
		{
			db.add("canexport",false);
		}
		
		throw new JsonException(new JsonObject(db));
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
