package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class TransportTrackCountAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		String type = StringUtil.getString(request,"type");
		if(type.equals("first"))
		{
			DBRow trackCount = transportMgrZJ.trackDeliveryAndTransportCountFirstMenu();
			throw new JsonException(new JsonObject(trackCount));
		}
		else if(type.equals("ready_delivery"))
		{
			DBRow[] track_delivery_pl_count = transportMgrZJ.trackDeliveryCountGroupByProductLine();
			throw new JsonException(new JsonObject(track_delivery_pl_count));
		}
		else if(type.equals("send_store_transport"))
		{
			DBRow[] track_send_ps_count = transportMgrZJ.trackSendTransportCountGroupByPs();
			throw new JsonException(new JsonObject(track_send_ps_count));
		}
		else if(type.equals("recive_store_transport"))
		{
			DBRow[] track_recive_ps_count = transportMgrZJ.trackReciveTransportCountGroupByPs();
			throw new JsonException(new JsonObject(track_recive_ps_count));
		}
		else if(type.equals("ocean_shipping"))
		{
			DBRow[] track_ocean_shipping_count = transportMgrZJ.trackOceanShippingCount();
			throw new JsonException(new JsonObject(track_ocean_shipping_count));
		}
		else if(type.equals("send_store_transport_ps"))
		{
			long ps_id = StringUtil.getLong(request,"ps_id");
			
			DBRow result = transportMgrZJ.trackSendTransportPsid(ps_id);
			throw new JsonException(new JsonObject(result));
		}
		else if(type.equals("recive_store_transport_ps"))
		{
			long ps_id = StringUtil.getLong(request,"ps_id");
			
			DBRow result = transportMgrZJ.trackReciveTransportPsid(ps_id);
			throw new JsonException(new JsonObject(result));
		}	
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
