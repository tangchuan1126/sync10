package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.productStorage.CanNotSerialNumberReserveException;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

/**
 * 转运单装箱
 * @author Administrator
 *
 */
public class IntransitTransportAction extends ActionFatherController {

	private TransportMgrIFaceZJ transportMgrZJ;
	private MessageAlerter messageAlerter;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String message = "系统错误";
		boolean rel = true;
		try 
		{
			transportMgrZJ.packingTransport(request);
		}
		catch (CanNotSerialNumberReserveException e)
		{
			message = Resource.getStringValue("","CanNotSerialNumberReserveException","");
			rel = false;
			//messageAlerter.setMessage(request,Resource.getStringValue("","CanNotSerialNumberReserveException",""));
		}
		
		DBRow db = new DBRow();
		db.add("rel",rel);
		db.add("message",message);
		throw new JsonException(new JsonObject(db));
	}
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	public void setMessageAlerter(MessageAlerter messageAlerter) {
		this.messageAlerter = messageAlerter;
	}

}
