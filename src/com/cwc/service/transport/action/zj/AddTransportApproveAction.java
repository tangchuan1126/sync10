package com.cwc.service.transport.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportApproveMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class AddTransportApproveAction extends ActionFatherController {

	private TransportApproveMgrIFaceZJ transportApproveMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, Exception 
	{
		transportApproveMgrZJ.addTransportApprove(request);
		throw new RedirectRefException();
	}
	public void setTransportApproveMgrZJ(
			TransportApproveMgrIFaceZJ transportApproveMgrZJ) {
		this.transportApproveMgrZJ = transportApproveMgrZJ;
	}

}
