package com.cwc.service.transport.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class TransportProductTagTypesAction extends ActionFatherController{
	
	private TransportMgrZyjIFace transportMgrZyj;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		
		
		
			transportMgrZyj.saveProductTagFile(request);

	 		 
		  	throw  new RedirectBackUrlException(); 
		
		
	}
	
	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

}
