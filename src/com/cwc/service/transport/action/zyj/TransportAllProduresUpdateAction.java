package com.cwc.service.transport.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class TransportAllProduresUpdateAction extends ActionFatherController{

	private TransportMgrZyjIFace transportMgrZyj;
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		transportMgrZyj.updateTransportProdures(request);
		
		String transport_id = StringUtil.getString(request,"transport_id");
		int finished = StringUtil.getInt(request,"finished",0);
		int isOutter = StringUtil.getInt(request, "isOutter");
		if(isOutter == 2)
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_wayout_update.html?transport_id="+transport_id+"&isOutter=2&isSubmitSuccess=2");
		else
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_freight_update.html?transport_id="+transport_id);
		
	}


	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

	
	
}
