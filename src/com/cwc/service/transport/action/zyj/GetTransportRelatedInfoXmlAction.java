package com.cwc.service.transport.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * 获取创建转运单所需信息
 * @author Administrator
 *
 */
public class GetTransportRelatedInfoXmlAction extends ActionFatherController
{
	static Logger log = Logger.getLogger("ACTION");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	
	private ProduresMgrZyjIFace produresMgrZyj;
	private AdminMgrIFaceZJ adminMgrZJ;
	private TransportMgrIfaceZr transportMgrZr;
	private OrderMgrIFace orderMgr;
	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
		String loginAccount = "";
		String password = "";
		String produres_id_str = "";
		String activities_vals = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		//人员
		String proJsIdStr = "";
		String ps_id_str = "";
		
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
			password		= StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			String machine		= StringUtil.getSampleNode(xml, "Machine");
			//人员
			proJsIdStr			= StringUtil.getSampleNode(xml,"proJsId");
			ps_id_str			= StringUtil.getSampleNode(xml,"ps_id");
			//流程
			String orderTypeId	= StringUtil.getSampleNode(xml, "orderTypeId");
			produres_id_str		= StringUtil.getSampleNode(xml, "produres_id");
			activities_vals		= StringUtil.getSampleNode(xml, "activities_vals");
			
			this.createRelatedInfoToZip(machine, ret, err, orderTypeId, produres_id_str, activities_vals, proJsIdStr, ps_id_str);
			
			byte[] buffer = new byte[1024 * 1024];
			int length = -1 ;
			
			
			FileInputStream  inputStream1 = new FileInputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+"_transport_related_info.zip"));
			response.setContentType("application/zip;charset=UTF-8");
			response.setHeader("content-disposition", "attachment;filename="+URLEncoder.encode(machine+"_transport_related_info.zip","utf-8"));  
			OutputStream outputStream =  response.getOutputStream();
			while((length = inputStream1.read(buffer)) != -1){
				outputStream.write(buffer, 0, length);
			}
			inputStream1.close();
			outputStream.flush();
			outputStream.close(); 
			
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"GetProduresDetailsByProduresIdDValsXMLAction:"+xml,log);
		}
		finally
		{
//			responseXML(produres_details, response, ret, err);
		}
		
	}
	
	private String parseToUtf8(String str) throws UnsupportedEncodingException
	{
		return new String(str.getBytes("UTF-8"), "UTF-8");
	}
	
	
	private void createRelatedInfoToZip(String machine, int ret, int err, String orderTypeId, String produres_id_str, String activities_vals, String proJsIdStr, String ps_id_str) throws Exception
	{
		String produresStr = parseToUtf8(this.produresArrayToStr(orderTypeId, ret, err));
		String produreDetailsStr = parseToUtf8(this.produreDetailssArrayToStr(produres_id_str, activities_vals, ret, err));
		String produreNoticesStr = parseToUtf8(this.prodursNoticessArrayToStr(produres_id_str, ret, err));
		String countryStr = parseToUtf8(this.parseCountrycodesArrayToString(ret, err));
//		String provinceStr = parseToUtf8(this.parseProvincesArrayToString(ret, err));
		String storageStr = parseToUtf8(this.storageArrayToStr(ret, err));
		String adminUsersStr = parseToUtf8(this.parseAdminUsersArrayToStr(proJsIdStr, ps_id_str, ret, err));
		
		ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(Environment.getHome()+"/bcs_down_zip/"+machine+"_transport_related_info.zip")));	 
 		
		ZipEntry produresEntry = new ZipEntry("produresStr.xml");
 		zipOutputStream.putNextEntry(produresEntry);
 		zipOutputStream.write(produresStr.getBytes("UTF-8"));
 		
 		ZipEntry produreDetailsXmlEntry = new ZipEntry("produreDetailsStr.xml");
 		zipOutputStream.putNextEntry(produreDetailsXmlEntry);
 		zipOutputStream.write(produreDetailsStr.getBytes("UTF-8"));
 		
 		ZipEntry produreNoticesXmlEntry = new ZipEntry("produreNoticesStr.xml");
 		zipOutputStream.putNextEntry(produreNoticesXmlEntry);
 		zipOutputStream.write(produreNoticesStr.getBytes("UTF-8"));
 		
 		ZipEntry countryStrEntry = new ZipEntry("countryStr.xml");
 		zipOutputStream.putNextEntry(countryStrEntry);
 		zipOutputStream.write(countryStr.getBytes("UTF-8"));
 		
// 		ZipEntry provinceStrEntry = new ZipEntry("provinceStr.xml");
// 		zipOutputStream.putNextEntry(provinceStrEntry);
// 		zipOutputStream.write(provinceStr.getBytes());
 		
 		ZipEntry storageStrEntry = new ZipEntry("storageStr.xml");
 		zipOutputStream.putNextEntry(storageStrEntry);
 		zipOutputStream.write(storageStr.getBytes("UTF-8"));
 		ZipEntry adminUsersStrEntry = new ZipEntry("adminUsersStr.xml");
 		zipOutputStream.putNextEntry(adminUsersStrEntry);
 		zipOutputStream.write(adminUsersStr.getBytes("UTF-8"));
 	 
 		zipOutputStream.flush();
 		zipOutputStream.close();
		
	}
	private String storageArrayToStr(int ret, int err) throws Exception
	{
		DBRow[] storages = storageCatalogMgrZyj.getProductStorageCatalogByType(-1, null);
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<product_storage_catalogs>\n");
		try
		{
			for (int i = 0; i < storages.length; i++)
			{
				sb.append("<product_storage_catalog id=\""+storages[i].get("id", 0L)+"\">");
				sb.append("<title>"+storages[i].getString("title")+" </title>");
				sb.append("<contact>"+storages[i].getString("contact")+" </contact>");
				sb.append("<phone>"+storages[i].getString("phone")+"</phone>");
				sb.append("<native>"+storages[i].get("native", 0L)+"</native>");
				sb.append("<storage_type>"+storages[i].get("storage_type", 0)+"</storage_type>");
				sb.append("<pro_id>"+storages[i].get("pro_id", 0L)+"</pro_id>");
				sb.append("<city>"+storages[i].getString("city")+" </city>");
				sb.append("<freight_coefficient>"+storages[i].get("freight_coefficient", 0)+"</freight_coefficient>");
				sb.append("<address>"+storages[i].getString("address")+" </address>");
				sb.append("<send_nation>"+storages[i].get("send_nation", 0L)+"</send_nation>");
				sb.append("<send_pro_id>"+storages[i].get("send_pro_id", 0L)+"</send_pro_id>");
				sb.append("<send_city>"+storages[i].getString("send_city")+" </send_city>");
				sb.append("<send_house_number>"+storages[i].getString("send_house_number")+" </send_house_number>");
				sb.append("<send_street>"+storages[i].getString("send_street")+" </send_street>");
				sb.append("<send_zip_code>"+storages[i].getString("send_zip_code")+"</send_zip_code>");
				sb.append("<send_contact>"+storages[i].getString("send_contact")+" </send_contact>");
				sb.append("<send_phone>"+storages[i].getString("send_phone")+"</send_phone>");
				sb.append("<send_pro_input>"+storages[i].getString("send_pro_input")+" </send_pro_input>");
				sb.append("<deliver_address>"+storages[i].getString("deliver_address")+" </deliver_address>");
				sb.append("<deliver_nation>"+storages[i].get("deliver_nation", 0L)+"</deliver_nation>");
				sb.append("<deliver_pro_id>"+storages[i].get("deliver_pro_id", 0L)+"</deliver_pro_id>");
				sb.append("<deliver_city>"+storages[i].getString("deliver_city")+" </deliver_city>");
				sb.append("<deliver_house_number>"+storages[i].getString("deliver_house_number")+" </deliver_house_number>");
				sb.append("<deliver_street>"+storages[i].getString("deliver_street")+" </deliver_street>");
				sb.append("<deliver_zip_code>"+storages[i].getString("deliver_zip_code")+"</deliver_zip_code>");
				sb.append("<deliver_contact>"+storages[i].getString("deliver_contact")+" </deliver_contact>");
				sb.append("<deliver_phone>"+storages[i].getString("deliver_phone")+"</deliver_phone>");
				sb.append("<deliver_pro_input>"+storages[i].getString("deliver_pro_input")+" </deliver_pro_input>");
				sb.append("</product_storage_catalog>");
			}
			sb.append("</product_storage_catalogs>");
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
		return sb.toString();
	}
	
	
	/**
	 * 
	 * 获取流程信息
	 * @author Administrator
	 * @param produres
	 * @return
	 * @throws Exception
	 */
	private String produresArrayToStr(String orderTypeId, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<produres>\n");
		try
		{
			int typeId		= "".equals(orderTypeId)?-1:Integer.parseInt(orderTypeId);
			DBRow[] produres = produresMgrZyj.getProduresByOrderType(typeId);
			for (int i = 0; i < produres.length; i++)
			{
				sb.append("<produre produres_id=\""+produres[i].get("produres_id", 0L)+"\">\n");
				sb.append("<process_key>"+produres[i].get("process_key",0)+"</process_key>\n");
				sb.append("<process_coloum>"+produres[i].getString("process_coloum")+"</process_coloum>\n");
				sb.append("<process_coloum_page>"+produres[i].getString("process_coloum_page")+"</process_coloum_page>\n");
				sb.append("<process_name>"+produres[i].getString("process_name")+"</process_name>\n");
				sb.append("<process_note_is_display>"+produres[i].get("process_note_is_display", 0)+"</process_note_is_display>\n");
				sb.append("<process_note>"+produres[i].getString("process_note")+"</process_note>\n");
				sb.append("<process_person_is_display>"+produres[i].get("process_person_is_display",0)+"</process_person_is_display>\n");
				sb.append("<process_person>"+produres[i].getString("process_person")+"</process_person>\n");
				sb.append("<activity_is_need_display>"+produres[i].get("activity_is_need_display", 0)+"</activity_is_need_display>\n");
				sb.append("<associate_order_type>"+produres[i].get("associate_order_type", 0)+"</associate_order_type>\n");
				sb.append("<process_start_time_is_create>"+produres[i].get("process_start_time_is_create", 0)+"</process_start_time_is_create>\n");
				sb.append("<process_period>"+produres[i].get("process_period", 0)+"</process_period>\n");
				sb.append("<activity_default_selected>"+produres[i].get("activity_default_selected", 0)+"</activity_default_selected>\n");
				sb.append("</produre>\n");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</produres>");
		return sb.toString();
	}
	
	/**
	 * 获取流程通知信息
	 * @author Administrator
	 * @param produres_id
	 * @param ret
	 * @param err
	 * @return
	 * @throws Exception
	 */
	private String prodursNoticessArrayToStr(String produres_id_str, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<produres_notices>\n");
		try
		{
			long produres_id	= "".equals(produres_id_str)?-1:Integer.parseInt(produres_id_str);
			DBRow[] produres_notices = produresMgrZyj.getProduresNoticesByProduresId(produres_id, YesOrNotKey.YES);
			for (int i = 0; i < produres_notices.length; i++)
			{
				sb.append("<produres_notice id=\""+produres_notices[i].get("id", 0L)+"\">\n");
				sb.append("<produres_id>"+produres_notices[i].get("produres_id", 0L)+"</produres_id>\n");
				sb.append("<notice_is_display>"+produres_notices[i].get("notice_is_display",0)+"</notice_is_display>\n");
				sb.append("<notice_coloum_name>"+produres_notices[i].getString("notice_coloum_name")+"</notice_coloum_name>\n");
				sb.append("<notice_name>"+produres_notices[i].getString("notice_name")+"</notice_name>\n");
				sb.append("<notice_default_selected>"+produres_notices[i].get("notice_default_selected", 0)+"</notice_default_selected>\n");
				sb.append("<notice_type>"+produres_notices[i].get("notice_type",0)+"</notice_type>\n");
				sb.append("</produres_notice>\n");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</produres_notices>");
		return sb.toString();
	}
	
	/**
	 * 获取流程明细信息
	 * @author Administrator
	 * @param produres
	 * @return
	 * @throws Exception
	 */
	private String produreDetailssArrayToStr(String produres_id_str, String activities_vals, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<produres_details>\n");
		try
		{
			long produres_id	= "".equals(produres_id_str)?-1:Integer.parseInt(produres_id_str);
			int[] activitiesVals = null;
			if(!"".equals(activities_vals))
			{
				String[] activities_vals_satr = activities_vals.split(",");
				activitiesVals = new int[activities_vals_satr.length];
				for (int i = 0; i < activities_vals_satr.length; i++) 
				{
					activitiesVals[i] = Integer.parseInt(activities_vals_satr[i]);
				}
			}
			DBRow[] produres_details = produresMgrZyj.getProduresDetailsByProduresId(produres_id,activitiesVals, null);
			
			for (int i = 0; i < produres_details.length; i++)
			{
				sb.append("<produres_detail id=\""+produres_details[i].get("id", 0L)+"\">\n");
				sb.append("<produres_id>"+produres_details[i].get("produres_id",0L)+"</produres_id>\n");
				sb.append("<activity_name>"+produres_details[i].getString("activity_name")+"</activity_name>\n");
				sb.append("<activity_val>"+produres_details[i].get("activity_val", 0)+"</activity_val>\n");
				sb.append("<val_status>"+produres_details[i].get("val_status",0)+"</val_status>\n");
				sb.append("</produres_detail>\n");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</produres_details>");
		return sb.toString();
	}

	/**
	 * 获取国家数据
	 * @author Administrator
	 * @param countrycodes
	 * @return
	 * @throws Exception
	 */
	private String parseCountrycodesArrayToString(int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<countryCodes>\n");
		try
		{
			DBRow[] countrycodes = orderMgr.getAllCountryCode();
			for (int i = 0; i < countrycodes.length; i++)
			{
				sb.append("<countryCode id=\""+countrycodes[i].get("ccid", 0L)+"\">\n");
				
				sb.append("<c_country>"+countrycodes[i].getString("c_country")+"</c_country>\n");
				sb.append("<c_code>"+countrycodes[i].getString("c_code")+"</c_code>\n");
				sb.append("<fusion_id>"+countrycodes[i].getString("fusion_id")+"</fusion_id>\n");
				sb.append("<map_flash>"+countrycodes[i].getString("map_flash")+"</map_flash>\n");
				sb.append("<sid>"+countrycodes[i].get("sid", 0L)+"</sid>\n");
				sb.append("<cid>"+countrycodes[i].get("cid", 0L)+"</cid>\n");
				
				sb.append("</countryCode>\n");
			}
			
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</countryCodes>");
		return sb.toString();
	}
	
	
	/**
	 * 获取所有省份
	 * @author Administrator
	 * @param ret
	 * @param err
	 * @return
	 * @throws Exception
	 */
	private String parseProvincesArrayToString(int ret, int err) throws Exception
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<provices>");
		try
		{
			DBRow[] provinces = storageCatalogMgrZyj.getStorageAllProvinces();
			for (int i = 0; i < provinces.length; i++)
			{
				sb.append("<provice id=\""+provinces[i].get("pro_id", 0L)+"\">");
				
				sb.append("<pro_name>"+provinces[i].getString("pro_name")+"</pro_name>");
				sb.append("<nation_id>"+provinces[i].get("nation_id", 0L)+"</nation_id>");
				sb.append("<p_code>"+provinces[i].getString("p_code")+"</p_code>");
				sb.append("<fusion_id>"+provinces[i].getString("fusion_id")+"</fusion_id>");
				
				sb.append("</provice>");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</provices>");
		return sb.toString();
	}
	
	/**
	 * 获取人员信息
	 * @author Administrator
	 * @param adminUsers
	 * @return
	 * @throws Exception
	 */
	private String parseAdminUsersArrayToStr(String proJsIdStr, String ps_id_str, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<adminUsers>\n");
		try
		{
			long proJsId = 0;
			if(null != proJsIdStr && !"".equals(proJsIdStr))
			{
				proJsId = Long.parseLong(proJsIdStr);
			}
			long ps_id = 0;
			if(null != ps_id_str && !"".equals(ps_id_str))
			{
				ps_id = Long.parseLong(ps_id_str);
			}
			DBRow[] adminUsers = transportMgrZr.getAllUserAndDept(proJsId, ps_id);
			for (int i = 0; i < adminUsers.length; i++)
			{
				sb.append("<adminUser adid=\""+adminUsers[i].get("adid", 0L)+"\">\n");
				sb.append("<account>"+adminUsers[i].getString("account")+"</account>\n");
				sb.append("<llock>"+adminUsers[i].get("llock",0)+"</llock>\n");
				sb.append("<adgid>"+adminUsers[i].get("adgid",0)+"</adgid>\n");
				sb.append("<employe_name>"+adminUsers[i].getString("employe_name")+"</employe_name>\n");
				sb.append("<ps_id>"+adminUsers[i].get("ps_id", 0L)+"</ps_id>\n");
				sb.append("<email>"+adminUsers[i].getString("email")+"</email>\n");
				sb.append("<skype>"+adminUsers[i].getString("skype")+"</skype>\n");
				sb.append("<msn>"+adminUsers[i].getString("msn")+"</msn>\n");
				sb.append("<AreaId>"+adminUsers[i].get("AreaId", 0L)+"</AreaId>\n");
				sb.append("<mobilePhone>"+adminUsers[i].getString("mobilePhone")+"</mobilePhone>\n");
				sb.append("<proQQ>"+adminUsers[i].getString("proQQ")+"</proQQ>\n");
				sb.append("<proJsId>"+adminUsers[i].get("proJsId", 0L)+"</proJsId>\n");
				sb.append("<file_path>"+adminUsers[i].getString("file_path")+"</file_path>\n");
				sb.append("<adgid_name>"+adminUsers[i].getString("name")+"</adgid_name>\n");
				sb.append("</adminUser>\n");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		sb.append("</adminUsers>");
		return sb.toString();
	}
	
	
	
	
	
	
	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setProduresMgrZyj(ProduresMgrZyjIFace produresMgrZyj) {
		this.produresMgrZyj = produresMgrZyj;
	}

	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr)
	{
		this.transportMgrZr = transportMgrZr;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj)
	{
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}

}
