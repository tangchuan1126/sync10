package com.cwc.service.transport.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.LoadUnloadOccupancyStatusKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetTransportsByRequireAction extends ActionFatherController
{

	private AdminMgrIFaceZJ adminMgrZJ;
	private TransportMgrZyjIFace transportMgrZyj;
	private DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj;
	 
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{

		InputStream inputStream = request.getInputStream();
		int ret = BCSKey.FAIL;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String password  = "";
		String transport_status = "";
		String send_storage_id = "";
		String receive_storage_id = "";
		String purchase_id = "";
		String xml = "";
		String loginAccount = "";
		String isSendOrReceive = "";
		StringBuffer returnDetails = new StringBuffer();
		int postcount = 0;
	 
		try{
			xml					= getPost(inputStream);
			loginAccount		= StringUtil.getSampleNode(xml,"LoginAccount");
			password			= StringUtil.getSampleNode(xml,"Password");
			transport_status 	= StringUtil.getSampleNode(xml, "transportStatus");
			isSendOrReceive		= StringUtil.getSampleNode(xml, "isSendOrReceive");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null){
				throw new MachineUnderFindException();
			}
			long ps_id = login.get("ps_id", 0L);
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			if("1".equals(isSendOrReceive))
			{
				send_storage_id = ""+ps_id;
				receive_storage_id = "0";
			}
			else
			{
				send_storage_id = "0";
				receive_storage_id = ""+ps_id;
			}
			
//			if(fc.equals("GetTransportByMachine")){
				
				//获取转运单信息
				DBRow[] data = transportMgrZyj.getTransportByStatusStroageCount(
						transport_status, parseStringToLong(send_storage_id),
						parseStringToLong(receive_storage_id), parseStringToLong(purchase_id), 20);
				if(data!=null && data.length > 0)
				{
					returnDetails = getCheckInTransportDetails(data);
				}
				
				ret = BCSKey.SUCCESS;
//			}
		
		}catch(MachineUnderFindException e){
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;	//登录失败，账号密码错误
		}finally{
			responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
		}
	}
		
		
		
		private StringBuffer getCheckInTransportDetails(DBRow[] rows) throws Exception{
			StringBuffer details = new StringBuffer("");
			if(rows != null && rows.length > 0){
				for(DBRow temp : rows ){
					if(temp != null){
						int transportNeedLoadReg = 1;//1需要签到，2已签到,3签到了，但是未释放
						DBRow[] locationOccupancysLoad = doorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(0,0,
								ProductStoreBillKey.TRANSPORT_ORDER, temp.get("transport_id",0l) , -LoadUnloadOccupancyStatusKey.QUIT, null, null, null, null,temp.get("receive_psid", 0L),2,TransportRegistrationTypeKey.DELEIVER);
						if(0 != locationOccupancysLoad.length)
			  	  		{
							transportNeedLoadReg = 2;
			  	  			for(int m = 0; m < locationOccupancysLoad.length; m ++)
			  	  			{
			  	  				if(LoadUnloadOccupancyStatusKey.BOOKING == locationOccupancysLoad[m].get("occupancy_status",0))
			  	  				{
			  	  					transportNeedLoadReg = 3;
			  	  					break;
			  	  				}
			  	  			}
			  	  		}
						else
						{
							transportNeedLoadReg = 1;
						}
						details.append("<transport>");
						details.append("<TransportId>").append(temp.get("transport_id", 0l)).append("</TransportId>");
						details.append("<Fromwarehouse>").append(temp.getString("fromwarehouse")).append("</Fromwarehouse>");
						details.append("<Towarehouse>").append(temp.getString("towarehouse")).append("</Towarehouse>");
						details.append("<Towarehouse>").append(temp.getString("towarehouse")).append("</Towarehouse>");
						details.append("<transportNeedLoadReg>"+transportNeedLoadReg+"</transportNeedLoadReg>");
						details.append("</transport>");
					}
				}
			}
			return details ;
		}
		
		
		private String getPost(InputStream inputStream) throws Exception{
			BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
			byte[] buffer = new byte[1024];                                             //数据缓冲区
			int count = 0;                                                            //每个缓冲区的实际数据长度
			ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
			byte[] iXMLData = null;   
			try {
				input = new BufferedInputStream(inputStream);
				while ((count = input.read(buffer)) != -1){
				    streamXML.write(buffer, 0, count);
				} 
			 }catch (Exception e){
				e.printStackTrace();
			 }finally{
				if(input != null){
				    try {
				     input.close();
				    } catch (Exception f) {
				    	f.printStackTrace();
				    }
				 }
			}
			iXMLData = streamXML.toByteArray();  
			return (new String(iXMLData));
		}
		
		
		private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)throws Exception{
			response.setContentType("text/html; charset=UTF-8");		
			StringBuilder boxQtyXml = new StringBuilder();
			boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			boxQtyXml.append("<fc>"+fc+"</fc>");
			boxQtyXml.append("<ret>"+ret+"</ret>");
			boxQtyXml.append("<err>"+err+"</err>");
			boxQtyXml.append("<transports>");
			if(details.length() > 1 )
			{
				boxQtyXml.append(details);
			}
			boxQtyXml.append("</transports>");
			
		 	try {
			   response.setCharacterEncoding("UTF-8");
		
			   OutputStream out = response.getOutputStream();
			   
			   response.setContentLength(boxQtyXml.toString().getBytes("UTF-8").length);                      //添加响应包长度
			   out.write(boxQtyXml.toString().getBytes("UTF-8"));
			   out.flush();
			   out.close();
			   
			}catch (Exception e){
			   e.printStackTrace();
			} 
		}

		
		private int parseStringToInt(String str)
		{
			int reInt = 0;
			if(null == str || "".equals(str.trim()))
			{
				reInt = 0;
			}
			else
			{
				try
				{
					reInt = Integer.parseInt(str);
				}
				catch (NumberFormatException e)
				{
					reInt = 0;
				}
			}
			return reInt;
		}
		
		private long parseStringToLong(String str)
		{
			long reInt = 0;
			if(null == str || "".equals(str.trim()))
			{
				reInt = 0;
			}
			else
			{
				try
				{
					reInt = Integer.parseInt(str);
				}
				catch (NumberFormatException e)
				{
					reInt = 0;
				}
			}
			return reInt;
		}


		public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ)
		{
			this.adminMgrZJ = adminMgrZJ;
		}



		public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj)
		{
			this.transportMgrZyj = transportMgrZyj;
		}

		public void setDoorOrLocationOccupancyMgrZyj(
				DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj)
		{
			this.doorOrLocationOccupancyMgrZyj = doorOrLocationOccupancyMgrZyj;
		}
}
