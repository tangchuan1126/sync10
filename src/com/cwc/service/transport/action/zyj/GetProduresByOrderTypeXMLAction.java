package com.cwc.service.transport.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetProduresByOrderTypeXMLAction extends ActionFatherController{

	private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	private ProduresMgrZyjIFace produresMgrZyj;
	private AdminMgrIFaceZJ adminMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
		String loginAccount = "";
		String password = "";
		String orderTypeId = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow[] produres = new DBRow[0];
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
			password		= StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			orderTypeId		= StringUtil.getSampleNode(xml, "orderTypeId");
			int typeId		= "".equals(orderTypeId)?-1:Integer.parseInt(orderTypeId);
			
			produres = produresMgrZyj.getProduresByOrderType(typeId);
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"GetProduresByOrderTypeXMLAction:"+xml,log);
		}
		finally
		{
			responseXML(produres, response, ret, err);
		}
		
		
	}

	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow[] produres, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<produres>");
		sb.append(storagesArrayToString(produres));
		sb.append("</produres>");
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

	private String storagesArrayToString(DBRow[] produres) throws Exception
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			for (int i = 0; i < produres.length; i++)
			{
				sb.append("<produre produres_id=\""+produres[i].get("produres_id", 0L)+"\">");
				sb.append("<process_key>"+produres[i].get("process_key",0)+"</process_key>");
				sb.append("<process_coloum>"+produres[i].getString("process_coloum")+"</process_coloum>");
				sb.append("<process_coloum_page>"+produres[i].getString("process_coloum_page")+"</process_coloum_page>");
				sb.append("<process_name>"+produres[i].getString("process_name")+" </process_name>");
				sb.append("<process_note_is_display>"+produres[i].get("process_note_is_display", 0)+"</process_note_is_display>");
				sb.append("<process_note>"+produres[i].getString("process_note")+" </process_note>");
				sb.append("<process_person_is_display>"+produres[i].get("process_person_is_display",0)+"</process_person_is_display>");
				sb.append("<process_person>"+produres[i].getString("process_person")+" </process_person>");
				sb.append("<activity_is_need_display>"+produres[i].get("activity_is_need_display", 0)+"</activity_is_need_display>");
				sb.append("<associate_order_type>"+produres[i].get("associate_order_type", 0)+"</associate_order_type>");
				sb.append("<process_start_time_is_create>"+produres[i].get("process_start_time_is_create", 0)+"</process_start_time_is_create>");
				sb.append("<process_period>"+produres[i].get("process_period", 0)+"</process_period>");
				sb.append("<activity_default_selected>"+produres[i].get("activity_default_selected", 0)+"</activity_default_selected>");
				sb.append("</produre>");
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
		}
		return sb.toString();
	}
	

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj) {
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}


	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setProduresMgrZyj(ProduresMgrZyjIFace produresMgrZyj) {
		this.produresMgrZyj = produresMgrZyj;
	}


}
