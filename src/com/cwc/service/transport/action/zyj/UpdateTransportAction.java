package com.cwc.service.transport.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateTransportAction extends ActionFatherController{

	private TransportMgrIFaceZJ transportMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		
		String transport_id = StringUtil.getString(request, "transport_id");
		transportMgrZJ.updateTransportBasic(request);
		int isOutter = StringUtil.getInt(request, "isOutter");
		if(2 == isOutter)
		{
//			String url = ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_basic_update.html?transport_id="+transport_id+"&isOutter=2&isSubmitSuccess=2";
//			throw new RedirectException(url);
			throw new WriteOutResponseException("<script>" +
					"parent.location.reload()" +
					"</script>");
		}
		else
		{
			String backurl = StringUtil.getString(request,"backurl");
			backurl +="?transport_id="+transport_id+"&finished=1";
			throw new RedirectException(backurl);
		}
	}
	
	
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

}
