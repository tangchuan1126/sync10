package com.cwc.service.transport.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.ll.FreightMgrIFaceLL;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetCompanysXMLAction extends ActionFatherController{
	
		private FreightMgrIFaceLL freightMgrLL;
		private AdminMgrIFaceZJ adminMgrZJ;
		static Logger log = Logger.getLogger("ACTION");
		private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
		
		
		@Override
		public void perform(HttpServletRequest request, HttpServletResponse response)
				throws WriteOutResponseException, RedirectRefException,
				ForwardException, Forward2JspException, RedirectBackUrlException,
				RedirectException, OperationNotPermitException,
				PageNotFoundException, DoNothingException, Exception {

			
			response.setContentType(CONTENT_TYPE);		
			InputStream inputStream = request.getInputStream(); //用二进制流接收数据
			String xml = "";
			String loginAccount = "";
			String password = "";
			int ret = BCSKey.SUCCESS;//默认成功
			int err = BCSKey.DATASIZEINCORRECTLY;
			DBRow[] companys = new DBRow[0];
			try 
			{

				xml				= getPost(inputStream);
				loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
				password		= StringUtil.getSampleNode(xml,"Password");
				
				DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
				if(login == null)
				{
					throw new MachineUnderFindException();
				}
				
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAccount(loginAccount);
				adminLoggerBean.setAdgid(login.get("adgid",0l));
				adminLoggerBean.setAdid(login.get("adid",0l));
				adminLoggerBean.setPs_id(login.get("ps_id",0l));
				adminLoggerBean.setEmploye_name(login.getString("employe_name"));
				
				HttpSession session = request.getSession(true);
				session.setAttribute(Config.adminSesion,adminLoggerBean);
				
				companys = freightMgrLL.getCompanys();
				
			}
			catch (XMLDataErrorException e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
			}
			catch (XMLDataLengthException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
			}
			catch(MachineUnderFindException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.LOGINERROR;//登录失败，账号密码错误
			}
			catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
			{
				ret = BCSKey.FAIL;
				err = BCSKey.DATATYPEINCRRECTLY;
			}
			catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
			{
				ret = BCSKey.FAIL;
				err = BCSKey.NETWORKEXCEPTION;
			}
			catch(OperationNotPermitException e)
			{
				ret = BCSKey.FAIL;
				err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
			}
			catch (Exception e) 
			{
				ret = BCSKey.FAIL;
				err = BCSKey.SYSTEMERROR;
				throw new SystemException(e,"GetProduresNoticesByProduresIdXMLAction:"+xml,log);
			}
			finally
			{
				responseXML(companys, response, ret, err);
			}
			
			
		}

		/**
		 * 获得android手机端发送的请求
		 * @param inputStream
		 * @return
		 * @throws Exception
		 */
		private String getPost(InputStream inputStream)
			throws Exception
		{
			BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
			byte[] buffer = new byte[1024];                                             //数据缓冲区
			int count = 0;                                                            //每个缓冲区的实际数据长度
			ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
			byte[] iXMLData = null;   
			try 
			{
				input = new BufferedInputStream(inputStream);
				while ((count = input.read(buffer)) != -1)
				{
				    streamXML.write(buffer, 0, count);
				} 
			 }
			 catch (Exception e)
			 {
				e.printStackTrace();
			 }
			 finally
			 {
				if(input != null)
				{
				    try 
				    {
				     input.close();
				    }
				    catch (Exception f)
				    {
				    	f.printStackTrace();
				    }
				 }
			}
			iXMLData = streamXML.toByteArray();  
			
			return (new String(iXMLData,"UTF-8"));
		}
		
		private void responseXML(DBRow[] companys, HttpServletResponse response, int ret, int err) throws Exception
		{
			StringBuilder sb = new StringBuilder();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.append("<ret>"+ret+"</ret>");
			sb.append("<err>"+err+"</err>");
			sb.append("<companys>");
			sb.append(companysArrayToString(companys));
			sb.append("</companys>");
			
			try 
			{
			   response.setCharacterEncoding("UTF-8");
		
			   OutputStream out = response.getOutputStream();
			   
			   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
			   out.write(sb.toString().getBytes("UTF-8"));
			   out.flush();
			   out.close();
			   
			}
			catch (Exception e)
			{
			   e.printStackTrace();
			} 
		}

		private String companysArrayToString(DBRow[] companys) throws Exception
		{
			StringBuffer sb = new StringBuffer();
			try
			{
				for (int i = 0; i < companys.length; i++)
				{
					sb.append("<company fr_id=\""+companys[i].get("fr_id", 0L)+"\">");
					sb.append("<fr_company>"+companys[i].getString("fr_company")+"</fr_company>");
					sb.append("<fr_way>"+companys[i].get("fr_way",0)+"</fr_way>");
					sb.append("<fr_undertake_company>"+companys[i].getString("fr_undertake_company")+"</fr_undertake_company>");
					sb.append("<fr_from_country>"+companys[i].get("fr_from_country", 0L)+"</fr_from_country>");
					sb.append("<fr_to_country>"+companys[i].get("fr_to_country",0L)+"</fr_to_country>");
					sb.append("<fr_from_port>"+companys[i].getString("fr_from_port")+"</fr_from_port>");
					sb.append("<fr_to_port>"+companys[i].getString("fr_to_port")+"</fr_to_port>");
					sb.append("<fr_contact>"+companys[i].getString("fr_contact")+"</fr_contact>");
					sb.append("<fr_contact_tel>"+companys[i].getString("fr_contact_tel")+"</fr_contact_tel>");
					sb.append("<fr_delivery_address>"+companys[i].getString("fr_delivery_address")+"</fr_delivery_address>");
					sb.append("<fr_remark>"+companys[i].getString("fr_remark")+"</fr_remark>");
					sb.append("</company>");
				}
			}
			catch (Exception e)
			{
				 e.printStackTrace();
			}
			return sb.toString();
		}
		

		public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
			this.adminMgrZJ = adminMgrZJ;
		}

		public void setFreightMgrLL(FreightMgrIFaceLL freightMgrLL) {
			this.freightMgrLL = freightMgrLL;
		}


}
