package com.cwc.service.transport.action.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class GetDetailTransportByIdXMLAction extends ActionFatherController{

	private TransportMgrIFaceZJ transportMgrZJ;
	private AdminMgrIFaceZJ adminMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream(); //用二进制流接收数据
		String xml = "";
		String loginAccount = "";
		String password = "";
		String transport_id_str = "";
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		DBRow transportRow = new DBRow();
		try 
		{

			xml				= getPost(inputStream);
			loginAccount	= StringUtil.getSampleNode(xml,"LoginAccount");
			password		= StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			transport_id_str	= StringUtil.getSampleNode(xml, "transport_id");
			long transport_id	= "".equals(transport_id_str)?-1:Integer.parseInt(transport_id_str);
			
			
			transportRow = transportMgrZJ.getDetailTransportById(transport_id);
			
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"GetProduresDetailsByProduresIdDValsXMLAction:"+xml,log);
		}
		finally
		{
			responseXML(transportRow, response, ret, err);
		}
		
		
	}

	/**
	 * 获得android手机端发送的请求
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	
	private void responseXML(DBRow transportRow, HttpServletResponse response, int ret, int err) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<ret>"+ret+"</ret>");
		sb.append("<err>"+err+"</err>");
		sb.append("<details>");
		if(null != transportRow)
		{
			sb.append("<transport id=\""+transportRow.get("transport_id", 0L)+"\">");
			sb.append("<send_psid>"+transportRow.get("send_psid", 0L)+"</send_psid>");
			sb.append("<receive_psid>"+transportRow.get("receive_psid", 0L)+"</receive_psid>");
			sb.append("<transport_number>"+transportRow.getString("transport_number")+"</transport_number>");
			sb.append("<transport_waybill_number>"+transportRow.getString("transport_waybill_number")+"</transport_waybill_number>");
			sb.append("<transport_waybill_name>"+transportRow.getString("transport_waybill_name")+" </transport_waybill_name>");
			sb.append("<transport_address>"+transportRow.getString("transport_address")+" </transport_address>");
			sb.append("<transport_linkman>"+transportRow.getString("transport_linkman")+" </transport_linkman>");
			sb.append("<transport_linkman_phone>"+transportRow.getString("transport_linkman_phone")+"</transport_linkman_phone>");
			sb.append("<transport_status>"+transportRow.get("transport_status", 0)+"</transport_status>");
			sb.append("<transport_date>"+transportRow.getString("transport_date")+"</transport_date>");
			sb.append("<transport_receive_date>"+transportRow.getString("transport_receive_date")+"</transport_receive_date>");
			sb.append("<transport_send_country>"+transportRow.get("transport_send_country", 0L)+"</transport_send_country>");
			sb.append("<transport_send_place>"+transportRow.getString("transport_send_place")+"</transport_send_place>");
			sb.append("<transport_send_freight>"+transportRow.getString("transport_send_freight")+"</transport_send_freight>");
			sb.append("<transport_receive_country>"+transportRow.get("transport_receive_country", 0L)+"</transport_receive_country>");
			sb.append("<transport_receive_place>"+transportRow.getString("transport_receive_place")+"</transport_receive_place>");
			sb.append("<transport_receive_freight>"+transportRow.getString("transport_receive_freight")+"</transport_receive_freight>");
			sb.append("<create_account_id>"+transportRow.get("create_account_id", 0L)+"</create_account_id>");
			sb.append("<create_account>"+transportRow.getString("create_account")+"</create_account>");
			sb.append("<packing_account>"+transportRow.getString("packing_account")+"</packing_account>");
			sb.append("<remark>"+transportRow.getString("remark")+" </remark>");
			sb.append("<carriers>"+transportRow.getString("carriers")+" </carriers>");
			sb.append("<transportby>"+transportRow.get("transportby", 0)+" </transportby>");
			sb.append("<declaration>"+transportRow.get("declaration", 0)+"</declaration>");
			sb.append("<clearance>"+transportRow.get("clearance", 0)+"</clearance>");
			sb.append("<drawback>"+transportRow.get("drawback", 0)+"</drawback>");
			sb.append("<invoice>"+transportRow.get("invoice", 0)+"</invoice>");
			sb.append("<updatedate>"+transportRow.getString("updatedate")+"</updatedate>");
			sb.append("<updateby>"+transportRow.get("updateby", 0L)+"</updateby>");
			sb.append("<updatename>"+transportRow.getString("updatename")+"</updatename>");
			sb.append("<declaration_over>"+transportRow.get("declaration_over", 0D)+"</declaration_over>");
			sb.append("<clearance_over>"+transportRow.get("clearance_over", 0D)+"</clearance_over>");
			sb.append("<drawback_over>"+transportRow.get("drawback_over", 0D)+"</drawback_over>");
			sb.append("<invoice_over>"+transportRow.get("invoice_over", 0D)+"</invoice_over>");
			sb.append("<all_over>"+transportRow.get("all_over", 0D)+"</all_over>");
			sb.append("<cost>"+transportRow.get("cost", 0D)+"</cost>");
			sb.append("<fr_id>"+transportRow.get("fr_id", 0L)+"</fr_id>");
			sb.append("<stock_in_set>"+transportRow.get("stock_in_set", 0)+"</stock_in_set>");
			sb.append("<send_zip_code>"+transportRow.getString("send_zip_code")+"</send_zip_code>");
			sb.append("<sc_id>"+transportRow.get("sc_id", 0L)+"</sc_id>");
			sb.append("<deliver_ccid>"+transportRow.get("deliver_ccid", 0L)+"</deliver_ccid>");
			sb.append("<deliver_pro_id>"+transportRow.get("deliver_pro_id", 0L)+"</deliver_pro_id>");
			sb.append("<invoice_path>"+transportRow.getString("invoice_path")+"</invoice_path>");
			sb.append("<send_house_number>"+transportRow.getString("send_house_number")+"</send_house_number>");
			sb.append("<send_street>"+transportRow.getString("send_street")+" </send_street>");
			sb.append("<send_address3>"+transportRow.getString("send_address3")+" </send_address3>");
			sb.append("<deliver_house_number>"+transportRow.getString("deliver_house_number")+" </deliver_house_number>");
			sb.append("<deliver_street>"+transportRow.getString("deliver_street")+" </deliver_street>");
			sb.append("<deliver_address3>"+transportRow.getString("deliver_address3")+" </deliver_address3>");
			sb.append("<send_name>"+transportRow.getString("send_name")+" </send_name>");
			sb.append("<send_linkman_phone>"+transportRow.getString("send_linkman_phone")+"</send_linkman_phone>");
			sb.append("<deliver_zip_code>"+transportRow.getString("deliver_zip_code")+"</deliver_zip_code>");
			sb.append("<send_city>"+transportRow.getString("send_city")+"</send_city>");
			sb.append("<deliver_city>"+transportRow.getString("deliver_city")+"</deliver_city>");
			sb.append("<weight>"+transportRow.get("weight", 0F)+"</weight>");
			sb.append("<hs_code>"+transportRow.getString("hs_code")+"</hs_code>");
			sb.append("<pkcount>"+transportRow.get("pkcount", 0)+"</pkcount>");
			sb.append("<send_ccid>"+transportRow.get("send_ccid", 0L)+"</send_ccid>");
			sb.append("<send_pro_id>"+transportRow.get("send_pro_id", 0L)+"</send_pro_id>");
			sb.append("<purchase_id>"+transportRow.get("purchase_id", 0L)+"</purchase_id>");
			sb.append("<from_ps_type>"+transportRow.get("from_ps_type", 0)+"</from_ps_type>");
			sb.append("<target_ps_type>"+transportRow.get("target_ps_type", 0)+"</target_ps_type>");
			sb.append("<packing_date>"+transportRow.getString("packing_date")+"</packing_date>");
			sb.append("<deliveryer_id>"+transportRow.get("deliveryer_id", 0L)+"</deliveryer_id>");
			sb.append("<deliveryed_date>"+transportRow.getString("deliveryed_date")+"</deliveryed_date>");
			sb.append("<warehousinger_id>"+transportRow.get("warehousinger_id", 0L)+"</warehousinger_id>");
			sb.append("<warehousing_date>"+transportRow.getString("warehousing_date")+"</warehousing_date>");
			sb.append("<stock_in_set_over>"+transportRow.get("stock_in_set_over", 0D)+"</stock_in_set_over>");
			sb.append("<tag>"+transportRow.get("tag", 0)+"</tag>");
			sb.append("<certificate>"+transportRow.get("certificate", 0)+"</certificate>");
			sb.append("<tag_over>"+transportRow.get("tag_over", 0D)+"</tag_over>");
			sb.append("<certificate_over>"+transportRow.get("certificate_over", 0D)+"</certificate_over>");
			sb.append("<product_file>"+transportRow.get("product_file", 0)+"</product_file>");
			sb.append("<product_file_over>"+transportRow.get("product_file_over", 0D)+"</product_file_over>");
			sb.append("<quality_inspection>"+transportRow.get("quality_inspection", 0)+"</quality_inspection>");
			sb.append("<quality_inspection_over>"+transportRow.get("quality_inspection_over", 0D)+"</quality_inspection_over>");
			sb.append("<tag_third>"+transportRow.get("tag_third", 0)+"</tag_third>");
			sb.append("<tag_third_over>"+transportRow.get("tag_third_over", 0D)+"</tag_third_over>");
			sb.append("<transport_out_date>"+transportRow.getString("transport_out_date")+"</transport_out_date>");
			sb.append("<address_state_send>"+transportRow.getString("address_state_send")+" </address_state_send>");
			sb.append("<address_state_deliver>"+transportRow.getString("address_state_deliver")+"</address_state_deliver>");
			sb.append("<deleiver_registration>"+transportRow.get("deleiver_registration", 0L)+"</deleiver_registration>");
			sb.append("<deleiver_registration_time>"+transportRow.getString("deleiver_registration_time")+"</deleiver_registration_time>");
			sb.append("<send_registration>"+transportRow.get("send_registration", 0L)+"</send_registration>");
			sb.append("<send_registration_time>"+transportRow.getString("send_registration_time")+"</send_registration_time>");
			sb.append("</transport>");
		}
		sb.append("</details>");
		
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(sb.toString().getBytes("UTF-8").length);                      //添加响应包长度
		   out.write(sb.toString().getBytes("UTF-8"));
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ)
	{
		this.transportMgrZJ = transportMgrZJ;
	}


}
