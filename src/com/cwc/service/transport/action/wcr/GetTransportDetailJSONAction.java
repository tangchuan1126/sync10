package com.cwc.service.transport.action.wcr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.ccc.ComputeProductMgrIFaceCCC;
import com.cwc.app.iface.wcr.OutboundOrderMgrIFaceWCR;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetTransportDetailJSONAction extends ActionFatherController {
	private TransportMgrIFaceZJ transportMgrZJ;
	private ComputeProductMgrIFaceCCC computeProductMgrCCC;
	private SystemConfigIFace systemConfig;
	private TransportMgrIfaceZr transportMgrZr;
	private OutboundOrderMgrIFaceWCR outboundOrderMgrWCR;
	
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	public void setComputeProductMgrCCC(ComputeProductMgrIFaceCCC computeProductMgrCCC) {
		this.computeProductMgrCCC = computeProductMgrCCC;
	}
	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
	public void setOutboundOrderMgrWCR(OutboundOrderMgrIFaceWCR outboundOrderMgrWCR) {
		this.outboundOrderMgrWCR = outboundOrderMgrWCR;
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException, PageNotFoundException, DoNothingException, Exception {
		DBRow[] details = null;
		List<DBRow> rt = new ArrayList<DBRow>();
		FilterBean filterBean = null;
		int pages = StringUtil.getInt(request,"page",1);
		int pageSize = StringUtil.getInt(request,"rows",-1);
		PageCtrl pc = null;
		if(pageSize!=-1) {
			pc = new PageCtrl();
			pc.setPageSize(pageSize);
			pc.setPageNo(pages);
		}
		long transport_id = StringUtil.getLong(request, "transport_id");
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		if (transport != null) {
			details = transportMgrZJ.getTransportDetailByTransportId(transport_id, pc, "transport_detail_id", "asc", filterBean);
			if (details != null && details.length > 0) {
				DBRow sum = computeProductMgrCCC.getSumTransportFreightCost(Long.toString(transport_id));
				double sum_price = sum.get("sum_price", 0d);
				double sum_weight = 0;
				for (DBRow item : details) {
					double count = 0;
					int status = transport.get("transport_status", 0);
					switch (status) {
					case 4:
						count = item.get("transport_reap_count", 0d);
						break;
					default:
						count = item.get("transport_count", 0d);
						break;
					}
					sum_weight += item.get("weight", 0d) * count;
				}
				for (DBRow item : details) {
					double count = 0;
					int status = transport.get("transport_status", 0);
					switch (status) {
					case 4:
						count = item.get("transport_reap_count", 0d);
						break;
					default:
						count = item.get("transport_count", 0d);
						break;
					}
					item.add("freight_cost", sum_price * (item.get("weight", 0d) * count / sum_weight) / count);
					StringBuffer html = new StringBuffer();
					html.append("<span style='cursor: pointer;'>");
					// 显示出来商品文件的个数 和 商品标签的个数
					// 读取配置文件中的配置的数据
					String value = systemConfig.getStringConfigValue("transport_product_file");
					// String file_with_class =
					// StringUtil.getString(request,"file_with_class");
					String[] arraySelected = value.split("\n");
					// 将arraySelected组成一个List
					ArrayList<String> selectedList = new ArrayList<String>();
					for (String tempSelect : arraySelected) {
						if (tempSelect.indexOf("=") != -1) {
							String[] tempArray = tempSelect.split("=");
							String tempHtml = tempArray[1];
							selectedList.add(tempHtml);
						}
					}
					// long pc_id , long fileWithId,int file_with_type
					Map<Integer, DBRow> productFileMap = transportMgrZr.getProductFileAndTagFile(item.get("pc_id", 0l),	transport_id, FileWithTypeKey.product_file);
					for (int indexOfList = 0, countOfList = selectedList.size(); indexOfList < countOfList; indexOfList++) {
						DBRow tempCount = productFileMap.get(indexOfList + 1);
						int tempCountNumber = tempCount != null ? tempCount.get("count", 0) : 0;
						html.append(selectedList.get(indexOfList).trim());
						html.append(":");
						html.append(tempCountNumber);
						if (1 == indexOfList % 2) {
							html.append("<br/>");
						} else {
							html.append("&nbsp;");
						}
					}
					html.append("<br/>");
					// 读取商品标签的个数
					String valueTag = systemConfig.getStringConfigValue("transport_tag_types");
					String[] arraySelectedTag = valueTag.split("\n");
					ArrayList<String> selectedListTag = new ArrayList<String>();
					for (String tempSelect : arraySelectedTag) {
						if (tempSelect.indexOf("=") != -1) {
							String[] tempArray = tempSelect.split("=");
							String tempHtml = tempArray[1];
							selectedListTag.add(tempHtml);
						}
					}
					Map<Integer, DBRow> productTagFileMap = transportMgrZr.getProductFileAndTagFile(item.get("pc_id", 0l), transport_id, FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE);
					for (int indexOfList = 0, countOfList = selectedListTag.size(); indexOfList < countOfList; indexOfList++) {
						DBRow tempCount = productTagFileMap.get(indexOfList + 1);
						int tempCountNumber = null != tempCount ? tempCount.get("count", 0) : 0;
						html.append(selectedListTag.get(indexOfList).trim());
						html.append(":");
						html.append(tempCountNumber);
						if (1 == indexOfList % 2) {
							html.append("<br/>");
						} else {
							html.append("&nbsp;");
						}
					}
					html.append("</span>");
					item.add("button", html.toString());

					// transport_volume, transport_weight, freight_cost
					String volume_weight_freight = "<table border=\"0\" style=\"width:100%\"><tr><td nowrap>体积:" + item.get("transport_volume", 0F) + ",</td><td nowrap>" + "重量:" + item.get("transport_weight", 0F) + ",</td><td nowrap>";
					if (TransportOrderKey.FINISH == item.get("transport_status", 0)) {
						volume_weight_freight += "运费:";
					} else {
						volume_weight_freight += "估算运费:";
					}
					volume_weight_freight += item.get("freight_cost", 0F)+"</td></tr></table>";
					item.add("volume_weight_freight", volume_weight_freight);
					if (item.get("transport_product_serial_number", "").equals("")) item.add("transport_product_serial_number", item.get("transport_product_serial_number", ""));
					
					List<DBRow> containers = new ArrayList<DBRow>();
					DBRow[] outListDetailCLP = outboundOrderMgrWCR.getOutListDetail(item.get("transport_pc_id", 0L), ProductStoreBillKey.TRANSPORT_ORDER, item.get("transport_id", 0l), ContainerTypeKey.CLP, 0L);
					DBRow[] outListDetailBLP = new DBRow[0];//outboundOrderMgrWCR.getOutListDetail(item.get("transport_pc_id", 0L), ProductStoreBillKey.TRANSPORT_ORDER, item.get("transport_id", 0l), ContainerTypeKey.BLP, 0L);
					DBRow[] outListDetailILP = new DBRow[0];//outboundOrderMgrWCR.getOutListDetail(item.get("transport_pc_id", 0L), ProductStoreBillKey.TRANSPORT_ORDER, item.get("transport_id", 0l), ContainerTypeKey.ILP, 0L);//去掉ILP和BLP
					int outListDetailORI = outboundOrderMgrWCR.getOutListDetailOriginalCount(item.get("transport_pc_id", 0L), ProductStoreBillKey.TRANSPORT_ORDER, item.get("transport_id", 0l));
					long container_quantity = 0;
					//获取容器信息
					if (outListDetailCLP.length>0) {
						DBRow clp = new DBRow();
						clp.add("pick_container_type", ContainerTypeKey.CLP);
						clp.add("code", outboundOrderMgrWCR.getLPName(ContainerTypeKey.CLP, outListDetailCLP[0].get("pick_container_type_id", 0L)));
						clp.add("pick_up_quantity", outListDetailCLP.length);
						container_quantity += outListDetailCLP.length;
						containers.add(clp);
					}
					/*
					if (outListDetailBLP.length>0) {
						DBRow blp = new DBRow();
						blp.add("pick_container_type", ContainerTypeKey.BLP);
						blp.add("code", outboundOrderMgrWCR.getLPName(ContainerTypeKey.BLP, outListDetailBLP[0].get("pick_container_type_id", 0L)));
						blp.add("pick_up_quantity", outListDetailBLP.length);
						container_quantity += outListDetailBLP.length;
						containers.add(blp);
					}
					
					if (outListDetailILP.length>0) {
						DBRow ilp = new DBRow();
						ilp.add("pick_container_type", ContainerTypeKey.ILP);
						ilp.add("code", outboundOrderMgrWCR.getLPName(ContainerTypeKey.ILP, outListDetailILP[0].get("pick_container_type_id", 0L)));
						ilp.add("pick_up_quantity", outListDetailILP.length);
						container_quantity += outListDetailILP.length;
						containers.add(ilp);
					}*////去掉ILP和BLP
					//Original 单独获取单独
					if (outListDetailORI>0) {
						DBRow ori = new DBRow();
						ori.add("pick_container_type", ContainerTypeKey.Original);
						ori.add("code", "Original");
						ori.add("pick_up_quantity", outListDetailORI);
						containers.add(ori);
					}
					item.add("container_quantity", container_quantity);
					item.addObject("containers", new JsonObject(containers.toArray(new DBRow[0])));
					rt.add(item);
				}
			}
		}
		DBRow data = new DBRow();
		data.add("page",pages);
		if(pc!=null) {
			data.add("total",pc.getPageCount());//total，总共页数
		} else {
			data.add("total",1);//total，总共页数
		}
		data.add("rows",rt.toArray(new DBRow[0]));//rows，返回数据
		data.add("records",pc.getAllCount());//records，总记录数
		
		throw new JsonException(new JsonObject(data).toString());
	}
}