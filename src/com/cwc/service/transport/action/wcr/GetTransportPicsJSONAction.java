package com.cwc.service.transport.action.wcr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * 获取产品图片Json信息
 * 
 * @author wangcr
 *
 */
public class GetTransportPicsJSONAction extends ActionFatherController {
	
	private TransportMgrIfaceZr transportMgrZr;
	
	
	
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		long transport_id = StringUtil.getLong(request,"transport_id");
		String product_id = StringUtil.getString(request,"product_id");
		//获取运单中该产品的所有类型的图片，按product_file_type区分
		DBRow[] pics = transportMgrZr.getAllProductFileByPcId(product_id, FileWithTypeKey.product_file, transport_id);
		throw new JsonException(new JsonObject(pics));
	}
}