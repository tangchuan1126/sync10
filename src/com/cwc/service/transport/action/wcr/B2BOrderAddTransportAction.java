package com.cwc.service.transport.action.wcr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.wcr.TransportMgrIFaceWCR;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class B2BOrderAddTransportAction extends ActionFatherController{
	
	private TransportMgrIFaceWCR transportMgrWCR;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long transport_id = transportMgrWCR.addB2BOrderTransport(request);
		
		String backurl = StringUtil.getString(request,"backurl");
		
		backurl +="?transport_id="+transport_id;
		
		String result="<script type=\"text/javascript\">"+"parent.window.location.href='"+backurl+"'</script>";
		throw new WriteOutResponseException(result);
		
	}

	public void setTransportMgrWCR(TransportMgrIFaceWCR transportMgrWCR) {
		this.transportMgrWCR = transportMgrWCR;
	}
}
