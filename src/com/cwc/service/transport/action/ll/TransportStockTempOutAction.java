package com.cwc.service.transport.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.transport.TransportStockOutHandleErrorException;
import com.cwc.app.iface.ll.TransportMgrIFaceLL;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class TransportStockTempOutAction  extends ActionFatherController {
	
	private MessageAlerter messageAlert;
	public TransportMgrIFaceZJ transportMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception
	{
		try 
		{
			transportMgrZJ.transportSimulationOut(request);
			throw new RedirectBackUrlException();
		} 
		catch (TransportStockOutHandleErrorException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","TransportStockOutHandleErrorException",""));
		}
		
		throw new RedirectRefException();
	}

	public MessageAlerter getMessageAlert() {
		return messageAlert;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
}