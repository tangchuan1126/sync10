package com.cwc.service.transport.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.iface.ll.TransportMgrIFaceLL;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class TransportApproveCancelAction extends ActionFatherController {
	public TransportMgrIFaceLL transportMgrLL;
	
	public TransportMgrIFaceLL getTransportMgrLL() {
		return transportMgrLL;
	}

	public void setTransportMgrLL(TransportMgrIFaceLL transportMgrLL) {
		this.transportMgrLL = transportMgrLL;
	}

	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception
	{
		String transport_approve_id = StringUtil.getString(request,"transport_approve_id");
		transportMgrLL.transportAproveCancel(transport_approve_id,(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)));
		throw new RedirectBackUrlException();
	}
}
