package com.cwc.service.transport.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.ll.TransportMgrLL;
import com.cwc.app.iface.ll.TransportMgrIFaceLL;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;


public class TransportWayoutUpdateAction extends ActionFatherController {
	public TransportMgrIFaceLL transportMgrLL;
	
	public TransportMgrIFaceLL getTransportMgrLL() {
		return transportMgrLL;
	}

	public void setTransportMgrLL(TransportMgrIFaceLL transportMgrLL) {
		this.transportMgrLL = transportMgrLL;
	}
	
	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception
	{
		transportMgrLL.wayoutTransport(request);
		String transport_id = StringUtil.getString(request,"transport.transport_id");
		int finished = StringUtil.getInt(request,"finished",0);
		if(finished != 1)
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_wayout_update.html?transport_id="+transport_id+"&updated=1");
		else
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/transport/transport_freight_update.html?transport_id="+transport_id+"&finished=1");
	}
}

