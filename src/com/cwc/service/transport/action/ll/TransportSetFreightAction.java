package com.cwc.service.transport.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ll.FreightMgrIFaceLL;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class TransportSetFreightAction extends ActionFatherController {
	private FreightMgrIFaceLL freightMgrLL;

	public FreightMgrIFaceLL getFreightMgrLL() {
		return freightMgrLL;
	}

	public void setFreightMgrLL(FreightMgrIFaceLL freightMgrLL) {
		this.freightMgrLL = freightMgrLL;
	}

	public void perform(HttpServletRequest request, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, Exception
	{
		DBRow result = freightMgrLL.transportSetFreight(request);
		throw new JsonException(new JsonObject(result));
//		throw new RedirectBackUrlException();
	}
}
