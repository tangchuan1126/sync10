package com.cwc.service.transport.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class TransportQualityInspectionAction  extends ActionFatherController{
	
	private TransportMgrIfaceZr transportMgrZr;
	
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			 
		   transportMgrZr.hanleTransportQualityInspection(request);
		   String cmd  = StringUtil.getString(request, "cmd");
		   if(cmd.length() > 0 && cmd.equals("submit")){
			   throw  new RedirectBackUrlException(); 
		 		 
		   }else{
			   DBRow result = new DBRow();
			   result.add("flag", "success");
			   throw new JsonException(new JsonObject(result));
			}
		
	}

}
