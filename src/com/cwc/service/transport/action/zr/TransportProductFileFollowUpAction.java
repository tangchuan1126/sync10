package com.cwc.service.transport.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class TransportProductFileFollowUpAction extends ActionFatherController
{
	private TransportMgrIfaceZr transportMgrZr;
	private TransportMgrZyjIFace transportMgrZyj;
	
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr)
	{
		this.transportMgrZr = transportMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		
		int productFile = StringUtil.getInt(request, "product_file");
		String returnStr = "";
		if(TransportProductFileKey.FINISH == productFile)
		{
			long transport_id = StringUtil.getLong(request, "transport_id");
			returnStr = transportMgrZyj.checkTransportProductFileFinish(transport_id);
		}
		DBRow result = new DBRow();
		if("".equals(returnStr))
		{
			result.add("flag", "success");
			transportMgrZr.transportProductFileFollowUp(request);
		}
		else
		{
			result.add("flag", returnStr);
		}
		throw new JsonException(new JsonObject(result));
	}

	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

}
