package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

/**
 * @author Administrator
 * (这个后面要用存储过程来代替)
 */
public class AddScheduleRepeatAction extends ActionFatherController{

	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			String schedule_id = StringUtil.getString(request,"schedule_id");
			String end = StringUtil.getString(request,"end");
			String start = StringUtil.getString(request,"start");
			String show = StringUtil.getString(request,"show");
			DBRow bean = new DBRow();
			DBRow queryRow = new DBRow();
			queryRow.add("schedule_id", schedule_id);
			queryRow.add("end",end);
			queryRow.add("start", start);
			queryRow.add("show",show);
			DBRow[] repeat = scheduleMgr.addScheduleRepeat(queryRow);
			bean.add("flag", "success");
			bean.add("schedule_repeat", repeat);
			throw new JsonException(new JsonObject(bean));	
	}
 

}
