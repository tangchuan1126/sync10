package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.schedule.ScheduleGenerateException;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
public class AddInventryScheduleAction extends ActionFatherController{
	@Autowired
	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
			try{
				long createAdid = StringUtil.getLong(request, "create_adid");
				String executers = StringUtil.getString(request, "execute_user_ids");
				String startTime = StringUtil.getString(request, "start_time");
				String endTime = StringUtil.getString(request, "end_time");
				long inventry_id = StringUtil.getLong(request, "inventory_id");
				String title = StringUtil.getString(request, "title");
				int inventory_type = StringUtil.getInt(request, "inventory_type");
 
				ScheduleModel scheduleModel =  ScheduleModel.getInventroyScheduleModel(createAdid, executers, inventry_id, title, startTime, endTime,inventory_type);
				long schedule_id = scheduleMgr.addSchedule(scheduleModel);
				result.add("schedule_id", schedule_id);
				result.add("ret", BCSKey.SUCCESS);
 			}catch(ScheduleGenerateException e){
				result.add("ret", BCSKey.FAIL);
				result.add("err", BCSKey.DataFormateException);
			}catch(Exception e){
				result.add("ret", BCSKey.FAIL);
				result.add("err", BCSKey.SYSTEMERROR);
			}
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		 
	}

}
