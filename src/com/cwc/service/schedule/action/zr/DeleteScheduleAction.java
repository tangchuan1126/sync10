package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DeleteScheduleAction extends ActionFatherController{

	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow bean = new DBRow();
			long id = Long.parseLong(StringUtil.getString(request, "id"));
			String flag = StringUtil.getString(request , "flag");
			if(flag.length() > 0){
				// 如果是删除多条任务的话 那么就要是返回很多的Id然后在页面去删除 。如果flag的长度是大于10 表示的是在这个之后的
				// 如果flag == "all"表示删除所有的
				
				DBRow[]  rows = scheduleMgr.deleteMutiRepeatScheduleById(id, flag);
				bean.add("value", rows);
			}else{
				scheduleMgr.deleteScheduleByScheduleId(id, request);
			}
			
			bean.add("flag", "success");
			throw new JsonException(new JsonObject(bean));
	}

}
