package com.cwc.service.schedule.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * @author Administrator(更新所有的属性)
 */
public class UpdateAllScheduleAction extends ActionFatherController{
	
	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			Enumeration<String> enumer = request.getParameterNames();
			DBRow row = new DBRow();
			while(enumer.hasMoreElements()){
				String name = enumer.nextElement();
				row.add(name, StringUtil.getString(request,name));
			}
			long id = Long.parseLong( row.getString("schedule_id"));
			row.remove("schedule_id");
			scheduleMgr.updateSchedule(id, row,request);
			throw new JsonException("success");
	}
 
}
