package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
public class AddFinanceScheduleAction extends ActionFatherController{
	@Autowired
	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			DBRow bean = new DBRow();

//			long id = scheduleMgr.addSchedule(getDBRowFromRequest(request),request);

			long assignUserId = StringUtil.getLong(request, "assignUserId");
			String executeUserId = StringUtil.getString(request, "executeUserId");
			long associateId = StringUtil.getLong(request, "associateId");
			String emailTitle = StringUtil.getString(request, "emailTitle");
			String context = StringUtil.getString(request, "context");
			StringBuilder sb = new StringBuilder();
			if(assignUserId<=0){
				if(sb.length()>5){
					sb.append(",");
				}
				sb.append("任务安排人不能为空");
			}
			if(StringUtil.isBlank(executeUserId)){
				if(sb.length()>5){
					sb.append(",");
				}
				sb.append("任务执行人不能为空");
			}
			if(associateId<=0){
				if(sb.length()>5){
					sb.append(",");
				}
				sb.append("任务的业务ID不能为空");
			}
			if(StringUtil.isBlank(emailTitle)){
				if(sb.length()>5){
					sb.append(",");
				}
				sb.append("任务的标题不能为空");
			}
			if(StringUtil.isBlank(context)){
				if(sb.length()>5){
					sb.append(",");
				}
				sb.append("任务正文不能为空");
			}
			if(sb.length()>5){
				bean.add("flag", "0");
				bean.add("msg", sb);
				
				throw new JsonException(new JsonObject(bean));
			}
			
			
			ScheduleModel sm = ScheduleModel.getFinanceScheduleModel(assignUserId, executeUserId, Long.valueOf(associateId), ModuleKey.Finance, ProcessKey.FinanceApplyPayment, emailTitle, context);
			
			try{
				long id = scheduleMgr.addSchedule(sm);
				bean.add("flag", "1");
				bean.add("msg", "成功");
				bean.add("scheduleid", id);
			}catch(Exception e){
				e.printStackTrace();
				bean.add("flag", "0");
				bean.add("msg", e);
			} 
			throw new JsonException(new JsonObject(bean));
	}

}
