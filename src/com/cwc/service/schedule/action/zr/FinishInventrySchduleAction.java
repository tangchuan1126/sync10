/**  
* @Title: FinishSchduleAction.java
* @Package com.cwc.service.schedule.action.zr
* @Description: TODO(用一句话描述该文件做什么)
* @author A18ccms A18ccms_gmail_com  
* @date 2015年8月6日 下午5:13:01
* @version V1.0  
*/ 
package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

/**
 * @ClassName: FinishSchduleAction
 * @Description: TODO(提供对外的接口完成schdule)
 * @author zhangrui 
 * @date 2015年8月6日 下午5:13:01
 *
 */
public class FinishInventrySchduleAction extends ActionFatherController {

	@Autowired
	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow result = new DBRow();
 			try{
 				int associate_type =  ModuleKey.Inventory; //StringUtil.getInt(request, "associate_type");
 				int associate_process = StringUtil.getInt(request, "inventory_type");
 				long associate_id = StringUtil.getLong(request, "inventory_id");
 				long adid = StringUtil.getLong(request, "adid");
 				scheduleMgr.finishScheduleByAssociateParams(associate_type, associate_process, associate_id, adid);
 				result.add("ret", BCSKey.SUCCESS);
 			}catch(Exception e){
 				result.add("ret", BCSKey.FAIL);
				result.add("err", BCSKey.SYSTEMERROR);
 			}
			throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		
	}

}
