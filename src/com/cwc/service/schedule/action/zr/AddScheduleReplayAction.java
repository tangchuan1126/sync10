package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleReplayMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddScheduleReplayAction extends ActionFatherController{

	private ScheduleReplayMgrIfaceZR scheduleReplayMgrZr;
	
	public void setScheduleReplayMgrZr(ScheduleReplayMgrIfaceZR scheduleReplayMgrZr) {
		this.scheduleReplayMgrZr = scheduleReplayMgrZr;	
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow bean = new DBRow();
			try{
				long id = scheduleReplayMgrZr.addScheduleReplay(getRowFromRequest(request),request);
				bean.add("flag", "success");
				bean.add("entityId", id); 
			}catch (Exception e) {
				bean.add("flag", "failed"); 
			}
			throw new JsonException(new JsonObject(bean));	
	}
	public DBRow getRowFromRequest(HttpServletRequest request){
		DBRow row = new DBRow();
		row.add("sch_replay_context", StringUtil.getString(request,"sch_replay_context"));
		row.add("sch_replay_type", StringUtil.getString(request,"sch_replay_type"));
		row.add("schedule_id", StringUtil.getString(request,"schedule_id"));
		row.add("employe_name",StringUtil.getString(request,"employe_name"));
		row.add("adid",StringUtil.getString(request,"adid"));
		row.add("sch_state", StringUtil.getString(request,"sch_state"));
		row.add("total_state",StringUtil.getString(request,"total_state"));
		row.add("schedule_sub_id", StringUtil.getString(request,"schedule_sub_id"));
		return row;
		
	}

}
