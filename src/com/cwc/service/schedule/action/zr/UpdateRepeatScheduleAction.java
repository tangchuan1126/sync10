package com.cwc.service.schedule.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
/**
 * repeat任务的修改 ,主要是要返回一个新添加的一个结果集 
 * 先简单的考虑一种情况就是 删除改任务以后的所有任务然后在添加新的任务
 * 删除的任务只包含有没有跟进信息的
 * @author Administrator
 *
 */
public class UpdateRepeatScheduleAction extends ActionFatherController{

	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		 
		Enumeration<String> enumer = request.getParameterNames();
		DBRow row = new DBRow();
		DBRow result = new DBRow();
		while(enumer.hasMoreElements()){
			String name = enumer.nextElement();
			row.add(name, StringUtil.getString(request,name));
		}
		long id = Long.parseLong( row.getString("schedule_id"));
		row.remove("schedule_id");
		result.add("value", scheduleMgr.updateRepeatScheduleBy(id, row));	
		result.add("flag","success");
		throw new JsonException(new JsonObject(result));
	}

}
