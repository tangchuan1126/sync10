package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
public class AddScheduleAction extends ActionFatherController{

	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow bean = new DBRow();
			long id = scheduleMgr.addSchedule(getDBRowFromRequest(request),request);
			bean.add("flag", "success");
			bean.add("entityId", id); 
			throw new JsonException(new JsonObject(bean));	
	}
	private DBRow getDBRowFromRequest(HttpServletRequest request) throws Exception {
		DBRow row = new DBRow();
		long assign_user_id = StringUtil.getLong(request, "assign_user_id");
		String execute_user_id = StringUtil.getString(request, "execute_user_id");
		String startTime = StringUtil.getString(request, "startTime");
		String endTime = StringUtil.getString(request,"endTime");
		String schedule_detail = StringUtil.getString(request,"schedule_detail");
		String schedule_is_note = StringUtil.getString(request,"schedule_is_note");
		String is_need_replay = StringUtil.getString(request,"is_need_replay");
		String is_update = StringUtil.getString(request,"is_update");
		String is_all_day = StringUtil.getString(request,"is_all_day");
		String is_schedule = StringUtil.getString(request,"is_schedule");
	    String schedule_overview = StringUtil.getString(request,"schedule_overview"); 
	    String schedule_join_execute_id = StringUtil.getString(request,"schedule_join_execute_id");
	    int sms_email_notify = StringUtil.getInt(request,"sms_email_notify");
	    int sms_short_notify = StringUtil.getInt(request,"sms_short_notify");
	    //repeat schedule
	    String corn_express = StringUtil.getString(request,"corn_express");
	    if(null != corn_express && corn_express.length() > 0){
	    	String repeat_start_time = StringUtil.getString(request,"repeat_start_time");
	    	String repeat_end_time = StringUtil.getString(request,"repeat_end_time");
	    	String repeat_times = StringUtil.getString(request,"repeat_times");
	    	
	    	row.add("corn_express", corn_express);
	    	row.add("repeat_start_time", repeat_start_time);
	    	if(repeat_end_time != null && repeat_end_time.length() > 0){
	    		row.add("repeat_end_time", repeat_end_time);
	    	}
	    	if(repeat_times != null && repeat_times.length() > 0){
	    		row.add("repeat_times", repeat_times);
	    	}	
	    }
		row.add("is_schedule",is_schedule);
		row.add("assign_user_id", assign_user_id);
		row.add("execute_user_id", execute_user_id);
		if(startTime.length() < 1){
			row.add("start_time",new TDate().getCurrentTime().replaceAll("\"", ""));
		}else{
			row.add("start_time",startTime);
		}
		if(endTime.length() < 1){
			row.add("end_time",new TDate().getCurrentTime().replaceAll("\"", ""));
		}else{
			row.add("end_time", endTime);
		}
		
		row.add("schedule_detail",schedule_detail);
		row.add("schedule_is_note",schedule_is_note);
		row.add("is_need_replay",is_need_replay);
		row.add("is_update", is_update);
		row.add("is_all_day", is_all_day);
		row.add("schedule_state","0");
		row.add("schedule_overview", schedule_overview);
		row.add("schedule_join_execute_id",schedule_join_execute_id);
		row.add("sms_short_notify", sms_short_notify);
		row.add("sms_email_notify",sms_email_notify);
		return row;
	}

}
