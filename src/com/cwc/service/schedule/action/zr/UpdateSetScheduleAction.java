package com.cwc.service.schedule.action.zr;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleSetMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateSetScheduleAction extends ActionFatherController{

	private ScheduleSetMgrIfaceZR  scheduleSetMgrIfaceZR;
	 
	public void setScheduleSetMgrIfaceZR(ScheduleSetMgrIfaceZR scheduleSetMgrIfaceZR) {
		this.scheduleSetMgrIfaceZR = scheduleSetMgrIfaceZR;
	}


	@Override
	public void perform(HttpServletRequest requset, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
		{
				Enumeration<String> enumer = requset.getParameterNames();
				DBRow row = new DBRow();
				while(enumer.hasMoreElements()){
					String name = enumer.nextElement();
					row.add(name, StringUtil.getString(requset,name));
				}
				long id = Long.parseLong( row.getString("schedule_set_id"));
				row.remove("schedule_id");
				scheduleSetMgrIfaceZR.updateScheduleSet(id, row);
				throw new JsonException("success");
		}

}
