package com.cwc.service.schedule.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CopyScheduleAction extends ActionFatherController{

	private ScheduleMgrIfaceZR scheduleMgr;
	
	public void setScheduleMgr(ScheduleMgrIfaceZR scheduleMgr) {
		this.scheduleMgr = scheduleMgr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow bean = new DBRow();
			String c = StringUtil.getString(request,"schedule_id");
			long retrunId = scheduleMgr.copySchedule(Long.parseLong(c), getInfoByRequest(request));
			if(0l == retrunId){
				bean.add("flag", "failed");
				bean.add("entityId", 0l);
			}else{
				bean.add("flag", "success");
				bean.add("entityId", retrunId);
			}
			throw new JsonException(new JsonObject(bean));	
	}
	
	public DBRow getInfoByRequest(HttpServletRequest request){
		DBRow row = new DBRow();
		row.add("start_time", StringUtil.getString(request, "start_time"));
		row.add("end_time", StringUtil.getString(request, "end_time"));
		row.add("is_all_day", StringUtil.getString(request, "is_all_day"));
		row.add("is_schedule", StringUtil.getString(request, "is_schedule"));
		return row;
	}
}
