package com.cwc.service.lableTemplate.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.LableTemplateMgrIFaceZJ;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class AddLableTemplateAction extends ActionFatherController {

	private LableTemplateMgrIFaceZJ lableTemplateMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		lableTemplateMgrZJ.addLableTemplate(request);
		throw new RedirectRefException();
	}
	public void setLableTemplateMgrZJ(LableTemplateMgrIFaceZJ lableTemplateMgrZJ) {
		this.lableTemplateMgrZJ = lableTemplateMgrZJ;
	}
}
