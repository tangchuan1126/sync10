package com.cwc.service.lableTemplate.action.zwb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.gql.CustomSeachConditionsIfaceGql;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public class CustomSeachConditionsAction extends ActionFatherController {

	
	private TransactionTemplate txTemplate;
	private ProductLineMgrIfaceZwb productLineMgrIfaceZwb;
	private CustomSeachConditionsIfaceGql flooCustomSeachConditions;
	
	static Logger log = Logger.getLogger("ACTION");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws Exception {

		JSONArray resultlist = new JSONArray();
		
		DBRow[] titleRows=productLineMgrIfaceZwb.selectAllTitle();
		DBRow[] title_list = new DBRow[titleRows.length];
		for(int i=0;i<titleRows.length;i++){
			DBRow row=new DBRow();
			row.add("id",titleRows[i].get("title_id",0l));
			row.add("name",titleRows[i].getString("title_name"));
			row.add("key","title");
			title_list[i]=row;
		}
		Map<String, Object> result_title_row = new HashMap<String, Object>();
		result_title_row.put("key","title");
		result_title_row.put("type","TITLE：");
		result_title_row.put("mulit",false);
		result_title_row.put("url","/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=conditions");
		result_title_row.put("data",DBRowUtils.dbRowArrayAsJSON(title_list));
		resultlist.put(result_title_row);
		
		//仓库
//		Map<String, Object> result_warehouse_row = this.flooCustomSeachConditions.getWarehouseData();
//		resultlist.put(result_warehouse_row);
		
		//商品类型
		Map<String, Object> product_type=this.flooCustomSeachConditions.getProductType();
		resultlist.put(product_type);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
     	JSONObject result = new JSONObject();
     	
		//result.put("datas", resultlist);
		response.getWriter().print(resultlist);
	}
	
	// 查询条件
	public void getConditions(HttpServletRequest request, HttpServletResponse response)
				throws Exception {
			
			JSONObject result = new JSONObject();
			
			String title_id=StringUtil.getString(request, "title");
			String product_line = StringUtil.getString(request, "pcLine");
			String one_catalog=StringUtil.getString(request, "oneCatalog");
			String two_catalog=StringUtil.getString(request, "twoCatalog");
			String three_catalog=StringUtil.getString(request, "threeCatalog");
			int productType=StringUtil.getInt(request, "productType");
			
			String product_catclog="";
			if(!three_catalog.equals("")){  	 //点击3级分类 只查询商品
				product_catclog=three_catalog;
			}else if(!two_catalog.equals("")){   //点击2级分类 查询 3级分类
				product_catclog=two_catalog;
				result=this.flooCustomSeachConditions.getThreeCatalog(product_catclog, title_id, request);				
			}else if(!one_catalog.equals("")){   //点击1级分类查询2级分类
				product_catclog=one_catalog;
				result=this.flooCustomSeachConditions.getTwoCatalog(product_catclog, title_id, request);
			}else if(!product_line.equals("")){  //点击了产品线查询1级分类 
				result=this.flooCustomSeachConditions.getOneCatalog(product_line, title_id, request);
			}else if(!title_id.equals("")){ 								 //点击了title查询
				//获取产品线
				result=this.flooCustomSeachConditions.getProductLineList(title_id,request); 
				
			}		
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(result);
		}
	
	// 根据title条件查询商品及 产品线
	public void doGetProducts(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		JSONObject result_map = new JSONObject();
		
		String title_id=StringUtil.getString(request, "title");
		String product_line = StringUtil.getString(request, "pcLine");
		String one_catalog=StringUtil.getString(request, "oneCatalog");
		String two_catalog=StringUtil.getString(request, "twoCatalog");
		String three_catalog=StringUtil.getString(request, "threeCatalog");
		int productType=StringUtil.getInt(request, "productType");
		
		String product_catclog="";
		if(!three_catalog.equals("")){  	 //点击3级分类 只查询商品
			product_catclog=three_catalog;
		}else if(!two_catalog.equals("")){   //点击2级分类 查询 3级分类
			product_catclog=two_catalog;
		}else if(!one_catalog.equals("")){   //点击1级分类查询2级分类
			product_catclog=one_catalog;
		}else if(!product_line.equals("")){  //点击了产品线查询1级分类 
			
		}else if(!title_id.equals("")){ 								 //点击了title查询
			//获取产品线
			//JSONArray resultlist=this.flooCustomSeachConditions.getProductLineList(title_id,request); 
			//result_map.put("datas", resultlist);
		}
		
		PageCtrl pc = new PageCtrl();
		int pageNo = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageSize"), "1000"));
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		
		//获取商品数据                                                                                 
		DBRow[] product_list=this.flooCustomSeachConditions.getProductList(title_id,product_catclog,product_line,pc,request,productType);  
		if(product_list.length>0 && product_list!=null){
			//result_map.put("products", new JSONObject(StringUtil.convertDBRowsToJsonString(product_list)));
			result_map.put("products", DBRowUtils.dbRowArrayAsJSON(product_list));
		}
 		//分页
		result_map.put("pageCtrl", new JSONObject(pc));
	
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(result_map.toString());
	}
	
	//批量添加 商品模版
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws Exception {
		this.flooCustomSeachConditions.addProductDetailLable(request);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true));
	}
	
	//单个商品添加模版
	public void addSingleDetailLable(HttpServletRequest request, HttpServletResponse response)throws Exception{
		long id=this.flooCustomSeachConditions.addSingleDetailLable(request);
		String detail=String.valueOf(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}
	
	//修改模板
	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		long id=this.flooCustomSeachConditions.updateDetailLableTemplate(row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		if(id>0){
			response.getWriter().print(new JSONObject().put("success", true).toString());
		}else{
			response.getWriter().print(new JSONObject().put("system error", true).toString());
		}
		
	}
	
	//删除 商品与模版关系
	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	
		long relation_id = StringUtil.getLong(request, "relation_id");
		long detail_lable_id = StringUtil.getLong(request, "detail_lable_id");
		
		long id=this.flooCustomSeachConditions.detLableTemplateRelation(relation_id,detail_lable_id);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}
	
	
	
	//根据商品名索引查询商品 
	public void getProductByName(HttpServletRequest request,HttpServletResponse response)throws Exception{
		JSONObject result_map = new JSONObject();
		PageCtrl pc = new PageCtrl();
		int pageNo = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageSize"), "1000"));
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		//获取商品数据                                                                                 
		DBRow[] product_list=this.flooCustomSeachConditions.getProductByName(request, pc);
		
		if(product_list.length>0 && product_list!=null){
			
			result_map.put("products", DBRowUtils.dbRowArrayAsJSON(product_list));
//			result_map.put("products", new JSONObject(StringUtil.convertDBRowsToJsonString(product_list)));
//			result_map.put("products", new JSONArray(product_list));
		}
 		//分页
		result_map.put("pageCtrl", new JSONObject(pc));
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(result_map.toString());
		
	}
	
	//根据模版名字查询模版 删除用
	public void getLableByName(HttpServletRequest request,HttpServletResponse response)throws Exception{
		JSONObject result_map = new JSONObject();
		DBRow[] rows=this.flooCustomSeachConditions.getLableByName(request);
		if(rows.length>0 && rows!=null){
			result_map.put("products", DBRowUtils.dbRowArrayAsJSON(rows));
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(result_map.toString());
	}
	
	//删除商品与模版关系 根据 商品id 和模版id
	public void batchDelete(HttpServletRequest request,HttpServletResponse response)throws Exception{
		this.flooCustomSeachConditions.batchDelete(request);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(new JSONObject().put("success", true).toString());
	}
	
	
	
	
	
	


	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							String search = request.getParameter("search");
							if(search!=null &&"yes".equals(search)){
								doGet(request, response);
							}else if(search!=null&&"seachName".equals(search)){
								getProductByName(request, response);
							}else if("lableName".equals(search)){
								getLableByName(request, response);
							}else if(search.equals("conditions")){
								getConditions(request, response);
							}else{
								doGetProducts(request, response);
							}
							break;
						case "POST":
							String add = request.getParameter("add");
							if(add!=null &&"single".equals(add)){
								addSingleDetailLable(request, response);
							}else{
								doPost(request, response);
							}
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							String del=request.getParameter("del");
							if("batch".equals(del)){
								batchDelete(request, response);
							}else{
								doDelete(request, response);
							}
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}
	}
	

	public ProductLineMgrIfaceZwb getProductLineMgrIfaceZwb() {
		return productLineMgrIfaceZwb;
	}

	public void setProductLineMgrIfaceZwb(
			ProductLineMgrIfaceZwb productLineMgrIfaceZwb) {
		this.productLineMgrIfaceZwb = productLineMgrIfaceZwb;
	}
	
	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public CustomSeachConditionsIfaceGql getFlooCustomSeachConditions() {
		return flooCustomSeachConditions;
	}

	public void setFlooCustomSeachConditions(
			CustomSeachConditionsIfaceGql flooCustomSeachConditions) {
		this.flooCustomSeachConditions = flooCustomSeachConditions;
	}
	

}
