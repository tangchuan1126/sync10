package com.cwc.service.lableTemplate.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.gql.BOLCombinationPrintIfaceGql;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

/**
 * @author Zhengziqi 2014年9月15日
 *
 */
public class BOLCombinationPrintAction extends ActionFatherController {

	private TransactionTemplate txTemplate;
	static Logger log = Logger.getLogger("ACTION");
	private BOLCombinationPrintIfaceGql bolCombinationPrintMgrGql;

	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doBolGet(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// handle params from request
		String printType = StringUtil.getString(request, "printType");
		if(StringUtils.equals("a4print_generic_not_vizio", printType) || StringUtils.equals("a4print", printType)){
			printType = "masterBol";
		}else if(StringUtils.equals("a4print_bol_order", printType) ||  StringUtils.equals("a4print_vics2_order", printType) || StringUtils.equals("a4print_vics2", printType)){
			printType = "bol";
		}
		String pageName = StringUtil.getString(request, "pageName");
		String jsonString = StringUtil.getString(request, "jsonString");
		String out_seal = StringUtil.getString(request, "out_seal");
		long entry_id = StringUtil.getLong(request, "entry_id");
		long adid = StringUtil.getLong(request, "adid");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		adid = (adid == 0l)?adminLoggerBean.getAdid():adid;
		//adid = 100198;
		
		// get data json array from interface
		JSONArray jsons = new JSONArray(jsonString);
		JSONArray bolCombinationPrintJsonArray;
		bolCombinationPrintJsonArray = bolCombinationPrintMgrGql
				.bolCombinationPrintMessage(jsons, out_seal, entry_id, adid, printType, pageName);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject result = new JSONObject();
		result.put("datas", bolCombinationPrintJsonArray);
		response.getWriter().print(result);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doLoadGet(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// handle params from request
		String printType = StringUtil.getString(request, "printType");
		String loadNo = StringUtil.getString(request,"loadNo");
		String orderNo = StringUtil.getString(request,"orderNo");
		String window_check_in_time = StringUtil.getString(request,"window_check_in_time");
		String dockID = StringUtil.getString(request, "DockID");
		String company_name = StringUtil.getString(request, "company_name");
		String gate_container_no = StringUtil.getString(request, "gate_container_no");
		String seal = StringUtil.getString(request, "seal");
		String companyId = StringUtil.getString(request, "CompanyID");
		String customerId = StringUtil.getString(request, "CustomerID");
		String printName=StringUtil.getString(request,"print_name");
		long entryId = StringUtil.getLong(request, "entryId");
		long adid = StringUtil.getLong(request, "adid");
		
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		adid = (adid == 0l)?adminLoggerBean.getAdid():adid;
		//adid = 100198;
		
		JSONObject loadCombinationPrintJsonObject;
		loadCombinationPrintJsonObject = bolCombinationPrintMgrGql
				.loadCombinationPrintMessage(loadNo, orderNo, companyId, customerId, adid, window_check_in_time, printType);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		loadCombinationPrintJsonObject.put("printType", printType);
		loadCombinationPrintJsonObject.put("dockID", dockID);
		loadCombinationPrintJsonObject.put("company_name", company_name);
		loadCombinationPrintJsonObject.put("gate_container_no", gate_container_no);
		loadCombinationPrintJsonObject.put("seal", seal);
		loadCombinationPrintJsonObject.put("printName", printName);
		loadCombinationPrintJsonObject.put("entryId", entryId);
		loadCombinationPrintJsonObject.put("loadNo", loadNo);
		loadCombinationPrintJsonObject.put("orderNo", orderNo);
		loadCombinationPrintJsonObject.put("companyId", companyId);
		loadCombinationPrintJsonObject.put("customerId", customerId);
		JSONObject datas = new JSONObject();
		datas.put("datas", loadCombinationPrintJsonObject);
		response.getWriter().print(datas);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doCountingSheetGet(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		String printType = StringUtil.getString(request, "printType");
		long entry_id = StringUtil.getLong(request, "entryId");
		String jsonString = StringUtil.getString(request, "jsonString");
		JSONArray jsons = new JSONArray(jsonString);
//		boolean isPrint = StringUtil.getInt(request, "isprint") == 1; 
//		String printName=StringUtil.getString(request,"print_name");
		long adid  = StringUtil.getLong(request, "adid");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		adid = (adid == 0l)?adminLoggerBean.getAdid():adid;
		
		// get data json array from interface
		JSONArray countingSheetJSONOject = bolCombinationPrintMgrGql.countingSheetMessage(jsons, entry_id, adid, printType);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject result = new JSONObject();
		result.put("datas", countingSheetJSONOject);
		response.getWriter().print(result);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doPackingGet(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		String printType = StringUtil.getString(request, "printType");
//		boolean isPrint = StringUtil.getInt(request, "isprint") == 1; 
		String jsonString = StringUtil.getString(request, "jsonString");
		JSONArray jsons = new JSONArray(jsonString);
//		String printName=StringUtil.getString(request, "print_name");
		long adid  = StringUtil.getLong(request, "adid");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		adid = (adid == 0l)?adminLoggerBean.getAdid():adid;
		
		// get data json array from interface
		JSONArray packingJSONOject = bolCombinationPrintMgrGql.packingMessage(jsons, adid, printType);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject result = new JSONObject();
		result.put("datas", packingJSONOject);
		response.getWriter().print(result);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doCargoTrackingNoteTicketGet(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		String printType = StringUtil.getString(request, "printType");
		long entryId = StringUtil.getLong(request, "entryId");
	    String bolNo=StringUtil.getString(request, "bol");
	    String ctnNo=StringUtil.getString(request, "ctnr");
	    String door_name=StringUtil.getString(request, "door_name");
	    long adid = StringUtil.getLong(request, "adid");
		String companyId = StringUtil.getString(request, "CompanyID");
	    String customerId = StringUtil.getString(request, "CustomerID");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		adid = (adid == 0l)?adminLoggerBean.getAdid():adid;
		
		// get data json array from interface
		JSONObject cargoTrackingNoteTicketJSONOject = bolCombinationPrintMgrGql.cargoTrackingNoteTicketMessage(door_name, entryId, adid, bolNo, ctnNo, companyId, customerId, printType);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject result = new JSONObject();
		result.put("datas", cargoTrackingNoteTicketJSONOject);
		response.getWriter().print(result);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void doGateCheckGet(HttpServletRequest request, HttpServletResponse response) throws Exception { 
		String printType = StringUtil.getString(request, "printType");
		long infoId=StringUtil.getLong(request,"main_id");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
	    long psId=adminLoggerBean.getPs_id();
		
		// get data json array from interface
		JSONObject gateCheckJSONOject = bolCombinationPrintMgrGql.gateCheckMessage(infoId, psId, printType);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject result = new JSONObject();
		result.put("datas", gateCheckJSONOject);
		response.getWriter().print(result);
	}
	
	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						String printType = StringUtil.getString(request, "printType");
						if(StringUtils.equals("a4print_generic_not_vizio", printType) || StringUtils.equals("a4print", printType) || StringUtils.equals("a4print_bol_order", printType) || StringUtils.equals("a4print_bol", printType) || StringUtils.equals("a4print_vics2_order", printType) || StringUtils.equals("a4print_vics2", printType)){
							doBolGet(request, response);
						}else if(StringUtils.equals("print_order_master_wms", printType) || StringUtils.equals("print_order_no_master_wms", printType) || StringUtils.equals("print_order_no_master_wms_order", printType)){
							doLoadGet(request, response);
						}else if(StringUtils.equals("a4print_counting_sheet_order", printType) || StringUtils.equals("a4print_counting_sheet", printType)){
							doCountingSheetGet(request, response);
						}else if(StringUtils.equals("a4print_packing_list", printType)){
							doPackingGet(request, response);
						}else if(StringUtils.equals("print_receipts_wms_by_bol_container", printType)){
							doCargoTrackingNoteTicketGet(request, response);
						}else if(StringUtils.equals("check_in_open_print", printType)){
							doGateCheckGet(request, response);
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false)
							.put("error", e.getMessage()).toString());
		}
	}

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public BOLCombinationPrintIfaceGql getBolCombinationPrintMgrGql() {
		return bolCombinationPrintMgrGql;
	}

	public void setBolCombinationPrintMgrGql(
			BOLCombinationPrintIfaceGql bolCombinationPrintMgrGql) {
		this.bolCombinationPrintMgrGql = bolCombinationPrintMgrGql;
	}

}
