package com.cwc.service.lableTemplate.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zr.ClpTypeMgrIfaceZr;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONObject;

/**
 * @author 	Zhengziqi
 * 2014年10月15日
 *
 */
public class AquireContainerMessagePrintAction extends ActionFatherController {

		
	private ClpTypeMgrIfaceZr clpTypeMgrZr;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;

	public ClpTypeMgrIfaceZr getClpTypeMgrZr() {
		return clpTypeMgrZr;
	}

	public void setClpTypeMgrZr(ClpTypeMgrIfaceZr clpTypeMgrZr) {
		this.clpTypeMgrZr = clpTypeMgrZr;
	}

	public ProprietaryMgrIFaceZyj getProprietaryMgrZyj() {
		return proprietaryMgrZyj;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long id=StringUtil.getLong(request,"id");
		long detail_type = StringUtil.getInt(request, "detail_type");
		//获取登录人
		long login_id = StringUtil.getLong(request, "adid");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		login_id = (login_id == 0l)?adminLoggerBean.getAdid():login_id;
		if(login_id==100198){
			login_id=0;
		}
		
		String supplier = null;	
		String piece = null;
		String packeges = null;
		JSONObject datas = new JSONObject();
		datas.put("DATE", new TDate().getFormateTime(DateUtil.NowStr(), "yy-MM-dd"));
		datas.put("CUSTOMER", "&nbsp;");
		
		DBRow row = clpTypeMgrZr.findContainerById(id);	
		datas.put("LOT", row.getString("lot_number"));
		datas.put("PCID", row.getString("container"));
		
		DBRow titleRow=proprietaryMgrZyj.findProprietaryByTitleId(row.get("title_id",0l));
		supplier = (null!=titleRow)?titleRow.getString("title_name"):"&nbsp;";	
		datas.put("SUPPLIER", supplier);
		
		if(detail_type == 3){
			DBRow ilp=clpTypeMgrZr.findIlpTypeProductByIlpTypeId(row.get("type_id",0l));
			datas.put("TYPEID", ilp.get("ibt_id",0l));
			datas.put("PID", ilp.get("ibt_pc_id",0l));
			datas.put("NAME", ilp.getString("p_name"));					
			datas.put("TOTPDT", ilp.get("ibt_total",0l));
			datas.put("TOTPKG", ilp.get("ibt_total",0l));
			datas.put("PIECE", "Product");
			datas.put("PACKAGES", ilp.get("ibt_total_length",0l)+"&nbsp;*&nbsp;"+ilp.get("ibt_total_width",0l)+"&nbsp;*&nbsp;"+ilp.get("ibt_total_height",0l));
		}else if(detail_type == 2){
			long type_id = row.get("type_id",0l);
			DBRow blp=clpTypeMgrZr.findBlpTypeProductByBlpTypeId(type_id); 		
			//Piece
			DBRow ilpRow=clpTypeMgrZr.selectContainerIlpById(blp.get("box_inner_type",0l));
			piece = (blp.get("box_inner_type",0l)==0)?"Product":("ILP&nbsp;"+ilpRow.get("ibt_total_length",0l)+"X"+ilpRow.get("ibt_total_length",0l)+"X"+ilpRow.get("ibt_total_height",0l));
			//Packeges
			packeges = blp.get("box_total_length",0l) + "X" + blp.get("box_total_width",0l) + "X" + blp.get("box_total_height",0l);
			
			datas.put("TYPEID", blp.get("box_type_id",0l));
			datas.put("PID", blp.get("box_pc_id",0l));
			datas.put("NAME", blp.getString("p_name"));
			datas.put("TOTPDT", blp.get("box_total_piece",0l));
			datas.put("TOTPKG", blp.get("box_total",0l));
			datas.put("PIECE", piece);
			datas.put("PACKAGES", packeges);
		}else if(detail_type == ContainerTypeKey.CLP){
			DBRow clp=clpTypeMgrZr.selectContainerClpById(row.get("type_id",0l)); 
			supplier = (null!=titleRow)?titleRow.getString("title_name"):"&nbsp;";
			String total_package = clp.get("stack_length_qty", 0l) + "*" + clp.get("stack_width_qty", 0l) + "*" + clp.get("stack_height_qty", 0l);
			DBRow productCode = proprietaryMgrZyj.findDetailProductByPcId(clp.get("pc_id",0l));
			datas.put("TYPEID", StringUtil.isBlank(clp.getString("type_name"))?"&nbsp;":clp.getString("type_name"));
			datas.put("CLPTYPE", StringUtil.isBlank(clp.getString("lp_name"))?"&nbsp;":clp.getString("lp_name"));
			datas.put("TOTPKG",total_package);
			datas.put("LOT",StringUtil.isBlank(row.getString("lot_number"))?"&nbsp;":row.getString("lot_number"));
			datas.put("TOTPDT",clp.get("inner_total_pc", 0l));
			datas.put("CONID", StringUtil.isBlank(row.getString("container"))?"&nbsp;":row.getString("container"));
			datas.put("PCODE", StringUtil.isBlank(productCode.getString("p_code"))?"&nbsp;":productCode.getString("p_code"));
			datas.put("TITLE", supplier);
		}
		throw new JsonException(datas);
	}

}
