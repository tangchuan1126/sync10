package com.cwc.service.lableTemplate.action.zwb;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import us.monoid.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.ProductLableTempMgrIfaceWFH;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.LableTemplateProductCommonKey;
import com.cwc.app.key.LableTemplateProductKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.javaps.test.TestUtils;

public class ProdutLableTemplateAction extends ActionFatherController {
	private TransactionTemplate txTemplate;
	private ProductLableTempMgrIfaceWFH productLableTemp;
	static Logger log = Logger.getLogger("ACTION");
	
	
	private void doGet(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PageCtrl pc = new PageCtrl();
		int pageNo = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageNo"), "1"));
		int pageSize = Integer.parseInt(StringUtils.defaultString(
				request.getParameter("pageSize"), "1000"));
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		
		String lableName = StringUtil.getString(request, "lablename");
//		String lableType = StringUtil.getString(request, "labletype");
		int templateType = StringUtil.getInt(request, "templateType"); //商品、容器模板类型
		String type = StringUtil.getString(request, "type"); //容器标签类型，containerTypeKey();产品添加模板时，只有clp;
		DBRow[] rows = productLableTemp.findAllLable(lableName,type,templateType,pc); 
		
		//获取类型key
		LableTemplateProductKey lableKey = new LableTemplateProductKey();
		ArrayList typeKey =  lableKey.getLableTemplates();
		DBRow[] typeKeyRows = this.getTypeKey(typeKey, lableKey);
		
//		LableTemplateContainerKey containerKey = new LableTemplateContainerKey();
//		ArrayList containerTypeKey =  containerKey.getLableTemplates();
		//容器类型只有clp/、tlp
		ContainerTypeKey containerKey = new ContainerTypeKey();
		ArrayList containerTypeKey =  containerKey.getContainerTypeKeys();
		ArrayList<DBRow> ctnrList =  new ArrayList<DBRow>();
		for(int i=0;i<containerTypeKey.size();i++){
			int ctnrKey = Integer.parseInt((String)containerTypeKey.get(i));
			if(ctnrKey!=0){
				String ctnrValue = containerKey.getContainerTypeKeyValue(ctnrKey);
				DBRow ctnType = new DBRow();
				ctnType.add("key", ctnrKey);
				ctnType.add("value", ctnrValue);
				ctnrList.add(ctnType);
			}
		}
		DBRow[] containerTypeKeyRows = ctnrList.toArray(new DBRow[ctnrList.size()]);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
     	JSONObject result = new JSONObject();
     	
		result.put("lables", DBRowUtils.dbRowArrayAsJSON(rows));
		result.put("pageCtrl", new JSONObject(pc));
		result.put("typeKey", DBRowUtils.dbRowArrayAsJSON(typeKeyRows));
		result.put("containerTypeKey", DBRowUtils.dbRowArrayAsJSON(containerTypeKeyRows));
		
		response.getWriter().print(result);
	}
	
	//获取 LableTemplateProductKey
	public DBRow[] getTypeKey(ArrayList typeKey, LableTemplateProductCommonKey lableKey)throws Exception{
		try{			
			DBRow[] typeKeyRows = new DBRow[typeKey.size()];
			for(int i=0;i<typeKey.size();i++){
				DBRow row=new DBRow();
				int key = Integer.parseInt(typeKey.get(i).toString());
				row.add("key",key);
				row.add("value",lableKey.getLableTemplateNameById(key));
				typeKeyRows[i]=row;
			}
			return typeKeyRows;	
		}catch(Exception e){
			throw new SystemException(e,"getProductLineList",log);
		}
	}
	
	// 新建基础标签模板
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JSONObject jsonData = new JSONObject(IOUtils.toString(request
				.getReader()));
		DBRow row = DBRowUtils.convertToDBRow(jsonData);
		long adid = this.productLableTemp.addLableTemp(row);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject(DBRowUtils.dbRowAsMap(this.productLableTemp
						.getLableTempById(adid))).toString());
	}
	//修改
	public void doPut(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				JSONObject jsonData = new JSONObject(IOUtils.toString(request
						.getReader()));
//				//system.out.println("修改： jsonData=" + jsonData);
				DBRow row = DBRowUtils.convertToDBRow(jsonData);
//				//system.out.println("修改"+TestUtils.dbRowAsString(row));
				long adid = row.get("LABLE_ID", 0L);
//				//system.out.println("lableid="+adid);
				this.productLableTemp.modifyLableTemp(adid, row);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				DBRow detail = this.productLableTemp.getLableTempById(adid);
				
				response.setStatus(detail==null?500:200);
				response.getWriter().print(
					detail ==  null ? new JSONObject().put("success", false).put("error","修改失败："+adid) :
					new JSONObject(DBRowUtils.dbRowAsMap(detail))
				);
	
	}

	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		this.productLableTemp.delLabelTemp(Long.parseLong(request.getParameter("id")));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(
				new JSONObject().put("success", true).toString());
	}
	@Override
	public void perform(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		try {
			txTemplate.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try {
						switch (request.getMethod()) {
						case "GET":
							doGet(request, response);
							break;
						case "POST":
							doPost(request, response);
							break;
						case "PUT":
							doPut(request, response);
							break;
						case "DELETE":
							doDelete(request, response);
							break;
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("application/json");
			response.setStatus(500);
			response.getWriter().print(
					new JSONObject().put("success", false).put("error", e.getMessage()).toString());
		}

	}

	public TransactionTemplate getTxTemplate() {
		return txTemplate;
	}

	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.txTemplate = txTemplate;
	}

	public ProductLableTempMgrIfaceWFH getProductLableTemp() {
		return productLableTemp;
	}

	public void setProductLableTemp(ProductLableTempMgrIfaceWFH productLableTemp) {
		this.productLableTemp = productLableTemp;
	}
	
	

}
