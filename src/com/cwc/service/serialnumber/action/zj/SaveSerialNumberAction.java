package com.cwc.service.serialnumber.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.serialnumber.NumberIndexOfException;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class SaveSerialNumberAction extends ActionFatherController {

	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow dbrow = new DBRow();
		try 
		{
			serialNumberMgrZJ.addSerialNumber(request);
			dbrow.add("result","success");
		}
		catch (NumberIndexOfException e) 
		{
			dbrow.add("result","error");
		}
		throw new JsonException(new JsonObject(dbrow));
	}
	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ) {
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}

}
