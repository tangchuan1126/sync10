package com.cwc.service.serialnumber.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.serialnumber.NumberIndexOfException;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class MadeSerialNumberAction extends ActionFatherController {

	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	private MessageAlerter messageAlert;

	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		DBRow serialNumber = new DBRow();
		try 
		{
			serialNumber  = serialNumberMgrZJ.madeSerialNumber(request);
			serialNumber.add("result","success");
		} 
		catch (NumberIndexOfException e) 
		{
			serialNumber.add("result","error");
		}
		throw new JsonException(new JsonObject(serialNumber));
	}

	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ) {
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
