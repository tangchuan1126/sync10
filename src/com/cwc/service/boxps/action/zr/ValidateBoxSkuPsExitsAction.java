package com.cwc.service.boxps.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.BoxSkuPsMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ValidateBoxSkuPsExitsAction extends ActionFatherController  {
	
	private BoxSkuPsMgrIfaceZr boxSkuPsMgrZr ;
	
	
	public void setBoxSkuPsMgrZr(BoxSkuPsMgrIfaceZr boxSkuPsMgrZr) {
		this.boxSkuPsMgrZr = boxSkuPsMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
			DBRow result = new DBRow();
			boolean flag = boxSkuPsMgrZr.validateBoxSkuPsExits(request);
			result.add("flag", flag+"");
			throw new JsonException(new JsonObject(result));
		
	}

}
