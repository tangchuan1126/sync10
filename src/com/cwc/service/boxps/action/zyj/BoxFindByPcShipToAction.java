package com.cwc.service.boxps.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.ContainerMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class BoxFindByPcShipToAction extends ActionFatherController{

	private ContainerMgrIFaceZyj containerMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long pc_id = StringUtil.getLong(request, "pc_id");
		long ship_to = StringUtil.getLong(request, "ship_to");
		DBRow[] result = containerMgrZyj.getBoxSkuByPcId(pc_id, ship_to);
		throw new JsonException(new JsonObject(result));
	}

	public void setContainerMgrZyj(ContainerMgrIFaceZyj containerMgrZyj) {
		this.containerMgrZyj = containerMgrZyj;
	}
}
