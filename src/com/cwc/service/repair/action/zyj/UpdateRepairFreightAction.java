package com.cwc.service.repair.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.RepairOrderMgrZyjIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateRepairFreightAction extends ActionFatherController{

	private RepairOrderMgrZyjIFace repairOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		repairOrderMgrZyj.updateRepairFreight(request);
		String repair_order_id = StringUtil.getString(request,"repair_order.repair_order_id");
		int isOutter = StringUtil.getInt(request, "isOutter");
		String fr_id = StringUtil.getString(request,"repair_order.fr_id");
		
		if(isOutter == 2)
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/repair/repair_freight_update.html?repair_order_id="+repair_order_id+"&updated=1");
		else
			throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/repair/setRepairFreight.html?repair_order_id="+repair_order_id+"&fr_id="+fr_id);
		
	}

	public void setRepairOrderMgrZyj(RepairOrderMgrZyjIFace repairOrderMgrZyj) {
		this.repairOrderMgrZyj = repairOrderMgrZyj;
	}

}
