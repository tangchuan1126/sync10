package com.cwc.service.repair.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.transport.TransportStockInHandleErrorException;
import com.cwc.app.iface.zyj.RepairOrderMgrZyjIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;

public class RepairStockTempInAction extends ActionFatherController{

	private RepairOrderMgrZyjIFace repairOrderMgrZyj;
	private MessageAlerter messageAlert;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		try 
		{
			repairOrderMgrZyj.repairStockTempIn(request);
			
			throw new RedirectBackUrlException();
		} 
		catch (TransportStockInHandleErrorException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","TransportStockInHandleErrorException",""));
		}
		throw new RedirectRefException();
	}

	public void setRepairOrderMgrZyj(RepairOrderMgrZyjIFace repairOrderMgrZyj) {
		this.repairOrderMgrZyj = repairOrderMgrZyj;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
