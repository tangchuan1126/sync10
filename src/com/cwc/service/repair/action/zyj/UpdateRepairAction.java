package com.cwc.service.repair.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zyj.RepairOrderMgrZyjIFace;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdateRepairAction extends ActionFatherController{

	private RepairOrderMgrZyjIFace repairOrderMgrZyj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		String repair_order_id = StringUtil.getString(request, "repair_order_id");
		repairOrderMgrZyj.updateRepairBasic(request);
		int isOutter = StringUtil.getInt(request, "isOutter");
		if(2 == isOutter)
		{
			throw new WriteOutResponseException("<script>" +
					"parent.location.reload()" +
					"</script>");
		}
		else
		{
			String backurl = StringUtil.getString(request,"backurl");
			backurl +="?repair_order_id="+repair_order_id+"&finished=1";
			throw new RedirectException(backurl);
		}
	}

	public void setRepairOrderMgrZyj(RepairOrderMgrZyjIFace repairOrderMgrZyj) {
		this.repairOrderMgrZyj = repairOrderMgrZyj;
	}

}
