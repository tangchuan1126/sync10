package com.cwc.service.question.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.QuestionMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class KeyWorldSuggestAction extends ActionFatherController
{

    private QuestionMgrIfaceZr questionMgrZr ;
	
	
	public void setQuestionMgrZr(QuestionMgrIfaceZr questionMgrZr)
	{
		this.questionMgrZr = questionMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception
	{
		 String keyWorld = StringUtil.getString(request, "key_world");
		 DBRow[] rows = questionMgrZr.fileIndexSuggest(keyWorld);
		 throw new JsonException(new JsonObject(rows));
	}

}
