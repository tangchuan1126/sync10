package com.cwc.service.question.action.zwb;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxQuestionAppendixAction extends ActionFatherController {

	private FileMgrIfaceZr fileMgrZr;
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {

		    long file_with_id= StringUtil.getLong(request,"catalogId");
		    DBRow[] row=this.fileMgrZr.getFilesByFileWithIdAndFileWithType(file_with_id, FileWithTypeKey.KNOWLEDGE);
		    for(int i=0;i<row.length;i++){
		    	String fileName=row[i].getString("file_name");
		    	//查询出附件内容  没有返回  预备以后用
		    	String fileContent=this.questionCatalogMgrZwb.selectAppendixContent(fileName);
		    }
		    
		    throw new JsonException(new JsonObject(row));
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}

	public void setQuestionCatalogMgrZwb(
			QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}
	
	

}
