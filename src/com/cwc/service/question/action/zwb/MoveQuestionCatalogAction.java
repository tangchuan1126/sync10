package com.cwc.service.question.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class MoveQuestionCatalogAction extends ActionFatherController{
	
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
    
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	     
		String message=this.questionCatalogMgrZwb.moveQuestionCatalog(request);
		DBRow  result = new DBRow();
		result.add("flag", message);
		throw new JsonException(new JsonObject(result));
	}

	public void setQuestionCatalogMgrZwb(
			QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}
		
}
