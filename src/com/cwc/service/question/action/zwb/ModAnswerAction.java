package com.cwc.service.question.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class ModAnswerAction extends ActionFatherController{
	
		private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
	    
		public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,
		ForwardException, Forward2JspException, RedirectBackUrlException,
		RedirectException, OperationNotPermitException,
		PageNotFoundException, DoNothingException, Exception {
		     
			this.questionCatalogMgrZwb.modAnswer(request);
			throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");
			
		}

		public void setQuestionCatalogMgrZwb(
				QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
			this.questionCatalogMgrZwb = questionCatalogMgrZwb;
		}
			
	}

