package com.cwc.service.question.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetQuestionByInfoAction extends ActionFatherController{
	
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
    
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
		String title = StringUtil.getString(request,"title");
		String question_title = StringUtil.getString(request,"question_title");
		String question_describe = StringUtil.getString(request,"question_describe");
		String appendix = StringUtil.getString(request,"appendix");
		String search_name = StringUtil.getString(request,"search_name");
		long product_id = StringUtil.getLong(request,"product_id");
		long product_catalog_id = StringUtil.getLong(request,"product_catalog_id");
		String pLineClick = StringUtil.getString(request,"pLineClick");
		
		throw new JsonException(new JsonObject(null));
	}
	
	public void setQuestionCatalogMgrZwb(
			QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}
	
}
