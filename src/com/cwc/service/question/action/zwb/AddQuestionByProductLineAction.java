package com.cwc.service.question.action.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AddQuestionByProductLineAction extends ActionFatherController{
	
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
    
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
	throws WriteOutResponseException, RedirectRefException,
	ForwardException, Forward2JspException, RedirectBackUrlException,
	RedirectException, OperationNotPermitException,
	PageNotFoundException, DoNothingException, Exception {
	     
		this.questionCatalogMgrZwb.addQuestionByProductLine(request);
		DBRow  result = new DBRow();
		result.add("flag", "success");
		throw new JsonException(new JsonObject(result));
		//throw new WriteOutResponseException("<script>parent.closeWinRefresh();</script>");
		
	}
	
	public void setQuestionCatalogMgrZwb(
			QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) {
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}
	
}
