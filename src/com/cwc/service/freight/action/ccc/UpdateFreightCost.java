package com.cwc.service.freight.action.ccc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ccc.FreightCostMgrIFaceCCC;
import com.cwc.app.iface.ccc.FreightResourcesMgrIFaceCCC;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class UpdateFreightCost extends ActionFatherController {

	private FreightCostMgrIFaceCCC freightCostMgrCCC;
	
	public FreightCostMgrIFaceCCC getFreightCostMgrCCC() {
		return freightCostMgrCCC;
	}

	public void setFreightCostMgrCCC(FreightCostMgrIFaceCCC freightCostMgrCCC) {
		this.freightCostMgrCCC = freightCostMgrCCC;
	}

	@Override
	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		long fc_id = this.freightCostMgrCCC.updateFreightCost(arg0);
		throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/freight_cost/updateFreightCostAction.html?updated=1&fc_id="+fc_id);
		
	}

}
