package com.cwc.service.freight.action.ccc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ccc.FreightResourcesMgrIFaceCCC;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

public class AddFreightResources extends ActionFatherController {

	private FreightResourcesMgrIFaceCCC freightResourcesMgrCCC;
	

	public FreightResourcesMgrIFaceCCC getFreightResourcesMgrCCC() {
		return freightResourcesMgrCCC;
	}


	public void setFreightResourcesMgrCCC(
			FreightResourcesMgrIFaceCCC freightResourcesMgrCCC) {
		this.freightResourcesMgrCCC = freightResourcesMgrCCC;
	}


	@Override
	public void perform(HttpServletRequest arg0, HttpServletResponse arg1)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		this.freightResourcesMgrCCC.addFreightResources(arg0);
		throw new RedirectException(ConfigBean.getStringValue("systenFolder")+"administrator/freight_resources/addFreightResourcesAction.html?added=1");
		
	}

}
