package com.cwc.service.freight.action.ccc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ccc.FreightCostMgrIFaceCCC;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ChangeFreightIndexJSONAction extends ActionFatherController {

	private FreightCostMgrIFaceCCC freightCostMgrCCC;

	public FreightCostMgrIFaceCCC getFreightCostMgrCCC() {
		return freightCostMgrCCC;
	}

	public void setFreightCostMgrCCC(FreightCostMgrIFaceCCC freightCostMgrCCC) {
		this.freightCostMgrCCC = freightCostMgrCCC;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		DBRow row = new DBRow();
		row.add("return", freightCostMgrCCC.changeFreightIndex(request));
		throw new JsonException(new JsonObject(row));
	}

}
