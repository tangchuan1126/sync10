package com.cwc.service.productStorage.action.ll;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.ll.StorageIFaceMgrLL;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class SaveStorageCostAction extends ActionFatherController {
	private StorageIFaceMgrLL storageMgrLL;

	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		// TODO Auto-generated method stub
		storageMgrLL.saveImportStorageCost(request);
		
		DBRow db = new DBRow();
		
		db.add("close", "true");
		
		throw new JsonException(new JsonObject(db));
		
	}

	public StorageIFaceMgrLL getStorageMgrLL() {
		return storageMgrLL;
	}

	public void setStorageMgrLL(StorageIFaceMgrLL storageMgrLL) {
		this.storageMgrLL = storageMgrLL;
	}

}