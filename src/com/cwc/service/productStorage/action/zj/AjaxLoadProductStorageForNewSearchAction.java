package com.cwc.service.productStorage.action.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class AjaxLoadProductStorageForNewSearchAction extends ActionFatherController {

	private ProductStorageMgrIFaceZJ productStorageMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,	PageNotFoundException, DoNothingException, Exception 
	{
		
		DBRow[] storageCatalogs = productStorageMgrZJ.searchProductStorageCatalog(0,0,0,0,null);
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++) 
		{
			DBRow storage = new DBRow();
			storage.add("id",storageCatalogs[i].get("id",0l));
			storage.add("name",storageCatalogs[i].getString("title"));
			
			list.add(storage);
		}
		
		throw new JsonException(new JsonObject(list.toArray(new DBRow[0])));
	}
	
	public void setProductStorageMgrZJ(ProductStorageMgrIFaceZJ productStorageMgrZJ) {
		this.productStorageMgrZJ = productStorageMgrZJ;
	}

}
