package com.cwc.service.productStorage.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.app.iface.zj.ProductStorageHistoryMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ExportProductStorageHistoryAction extends ActionFatherController 
{
	private ProductStorageHistoryMgrIFaceZJ productStorageHistoryMgZJ;


	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException,RedirectRefException, ForwardException,Forward2JspException, RedirectBackUrlException, RedirectException,OperationNotPermitException, Exception
	{
		
		
		try
		{
			String path = productStorageHistoryMgZJ.exportProductStorageHistory(request);
			throw new WriteOutResponseException(path);
		}
		catch (WareHouseErrorException e)
		{
			throw new WriteOutResponseException("WareHouseErrorException");
		}
		
	}


	public void setProductStorageHistoryMgZJ(
			ProductStorageHistoryMgrIFaceZJ productStorageHistoryMgZJ) {
		this.productStorageHistoryMgZJ = productStorageHistoryMgZJ;
	}

	
}
