package com.cwc.service.productStorage.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class DownLoadProductStorageAction extends ActionFatherController {

	private ProductStorageMgrIFaceZJ productStorageMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		String path = "../../"+ productStorageMgrZJ.downLoadProductStorage(request);
		DBRow dbrow = new DBRow();
		
		dbrow.add("canexport",true);
		dbrow.add("fileurl",path);
		
		throw new JsonException(new JsonObject(dbrow));
	}
	public void setProductStorageMgrZJ(ProductStorageMgrIFaceZJ productStorageMgrZJ) {
		this.productStorageMgrZJ = productStorageMgrZJ;
	}

}
