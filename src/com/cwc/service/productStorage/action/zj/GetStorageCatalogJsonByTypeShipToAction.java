package com.cwc.service.productStorage.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GetStorageCatalogJsonByTypeShipToAction extends
		ActionFatherController {

	private ProductStorageMgrIFaceZJ productStorageMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		int storage_type = StringUtil.getInt(request,"storage_type");
		long storage_type_id = StringUtil.getLong(request,"storage_type_id");
		
		DBRow[] storageCatalogs = productStorageMgrZJ.getProductStorageCatalogsByTypeShipTo(storage_type,storage_type_id);
		
		throw new JsonException(new JsonObject(storageCatalogs));
		
	}
	public void setProductStorageMgrZJ(ProductStorageMgrIFaceZJ productStorageMgrZJ) {
		this.productStorageMgrZJ = productStorageMgrZJ;
	}

}
