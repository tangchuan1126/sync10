package com.cwc.service.bcs.action.zj;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.OrderHandleErrorException;
import com.cwc.app.exception.bcs.ReceiveProductSNRepeatException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.damagedRepair.DamagedRepairOrderCanNotOutboundException;
import com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderCanNotWareHouseException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderHasWareHouseException;
import com.cwc.app.exception.deliveryOrder.NoExistDeliveryOrderException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.exception.purchase.NoPurchaseException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.exception.serialnumber.SerialNumberAlreadyInStoreException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportOrderCanNotOutboundException;
import com.cwc.app.exception.transport.TransportOrderCanNotWareHouseException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class BCSReceiveProductAction extends ActionFatherController {

	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private BCSMgrIFaceZJ bcsMgrZJ;
	private AdminMgrIFaceZJ adminMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception,MachineUnderFindException,XMLDataLengthException,XMLDataLengthException 
	{
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String machine = "";
		String password  = "";
		String xml = "";
		String loginAccount = "";
		StringBuffer returnDetails = new StringBuffer("");
		int postcount = 0;
		try 
		{

			xml = getPost(inputStream);
//			//system.out.println(xml+"---requestProduct");
			logBcs.info("request:"+xml);
			fc = StringUtil.getSampleNode(xml,"fc");
			machine = StringUtil.getSampleNode(xml,"Machine");
			loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			password = StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);			
			
			if(fc.equals("TransportReceiveCompare"))
			{
				postcount = bcsMgrZJ.transportReceiveAndroid(xml,adminLoggerBean);
				returnDetails.append(bcsMgrZJ.transportReciveCompare(xml));
			}
			
			if(fc.equals("TransportReceiveComplete"))
			{
				postcount = bcsMgrZJ.transportReceiveAndroid(xml, adminLoggerBean);
				returnDetails.append(bcsMgrZJ.transportReceiveCompleteResult(xml));
			}
			if(fc.equals("TransportReceiveOver"))
			{
				bcsMgrZJ.transportReceiveOver(xml, adminLoggerBean);
			}
			if(fc.equals("TransportReceivePut"))
			{
				postcount = bcsMgrZJ.putProductToLocation(xml, adminLoggerBean);
				returnDetails.append(bcsMgrZJ.transportReceivePutLocationCompare(xml));
				returnDetails.append(bcsMgrZJ.getTransportWareHouse(xml));
			}
			
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		
		catch(NoPurchaseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOPURCHASE;//无此采购单
		}
		catch(NoProductInPurchaseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOPRODUCTINPURCHASE;//采购单内无此商品
		}
		
		catch (OrderHandleErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ORDERHANDLEERROR;//订单非已打印状态
		}
		
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		
		catch (OrderNotFoundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ORDERNOTFOUND;//找不到订单
		}
		
		catch(CanNotInbondScanException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.CANNOTINBONDSCAN;
		}
		
		catch(ProductNotExistException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOTEXITSPRODUCT;
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(DeliveryOrderCanNotWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DELIVERYORDERCANNOTINWARE;
		}
		catch(NoExistDeliveryOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSDELIVERYORDER;
		}
		catch(DeliveryOrderHasWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.HASSUBMIT;
		}
		catch(TransportOrderCanNotWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.TRANSPORTORDERCANNOTINWARE;
		}
		catch(NoExistTransportOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSTRANSPORTORDER;
		}
		catch(TransportOrderCanNotOutboundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.TRANSPORTCANNOTSEND;
		}
		catch(NoExistDamagedRepairOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSDAMAGEDREPAIR;
		}
		catch(DamagedRepairOrderCanNotOutboundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DAMAGEDREPAIRCANNOTSEND;
		}
		catch (ReceiveProductSNRepeatException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SerialNumberRepeat;
			
			returnDetails.append(bcsMgrZJ.transportReciveSerialNumberRepeat(xml));
		}
		catch (SerialNumberAlreadyInStoreException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SerialNumberAlreadyInStore;
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		
		
		finally
		{
			responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
		}

	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		if(ret==0)
		{
			boxQtyXml.append("<err>"+err+"</err>");
		}
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		logBcs.info("response:"+boxQtyXml.toString());
//		//system.out.println(boxQtyXml.toString()+"----返回值");
		try 
		{
		   response.setCharacterEncoding("UTF-8");

		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setBcsMgrZJ(BCSMgrIFaceZJ bcsMgrZJ) {
		this.bcsMgrZJ = bcsMgrZJ;
	}
}
