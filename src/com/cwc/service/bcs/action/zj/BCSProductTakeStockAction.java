package com.cwc.service.bcs.action.zj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;








import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.AndroidMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class BCSProductTakeStockAction extends ActionFatherController {
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private AdminMgrIFaceZJ adminMgrZJ;
	private BCSMgrIFaceZJ bcsMgrZJ;
	private AndroidMgrIFaceZJ androidMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String machine = "";
		String password  = "";
		String xml = "";
		String loginAccount = "";
		StringBuffer returnDetails = new StringBuffer("");
		int postcount = 0;
		
		try 
		{
			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><fc>TakeStockApprove</fc><Machine>BJ1001</Machine><LoginAccount>admin</LoginAccount><Password>admin</Password></data>";//getPost(inputStream);
//			xml = getPost(inputStream);
			//system.out.println(xml+"---request");
			
			logBcs.info("request"+xml);
			fc = StringUtil.getSampleNode(xml,"fc");
			machine = StringUtil.getSampleNode(xml,"Machine");
			loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			password = StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
			if(login == null)
			{
				response.setStatus(800);
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			if(fc.equals("OperatorArea"))
			{
				returnDetails.append(bcsMgrZJ.getOperatorArea(adminLoggerBean.getPs_id()));
			}
			if(fc.equals("GetProductStoreLocation"))
			{
				returnDetails.append(androidMgrZJ.getProductStoreLocationBySlc(xml,adminLoggerBean));
			}
			if(fc.equals("TakeStock"))
			{
				postcount = bcsMgrZJ.productTakeStock(xml, adminLoggerBean, machine);
			}
			if(fc.equals("TakeStockOver")) 
			{
				returnDetails.append(androidMgrZJ.storageTakeStockDifferents(xml, adminLoggerBean));
			}
			if(fc.equals("TakeStockApprove"))
			{
				androidMgrZJ.storageTakeStockApprove(xml, adminLoggerBean);
			}
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		finally
		{
			responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
		}
	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		if(ret==0)
		{
			boxQtyXml.append("<err>"+err+"</err>");
		}
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		logBcs.info("response:"+boxQtyXml.toString());
	//	//system.out.println(boxQtyXml.toString()+"----返回值");
		try 
		{
		   response.setCharacterEncoding("UTF-8");
	
		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setBcsMgrZJ(BCSMgrIFaceZJ bcsMgrZJ) {
		this.bcsMgrZJ = bcsMgrZJ;
	}

	public void setAndroidMgrZJ(AndroidMgrIFaceZJ androidMgrZJ) {
		this.androidMgrZJ = androidMgrZJ;
	}

}
