package com.cwc.service.bcs.action.zj;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.OrderHandleErrorException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.damagedRepair.DamagedRepairOrderCanNotOutboundException;
import com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderCanNotWareHouseException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderHasWareHouseException;
import com.cwc.app.exception.deliveryOrder.NoExistDeliveryOrderException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.exception.purchase.NoPurchaseException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportOrderCanNotOutboundException;
import com.cwc.app.exception.transport.TransportOrderCanNotWareHouseException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;

public class BCSProductAction extends ActionFatherController {

	static Logger log = Logger.getLogger("ACTION");
	static Logger logBcs = Logger.getLogger("BCS");
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";//回传设定
	private BCSMgrIFaceZJ BCSMgr;
	private AdminMgrIFaceZJ adminMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception,MachineUnderFindException,XMLDataLengthException,XMLDataLengthException 
	{
		response.setContentType(CONTENT_TYPE);		
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String machine = "";
		String password  = "";
		String xml = "";
		String loginAccount = "";
		StringBuffer returnDetails = new StringBuffer("");
		int postcount = 0;
		try 
		{

			xml = getPost(inputStream);
			//system.out.println(xml+"---requestProduct");
			logBcs.info("request"+xml);
			fc = BCSMgr.getSampleNode(xml,"fc");
			machine = BCSMgr.getSampleNode(xml,"Machine");
			loginAccount = BCSMgr.getSampleNode(xml,"LoginAccount");
			password = BCSMgr.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
			if(login == null)
			{
				throw new MachineUnderFindException();
			}
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(loginAccount);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			
			HttpSession session = request.getSession(true);
			session.setAttribute(Config.adminSesion,adminLoggerBean);
			
			if(fc.equals("ProudctLocation"))
			{
				postcount = BCSMgr.productLocation(xml,adminLoggerBean, machine);
			}
			
			if(fc.equals("CompareProductStorage"))
			{
				returnDetails.append("<Details>"+BCSMgr.compareProductStorage(xml,adminLoggerBean,machine)+"</Details>");
			}
			
			if(fc.equals("Waiting4Delivery"))
			{
				HashMap result = BCSMgr.waitingForDelivery(xml, adminLoggerBean);
				postcount = Integer.valueOf(result.get("postcount").toString()).intValue();
				if(!result.get("error").toString().trim().equals(""))
				{
					returnDetails.append("<Details>"+result.get("error").toString()+"</Details>");
					if(result.get("fc").toString().equals("handle"))
					{
						throw new OrderHandleErrorException();
					}
					if(result.get("fc").toString().equals("noOrder"))
					{
						throw new OrderNotFoundException();
					}
				}
				
			}
			
			if(fc.equals("Shipped"))
			{
				HashMap result = BCSMgr.shipped(xml, adminLoggerBean);
				postcount = Integer.valueOf(result.get("postcount").toString()).intValue();
				if(!result.get("error").toString().trim().equals(""))
				{
					returnDetails.append("<Details>"+result.get("error").toString()+"</Details>");
					
					if(result.get("fc").toString().equals("handle"))
					{
						throw new OrderHandleErrorException();
					}
					if(result.get("fc").toString().equals("noOrder"))
					{
						throw new OrderNotFoundException();
					}
					
				}
			}
			
			if(fc.equals("DeliveryDownload"))//下载交货单
			{
				returnDetails.append(BCSMgr.deliveryDownload(xml,adminLoggerBean));
			}
			
			if(fc.equals("DeliveryUpload"))//交货单比较
			{
				returnDetails.append(BCSMgr.compareDelivery(xml, adminLoggerBean));
			}
			
			if(fc.equals("DeliverySuccess"))//交货单确认入库
			{
				BCSMgr.deliverySuccess(xml, adminLoggerBean, machine);
				postcount = 1;
			}
			
			if(fc.equals("TransportUpload"))
			{
				postcount = BCSMgr.transportReceiveAndroid(xml,adminLoggerBean);
				returnDetails.append(BCSMgr.transportReciveCompare(xml));
			}
			
			if(fc.equals("TransportSend"))
			{
				returnDetails.append(BCSMgr.compareTransportPacking(xml, adminLoggerBean));
			}
			
			if(fc.equals("TransportPacking"))
			{
				returnDetails.append(BCSMgr.transportPacking(xml,adminLoggerBean));
			}
			
			if(fc.equals("TransportDownload"))
			{
				returnDetails.append(BCSMgr.transportDownload(xml,adminLoggerBean));
			}
			
			if(fc.equals("TransportSendSuccess"))
			{
				BCSMgr.transportDelivery(xml, adminLoggerBean, machine);
			}
			if(fc.equals("TransportSuccess"))
			{
				BCSMgr.transportSuccess(xml, adminLoggerBean, machine);
			}
			if(fc.equals("ReturnSendDowload"))
			{
				returnDetails.append(BCSMgr.damagedRepairPacking(xml,adminLoggerBean));//返修单条码机下载
			}
			if(fc.equals("ReturnSend"))
			{
				returnDetails.append(BCSMgr.compareDamagedRepairPacking(xml, adminLoggerBean));
			}
			if(fc.equals("ReturnSendSuccess"))
			{
				BCSMgr.damagedRepairDelivery(xml, adminLoggerBean, machine);
			}
		}
		catch (XMLDataErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;//数据格式错误
		}
		catch (XMLDataLengthException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATASIZEINCORRECTLY;//数据长度错误
		}
		catch(MachineUnderFindException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.LOGINERROR;//登录失败，账号密码错误
		}
		
		catch(NoPurchaseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOPURCHASE;//无此采购单
		}
		catch(NoProductInPurchaseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOPRODUCTINPURCHASE;//采购单内无此商品
		}
		
		catch (OrderHandleErrorException e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ORDERHANDLEERROR;//订单非已打印状态
		}
		
		catch(OperationNotPermitException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.OPERATIONNOTPERMIT;//无权限操作
		}
		
		catch (OrderNotFoundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.ORDERNOTFOUND;//找不到订单
		}
		
		catch(CanNotInbondScanException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.CANNOTINBONDSCAN;
		}
		
		catch(ProductNotExistException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOTEXITSPRODUCT;
		}
		catch(ArrayIndexOutOfBoundsException e)//区数据越界，一般是格式错误
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DATATYPEINCRRECTLY;
		}
		catch (NetWorkException e)//取xml格式数据数组越界，多为美国网络引起，暂用格式错误显示
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NETWORKEXCEPTION;
		}
		catch(DeliveryOrderCanNotWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DELIVERYORDERCANNOTINWARE;
		}
		catch(NoExistDeliveryOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSDELIVERYORDER;
		}
		catch(DeliveryOrderHasWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.HASSUBMIT;
		}
		catch(TransportOrderCanNotWareHouseException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.TRANSPORTORDERCANNOTINWARE;
		}
		catch(NoExistTransportOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSTRANSPORTORDER;
		}
		catch(TransportOrderCanNotOutboundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.TRANSPORTCANNOTSEND;
		}
		catch(NoExistDamagedRepairOrderException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.NOEXITSDAMAGEDREPAIR;
		}
		catch(DamagedRepairOrderCanNotOutboundException e)
		{
			ret = BCSKey.FAIL;
			err = BCSKey.DAMAGEDREPAIRCANNOTSEND;
		}
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
		
		
		finally
		{
			responseXML(fc, ret, err, response,returnDetails.toString(),postcount);
		}

	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	private void responseXML(String fc,int ret,int err,HttpServletResponse response,String details,int postcount)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<fc>"+fc+"</fc>");
		boxQtyXml.append("<ret>"+ret+"</ret>");
		if(ret==0)
		{
			boxQtyXml.append("<err>"+err+"</err>");
		}
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<Processingcount>"+postcount+"</Processingcount>");
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		logBcs.info("response:"+boxQtyXml.toString());
		//system.out.println(boxQtyXml.toString()+"----返回值");
		try 
		{
		   response.setCharacterEncoding("UTF-8");

		   OutputStream out = response.getOutputStream();
		   
		   response.setContentLength(boxQtyXml.toString().getBytes().length);                      //添加响应包长度
		   out.write(boxQtyXml.toString().getBytes());
		   out.flush();
		   out.close();
		   
		}
		catch (Exception e)
		{
		   e.printStackTrace();
		} 
	}
	
	public void setBCSMgr(BCSMgrIFaceZJ mgr) {
		BCSMgr = mgr;
	}
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
}
