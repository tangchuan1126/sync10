package com.cwc.service.bcs.action.zj;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.OrderHandleErrorException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderCanNotWareHouseException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderHasWareHouseException;
import com.cwc.app.exception.deliveryOrder.NoExistDeliveryOrderException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.exception.purchase.NoPurchaseException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.service.ebay.action.FileUpAction;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class BCSTestAction extends ActionFatherController {
	
	private BCSMgrIFaceZJ BCSMgr;
	private AdminMgrIFaceZJ adminMgrZJ;
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException,Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, Exception 
	{
		//system.out.println("---Start---BCSTestAction-----");
		try 
		{
			String file1  = "123";
			String file2 = "567";
			
			InputStream is1 = new ByteArrayInputStream(file1.getBytes());
			InputStream is2 = new ByteArrayInputStream(file2.getBytes());
			
			ArrayList<InputStream> list = new ArrayList<InputStream>();
			list.add(is1);
			list.add(is2);
			
			FileUtil.delFile("E:\\ZipTest\\AddStreamToZip.zip");
			ZipFile zipFile = new ZipFile("Zhanjie.zip");
			
			for (int i = 0; i <list.size(); i++) 
			{
				ZipParameters parameters = new ZipParameters();
				parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
				parameters.setFileNameInZip("zhanjie"+i+1+".txt");
				parameters.setSourceExternalStream(true);
				zipFile.addStream(list.get(i), parameters);
				list.get(i).close();
			}
			
			File file=new File("E:\\ZipTest\\AddStreamToZip.zip");
			InputStream fis=new FileInputStream(file);
			int length=(int)file.length();
			
			
			
			byte[] image=new byte[length];
			fis.read(image);
			
			 response.setContentType("application/zip;charset=UTF-8");
			 OutputStream output = response.getOutputStream();
			 response.setContentLength(image.length);
			 output.write(image);
			 output.close();
			 output.flush();
//			 response.reset();
			
			
			
//			long purchase_id = StrUtil.getLong(request,"purchase_id");
//			String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><fc>Shipped</fc><Machine>BJ002</Machine> <Password>renweidong</Password><LoginAccount>renweidong</LoginAccount><Ware>BJ</Ware> <Detail> <Details>474379,1.425,6,|474462,4.860,6,|474483,1.225,6,|474422,8.035,6,|474307,2.310,6,|1715477606,1.200,6,|474400,2.385,6,|474401,2.360,6,|474496,1.460,6,|474375,1.450,6,|474459,1.220,6,|474385,1.425,6,|474417,1.410,6,|474453,1.190,6,|474424,2.840,6,|474361,1.235,6,|474511,1.335,6,|474512,2.485,6,|474463,1.410,6,|473953,1.230,6,|474355,2.570,6,|474420,1.220,6,|473840,2.395,6,|474394,1.260,6,|474311,1.235,6,|474477,6.195,6,|474371,1.215,6,|474430,1.260,6,|474405,3.555,6,|474497,1.400,6,|474466,1.460,6,|474382,1.240,6,|474451,1.230,6,|474436,1.450,6,|474389,1.245,6,|474404,2.365,6,|474352,1.205,6,|474392,1.205,6,|474418,1.305,6,|474253,1.425,6,|474513,2.720,6,|474514,1.710,6,|474391,1.200,6,|474421,1.225,6,|474493,1.215,6,|474516,1.225,6,|474373,1.445,6,|474460,2.745,6,|474530,2.740,6,|474414,1.210,6,|474409,1.210,6,|474490,3.845,6,|474362,1.200,6,|474321,1.205,6,|474323,1.210,6,|474519,1.205,6,|474383,1.220,6,|474508,1.195,6,|474447,1.360,6,|474423,1.225,6,|474335,2.360,6,|474413,2.715,6,|474469,0.905,6,|474411,0.155,6,|474410,0.700,6,|474412,0.160,6,|474472,0.140,6,|474428,0.145,6,|474343,0.520,6,|474407,0.280,6,|474449,0.495,6,|474439,0.380,6,|472043,0.125,6,|474445,0.500,6,|474332,0.195,6,|474370,0.230,6,|474406,0.190,6,|474398,0.620,6,|474396,0.180,6,|474301,1.435,6,|474079,1.250,6,|474426,1.270,6,|474531,0.005,6,|</Details></Detail><Efficacy>j9Z5kdHLODNcyI9lCkB41GxueTQ=</Efficacy><Length>1332</Length></data>";
//			String fc = "";
//			String machine = "";
//			String password  = "";
//			
//			String loginAccount = "";
//			fc = BCSMgr.getSampleNode(xml,"fc");
//			machine = BCSMgr.getSampleNode(xml,"Machine");
//			loginAccount = BCSMgr.getSampleNode(xml,"LoginAccount");
//			password = BCSMgr.getSampleNode(xml,"Password");
//			
//			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
//			
//			if(login == null)
//			{
//				throw new MachineUnderFindException();
//			}
//			
//			AdminLoginBean adminLoggerBean = new AdminLoginBean();
//			adminLoggerBean.setAccount(loginAccount);
//			adminLoggerBean.setAdgid(login.get("adgid",0l));
//			adminLoggerBean.setAdid(login.get("adid",0l));
//			adminLoggerBean.setPs_id(login.get("ps_id",0l));
//			HttpSession session = request.getSession(true);
//			session.setAttribute(Config.adminSesion,adminLoggerBean);
//			
//			BCSMgr.shipped(xml, adminLoggerBean);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setBCSMgr(BCSMgrIFaceZJ mgr) {
		BCSMgr = mgr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
