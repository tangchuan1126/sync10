package com.cwc.service.bcs.action.zj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class BCSDownLoadOutListAction extends ActionFatherController {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgrIFaceZJ adminMgrZJ;
	private BCSMgrIFaceZJ bcsMgrZJ;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String machine = "";
		String password  = "";
		String xml = "";
		String loginAccount = "";
		StringBuffer returnDetails = new StringBuffer("");
		int postcount = 0;
		
		
		try 
		{
//			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><fc>Test</fc><Machine>GZ002</Machine><LoginAccount>chenwc</LoginAccount><Password>123456</Password></data>";//getPost(inputStream);
			xml = getPost(inputStream);
			//system.out.println(xml+"---request");
			
			machine = StringUtil.getSampleNode(xml,"Machine");
			loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			password = StringUtil.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
			if(login == null)
			{
				response.setStatus(800);
			}
			
			byte[] image = bcsMgrZJ.downLoadOutStoreBillBasicData(xml, machine);
			
			response.setContentType("application/zip;charset=UTF-8");
			OutputStream output = response.getOutputStream();
			response.setContentLength(image.length);
			output.write(image);
			output.close();
			output.flush();
		} 
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

	public void setBcsMgrZJ(BCSMgrIFaceZJ bcsMgrZJ) {
		this.bcsMgrZJ = bcsMgrZJ;
	}

}
