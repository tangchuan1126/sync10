package com.cwc.service.bcs.action.zj;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.FileUtil;

public class BCSDownLoadFileAction extends ActionFatherController {
	static Logger log = Logger.getLogger("ACTION");
	private BCSMgrIFaceZJ BCSMgr;
	private AdminMgrIFaceZJ adminMgrZJ;
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		int ret = BCSKey.SUCCESS;//默认成功
		int err = BCSKey.DATASIZEINCORRECTLY;
		String fc = "";
		String machine = "";
		String password  = "";
		String xml = "";
		String loginAccount = "";
		StringBuffer returnDetails = new StringBuffer("");
		int postcount = 0;
		
		
		try 
		{
//			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><fc>Test</fc><Machine>GZ002</Machine><LoginAccount>chenwc</LoginAccount><Password>123456</Password></data>";//getPost(inputStream);
			xml = getPost(inputStream);
//			//system.out.println(xml+"---request");
			
			//fc = BCSMgr.getSampleNode(xml,"fc");
			machine = BCSMgr.getSampleNode(xml,"Machine");
			loginAccount = BCSMgr.getSampleNode(xml,"LoginAccount");
			password = BCSMgr.getSampleNode(xml,"Password");
			
			DBRow login = adminMgrZJ.MachineLogin(loginAccount, password);
			
//			if(login==null)
//			{
//				response.setStatus(800);
//			}
			
			String catalog = getResponseXML(BCSMgr.category());
			String ware = getResponseXML(BCSMgr.ware());
			String product = getResponseXML(BCSMgr.product());
			String productCode = getResponseXML(BCSMgr.productCode());
			String location = getResponseXML(BCSMgr.location(login.get("ps_id",0l)));
			
			InputStream isCatalog = new ByteArrayInputStream(catalog.getBytes());
			InputStream isWare = new ByteArrayInputStream(ware.getBytes());
			InputStream isProduct = new ByteArrayInputStream(product.getBytes());
			InputStream isProductCode = new ByteArrayInputStream(productCode.getBytes());
			InputStream isLocation = new ByteArrayInputStream(location.getBytes());
			
			ArrayList<InputStream> list = new ArrayList<InputStream>();
			list.add(isCatalog);
			list.add(isWare);
			list.add(isProduct);
			list.add(isProductCode);
			list.add(isLocation);
			
			FileUtil.delFile(Environment.getHome()+"/bcs_down_zip/"+machine+".zip");
			ZipFile zipFile = new ZipFile(Environment.getHome()+"/bcs_down_zip/"+machine+".zip");
			
			for (int i = 0; i <list.size();i++)
			{
				ZipParameters parameters = new ZipParameters();
				parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
				parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
				
				switch (i) 
				{
					case 0:
						parameters.setFileNameInZip("catalog.txt");
					break;

					case 1:
						parameters.setFileNameInZip("ware.txt");
					break;
						
					case 2:
						parameters.setFileNameInZip("product.txt");
					break;
					
					case 3:
						parameters.setFileNameInZip("productCode.txt");
					break;
					
					case 4:
						parameters.setFileNameInZip("location.txt");
					break;
				}
				
				parameters.setSourceExternalStream(true);
				
				zipFile.addStream(list.get(i), parameters);
				list.get(i).close();
			}
			
			File file = new File(Environment.getHome()+"/bcs_down_zip/"+machine+".zip");
			InputStream fis=new FileInputStream(file);
			int length=(int)file.length();
			
			
			
			byte[] image=new byte[length];
			fis.read(image);
			fis.close();
			
			response.setContentType("application/zip;charset=UTF-8");
			OutputStream output = response.getOutputStream();
			response.setContentLength(image.length);
			output.write(image);
			output.close();
			output.flush();
		} 
		catch (Exception e) 
		{
			ret = BCSKey.FAIL;
			err = BCSKey.SYSTEMERROR;
			throw new SystemException(e,"bcsrequest:"+xml,log);
		}
	}
	
	private String getResponseXML(String details)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		return boxQtyXml.toString();
	}
	
	/**
	 * 获得条码机传送数据
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private String getPost(InputStream inputStream)
		throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	public void setBCSMgr(BCSMgrIFaceZJ mgr) {
		BCSMgr = mgr;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}

}
