package com.cwc.service.refund.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.RefundMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

public class ApplyRefundAction extends ActionFatherController{

	private RefundMgrIfaceZr refundMgrZr ;
	
	
	public void setRefundMgrZr(RefundMgrIfaceZr refundMgrZr) {
		this.refundMgrZr = refundMgrZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			
			refundMgrZr.addRefund(request);
			DBRow result = new DBRow();
			result.add("flag", "success");
			throw new JsonException(new JsonObject(result));
		
	}
	
}
