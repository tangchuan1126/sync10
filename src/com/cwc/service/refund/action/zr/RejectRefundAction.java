package com.cwc.service.refund.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.RefundMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;

/**
 * 驳回一条退款申请
 * 给申请退款的人创建一个任务。这个任务还是管理这个退款
 * 标识原来的退款已经被驳回。
 * 
 * @author Administrator
 *
 */
public class RejectRefundAction extends ActionFatherController{


	private RefundMgrIfaceZr refundMgrZr ;
	
	
	public void setRefundMgrZr(RefundMgrIfaceZr refundMgrZr) {
		this.refundMgrZr = refundMgrZr;
	}
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
	 
			refundMgrZr.refundReject(request);
			DBRow result = new DBRow();
			result.add("flag", "success");
			throw new JsonException(new JsonObject(result));
	
	}

}
