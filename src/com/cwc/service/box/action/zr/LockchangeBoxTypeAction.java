package com.cwc.service.box.action.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class LockchangeBoxTypeAction extends ActionFatherController {
	
	private BoxTypeMgrIfaceZr boxTypeMgrZr ;
	
	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
 			
			DBRow result = new DBRow();
			long id = StringUtil.getLong(request, "id");
			long lockId = StringUtil.getLong(request, "lockId");

			boxTypeMgrZr.updateLock(id,lockId);
			result.add("flag", "success");
			throw new JsonException(new JsonObject(result));
			
	}
}
