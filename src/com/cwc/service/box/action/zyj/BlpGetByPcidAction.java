package com.cwc.service.box.action.zyj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zr.BoxTypeMgrZr;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class BlpGetByPcidAction extends ActionFatherController {
	
	private BoxTypeMgrIfaceZr boxTypeMgrZr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		int begin = StringUtil.getInt(request, "begin");
		int length = StringUtil.getInt(request, "length");
		long pcid = StringUtil.getLong(request, "pc_id");
		DBRow result = boxTypeMgrZr.getBoxTypesOrShiptoByPcId(pcid, length);
		throw new JsonException(new JsonObject(result));
	}

	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}

}
