package com.cwc.service.box.action.zyj;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.service.CustomerService;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class ClpGetByPcidAction extends ActionFatherController {
	
	private BoxTypeMgrIfaceZr boxTypeMgrZr;
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	private ShipToMgrIFaceZJ shipToMgrZJ ;
	private CustomerService customerService;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		long pcid = StringUtil.getLong(request, "pc_id");
		//Customer ID
		int customerId = 0;
		String customerId_str = request.getParameter("customerId");
		if(customerId_str != null && customerId_str.trim().length() > 0){
			customerId = Integer.parseInt(customerId_str);
		}
		
		int active = StringUtil.getInt(request, "active");
		
		// 获取当前商品的所有CLP信息
		DBRow[] clpRows = boxTypeMgrZr.getBoxTypes(pcid,0,0,0,customerId,active);
		for(int c=0; c<clpRows.length; c++){
			DBRow innerLp = null;
			if(clpRows[c].get("inner_pc_or_lp",0l) > 0){
				//innerLp = boxTypeMgrZr.findIlpByIlpId(clpRows[c].get("inner_pc_or_lp",0l));
				innerLp = boxTypeMgrZr.getCascadeClpType(clpRows[c].get("inner_pc_or_lp",0l));
			}
			
			ArrayList<DBRow> cascadeLpList = new ArrayList<DBRow>();
			
			//JsonObject不支持DBRow多层嵌套，换成数组
			if(innerLp != null){
				DBRow cascade = innerLp;
				do{
					cascadeLpList.add(cascade);
					cascade = cascade.get("subPlateType")==null?null:(DBRow)cascade.get("subPlateType");			
				}while(cascade != null);
			}
			
			// 设置CLP内置物品（CLP或者商品）
			clpRows[c].add("inner_clp", new JsonObject(cascadeLpList.toArray(new DBRow[cascadeLpList.size()])));
			//
			List<DBRow> clpTitles = new ArrayList<DBRow>();
			List<DBRow> clpShipTos = new ArrayList<DBRow>();
			List<DBRow> clpCustomers = new ArrayList<DBRow>();
			// 设置当前CLP的Title ShipTo信息
			String titleIds = clpRows[c].getString("title_ids");
			String shipToIds = clpRows[c].getString("ship_to_ids");
			String customerIds = clpRows[c].getString("customer_ids");
			
			if(StringUtils.isNotEmpty(titleIds)){
				String[] titleIdArr = titleIds.split(",");
				String[] customerIdArr = customerIds.split(",");
				String[] shipToIdArr = shipToIds.split(",");
				for (int k=0;k<titleIdArr.length;k++) {
					DBRow clpTitle = proprietaryMgrZyj.findProprietaryByTitleId(Long.valueOf(titleIdArr[k]));
					DBRow clpShipTo = shipToMgrZJ.getShipToById(Long.valueOf(shipToIdArr[k]));
					DBRow  customer_row = new DBRow();
					Customer customer = null;
					if(customerId > 0 && Integer.parseInt(customerIdArr[k]) > 0 && Integer.parseInt(customerIdArr[k]) != customerId){
						continue;
					}

					
					if(Integer.parseInt(customerIdArr[k]) > 0){
						customer = customerService.getCustomer(Integer.parseInt(customerIdArr[k]));
						if(customer != null){
							customer_row.add("id", customer.getId());
							customer_row.add("name", customer.getName());
						}
					}else{
						customer_row.add("id", 0);
						customer_row.add("name", "*");						
					}
					
					DBRow clpTitles_row = new DBRow();
					DBRow clpShipTos_row = new DBRow();
					DBRow clpCustomers_row = new DBRow();
					if(clpTitle == null){
						clpTitle = new DBRow();
						clpTitle.add("title_id", 0);
						clpTitle.add("title_name", "*");
					}
					if(clpShipTo == null){
						clpShipTo = new DBRow();
						clpShipTo.add("ship_to_id", 0);
						clpShipTo.add("ship_to_name", "*");
					}
					if(customer == null){
						customer = new Customer();
						customer.setName("*");
					}
					clpTitles_row.add("clp_title", new JsonObject(clpTitle));
					clpShipTos_row.add("clp_shipto", new JsonObject(clpShipTo));
					
					clpCustomers_row.add("clp_customer", new JsonObject(customer_row));
					clpTitles.add(clpTitles_row);
					clpShipTos.add(clpShipTos_row);
					clpCustomers.add(clpCustomers_row);
				}
			}
			clpRows[c].add("clp_titles", new JsonObject(clpTitles.toArray(new DBRow[clpTitles.size()])));
			clpRows[c].add("clp_shiptos", new JsonObject(clpShipTos.toArray(new DBRow[clpShipTos.size()])));
			clpRows[c].add("clp_customers", new JsonObject(clpCustomers.toArray(new DBRow[clpCustomers.size()])));
		}
		throw new JsonException(new JsonObject(clpRows));
	}

	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}
	
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	

}
