package com.cwc.service.productCode.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import us.monoid.json.JSONObject;

import com.cwc.action.MessageAlerter;
import com.cwc.action.core.ActionFatherController;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.iface.zj.ProductCodeMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.initconf.Resource;
import com.cwc.json.JsonObject;

public class AddProductCodeAction extends ActionFatherController {

	private ProductCodeMgrIFaceZJ productCodeMgrZJ;
	private MessageAlerter messageAlert;
	
	public void perform(HttpServletRequest request, HttpServletResponse response)
		throws WriteOutResponseException, RedirectRefException,ForwardException, Forward2JspException, RedirectBackUrlException,RedirectException, OperationNotPermitException,PageNotFoundException, DoNothingException, Exception 
	{		
		JSONObject output = new JSONObject();
		try 
		{
			DBRow codeHtml = productCodeMgrZJ.addProductCode(request);
			output = new JSONObject(codeHtml);
		}
		catch (ProductCodeIsExistException e) 
		{
			messageAlert.setMessage(request,Resource.getStringValue("","ProductCodeIsExistException",""));
			//初始化一下 sessionName
			messageAlert.clear(request);
		}
		throw new JsonException(output);
	}

	public void setProductCodeMgrZJ(ProductCodeMgrIFaceZJ productCodeMgrZJ) {
		this.productCodeMgrZJ = productCodeMgrZJ;
	}

	public void setMessageAlert(MessageAlerter messageAlert) {
		this.messageAlert = messageAlert;
	}

}
