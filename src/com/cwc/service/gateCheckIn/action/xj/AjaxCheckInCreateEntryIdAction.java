package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.lucene.zr.TransportIndexMgr;
import com.cwc.app.lucene.zwb.CheckInIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxCheckInCreateEntryIdAction extends ActionFatherController{

private CheckInMgrIfaceXj checkInMgrXj;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		
		long id = checkInMgrXj.createEntryId(request);
		DBRow row = new DBRow();
		row.add("id",id);
		throw new JsonException(new JsonObject(row));
		
	}

	public void setCheckInMgrXj(CheckInMgrIfaceXj checkInMgrXj) {
		this.checkInMgrXj = checkInMgrXj;
	}




}
