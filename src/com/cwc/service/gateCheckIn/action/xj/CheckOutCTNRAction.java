package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.iface.zr.StorageYardControlIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.lucene.zr.TransportIndexMgr;
import com.cwc.app.lucene.zwb.CheckInCarrierIndexMgr;
import com.cwc.app.lucene.zwb.CheckInIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CheckOutCTNRAction extends ActionFatherController{

	static Logger javapsLog = Logger.getLogger("JAVAPS");
	
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private StorageDoorMgrIfaceZr storageDoorMgrZr;
	private StorageYardControlIfaceZr storageYardControlZr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		
		
		
		try
		{
			long dloId = StringUtil.getLong(request, "dlo_id");
	//		int relType = StringUtil.getInt(request, "relType");
	//		int yc_id = StringUtil.getInt(request, "yc_id");
				int ps_id = StringUtil.getInt(request, "ps_id");
	//		String sd_id = StringUtil.getString(request, "sd_id");
				String ctnr = StringUtil.getString(request, "ctnr");
			int flag = StringUtil.getInt(request,"flag");
			if(flag==1){
				DBRow row = new DBRow();
				row.add("dlo_id", dloId);
				DBRow[] doors = storageDoorMgrZr.getUseDoorByAssociateIdAndType(dloId, ModuleKey.CHECK_IN) ;
				DBRow[] fixDoors = checkInMgrZwb.fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
				if(fixDoors != null){
					//result.add("doors",fixDoors);
					row.add("doors",fixDoors);
				}
			
				DBRow[] spots = storageYardControlZr.getUseSpotByAssociateIdAndType(dloId, ModuleKey.CHECK_IN);
				DBRow[] fixSpots = checkInMgrZwb.fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
				if(fixSpots != null){
					//result.add("spots",fixSpots);
					row.add("spots",fixSpots);
				}
				
				
				
				checkInMgrZwb.checkOutCTNR(request,row);
				
			}else if(flag==2){
				checkInMgrZwb.getcheckoutByCTNR(request,dloId,ctnr,ps_id);
			}
			
		}
		catch (JsonException e)
		{
			throw new SystemException("CheckOutCTNRAction"+e);
		}
		
		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setStorageDoorMgrZr(StorageDoorMgrIfaceZr storageDoorMgrZr) {
		this.storageDoorMgrZr = storageDoorMgrZr;
	}

	public void setStorageYardControlZr(
			StorageYardControlIfaceZr storageYardControlZr) {
		this.storageYardControlZr = storageYardControlZr;
	}
	


}
