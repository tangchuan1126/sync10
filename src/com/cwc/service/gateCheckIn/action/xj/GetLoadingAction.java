package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zj.SQLServerMgrZJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.fr.data.core.db.DBUtils;

public class GetLoadingAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	private SQLServerMgrZJ sqlServerMgrZJ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long type=StringUtil.getLong(request,"number_type");
		String search_number=StringUtil.getString(request,"search_number");
		long mainId=StringUtil.getLong(request,"mainId");
		long ps_id=StringUtil.getLong(request,"ps_id");
		DBRow result =checkInMgrZwb.getDoorByStagingOrTitle(type,search_number,mainId,ps_id,0l,request);
	 
		throw new WriteOutResponseException(StringUtil.convertDBRowsToJsonString(result));
		
		
	}
	public void setSqlServerMgrZJ(SQLServerMgrZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	

}
