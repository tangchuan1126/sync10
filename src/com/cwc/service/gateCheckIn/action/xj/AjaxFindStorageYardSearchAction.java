package com.cwc.service.gateCheckIn.action.xj;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxFindStorageYardSearchAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		String yc = StringUtil.getString(request, "yc");
		long ps_id = StringUtil.getLong(request, "ps_id");
		long flag = StringUtil.getLong(request, "flag");
		long spotArea = StringUtil.getLong(request, "spotArea");
		long mainId = StringUtil.getLong(request, "mainId");

		if(flag==2){
			DBRow[] result = checkInMgrZwb.getVacancySpotByYcno(mainId, spotArea ,ps_id , yc);  //模糊查询根据停车位名 2
			throw new JsonException(new JsonObject(result));
		}else if(flag==3){          
			DBRow[] result = checkInMgrZwb.getVacancySpotByYcnoAccurate(mainId, spotArea ,ps_id , yc);  //精确查找 3
			throw new JsonException(new JsonObject(result));
		}else if(flag==5){
			DBRow[] result=this.checkInMgrZwb.getVacancySpot(ps_id, spotArea, mainId);  // 根据zone 查询可用spot  5
			throw new JsonException(new JsonObject(result));
		}else{	
			DBRow[] result=this.checkInMgrZwb.getVacancySpot(ps_id, spotArea, mainId);  //gate 页面自动选门
			List<DBRow> singleList = new ArrayList<DBRow>(); 
			List<DBRow> doubleList = new ArrayList<DBRow>();
			//计算优先单数  然后双数 没有弹开页面
			if(result.length>0){  
				for(int i=0;i<result.length;i++){
					long yc_no=result[i].get("yc_id", 0l);
					if(yc_no%2==1){
						singleList.add(result[i]);
					}else if(yc_no%2==0){
						doubleList.add(result[i]);
					}
				}
			}
			if(singleList.size()>0){
				throw new JsonException(new JsonObject(singleList.toArray(new DBRow[singleList.size()])));
			}else if(doubleList.size()>0){
				throw new JsonException(new JsonObject(doubleList.toArray(new DBRow[doubleList.size()])));
			}else{
				throw new JsonException(new JsonObject(null));
			}
		}
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	

}
