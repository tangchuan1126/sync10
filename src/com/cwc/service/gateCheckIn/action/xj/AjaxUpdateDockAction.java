package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxUpdateDockAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long dlo_id=StringUtil.getLong(request,"dlo_id");
		long searchdlo_id=StringUtil.getLong(request,"searchdlo_id");
		long doorId=StringUtil.getLong(request,"doorId");
		long searchDoorId=StringUtil.getLong(request,"searchDoorId");
		int flag=StringUtil.getInt(request,"flag");
		long adminId=StringUtil.getLong(request,"adminId");
		long area_id=StringUtil.getLong(request,"area_id");
		//checkInMgrZwb.updateDock(adminId,dlo_id, searchdlo_id, doorId, searchDoorId, flag,1,area_id);
		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	

}
