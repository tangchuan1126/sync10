package com.cwc.service.gateCheckIn.action.xj;

import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.checkin.ResourceHasUsedException;
import com.cwc.app.exception.checkin.VerifyShippingCTNRException;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class GateCheckInAddAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		DBRow result = new DBRow();
		try
		{
			
			int checkOutFlag = StringUtil.getInt(request, "checkOutFlag");
			//int ymsFlag = StringUtil.getInt(request, "yms");
			//添加数据 
			
			DBRow row = checkInMgrZwb.gateCheckInAddDBRow(request);//为了得到是哪个entry  CheckOut其他相同设备 顾放在CheckOut其他相同设备的上面;
			/*if(ymsFlag == 1){
				row = checkInMgrZwb.ymsGateCheckInAdd(request);//为了得到是哪个entry  CheckOut其他相同设备 顾放在CheckOut其他相同设备的上面
			}
			else{
				row = checkInMgrZwb.gateCheckInAddDBRow(request);//为了得到是哪个entry  CheckOut其他相同设备 顾放在CheckOut其他相同设备的上面
			}*/
			
			
			if(checkOutFlag==1){
				AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				long adid = adminLoginBean.getAdid();
				DBRow data = new DBRow();
				data.add("gate_container_no", StringUtil.getString(request, "gate_container_no"));
				data.add("liscense_plate", StringUtil.getString(request, "liscense_plate"));
				data.add("adid", adid);
				data.add("ps_id", adminLoginBean.getPs_id());
				//给新添加的entry添加checkOut其他设备的日志
				checkInMgrWfh.addCheckOutLog(data, row.get("entry_id", 0L));
				//checkOut  其他相同的设备
				checkInMgrWfh.checkOutEquipment(row.get("entry_id", 0L),data);
			}
		
			if(row.get("entry_id", 0l)==0){

				result.add("error", "true");
			}else{
				//result =checkInMgrZwb.findGateCheckInById(row.get("main_id", 0l));
				result.add("entry_id",row.get("entry_id", 0l));
			}
		}
		catch (ResourceHasUsedException e) {
			throw new SendResponseServerCodeAndMessageException(50000,"["+e.getMessage()+" has occupied.]");
		}
		catch(FileNotFoundException e4){
			throw new SendResponseServerCodeAndMessageException(50000,"[File Not Found Exception.]");
		}
		catch(SystemException e2)
		{
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		catch(VerifyShippingCTNRException e){
			result.add("success", false);
			result.add("data", e.getMessage());
		}
		catch (Exception e5) {
			throw new SendResponseServerCodeAndMessageException(50000,"[System error.]");
		}
		
		throw new JsonException(new JsonObject(result));
		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
	

}
