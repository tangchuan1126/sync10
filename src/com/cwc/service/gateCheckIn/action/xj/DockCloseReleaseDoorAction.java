package com.cwc.service.gateCheckIn.action.xj;

import java.io.FileNotFoundException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class DockCloseReleaseDoorAction extends ActionFatherController{

	private CheckInMgrIfaceZr checkInMgrZr;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
			AdminLoginBean adminLoginBean  =convertObjectToAdminLoginBean(request.getSession().getAttribute(Config.adminSesion));
			long main_id=StringUtil.getLong(request, "main_id") ;
			long nofityflag=StringUtil.getLong(request, "nofityflag") ;
			long is_leave = StringUtil.getLong(request,"is_leave");
			long resources_id = StringUtil.getLong(request,"resources_id");
			int equipment_id = StringUtil.getInt(request,"equipment_id");
			int resources_type = StringUtil.getInt(request,"resources_type");
			DBRow data = new DBRow();
			data.add("entry_id", main_id);
			data.add("nofityflag", nofityflag);
			data.add("status", is_leave);
			data.add("resources_id", resources_id);
			data.add("equipment_id", equipment_id);
			data.add("resources_type", resources_type);
			long id =checkInMgrZr.DockCloseReleaseDoor(data,adminLoginBean);
			DBRow result=new DBRow();
			result.add("id", id);
			throw new JsonException(new JsonObject(result));
		
	}
	/**
	 * redis 的原因
	 * @param obj
	 * @return
	 * @author zhangrui
	 * @Date   2014年11月20日
	 */
	private  AdminLoginBean convertObjectToAdminLoginBean(Object obj){
		if(obj != null){
			if(obj instanceof AdminLoginBean){
				return (AdminLoginBean) obj ;
			}
			Map<String,Object> linkedMap = (Map<String,Object>)obj;
			AdminLoginBean loginBean = new AdminLoginBean();
			loginBean.setAccount(linkedMap.get("account").toString());
			//{account=admin, adid=100198, adgid=10000, loginDate=2014-11-10 09:29:31, province=0, city=0, ps_id=1000005, employe_name=系统管理, email=110106315@qq.com, attach=null, titles=[{TITLE_ID=251, TITLE_NAME=AMTRAN01}], login=true, loginRightPath=true}
			////system.out.println(linkedMap.get("adid").toString() + "adid ...");
	 		loginBean.setAdid((Integer)linkedMap.get("adid"));
	 		loginBean.setAdgid((Integer)linkedMap.get("adgid"));
	 		loginBean.setEmploye_name((String)linkedMap.get("employe_name"));
	 		loginBean.setLoginDate((String)linkedMap.get("loginDate"));
	 		loginBean.setPs_id((Integer)linkedMap.get("ps_id"));
	 		return loginBean ;
		}
		return null ;
	}
	
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	
	
	

}
