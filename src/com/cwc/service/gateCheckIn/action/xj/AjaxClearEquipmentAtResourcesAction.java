package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxClearEquipmentAtResourcesAction extends ActionFatherController{

	private CheckInMgrIfaceXj checkInMgrXj;
	


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long relation_id=StringUtil.getLong(request,"relation_id");
		int relation_type=StringUtil.getInt(request,"relation_type");
		long adminId=StringUtil.getLong(request,"adminId");
		this.checkInMgrXj.delSpaceResourcesRelation(relation_type, relation_id, adminId);;
		
	}



	public void setCheckInMgrXj(CheckInMgrIfaceXj checkInMgrXj) {
		this.checkInMgrXj = checkInMgrXj;
	}

	

}
