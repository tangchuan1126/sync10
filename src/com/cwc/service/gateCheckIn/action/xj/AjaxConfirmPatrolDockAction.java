package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zj.SQLServerMgrZJ;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxConfirmPatrolDockAction extends ActionFatherController{

	private CheckInMgrIfaceXj checkInMgrXj;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long sd_id = StringUtil.getLong(request, "sd_id");
		long dlo_id = StringUtil.getLong(request, "dlo_id");
		long area_id = StringUtil.getLong(request, "area_id");
		long id=this.checkInMgrXj.ConfirmPatrolDock(sd_id, dlo_id, area_id);
		DBRow row = new DBRow();
		row.add("id", id);
		throw new JsonException(new JsonObject(row));
		
		
	}

	public void setCheckInMgrXj(CheckInMgrIfaceXj checkInMgrXj) {
		this.checkInMgrXj = checkInMgrXj;
	}
	

}
