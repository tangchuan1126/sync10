package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxFindDockMainByCondition extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long ps_id = StringUtil.getLong(request, "ps_id");
		long entryId = StringUtil.getLong(request, "entryId");
		String license_plate = StringUtil.getString(request, "license_plate");
		String trailerNo = StringUtil.getString(request, "trailerNo");
		DBRow[] result = checkInMgrZwb.findDockMainMesByCondition(ps_id, entryId, license_plate, trailerNo);
		throw new JsonException(new JsonObject(result));
		
		
	}

	 
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	

}
