package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class AjaxGetAreaAction extends ActionFatherController {
	
	
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{
		long psId = StringUtil.getLong(request, "ps_id");
		//droop 类型
		int dropOffKey = StringUtil.getInt(request,"DropOffKey");
		//pick or delivery 类型 决定area_subtype 按照正序还是倒叙   delivery倒叙  pick相反
		int type = StringUtil.getInt(request,"type");
		long mainId = StringUtil.getLong(request, "mainId");
		DBRow[] rows = checkInMgrZwb.getAreaBydropOffKey(psId, dropOffKey,type,mainId) ;
		
		
		throw new JsonException(new JsonObject(rows));
		
		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

}
