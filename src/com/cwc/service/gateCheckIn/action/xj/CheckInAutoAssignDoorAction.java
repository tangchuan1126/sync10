package com.cwc.service.gateCheckIn.action.xj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.zj.SQLServerMgrZJ;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class CheckInAutoAssignDoorAction extends ActionFatherController{

	private CheckInMgrIfaceZwb checkInMgrZwb;
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception 
	{

		String area_name=StringUtil.getString(request,"area_name");
		String title =StringUtil.getString(request,"title");
		String search_number=StringUtil.getString(request,"search_number");
		long mainId=StringUtil.getLong(request,"mainId");
		long ps_id=StringUtil.getLong(request,"ps_id");
		long type = 0l;
		if(!StrUtil.isBlank(area_name)){
			type = 1;
		}
		if(!StrUtil.isBlank(title)){
			type = 2;
		}
		DBRow result =checkInMgrZwb.autoAssignDoor(area_name, title, type, search_number, mainId, ps_id);
		throw new JsonException(new JsonObject(result));
		
		
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	

}
