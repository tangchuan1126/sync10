package com.cwc.service.payment.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.PayMentMethodIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class AddPayMentMethodAction extends ActionFatherController{

	private PayMentMethodIfaceZR payMentMethodZr;
	
	
	public void setPayMentMethodZr(PayMentMethodIfaceZR payMentMethodZr) {
		this.payMentMethodZr = payMentMethodZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow row = new DBRow();
			int key = StringUtil.getInt(request, "key");
			String is_tip = StringUtil.getString(request, "is_tip");
			String account_name = StringUtil.getString(request, "account_name");
			String account = StringUtil.getString(request, "account");
			row.add("key_type", key);
			row.add("account_name", account_name);
			row.add("account", account);
			row.add("is_tip", is_tip);
			payMentMethodZr.addPayMentMethod(row);
			throw new JsonException("success");
			
		
	}

}
