package com.cwc.service.payment.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.iface.zr.PayMentMethodIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.util.StringUtil;

public class UpdatePayMentMethodAction extends ActionFatherController{

	private PayMentMethodIfaceZR payMentMethodZr;
	
	
	public void setPayMentMethodZr(PayMentMethodIfaceZR payMentMethodZr) {
		this.payMentMethodZr = payMentMethodZr;
	}


	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
			DBRow row = new DBRow();
			 
			int id = StringUtil.getInt(request,"id");
			String account_name = StringUtil.getString(request, "account_name");
			String account = StringUtil.getString(request, "account");
	 
			row.add("account_name", account_name);
			row.add("account", account);
			 
			payMentMethodZr.updatePayMentMethod(row, id);//(row);
			throw new JsonException("success");
			
		
	}

}
