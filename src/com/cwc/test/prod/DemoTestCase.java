package com.cwc.test.prod;

import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class DemoTestCase extends BaseTestCase {

	@Resource
	private FloorProductStoreMgr productStoreMgr;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test_physicalCount() throws Exception {
		
		for(long ps_id: new long[] {  0L, 1000005L, 1000006L, 1000007L }){
			Map<String,DBRow> pc = productStoreMgr.physicalCount(ps_id, 0, 0);
			for(Map.Entry<String,DBRow> e: pc.entrySet()){
				System.out.printf("%d -> %s -> %s\n", ps_id, e.getKey(), DBRowUtils.dbRowAsString(e.getValue()));
			}
		}
	}
}
