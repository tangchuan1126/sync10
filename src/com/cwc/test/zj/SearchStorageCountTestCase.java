package com.cwc.test.zj;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.key.ContainerTypeKey;

public class SearchStorageCountTestCase extends BaseTestCase {
	
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	long ps_id = 100000;
	long title_id = 18;
	long pc_id = 0;
	long clp_type_id = 0;
	long blp_type_id = 0;
	long ilp_type_id = 0;
	String lot_number = "";
	
	@Before
	public void setUp()
	{
		ps_id = 100006;
		title_id = 18;
		pc_id = 1093152;
		clp_type_id = 1051;
	}
	
	@Test
	public void searchProductStore()
		throws Exception
	{
		//正常数量
		int clp_store_count = productStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.CLP,clp_type_id, lot_number);	//CLP库存内数量
//		int blp_store_count = productStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.BLP,blp_type_id, lot_number);	//BLP库存内数量
//		int ilp_store_count = productStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.ILP,ilp_type_id, lot_number);	//ILP库存内数量//去掉ILP和BLP
		int orignail_store_count = productStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.Original,0, lot_number);	//库内散装放置的货物
		
		System.out.println("clp_store_count:"+clp_store_count);
		
	}
	
}
