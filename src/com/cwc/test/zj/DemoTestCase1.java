package com.cwc.test.zj;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.zj.InitDataMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;
import com.javaps.test.TestUtils;

public class DemoTestCase1 extends BaseTestCase {
	@Resource
	private YMSMgrAPI ymsMgrAPI;
	
	@Test
	public void test1()
	throws Exception
	{
		ymsMgrAPI.releaseResourcesByRelation(1, 1, 1);
	}
}
