package com.cwc.test.zj;

import java.net.SocketException;

import org.junit.Test;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.dhl.BarcodeDecoder;
import com.cwc.app.dhl.DHLClient;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class DHLTestCase extends BaseTestCase {
	
	@Test
	public void DHLRequest()
		throws Exception
	{
		String airWayBillNumber = "";
		String destination = "";
		double shippment_rate = 0;
		String errorMsg = "";
		try
		{
			DHLClient dhlClient = new DHLClient();
			
			dhlClient.setConsigneeCompanyName("(alfredo) Claudio auto servic");
			dhlClient.setConsigneeAddressLine("86 Sandford St");
			dhlClient.setConsigneeCity("New Brunswick");
			dhlClient.setConsigneeDivisionCode("New Jersey");
			dhlClient.setConsigneePostalCode("08901");
			dhlClient.setConsigneeCountryCode("US");
			dhlClient.setConsigneeCountryName("United States");
			dhlClient.setConsigneeContactPersonName("(alfredo) Claudio auto service");
			dhlClient.setConsigneeContactPhoneNumber("7322537107");
			dhlClient.setConsigneeContactEmailFrom("vvmecom@gmail.com");
			dhlClient.setConsigneeContactEmailTo("fredy39_2807@hotmail.com");
			dhlClient.setDutiableDeclaredValue("5.30");
			dhlClient.setShipmentDetailsNumberOfPieces("1");//String.valueOf(NumberOfPieces)
			dhlClient.setShipmentDetailsWeight(0.5f);
			
			//设置发票信息
			dhlClient.setContents("Lighting bulb for Car Accessories");
			
			//设置发件人信息
			dhlClient.setDev_CompanyName("WangJun");
			dhlClient.setDev_City("Guang Zhou");
			dhlClient.setDev_DivisionCode("GuangZhou");
			dhlClient.setDev_PostalCode("511442");
			dhlClient.setDev_CountryCode("CN");
			dhlClient.setDev_CountryName("China");
			
			dhlClient.setDev_AddressLine1("No.5th,Shun Yi Fang Avenue East Rd");
			dhlClient.setDev_AddressLine2("Yuan Gang Village,");
			dhlClient.setDev_AddressLine3("NanCun Town");
			dhlClient.setDev_PhoneNumber("+86 10 848-270-66");
			
			//设置关税支付方
			dhlClient.setDutyPaymentType("recipient");
	
			//dhlClient.setShipmentDetailsWeight(1.5f*order.get("quantity",0f));
			
			//weight 1.5 8 n
			
			dhlClient.setOid( String.valueOf(999) );
			
			airWayBillNumber = dhlClient.commit();
			destination = dhlClient.getDestination();
			
			
			if(airWayBillNumber.trim().equals(""))
			{
				throw new Exception();
			}
			
			errorMsg = dhlClient.getErrorPage();
			
		}
		catch(SocketException e)
		{
			System.out.println("错误信息：链接失败");
		}
		catch (Exception e) 
		{
			if(e.getMessage().indexOf("Server returned HTTP response code: 502 for URL: http://xmlpi.dhl-usa.com/XMLShippingServlet")>=0)
			{
				System.out.println("错误信息："+e.getMessage()+"<br/>DHL服务器异常，请与DHL联系！");
			}
		}
		
		if (airWayBillNumber.equals("0000000000"))
		{
			System.out.println("../TransformXMLtoHTML/ResponseXMLS/"+errorMsg);
		}
		else
		{
			new BarcodeDecoder(airWayBillNumber);
			System.out.println(airWayBillNumber);
		}
	}
	
	public static void main(String[] args) 
	{
		String airWayBillNumber = "";
		String destination = "";
		double shippment_rate = 0;
		String errorMsg = "";
		try
		{
			DHLClient dhlClient = new DHLClient();
			
			dhlClient.setConsigneeCompanyName("(alfredo) Claudio auto servic");
			dhlClient.setConsigneeAddressLine("86 Sandford St");
			dhlClient.setConsigneeCity("New Brunswick");
			dhlClient.setConsigneeDivisionCode("New Jersey");
			dhlClient.setConsigneePostalCode("08901");
			dhlClient.setConsigneeCountryCode("US");
			dhlClient.setConsigneeCountryName("United States");
			dhlClient.setConsigneeContactPersonName("(alfredo) Claudio auto service");
			dhlClient.setConsigneeContactPhoneNumber("7322537107");
			dhlClient.setConsigneeContactEmailFrom("vvmecom@gmail.com");
			dhlClient.setConsigneeContactEmailTo("fredy39_2807@hotmail.com");
			dhlClient.setDutiableDeclaredValue("5.30");
			dhlClient.setShipmentDetailsNumberOfPieces("1");//String.valueOf(NumberOfPieces)
			dhlClient.setShipmentDetailsWeight(0.5f);
			
			//设置发票信息
			dhlClient.setContents("Lighting bulb for Car Accessories");
			
			//设置发件人信息
			dhlClient.setDev_CompanyName("WangJun");
			dhlClient.setDev_City("Guang Zhou");
			dhlClient.setDev_DivisionCode("GuangZhou");
			dhlClient.setDev_PostalCode("511442");
			dhlClient.setDev_CountryCode("CN");
			dhlClient.setDev_CountryName("China");
			
			dhlClient.setDev_AddressLine1("No.5th,Shun Yi Fang Avenue East Rd");
			dhlClient.setDev_AddressLine2("Yuan Gang Village,");
			dhlClient.setDev_AddressLine3("NanCun Town");
			dhlClient.setDev_PhoneNumber("+86 10 848-270-66");
			
			//设置关税支付方
			dhlClient.setDutyPaymentType("recipient");
	
			//dhlClient.setShipmentDetailsWeight(1.5f*order.get("quantity",0f));
			
			//weight 1.5 8 n
			
			dhlClient.setOid( String.valueOf(999) );
			
			airWayBillNumber = dhlClient.commit();
			destination = dhlClient.getDestination();
			
			
			if(airWayBillNumber.trim().equals(""))
			{
				throw new Exception();
			}
			
			errorMsg = dhlClient.getErrorPage();
			
		}
		catch(SocketException e)
		{
			System.out.println("错误信息：链接失败");
		}
		catch (Exception e) 
		{
			if(e.getMessage().indexOf("Server returned HTTP response code: 502 for URL: http://xmlpi.dhl-usa.com/XMLShippingServlet")>=0)
			{
				System.out.println("错误信息："+e.getMessage()+"<br/>DHL服务器异常，请与DHL联系！");
			}
		}
		
		if (airWayBillNumber.equals("0000000000"))
		{
			System.out.println("../TransformXMLtoHTML/ResponseXMLS/"+errorMsg);
		}
		else
		{
			new BarcodeDecoder(airWayBillNumber);
			System.out.println(airWayBillNumber);
		}
	}
}
