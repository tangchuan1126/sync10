package com.cwc.test.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;


public class StorageAreaApproveTestCase extends BaseTestCase {
	
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	@Resource
	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	
	@Test
	public void storageAreaApprove()
		throws Exception
	{
		long area_id = 1000771;
		long adid = 100198;
		long ps_id = 100000;
		AdminLoginBean adminLoggerBean = new AdminLoginBean();
		adminLoggerBean.setAdid(adid);
		adminLoggerBean.setPs_id(ps_id);
		storageApproveMgrZJ.storageTakeStockNew(area_id,adminLoggerBean);
	}
}
