package com.cwc.test.zj;

import javax.annotation.Resource;

import org.junit.Test;

import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.db.DBRow;
import com.javaps.test.TestUtils;

import static org.junit.Assert.*;

public class DemoTestCase2 extends BaseTestCase {
	
	@Resource
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	
	@Resource
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;

	@Test
	public void test_1() throws Exception{
		DBRow[] pcs = floorProductCatalogMgrZJ.getAllProductCatalogChild();
		assertTrue(pcs != null && pcs.length>0);
		for(DBRow pc:pcs){
			System.out.println(TestUtils.dbRowAsString(pc));
		}
	}

	@Test
	public void test_2() throws Exception {
		DBRow[] types = floorLPTypeMgrZJ.getBLPTypeForSku(1000034);
		assertTrue(types != null && types.length>0);
		for(DBRow t:types){
			System.out.println(TestUtils.dbRowAsString(t));
		}
	}
}
