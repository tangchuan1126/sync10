package com.cwc.test.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.floor.api.zr.FloorContainerMgrZr;
import com.cwc.app.floor.api.zr.FloorTempContainerMgrZr;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class CreateCLPInnerDifferentApproveCase extends BaseTestCase {
	
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	@Resource
	private FloorLocationMgrZJ floorLocationMgrZJ;
	@Resource
	private FloorContainerMgrZr floorContainerMgrZr;
	@Resource
	private FloorProductMgr floorProductMgr;
	@Resource
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	@Resource
	private FloorTempContainerMgrZr floorTempContainerMgrZr;
	@Resource
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	@Resource
	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	
	static long ps_id;
	static long area_id;
	static int containerCount = 1;
	static long pc_id;
	static long title_id;
	static long clp_type_id;
	static int inner_piece = 0;
	long adid = 100198;
	Map<Long,ArrayList<Long>> locationCLPRelation = new HashMap<Long, ArrayList<Long>>();
	Map<Long,ArrayList<Long>> systemCLPBLPRelation = new HashMap<Long, ArrayList<Long>>();
	Map<Long,ArrayList<Long>> tempCLPBLPRelation = new HashMap<Long, ArrayList<Long>>();
	
	@Before
	public void initData()
		throws Exception
	{
		ps_id = 100000;
		area_id = 1000771;//1000781;//1000771;
		clp_type_id = 1051;
		title_id = 18;
		containerCount = 3;
		
		productStoreMgr.deleteOnArea(0, area_id);
		productStoreMgr.deleteOnArea(ps_id, area_id);
		 
		productStoreMgr.clearGraphDB(0, false);
		productStoreMgr.clearGraphDB(ps_id, false);
	}

	@Test
	public void Test()
		throws Exception
	{
		//创建仓库位置节点，放置容器
		this.createLocationNode();
		
		//创建实例库商品节点
		Map<String,Object> systemProductNode = this.createProductNode(ps_id, pc_id, title_id);
		//创建盘点库商品节点		
		Map<String,Object> tempProductNode = this.createProductNode(0, pc_id, title_id);
		//实例库
		this.addAllRelation(ps_id,systemProductNode,this.systemCLPBLPRelation);
		//临时库
		this.addAllRelation(0,tempProductNode,this.tempCLPBLPRelation);
		
		AdminLoginBean adminLoggerBean = new AdminLoginBean();
		adminLoggerBean.setAdid(adid);
		adminLoggerBean.setPs_id(ps_id);
		storageApproveMgrZJ.storageTakeStockNew(area_id, adminLoggerBean);
		
		DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
		for (int i = 0; i < locations.length; i++) 
		{
			DBRow row = locations[i];
			long slc_id = row.get("slc_id",0);
			
			Set<Long> result = productStoreMgr.compareTree(ps_id, 0,slc_id);
			System.out.println(slc_id+":"+result);
		}
	}
	
	@Ignore
	@Test
	public void test1() throws Exception
	{
		AdminLoginBean adminLoggerBean = new AdminLoginBean();
		adminLoggerBean.setAdid(adid);
		adminLoggerBean.setPs_id(ps_id);
		storageApproveMgrZJ.storageTakeStockNew(area_id, adminLoggerBean);
	}
	
	@Ignore
	@Test
	public void test2()throws Exception
	{
		DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
		locationCLPRelation = new HashMap<Long, ArrayList<Long>>();
		
		for (int i = 0; i < locations.length; i++) 
		{
			DBRow row = locations[i];
			long slc_id = row.get("slc_id",0);
			
			Set<Long> result = productStoreMgr.compareTree(ps_id, 0,slc_id);
			System.out.println(slc_id+":"+result);
		}
		
	}
	/**
	 * 创建所有容器节点，位置节点，同时初始化pc_id
	 * @throws Exception
	 */
	public void createLocationNode()
		throws Exception
	{
		DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
		locationCLPRelation = new HashMap<Long, ArrayList<Long>>();
		
		for (int i = 0; i < locations.length; i++) 
		{
			DBRow row = locations[i];
			long slc_id = row.get("slc_id",0);
			Map<String,Object> storageLocation = new HashMap<String, Object>();
			storageLocation.put("is_three_dimensional",row.get("is_three_dimensional",0));
//			storageLocation.put("ps_id",ps_id);
			storageLocation.put("slc_area",row.get("slc_area",0l));
			storageLocation.put("slc_id",row.get("slc_id",0));
			storageLocation.put("slc_position",row.getString("slc_position"));
			storageLocation.put("slc_position_all",row.getString("slc_position_all"));
			storageLocation.put("slc_type",row.getString("slc_type"));
			storageLocation.put("slc_x",row.get("slc_x",0f));
			storageLocation.put("slc_y",row.get("slc_y",0f));
			
			productStoreMgr.addNode(ps_id,NodeType.StorageLocationCatalog,storageLocation);
			productStoreMgr.addNode(0,NodeType.StorageLocationCatalog,storageLocation);
			
			ArrayList<Long> conIds = new ArrayList<Long>();
			for (int j = 0; j < containerCount; j++) 
			{
				long con_id = createSystemCLPNode(clp_type_id);
				
				createTempCLPNode(con_id, clp_type_id);
				
				conIds.add(con_id);
			}
			
			locationCLPRelation.put(slc_id,conIds);
		}
	}
	
	/**
	 * 创建实例库CLP节点
	 * @param clp_type_id
	 * @return
	 * @throws Exception
	 */
	public long createSystemCLPNode(long clp_type_id)
		throws Exception
	{
		DBRow container = new DBRow();
		container.add("type_id",clp_type_id);
		container.add("container_type",ContainerTypeKey.CLP);
		container.add("is_full",ContainerProductState.FULL);
		container.add("is_has_sn",ContainerHasSnTypeKey.NOSN);
		container.add("title_id",18);
		container.add("lot_number","");
		container.add("at_ps_id",ps_id);
		
		long clp_con_id;
		if (ps_id!=0)
		{
			DBRow insertRow = floorContainerMgrZr.addContainer(container);
			clp_con_id = insertRow.get("con_id",0l);
		}
		else
		{
			clp_con_id = floorTempContainerMgrZr.addTempContainer(container);
		}
			
		 //像图数据库添加节点
		 Map<String,Object> containerNode = new HashMap<String, Object>();
		 containerNode.put("con_id",clp_con_id);
		 containerNode.put("container_type",container.get("container_type", 0));
		 containerNode.put("type_id",container.get("type_id", 0l));
		 containerNode.put("container",container.getString("container"));
		 containerNode.put("is_full",ContainerProductState.EMPTY);
		 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
		 
		 productStoreMgr.addNode(ps_id,NodeType.Container,containerNode);
		 
		 DBRow clpType = floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
		 pc_id = clpType.get("sku_lp_pc_id",0l);
		 
		 long blp_id = clpType.get("sku_lp_box_type",0l);
		 int blp_count = clpType.get("sku_lp_total_box",0);
		 
		 if (blp_id ==0)
		 {
			inner_piece = blp_count;
		 }
		 
		 ArrayList<Long> blpConList = createBLPNodes(ps_id,clp_type_id,blp_id,blp_count);
		 
		 systemCLPBLPRelation.put(clp_con_id,blpConList);
		 
		 return clp_con_id;
	}
	
	public long createTempCLPNode(long clp_con_id,long clp_type_id)
			throws Exception
		{
			DBRow container = floorContainerMgrZr.getDetailContainerById(clp_con_id);
			floorTempContainerMgrZr.copyToTempContainer(container);
				
			 //像图数据库添加节点
			 Map<String,Object> containerNode = new HashMap<String, Object>();
			 containerNode.put("con_id",clp_con_id);
			 containerNode.put("container_type",container.get("container_type",0));
			 containerNode.put("type_id",container.get("type_id", 0l));
			 containerNode.put("container",container.getString("container"));
			 containerNode.put("is_full",ContainerProductState.EMPTY);
			 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
			 
			 productStoreMgr.addNode(0,NodeType.Container,containerNode);
			 
			 DBRow clpType = floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
			 pc_id = clpType.get("sku_lp_pc_id",0l);
			 
			 long blp_id = clpType.get("sku_lp_box_type",0l);
			 int blp_count = clpType.get("sku_lp_total_box",0);
			 
			 if (blp_id ==0)
			 {
				inner_piece = blp_count;
			 }
			 
			 ArrayList<Long> blpConList = createBLPNodes(0,clp_type_id,blp_id,blp_count);
			 
			 tempCLPBLPRelation.put(clp_con_id,blpConList);
			 
			 return clp_con_id;
		}
	
	public ArrayList<Long> createBLPNodes(long ps_id,long clp_id,long blp_type_id,int blp_count)
		throws Exception
	{
		ArrayList<Long> blpList = new ArrayList<Long>();
		if (blp_type_id!=0)
		{
			for (int i = 0; i < blp_count; i++) 
			{
				DBRow container = new DBRow();
				container.add("type_id",blp_type_id);
//				container.add("container_type",ContainerTypeKey.BLP);//去掉ILP和BLP
				container.add("is_full",ContainerProductState.FULL);
				container.add("is_has_sn",ContainerHasSnTypeKey.NOSN);
				container.add("title_id",18);
				container.add("lot_number","");
				container.add("at_ps_id",ps_id);
				
				long con_id;
				if (ps_id!=0)
				{
					DBRow insertRow = floorContainerMgrZr.addContainer(container);
					con_id = insertRow.get("con_id",0l);
				}
				else
				{
					con_id = floorTempContainerMgrZr.addTempContainer(container);
				}
					
				 //像图数据库添加节点
				 Map<String,Object> containerNode = new HashMap<String, Object>();
				 containerNode.put("con_id",con_id);
				 containerNode.put("container_type",container.get("container_type", 0));
				 containerNode.put("type_id",container.get("type_id", 0l));
				 containerNode.put("container",container.getString("container"));
				 containerNode.put("is_full",ContainerProductState.EMPTY);
				 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
				 
				 productStoreMgr.addNode(ps_id,NodeType.Container,containerNode);
				 
				 blpList.add(con_id);
				 
				 DBRow blp_type = floorLPTypeMgrZJ.getDetailBLPType(blp_type_id);
				 inner_piece = blp_type.get("box_total_piece",0);
			}
		}
		return blpList;
	}
	
	public Map<String,Object> createProductNode(long ps_id,long pc_id,long title_id)
			throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		HashMap mangoDBProduct = this.productMongoDB(pc_id);
		long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
		long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
			
		Map<String,Object> productNode = new HashMap<String, Object>();
		productNode.put("pc_id",pc_id);
		productNode.put("title_id",title_id);
		productNode.put("product_line",product_line_id);
		productNode.put("catalogs",catalogs);
		productNode.put("p_name",product.getString("p_name"));
		productNode.put("union_flag",product.get("union_flag",0));
		productStoreMgr.addNode(ps_id,NodeType.Product,productNode);//添加商品节点
			
		//移除商品节点不参与搜索的属性
		productNode.remove("product_line");
		productNode.remove("catalogs");
		productNode.remove("union_flag");
			
		return productNode;
	}
	
	public HashMap productMongoDB(long pc_id)
		throws Exception
	{
		HashMap result = new HashMap();
				
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		long catalog_id = product.get("catalog_id",0l);
				
		DBRow catalog = floorProductCatalogMgrZJ.getDetailProductCatalog(catalog_id);
		long product_line_id = catalog.get("product_line_id",0l);
				
		result.put("product_line_id",product_line_id);
				
		DBRow[] fatherCatalogs = floorProductCatalogMgrZJ.getFatherTree(catalog_id);
				
		ArrayList<Long> list = new ArrayList<Long>();
		for (int i = 0; i < fatherCatalogs.length; i++) 
		{
			list.add(fatherCatalogs[i].get("id",0l));
		}
		list.add(catalog_id);
				
		long[] catalogs = new long[list.size()];
		for (int i = 0; i < catalogs.length; i++) 
		{
			catalogs[i] = list.get(i).longValue();
		}
		result.put("catalogs",catalogs);
			
		return result;
	}
	
	public void bLPToCLPRelation(long ps_id,long clp_con_id,long blp_con_id)
		throws Exception
	{
		HashMap<String,Object> parentContainerNode = new HashMap<String, Object>();
		parentContainerNode.put("con_id", clp_con_id);
		
		HashMap<String,Object> sonContainerNode = new HashMap<String, Object>();
		sonContainerNode.put("con_id",blp_con_id);
			
		productStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.Container,parentContainerNode,sonContainerNode,new HashMap<String,Object>());
	}
	
	public void putProductToLP(long ps_id,long con_id,Map<String,Object> productNode,long title_id,int quantity)
		throws Exception
	{
		Map<String,Object> containerNode = new HashMap<String, Object>();
		containerNode.put("con_id",con_id);

		Map<String,Object> relProps = new HashMap<String, Object>();
		relProps.put("quantity",quantity);
		relProps.put("locked_quantity",0);
			
		productStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.Product,containerNode,productNode,relProps);
	}
	
	public void containerToLocationRelation(long ps_id,long slc_id,long con_id,long pc_id,long title_id)
		throws Exception
	{
		Map<String,Object> storageLocationCatalogNode = new HashMap<String, Object>();
		storageLocationCatalogNode.put("slc_id",slc_id);
		
		Map<String,Object> containerNode = new HashMap<String, Object>();
		containerNode.put("con_id",con_id);
			
		Map<String,Object> locates = new HashMap<String, Object>();
		locates.put("pc_id",pc_id);
		locates.put("title_id",title_id);
		locates.put("lot_number","");
		locates.put("time_number",0);
			
		productStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.StorageLocationCatalog,containerNode,storageLocationCatalogNode, locates);
	}
	
	public void addAllRelation(long ps_id,Map<String,Object> productNode,Map<Long,ArrayList<Long>> clpblpRelation)
		throws Exception
	{
		//Neo4j add CLP&BLP Product Relation
		Set<Long> clpList = clpblpRelation.keySet();
		
		for (Long clp_con_id : clpList)
		{
			ArrayList<Long> blpList = clpblpRelation.get(clp_con_id);
			if (blpList.size()>0)
			{
				for (int i = 0; i < blpList.size(); i++) 
				{
					long blp_con_id = blpList.get(i);
					bLPToCLPRelation(ps_id, clp_con_id,blp_con_id);
						
					putProductToLP(ps_id,blp_con_id, productNode,title_id,inner_piece);
				}
			}
			else
			{
				putProductToLP(ps_id,clp_con_id,productNode,title_id,inner_piece);
			}
			
		}
		
		//Neo4j add CLP&Location Relation
		Set<Long> slc_ids = this.locationCLPRelation.keySet();
		for (Long slc_id : slc_ids)
		{
			ArrayList<Long> clpConList = locationCLPRelation.get(slc_id);
			for (int i = 0; i < clpConList.size(); i++) 
			{
				containerToLocationRelation(ps_id, slc_id,clpConList.get(i),pc_id,title_id);
			}
		}
	}
	
	public void addTempRelation(long ps_id,Map<String,Object> productNode,Map<Long,ArrayList<Long>> clpblpRelation)
			throws Exception
		{
			//Neo4j add CLP&BLP Product Relation
			Set<Long> clpList = clpblpRelation.keySet();
			
			for (Long clp_con_id : clpList)
			{
				ArrayList<Long> blpList = clpblpRelation.get(clp_con_id);
				if (blpList.size()>0)
				{
					for (int i = 0; i < blpList.size(); i++) 
					{
						long blp_con_id = blpList.get(i);
						bLPToCLPRelation(ps_id, clp_con_id,blp_con_id);
							
						putProductToLP(ps_id,blp_con_id, productNode,title_id,inner_piece);
					}
				}
				else
				{
					putProductToLP(ps_id,clp_con_id,productNode,title_id,inner_piece);
				}
				
			}
			
		}
}
