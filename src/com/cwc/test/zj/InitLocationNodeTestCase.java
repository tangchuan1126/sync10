package com.cwc.test.zj;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.junit.Test;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.db.DBRow;
import com.cwc.test.zj.BaseTestCase;

public class InitLocationNodeTestCase extends BaseTestCase 
{
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	@Resource
	private FloorLocationMgrZJ floorLocationMgrZJ;
	
	long ps_id = 100000;
	
	@Test
	public void createLocationNode()
		throws Exception
	{
		DBRow[] areas = floorLocationMgrZJ.getLocationAreaByPsid(ps_id);
		
		ArrayList<DBRow> locationList = new ArrayList<DBRow>();
		
		for (int i = 0; i < areas.length; i++) 
		{
			DBRow[] location = floorLocationMgrZJ.getLocationCatalogsByAreaId(areas[i].get("area_id",0l),null,null,null);
			
			for (int j = 0; j < location.length; j++) 
			{
				DBRow locationNode = new DBRow();
				locationNode.add("is_three_dimensional",location[j].get("is_three_dimensional",0));
				locationNode.add("slc_area",location[j].get("slc_area",0l));
				locationNode.add("slc_id",location[j].get("slc_id",0));
				locationNode.add("slc_position",location[j].getString("slc_position"));
				locationNode.add("slc_position_all",location[j].getString("slc_position_all"));
				locationNode.add("slc_type",location[j].getString("slc_type"));
				locationNode.add("slc_x",location[j].get("slc_x",0f));
				locationNode.add("slc_y",location[j].get("slc_y",0f));
				
				locationList.add(locationNode);
			}
			
			productStoreMgr.addNodes(ps_id,NodeType.StorageLocationCatalog,locationList.toArray(new DBRow[0]));
		}
	}
}
