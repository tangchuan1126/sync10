package com.cwc.test.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.floor.api.zr.FloorAndroidMgrZr;
import com.cwc.app.floor.api.zr.FloorContainerMgrZr;
import com.cwc.app.floor.api.zr.FloorTempContainerMgrZr;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;


public class TLPStorageTestCase extends BaseTestCase {
	
	@Resource
	private FloorLocationMgrZJ floorLocationMgrZJ;
	@Resource
	private FloorContainerMgrZr floorContainerMgrZr;
	@Resource
	private FloorProductMgr floorProductMgr;
	@Resource
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	@Resource
	private FloorTempContainerMgrZr floorTempContainerMgrZr;
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	@Resource
	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	@Resource
	private FloorAndroidMgrZr floorAndroidMgrZr;
	
	long area_id = 1000777;
	long adid = 100198;
	long ps_id = 100000;
	long default_ps_id = 0;
	
	long pc_id = 1092955;
	long title_id = 18;
	
	int default_container_count = 5;
	int approve_container_count = 3;
	
	int default_inner_count = 50;
	int approve_inner_count = 100;
	
	@Before
	public void setUp()
		throws Exception
	{
		 area_id = 1000226;
		 adid = 100198;
		 ps_id = 1000005;
		 default_ps_id = 0;
		
		 pc_id = 1092969;
		 title_id = 18;
		
		 default_container_count = 3;
		 approve_container_count = 2;
		
		 default_inner_count = 50;
		 approve_inner_count = 100;
		 
		productStoreMgr.deleteOnArea(default_ps_id, area_id);
		productStoreMgr.deleteOnArea(ps_id, area_id);
		 
		productStoreMgr.clearGraphDB(default_ps_id, false);
		productStoreMgr.clearGraphDB(ps_id, false);	
	}
	
	@Test
	public void creatStorageCount()
		throws Exception
	{
		DBRow[] areas = floorLocationMgrZJ.getLocationAreaByPsid(ps_id);
		
		for (int i = 0; i < areas.length; i++) 
		{
			//创建实际库的数据
			Map<Long,ArrayList<Long>> containerLocation = createStorageLocationNode(ps_id,areas[i].get("area_id",0l),approve_container_count,approve_inner_count);
		}
		
	}
	
	public Map<Long,ArrayList<Long>> createStorageLocationNode(long ps_id,long area_id,int container_count,int inner_count)
		throws Exception
	{
		
		DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
		
		Map<String,Object> productNode = createProductNode(ps_id,pc_id, title_id);
		Map<Long,ArrayList<Long>> containerLocation = new HashMap<Long, ArrayList<Long>>();
		
		
		for (int i = 0; i < locations.length; i++) 
		{
			DBRow row = locations[i];
			long slc_id = row.get("slc_id",0);
			Map<String,Object> storageLocation = new HashMap<String, Object>();
			storageLocation.put("is_three_dimensional",row.get("is_three_dimensional",0));
//			storageLocation.put("ps_id",ps_id);
			storageLocation.put("slc_area",row.get("slc_area",0l));
			storageLocation.put("slc_id",row.get("slc_id",0));
			storageLocation.put("slc_position",row.getString("slc_position"));
			storageLocation.put("slc_position_all",row.getString("slc_position_all"));
			storageLocation.put("slc_type",row.getString("slc_type"));
			storageLocation.put("slc_x",row.get("slc_x",0f));
			storageLocation.put("slc_y",row.get("slc_y",0f));
			
			productStoreMgr.addNode(ps_id,NodeType.StorageLocationCatalog,storageLocation);
			
			ArrayList<Long> conIds = new ArrayList<Long>();
			for (int j = 0; j < container_count; j++) 
			{
				long con_id = createTLPContainer(ps_id,slc_id,title_id,pc_id);
				
				conIds.add(con_id);
			}
			
			containerLocation.put(slc_id,conIds);
		}
		
		addAllRelation(ps_id,productNode, containerLocation,inner_count);
		
		return containerLocation;
	}
	
	public void createDefaultLocationNode(long default_ps_id,long area_id,int approve_container_count,int approve_inner_count,Map<Long,ArrayList<Long>> storageLocationContainer)
		throws Exception
	{
		DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
		
		Map<String,Object> productNode = createProductNode(default_ps_id,pc_id, title_id);
		Map<Long,ArrayList<Long>> containerLocation = new HashMap<Long, ArrayList<Long>>();
		
		
		for (int i = 0; i < locations.length; i++) 
		{
			DBRow row = locations[i];
			long slc_id = row.get("slc_id",0);
			Map<String,Object> storageLocation = new HashMap<String, Object>();
			storageLocation.put("is_three_dimensional",row.get("is_three_dimensional",0));
//			storageLocation.put("ps_id",ps_id);
			storageLocation.put("slc_area",row.get("slc_area",0l));
			storageLocation.put("slc_id",row.get("slc_id",0));
			storageLocation.put("slc_position",row.getString("slc_position"));
			storageLocation.put("slc_position_all",row.getString("slc_position_all"));
			storageLocation.put("slc_type",row.getString("slc_type"));
			storageLocation.put("slc_x",row.get("slc_x",0f));
			storageLocation.put("slc_y",row.get("slc_y",0f));
			
			productStoreMgr.addNode(default_ps_id,NodeType.StorageLocationCatalog,storageLocation);
			
			ArrayList<Long> conIds = new ArrayList<Long>();
			
			ArrayList<Long>storageLocationContainers = storageLocationContainer.get(slc_id);
			
			for (int j = 0; j < approve_container_count; j++) 
			{
				long con_id;
				
				try 
				{
					con_id = storageLocationContainers.get(j);
					con_id = this.copyTLPToTemp(con_id, slc_id, title_id, pc_id);
				}
				catch(IndexOutOfBoundsException e) 
				{
					con_id = this.createTLPContainer(default_ps_id, slc_id, title_id, pc_id);
				} 
				conIds.add(con_id);
			}
			
			containerLocation.put(slc_id,conIds);
		}
		
		addAllRelation(default_ps_id,productNode, containerLocation, approve_inner_count);
	}
	
	public void addAllRelation(long ps_id,Map<String,Object> productNode,Map<Long,ArrayList<Long>> containerLocation,int quantity)
		throws Exception
	{
		Set<Long> slc = containerLocation.keySet();
		
		long title_id = (long) productNode.get("title_id");
		long pc_id = (long) productNode.get("pc_id");
		
		for (Long slc_id:slc)
		{
			//容器与产品添加关系
			ArrayList<Long> containerIds = containerLocation.get(slc_id);
			for (int i = 0; i < containerIds.size(); i++) 
			{
				this.putProductToTLP(ps_id,containerIds.get(i), productNode, title_id, quantity);
				this.containerToLocationRelation(ps_id,slc_id,containerIds.get(i), pc_id, title_id);
			}
		}
	}
	
	public long createTLPContainer(long ps_id,long slc_id,long title_id,long pc_id)
			throws Exception
	{
		DBRow container = new DBRow();
		container.add("type_id",0);
		container.add("container_type",ContainerTypeKey.TLP);
		container.add("is_full",ContainerProductState.FULL);
		container.add("is_has_sn",ContainerHasSnTypeKey.NOSN);
		container.add("title_id",18);
		container.add("lot_number","");
		container.add("at_ps_id",ps_id);			
		
		long con_id;
		if (ps_id!=0)
		{
			DBRow insertRow = floorContainerMgrZr.addContainer(container);
			con_id = insertRow.get("con_id",0l);
		}
		else
		{
			con_id = floorTempContainerMgrZr.addTempContainer(container);
		}
		 //像图数据库添加节点
		 Map<String,Object> containerNode = new HashMap<String, Object>();
		 containerNode.put("con_id",con_id);
		 containerNode.put("container_type",container.get("container_type", 0));
		 containerNode.put("type_id",container.get("type_id", 0l));
		 containerNode.put("container",container.getString("container"));
		 containerNode.put("is_full",ContainerProductState.EMPTY);
		 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
		 
		 productStoreMgr.addNode(ps_id,NodeType.Container,containerNode);
		 
		 return con_id;
	}
	
	public long copyTLPToTemp(long con_id,long slc_id,long title_id,long pc_id)
		throws Exception
	{
		DBRow container = floorTempContainerMgrZr.getContainer(con_id);
		if(container == null)
		{
			container = floorAndroidMgrZr.getContainer(con_id);
			if(container != null)
			{
				floorTempContainerMgrZr.copyToTempContainer(container);	
			}
			else
			{
				return 0;
			}
		}
		
		 //像图数据库添加节点
		 Map<String,Object> containerNode = new HashMap<String, Object>();
		 containerNode.put("con_id",con_id);
		 containerNode.put("container_type",container.get("container_type", 0));
		 containerNode.put("type_id",container.get("type_id", 0l));
		 containerNode.put("container",container.getString("container"));
		 containerNode.put("is_full",ContainerProductState.EMPTY);
		 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
		 
		 productStoreMgr.addNode(default_ps_id,NodeType.Container,containerNode);
		 
		 return con_id;
	}
	
	
	public Map<String,Object> createProductNode(long ps_id,long pc_id,long title_id)
		throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		HashMap mangoDBProduct = this.productMongoDB(pc_id);
		long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
		long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
		
		Map<String,Object> productNode = new HashMap<String, Object>();
		productNode.put("pc_id",pc_id);
		productNode.put("title_id",title_id);
		productNode.put("product_line",product_line_id);
		productNode.put("catalogs",catalogs);
		productNode.put("p_name",product.getString("p_name"));
		productNode.put("union_flag",product.get("union_flag",0));
		productStoreMgr.addNode(ps_id,NodeType.Product,productNode);//添加商品节点
		
		//移除商品节点不参与搜索的属性
		productNode.remove("product_line");
		productNode.remove("catalogs");
		productNode.remove("union_flag");
		
		return productNode;
	}
	
	public void putProductToTLP(long ps_id,long con_id,Map<String,Object> productNode,long title_id,int quantity)
		throws Exception
	{
		Map<String,Object> containerNode = new HashMap<String, Object>();
		containerNode.put("con_id",con_id);

		Map<String,Object> relProps = new HashMap<String, Object>();
		relProps.put("quantity",quantity);
		relProps.put("locked_quantity",0);
		
		productStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);
	}
	
	public void containerToLocationRelation(long ps_id,long slc_id,long con_id,long pc_id,long title_id)
		throws Exception
	{
		Map<String,Object> containerNode = new HashMap<String, Object>();
		containerNode.put("con_id",con_id);
		
		Map<String,Object> storageLocationCatalogNode = new HashMap<String, Object>();
		storageLocationCatalogNode.put("slc_id",slc_id);
		 
		Map<String,Object> locates = new HashMap<String, Object>();
		locates.put("pc_id",pc_id);
		locates.put("title_id",title_id);
		locates.put("lot_number","");
		locates.put("time_number",0);
		
		productStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.StorageLocationCatalog,containerNode,storageLocationCatalogNode, locates);
	}
	
	public HashMap productMongoDB(long pc_id)
		throws Exception
	{
		HashMap result = new HashMap();
			
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		long catalog_id = product.get("catalog_id",0l);
			
		DBRow catalog = floorProductCatalogMgrZJ.getDetailProductCatalog(catalog_id);
		long product_line_id = catalog.get("product_line_id",0l);
			
		result.put("product_line_id",product_line_id);
			
		DBRow[] fatherCatalogs = floorProductCatalogMgrZJ.getFatherTree(catalog_id);
			
		ArrayList<Long> list = new ArrayList<Long>();
		for (int i = 0; i < fatherCatalogs.length; i++) 
		{
			list.add(fatherCatalogs[i].get("id",0l));
		}
		list.add(catalog_id);
			
		long[] catalogs = new long[list.size()];
		for (int i = 0; i < catalogs.length; i++) 
		{
			catalogs[i] = list.get(i).longValue();
		}
		result.put("catalogs",catalogs);
			
		return result;
	}
}
