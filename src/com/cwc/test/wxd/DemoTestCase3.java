package com.cwc.test.wxd;

import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class DemoTestCase3 extends BaseTestCase {
	
	@Resource
	TestTransactionRollbackIFace  proxySqlServerMgrZJ;
	
	@Resource
	FloorAdminMgr  floorAdminMgr;
	
	@Resource
	DBUtilAutoTran dbUtilAutoTran;
	
	@Resource
	DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	@Before
	public void setUp() throws Exception {
		dbUtilAutoTran.executeSQL("delete from admin where adgid=999");
		dbUtilAutoTranSQLServer.executeSQL("delete from Users where UserID like 'TEST%'");
	}
	
	@Test
	public void test_1() throws Exception {
		try{
			proxySqlServerMgrZJ.testTransactionRollback(999);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		DBRow[] result = floorAdminMgr.getAdminByAdgid(999, null);
		assertTrue(result.length == 2);
		assertTrue("testAdminUser2".equals(result[0].get("ACCOUNT", "")));
		assertTrue("testAdminUser1".equals(result[1].get("ACCOUNT", "")));
	}
	
	

}
