package com.cwc.test.wxd;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class /*,DemoTestCase3.class */ })
public class TestSuite {
	
}

