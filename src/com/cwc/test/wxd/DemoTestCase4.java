package com.cwc.test.wxd;

import javax.annotation.Resource;

import org.junit.Test;

import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class DemoTestCase4 extends BaseTestCase {
	@Resource
	private CacheDemo cacheDemo;
	
	@Test
	public void test_cacheDemo() throws Exception {
		for(int i=0; i<10; i++)
			System.out.println(DBRowUtils.dbRowArrayAsJSON(cacheDemo.searchAdminsByAccount("test%")));
		
		DBRow props = new DBRow();
		long randNum = 9000000 + (long)(Math.floor(Math.random()*1000000));
		props.add("adid", randNum);
		props.add("account", "test_"+randNum);
		cacheDemo.addAdmin(props);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(cacheDemo.searchAdminsByAccount("test%")));
	}
}
