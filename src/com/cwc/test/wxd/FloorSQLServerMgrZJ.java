package com.cwc.test.wxd;

import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;
import com.cwc.app.util.StrUtil;

public class FloorSQLServerMgrZJ 
{
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	public DBRow[] getLoadingSQLServer(String search_number,String ps_name,int mumber_type) 
		throws Exception 
	{
		String sql = "";//"select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where Status = 'Open' and Facility = '"+ps_name+"'";
		
		if(mumber_type==GateCheckLoadingTypeKey.LOAD)
		{
			sql +="select TOP 1 UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported' or Status = 'Picking' or Status = 'Picked') and Facility = '"+ps_name+"' and type = 'Outbount' ";
			sql +=" and LoadNo = '"+search_number+"'";
			sql +=" GROUP BY StagingAreaID,CarrierID,AppointmentDate ORDER BY COUNT(StagingAreaID) DESC";
		}
		else if (mumber_type==GateCheckLoadingTypeKey.CTNR)
		{
			sql +="select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported') and type = 'Inbound' and Facility = '"+ps_name+"'";			
			sql +=" and ContainerNo = '"+search_number+"'";
		}
		else if(mumber_type==GateCheckLoadingTypeKey.BOL)
		{
			sql +="select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported') and type = 'Inbound' and Facility = '"+ps_name+"'";
			sql +=" and BOLNo = '"+search_number+"'";
	    }
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	public DBRow[] getLoadingAll()
	throws Exception
	{
		String sql = "select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where Status = 'Open' ";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	public DBRow[] findB2BOrderWmsByLoadNo(String loadNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from MasterBOLs where LoadNo = '"+loadNo+"'");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.addB2BOrderByJN error:"+e);
		}
	}
	
	public DBRow[] findB2BOrderItemPlatesWmsByMasterBolNo(long masterBOLNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.MasterBOLNo, boll.OrderNo, odl.Pallets, odl.ItemID, od.StagingAreaID, odlp.PlateNo, odlp.ShippedQty, od.PickingType, od.CustomerID, od.CompanyID ");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" JOIN OrderLinePlates odlp ON odl.[LineNo]=odlp.[LineNo] AND odl.OrderNo = odlp.OrderNo AND bol.CompanyID = odlp.CompanyID ");
			sql.append(" WHERE bol.MasterBOLNo = ").append(masterBOLNo);
			sql.append(" AND bol.CompanyID = '").append(companyId).append("'");
			sql.append(" ORDER BY boll.OrderNo, odl.ItemID, odlp.PlateNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findB2BOrderItemPlatesWmsByMasterBolNo error:"+e);
		}
	}
	
	public DBRow findLocationWmsByPlateNo(int plateNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT TOP 1 * FROM Movements WHERE PlateNo = ").append(plateNo);
			sql.append(" AND CompanyID = '").append(companyId).append("' ORDER BY DateCreated DESC ;");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findLocationWmsByPlateNo error:"+e);
		}
	}
	
	
	public DBRow[] findItemInfoByItemId(String ItemID, String CompanyID, String CustomerID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM CustomerItems WHERE 1=1");
			if(!StrUtil.isBlank(ItemID))
			{
				sql.append(" AND ItemID = '").append(ItemID).append("'");
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID = '").append(CustomerID).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findItemInfoByItemId error:"+e);
		}
	}
	
	public DBRow findReceiptByContainerNo(String containerNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 SupplierID,CarrierID,AppointmentDate,");
			sql.append(" (select count(*) countDate from (select count(*) countGroupDate from Receipts where ContainerNo = '").append(containerNo).append("' and Status != 'Closed' and Status != 'Received' group by AppointmentDate) a) countDate");
			sql.append(" from Receipts where ContainerNo = '").append(containerNo).append("' and Status != 'Closed' and Status != 'Received'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findReceiptByContainerNo error:"+e);
		}
	}
	
	public DBRow findReceiptByBolNo(String BolNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 SupplierID,CarrierID,AppointmentDate,");
			sql.append(" (select count(*) countDate from (select count(*) countGroupDate from Receipts where BOLNo = '").append(BolNo).append("' and Status != 'Closed' and Status != 'Received' group by AppointmentDate) a) countDate");
			sql.append(" from Receipts where BOLNo = '").append(BolNo).append("' and Status != 'Closed' and Status != 'Received'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findReceiptByBolNo error:"+e);
		}
	}
	
	public DBRow findOrderInfoByLoadNo(String loadNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1  bols.CustomerID, bols.CarrierID, bols.AppointmentDate, ");
			sql.append(" (select top 1 a.StagingAreaID from (");
			sql.append(" select count(*) countStaging, od.StagingAreaID");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");
			sql.append(" where bol.LoadNo = '").append(loadNo).append("'");
			sql.append(" GROUP BY od.StagingAreaID) a ORDER BY a.countStaging desc) StagingAreaID");
			sql.append(" from MasterBOLs bols ");
			sql.append(" where bols.LoadNo = '").append(loadNo).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findOrderInfoByLoadNo error:"+e);
		}
	}
	
	public DBRow findOrdersByLoadNo(String loadNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select boll.OrderNo, odl.Pallets, odl.ItemID, od.StagingAreaID, od.PickingType, od.CustomerID ");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND od.CompanyID = odl.CompanyID ");
			sql.append(" where bol.LoadNo = '").append(loadNo).append("'");
			sql.append(" ORDER BY boll.OrderNo");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findOrdersByLoadNo error:"+e);
		}
	}
	
	public DBRow findOrdersPlatesByOrderNoLineNoCompany(String CompanyID, int OrderNo, int LineNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select  odlp.PlateNo, odlp.ShippedQty");
			sql.append(" from OrderLinePlates odlp ");
			sql.append(" where odlp.OrderNo = ").append(OrderNo);
			sql.append(" AND odlp.[LineNo] = ").append(LineNo);
			sql.append(" AND odlp.CompanyID = '").append(CompanyID).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findOrdersPlatesByOrderNoLineNoCompany error:"+e);
		}
	}
	
	public DBRow findPalletsByOrderNoCompanyId(String CompanyID, long OrderNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(Pallets) as palletNum from OrderLines ");
			sql.append(" where OrderNo = ").append(OrderNo);
			sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("floorB2BOrderMgrZyj.findPalletsByOrderNoCompanyId error:"+e);
		}
	}

	public void setDbUtilAutoTranSQLServer(
			DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}	
	
	public void addUser(String userId,String userName, String password,String companyId) throws Exception {
		DBRow row = new DBRow();
		row.add("UserID", userId);
		row.add("UserName", userName);
		row.add("Password", password);
		row.add("CompanyID", companyId);
		
		dbUtilAutoTranSQLServer.insert("Users", row);
	}
}
