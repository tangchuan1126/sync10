package com.cwc.test.wxd;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.key.WmsSearchItemStatusKey;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class SQLServerMgrZJ implements TestTransactionRollbackIFace {
	private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
	private FloorAdminMgr floorAdminMgr;
	private TransactionTemplate sqlServerTransactionTemplate;

	public TransactionTemplate getSqlServerTransactionTemplate() {
		return sqlServerTransactionTemplate;
	}

	public void setSqlServerTransactionTemplate(
			TransactionTemplate sqlServerTransactionTemplate) {
		this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
	}

	public FloorAdminMgr getFloorAdminMgr() {
		return floorAdminMgr;
	}

	public void setFloorAdminMgr(FloorAdminMgr adminMgr) {
		this.floorAdminMgr = adminMgr;
	}

	public DBRow[] getLoading(HttpServletRequest request) throws Exception {

		int number_type = StringUtil.getInt(request, "number_type");
		String search_number = StringUtil.getString(request, "search_number");
		String ps_name = StringUtil.getString(request, "ps_name");

		// return floorSQLServerMgrZJ.getLoadingAll();

		return floorSQLServerMgrZJ.getLoadingSQLServer(search_number, ps_name,
				number_type);
	}

	public DBRow[] getLoading(int number_type, String search_number,
			String ps_name) throws Exception {
		return floorSQLServerMgrZJ.getLoadingSQLServer(search_number, ps_name,
				number_type);
	}

	public DBRow[] findB2BOrderWmsByLoadNo(String loadNo) throws Exception {
		DBRow[] bolRows = floorSQLServerMgrZJ.findB2BOrderWmsByLoadNo(loadNo);
		for (int i = 0; i < bolRows.length; i++) {
			DBRow[] orderRows = floorSQLServerMgrZJ
					.findB2BOrderItemPlatesWmsByMasterBolNo(
							bolRows[i].get("MasterBOLNo", 0L),
							bolRows[i].getString("CompanyID"));
			int error_order_status = 0;
			String orderNo = "";
			String itemId = "";
			for (int j = 0; j < orderRows.length; j++) {
				DBRow[] itemRows = floorSQLServerMgrZJ.findItemInfoByItemId(
						orderRows[j].getString("ItemID"),
						orderRows[j].getString("CompanyID"),
						orderRows[j].getString("CustomerID"));
				if (0 == itemRows.length) {
					error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST;
					orderNo = orderRows[j].getString("OrderNo");
					itemId = orderRows[j].getString("ItemID");
				} else if (itemRows.length > 1) {
					error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_MORE;
					orderNo = orderRows[j].getString("OrderNo");
					itemId = orderRows[j].getString("ItemID");
				} else {
					error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_CORRECT;
				}
				if (0 != error_order_status) {
					break;
				}
			}
			bolRows[i].add("error_order_no", orderNo);
			bolRows[i].add("error_order_item", itemId);
			bolRows[i].add("error_order_status", error_order_status);
		}
		return bolRows;
	}

	public DBRow[] findB2BOrderItemPlatesWmsByMasterBolNo(long masterBOLNo,
			String companyId) throws Exception {
		DBRow[] orderRows = floorSQLServerMgrZJ
				.findB2BOrderItemPlatesWmsByMasterBolNo(masterBOLNo, companyId);
		for (int i = 0; i < orderRows.length; i++) {
			DBRow[] itemRows = floorSQLServerMgrZJ.findItemInfoByItemId(
					orderRows[i].getString("ItemID"),
					orderRows[i].getString("CompanyID"),
					orderRows[i].getString("CustomerID"));
			if (0 == itemRows.length) {
				orderRows[i].add("item_unit_info",
						WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST);
			} else if (itemRows.length > 1) {
				orderRows[i].add("item_unit_info",
						WmsSearchItemStatusKey.ITEM_UNIT_MORE);
			} else {
				orderRows[i].add("item_unit_info",
						WmsSearchItemStatusKey.ITEM_UNIT_CORRECT);
			}
		}
		return orderRows;
	}

	public DBRow findLocationWmsByPlateNo(int plateNo, String companyId)
			throws Exception {
		return floorSQLServerMgrZJ.findLocationWmsByPlateNo(plateNo, companyId);
	}

	public DBRow findOrderCaseQtyByItemIdQty(String ItemID, String CompanyID,
			double qty, String CustomerID) throws Exception {
		DBRow[] itemRows = floorSQLServerMgrZJ.findItemInfoByItemId(ItemID,
				CompanyID, CustomerID);
		if (itemRows.length > 0) {
			DBRow itemRow = itemRows[0];
			String Unit = itemRow.getString("Unit");
			double QtyPerUnit = itemRow.get("QtyPerUnit", 1);
			double UnitsPerPackage = itemRow.get("UnitsPerPackage", 1);
			double caseQty = 0d;
			double pieceQty = 0d;
			double innerQty = 0d;
			double caseSumQty = 0d;

			// 37 6 5
			if ("Package".equals(Unit)) {
				caseQty = qty;
				pieceQty = 0;
				innerQty = 0;
				caseSumQty = qty;
			} else if ("Pallet".equals(Unit)) {
				caseQty = qty;
				pieceQty = 0;
				innerQty = 0;
				caseSumQty = qty;
			} else if ("Piece".equals(Unit)) {
				caseQty = qty / UnitsPerPackage; // 6
				pieceQty = qty % UnitsPerPackage; // 1
				innerQty = 0;
				caseSumQty = caseQty;
				if (0d != pieceQty) {
					caseSumQty = caseSumQty + 1;
				}
			} else if ("Inner Case".equals(Unit)) {
				caseQty = qty / (UnitsPerPackage * QtyPerUnit); // 1
				pieceQty = qty % (UnitsPerPackage * QtyPerUnit) / QtyPerUnit;// 1
				innerQty = qty % (UnitsPerPackage * QtyPerUnit) % QtyPerUnit;// 2
				caseSumQty = caseQty;
				if (0d != pieceQty) {
					caseSumQty = caseSumQty + 1;
				}
			}

			DBRow reRow = new DBRow();
			reRow.add("case_qty", caseQty);
			reRow.add("piece_qty", pieceQty);
			reRow.add("inner_qty", innerQty);
			reRow.add("case_sum_qty", caseSumQty);
			reRow.add("unit", Unit);
			return reRow;
		} else {
			return new DBRow();
		}
	}

	public DBRow findReceiptByContainerNo(String containerNo) throws Exception {
		DBRow row = floorSQLServerMgrZJ.findReceiptByContainerNo(containerNo);
		if (null != row && row.get("countDate", 0) > 1) {
			row.add("AppointmentDate", "");
		}
		return row;
	}

	public DBRow findReceiptByBolNo(String BolNo) throws Exception {
		DBRow row = floorSQLServerMgrZJ.findReceiptByBolNo(BolNo);
		if (null != row && row.get("countDate", 0) > 1) {
			row.add("AppointmentDate", "");
		}
		return row;
	}

	public DBRow findOrderInfoByLoadNo(String loadNo) throws Exception {
		return floorSQLServerMgrZJ.findOrderInfoByLoadNo(loadNo);
	}

	public DBRow findPalletsByOrderNoCompanyId(String CompanyID, long OrderNo)
			throws Exception {
		return floorSQLServerMgrZJ.findPalletsByOrderNoCompanyId(CompanyID,
				OrderNo);
	}

	public void setFloorSQLServerMgrZJ(FloorSQLServerMgrZJ floorSQLServerMgrZJ) {
		this.floorSQLServerMgrZJ = floorSQLServerMgrZJ;
	}

	@Override
	public void testTransactionRollback(int adgid) throws Exception {
		final DBRow adminUser1 = new DBRow();
		final DBRow adminUser2 = new DBRow();
		adminUser1.add("account", "testAdminUser1");
		adminUser1.add("adgid", adgid);
		adminUser2.add("account", "testAdminUser2");
		adminUser2.add("adgid", adgid);

		
		sqlServerTransactionTemplate
				.execute(new TransactionCallbackWithoutResult() {

					@Override
					protected void doInTransactionWithoutResult(
							TransactionStatus txStat) {
						try {
							floorAdminMgr.addAdmin(adminUser1);
							floorSQLServerMgrZJ.addUser("TEST1", "TEST1", "TEST1", "TEST1");
							floorAdminMgr.addAdmin(adminUser2);
							if(System.currentTimeMillis() >0) throw new SQLException();
							
							floorSQLServerMgrZJ.addUser("TEST2", "TEST2", "TEST2", "TEST2");
							
							
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}

				});

		
		
		
	}
}
