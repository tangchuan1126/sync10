package com.cwc.test.wxd;

import com.cwc.cache.OscacheArithmetic;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class CacheDemo {

		private DBUtilAutoTran dbUtil;
		private OscacheArithmetic cacheMgr;

		public OscacheArithmetic getCacheMgr() {
			return cacheMgr;
		}

		public void setCacheMgr(OscacheArithmetic cacheMgr) {
			this.cacheMgr = cacheMgr;
		}

		public DBUtilAutoTran getDbUtil() {
			return dbUtil;
		}

		public void setDbUtil(DBUtilAutoTran dbUtil) {
			this.dbUtil = dbUtil;
		}
		
		
		public DBRow[]  searchAdminsByAccount(String account) throws Exception {
			String cacheKey = "CacheDemo["+account+"]";
			
			if(cacheMgr.getObject(cacheKey) != null) {
				//system.out.println("CACHE HIT!!!");
				return (DBRow[])cacheMgr.getObject(cacheKey);
			}
			//system.out.println("CACHE MISSING!!!");
			DBRow para = new DBRow();
			para.addObject("account", account);
			DBRow[] result= dbUtil.selectPreMutliple("select * from admin where account like ?", para);
			
			cacheMgr.putObject(new String[] {"admin"}, cacheKey, result);
			
			return result;
		}
		
		public void addAdmin(DBRow props) throws Exception {
			dbUtil.insert("admin", props);
			cacheMgr.flush(new String[]{ "admin" });
		}
		
		
		
}
