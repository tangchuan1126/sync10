package com.cwc.test.wxd;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class DemoTestCase2 extends BaseTestCase {

	@Resource
	private FloorProductStoreMgr productStoreMgr;

	@Before
	public void setUp() throws Exception {
		//productStoreMgr.clearGraphDB(0, true);
	}
	
	@Ignore
	@Test
	public void test_refreshRouter() throws Exception {
		System.out.println(productStoreMgr.refreshRouter());
	}
	
	@Ignore
	@Test
	public void test_addNode() throws Exception {
		long ps_id = 100006;
		Map<String,Object> props = new HashMap<String,Object>();
		
		props.put("con_id", 999999L);
		props.put("container", "999999");
		props.put("container_type", 999999);
		props.put("is_full", 0);
		props.put("is_has_sn", 0);
		props.put("type_id", 999999);
		
		productStoreMgr.addNode(ps_id, NodeType.Container, props);
	}
	
	@Test
	public void test_availableCount() throws Exception {
		long ps_id = 100000;
		long title_id = 18;
		long pc_id = 1093053;
		int container_type=1;
		int type_id = 1048;
		String lot_number = "";
		
		int clp_store_count = productStoreMgr.availableCount(ps_id, title_id, pc_id,container_type,type_id, lot_number);	//CLP库存内数量
		
		System.out.println(clp_store_count);
		
		DBRow[] alloc = productStoreMgr.allocateList(ps_id, title_id, pc_id, container_type, type_id, lot_number, 2);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(alloc).toString(4));
		
		Set<String> groupby= new HashSet<String>();
		groupby.add("pc_id");
		
		alloc = productStoreMgr.productCount(ps_id, 0, 0, container_type, new long[]{title_id}, pc_id, lot_number, null, 0, groupby);
		
		System.out.println(DBRowUtils.dbRowArrayAsJSON(alloc).toString(4));
		
//		alloc = productStoreMgr.allocateProductStore(ps_id, pc_id, title_id, container_type, type_id, 2, lot_number);
//		System.out.println(DBRowUtils.dbRowArrayAsJSON(alloc).toString(4));
		
		alloc = productStoreMgr.productCount(ps_id, 0, 0, container_type, new long[]{title_id}, pc_id, lot_number, null, 0, groupby);
		
		System.out.println(DBRowUtils.dbRowArrayAsJSON(alloc).toString(4));
	}

	@Ignore
	@Test(expected = Exception.class)
	public void test_addRelation() throws Exception {
		long src_ps_id = 100000;
		long dest_ps_id = 0;
		long con_id = 69554;
		long c_con_id = 69569;

		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, null);

		// 1. 测试已经存在的关系，再次添加
		productStoreMgr.addRelation(dest_ps_id, NodeType.Container,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", con_id)), DBRowUtils
						.jsonObjectAsMap(new JSONObject().put("con_id",
								c_con_id)), new HashMap<String, Object>());

		// 2. 测试不存在的关系，添加（先删除已存在的一条关系，再添加）
		productStoreMgr.removeRelations(dest_ps_id, NodeType.Container,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", con_id)), DBRowUtils
						.jsonObjectAsMap(new JSONObject().put("con_id",
								c_con_id)), new HashMap<String, Object>());
		productStoreMgr.addRelation(dest_ps_id, NodeType.Container,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", con_id)), DBRowUtils
						.jsonObjectAsMap(new JSONObject().put("con_id",
								c_con_id)), new HashMap<String, Object>());

		// 3. 测试在不存在的节点间建关系，抛出异常视为正确
		productStoreMgr.addRelation(dest_ps_id, NodeType.Container,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", con_id)),
				DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", 66666)), new HashMap<String, Object>());

	}

	@Ignore
	@Test
	public void test_copyTree() throws Exception {
		long src_ps_id = 100000;
		long dest_ps_id = 0;
		long con_id = 69554;

		// 先测一下copyTree
		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, null);

		DBRow[] cons = productStoreMgr.searchNodes(src_ps_id,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("container_type", 2)));

		// 在目标节点下加一组干扰节点，以测试删除是否干净

		long mock_con_id = 66660000;
		DBRow con = cons[0];
		con.remove("CON_ID");
		con.add("con_id", mock_con_id); // 将con_id修改为模拟值
		productStoreMgr.addNode(dest_ps_id, NodeType.Container,
				DBRowUtils.dbRowAsMap(con));
		productStoreMgr.addRelation(dest_ps_id, NodeType.Container,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("con_id", con_id)), DBRowUtils
						.jsonObjectAsMap(new JSONObject().put("con_id",
								con.get("CON_ID", 0L))),
				new HashMap<String, Object>());

		// 测试重复拷贝（应该会引发先删除的动作）
		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, null);
	}

	@Ignore
	@Test
	public void test_removeNodes() throws Exception {
		long src_ps_id = 100000;
		long dest_ps_id = 0;
		long con_id = 69554;

		// 先测一下copyTree
		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, null);

		DBRow[] cons = productStoreMgr.searchNodes(src_ps_id,
				NodeType.Container, DBRowUtils.jsonObjectAsMap(new JSONObject()
						.put("container_type", 2)));

		// 在目标节点下加一组干扰节点，以测试删除是否干净

		long mock_con_id = 66660000;
		for (DBRow con : cons) {
			con.remove("CON_ID");
			con.add("con_id", mock_con_id++); // 将con_id修改为模拟值
			productStoreMgr.addNode(dest_ps_id, NodeType.Container,
					DBRowUtils.dbRowAsMap(con));
			productStoreMgr.addRelation(dest_ps_id, NodeType.Container,
					NodeType.Container, DBRowUtils
							.jsonObjectAsMap(new JSONObject().put("con_id",
									con_id)), DBRowUtils
							.jsonObjectAsMap(new JSONObject().put("con_id",
									con.get("CON_ID", 0L))),
					new HashMap<String, Object>());
		}

		// 测试重复拷贝（应该会引发先删除的动作）
		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, null);

		Set<Long> containers = productStoreMgr.containers(src_ps_id, con_id);

		// 测不强制删除的情况，应该不会删除节点
		assertTrue(productStoreMgr.removeNodes(dest_ps_id, NodeType.Container,
				DBRowUtils.jsonObjectAsMap(new JSONObject().put(
						"container_type", 2)), false) == 0);

		// 测强制删除的情况，应该删除所有容器节点
		assertTrue(productStoreMgr.removeNodes(dest_ps_id, NodeType.Container,
				DBRowUtils.jsonObjectAsMap(new JSONObject().put(
						"container_type", 2)), true) == containers.size() - 1);

		// 删除完那些容器后，仅剩根容器节点（与产品之间有一条关系线）
		assertTrue(productStoreMgr.removeNodes(dest_ps_id, NodeType.Container,
				DBRowUtils.jsonObjectAsMap(new JSONObject().put(
						"container_type", 3)
						.put("container", "TLP:BULB/H1/25K")), true) == 1);
		// 此时产品节点应该成为了孤立节点
		assertTrue(productStoreMgr.removeNodes(dest_ps_id, NodeType.Product,
				DBRowUtils.jsonObjectAsMap(new JSONObject().put("pc_id",
						1000034)), false) == 1);
	}
}
