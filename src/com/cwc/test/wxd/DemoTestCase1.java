package com.cwc.test.wxd;

import static com.javaps.test.TestUtils.random;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import com.cwc.app.beans.log.FilterProductStoreLogBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorLogFilterIFace;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	private RedisMessageListenerContainer redisMessageListenerContainer;
	
	@Resource
	private MessageListenerAdapter redisMessageListenerAdapter;
	
	@Before
	public void setUp() throws Exception {
		final ChannelTopic eventTopic = new ChannelTopic("logserv:event");
		redisMessageListenerContainer.addMessageListener(
				redisMessageListenerAdapter, eventTopic);

	}
	
	@Resource
	private FloorLogMgrIFace logMgr;
	
	@Resource
	private FloorLogFilterIFace logFilter;
	
/*	@Test
	public void test_1() throws Exception {
		ProductStoreLogBean psLog = new ProductStoreLogBean();

		psLog.setOid(random(1000000, 100000));
		psLog.setCancel_psl_id(random(1000000, 100000));
		psLog.setAdid(random(10000, 1000));
		psLog.setBill_type(random(1, 50));
		psLog.setOperation(random(1, 50));
		psLog.setPc_id(random(100000, 10000));
		psLog.setPs_id(random(1000, 100));
		psLog.setQuantity(random(0, 2) > 0 ? 1 : -1);
		psLog.setAccount("hanlong");
		psLog.setTitle_id(random(10000, 1000));
		psLog.setProduct_line_id(random(10000, 1000));
		psLog.setQuantity_type(1);
		psLog.setLot_number(random(1000000, 100000) + "");
		psLog.setCatalogs(new long[] { random(10000, 1000) });

		String returnId = logMgr.availableProductStoreLog(psLog, false);
		assertTrue(returnId != null && returnId.length() > 0);
	}
	*/
	@Test
	public void test_2() throws Exception {
		FilterProductStoreLogBean psLog = new FilterProductStoreLogBean();
		int onGroup = 0;
		int in_or_out = 1;
		PageCtrl pc = new PageCtrl();
		pc.setPageSize(100);
		pc.setPageNo(1);
		String start_date = "2013-07-01";
		String end_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		DBRow[] result = logFilter.getSearchProductStoreSysLogs(start_date,end_date,psLog, onGroup, in_or_out, pc);
		assertTrue(result != null && result.length > 0);
	}
	
	

}
