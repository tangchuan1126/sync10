package com.cwc.test.demo;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.javaps.test.TestUtils;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	private FloorAdminMgr floorAdminMgr;
	
	@Resource
	private AdminMgrIFaceZJ proxyAdminMgrZJ;
	//private AdminMgrIFaceZJ adminMgrZJ;

	@Test
	public void test_1() throws Exception{
		System.out.println(proxyAdminMgrZJ.getClass().getName());
		System.out.println(TestUtils.dbRowAsString(proxyAdminMgrZJ.MachineLogin("admin", "")));
	}
	
	@Test
	public void test_2() throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(1);
		pc.setPageSize(100);
		DBRow[] admins = floorAdminMgr.getAllAdmin(pc);
		assertTrue(admins != null && admins.length>0);
		for(DBRow ad:admins){
			System.out.println(TestUtils.dbRowAsString(ad));
		}
	}

}
