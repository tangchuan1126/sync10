package com.cwc.finereport;

import com.fr.data.TableData;
import com.fr.data.impl.DBTableData;
import com.fr.report.ReportTemplate;
import com.fr.report.WorkSheet;
import com.fr.web.Reportlet;
import com.fr.web.ReportletException;
import com.fr.web.ReportletRequest;

public class GroupReportletDemo  implements Reportlet
{

	public ReportTemplate createReport(ReportletRequest arg0)
			throws ReportletException
	{
		try
		{
			// 新建报表        
			WorkSheet workSheet = new WorkSheet();  
			
			TableData tableData = new DBTableData(null, "SELECT top 20 * FROM ORDERS");  
			// 将定义的数据集添加到报表中，命名为ds1        
			workSheet.putTableData("ds1", tableData); 
			
		}
		catch(Exception e)
		{
			
		}
		
		return null;
	}

}
