package com.cwc.asynchronized;

import org.apache.log4j.Logger;

/**
 * 所有异步执行类需要集成的类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public abstract class ThreadAction implements Runnable
{
	static Logger log = Logger.getLogger("PLATFORM");
	
    public void run() 
    {
        try
		{
			perform();
		} 
        catch (Exception e)
		{
        	//打印出错堆栈信息
        	log.error("ThreadAction.run() error:"+e);
			StackTraceElement[] ste = e.getStackTrace();
			StringBuffer sb = new StringBuffer();
			sb.append(e.getMessage() + "\r\n");
			for (int i = 0;i < ste.length;i++)
			{
				sb.append(ste[i].toString() + "\r\n");
			}
			log.error(sb.toString());
		}
    }

    /**
     * 子类实现具体业务
     * @throws Exception
     */
    protected abstract void perform() throws Exception;
}
