package com.cwc.asynchronized;

import org.apache.log4j.Logger;

import EDU.oswego.cs.dl.util.concurrent.BoundedBuffer;
import EDU.oswego.cs.dl.util.concurrent.DirectExecutor;
import EDU.oswego.cs.dl.util.concurrent.PooledExecutor;
import EDU.oswego.cs.dl.util.concurrent.ThreadFactory;

/**
 * 提供异步线程执行
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ThreadManager
{
	static Logger log = Logger.getLogger("PLATFORM");
	
    private PooledExecutor backgroundExecutor;
    private DirectExecutor nodelayExecutor;
    private static ThreadManager threadManager = null;

    public static ThreadManager getInstance()
    {
    	if ( threadManager==null ) 
    	{
    		threadManager = new ThreadManager();
    		log.info("Load ThreadManager successful!");
    	}
    	
    	return(threadManager);
    }
    
    /**
     * 初始化异步线程池（系统启动执行）
     *
     */
    private ThreadManager()
    {
        backgroundExecutor = new PooledExecutor(new BoundedBuffer(5), 10);//50为最大线程数,10个任务缓冲区
        backgroundExecutor.setMinimumPoolSize(3);
        backgroundExecutor.setKeepAliveTime(1000 * 60 * 5);
        backgroundExecutor.waitWhenBlocked();
        backgroundExecutor.createThreads(5);

        backgroundExecutor.setThreadFactory(new ThreadFactory() {
            public Thread newThread(Runnable command)
            {
                Thread t = new Thread(command);
                t.setDaemon(false);
                t.setName("Background Execution Threads");
                t.setPriority(Thread.NORM_PRIORITY);

                return t;
            }
        });

        nodelayExecutor = new DirectExecutor();
    }

    /**
     * 异步执行
     * @param runnable
     */
    public void executeInBackground(Runnable runnable)
    {
        try 
		{
			backgroundExecutor.execute(runnable);
			
		}
        catch (InterruptedException e)
		{
        	log.error("ThreadManager.executeInBackground() error:"+e);
		}
    }

    /**
     * 同步执行
     * @param runnable
     */
    public void executeInForeground(Runnable runnable)
    {
        try
		{
			nodelayExecutor.execute(runnable);
		} 
        catch (InterruptedException e)
		{
        	log.error("ThreadManager.executeInForeground() error:"+e);
		}
    }

    /**
     * 释放资源
     *
     */
    public void shutdown()
    {
        backgroundExecutor.shutdownAfterProcessingCurrentlyQueuedTasks();
        log.info("Shutdown ThreadManager");
    }

    public void release()
    {
    }
}