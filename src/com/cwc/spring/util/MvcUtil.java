package com.cwc.spring.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.directwebremoting.json.types.JsonObject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.AccessLogBean;
import com.cwc.app.page.core.VelocityPageFather;
import com.cwc.app.page.core.VelocityTemplate;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class MvcUtil
{
	private static HashMap<String, String> notPermitOporateNames = new HashMap<String, String>();				//存放提交事件数据库中配置的名称
	
    /**
     * 通过ID从spring容器加载页面CLASS并输出模板内容
     * 相当于把请求分发的到模板
     * @param request
     * @param response
     * @param pageID
     * @throws Exception
     */
    public static void dispatch2Template(HttpServletRequest request,HttpServletResponse response,String pageID,ArrayList<DBRow> appendPara)
    	throws Exception
    {

			//从spring容器加载视图类(ID)，并输出模板内容
			VelocityPageFather viewPage = (VelocityPageFather) getBeanFromContainer(pageID);
			//假如action对输出视图设置了参数，则把参数加入模板
			if (appendPara != null && appendPara.size() > 0)
			{
				for (int i = 0; i < appendPara.size(); i++)
				{
					DBRow v = (DBRow) appendPara.get(i);
					viewPage.putPara(v.getString("name"), v.get("val",new Object()));
				}
			}
			//输出模板内容
			VelocityTemplate.getInstance().doGet(request, response,  viewPage);

    }

    /**
     * 把请求交回spring mvc，由他分发到jsp页面，可以追加参数
     * @param request
     * @param response
     * @param jspPage
     * @param appendPara
     * @return
     * @throws Exception
     */
    public static ModelAndView dispatch2Jsp(HttpServletRequest request,HttpServletResponse response,String jspPage,ArrayList<DBRow> appendPara)
    	throws Exception
    {
		//假如action对输出视图设置了参数，则把参数加入模板
		if (appendPara.size()>0)
		{
			for (int i=0; i<appendPara.size(); i++)
			{
				DBRow v = (DBRow)appendPara.get(i);
				request.setAttribute(v.getString("name"),v.get("val", new Object()));
			}
		}
    	
    	return(new ModelAndView(jspPage));
    }
    
    /**
     * 把没有操作权限完整方法类型解释为数据库中的描述
     * @param notPermitMsg
     * @return
     */
    public static String getNotPermitOporateName(String notPermitMsg)
    {
    	/**
    	 * 提取错误路径
    	 * 从数据库取得没有操作权限的方法说明 
    	 */
		try
		{
			//先检查内存有没有，没有就从数据库查询
			String opn = (String)notPermitOporateNames.get(notPermitMsg);
			if (opn==null)
			{
				AdminMgr adminMgr = (AdminMgr)getBeanFromContainer("adminMgr");
				DBRow detailAuthenticationAction = adminMgr.getDetailAuthenticationActionByAction(notPermitMsg);
				opn = detailAuthenticationAction.getString("description");
					
				String key = notPermitMsg;
				notPermitOporateNames.put(key, opn);
				notPermitMsg = opn;
			}
			else
			{
				notPermitMsg = opn;
			}
			
			return(notPermitMsg);
		}
		catch(Exception e)
		{
			return(notPermitMsg);
		}
    }
    
    /**
     * 根据ID，从SPRING容器获得bean
     * @param id
     * @return
     */
    public static Object getBeanFromContainer(String id)
    {
     	WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
     	return( ctx.getBean(id) );
    }

    /**
     * 获得spring 容器 ApplicationContext
     * @return
     */
    public static ApplicationContext  getApplicationContext()
    {
    	return(ContextLoader.getCurrentWebApplicationContext());
    }
    
    public static AccessLogBean  accessLogBean(String requestUri, HttpServletRequest req) {
    	
    	AccessLogBean alb = new AccessLogBean();

		// HTTP基本属性
		alb.setRequest_uri(requestUri);

		// TODO 规则根据需要可细化
		alb.setOperate_type(requestUri.matches("^/action/.*") ? 2 : 1);

		alb.setMethod(req.getMethod());
		alb.setQuery_string(req.getQueryString());
		alb.setUser_agent(req.getHeader("User-Agent"));
		alb.setContent_type(req.getContentType());
		alb.setLocale(req.getLocale().toString());
		alb.setSecure(req.isSecure());
		String forwardedFor = req.getHeader("X-Forwarded-For");
		alb.setRemote_addr( (forwardedFor != null && forwardedFor.length() > 0) ? forwardedFor
				: req.getRemoteAddr() );
		alb.setLocal_addr(req.getLocalAddr());
		alb.setLocal_port(req.getLocalPort());

		// 用户属性
		HttpSession ses = StringUtil.getSession(req);
		if (ses != null && ses.getAttribute(Config.adminSesion) != null) {
			AdminLoginBean loginBean = new AdminMgr().getAdminLoginBean(ses);
			alb.setAccount( loginBean.getAccount());
			alb.setAdid(loginBean.getAdid());
			alb.setAdgid(loginBean.getAdgid());
			alb.setEmploye_name( loginBean.getEmploye_name());
		}

		// 主体部分
		alb.setMultipart(ServletFileUpload.isMultipartContent(req));
		
		
		alb.setParameters(new ArrayList<Map<String, Object>>());

		SortedMap<String, Object> sm = new TreeMap<String, Object>(
				req.getParameterMap());
		for (Map.Entry<String, Object> e : sm.entrySet()) {
			String[] v = (String[]) e.getValue();
			if (v.length == 1) {
				e.setValue(v[0]);
			}
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("key", e.getKey());
			m.put("value", e.getValue());
			alb.getParameters().add(m);
		}
		
		return alb;

	}
  public static JSONObject  accessLog(String requestUri, HttpServletRequest req) throws Exception {
    	
	  JSONObject alb = new JSONObject();

		// HTTP基本属性
		alb.put("request_uri",requestUri);

		// TODO 规则根据需要可细化
		alb.put("operate_type", requestUri.matches("^/action/.*") ? 2 : 1);

		alb.put("method",req.getMethod());
		alb.put("query_string", req.getQueryString());
		alb.put("user_agent",req.getHeader("User-Agent"));
		alb.put("content_type", req.getContentType());
		alb.put("locale",req.getLocale().toString());
		alb.put("secure", req.isSecure());
		String forwardedFor = req.getHeader("X-Forwarded-For");
		alb.put("remote_addr", (forwardedFor != null && forwardedFor.length() > 0) ? forwardedFor
				: req.getRemoteAddr() );
		alb.put("local_addr", req.getLocalAddr());
		alb.put("local_port",req.getLocalPort());

		// 用户属性
		HttpSession ses = StringUtil.getSession(req);
		if (ses != null && ses.getAttribute(Config.adminSesion) != null) {
			AdminLoginBean loginBean = new AdminMgr().getAdminLoginBean(ses);
			alb.put("account", loginBean.getAccount());
			alb.put("adid",loginBean.getAdid());
			alb.put("department",DBRowUtils.dbRowArrayAsJSON(loginBean.getDepartment()));
			alb.put("employe_name",loginBean.getEmploye_name());
		}

		// 主体部分
		alb.put("multipart",ServletFileUpload.isMultipartContent(req));
		
		
		alb.put("parameters", new JSONObject());

		SortedMap<String, Object> sm = new TreeMap<String, Object>(
				req.getParameterMap());
		for (Map.Entry<String, Object> e : sm.entrySet()) {
			String[] v = (String[]) e.getValue();
			if (v.length == 1) {
				e.setValue(v[0]);
			}
			alb.getJSONObject("parameters").put(e.getKey(), e.getValue());
		}
		
		return alb;

	}
    
}



