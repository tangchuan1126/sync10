package com.cwc.action.core;

import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.MethodBeforeAdvice;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.Config;
import com.cwc.app.util.Environment;
import com.cwc.app.util.ThreadContext;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.authentication.AuthActionList;
import com.cwc.exactprivilege.ExactPrivilegeFactoryBean;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.exactprivilege.ExactPrivilegeIFace;

/**
 * 事件类前置鉴权拦截器
 * @author Administrator
 *
 */
public class AdminActionAuthenBeforeAdvice implements MethodBeforeAdvice 
{
	static Logger log = Logger.getLogger("ACTION");
	
	public void before(Method md, Object[] pob, Object rob)
		throws Throwable
	{
		//在数据库配置的需要认证的事件
		ArrayList<String> authActionList = AuthActionList.getAuthActionList();
		String methodName = rob.getClass().getName().toString().concat(".").concat(md.getName());

		//把当前执行事件设置到线程变量中
		ThreadContext.put("methodName",methodName);

		//先检查执行事件是否需要权限验证
		//鉴权失败后，由fathercontroller做权限跳转
		////system.out.println(methodName+" - "+authActionList.contains(methodName));
		if (authActionList.contains(methodName))
		{
			HttpServletRequest request = null;
			AdminLoginBean adminLoggerBean = null;
			for(int i = 0;i<pob.length;i++)
			{
				try 
				{
					request = (HttpServletRequest)pob[i];
					adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
					break;//成功获得request，不需再循环Object[]
				}
				catch (Exception e) 
				{
					//类型无法转换继续执行
				}
				
				try 
				{
					adminLoggerBean = (AdminLoginBean) pob[i];
				} 
				catch (Exception e) 
				{
					//转换AdminLoggerBean失败
				}
			}
			
			//HttpServletRequest request = (HttpServletRequest)pob[0];
			
			//如果session取不到 adminLoggerBean，则从request属性里面获得.(兼容盘点机模拟登录)
			
			//先做粗粒度权限验证
			//扔出的无权限异常将会交给ActionFatherController处理
			//if ( adminLoggerBean==null || !AdminAuthenCenter.isPermitAction(methodName,adminLoggerBean.getAdgid(),adminLoggerBean.getAdid()))
			if ( adminLoggerBean==null || !AdminAuthenCenter.pageAuthorityValidate(adminLoggerBean.getAdid(), adminLoggerBean.getDepartment(), methodName))	
			{
				throw new OperationNotPermitException(methodName);	
			}
			
			//细粒度权限验证，想法传Session
			ExactPrivilegeFactoryBean exactPrivilegeFactoryBean = (ExactPrivilegeFactoryBean)MvcUtil.getBeanFromContainer("exactPrivilegeFactoryBean");
			if ( exactPrivilegeFactoryBean.isNeed2Authen(methodName) )
			{
				ExactPrivilegeIFace	exactPrivilegeInstance = exactPrivilegeFactoryBean.getExactPrivilegeInstance(methodName);
				exactPrivilegeInstance.validate(methodName,pob);
			}
		}
	}

}

