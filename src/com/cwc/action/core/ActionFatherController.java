package com.cwc.action.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.ThreadContext;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.SendResponseServerCodeException;
import com.cwc.exception.SystemException;
import com.cwc.exception.WriteOutResponseException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 业务action需要继承的父类 这里实现了根据子类抛出的异常，进行跳转正常和认证失败跳转
 * 
 * @author Administrator
 *
 */
public abstract class ActionFatherController implements Controller {
	
	static Logger log = Logger.getLogger("PLATFORM");
	
	private static boolean haveInit = false; // 是否已经执行初始化
	
	protected ArrayList<DBRow> appendPara = new ArrayList<DBRow>(); // 传给模板的参数
	
	private String DEFAULT_NOT_PERMIT_PAGE = "not_permit"; // 默认没有操作权限JSP错误处理页面

	public String homePath = ConfigBean.getStringValue("systenFolder");

	/**
	 * 把参数加入模板
	 * 
	 * @param name
	 * @param val
	 */
	protected void putPara(String name, Object val) {
		
		DBRow v = new DBRow();
		v.add("name", name);
		v.add("val", val);
		appendPara.add(v);
	}

	public ActionFatherController() {
		
		try {
			
			if (!haveInit) {
				init();
				haveInit = true;
			}
		} catch (Exception e) {
			log.error("FatherController init() error:" + e);
		}
	}

	/**
	 * spring mvc调用入口
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 通过异常来控制跳转的方式
		try {
			
			perform(request, response);
			
		} catch (ForwardException e){ 
			
			// 模板分发，从异常信息中获得模板class对应的ID
			MvcUtil.dispatch2Template(request, response, e.getMessage(),appendPara);
			
		} catch (Forward2JspException e) {
			
			// jsp分发，从异常信息中获得jsp路径，注意不要跟扩展名，例如：product/error,error=error.jsp
			return (MvcUtil.dispatch2Jsp(request, response, e.getMessage(),appendPara));
			
		} catch (RedirectBackUrlException e) {
			
			// 重定向到backurl
			String backurl = StringUtil.deCodeBackURL(StringUtil.getString(request, "backurl"));
			
			if (backurl.equals("")) {
				backurl = ConfigBean.getStringValue("systenFolder");
			}
			
			response.sendRedirect(java.net.URLDecoder.decode(backurl, "utf-8"));
			
		} catch (RedirectException e) {
			
			// 任意重定向
			response.sendRedirect(java.net.URLDecoder.decode(e.getMessage(), "utf-8"));
			
		} catch (RedirectRefException e) {
			
			// 重定向到来路
			String ref = request.getHeader("referer");
			if (ref == null) {
				ref = ConfigBean.getStringValue("systenFolder");
			}
			
			response.sendRedirect(ref);
			
		} catch (OperationNotPermitException e) {
			
			// 粗粒度权限认证失败,没有操作权限
			this.putPara("operate", MvcUtil.getNotPermitOporateName(ThreadContext.get("methodName").toString()));
			return (MvcUtil.dispatch2Jsp(request, response, DEFAULT_NOT_PERMIT_PAGE, appendPara));
			
		} catch (WriteOutResponseException e){
			
			// 直接向浏览器输出字符
			this.writeOutResponse(response, e.getMessage());
		} catch (JsonException e) {
			
			// 直接向浏览器输出字符
			this.writeOutResponse(response, e.getMessage());
		} catch (SendResponseServerCodeException e) {
			
			// 直接向浏览器输出错误代码
			response.sendError(e.getServerErrorCode());
		} catch (SendResponseServerCodeAndMessageException e) {	
			
			// 直接向浏览器输出错误代码和原因
			response.sendError(e.getServerErrorCode(), e.getServerErrorMessage());
			
		} catch (PageNotFoundException e) {
			
			response.sendRedirect(ConfigBean.getStringValue("systenFolder").concat("404.html"));
		} catch (DoNothingException e) {
			
			
		} catch (Exception e) {
			
			StringBuffer sb = new StringBuffer("");
			
			@SuppressWarnings("rawtypes")
			Enumeration enu = request.getParameterNames();
			
			String key;
			
			while (enu.hasMoreElements()) {
				
				key = (String) enu.nextElement();
				sb.append(key);
				sb.append("=");
				sb.append(StringUtil.getString(request, key));
				sb.append("&");
			}

			// 如果是系统管理员操作，则记录下操作人
			AdminMgr adminMgr = (AdminMgr) MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			log.error("ActionFatherController.handleRequest:" + e);
			log.error("Referer:" + request.getHeader("referer"));
			log.error("Current URL:" + request.getRequestURI());
			log.error("Parameters:" + sb.toString());
			log.error("IP:" + request.getRemoteAddr());
			
			if (adminLoggerBean != null) {
				
				log.error("System operator:" + adminLoggerBean.getAccount());
				
			} else {
				
				log.error("System operator:NULL");
			}

			throw new SystemException(e, "ActionFatherController.handleRequest error:", log);
		}

		return (null);
	}
	
	/**
	 * 由子类实现的业务内容
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public abstract void perform(HttpServletRequest request, HttpServletResponse response) throws WriteOutResponseException, RedirectRefException, ForwardException, Forward2JspException, RedirectBackUrlException, RedirectException, OperationNotPermitException, PageNotFoundException, DoNothingException, Exception;
	
	/**
	 * 预留扩展
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception {}
	
	/**
	 * 直接输出内容流
	 * 
	 * @param response
	 * @param msg
	 * @throws IOException
	 */
	private void writeOutResponse(HttpServletResponse response, String msg) throws IOException {

		response.setContentType("text/html;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter pw = response.getWriter();
		pw.print(msg);
		pw.flush();
		pw.close();
	}
}
