package com.cwc.action;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.app.api.MemberMgr;
import com.cwc.factory.Factory;
import com.cwc.util.StringUtil;

public class MessageAlerter 
{
	private String sessionName = "com.cwc.action.MessageAlerter.msgAlert";
	MemberMgr memberMgr = null;
	Factory factory = Factory.getInstance();
	
	public MessageAlerter()
		throws Exception
	{
		memberMgr = (MemberMgr)factory.getInstanceNonProxy("com.cwc.app.api.MemberMgr");
	}
	
	public void setMessage(HttpServletRequest request,String msg)
	{
		getSession(request).setAttribute(sessionName,msg);
	}

	public void setMessage(HttpServletRequest request,String msg,String para[])
	{
		for (int i=1; i<=para.length; i++)
		{
			msg = StringUtil.replaceString(msg, "{"+(i)+"}", para[i-1]);
		}
		
		getSession(request).setAttribute(sessionName,msg);
	}
	
	public void clear(HttpServletRequest request)
	{
		getSession(request).removeAttribute(sessionName);
	}
	
	private HttpSession getSession(HttpServletRequest request)
	{
		return(request.getSession(true));
	}
	
	public void alert(HttpServletRequest request,HttpServletResponse response)
		throws IOException
	{	
		if ( getSession(request).getAttribute(sessionName)!=null )
		{
			PrintWriter out = response.getWriter();
			//out.println("<script>");
			out.println("alert('"+getSession(request).getAttribute(sessionName)+"')");
			//out.println("</script>");
			clear(request);
			out.flush();
		}
	}

	public String alertM(HttpServletRequest request,HttpServletResponse response)
		throws IOException
	{
		String t = (String)getSession(request).getAttribute(sessionName);
		clear(request);
		
		if (t==null)
		{
			return("");
		}
		else
		{
			return("alert('"+t+"')");			
		}	
	}


	public static void main(String args[])
		throws Exception
	{
		/**
		HashMap<String, String> bl = new HashMap<String, String>();
		
		bl.put("a", "A");
		bl.put("b", "B");
		bl.put("c", "C");
		bl.put("d", "D");
		bl.put("e", "E");
		bl.put("f", "F");
		bl.put("g", "G");
		bl.put("h", "H");
		bl.put("i", "I");
		bl.put("j", "J");
		bl.put("k", "K");
		bl.put("l", "L");
		bl.put("m", "M");
		bl.put("n", "N");
		bl.put("o", "O");
		bl.put("p", "P");
		bl.put("q", "Q");
		bl.put("r", "R");
		bl.put("s", "S");
		bl.put("t", "T");
		bl.put("u", "U");
		bl.put("v", "V");
		bl.put("w", "W");
		bl.put("x", "X");
		bl.put("y", "Y");
		bl.put("z", "Z");
		
		String root = "E:/tomcat5.5.26/webapps/turboshop400/administrator/";
		String folder = "vote";
		String filePath = root+folder;

		String files [] = FileUtil.getFileFromFolder(filePath);
		
		String filename;
		
		StringBuffer aca = new StringBuffer("");
		StringBuffer acm = new StringBuffer("");
		StringBuffer ur = new StringBuffer("");
		
		for (int i=0; i<files.length; i++)
		{
			filename = files[i];
			
			if (filename.indexOf(".jsp")>=0)
			{
				String a1 = filename.substring(0,filename.lastIndexOf(".jsp"));
				
				////system.out.println(a1);
				
				int len = a1.length();
				StringBuffer lsb = new StringBuffer("");
				
				int st = 0;
				String c;
				boolean bigFlag = false;
				
				for (int ii=0; ii<len; ii++)
				{
					c = a1.substring(st++, ii+1);
					
					if (c.equals("_"))
					{
						bigFlag = true;
						continue;
					}
						
					if (ii==0)
					{
						lsb.append(bl.get(c));
					}
					else if (bigFlag)
					{
						lsb.append(bl.get(c));
						bigFlag = false;
					}
					else
					{
						lsb.append(c);
					}
				}

				aca.append("<bean id=\""+folder+lsb.toString()+"Page\" class=\"com.cwc.app.page.core.CommonJspPage\" lazy-init=\"true\">\n");
				aca.append("\t<property name=\"jspPage\" value=\"administrator/"+folder+"/"+filename+"\" />\n");
				aca.append("</bean>\n\n");
				
				acm.append("<prop key=\"/page/administrator/"+folder+"/"+lsb.toString()+"Page.action\">"+folder+lsb.toString()+"Page</prop>\n\n");
				
				ur.append("<rule>\n");
				ur.append("<from>^/administrator/"+folder+"/"+a1+".html(.*)</from>\n");
				ur.append("<to>/page/administrator/"+folder+"/"+lsb.toString()+"Page.action?nocache=1&amp;$1</to>\n");
				ur.append("</rule>\n\n");
			}
		}
		
		aca.append("-------------------------------\n\n");
		acm.append("-------------------------------\n\n");
		
		FileUtil.createFile("e:/"+folder+".txt", aca.toString()+acm.toString()+ur.toString());
		
		**/
	}
}



