package com.cwc.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.cwc.exception.ConnectErrorJmsException;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

/**
 * JMS 消息监听器
 * 扔出异常，消息不确认
 * @author Administrator
 *
 */
public class RemoteMessageListener implements SessionAwareMessageListener  
{
	static Logger log = Logger.getLogger("JMS");
	
	public void onMessage(Message message,Session session) 
		throws JMSException
	{
		String msgId = null;
		try
		{
			//log.info("["+new Date()+"] RemoteMessageListener ");
			
			//手工删除pending消息后，会造成内存数据残留，待解决(重启)
			if (message instanceof ObjectMessage &&  ((ObjectMessage)message).getObject() instanceof ActionObjMessage)
			{
//				msgId = String.valueOf(message.getJMSTimestamp());
				
				//ObjectMessage objMsg = (ObjectMessage)message;
				ActionObjMessage actionObjMessage = (ActionObjMessage)((ObjectMessage)message).getObject();
//				MessageDelayExeList.need2Exe(msgId);
				try 
				{
					actionObjMessage.run();
				}
				catch (ConnectErrorJmsException e)
				{
					com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
					
					long old_message_time =  message.getJMSTimestamp();
					
					long delay_time = (System.currentTimeMillis()-old_message_time+1000)*4;
					
					jmsMgr.DelayExcuteAction(actionObjMessage, delay_time);
				}
			}
			else
			{
//				ObjectMessage objMsg = (ObjectMessage)message;
//				ActionObjMessage actionObjMessage = (ActionObjMessage)objMsg.getObject();
//				MessageDelayExeList.need2Exe(msgId);
				
				log.error("ActionObjMessage:"+message);
			}
		}
		catch (JMSException e) //objMsg.getObject() 异常，直接重来，不进入队列
		{
			throw e;
		}
		catch(SystemException e)
		{
			e.printStackTrace();
			log.error(e);
		} 
	}
}




