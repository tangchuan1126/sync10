package com.cwc.jms;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.jms.support.converter.SimpleMessageConverter;

/**
 * 使用多线程接受消息
 */
public class TopicConsumer {

	ExecutorService exec = Executors.newFixedThreadPool(10);
	SimpleMessageConverter simpleMessageConverter;

	public void receive(final ActionObjMessage message) {
		//system.out.println(Thread.currentThread().getName());
		
		exec.submit((new Runnable() {
			@Override
			public void run() {
				//system.out.println(Thread.currentThread().getName());
				//system.out.println("************************************** Topic A : ");
			}
		}));
	}

}
