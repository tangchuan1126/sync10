package com.cwc.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.cwc.app.jms.action.SendMail;
import com.cwc.exception.SystemException;

public class TestJmsMgr
{
	static Logger log = Logger.getLogger("ACTION");
	
	private JmsTemplate jmsTemplate;
	private Destination destination;


	/**
	 * 通过JMS发送执行对象
	 * @param actionObjMessage
	 * @throws SystemException 
	 * @throws SystemException 
	 */
//	public void excuteAction( final ActionObjMessage actionObjMessage)
//	{
//		Connection connection = null;
//		Session session = null;
//		MessageProducer producer = null;
//		
//        try 
//        {
//        	connection = this.activeMQconnectionFactory.createConnection();
//        	connection.start();
//        	
//        	session = connection.createSession(false,Session.CLIENT_ACKNOWLEDGE);  
//        	producer = session.createProducer(destination);
//        	
//        	ObjectMessage message = session.createObjectMessage();
//        	message.setObject( actionObjMessage );
//        	producer.send(message);
//        }
//        catch (JmsException e) 
//        {
//			try 
//			{
//				throw new SystemException(e,"excuteAction JmsException",log);
//			}
//			catch (SystemException e1)
//			{
//			}
//        }
//        catch (JMSException e) 
//        {
//			try 
//			{
//				throw new SystemException(e,"excuteAction JMSException",log);
//			}
//			catch (SystemException e1)
//			{
//			}
//		}
//        finally
//        {
//        	try
//        	{
//				producer.close();
//			}
//        	catch (JMSException e2)
//        	{
//			}
//        	
//        	try 
//        	{
//				session.close();
//			}
//        	catch (JMSException e1)
//        	{
//			}
//        	
//        	try 
//        	{
//				connection.close();
//			} 
//        	catch (JMSException e)
//        	{
//			}
//        }
//	}

	

	public void excuteAction( final ActionObjMessage actionObjMessage)
	{
		
        try 
        {
        	jmsTemplate.send(destination, 
    				new MessageCreator() 
    				{
    		        	public Message createMessage(Session session) 
    		        	{
    		                try 
    		                {
    		                	ObjectMessage message = session.createObjectMessage();
    		                	message.setObject( actionObjMessage );
    							return(message);
    						} 
    		                catch (JMSException e) 
    		                {
    		                	try 
    		                	{
									throw new SystemException(e,"createMessage",log);
								} 
    		                	catch (SystemException e1) 
    		                	{
									return(null);
								}
    						}
    		        	}
    				}
        	); 
        }
        catch (JmsException e) 
        {
			try 
			{
				throw new SystemException(e,"excuteAction",log);
			}
			catch (SystemException e1)
			{
			}
        }
	}
	
	
//	private void receiver() 
//	{
//		try
//		{
//			//system.out.println("receiver:");
//			
//			Connection connection = this.activeMQconnectionFactory.createConnection();
//			connection.start();
//			
//			Session consumerSession = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
//			MessageConsumer consumer = consumerSession.createConsumer(destination);
//			
//			Message message = consumer.receive();
//			if (message instanceof ObjectMessage)
//			{
//				//system.out.println("run before():");
//				ObjectMessage objMsg = (ObjectMessage)message;
//				ActionObjMessage actionObjMessage = (ActionObjMessage)objMsg.getObject();
//				actionObjMessage.run();
//				//system.out.println("run after():");
//				//system.out.println("---------------------");
//				//message.acknowledge(); 
//			}
//			consumer.close();
//			consumerSession.close();
//			connection.close();
//		}
//		catch (JMSException e)
//		{
//			e.printStackTrace();
//		}
//	}


//	public void sender()
//		throws JMSException
//	{
//		template.send(destination, 
//			new MessageCreator() 
//			{
//            	public Message createMessage(Session session) 
//            	{
//                    try 
//                    {
//						return session.createTextMessage("Hello ActiveMQ("+System.currentTimeMillis()+") Text Message!");
//					} 
//                    catch (JMSException e) 
//                    {
//						e.printStackTrace();
//						return (null);
//					}
//            	}
//			}
//		); 
//	}

//	public void senderMap()
//		throws JMSException
//	{
//		template.send(destination, 
//			new MessageCreator() 
//			{
//	        	public Message createMessage(Session session) 
//	        	{
//	                try 
//	                {
//	                	HashMap hm = new HashMap();
//	                	hm.put("a", 1);
//	                	hm.put("b", 2);
//	                	
//	                	MapMessage message = session.createMapMessage();
//	                	message.setLong("time", System.currentTimeMillis());
//	                	message.setObject("data", hm);
//	                	
//						return(message);
//					} 
//	                catch (JMSException e) 
//	                {
//						e.printStackTrace();
//						return (null);
//					}
//	        	}
//			}
//		); 
//	}

 
	public void test()
	{
		//system.out.println("sendmail!!");
	}
	
	public void testAsyn() 
	{
//		this.excuteAction(
//			new SendMail()
//		);
		
		
		//this.receiver();
		
//		this.excuteAction(
//			new ActionObjMessage()
//			{
//				public void perform() throws Exception {
//					
//				}
//				
//			}
//		);
		
		
	} 
	
	public void setJmsTemplate(JmsTemplate jmsTemplate) 
	{
		this.jmsTemplate = jmsTemplate;
	}
	
	public void setDestination(Destination destination) 
	{
		this.destination = destination;
	}
}
