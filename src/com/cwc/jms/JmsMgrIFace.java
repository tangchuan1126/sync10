package com.cwc.jms;

import javax.jms.Destination;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;


public interface JmsMgrIFace 
{
	public void excuteAction( final ActionObjMessage actionObjMessage);
	
	public void excuteGetSellerAction(ActionObjMessage actionObjMessage);
	
	public void DelayExcuteAction(final ActionObjMessage actionObjMessage,long delay_time);
	
	public void excuteAction(JmsTemplate temple, Destination dest,final ActionObjMessage actionObjMessage ) throws JmsException;
	
}
