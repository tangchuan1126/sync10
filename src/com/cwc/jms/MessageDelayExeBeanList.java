package com.cwc.jms;

import java.io.Serializable;
import java.util.Hashtable;

public class MessageDelayExeBeanList implements Serializable
{
	private static Hashtable<String, MessageDelayExeBean> list = new Hashtable<String, MessageDelayExeBean>();

	public static void add(String key, MessageDelayExeBean messageDelayExeBean) 
	{
		MessageDelayExeBeanList.list.put(key, messageDelayExeBean);
	}
	
	public static int getSize() 
	{
		return( MessageDelayExeBeanList.list.size() );
	} 
	
	public static void remove(String key) 
	{
		MessageDelayExeBeanList.list.remove(key);
	}
	
	public static boolean containsKey(String key) 
	{
		return( MessageDelayExeBeanList.list.containsKey(key) );
	}
	
	public static MessageDelayExeBean get(String key)
	{
		return( MessageDelayExeBeanList.list.get(key) );
	} 
	
}
