package com.cwc.jms;

/**
 * 彻底放弃执行异常消息
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class GiveUpExeException extends Exception 
{
	public GiveUpExeException() 
	{
		super();
	}
	
	public GiveUpExeException(String inMessage)
	{
		super(inMessage);
	}
}
