package com.cwc.jms;

import java.io.Serializable;

/**
 * 存放执行异常的消息(暂时不在使用，用DBRow代替)
 * 决定下一次的执行时间和过期时间
 * @author Administrator
 *
 */
public class MessageDelayExeBean implements Serializable,Cloneable
{
	/**
	 * 获得消息ID
	 * @return
	 */
	public String getMQA_ID() 
	{
		return MQA_ID;
	}
	
	/**
	 * 设置消息ID
	 * @param mqa_id
	 */
	public void setMQA_ID(String mqa_id)
	{
		MQA_ID = mqa_id;
	}
	
	/**
	 * 得到当前执行时间
	 * @return
	 */
	public long getCP_TIME() 
	{
		return CP_TIME;
	}
	
	/**
	 * 设置当前执行时间
	 * @param cp_time
	 */
	public void setCP_TIME(long cp_time)
	{
		CP_TIME = cp_time;
	}
	
	/**
	 * 得到当前执行时间间隔
	 * @return
	 */
	public long getCP_STEP()
	{
		return CP_STEP;
	}

	/**
	 * 设置当前执行时间间隔
	 * @param cp_step
	 */
	public void setCP_STEP(long cp_step) 
	{
		CP_STEP = cp_step;
	}
	
	
	public int getST_SE()
	{
		return ST_SE;
	}

	public void setST_SE(int st_se)
	{
		ST_SE = st_se;
	}

	public int getSTEP_B() 
	{
		return STEP_B;
	}

	public void setSTEP_B(int step_b)
	{
		STEP_B = step_b;
	}

	public long getTIME_OUT() 
	{
		return TIME_OUT;
	}

	public void setTIME_OUT(long time_out)
	{
		TIME_OUT = time_out;
	}
	
	/**
	 * 获得第一次被执行的时间
	 * @return
	 */
	public long getFIRST_EXE_TIME() 
	{
		return FIRST_EXE_TIME;
	}

	/**
	 * 设置第一次被执行的时间
	 * @param first_exe_time
	 */
	public void setFIRST_EXE_TIME(long first_exe_time) 
	{
		FIRST_EXE_TIME = first_exe_time;
	}
	
	
	private String MQA_ID;//消息ID
	private long CP_TIME = 0;//当前执行时间（long型）
	private long CP_STEP = 0;//当前执行时间间隔（豪秒）
	private long FIRST_EXE_TIME = 0;  //第一次被执行的时间
	
	private int ST_SE;					//第一次异常之后，隔多少秒再执行
	private int STEP_B;						//步长倍数，两个执行时间之间的倍数
	private long TIME_OUT;			//消息执行异常，保留执行的时间

}
