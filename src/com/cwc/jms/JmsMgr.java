package com.cwc.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ScheduledMessage;
import org.apache.log4j.Logger;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.cwc.app.util.DateUtil;
import com.cwc.exception.SystemException;

public class JmsMgr implements JmsMgrIFace
{
	static Logger log = Logger.getLogger("JMS");
	static Logger sellerLog = Logger.getLogger("ebay_seller");
	
	private JmsTemplate jmsTemplate;
	private Destination destination;

	/**
	 * 通过JMS执行对象
	 * @param actionObjMessage
	 */
	public void excuteAction( final ActionObjMessage actionObjMessage )
	{
		try
		{
			this.excuteAction(jmsTemplate, destination, actionObjMessage);
	    }
	    catch (JmsException e) 
	    {
			try 
			{
				throw new SystemException(e,"jms.excuteAction",log);
			}
			catch (SystemException e1)
			{
				e1.printStackTrace();
				log.error(e1);
			}
	    }
	}
	
	public void excuteGetSellerAction(ActionObjMessage actionObjMessage )
	{
		try
		{
			sellerLog.error("excuteGetSellerAction message:"+actionObjMessage.getClass().getName());
			sellerLog.error("excuteGetSellerAction message Start:"+DateUtil.NowStr());
			long start = System.currentTimeMillis();
			this.excuteSellerAction(jmsTemplate, destination, actionObjMessage);
			long end = System.currentTimeMillis();
			sellerLog.error("excuteGetSellerAction message End:"+DateUtil.NowStr()+" useTime:"+(end-start));
	    }
	    catch (JmsException e) 
	    {
			try 
			{
				throw new SystemException(e,"jms.excuteAction",log);
			}
			catch (SystemException e1)
			{
				e1.printStackTrace();
				log.error(e1);
			}
	    }
	}
	
	/**
	 * 通过JMS发送消息
	 * @param temple
	 * @param dest
	 * @param actionObjMessage
	 */
	public void excuteAction(JmsTemplate temple, Destination dest,final ActionObjMessage actionObjMessage )
		throws JmsException
	{
        	temple.send(dest, 
    				new MessageCreator() 
    				{
    		        	public Message createMessage(Session session) 
    		        	{
    		                try 
    		                {
    		                	ObjectMessage message = session.createObjectMessage();
    		                	message.setObject( actionObjMessage );
    		                	
    							return(message);
    						} 
    		                catch (JMSException e) 
    		                {
    		                	try 
    		                	{
									throw new SystemException(e,"createMessage",log);
								} 
    		                	catch (SystemException e1) 
    		                	{
    		                		//system.out.println("createMessage error!!!");
    		                		log.error(e);
									return(null);
								}
    						}
    		        	}
    				}
        	); 

	}
	
	public void excuteSellerAction(JmsTemplate temple, Destination dest,final ActionObjMessage actionObjMessage )
		throws JmsException
	{
		sellerLog.error("excuteSellerAction send start:"+DateUtil.NowStr());
		long start = System.currentTimeMillis();
	    	temple.send(dest, 
					new MessageCreator() 
					{
			        	public Message createMessage(Session session) 
			        	{
			                try 
			                {
			                	ObjectMessage message = session.createObjectMessage();
			                	message.setObject( actionObjMessage );
			                	
								return(message);
							} 
			                catch (JMSException e) 
			                {
			                	try 
			                	{
									throw new SystemException(e,"createMessage",log);
								} 
			                	catch (SystemException e1) 
			                	{
			                		//system.out.println("createMessage error!!!");
			                		log.error(e);
									return(null);
								}
							}
			        	}
					}
	    	); 
	    	long end = System.currentTimeMillis();
	    	sellerLog.error("excuteSellerAction send end:"+DateUtil.NowStr()+" useTime:"+(end-start));
	
	}
	

	
	
	
	public void setJmsTemplate(JmsTemplate jmsTemplate) 
	{
		this.jmsTemplate = jmsTemplate;
	}
	
	public void setDestination(Destination destination) 
	{
		this.destination = destination;
	}

	@Override
	public void DelayExcuteAction(ActionObjMessage actionObjMessage,long delay_time) 
	{
		this.DelayExcuteAction(jmsTemplate,destination,actionObjMessage,delay_time);
	}
	
	public void DelayExcuteAction(JmsTemplate temple, Destination dest,final ActionObjMessage actionObjMessage,final long delay_time)
		throws JmsException
	{
			long oneday = 24*60*60*1000;
			if(delay_time>oneday)
			{
				log.error(actionObjMessage.getClass().getName());
			}
			else
			{
				temple.send(dest, 
						new MessageCreator() 
						{
				        	public Message createMessage(Session session) 
				        	{
				                try 
				                {
				                	ObjectMessage message = session.createObjectMessage();
				                	message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY,delay_time);
				                	message.setObject(actionObjMessage);
									return(message);
								} 
				                catch (JMSException e) 
				                {
				                	try 
				                	{
										throw new SystemException(e,"createMessage",log);
									} 
				                	catch (SystemException e1) 
				                	{
										return(null);
									}
								}
				        	}
						}
		    	); 
			}
	}
}
