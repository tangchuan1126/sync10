package com.cwc.jms;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import com.cwc.exception.ConnectErrorJmsException;
import com.cwc.exception.SystemException;

/**
 * 存放异常执行消息
 * 
 * 异常消息执行有三种状态：1、执行 2、不执行 3、放弃
 * 
 * @author Administrator
 *
 */
public class MessageDelayExeList
{
	private static boolean hasMessage(String msgId)
	{
		return( MessageDelayExeBeanList.containsKey(msgId) );
	}
	
	public static void need2Exe(String msgId)
		throws ConnectErrorJmsException,GiveUpExeException
	{
		////system.out.println("["+new Date()+"] need2Exe SIZE:"+MessageDelayExeBeanList.getSize());
		if ( MessageDelayExeList.hasMessage(msgId) )   //不是异常消息,直接执行
		{
			//检查消息是否达到执行的时间
			MessageDelayExeBean mdeb = MessageDelayExeBeanList.get(msgId);
			long curTime = System.currentTimeMillis();//当前时间
			long nextExeStep = mdeb.getCP_STEP()*mdeb.getSTEP_B();//下次执行时间间隔
			long time_out = mdeb.getFIRST_EXE_TIME() + mdeb.getTIME_OUT();//最长生命周期
			
			//如果已经超时，则彻底放弃
			////system.out.println(nextExeStep+" - "+ time_out +" - "+curTime + "["+(time_out<curTime)+"]" );
			if ( time_out<curTime )
			{
				////system.out.println("throw new GiveUpExeException");
				throw new GiveUpExeException();
			}
			else
			{
				//达到需要下一次执行的时间
				if ( mdeb.getCP_TIME()+nextExeStep<=curTime )
				{
					//执行前，需要更新当前时间参数
					mdeb.setCP_STEP(nextExeStep);
					mdeb.setCP_TIME(curTime);
				}
				else
				{
					throw new ConnectErrorJmsException();//不执行任务，但是不确认消息，保持循环
				}
			}

		}
	}
	
	/**
	 * 加入异常执行队列，消息第一执行异常被加入
	 * @param message
	 */
	public static void add(Message message)
	{
		try 
		{
				if ( message!=null&&!MessageDelayExeList.hasMessage(String.valueOf(message.getJMSTimestamp())) )
				{
					ObjectMessage objMsg = (ObjectMessage)message;
					
					//该初始化只会被执行一次
					////system.out.println("add("+String.valueOf(message.getJMSTimestamp())+")");
					long cur_time = System.currentTimeMillis();
					ActionObjMessage actionObjMessage = (ActionObjMessage)objMsg.getObject();

					MessageDelayExeBean messageDelayExeBean = new MessageDelayExeBean();
					messageDelayExeBean.setCP_STEP(actionObjMessage.getST_SE()/actionObjMessage.getSTEP_B());//下一次执行时间间隔(除了后，确保第一次执行时间间隔跟设置的一样)
					messageDelayExeBean.setCP_TIME(cur_time);//当前执行时间
					messageDelayExeBean.setFIRST_EXE_TIME(cur_time);//第一次执行时间
					messageDelayExeBean.setMQA_ID(String.valueOf(message.getJMSTimestamp()));
					messageDelayExeBean.setST_SE(actionObjMessage.getST_SE());//下一次执行时间
					messageDelayExeBean.setSTEP_B(actionObjMessage.getSTEP_B());//执行时间间隔倍数
					messageDelayExeBean.setTIME_OUT(actionObjMessage.getTIME_OUT());//最长生命周期
					
					MessageDelayExeBeanList.add(String.valueOf(message.getJMSTimestamp()), messageDelayExeBean);
				}				
		} 
		catch (JMSException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void remove(String msgId)
	{
		MessageDelayExeBeanList.remove(msgId);
	}
	
	public static void main(String args[])
	{
		int a = 30/4;
		//system.out.println( a );
	}
}




