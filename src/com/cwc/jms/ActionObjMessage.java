package com.cwc.jms;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.cwc.exception.ConnectErrorJmsException;
import com.cwc.exception.SystemException;

/**
 * 所有使用JMS服务的类，必须继承
 * @author Administrator
 *
 */
public abstract class ActionObjMessage implements Serializable
{
	static Logger log = Logger.getLogger("JMS");
	private static final long serialVersionUID = 1L;
	
	private int ST_SE = 30000;					//第一次异常之后，隔多少秒再执行
	private int STEP_B = 2;						//步长倍数，两个执行时间之间的倍数
	private long TIME_OUT = 24*60*60*1000;			//消息执行异常，保留执行的时间
	
	public final void run()
		throws ConnectErrorJmsException,SystemException
	{
		try
		{
			this.perform();
		} 
		catch (Exception e)
		{
			

			if(e.toString().contains("java.net.")||e.toString().contains("timeout"))
			{
				throw new ConnectErrorJmsException(e);
			}
			else
			{
				log.error(e.toString());
				throw new SystemException(e,"ActionObjMessage.run()",log);
			}
//			throw new SystemException(e,"ActionObjMessage.run() ",log);
		}
	}
	
	public abstract void perform() throws Exception;//子类必须实现

	/**
	 * 获得第一次异常之后，隔多少秒再执行
	 * @return
	 */
	public int getST_SE() 
	{
		return ST_SE;
	}

	/**
	 * 设置第一次异常之后，隔多少秒再执行(毫秒)
	 * 可以被继承的子类重置默认数据
	 * @param st_se
	 */
	public void setST_SE(int st_se) 
	{
		ST_SE = st_se;
	}

	/**
	 * 获得步长倍数
	 * @return
	 */
	public int getSTEP_B()
	{
		return STEP_B;
	}

	/**
	 * 设置步长倍数
	 * 可以被继承的子类重置默认数据
	 * @param step_b
	 */
	public void setSTEP_B(int step_b)
	{
		STEP_B = step_b;
	}
	
	/**
	 * 获得消息执行异常，保留执行的时间
	 * @return
	 */
	public long getTIME_OUT() 
	{
		return TIME_OUT;
	}

	/**
	 * 设置消息执行异常，保留执行的时间(毫秒)
	 * 可以被继承的子类重置默认数据
	 * @param time_out
	 */
	public void setTIME_OUT(long time_out) 
	{
		TIME_OUT = time_out;
	}
	
}

