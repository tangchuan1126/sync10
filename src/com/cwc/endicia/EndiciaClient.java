package com.cwc.endicia;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.*;
import java.net.SocketTimeoutException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.soap.encoding.soapenc.Base64;

import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.exception.SystemException;
import com.cwc.http.Client;
import com.cwc.usps.UspsResponseErrorException;
import com.sun.media.jai.codec.*;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class EndiciaClient  
{
	//
	static Logger log = Logger.getLogger("ACTION");

	private String RequesterID = "lvis";
	private String AccountID = "770373";
	private String PassPhrase = "Vvme91754";
	
	private String imgPath = Environment.getHome()+"administrator/order/UspsPrintLabel";
	private String server = "https://labelServer.Endicia.com/LabelService/EwsLabelService.asmx/GetPostageLabelXML";
	private String track_server = "https://www.endicia.com/ELS/ELSServices.cfc?wsdl";
	private String waybillNo;
	
	//收发件人信息
	private String fromName;
	private String fromAddress2;
	private String fromAddress1;
	private String fromCity;
	private String fromState;
	private String fromZip5;
	private String fromPhone;

	private String toName;
	private String toAddress2;
	private String toCity;
	private String toState;
	private String toZip5;
	private String weightOunces;
	private String toPhone;
	
	private String invoiceMailPieceShape;
	private double finalPostage;	//花费邮资

	private long oid;
	
	int timeout = 6000; //连接服务器超时时间
	
	public static float SLIM_SET_WEIGHT = 0.67f + 0.27f;//(一套超薄+包装盒，单位千克)
	public static float MIN_WEIGHT = 0.3686f;		//13安士=0.3686 kg

	public String getWaybillNo()
	{
		return waybillNo;
	}
	
	/**
	 * 生成Usps打印Label图片
	 * @param base64Str
	 * @param name
	 * @throws Exception
	 */
	public void createPrintLabelBase64Img(String base64Str,String name)
		throws Exception
	{
		try
		{
			//BASE64Decoder decoder = new BASE64Decoder();   
			byte[] buffer = Base64.decode(base64Str);	
			//log.info(base64Str);
			// Base64解码   
			//byte[] buffer = decoder.decodeBuffer(base64Str);   
//			for (int i = 0; i < buffer.length; ++i) 
//			{   
//				if (buffer[i] < 0)  // 调整异常数据   
//				{
//					buffer[i] += 256;   
//				}
//			}  
			
			File file = new File(imgPath+"/"+name+".png");
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
			bos.write(buffer);
			bos.flush();
			bos.close();
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"createPrintLabelBase64Img",log);
		}
	}

	/**
	 * 把TIF图片转换成PNG
	 * @param tif
	 * @param png
	 * @throws Exception
	 */
	public void convert2PNG(String tif,String png)
		throws Exception
	{
		try
		{
			FileOutputStream out = new FileOutputStream(png);
			File file = new File(tif);
			ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",file,null);
			ImageEncoder encoder = ImageCodec.createImageEncoder("png",out,null);
			encoder.encode( decoder.decodeAsRenderedImage() );
			decoder.getInputStream().close();//不关闭，源文件删不掉
			encoder.getOutputStream().close();
			file.delete();
			out.close();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"convert2PNG",log);
		}
	}
	
	/**
	 * 剪裁图片
	 * @param src
	 * @param out
	 * @param sx
	 * @param sy
	 * @param width
	 * @param height
	 * @throws Exception
	 */
	public void cropImage(String src,String out,int   sx,   int   sy,   int   width,   int   height)
		throws Exception
    {
		try
		{
	        //1.获得原始图象Image对象
	        File   file=new   File(src);
	        BufferedImage baseImage   =ImageIO.read(file);
	        
            ImageFilter   filter   =   new   CropImageFilter(sx,   sy,   width,   height);//根据图像裁剪过滤器产生过滤器
            //下面根据过滤器产生图像生产者
            ImageProducer   producer   =   new   FilteredImageSource(baseImage.getSource(),   filter);
            Image img   =   Toolkit.getDefaultToolkit().createImage(producer);//根据图像生产者产生新图像
            
            //3.生成图片文件
            BufferedImage tag   =   new   BufferedImage(width,   height,   BufferedImage.TYPE_INT_RGB);
            tag.getGraphics().drawImage(img,   0,   0,   width,   height,   null);
            
            ImageIO.write(tag, "png", new File(out));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"cropImage",log);
		}   
    } 
	
	/**
	 * 缩放图片
	 * @param srcImageFile
	 * @param result
	 * @param out_width
	 * @throws Exception
	 */
	 public void scale(String srcImageFile, String result, int out_width) 
	 	throws Exception
	 {
		  try
		  {
			   BufferedImage src = ImageIO.read(new File(srcImageFile)); // 读入文件
			   float width = src.getWidth(); // 得到源图宽
			   float height = src.getHeight(); // 得到源图长
			   
			   int out_height = (int)((out_width/width)*height);
			   
			   Image image = src.getScaledInstance(out_width, out_height, Image.SCALE_DEFAULT);
		
			   BufferedImage tag = new BufferedImage(out_width, out_height,BufferedImage.TYPE_INT_RGB);
			   Graphics g = tag.getGraphics();
			   g.drawImage(image, 0, 0, null); // 绘制缩小后的图
			   g.dispose();
			   ImageIO.write(tag, "png", new File(result));// 输出到文件流
		  }
		  catch (IOException e)
		  {
			  throw new SystemException(e,"scale",log);
		  }
	 }
	
	
		
		/**
		 * 提交数据给USPS并生成打印LABEL
		 * @throws Exception
		 */
		public void commit(String test,String mail_piece_shape)
			throws SocketTimeoutException,UspsResponseErrorException,Exception
		{
			try
			{
//				String mailpieceShape = this.getMailpieceShape(StrUtil.getFloat(this.weightOunces), this.fromZip5, this.toZip5 );
//				//system.out.println(mailpieceShape);
				this.invoiceMailPieceShape = mail_piece_shape;//返回给运单用
				
				String para = "labelRequestXML=<LabelRequest ImageFormat='GIF' ImageResolution='203' Test='"+test+"'>";
				para += "<RequesterID>"+RequesterID+"</RequesterID>";
				para += "<AccountID>"+AccountID+"</AccountID>";
				para += "<PassPhrase>"+PassPhrase+"</PassPhrase>";
				para += "<DateAdvance>0</DateAdvance>";
				para += "<Stealth>true</Stealth>";//打印邮资
				para += "<Services InsuredMail='OFF' SignatureConfirmation='OFF' />";
				para += "<Value>0</Value>";
				para += "<Description>I am ECHO</Description>";
//				para += "<ToDeliveryPoint>00</ToDeliveryPoint>";
				para += "<MailClass>Priority</MailClass>";
				para += "<MailpieceShape>"+mail_piece_shape+"</MailpieceShape>";
				para += "<ResponseOptions PostagePrice='FALSE'/>";//返回更细致邮费信息
				para += "<ValidateAddress>FALSE</ValidateAddress>";
				para += "<PartnerCustomerID>"+String.valueOf(this.oid)+"</PartnerCustomerID>";
				para += "<PartnerTransactionID>"+String.valueOf(this.oid)+"</PartnerTransactionID>";

				para += "<FromName>"+this.fromName+"</FromName>";
				para += "<ReturnAddress1>"+this.fromAddress1+this.fromAddress2+"</ReturnAddress1>";
				para += "<FromCity>"+this.fromCity+"</FromCity>";
				para += "<FromState>"+this.fromState+"</FromState>";
				para += "<FromPostalCode>"+this.fromZip5+"</FromPostalCode>";
//				para += "<FromZIP4>0000</FromZIP4>";
				para += "<FromPhone>"+this.fromPhone+"</FromPhone>";

				para += "<ToName>"+this.toName+"</ToName>";
				para += "<ToCompany></ToCompany>";
				para += "<ToAddress1>"+this.toAddress2+"</ToAddress1>";
				para += "<ToCity>"+this.toCity+"</ToCity>";
				para += "<ToState>"+this.toState+"</ToState>";
				para += "<ToPostalCode>"+this.toZip5+"</ToPostalCode>";
//				para += "<ToZIP4>0000</ToZIP4>";
				para += "<WeightOz>"+this.weightOunces+"</WeightOz>";
				para += "<ToPhone>"+this.toPhone+"</ToPhone>";
				para += "</LabelRequest>";
				
				//log.info(para);
//				//system.out.print(para);
				String responseXmlStr = Client.httpPost(server, para,timeout);
				//system.out.println(responseXmlStr);
				//log.info("==================================================");
				if (responseXmlStr.toLowerCase().indexOf("errormessage")>=0)
				{
					String description = getSampleNode(responseXmlStr, "ErrorMessage");
					throw new UspsResponseErrorException(description);
				}

				String base64Img = getSampleNode(responseXmlStr, "Base64LabelImage");
				String waybillNo = getSampleNode(responseXmlStr,  "TrackingNumber");
				String postage = getSampleNode(responseXmlStr,"FinalPostage");

				//log.info(base64Img);
				
				//long st = System.currentTimeMillis();
				
				createPrintLabelBase64Img(base64Img,waybillNo);//把XML转换成TIF图片
				
				//convert2PNG(imgPath+"/"+waybillNo+".tif",imgPath+"/"+waybillNo+".png");//把TIF转换成PNG
				//cropImage(imgPath+"/"+waybillNo+".png",imgPath+"/"+waybillNo+".png",130,   100,   1220, 830);//剪裁图片
				//scale(imgPath+"/"+waybillNo+".png", imgPath+"/"+waybillNo+".png", 600);//缩放图片
			
				//long en = System.currentTimeMillis();
				////system.out.println(en-st);
				this.waybillNo = waybillNo;
				this.finalPostage = Double.valueOf(postage);
			} 
			catch (SocketTimeoutException e)
			{
				throw e;
			}
			catch (UspsResponseErrorException e)
			{
				throw e;
			}
			catch(ProductDataErrorException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw new SystemException(e,"commit",log);
			}
		}

		public void commit2(String trackingNumber) throws SocketTimeoutException, Exception
		{
			String para = "<?xml version='1.0' encoding='utf-8'?>" 
						 +"<StatusRequest>" 
						 +"<AccountID>"+AccountID+"</AccountID>"
						 +"<Test>N</Test>" 
						 +"<PassPhrase>"+PassPhrase+"</PassPhrase>" 
						 +"<FullStatus>No</FullStatus>" 
						 +"<StatusList>"
						 	+"<PICNumber>"+trackingNumber+"</PICNumber>"
							+"<PieceID>"+trackingNumber+"</PieceID>"
							+"<CustomsID>"+trackingNumber+"</CustomsID>"
						 +"</StatusList>" 
						+"</StatusRequest>";
			//system.out.println(para);
			
			String soapRequestData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" 
									+"<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">" 
									+"<soap12:Body>"
									+ "<StatusRequest xmlns=\"http://els/\">"  
										+"<AccountID>"+AccountID+"</AccountID>"
										+"<Test>N</Test>" 
										+"<PassPhrase>"+PassPhrase+"</PassPhrase>" 
										+"<FullStatus>No</FullStatus>" 
										+"<StatusList>"
									 		+"<PICNumber>"+trackingNumber+"</PICNumber>"
									 		+"<PieceID>"+trackingNumber+"</PieceID>"
									 		+"<CustomsID>"+trackingNumber+"</CustomsID>"
									 		+"</StatusList>" 
									 	+"</StatusRequest>"   
									+"</soap12:Body>" 
									+"</soap12:Envelope>";
	          
//	        //system.out.println(soapRequestData); 
			String responseXmlStr = Client.HttpSoap(track_server,soapRequestData,timeout);
			
			//system.out.println(responseXmlStr);
		}
		
		/**
		 * 获得节点数据（只适合唯一名称节点）
		 * @return
		 */
		private String getSampleNode(String xml,String name)
		{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");

			return(xmlSplit2[0]);
		}
		
	

	public void setFromName(String fromName)
	{
		this.fromName = fromName;
	}

	public void setFromAddress2(String fromAddress2)
	{
		this.fromAddress2 = fromAddress2;
	}

	public void setFromCity(String fromCity)
	{
		this.fromCity = fromCity;
	}

	public void setFromState(String fromState)
	{
		this.fromState = fromState;
	}

	public void setFromZip5(String fromZip5)
	{
		this.fromZip5 = fromZip5;
	}

	public void setToName(String toName)
	{
		this.toName = toName;
	}

	public void setToAddress2(String toAddress2)
	{
		this.toAddress2 = toAddress2;
	}

	public void setToCity(String toCity)
	{
		this.toCity = toCity;
	}

	public void setToState(String toState)
	{
		this.toState = toState;
	}

	public void setToZip5(String toZip5)
	{
		this.toZip5 = toZip5;
	}

	public void setWeightOunces(String weightOunces)
	{
		this.weightOunces = weightOunces;
	}
	
	public void setFromAddress1(String fromAddress1)
	{
		this.fromAddress1 = fromAddress1;
	}

	public void setFromPhone(String fromPhone)
	{
		this.fromPhone = fromPhone;
	}

	public void setToPhone(String toPhone)
	{
		this.toPhone = toPhone;
	}
	
	public void setOid(long oid)
	{
		this.oid = oid;
	}
	
	
	public static void main(String args[]) throws Exception
	{
		
		EndiciaClient endiciaClient = new EndiciaClient();
		
		endiciaClient.setOid(910);
		
		endiciaClient.setFromName("Visionari e-Commcerce");
		endiciaClient.setFromAddress1("Keystone Drive");
		endiciaClient.setFromAddress2("150");
		endiciaClient.setFromCity("Montgomeryville");
		endiciaClient.setFromState("PA");
		endiciaClient.setFromZip5("18936");
		endiciaClient.setFromPhone("2157761095");
		
		endiciaClient.setToName("Jonathan Blank");
		endiciaClient.setToAddress2("4239 Coolidge Ave");
		endiciaClient.setToCity("Los Angeles");
		
		endiciaClient.setToState("CA");
//		endiciaClient.setToState(order.getString("address_state"));
		endiciaClient.setToZip5("90066");
		endiciaClient.setToPhone("");
		endiciaClient.setWeightOunces(String.valueOf(MoneyUtil.round(5*35.2733686f, 1)));
		
		boolean uspsResponseError = false;
		String uspsResponseErrorDescription = "";
		
		try
		{	
			
			endiciaClient.commit("no","Parcel");//yes代表当前为测试no为正式提交（会扣掉运费）
			String mail_piece_shape = endiciaClient.getInvoiceMailPieceShape();
		}
		catch (java.net.SocketTimeoutException e)
		{
			uspsResponseError = true;
			uspsResponseErrorDescription = "连接打印服务器超时，请重新打印！";
		}	
		catch (UspsResponseErrorException e)
		{
			uspsResponseError = true;
			uspsResponseErrorDescription = e.getMessage();
		}
		catch(ProductDataErrorException e)
		{
			uspsResponseError = true;
			uspsResponseErrorDescription = "商品数据有误，请联系调度！";
		}
		
		String wayBillNo = endiciaClient.getWaybillNo();
		
		//system.out.println(wayBillNo);
	}


	
	
	public String getInvoiceMailPieceShape()
	{
		return invoiceMailPieceShape;
	}

	public double getFinalPostage() {
		return finalPostage;
	}

	public void setFinalPostage(double finalPostage) {
		this.finalPostage = finalPostage;
	}
}








