package com.cwc.endicia;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.*;
import java.net.SocketTimeoutException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.soap.encoding.soapenc.Base64;

import com.cwc.app.util.Environment;
import com.cwc.exception.SystemException;
import com.cwc.http.Client;
import com.cwc.usps.UspsResponseErrorException;
import com.sun.media.jai.codec.*;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class EndiciaFirstClassClient  
{
	static Logger log = Logger.getLogger("ACTION");

	private String RequesterID = "lvis";
	private String AccountID = "770373";
	private String PassPhrase = "Vvme91754";
	
	private String imgPath = Environment.getHome()+"administrator/order/UspsPrintLabel";
	private String server = "https://labelServer.Endicia.com/LabelService/EwsLabelService.asmx/GetPostageLabelXML";
	private String waybillNo;
	
	//收发件人信息
	private String fromName;
	private String fromAddress2;
	private String fromAddress1;
	private String fromCity;
	private String fromState;
	private String fromZip5;
	private String fromPhone;

	private String toName;
	private String toAddress2;
	private String toCity;
	private String toState;
	private String toZip5;
	private String weightOunces;
	private String toPhone;
	
	private String invoiceMailPieceShape;
	
	private double finalPostage;	//花费邮资

	private long oid;
	
	int timeout = 10000; //连接服务器超时时间

	public String getWaybillNo()
	{
		return waybillNo;
	}
	
	/**
	 * 生成Usps打印Label图片
	 * @param base64Str
	 * @param name
	 * @throws Exception
	 */
	public void createPrintLabelBase64Img(String base64Str,String name)
		throws Exception
	{
		try
		{
			//BASE64Decoder decoder = new BASE64Decoder();   
			byte[] buffer = Base64.decode(base64Str);	
			//log.info(base64Str);
			// Base64解码   
			//byte[] buffer = decoder.decodeBuffer(base64Str);   
//			for (int i = 0; i < buffer.length; ++i) 
//			{   
//				if (buffer[i] < 0)  // 调整异常数据   
//				{
//					buffer[i] += 256;   
//				}
//			}  
			
			File file = new File(imgPath+"/"+name+".png");
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
			bos.write(buffer);
			bos.flush();
			bos.close();
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"createPrintLabelBase64Img",log);
		}
	}

	/**
	 * 把TIF图片转换成PNG
	 * @param tif
	 * @param png
	 * @throws Exception
	 */
	public void convert2PNG(String tif,String png)
		throws Exception
	{
		try
		{
			FileOutputStream out = new FileOutputStream(png);
			File file = new File(tif);
			ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",file,null);
			ImageEncoder encoder = ImageCodec.createImageEncoder("png",out,null);
			encoder.encode( decoder.decodeAsRenderedImage() );
			decoder.getInputStream().close();//不关闭，源文件删不掉
			encoder.getOutputStream().close();
			file.delete();
			out.close();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"convert2PNG",log);
		}
	}
	
	/**
	 * 剪裁图片
	 * @param src
	 * @param out
	 * @param sx
	 * @param sy
	 * @param width
	 * @param height
	 * @throws Exception
	 */
	public void cropImage(String src,String out,int   sx,   int   sy,   int   width,   int   height)
		throws Exception
    {
		try
		{
	        //1.获得原始图象Image对象
	        File   file=new   File(src);
	        BufferedImage baseImage   =ImageIO.read(file);
	        
            ImageFilter   filter   =   new   CropImageFilter(sx,   sy,   width,   height);//根据图像裁剪过滤器产生过滤器
            //下面根据过滤器产生图像生产者
            ImageProducer   producer   =   new   FilteredImageSource(baseImage.getSource(),   filter);
            Image img   =   Toolkit.getDefaultToolkit().createImage(producer);//根据图像生产者产生新图像
            
            //3.生成图片文件
            BufferedImage tag   =   new   BufferedImage(width,   height,   BufferedImage.TYPE_INT_RGB);
            tag.getGraphics().drawImage(img,   0,   0,   width,   height,   null);
            
            ImageIO.write(tag, "png", new File(out));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"cropImage",log);
		}   
    } 
	
	/**
	 * 缩放图片
	 * @param srcImageFile
	 * @param result
	 * @param out_width
	 * @throws Exception
	 */
	 public void scale(String srcImageFile, String result, int out_width) 
	 	throws Exception
	 {
		  try
		  {
			   BufferedImage src = ImageIO.read(new File(srcImageFile)); // 读入文件
			   float width = src.getWidth(); // 得到源图宽
			   float height = src.getHeight(); // 得到源图长
			   
			   int out_height = (int)((out_width/width)*height);
			   
			   Image image = src.getScaledInstance(out_width, out_height, Image.SCALE_DEFAULT);
		
			   BufferedImage tag = new BufferedImage(out_width, out_height,BufferedImage.TYPE_INT_RGB);
			   Graphics g = tag.getGraphics();
			   g.drawImage(image, 0, 0, null); // 绘制缩小后的图
			   g.dispose();
			   ImageIO.write(tag, "png", new File(result));// 输出到文件流
		  }
		  catch (IOException e)
		  {
			  throw new SystemException(e,"scale",log);
		  }
	 }
	
	
		
		/**
		 * 提交数据给USPS并生成打印LABEL
		 * @throws Exception
		 */
		public void commit(String test)
			throws SocketTimeoutException,UspsResponseErrorException,Exception
		{
			try
			{
				String mailpieceShape = "Parcel";
				this.invoiceMailPieceShape = mailpieceShape;//返回给运单用
				
				String para = "labelRequestXML=<LabelRequest ImageFormat='GIF' ImageResolution='203' Test='"+test+"' >";
				
				para += "<RequesterID>"+RequesterID+"</RequesterID>";
				para += "<AccountID>"+AccountID+"</AccountID>";
				para += "<PassPhrase>"+PassPhrase+"</PassPhrase>";
				para += "<DateAdvance>0</DateAdvance>";
				para += "<Stealth>true</Stealth>";//打印邮资
				para += "<Services DeliveryConfirmation='ON' />";
				para += "<Value>0</Value>";
				para += "<Description>I am ECHO</Description>";
//				para += "<ToDeliveryPoint>00</ToDeliveryPoint>";
				para += "<MailClass>First</MailClass>";
				para += "<MailpieceShape>"+mailpieceShape+"</MailpieceShape>";
				para += "<ResponseOptions PostagePrice='FALSE'/>";//返回更细致邮费信息
				para += "<ValidateAddress>FALSE</ValidateAddress>";

				para += "<PartnerCustomerID>"+String.valueOf(this.oid)+"</PartnerCustomerID>";
				para += "<PartnerTransactionID>"+String.valueOf(this.oid)+"</PartnerTransactionID>";

				para += "<FromName>"+this.fromName+"</FromName>";
				para += "<ReturnAddress1>"+this.fromAddress1+this.fromAddress2+"</ReturnAddress1>";
				para += "<FromCity>"+this.fromCity+"</FromCity>";
				para += "<FromState>"+this.fromState+"</FromState>";
				para += "<FromPostalCode>"+this.fromZip5+"</FromPostalCode>";
//				para += "<FromZIP4>0000</FromZIP4>";
				para += "<FromPhone>"+this.fromPhone+"</FromPhone>";

				para += "<ToName>"+this.toName+"</ToName>";
				para += "<ToCompany></ToCompany>";
				para += "<ToAddress1>"+this.toAddress2+"</ToAddress1>";
				para += "<ToCity>"+this.toCity+"</ToCity>";
				para += "<ToState>"+this.toState+"</ToState>";
				para += "<ToPostalCode>"+this.toZip5+"</ToPostalCode>";
//				para += "<ToZIP4>0000</ToZIP4>";
				para += "<WeightOz>"+this.weightOunces+"</WeightOz>";
				para += "<ToPhone>"+this.toPhone+"</ToPhone>";
				para += "</LabelRequest>";
				
				////system.out.println("> "+this.weightOunces);
				//log.info(para);
				
				String responseXmlStr = Client.httpPost(server, para,timeout);
				
				//log.info("==================================================");
				if (responseXmlStr.toLowerCase().indexOf("errormessage")>=0)
				{
					String description = getSampleNode(responseXmlStr, "ErrorMessage");
					throw new UspsResponseErrorException(description);
				}

				String base64Img = getSampleNode(responseXmlStr, "Base64LabelImage");
				String waybillNo = getSampleNode(responseXmlStr,  "TrackingNumber");
				String postage = getSampleNode(responseXmlStr,"FinalPostage");
				//log.info(base64Img);
				
				//long st = System.currentTimeMillis();
				
				createPrintLabelBase64Img(base64Img,waybillNo);//把XML转换成TIF图片
				//convert2PNG(imgPath+"/"+waybillNo+".tif",imgPath+"/"+waybillNo+".png");//把TIF转换成PNG
				//cropImage(imgPath+"/"+waybillNo+".png",imgPath+"/"+waybillNo+".png",130,   100,   1220, 830);//剪裁图片
				//scale(imgPath+"/"+waybillNo+".png", imgPath+"/"+waybillNo+".png", 600);//缩放图片
			
				//long en = System.currentTimeMillis();
				////system.out.println(en-st);
				this.waybillNo = waybillNo;
				this.finalPostage = Double.valueOf(postage);
			} 
			catch (SocketTimeoutException e)
			{
				throw e;
			}
			catch (UspsResponseErrorException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw new SystemException(e,"commit",log);
			}
		}

		/**
		 * 获得节点数据（只适合唯一名称节点）
		 * @return
		 */
		private String getSampleNode(String xml,String name)
		{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");

			return(xmlSplit2[0]);
		}
		
		
	
	 
	 
	 
	

	public void setFromName(String fromName)
	{
		this.fromName = fromName;
	}

	public void setFromAddress2(String fromAddress2)
	{
		this.fromAddress2 = fromAddress2;
	}

	public void setFromCity(String fromCity)
	{
		this.fromCity = fromCity;
	}

	public void setFromState(String fromState)
	{
		this.fromState = fromState;
	}

	public void setFromZip5(String fromZip5)
	{
		this.fromZip5 = fromZip5;
	}

	public void setToName(String toName)
	{
		this.toName = toName;
	}

	public void setToAddress2(String toAddress2)
	{
		this.toAddress2 = toAddress2;
	}

	public void setToCity(String toCity)
	{
		this.toCity = toCity;
	}

	public void setToState(String toState)
	{
		this.toState = toState;
	}

	public void setToZip5(String toZip5)
	{
		this.toZip5 = toZip5;
	}

	public void setWeightOunces(String weightOunces)
	{
		this.weightOunces = weightOunces;
	}
	
	public void setFromAddress1(String fromAddress1)
	{
		this.fromAddress1 = fromAddress1;
	}

	public void setFromPhone(String fromPhone)
	{
		this.fromPhone = fromPhone;
	}

	public void setToPhone(String toPhone)
	{
		this.toPhone = toPhone;
	}
	
	public void setOid(long oid)
	{
		this.oid = oid;
	}
	
	
	public static void main(String args[]) throws Exception
	{
		//String postageLabelRequestMethod = "https://www.envmgr.com/LabelService/EwsLabelService.asmx/GetPostageLabelXML";
		//String postageLabelRequestXML = "labelRequestXML=<LabelRequest ImageFormat='PNG' Test='YES'><RequesterID>abcd</RequesterID><AccountID>123456</AccountID><PassPhrase>samplePassPhrase</PassPhrase><MailClass>FIRST</MailClass><DateAdvance>0</DateAdvance><WeightOz>1</WeightOz><Stealth>FALSE</Stealth><Services InsuredMail='OFF' SignatureConfirmation='OFF' /><Value>0</Value><Description>Sample Label</Description><PartnerCustomerID>12345ABCD</PartnerCustomerID><PartnerTransactionID>6789EFGH</PartnerTransactionID><ToName>Amine Khechfe</ToName><ToCompany>Endicia</ToCompany><ToAddress1>247 High Street</ToAddress1><ToCity>Palo Alto</ToCity><ToState>CA</ToState><ToPostalCode>84301</ToPostalCode><ToZIP4>0000</ToZIP4><ToDeliveryPoint>00</ToDeliveryPoint><ToPhone>8005763279</ToPhone><FromName>John Doe</FromName><ReturnAddress1>123 Main Street</ReturnAddress1><FromCity>Boise</FromCity><FromState>ID</FromState><FromPostalCode>83702</FromPostalCode><FromZIP4>7261</FromZIP4><FromPhone>8005551212</FromPhone></LabelRequest>";


//		String postageRateRequestMethod = "https://labelServer.Endicia.com/LabelService/EwsLabelService.asmx/CalculatePostageRateXML";
//		String postageRateRequestXML = "postageRateRequestXML=<PostageRateRequest><RequesterID>lvis</RequesterID><CertifiedIntermediary><AccountID>770373</AccountID><PassPhrase>Vvme91754</PassPhrase></CertifiedIntermediary><MailClass>Priority</MailClass><WeightOz>1.3</WeightOz><MailpieceShape>SmallFlatRateBox</MailpieceShape><Machinable>True</Machinable><Services DeliveryConfirmation='ON' SignatureConfirmation='OFF'/><FromPostalCode>91754</FromPostalCode><ToPostalCode>60201</ToPostalCode><ResponseOptions PostagePrice='TRUE'/></PostageRateRequest>";
//		
//		String responseXmlStr = Client.httpPost(postageRateRequestMethod, postageRateRequestXML);
//		
//		//system.out.println(responseXmlStr);

		
		
		
//		EndiciaClient endiciaClient = new EndiciaClient();
//		endiciaClient.getMailpieceShape(1.3f,91754,10977);
		
//		float a = 1.3f*1000f*0.0352733686f;
//		//system.out.println(a);
//		
//		float b = a/1000f/0.0352733686f*2.2f;
//		
//		//system.out.println(b);
		
		//修改密码
//		String postageRateRequestMethod = "https://labelServer.Endicia.com/LabelService/EwsLabelService.asmx/ChangePassPhraseXML";
//		String postageRateRequestXML = "changePassPhraseRequestXML=<ChangePassPhraseRequest> <RequesterID>lvis</RequesterID> <RequestID>CPP123</RequestID> <CertifiedIntermediary> <AccountID>770373</AccountID> <PassPhrase>Vvme1980</PassPhrase> </CertifiedIntermediary> <NewPassPhrase>Vvmehanfei</NewPassPhrase> </ChangePassPhraseRequest>";
//		
//		String responseXmlStr = Client.httpPost(postageRateRequestMethod, postageRateRequestXML);
//		
//		//system.out.println(responseXmlStr);
		
		//买邮票
//		String recreditRequestMethod = "https://labelServer.Endicia.com/LabelService/EwsLabelService.asmx/BuyPostageXML";
//		String recreditRequestXML = "recreditRequestXML=<RecreditRequest> <RequesterID>lvis</RequesterID> <RequestID>BP123</RequestID> <CertifiedIntermediary> <AccountID>770373</AccountID> <PassPhrase>Vvmehanfei</PassPhrase> </CertifiedIntermediary> <RecreditAmount>500</RecreditAmount> </RecreditRequest>";
//		
//		String recreditResponseXmlStr = Client.httpPost(recreditRequestMethod, recreditRequestXML);
//		
//		//system.out.println(recreditResponseXmlStr);

		//String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><PostageRateResponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"www.envmgr.com/LabelService\">	<Status>0</Status>  	<Zone>8</Zone> 	<PostagePrice TotalAmount=\"13.21\">   		<Postage TotalAmount=\"14.21\">    			<MailService>Priority Mail</MailService>   			<Zone>8</Zone>    			<IntraBMC>false</IntraBMC>  			<Pricing>CommercialPlus</Pricing>   		</Postage>  		<Fees TotalAmount=\"0\">    			<CertificateOfMailing>0</CertificateOfMailing>  			<CertifiedMail>0</CertifiedMail>   			<CollectOnDelivery>0</CollectOnDelivery> 			<DeliveryConfirmation>0</DeliveryConfirmation>  			<ElectronicReturnReceipt>0</ElectronicReturnReceipt>  			<InsuredMail>0</InsuredMail>   			<RegisteredMail>0</RegisteredMail>  			<RestrictedDelivery>0</RestrictedDelivery>  			<ReturnReceipt>0</ReturnReceipt>   			<ReturnReceiptForMerchandise>0</ReturnReceiptForMerchandise>   			<SignatureConfirmation>0</SignatureConfirmation>   			<SpecialHandling>0</SpecialHandling>   		</Fees> 	</PostagePrice></PostageRateResponse>";
		
	}

	
	
	
	public String getInvoiceMailPieceShape()
	{
		return invoiceMailPieceShape;
	}

	public double getFinalPostage() {
		return finalPostage;
	}

	public void setFinalPostage(double finalPostage) {
		this.finalPostage = finalPostage;
	}



	
}








