package com.cwc.fedex;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Calendar;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.axis.types.PositiveInteger;
import org.apache.log4j.Logger;

import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.exception.SystemException;
import com.cwc.usps.UspsResponseErrorException;
import com.fedex.ship.stub.Address;
import com.fedex.ship.stub.ClientDetail;
import com.fedex.ship.stub.CompletedPackageDetail;
import com.fedex.ship.stub.CompletedShipmentDetail;
import com.fedex.ship.stub.Contact;
import com.fedex.ship.stub.DropoffType;
import com.fedex.ship.stub.LabelFormatType;
import com.fedex.ship.stub.LabelSpecification;
import com.fedex.ship.stub.LabelStockType;
import com.fedex.ship.stub.Notification;
import com.fedex.ship.stub.NotificationSeverityType;
import com.fedex.ship.stub.PackageRateDetail;
import com.fedex.ship.stub.PackagingType;
import com.fedex.ship.stub.Party;
import com.fedex.ship.stub.Payment;
import com.fedex.ship.stub.PaymentType;
import com.fedex.ship.stub.Payor;
import com.fedex.ship.stub.ProcessShipmentReply;
import com.fedex.ship.stub.ProcessShipmentRequest;
import com.fedex.ship.stub.RateDiscount;
import com.fedex.ship.stub.RateRequestType;
import com.fedex.ship.stub.RequestedPackageLineItem;
import com.fedex.ship.stub.RequestedShipment;
import com.fedex.ship.stub.ServiceType;
import com.fedex.ship.stub.ShipPortType;
import com.fedex.ship.stub.ShipServiceLocator;
import com.fedex.ship.stub.ShipmentRateDetail;
import com.fedex.ship.stub.ShipmentRating;
import com.fedex.ship.stub.ShippingDocument;
import com.fedex.ship.stub.ShippingDocumentImageType;
import com.fedex.ship.stub.ShippingDocumentPart;
import com.fedex.ship.stub.Surcharge;
import com.fedex.ship.stub.TransactionDetail;
import com.fedex.ship.stub.VersionId;
import com.fedex.ship.stub.WebAuthenticationCredential;
import com.fedex.ship.stub.WebAuthenticationDetail;
import com.fedex.ship.stub.Weight;
import com.fedex.ship.stub.WeightUnits;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FedexClient  
{
	static Logger log = Logger.getLogger("ACTION");
	
	private String imgPath = Environment.getHome()+"administrator/order/FedexPrintLabel";
	private String waybillNo = "";
	
	//发件人信息
	private String fromName;
	private String fromAddress;
	private String fromCity;
	private String fromState;
	private String fromZip;
	private String fromTel;
	
	//收件人
	private String toName;
	private String toAddress;
	private String toCity;
	private String toState;
	private String toZip;
	private String toTel;
	private String weightLB;
	
	//地址验证结果
	private String residentialStatus;
	
	private double baseCharge;			//基础运费
	private double fuelSurchargePercent;//燃油附加费费率
	private double freightDiscounts;	//折扣总值
	private double shippmentRate;		//总运费
	
	
	public void commit()
		throws UspsResponseErrorException,Exception
	{
		ProcessShipmentRequest request = buildRequest(); // Build a request object
	    //
		try
		{
			// Initialize the service
			ShipServiceLocator service;
			ShipPortType port;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();
		    //
			ProcessShipmentReply reply = port.processShipment(request); // This is the call to the ship web service passing in a request object and returning a reply object
			//
			if (isResponseOk(reply.getHighestSeverity())) // check if the call was successful
			{
				writeServiceOutput(reply);
			}

			printNotifications(reply.getNotifications());

		} 
		catch (UspsResponseErrorException e) 
		{
			throw e;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"commit",log);
		} 
	}
	
	
	
	private  ProcessShipmentRequest buildRequest() 
		throws UspsResponseErrorException
	{
		ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        //
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setCustomerTransactionId("java sample - Domestic Ground Shipment"); // The client will get the same value back in the response
        request.setTransactionDetail(transactionDetail);
        //
        VersionId versionId = new VersionId("ship", 12, 0, 0);
        request.setVersion(versionId);
        //
        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
     // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
        
        AddressValidationWebServiceClient addressValidationWebServiceClient = new AddressValidationWebServiceClient();
        try
        {
//        	//system.out.println("residentialStatus toName:"+this.toName+"____toAddress:"+this.toAddress+"___toZip:"+toZip);
            this.residentialStatus = addressValidationWebServiceClient.getResidentialStatus(this.toAddress, this.toZip,"US");
        }
        catch(Exception e)
        {
//        	e.printStackTrace();
        	throw new UspsResponseErrorException("地址校验网络错误，请重新打印！");
        }

//        //system.out.println(residentialStatus);
        if (this.residentialStatus.equals(AddressValidationWebServiceClient.RESIDENTIAL))
        {
        	////system.out.println("RESIDENTIAL!");
        	requestedShipment.setServiceType(ServiceType.GROUND_HOME_DELIVERY); 
        }
        else
        {
        	////system.out.println("Non-RESIDENTIAL FEDEX_GROUND!");
        	requestedShipment.setServiceType(ServiceType.FEDEX_GROUND); 
        }
        requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP);
        requestedShipment.setPackagingType(PackagingType.YOUR_PACKAGING); // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
        
        Party shipper = new Party();
        Contact contactShip = new Contact();
        contactShip.setCompanyName(this.fromName);
        contactShip.setPhoneNumber(this.fromTel);
        shipper.setContact(contactShip);        
        
        
        Address addressShip = new Address();
        addressShip.setStreetLines(new String[] { this.fromAddress });
        addressShip.setCity(this.fromCity);
        addressShip.setStateOrProvinceCode(this.fromState);
        addressShip.setPostalCode(this.fromZip);
        addressShip.setCountryCode("US");
        shipper.setAddress(addressShip);
        
        requestedShipment.setShipper(shipper); // Sender information
        //
        Party recipient = new Party(); // Recipient information
        
        Contact contactRecip = new Contact();
        contactRecip.setCompanyName(this.toName);
        
        contactRecip.setPhoneNumber(this.toTel);
        recipient.setContact(contactRecip);
        //
        Address addressRecip = new Address();
        addressRecip.setStreetLines(new String[] { this.toAddress });
        addressRecip.setCity(this.toCity);
        addressRecip.setStateOrProvinceCode(this.toState);
        addressRecip.setPostalCode(this.toZip);
        addressRecip.setCountryCode("US");
        //设置投递家庭住址

        if (this.residentialStatus.equals(AddressValidationWebServiceClient.RESIDENTIAL))
        {
        	addressRecip.setResidential(new Boolean(true));        	
        }
        else
        {
        	addressRecip.setResidential(new Boolean(false));
        }
        
        recipient.setAddress(addressRecip);
        requestedShipment.setRecipient(recipient);

        //
        Payment shippingChargesPayment = new Payment(); // Payment information
		shippingChargesPayment.setPaymentType(PaymentType.SENDER);
		Party responsibleParty = new Party();
		responsibleParty.setAccountNumber(getPayorAccountNumber());
		Address responsiblePartyAddress = new Address();
		responsiblePartyAddress.setCountryCode("US");
		responsibleParty.setAddress(responsiblePartyAddress);
		responsibleParty.setContact(new Contact());
		Payor payor = new Payor();
		payor.setResponsibleParty(responsibleParty);
		shippingChargesPayment.setPayor(payor);
        
        requestedShipment.setShippingChargesPayment(shippingChargesPayment);
        //
     
        LabelSpecification labelSpecification = new LabelSpecification(); // Label specification
		labelSpecification.setImageType(ShippingDocumentImageType.PNG);// Image types PDF, PNG, DPL, ...
	    labelSpecification.setLabelFormatType(LabelFormatType.COMMON2D);
	    labelSpecification.setLabelStockType(LabelStockType.value1); // STOCK_4X6
	    requestedShipment.setLabelSpecification(labelSpecification);
        
        //
        RateRequestType[] rrt= new RateRequestType[] { RateRequestType.ACCOUNT }; // Rate types requested LIST, MULTIWEIGHT, ...
        requestedShipment.setRateRequestTypes(rrt);
        requestedShipment.setPackageCount(new NonNegativeInteger("1"));
//        requestedShipment.setPackageDetail(RequestedPackageDetailType.INDIVIDUAL_PACKAGES);//V9与V12的不同
        //
        RequestedPackageLineItem[] rp = new RequestedPackageLineItem[] { new RequestedPackageLineItem() };
        rp[0].setWeight(new Weight()); // Package weight information
        rp[0].getWeight().setValue(new BigDecimal(this.weightLB));
        rp[0].getWeight().setUnits(WeightUnits.LB);
//        rp[0].setDimensions(new Dimensions());
//        rp[0].getDimensions().setLength(new NonNegativeInteger("108"));
//        rp[0].getDimensions().setWidth(new NonNegativeInteger("5"));
//        rp[0].getDimensions().setHeight(new NonNegativeInteger("5"));
//        rp[0].getDimensions().setUnits(LinearUnits.IN);
//        rp[0].setCustomerReferences(new CustomerReference[] { new CustomerReference(), new CustomerReference(), new CustomerReference() }); // Reference details
//        rp[0].getCustomerReferences()[0].setCustomerReferenceType(CustomerReferenceType.CUSTOMER_REFERENCE);
//        rp[0].getCustomerReferences()[0].setValue("GR4567892");
//        rp[0].getCustomerReferences()[1].setCustomerReferenceType(CustomerReferenceType.INVOICE_NUMBER);
//        rp[0].getCustomerReferences()[1].setValue("INV4567892");
//        rp[0].getCustomerReferences()[2].setCustomerReferenceType(CustomerReferenceType.P_O_NUMBER);
//        rp[0].getCustomerReferences()[2].setValue("PO4567892");
        
        rp[0].setSequenceNumber(new PositiveInteger("1"));
	    //
	    requestedShipment.setRequestedPackageLineItems(rp);
        //
	    request.setRequestedShipment(requestedShipment);
		//
	    return request;
	}
	//
	private  void writeServiceOutput(ProcessShipmentReply reply) throws Exception
	{
		try
		{
			CompletedShipmentDetail csd = reply.getCompletedShipmentDetail();
			
			ShipmentRating sr = csd.getShipmentRating();
			if(sr!=null)
			{
				ShipmentRateDetail[] srd = sr.getShipmentRateDetails();
				for (int i = 0; i < srd.length; i++) 
				{
					BigDecimal bc = srd[i].getTotalBaseCharge().getAmount();
					BigDecimal fd = srd[i].getTotalFreightDiscounts().getAmount();
					BigDecimal fsp = srd[i].getFuelSurchargePercent().divide(new BigDecimal(100));
					fsp = fsp.add(new BigDecimal(1));
					bc = bc.subtract(fd);
					bc = bc.multiply(fsp);
					
					this.setShippmentRate(MoneyUtil.round(bc.doubleValue(),2));
				}
			}
			
			CompletedPackageDetail cpd[] = csd.getCompletedPackageDetails();
			////system.out.println("Package details\n");
			for (int i=0; i < cpd.length; i++) { // Package details / Rating information for each package
				String trackingNumber = cpd[i].getTrackingIds()[0].getTrackingNumber();
				////system.out.println("Tracking #: " + trackingNumber							+ " Form ID: " + cpd[i].getTrackingIds()[0].getFormId());
				////system.out.println("\nRate details\n");
				//
				PackageRateDetail[] prd = cpd[i].getPackageRating().getPackageRateDetails();
				for(int j=0; j < prd.length; j++)
				{
					if (prd[j].getBillingWeight() != null) {
						////system.out.println("Billing weight: " + prd[j].getBillingWeight().getValue() 							+ " " + prd[j].getBillingWeight().getUnits());
					}
					////system.out.println("Base charge: " + prd[j].getBaseCharge().getAmount()							+ " " + prd[j].getBaseCharge().getCurrency());
					////system.out.println("Net charge: " + prd[j].getNetCharge().getAmount() 							+ " " + prd[j].getBaseCharge().getCurrency());
					if (null != prd[j].getSurcharges())
					{
						Surcharge[] s = prd[j].getSurcharges();
						for(int k=0; k < s.length; k++)
						{
							////system.out.println(s[k].getSurchargeType() + " surcharge " + 									s[k].getAmount().getAmount() + " " + s[k].getAmount().getCurrency());
						}
					}
					////system.out.println("Total surcharge: " + prd[j].getTotalSurcharges().getAmount() + " " + 							prd[j].getTotalSurcharges().getCurrency());
					////system.out.println("\nRouting details\n");
					////system.out.println("URSA prefix: " + csd.getRoutingDetail().getUrsaPrefixCode()							+ " suffix: " + csd.getRoutingDetail().getUrsaSuffixCode());
					////system.out.println("Service commitment: " + csd.getRoutingDetail().getCommitDay()							+ " Airport ID: " + csd.getRoutingDetail().getAirportId());
					////system.out.println("Delivery day: " + csd.getRoutingDetail().getDeliveryDay()); 

				}
				//	Write label buffer to file
				ShippingDocument sd = cpd[i].getLabel();
				saveLabelToFile(sd, trackingNumber);
//				ShippingDocument codLabel = cpd[i].getCodReturnDetail().getLabel();
//				saveCodLabelToFile(codLabel, trackingNumber);
			}
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"commit",log);
		}
		finally
		{
			//
		}
	}
	
	private  void saveLabelToFile(ShippingDocument shippingDocument, String trackingNumber) throws Exception {
		ShippingDocumentPart[] sdparts = shippingDocument.getParts();
		for (int a=0; a < sdparts.length; a++) {
			ShippingDocumentPart sdpart = sdparts[a];
			String labelLocation = imgPath;
			String labelFileName =  new String(labelLocation + "/shipping_label." + trackingNumber + "_" + a + ".png");					
			File labelFile = new File(labelFileName);
			FileOutputStream fos = new FileOutputStream( labelFile );
			fos.write(sdpart.getImage());
			fos.close();
			////system.out.println("\nlabel file name " + labelFile.getAbsolutePath());
			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + labelFile.getAbsolutePath());
			
			this.waybillNo = trackingNumber;
		}
	}
    
	private  String getPayorAccountNumber() {
		// See if payor account number is set as system property,
		// if not default it to "XXX"
		String payorAccountNumber = System.getProperty("Payor.AccountNumber");
		if (payorAccountNumber == null) {
			payorAccountNumber = "128544119"; // Replace "XXX" with the payor account number
		}
		return payorAccountNumber;
	}

	private  boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}
    

	private  ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
        
        //
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (accountNumber == null) {
        	accountNumber = "128544119"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "102483532"; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private  WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "6ukdvDUaV0lc92jB"; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "IWBGFds3SiaEvzDl6Hn3JcZHM"; // Replace "XXX" with clients password
        }
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}

	private  void printNotifications(Notification[] notifications) throws UspsResponseErrorException 
	{
		StringBuffer msgSB = new StringBuffer("");
		
		msgSB.append("Notifications:");
		if (notifications == null || notifications.length == 0) {
			msgSB.append("  No notifications returned");
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			msgSB.append("  Notification no. " + i + ": ");
			if (n == null) {
				msgSB.append("null");
				continue;
			} else {
				msgSB.append("");
			}
			NotificationSeverityType nst = n.getSeverity();

			msgSB.append("    Severity: " + (nst == null ? "null" : nst.getValue()));
			msgSB.append("    Code: " + n.getCode());
			msgSB.append("    Message: " + n.getMessage());
			msgSB.append("    Source: " + n.getSource());
		}
		
		String msg = msgSB.toString();
		
		if (msg.toLowerCase().indexOf("error")>=0)
		{
			throw new UspsResponseErrorException(msg);
		}
	}
	
	private  void updateEndPoint(ShipServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setShipServicePortEndpointAddress(endPoint);
		}
	}
	
	
	
	
	
	public void setFromName(String fromName)
	{
		this.fromName = fromName;
	}
	public void setFromAddress(String fromAddress)
	{
		this.fromAddress = fromAddress;
	}
	public void setFromCity(String fromCity)
	{
		this.fromCity = fromCity;
	}
	public void setFromState(String fromState)
	{
		this.fromState = fromState;
	}
	public void setFromZip(String fromZip)
	{
		this.fromZip = fromZip;
	}
	public void setFromTel(String fromTel)
	{
		this.fromTel = fromTel;
	}
	
	
	public void setToName(String toName)
	{
		this.toName = toName;
	}
	public void setToAddress(String toAddress)
	{
		this.toAddress = toAddress;
	}
	public void setToCity(String toCity)
	{
		this.toCity = toCity;
	}
	public void setToState(String toState)
	{
		this.toState = toState;
	}
	public void setToZip(String toZip)
	{
		this.toZip = toZip;
	}
	public void setToTel(String toTel)
	{
		this.toTel = toTel;
	}
	public void setWeightLB(String weightLB)
	{
		this.weightLB = weightLB;
	}
	public String getWaybillNo()
	{
		return waybillNo;
	}



	public String getResidentialStatus() {
		return residentialStatus;
	}



	public double getBaseCharge() {
		return baseCharge;
	}



	public void setBaseCharge(double baseCharge) {
		this.baseCharge = baseCharge;
	}



	public double getFuelSurchargePercent() {
		return fuelSurchargePercent;
	}



	public void setFuelSurchargePercent(double fuelSurchargePercent) {
		this.fuelSurchargePercent = fuelSurchargePercent;
	}



	public double getFreightDiscounts() {
		return freightDiscounts;
	}



	public void setFreightDiscounts(double freightDiscounts) {
		this.freightDiscounts = freightDiscounts;
	}



	public double getShippmentRate() {
		return shippmentRate;
	}



	public void setShippmentRate(double shippmentRate) {
		this.shippmentRate = shippmentRate;
	}
	
	
}








