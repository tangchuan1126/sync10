package com.cwc.fedex;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.cwc.db.DBRow;
import com.fedex.track.stub.*;

/** 
 * Demo of using the Track service with Axis 
 * to track a shipment.
 * <p>
 * com.fedex.track.stub is generated via WSDL2Java, like this:<br>
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.track.stub http://www.fedex.com/...../TrackService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
public class TrackWebServiceClient {
	//
	
	public Map trackCommit(String trackingNumber) 
		throws Exception 
	{   

		//
	    TrackRequest request = new TrackRequest();

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        //
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setCustomerTransactionId("java sample - Tracking Request"); //This is a reference field for the customer.  Any value can be used and will be provided in the response.
        request.setTransactionDetail(transactionDetail);
 
        //
        VersionId versionId = new VersionId("trck", 6, 0, 0);
        request.setVersion(versionId);
        //
        TrackPackageIdentifier packageIdentifier = new TrackPackageIdentifier();
        packageIdentifier.setValue(trackingNumber);// tracking number
        packageIdentifier.setType(TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG); // Track identifier types are TRACKING_NUMBER_OR_DOORTAG, TRACKING_CONTROL_NUMBER, ....
        request.setPackageIdentifier(packageIdentifier);
      
        request.setIncludeDetailedScans(new Boolean(true));
        
        Map returnMap = new HashMap();
	    //
		try 
		{
			// Initializing the service
			TrackServiceLocator service;
			TrackPortType port;
			//
			service = new TrackServiceLocator();
			updateEndPoint(service);
			port = service.getTrackServicePort();
		    //
			TrackReply reply = port.track(request); // This is the call to the web service passing in a request object and returning a reply object
						//
			if (isResponseOk(reply.getHighestSeverity())) // check if the call was successful
			{
//				//system.out.println("Tracking detail\n");
				TrackDetail td[] = reply.getTrackDetails();
				for (int i=0; i< td.length; i++) 
				{ 
					// package detail information
//					//system.out.println("Package # : " + td[i].getPackageSequenceNumber() 
//								+ " and Package count: " + td[i].getPackageCount());
//					//system.out.println("Tracking number: " + td[i].getTrackingNumber() 
//								+ " and Tracking number unique identifier: " + td[i].getTrackingNumberUniqueIdentifier());
//					//system.out.println("Status code: " + td[i].getStatusCode()); 
//					//system.out.println("Status description: " + td[i].getStatusDescription());
					if(td[i].getOtherIdentifiers() != null)
					{
						TrackPackageIdentifier[] tpi = td[i].getOtherIdentifiers();
//						for (int j=0; j< tpi.length; j++) {
//							//system.out.println(tpi[j].getType() + " " + tpi[j].getValue());
//						}
					}
//					print("Packaging", td[i].getPackaging());
					
					
					MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);//2位小数，四舍五入
					
					Weight packageWeight = td[i].getPackageWeight();
					Weight shipmentWeight = td[i].getShipmentWeight();
					
					float packageWeightFloat = 0;
					float shipmentWeightFloat = 0;
					
					if(packageWeight !=null)
					{
						String packageWeightUnits = packageWeight.getUnits().getValue();
						BigDecimal packageWeightBD = td[i].getPackageWeight().getValue();
						if(packageWeightUnits.toUpperCase().equals("LB"))
						{
							packageWeightFloat = packageWeightBD.divide(new BigDecimal(2.2),mc).floatValue();
						}
						else
						{
							packageWeightFloat = packageWeightBD.floatValue();
						}
					}
					
					if(shipmentWeight !=null)
					{
						String shipmentWeightUnits = shipmentWeight.getUnits().getValue();
						BigDecimal shipmentWeightBD = td[i].getShipmentWeight().getValue();
						if(shipmentWeightUnits.toUpperCase().equals("LB"))
						{
							shipmentWeightFloat = shipmentWeightBD.divide(new BigDecimal(2.2),mc).floatValue();
						}
						else
						{
							shipmentWeightFloat = shipmentWeightBD.floatValue();
						}
					}
					
					float weight = 0;
					
					if(shipmentWeightFloat>packageWeightFloat)
					{
						weight = shipmentWeightFloat;
					}
					else
					{
						weight = packageWeightFloat;
					}
					
					returnMap.put("weight",String.valueOf(weight));
					returnMap.put("shippments",new DBRow[0]);
					
//					printWeight("Package weight", td[i].getPackageWeight());
//					printWeight("Shipment weight", td[i].getShipmentWeight());
//				
//					print("Ship date & time", td[i].getShipTimestamp().getTime());
//					//system.out.println("Destination: " + td[i].getDestinationAddress().getCity() 
//							+ " " + td[i].getDestinationAddress().getPostalCode()
//							+ " " + td[i].getDestinationAddress().getCountryCode());
					

					TrackEvent[] trackEvents = td[i].getEvents();
					if (trackEvents != null) 
					{
						 ArrayList<DBRow> returnList = new ArrayList<DBRow>();
			           	 int count = 0;
			           	 while (count<trackEvents.length&&count<3)
			           	 {
			           		TrackEvent trackEvent = trackEvents[count];
							if (trackEvent != null)
							{
								DBRow shipment = new DBRow();
				           		shipment.add("status_code",trackEvent.getEventType());
	                    		
	                    		shipment.add("description",trackEvent.getEventDescription());
				           		returnList.add(shipment);
				           		
//				           		print("    Timestamp", trackEvent.getTimestamp().getTime());
//								print("    Description", trackEvent.getEventDescription());
//								print("    EventType",trackEvent.getEventType());
//								print("    StatusExceptionCode",trackEvent.getStatusExceptionCode());
//								print("    StatusExceptionDescription",trackEvent.getStatusExceptionDescription());
				           		
				           		count++;
							}
			           	 }
			           	 
			           	returnMap.put("shippments",returnList.toArray(new DBRow[0]));
						
//						//system.out.println("Events:");
//						for (int k = 0; k < trackEvents.length; k++) 
//						{
//							//system.out.println("  Event no.: " + (k+1));
//							TrackEvent trackEvent = trackEvents[k];
//							if (trackEvent != null) 
//							{
//								print("    Timestamp", trackEvent.getTimestamp().getTime());
//								print("    Description", trackEvent.getEventDescription());
//								print("    EventType",trackEvent.getEventType());
//								print("    StatusExceptionCode",trackEvent.getStatusExceptionCode());
//								print("    StatusExceptionDescription",trackEvent.getStatusExceptionDescription());
//								Address address = trackEvent.getAddress();
//								if (address != null) 
//								{
//									//system.out.println(address.getCity());
//									print("    City", address.getCity());
//									print("    State", address.getStateOrProvinceCode());
//								}
//							}
						}
				}
			}

			printNotifications(reply.getNotifications());

			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw e;
		} 
		
		return returnMap;
	}
	
	private  boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}
    

	private  ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
        
        //
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (accountNumber == null) {
        	accountNumber = "337755917"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "	104332939"; // Replace "XXX" with clients meter number
        }
        
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private  WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "etmjNJ4eC4PQJvzP ";//""; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "NrVixe1KEvQRffyZaCavcqx8v";//"IWBGFds3SiaEvzDl6Hn3JcZHM"; // Replace "XXX" with clients password
        }
        
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}
	
	private  void printNotifications(Notification[] notifications) {
//		//system.out.println("Notifications:");
		if (notifications == null || notifications.length == 0) {
//			//system.out.println("  No notifications returned");
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
//			//system.out.print("  Notification no. " + i + ": ");
			if (n == null) {
//				//system.out.println("null");
				continue;
			} else {
//				//system.out.println("");
			}
			NotificationSeverityType nst = n.getSeverity();

//			//system.out.println("    Severity: " + (nst == null ? "null" : nst.getValue()));
//			//system.out.println("    Code: " + n.getCode());
//			//system.out.println("    Message: " + n.getMessage());
//			//system.out.println("    Source: " + n.getSource());
		}
	}
	
	private void updateEndPoint(TrackServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setTrackServicePortEndpointAddress(endPoint);
		}
	}

	private void print(String msg, Object obj) {
		if (msg == null || obj == null) {
			return;
		}
		//system.out.println(msg + ": " + obj.toString());
	}
	
	
	private void printWeight(String msg, Weight weight) {
		if (msg == null || weight == null) {
			return;
		}
		//system.out.println(msg + ": " + weight.getValue() + " " + weight.getUnits());
	}

	private  String getSystemProperty(String property){
		String returnProperty = System.getProperty(property);
		if (returnProperty == null){
			return "XXX";
		}
		return returnProperty;
	}
	
	public static void main(String[] args) 
		throws Exception 
	{
		try {
			TrackWebServiceClient t = new TrackWebServiceClient();
			Map c = t.trackCommit("793893451534");
			DBRow[] cc = (DBRow[]) c.get("shippments");
			
			for (int i = 0; i < cc.length; i++) 
			{
				//system.out.println(cc[i].getString("status_code"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
