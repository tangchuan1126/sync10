package com.cwc.fedex;


import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.axis.types.PositiveInteger;
import org.apache.log4j.Logger;

import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.usps.UspsResponseErrorException;
import com.fedex.ship.stub.*;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FedexIPGZClient  
{
	static Logger log = Logger.getLogger("ACTION");
	
	private String imgPath = Environment.getHome()+"administrator/order/FedexInternationalPrintLabel";
	private String waybillNo = "";
	
	//发件人信息
	private String fromName;
	private String fromAddress;
	private String fromCity;
	private String fromState;
	private String fromZip;
	private String fromTel;
	private String fromCountry;
	
	//收件人
	private String toName;
	private String toAddress;
	private String toCity;
	private String toState;
	private String toZip;
	private String toTel;
	private double weightLB;
	private String toCountry;
	
	//发票信息
	private double customsValue;	//发票总申报价值
	private DBRow[] invoiceProducts;	//发票货物信息
	private String description;			//发票货物描述
	private float privilege;			//打印重折
	private String countryOfManufacture;//制造国
	private String currency;			//货币
	private String hscode;				//HSCode
	
	//关税支付
	private String dutiesPayment;	//支付关税
	private String creater;			//运单创建人对应DEPT
	private String note="";			//英文备注对应REF
	private String oids;			//关联的订单号对应INV
	
	private double baseCharge;			//基础运费
	private double fuelSurchargePercent;//燃油附加费费率
	private double freightDiscounts;	//折扣总值
	private double shippmentRate;		//总运费
	
	
	public void commit()
		throws UspsResponseErrorException,Exception
	{
		ProcessShipmentRequest request = buildRequest(); // Build a request object
	    //
		try
		{
			// Initialize the service
			ShipServiceLocator service;
			ShipPortType port;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();
		    //
			ProcessShipmentReply reply = port.processShipment(request); // This is the call to the ship web service passing in a request object and returning a reply object
			//
			if (isResponseOk(reply.getHighestSeverity())) // check if the call was successful
			{
				writeServiceOutput(reply,"1");
			}

			printNotifications(reply.getNotifications());

		} 
		catch (UspsResponseErrorException e) 
		{
			throw e;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"commit",log);
		} 
	}
	
	
	
	public void commitMPS(int pkcount)
		throws UspsResponseErrorException,Exception
	{
		// Initialize the service
		ShipServiceLocator service;
		ShipPortType port;
		//
		service = new ShipServiceLocator();
		updateEndPoint(service);
		port = service.getShipServicePort();
		
		ProcessShipmentRequest masterRequest = buildRequestMPS(pkcount, "1"); // Build a masterRequest object
		ProcessShipmentReply masterReply = port.processShipment(masterRequest); // This is the call to the ship web service passing in a request object and returning a reply object
		
		boolean create = true;
		
		if (isResponseOk(masterReply.getHighestSeverity())) // check if the call was successful
		{
			if(masterReply.getHighestSeverity().equals(NotificationSeverityType.ERROR))
			{
				printNotifications(masterReply.getNotifications());
			}
			
			ArrayList<ProcessShipmentReply> list = new ArrayList<ProcessShipmentReply>();
			
			for (int i = 1; i < pkcount; i++) 
			{
				create = false;
				
				ProcessShipmentRequest childRequest = buildRequestMPS(pkcount,String.valueOf(i+1));
				//change values for child request appropriately
				childRequest.getTransactionDetail().setCustomerTransactionId("java sample - Domestic MPS Express Ship Request - Child");
				childRequest.getRequestedShipment().setMasterTrackingId(new TrackingId());
				String trkNum = masterReply.getCompletedShipmentDetail().getCompletedPackageDetails()[0].getTrackingIds()[0].getTrackingNumber();
				childRequest.getRequestedShipment().getMasterTrackingId().setTrackingNumber(trkNum);
				
				service = new ShipServiceLocator();
				updateEndPoint(service);
				port = service.getShipServicePort();
			    //
				ProcessShipmentReply childReply = port.processShipment(childRequest); // This is the call to the ship web service passing in a request object and returning a reply object
				if (isResponseOk(childReply.getHighestSeverity())) // check if the call was successful
				{
					list.add(childReply);//成功的子单加入集合
					create = true;
				}
				else
				{
					create = false;
					break;
				}
			}
			
			if(create)
			{
				writeServiceOutput(masterReply, "1");
				
				for (int i = 0; i < list.size(); i++) 
				{
					writeServiceOutput(list.get(i),String.valueOf(i+2));
					
					printNotifications(list.get(i).getNotifications());
				}
				printNotifications(masterReply.getNotifications());
			}
			
			
		}
		
		
	}
	
	
	private void writeServiceOutput(ProcessShipmentReply reply,String sequceNumber) throws Exception
	{
		try
		{
			CompletedShipmentDetail csd = reply.getCompletedShipmentDetail(); 
			
			
			ShipmentRating sr = csd.getShipmentRating();
			if(sr!=null)
			{
				ShipmentRateDetail[] srd = sr.getShipmentRateDetails();
				for (int i = 0; i < srd.length; i++) 
				{
					BigDecimal bc = srd[i].getTotalBaseCharge().getAmount();
					BigDecimal fd = srd[i].getTotalFreightDiscounts().getAmount();
					BigDecimal fsp = srd[i].getFuelSurchargePercent().divide(new BigDecimal(100));
					fsp = fsp.add(new BigDecimal(1));
					bc = bc.subtract(fd);
					bc = bc.multiply(fsp);
					
					this.setShippmentRate(MoneyUtil.round(bc.doubleValue(),2));
				}
			}
			
			String masterTrackingNumber=printMasterTrackingNumber(csd);//单据的主单TrackingNumber，此次一票多件未使用该方式，使用内部定义好的waybill_id做得处理
			
			printShipmentOperationalDetails(csd.getOperationalDetail());
			printShipmentRating(csd.getShipmentRating());
			CompletedPackageDetail cpd[] = csd.getCompletedPackageDetails();
			printPackageDetails(cpd,sequceNumber);
			
			saveShipmentDocumentsToFile(csd.getShipmentDocuments(), masterTrackingNumber);
			//  If Express COD shipment is requested, the COD return label is returned as an Associated Shipment.
//			getAssociatedShipmentLabels(csd.getAssociatedShipments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			//
		}
	}
	
	private void printPackageDetails(CompletedPackageDetail[] cpd,String sequceNumber) throws Exception{
		if(cpd!=null)
		{
			//system.out.println("Package Details");
			for (int i=0; i < cpd.length; i++) { // Package details / Rating information for each package
				
				//system.out.println(cpd[i].getOperationalDetail().getOperationalInstructions(2).getContent().trim());
				
				String trackingNumber = cpd[i].getTrackingIds()[0].getTrackingNumber();
				
				printTrackingNumbers(cpd[i]);
				//system.out.println();
				//
				printPackageRating(cpd[i].getPackageRating());
				//	Write label buffer to file
				ShippingDocument sd = cpd[i].getLabel();
				saveLabelToFile(sd, trackingNumber,sequceNumber);
				
				savePackDocument(cpd[i].getPackageDocuments(), trackingNumber);
				printPackageOperationalDetails(cpd[i].getOperationalDetail());
				// If Ground COD shipment is requested, the COD return label is returned as in CodReturnPackageDetail.
//				printGroundCodLabel(cpd[i], trackingNumber);
				//system.out.println();
			}
		}
	}
	
	//Package level reply information
	private void printPackageOperationalDetails(PackageOperationalDetail packageOperationalDetail){
		if(packageOperationalDetail!=null){
			//system.out.println("  Routing Details");
			printString(packageOperationalDetail.getAstraHandlingText(), "Astra", "    ");
			printString(packageOperationalDetail.getGroundServiceCode(), "Ground Service Code", "    ");
			//system.out.println();
		}
	}
	
	private void printPackageRating(PackageRating packageRating){
		if(packageRating!=null){
			//system.out.println("Package Rate Details");
			PackageRateDetail[] prd = packageRating.getPackageRateDetails();
			for(int j=0; j < prd.length; j++)
			{
				//system.out.println("  Rate Type: " + prd[j].getRateType().getValue());
				printWeight(prd[j].getBillingWeight(), "Billing Weight", "    ");
				printMoney(prd[j].getBaseCharge(), "Base Charge", "    ");
				printMoney(prd[j].getNetCharge(), "Net Charge", "    ");
				printMoney(prd[j].getTotalSurcharges(), "Total Surcharge", "    ");
				if (null != prd[j].getSurcharges())
				{
					//system.out.println("    Surcharge Details");
					Surcharge[] s = prd[j].getSurcharges();
					for(int k=0; k < s.length; k++)
					{
						printMoney(s[k].getAmount(),s[k].getSurchargeType().getValue(), "      ");
					}
				}
				//system.out.println();
			}
		}
	}
	
	private void printTrackingNumbers(CompletedPackageDetail completedPackageDetail){
		if(completedPackageDetail.getTrackingIds()!=null){
			TrackingId[] trackingId = completedPackageDetail.getTrackingIds();
			for(int i=0; i< trackingId.length; i++){
				String trackNumber = trackingId[i].getTrackingNumber();
				String trackType = trackingId[i].getTrackingIdType().getValue();
				String formId = trackingId[i].getFormId();
				printString(trackNumber, trackType + " tracking number", "  ");
				printString(formId, "Form Id", "  ");
			}
		}
	}
	
	private void printShipmentRating(ShipmentRating shipmentRating){
		if(shipmentRating!=null){
			//system.out.println("Shipment Rate Details");
			ShipmentRateDetail[] srd = shipmentRating.getShipmentRateDetails();
			for(int j=0; j < srd.length; j++)
			{
				//system.out.println("  Rate Type: " + srd[j].getRateType().getValue());
				printWeight(srd[j].getTotalBillingWeight(), "Shipment Billing Weight", "    ");
				printMoney(srd[j].getTotalBaseCharge(), "Shipment Base Charge", "    ");
				printMoney(srd[j].getTotalNetCharge(), "Shipment Net Charge", "    ");
				printMoney(srd[j].getTotalSurcharges(), "Shipment Total Surcharge", "    ");
				if (null != srd[j].getSurcharges())
				{
					//system.out.println("    Surcharge Details");
					Surcharge[] s = srd[j].getSurcharges();
					for(int k=0; k < s.length; k++)
					{
						printMoney(s[k].getAmount(),s[k].getSurchargeType().getValue(), "      ");
					}
				}
				printFreightDetail(srd[j].getFreightRateDetail());
				//system.out.println();
			}
		}
	}
	
	private  void printFreightDetail(FreightRateDetail freightRateDetail){
		if(freightRateDetail!=null){
			//system.out.println("  Freight Details");
			printFreightNotations(freightRateDetail);
			printFreightBaseCharges(freightRateDetail);
			
		}
	}
	
	private  void printFreightNotations(FreightRateDetail frd){
		if(null != frd.getNotations()){
			//system.out.println("    Notations");
			FreightRateNotation notations[] = frd.getNotations();
			for(int n=0; n< notations.length; n++){
				printString(notations[n].getCode(), "Code", "      ");
				printString(notations[n].getDescription(), "Notification", "      ");
			}
		}
	}
	
	private  void printFreightBaseCharges(FreightRateDetail frd){
		if(null != frd.getBaseCharges()){
			FreightBaseCharge baseCharges[] = frd.getBaseCharges();
			for(int i=0; i < baseCharges.length; i++){
				//system.out.println("    Freight Rate Details");
				printString(baseCharges[i].getDescription(), "Description", "      ");
				printString(baseCharges[i].getFreightClass().getValue(), "Freight Class", "      ");
				printString(baseCharges[i].getRatedAsClass().getValue(), "Rated Class", "      ");
				printWeight(baseCharges[i].getWeight(), "Weight", "      ");
				printString(baseCharges[i].getChargeBasis().getValue(), "Charge Basis", "      ");
				printMoney(baseCharges[i].getChargeRate(), "Charge Rate", "      ");
				printMoney(baseCharges[i].getExtendedAmount(), "Extended Amount", "      ");
				printString(baseCharges[i].getNmfcCode(), "NMFC Code", "      ");
			}
		}
	}
	
	private  void printMoney(Money money, String description, String space){
		if(money!=null){
			//system.out.println(space + description + ": " + money.getAmount() + " " + money.getCurrency());
		}
	}
	private  void printWeight(Weight weight, String description, String space){
		if(weight!=null){
			//system.out.println(space + description + ": " + weight.getValue() + " " + weight.getUnits());
		}
	}
	
	private String printMasterTrackingNumber(CompletedShipmentDetail csd){
		String trackingNumber="";
		if(null != csd.getMasterTrackingId()){
			trackingNumber = csd.getMasterTrackingId().getTrackingNumber();
			//system.out.println("Master Tracking Number");
			//system.out.println("  Type: "
					//+ csd.getMasterTrackingId().getTrackingIdType());
			//system.out.println("  Tracking Number: " 
					//+ trackingNumber);
		}
		return trackingNumber;
	}
	
	//Shipment level reply information
	private void printShipmentOperationalDetails(ShipmentOperationalDetail shipmentOperationalDetail){
		if(shipmentOperationalDetail!=null){
			//system.out.println("Routing Details");
			printString(shipmentOperationalDetail.getUrsaPrefixCode(), "URSA Prefix", "  ");
			if(shipmentOperationalDetail.getCommitDay()!=null)
				printString(shipmentOperationalDetail.getCommitDay().getValue(), "Service Commitment", "  ");
			printString(shipmentOperationalDetail.getAirportId(), "Airport Id", "  ");
			if(shipmentOperationalDetail.getDeliveryDay()!=null)
				printString(shipmentOperationalDetail.getDeliveryDay().getValue(), "Delivery Day", "  ");
			//system.out.println();
		}
	}
	
	
	private  ProcessShipmentRequest buildRequest()
	{
		ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        //
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setCustomerTransactionId("java sample - International Express Shipment"); // The client will get the same value back in the response
        request.setTransactionDetail(transactionDetail);

        //
        VersionId versionId = new VersionId("ship",12,0, 0);
        request.setVersion(versionId);

        //
        RequestedShipment requestedShipment = new RequestedShipment();
        requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
        requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP);
        requestedShipment.setServiceType(ServiceType.INTERNATIONAL_PRIORITY); // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
        requestedShipment.setPackagingType(PackagingType.YOUR_PACKAGING); // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
        //
        requestedShipment.setShipper(addShipper()); // Sender information
        requestedShipment.setRecipient(addRecipient());
        //
        requestedShipment.setShippingChargesPayment(addShippingChargesPayment());
        //
        requestedShipment.setCustomsClearanceDetail(addCustomsClearanceDetail());
        //
	    requestedShipment.setLabelSpecification(addLabelSpecification());
        //
        RateRequestType[] rrt= new RateRequestType[] { RateRequestType.ACCOUNT }; // Rate types requested LIST, MULTIWEIGHT, ...
        requestedShipment.setRateRequestTypes(rrt);
        requestedShipment.setEdtRequestType(EdtRequestType.NONE);
        
        requestedShipment.setPackageCount(new NonNegativeInteger("1"));
       
        //
        RequestedPackageLineItem[] rp = new RequestedPackageLineItem[] {addRequestedPackageLineItem()};
	    requestedShipment.setRequestedPackageLineItems(rp);
   
	    
	    request.setRequestedShipment(requestedShipment);
		//
	    return request;
	}
	
	private  void saveLabelToFile(ShippingDocument shippingDocument, String trackingNumber,String sequceNumber) 
		throws Exception 
	{
		if(this.waybillNo.equals(""))
		{
			this.waybillNo = trackingNumber;
		}
		else
		{
			trackingNumber = this.waybillNo;
		}
		
		ShippingDocumentPart[] sdparts = shippingDocument.getParts();
		for (int a=0; a < sdparts.length; a++) 
		{
			ShippingDocumentPart sdpart = sdparts[a];
			String labelLocation = imgPath;
			String shippingDocumentType = shippingDocument.getType().getValue();
			String labelFileName =  new String(labelLocation + "/"+shippingDocumentType+"_" + trackingNumber + "_" + a +"_"+sequceNumber+ ".png");					
			File labelFile = new File(labelFileName);
			FileOutputStream fos = new FileOutputStream( labelFile );
			fos.write(sdpart.getImage());
			fos.close();
			////system.out.println("\nlabel file name " + labelFile.getAbsolutePath());
			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + labelFile.getAbsolutePath());
		}
	}
	
	private void saveShipmentDocumentsToFile(ShippingDocument[] shippingDocument, String trackingNumber) throws Exception{
		if(shippingDocument!= null){
			for(int i=0; i < shippingDocument.length; i++){
				ShippingDocumentPart[] sdparts = shippingDocument[i].getParts();
				for (int a=0; a < sdparts.length; a++) 
				{
					ShippingDocumentPart sdpart = sdparts[a];
					String labelLocation = imgPath;
					String labelName = shippingDocument[i].getType().getValue();
					String shippingDocumentLabelFileName =  new String(labelLocation + labelName + "." + trackingNumber + "_" + a + ".png");					
					File shippingDocumentLabelFile = new File(shippingDocumentLabelFileName);
					FileOutputStream fos = new FileOutputStream( shippingDocumentLabelFile );
					fos.write(sdpart.getImage());
					fos.close();
				}
			}
		}
	}
    
	private  String getPayorAccountNumber() {
		// See if payor account number is set as system property,
		// if not default it to "XXX"
		String payorAccountNumber = System.getProperty("Payor.AccountNumber");
		if (payorAccountNumber == null) {
			payorAccountNumber = "344182191"; // Replace "XXX" with the payor account number
		}
		return payorAccountNumber;
	}

	private  boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)||
			notificationSeverityType.equals(NotificationSeverityType.ERROR)) {
			return true;
		}
 		return false;
	}
    

	private  ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
        
        //
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (accountNumber == null) {
        	accountNumber = "344182191";//"510087283";// Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "104591186"; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private  WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "vfZLYoG9Id6y8y9z";//""; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "IWwM7vmwfwUr3hpNCoCLaVODP";//"IWBGFds3SiaEvzDl6Hn3JcZHM"; // Replace "XXX" with clients password
        }
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}

	private  void printNotifications(Notification[] notifications) throws UspsResponseErrorException 
	{
		StringBuffer msgSB = new StringBuffer("");
		
		msgSB.append("Notifications:");
		if (notifications == null || notifications.length == 0) {
			msgSB.append("  No notifications returned");
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			msgSB.append("  Notification no. " + i + ": ");
			if (n == null) {
				msgSB.append("null");
				continue;
			} else {
				msgSB.append("");
			}
			NotificationSeverityType nst = n.getSeverity();

			msgSB.append("    Severity: " + (nst == null ? "null" : nst.getValue()));
			msgSB.append("    Code: " + n.getCode());
			msgSB.append("    Message: " + n.getMessage());
			msgSB.append("    Source: " + n.getSource());
		}
		
		String msg = msgSB.toString();
		
		if (msg.toLowerCase().indexOf("error")>=0)
		{
			//system.out.println(msg);
			throw new UspsResponseErrorException(msg);
		}
	}
	
	private  void updateEndPoint(ShipServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setShipServicePortEndpointAddress(endPoint);
		}
	}
	
	private Party addShipper()
	{
		
		Party shipper = new Party();
		Contact contactShip = new Contact();
		contactShip.setCompanyName(this.fromName);
		contactShip.setPhoneNumber(this.fromTel);
		shipper.setContact(contactShip);        
       
       
		Address addressShip = new Address();
		addressShip.setStreetLines(new String[] { this.fromAddress });
		addressShip.setCity(this.fromCity);
		addressShip.setStateOrProvinceCode(this.fromState);
		addressShip.setPostalCode(this.fromZip);
		addressShip.setCountryCode(this.fromCountry);
		shipper.setAddress(addressShip);
		
		return shipper;
	}
	
	private Party addRecipient()
	{
		Party recipient = new Party(); // Recipient information
	      
		Contact contactRecip = new Contact();
		contactRecip.setCompanyName(this.toName);
		contactRecip.setPhoneNumber(this.toTel);
		recipient.setContact(contactRecip);
		
		Address addressRecip = new Address();
		addressRecip.setStreetLines(new String[] { this.toAddress });
		addressRecip.setCity(this.toCity);
		addressRecip.setStateOrProvinceCode(this.toState);
		addressRecip.setPostalCode(this.toZip);
		addressRecip.setCountryCode(this.toCountry);
      
		recipient.setAddress(addressRecip);
		
        return recipient;
	}
	
	private Payment addShippingChargesPayment()
	{
		
		Payment shippingChargesPayment = new Payment(); // Payment information
		shippingChargesPayment.setPaymentType(PaymentType.SENDER);
		
		
		 
		 
		 Party responsibleParty = new Party();
		 responsibleParty.setAccountNumber(getPayorAccountNumber());
		 
		 Address responsiblePartyAddress = new Address();
		 responsiblePartyAddress.setCountryCode("CN");
		 
		 responsibleParty.setAddress(responsiblePartyAddress);
		 responsibleParty.setContact(new Contact());
		 
		 Payor payor = new Payor();
		 payor.setResponsibleParty(responsibleParty);
		 shippingChargesPayment.setPayor(payor);
		
		
		
//		shippingChargesPayment.setPayor(new Payor());
//		shippingChargesPayment.getPayor().setAccountNumber(getPayorAccountNumber());
//		shippingChargesPayment.getPayor().setCountryCode("CN");
		 
	    
	    return shippingChargesPayment;
	}
	
	private CustomsClearanceDetail addCustomsClearanceDetail(){
        CustomsClearanceDetail customs = new CustomsClearanceDetail (); // International details
        customs.setDutiesPayment(addDutiesPayment());
        customs.setCustomsValue(addMoney(this.currency,this.customsValue));
        customs.setDocumentContent(InternationalDocumentContentType.NON_DOCUMENTS); 
        
        // Set export detail - To be used for Shipments that fall under AES Compliance
        //ExportDetail exportDetail = new ExportDetail();
        //exportDetail.setExportComplianceStatement("AESX20091127123456");
        //intd.setExportDetail(exportDetail);
        customs.setCommodities(addCommoditys());// Commodity details
        
        return customs;
	}
	
	private  Payment addDutiesPayment()
	{
		Payment dutiesPayment = new Payment(); // Payment information
		if(this.dutiesPayment.toUpperCase().equals(PaymentType._RECIPIENT))
		{
			dutiesPayment.setPaymentType(PaymentType.RECIPIENT);
		}
		else if(this.dutiesPayment.toUpperCase().equals(PaymentType._SENDER))
		{
			dutiesPayment.setPaymentType(PaymentType.SENDER);
			
			 Payor payor = new Payor();
			 Party responsibleParty = new Party();
			 responsibleParty.setAccountNumber(getPayorAccountNumber());
			 
			 Address responsiblePartyAddress = new Address();
			 responsiblePartyAddress.setCountryCode("CN");
			 responsibleParty.setAddress(responsiblePartyAddress);
			 responsibleParty.setContact(new Contact());
			 payor.setResponsibleParty(responsibleParty);
			 dutiesPayment.setPayor(payor);
		}
	    return dutiesPayment;
	}
	
	private Money addMoney(String currency, Double value)
	{
		Money money = new Money();
		money.setCurrency(currency);
		money.setAmount(new BigDecimal(value));
		return money;
	}
	
	private LabelSpecification addLabelSpecification()
	{
		 LabelSpecification labelSpecification = new LabelSpecification(); // Label specification
		 labelSpecification.setImageType(ShippingDocumentImageType.PNG);// Image types PDF, PNG, DPL, ...
		 labelSpecification.setLabelFormatType(LabelFormatType.COMMON2D);
		 labelSpecification.setLabelStockType(LabelStockType.value1); // STOCK_4X6
		    
	    return labelSpecification;
	}
	
	private RequestedPackageLineItem addRequestedPackageLineItem()
	{
		RequestedPackageLineItem requestedPackageLineItem = new RequestedPackageLineItem();
		requestedPackageLineItem.setSequenceNumber(new PositiveInteger("1"));
//		requestedPackageLineItem.setGroupPackageCount(new PositiveInteger("1"));
		requestedPackageLineItem.setWeight(addPackageWeight(this.weightLB, WeightUnits.KG));
		
//		requestedPackageLineItem.setDimensions(addPackageDimensions(108, 5, 5, LinearUnits.IN));
//		
//		requestedPackageLineItem.setCustomerReferences(new CustomerReference[]{
//				addCustomerReference(CustomerReferenceType.CUSTOMER_REFERENCE.getValue(), "CR1234"),
//				addCustomerReference(CustomerReferenceType.INVOICE_NUMBER.getValue(), "IV1234"),
//				addCustomerReference(CustomerReferenceType.P_O_NUMBER.getValue(), "PO1234"),
//		});
		return requestedPackageLineItem;
	}
	
	private RequestedPackageLineItem addRequestedPackageLineItemMPS(String sequenceNumber,int packcount)
	{
		RequestedPackageLineItem requestedPackageLineItem = new RequestedPackageLineItem();
		requestedPackageLineItem.setSequenceNumber(new PositiveInteger(sequenceNumber));
		if(sequenceNumber.equals("1"))
		{
			requestedPackageLineItem.setGroupPackageCount(new PositiveInteger("1"));
		}
//		
		
		requestedPackageLineItem.setWeight(addPackageWeight(this.weightLB/packcount, WeightUnits.KG));
		
//		requestedPackageLineItem.setDimensions(addPackageDimensions(108, 5, 5, LinearUnits.IN));
//		
		requestedPackageLineItem.setCustomerReferences(new CustomerReference[]{
				addCustomerReference(CustomerReferenceType.CUSTOMER_REFERENCE.getValue(),this.note),
				addCustomerReference(CustomerReferenceType.INVOICE_NUMBER.getValue(),this.oids.substring(0,this.oids.length()-1)),
				addCustomerReference(CustomerReferenceType.DEPARTMENT_NUMBER.getValue(),this.creater)
		});
		return requestedPackageLineItem;
	}
	
	private CustomerReference addCustomerReference(String customerReferenceType, String customerReferenceValue){
		CustomerReference customerReference = new CustomerReference();
		customerReference.setCustomerReferenceType(CustomerReferenceType.fromString(customerReferenceType));
		customerReference.setValue(customerReferenceValue);
		return customerReference;
	}
	
	private  void savePackDocument(ShippingDocument[] shippingDocument, String trackingNumber) 
		throws Exception 
	{
		if(shippingDocument!=null&&shippingDocument.length>0)
		{
			for (int i = 0; i < shippingDocument.length; i++)
			{
				ShippingDocumentPart[] sdparts = shippingDocument[i].getParts();
				for (int a=0; a < sdparts.length; a++) 
				{
					ShippingDocumentPart sdpart = sdparts[a];
					String labelLocation = System.getProperty("file.label.location");
					if (labelLocation == null) 
					{
						labelLocation = imgPath;
					}
					String shippingDocumentType = shippingDocument[i].getType().getValue();
					
					if(shippingDocumentType.equals("TERMS_AND_CONDITIONS"))
					{
						continue;
					}
					else
					{
						String labelFileName =  new String(labelLocation +"/"+ shippingDocumentType + "_" + trackingNumber + "_" + i +"_"+a+".png");					
						File labelFile = new File(labelFileName);
						FileOutputStream fos = new FileOutputStream( labelFile );
						fos.write(sdpart.getImage());
						fos.close();
					}
					
				}
			}
		}
	}
	
	private static Weight addPackageWeight(Double packageWeight, WeightUnits weightUnits){
		Weight weight = new Weight();
		weight.setUnits(weightUnits);
		weight.setValue(new BigDecimal(packageWeight));
		return weight;
	}
	
	private Commodity[] addCommoditys()
	{
		ArrayList<Commodity> list = new ArrayList<Commodity>();
		
		for (int i = 0; i < this.invoiceProducts.length; i++) 
		{
			Commodity commodity = new Commodity();
			commodity.setNumberOfPieces(new NonNegativeInteger("1"));
			commodity.setDescription(this.description);
			commodity.setWeight(new Weight());
			commodity.getWeight().setValue(new BigDecimal((int)(invoiceProducts[i].get("weight",0f)*this.privilege)));
			commodity.getWeight().setUnits(WeightUnits.KG);
			
			commodity.setQuantity(new NonNegativeInteger(String.valueOf((int)MoneyUtil.round(invoiceProducts[i].get("quantity",0f),0))));
			commodity.setQuantityUnits(invoiceProducts[i].getString("unit_name"));
			commodity.setUnitPrice(new Money());
			commodity.getUnitPrice().setAmount(new BigDecimal(invoiceProducts[i].get("unit_price",0d)));
			commodity.getUnitPrice().setCurrency(this.currency);
			commodity.setCustomsValue(new Money());
			commodity.getCustomsValue().setAmount(new BigDecimal(invoiceProducts[i].get("customValue",0d)));
			commodity.getCustomsValue().setCurrency(this.currency);
			commodity.setCountryOfManufacture(this.countryOfManufacture);
			commodity.setHarmonizedCode(this.hscode);
			
			list.add(commodity);
		}
        return list.toArray(new Commodity[0]);
	}
	
	private void printString(String value, String description, String space){
		if(value!=null){
			//system.out.println(space + description + ": " + value);
		}
	}
	
	public void setFromName(String fromName)
	{
		this.fromName = fromName;
	}
	public void setFromAddress(String fromAddress)
	{
		this.fromAddress = fromAddress;
	}
	public void setFromCity(String fromCity)
	{
		this.fromCity = fromCity;
	}
	public void setFromState(String fromState)
	{
		this.fromState = fromState;
	}
	public void setFromZip(String fromZip)
	{
		this.fromZip = fromZip;
	}
	public void setFromTel(String fromTel)
	{
		this.fromTel = fromTel;
	}
	
	
	public void setToName(String toName)
	{
		this.toName = toName;
	}
	public void setToAddress(String toAddress)
	{
		this.toAddress = toAddress;
	}
	public void setToCity(String toCity)
	{
		this.toCity = toCity;
	}
	public void setToState(String toState)
	{
		this.toState = toState;
	}
	public void setToZip(String toZip)
	{
		this.toZip = toZip;
	}
	public void setToTel(String toTel)
	{
		this.toTel = toTel;
	}
	public void setWeightLB(double weightLB)
	{
		this.weightLB = weightLB;
	}
	public String getWaybillNo()
	{
		return waybillNo;
	}



	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}



	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}



	public void setCustomsValue(double customsValue) {
		this.customsValue = customsValue;
	}
	
	private ProcessShipmentRequest buildRequestMPS(int pkgCount, String sequenceNumber)
	{
		ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        
        
        // 
	    TransactionDetail transactionDetail = new TransactionDetail();
	    String masterOrChild = (sequenceNumber.equals("1") ? "Master" : "Child");
	    transactionDetail.setCustomerTransactionId("java sample - Domestic MPS Express Ship Request - " + masterOrChild); // The client will get the same value back in the response
	    request.setTransactionDetail(transactionDetail);
	    //
        VersionId versionId = new VersionId("ship", 12, 0, 0);
        request.setVersion(versionId);
        //
	    RequestedShipment requestedShipment = new RequestedShipment();
	    requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
	    requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP); // Dropoff Types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
	    requestedShipment.setServiceType(ServiceType.INTERNATIONAL_PRIORITY); // Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND ...
	    requestedShipment.setPackagingType(PackagingType.YOUR_PACKAGING); // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	    requestedShipment.setShipper(addShipper());
	    requestedShipment.setRecipient(addRecipient());
	    requestedShipment.setShippingChargesPayment(addShippingChargesPayment());     
	    //Example Shipment special service (Express COD).
//	    requestedShipment.setSpecialServicesRequested(addShipmentSpecialServicesRequested()); 
	    
	    requestedShipment.setCustomsClearanceDetail(addCustomsClearanceDetail());
	    
	    requestedShipment.setTotalWeight(addPackageWeight(MoneyUtil.round(this.weightLB,2),WeightUnits.KG));
	    //
	    if(sequenceNumber.equals("1"))
	    {
	    	requestedShipment.setRequestedPackageLineItems(new RequestedPackageLineItem[]{addRequestedPackageLineItemMPS("1",pkgCount)});
	    }
	    else
	    {
	    	RequestedPackageLineItem childPackageLineItem = addRequestedPackageLineItemMPS(sequenceNumber,pkgCount);
//	    	childPackageLineItem.setSequenceNumber(new PositiveInteger("2"));
//	    	childPackageLineItem.setWeight(addPackageWeight(this.weightLB/2, WeightUnits.LB));
//	    	childPackageLineItem.setDimensions(addPackageDimensions(10, 10, 5, LinearUnits.IN));
	    	requestedShipment.setRequestedPackageLineItems(new RequestedPackageLineItem[]{childPackageLineItem});
	    }    
	    requestedShipment.setLabelSpecification(addLabelSpecification());
        //
	    RateRequestType rateRequestType[] = new RateRequestType[1];
	    rateRequestType[0] = RateRequestType.ACCOUNT; // Rate types requested LIST, MULTIWEIGHT, ...
	    requestedShipment.setRateRequestTypes(rateRequestType);
	    requestedShipment.setPackageCount(new NonNegativeInteger(String.valueOf(pkgCount)));
        //
	    request.setRequestedShipment(requestedShipment);
		//
	    return request;
	}

	private  ShipmentSpecialServicesRequested addShipmentSpecialServicesRequested()
	{
	    ShipmentSpecialServicesRequested shipmentSpecialServicesRequested = new ShipmentSpecialServicesRequested();
	    ShipmentSpecialServiceType shipmentSpecialServiceType[]=new ShipmentSpecialServiceType[1];
	    shipmentSpecialServiceType[0]=ShipmentSpecialServiceType.COD;
	    shipmentSpecialServicesRequested.setSpecialServiceTypes(shipmentSpecialServiceType);
	    CodDetail codDetail = new CodDetail();
	    codDetail.setCollectionType(CodCollectionType.ANY);
	    Money codMoney = new Money();
	    codMoney.setCurrency("USD");
	    codMoney.setAmount(new BigDecimal(this.customsValue/2));
	    codDetail.setCodCollectionAmount(codMoney);
	    shipmentSpecialServicesRequested.setCodDetail(codDetail);
	    return shipmentSpecialServicesRequested;
	}
	

	public void setInvoiceProducts(DBRow[] invoiceProducts,double CustomsValue,double allPrice) 
	{
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < invoiceProducts.length; i++) 
		{
			double productPrice = invoiceProducts[i].get("unit_price",0d)*invoiceProducts[i].get("cart_quantity",0f);
			
			double customPercentageValue = productPrice/allPrice*customsValue;
			
			DBRow productInvoice = new DBRow();
			productInvoice.add("weight",invoiceProducts[i].get("weight",0f));
			productInvoice.add("quantity",invoiceProducts[i].get("cart_quantity",0f));
			productInvoice.add("unit_name",invoiceProducts[i].getString("unit_name"));
			productInvoice.add("unit_price",MoneyUtil.round(customPercentageValue,2));
			productInvoice.add("customValue",customPercentageValue);
			
			list.add(productInvoice);
		}
		this.invoiceProducts = list.toArray(new DBRow[0]);
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public void setPrivilege(float privilege) {
		this.privilege = privilege;
	}



	public void setCountryOfManufacture(String countryOfManufacture) {
		this.countryOfManufacture = countryOfManufacture;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public void setHscode(String hscode) {
		this.hscode = hscode;
	}


	public void setDutiesPayment(String dutiesPayment) {
		this.dutiesPayment = dutiesPayment;
	}


	public void setCreater(String creater) {
		this.creater = creater;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public void setOids(String oids) {
		this.oids = oids;
	}



	public double getBaseCharge() {
		return baseCharge;
	}



	public void setBaseCharge(double baseCharge) {
		this.baseCharge = baseCharge;
	}



	public double getFuelSurchargePercent() {
		return fuelSurchargePercent;
	}



	public void setFuelSurchargePercent(double fuelSurchargePercent) {
		this.fuelSurchargePercent = fuelSurchargePercent;
	}



	public double getFreightDiscounts() {
		return freightDiscounts;
	}



	public void setFreightDiscounts(double freightDiscounts) {
		this.freightDiscounts = freightDiscounts;
	}



	public double getShippmentRate() {
		return shippmentRate;
	}



	public void setShippmentRate(double shippmentRate) {
		this.shippmentRate = shippmentRate;
	}
	
	
}