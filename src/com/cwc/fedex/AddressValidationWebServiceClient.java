package com.cwc.fedex;

import java.util.Calendar;
import org.apache.axis.types.PositiveInteger;

import com.fedex.addressvalidation.stub.*;


/** 
 * Sample code to call the FedEx Address validation Web Service
 * <p>
 * com.fedex.addressvalidation.stub is generated via WSDL2Java, like this:<br>
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.addressvalidation.stub http://www.fedex.com/...../AddressValidationService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
public class AddressValidationWebServiceClient {
	
	public static String BUSINESS = "BUSINESS";
	public static String RESIDENTIAL = "RESIDENTIAL";
	public static String UNDETERMINED = "UNDETERMINED";
	public static String UNAVAILABLE = "UNAVAILABLE";
	public static String NOT_APPLICABLE_TO_COUNTRY = "NOT_APPLICABLE_TO_COUNTRY";
	public static String INSUFFICIENT = "INSUFFICIENT";
	
	public static void main(String[] args) throws Exception
	{ 
		/**
		
	    //
		AddressValidationRequest request = new AddressValidationRequest();
        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        //
        VersionId versionId = new VersionId("aval", 2, 0, 0);
        request.setVersion(versionId);
	    //
	    TransactionDetail transactionDetail = new TransactionDetail();
	    transactionDetail.setCustomerTransactionId("java sample - AddressValidationRequest"); //This is a reference field for the customer.  Any value can be used and will be provided in the return.
	    request.setTransactionDetail(transactionDetail);
        //
	    Calendar c = Calendar.getInstance();
	    request.setRequestTimestamp(c);
        //
	    AddressValidationOptions vo = new AddressValidationOptions();
	    vo.setMaximumNumberOfMatches(new PositiveInteger("5"));
	    vo.setStreetAccuracy(AddressValidationAccuracyType.LOOSE);
	    vo.setDirectionalAccuracy(AddressValidationAccuracyType.LOOSE);
	    vo.setCompanyNameAccuracy(AddressValidationAccuracyType.LOOSE);
	    //vo.setConvertToUpperCase(new Boolean(true));
	    //vo.setRecognizeAlternateCityNames(new Boolean(true));
	    vo.setReturnParsedElements(new Boolean(true));
	    vo.setCheckResidentialStatus(new Boolean(true));
	    request.setOptions(vo);
	    //
	    AddressToValidate av1 = new AddressToValidate();
	    Address a1 = new Address();
	    a1.setStreetLines(new String[] {" 6703 NW 7th St # SJO-21976  "});
	    a1.setPostalCode("33126     ");
	    
	    av1.setAddress(a1);
	    av1.setCompanyName("FedEx Services");
	    //av1.setAddressId("WTC");
	    
//	    AddressToValidate av2 = new AddressToValidate();
//	    Address a2 = new Address();
//	    a2.setStreetLines(new String[] {"50 N Front St"});
//	    a2.setPostalCode("38103");
//	    av2.setAddress(a2);
//	    av2.setCompanyName("FedEx Office");
//	    av2.setAddressId("FedEx_Office");
//	    request.setAddressesToValidate(new AddressToValidate[] {av1, av2});
	    
	    request.setAddressesToValidate(new AddressToValidate[] {av1});
	    
	    //
		try {
			// Initialize the service
			AddressValidationServiceLocator service;
			AddressValidationPortType port;
			//	
			service = new AddressValidationServiceLocator();
			updateEndPoint(service);
			port = service.getAddressValidationServicePort();	
			// This is the call to the web service
			AddressValidationReply reply = port.addressValidation(request);
			VersionId vid = reply.getVersion();
//			//system.out.println("version major: " + vid.getMajor());
//			//system.out.println("version intermediate: " + vid.getIntermediate());
//			//system.out.println("version minor: "+ vid.getMinor());
			//
			if (isResponseOk(reply.getHighestSeverity()))
			{
				AddressValidationResult[] avr = reply.getAddressResults();
				for(int i = 0; i < avr.length; i++)
				{
					////system.out.println("Address Id - " + avr[i].getAddressId());
					////system.out.println("--- Proposed details ---");
					ProposedAddressDetail[] ad1 = avr[i].getProposedAddressDetails();
					for(int j = 0; j < ad1.length; j++)
					{
//						//system.out.println("Score - " + ad1[j].getScore());
//						//system.out.println("Address - " + ad1[j].getAddress().getStreetLines(0));
//						//system.out.println(ad1[j].getAddress().getStateOrProvinceCode());
//						//system.out.println(ad1[j].getAddress().getPostalCode());
//						//system.out.println(ad1[j].getAddress().getCountryCode());
//						//system.out.println("Changes - " + ad1[j].getScore());
						//system.out.println("ResidentialStatus:"+ad1[j].getResidentialStatus());
						
//						AddressValidationChangeType[] avct = ad1[j].getChanges();
//						for(int k = 0; k < avct.length; k++)
//						{
//							//system.out.println("AddressValidationChangeType:"+avct[k].toString());
//						}
					}
				}
			}
			////system.out.println("--- Notifications ---");
			//printNotifications(reply.getNotifications());

		}
		catch (Exception e)
		{
            e.printStackTrace();
		}
		
		**/
		
		AddressValidationWebServiceClient addressValidationWebServiceClient = new AddressValidationWebServiceClient();
//		//system.out.println("=> "+addressValidationWebServiceClient.getResidentialStatus("339a Stemmans Road", "70583"));
		//339 Stemmans Road", "70583"  ---------------->RESIDENTIAL 
		//339a Stemmans Road           ---------------->INSUFFICIENT_DATA
		
	}
	
	public String getResidentialStatus(String address,String zipcode,String countryCode)
		throws Exception
	{
		AddressValidationRequest request = new AddressValidationRequest();
        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        VersionId versionId = new VersionId("aval", 2, 0, 0);
        request.setVersion(versionId);
	    TransactionDetail transactionDetail = new TransactionDetail();
	    transactionDetail.setCustomerTransactionId("AddressValidationRequest"); 
	    request.setTransactionDetail(transactionDetail);
	    Calendar c = Calendar.getInstance();
	    request.setRequestTimestamp(c);
	    AddressValidationOptions vo = new AddressValidationOptions();
	    vo.setMaximumNumberOfMatches(new PositiveInteger("1"));
	    vo.setStreetAccuracy(AddressValidationAccuracyType.LOOSE);
	    vo.setDirectionalAccuracy(AddressValidationAccuracyType.LOOSE);
	    vo.setCompanyNameAccuracy(AddressValidationAccuracyType.LOOSE);
	    vo.setReturnParsedElements(new Boolean(true));
	    vo.setCheckResidentialStatus(new Boolean(true));
	    request.setOptions(vo);
	    AddressToValidate av1 = new AddressToValidate();
	    Address a1 = new Address();
	    a1.setStreetLines(new String[] {address});
	    a1.setPostalCode(zipcode);
	    a1.setCountryCode(countryCode);
	    av1.setAddress(a1);
	    request.setAddressesToValidate(new AddressToValidate[] {av1});
	    
		try 
		{
			
			AddressValidationServiceLocator service;
			AddressValidationPortType port;
			service = new AddressValidationServiceLocator();
			
			updateEndPoint(service);
			
			port = service.getAddressValidationServicePort();
			
			
			AddressValidationReply reply = port.addressValidation(request);
			
		
			VersionId vid = reply.getVersion();
	        
        
			if (isResponseOk(reply.getHighestSeverity()))
			{
				AddressValidationResult[] avr = reply.getAddressResults();
				for(int i = 0; i < avr.length; i++)
				{
					ProposedAddressDetail[] ad1 = avr[i].getProposedAddressDetails();

					for(int j = 0; j < ad1.length; j++)
					{
						return(ad1[j].getResidentialStatus().getValue());
					}
				}
			}
		}
//		catch(org.apache.axis.AxisFault e)
//		{
//			//system.out.println(e.detail);
//			//system.out.println(e.detail.getClass());
//			//system.out.println(e.detail.getClass().getName());
//			
//			if(e.detail.getClass().newInstance().equals(new java.net.UnknownHostException()));
//			{
//				throw new java.net.UnknownHostException();
//			}
//		}
		catch (Exception e) 
		{
            throw e;
		}
		
		return("");
	}
	

	private static boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}

	private static ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
        //
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (accountNumber == null) {
        	accountNumber = "128544119"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "102483532"; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private static WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "6ukdvDUaV0lc92jB"; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "IWBGFds3SiaEvzDl6Hn3JcZHM"; // Replace "XXX" with clients password
        }
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}
	
	private static void printNotifications(Notification[] notifications) {
		//system.out.println("Notifications:");
		if (notifications == null || notifications.length == 0) {
			//system.out.println("  No notifications returned");
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			//system.out.print("  Notification no. " + i + ": ");
			if (n == null) {
				//system.out.println("null");
				continue;
			} else {
				//system.out.println("");
			}
			NotificationSeverityType nst = n.getSeverity();

			//system.out.println("    Severity: " + (nst == null ? "null" : nst.getValue()));
			//system.out.println("    Code: " + n.getCode());
			//system.out.println("    Message: " + n.getMessage());
			//system.out.println("    Source: " + n.getSource());
		}
	}
	
	private static void updateEndPoint(AddressValidationServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setAddressValidationServicePortEndpointAddress(endPoint);
		}
	}
	
}
