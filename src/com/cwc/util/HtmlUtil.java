
package com.cwc.util;

import com.cwc.app.util.Handler;

/**
 * HTML处理工具包
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class HtmlUtil implements Handler
{
	/**
	 * 
	 * @param str
	 * @return
	 */
    public static final String dealForm(String str)
    {
  	    if ( str == null )
        {
            return null;
        }
  	    else
        {
  	        return str;
            //if jsp server is tomcat ,use this method;
  	        //return ISO2GBK(str);
        }

    }

    /**
     * 
     * @param linkName
     * @param linkUrl
     * @return
     */
    public static final String aLink(String linkName,String linkUrl)
    {
  	    return aLink(linkName,linkUrl,true);
    }
    
    /**
     * 获得一个带样式的链接
     * @param classn
     * @param linkName
     * @param linkUrl
     * @param title
     * @param flag
     * @return
     */
    public static final String aStyleLink(String classn,String linkName,String linkUrl,String title,boolean flag)
    {
        StringBuffer sb = new StringBuffer();

        if( flag )
        {
            sb.append("\n<a href=\""+linkUrl+"\"");
            
            if (classn!=null||!classn.equals(""))
            {
            	sb.append(" class='"+classn+"' ");
            }
        }
        else
        {
            sb.append("\n<font color=\"#AAAAAA\"");
        }

        if( title != null && title.length() > 0 )
        {
            sb.append(" title=\""+title+"\"");
        }

        sb.append(">" + linkName + "<");
        
        if( flag )
        {
            sb.append("/a>\n");
        }
        else
        {
            sb.append("/font>\n");
        }

        return sb.toString();
    }

    /**
     * 获得一个链接
     * @param linkName
     * @param linkUrl
     * @param flag
     * @return
     */
    public static final  String aLink(String linkName,
                                      String linkUrl, boolean flag)
    {
         return aLink(linkName,linkUrl,null,flag);
    }

    /**
     * 
     * @param linkName
     * @param linkUrl
     * @param linkTitle
     * @return
     */
    public static final String aLink(String linkName,
                                     String linkUrl, String linkTitle)
    {
        return aLink(linkName, linkUrl, linkTitle, true);
    }

    /**
     * 获得一个链接
     * @param linkName			链接名
     * @param linkUrl			链接URL
     * @param title				链接标题
     * @param flag				是否加链接
     * @return
     */
     public static final  String aLink(String linkName,String linkUrl,String title,boolean flag)
    {
        StringBuffer sb = new StringBuffer();

        if( flag )
        {
            sb.append("\n<a href=\""+linkUrl+"\"");
        }
        else
        {
            sb.append("\n<font color=\"#AAAAAA\"");
        }

        if( title != null && title.length() > 0 )
        {
            sb.append(" title=\""+title+"\"");
        }

        sb.append(">" + linkName + "<");
        if( flag )
        {
            sb.append("/a>\n");
        }
        else
        {
            sb.append("/font>\n");
        }

        return sb.toString();
    }

     /**
      * 
      * @param linkName
      * @param linkUrl
      * @param flag
      * @return
      */
     public static final  String bLink(String linkName,
                                      String linkUrl, boolean flag)
    {
        return bLink(linkName,linkUrl,null,flag);
    }

     /**
      * 
      * @param linkName
      * @param linkUrl
      * @param title
      * @param flag
      * @return
      */
     public static final  String bLink(String linkName,String  linkUrl,
                                      String title,boolean flag)
    {
         return bLink(linkName,linkUrl,title,null,flag);
    }
     
     /**
      * 获得一个按钮链接
      * @param linkName
      * @param linkUrl
      * @param title
      * @param style
      * @param flag
      * @return
      */
    public static final  String bLink(String linkName,String linkUrl,
                                      String title,String style,boolean flag)
    {
        StringBuffer buf = new StringBuffer();

        buf.append("\n<INPUT TYPE=\"button\" value=\"" + linkName);

        buf.append("\" onclick=\"location='"+ linkUrl +"'\"");

        if( title!=null && title.length()>0 )
        {
            buf.append(" title=\"" + title + "\"");
        }
        if( style!=null && style.length()>0 )
        {
            buf.append(" style=\"" + style + "\"");
        }
		if( flag )
        {
            buf.append(" DISABLED");
        }

        buf.append("\n");

        return buf.toString();

    }

    /**
     * 
     * @param flag
     * @return
     */
    public static final  String MULTIPLE(boolean flag)
    {
       if(flag)
       {
           return " MULTIPLE";
       }
       else
       {
           return "";
       }
    }

    /**
     * 
     * @param flag
     * @return
     */
    public static final  String READONLY(boolean flag)
    {
       if(flag)
       {
           return " READONLY";
       }
       else
       {
           return "";
       }
    }

    /**
     * 
     * @param flag
     * @return
     */
    public static final  String DISABLED(boolean flag)
    {
       if( flag )
       {
           return " DISABLED";
       }
       else
       {
           return "";
       }
    }

    /**
     * 
     * @param flag
     * @return
     */
    public static final  String SELECTED(boolean flag)
    {
       if( flag )
       {
           return " SELECTED";
       }
       else
       {
           return "";
       }
    }

    /**
     * 
     * @param flag
     * @return
     */
    public static final  String CHECKED(boolean flag)
    {
       if(flag)
       {
           return " CHECKED";
       }
       else
       {
           return "";
       }
    }

}
