 package com.cwc.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.antlr.grammar.v3.ANTLRParser.finallyClause_return;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.extractor.WordExtractor;

import seamoonotp.stringchange;

import com.cwc.app.util.Environment;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.client.ClientProtocolException;
/**
 * 文件操作类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FileUtil
{
    public static File file;
    public static Logger log = Logger.getLogger("ACTION");
    private static final String FILE_DOWNLOAD_URL = "http://xmwise.320.io:18080/shared/file-app/file-download/";
    private static final String FILE_SERVICE_URL = "/shared/file-app/file-download/";
    private static final String MODULE = "WISE";
    private static final String SERCERT_KEY = "d09adbbb-ba37-4cf1-859e-4dce55b6486a";
    

    public FileUtil()
    {

    }

    /**
     * 创建目录
     * @param fname
     * @return
     */
    public static boolean createDirectory(String fname)
    {
        try
        {
             file=new File(fname);
            if(file.exists())
            {
                throw new IOException("Directory exists!");
            }
            else
            {
                 return file.mkdir();
            }
        }
        catch(IOException e)
        {
            //system.out.println(e.getMessage());
            return false;
        }
     }

    /**
     * 创建文件
     * @param fname
     * @return
     */
    public static boolean createFile(String fname)
    {
        try
        {
            file=new File(fname);
            if(file.exists())
            {
                throw new IOException("File exists!");
            }
            else
            {
                return file.createNewFile();
            }
        }
        catch(IOException e)
        {
            //system.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * 读取属性文件
     * @param proFile
     * @return
     * @throws IOException
     */
    public static Enumeration readAllProperties(String proFile)
            throws IOException
    {
            InputStream is = new FileInputStream(new File(proFile));
            Properties dbProps = new Properties();
            try
            {
                  dbProps.load(is);
            }
            catch (Exception e)
            {
                throw new IOException("can't read file:. " + proFile );
            }

            return dbProps.propertyNames();
    }

    /**
     * 获得属性文件内某个属性
     * @param proFile
     * @param proName
     * @return
     * @throws IOException
     */
    public static String readPropertiesFile(String proFile,String proName)
                throws IOException
        {
            InputStream is = new FileInputStream(new File(proFile));
            Properties dbProps = new Properties();
            try
            {
                  dbProps.load(is);
            }
            catch (Exception e)
            {
                throw new IOException("���ܶ�ȡ�����ļ�readPropertiesFile error: " +proFile+ "��ȷ�ļ��Ƿ������ָ����·����");
            }

            if(dbProps.containsKey(proName))
            {
                return dbProps.getProperty(proName,null);
            }
            else
            {
                throw new IOException("�����ļ��в���(������"+proName);
            }
        }

    /**
     * 修改属性文件
     * @param proFile				属性文件
     * @param proName				参数名
     * @param proValue				值
     * @throws IOException
     */
        public static void  writePropertiesFile(String proFile,String proName,String proValue)
                throws IOException
        {

            InputStream is = new FileInputStream(new File(proFile));
            FileOutputStream out = new FileOutputStream(new File("./temp.pro"));

            Properties dbProps = new Properties();
            try
            {
                  dbProps.load(is);
            }
            catch (Exception e)
            {
                throw new IOException("���ܶ�ȡ�����ļ�writePropertiesFile error:" +proFile+ "��ȷ�ļ��Ƿ������ָ����·����");
            }
            if(!dbProps.containsKey(proName))
            {
                throw new IOException("�����ļ��в���(������"+proName);
            }
            else
            {
                Enumeration enum1=dbProps.propertyNames();
                String key1=null;
                while(enum1.hasMoreElements())
                {
                    key1=(String)enum1.nextElement();

                    if(key1.equals(proName))
                    {
                        dbProps.put(key1,proValue);
                    }
                    else
                    {
                        dbProps.put(key1,dbProps.getProperty(key1));
                    }
                }
            }

            dbProps.store(out,"properties file");
            is.close();
            out.close();
            File file=new File(proFile);
            File tempfile=new File("./temp.pro");
            file.deleteOnExit();
            if(file.delete())
            {
                tempfile.renameTo(file);
            }
            else
            {
                tempfile.delete();
                throw new IOException("ɾ���ļ�ʱʧ�ܣ�");
            }
        }

        /**
         * 删除文件
         * @param fileName
         */
	public static final void delFile(String fileName) 
	{				
		File f = new File(fileName);
		if (f.exists())
		{
			f.delete();
		}
		else
		{
			//log.info("DelFile msg:File "+fileName+" not found!");
		}
	}

	/**
	 * 删除文件夹
	 * @param folderName
	 * @throws Exception
	 */
	public static final void delFolder(String folderName) 
	  throws Exception
	{				
			File f = new File(folderName);
			if ( f.exists()&&f.isDirectory())
			{
				File[] allFiles = f.listFiles(); 
				for (int i=0; i<allFiles.length; i++)
				{
					if ( allFiles[i].isFile() )
					{
						////system.out.println("File:"+allFiles[i]);
						
						if ( allFiles[i].exists() )
						{
							allFiles[i].delete();
						}
					}
					else if ( allFiles[i].isDirectory() )
					{
						////system.out.println("Folder:"+allFiles[i]);
						
						delFolder(allFiles[i].toString());
					}
				}

				f.delete();
			}
	}
	
	/**
	 * 清空文件夹（包括子目录和所有文件）
	 * @param folderName
	 * @throws Exception
	 */
	public static final void cleanFolder(String folderName) 
	  throws Exception
	{				
			File f = new File(folderName);
			if ( f.exists()&&f.isDirectory())
			{
				File[] allFiles = f.listFiles(); 
				for (int i=0; i<allFiles.length; i++)
				{
					if ( allFiles[i].isFile() )
					{
						if ( allFiles[i].exists() )
						{
							allFiles[i].delete();
						}
					}
					else if ( allFiles[i].isDirectory() )
					{
						delFolder(allFiles[i].toString());
					}
				}
			}
	}
	
	/**
	 * 读取文件返回字符串内容
	 * @param FileName
	 * @return
	 * @throws Exception
	 */
	public static final String readFile2String(String FileName)
		throws Exception 
	{
		StringBuffer sb = new StringBuffer("");
		String fileStr="";
		
		try {
			BufferedReader file = new BufferedReader(new FileReader(FileName));
			while ( (fileStr=file.readLine())!=null )
			{
				sb.append(fileStr);
				sb.append("\n");
			}
			file.close();
		}
		catch (IOException e)
		{
			throw new Exception("Read file error:" + e.getMessage());
		}
	
		return(sb.toString());
	}
	
	/**
	 * 读取文件返回字符串内容（可设置编码）
	 * @param FileName
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static final String readFile2String(String FileName,String code)
		throws Exception 
	{
		StringBuffer sb = new StringBuffer("");
		String fileStr="";
		
		try 
		{
			File f = new File(FileName);
			InputStreamReader read = new InputStreamReader (new FileInputStream(f),code);
			BufferedReader file = new BufferedReader(read);
			while ( (fileStr=file.readLine())!=null )
			{
				sb.append(fileStr);
				sb.append("\n\r");
			}
			read.close();
			
			file.close();
			f = null;
		}
		catch (IOException e)
		{
			throw new Exception("Read file error:" + e.getMessage());
		}
	
		return(sb.toString());
	}
	
	/**
	 * 读取文件，返回数组内容
	 * @param FileName
	 * @return
	 * @throws Exception
	 */
	public static final String[] readFile2Array(String FileName)
		throws Exception 
	{
		ArrayList strAl = new ArrayList();
		String fileStr = null;
		BufferedReader file = null;
		
		try
		{
			/**
			File f = new File(FileName);
			InputStreamReader read = new InputStreamReader (new FileInputStream(f),code);
			BufferedReader file = new BufferedReader(read);
			while ( (fileStr=file.readLine())!=null )
			**/
			
			file = new BufferedReader(new FileReader(FileName));
			while ( (fileStr=file.readLine())!=null )
			{
				strAl.add(fileStr);
			}
		}
		catch (IOException e)
		{
			throw new Exception("Read file error:" + e.getMessage());
		}
		finally
		{
			file.close();
		}
	
		return((String[])strAl.toArray(new String[0]));
	}
	
	/**
	 * 读取文件，返回数组内容（可设置编码）
	 * @param FileName
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static final String[] readFile2Array(String FileName,String code)
		throws Exception 
	{
		ArrayList strAl = new ArrayList();
		String fileStr = null;
		BufferedReader file = null;
		
		try
		{
			File f = new File(FileName);
			InputStreamReader read = new InputStreamReader (new FileInputStream(f),code);
			
			file = new BufferedReader(read);
			while ( (fileStr=file.readLine())!=null )
			{
				if (!fileStr.trim().equals(""))
				{
					strAl.add(fileStr);					
				}
			}
			
			read.close();
		}
		catch (IOException e)
		{
			throw new Exception("Read file error:" + e.getMessage());
		}
		finally
		{
			file.close();
		}
	
		return((String[])strAl.toArray(new String[0]));
	}

	/**
	 * 读取一个文件夹，返回文件夹下所有内容
	 * @param folder	
	 * @return		数组
	 */
	public static final String[] getFileList(String folder)
	{	
		File f = new File(folder);
		String arrayFiles[] = f.list();
		return(arrayFiles);
	}
	
	/**
	 * 文件改名
	 * @param file
	 * @param newName
	 */
	public static final void reName(String file,String newName)
	{				
		File f = new File(file);
		if ( f.exists() )
		{
			f.renameTo(new File(newName));
		}
	}
	
	/**
	 * 获得一个文件夹下所有文件
	 * @param filePath
	 * @return			数组
	 */
	public static final String[] readFolder(String filePath) 
	{
		ArrayList al = new ArrayList();

		File file = new File(filePath); 
		File[] tempFile = file.listFiles(); 
		for(int i = 0;i<tempFile.length;i++) 
		{ 
			//if(tempFile[i].isFile())
			al.add(tempFile[i].getName());
		}
		
		return((String[])al.toArray(new String[0]));
	}
	
	/**
	 * 获得一个文件夹下所有文件夹
	 * @param filePath
	 * @return			数组
	 */
	public static final String[] getFolderFromFolder(String filePath) 
	{
		ArrayList al = new ArrayList();

		File file = new File(filePath); 
		File[] tempFile = file.listFiles(); 
		for(int i = 0;i<tempFile.length;i++) 
		{ 
			if(tempFile[i].isDirectory())
			{
				al.add(tempFile[i].getName());				
			}
		}
		
		return((String[])al.toArray(new String[0]));
	}
	
	/**
	 * 获得一个文件夹下所有文件
	 * @param filePath
	 * @return			数组
	 */
	public static final String[] getFileFromFolder(String filePath) 
	{
		ArrayList al = new ArrayList();

		File file = new File(filePath); 
		File[] tempFile = file.listFiles(); 
		for(int i = 0;i<tempFile.length;i++) 
		{ 
			if(tempFile[i].isFile())
			{
				al.add(tempFile[i].getName());				
			}
		}
		
		return((String[])al.toArray(new String[0]));
	}
	
	/**
	 * 创建一个新文件（可设置编码）
	 * @param name
	 * @param code
	 * @param content
	 * @throws Exception
	 */
	public static final void createFile(String name,String code,String content) 
		throws Exception
	{
		Writer out = null;
		
		try 
		{
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(name),code));
			out.write(content);
		} 
		catch (IOException e) 
		{
			throw new Exception(e);
		}
		finally
		{
			if ( out!=null )
			{
				out.close();
			}
		}
	}
	
	/**
	 * 创建一个新文件
	 * @param name
	 * @param content
	 * @throws Exception
	 */
	public static final void createFile(String name,String content) 
		throws Exception
	{
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(name)));
	    out.write(content);
	    out.close();
	}

	/**
	 * 移动文件
	 * @param in			源路径
	 * @param out			目标路径
	 */
	public static final void moveFile(String in,String out)
	{
		FileInputStream fis = null;
		FileOutputStream fos = null;

		try
		{
			//先检查目标文件是否存在，如果存在则删除
			File delfile = new File(out);
			if (delfile.exists())
			{
				delfile.delete();
			}
			
			fis = new FileInputStream(in);			
			fos = new FileOutputStream(out);
			byte[] buf = new byte[1024];
			int i = 0;
			while ((i = fis.read(buf)) != -1)
			{
				fos.write(buf, 0, i);
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}		
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				fis.close();
				fos.close();
			}
			catch (Exception e)
			{
				// TODO: handle exception
			}			
		}
		
		//删除旧文件
		File delfile = new File(in);
		if (delfile.exists())
		{
			delfile.delete();
		}
		
	}
	
	/**
	 * 剪切图片文件
	 * @param in
	 * @param out
	 */
	public static final void moveImg(String in,String out)
	{
		//->e:/tomcat5.5.26/webapps/turboshop/./upl_imags_tmp/product/20080731/1217498506062.jpg

		FileInputStream fis = null;
		FileOutputStream fos = null;

		try
		{
			String pathM[] = out.split("/");

			String root = "";
			
			for (int i=0; i<pathM.length-3; i++)
			{
				root += pathM[i]+"/";
			}
			
			String fieldFolder = pathM[pathM.length-3];
			String dateFolder = pathM[pathM.length-2];
			
			//检查product文件夹是否存在
    		File folder = new File(root+fieldFolder);
    		if (!folder.exists())
    		{
    			folder.mkdir();
    		}
    		
    		//检查日期文件夹是否存在
    		File folderDate = new File(root+fieldFolder+"/"+dateFolder);
    		if (!folderDate.exists())
    		{
    			folderDate.mkdir();
    		}
			
    		//先要确保进行剪切的文件存在，才能进行剪切工作
    		File inFile = new File(in);
    		if (inFile.exists())
    		{
    			//先检查目标文件是否存在，如果存在则删除
    			File delfile = new File(out);
    			if (delfile.exists())
    			{
    				delfile.delete();
    			}
    			
    			fis = new FileInputStream(in);			
    			fos = new FileOutputStream(out);
    			byte[] buf = new byte[1024];
    			int i = 0;
    			while ((i = fis.read(buf)) != -1)
    			{
    				fos.write(buf, 0, i);
    			}
    		}  		
		}
		catch (FileNotFoundException e)
		{
			log.error("FileUtil.moveImg FileNotFoundException:"+e);
		}		
		catch (IOException e)
		{
			log.error("FileUtil.moveImg IOException:"+e);
		}
		finally
		{
			try
			{
				fis.close();
				fos.close();
			}
			catch (Exception e)
			{
				// TODO: handle exception
			}			
		}
		
		//删除旧文件
		File delfile = new File(in);
		if (delfile.exists())
		{
			delfile.delete();
		}
		
	}
	
	/**
	 * 复制文件
	 * @param in	格式：d:/123/1.jpg
	 * @param out	格式：d:/123/
	 */
	public static final void copyFile(String in,String out)
	{

		FileInputStream fis = null;
		FileOutputStream fos = null;

		try
		{
			//确保out路径最后有/
			if ( out.lastIndexOf("/")!=out.length() )
			{
				out = out + "/";
			}
			//追加复制的文件名
			File inF = new File(in);
			out = out + inF.getName();
			
			fis = new FileInputStream(in);			
			fos = new FileOutputStream(out);
			byte[] buf = new byte[1024];
			int i = 0;
			while ((i = fis.read(buf)) != -1)
			{
				fos.write(buf, 0, i);
			}
		}
		catch (FileNotFoundException e)
		{
			log.error("copyFile error:"+e);
		}		
		catch (IOException e)
		{
			log.error("copyFile error:"+e);
		}
		finally
		{
			try
			{
				fis.close();
				fos.close();
			}
			catch (Exception e)
			{
				// TODO: handle exception
			}			
		}
		
	}
	
	/**
	 * 拷贝（文件、文件夹）
	 * @param in		格式：d:/112或d:/123/1.jpg
	 * @param out		格式：d:/
	 */
	public static final void copy(String in,String out)
	{
		//先判断需要拷贝的是文件夹还是文件
		File inF = new File(in);
		
		if (inF.isFile())
		{
			copyFile( in, out);
		}
		else if (inF.isDirectory())
		{
			//确保out路径最后有/
			if ( out.lastIndexOf("/")!=out.length() )
			{
				out = out + "/";
			}
			
			//确保in路径最后有/
			if ( in.lastIndexOf("/")!=in.length() )
			{
				in = in + "/";
			}
			
			String folderName = inF.getName();
			File outF = new File(out+folderName);
			outF.mkdir();
			
			//进入文件夹
			File inFS[] = inF.listFiles();
			for (int i=0; i<inFS.length; i++)
			{
				copy(in+inFS[i].getName(),out+folderName);
			}
		}
	}

	/**
	 * 复制并压缩
	 * @param in	格式：d:/123.jpg或d:/123
	 * @param out	格式：d:/
	 */
	public static final void copy2zip(String in,String out)
		throws Exception
	{
		copy(in,out);
		boolean isFile = false;
		String orgOut;
		
		try
		{
			File inF = new File(in);
			
			//确保out路径最后有/
			if ( out.lastIndexOf("/")!=out.length() )
			{
				out = out + "/";
			}
			
			out = out + inF.getName();
			orgOut = out;
			
			if (inF.isFile())
			{
				out = out.substring(0,out.lastIndexOf("."));
				isFile = true;
			}
			
			CompressFile.zip(in, out);
			
			//删除临时文件夹/文件
			if (isFile)
			{
				delFile(orgOut);
			}
			else
			{
				delFolder(orgOut);
			}
		}
		catch (Exception e)
		{
			log.error("copy2zip", e);
			throw e;
		}	
	}
	
	public static String readDoc(File file) throws Exception 
	{
		try
		{
			WordExtractor extractor = new WordExtractor(new FileInputStream(file));
			return extractor.getText();
		} catch (Exception e)
		{
			log.error("readDoc",e);
			throw e;
		}
	}
	public static String readExcel(File file)throws Exception 
	{
		try
		{
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file));
			ExcelExtractor extractor = new ExcelExtractor(workbook);
			extractor.setFormulasNotResults(true);
			extractor.setIncludeSheetNames(false);
			return extractor.getText();
		} catch (Exception e)
		{
			log.error("readExcel",e);
			throw e;
		}
	}
	public static String readTxt(File file) throws Exception
	{
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		try
		{
			reader = new BufferedReader(new FileReader(file));
			String strCxt = "";
			while ((strCxt = reader.readLine()) != null)
			{
				sb.append(strCxt);
			}
			reader.close();
			return sb.toString();
		} catch (Exception e){
			log.error("readTxt",e);
			throw e;
		} 
		
	}
	
	public static String readPdf(File file) throws Exception
	{
		try
		{
			PDFParser parser = new PDFParser(new FileInputStream(file));
			parser.parse();
			PDFTextStripper stripper = new PDFTextStripper();
			return stripper.getText(parser.getPDDocument());
		} catch (Exception e){
			log.error("readTxt",e);
			throw e;
		}
	}
	public static String readPpt(File file) throws Exception
	{
		String text = "";
		  try {
		   SlideShow ss = new SlideShow(new HSLFSlideShow(new FileInputStream(file)));
		   Slide[] slides = ss.getSlides();
		   for (int i = 0; i < slides.length; i++) {
		    TextRun[] t = slides[i].getTextRuns();
		    for (int j = 0; j < t.length; j++) {
		     text += t[j].getText();
		    }
		   }
			 return text;
		  } catch (Exception e) {
			  log.error("readPpt",e);
			  throw e;
		  }
	
	}
	public static String readOfficeFile(File file) throws Exception {
		try{
			String contexts = "" ;
			if(file.exists()){
				String fileType = StringUtil.getSuffix(file.getName()).toLowerCase();
			
				if(fileType.equals("doc")){
					contexts = FileUtil.readDoc(file);
				}else if(fileType.equals("xls")){
					contexts = FileUtil.readExcel(file);
				}else if(fileType.equals("ppt")){
					contexts = FileUtil.readPpt(file);
				}else if(fileType.equals("pdf")){
					contexts = FileUtil.readPdf(file);
				}else if(fileType.equals("txt")){
					contexts = FileUtil.readTxt(file);
				}else{
					log.error("File not support!");
					throw new Exception();
				}
			}
			return contexts;
		}catch (Exception e) {
			log.error("readOffice File Error!",e);
			throw e;
		}
	}
	/**
	 * 用于把上传的zip文件流，生成zip文件，并且解析zip文件里面的内容，同时会返回一个地址
	 * @param inputStream
	 * @param zipfolder
	 * @param zipFile
	 * @return
	 * @throws Exception
	 */
	public static String saveZipFile(InputStream inputStream , String zipfolder , String zipFile ) throws Exception{
		long time =  System.currentTimeMillis();
		FileOutputStream fileOutputStream = null;	 
		try{
 			String parentDir = Environment.getHome()+"/upl_excel_tmp/"+time+zipfolder;
			File file = new File(Environment.getHome()+"/upl_excel_tmp/"+time+zipFile);
			fileOutputStream = new FileOutputStream(file);
			byte[] buffer = new byte[1024 * 50];
			int length = -1 ;
			while((length =  inputStream.read(buffer)) != -1 ){
				fileOutputStream.write(buffer, 0, length);
			}
			//解压出zip文件
			
			createZipFile(file,parentDir);
			return parentDir;
		 
		}catch (Exception e) {
			throw e ;
 		}finally{
 			inputStream.close();
 			fileOutputStream.close();
 		}
	}
	private static void createZipFile(File file,String  parentDir)throws Exception {
		FileInputStream fileInputStream = null;
		ZipFile zipFile = new ZipFile(file);
		ZipEntry entry = null; 
 		ZipInputStream zipInputStream = null;
 		File parentDirs = new File(parentDir);
 		if(!parentDirs.exists()){parentDirs.mkdirs();}
 		try{
 			fileInputStream = new FileInputStream(file);
 			zipInputStream = new ZipInputStream(fileInputStream);
			byte[] bufferd = new byte[1024 * 50];
			int length = -1 ;
			List<String> files  = new ArrayList<String>();
			while((entry = zipInputStream.getNextEntry()) != null){
				String name = entry.getName();
				
				File tempFile = new File(parentDir +File.separator+name);
				tempFile.createNewFile();
				InputStream inputStream = zipFile.getInputStream(entry);
				OutputStream outputStream = new FileOutputStream(tempFile);
				
				while((length = inputStream.read(bufferd)) != -1){
					outputStream.write(bufferd, 0, length);
				}
				files.add(name);
				inputStream.close();
				outputStream.close();
			}
 		}catch (Exception e) {
 			throw e ;
 		}finally{
			if(zipInputStream != null){zipInputStream.close();}
			if(fileInputStream != null){fileInputStream.close();
		}
	 }
	}
	/**
	 * 复制一个文件 或者是文件夹到指定的目录(不支持递归的方法)
	 * @param surcePath	(文件或者是文件夹)
	 * @param desFilePathDir
	 */
	public static void removeFile(String surcePath, String desFilePathDir) throws Exception{
		File file = new File(surcePath);
		if(file.exists()){
			if(file.isDirectory()){
				removeFile(file.listFiles(),desFilePathDir);
			}else{
				File[] files = new File[1];
				files[0] = file ;
				removeFile(files,desFilePathDir);
			}
		}
 	}
	/**
	 * 
	 * @param pictures
	 * @param desFilePathDir
	 * @throws Exception
	 */
	public static void removeFile(File[]  pictures , String desFilePathDir) throws Exception {
		try{
			if(pictures != null){
			 for(File file : pictures){
				 copyProductFile(file,desFilePathDir);
			 }
			}
		}catch (Exception e) {
			log.error("removeFile",e);
			throw e;
		}
	}
	public static void copyProductFile(File file , String parentPath) throws Exception{
		 OutputStream outputStream  = null ;
		 FileInputStream inputStream = null ;
		try{
			 File Outfile = new File(parentPath, file.getName());
			 if(Outfile.exists()){Outfile.delete();}
			 if(!Outfile.getParentFile().exists()){Outfile.getParentFile().mkdirs();}
			 Outfile.createNewFile();
			 inputStream = new FileInputStream(file);
			 outputStream = new FileOutputStream(Outfile);
			 int length = -1;
			 byte[] buffered = new byte[1024 * 50];
			 while((length = inputStream.read(buffered)) != -1){
				 outputStream.write(buffered, 0, length);
			 }
		}catch (Exception e) {
			log.error("copyProductFile",e);
			throw e;
 
 		}finally{
 			if(outputStream != null){outputStream.close();}
 			if(inputStream != null){inputStream.close();}
 		}
	}
	
	public static void checkInAndroidCopyFile(File srcFile , String prefix , String parentPath) throws Exception{
		try{
			File destFile = new File(parentPath,prefix + srcFile.getName());
			if(!destFile.exists()){destFile.createNewFile();}
			FileUtils.copyFile(srcFile, destFile);
		}catch (Exception e) {
			log.error("checkInAndroidCopyFile",e);
			throw e;

 		}
		
	}
	
	/**
	 * 复制文件并且重命名
	 * @param in 格式：d:/123/1.jpg
	 * @param out 格式：d:/123/
	 * @author subin
	 */
	public static final void copyFileAndRename(String in,String out,String fileName){

		FileInputStream fis = null;
		FileOutputStream fos = null;

		try{
			
			//确保out路径最后有/
			if ( out.lastIndexOf("/")!=out.length()){
				
				out = out + "/";
			}
			
			//追加复制的文件名
			File inF = new File(in);
			
			//如果有复制后的重命名,否则使用原名.
			if(fileName!=null && !fileName.equals("")){
				
				out = out + fileName;
			}else{
				
				out = out + inF.getName();
			}
			
			fis = new FileInputStream(in);			
			fos = new FileOutputStream(out);
			
			byte[] buf = new byte[1024];
			
			int i = 0;
			while ((i = fis.read(buf)) != -1){
				
				fos.write(buf, 0, i);
			}
		}catch (FileNotFoundException e){
			
			log.error("copyFile error:"+e);
		}catch (IOException e){
			
			log.error("copyFile error:"+e);
		}finally{
			try{
				fis.close();
				fos.close();
			}catch (Exception e){
			}			
		}
	}

	
	public static File downloadFromFileServ( long fileId,String downLoadUrl,String savePath)throws Exception{
	
		HttpClient httpClient = new DefaultHttpClient();
		File fileDownload = null;
		try {
            
			HttpGet httpget = new HttpGet(downLoadUrl + fileId);
			HttpResponse response = httpClient.execute(httpget);
			StatusLine statusLine = response.getStatusLine();
			int httpStatus=statusLine.getStatusCode();
			fileDownload = new File(savePath);
			if (httpStatus == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				
				InputStream in = entity.getContent();
				try {
					FileOutputStream fout = new FileOutputStream(fileDownload);
					int l = -1;
					byte[] tmp = new byte[1024];
					while ((l = in.read(tmp)) != -1) {
						fout.write(tmp, 0, l);
					}
					fout.flush();
					fout.close();
				} catch(Exception ex){
					ex.printStackTrace();
				}
				finally {
					// 关闭低层流。
					in.close();
				}
				//把底层的流给关闭
				EntityUtils.consume(entity);
				//result = filePath;
			}
			else{
				System.out.println("file:["+fileId+"] dose not existed");
			}
		} catch (Exception e) {
			System.out.println("file:["+fileId+"] dose not existed");
			e.printStackTrace();
			//throw new SystemException(e, "deleteFromFileServ", log);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return fileDownload;
	}
	
	public static File downloadFromSShFileServ( long fileId,String downLoadUrl,String savePath)throws Exception{
		
		File fileDownload = null;
		try {
			fileDownload = new File(savePath);
			FileOutputStream fout = new FileOutputStream(fileDownload);
			byte[] bytes = sshPost(downLoadUrl+fileId,null,"GET","UTF-8");
			fout.write(bytes, 0, bytes.length);
		} catch (Exception e) {
			System.out.println("file:["+fileId+"] dose not existed");
			e.printStackTrace();
		}finally{
			
		}
		return fileDownload;
	}
	
	
	private static class TrustAnyTrustManager implements X509TrustManager {
		  public void checkClientTrusted(X509Certificate[] chain, String authType)
		    throws CertificateException {
		  }
		  public void checkServerTrusted(X509Certificate[] chain, String authType)
		    throws CertificateException {
		  }
		  public X509Certificate[] getAcceptedIssuers() {
		   return new X509Certificate[] {};
		  }
		 }
		 private static class TrustAnyHostnameVerifier implements HostnameVerifier {
		  public boolean verify(String hostname, SSLSession session) {
		   return true;
		  }
		 }
		 /**
		  * post方式请求服务器(https协议)
		  * 
		  * @param url
		  *            请求地址
		  * @param content
		  *            参数
		  * @param charset
		  *            编码
		  * @return
		  * @throws NoSuchAlgorithmException
		  * @throws KeyManagementException
		  * @throws IOException
		  * @throws NoSuchProviderException
		  */
	public static byte[] sshPost(String url, String content,String method, String charset) throws NoSuchAlgorithmException, KeyManagementException,IOException, NoSuchProviderException {
		try {
			TrustManager[] tm = { new TrustAnyTrustManager() };
			// SSLContext sc = SSLContext.getInstance("SSL");
			SSLContext sc = SSLContext.getInstance("SSL", "SunJSSE");
			sc.init(null, tm, new java.security.SecureRandom());
			URL console = new URL(url);

			HttpsURLConnection conn = (HttpsURLConnection) console
					.openConnection();
			conn.setSSLSocketFactory(sc.getSocketFactory());
			conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod(method);
			conn.connect();
			if(content!=null ){
				DataOutputStream out = new DataOutputStream(conn.getOutputStream());
				out.write(content.getBytes(charset));
				// 刷新、关闭
				out.flush();
				out.close();
			}
			InputStream is = conn.getInputStream();
			if (is != null) {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}
				is.close();
				return outStream.toByteArray();
			}
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static void main(String[] args) throws ClientProtocolException,IOException {
		try {
			downloadFromSShFileServ(634,"https://wise.logisticsteam.com/yms/file-app/file-download/","d:\\temp\\634.png");
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}

}