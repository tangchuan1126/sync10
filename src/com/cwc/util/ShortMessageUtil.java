package com.cwc.util;

import java.io.UnsupportedEncodingException;



public class ShortMessageUtil {

	
	private String strReg;   //注册号（由华兴软通提供）
    private String strPwd;   //密码（由华兴软通提供）
    private String strSourceAdd;    //子通道号，可为空（预留参数一般为空）
    private String strPhone;	//手机号码，多个手机号用半角逗号分开，最多1000个
    private String strContent;
	private String strSmsUrl;
	private String strSmsParam;
	private String strRes;
	private String strBalanceUrl;
	private String strBalance;	//余额
	
       
	
	public ShortMessageUtil(){
		
		this.strReg			= "101100-WEB-HUAX-540526";
		this.strPwd			= "PFUPQSLH";
		this.strSourceAdd	= "";
		this.strSmsUrl		= "http://www.stongnet.com/sdkhttp/sendsms.aspx";
		this.strBalanceUrl	= "http://www.stongnet.com/sdkhttp/getbalance.aspx";
		
	}
	
	/**
	 * 发送短信
	 * @param message
	 * @param phone
	 * 短信发送成功的回执：result=0&message=短信发送成功&smsid=20121206135446312
	 * @throws UnsupportedEncodingException
	 */
	public int sendPhoneMessage(String message, String phone) throws UnsupportedEncodingException{
		
		int reInt = 2;
		//手机号
		this.strPhone  = phone;
		//短信内容
		this.strContent = HttpSendUtil.paraTo16(message);
		
		this.strSmsParam  = "reg=" + this.strReg + "&pwd=" + this.strPwd 
							+ "&sourceadd=" + this.strSourceAdd + "&phone=" + this.strPhone
							+ "&content=" + this.strContent;
		//发送短信
		strRes = HttpSendUtil.postSend(strSmsUrl, strSmsParam);
		String smState = "";
		if(!"".equals(strRes)){
			String[] strResArr = strRes.split("&");
			if(strResArr.length > 0){
				String resultStr = strResArr[0];
				String[] resultArr = resultStr.split("=");
				if(resultArr.length > 1){
					smState = resultArr[1];
				}
			}
		}
		try
		{
			reInt = Integer.parseInt(smState);
		}
		catch(NumberFormatException e)
		{
			reInt = 2;
		}
		if(reInt == 0){
			reInt = 1;//兼容历史数据
		}
		return reInt;
	}
	
	/**
	 * 查询余额
	 * 回执：result=0&balance=17957
	 */
	public long getBalance(){
		
		String strBalanceParam	= "reg=" + this.strReg + "&pwd=" + this.strPwd;
		long balance = 0L;
		//查询余额
		strBalance	= HttpSendUtil.postSend(this.strBalanceUrl, strBalanceParam);
		
		
		//处理回执，取出余额数值
		if(null != strBalance && !"".equals(strBalance)){
			String[] balanceAllInfo = strBalance.split("&");
			for (int i = 0; i < balanceAllInfo.length; i++) {
				if(balanceAllInfo[i].contains("balance=")){
					String[] balanceInfo = balanceAllInfo[i].split("=");
					balance					= Long.parseLong(balanceInfo[1]);
					break;
				}
			}
		}
		return balance;
	}
    
    
}
