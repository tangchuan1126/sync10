package com.cwc.util;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

//import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;

@SuppressWarnings("deprecation")
public class TUpload 
{
	static Logger log = Logger.getLogger("ACTION");
	private DBRow requestRow = new DBRow();
	private int filesize = 1024*1024*3;
	private int cacheSize = 1024;
	private String headerEncoding="UTF-8";
	private String outFileName = null;
	private String imgFolder;
	private String imgDate;
	private String rootFolder;
	private String permitFile;
	private String defineFileName = null;
	private boolean orgName = false;

	public void useOrgName()
	{
		this.orgName = true;
	}
	
	public void setFileName(String fileName)
	{
		this.defineFileName = fileName;
	}
	
	public void setFileSize(int filesize)
	{
		this.filesize = filesize;
	}

	public void setCacheSize(int cacheSize)
	{
		this.cacheSize = cacheSize;
	}
	
	public void setPermitFile(String permitFile)
	{
		this.permitFile = permitFile;
	}
	
	public void setImgFolder(String imgFolder)
	{
		this.imgFolder = imgFolder;
	}
	
	public void setImgDate(String imgDate)
	{
		this.imgDate = imgDate;
	}

	public void setRootFolder(String rootFolder)
	{
		this.rootFolder = rootFolder;
	}
	
	public void setHeaderEncoding(String headerEncoding)
	{
		this.headerEncoding = headerEncoding;
	}
	
	public DBRow getRequestRow()
	{
		return(requestRow);
	}
	
	private void wrapRequestRow(List items)
		throws Exception
	{
		for (int i = 0; i < items.size(); i++)
		{
		    FileItem item = (FileItem) items.get(i);
		    ////system.out.println(item+"  "+item.isFormField()+" - "+item.getFieldName()+" - "+item.getString("UTF-8"));
		    if (item.isFormField())
		    {
		    	requestRow.add(item.getFieldName(),item.getString("UTF-8"));
		    }
		}
	}
	
	public String getFileName()
	{
		return(outFileName);
	}
	
	private String getExtend(String f)
	{
		return(f.substring(f.indexOf(".")));
	}
	
	private long getTime()
	{
		return(System.currentTimeMillis());
	}
	
	
	
	public int upload(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String fileName;
			String root = Environment.getHome().replace("\\", "/")+"."+rootFolder;	
			
			DiskFileItemFactory diskFactory = new DiskFileItemFactory();  
            // threshold 极限、临界值，即硬盘缓存 1M  
            diskFactory.setSizeThreshold(4 * 1024);  
            // repository 贮藏室，即临时文件目录  
            diskFactory.setRepository(new File(root));  
          
            ServletFileUpload upload = new ServletFileUpload(diskFactory);  
            // 设置允许上传的最大文件大小 4M  
            upload.setSizeMax(4 * 1024 * 1024);
			
			
						
//			DiskFileUpload upload = new DiskFileUpload();
			upload.setSizeMax(filesize);
//			upload.setSizeThreshold(cacheSize);
//			upload.setRepositoryPath(root);
			upload.setHeaderEncoding(headerEncoding);
	
			List items = upload.parseRequest(request);

			wrapRequestRow(items);
			for (int i = 0; i < items.size(); i++)
			{
			    FileItem item = (FileItem) items.get(i);
			    if (!item.isFormField())
			    {
			    	fileName = item.getName();
			    	if (fileName!=null&&!fileName.equals("")&&item.getSize()>0)
			    	{
			    		fileName = fileName.replace('\\','/');
			    		File fullFile = new File(fileName);

			    		if (this.orgName)
			    		{
			    			outFileName = fullFile.getName();
			    		}
			    		else if (defineFileName==null)
			    		{
				    		outFileName = imgFolder+"/"+imgDate+"/"+ getTime() + getExtend(fullFile.getName());			    			
			    		}
			    		else
			    		{
				    		outFileName = defineFileName+ getExtend(fullFile.getName());
			    		}
			    		
			    		if ( permitFile.indexOf( getExtend(fullFile.getName()).toLowerCase().substring(1) )==-1 )
			    		{
			    			return(2);
			    		}

			    		/**
			    		File folder = new File(root+imgFolder);
			    		if (!folder.exists())
			    		{
			    			folder.mkdir();
			    		}
			    		
			    		File folderDate = new File(root+imgFolder+"/"+imgDate);
			    		if (!folderDate.exists())
			    		{
			    			folderDate.mkdir();
			    		}
			    		**/
			    		
						try 
						{
							//如果文件已经存在，先删除
							FileUtil.delFile(root + outFileName);
							////system.out.println(root + outFileName);
						}
						catch (Exception e) 
						{
							
						}
						
						File uploadedFile = new File(root+outFileName);
			    		item.write(uploadedFile);		        		
			    	}
			    }
			}
		} 
		catch (FileUploadException e) 
		{
			log.error("TUpload.upload(request) FileUploadException:" + e);
			return(1);
		}
		catch (Exception e)
		{
			log.error("TUpload.upload(request) error:" + e);
			throw new Exception("upload(request) error:" + e);
		}
			
		return(0);
	}
	
	public static void main(String args[])
	{
		String t = "product/20070619/1182268341953.jpg";
		//system.out.println(t.split("/")[2]);
	}
	
}


