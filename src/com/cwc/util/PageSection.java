package com.cwc.util;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PageSection {
	int st = 0;
	int en = 0;
	int offP = 1;
	int offCount = 0;
	int offSize = 0;
	int maxP = 0;

	public int getOffCount() {
		return offCount;
	}

	public int getOffP() {
		return offP;
	}
	
	public int getSt() {
		if ( offP<1 )
		{
			offP = 1;
		}
		st = (offP-1) * offSize;
		
		return st;
	}
	
	public int getEn() {
		if ( offP>getMaxP() )
		{
			offP = getMaxP();
		}
		en = offP * offSize;
		if ( en>offSize )
		{
			en = offCount;
		}
		
		return en;
	}
	
	private int getMaxP() {
		if ( offSize>offCount )
		{
			return(1);
		}
		else
		{
			if ( offCount%offSize==0 )
			{
				return( offCount/offSize );
			}
			else
			{
				return( (offCount/offSize)+1 );
			}
		}
	}

	public void setOffCount(int i) {
		offCount = i;
	}

	public void setOffP(int i) {
		if ( i<0 )
		{
			i = 1;
		}
		offP = i;
	}
	
	public int getOffSize() {
		return offSize;
	}
	
	public void setOffSize(int i) {
		offSize = i;
	}
	
	public static void main(String args[])
	{
		PageSection ps = new PageSection();
		ps.setOffSize(3);
		ps.setOffCount(13);
		ps.setOffP(5);
		
		int st = ps.getSt();
		int en = ps.getEn();
		
		for ( int i=st; i<en; i++ )
		{
			int j = i + 1;
			//system.out.println(j);
		}
		
		//system.out.println("st:" + st);
		//system.out.println("en:" + en);
		//system.out.println("max p:" + ps.getMaxP());
	}

}
