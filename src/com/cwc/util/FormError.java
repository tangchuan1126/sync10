package com.cwc.util;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FormError
{
	private static String errorMsg = "";

	public static void setField(String field,String condiction,String msg)
		throws Exception
	{
		if ( field.equals(condiction) )
		{
			errorMsg += msg + "<br>";
		}
	}

	public static void setField(int field,int condiction,String msg)
		throws Exception
	{
		if ( field == condiction )
		{
			errorMsg += msg;
		}
	}
	
	public static boolean check(HttpServletResponse response)
		throws Exception
	{
		PrintWriter out = null;
		
		try
		{
			if ( errorMsg.equals("") )
			{
				return(true);
			}
			else
			{
				out = response.getWriter();
				out.println(errorMsg);
				out.close();
				errorMsg = "";
				return(false);
			}
		}
		catch (IOException e)
		{
			throw new Exception("check() error:" + e.getMessage());
		}
	}
}
