package com.cwc.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Encryption 
{
	public static String generateKey(String src, String algorithm)
	{
		MessageDigest m = null;

		try
		{
			m = MessageDigest.getInstance(algorithm);
			m.update(src.getBytes("UTF8"));
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		byte s[] = m.digest();
		String result = "";
		
		for (int i = 0; i < s.length; i++)
		{
			result += Integer.toHexString((0x000000FF & s[i]) | 0xFFFFFF00).substring(6);
		}

		return result;

	}

	public static void main(String args[]) throws Exception
	{
		/*
		Map mb = new LinkedHashMap();
		mb.put("time", "2008");
		mb.put("username", "aaa");
		mb.put("password", "bbb");
		mb.put("email", "aa@aa.com");

		String key = "12345a"; //私钥
		String enc=TurboShopPassport.passportEncode(mb);
		String auth = TurboShopPassport.encrypt(enc, key);

		String forward = "http://echo";
		String verify = "login" + auth + forward + key;
		verify = Encryption.generateKey(verify, "MD5");   
		
		auth = java.net.URLEncoder.encode(auth, "UTF-8");

		//system.out.println("===== 加密 =====");
		//system.out.println("auth: "+auth);
		//system.out.println("verify: "+verify);
		
		//system.out.println("===== 解密 =====");
		String tmpAuth = TurboShopPassport.decrypt(java.net.URLDecoder.decode(auth,"UTF-8"),key);
		//system.out.println("auth: "+ tmpAuth );
		*/

		//String location = "http://127.0.0.1:8088/api/passport.php?action=login&auth=" + java.net.URLEncoder.encode(auth, "UTF-8") + "&forward=" +java.net.URLEncoder.encode(forward, "UTF-8")  + "&verify=" + verify;
		
		////system.out.println(java.net.URLEncoder.encode(TurboShopPassport.encrypt("login:account=echo&pwd=123", "turboshop"),"utf-8"));
		////system.out.println(java.net.URLEncoder.encode(TurboShopPassport.encrypt("logout:", "turboshop"),"utf-8"));
		//system.out.println(java.net.URLEncoder.encode(TurboShopPassport.encrypt("modify:account=xxx&pwd=xxx&email=xxx&mid=xxx&verify=xxx", "turboshop"),"utf-8"));

	}

}
