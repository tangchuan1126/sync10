package com.cwc.util;

import java.awt.image.PixelGrabber;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.lang.InterruptedException;

/**
 * 没有使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PixelColor
{
    private String imageFile = null;
    private int imageHeight = 0;
    private int imageWidth = 0;
    private int alpha = 0;
    private int red  = 0;
    private int green = 0;
    private int blue  = 0;
    private String errMesage  = "";
    public PixelColor(String ImageFile)
    {
        setImageFile( ImageFile);
        getImageInfo();
    }

    private void setImageFile(String ImageFile)
    {
        if( ( ImageFile == null) || (ImageFile.trim().length() == 0))
        {
            return;
        }
        else
        {
            this.imageFile = ImageFile;
        }
    }

    private void getImageInfo()
    {
        if( this.imageFile == null)
        {
            return;
        }
        else
        {
            ImageIcon imageIcon = new ImageIcon( this.imageFile );

		    this.imageHeight = imageIcon.getIconHeight();
		    this.imageWidth = imageIcon.getIconWidth();

		    Image picture = imageIcon.getImage();

		    int[] pixels = new int[this.imageHeight * this.imageWidth];
		    PixelGrabber pg;
            pg = new PixelGrabber(picture, 0, 0, this.imageWidth,
                           this.imageHeight, pixels, 0, this.imageWidth);
            try
            {
    			if(!pg.grabPixels())
                {
                    this.errMesage = "image file not found!";
                }
    		}
            catch (InterruptedException e)
            {
    			e.printStackTrace();
    		}

	    	int x = 3;
    		int y = 3;
	    	int pixel = pixels[x + y * this.imageWidth];

		    this.alpha = (pixel >> 24) & 0xff;
		    this.red   = (pixel >> 16) & 0xff;
		    this.green = (pixel >>  8) & 0xff;
		    this.blue  = (pixel      ) & 0xff;
        }
    }

    public int getImageWidth()
    {
        return this.imageWidth;
    }

    public int getImageHeight()
    {
        return this.imageHeight;
    }

    public int getImageRed()
    {
        return this.red;
    }

    public int getImageGreen()
    {
        return this.green;
    }

    public int getImageBlue()
    {
        return this.blue;
    }

    public int getImageAlpha()
    {
        return this.alpha;
    }

    public String getImageFile()
    {
        return this.imageFile;
    }

    public String getErrorMessage()
    {
        return this.errMesage;
    }
}
