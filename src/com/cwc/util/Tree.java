package com.cwc.util;

import java.sql.*;
import java.util.ArrayList;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;
import com.cwc.spring.util.MvcUtil;

public class Tree {
	ArrayList orgAl = new ArrayList();
	String tbName;
	int enterId = -1;
	String appendCond = "1=1";

	public Tree(String tbName)
		throws Exception
	{
		this.tbName = tbName;
	}

	public Tree(String tbName,String appendCond)
		throws Exception
	{
		this.tbName = tbName;
		this.appendCond = appendCond;
	}

//	public Tree(String tbName,String title)
//		throws Exception
//	{
//		this.tbName = tbName;
//		DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
//		DBRow row = dbUtilAutoTran.selectSingle("select id from " + tbName + " where title='" + title + "'");
//		if ( row !=null )
//		{
//			this.enterId = row.get("id",0);
//		}
//	}

	private ArrayList getAllLawClassBeanVector()
		throws Exception
	{
		ArrayList allLawClassBeanVector = new ArrayList();
		String sqlStr;
		
		try {
			sqlStr = "select * from " + tbName + " where 1=1 and "+this.appendCond+" order by parentid asc,sort asc,id asc";

			DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
			DBRow dbrow[] = dbUtilAutoTran.selectMutliple(sqlStr);//dbUtilAutoTran.selectMutlipleCache(sqlStr,new String[]{tbName});
			for ( int i=0; i<dbrow.length; i++ )
			{
				TreeBean TreeBean = new TreeBean();
				TreeBean.setID(dbrow[i].get("id",0));
				TreeBean.setParentID(dbrow[i].get("parentid",0));
				TreeBean.setRows(dbrow[i]);
				allLawClassBeanVector.add(TreeBean);
			}
		}
		catch (Exception e) {
			throw new Exception("getAllLawClassBeanVector() failure: " + e.getMessage());
		}

		orgAl = allLawClassBeanVector;
		return(allLawClassBeanVector);
	}

	//��ڲ���vector,Ŀ������,nextInde
	private ArrayList setNextIndex(ArrayList v,int index,int nextIndex) throws Exception {
		try {
			TreeBean TreeBean = (TreeBean)v.get(index);
			TreeBean.setNextIndex(nextIndex);
			v.set(index,TreeBean);
		}
		catch (Exception e) {
			throw new Exception("error:setNextIndex() failure: " + e.getMessage());
		}

		return(v);
	}

	//ͨ��id��ȡnextIndex
	private int getNextIndex(ArrayList v,int id) {
		for (int i=0; i<v.size(); i++) {
			TreeBean TreeBean = (TreeBean)v.get(i);
			if ((TreeBean.getID()) == id) return(TreeBean.getNextIndex());
		}
		return(-1);
	}

	//ͨ��id���index
	private int getIndex(ArrayList v,int id) {
		for (int i=0; i<v.size(); i++) {
			TreeBean TreeBean = (TreeBean)v.get(i);
			if ((TreeBean.getID()) == id) return(i);
		}
		return(-1);
	}
	
	//ȡ��ͬһ��ڵ��½ڵ��id
	//�����˳��㷴�ˣ�������������ȷ
	private ArrayList getSonIdByNode(ArrayList v,int node)
	{
		ArrayList outAl = new ArrayList();
		
		for ( int i=0; i<v.size(); i++ )
		{
			TreeBean TreeBean = (TreeBean)v.get(i);
			if ( TreeBean.getParentID() == node ) outAl.add(new Integer(TreeBean.getID()));
		}

		return(outAl);
	}

	//��ͬһ��ڵ��µ���ݴ���4
	private ArrayList makeLink(ArrayList v,int node)
		throws Exception
	{

		int currentIndex = 0;
		int preIndex = -1;
		int id;

		ArrayList al = getSonIdByNode(v,node);
		
		for ( int i=0; i<al.size(); i++ )
		{
			id = StringUtil.getInt(al.get(i).toString());
			currentIndex = getIndex(v,id);
			if (preIndex != -1) v = setNextIndex(v,preIndex,currentIndex);
			preIndex = currentIndex;
		}
		
//		if ( node == 100044 )
//		{
//			//system.out.println("---------------------------------------------");
//			for ( int i=0; i<v.size(); i++ )
//			{
//				//system.out.println(((TreeBean)v.get(i)).getParentID() +" - "+ ((TreeBean)v.get(i)).getID() +" - "+ ((TreeBean)v.get(i)).getNextIndex() );
//			}
//			//system.out.println("---------------------------------------------");			
//		}
		
		return(v);
	}

	private ArrayList makeAllLink(ArrayList v)
		throws Exception
	{
		int node;

		v = makeLink(v,0);
		for ( int i=0; i<orgAl.size(); i++ )
		{
			TreeBean TreeBean = (TreeBean)orgAl.get(i);
			node = TreeBean.getID();
			v = makeLink(v,node); 
		}

		return(v);
	}


	private int getLastElemIndex(ArrayList v,int firstIndex) 
	{
		int out = -1;

		while (true) {
			TreeBean TreeBean = (TreeBean)v.get(firstIndex);
			if (TreeBean.getNextIndex() != -1) firstIndex = TreeBean.getNextIndex();
			else {
				out = firstIndex;
				break;
			}
		}

		return(out);
	}

	private ArrayList makeTree(ArrayList v)
		throws Exception
	{
		int fatherNextIndex,fatherIndex;
		int firstSonIndex;
		int lastElemIndex;
		int fatherid;

		for ( int i=0; i<v.size(); i++ )
		{
			TreeBean TreeBean = (TreeBean)v.get(i);
			
 			fatherid = TreeBean.getID();
			fatherIndex = getIndex(v,fatherid);
			fatherNextIndex = getNextIndex(v,fatherid);
			
			ArrayList sonIdAl = getSonIdByNode(v,fatherid);
			if ( sonIdAl.size()!= 0 ) 
			{
				firstSonIndex = getIndex(v,StringUtil.getInt(sonIdAl.get(0).toString()));
				v = setNextIndex(v,fatherIndex,firstSonIndex);
				lastElemIndex = getLastElemIndex(v,firstSonIndex);
				v = setNextIndex(v,lastElemIndex,fatherNextIndex);
			}
		}		

		return(v);
	}


	private ArrayList makeLevel(ArrayList v)
		throws Exception
	{
		int id;
		int level;
		int index;

		for ( int i=orgAl.size()-1; i>=0; i-- )
		{
			TreeBean TreeBean = (TreeBean)orgAl.get(i);
 
			id = TreeBean.getID();
			index = getIndex(v,id);
			level = findLevel(tbName,id);
			v = setLevel(v,index,level);
		}

		return(v);
	}
	
	private int getParentidById(int id)
	{
		for ( int i=0; i<orgAl.size(); i++ )
		{
			TreeBean TreeBean = (TreeBean)orgAl.get(i);
			if ( TreeBean.getID() == id ) return(TreeBean.getParentID());
		}
		
		return(-1);
	}

	//���id�������������
	private int findLevel(String tbName,int id)
		throws Exception
	{
		String sqlStr;
		ResultSet rs;
		int parentid = 0;
		int level = 0;

		while (true) {
			level++;
			if ( getParentidById(id) != -1 ) parentid = getParentidById(id);
			if (parentid == 0) break;
			else id = parentid;
		}

		return(level);
	}

	//���index������Ӧ��nextIndex
	private ArrayList setLevel(ArrayList v,int index,int level) throws Exception {
		try {
			TreeBean TreeBean = (TreeBean)v.get(index);
			TreeBean.setLevel(level);
			v.set(index,TreeBean);
		}
		catch (Exception e) {
			throw new Exception("setLevel() failure: " + e.getMessage());
		}

		return(v);
	}

	public DBRow[] getTree()
		throws Exception
	{
		ArrayList lawClassBeanVector = new ArrayList();
		ArrayList outTreeVector = new ArrayList();
		int firstIndex =0;
		int nextIndex;
		TreeBean TreeBean;
		int parentId = -1;

		lawClassBeanVector = getAllLawClassBeanVector();
		lawClassBeanVector = makeAllLink(lawClassBeanVector);
		lawClassBeanVector = makeTree(lawClassBeanVector);
		lawClassBeanVector = makeLevel(lawClassBeanVector);
		

		/*for ( int i=0; i<lawClassBeanVector.size(); i++ )
	 	{
	 		TreeBean la = (TreeBean)lawClassBeanVector.get(i);
	 		//system.out.println(la.getRows().getString("title")+"\t\t"+la.getID()+"\t\t"+la.getParentID()+"\t\t"+la.getNextIndex());
	 	}*/
		
		////system.out.println("========================"+lawClassBeanVector.size());
		for (int i=0; i<lawClassBeanVector.size(); i++) 
		{
			TreeBean = (TreeBean)lawClassBeanVector.get(firstIndex);
			nextIndex = TreeBean.getNextIndex();
 			DBRow row = TreeBean.getRows();
			row.add("level",TreeBean.getLevel());
			outTreeVector.add(row);
			
			firstIndex = nextIndex;
			if ( firstIndex==-1 )
			{
				break;
			}
		}
		////system.out.println(outTreeVector.size());
		return((DBRow[])outTreeVector.toArray(new DBRow[0]));
	}

	public static String makeSpace(String space,int level) {
		String outSpace = "";
		level = level - 1;

		for (int i=0; i<level; i++) {
			outSpace += space;
		}

		return(outSpace);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public ArrayList getAllSonNode(long id)
		throws Exception
	{
		ArrayList allNode = new ArrayList();
		String sql;
		int pos = 0;

		try
		{
			while ( true )
			{
				sql = "select * from " + tbName + " where parentid=" + id;		
				DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
				DBRow rows[] = dbUtilAutoTran.selectMutliple(sql);//dbUtilAutoTran.selectMutlipleCache(sql,new String[]{tbName});
				
				for(int i=0;i<rows.length;i++)
				{
					allNode.add(rows[i].getString("id"));
				}
				if ( pos > allNode.size()-1 )
				{
					break;
				}
				id  = StringUtil.getInt((String)allNode.get(pos++));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getAllSonNode(" + id + ") error:" + e);
		}

		return(allNode);
	}

	public DBRow getNUp(long id,int n)
		throws Exception
	{
		String sql;
		long parentid = -1;
		int i=0;
		DBRow row = new DBRow();
		
		try
		{
			DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
			while ( parentid!=0&&i<(n+1) )
			{
				sql = "select * from " + tbName + " where id=" + id;	
				row = dbUtilAutoTran.selectSingle(sql);
				parentid = row.get("parentid",0l);
				id = parentid;
				i++;
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getNUp() error:" + e);
		}
	}

	public static int getMyLevel(String tbName,long id)
		throws Exception
	{
		String sql;
		long parentid = -1;
		int level=0;
		DBRow row;
		
		try
		{
			DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
			while ( parentid!=0 )
			{
				sql = "select * from " + tbName + " where id=" + id;					
				row = dbUtilAutoTran.selectSingle(sql);
				parentid = row.get("parentid",0l);
				id = parentid;
				level++;
			}
			
			return(level);
		}
		catch (Exception e)
		{
			throw new Exception("getMyLevel() error:" + e);
		}
	}
	
	public DBRow[] getAllFatherSQL(long id)
		throws Exception
	{
		ArrayList al = new ArrayList();
		ArrayList tmpAl = new ArrayList();
		DBUtilAutoTran dbUtilAutoTran = (DBUtilAutoTran)MvcUtil.getBeanFromContainer("dbUtilAutoTran");
		String sql;
		long parentid = -1;
		
		try
		{
			while ( parentid!=0 )
			{
				sql = "select * from " + tbName + " where id=" + id;	
//				DBRow row = dbUtilAutoTran.selectSingleCache(sql,new String[]{tbName});
				DBRow row = dbUtilAutoTran.selectSingle(sql);
				parentid = row.get("parentid",0l);
				id = parentid;
				al.add(row);
			}

			for (int i=al.size()-1; i>=0; i--)
			{
				tmpAl.add(al.get(i));
			}
			
			return((DBRow[])tmpAl.toArray(new DBRow[0]));
		}
		catch (Exception e)
		{
			throw new Exception("getAllFatherSQL() error:" + e);
		}
	}

	public DBRow[] getAllFather(long id)
		throws Exception
	{
		ArrayList al = new ArrayList();
		ArrayList tmpAl = new ArrayList();
		boolean isMatch = false;
		
		try
		{
			DBRow treeRows[] = this.getTree();

			while (id>0)
			{
				for (int i=0;i<treeRows.length; i++)
				{
					if (treeRows[i].get("id", 0l)==id)
					{
						al.add(treeRows[i]);
						id = treeRows[i].get("parentid", 0l);
						isMatch = true;
					}
				}
				
				//如果id没有匹配任何数据，即退出
				if (!isMatch)
				{
					break;
				}
			}

			for (int i=al.size()-1; i>=0; i--)
			{
				tmpAl.add(al.get(i));
			}
			
			return((DBRow[])tmpAl.toArray(new DBRow[0]));
		}
		catch (Exception e)
		{
			throw new Exception("getAllFather() error:" + e);
		}
	}

	
	public static void main(String args[])
		throws Exception
	{
		ConfigBean.putBeans("jdbc_internal_pool","true");
		ConfigBean.putBeans("jdbc_driver","com.mysql.jdbc.Driver");
		ConfigBean.putBeans("jdbc_username","root");
		ConfigBean.putBeans("jdbc_password","root");
		ConfigBean.putBeans("jdbc_url","jdbc:mysql://localhost:3306/pl_commerce?characterEncoding=utf-8");
		ConfigBean.putBeans("pool_max_active","100");
		ConfigBean.putBeans("pool_max_idle","30");
		ConfigBean.putBeans("pool_max_wait","10000");
		
		ConfigBean.putBeans("sequence_table","sequence");
		ConfigBean.putBeans("org_sequence","10000");
		
		String qx;
		Tree tree = new Tree("catalog");
//		DBRow treeRows[] = tree.getTree();
//		for ( int i=0; i<treeRows.length; i++ )
//		{		
//			if ( treeRows[i].get("parentid",0) != 0 )
//			 {
//			 	qx = "├ ";
//			 }
//			 else
//			 {
//			 	qx = "";
//			 }
//			
//			//system.out.print(Tree.makeSpace("    ",treeRows[i].get("level",0)));
//			//system.out.print(qx);
//			//system.out.println(treeRows[i].getString("title"));
//		}
		
		DBRow treeRows[] = tree.getAllFather(1010048l);
		for ( int i=0; i<treeRows.length; i++ )
		{
			//system.out.println(treeRows[i].getString("title"));
		}
		
	}
}






