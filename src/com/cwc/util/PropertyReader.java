package com.cwc.util;

import java.util.ResourceBundle;
import java.util.Enumeration;
import java.util.ArrayList;

/**
 * 操作属性文件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PropertyReader
{
	private static ResourceBundle resourceBundle = null;

	/**
	 * 获得属性文件
	 * @param propertyName
	 */
	public PropertyReader(String propertyName)
	{
		getResourceBundle(propertyName);
	}

	/**
	 * 获得属性文件
	 * @param propertyName
	 */
	private void getResourceBundle(String propertyName)
	{
		resourceBundle = ResourceBundle.getBundle(propertyName);
	}

	/**
	 * 获得属性文件内所有参数名
	 * @return
	 */
	public ArrayList getKeys()
	{
		ArrayList al = new ArrayList();
		
		Enumeration keys = resourceBundle.getKeys();
		while(keys.hasMoreElements())
		{
			al.add(keys.nextElement());
		}
		
		return(al);
	}

	/**
	 * 通过参数名获得string属性
	 * @param key
	 * @return
	 */
	public String getString(String key)
	{
		return(resourceBundle.getString(key));	
	}

	/**
	 * 通过参数名获得int属性
	 * @param key
	 * @return
	 */
	public int getInt(String key)
	{
		int val = (new Integer(resourceBundle.getString(key)).intValue());
		return(val);
	}

	public static void main(String args[])
		throws Exception 
	{
		PropertyReader props = new PropertyReader("jutiltest");
		//system.out.println(props.getInt("bbbb") == 999);
	}
}



