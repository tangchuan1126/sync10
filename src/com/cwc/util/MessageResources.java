package com.cwc.util;

import java.util.ResourceBundle;

/**
 * 获得资源文件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MessageResources
{
    private String PropertiesFileName = null;

    public MessageResources( String fName)
    {
        setPropertiesName( fName );
    }

    public void setPropertiesName(String fName)
    {
        this.PropertiesFileName = fName;
    }

    public String getProperties(String propertiesName)
    {
        try
        {
            ResourceBundle rb = ResourceBundle.getBundle( this.PropertiesFileName);

            return rb.getString( propertiesName );
        }
        catch(Exception e)
        {
            return e.getMessage();
        }

    }
}
