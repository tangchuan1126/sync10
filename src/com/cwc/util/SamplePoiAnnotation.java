package com.cwc.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SamplePoiAnnotation {
	
	String title();
	String width() default "2500";
	String color() default "FFFF66";
	String isHidden() default "false";
}
