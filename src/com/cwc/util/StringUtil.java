package com.cwc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Handler;
import com.cwc.app.util.Md5;
import com.cwc.app.util.ThreadContext;
import com.cwc.app.util.TurnStep;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.initconf.Resource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

/**
 * 常用字符串处理工具包
 * 
 * @author TurboShop
 *
 *         TurboShop.cn all rights reserved.
 */
public class StringUtil implements Handler {
	private static String PICTRUEFILE = "jpg,bmp,gif,jpeg,png";
	private static String OFFICEFILE = "pdf,xls,ppt,doc,docx,xlsx";
	private static Pattern NumberPattern = Pattern.compile("\\d+");
	public static final String HMAC_MESSAGE_FORMAT = "{0}:{1}";
    public static final String HMAC_AUTH = "HMAC_AUTH";
    // HMAC WH-PRINT-APP <message-hash>
    public static final String HMAC_AUTH_FORMAT = "HMAC {0} {1}";

	/**
	 * 字符串替换
	 * 
	 * @param s
	 *            源字符串
	 * @param strb
	 *            被替换字符串
	 * @param strh
	 *            替换内容
	 * @return
	 */
	public static final String replaceString(String s, String strb, String strh) {
		if ((s == null) || (s.length() == 0)) {
			return s;
		}

		StringBuffer tmp = new StringBuffer();
		int k;
		while ((k = s.indexOf(strb)) >= 0) {
			tmp.append(s.substring(0, k));
			tmp.append(strh);
			s = s.substring(k + strb.length());
		}
		if (s.length() > 0)
			tmp.append(s);
		return tmp.toString();
	}

	/**
	 * 把回车、换行替换成<br>
	 * 
	 * @param s
	 * @return
	 */
	public static final String replaceEnter(String s) {
		if ((s == null || s.length() == 0)) {
			return s;
		} else {
			s = replaceString(s, "\r\n", "<br>");
			s = replaceString(s, "\n", "<br>");
			return s;
		}
	}

	/**
	 * 把asc字符转换为HTML字符
	 * 
	 * @param s
	 * @return
	 */
	public static final String ascii2Html(String s) {
		if ((s == null) || (s.length() == 0)) {
			return s;
		} else {
			s = replaceString(s, "&", "&amp;");
			s = replaceString(s, "\"", "&quot;");
			s = replaceString(s, "<", "&lt;");
			s = replaceString(s, ">", "&gt;");
			s = replaceString(s, " ", "&nbsp;");
			s = replaceString(s, "'", "&#39");
			s = replaceString(s, "\n", "<br>");
			return s;
		}
	}

	/**
	 * 把HTML字符转换为ASC
	 * 
	 * @param s
	 * @return
	 */
	public static final String html2Ascii(String s) {
		if ((s == null) || (s.length() == 0)) {
			return s;
		} else {
			s = replaceString(s, "&amp;", "&");
			s = replaceString(s, "&quot;", "\"");
			s = replaceString(s, "&lt;", "<");
			s = replaceString(s, "&gt;", ">");
			s = replaceString(s, "&nbsp;", " ");
			s = replaceString(s, "&#39", "'");
			s = replaceString(s, "<br>", "\r\n");
			return s;
		}
	}

	/**
	 * 内码转换：gbk->iso
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String GBK2ISO(String InputStr) {
		try {
			return (new String(InputStr.getBytes("GBK"), "ISO8859_1"));
		} catch (Exception e) {
			System.err.println("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}

	/**
	 * 内码转换：bg2312->utf8
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String GB23122UTF8(String InputStr) {
		try {
			return (new String(InputStr.getBytes("GB2312"), "UTF-8"));
		} catch (Exception e) {
			System.err.println("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}

	/**
	 * 内码转换：iso->utf8
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String ISO2UTF8(String InputStr) {
		try {
			return (new String(InputStr.getBytes("ISO8859-1"), "UTF-8"));
		} catch (Exception e) {
			// system.out.println ("ISO2UTF8");
			return null;
		}
	}

	/**
	 * 内码转换：window1252->utf8
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String Windows1252UTF8(String InputStr) {
		try {
			return (new String(InputStr.getBytes("Windows-1252"), "UTF-8"));
		} catch (Exception e) {
			// system.out.println ("Windows1252UTF8");
			return null;
		}
	}

	/**
	 * 内码转换：utf8->iso
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String UTF82ISO(String InputStr) {
		try {
			return (new String(InputStr.getBytes("UTF-8"), "ISO8859-1"));
		} catch (Exception e) {
			System.err.println("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}

	/**
	 * 内码转换：utf8->gbk
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String UTF82GBK(String InputStr) {
		try {
			return (new String(InputStr.getBytes("UTF-8"), "GBK"));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 内码转换：utf8->bg2312
	 * 
	 * @param InputStr
	 * @return
	 */
	public static final String UTF82GB2312(String InputStr) {
		try {
			return (new String(InputStr.getBytes("UTF-8"), "gb2312"));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 内码转换：iso->gbk
	 * 
	 * @param InputStr
	 * @return
	 */
	public static String ISO2GBK(String InputStr) {
		try {
			return (new String(InputStr.getBytes("ISO8859_1"), "GBK"));
		} catch (Exception e) {
			System.err.println("ISO2GBK����ת��ʧ��!");
			return null;
		}
	}

	/**
	 * 处理NULL值（返回空）
	 * 
	 * @param str
	 * @return
	 */
	public static final String dealNull(String str) {
		if (str == null) {
			return "";
		} else {
			return str;
		}
	}

	/**
	 * 处理SQL
	 * 
	 * @param sql
	 * @return
	 */
	public static final String dealSql(String sql) {
		if ((sql == null) || (sql.trim().length() == 0)) {
			return sql;
		} else {
			String s = replaceString(sql.trim().replaceAll("\r", ""), "'", "''");
			s = replaceString(s, "\\", "\\\\");
			if (s.charAt(0) != '\'') {
				s = "'" + s;
			}
			if (!s.endsWith("'")) {
				s = s + "'";
			}
			return s;
		}
	}

	/**
	 * 把一个参数数组组织成,号分隔字符串
	 * 
	 * @param paraName
	 * @return
	 */
	public static final String getMultiString(String[] paraName) {
		return getMultiString(paraName, ",");
	}

	/**
	 * 把一个参数数组组织成某个号分隔字符串
	 * 
	 * @param paraName
	 * @param spChar
	 * @return
	 */
	public static final String getMultiString(String[] paraName, String spChar) {

		if ((paraName == null) || (paraName.length == 0)) {
			return "";
		} else {
			StringBuffer valuesTemp = new StringBuffer();
			String specialChar = spChar;
			String[] values;
			values = paraName;

			int i = 0;
			for (i = 0; i < (values.length - 1); i++) {
				valuesTemp.append(values[i] + specialChar);
			}

			valuesTemp.append(values[i]);

			return valuesTemp.toString();

		}
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static final boolean isBlank(String str) {
		return (str == null) || (str.trim().length() == 0);
	}

	/**
	 * 获得字符串
	 * 
	 * @param strName
	 * @return
	 */
	public static final String getString(String strName) {
		return getString(strName, null);
	}

	/**
	 * 获得字符串
	 * 
	 * @param strName
	 * @param def
	 * @return
	 */
	public static final String getString(String strName, String def) {
		if (strName == null) {
			return def;
		} else {
			return strName;
		}
	}

	/**
	 * 把string转换成int
	 * 
	 * @param strName
	 * @return
	 */
	public static final int getInt(String strName) {
		return getInt(strName, 0);
	}

	/**
	 * 把string转换成int，可设置默认值
	 * 
	 * @param strName
	 * @param defaultvalue
	 * @return
	 */
	public static final int getInt(String strName, int defaultvalue) {
		if (strName == null) {
			return defaultvalue;
		} else {
			try {
				return Integer.parseInt(strName.trim());
			} catch (Exception e) {
				return defaultvalue;
			}
		}
	}

	/**
	 * 把string转换成long
	 * 
	 * @param strName
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final long getLong(String strName) throws Exception,
			NumberFormatException {
		if (strName == null) {
			throw new Exception("getLong(String strName):Input value is NULL!");
		} else {
			try {
				return (Long.parseLong(strName.trim()));
			} catch (NumberFormatException e) {
				return (0);
			}
		}
	}

	/**
	 * 把string转换成double
	 * 
	 * @param strName
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final double getDouble(String strName) throws Exception,
			NumberFormatException {
		if (strName == null) {
			throw new Exception(
					"getDouble(String strName):Input value is NULL!");
		} else {
			try {
				return (Double.parseDouble(strName));
			} catch (NumberFormatException e) {
				return (0);
			}
		}
	}

	/**
	 * 从request中获得double参数
	 * 
	 * @param request
	 * @param strName
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final double getDouble(HttpServletRequest request,
			String strName) throws Exception, NumberFormatException {
		String val = getString(request, strName);

		try {
			return (Double.parseDouble(val));
		} catch (NumberFormatException e) {
			return (0);
		}
	}

	/**
	 * 把string转换为float
	 * 
	 * @param strName
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final float getFloat(String strName) throws Exception,
			NumberFormatException {
		if (strName == null) {
			throw new Exception("Input value is NULL!");
		} else {
			try {
				return (Float.parseFloat(strName.trim()));
			} catch (NumberFormatException e) {
				throw new NumberFormatException(
						"getFloat(String) NumberFormatException for input string:"
								+ strName);
			}
		}
	}

	public static final float getFloat(String strName, float def)
			throws Exception, NumberFormatException {
		if (strName == null) {
			return (def);
		} else {
			try {
				return (Float.parseFloat(strName.trim()));
			} catch (NumberFormatException e) {
				return (def);
			}
		}
	}

	/**
	 * 从request中获得float参数
	 * 
	 * @param request
	 * @param strName
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final float getFloat(HttpServletRequest request,
			String strName) throws Exception, NumberFormatException {
		String val = request.getParameter(strName);

		if (val == null) {
			return (0);
		} else {
			try {
				return (Float.parseFloat(val.trim()));
			} catch (NumberFormatException e) {
				return (0);
			}
		}
	}

	/**
	 * 从request中获得float参数，可以设置默认值
	 * 
	 * @param request
	 * @param strName
	 * @param def
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	public static final float getFloat(HttpServletRequest request,
			String strName, float def) throws Exception, NumberFormatException {
		String val = request.getParameter(strName);

		if (val == null) {
			return (def);
		} else {
			try {
				return (Float.parseFloat(val.trim()));
			} catch (NumberFormatException e) {
				// system.out.println("getFloat(HttpServletRequest request,String strName,float def) NumberFormatException for input string:"
				// + val + ",so return default!");
				return (def);
			}
		}
	}

	/**
	 * 把string转换成long
	 * 
	 * @param strName
	 * @param defaultvalue
	 * @return
	 */
	public static final long getLong(String strName, long defaultvalue) {
		if (strName == null) {
			return defaultvalue;
		} else {
			try {
				return Long.parseLong(strName);
			} catch (Exception e) {
				return defaultvalue;
			}
		}
	}

	/**
	 * 截取字符串一定长度
	 * 
	 * @param s
	 * @param length
	 * @return
	 */
	public static final String getString(String s, int length) {
		if (s == null) {
			return null;
		} else {
			if (getNumofByte(s) <= length) {
				return s;
			} else {
				byte[] bb = s.getBytes();
				byte[] temp = new byte[length];
				for (int i = 0; i < length; i++) {
					temp[i] = bb[i];
				}
				return getString(temp);
			}
		}

	}

	/**
	 * 获得字符串字节长度
	 * 
	 * @param s
	 * @return
	 */
	private static int getNumofByte(String s) {
		if (s == null) {
			return 0;
		} else {
			return s.getBytes().length;
		}
	}

	/**
	 * 把byte转换成string
	 * 
	 * @param bb
	 * @return
	 */
	private static final String getString(byte[] bb) {
		String s = new String(bb);
		if (s.length() == 0) {
			int length = bb.length;
			if (length > 1) {
				byte[] temp = new byte[length - 1];
				for (int i = 0; i < length - 1; i++) {
					temp[i] = bb[i];
				}
				return getString(temp);
			} else {
				return "";
			}

		} else {
			return s;
		}
	}

	/**
	 * 对字符串进行md5加密
	 * 
	 * @param s
	 * @return
	 */
	public static final String getMD5(String s) {
		if (s == null) {
			return null;
		}
		if (s.trim().length() == 0) {
			return s;
		}

		Md5 md5 = new Md5();

		return md5.getMD5ofStr(s);
	}

	/**
	 * 从request中获得int参数，默认返回0
	 * 
	 * @param request
	 * @param ParamName
	 * @return
	 */
	public static final int getInt(HttpServletRequest request, String ParamName) {
		return (getInt(request, ParamName, 0));
	}

	/**
	 * 从request中获得int参数，可以设置默认值
	 * 
	 * @param request
	 * @param ParamName
	 * @param defV
	 * @return
	 */
	public static final int getInt(HttpServletRequest request,
			String ParamName, int defV) {
		int t;

		if (request.getParameter(ParamName) != null) {
			try {
				t = Integer.parseInt(request.getParameter(ParamName));
				return (t);
			} catch (NumberFormatException e) {
				return (defV);
			}
		} else
			return (defV);
	}

	/**
	 * 从request中获得long参数
	 * 
	 * @param request
	 * @param ParamName
	 * @return
	 */
	public static final long getLong(HttpServletRequest request,
			String ParamName) {
		long t;

		if (request.getParameter(ParamName) != null) {
			try {
				t = Long.parseLong(request.getParameter(ParamName).trim());
				return (t);
			} catch (NumberFormatException e) {
				return (0);
			}
		} else {
			return (0);
		}
	}

	/**
	 * 从request中获得long参数，可以设置默认值
	 * 
	 * @param request
	 * @param ParamName
	 * @param def
	 * @return
	 */
	public static final long getLong(HttpServletRequest request,
			String ParamName, long def) {
		long t;

		if (request.getParameter(ParamName) != null) {
			try {
				t = Long.parseLong(request.getParameter(ParamName).trim());
				return (t);
			} catch (NumberFormatException e) {
				return (0);
			}
		} else {
			return (def);
		}
	}

	/**
	 * 从request中获得String参数
	 * 
	 * @param request
	 * @param paramName
	 * @return
	 */
	public static final String getString(HttpServletRequest request,
			String paramName) {
		// String database = null;
		// try
		// {
		// DBUtil dbUtil = new DBUtil();
		// database = dbUtil.getDatabaseType();
		// }
		// catch (Exception e)
		// {
		// //system.out.println("getDatabaseType() error:" + e);
		// }
		if (paramName == null) {
			return (null);
		}

		String t = request.getParameter(paramName);
		if (t != null && !t.equals("")) {
			// if ( database.equals("Microsoft SQL Server") ||
			// database.equals("Oracle") )
			// {
			// t = ISO2GBK(t);
			// }
			// t = ISO2UTF8(t);

			return (t.trim());
		} else {
			return ("");
		}
	}

	/**
	 * 从request中获得String参数，可设置默认值
	 * 
	 * @param request
	 * @param paramName
	 * @param defaultVal
	 * @return
	 */
	public static final String getString(HttpServletRequest request,
			String paramName, String defaultVal) {
		String t = request.getParameter(paramName);
		if (t != null && !t.equals("")) {
			return (t.trim());
		} else {
			return (defaultVal);
		}
	}

	/**
	 * 从request中获得session
	 * 
	 * @param request
	 * @return
	 */
	public static final HttpSession getSession(HttpServletRequest request) {
		return (request.getSession(true));
	}

	/**
	 * 按一定长度剪裁字符串，在末尾追加自定义符号
	 * 
	 * @param str
	 * @param len
	 * @param tail
	 * @return
	 */
	public static final String cutString(String str, int len, String tail) {
		int slen = str.length();

		if (slen > len) {
			str = str.substring(0, len) + tail;
		}
		return (str);
	}

	/**
	 * 按一定格式格式化double(#0.00)
	 * 
	 * @param formatstr
	 * @param number
	 * @return
	 */
	public static String formatNumber(String formatstr, double number) {
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstr);
		return (df.format(number));
	}

	/**
	 * 按一定格式格式化long(#0.00)
	 * 
	 * @param formatstr
	 * @param number
	 * @return
	 */
	public static String formatNumber(String formatstr, long number) {
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstr);
		return (df.format(number));
	}

	/**
	 * 从request中获得backurl
	 * 
	 * @param request
	 * @return
	 */
	public static final String getBackURL(HttpServletRequest request) {
		String backurl = StringUtil.getString(request, "backurl");
		backurl = replaceString(backurl, "@", "");

		if (backurl.equals("")) {
			String ref = request.getHeader("referer");
			if (ref == null) {
				ref = "";
			}
			return (ref);
		} else {
			return (backurl);
		}
	}

	/**
	 * 获得当前访问URL
	 * 
	 * @param request
	 * @return
	 */
	public static String getCurrentURL(HttpServletRequest request) {
		// String paras = request.getQueryString();
		// String uri = request.getContextPath()+request.getServletPath();
		// String uri = request.getRequestURI();

		// //system.out.println("--------------");
		// //system.out.println(request.getRequestURI());
		// //system.out.println(request.getContextPath()+request.getServletPath());

		// if ( paras==null )
		// {
		// return(uri);
		// }
		// else
		// {
		// return(uri + "?" + paras);
		// }
		if (request.getAttribute("originalRequestURL") != null) {
			return request.getAttribute("originalRequestURL").toString();
		} else {
			return "";
		}
	}

	/**
	 * 编码backurl（主要对&进行编码）
	 * 
	 * @param backurl
	 * @return
	 */
	public static String enCodeBackURL(String backurl) {
		return (backurl.replace('&', '!'));
	}

	/**
	 * 解码backurl
	 * 
	 * @param backurl
	 * @return
	 */
	public static String deCodeBackURL(String backurl) {
		return (backurl.replace('!', '&'));
	}

	/**
	 * 把二进制转换成long
	 * 
	 * @param bin
	 * @return
	 */
	public static long bin2Dec(String bin) {
		int binLen = bin.length();
		int bit;
		long sum = 0;

		for (int i = 0; i < binLen; i++) {
			bit = getInt(bin.substring(binLen - i - 1, binLen - i));
			sum += bit * Math.pow(2, i);
		}

		return (sum);
	}

	/**
	 * 获得表单checkbox状态
	 * 
	 * @param i
	 * @return
	 */
	public static String getStatusForCheckBoxAndRadio(int i) {
		if (i == 0) {
			return ("");
		} else {
			return ("checked");
		}
	}

	/**
	 * 设置cookie
	 * 
	 * @param response
	 * @param CookieName
	 *            名称
	 * @param CookieVal
	 *            值
	 * @param CookieAge
	 *            生命期（毫秒）
	 * @throws UnsupportedEncodingException
	 */
	public static void setCookie(HttpServletResponse response,
			String CookieName, String CookieVal, int CookieAge)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(CookieName, URLEncoder.encode(CookieVal,
				"utf-8"));
		cookie.setMaxAge(CookieAge);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	/**
	 * 设置绘画cookie 只存在一个浏览器会话，浏览器关闭即消失
	 * 
	 * @param response
	 * @param CookieName
	 *            名称
	 * @param CookieVal
	 *            值
	 * @throws UnsupportedEncodingException
	 */
	public static void setSessionCookie(HttpServletResponse response,
			String CookieName, String CookieVal)
			throws UnsupportedEncodingException {
		Cookie cookie = new Cookie(CookieName, URLEncoder.encode(CookieVal,
				"utf-8"));
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	/**
	 * 设置cookie
	 * 
	 * @param response
	 * @param CookieName
	 *            名称
	 * @param CookieVal
	 *            值
	 * @param CookieAge
	 *            生命期
	 * @param domain
	 *            域
	 * @throws UnsupportedEncodingException
	 */
	public static void setCookie(HttpServletResponse response,
			String CookieName, String CookieVal, int CookieAge, String domain[])
			throws UnsupportedEncodingException {
		for (int i = 0; i < domain.length; i++) {
			Cookie cookie = new Cookie(CookieName, URLEncoder.encode(CookieVal,
					"utf-8"));
			cookie.setMaxAge(CookieAge);
			cookie.setPath("/");
			cookie.setDomain(domain[i]);
			response.addCookie(cookie);
		}
	}

	/**
	 * 设置绘画cookie 只存在一个浏览器会话，浏览器关闭即消失
	 * 
	 * @param response
	 * @param CookieName
	 *            名称
	 * @param CookieVal
	 *            值
	 * @param domain
	 *            域
	 * @throws UnsupportedEncodingException
	 */
	public static void setSessionCookie(HttpServletResponse response,
			String CookieName, String CookieVal, String domain[])
			throws UnsupportedEncodingException {
		for (int i = 0; i < domain.length; i++) {
			Cookie cookie = new Cookie(CookieName, URLEncoder.encode(CookieVal,
					"utf-8"));
			cookie.setPath("/");
			cookie.setDomain(domain[i]);
			response.addCookie(cookie);
		}
	}

	/**
	 * 获得cookie
	 * 
	 * @param request
	 * @param CookieName
	 *            名称
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getCookie(HttpServletRequest request, String CookieName)
			throws UnsupportedEncodingException {
		Cookie cookies[] = request.getCookies();

		if (cookies == null)
			return (null);
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i] != null && cookies[i].getName().equals(CookieName)) {
				return (URLDecoder.decode(cookies[i].getValue(), "utf-8"));
			}
		}
		return (null);
	}

	/**
	 * 获得所有cookie
	 * 
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String[] getCookie(HttpServletRequest request)
			throws UnsupportedEncodingException {
		Cookie cookies[] = request.getCookies();
		ArrayList al = new ArrayList();

		if (cookies == null)
			return (null);
		for (int i = 0; i < cookies.length; i++) {
			al.add(cookies[i].getName() + " = "
					+ URLDecoder.decode(cookies[i].getValue(), "utf-8"));
		}
		return ((String[]) al.toArray(new String[0]));
	}

	/**
	 * 删除cookie
	 * 
	 * @param response
	 * @param request
	 * @param CookieName
	 * @throws UnsupportedEncodingException
	 */
	public static void delCookie(HttpServletResponse response,
			HttpServletRequest request, String CookieName)
			throws UnsupportedEncodingException {
		Cookie[] cookies = request.getCookies();
		// //system.out.println("+++++>"+cookies);
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String tempuid_1 = cookies[i].getName();
				// //system.out.println(tempuid_1);

				if (tempuid_1.equals(CookieName)) {
					cookies[i].setMaxAge(0);
					response.addCookie(cookies[i]);
				}
			}
		}
	}

	/**
	 * 
	 * @param response
	 * @param CookieName
	 * @param domain
	 * @throws UnsupportedEncodingException
	 */
	public static void delCookie(HttpServletResponse response,
			String CookieName, String domain[])
			throws UnsupportedEncodingException {
		for (int i = 0; i < domain.length; i++) {
			Cookie cookie = new Cookie(CookieName, null);
			cookie.setMaxAge(0);
			cookie.setPath("/");
			cookie.setDomain(domain[i]);
			response.addCookie(cookie);
		}
	}

	/**
	 * 去除字符串中的回车换行
	 * 
	 * @param str
	 * @return
	 */
	public static String removeHH(String str) {
		return (StringUtil.replaceString(
				StringUtil.replaceString(str, "\n", ""), "\r", ""));
	}

	/**
	 * 读取一个URL页面内容
	 * 
	 * @param Inputurl
	 * @return
	 * @throws Exception
	 */
	public static String readURL(String Inputurl) throws Exception {
		String sCurrentLine = "";
		String sTotalString = "";

		try {
			InputStream urlInputStream;
			URL url;
			HttpURLConnection urlConnection;
			BufferedReader BufferReader;
			// 定点、打开、连接
			url = new URL(Inputurl);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.connect();
			urlInputStream = urlConnection.getInputStream();
			// 缓冲后可以以characters, arrays, and lines 方式读取数据。
			BufferReader = new BufferedReader(new InputStreamReader(
					urlInputStream));
			while ((sCurrentLine = BufferReader.readLine()) != null)
				sTotalString += sCurrentLine + "\n";
			BufferReader.close();
		} catch (MalformedURLException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}

		return (sTotalString);
	}

	/**
	 * 着色
	 * 
	 * @param osStr
	 * @param color
	 * @param flag
	 * @return
	 */
	public static String withColor(String osStr, String color, boolean flag) {
		if (flag) {
			return ("<font color='".concat(color).concat("'>").concat(osStr)
					.concat("</font>"));
		} else {
			return (osStr);
		}
	}

	/**
	 * 从session获得字符串
	 * 
	 * @param ses
	 * @param name
	 * @return
	 */
	public static String getStringFromSession(HttpSession ses, String name) {
		if (ses.getAttribute(name) != null) {
			return (ses.getAttribute(name).toString());
		} else {
			return (null);
		}
	}

	/**
	 * 从session获得整型
	 * 
	 * @param ses
	 * @param name
	 * @return
	 */
	public static int getIntFromSession(HttpSession ses, String name) {
		if (getStringFromSession(ses, name) != null) {
			return (StringUtil.getInt(getStringFromSession(ses, name)));
		} else {
			return (0);
		}
	}

	/**
	 * 从session获得浮点
	 * 
	 * @param ses
	 * @param name
	 * @return
	 */
	public static float getFloatFromSession(HttpSession ses, String name) {
		if (getStringFromSession(ses, name) != null) {
			try {
				return (StringUtil.getFloat(getStringFromSession(ses, name)));
			} catch (NumberFormatException e) {
				return (0);
			} catch (Exception e) {
				return (0);
			}
		} else {
			return (0);
		}
	}

	/**
	 * 从request获得所有参数，并转换为a=b&c=d
	 * 
	 * @param request
	 * @return
	 */
	public static String getAllParameters(HttpServletRequest request) {
		Enumeration en = request.getParameterNames();
		StringBuffer backSb = new StringBuffer("");
		String paramName;
		String paramValue;

		while (en.hasMoreElements()) {
			paramName = (String) en.nextElement();
			paramValue = request.getParameter(paramName);

			backSb.append(paramName);
			backSb.append("=");
			backSb.append(paramValue);
			backSb.append("&");
		}

		return (backSb.toString());
	}

	/**
	 * 从线程变量获得当前URL（不需要request）
	 * 
	 * @return
	 */
	public static String getCurrentURL() {
		HttpServletRequest request = (HttpServletRequest) ThreadContext
				.get("request");
		if (request == null) {
			return (null);
		}
		try {
			return (java.net.URLEncoder.encode(getCurrentURL(request), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			return (getCurrentURL(request));
		}
	}

	public static String getCurrentURI(HttpServletRequest request) {
		// HttpServletRequest request =
		// (HttpServletRequest)ThreadContext.get("request");
		return (request.getAttribute("originalRequestURI").toString());
	}

	/**
	 * 获得访问来路（不需要request）
	 * 
	 * @return
	 */
	public static String getRefer() {
		HttpServletRequest request = (HttpServletRequest) ThreadContext
				.get("request");
		if (request == null) {
			return (null);
		}
		return (request.getHeader("referer"));
	}

	/**
	 * 从线程变量中的request获得参数
	 * 
	 * @param paramName
	 * @return
	 */
	public static final String getStringOnThreadLocal(String paramName) {
		HttpServletRequest request = (HttpServletRequest) ThreadContext
				.get("request");
		return (getString(request, paramName));
	}

	/**
	 * 模拟浏览器访问一个URL，并获得页面内容
	 * 
	 * @param wurl
	 * @return
	 * @throws Exception
	 */
	public static final String loginWebSite(String wurl) throws Exception {
		java.io.InputStream in;
		String str;
		StringBuffer sb = new StringBuffer("");

		java.net.URL url = new java.net.URL(wurl);
		java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url
				.openConnection();
		connection = (java.net.HttpURLConnection) url.openConnection();
		connection.setRequestProperty("User-Agent",
				"Mozilla/4.0 (compatible; MSIE 6.0; Windows 2000)");
		connection.connect();
		in = connection.getInputStream();
		java.io.BufferedReader breader = new BufferedReader(
				new InputStreamReader(in, "UTF-8"));

		while ((str = breader.readLine()) != null) {
			sb.append(str);
			sb.append("\n");
		}

		return (sb.toString());
	}

	/**
	 * 通过正则表达式替换字符串
	 * 
	 * @param str
	 *            源字符串
	 * @param cp
	 *            正则表达式
	 * @param mc
	 *            替换内容
	 * @return
	 */
	public static String regReplace(String str, String cp, String mc) {
		String txt = new String();
		txt = str;

		if (str != null && !str.equals("")) {
			txt = str;
			Pattern p = Pattern.compile(cp, 2); // 参数2表示大小写不区分
			Matcher m = p.matcher(txt);
			StringBuffer sb = new StringBuffer();
			int i = 0;
			boolean result = m.find();
			// 使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
			while (result) {
				i++;
				m.appendReplacement(sb, mc);
				// 继续查找下一个匹配对象
				result = m.find();
			}
			// 最后调用appendTail()方法将最后一次匹配后的剩余字符串加到sb里；
			m.appendTail(sb);
			txt = sb.toString();
		} else {
			txt = "";
		}

		return (txt);
	}

	public static String[] regMatchers(String s, String reg, int pos) {
		ArrayList al = new ArrayList();

		if (s != null && !s.equals("")) {
			Pattern p = Pattern.compile(reg, 2); // 参数2表示大小写不区分
			Matcher m = p.matcher(s);
			int i = 0;
			// 使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
			while (m.find()) {
				i++;
				al.add(m.group(pos));
			}
		} else {
			return (null);
		}

		return ((String[]) al.toArray(new String[0]));
	}

	public static String[][] regMatchers(String s, String reg, int pos[]) {
		ArrayList al = new ArrayList();

		if (s != null && !s.equals("")) {
			Pattern p = Pattern.compile(reg, 2); // 参数2表示大小写不区分
			Matcher m = p.matcher(s);
			int i = 0;
			// 使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
			while (m.find()) {
				i++;

				String row[] = new String[pos.length];
				for (int ii = 0; ii < pos.length; ii++) {
					row[ii] = m.group(pos[ii]);
					// //system.out.println(pos[ii]+" - "+row[ii]);
				}
				al.add(row);
			}
		} else {
			return (null);
		}

		return ((String[][]) al.toArray(new String[0][0]));
	}

	/**
	 * 获得匹配正则表达式字符串
	 * 
	 * @param s
	 * @param reg
	 * @param i
	 * @return
	 */
	public static String regMatcher(String s, String reg, int i) {
		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher(s);

		if (matcher.find()) {
			return (matcher.group(i));
		} else {
			return (null);
		}
	}

	/**
	 * 
	 * @param row
	 * @return
	 */
	public static DBRow handleNullRow(DBRow row) {
		if (row == null) {
			return (new DBRow());
		} else {
			return (row);
		}
	}

	public static String handleNull(String val, String def) {
		if (val == null) {
			return (def);
		} else {
			return (val);
		}
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static final String cuteNR(String s) {
		s = replaceString(s, "\n", "");
		s = replaceString(s, "\r", "");
		return (s);
	}

	/**
	 * 
	 * @param r
	 * @return
	 */
	public static final int getRowLength(DBRow r[]) {
		if (r == null) {
			return (0);
		} else {
			return (r.length);
		}
	}

	/**
	 * 
	 * @return
	 */
	public static final double getDoubleType() {
		return (0d);
	}

	/**
	 * 
	 * @return
	 */
	public static final float getFloatType() {
		return (0f);
	}

	/**
	 * 
	 * @return
	 */
	public static final long getLongType() {
		return (0l);
	}

	/**
	 * 
	 * @param x
	 * @return
	 */
	public static final long getLongType(String x) {
		return (Long.parseLong(x));
	}

	/**
	 * 
	 * @return
	 */
	public static final boolean getTrueType() {
		return (true);
	}

	/**
	 * 
	 * @return
	 */
	public static final boolean getFalseType() {
		return (false);
	}

	/**
	 * 
	 * @return
	 */
	public static final Object getObjectType() {
		return (new Object());
	}

	/**
	 * 
	 * @param x
	 * @return
	 */
	public static final boolean getBooleanType(int x) {
		if (x == 1) {
			return (true);
		} else {
			return (false);
		}

	}

	/**
	 * 
	 * @return
	 */
	public static final Object getNullType() {
		return (null);
	}

	/**
	 * 获得封装好的翻页按钮
	 * 
	 * @param pc
	 * @param p
	 * @return
	 */
	public static final String getTurnPage(PageCtrl pc, int p) {
		TurnStep turnStep = new TurnStep(); // 计算翻页按钮类
		turnStep.setP(pc.getPageNo()); // 设置当前页
		turnStep.setStepRange(10); // 设置步长
		turnStep.setRight(4); // 设置右边距
		turnStep.setTotal(pc.getPageCount()); // 设置总页数
		turnStep.init(); //

		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;

		StringBuffer turnPageHtml = new StringBuffer();

		if (pc.isFirst()) {
			turnPageHtml
					.append("<a onfocus='this.blur()' href=javascript:go(1)>"
							+ Resource.getStringValue("",
									"class.turnpage.first", "null") + "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource.getStringValue("", "class.turnpage.first",
							"null") + "</span>");
		}

		if (pc.isFornt()) {
			turnPageHtml.append("<a onfocus='this.blur()'  href=javascript:go("
					+ pre
					+ ")>"
					+ Resource
							.getStringValue("", "class.turnpage.back", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.back", "null")
					+ "</span>");
		}

		// 打印翻页数字
		for (int pi = turnStep.getST(); pi <= turnStep.getEN(); pi++) {
			if (pi == p || (p == 0 && pi == 1)) {
				turnPageHtml.append(" <span class=current>" + pi + "</span> ");
			} else {
				turnPageHtml
						.append(" <a  onfocus='this.blur()' href=javascript:go("
								+ pi + ")>" + pi + "</a> ");
			}
		}

		if (pc.isNext()) {
			turnPageHtml.append("<a  onfocus='this.blur()' href=javascript:go("
					+ next
					+ ")>"
					+ Resource
							.getStringValue("", "class.turnpage.next", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.next", "null")
					+ "</span>");
		}

		if (pc.isLast()) {
			turnPageHtml.append("<a  onfocus='this.blur()' href=javascript:go("
					+ pc.getPageCount()
					+ ")>"
					+ Resource
							.getStringValue("", "class.turnpage.last", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.last", "null")
					+ "</span>");
		}

		return (turnPageHtml.toString());
	}

	/**
	 * 获得封装好的翻页按钮（翻页按钮为链接）
	 * 
	 * 该翻页按钮为实际连接，而非javascript,目的是为了利于搜索引擎收录链接
	 * 
	 * @param page
	 *            页面前序
	 * @param paras
	 *            参数串（a,b,c）
	 * @param pc
	 * @param p
	 * @return
	 */
	public static final String getTurnPage(String page, String paras,
			PageCtrl pc, int p) {
		TurnStep turnStep = new TurnStep();
		turnStep.setP(pc.getPageNo());
		turnStep.setStepRange(10);
		turnStep.setRight(4);
		turnStep.setTotal(pc.getPageCount());
		turnStep.init();

		int pre = pc.getPageNo() - 1;
		int next = pc.getPageNo() + 1;

		StringBuffer turnPageHtml = new StringBuffer();

		String paraA[] = paras.split(",");
		StringBuffer paraStr = new StringBuffer("");

		// 分解参数串，组成a-b-c-
		for (int i = 0; i < paraA.length; i++) {
			paraStr.append(paraA[i]);
			paraStr.append("-");
		}

		if (pc.isFirst()) {
			turnPageHtml.append("<a onfocus='this.blur()' href='");
			turnPageHtml.append(page);
			turnPageHtml.append(paraStr.toString());
			turnPageHtml.append("1.html");
			turnPageHtml.append("'>"
					+ Resource.getStringValue("", "class.turnpage.first",
							"null") + "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource.getStringValue("", "class.turnpage.first",
							"null") + "</span>");
		}

		if (pc.isFornt()) {
			turnPageHtml.append("<a onfocus='this.blur()'  href='");
			turnPageHtml.append(page);
			turnPageHtml.append(paraStr.toString());
			turnPageHtml.append(pre);
			turnPageHtml.append(".html");
			turnPageHtml.append("'>"
					+ Resource
							.getStringValue("", "class.turnpage.back", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.back", "null")
					+ "</span>");
		}

		for (int pi = turnStep.getST(); pi <= turnStep.getEN(); pi++) {
			if (pi == p || (p == 0 && pi == 1)) {
				turnPageHtml.append(" <span class=current>" + pi + "</span> ");
			} else {
				turnPageHtml.append(" <a  onfocus='this.blur()' href='");
				turnPageHtml.append(page);
				turnPageHtml.append(paraStr.toString());
				turnPageHtml.append(pi);
				turnPageHtml.append(".html");
				turnPageHtml.append("'>" + pi + "</a> ");
			}
		}

		if (pc.isNext()) {
			turnPageHtml.append("<a  onfocus='this.blur()' href='");
			turnPageHtml.append(page);
			turnPageHtml.append(paraStr.toString());
			turnPageHtml.append(next);
			turnPageHtml.append(".html");
			turnPageHtml.append("'>"
					+ Resource
							.getStringValue("", "class.turnpage.next", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.next", "null")
					+ "</span>");
		}

		if (pc.isLast()) {
			turnPageHtml.append("<a  onfocus='this.blur()' href='");
			turnPageHtml.append(page);
			turnPageHtml.append(paraStr.toString());
			turnPageHtml.append(pc.getPageCount());
			turnPageHtml.append(".html");
			turnPageHtml.append("'>"
					+ Resource
							.getStringValue("", "class.turnpage.last", "null")
					+ "</a>");
		} else {
			turnPageHtml.append("<SPAN class=nextprev>"
					+ Resource
							.getStringValue("", "class.turnpage.last", "null")
					+ "</span>");
		}

		return (turnPageHtml.toString());
	}

	/**
	 * 获得表单select状态
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String selectIt(long a, long b) {
		return (a == b ? "selected" : "");
	}

	/**
	 * 获得表单checkbox状态
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String checkedIt(long a, long b) {
		return (a == b ? "checked" : "");
	}

	/**
	 * 获得表单checkbox状态
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String checkedIt(String a, String b) {
		return (a.equals(b) ? "checked" : "");
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public float getPayCostPercent(float c) {
		return (c * 100);
	}

	/**
	 * 
	 * @param o
	 * @return
	 */
	public static final int isNull(Object o) {
		if (o == null) {
			return (1);
		} else {
			return (0);
		}
	}

	public static final String mergeSpace(String key) {
		if (key.trim().equals("")) {
			return ("");
		}

		StringBuffer sb = new StringBuffer("");
		int strLen = key.length();
		String tmpStr = "", currStr;
		int px;

		// 先合并连续空格
		for (int i = 0; i < strLen; i++) {
			px = i + 1;
			currStr = key.substring(i, px);

			if (currStr.equals(" ")) {
				if (!currStr.equals(tmpStr)) {
					sb.append(currStr);
				}
			} else {
				sb.append(currStr);
			}

			tmpStr = currStr;
		}

		return (sb.toString().trim());
	}

	public static final String mergeChars(String key, String chars) {
		if (key.trim().equals("")) {
			return ("");
		}

		StringBuffer sb = new StringBuffer("");
		int strLen = key.length();
		String tmpStr = "", currStr;
		int px;

		// 先合并连续空格
		for (int i = 0; i < strLen; i++) {
			px = i + 1;
			currStr = key.substring(i, px);

			if (currStr.equals(chars)) {
				if (!currStr.equals(tmpStr)) {
					sb.append(currStr);
				}
			} else {
				sb.append(currStr);
			}

			tmpStr = currStr;
		}

		return (sb.toString().trim());
	}

	/**
	 * 过滤掉HTML中所有HTML标签
	 * 
	 * @param x
	 * @return
	 */
	public static final String getASCFromHTML(String x) {
		String htmlReg1 = "<([^>]*)>";
		String htmlReg2 = "&nbsp;";

		x = StringUtil.regReplace(x, htmlReg1, "");
		x = StringUtil.regReplace(x, htmlReg2, "");
		x = x.trim();
		x = StringUtil.replaceString(x, "\t", "");

		return (x);
	}

	/**
	 * 获得客户端MAC地址
	 * 
	 * @return
	 */
	public static String getMACAddress() {
		String address = null;
		String os = System.getProperty("os.name");
		// //system.out.println(os);

		if (os != null && os.startsWith("Windows")) {
			try {
				ProcessBuilder pb = new ProcessBuilder("ipconfig", "/all");
				Process p = pb.start();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				String line;

				while ((line = br.readLine()) != null) {
					if (line.indexOf("Physical Address") != -1) {
						int index = line.indexOf(":");
						address = line.substring(index + 1);
						break;
					}
				}
				br.close();
				return address.trim();
			} catch (IOException e) {
			}
		}
		return address;
	}

	public static boolean isNull(String value) {
		if (value == null) {
			return true;
		} else {
			return value.trim().length() < 1;
		}
	}

	public static String convertStringArrayToString(String[] array) {
		if (array != null) {
			StringBuffer sb = new StringBuffer();
			for (String s : array) {
				sb.append(",").append(s);
			}
			return sb.length() > 0 ? sb.substring(1) : "";
		}
		return "";
	}

	public static String[] convertStringToStringArray(String value) {
		return value != null && value.trim().length() > 0 ? value.split(",")
				: null;
	}

	/**
	 * 全角转半角
	 * 
	 * @param QJstr
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static final String full2HalfChange(String QJstr)
			throws UnsupportedEncodingException {
		StringBuffer outStrBuf = new StringBuffer("");
		String Tstr = "";
		byte[] b = null;
		for (int i = 0; i < QJstr.length(); i++) {
			Tstr = QJstr.substring(i, i + 1);
			// 全角空格转换成半角空格
			if (Tstr.equals("　")) {
				outStrBuf.append(" ");
				continue;
			}
			b = Tstr.getBytes("unicode");
			// 得到 unicode 字节数据
			if (b[2] == -1) {
				// 表示全角？
				b[3] = (byte) (b[3] + 32);
				b[2] = 0;
				outStrBuf.append(new String(b, "unicode"));
			} else {
				outStrBuf.append(Tstr);
			}
		} // end for.

		return outStrBuf.toString();

	}

	// 半角转全角

	public static final String half2Fullchange(String QJstr)
			throws UnsupportedEncodingException {
		StringBuffer outStrBuf = new StringBuffer("");
		String Tstr = "";
		byte[] b = null;
		for (int i = 0; i < QJstr.length(); i++) {
			Tstr = QJstr.substring(i, i + 1);
			if (Tstr.equals(" ")) {
				// 半角空格
				outStrBuf.append(Tstr);
				continue;
			}
			b = Tstr.getBytes("unicode");
			if (b[2] == 0) {
				// 半角?
				b[3] = (byte) (b[3] - 32);
				b[2] = -1;
				outStrBuf.append(new String(b, "unicode"));
			} else {
				outStrBuf.append(Tstr);
			}
		}
		return outStrBuf.toString();
	}

	public static String getSuffix(String filename) {
		String suffix = "";
		int pos = filename.lastIndexOf('.');
		if (pos > 0 && pos < filename.length() - 1) {
			suffix = filename.substring(pos + 1);
		}
		return suffix;
	}

	public static boolean isPictureFile(String fileName) {
		String suffix = getSuffix(fileName).toLowerCase();
		return PICTRUEFILE.indexOf(suffix) != -1;
	}

	public static boolean isOfficeFile(String fileName) {
		String suffix = getSuffix(fileName).toLowerCase();
		return OFFICEFILE.indexOf(suffix) != -1;
	}

	/**
	 * 处理系统配置(数字=中文描述【回车换行】)组织成Map
	 * 
	 * @param value
	 * @return
	 */
	public static Map<Integer, String> getConfigMap(String value) {
		Map<Integer, String> tempMap = new HashMap<Integer, String>();
		if (value != null && value.trim().length() > 0) {
			String[] arraySelected = value.split("\n");
			if (arraySelected != null && arraySelected.length > 0) {
				for (int index = 0, count = arraySelected.length; index < count; index++) {
					String tempStr = arraySelected[index];
					if (tempStr.indexOf("=") != -1) {
						String[] tempValues = tempStr.split("=");
						if (tempValues != null && tempValues.length == 2) {
							tempMap.put(Integer.parseInt(tempValues[0]),
									tempValues[1]);
						}
					}
				}
			}

		}
		return tempMap;
	}

	/**
	 * 获得节点数据（只适合唯一名称节点）
	 * 
	 * @return
	 */
	public static String getSampleNode(String xml, String name)
			throws Exception {
		String xmlSplit1[] = xml.split("<" + name + ">");
		String xmlSplit2[] = xmlSplit1[1].split("</" + name + ">");
		if (0 != xmlSplit2.length) {
			return (xmlSplit2[0]);
		} else {
			return "";
		}
	}

	/**
	 * 加密算法
	 * 
	 * @param str
	 * @return
	 */
	public static String HashBase64(String str) throws Exception {
		String ret = "";
		try {
			// Hash算法
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(str.getBytes());

			ret = new String(
					new org.apache.commons.codec.binary.Base64().encode(sha
							.digest()));

			// ret=new sun.misc.BASE64Encoder().encode(sha.digest());
		} catch (Exception e) {
			throw e;
		}
		return ret;
	}

	/**
	 * 判断字条串是否为空，且是否可以转换成long型
	 * 
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static boolean isBlankAndCanParseLong(String str) throws Exception {
		if (str != null && str.trim().length() != 0 && !"undefined".equals(str)) {
			long parseLong = 0L;
			try {
				parseLong = Long.parseLong(str);
			} catch (NumberFormatException e) {
				parseLong = 0L;
			}
			if (0 == parseLong) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * 取出文件名 zhangrui.txt 返回zhangrui zhangrui.124.txt 返回zhangrui.124
	 * 

	 * @return
	 */
	public static JSONArray getJsonArrayFromJson(JSONObject json, String key) {
		try {
			return json.getJSONArray(key);
		} catch (JSONException e) {
			// e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getJsonObject(String value) {
		try {
			return new JSONObject(value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getFileName(String fileName) {
		int indexLastDoll = fileName.lastIndexOf(".");
		if (indexLastDoll != -1) {
			return fileName.substring(0, indexLastDoll);
		}
		return fileName;
	}

	public static String getJsonString(JSONObject json, String fileName) {
		String value = "";
		try {
			value = json.getString(fileName);
		} catch (JSONException e) {

		}
		return value;
	}

	public static double getJsonDouble(JSONObject json, String fileName) {
		double result = 0.0d;
		try {
			result = Double.parseDouble(json.getString(fileName));
		} catch (JSONException e) {
		}
		return result;
	}

	public static int getJsonInt(JSONObject json, String fileName) {
		int result = 0;
		try {
			result = Integer.parseInt(json.getString(fileName));
		} catch (JSONException e) {
		}
		return result;
	}

	public static JSONObject getJsonObjectFromArray(JSONArray array, int index) {
		JSONObject returnJson = null;
		try {
			return array.getJSONObject(index);
		} catch (JSONException e) {
		}
		return returnJson;
	}

	public static Long getJsonLong(JSONObject json, String fileName) {
		Long result = 0l;
		try {
			result = Long.parseLong(json.getString(fileName));
		} catch (JSONException e) {
		}
		return result;
	}

	/**
	 * DBRow[] toJsonString
	 * 

	 * @return
	 */
	public static String convertDBRowArrayToJsonString(DBRow[] data)
			throws Exception {
		String value = "";
		if (data != null && data.length > 0) {
			DBRow[] dbrows = data;
			JSONArray array = new JSONArray();
			for (DBRow temp : dbrows) {
				array.put(getJSON(temp));
			}
			value = array.toString();
		}
		return value;
	}

	/**
	 * 把DBRow[] 或者是 DBRow 转换成JSON.toString();
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static String convertDBRowsToJsonString(DBRow... data)
			throws Exception {
		String value = "";
		if (data != null && data.length == 1) {
			DBRow dbrowData = (DBRow) data[0];
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData
					.getFieldNames();
			for (String fileName : filedNames) {
				Object inner = dbrowData.getValue(fileName);

				if (inner instanceof DBRow[]) {
					json.put(fileName.toLowerCase(), getJSON(inner));
				} else if (inner instanceof DBRow) {
					json.put(fileName.toLowerCase(), getJSON(inner));
				} else {

					json.put(fileName.toLowerCase(), inner);
				}

			}
			value = json.toString();
		}
		if (data != null && data.length > 1) {
			DBRow[] dbrows = data;
			JSONArray array = new JSONArray();
			for (DBRow temp : dbrows) {
				array.put(getJSON(temp));
			}
			value = array.toString();
		}

		return value;
	}

	/**
	 * 把DBRow[] 或者是 DBRow 转换成JSON.toString(); key 是大写
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static String convertDBRowsToJsonStringKeyIsUpperCase(DBRow... data)
			throws Exception {
		String value = "";
		if (data != null && data.length == 1) {
			DBRow dbrowData = (DBRow) data[0];
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData
					.getFieldNames();
			for (String fileName : filedNames) {
				Object inner = dbrowData.getValue(fileName);

				if (inner instanceof DBRow[]) {
					json.put(fileName.toUpperCase(), getJSON(inner));
				} else if (inner instanceof DBRow) {
					json.put(fileName.toUpperCase(), getJSON(inner));
				} else {

					json.put(fileName.toUpperCase(), inner);
				}

			}
			value = json.toString();
		}
		if (data != null && data.length > 1) {
			DBRow[] dbrows = data;
			JSONArray array = new JSONArray();
			for (DBRow temp : dbrows) {
				array.put(getJSON(temp));
			}
			value = array.toString();
		}

		return value;
	}

	public static Object getJSON(Object data) throws Exception {
		Object obj = new Object();
		if (data instanceof DBRow[]) {
			DBRow[] datas = (DBRow[]) data;
			JSONArray jas = new JSONArray();
			for (DBRow temp : datas) {
				jas.put(getJSON(temp));
			}
			obj = jas;
		} else if (data instanceof DBRow) {
			DBRow dbrowData = (DBRow) data;
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData
					.getFieldNames();
			for (String fileName : filedNames) {
				json.put(fileName.toLowerCase(),
						getJSON(dbrowData.getValue(fileName)));
			}
			obj = json;
		} else {
			obj = data;
		}
		return obj;
	}

	public static String parseIndexTo4Str(int i) {
		String iStr = String.valueOf(i);
		String l = "";
		if (iStr.length() < 4) {
			int c = 4 - iStr.length();
			for (int j = 0; j < c; j++) {
				l += "0";
			}
		}
		return l + iStr;
	}

	public static String fromStringGetNumber(String str) {
		String reStr = "";
		if (!isBlank(str)) {
			String regEx = "[^0-9]";
			Pattern p = Pattern.compile(regEx);
			Matcher m = p.matcher(str);
			reStr = m.replaceAll("").trim();
		}
		return reStr;
	}

	public static final String replaceEnterToBlank(String s) {
		if ((s == null || s.length() == 0)) {
			return s;
		} else {
			s = replaceString(s, "\r\n", "");
			s = replaceString(s, "\n", "");
			return s;
		}
	}

	/**
	 * 判断字条串是否为空，且是否可以转换成long型
	 * 
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static boolean isBlankAndCanParseLongOriginal(String str)
			throws Exception {
		if (str != null && str.trim().length() != 0 && !"undefined".equals(str)) {
			boolean isCan = true;
			try {
				Long.parseLong(str);
			} catch (NumberFormatException e) {
				isCan = false;
			}
			return isCan;
		} else {
			return true;
		}
	}

	public static boolean isNumber(String str) {
		Matcher matcher = NumberPattern.matcher(str);
		return matcher.matches();
	}

	private static int covertChar2PhotoNumber(char c) {
		int covertInt = (int) c;
		int returnInt = 0;
		if (covertInt <= (int) 'C' && covertInt >= (int) 'A') {
			return 2;
		}
		if (covertInt <= (int) 'F' && covertInt >= (int) 'D') {
			return 3;
		}
		if (covertInt <= (int) 'I' && covertInt >= (int) 'G') {
			return 4;
		}
		if (covertInt <= (int) 'L' && covertInt >= (int) 'J') {
			return 5;
		}
		if (covertInt <= (int) 'O' && covertInt >= (int) 'M') {
			return 6;
		}
		if (covertInt <= (int) 'S' && covertInt >= (int) 'P') {
			return 7;
		}
		if (covertInt <= (int) 'V' && covertInt >= (int) 'T') {
			return 8;
		}
		if (covertInt <= (int) 'Z' && covertInt >= (int) 'W') {
			return 9;
		}
		return returnInt;
	}

	public static String convertStr2PhotoNumber(String value) {
		if (value != null && value.length() > 0) {
			value = value.toUpperCase();
			StringBuilder arraysChars = new StringBuilder();
			for (int index = 0, count = value.length(); index < count; index++) {
				char a = value.charAt(index);
				if (isNumber(String.valueOf(a))) {
					arraysChars.append(a);
				} else {
					arraysChars.append(covertChar2PhotoNumber(a));
				}
			}
			return arraysChars.toString();
		}
		return "";
	}

	/**
	 * 通过entry_id去打印Plate 生成112062001
	 * 
	 * @param number
	 * @return
	 * @author zhangrui
	 * @Date 2014年10月16日
	 */
	public static String fixContianerPlateNumber(int number) {
		if (number <= 9) {
			return "00" + number;
		}
		if (number <= 100) {
			return "0" + number;
		}
		return String.valueOf(number);
	}

	public static String MergerTemplate(String template, DBRow row) {
		return MergerTemplate(template, row, "{", "}");
	}

	public static String MergerTemplate(String template, DBRow row,String symbolStart, String symbolEnd) {
		String strData = "";
		if (template != null && row != null) {
			strData = template;
			Pattern p = Pattern.compile("\\" + symbolStart + ".*?\\" + symbolEnd);
			Matcher m = p.matcher(template);
			while (m.find()) {
				try {
					String field = "";
					String value = "";
					String defaultValue = "";
					String formate = "";
					String placeHolder = m.group().toString();
					if (placeHolder.indexOf("|") != -1) {
						field = placeHolder.split("\\|")[0].replace(symbolStart, "");
						formate = placeHolder.split("\\|")[1].replace(symbolEnd, "");
					} else if (placeHolder.indexOf(",") != -1) {
						field = placeHolder.split(",")[0].replace(symbolStart,"");
						defaultValue = placeHolder.split(",")[1].replace(symbolEnd, "");
					} else {
						field = placeHolder.replace(symbolStart, "").replace(symbolEnd, "");
					}
					if (!row.containsKey(field))
						continue;
					
					value = row.getString(field, defaultValue);
					if (!StringUtil.isBlank(formate) && !StringUtil.isBlank(value)) {
						value = DateUtil.parseDateFormat(value, formate);
					}
					strData = StringUtil.replaceString(strData, placeHolder,value);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}

		}
		return strData;
	}

	public static String generate(String productName, String hmacSecret,String path,String localTime) throws JsonProcessingException {
		String messageHash = hmacSha256(hmacSecret, MessageFormat.format(HMAC_MESSAGE_FORMAT, localTime, path));
		return MessageFormat.format(HMAC_AUTH_FORMAT, productName, messageHash);
	}

	public static String getLocalTime() throws Exception {
		java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		return format.format(new Date());
	}
	



	
	
	/*public static String toISOFormat(LocalDateTime time) throws JsonProcessingException {
		
		return OBJECT_MAPPER.writerWithDefaultPrettyPrinter()
				.writeValueAsString(time)
				.replace("\"", "");
        
    }*/
	
	public static String hmacSha256(String secretKey, String message) {
		try {
			SecretKeySpec sha256Key = new SecretKeySpec(secretKey.getBytes(),"HmacSHA256");
			Mac sha256HMAC = Mac.getInstance("HmacSHA256");
			sha256HMAC.init(sha256Key);
			return org.apache.commons.codec.binary.Base64.encodeBase64String(sha256HMAC.doFinal(message.getBytes()));

		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			throw new RuntimeException("Error hashing message using HMAC SHA256", e);
		}
	}
	
	

}
