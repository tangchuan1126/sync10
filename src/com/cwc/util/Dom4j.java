package com.cwc.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 * dom4j操作xml
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Dom4j 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	/**
	 * 获得document
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public Document getDocument(String fileName)
		throws Exception
	{
		try 
		{
			SAXReader reader = new SAXReader();
			Document document = reader.read(new File(fileName));
			return(document);
		}
		catch (DocumentException e)
		{
			log.error("Dom4j.getDocument DocumentException:" + e);
			throw new Exception("Dom4j.getDocument DocumentException:" + e);
		}
	}
	
	public Document getDocumentForString(String xml)
		throws Exception
	{
		try 
		{
//			SAXReader reader = new SAXReader();
			Document document = DocumentHelper.parseText(xml);//reader.read(new ByteArrayInputStream(xml.getBytes("UTF-8")));
			
			return (document);
		}
		catch (Exception e) 
		{
			log.error("Dom4j.getDocument DocumentException:" + e);
			throw new Exception("Dom4j.getDocument DocumentException:" + e);
		}
	}

	/**
	 * 获得XML跟
	 * @param doc
	 * @return
	 */
	public Element getRootElement(Document doc)
	{
		return(doc.getRootElement());
	}

	/**
	 * 把所有元素转换为arraylisy
	 * @param root
	 * @return
	 */
	public ArrayList getElement(Element root)
	{
		ArrayList elementAl = new ArrayList();
		
		for( Iterator i = root.elementIterator();i.hasNext(); )
		{
			Element element = (Element) i.next();
			elementAl.add(element);
	    }
		
		return(elementAl);
	}

	/**
	 * 通过XPATH查找元素
	 * @param doc
	 * @param xPath
	 */
	public void query(Document doc,String xPath)
	{
		List list = doc.selectNodes(xPath);
		for (Iterator iter = list.iterator();iter.hasNext();) 
		{
			//system.out.println(iter.next());
		}
        //Node node = doc.selectSingleNode(xPath);
        //String name = node.valueOf("");
	}

	/**
	 * 把xml回写文件
	 * @param doc
	 * @param filePath
	 * @param encoding
	 */
	public void writeToFile(Document doc, String filePath,String encoding) 
	{
		try
		{
			OutputFormat fmt = OutputFormat.createPrettyPrint();
			fmt.setEncoding(encoding);

			XMLWriter xmlWriter = new XMLWriter(new OutputStreamWriter(	new FileOutputStream(filePath), encoding), fmt);
			xmlWriter.write(doc);
			xmlWriter.close();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String args[])
		throws Exception
	{
		Dom4j dom4j = new Dom4j();
		
		//system.out.println(dom4j.getDocument("d:/url_mapping.xml"));
		Document doc = dom4j.getDocument("d:/url_mapping.xml");
		Element root = dom4j.getRootElement(doc);
		dom4j.getElement(root);
		//root.accept(new Dom4jVisitor());
		//dom4j.query(doc,"//application/@class");
		
		ArrayList applicationElementAl = dom4j.getElement(root);
		for (int i=0; i<applicationElementAl.size(); i++)
		{
			ArrayList al = dom4j.getElement( (Element)applicationElementAl.get(i) );
			for (int j=0; j<al.size(); j++)
			{
				Element ne = (Element)al.get(j);
				//system.out.println( ne.getText() );
			}
			//system.out.println("");
		}
		
		HashMap hm = new HashMap();
		hm.put("/detail.do","123");
		//system.out.println(hm.get("/detail.do"));
		
	}
}




