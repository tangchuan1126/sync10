package com.cwc.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.log4j.Logger;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

public class CompressFile
{
	static Logger log = Logger.getLogger("PLATFORM");
	
		/**
		 * 压缩文件夹
		 * @param inputFileName 		格式：d:/20100109154513
		 * @param outputFileName		格式：d:/20100109154513
		 * @throws Exception
		 */
	    public final static void zipFolder(String inputFileName,String outputFileName) 
	    	throws Exception 
	    {
	    	outputFileName = outputFileName+".zip";
	        zip(outputFileName, new File(inputFileName));
	    }
	    
	    /**
	     * 压缩文件/文件夹
	     * @param inputFileName		格式：d:/123或d:/123/txt
	     * @param outputFileName	格式：d:/123
	     * @throws Exception
	     */
	    public final static void zip(String inputFileName,String outputFileName) 
	    	throws Exception 
	    {
	    	File inF = new File(inputFileName);
	    	
	    	if (inF.isFile())
	    	{
	    		zipFile( inputFileName, outputFileName);
	    	}
	    	else
	    	{
	    		zipFolder(inputFileName,outputFileName);
	    	}
	    }
	    
	    /**
	     * 压缩文件
	     * @param inputFileName			
	     * @param outputFileName		
	     * @throws Exception
	     */
	    public  final static void zipFile(String inputFileName,String outputFileName) 
	    	throws Exception 
	    {
	        File f = new File(inputFileName);
	        FileInputStream fis = new FileInputStream(f);
	        BufferedInputStream bis = new BufferedInputStream(fis);
	        byte[] buf = new byte[1024];
	        int len;
	        FileOutputStream fos = new FileOutputStream(outputFileName +".zip");
	        BufferedOutputStream bos = new BufferedOutputStream(fos);
	        java.util.zip.ZipOutputStream zos = new java.util.zip.ZipOutputStream(bos);//压缩包
	        java.util.zip.ZipEntry ze = new java.util.zip.ZipEntry(f.getName());//这是压缩包名里的文件名
	        zos.putNextEntry(ze);//写入新的 ZIP 文件条目并将流定位到条目数据的开始处

	        while((len=bis.read(buf))!=-1)
	        {
	           zos.write(buf,0,len);
	           zos.flush();
	        }
	        bis.close();
	        zos.close(); 
	    }
	    
	    /**
	     * 解压缩文件/文件夹
	     * @param unZipfileName		格式:d:/11.zip
	     * @param uzipBasePath		格式:d:/123或者d:
	     * @param dirUnzip			true:解压到文件夹，false：解压到当前闻之；是否解压到当前位置还是解压到当前文件名的文件夹
	     */
	    public  final static void unZip(String unZipfileName,String uzipBasePath,boolean dirUnzip)
	    {
	        FileOutputStream fileOut; 
	        File file; 
	        InputStream inputStream; 
	        byte[] buf = new byte[512]; 
	        int readedBytes;

	        try
	        { 
	        	ZipFile zipFile = new ZipFile(unZipfileName); 
	        	
	        	if ( (uzipBasePath.length()-1)!=uzipBasePath.lastIndexOf("/") )
	        	{
	        		uzipBasePath = uzipBasePath + "/";
	        	}
	        	
	        	//是否解压到当前文件夹
	        	if (dirUnzip)
	        	{
	        		//获得压缩文件名
	        		File filename = new File(unZipfileName);
	        		String unzipFolderName = filename.getName();
	        		unzipFolderName = unzipFolderName.substring(0, unzipFolderName.lastIndexOf("."));
	        		uzipBasePath = uzipBasePath+unzipFolderName+"/";
	        		
	        		//创建解压文件夹
	        		File unzipFolder = new File(uzipBasePath);
	        		if (!unzipFolder.exists())
	        		{
	        			unzipFolder.mkdir();
	        		}
	        	}
	        	
	            for(Enumeration entries = zipFile.getEntries(); entries.hasMoreElements();)
	            { 
	                ZipEntry entry = (ZipEntry)entries.nextElement(); 
	                file = new File(uzipBasePath+entry.getName()); 

	                if(entry.isDirectory())
	                { 
	                    file.mkdirs(); 
	                } 
	                else
	                { 
	                    //如果指定文件的目录不存在,则创建之. 
	                    File parent = file.getParentFile(); 
	                    if(parent!=null&&!parent.exists())
	                    { 
	                        parent.mkdirs(); 
	                    } 

	                    log.warn(uzipBasePath+entry.getName()+"......");
	                    long st = System.currentTimeMillis();
	                    
	                    
	                    inputStream = zipFile.getInputStream(entry); 

	                    fileOut = new FileOutputStream(file); 
	                    while(( readedBytes = inputStream.read(buf) ) > 0)
	                    { 
	                        fileOut.write(buf , 0 , readedBytes ); 
	                    } 
	                    fileOut.close(); 

	                    inputStream.close(); 
	                    
	                    
	                    long en = System.currentTimeMillis();
	                    log.warn("["+ (en-st)/1000 +" sec]");
	                }    
	            } 
	            zipFile.close();
	        }
	        catch(IOException ioe)
	        { 
	            ioe.printStackTrace(); 
	        } 
	    } 

	    
	    
	    
	    
	    
	    
	    
	    
	    private  final static void zip(String zipFileName, File inputFile) 
	    	throws Exception 
	    {
	        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
	        zip(out, inputFile, "");
	        out.close();
	    }

	    private  final static void zip(ZipOutputStream out, File f, String base)
	    	throws Exception 
	    {
	        if (f.isDirectory()) 
	        {
	           File[] fl = f.listFiles();
	           out.putNextEntry(new org.apache.tools.zip.ZipEntry(base + "/"));
	           base = base.length() == 0 ? "" : base + "/";
	           for (int i = 0; i < fl.length; i++)
	           {
	        	   zip(out, fl[i], base + fl[i].getName());
	           }
	        }
	        else
	        {
	        	log.warn(base+"......");
	        	
	           long st = System.currentTimeMillis();
	           
	           out.putNextEntry(new org.apache.tools.zip.ZipEntry(base));
	           FileInputStream in = new FileInputStream(f);
	           int b;
	           while ( (b = in.read()) != -1) 
	           {
	        	   out.write(b);
	           }
	           in.close();
	           
	           long en = System.currentTimeMillis();
	           log.warn("["+ (en-st)/1000 +" sec]");
	       }
	    }

	    public static void main(String [] temp)
	    {
	        try 
	        {
	        	//CompressFile.zipFolder("d:/taobaoshop","d:/taobaoshop");//你要压缩的文件夹
	        	//CompressFile.zipFile("d:/1.jpg","d:/1");
	        	//CompressFile.unZip("d:/Toad9.5.zip","d:/",true);
	        	//CompressFile.unZip("d:/1.zip","d:",false);
	        	
	        	CompressFile.zip("d:/123","d:/123/");
	        }
	        catch (Exception ex) 
	        {
	           ex.printStackTrace();
	       }
	    }
}
