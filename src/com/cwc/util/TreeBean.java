package com.cwc.util;

import com.cwc.db.DBRow;

/**
 * 配合tree
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TreeBean
{
	private int id = 0;
	private int parentid = 0;
	private int nextIndex = -1;
	private int level = 0;
	private DBRow rows = null;;

	public int getID()
	{
		return id;
	}

	public int getLevel()
	{
		return level;
	}

	public int getNextIndex()
	{
		return nextIndex;
	}
	public int getParentID()
	{
		return parentid;
	}

	public void setID(int i)
	{
		id = i;
	}
	public void setLevel(int i)
	{
		level = i;
	}
	public void setNextIndex(int i)
	{
		nextIndex = i;
	}
	public void setParentID(int i)
	{
		parentid = i;
	}

	public DBRow getRows()
	{
		return rows;
	}

	public void setRows(DBRow rows)
	{
		this.rows = rows;
	}

}
