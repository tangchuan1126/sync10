package com.cwc.util;//ע���޸���Ӧ�İ�·��

import javax.mail.*;

/**
 * smtp验证登录类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SmtpAuthenticator extends Authenticator {
   String username=null;
   String password=null;

   public SmtpAuthenticator(String username,String password) {
       this.username=username;
       this.password=password;
   }

   public PasswordAuthentication getPasswordAuthentication() {
	return new  PasswordAuthentication(this.username,this.password);
   }
}