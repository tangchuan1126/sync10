package com.cwc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import com.cwc.app.util.Environment;
import com.cwc.app.util.Handler;
import com.cwc.db.DBRow;

public class AndroidUtil implements Handler
{
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws DataFormatException 说明XML解析错误
	 * Map<String,Object> returnMap ;
	 * 	key:			valueType : 
	 * 		LoginAccount	String
	 * 		Password		String
	 * 		Machine 		String	
	 * 		Method(fc)		String 
	 * 		containers 		DBRow[] [con_id ,container_type,container_type_id,parent_con_id,parent_container_type,parent_container_type_id,title_id,lot_number] 
	 * 		products		DBRow[]	[cp_sn,cp_quantity,cp_pc_id,cp_lp_id,title_id]
	 *		allContainerIds Long[]
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getDataFromAndroidXml(InputStream inputStream) throws DataFormatException,Exception{
		Map<String , Object> returnMap = new HashMap<String, Object>();
 	 //	InputStream inputStream = request.getInputStream();
	    // 测试使用
	//	inputStream =  new FileInputStream(new File("D:\\ZR\\log-container-20140326-zyj.txt"));

		try
		{
			List<DBRow> containersList = new ArrayList<DBRow>();
		  	List<DBRow> containerProducts = new ArrayList<DBRow>();
		  	List<Long>	allContainerIds = new ArrayList<Long>();
	        SAXReader reader = new SAXReader();  
			Document document = reader.read(inputStream);
			Element root = document.getRootElement();
			
			//>>>>>>>>>>>>>>添加基础信息<<<<<<<<<<<<<<<<<<<<
			List<Node> listLoginAccount = 	root.selectNodes("LoginAccount");
			List<Node> listPassword = 	root.selectNodes("Password");
			List<Node> listMachine = 	root.selectNodes("Machine");
			List<Node> listMethod = 	root.selectNodes("fc");
			
			returnMap.put("LoginAccount", listLoginAccount.get(0).getText());
			returnMap.put("Password", listPassword.get(0).getText());
			returnMap.put("Machine", listMachine.get(0).getText());
			returnMap.put("Method", listMethod.get(0).getText()) ;
			//>>>>>>>>>>>>>>添加基础信息结束<<<<<<<<<<<<<<<<<<
			
			
			//>>>>>>>>>>>>>>添加container 和  products 信息
			 Element details =	root.element("Detail").element("Details");
			 if(details != null){
				 List<Element> containers =  details.elements("Container");
				 if(containers != null && containers.size() > 0 ){
 						Element container = containers.get(0);
 						if(container != null){ 
							getContainerElement(container, containersList, containerProducts,allContainerIds);
						}
							 
				 }
			 }
			 returnMap.put("containers", containersList.toArray(new DBRow[containersList.size()]));
			 returnMap.put("products", containerProducts.toArray(new DBRow[containerProducts.size()]));
			 returnMap.put("allContainerIds", allContainerIds.toArray(new Long[0]));

	  	}catch (Exception e)
	  	{
	  		throw new DataFormatException();
 		}
		finally
		{
 			if(inputStream != null){inputStream.close();}
 		}
		return returnMap ;
	}
	/**
	 * 一个递归的方法
	 * @param containerElement
	 * @return
	 */
	private static void getContainerElement(Element containerElement , List<DBRow> containers , List<DBRow> products , List<Long> allContainerIds ){
		 DBRow returnRow = new DBRow();
		 long con_id = 	Long.parseLong((containerElement.attributeValue("con_id")));
		 int container_type = Integer.parseInt((containerElement.attributeValue("container_type")));
		 long container_type_id = Long.parseLong((containerElement.attributeValue("container_type_id")));
		 long parent_con_id = Long.parseLong((containerElement.attributeValue("parent_con_id")));
		 int parent_container_type = Integer.parseInt((containerElement.attributeValue("parent_container_type")));
		 long parent_container_type_id = Long.parseLong((containerElement.attributeValue("parent_container_type_id")));
		 	
		 long titleId = Long.parseLong((containerElement.attributeValue("title_id")));
		 String lotNumber = containerElement.attributeValue("lotNumber");
		 returnRow.add("con_id", con_id);
		 returnRow.add("container_type", container_type);
		 returnRow.add("container_type_id", container_type_id);
		 returnRow.add("parent_con_id", parent_con_id);
		 returnRow.add("parent_container_type", parent_container_type);
		 returnRow.add("parent_container_type_id", parent_container_type_id);
		 returnRow.add("title_id", titleId);
		 returnRow.add("lot_number", lotNumber);
		 allContainerIds.add(con_id);
		 containers.add(returnRow);
		 List<Element> productsNodes = containerElement.elements("Product");
		 if(productsNodes != null && productsNodes.size() > 0 ){
			 for(Element productNode : productsNodes){
				 String sn = productNode.attributeValue("sn");
				 double cp_quantity = Double.parseDouble(productNode.attributeValue("qty"));
				 long cp_pc_id = Long.parseLong(productNode.attributeValue("pcid"));
 				 DBRow productRow = new DBRow();
				 productRow.add("cp_sn", sn);
				 productRow.add("cp_quantity", cp_quantity);
				 productRow.add("cp_pc_id", cp_pc_id);
				 productRow.add("cp_lp_id", con_id);
				 productRow.add("title_id", titleId);
 				 products.add(productRow);
 			}
		 }
		 List<Element> subContainerElements = containerElement.elements("Container");
		 if(subContainerElements != null && subContainerElements.size() > 0 ){
			 for(Element subContainerElement :  subContainerElements){
				 getContainerElement(subContainerElement, containers, products,allContainerIds);
			 }
		 }
 	}
	/**
	 * 添加获取服务器端android的版本
	 * 读取文件，返回version
	 * @return
	 * @throws Exception 
	 */
	public static String getVersion() throws Exception{
		String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/android/androidVerion.xml";
		String xml =	FileUtils.readFileToString(new File(fileUploadPath));
		if(!StringUtil.isNull(xml)){
			return StringUtil.getSampleNode(xml, "Value");
		}
		return ""; 
 	}
	
	/**
	 * android screen version
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public static String getScreenVersion() throws Exception{
		String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/android/androidVerion.xml";
		String xml =	FileUtils.readFileToString(new File(fileUploadPath));
		if(!StringUtil.isNull(xml)){
			return StringUtil.getSampleNode(xml, "ScreenValue");
		}
		return "";
	}
	
	/**
	 * 清除printServer 不需要返回的数据
	 * @param rows
	 * @throws Exception
	 */
	public static void fixPrintServerData(DBRow[] rows) throws Exception{
	  if(rows != null && rows.length > 0){
		  for(DBRow row : rows){
			  row.remove("adid");
			  row.remove("last_refresh_time");
			  row.remove("title");
			  row.remove("employe_name");
			  row.remove("ps_id");
		  }
	  }
	}
}
