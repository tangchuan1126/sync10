package com.cwc.util;

import org.dom4j.Document;
import org.dom4j.Element;

import com.cwc.db.DBRow;

public class XmlUtil {
	
	public static Document getXMLDoc(String path)
    	throws Exception
    {
    	Dom4j dom4j = new Dom4j();
    	Document doc = dom4j.getDocument(path);
    	return(doc);
    }
    	
    public static Document getXMLDocForStringXML(String xml)
    	throws Exception
    {
    	Dom4j dom4j = new Dom4j();
    	Document doc = dom4j.getDocumentForString(xml);
    	return(doc);
    }
    
    public static DBRow initDataXmlStringToDBRow(String xmlString,String[] nodeNames)
    	throws Exception
    {
    	Document document = XmlUtil.getXMLDocForStringXML(xmlString);
    	
    	DBRow row = new DBRow();
    	for (int i = 0; i < nodeNames.length; i++) 
    	{
    		String nodeName = "//"+nodeNames[i];
    		Element element = (Element)document.selectSingleNode(nodeName);
    		String value = element.getStringValue().replaceAll(" ","");
    		//String value = element.getStringValue();

    		
    		row.add(nodeNames[i].toLowerCase(),value);
		}
    	
    	return row;
    }
    
    //去掉两边空格
    public static DBRow initDataXmlStringToDBRowCategory(String xmlString,String[] nodeNames) throws Exception {
        	
    	Document document = XmlUtil.getXMLDocForStringXML(xmlString);
        	
    	DBRow row = new DBRow();
    	for (int i = 0; i < nodeNames.length; i++) {
    		
    		String nodeName = "//"+nodeNames[i];
    		Element element = (Element)document.selectSingleNode(nodeName);
    		String value = element.getStringValue().trim();
    		
    		row.add(nodeNames[i].toLowerCase(),value);
		}
    	
    	return row;
	}
    
    public static DBRow initDataXmlHasSpaceStringToDBRow(String xmlString,String[] nodeNames)
        	throws Exception
        {
        	Document document = XmlUtil.getXMLDocForStringXML(xmlString);
        	
        	DBRow row = new DBRow();
        	for (int i = 0; i < nodeNames.length; i++) 
        	{
        		String nodeName = "//"+nodeNames[i];
        		Element element = (Element)document.selectSingleNode(nodeName);
        		String value = element.getStringValue();
        		row.add(nodeNames[i].toLowerCase(),value);
    		}
        	
        	return row;
        }
    
    public static void main(String[] args)
    throws Exception
    {
		XmlUtil xmlUtil = new XmlUtil();
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Account>zhanjie</Account><Password>zhanjie</Password><Sheet>SKU-CATEGORY-TITLE</Sheet><Data><Pname>S5430PCTRS/24000009</Pname><KeyCode>24000009</KeyCode><Category>SOUNDBAR</Category><UPC>845226011368</UPC><UOM>PIECE</UOM><PieceDepth>48</PieceDepth><PieceHeight>29.19</PieceHeight><PieceWidth>40</PieceWidth><WeightPerUnit>16.50</WeightPerUnit><UnitCost>20</UnitCost><Class>100</Class><NMFCCode>63036</NMFCCode></Data></Request>";
		String xml1      = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Account>zhanjie</Account><Password>zhanjie</Password><Sheet>TITLE</Sheet><Data><Title>AMTRAN</Title></Data></Request>";
		String[] nodeNames = new String[]{"Password","Title"};
		DBRow row = xmlUtil.initDataXmlStringToDBRow(xml1, nodeNames);
		
		//system.out.println(row.getString("title"));
		//system.out.println(row.getString("password"));
	}
}
