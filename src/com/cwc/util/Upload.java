package com.cwc.util;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.cwc.initconf.Resource;
import com.jspsmart.upload.*;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Upload
{
	private HashMap fileNames = new HashMap();
	private ArrayList fileSize = new ArrayList();
	private ArrayList fileExtend = new ArrayList();
	private ArrayList filePath = new ArrayList();
	private String uploadPath = null;
	private int uploadCount = 0;
	private SmartUpload smartUpload;
	
	private String permitUpFile;
	int permitFileSize;
	String filename = null;
	boolean autoChangeFileName = true;
	
	public Upload()
	{
		smartUpload = new SmartUpload();
	}

	public void autoChangeFileName(boolean flag)
	{
		autoChangeFileName = flag;
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public String save()
		throws Exception
	{
		String exceptionPath = "";
		
		try 
		{
			String msg = "";
			smartUpload.upload();
			uploadCount = smartUpload.getFiles().getCount();
			
			String lastFileName;
			
			for (int i=0;i<uploadCount;i++)
			{
				com.jspsmart.upload.File uploadFile = smartUpload.getFiles().getFile(i);
				if ( !uploadFile.isMissing() )
				{
					if ( getPermitUpFile().indexOf(uploadFile.getFileExt().toLowerCase() ) == -1 )
					{
						msg = uploadFile.getFileExt() + " "+Resource.getStringValue("","class.upload.filetype_outside","null")+ getPermitUpFile();
						break;
					}
					else if ( uploadFile.getSize()>getPermitFileSize() )
					{
						msg =  uploadFile.getSize() + " "+Resource.getStringValue("","class.upload.filesize_outside","null")+" " + getPermitFileSize();
						break;
					}
					else
					{
						String defFileName = getDefFileName();
						if ( autoChangeFileName )
						{
							lastFileName = defFileName + String.valueOf(i) + "." + uploadFile.getFileExt();
						}
						else
						{
							lastFileName = uploadFile.getFileName();
						}
						
						exceptionPath = uploadPath +  lastFileName;
					
						uploadFile.saveAs(exceptionPath);
						fileNames.put(uploadFile.getFieldName(),lastFileName);
						fileSize.add(new Integer(uploadFile.getSize()));
						fileExtend.add(uploadFile.getFileExt());
						filePath.add(uploadFile.getFilePathName());
					}
				}
				else
				{
					//msg = "��ѡ���ļ��ϴ�";
					continue;
				}
			}
			
			return(msg);
		} 
		catch (Exception e)
		{
			throw new ServletException("save(" + exceptionPath + ")" + e);
		}
	}

	public String saveAsSameName()
		throws Exception
	{
		String exceptionPath = "";
		
		try 
		{
			String msg = "";
			smartUpload.upload();
			uploadCount = smartUpload.getFiles().getCount();
			
			for (int i=0;i<uploadCount;i++)
			{
				com.jspsmart.upload.File uploadFile = smartUpload.getFiles().getFile(i);
				if ( !uploadFile.isMissing() )
				{
					if ( getPermitUpFile().indexOf(uploadFile.getFileExt().toLowerCase() ) == -1 )
					{
						msg = uploadFile.getFileExt() + " 不在允许上传的文件类型：" + getPermitUpFile() + " 范围内";
						break;
					}
					else if ( uploadFile.getSize()>getPermitFileSize() )
					{
						msg = "文件大小 " + uploadFile.getSize() + " 超过 " + getPermitFileSize() + " 规定范围";
						break;
					}
					else
					{
						//String defFileName = getDefFileName();
						String defFileName;
						if ( getFileName()!=null )
						{
							defFileName = getFileName();
						}
						else
						{
							defFileName = uploadFile.getFileName();
						}
						
						exceptionPath = uploadPath + defFileName ;
					
						uploadFile.saveAs(exceptionPath);
						fileNames.put(uploadFile.getFieldName(),defFileName);
						fileSize.add(new Integer(uploadFile.getSize()));
						fileExtend.add(uploadFile.getFileExt());
						filePath.add(uploadFile.getFilePathName());
					}
				}
				else
				{
					continue;
				}
			}
			
			return(msg);
		} 
		catch (Exception e)
		{
			throw new ServletException("save(" + exceptionPath + ")" + e);
		}
	}

	private String getDefFileName()
	{
		if ( getFileName()!=null )
		{
			return(getFileName());
		}
		else
		{
			long time = (new java.util.Date()).getTime();
			return(String.valueOf(time));	
		}
	}

	public void setPageContext(PageContext p)
		throws ServletException 
	{
		try
		{
			smartUpload.initialize(p);
		}
		catch (ServletException e)
		{
			throw new ServletException("setPageContext(" + p + ")" + e.getMessage());
		}
	}

	public void setUploadPath(String string)
	{
		uploadPath = string;
	}
	public String getUploadPath()
	{
		return uploadPath;
	}
	
	public String getString(String key)
	{
		if ( smartUpload.getRequest().getParameter(key) == null )
		{
			return("");
		}
		return(smartUpload.getRequest().getParameter(key));
	}
	
	public int getInt(String key)
	{
		int t;
		try
		{
			t = new Integer(smartUpload.getRequest().getParameter(key)).intValue();
		}
		catch (NumberFormatException e)
		{
			t = 0;
		}
		return(t);
	}
	
	public float getFloat(String key)
	{
		float t = 0;
		
		if ( smartUpload.getRequest().getParameter(key) == null )
		{
			return(0);
		}
				
		try
		{
			t = Float.parseFloat(smartUpload.getRequest().getParameter(key));
		}
		catch (NumberFormatException e)
		{
		}
		return(t);
	}
	
	public ArrayList getFileExtend()
	{
		return fileExtend;
	}
	public HashMap getFileNames()
	{
		return fileNames;
	}
	public ArrayList getFilePath()
	{
		return filePath;
	}
	public ArrayList getFileSize()
	{
		return fileSize;
	}

	public String getPermitUpFile()
	{
		return permitUpFile;
	}
	public void setPermitUpFile(String string)
	{
		permitUpFile = string;
	}

	public int getPermitFileSize()
	{
		return permitFileSize;
	}
	public void setPermitFileSize(int i)
	{
		permitFileSize = i;
	}

	public String getFileName()
	{
		return(filename);
	}
	
	public void setFileName(String fn)
	{
		filename = fn;
	}

}
