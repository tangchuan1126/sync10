package com.cwc.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 

 
 
public class ReflectUtil {
	
	
	 
	public static Map<String,String[]> returnTitleAndAWidthOfSamplePoiAnnotation(Class clazz){
		Field[] fields = clazz.getDeclaredFields();
		List<String> titles = new ArrayList<String>();
		List<String> widths = new ArrayList<String>();
		List<String> colors = new ArrayList<String>();
		List<String> isHidden = new ArrayList<String>();
		Map<String,String[]> map = new HashMap<String, String[]>();
		for(Field field :  fields){
			SamplePoiAnnotation annotation = field.getAnnotation(SamplePoiAnnotation.class);
			if(annotation != null){
				titles.add( annotation.title());
				widths.add( annotation.width());
				colors.add( annotation.color());
				isHidden.add(annotation.isHidden());
			}
		}
		int size = titles.size();
		String[] StringArrayTitles = new String[size];
		String[] StringArrayWidths = new String[size];
		for(int i = 0 , count = size ; i < count ; i++ ){
			StringArrayTitles[i] = titles.get(i);
			StringArrayWidths[i] = widths.get(i);
		}
		map.put("titles", StringArrayTitles);
		map.put("widths", StringArrayWidths);
		map.put("colors", colors.toArray(new String[colors.size()]));
		map.put("isHiddens", isHidden.toArray(new String[isHidden.size()]));
		return  map;
	}
	
	public static List<Method> getSellerInfoMethod(Class clazz) throws NoSuchMethodException, SecurityException{
		Field[] fields = clazz.getDeclaredFields();
		List<String> fieldList = new ArrayList<String>();
		for(Field field : fields){
			String fieldName = field.getName();
			if(
					(fieldName.indexOf("seller") != -1 && !fieldName.equals("seller")) 
				 || (fieldName.indexOf("returnPolicy") != -1 && !fieldName.equals("returnPolicy"))
				 || (fieldName.indexOf("listingType") != -1 && !fieldName.equals("listingType"))
				 || (fieldName.indexOf("bestOffer") == 0 && !fieldName.equals("bestOfferDetails"))
				 || (fieldName.indexOf("itemPolicy") != -1 && !fieldName.equals("itemPolicyViolation"))
				 || (fieldName.indexOf("payMentDetail") != -1 && !fieldName.equals("paymentDetailsType"))
			){
				fieldList.add("get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1));
			}
		}
		List<Method> methodList = new ArrayList<Method>();
		for(String field : fieldList){
			 Method method =	clazz.getMethod(field,new Class[]{});
			 methodList.add(method);
		}
		return methodList;
	 
	}
}
