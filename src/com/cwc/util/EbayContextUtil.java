package com.cwc.util;

import com.ebay.sdk.ApiAccount;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.CallRetry;

public class EbayContextUtil {
	
	private static ApiContext apiContext = new ApiContext();
	private static String apiServerUrl = "https://api.ebay.com/wsapi";
	private static String epsServerUrl = "https://api.ebay.com/ws/api.dll";
	private static String signInUrl = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn";
	private static String developer = "88f39232-2e13-4baf-bbd9-a5694e58f12a";
	private static String application = "Visionar-8d75-480d-aa32-a66d3a89574b";
	private static String certificate = "eb9b8d5b-97b2-4adc-8bc8-9cff565f0607";
	private static String eBayToken = "AgAAAA**AQAAAA**aAAAAA**iINgTg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wDlounCJSHoQSdj6x9nY+seQ**UDABAA**AAMAAA**4VckBPfbpuJA3MEOmMKq1Yqj2I6Rhkdr7aY/m1jE69UEsGQKwkWnPq2KojD+OKuW4chmQODPAuAte02bEyYqDD6JoETXWGkAt+WkKDB895V/ucD5FcIluyDycYxZqP6CYXLoDD9ynIoiigUdBacPINosy+HnivCCUyRxTusDgRo1x8CPCsqf2ZyRMtQdlVkyFz7eqd2an350tF8Q7VDMI3zn+e1m9Fi6nvd1vflnPvgNAcCatNGYxIJoXAWqQdS2yauQSFEJZhsOmN4DHdd3AMmbF/NvlmBU7ZN2hkXzqraQlqKRgoj4tq5ZsgfngUwjcoPiNEE4J+WDnFAaiOgIV22c2MM4KtXfoVr3DUXa3O4HFa6jzAlIghxGeL1OM9tfslrXFlAh64I+MXy+8vPnQNU/9SqDIMPjSHeQAjD8gRlb94UuAJ8s6mVmGjFNm8VVUX0EzuSAiMAS5vVa9tjfGgou0KHlSmZ+lDFuMeGVqzLJ1I31r20yIAlCEJ0+Cqo+pJ4aF/jqNqOKNDNn1yM8A84tGHBD8cF4XV9t7zrJvcfepMPZIeivlX+IvDhXa1xMC+//JMAjB1kxHRSAYxpav4/3XhhFOEfgRfmbgT7ewy43JI/bLGMgSZ4wVWgs/8lZsXeVenJPR8fgXxQhwFR1kGdQ3PPCkBL+LYfbCwUHyAr3QLGcl5MIVnRil9YDB1/zC/vgjORIshKAGu4sjtt+s2Mb56ryyhGTeoidlSml3fA6G9hpUjLccMjS/QgJ3ciW";
	static{
		 CallRetry cr = new CallRetry();
	      cr.setMaximumRetries(3);
	      cr.setDelayTime(1000); // Wait for one second between each retry-call.
	      String[] apiErrorCodes = new String[] {
	          "10007", 					// "Internal error to the application."
	          "931", 					// "Validation of the authentication token in API request failed."
	          "521",					 // Test of Call-Retry: "The specified time window is invalid."
	          "124" 					// Test of Call-Retry: "Developer name invalid."
	      };
	      cr.setTriggerApiErrorCodes(apiErrorCodes);
	      java.lang.Class[] tcs = new java.lang.Class[] {
	          com.ebay.sdk.SdkSoapException.class
	      };
	      cr.setTriggerExceptions(tcs);
	      apiContext.setCallRetry(cr);
	      apiContext.setTimeout(180000);
	      apiContext.setApiServerUrl(apiServerUrl);  
	      apiContext.setEpsServerUrl(epsServerUrl);
	      apiContext.setSignInUrl(signInUrl);
	      ApiCredential apiCred = new ApiCredential();
	      ApiAccount ac = new ApiAccount();
	      apiCred.setApiAccount(ac);
	      apiCred.seteBayToken(eBayToken);
	      ac.setDeveloper(developer);
		  ac.setApplication(application);
	      ac.setCertificate(certificate);
	      apiContext.setApiCredential(apiCred);
	      apiContext.getApiLogging().setLogSOAPMessages(false);//Ebay交易信息是否输出
	}
	
	
 
	
	private EbayContextUtil() {
		 super();
	}





	public static ApiContext getContext(){
		return  apiContext;
	}


	public static ApiContext getContext(String token){
	 
		ApiContext apiContextnew = new ApiContext();
		 CallRetry cr = new CallRetry();
	      cr.setMaximumRetries(3);
	      cr.setDelayTime(1000); // Wait for one second between each retry-call.
	      String[] apiErrorCodes = new String[] {
	          "10007", 					// "Internal error to the application."
	          "931", 					// "Validation of the authentication token in API request failed."
	          "521",					 // Test of Call-Retry: "The specified time window is invalid."
	          "124" 					// Test of Call-Retry: "Developer name invalid."
	      };
	      cr.setTriggerApiErrorCodes(apiErrorCodes);
	      java.lang.Class[] tcs = new java.lang.Class[] {
	          com.ebay.sdk.SdkSoapException.class
	      };
	      cr.setTriggerExceptions(tcs);
	      apiContextnew.setCallRetry(cr);
	      apiContextnew.setTimeout(180000);
	      apiContextnew.setApiServerUrl(apiServerUrl);  
	      apiContextnew.setEpsServerUrl(epsServerUrl);
	      apiContextnew.setSignInUrl(signInUrl);
	      ApiCredential apiCred = new ApiCredential();
	      ApiAccount ac = new ApiAccount();
	      apiCred.setApiAccount(ac);
	      apiCred.seteBayToken(token);
	      ac.setDeveloper(developer);
		  ac.setApplication(application);
	      ac.setCertificate(certificate);
	      apiContextnew.setApiCredential(apiCred);
	      apiContextnew.getApiLogging().setLogSOAPMessages(false);
		return  apiContextnew;
	}
	
}
