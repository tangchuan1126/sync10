package com.cwc.util;

import com.cwc.app.util.Handler;
import com.cwc.db.DBRow;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Iterator;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class RequestUtil   implements Handler
{
    public static HashMap Request2HashMap(HttpServletRequest request,
                                          boolean flag)
    {
//        if( request == null )
//        {
//            return null;
//        }
//
//        HashMap hashMap = new HashMap();
//        Enumeration enum = request.getParameterNames();
//        while ( enum.hasMoreElements() )
//        {
//            String key = (String)enum.nextElement();
//            String value =  request.getParameter(key);
//            if(flag)
//            {
//                value = StrUtil.ISO2GBK(value);
//            }
//            hashMap.put(key, value);
//        }
        return null;
    }

    /*
     *
     *@param
     *@return
     */

    public static DBRow HashMap2DBRow(HashMap hashMap)
    {
        DBRow row = new DBRow();
        return HashMap2DBRow( hashMap, row );
    }
    public static DBRow HashMap2DBRow(HashMap hashMap, DBRow row)
    {
        if( hashMap == null )
        {
            return null;
        }

        Iterator keys = hashMap.keySet().iterator();

        if( ( keys == null ))
        {
            return null;
        }


        while( keys.hasNext())
        {
            String key = (String)keys.next();
            ////system.out.println("key=" + key);
            row.add(key, hashMap.get(key).toString());
        }

        return row;
    }
    public static DBRow Request2DBRow(HttpServletRequest request)
    {
        return Request2DBRow(request,new DBRow());
    }

    public static DBRow Request2DBRow(HttpServletRequest request, DBRow row)
    {
      return HashMap2DBRow( Request2HashMap( request, false), row);
    }

    public static DBRow Request2DBRow(HttpServletRequest request, DBRow row,
                                      boolean flag)
    {
      return HashMap2DBRow( Request2HashMap( request, flag), row);
    }

    /*private static HashMap hashMap2HashMap( HashMap hashMap )
    {
        if( ( hashMap == null ) || (hashMap.size() == 0 )  )
        {
            return hashMap;
        }

        Iterator iterator = hashMap.keySet().iterator();

        while(iterator.hasNext())
        {
            String key = iterator.next().toString();
        }

    } */
}
