package com.cwc.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class IoUtil {
	
	static Logger log = Logger.getLogger("sys");
	
	public void writeXMLToFile(String filePathName , Document document) throws Exception{
		try{
			 XMLWriter output;
		        //输出格式化
			 OutputFormat format = OutputFormat.createPrettyPrint();
	 
			 output = new XMLWriter(new FileWriter(filePathName), format);
			 output.write(document);
			 output.close(); 
		}catch (Exception e) {
			log.error(filePathName + " write xml error");
			throw new RuntimeException(filePathName + " write xml error");
		}
	}
	 public String readFile(String filePath) throws Exception {
		 try{
			 StringBuilder sb=new StringBuilder();  
	         BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));  
	         String line="";  
	         for(line=br.readLine();line!=null;line=br.readLine()) {  
	             sb.append(line+"\n");  
	         }    
	         br.close();
		     return sb.toString();  
		 }catch (Exception e) {
			 log.error(filePath + " read file error");
			 throw new RuntimeException(filePath + " read file error");
		}
	 }
	 
	 public void writeFile(String value , String filePath) throws Exception{
		 try{
			  BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
			  bw.write(value);
			  bw.close();	 
		 }catch (Exception e) {
			 log.error(filePath + "  " + "write File Error");
			 throw new RuntimeException(filePath + "  " + "write File Error");
		}
		 
	 }
	 public static void main(String[] args) {
		log.error("ddd");
	}
}
