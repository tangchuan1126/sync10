package com.cwc.transaction;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cwc.app.util.ThreadContext;

/**
 * 通过线程变量，保存当前事务状态
 * 通过拦截器标志操作事务是否打开，关闭、进行中
 * 
 * 模拟JTA，提供对非数据库操作的事务支持
 * 
 * @author turboshop
 *
 */
public class TransactionThread 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private static String key = "com.cwc.db.TransactionThread";
	private static String transactionActionKey = "transactionActionKey";
	private static String updateTableKey = "updateTableKey";
	
	private static boolean debugLog = false;

	/**
	 * 把与事务相关执行的操作加入到事务列表,这些操作会在事务提交的时候被执行，保持与数据库事务一致
	 * @param transactionAction
	 */
	public static void addTransactionAction(TransactionActionIFace transactionAction)
	{
		if ( isInTransaction() )
		{
			ArrayList transactionActionPool = (ArrayList)ThreadContext.get(transactionActionKey);
			
			if (transactionActionPool==null)
			{
				transactionActionPool = new ArrayList();
			}
			
			transactionActionPool.add(transactionAction);
			ThreadContext.put(transactionActionKey,transactionActionPool);
		}
	}
	
	/**
	 * 把在事务中执行了update操作的表明记录下来
	 * @param tablename
	 */
	public static void addTransactionUpdateTable(String tablename)
	{
		ArrayList transactionUpdateTablePool = (ArrayList)ThreadContext.get(updateTableKey);
		
		if (transactionUpdateTablePool==null)
		{
			transactionUpdateTablePool = new ArrayList();
		}
		
		if (!transactionUpdateTablePool.contains(tablename))
		{
			transactionUpdateTablePool.add(tablename);		
		}

		ThreadContext.put(updateTableKey,transactionUpdateTablePool);
	}
	
	/**
	 * 提供带cache函数根据表名判断查询的表是否在事务中被更新过，如果更新了，则从数据库中查询，而不能从内存中读取
	 * @param tablename
	 * @return
	 */
	public static boolean isUpdated(String tablename[])
	{
		ArrayList transactionUpdateTablePool = (ArrayList)ThreadContext.get(updateTableKey);
		
		if (transactionUpdateTablePool!=null)
		{
			for (int i=0; i<tablename.length; i++)
			{
				if (transactionUpdateTablePool.contains(tablename[i]))
				{
					return(true);
				}
			}
		}
		
		return(false);
	}
	
	
	
	/**
	 * 创建一个新事务存放到线程变量里面
	 * 新修改适应sring 事务拦截器
	 */
	public static void openTransaction()
	{
		try 
		{
			//创建新事务前，先检查原来线程中是否有存在的残余事务，如果有，则先关闭，避免造成内存溢出
			
			if (debugLog)
			{
				log.info("["+Thread.currentThread()+"] ["+ThreadContext.get("methodName")+"] openTransaction");	
			}
			
			ThreadContext.put(key,"ok");
		} 
		catch (Exception e) 
		{
			log.error("TransactionThread create transaction error:"+e);
		}
	}

	/**
	 * 正常关闭事务（TransactionInterceptorAfter 调用）
	 * 
	 *
	 */
	public static void closeTransaction()
	{
		Object tran = ThreadContext.get(key);
		
		if (tran!=null)
		{
			try 
			{
				//把所有相关事务执行
				ArrayList transactionActionPool = (ArrayList)ThreadContext.get(transactionActionKey);

				if (transactionActionPool!=null&&transactionActionPool.size()>0)
				{
					for (int i=0; i<transactionActionPool.size(); i++)
					{
						TransactionActionIFace transactionAction = (TransactionActionIFace)transactionActionPool.get(i);
						transactionAction.perform();	//执行事务同步任务
					}
					transactionActionPool.clear();
					transactionActionPool = null;
				}
				ThreadContext.remove(transactionActionKey);		//清空事务池
				ThreadContext.remove(updateTableKey);			//清空表名
				
				if (debugLog)
				{
					log.info("["+Thread.currentThread()+"] ["+ThreadContext.get("methodName")+"] closeTransaction");
				}
			} 
			catch (Exception e) 
			{
				log.error("TransactionThread closeTransaction error:"+e);
			}
		}

		//把事务标志去掉
		ThreadContext.remove(key);
	}
	
	/**
	 * 异常事务关闭 （StandardAopProxy 调用）
	 *
	 */
	public static void closeExceptionTransaction()
	{
		//先把事务关闭
		Object tran = ThreadContext.get(key);
		if (tran!=null)
		{
			try 
			{
				//把所有相关事务清除但不执行
				ArrayList transactionActionPool = (ArrayList)ThreadContext.get(transactionActionKey);
				if (transactionActionPool!=null)
				{
					transactionActionPool.clear();
					transactionActionPool = null;
				}
				ThreadContext.remove(transactionActionKey);			//清空事务池
				ThreadContext.remove(updateTableKey);				//清空表名
				
				if (debugLog)
				{
					log.info("["+Thread.currentThread()+"] ["+ThreadContext.get("methodName")+"] closeExceptionTransaction");					
				}
			}
			catch (Exception e) 
			{
				log.error("TransactionThread closeExceptionTransaction error:"+e);
			}
		}
		
		//把事务标志去掉
		ThreadContext.remove(key);
	}
	
	/**
	 * 回滚事务（StandardAopProxy 调用）
	 *
	 */
//	public static void rollbackTransaction()
//	{
//		//先把事务关闭
//		Transaction tran = (Transaction)ThreadContext.get(key);
//		if (tran!=null)
//		{
//			try 
//			{
//				//log.info("["+Thread.currentThread()+ "] ["+ThreadContext.get("currentAction")+"] rollbackTransaction");
//				tran.rollback();
//			} 
//			catch (Exception e) 
//			{
//				log.error("TransactionThread rollbackTransaction error:"+e);
//			}			
//		}
//	}
	
	/**
	 * 提交事务，在TransactionInterceptorAfter 调用
	 *
	 */
//	public static void commitTransaction()
//	{
//		Transaction tran = (Transaction)ThreadContext.get(key);
//
//		if (tran!=null)
//		{
//			try 
//			{
//				if (debugLog)
//				{
//					log.info("["+Thread.currentThread()+"] ["+currentAction+"] commitTransaction");					
//				}
//				tran.commit();
//			} 
//			catch (Exception e) 
//			{
//				log.error("TransactionThread commitTransaction error:"+e);
//			}			
//		}
//	}

//	public static Transaction getTransaction()
//	{
//		return( (Transaction)ThreadContext.get(key) );
//	}

	public static boolean isInTransaction()
	{
		return( ThreadContext.containsKey(key) );
	}
}




