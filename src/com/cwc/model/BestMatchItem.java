package com.cwc.model;

import com.cwc.util.SamplePoiAnnotation;

public class BestMatchItem {

	@SamplePoiAnnotation(width = "7000", title = "Item Id")
	private String itemId;

	public BestMatchItem(String itemId, String itemRank, String itemName,
			String sellerId, double price, String curreny, String quantitySold,
			String quantityAvailable, String primaryCategoryName,
			String secondaryCategoryName, String expeditedShipping) {
		super();
		this.itemId = itemId;
		this.itemRank = itemRank;
		this.itemName = itemName;
		this.sellerId = sellerId;
		this.price = price;
		this.curreny = curreny;
		this.quantitySold = quantitySold;
		this.quantityAvailable = quantityAvailable;
		this.primaryCategoryName = primaryCategoryName;
		this.secondaryCategoryName = secondaryCategoryName;
		this.expeditedShipping = expeditedShipping;
	}

	public BestMatchItem() {
		super();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemRank() {
		return itemRank;
	}

	public void setItemRank(String itemRank) {
		this.itemRank = itemRank;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCurreny() {
		return curreny;
	}

	public void setCurreny(String curreny) {
		this.curreny = curreny;
	}

	public String getQuantitySold() {
		return quantitySold;
	}

	public void setQuantitySold(String quantitySold) {
		this.quantitySold = quantitySold;
	}

	public String getQuantityAvailable() {
		return quantityAvailable;
	}

	public void setQuantityAvailable(String quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	public String getPrimaryCategoryName() {
		return primaryCategoryName;
	}

	public void setPrimaryCategoryName(String primaryCategoryName) {
		this.primaryCategoryName = primaryCategoryName;
	}

	public String getSecondaryCategoryName() {
		return secondaryCategoryName;
	}

	public void setSecondaryCategoryName(String secondaryCategoryName) {
		this.secondaryCategoryName = secondaryCategoryName;
	}

	public String getExpeditedShipping() {
		return expeditedShipping;
	}

	public void setExpeditedShipping(String expeditedShipping) {
		this.expeditedShipping = expeditedShipping;
	}

	@SamplePoiAnnotation(width = "3000", title = "Item Rank")
	private String itemRank;

	@SamplePoiAnnotation(width = "23000", title = "Item Name")
	private String itemName;

	@SamplePoiAnnotation(width = "5000", title = "Seller Id")
	private String sellerId;

	@SamplePoiAnnotation(width = "5000", title = "Price")
	private double price;

	@SamplePoiAnnotation(width = "5000", title = "Curreny")
	private String curreny;

	@SamplePoiAnnotation(width = "3000", title = "Available")
	private String quantityAvailable;
	
	@SamplePoiAnnotation(width = "3000", title = "Sold")
	private String quantitySold;

	

	@SamplePoiAnnotation(width = "8000", title = "1 Category Name")
	private String primaryCategoryName;

	@SamplePoiAnnotation(width = "8000", title = "2 Category Name")
	private String secondaryCategoryName;

	@SamplePoiAnnotation(width = "8000", title = "Expedited Shipping")
	private String expeditedShipping;

}
