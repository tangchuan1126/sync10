package com.cwc.model;

import com.cwc.util.SamplePoiAnnotation;
import com.cwc.util.StringUtil;
import com.ebay.soap.eBLBaseComponents.AmountType;
import com.ebay.soap.eBLBaseComponents.BestOfferDetailsType;
import com.ebay.soap.eBLBaseComponents.BestOfferStatusCodeType;
import com.ebay.soap.eBLBaseComponents.BestOfferTypeCodeType;
import com.ebay.soap.eBLBaseComponents.BuyerPaymentMethodCodeType;
import com.ebay.soap.eBLBaseComponents.BuyerProtectionCodeType;
import com.ebay.soap.eBLBaseComponents.CategoryType;
import com.ebay.soap.eBLBaseComponents.CountryCodeType;
import com.ebay.soap.eBLBaseComponents.DistanceType;
import com.ebay.soap.eBLBaseComponents.HitCounterCodeType;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityListType;
import com.ebay.soap.eBLBaseComponents.ItemPolicyViolationType;
import com.ebay.soap.eBLBaseComponents.ListingDetailsType;
import com.ebay.soap.eBLBaseComponents.ListingTypeCodeType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.PaymentDetailsType;
import com.ebay.soap.eBLBaseComponents.ProductListingDetailsType;
import com.ebay.soap.eBLBaseComponents.ReturnPolicyType;
import com.ebay.soap.eBLBaseComponents.SellerType;
import com.ebay.soap.eBLBaseComponents.ShippingDetailsType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.ebay.soap.eBLBaseComponents.UserType;
import com.ebay.soap.eBLBaseComponents.VariationsType;

 

public class BaseItem {
	
	@SamplePoiAnnotation(width = "5000" , title= "itemNumber")
	private String itemNumber ;
	
	@SamplePoiAnnotation(width = "7000" , title= "title")
	private String title;
	
	@SamplePoiAnnotation(width = "2500" , title= "quantity")
	private int quantity;
	

	
	
	@SamplePoiAnnotation(width = "2500" , title= "1st CategoryID")
	private CategoryType  primaryCategory;
	
	@SamplePoiAnnotation(width = "5000" , title= "2nd CategoryID")
	private CategoryType  secondCategory ;
	
	@SamplePoiAnnotation(width = "2500" , title= "location")
	private String location;
	
	@SamplePoiAnnotation(width = "2500" , title= "country")
	private CountryCodeType country;
	
	/**
	 * 新增加字段
	 */
	@SamplePoiAnnotation(width = "2500" , title= "sku")
	private String sku;
	
	
	@SamplePoiAnnotation(width = "2500" , title= "postalCode")
	private String postalCode;
	
	

	 
	
	@SamplePoiAnnotation(width = "2500" , title= "hitCounter")
	private HitCounterCodeType  hitCounter;
 
	@SamplePoiAnnotation(width = "3000" , title= "listingType")
	private ListingTypeCodeType  listingType;
	
	

	
	
	@SamplePoiAnnotation(width = "3000" , title= "dispatchTimeMax")
	private int dispatchTimeMax;
	
	@SamplePoiAnnotation(width = "4000" , title= "listingDuration")
	private String listingDuration;
	
	@SamplePoiAnnotation(width = "2000" , title= "site")
	private SiteCodeType site;
	
	@SamplePoiAnnotation(width = "5000" , title= "shipToLocations")
	private String[] shipToLocations;
	// 数组类型
	@SamplePoiAnnotation(width = "7000" , title= "paymentMethods"  )
	private BuyerPaymentMethodCodeType[] paymentMethods;
	
	@SamplePoiAnnotation(width = "5000" , title= "payPalEmailAddress")
	private String payPalEmailAddress;
	
	@SamplePoiAnnotation(width = "5000" , title= "conditionID")
	private int conditionID;
	
	@SamplePoiAnnotation(width = "5000" , title= "conditionDisplayName")
	private String conditionDisplayName;
	
	
	// 新增加字段
	@SamplePoiAnnotation(width = "3500" , title= "isBestOfferEnabled")
	private String isBestOfferEnabled;
	
	
	@SamplePoiAnnotation(width = "3500" , title= "isSkypeEnabled")
	private String isSkypeEnabled;
	
	
	@SamplePoiAnnotation(width = "3500" , title= "isAutoPay")
	private String isAutoPay;
	
	
	@SamplePoiAnnotation(width = "3500" , title= "isDisableBuyerRequirements")
	private String isDisableBuyerRequirements;
	
	@SamplePoiAnnotation(width = "3500" , title= "watchCount")
	private String watchCount;
	
	@SamplePoiAnnotation(width = "3500" , title= "distance")
	private DistanceType  distance;
	
	@SamplePoiAnnotation(width = "5500" , title= "buyerProtection") 
	private BuyerProtectionCodeType  buyerProtection;
	
	@SamplePoiAnnotation(width = "5500" , title= "ebayNotes") 
	private String ebayNotes;
	
	@SamplePoiAnnotation(width = "5500" , title= "floorPrice") 
	private AmountType  floorPrice;
	
	@SamplePoiAnnotation(width = "5500" , title= "subTitle") 
	private String subTitle;
	
	@SamplePoiAnnotation(width = "5500" , title= "reservePrice") 
	private AmountType  reservePrice ;
	
	@SamplePoiAnnotation(width = "5500" , title= "buyItNowPrice") 
	private AmountType  buyItNowPrice ;
	
	@SamplePoiAnnotation(width = "5500" , title= "ceilingPrice") 
	private AmountType ceilingPrice;
	
	@SamplePoiAnnotation(width = "7500" , title= "classifiedAdPayPerLeadFee") 
	private AmountType classifiedAdPayPerLeadFee;
	
	@SamplePoiAnnotation(width = "5500" , title= "groupCategoryID") 
	private String groupCategoryID;
	
	@SamplePoiAnnotation(width = "5500" , title= "paymentAllowedSite") 
	private SiteCodeType[] paymentAllowedSite;
	
	@SamplePoiAnnotation(width = "5000"  , title= "startPrice" ,color = "99FF33")
	private AmountType startPrice;
	
	
	
	// seller info declare
	@SamplePoiAnnotation(width = "7000" , title= "seller AboutMePage" ,color = "99FF99"  , isHidden = "true" )
	private String sellerAboutMePage;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller Email" ,color = "99FF99" , isHidden = "true")
	private String sellerEmail;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller FeedbackScore" ,color = "99FF99", isHidden = "true" )
	private String sellerFeedbackScore;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller PositiveFeedbackPercent" ,color = "99FF99")
	private String sellerPositiveFeedbackPercent;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller FeedbackPrivate" ,color = "99FF99")
	private String sellerFeedbackPrivate;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller FeedbackRatingStar" ,color = "99FF99")
	private String sellerFeedbackRatingStar;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller IDVerified" ,color = "99FF99", isHidden = "true" )
	private String sellerIDVerified;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller eBayGoodStanding" ,color = "99FF99", isHidden = "true" )
	private String sellereBayGoodStanding;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller NewUser" ,color = "99FF99", isHidden = "true" )
	private String sellerNewUser;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller RegistrationDate" ,color = "99FF99")
	private String sellerRegistrationDate;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller Site" ,color = "99FF99", isHidden = "true" )
	private String sellerSite;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller Status" ,color = "99FF99", isHidden = "true" )
	private String sellerStatus;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller UserID" ,color = "99FF99")
	private String sellerUserID;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller VATStatus" ,color = "99FF99", isHidden = "true" )
	private String sellerVATStatus;
	
	@SamplePoiAnnotation(width = "7000" , title= "seller MotorsDealer" ,color = "99FF99", isHidden = "true" )
	private String sellerMotorsDealer;
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerIn foAllowPaymentEdit" ,color = "99FF00", isHidden = "true" )
	private String sellerInfoAllowPaymentEdit;
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerInfo CheckoutEnabled" ,color = "99FF00", isHidden = "true" )
	private String sellerInfoCheckoutEnabled;
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerInfo CIPBankAccountStored" ,color = "99FF00", isHidden = "true" )
	private String sellerInfoCIPBankAccountStored;
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerInfo GoodStanding" ,color = "99FF00", isHidden = "true" )
	private String sellerInfoGoodStanding;
 
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerInfo MerchandizingPref" ,color = "99FF00", isHidden = "true" )
	private String sellerInfoMerchandizingPref;
	
	@SamplePoiAnnotation(width = "7000" , title= "sellerinfo QualifiesForB2BVAT" ,color = "99FF00", isHidden = "true" )
	private String sellerinfoQualifiesForB2BVAT;
	
	
	
	
	//ReturnPolicy info
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy RefundOption" ,color = "CCFF00")
	private String returnPolicyRefundOption;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy Refund" ,color = "CCFF00")
	private String returnPolicyRefund;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ReturnsWithinOption" ,color = "CCFF00")
	private String returnPolicyReturnsWithinOption;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ReturnsWithin" ,color = "CCFF00")
	private String returnPolicyReturnsWithin;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ReturnsAcceptedOption" ,color = "CCFF00")
	private String returnPolicyReturnsAcceptedOption;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ReturnsAccepted" ,color = "CCFF00")
	private String returnPolicyReturnsAccepted;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy description" ,color = "CCFF00")
	private String returnPolicydescription;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ShippingCostPaidByOption" ,color = "CCFF00")
	private String returnPolicyShippingCostPaidByOption;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy ShippingCostPaidBy" ,color = "CCFF00")
	private String returnPolicyShippingCostPaidBy;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyDuration" ,color = "CCFF00")
	private String returnPolicyWarrantyDuration;
	
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyDurationOption" ,color = "CCFF00")
	private String returnPolicyWarrantyDurationOption;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyOffered " ,color = "CCFF00")
	private String returnPolicyWarrantyOffered ;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyOfferedOption " ,color = "CCFF00")
	private String returnPolicyWarrantyOfferedOption ;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyType" ,color = "CCFF00")
	private String returnPolicyWarrantyType ;
	
	@SamplePoiAnnotation(width = "7000" , title= "returnPolicy WarrantyTypeOption" ,color = "CCFF00")
	private String returnPolicyWarrantyTypeOption;
  
	
	// ListingDetailsType
	@SamplePoiAnnotation(title= "isAdult" ,color = "CCFF33")
	private String listingTypeIsAdult;
	
	@SamplePoiAnnotation(width = "5000"  , title= "isBindingAuction" ,color = "CCFF33")
	private String listingTypeIsBindingAuction;
	
	
	@SamplePoiAnnotation(width = "5000"  , title= "isBuyItNowAvailable" ,color = "CCFF33")
	private String listingTypeIsBuyItNowAvailable;
	
	@SamplePoiAnnotation(width = "7000"  , title= "isCheckoutEnabled" ,color = "CCFF33")
	private String listingTypeIsCheckoutEnabled;
	
	@SamplePoiAnnotation(width = "5000"  , title= "minimumBestOfferPrice" ,color = "CCFF33")
	private String listingTypeMinimumBestOfferPrice;
	
	@SamplePoiAnnotation(width = "7000"  , title= "bestOfferAutoAcceptPrice" ,color = "CCFF33")
	private String listingTypeBestOfferAutoAcceptPrice;
	
	@SamplePoiAnnotation(width = "5000"  , title= "bestOffer" ,color = "CCFF33")
	private String bestOffer;
	
	@SamplePoiAnnotation(width = "5000"  , title= "bestOfferCount" ,color = "CCFF33")
	private String bestOfferCount;
	
	@SamplePoiAnnotation(width = "5000"  , title= "bestOfferStatus" ,color = "CCFF33")
	private String bestOfferStatus;
	
	@SamplePoiAnnotation(width = "5000"  , title= "bestOfferType" ,color = "CCFF33")
	private String bestOfferType;
	
	@SamplePoiAnnotation(width = "5000"  , title= "bestOfferEnabled" ,color = "CCFF33")
	private String bestOfferEnabled;
	
	
	
	@SamplePoiAnnotation(width = "5000"  , title= "policy Id" ,color = "669933")
	private String itemPolicyViolationId ;
	
	@SamplePoiAnnotation(width = "5000"  , title= "policy Text" ,color = "669933")
	private String itemPolicyViolationText;
//	99FF33
	@SamplePoiAnnotation(width = "5000"  , title= "payMentDetailDaysToFullPayment" ,color = "99FF33")
	private String payMentDetailDaysToFullPayment;
	
	@SamplePoiAnnotation(width = "5000"  , title= "payMentDetailDepositAmount" ,color = "99FF33")
	private String payMentDetailDepositAmount;
	
	@SamplePoiAnnotation(width = "5000"  , title= "payMentDetailDepositType" ,color = "99FF33")
	private String payMentDetailDepositType;
	
	@SamplePoiAnnotation(width = "5000"  , title= "payMentHoursToDeposit" ,color = "99FF33")
	private String payMentHoursToDeposit;
	

	
	private ListingDetailsType listingDetailsType;
	private ShippingDetailsType shippingDetails;
	private ReturnPolicyType returnPolicy ;
	private UserType seller ;
	private NameValueListArrayType itemSpecifics ;
	private VariationsType variations;
	private ItemCompatibilityListType itemCompatibilityList;
	private BestOfferDetailsType bestOfferDetails;
	private ItemPolicyViolationType  itemPolicyViolation;
	private PaymentDetailsType paymentDetailsType;
	private ProductListingDetailsType productListingDetails ;
	
	public BaseItem(String itemNumber,String title, CountryCodeType country,
			HitCounterCodeType hitCounter, ListingTypeCodeType listingType,
			String location, int dispatchTimeMax, String listingDuration,
			SiteCodeType site, String[] shipToLocations,
			BuyerPaymentMethodCodeType[] paymentMethods,
			String payPalEmailAddress, int conditionID,
			String conditionDisplayName, CategoryType primaryCategory,NameValueListArrayType itemSpecifics,VariationsType variations ,ItemCompatibilityListType itemCompatibilityList,
			ShippingDetailsType shippingDetails,CategoryType secondCategory , Integer quantity ,
			String isBestOfferEnabled ,String isSkypeEnabled , String isAutoPay , String isDisableBuyerRequirements ,String sku , String postalCode ,
			String watchCount, DistanceType distance ,BuyerProtectionCodeType  buyerProtection,
			String ebayNotes,AmountType  floorPrice,String subTitle ,
			AmountType reservePrice, AmountType buyItNowPrice,AmountType ceilingPrice,AmountType classifiedAdPayPerLeadFee,
			String groupCategoryID , SiteCodeType[] paymentAllowedSite,ProductListingDetailsType productListingDetails,
			AmountType startPrice
		) {
		super();
		this.itemNumber = itemNumber;
		this.title = title;
		this.country = country;
		this.hitCounter = hitCounter;
		this.listingType = listingType;
		this.location = location;
		this.dispatchTimeMax = dispatchTimeMax;
		this.listingDuration = listingDuration;
		this.site = site;
		this.shipToLocations = shipToLocations;
		this.paymentMethods = paymentMethods;
		this.payPalEmailAddress = payPalEmailAddress;
		this.conditionID = conditionID;
		this.conditionDisplayName = conditionDisplayName;
		this.primaryCategory = primaryCategory;
		this.itemSpecifics = itemSpecifics;
		this.variations = variations;
		this.itemCompatibilityList = itemCompatibilityList;
		this.shippingDetails = shippingDetails;
		this.secondCategory  = secondCategory;
		this.quantity = quantity;
		this.isBestOfferEnabled = isBestOfferEnabled ;
		this.isSkypeEnabled = isSkypeEnabled;
		this.isAutoPay = isAutoPay ;
		this.isDisableBuyerRequirements = isDisableBuyerRequirements;
		this.sku = sku ;
		this.postalCode = postalCode;
		this.watchCount = watchCount;
		this.distance = distance;
		this.buyerProtection = buyerProtection;
		this.ebayNotes = ebayNotes;
		this.floorPrice = floorPrice;
		this.subTitle = subTitle;
		this.reservePrice = reservePrice;
		this.buyItNowPrice = buyItNowPrice;
		this.ceilingPrice = ceilingPrice;
		this.classifiedAdPayPerLeadFee = classifiedAdPayPerLeadFee;
		this.groupCategoryID = groupCategoryID;
		this.paymentAllowedSite = paymentAllowedSite;
		this.productListingDetails = productListingDetails;
		this.startPrice = startPrice;
		// 更多信息的设置
		
		
		
		
		
	}
	
	
	
	public ProductListingDetailsType getProductListingDetails() {
		return productListingDetails;
	}
 
	public void setProductListingDetails(
			ProductListingDetailsType productListingDetails) {
		this.productListingDetails = productListingDetails;
	}
  
	public String getPaymentAllowedSite() {
		StringBuffer sb = new StringBuffer();
		if(paymentAllowedSite != null && paymentAllowedSite.length > 0){
		 
			for(SiteCodeType type :paymentAllowedSite ){
				sb.append(",").append(type.value());
			}
		}
		return sb.length() > 0 ? sb.substring(1) : sb.toString();
	}



	public void setPaymentAllowedSite(SiteCodeType[] paymentAllowedSite) {
		this.paymentAllowedSite = paymentAllowedSite;
	}



	private String customeAmountTypeValue(AmountType  type){
		if(type != null){
			return type.getCurrencyID() + "," + type.getValue();
		}
		return "";
	}
	
	
	public String getReservePrice() {
		return customeAmountTypeValue(reservePrice);
	}

	public void setReservePrice(AmountType reservePrice) {
		this.reservePrice = reservePrice;
	}

	public String getBuyItNowPrice() {
		return customeAmountTypeValue(buyItNowPrice);
	}

	public void setBuyItNowPrice(AmountType buyItNowPrice) {
		this.buyItNowPrice = buyItNowPrice;
	}
	
	public String getStartPrice() {
		return customeAmountTypeValue(startPrice);
	}



	public void setStartPrice(AmountType startPrice) {
		this.startPrice = startPrice;
	}



	public String getCeilingPrice() {
		return customeAmountTypeValue(ceilingPrice);
	}

	public void setCeilingPrice(AmountType ceilingPrice) {
		this.ceilingPrice = ceilingPrice;
	}

	public String getClassifiedAdPayPerLeadFee() {
		return customeAmountTypeValue(classifiedAdPayPerLeadFee);
	}

	public void setClassifiedAdPayPerLeadFee(AmountType classifiedAdPayPerLeadFee) {
		this.classifiedAdPayPerLeadFee = classifiedAdPayPerLeadFee;
	}

	public String getGroupCategoryID() {
		return groupCategoryID;
	}

	public void setGroupCategoryID(String groupCategoryID) {
		this.groupCategoryID = groupCategoryID;
	}

	public String getBuyerProtection() {
		if(buyerProtection != null){
			return buyerProtection.value();
		}
		return "";
	}

	public void setBuyerProtection(BuyerProtectionCodeType buyerProtection) {
		this.buyerProtection = buyerProtection;
	}

	public ItemPolicyViolationType getItemPolicyViolation() {
		return itemPolicyViolation;
	}



	public void setItemPolicyViolation(ItemPolicyViolationType itemPolicyViolation) {
		this.itemPolicyViolation = itemPolicyViolation;
		if(itemPolicyViolation != null){
			this.setItemPolicyViolationId(itemPolicyViolation.getPolicyID()+"");
			this.setItemPolicyViolationText(itemPolicyViolation.getPolicyText());
		}
	}

	
	public BestOfferDetailsType getBestOfferDetails() {
		return bestOfferDetails;
	}



	public void setBestOfferDetails(BestOfferDetailsType bestOfferDetails) {
		this.bestOfferDetails = bestOfferDetails;
		if(bestOfferDetails != null){
			if(	bestOfferDetails.getBestOffer() != null){
				AmountType  type = bestOfferDetails.getBestOffer();
				this.setBestOffer(type.getCurrencyID()+","+type.getValue());
			}
			if(bestOfferDetails.getBestOfferCount() != null){
				this.setBestOfferCount(bestOfferDetails.getBestOfferCount()+"");
			}
			if(bestOfferDetails.getBestOfferStatus() != null){
				BestOfferStatusCodeType codeType = bestOfferDetails.getBestOfferStatus();
				this.setBestOfferStatus(codeType.value());
			}
			if(bestOfferDetails.getBestOfferType() != null){
				BestOfferTypeCodeType codeType = bestOfferDetails.getBestOfferType() ;
				this.setBestOfferType(codeType.value());
			}
			if(bestOfferDetails.isBestOfferEnabled() != null){
				String booleanString = bestOfferDetails.isBestOfferEnabled()+"";
				this.setBestOfferEnabled(booleanString);
			}
 
			 
		}
	}



	public ListingDetailsType getListingDetailsType() {
		return listingDetailsType;
	}


	/**
	 * 设置listingDetailsType
	 * @param listingDetailsType
	 */
	public void setListingDetailsType(ListingDetailsType listingDetailsType) {
		this.listingDetailsType = listingDetailsType;
		this.setListingTypeIsAdult(listingDetailsType.isAdult() + "");
		this.setListingTypeIsCheckoutEnabled(listingDetailsType.isCheckoutEnabled() + "");
 
		this.setListingTypeIsBindingAuction(listingDetailsType.isBindingAuction() + "");
		//isCheckoutEnabled,minimumBestOfferPrice,bestOfferAutoAcceptPrice
		this.setListingTypeIsCheckoutEnabled(listingDetailsType.isCheckoutEnabled() + "");
		AmountType  mini = listingDetailsType.getMinimumBestOfferPrice();
		if(mini != null){
			this.setListingTypeMinimumBestOfferPrice(mini.getCurrencyID().value() + "," + mini.getValue());
		}
		AmountType  bestOffer = listingDetailsType.getBestOfferAutoAcceptPrice();
		if(bestOffer != null){
			this.setListingTypeBestOfferAutoAcceptPrice(bestOffer.getCurrencyID().value() + "," + bestOffer.getValue());
		}
		 
	}



	public ReturnPolicyType getReturnPolicy() {
		return returnPolicy;
	}


	public void setReturnPolicy(ReturnPolicyType returnPolicy) {
		this.returnPolicy = returnPolicy;
		if(returnPolicy != null){
			this.setReturnPolicydescription(returnPolicy.getDescription());
			this.setReturnPolicyRefund(returnPolicy.getRefund());
			this.setReturnPolicyRefundOption(returnPolicy.getRefundOption());
			this.setReturnPolicyReturnsAccepted(returnPolicy.getReturnsAccepted());
			this.setReturnPolicyReturnsAcceptedOption(returnPolicy.getReturnsAcceptedOption());
			this.setReturnPolicyReturnsWithin(returnPolicy.getReturnsWithin());
			this.setReturnPolicyReturnsWithinOption(returnPolicy.getReturnsWithinOption());
			this.setReturnPolicyShippingCostPaidBy(returnPolicy.getShippingCostPaidBy());
			this.setReturnPolicyShippingCostPaidByOption(returnPolicy.getShippingCostPaidByOption());
			this.setReturnPolicyWarrantyDuration(returnPolicy.getWarrantyDuration());
			this.setReturnPolicyWarrantyDurationOption(returnPolicy.getWarrantyDurationOption());
			this.setReturnPolicyWarrantyOffered(returnPolicy.getWarrantyOffered());
			this.setReturnPolicyWarrantyOfferedOption(returnPolicy.getWarrantyOfferedOption());
			this.setReturnPolicyWarrantyType(returnPolicy.getWarrantyType());
			this.setReturnPolicyWarrantyTypeOption(returnPolicy.getWarrantyTypeOption());
		}
	}


	public UserType getSeller() {
		return seller;
	}


	public void setSeller(UserType seller) {
		this.seller = seller;
		// seller and sellerInfo
		if(seller != null){
		 
			this.setSellerAboutMePage(seller.isAboutMePage()+"");
			this.setSellereBayGoodStanding(seller.isEBayGoodStanding()+"");
			this.setSellerEmail(seller.getEmail());
			this.setSellerFeedbackPrivate(seller.isFeedbackPrivate()+"");
			this.setSellerFeedbackRatingStar(seller.getFeedbackRatingStar()+"");
			this.setSellerFeedbackScore(seller.getFeedbackScore()+"");
			this.setSellerIDVerified(seller.isIDVerified()+"");
			this.setSellerMotorsDealer(seller.isMotorsDealer()+"");
			this.setSellerNewUser(seller.isNewUser()+"");
			this.setSellerPositiveFeedbackPercent(seller.getPositiveFeedbackPercent()+"");
			this.setSellerRegistrationDate(seller.getRegistrationDate().getTime()+"");
			this.setSellerSite(seller.getSite().value());
			this.setSellerStatus(seller.getStatus().value());
			this.setSellerUserID(seller.getUserID());
			this.setSellerVATStatus(seller.getVATStatus().value());
			// sellerInfo
			SellerType  sellerInfo = seller.getSellerInfo();
			this.setSellerInfoAllowPaymentEdit(sellerInfo.isAllowPaymentEdit()+"");
			this.setSellerInfoCheckoutEnabled(sellerInfo.isCheckoutEnabled()+"");
			this.setSellerInfoCIPBankAccountStored(sellerInfo.isCIPBankAccountStored()+"");
			this.setSellerInfoGoodStanding(sellerInfo.isGoodStanding()+"");
			this.setSellerInfoMerchandizingPref(sellerInfo.getMerchandizingPref().value());
			this.setSellerinfoQualifiesForB2BVAT(sellerInfo.isQualifiesForB2BVAT()+"");
		}
	}


	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public ItemCompatibilityListType getItemCompatibilityList() {
		return itemCompatibilityList;
	}

	public void setItemCompatibilityList(ItemCompatibilityListType itemCompatibilityList) {
		this.itemCompatibilityList = itemCompatibilityList;
	}
	
	public VariationsType getVariations() {
		return variations;
	}

	public void setVariations(VariationsType variations) {
		this.variations = variations;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country.value();
	}

	public void setCountry(String country) {
		this.country = CountryCodeType.fromValue(country);
	}

	public String getHitCounter() {
		return hitCounter.value();
	}

	public void setHitCounter(String hitCounter) {
		//system.out.println(hitCounter);
		
		this.hitCounter = HitCounterCodeType.valueOf(hitCounter);
	}

	public String getListingType() {
		return listingType.value();
	}

	public void setListingType(String listingType) {
		this.listingType = ListingTypeCodeType.valueOf(listingType);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getDispatchTimeMax() {
		return dispatchTimeMax;
	}

	public void setDispatchTimeMax(int dispatchTimeMax) {
		this.dispatchTimeMax = dispatchTimeMax;
	}

	public String getListingDuration() {
		return listingDuration;
	}

	public void setListingDuration(String listingDuration) {
		this.listingDuration = listingDuration;
	}

	public String getSite() {
		return site.value();
	}

 

	public void setSite(String site) {
		this.site = SiteCodeType.fromValue(site);
	}

	public String getShipToLocations() {
		return StringUtil.convertStringArrayToString(shipToLocations);
	}

	public void setShipToLocations(String shipToLocations) {
		
		this.shipToLocations = StringUtil.convertStringToStringArray(shipToLocations);
	}

	public String getPaymentMethods() {
		StringBuffer sb = new StringBuffer();
		if(paymentMethods != null && paymentMethods.length > 0){
			for(BuyerPaymentMethodCodeType type : paymentMethods){
				sb.append(",").append(type.value());
			}
			return sb.substring(1);
		}else{
			return "";
		}
	}

	public void setPaymentMethods(String paymentMethods) {
		String[] types = paymentMethods.split(",");
		BuyerPaymentMethodCodeType[] type = new BuyerPaymentMethodCodeType[types.length];
		for(int index = 0 , count = type.length ; index < count ; index++ ){
			type[index] = BuyerPaymentMethodCodeType.fromValue(types[index]);
		}
		this.paymentMethods = type;
	}

	
	public String getSecondCategory() {
		if(secondCategory != null){
			return secondCategory.getCategoryID();
		}else{
			return "";
		}
	 
	}


	public void setSecondCategory(CategoryType secondCategory) {
		this.secondCategory = secondCategory;
	}


	public String getPayPalEmailAddress() {
		return payPalEmailAddress;
	}

	public void setPayPalEmailAddress(String payPalEmailAddress) {
		this.payPalEmailAddress = payPalEmailAddress;
	}

	public int getConditionID() {
		return conditionID;
	}

	public void setConditionID(int conditionID) {
		this.conditionID = conditionID;
	}

	public String getConditionDisplayName() {
		return conditionDisplayName;
	}

	public void setConditionDisplayName(String conditionDisplayName) {
		this.conditionDisplayName = conditionDisplayName;
	}

	public String getPrimaryCategory() {
		return primaryCategory.getCategoryID();
	}

	public void setPrimaryCategory(String primaryCategory) {
		CategoryType type = new CategoryType();
		type.setCategoryID(primaryCategory);
		this.primaryCategory = type;
	}

	/*public BaseItem(String title, String country, String hitCounter,
			String listingType, String location, int dispatchTimeMax,
			String listingDuration, String site, String shipToLocations,
			String paymentMethods, String payPalEmailAddress, int conditionID,
			String conditionDisplayName, String primaryCategory) {
		super();
		this.title = title;
		this.location = location;
		this.dispatchTimeMax = dispatchTimeMax;
		this.listingDuration = listingDuration;
		this.payPalEmailAddress = payPalEmailAddress;
		this.conditionID = conditionID;
		this.conditionDisplayName = conditionDisplayName;
		this.setCountry(country);
		this.setHitCounter(hitCounter);
		this.setListingType(listingType);
		this.setSite(site);
		this.setShipToLocations(shipToLocations);
		this.setPaymentMethods(paymentMethods);
		this.setPrimaryCategory(primaryCategory);
	}	*/
 
	public NameValueListArrayType getItemSpecifics() {
		return itemSpecifics;
	}

	public void setItemSpecifics(NameValueListArrayType itemSpecifics) {
		this.itemSpecifics = itemSpecifics;
	}


	public String getSellerAboutMePage() {
		return sellerAboutMePage;
	}


	public void setSellerAboutMePage(String sellerAboutMePage) {
		this.sellerAboutMePage = sellerAboutMePage;
	}


	public String getSellerEmail() {
		return sellerEmail;
	}


	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}


	public String getSellerFeedbackScore() {
		return sellerFeedbackScore;
	}


	public void setSellerFeedbackScore(String sellerFeedbackScore) {
		this.sellerFeedbackScore = sellerFeedbackScore;
	}


	public String getSellerPositiveFeedbackPercent() {
		return sellerPositiveFeedbackPercent;
	}


	public void setSellerPositiveFeedbackPercent(
			String sellerPositiveFeedbackPercent) {
		this.sellerPositiveFeedbackPercent = sellerPositiveFeedbackPercent;
	}


	public String getSellerFeedbackPrivate() {
		return sellerFeedbackPrivate;
	}


	public void setSellerFeedbackPrivate(String sellerFeedbackPrivate) {
		this.sellerFeedbackPrivate = sellerFeedbackPrivate;
	}


	public String getSellerFeedbackRatingStar() {
		return sellerFeedbackRatingStar;
	}


	public void setSellerFeedbackRatingStar(String sellerFeedbackRatingStar) {
		this.sellerFeedbackRatingStar = sellerFeedbackRatingStar;
	}


	public String getSellerIDVerified() {
		return sellerIDVerified;
	}


	public void setSellerIDVerified(String sellerIDVerified) {
		this.sellerIDVerified = sellerIDVerified;
	}


	public String getSellereBayGoodStanding() {
		return sellereBayGoodStanding;
	}


	public void setSellereBayGoodStanding(String sellereBayGoodStanding) {
		this.sellereBayGoodStanding = sellereBayGoodStanding;
	}


	public String getSellerNewUser() {
		return sellerNewUser;
	}


	public void setSellerNewUser(String sellerNewUser) {
		this.sellerNewUser = sellerNewUser;
	}


	public String getSellerRegistrationDate() {
		return sellerRegistrationDate;
	}


	public void setSellerRegistrationDate(String sellerRegistrationDate) {
		this.sellerRegistrationDate = sellerRegistrationDate;
	}


	public String getSellerSite() {
		return sellerSite;
	}


	public void setSellerSite(String sellerSite) {
		this.sellerSite = sellerSite;
	}


	public String getSellerStatus() {
		return sellerStatus;
	}


	public void setSellerStatus(String sellerStatus) {
		this.sellerStatus = sellerStatus;
	}


	public String getSellerUserID() {
		return sellerUserID;
	}


	public void setSellerUserID(String sellerUserID) {
		this.sellerUserID = sellerUserID;
	}


	public String getSellerVATStatus() {
		return sellerVATStatus;
	}


	public void setSellerVATStatus(String sellerVATStatus) {
		this.sellerVATStatus = sellerVATStatus;
	}


	public String getSellerMotorsDealer() {
		return sellerMotorsDealer;
	}


	public void setSellerMotorsDealer(String sellerMotorsDealer) {
		this.sellerMotorsDealer = sellerMotorsDealer;
	}


	public String getSellerInfoAllowPaymentEdit() {
		return sellerInfoAllowPaymentEdit;
	}


	public void setSellerInfoAllowPaymentEdit(String sellerInfoAllowPaymentEdit) {
		this.sellerInfoAllowPaymentEdit = sellerInfoAllowPaymentEdit;
	}


	public String getSellerInfoCheckoutEnabled() {
		return sellerInfoCheckoutEnabled;
	}


	public void setSellerInfoCheckoutEnabled(String sellerInfoCheckoutEnabled) {
		this.sellerInfoCheckoutEnabled = sellerInfoCheckoutEnabled;
	}


	public String getSellerInfoCIPBankAccountStored() {
		return sellerInfoCIPBankAccountStored;
	}


	public void setSellerInfoCIPBankAccountStored(
			String sellerInfoCIPBankAccountStored) {
		this.sellerInfoCIPBankAccountStored = sellerInfoCIPBankAccountStored;
	}


	public String getSellerInfoGoodStanding() {
		return sellerInfoGoodStanding;
	}


	public void setSellerInfoGoodStanding(String sellerInfoGoodStanding) {
		this.sellerInfoGoodStanding = sellerInfoGoodStanding;
	}
 
	public String getSellerInfoMerchandizingPref() {
		return sellerInfoMerchandizingPref;
	}


	public void setSellerInfoMerchandizingPref(String sellerInfoMerchandizingPref) {
		this.sellerInfoMerchandizingPref = sellerInfoMerchandizingPref;
	}


	public String getSellerinfoQualifiesForB2BVAT() {
		return sellerinfoQualifiesForB2BVAT;
	}


	public void setSellerinfoQualifiesForB2BVAT(String sellerinfoQualifiesForB2BVAT) {
		this.sellerinfoQualifiesForB2BVAT = sellerinfoQualifiesForB2BVAT;
	}


	public void setShipToLocations(String[] shipToLocations) {
		this.shipToLocations = shipToLocations;
	}


	public String getReturnPolicyRefundOption() {
		return returnPolicyRefundOption;
	}


	public void setReturnPolicyRefundOption(String returnPolicyRefundOption) {
		this.returnPolicyRefundOption = returnPolicyRefundOption;
	}


	public String getReturnPolicyRefund() {
		return returnPolicyRefund;
	}


	public void setReturnPolicyRefund(String returnPolicyRefund) {
		this.returnPolicyRefund = returnPolicyRefund;
	}


	public String getReturnPolicyReturnsWithinOption() {
		return returnPolicyReturnsWithinOption;
	}


	public void setReturnPolicyReturnsWithinOption(
			String returnPolicyReturnsWithinOption) {
		this.returnPolicyReturnsWithinOption = returnPolicyReturnsWithinOption;
	}


	public String getReturnPolicyReturnsWithin() {
		return returnPolicyReturnsWithin;
	}


	public void setReturnPolicyReturnsWithin(String returnPolicyReturnsWithin) {
		this.returnPolicyReturnsWithin = returnPolicyReturnsWithin;
	}


	public String getReturnPolicyReturnsAcceptedOption() {
		return returnPolicyReturnsAcceptedOption;
	}


	public void setReturnPolicyReturnsAcceptedOption(
			String returnPolicyReturnsAcceptedOption) {
		this.returnPolicyReturnsAcceptedOption = returnPolicyReturnsAcceptedOption;
	}


	public String getReturnPolicyReturnsAccepted() {
		return returnPolicyReturnsAccepted;
	}


	public void setReturnPolicyReturnsAccepted(String returnPolicyReturnsAccepted) {
		this.returnPolicyReturnsAccepted = returnPolicyReturnsAccepted;
	}


	public String getReturnPolicydescription() {
		return returnPolicydescription;
	}


	public void setReturnPolicydescription(String returnPolicydescription) {
		this.returnPolicydescription = returnPolicydescription;
	}


	public String getReturnPolicyShippingCostPaidByOption() {
		return returnPolicyShippingCostPaidByOption;
	}


	public void setReturnPolicyShippingCostPaidByOption(
			String returnPolicyShippingCostPaidByOption) {
		this.returnPolicyShippingCostPaidByOption = returnPolicyShippingCostPaidByOption;
	}


	public String getReturnPolicyShippingCostPaidBy() {
		return returnPolicyShippingCostPaidBy;
	}


	public void setReturnPolicyShippingCostPaidBy(
			String returnPolicyShippingCostPaidBy) {
		this.returnPolicyShippingCostPaidBy = returnPolicyShippingCostPaidBy;
	}


	public String getReturnPolicyWarrantyDuration() {
		return returnPolicyWarrantyDuration;
	}


	public void setReturnPolicyWarrantyDuration(String returnPolicyWarrantyDuration) {
		this.returnPolicyWarrantyDuration = returnPolicyWarrantyDuration;
	}


	public String getReturnPolicyWarrantyDurationOption() {
		return returnPolicyWarrantyDurationOption;
	}


	public void setReturnPolicyWarrantyDurationOption(
			String returnPolicyWarrantyDurationOption) {
		this.returnPolicyWarrantyDurationOption = returnPolicyWarrantyDurationOption;
	}


	public String getReturnPolicyWarrantyOffered() {
		return returnPolicyWarrantyOffered;
	}


	public void setReturnPolicyWarrantyOffered(String returnPolicyWarrantyOffered) {
		this.returnPolicyWarrantyOffered = returnPolicyWarrantyOffered;
	}


	public String getReturnPolicyWarrantyOfferedOption() {
		return returnPolicyWarrantyOfferedOption;
	}


	public void setReturnPolicyWarrantyOfferedOption(
			String returnPolicyWarrantyOfferedOption) {
		this.returnPolicyWarrantyOfferedOption = returnPolicyWarrantyOfferedOption;
	}


	public String getReturnPolicyWarrantyType() {
		return returnPolicyWarrantyType;
	}


	public void setReturnPolicyWarrantyType(String returnPolicyWarrantyType) {
		this.returnPolicyWarrantyType = returnPolicyWarrantyType;
	}


	public String getReturnPolicyWarrantyTypeOption() {
		return returnPolicyWarrantyTypeOption;
	}


	public void setReturnPolicyWarrantyTypeOption(
			String returnPolicyWarrantyTypeOption) {
		this.returnPolicyWarrantyTypeOption = returnPolicyWarrantyTypeOption;
	}


	public ShippingDetailsType getShippingDetails() {
		return shippingDetails;
	}


	public void setShippingDetails(ShippingDetailsType shippingDetails) {
		this.shippingDetails = shippingDetails;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public String getSku() {
		return sku;
	}


	public void setSku(String sku) {
		this.sku = sku;
	}


	public String getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getIsBestOfferEnabled() {
		
		return isBestOfferEnabled.equalsIgnoreCase("null") ?"":isBestOfferEnabled;
	}


	public void setIsBestOfferEnabled(String isBestOfferEnabled) {
		this.isBestOfferEnabled = isBestOfferEnabled;
	}


	public String getIsSkypeEnabled() {
		return isSkypeEnabled.equalsIgnoreCase("null") ? "" :isSkypeEnabled;
	}


	public void setIsSkypeEnabled(String isSkypeEnabled) {
		this.isSkypeEnabled = isSkypeEnabled;
	}


	public String getIsAutoPay() {
		return isAutoPay.equalsIgnoreCase("null")?"":isAutoPay;
	}


	public void setIsAutoPay(String isAutoPay) {
		this.isAutoPay = isAutoPay;
	}


	public String getIsDisableBuyerRequirements() {
		return isDisableBuyerRequirements;
	}


	public void setIsDisableBuyerRequirements(String isDisableBuyerRequirements) {
		this.isDisableBuyerRequirements = isDisableBuyerRequirements;
	}



	public String getListingTypeIsAdult() {
		return listingTypeIsAdult;
	}



	public void setListingTypeIsAdult(String listingTypeIsAdult) {
		this.listingTypeIsAdult = listingTypeIsAdult;
	}



	public String getListingTypeIsBindingAuction() {
		return listingTypeIsBindingAuction;
	}



	public void setListingTypeIsBindingAuction(String listingTypeIsBindingAuction) {
		this.listingTypeIsBindingAuction = listingTypeIsBindingAuction;
	}



	public String getListingTypeIsBuyItNowAvailable() {
		return listingTypeIsBuyItNowAvailable;
	}



	public void setListingTypeIsBuyItNowAvailable(
			String listingTypeIsBuyItNowAvailable) {
		this.listingTypeIsBuyItNowAvailable = listingTypeIsBuyItNowAvailable;
	}



	public String getListingTypeIsCheckoutEnabled() {
		return listingTypeIsCheckoutEnabled;
	}



	public void setListingTypeIsCheckoutEnabled(String listingTypeIsCheckoutEnabled) {
		this.listingTypeIsCheckoutEnabled = listingTypeIsCheckoutEnabled;
	}



	public String getListingTypeMinimumBestOfferPrice() {
		return listingTypeMinimumBestOfferPrice;
	}



	public void setListingTypeMinimumBestOfferPrice(
			String listingTypeMinimumBestOfferPrice) {
		this.listingTypeMinimumBestOfferPrice = listingTypeMinimumBestOfferPrice;
	}



	public String getListingTypeBestOfferAutoAcceptPrice() {
		return listingTypeBestOfferAutoAcceptPrice;
	}



	public void setListingTypeBestOfferAutoAcceptPrice(
			String listingTypeBestOfferAutoAcceptPrice) {
		this.listingTypeBestOfferAutoAcceptPrice = listingTypeBestOfferAutoAcceptPrice;
	}



	public void setListingType(ListingTypeCodeType listingType) {
		this.listingType = listingType;
	}



	public String getWatchCount() {
		return watchCount;
	}



	public void setWatchCount(String watchCount) {
		this.watchCount = watchCount;
	}



	public String getBestOffer() {
		return bestOffer;
	}



	public void setBestOffer(String bestOffer) {
		this.bestOffer = bestOffer;
	}



	public String getBestOfferCount() {
		return bestOfferCount;
	}



	public void setBestOfferCount(String bestOfferCount) {
		this.bestOfferCount = bestOfferCount;
	}



	public String getBestOfferStatus() {
		return bestOfferStatus;
	}



	public void setBestOfferStatus(String bestOfferStatus) {
		this.bestOfferStatus = bestOfferStatus;
	}



	public String getBestOfferType() {
		return bestOfferType;
	}



	public void setBestOfferType(String bestOfferType) {
		this.bestOfferType = bestOfferType;
	}

	

	public String getBestOfferEnabled() {
		return bestOfferEnabled;
	}



	public void setBestOfferEnabled(String bestOfferEnabled) {
		this.bestOfferEnabled = bestOfferEnabled;
	}



	public String getDistance() {
		if(this.distance != null){
			return this.distance.getDistanceUnit() + "," + this.distance.getDistanceMeasurement();
		}
		return "";
	}



	public void setDistance(DistanceType distance) {
		this.distance = distance;
	}



	public String getItemPolicyViolationId() {
		return itemPolicyViolationId;
	}



	public void setItemPolicyViolationId(String itemPolicyViolationId) {
		this.itemPolicyViolationId = itemPolicyViolationId;
	}



	public String getItemPolicyViolationText() {
		return itemPolicyViolationText;
	}



	public void setItemPolicyViolationText(String itemPolicyViolationText) {
		this.itemPolicyViolationText = itemPolicyViolationText;
	}

	public String getEbayNotes() {
		return ebayNotes;
	}

	public void setEbayNotes(String ebayNotes) {
		this.ebayNotes = ebayNotes;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFloorPrice() {
		return customeAmountTypeValue(floorPrice);
	}

	public void setFloorPrice(AmountType floorPrice) {
		
		this.floorPrice = floorPrice;
	}



	public String getPayMentDetailDaysToFullPayment() {
		return payMentDetailDaysToFullPayment;
	}



	public void setPayMentDetailDaysToFullPayment(
			String payMentDetailDaysToFullPayment) {
		this.payMentDetailDaysToFullPayment = payMentDetailDaysToFullPayment;
	}



	public String getPayMentDetailDepositAmount() {
		return payMentDetailDepositAmount;
	}



	public void setPayMentDetailDepositAmount(String payMentDetailDepositAmount) {
		this.payMentDetailDepositAmount = payMentDetailDepositAmount;
	}



	public String getPayMentDetailDepositType() {
		return payMentDetailDepositType;
	}



	public void setPayMentDetailDepositType(String payMentDetailDepositType) {
		this.payMentDetailDepositType = payMentDetailDepositType;
	}



	public String getPayMentHoursToDeposit() {
		return payMentHoursToDeposit;
	}



	public void setPayMentHoursToDeposit(String payMentHoursToDeposit) {
		this.payMentHoursToDeposit = payMentHoursToDeposit;
	}



	public PaymentDetailsType getPaymentDetailsType() {
		return paymentDetailsType;
	}



	public void setPaymentDetailsType(PaymentDetailsType paymentDetailsType) {
		
		this.paymentDetailsType = paymentDetailsType;
		if(paymentDetailsType != null){
			this.setPayMentDetailDaysToFullPayment(paymentDetailsType.getDaysToFullPayment()+"");
			this.setPayMentDetailDepositAmount(customeAmountTypeValue(paymentDetailsType.getDepositAmount()));
			this.setPayMentDetailDepositType(paymentDetailsType.getDepositType().value());
			this.setPayMentHoursToDeposit(paymentDetailsType.getHoursToDeposit()+"");
		}
	}
	
	
	
	
	
	 
	
	
 
}
