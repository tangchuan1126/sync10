package com.cwc.listener;

import java.util.Vector;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 在线人数统计
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Online implements HttpSessionListener
{
	private static Vector onlineCustomer = new Vector();
	
	public void sessionCreated(HttpSessionEvent arg0)
	{
		onlineCustomer.add(arg0.getSession().getId());
	}

	public void sessionDestroyed(HttpSessionEvent arg0)
	{
		onlineCustomer.remove(arg0.getSession().getId());
	}
	
	public static int getOnlineCount()
	{
		return(onlineCustomer.size());
	}
}
