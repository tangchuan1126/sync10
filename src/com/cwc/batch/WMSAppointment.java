package com.cwc.batch;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.asynchronized.ThreadAction;

public class WMSAppointment extends ThreadAction {

	private long ps_id;
	private YMSMgrAPI ymsMgrAPI;
	private String start_date;
	private String end_date;
	private boolean del_flag;
	
	public WMSAppointment(long ps_id,YMSMgrAPI ymsMgrAPI,String start_date,String end_date, boolean del_flag) 
	{
		this.ps_id = ps_id;
		this.ymsMgrAPI = ymsMgrAPI;
		this.start_date = start_date;
		this.end_date = end_date;
		this.del_flag = del_flag;
	}
	
	@Override
	protected void perform() 
		throws Exception 
	{
		ymsMgrAPI.getAppointment(ps_id, start_date, end_date, del_flag);
	}

}
