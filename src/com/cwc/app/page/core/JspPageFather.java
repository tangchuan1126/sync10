package com.cwc.app.page.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;

/**
 * 所有jsp模板类都需要实现该接口
 * 该类目前除了实现父类方法外，没有自己扩展方法，留作扩展使用
 * 
 * @author TurboShop
 * TurboShop.cn all rights reserved.
 */
public abstract class JspPageFather  extends PageFatherController
{
	/**
	 * jsp模板无需实现,velocity模板实现
	 * @param request
	 * @param response
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public final void doGet(HttpServletRequest request, HttpServletResponse response)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException, Exception
	{
			
	}

	/**
	 * jsp模板无需实现,velocity模板实现
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public final Template getFatherTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException, Exception
	{
		return(null);
	}
	
	/**
	 * jsp模板子类必须事先，返回模板路径，形式如：a/b(b.jsp)
	 * @param request
	 * @param response
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
    public abstract String doGetJsp( HttpServletRequest request, HttpServletResponse response ) 
		throws MemberNotLoginException, PageNotFoundException, OperationNotPermitException,Exception;
}
