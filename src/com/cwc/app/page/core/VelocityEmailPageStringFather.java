package com.cwc.app.page.core;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.velocity.context.Context;

import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;

/**
 * 所有模板页面需要继承的父类
 * 目的是为了做一些公共处理，增强扩展性
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public abstract class VelocityEmailPageStringFather implements VelocityEmailPageStringInterface
{
	static Logger log = Logger.getLogger("PAGE");
	
	public ArrayList<String> star = new ArrayList<String>();
	private ArrayList<DBRow> appendPara = new ArrayList<DBRow>();			//存放外部参数
	private String body;//框架把解释后的模板内容塞进这里
	
	/**
	 * 初始化一些公共类，方便子类使用
	 *
	 */
	public VelocityEmailPageStringFather()
	{
	}
	
	/**
	 * 从外部给模板传入参数
	 * 主要用在一些提交的action，需要重定向到一些页面，并且需要传递参数
	 * @param name
	 * @param val
	 */
	public void putPara(String name,Object val)
	{
		DBRow v = new DBRow();
		v.add("name", name);
		v.add("val", val);
		appendPara.add(v);
	}
	
    /**
 	 * 初始化一些全局常量
	 * 所有页面均可以使用
     * @param request
     * @param response
     * @param context
     */
    private void initGlobeContext(Context context)
    {
        //context.put("systenFolder", ConfigBean.getStringValue("systenFolder") );	//系统路径
    }
	
	/**
	 * 该方法由子类实现
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public abstract String getTemplate(Context context,String encodeing)
		throws Exception;
	
	/**
	 * 默认获得邮件模板标题处理方法
	 * 子类可以重载自己的分解规则
	 * @return
	 * @throws Exception
	 */
	public String getTemplateTitle()
		throws Exception 
	{
		String title = "";
		
		if ( this.getBody().indexOf("<title>")>=0&&this.getBody().indexOf("</title>")>=0 )
		{
			String t[] = this.getBody().split("<title>");
			title = t[1];
			title = title.split("</title>")[0];
		}
		
		return (title);
	}

	/**
	 * 总控制SERVLET调用的方法，获得模板
	 * 该方法主要目的是在饭或模板前，做一些设局的参数设置
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public String getFatherTemplate(Context context,String encodeing)
		throws Exception
	{
		initGlobeContext(context);
		
		//追加外部参数
		if (appendPara.size()>0)
		{
			for (int i=0; i<appendPara.size(); i++)
			{
				DBRow v = (DBRow)appendPara.get(i);
				context.put(v.getString("name"),v.get("val", new Object()));
			}
		}
		
		return( getTemplate( context, encodeing) );
	}

	public final void setBody(String body) 
	{
		this.body = body;
	}

	public final String getBody() 
	{
		return body;
	}
	
	//邮件发送帐号信息接口，子类必须实现
	public abstract String getUserName()
		throws Exception;
	
	public abstract String getPassWord()
		throws Exception;
	
	public abstract String getHost()
		throws Exception;
	
	public abstract int getPort()
		throws Exception;
	
	public abstract String getAuthor()
		throws Exception;
	
	public abstract String getRePly()
		throws Exception;
	
}




