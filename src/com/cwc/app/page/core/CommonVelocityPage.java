package com.cwc.app.page.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;

/**
 * 通用模板展示页面类
 * 为了避免一些纯粹为了展示固定文字图片内容页面而新建一个class
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CommonVelocityPage extends VelocityPageFather 
{
	private String velocityPage;

	public Template getTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception 
	{
		Template t = ve.getTemplate( velocityPage,encodeing);
	
		return(t);
	}

	public void setVelocityPage(String velocityPage)
	{
		this.velocityPage = velocityPage;
	}
}
