package com.cwc.app.page.core;


import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 为了兼容原来模板架构，该类将会模拟一个serlvet
 * 实现它的子类将会是一个spring mvc controller，会被通过请求mapping方式直接装配
 * @author Administrator
 *
 */
public abstract class PageFatherController implements Controller
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	public PageFatherController()
	{
	}

	public ModelAndView handleRequest(HttpServletRequest request,HttpServletResponse response)
		throws Exception
	{
		try
		{
			if (this instanceof VelocityPageFather)
			{
				//由于使用系统原velocity模板架构，这里返回null，不走spring mvc 渲染模板流程，由原模板架构内容直接输出
				VelocityTemplate.getInstance().doGet(request, response,  (VelocityPageFather)this);
			}
			else if (this instanceof JspPageFather)	//	jsp模板输出
			{
				return(new ModelAndView(doGetJsp(  request, response )));
			}
			else
			{
				log.error("PageFatherController.handleRequest:Can confirm page class template type!");
				throw new PageNotFoundException();
			}
		} 
		catch (MemberNotLoginException e)
		{
			response.sendRedirect(ConfigBean.getStringValue("systenFolder").concat("member/login.html?backurl=").concat(StringUtil.getCurrentURL(request)));
		}
		catch (PageNotFoundException e)
		{
			response.sendRedirect(ConfigBean.getStringValue("systenFolder").concat("404.html"));
		}
		catch (RedirectException e)
		{
			response.sendRedirect(e.getMessage());
		}
		catch (Exception e)
		{
			StringBuffer sb = new StringBuffer("");
			Enumeration enu = request.getParameterNames();
			String key;
			while( enu.hasMoreElements() )
			{
				key = (String)enu.nextElement();
				sb.append(key);
				sb.append("=");
				sb.append(StringUtil.getString(request,key));
				sb.append("&");
			}
			
			log.error("PageFatherController.handleRequest:" + e);
			log.error("Referer:" + request.getHeader("referer"));
			log.error("Current URL:" + request.getRequestURI());
			log.error("Parameters:" + sb.toString());
			log.error("IP:" + request.getRemoteAddr());
			
			//如果是系统管理员操作，则记录下操作人
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			if (adminLoggerBean!=null)
			{
				log.error("System operator:" + adminLoggerBean.getAccount());
			}
			else
			{
				log.error("System operator:NULL");
			}
			
			
			throw new SystemException(e,"PageFatherController.handleRequest error:",log);
		}

		
		return(null);
	}
	
	/**
	 * jsp模板类必须实现该方法
	 * @param request
	 * @param response
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
    public String doGetJsp( HttpServletRequest request, HttpServletResponse response ) 
		throws MemberNotLoginException, PageNotFoundException, OperationNotPermitException,Exception
	{
    	return(null);
	}

	public abstract Template getFatherTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing)
	throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException, Exception ;
}
