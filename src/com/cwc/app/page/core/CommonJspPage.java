package com.cwc.app.page.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.page.core.JspPageFather;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;

public class CommonJspPage extends JspPageFather
{
	private String jspPage;
	
	public String doGetJsp(HttpServletRequest request,HttpServletResponse response) 
		throws MemberNotLoginException,PageNotFoundException, OperationNotPermitException, Exception
	{
		int jspPos = jspPage.lastIndexOf(".jsp");
		return(jspPage.substring(0,jspPos));
	}

	public void setJspPage(String jspPage)
	{
		this.jspPage = jspPage;
	}

}
