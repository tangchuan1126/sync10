package com.cwc.app.page.core;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.ConfigBean;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * page前置拦截器，管理后台页面资源鉴权
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class EntityActionAuthenBeforeAdvice extends HandlerInterceptorAdapter 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private AdminMgr adminMgr;
	private String cutPath = "administrator/";
	
    public boolean preHandle(HttpServletRequest request,HttpServletResponse response,Object handler) 
    	throws Exception 
    {
//    	String controlPage = request.getHeader("referer");
    	String controlAction = StringUtil.getCurrentURL(request);
    	int operate_type = 2;	//1-页面，2-action

    	try
		{
    		controlAction = java.net.URLDecoder.decode(controlAction,"utf-8");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));		
			int endSub;
			if(controlAction.contains("?"))
			{
				endSub = controlAction.indexOf("?");
			}
			else
			{
				endSub = controlAction.length();
			}
			
			String page_name = controlAction.substring(controlAction.indexOf(cutPath)+cutPath.length(),endSub);
    		
			//记录系统日志
			if (controlAction.indexOf("paypalNotify.action")==-1)
			{
				StringBuffer sb = new StringBuffer("");
				Enumeration enu = request.getParameterNames();
				String key;
				while( enu.hasMoreElements() )
				{
					key = (String)enu.nextElement();
					sb.append(key);
					sb.append("=");
					sb.append(StringUtil.getString(request,key));
					sb.append("&");
				}
				
//				if (handler instanceof PageFatherController==false)
//				{
//					operate_type = 2;
//				}
				
				//ThreadManager.getInstance().executeInBackground(new AddSysLog(adminLoggerBean,operate_type,controlPage,sb.toString(),request.getRemoteAddr(),request.getHeader("referer"),request.getHeader("User-Agent")));
				SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
				systemConfig.addSysLog(adminLoggerBean,operate_type,page_name,sb.toString(),request.getRemoteAddr(),request.getHeader("referer"),request.getHeader("User-Agent"));
			}
			
			//只对页面page action鉴权    	
//			if (handler instanceof PageFatherController)
//			{
//				//如果用户请求直接访问页面action，则跳转到首页，防止绕过urlrewrite
//				if (controlPage.equals(controlPageAction))
//				{
//					response.sendRedirect(ConfigBean.getStringValue("systenFolder"));
//					return(false);
//				}
//				
//				//只对管理后台页面鉴权
//				if ( controlPage.indexOf("administrator") >= 0 )
//				{
//					//这里先做页面鉴权
//					//验证不通过，会抛出OperationNotPermitException
//					AdminAuthenCenter.isPermitPage(page_name,adminLoggerBean.getAdgid(),adminLoggerBean.getAdid());
//				}
//			}
		}
//    	catch (OperationNotPermitException e)
//		{
//			ArrayList<DBRow> appendPara = new ArrayList<DBRow>();					//传给模板的参数
//			String DEFAULT_NOT_PERMIT_PAGE = "not_permit";							//默认没有操作权限JSP错误处理页面
//			
//			controlPage = java.net.URLDecoder.decode(controlPage,"utf-8");
//			String page_name = controlPage.substring(controlPage.indexOf(cutPath)+cutPath.length(), controlPage.length());
//			String page_name = e.getMessage();
//			////system.out.println("==> "+page_name);
//			DBRow detailPage = adminMgr.getDetailControlTreeByLink(page_name);
//			
//			if (detailPage==null)
//			{
//				page_name = page_name.substring(0,page_name.indexOf("?"));
//				detailPage = adminMgr.getDetailControlTreeByLink(page_name);
//			}
//			
//			DBRow v = new DBRow();
//			v.add("name", "operate");
//			v.add("val",detailPage.getString("title"));
//			appendPara.add(v);
//
//			ModelAndView jspMV = MvcUtil.dispatch2Jsp( request, response,DEFAULT_NOT_PERMIT_PAGE,appendPara);
//			
//			//返回没有操作权限视图（即交由spring做流程转向）
//			throw new ModelAndViewDefiningException(jspMV);
//			
//		}
    	catch (Exception e)
		{
    		log.error("EntityActionAuthenBeforeAdvice.preHandle error:"+e);
		}
    	
		return(true);
    }

	public void setAdminMgr(AdminMgr adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	
}



