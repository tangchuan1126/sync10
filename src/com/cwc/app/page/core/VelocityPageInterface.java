package com.cwc.app.page.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;

/**
 * 模板页面需要实现的接口
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface VelocityPageInterface
{
	/**
	 * 模板页面实现的方法
	 * @param request								
	 * @param response
	 * @param ve										模板引擎
	 * @param context									模板上下文
	 * @param encodeing									模板页面编码
	 * @return
	 * @throws MemberNotLoginException					会员没有登录异常
	 * @throws PageNotFoundException					没有找到页面异常
	 * @throws OperationNotPermitException				没有操作权限异常
	 * @throws RedirectException						重定向异常
	 * @throws Exception
	 */
	public Template getTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing) throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception ;

	/**
	 * 父模板页面实现的方法
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws OperationNotPermitException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public Template getFatherTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing) throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception ; 
}
