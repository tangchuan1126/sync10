package com.cwc.app.page.core;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.io.VelocityWriter;
import org.apache.velocity.util.SimplePool;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;;

/**
 * 模板内容输出,该类doget()会被spring mvc controller调用，从而输出模板内容
 * 
 * 被装配的模板页面类关系：pageclass extends PageFather extends MyVelocityServlet extends FatherController implements Controller
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class VelocityTemplate 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
    private static SimplePool writerPool = new SimplePool(40);			//输出池
    protected static String DEFAULT_ENCODING = "UTF-8";    				//默认模板编码
    protected String DEFAULT_CONTENTTYPE = "text/html; charset=UTF-8";    	//默认输出编码
    protected static String home = Environment.getHome();					//系统安装路径
    private static String DEFAULT_TEMPLATE = "default";					//默认模板文件夹
    protected static VelocityEngine ve = null;
    
    private static VelocityTemplate velocityTemplate = null;
	
    public synchronized static VelocityTemplate getInstance()
    	throws Exception
    {
    	if (velocityTemplate==null)
    	{
    		velocityTemplate = new VelocityTemplate();
    	}
    	
    	return(velocityTemplate);
    }
    
    /**
     * 初始化模板参数
     * @param config
     * @throws ServletException
     */
    private VelocityTemplate()
        throws Exception
    {
        try 
        {
			initVelocityEngine( getVecPro() );
		}
        catch (Exception e) 
        {
        	throw new SystemException(e,"VelocityServlet.init error:",log);
		}
    }
    
    /**
     * 获得正在使用的模板
     * @return
     */
    public static String getUseingTemplate()
    {
    	//从容器获得systemConfig
    	SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");

    	try
    	{
			DEFAULT_TEMPLATE = systemConfig.getUsedTemplateName();		//从数据库获得目前使用的模板
		}
    	catch (Exception e) 
    	{
			log.error("MyVelocityServlet getUseingTemplate error:"+e);
		}
    	return(DEFAULT_TEMPLATE);
    }

    /**
     * 初始化模板参数
     * @return
     */
    private Properties getVecPro()
    {
    	Properties p = new Properties();
    	p.setProperty(Velocity.RUNTIME_LOG, home+"/../../VVMElogs/velocity.log");								//模板日志
    	p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, home+"user_case/"+getUseingTemplate()+"/");		//模板目录
    	p.setProperty(Velocity.INPUT_ENCODING, DEFAULT_ENCODING);											//模板页面编码
    	p.setProperty(Velocity.OUTPUT_ENCODING, DEFAULT_ENCODING);											//模板输出编码
    	p.setProperty(Velocity.COUNTER_INITIAL_VALUE,"0");													//模板循环开始INDEX
    	p.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS,"org.apache.velocity.runtime.log.Log4JLogChute");//使用log4j输出日志
    	
    	//p.setProperty(Velocity.FILE_RESOURCE_LOADER_CACHE,ConfigBean.getStringValue("templatecache"));		//是否缓存模板  设置不起作用？？
    	
    	//directive.foreach.counter.initial.value
    	return(p);
    }

    /**
     * 重新加载模板
     * @throws Exception
     */
    public void reInitVelocityEngine()
    	throws Exception
    {
    	initVelocityEngine( getVecPro() );
    }
    
    /**
     * 切换模板
     * 
     *
     */
    public static void changeTemplate()
    {
    	ve = null;
    }
    
    /**
     * 初始化模板引擎
     * @param p
     * @throws Exception
     */
    private void initVelocityEngine(Properties p) 
    	throws Exception
    {
		try
		{
			ve = new VelocityEngine();
			ve.init(p);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"VelocityServlet.initVelocityEngine error:",log);
		}    	
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response,VelocityPageFather velocityPage)
        throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception 
    {

    	try 
    	{
    		//如果VE==null，证明模板被切换过，需要重新初始化
    		if (ve==null)
    		{
    			reInitVelocityEngine();    			
    		}
		} 
    	catch (Exception e) 
    	{
			log.error("MyVelocityServlet reInitVelocityEngine error"+e);
			throw new Exception();
		}
    	
        doRequest(request, response, velocityPage);
    }

    protected void doRequest(HttpServletRequest request, HttpServletResponse response,VelocityPageFather velocityPage )
         throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception 
    {
        Context context = null;
        try
        {

            context = createContext( request, response );					//创建上下文
            //response.setContentType(DEFAULT_CONTENTTYPE);
            response.setHeader("Content-Type",DEFAULT_CONTENTTYPE);			//设置输出头

            Template template = velocityPage.getFatherTemplate(request,response,ve,context,DEFAULT_ENCODING);	//获得模板

            if ( template == null )
            {
            	log.error("VelocityServlet.doRequest error:template is null!");
            }

            //输出模板内容
            mergeTemplate( template, context, response );
        }
        catch (MemberNotLoginException e)
        {
        	throw new MemberNotLoginException(e.getMessage());
        }
        catch (PageNotFoundException e)
        {
        	throw new PageNotFoundException(e.getMessage());
        }
        catch (OperationNotPermitException e)
        {
        	throw new OperationNotPermitException(e.getMessage());
        }
		catch (RedirectException e)
		{
			throw new RedirectException(e.getMessage());
		}
        catch (Exception e)
        {
        	log.error("VelocityServlet.doRequest error:"+e);
    		//把堆栈信息记录到日志
    		StackTraceElement[] ste = e.getStackTrace();
    		StringBuffer sb = new StringBuffer();
    		sb.append(e.getMessage() + "\r\n");
    		for (int i = 0;i < ste.length;i++)
    		{
    			sb.append(ste[i].toString() + "\r\n");
    		}
    		log.error(sb.toString());

            error( request, response, e);
        }
        finally
        {
            requestCleanup( request, response, context );
        }
    }

    protected void requestCleanup( HttpServletRequest request, HttpServletResponse response, Context context )
    {
    }

    /**
     * 输出模板内容
     * @param template								模板
     * @param context								上下文
     * @param response						
     * @throws ResourceNotFoundException			重定向
     * @throws ParseErrorException					模板解析错误
     * @throws MethodInvocationException			方法错误
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws Exception
     */
    protected void mergeTemplate( Template template, Context context, HttpServletResponse response )
        throws ResourceNotFoundException, ParseErrorException,
               MethodInvocationException, IOException, UnsupportedEncodingException, Exception
    {
        ServletOutputStream output = response.getOutputStream();
        VelocityWriter vw = null;

        try
        {
        	//从池获得一个writer，提高输出性能
            vw = (VelocityWriter) writerPool.get();

            if (vw == null)
            {
                vw = new VelocityWriter(new OutputStreamWriter(output,DEFAULT_ENCODING),4 * 1024, true);
            }
            else
            {
                vw.recycle(new OutputStreamWriter(output, DEFAULT_ENCODING));
            }

            //输出模板
            template.merge(context, vw);
        }
        finally
        {
            if (vw != null)
            {
                try
                {
                    vw.flush();
                }
                catch (IOException e)
                {

                }
                vw.recycle(null);
                writerPool.put(vw);
            }
        }
    }

    protected void setContentType(String contentType)
    {
    	DEFAULT_CONTENTTYPE = contentType;
    }

    protected Context createContext(HttpServletRequest request,  HttpServletResponse response )
    {
        VelocityContext context = new VelocityContext();
        return context;
    }

    protected Template handleRequest( HttpServletRequest request, HttpServletResponse response, Context ctx )
        throws Exception
    {
    	return(null);
    }
    
    /**
     * 向模板输出错误信息
     * @param request
     * @param response
     * @param cause
     * @throws ServletException
     * @throws IOException
     */
    protected void error( HttpServletRequest request, HttpServletResponse response, Exception cause )
        throws ServletException, IOException
    {
    	response.setHeader("Content-Type",DEFAULT_CONTENTTYPE);
        StringBuffer html = new StringBuffer();
        html.append("<html>");
        html.append("<title>Error</title>");
        html.append("<body bgcolor=\"#ffffff\">");
        html.append("Template error,please contact administrator.<br>Click <a href='"+ConfigBean.getStringValue("systenFolder")+"'><b>here</b></a> return to index.");
        html.append("</body>");
        html.append("</html>");
        response.getOutputStream().print( html.toString() );
    }
}
