package com.cwc.app.page.core;

import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

/**
 * 模板内容输出,该类doget()会被spring mvc controller调用，从而输出模板内容
 * 
 * 被装配的模板页面类关系：pageclass extends PageFather extends MyVelocityServlet extends FatherController implements Controller
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class VelocityTemplateString 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
    protected static String DEFAULT_ENCODING = "UTF-8";    				//默认模板编码
    protected String DEFAULT_CONTENTTYPE = "text/html; charset=UTF-8";    	//默认输出编码
    protected static String home = Environment.getHome();					//系统安装路径
    
    private static VelocityTemplateString velocityTemplate = null;
	
    public synchronized static VelocityTemplateString getInstance()
    	throws Exception
    {
    	if (velocityTemplate==null)
    	{
    		velocityTemplate = new VelocityTemplateString();
    	}
    	
    	return(velocityTemplate);
    }
    
    /**
     * 初始化模板参数
     * @param config
     * @throws ServletException
     */
    private VelocityTemplateString()
        throws Exception
    {
    }

    
    /**
     * 获得模板内容
     * @param velocityPage
     * @return 
     * @return
     * @throws Exception
     */
    public void getPageContent( VelocityEmailPageStringFather velocityPage)
	    throws Exception 
	{
	    doPageContent(velocityPage);
	}
	    
    protected void doPageContent(VelocityEmailPageStringFather velocityPage )
	    throws Exception 
	{
	   Context context = null;
	   String html = "";
	   
	   try
	   {
	       context = createContext();					//创建上下文
	       String template = velocityPage.getFatherTemplate(context,DEFAULT_ENCODING);	//获得模板

	       StringWriter sw = new StringWriter();
	       Velocity.evaluate( context, sw, "", template );//模板变量替换，整合模板内容

	       html = sw.toString();
	       //把解释后的模板内容塞入标题函数，提供标题函数提取标题用
	       velocityPage.setBody(html);
	   }
	   catch (Exception e)
	   {
		   throw new SystemException(e,"doPageContent",log);
	   }
	   finally
	   {
	       requestCleanup(  context );
	   }
	   
	}
	    
    
    
    
    
    

    protected void requestCleanup( Context context )
    {
    }

    protected void setContentType(String contentType)
    {
    	DEFAULT_CONTENTTYPE = contentType;
    }

    protected Context createContext()
    {
        VelocityContext context = new VelocityContext();
        return context;
    }

    protected Template handleRequest( HttpServletRequest request, HttpServletResponse response, Context ctx )
        throws Exception
    {
    	return(null);
    }
    
    
	public static void main(String args[]) throws Exception
	{
        /* first, we init the runtime engine.  Defaults are fine. */

        //Velocity.init();

        /* lets make a Context and put data into it */

        VelocityContext context = new VelocityContext();

        context.put("name", "Velocity");
        context.put("project", "Jakarta");
        
        DBRow returnSubProducts[] = new DBRow[2];
        
        returnSubProducts[0] = new DBRow();
        returnSubProducts[0].add("p_name", "A1");
        
        returnSubProducts[1] = new DBRow();
        returnSubProducts[1].add("p_name", "A2");
        
        context.put("returnSubProducts", returnSubProducts);
        
        
        /* lets render a template */

        StringWriter w = new StringWriter();

        /* lets make our own string to render */

        String s = "We are using $project $name to render this. #foreach ( $returnSubProduct in ${returnSubProducts} ) ${returnSubProduct.getString('p_name')} #end";
        w = new StringWriter();
        Velocity.evaluate( context, w, "", s );
        ////system.out.println(" string : " + w );

	}
	    
    

}
