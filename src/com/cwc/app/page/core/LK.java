package com.cwc.app.page.core;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.util.ConfigBean;

/**
 * 处理模板中图片路径，转换为网站相对路径
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class LK  extends HttpServlet
{
    public void doGet( HttpServletRequest request, HttpServletResponse response )
	    throws ServletException, IOException
	{
	    	String path = request.getPathInfo();
	    	String currentURI = request.getRequestURI();
	    	String root = ConfigBean.getStringValue("systenFolder");

    		try 
    		{
    			if (currentURI.indexOf(root+"imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/imgs"+path;
    			}
    			else if (currentURI.indexOf(root+"product/imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/product/imgs"+path;
    			}
    			else if (currentURI.indexOf(root+"news/imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/news/imgs"+path;
    			}
    			else if (currentURI.indexOf(root+"member/imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/member/imgs"+path;
    			}
    			else if (currentURI.indexOf(root+"catalog/imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/catalog/imgs"+path;
    			}
    			else if (currentURI.indexOf(root+"css/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/css"+path;
    			}
    			else if (currentURI.indexOf(root+"js/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/js"+path;
    			}
    			else if (currentURI.indexOf(root+"group/css/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/group/css"+path;
    			}
    			else if (currentURI.indexOf(root+"group/js/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/group/js"+path;
    			}
    			else if (currentURI.indexOf(root+"group/imgs/")==0)
    			{
    				path = "/user_case/" + VelocityTemplate.getUseingTemplate() + "/group/imgs"+path;
    			}
			}
    		catch (Exception e) 
    		{
				e.printStackTrace();
			}

	    	RequestDispatcher rd = getServletContext().getRequestDispatcher(path);
	    	rd.forward(request, response);
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
	    throws ServletException, IOException
	{
		doGet( request, response );
	}
}
