package com.cwc.app.page.core;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.app.api.MemberMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;
import com.cwc.util.StringUtil;

/**
 * 所有模板页面需要继承的父类
 * 目的是为了做一些公共处理，增强扩展性
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public abstract class VelocityPageFileFather4Email implements VelocityPageFileInterface4Email 
{
	static Logger log = Logger.getLogger("PAGE");
	
	public ArrayList<String> star = new ArrayList<String>();
	
	protected MemberMgr memberMgr = null;
	
	private ArrayList<DBRow> appendPara = new ArrayList<DBRow>();			//存放外部参数
	
	/**
	 * 初始化一些公共类，方便子类使用
	 *
	 */
	public VelocityPageFileFather4Email()
	{
	}
	
	/**
	 * 从外部给模板传入参数
	 * 主要用在一些提交的action，需要重定向到一些页面，并且需要传递参数
	 * @param name
	 * @param val
	 */
	public void putPara(String name,Object val)
	{
		DBRow v = new DBRow();
		v.add("name", name);
		v.add("val", val);
		appendPara.add(v);
	}
	
    /**
 	 * 初始化一些全局常量
	 * 所有页面均可以使用
     * @param request
     * @param response
     * @param context
     */
    private void initGlobeContext(Context context)
    {
        //context.put("systenFolder", ConfigBean.getStringValue("systenFolder") );	//系统路径
    }
	
	/**
	 * 该方法由子类实现
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public abstract Template getTemplate(VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception;

	/**
	 * 总控制SERVLET调用的方法，获得模板
	 * 该方法主要目的是在饭或模板前，做一些设局的参数设置
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public Template getFatherTemplate(VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception
	{
		//追加外部参数
		if (appendPara.size()>0)
		{
			for (int i=0; i<appendPara.size(); i++)
			{
				DBRow v = (DBRow)appendPara.get(i);
				context.put(v.getString("name"),v.get("val", new Object()));
			}
		}
		
		return( getTemplate( ve, context, encodeing) );
	}

}




