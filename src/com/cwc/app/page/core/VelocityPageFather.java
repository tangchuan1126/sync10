package com.cwc.app.page.core;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cwc.app.api.MemberMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;
import com.cwc.initconf.Resource;
import com.cwc.util.HtmlUtil;
import com.cwc.util.StringUtil;

/**
 * 所有模板页面需要继承的父类
 * 目的是为了做一些公共处理，增强扩展性
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public abstract class VelocityPageFather extends PageFatherController  implements VelocityPageInterface 
{
	static Logger log = Logger.getLogger("PAGE");
	
	public ArrayList<String> star = new ArrayList<String>();
	
	protected MemberMgr memberMgr = null;
	
	private ArrayList<DBRow> appendPara = new ArrayList<DBRow>();			//存放外部参数
	
	public String cp = "";
	
	/**
	 * 初始化一些公共类，方便子类使用
	 *
	 */
	public VelocityPageFather()
	{
	}
	
	/**
	 * 从外部给模板传入参数
	 * 主要用在一些提交的action，需要重定向到一些页面，并且需要传递参数
	 * @param name
	 * @param val
	 */
	public void putPara(String name,Object val)
	{
		DBRow v = new DBRow();
		v.add("name", name);
		v.add("val", val);
		appendPara.add(v);
	}
	
    /**
 	 * 初始化一些全局常量
	 * 所有页面均可以使用
     * @param request
     * @param response
     * @param context
     */
    private void initGlobeContext(HttpServletRequest request,  HttpServletResponse response,Context context)
    {
		context.put("request",request);	
		context.put("session",StringUtil.getSession(request));
		context.put("response",response);	
		context.put("StrUtil",new StringUtil());			//工具包
        context.put("systenFolder", ConfigBean.getStringValue("systenFolder") );	//系统路径
        context.put("upload_pro_img", ConfigBean.getStringValue("upload_pro_img") );     //图片文件夹
        context.put("systemConfig", new SystemConfig());							//系统类
        context.put("backurl", StringUtil.getCurrentURL(request));							//页面当前访问路径
        context.put("ConfigBean", new ConfigBean());								//系统配置类
        context.put("HtmlUtil",new HtmlUtil());  									//工具包
        context.put("Resource",new Resource());  									//国际化资源
    }
	
	/**
	 * 该方法由子类实现
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public abstract Template getTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception;

	/**
	 * 总控制SERVLET调用的方法，获得模板
	 * 该方法主要目的是在饭或模板前，做一些设局的参数设置
	 * @param request
	 * @param response
	 * @param ve
	 * @param context
	 * @param encodeing
	 * @return
	 * @throws MemberNotLoginException
	 * @throws PageNotFoundException
	 * @throws RedirectException
	 * @throws Exception
	 */
	public Template getFatherTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing)
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException ,RedirectException,Exception
	{
		//初始化下所有模板页面都需要用到的变量
		initGlobeContext(request,response,context);
		
//		//从spring容器加载类
//		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(Environment.getServletContext());
//		
//		//实例化3个模板页面
//		VelocityPageInterface headPageView = (VelocityPageInterface)ctx.getBean("headPage");
//		VelocityPageInterface footPageView = (VelocityPageInterface)ctx.getBean("footPage");
//
//		//获得商城头部页面HTML，并存到一个变量中
//		Template headTemplate = headPageView.getTemplate(request, response, ve, context, encodeing);		
//		StringWriter headSW = new StringWriter();
//		headTemplate.merge(context, headSW);
//		context.put("head",headSW.toString());
//
//		//获得商城尾部页面HTML，并存到一个变量中
//		Template footTemplate = footPageView.getTemplate(request, response, ve, context, encodeing);		
//		StringWriter footSW = new StringWriter();
//		footTemplate.merge(context, footSW);
//		
//		//追加外部参数
//		if (appendPara.size()>0)
//		{
//			for (int i=0; i<appendPara.size(); i++)
//			{
//				DBRow v = (DBRow)appendPara.get(i);
//				context.put(v.getString("name"),v.get("val", new Object()));
//			}
//		}
//		
//		context.put("foot",footSW.toString()+cp);
		
		return( getTemplate(request, response, ve, context, encodeing) );
	}

}




