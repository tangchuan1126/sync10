package com.cwc.app.page.core;

import org.apache.velocity.context.Context;

/**
 * 模板页面需要实现的接口
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface VelocityEmailPageStringInterface
{
	public String getTemplate(Context context,String encodeing)throws Exception ;

	public String getFatherTemplate(Context context,String encodeing) throws Exception ;
	
}
