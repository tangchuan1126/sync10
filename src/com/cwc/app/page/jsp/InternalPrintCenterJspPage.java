package com.cwc.app.page.jsp;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zyj.FloorRepairOrderMgrZyj;
import com.cwc.app.page.core.JspPageFather;
import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.order.print.*;

public class InternalPrintCenterJspPage extends JspPageFather
{
	private FloorDeliveryMgrZJ floorDeliveryMgrZJ;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorRepairOrderMgrZyj floorRepairOrderMgrZyj;
	
	
	private String SELECT_PRINT_PAGE = "select_express_internal_print.jsp";

	public String doGetJsp(HttpServletRequest request,HttpServletResponse response) 
		throws MemberNotLoginException,PageNotFoundException, OperationNotPermitException, Exception
	{
		String jspPage = "administrator/order/print/";
		
		long id = StringUtil.getLong(request, "id");
		
		float weight = StringUtil.getFloat(request,"weight");
		
		long ps_id = StringUtil.getLong(request,"ps_id");//detailOrder.get("ps_id", 0l);
		
		long send_ccid = StringUtil.getLong(request,"send_ccid");
		long send_pro_id = StringUtil.getLong(request,"send_pro_id");
		String send_name = StringUtil.getString(request,"send_name");
		String send_zip_code = StringUtil.getString(request,"send_zip_code");
		String send_house_number = StringUtil.getString(request,"send_house_number");
		String send_street = StringUtil.getString(request,"send_street");
		String send_address3 = StringUtil.getString(request,"send_address3");
		String send_linkman_phone = StringUtil.getString(request,"send_linkman_phone");
		String send_city = StringUtil.getString(request,"send_city");
		
		long deliver_ccid = StringUtil.getLong(request,"deliver_ccid");
		long deliver_pro_id = StringUtil.getLong(request,"deliver_pro_id");
		String deliver_name = StringUtil.getString(request,"deliver_name");
		String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
		String deliver_house_number = StringUtil.getString(request,"deliver_house_number");
		String deliver_street = StringUtil.getString(request,"deliver_street");
		String deliver_address3 = StringUtil.getString(request,"deliver_address3");
		String deliver_linkman_phone = StringUtil.getString(request,"deliver_linkman_phone");
		String deliver_city = StringUtil.getString(request,"deliver_city");
		
		String type = StringUtil.getString(request,"type");
		
		ArrayList<DBRow> productList = new ArrayList<DBRow>();//商品
		if(type.toUpperCase().equals("D"))
		{
			DBRow[] deliveryProducts = floorDeliveryMgrZJ.getDeliverOrderDetailsByDeliveryId(id);
			
			for (int i = 0; i < deliveryProducts.length; i++) 
			{
				DBRow cart = new DBRow();
				cart.add("cart_pid",deliveryProducts[i].get("product_id",0l));
				cart.add("cart_quantity",deliveryProducts[i].get("delivery_count",0f));
				
				productList.add(cart);
			}
		}
		else if(type.toUpperCase().equals("T"))
		{
			DBRow[] transportProducts = floorTransportMgrZJ.getTransportDetailByTransportId(id, null, null, null, null);
			
			for (int i = 0; i < transportProducts.length; i++) 
			{
				DBRow cart = new DBRow();
				cart.add("cart_pid",transportProducts[i].get("transport_pc_id",0l));
				cart.add("cart_quantity",transportProducts[i].get("transport_send_count",0f));
				
				productList.add(cart);
			}
		}
		else if(type.toUpperCase().equals("R"))
		{
			DBRow[] transportProducts = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(id, null, null, null, null);
			
			for (int i = 0; i < transportProducts.length; i++) 
			{
				DBRow cart = new DBRow();
				cart.add("cart_pid",transportProducts[i].get("transport_pc_id",0l));
				cart.add("cart_quantity",transportProducts[i].get("transport_send_count",0f));
				
				productList.add(cart);
			}
		}
				
		ExpressPolicyCore expressPolicy = (ExpressPolicyCore)MvcUtil.getBeanFromContainer("NotLimitWeightExpressPolicyImp");//catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("express_policy_class");
		expressPolicy.setCcid(deliver_ccid);
		expressPolicy.setProId(deliver_pro_id);
		expressPolicy.setPsId(ps_id);

		expressPolicy.setOid(id);
		expressPolicy.setWeight(weight);
			
		ExpressPrintBean expressPrintBean[] = expressPolicy.getExpressPrintPage();
		
		ArrayList<ExpressPrintBean> selectExpressPrintBean = new ArrayList<ExpressPrintBean>();
		ArrayList<ExpressPrintBean> noSelectExpressPrintBean = new ArrayList<ExpressPrintBean>();
		
		ExpressPrintBean tmpShippingInfoBean;
		for (int sorti=0; sorti<expressPrintBean.length; sorti++)
		{
			for (int sortj=sorti+1; sortj<expressPrintBean.length; sortj++)
			{
				if (expressPrintBean[sorti].getShippingFee()>expressPrintBean[sortj].getShippingFee())
				{
					tmpShippingInfoBean = expressPrintBean[sorti];
					expressPrintBean[sorti] = expressPrintBean[sortj];
					expressPrintBean[sortj] = tmpShippingInfoBean;
				}
			}
		}
		
		for (int i = 0; i < expressPrintBean.length; i++) 
		{
			if(expressPrintBean[i].isSelectAuto())
			{
				selectExpressPrintBean.add(expressPrintBean[i]);
			}
			else
			{
				noSelectExpressPrintBean.add(expressPrintBean[i]);
			}
		}

			
		jspPage += SELECT_PRINT_PAGE;
				
				//把可供选择的快递页面存到参数里面
		request.setAttribute("expressPrintBean",selectExpressPrintBean.toArray(new ExpressPrintBean[0]));
		request.setAttribute("noSelectExpressPrintBean",noSelectExpressPrintBean.toArray(new ExpressPrintBean[0]));
		
		request.setAttribute("ps_id",ps_id);
		request.setAttribute("id",id);
		request.setAttribute("type",type);
		request.setAttribute("ccid",deliver_ccid);
		request.setAttribute("pro_id",deliver_pro_id);
				
		request.setAttribute("send_name",send_name);
		request.setAttribute("send_zip_code",send_zip_code);
		request.setAttribute("send_address1",send_house_number);
		request.setAttribute("send_address2",send_street);
		request.setAttribute("send_address3",send_address3);
		request.setAttribute("send_linkman_phone",send_linkman_phone);
		request.setAttribute("send_city",send_city);
		
		request.setAttribute("deliver_name",deliver_name);
		request.setAttribute("deliver_zip_code",deliver_zip_code);
		request.setAttribute("deliver_address1",deliver_house_number);
		request.setAttribute("deliver_address2",deliver_street);
		request.setAttribute("deliver_address3",deliver_address3);
		request.setAttribute("deliver_linkman_phone",deliver_linkman_phone);
		request.setAttribute("deliver_city",deliver_city);
		request.setAttribute("weight",weight);
		
		int jspPos = jspPage.lastIndexOf(".jsp");
		return(jspPage.substring(0,jspPos));
	}


	public void setFloorDeliveryMgrZJ(FloorDeliveryMgrZJ floorDeliveryMgrZJ) {
		this.floorDeliveryMgrZJ = floorDeliveryMgrZJ;
	}


	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}


	public void setFloorRepairOrderMgrZyj(
			FloorRepairOrderMgrZyj floorRepairOrderMgrZyj) {
		this.floorRepairOrderMgrZyj = floorRepairOrderMgrZyj;
	}

}
