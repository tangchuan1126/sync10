package com.cwc.app.page.jsp;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.page.core.JspPageFather;
import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.order.print.*;
import com.cwc.app.api.CatalogMgr;
import com.cwc.app.api.zj.CartWaybill;

public class PrintCenterJspPage extends JspPageFather
{
	private FloorExpressMgr fem;
	private OrderMgr orderMgr;
	private CatalogMgr catalogMgr;
	private String SELECT_PRINT_PAGE = "select_express_print.jsp";

	public String doGetJsp(HttpServletRequest request,HttpServletResponse response) 
		throws MemberNotLoginException,PageNotFoundException, OperationNotPermitException, Exception
	{
		String jspPage = "administrator/order/print/";
		
		long oid = StringUtil.getLong(request, "oid");
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		long ps_id = StringUtil.getLong(request,"ps_id");//detailOrder.get("ps_id", 0l);
		long ccid = StringUtil.getLong(request,"ccid");//detailOrder.get("ccid", 0l);
		long pro_id = StringUtil.getLong(request,"pro_id");//detailOrder.get("pro_id", 0l);
		String islacking = StringUtil.getString(request,"islacking");
		
		CartWaybill cartWaybill = (CartWaybill)MvcUtil.getBeanFromContainer("cartWaybill");//普通商品				
		cartWaybill.flush(StringUtil.getSession(request));
		
		DBRow[] waybill = cartWaybill.getDetailProduct();
		float weight = cartWaybill.getCartWeight(StringUtil.getSession(request));//orderMgr.getOrderWeightByOid(oid);
			
//		if (detailOrder.get("recom_express", 0l)>0) //指定快递公司
//		{
//			DBRow detailExpress = fem.getDetailCompany(detailOrder.get("recom_express", 0l));
//			jspPage += detailExpress.getString("print_page");
//		}
//		else //没有指定，根据系统策略选择快递
//		{
			ExpressPolicyCore expressPolicy = (ExpressPolicyCore)MvcUtil.getBeanFromContainer("NotLimitWeightExpressPolicyImp");//catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("express_policy_class");
			expressPolicy.setCcid(ccid);
			expressPolicy.setProId(pro_id);
			expressPolicy.setPsId(ps_id);

			expressPolicy.setOid(oid);
			expressPolicy.setWeight(weight);
			
			ExpressPrintBean expressPrintBean[] = expressPolicy.getNewExpressPrintPage(waybill);
			
			ArrayList<ExpressPrintBean> selectExpressPrintBean = new ArrayList<ExpressPrintBean>();
			ArrayList<ExpressPrintBean> noSelectExpressPrintBean = new ArrayList<ExpressPrintBean>();
			
			ExpressPrintBean tmpShippingInfoBean;
			for (int sorti=0; sorti<expressPrintBean.length; sorti++)
			{
				for (int sortj=sorti+1; sortj<expressPrintBean.length; sortj++)
				{
					if (expressPrintBean[sorti].getShippingFee()>expressPrintBean[sortj].getShippingFee())
					{
						tmpShippingInfoBean = expressPrintBean[sorti];
						expressPrintBean[sorti] = expressPrintBean[sortj];
						expressPrintBean[sortj] = tmpShippingInfoBean;
					}
				}
			}
			
			for (int i = 0; i < expressPrintBean.length; i++) 
			{
				if(expressPrintBean[i].isSelectAuto())
				{
					selectExpressPrintBean.add(expressPrintBean[i]);
				}
				else
				{
					noSelectExpressPrintBean.add(expressPrintBean[i]);
				}
			}

			
			//返回快递打印页面。如果返回一个页面，则直接跳转到打印页面，如果返回多个，则跳转到选择打印页面
//			if (expressPrintBean.length==1)
//			{
//				jspPage += expressPrintBean[0].getPage();
//				request.setAttribute("sc_id", expressPrintBean[0].getScID());
//				request.setAttribute("ps_id",ps_id);
//			}
//			else
//			{
				jspPage += SELECT_PRINT_PAGE;
				
				//把可供选择的快递页面存到参数里面
				request.setAttribute("expressPrintBean",selectExpressPrintBean.toArray(new ExpressPrintBean[0]));
				request.setAttribute("noSelectExpressPrintBean",noSelectExpressPrintBean.toArray(new ExpressPrintBean[0]));
				request.setAttribute("ps_id",ps_id);
				request.setAttribute("islacking",islacking);
		
//			}
//		}
		
		int jspPos = jspPage.lastIndexOf(".jsp");
		return(jspPage.substring(0,jspPos));
	}

	
	public void setFem(FloorExpressMgr fem)
	{
		this.fem = fem;
	}

	public void setOrderMgr(OrderMgr orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public void setCatalogMgr(CatalogMgr catalogMgr)
	{
		this.catalogMgr = catalogMgr;
	}

}
