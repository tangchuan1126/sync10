package com.cwc.app.page.jsp;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.app.page.core.JspPageFather;
import com.cwc.db.DBRow;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.order.print.*;
import com.cwc.app.api.CatalogMgr;
import com.cwc.app.api.zj.CartWaybill;

public class ExpressPrintCenterJspPage extends JspPageFather
{
	 
	private WayBillMgrIfaceZR wayBillMgrZr;
	
	public void setWayBillMgrZr(WayBillMgrIfaceZR wayBillMgrZr) {
		this.wayBillMgrZr = wayBillMgrZr;
	}
	
	private String SELECT_PRINT_PAGE = "select_express_print_zhr.jsp";

	public String doGetJsp(HttpServletRequest request,HttpServletResponse response) 
		throws MemberNotLoginException,PageNotFoundException, OperationNotPermitException, Exception{
		String jspPage = "administrator/order/print/";
	 
		String ids = request.getParameter("ids");//waybill_order_item_id
		String numbers =  request.getParameter("numbers");//页面记录count集合与ids的顺序一致
		String wayBillId =  request.getParameter("way_bill_id");
		CartWaybill cartWaybill = (CartWaybill)MvcUtil.getBeanFromContainer("cartWaybill");//普通商品
		// 生成一个DBRow[] 
		DBRow[] rows = wayBillMgrZr.getWayOrderItem(ids, numbers);
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("wait_quantity",rows[i].get("cart_quantity",0f));
			list.add(rows[i]);
		}
		
		float weight = cartWaybill.getCartWeight(rows);
		DBRow wayBill = wayBillMgrZr.getWaybillInfo(Long.parseLong(wayBillId));
		ExpressPolicyCore expressPolicy = (ExpressPolicyCore)MvcUtil.getBeanFromContainer("NotLimitWeightExpressPolicyImp");//catalogMgr.getDetailProductStorageCatalogById(ps_id).getString("express_policy_class");
	
		expressPolicy.setCcid(wayBill.get("ccid", 0l));
		expressPolicy.setProId(wayBill.get("pro_id", 0l));
		expressPolicy.setPsId(wayBill.get("ps_id", 0l));
	 
		expressPolicy.setWeight(weight);
		
		ExpressPrintBean expressPrintBean[] = expressPolicy.getNewExpressPrintPage(rows);
		
		ArrayList<ExpressPrintBean> selectExpressPrintBean = new ArrayList<ExpressPrintBean>();
		ArrayList<ExpressPrintBean> noSelectExpressPrintBean = new ArrayList<ExpressPrintBean>();
		
		ExpressPrintBean tmpShippingInfoBean;
		for (int sorti=0; sorti<expressPrintBean.length; sorti++)
		{
			for (int sortj=sorti+1; sortj<expressPrintBean.length; sortj++)
			{
				if (expressPrintBean[sorti].getShippingFee()>expressPrintBean[sortj].getShippingFee())
				{
					tmpShippingInfoBean = expressPrintBean[sorti];
					expressPrintBean[sorti] = expressPrintBean[sortj];
					expressPrintBean[sortj] = tmpShippingInfoBean;
				}
			}
		}
		
		for (int i = 0; i < expressPrintBean.length; i++) 
		{
			if(expressPrintBean[i].isSelectAuto())
			{
				selectExpressPrintBean.add(expressPrintBean[i]);
			}
			else
			{
				noSelectExpressPrintBean.add(expressPrintBean[i]);
			}
		}
		
		request.setAttribute("expressPrintBean",selectExpressPrintBean.toArray(new ExpressPrintBean[0]));
		request.setAttribute("noSelectExpressPrintBean",noSelectExpressPrintBean.toArray(new ExpressPrintBean[0]));
		
		
		
		
		
		request.setAttribute("ps_id",wayBill.get("ps_id", 0l));
		request.setAttribute("wayBillOrder", wayBill);
		request.setAttribute("weight", weight);
		request.setAttribute("detailproduct",list);
		request.setAttribute("waybillid", wayBillId);
		request.setAttribute("ids", ids);
		request.setAttribute("numbers", numbers);
		jspPage += SELECT_PRINT_PAGE;
		
		int jspPos = jspPage.lastIndexOf(".jsp");
		return(jspPage.substring(0,jspPos));
	}
}
