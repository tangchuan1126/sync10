package com.cwc.app.page.velocity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.app.page.core.VelocityPageFather;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;

/**
 * 系统错误页面
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SystemInternalError extends VelocityPageFather
{

	public Template getTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing) 
		throws MemberNotLoginException, PageNotFoundException, OperationNotPermitException,RedirectException,Exception 
	{
		if (context.get("errorMsg")==null)
		{
			context.put("errorMsg","");
		}
		
		Template t = ve.getTemplate( "internal_error.html",encodeing);
		
		return(t);
	}

}
