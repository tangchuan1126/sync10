package com.cwc.app.page.velocity;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.page.core.VelocityPageFather;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.MemberNotLoginException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectException;
import com.cwc.util.StringUtil;

/**
 * 404错误页面
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SystemPageNotFound extends VelocityPageFather 
{
	public Template getTemplate(HttpServletRequest request,HttpServletResponse response,VelocityEngine ve,Context context,String encodeing) 
		throws MemberNotLoginException, PageNotFoundException,OperationNotPermitException,RedirectException, Exception 
	{	
		HttpSession session = request.getSession();
		boolean reLogin = false;
		if ( session==  null || session.getAttribute(Config.adminSesion) == null ){
			reLogin = true;
		}
		else{
			HashMap map =(HashMap)session.getAttribute(Config.adminSesion);
			if(map.get("account")==null || "".equals(map.get("account"))){
				reLogin = true;
			}
		}
		
		if ( reLogin )
		{
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setIsLoginRightPath();
			StringUtil.getSession(request).setAttribute(Config.adminSesion,adminLoggerBean);	
//			//system.out.println(ConfigBean.getStringValue("systenFolder")+config.getInitParameter("adminLoginFalse"));
			response.sendRedirect("/Sync10/login");
			return null;//防止往下继续执行
		}
		
		Template t = ve.getTemplate("404.html",encodeing);
		
		return(t);
	}

}
