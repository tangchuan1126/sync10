package com.cwc.app.outface.core;

/**
 * 重定向异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SendRedirectException extends Exception 
{
	public SendRedirectException() 
	{
		super();
	}
	
	public SendRedirectException(String inMessage)
	{
		super(inMessage);
	}
}
