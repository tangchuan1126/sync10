package com.cwc.app.outface.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 对外接口实现类需要实现的接口
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface ActionIFace 
{
	//需要实现的接口
	public String perform(HttpServletRequest request,HttpServletResponse response) throws SendRedirectException,ParameterException,Exception;
}
