package com.cwc.app.outface.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.factory.Factory;
import com.cwc.util.StringUtil;

/**
 * 控制中心
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Center extends HttpServlet
{
	static Logger log = Logger.getLogger("ACTION");
	private boolean codePostFlag = false;
	
    public void doGet( HttpServletRequest request, HttpServletResponse response )
	    throws ServletException, IOException
	{
    	String face = request.getPathInfo();   									//获得请求接口 	
		Factory factory = Factory.getInstance();
		String errorMsg = "Parameter error! Please contact www.turboshop.cn.";	//默认错误信息

		try
		{
			if (face==null)
			{
				writeOut(response,errorMsg);
				return;
			}
			
			//判断是否由浏览器提交的请求
			if ( request.getHeader("user-agent").toLowerCase().indexOf("mozilla")==-1 )
			{
				codePostFlag = true;
			}
			else
			{
				codePostFlag = false;
			}
			
			if (face.indexOf("/")==0)
			{
				face = face.substring(1);
			}
			
			StringBuffer outSB = new StringBuffer("");
			outSB.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
			
			//根据命令加载相关类并执行
			ActionIFace action = (ActionIFace) factory.getInstanceNonProxy("com.cwc.app.outface.action.".concat(face));
			outSB.append( action.perform(request,response) );
			writeOut(response,outSB.toString());
		}
		catch (SendRedirectException e)
		{
			//通过异常来控制流程跳转
			response.sendRedirect(e.getMessage());
		}
		catch (ParameterException e)
		{
			writeOut(response,errorMsg);
		}
		catch (Exception e) 
		{
			if (log.isDebugEnabled())
			{
				log.error("Center error:"+e);				
			}
			writeOut(response,errorMsg);
		}
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
	    throws ServletException, IOException
	{
		doGet( request, response );
	}
	
	/**
	 * 网页面写返回数据XML
	 * @param response
	 * @param msg
	 * @throws IOException
	 */
	private void writeOut(HttpServletResponse response,String msg)
		throws IOException
	{
		response.setHeader("Content-Type", "text/xml;charset=utf-8");
		ServletOutputStream webOutput = response.getOutputStream();
		if (codePostFlag)
		{
			webOutput.print(msg);
		}
		else
		{
			webOutput.print(StringUtil.UTF82ISO(msg));
		}
		webOutput.flush();
		webOutput.close();
	}

	public static void main(String args[])
	{

	}
}
