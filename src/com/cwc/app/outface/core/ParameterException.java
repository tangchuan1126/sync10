package com.cwc.app.outface.core;

/**
 * 参数错误自定义异常
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ParameterException extends Exception 
{
	public ParameterException() 
	{
		super();
	}
	
	public ParameterException(String inMessage)
	{
		super(inMessage);
	}
}
