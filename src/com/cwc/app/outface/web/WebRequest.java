package com.cwc.app.outface.web;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

/**
 * 模拟浏览器
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class WebRequest 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private String requestURI = null;
	private String server = null;
	
	public static String HTTP_PROTOCOL = "http";
	public static String HTTPS_PROTOCOL = "https";
	int port = 80;
	
	private String protocol = HTTP_PROTOCOL;
	
	
	private TextPara textPara = null;
	private int serverStatus = 0;
	private String serverResponse = null;
	
	public HttpMethod outMethod = null;
	
	public void setProtocol(String protocol)
	{
		this.protocol = protocol;
	}
	
	public void setRequestURI(String requestURI)
	{
		this.requestURI = requestURI;
	}
	
	public void setServer(String server)
	{
		this.server = server;
	}
	
	public void setPort(int port)
	{
		this.port = port;
	}
	
	public void setPara(TextPara textPara)
	{
		this.textPara = textPara;
	}
	
	/**
	 * 得到GET方式
	 * @return
	 */
	private HttpMethod getGetMethod()
	{
		if (textPara==null)
		{
			return new GetMethod(requestURI);
		}
		else
		{
			return new GetMethod(requestURI+textPara.getGETPara());
		}
	}

	/**
	 * 得到POST方式
	 * @return
	 */
	private HttpMethod getPostMethod()
    {
		PostMethod post = new MyPostMethod(requestURI);
		////system.out.println("------------->2echo println:"+post);
		post.setRequestBody(textPara.getPOSTPara());
		return(post);
    }

	/**
	 * 用GET方式提交请求
	 * @throws Exception
	 */
	public void commitGET()
		throws Exception
	{
		HttpMethod method = null;
		
		try
		{
			HttpClient client = new HttpClient();
			client.getHostConfiguration().setHost(server,port,protocol);
			method = getGetMethod();
			client.executeMethod(method);
			
			serverStatus = method.getStatusLine().getStatusCode();		//获得返回代码
			serverResponse = method.getResponseBodyAsString();			//获得返回文本
			
			outMethod = method;
		}
		catch (HttpException e) 
		{
			log.error("commitGET HttpException error:"+e);
			throw new Exception("commitGET HttpException error:"+e);
		}
		catch (IOException e)
		{
			log.error("commitGET IOException error:"+e);
			throw new Exception("commitGET IOException error:"+e);
		}
		finally
		{
			method.releaseConnection();	
		}
	}
	
	/**
	 * 用POST方式提交请求
	 * @throws Exception
	 */
	public void commitPOST()
		throws Exception
	{
		HttpMethod method = null;
		
		try
		{
			HttpClient client = new HttpClient();
			client.getHostConfiguration().setHost(server,port,protocol);
			////system.out.println("------------->1echo println:"+client);
			method = getPostMethod();
			client.executeMethod(method);
			
			serverStatus = method.getStatusLine().getStatusCode();		//或得返回代码
			serverResponse = method.getResponseBodyAsString();			//获得返回文本
			
			/*
			InputStreamReader read = new InputStreamReader (method.getResponseBodyAsStream(),"UTF-8");
			BufferedReader sb = new BufferedReader(read);
			String tmpStr;
			while ( (tmpStr=sb.readLine())!=null )
			{
				if (!tmpStr.trim().equals(""))
				{
					serverResponse += tmpStr;
				}
			}
			*/
			
			outMethod = method;
		}
		catch (HttpException e) 
		{
			////system.out.println("commitPOST HttpException error:"+e);
			throw new Exception("commitGET HttpException error:"+e);
		}
		catch (IOException e)
		{
			////system.out.println("commitPOST IOException error:"+e);
			throw new Exception("commitGET IOException error:"+e);
		}
		finally
		{
			if (method!=null)
			{
				method.releaseConnection();	
			}
		}
	}
	
	public int getServerStatus()
	{
		return(serverStatus);
	}
	
	public String getServerResponse()
	{
		return(serverResponse);
	}
	
	public static void main(String args[]) throws Exception
	{

    }   


}
