package com.cwc.app.outface.web;

/**
 * 测试类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Test
{
	public static void main(String args[])
	{
		try
		{

			WebRequest wr = new WebRequest();
			
			TextPara tp = new TextPara();
			
			tp.putPara("groupName", "精品推荐");
			tp.putPara("pSize", "100");
			tp.putPara("p", "1");
			wr.setPort(80);
			wr.setPara(tp);
			wr.setProtocol(WebRequest.HTTP_PROTOCOL);
			wr.setRequestURI("/turboshop/OutFaceCenter/GetProductsByGroupName");
			wr.setServer("echo");
			wr.commitPOST();
		
			////system.out.println("status:"+wr.getServerStatus());
			
			if (wr.getServerStatus() == 200) 
			{
				
				////system.out.println(wr.getServerResponse());
			}
			
		}
		catch (Exception e) 
		{
			////system.out.println("error:"+e);
		}		
	}
}
