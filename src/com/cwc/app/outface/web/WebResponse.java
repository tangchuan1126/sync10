package com.cwc.app.outface.web;


public class WebResponse 
{
	private String html = null;
	
	public String getHtml() 
	{
		return html;
	}

	public void setHtml(String html) 
	{
		this.html = html;
	}
}
