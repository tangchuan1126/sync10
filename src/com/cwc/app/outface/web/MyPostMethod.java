package com.cwc.app.outface.web;

import org.apache.commons.httpclient.methods.PostMethod;

/**
 * 自定义POST
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MyPostMethod extends PostMethod
{
	private String encode = "UTF-8";
	
	public MyPostMethod(String url)
	{
        super(url);
    }

    public String getRequestCharSet() 
    {
        return(encode);
    }
    
    public void setCode(String encode)
    {
    	this.encode = encode;
    }
}
