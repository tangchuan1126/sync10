package com.cwc.app.ordertrace;

import java.util.ArrayList;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.key.AfterServiceKey;


/**
 * 疑问支付订单任务类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class OtherDisputeOrderTask extends OrderTraceFather
{
	private SystemConfig systemConfig ;
	
	protected boolean isValidate(int handle,int after_service_status, int handle_status,	int product_status)
	{
		if ( after_service_status==AfterServiceKey.OTHER_DISPUTING )
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}

	protected int getHandlePeriod() throws Exception
	{
		return (systemConfig.getIntConfigValue("other_dispute_period"));
	}

	protected ArrayList<Long> getOperatorRoleIDs()
	{
		ArrayList<Long> operatorRoleIDs = new ArrayList<Long>();
		operatorRoleIDs.add(100006l);//客服
		
		return(operatorRoleIDs);
	}
	
	
	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
	
}
