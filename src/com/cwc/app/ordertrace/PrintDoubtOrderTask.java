package com.cwc.app.ordertrace;

import java.util.ArrayList;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.key.HandStatusleKey;


/**
 * 疑问订单任务类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PrintDoubtOrderTask extends OrderTraceFather
{
	private SystemConfig systemConfig ;
	
	protected boolean isValidate(int handle,int after_service_status, int handle_status,	int product_status)
	{
		if ( handle_status==HandStatusleKey.PRINT_DOUBT )
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}

	protected int getHandlePeriod() throws Exception
	{
		return (systemConfig.getIntConfigValue("print_doubt_handle_period"));
	}

	protected ArrayList<Long> getOperatorRoleIDs()
	{
		ArrayList<Long> operatorRoleIDs = new ArrayList<Long>();
		operatorRoleIDs.add(100006l);//客服
		
		return(operatorRoleIDs);
	}
	
	
	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
	
}
