package com.cwc.app.ordertrace;

import java.util.ArrayList;

import com.cwc.app.util.TDate;
import com.cwc.util.StringUtil;

public abstract class OrderTraceFather
{
	private long nextOperatorRoleID;
	private String nextTraceDate;
	
	public long getNextOperatorRoleID() 
		throws Exception
	{
		return(nextOperatorRoleID);
	}

	public String getNextTraceDate()
		throws Exception
	{
		return(nextTraceDate);
	}
	
	/**
	 * 根据订单状态判断是否需要创建任务，如果任务已经被创建，那么任务状态会被更新
	 * 输出记过计算后的任务状态
	 * @param handle
	 * @param handle_status
	 * @param product_status
	 * @param trace_flag
	 * @param trace_operator_role_id
	 * @param pre_operator_role_id
	 * @param current_operator_role_id
	 * @return
	 * @throws Exception
	 */
	public boolean need2CreateTask(int handle,int after_service_status,int handle_status,int product_status,String oldTraceTaskName,int trace_flag,long trace_operator_role_id,long pre_operator_role_id,long current_operator_role_id) 
		throws Exception
	{
		if ( this.isValidate(handle,after_service_status,handle_status,product_status) )
		{
			//该任务需要协作参与角色，按先后顺序
			ArrayList<Long> operatorRoleIDs = this.getOperatorRoleIDs();		
			
			//计算任务下次执行日期
			int handle_period = this.getHandlePeriod();
			TDate tDate = new TDate(); 
			tDate.addDay(handle_period);
			
			if (trace_flag==1&&oldTraceTaskName.equals(this.getClass().getSimpleName()))//同一个任务，已经被创建，则更新任务状态
			{
				if ( operatorRoleIDs.size()>1 )
				{
					if ( pre_operator_role_id==0 )  //任务第一次被执行
					{
						//执行角色必须为任务制定第一个角色
						if (trace_operator_role_id==current_operator_role_id)
						{
							//所有角色没有参与完任务前，任务执行日期不更新
							//第一个角色执行完毕，直接引导到第二个角色
							nextTraceDate = null;
							nextOperatorRoleID =  StringUtil.getLong(operatorRoleIDs.get(1).toString()) ;
						}
						else
						{
							nextTraceDate = null;		//不更新任务日期
							nextOperatorRoleID = -1;	//不更新任务下一个执行角色
						}
					}
					else
					{
						//检测当前角色属于计划角色中的哪个
						int i=0;
						for (; i<operatorRoleIDs.size(); i++)
						{
							/**
							 * 1、当前执行角色=任务计划执行角色
							 * 2、当前执行角色属于任务计划执行角色
							 * 满足以上条件，才能把任务交给下一个角色处理
							 */
							if (trace_operator_role_id==current_operator_role_id&&StringUtil.getLong(operatorRoleIDs.get(i).toString())==current_operator_role_id)
							{
								break;
							}
						}

						if ( i==operatorRoleIDs.size() )	//添加跟踪备注的角色，不在计划角色内
						{
							nextTraceDate = null;		//不更新任务日期
							nextOperatorRoleID = -1;	//不更新任务下一个执行角色
						}
						else if (i==operatorRoleIDs.size()-1)//最后一个角色执行完，下一次任务时间从头再来
						{
							nextTraceDate = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
							nextOperatorRoleID =  StringUtil.getLong(operatorRoleIDs.get(0).toString());
						}
						else
						{
							//所有角色没有参与完任务前，任务执行日期不更新
							nextTraceDate = null;
							nextOperatorRoleID =  StringUtil.getLong(operatorRoleIDs.get(i+1).toString()) ;
						}
					}
				}
				else//常规单角色参与任务
				{
					if (trace_operator_role_id==current_operator_role_id)
					{
						nextTraceDate = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
						nextOperatorRoleID =  StringUtil.getLong(operatorRoleIDs.get(0).toString());
					}
					else
					{
						nextTraceDate = null;		//不更新任务日期
						nextOperatorRoleID = -1;	//不更新任务下一个执行角色
					}
				}
			}
			else//创建新任务
			{ 
				nextTraceDate = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
				nextOperatorRoleID =  StringUtil.getLong(operatorRoleIDs.get(0).toString()) ;
			}
			
			return(true);
		}
		else
		{
			return(false);
		}
	}
	
	/**
	 * 必须有子类实现任务判断条件
	 * @param handle
	 * @param handle_status
	 * @param product_status
	 * @return
	 */
	protected abstract boolean isValidate(int handle,int after_service_status,int handle_status,int product_status);

	/**
	 * 子类实现，设置任务需要参与的角色
	 * @return
	 */
	protected abstract ArrayList<Long> getOperatorRoleIDs();
	
	/**
	 * 子类实现，设置任务处理周期
	 * @return
	 * @throws Exception 
	 */
	protected abstract int getHandlePeriod() throws Exception;
	
	
}
