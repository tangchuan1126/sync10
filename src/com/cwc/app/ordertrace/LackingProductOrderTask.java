package com.cwc.app.ordertrace;

import java.util.ArrayList;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.key.ProductStatusKey;


/**
 * 缺货任务类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class LackingProductOrderTask extends OrderTraceFather
{
	private SystemConfig systemConfig ;
	
	protected boolean isValidate(int handle,int after_service_status, int handle_status,int product_status)
	{
		if ( product_status==ProductStatusKey.STORE_OUT )
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}

	protected int getHandlePeriod() 
		throws Exception
	{
		return (systemConfig.getIntConfigValue("lacking_order_handle_period"));
	}

	protected ArrayList<Long> getOperatorRoleIDs()
	{
		ArrayList<Long> operatorRoleIDs = new ArrayList<Long>();
		//operatorRoleIDs.add(100015l);//调度
		operatorRoleIDs.add(100006l);//客服
		
		return(operatorRoleIDs);
	}

	
	
	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
}
