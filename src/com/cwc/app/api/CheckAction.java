package com.cwc.app.api;

import javax.xml.bind.annotation.XmlEnumValue;

public enum CheckAction {
	
	@XmlEnumValue("Gate Checkin")
    GATE_CHECKIN,

    @XmlEnumValue("Window Entry Create")
    WINDOW_ENTRY_CREATE,

    @XmlEnumValue("Window Checkin")
    WINDOW_CHECKIN,

    @XmlEnumValue("Dock Checkin")
    DOCK_CHECKIN,

    @XmlEnumValue("Dock Release")
    DOCK_RELEASE,

    @XmlEnumValue("Gate Checkout")
    GATE_CHECKOUT
}
