package com.cwc.app.api.auth;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.util.StringUtil;

public class PermitMgr {
	
	private FloorAdminMgr floorAdminMgr;
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 功能：获取角色授权的Uri
	 * @param adgid 
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getRoleAuthUri(long adgid) throws Exception {
		HashMap<String, DBRow> roleAuthUriMap = new HashMap<String, DBRow>();
		
		/*DBRow dept = new DBRow();
		dept.add("adgid", deptid);
		
		DBRow[] roleAuthUri = floorAdminMgr.findAdminAthAction(new DBRow[]{dept}, 0L, 0L, null);*/
		
		DBRow[] roleAuthUri = findRoleAuthResource(adgid);
		
		if(roleAuthUri != null){
			for(DBRow authUri : roleAuthUri){
				if(!StringUtil.isBlank(authUri.getString("action_uri"))){
					roleAuthUriMap.put(authUri.getString("action_uri"), authUri);
				}
			}
		}
		
		return roleAuthUriMap;
	}
	
	/**
	 * 功能：获取指定用户拥有角色的授权Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getUserRolesAuthUri(long adid) throws Exception {
		HashMap<String, DBRow> userRolesAuthUriMap = new HashMap<String, DBRow>();
		
		/*DBRow[] userRoles = floorAdminMgr.findAdminRoleByAdid(adid);
		if(userRoles==null || userRoles.length<1){
			return userRolesAuthUriMap;
		}
		
		DBRow[] userRolesAuthUri = floorAdminMgr.findAdminAthAction(userRoles, 0L, 0L, null);*/
		
		DBRow[] userRolesAuthUri = findUserRolesAuthResource(adid);
		
		if(userRolesAuthUri != null){
			for(DBRow authUri : userRolesAuthUri){
				if(!StringUtil.isBlank(authUri.getString("action_uri"))){
					userRolesAuthUriMap.put(authUri.getString("action_uri"), authUri);
				}
			}
		}
		
		return userRolesAuthUriMap;
	}
	
	/**
	 * 功能：获取用户授权的Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getUserAuthUri(long adid) throws Exception {
		HashMap<String, DBRow> userAuthUriMap = new HashMap<String, DBRow>();
		
		/*DBRow[] userRoles = floorAdminMgr.findAdminRoleByAdid(adid);
		if(userRoles==null || userRoles.length<1){
			return userAuthUriMap;
		}
		
		DBRow[] userAuthUri = floorAdminMgr.findAdminAthAction(userRoles, adid, 0L, null);*/
		
		DBRow[] userAuthUri = findUserAuthResource(adid);
		
		if(userAuthUri != null){
			for(DBRow authUri : userAuthUri){
				if(!StringUtil.isBlank(authUri.getString("action_uri"))){
					userAuthUriMap.put(authUri.getString("action_uri"), authUri);
				}
			}
		}
		
		return userAuthUriMap;
	}
	
	/**
	 * 功能：根据adid获取用户扩展的Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getUserExtendAuthUri(long adid) throws Exception {
		HashMap<String,DBRow> userExtendAuthUriMap = new HashMap<String, DBRow>();
		
		//DBRow[] userExtendAuthUri = floorAdminMgr.findAdminAthAction(new DBRow[0], adid, 0L, null);
		
		DBRow[] userExtendAuthUri = findUserAuthResource(adid);
		
		if(userExtendAuthUri != null){
			for(DBRow authUri : userExtendAuthUri){
				if(!StringUtil.isBlank(authUri.getString("action_uri"))){
					userExtendAuthUriMap.put(authUri.getString("action_uri"), authUri);
				}
			}
		}
		
		return userExtendAuthUriMap;
	}
	
	/**
	 * 功能：获取角色的授权资源
	 * @param adgid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow[] findRoleAuthResource(long adgid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append(" WHERE aga.adgid = "+ adgid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户拥有角色的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow[] findUserRolesAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append("	LEFT JOIN admin_department ad ON ad.department_id = aga.adgid ")
				 .append(" WHERE ad.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow[] findUserAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();

		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append("	LEFT JOIN admin_department ad ON ad.department_id = aga.adgid ")
				 .append(" WHERE ad.adid = "+ adid).append(" ")
				 .append("UNION ")
				 .append("SELECT aa.* ")
				 .append("	FROM turboshop_admin_group_auth_extend agae ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = agae.ataid ")
				 .append(" WHERE agae.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户扩展的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow[] findUserExtendsAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM turboshop_admin_group_auth_extend agae ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = agae.ataid ")
				 .append(" WHERE agae.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取所有需要控制权限的资源
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow[] findAllCtrlPermResource() throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM authentication_action aa ");
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取界面权限数据
	 * @param model
	 * @param adid
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月20日
	 * @author  lixf
	 */
	public DBRow getUIPermsInfo(String model, long adid) throws Exception{
		DBRow perm = new DBRow();
		
		DBRow[] ctrlPerms = findAllCtrlPermResource();
		
		List<DBRow> ctrlPermsList = new LinkedList<DBRow>();
		
		for(DBRow ctrlPerm : ctrlPerms){
			String uri = ctrlPerm.getString("action_uri");
			String method = ctrlPerm.getString("request_method");
			
			if(!StringUtil.isBlank(uri)){
				DBRow tmpPerm = new DBRow();
				
				tmpPerm.add("uri", uri);
				tmpPerm.add("method", method);
				
				ctrlPermsList.add(tmpPerm);
			}
		}
		
		Map<String,Object> ctrlPerm = new HashMap<String, Object>();
		ctrlPerm.put("size", ctrlPermsList.size());
		ctrlPerm.put("perms", ctrlPermsList);
		
		DBRow[] userPerms = findUserRolesAuthResource(adid);
		
		List<DBRow> userPermsList = new LinkedList<DBRow>();
		
		if(userPerms != null){
			for(DBRow userPerm : userPerms){
				String uri = userPerm.getString("action_uri");
				String method = userPerm.getString("request_method");
				
				if(!StringUtil.isBlank(uri)){
					DBRow tmpPerm = new DBRow();
					
					tmpPerm.add("uri", uri);
					tmpPerm.add("method", method);
					
					userPermsList.add(tmpPerm);
				}
			}
		}
		
		Map<String,Object> userPerm = new HashMap<String, Object>();
		userPerm.put("size", userPermsList.size());
		userPerm.put("perms", userPermsList);	
		
		perm.add("ctrlPerm", ctrlPerm);
		perm.add("userPerm", userPerm);
		
		return perm;
	}

	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
