package com.cwc.app.api;

public enum CheckinActionType {

	DELIVERY(1),

	PICK_UP(2),

	BOTH(3),

	NONE(4),

	CTNR(5),

	PATROL(6),

	VISITOR(7),

	SMALL_PARCEL(8);

	private int relType;

	private CheckinActionType(int relType) {
		this.relType = relType;
	}
	
	public int getRelType(){
		return relType;
	}
}
