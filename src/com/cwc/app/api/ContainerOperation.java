package com.cwc.app.api;

import javax.xml.bind.annotation.XmlEnumValue;

public enum ContainerOperation {
		@XmlEnumValue("Drop Off")
	    DROP_OFF,

	    @XmlEnumValue("Swap Container")
	    SWAP_CONTAINER,

	    @XmlEnumValue("Unknown")
	    UNKNOWN,

	    @XmlEnumValue("Live Load")
	    LIVE_LOAD
}
