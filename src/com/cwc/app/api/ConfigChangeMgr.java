package com.cwc.app.api;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;












import com.cwc.app.api.zj.LPTypeMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuConfigChangeBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorConfigChangeMgrZJ;
import com.cwc.app.iface.zj.ConfigChangeMgrIFaceZJ;
import com.cwc.app.key.ChangeStatusKey;
import com.cwc.app.key.ConfigChangeStoreStatusKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ConfigChangeMgr implements ConfigChangeMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorConfigChangeMgrZJ floorConfigChangeMgrZJ;
	private FloorProductMgr floorProductMgr;
	private LPTypeMgrZJ lpTypeMgrZJ;
	private ProductStoreMgr productStoreMgrZJ;
	
	/**
	 * 添加配置更改
	 */
	public long addConfigChange(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long ps_id = StringUtil.getLong(request,"ps_id");
			long title_id = StringUtil.getLong(request,"title_id");
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			String filename = StringUtil.getString(request,"file_name"); 
				
			DBRow configChange = new DBRow();
			configChange.add("cc_psid",ps_id);
			configChange.add("cc_create_adid",adminLoggerBean.getAdid());
			configChange.add("cc_create_time",DateUtil.NowStr());
			configChange.add("cc_status",ChangeStatusKey.Readying);
			configChange.add("cc_title",title_id);
			
			long cc_id = floorConfigChangeMgrZJ.addConfigChange(configChange);
			
			if (!filename.equals(""))
			{
				saveUploadConfigChangeItemSubmit(cc_id, filename);
			}
			
			return cc_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addConfigChange",log);
		}
	}
	
	/**
	 * 获得配置更改列表
	 */
	public DBRow[] getSearchConfigChange(long ps_id,long title_id,int cc_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorConfigChangeMgrZJ.getSearchConfigChange(ps_id, title_id, cc_status, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchConfigChange",log);
		}
	}
	
	/**
	 * 根据cc_id获得配置更改明细
	 */
	public DBRow[] getConfigChangeItemByCcid(long cc_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			return floorConfigChangeMgrZJ.getConfigChangeItemByCcid(cc_id, pc, sidx, sord, fillterBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getConfigChangeItemByCcid",log);
		}
	}
	
	/**
	 * 获得配置详细
	 */
	public DBRow getDetailConfigChangeByCcid(long cc_id)
		throws Exception
	{
		try 
		{
			return floorConfigChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailConfigChangeByCcid",log);
		}
	}

	/**
	 * jqgrid添加配置更改明细
	 */
	public long addConfigChangeItemJqgrid(HttpServletRequest request)
		throws Exception 
	{
		try
		{
			long cci_cc_id = StringUtil.getLong(request,"cci_cc_id");
			int cci_type = StringUtil.getInt(request,"cci_type");
			long cci_type_id = StringUtil.getLong(request,"cci_type_id");
			int cci_count = StringUtil.getInt(request,"cci_count");
			String p_name = StringUtil.getString(request,"p_name");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			if (product == null)
			{
				throw new ProductNotExistException();
			}
			
			long pc_id = product.get("pc_id",0l);
			
			DBRow configChangeItem = new DBRow();
			configChangeItem.add("cci_cc_id",cci_cc_id);
			configChangeItem.add("cci_type",cci_type);
			configChangeItem.add("cci_type_id",cci_type_id);
			configChangeItem.add("cci_count",cci_count);
			configChangeItem.add("cci_pc_id",pc_id);
			
			return floorConfigChangeMgrZJ.addConfigChangeItem(configChangeItem);
		} 
		catch (ProductNotExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addConfigChangeItemJqgrid",log);
		}
	}

	/**
	 * jqgrid删除配置更改明细
	 */
	public void delConfigChangeItemJqgrid(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long cci_id = StringUtil.getLong(request,"id");
			floorConfigChangeMgrZJ.delConfigChangeItem(cci_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delConfigChangeItemJqgrid",log);
		}
	}
	
	/**
	 * 预计算ConfigChange的库存
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public PreCalcuConfigChangeBean preCalcuConfigChange(long cc_id)
		throws Exception
	{
		try 
		{
			DBRow blpCount = new DBRow();
			DBRow ilpCount = new DBRow();
			DBRow orignailCount = new DBRow();
			
			DBRow configChange = floorConfigChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
			long ps_id = configChange.get("cc_psid",0l);
			long title_id = configChange.get("cc_title",0l);
			
			DBRow[] configChangeDetails = floorConfigChangeMgrZJ.getConfigChangeItemByCcid(cc_id, null, null, null, null);
			
			ArrayList<DBRow> resultList = new ArrayList<DBRow>();
			int configChangeStatus = ConfigChangeStoreStatusKey.Enough;
			for (int i = 0; i < configChangeDetails.length; i++)
			{
				int itemStatus = ConfigChangeStoreStatusKey.Enough;
				
				DBRow configChangeDetail = configChangeDetails[i];
				String lot_number = configChangeDetail.getString("cci_lot_number");
				
				long pc_id = configChangeDetail.get("cci_pc_id",0l);
				int lp_type = configChangeDetail.get("cci_type",0);
				long type_id = configChangeDetail.get("cci_type_id",0l);
				int cci_count = configChangeDetail.get("cci_count",0);
				int store_count = 0;	//需要类型的库存数量
				int inner_count = 0;	//内部装载数量
				int inner_need_count = 0;		//单种需要数量，需要的容器（商品）数等于许拼装数X内置数
				long inner_type_id = 0;	//内装lp_type_id
				int inner_type = ContainerTypeKey.Original;		//内装类型，只为预算后页面演示
				//获得容器的库存
				if (lp_type == ContainerTypeKey.CLP)
				{
					DBRow clp = lpTypeMgrZJ.getDetailCLPType(type_id);
					inner_count = clp.get("sku_lp_total_box",0);
					inner_type_id = clp.get("sku_lp_box_type",0);
					inner_need_count = inner_count*cci_count;
					
					if (inner_type_id==ContainerTypeKey.Original)
					{
//						store_count = productStoreMgrZJ.getTLPProductAvailableCount(ps_id, pc_id, title_id, lot_number);
						store_count = productStoreMgrZJ.getContainerStoreAvailableCount(ps_id, pc_id, title_id, ContainerTypeKey.Original,0, lot_number);
						this.addSumProductCount(orignailCount, pc_id, inner_need_count);
					}
					else
					{
						store_count = 0;//productStoreMgrZJ.getContainerStoreAvailableCount(ps_id, pc_id, title_id, ContainerTypeKey.BLP,0, lot_number);//去掉ILP和BLP
						inner_type = 0;//ContainerTypeKey.BLP;//去掉ILP和BLP
						
						this.addSumLPCount(blpCount,inner_type_id,inner_need_count);
					}
				}
				/*
				else if(lp_type == ContainerTypeKey.BLP)
				{
					DBRow blp = lpTypeMgrZJ.getDetailBLPType(type_id);
					inner_count = blp.get("box_total",0);
					inner_type_id = blp.get("box_inner_type",0);
					inner_need_count = inner_count*cci_count;
					
					if (inner_type_id==ContainerTypeKey.Original)
					{
						store_count = productStoreMgrZJ.getContainerStoreAvailableCount(ps_id, pc_id, title_id,ContainerTypeKey.Original,0, lot_number);

						this.addSumProductCount(orignailCount, pc_id,inner_need_count);
					}
					else
					{
						store_count = productStoreMgrZJ.getContainerStoreAvailableCount(ps_id, pc_id, title_id,ContainerTypeKey.ILP,inner_type_id, lot_number);
						inner_type = ContainerTypeKey.ILP;
						this.addSumProductCount(ilpCount, inner_type_id, inner_need_count);
					}
				}
				else if (lp_type == ContainerTypeKey.ILP) 
				{
					DBRow ilp = lpTypeMgrZJ.getDetailILPType(type_id);
					inner_count = ilp.get("ibt_total",0);
					inner_need_count = inner_count*cci_count;
					store_count = productStoreMgrZJ.getContainerStoreAvailableCount(ps_id, pc_id, title_id, ContainerTypeKey.Original,0, lot_number);
				}*///去掉ILP和BLP
				
				//内装是原商品判断整张单据需要原商品数量是否大于库存
				if (inner_type==ContainerTypeKey.Original)
				{
					if (this.getSumProductCount(orignailCount, pc_id)>store_count)
					{
						itemStatus = ConfigChangeStoreStatusKey.NotEnough;
						configChangeStatus = ConfigChangeStoreStatusKey.NotEnough;
					}
				}
				else//内装是容器是判断整张单据需要的容器数量是否大于库存量
				{
					DBRow countRow = new DBRow();
					/*
					if (inner_type ==ContainerTypeKey.BLP)
					{
						countRow = blpCount;
					}
					else if (inner_type == ContainerTypeKey.ILP) 
					{
						countRow = ilpCount;
					}
					*///去掉ILP和BLP
					if (this.getSumLPCount(countRow, inner_type_id)>store_count)
					{
						itemStatus = ConfigChangeStoreStatusKey.NotEnough;
						configChangeStatus = ConfigChangeStoreStatusKey.NotEnough;
					}
				}
				
				DBRow preCalcuConfigChange = new DBRow();
				preCalcuConfigChange.add("cci_pc_id",pc_id);
				preCalcuConfigChange.add("cci_type",lp_type);
				preCalcuConfigChange.add("cci_type_id",type_id);
				preCalcuConfigChange.add("cci_count",cci_count);
				preCalcuConfigChange.add("inner_type",inner_type);
				preCalcuConfigChange.add("inner_type_id",inner_type_id);
				preCalcuConfigChange.add("inner_need_count",inner_need_count);
				preCalcuConfigChange.add("item_status",itemStatus);
				
				resultList.add(preCalcuConfigChange);
			}
			
			PreCalcuConfigChangeBean preCalcuConfigChangeBean = new PreCalcuConfigChangeBean();
			preCalcuConfigChangeBean.setPs_id(ps_id);
			preCalcuConfigChangeBean.setStoreStatus(configChangeStatus);
			preCalcuConfigChangeBean.setResult(resultList.toArray(new DBRow[0]));
			
			return preCalcuConfigChangeBean;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"preCalcuConfigChange",log);
		}
	}

	/**
	 * jqgrid修改CC明细
	 */
	public void modConfigChangeItemJqgrid(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long cci_id = StringUtil.getLong(request,"id");//grid默认参数是ID
			Map parameter = request.getParameterMap();
			
			long cci_pc_id;
			int cci_type = 0;
			long cci_type_id = 0;
			int cci_count;
			
			DBRow para = new DBRow();
			if(parameter.containsKey("p_name"))
			{
				String p_name = StringUtil.getString(request,"p_name");
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				cci_pc_id = product.get("pc_id",0l);
				
				para.add("cci_pc_id",cci_pc_id);
				para.add("cci_type",cci_type);
				para.add("cci_type_id",cci_type_id);
			}
			
			if (parameter.containsKey("cci_type"))
			{
				cci_type = StringUtil.getInt(request,"cci_type");
				cci_type_id = 0;
				
				para.add("cci_type",cci_type);
				para.add("cci_type_id",cci_type_id);
			}
			
			if (parameter.containsKey("cci_type_id"))
			{
				cci_type_id = StringUtil.getLong(request,"cci_type_id");
				
				para.add("cci_type_id",cci_type_id);
			}
			
			if (parameter.containsKey("cci_count"))
			{
				cci_count = StringUtil.getInt(request,"cci_count");
				
				para.add("cci_count",cci_count);
			}
			
			floorConfigChangeMgrZJ.modConfigChangeItem(cci_id, para);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modConfigChangeItemJqgrid",log);
		}
	}
	
	public DBRow getDetailConfigChangeItemJqgrid(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cci_id = StringUtil.getLong(request,"cci_id");
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
			DBRow configChangeItem = floorConfigChangeMgrZJ.getDetailConfigChangeItem(cci_id);
			
			int cci_type = configChangeItem.get("cci_type",0);
			int cci_type_id = configChangeItem.get("cci_type_id",0);
			
			configChangeItem.add("cci_type",containerTypeKey.getContainerTypeKeyValue(cci_type));
			
			if(cci_type==ContainerTypeKey.CLP)
			{
				configChangeItem.add("cci_type_id",lpTypeMgrZJ.getCLPName(cci_type_id));
			}
			/*
			else if(cci_type==ContainerTypeKey.BLP)
			{
				configChangeItem.add("cci_type_id",lpTypeMgrZJ.getBLPName(cci_type_id));
			}
			*///去掉ILP和BLP
			if (cci_type_id == 0)
			{
				configChangeItem.add("cci_type_id","");
			}
			
			return configChangeItem;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailConfigChangeItemJqgrid",log);
		}
	}
	
	/**
	 * 添加LP计数
	 * @param countRow
	 * @param lp_type_id
	 * @param count
	 * @throws Exception
	 */
	public void addSumLPCount(DBRow countRow,long lp_type_id,int count)
		throws Exception
	{
		if(countRow.getString(String.valueOf(lp_type_id)).equals(""))
		{
			countRow.add(String.valueOf(lp_type_id),count);
		}
		else
		{
			int currentQuantity = countRow.get(String.valueOf(lp_type_id),0);
			currentQuantity += count;
			countRow.add(String.valueOf(lp_type_id),currentQuantity);
		}
	}
	
	/**
	 * 获得LP计数
	 * @param countRow
	 * @param lp_type_id
	 * @return
	 * @throws Exception
	 */
	public int getSumLPCount(DBRow countRow,long lp_type_id)
		throws Exception
	{
		if (countRow.getString( String.valueOf(lp_type_id)).equals(""))
		{
			return(0);
		}
		else
		{
			return (countRow.get(String.valueOf(lp_type_id),0));
		}
	}
	
	/**
	 * 添加商品计数
	 * @param productCount
	 * @param pc_id
	 * @param count
	 * @throws Exception
	 */
	private void addSumProductCount(DBRow productCount,long pc_id,int count)
		throws Exception
	{
		if(productCount.getString(String.valueOf(pc_id)).equals(""))
		{
			productCount.add(String.valueOf(pc_id),count);
		}
		else
		{
			int currentQuantity = productCount.get(String.valueOf(pc_id),0);
			currentQuantity += count;
			productCount.add(String.valueOf(pc_id),currentQuantity);
		}
	}
	
	/**
	 * 获得商品计数
	 * @param productCount
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	private int getSumProductCount(DBRow productCount,long pc_id)
		throws Exception
	{
		if (productCount.getString( String.valueOf(pc_id)).equals(""))
		{
			return(0);
		}
		else
		{
			return (productCount.get(String.valueOf(pc_id),0));
		}
	}
	
	public void allocateConfigChange(HttpServletRequest request)
		throws Exception
	{
		long cc_id = StringUtil.getLong(request,"cc_id");
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		this.allocateConfigChangeSubmit(cc_id, adminLoggerBean);
	}
	/**
	 * 预保留库存
	 */
	public void allocateConfigChangeSubmit(long cc_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			
			DBRow configChange = floorConfigChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
			long ps_id = configChange.get("cc_psid",0l);
			long title_id = configChange.get("cc_title",0l);
			
			
			PreCalcuConfigChangeBean preConfigChangeBean = this.preCalcuConfigChange(cc_id);
			if (preConfigChangeBean.getStoreStatus()==ConfigChangeStoreStatusKey.Enough)
			{
				DBRow[] configChangeItems = preConfigChangeBean.getResult();
				
				for (int i = 0; i < configChangeItems.length; i++) 
				{
					long pc_id = configChangeItems[i].get("cci_pc_id",0l);
					int container_type = configChangeItems[i].get("inner_type",0);
					long container_type_id = configChangeItems[i].get("inner_type_id",0l);
					int need_count = configChangeItems[i].get("inner_need_count",0);
					productStoreMgrZJ.reserveProductStoreSub(ps_id, pc_id, title_id,cc_id,ProductStoreBillKey.CONFIG_CHANGE,"", adminLoggerBean, container_type, container_type_id,need_count);
				}
			}
			
			DBRow para = new DBRow();
			para.add("cc_status",ChangeStatusKey.Allocate);
			
			floorConfigChangeMgrZJ.modConfigChange(cc_id, para);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"allocateConfigChange",log);
		}
	}
	
	public void addConfigChangeForSystemBill(long ps_id,long title_id,AdminLoginBean adminLoggerBean,int system_bill_type,long system_bill_id,DBRow[] configChangeProducts)
		throws Exception
	{
		try 
		{
			DBRow configChange = new DBRow();
			configChange.add("cc_psid",ps_id);
			configChange.add("cc_create_adid",adminLoggerBean.getAdid());
			configChange.add("cc_create_time",DateUtil.NowStr());
			configChange.add("cc_status",ChangeStatusKey.Readying);
			configChange.add("cc_title",title_id);
			configChange.add("cc_system_type",system_bill_type);
			configChange.add("cc_system_id",system_bill_id);
			long cc_id = floorConfigChangeMgrZJ.addConfigChange(configChange);
			
			for (int i = 0; i < configChangeProducts.length; i++) 
			{
				DBRow configChangeItem = new DBRow();
				configChangeItem.add("cci_cc_id",cc_id);
				configChangeItem.add("cci_type",configChangeProducts[i].get("cci_type",0));
				configChangeItem.add("cci_type_id",configChangeProducts[i].get("cci_type_id",0l));
				configChangeItem.add("cci_count",configChangeProducts[i].get("cci_count",0));
				configChangeItem.add("cci_pc_id",configChangeProducts[i].get("cci_pc_id",0l));
				
				floorConfigChangeMgrZJ.addConfigChangeItem(configChangeItem);
			}
			
			this.allocateConfigChangeSubmit(cc_id, adminLoggerBean);
		}
		catch (Exception e) 
		{	
			throw new SystemException(e,"addConfigChangeForSystemBill",log);
		}
	}

	public void saveUploadConfigChangeItem(HttpServletRequest request)
		throws Exception
	{
		long cc_id = StringUtil.getLong(request,"cc_id");
		String filename = StringUtil.getString(request,"file_name");
		
		saveUploadConfigChangeItemSubmit(cc_id, filename);
	}
	
	public void saveUploadConfigChangeItemSubmit(long cc_id,String filename)
		throws Exception
	{
		floorConfigChangeMgrZJ.delConfigChangeItemByCcid(cc_id);
		DBRow[] configChangeItmes = uploadFilteToDBRow(filename);
		for (int i = 0; i < configChangeItmes.length; i++) 
		{
			DBRow configChangeItem = new DBRow();
			configChangeItem.add("cci_cc_id",cc_id);
			configChangeItem.add("cci_type",-1);
			configChangeItem.add("cci_type_id",-1);
			configChangeItem.add("cci_count",0);
			configChangeItem.add("cci_pc_id",configChangeItmes[i].get("pc_id",0l));
			
			floorConfigChangeMgrZJ.addConfigChangeItem(configChangeItem);
		}
	}
	
	public DBRow[] uploadFilteToDBRow(String filename)
		throws Exception
	{
		HashMap<String,String> filedName = new HashMap<String, String>();
		filedName.put("0","p_name");
		
		HashMap<String,String>pcidMap = new HashMap<String, String>();
		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		String path = Environment.getHome() + "upl_imags_tmp/" + filename;
		
		InputStream is = new FileInputStream(path);
		Workbook rwb = Workbook.getWorkbook(is);

		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数

		  for(int i=1;i<rsRows;i++)
		  {
		   DBRow pro = new DBRow();			   
		   
		   boolean result = false;//判断是否纯空行数据
		   for (int j=0;j<rsColumns;j++)
		   {
			   if(j==0)
			   {
				   DBRow product = floorProductMgr.getDetailProductByPname(rs.getCell(j,i).getContents().trim());
				   if(product!=null)//商品查不到，跳过循环，不添加入数组
				   {
					   pcidMap.put(String.valueOf(product.get("pc_id",0l)),String.valueOf(product.get("pc_id",0l)));
					   pro.add("pc_id",product.get("pc_id",0l));
//					   continue;
				   }
				   
				   //去重商品
//				   if(pcidMap.containsKey(String.valueOf(product.get("pc_id",0l))))
//				   {
//					   continue;
//				   }
			   }
			   
			   pro.add(filedName.get(String.valueOf(j)),rs.getCell(j,i).getContents().trim());
			   
			    ArrayList proName=pro.getFieldNames();//DBRow 字段名
			    for(int p=0;p<proName.size();p++)
			    {
			    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
			    	{
			    		result = true;
			    	}
			    }
		   }
		   
		   if(result)//不是纯空行就加入到DBRow
		   {
			   al.add(pro);   
		   }
		 }
		 return (al.toArray(new DBRow[0]));
	}
	
	public void setFloorConfigChangeMgrZJ(
			FloorConfigChangeMgrZJ floorConfigChangeMgrZJ) {
		this.floorConfigChangeMgrZJ = floorConfigChangeMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setLpTypeMgrZJ(LPTypeMgrZJ lpTypeMgrZJ) {
		this.lpTypeMgrZJ = lpTypeMgrZJ;
	}

	public void setProductStoreMgrZJ(ProductStoreMgr productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}

}
