package com.cwc.app.api.wfh;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 



import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Font;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.xj.CheckInMgrXj;
import com.cwc.app.api.zj.SQLServerMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zyj.FloorCheckInMgrZyj;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zr.SysNotifyIfaceZr;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zr.WmsLoadMgrZrIface;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInOrderComeStatusKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ReceiptLinesStatusKey;
import com.cwc.app.key.SealWithStatusKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.StrUtil;
import com.cwc.app.util.Md5;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;
 
public class CheckInMgrWfh implements CheckInMgrIfaceWfh{
	
	
    private CheckInMgrIfaceZwb checkInMgrZwb;
    private FloorCheckInMgrWfh floorCheckInMgrWfh;
    private AdminMgr adminMgr;
    private YMSMgrAPI ymsMgrAPI;
    static Logger log = Logger.getLogger("ACTION");
    private FloorCheckInMgrZwb floorCheckInMgrZwb;
    private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr;
    private WmsLoadMgrZrIface wmsLoadMgrZr;
    private FloorCheckInMgrZyj floorCheckInMgrZyj;
    private SysNotifyIfaceZr sysNotifyZr;
    private FloorScheduleMgrZr floorScheduleMgrZr;
    private SQLServerMgrZJ sqlServerMgrZJ;
    private CheckInMgrIfaceXj checkInMgrXj;
	
    /**
	 * 通过AccountId获取Packlist打印的模板
	 * @param accountId
	 * @param companyId
	 * @return 如果AccountId对应的模板存在则返回配置的模板，否则返回默认的模板
	 * @throws Exception
	 */
    public DBRow getPacklistPrintTemplate(String accountId,String companyId)throws Exception{
    	return floorCheckInMgrWfh.getPacklistPrintTemplate(accountId, companyId);
    }
	/**
	 * 查找所有的pallet
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 * 	wfh
	 */
	@Override
	public DBRow[] getAllPallet(long ps_id,int invenStatus, int titleId , PageCtrl pc) throws Exception {
		try{
			
			
			DBRow[] mains = null;
		//	if(titleId!=0){
		//		mains = floorCheckInMgrZwb.getAllPalletInventory(ps_id, 0, titleId, pc);
		//	}else{
				mains = floorCheckInMgrWfh.getAllPalletInventory(ps_id, invenStatus, titleId,pc);
		//	}
			for(int i = 0; i< mains.length; i++){
				
				long psId = mains[i].get("ps_id", 0L);
				if(psId!=ps_id){
					ps_id = psId;
				}
				int palletId = mains[i].get("pallet_id", 0);
				float pallet_count = 0F;
				float damaged_count = 0F;
				DBRow[] titles = null;
				if(titleId!=0){
					titles = floorCheckInMgrWfh.findPalletForTitleByStorageAndType(palletId, ps_id, 0, 0); //不传入titleId 传入之后 总数就会有错误 只将查询的title放在最前面 （总数是计算所得
					
					for (int j = 0; j < titles.length; j++) {
						int title_id = titles[j].get("title_id", 0);
						pallet_count += titles[j].get("pallet_count", 0F);
						damaged_count += titles[j].get("damaged_count", 0F);
						if(title_id == titleId){
							DBRow _temp = titles[j];
							titles[j] = titles[0];
							titles[0] = _temp;
						}
					}
				}else{
					titles = floorCheckInMgrWfh.findPalletForTitleByStorageAndType(palletId, ps_id, 0, 0);	//不传入titleId 传入之后 总数就会有错误 （总数是计算所得）

					for (int j = 0; j < titles.length; j++) {
						int title_id = titles[j].get("title_id", 0);
						pallet_count += titles[j].get("pallet_count", 0F);
						damaged_count += titles[j].get("damaged_count", 0F);
					}
				}
				mains[i].add("pallet_count", pallet_count);
				mains[i].add("damaged_count", damaged_count);
				mains[i].add("titles", titles);
			}
			
			return mains;
		}catch (Exception e){
			throw new SystemException(e,"getAllPallet",log);
		}
	}
	
	/**
	 * 查询 pallet 所有的 title
	 * @param palletId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow getPalletDetails(int palletId) throws Exception {
		try{
			DBRow mains  = floorCheckInMgrWfh.findPalletInventoryById(palletId);
			long ps_id = mains.get("ps_id", 0L);
			float pallet_count = 0F;
			float damaged_count = 0F;
			DBRow[] titles  = floorCheckInMgrWfh.findPalletForTitleByStorageAndType(palletId, ps_id, 0, 0);	//不传入titleId 传入之后 总数就会有错误 （总数是计算所得）
			for (int j = 0; j < titles.length; j++) {
				pallet_count += titles[j].get("pallet_count", 0F);
				damaged_count += titles[j].get("damaged_count", 0F);
			}
			mains.add("pallet_totaily", pallet_count);
			mains.add("damaged_totaily", damaged_count);
			mains.add("titles", titles);
			
			return mains;
		}catch (Exception e){
			throw new SystemException(e,"getAllPallet",log);
		}
	}
	/**
	 * 修改或者添加pallet
	 * @param para
	 * @return
	 * @throws Exception
	 */
	@Override
	public long saveOrEditPallet(DBRow para,long adid) throws Exception {
		
		try{
			int flag = para.get("flag", 0);
			para.remove("flag");
			long id = 0;
			long pitId = 0L;
			DBRow logs =new DBRow();
			DBRow pallet = floorCheckInMgrWfh.findPalletCountByIdTypeTitle(para.get("ps_id", 0L),para.get("pallet_type", ""),para.get("title_id", 0));
			pallet = pallet==null?new DBRow():pallet;
			
			DBRow palletStorage = floorCheckInMgrWfh.findPalletInventoryByPsIdAndPalletType(para.get("ps_id", 0L),para.get("pallet_type", ""));
			palletStorage = palletStorage==null?new DBRow():palletStorage;
			if(flag==0){
				
				if(pallet.get("pallet_id", 0)==0){
					
					//****************  判断仓库中是否有这种类型
					if(palletStorage.get("pallet_id", 0)!=0){
						id = palletStorage.get("pallet_id", 0);
						//****************   添加关系
						DBRow detail = new DBRow();
						detail.add("pallet_id", id);
						detail.add("title_id", para.get("title_id", 0));
						detail.add("pallet_count", para.get("pallet_count", 0F));
						detail.add("damaged_count", para.get("damaged_count", 0F));
						detail.add("post_date", para.get("post_date", ""));
						pitId = floorCheckInMgrWfh.addPalletInventroyDetail(detail);
					}else{
						
						DBRow data = new DBRow();
						data.append(para);
						data.remove("pallet_count");
						data.remove("pallet_id");
						data.remove("damaged_count");
						data.remove("title_id");
						id = floorCheckInMgrWfh.addPalletInventory(data);
						
						//****************   添加关系
						DBRow detail = new DBRow();
						detail.add("pallet_id", id);
						detail.add("title_id", para.get("title_id", 0));
						detail.add("pallet_count", para.get("pallet_count", 0F));
						detail.add("damaged_count", para.get("damaged_count", 0F));
						detail.add("post_date", para.get("post_date", "")); 
						pitId = floorCheckInMgrWfh.addPalletInventroyDetail(detail);
					}
					
					logs.add("operation", "Add");
					para.add("pallet_id", id);
				}else{
					//*************更新 关系表中的数量
					DBRow data = new DBRow();
					data.add("pallet_count", pallet.get("pallet_count",0F)+para.get("pallet_count", 0F));
					data.add("damaged_count", pallet.get("damaged_count",0F)+para.get("damaged_count", 0F));
					Long palletId = pallet.get("pallet_id", 0L);
					Long titleId = pallet.get("title_id", 0L);
					long count = floorCheckInMgrWfh.updatePalletInventoryDetail(palletId,titleId,data);
					para.add("pallet_id", palletId);
					logs.add("operation", "Update");
				}
			}else{
				float palletCount = pallet.get("pallet_count", 0F);
				float damagedCount = pallet.get("damaged_count", 0F);
				float _palletCount = para.get("pallet_count", 0F);
				float _damagedCount = para.get("damaged_count", 0F);
				palletCount = palletCount == _palletCount ? 0F:_palletCount;
				damagedCount = damagedCount == _damagedCount?0F:_damagedCount;
				para.add("pallet_count", palletCount);
				para.add("damaged_count", damagedCount);
				//*************更新 关系表中的数量
				DBRow data = new DBRow();
				data.add("pallet_count",_palletCount);
				data.add("damaged_count",_damagedCount);
				Long palletId = para.get("pallet_id", 0L);
				Long titleId = para.get("title_id", 0L);
				long count = floorCheckInMgrWfh.updatePalletInventoryDetail(palletId,titleId,data);
				logs.add("operation", "Update");
			}
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String postDate = dateformat.format(date);
			logs.add("post_date", postDate);
			logs.add("pallet_id", para.get("pallet_id", 0));
			logs.add("pallet_type", para.get("pallet_type", 0));
			logs.add("title_id", para.get("title_id", 0)==0?pitId:para.get("title_id", 0));
			logs.add("adid", adid);
			logs.add("quantity", para.get("pallet_count", 0F));
			logs.add("damaged_quantity", para.get("damaged_count", 0F));
			logs.add("ps_id", para.get("ps_id", 0L));
		
			this.palletAddLog(logs);
			return id;
		}catch (Exception e){
			throw new SystemException(e,"saveOrEditPallet",log);
		}
	}
	
	/**
	 * 按照palletId查找pallet
	 * 
	 * wfh
	 */
	@Override
	public DBRow findPalletInventoryById(int palletId) throws Exception {
		try{
			DBRow pallet =  floorCheckInMgrWfh.findPalletInventoryById(palletId);
			return pallet;
		}catch (Exception e){
			throw new SystemException(e,"findPalletInventoryById",log);
		}
	}
	
	public DBRow findPalletInventoryByPsIdAndPalletType(long ps_id, String palletType ) throws Exception{
		try{
			DBRow pallet =  floorCheckInMgrWfh.findPalletInventoryByPsIdAndPalletType(ps_id, palletType );
			return pallet;
		}catch (Exception e){
			throw new SystemException(e,"findPalletInventoryById",log);
		}
	}
	
	
	/**
	 * 修改pallet时添加日志 
	 * 
	 * wfh
	 */
	public void palletAddLog(DBRow logs) throws Exception {
		try{
			floorCheckInMgrWfh.addPalletInventoryLogs(logs);
		}catch (Exception e){
			throw new SystemException(e,"palletAddLog",log);
		}
	}
	/**
	 * 查看pallet的日志
	 * wfh
	 */
	@Override
	public DBRow[] findPalletLog(String startTime, String endTime,String palletType,long ps_id, PageCtrl pc) throws Exception {
		try{
			palletType = palletType.equals("0")?"":palletType;
			if(!StringUtil.isBlank(startTime)){
				startTime = DateUtil.showUTCTime(startTime, ps_id);
			}
			if(!StringUtil.isBlank(endTime)){
				endTime = DateUtil.showUTCTime(endTime, ps_id);
			}
			DBRow[] data = floorCheckInMgrWfh.findPalletInventoryLog( startTime,  endTime,palletType, ps_id,   pc);
			return data;
		}catch (Exception e){
			throw new SystemException(e,"findPalletLog",log);
		}
	}
	/**
	 * 查找所有的Title
	 * 
	 * wfh
	 */
	@Override
	public DBRow[] findAllTitle(String palletType, long ps_id) throws Exception {
		try{
			
			DBRow[] data = floorCheckInMgrWfh.findAllTitle(palletType,ps_id);
			return data;
		}catch (Exception e){
			throw new SystemException(e,"findAllTitle",log);
		}
	}
	/**
	 * 按照pallet的仓库 title  type 查找
	 * 
	 * wfh
	 */
	@Override
	public DBRow findPalletCountByIdTypeTitle(long ps_id, String palletType, long titleId) throws Exception {
		
		try{
			DBRow data = floorCheckInMgrWfh.findPalletCountByIdTypeTitle(ps_id,palletType,titleId);
			return data;
		}catch (Exception e){
			throw new SystemException(e,"findPalletCountByIdTypeTitle",log);
		}
	}
	/**
	 * xls 导入seal
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow importSeal4xls(HttpServletRequest request) throws Exception{
		try{
			String fileId = StringUtil.getString(request,"file_id");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
	 		long psId=adminLoggerBean.getPs_id(); 
	 		long adId=adminLoggerBean.getAdid();
	 		
	 		DBRow msg = new DBRow(); 
	 		if(fileId.equals("")){
				msg.add("data", "上传出错");
				return msg;
			}else{
				HttpClient client = new HttpClient();
				GetMethod get = new GetMethod("http://192.168.1.15:8888/huang/file/"+fileId);
				client.getHostConfiguration().setHost("http://192.168.1.15", 8888);
				get.setRequestHeader("Cookie", "JSESSIONID="+request.getCookies()[0].getValue());
				client.executeMethod(get);
				InputStream is = null;
				Workbook rwb = null;  
				try{
					is = get.getResponseBodyAsStream();
					rwb = Workbook.getWorkbook(is);
					Sheet pallets = rwb.getSheet(0);	//得到第一个sheet页
					int rsRows = pallets.getRows();    //excel表记录行数
					StringBuffer stb = new StringBuffer();
					for(int y=1;y<rsRows;y++){
						if (pallets.getCell(0,y).getContents().trim().equals("")){
							continue;
						}
						String postDate = DateUtil.NowStr();	//获得当前系统时间
						DBRow p = new DBRow();
						//p.add("flag", 0);		// 0--add 1--edit
						p.add("seal_no", pallets.getCell(0,y).getContents());
						p.add("ps_id", psId);
						p.add("import_user", adId);
						p.add("seal_status", 1);	//1,import2.used3.return
						p.add("import_time", postDate);
						
						long count = floorCheckInMgrWfh.addSealInventory(p);			//添加Seal
						if(count==0){
							stb.append(p.get("load_bar_no"));
							if(y!=rsRows-1)
								stb.append(",");
						}
						////system.out.println(y);
					}
					if(!StringUtil.isBlank(stb.toString())){
						msg.add("data", stb);
					}else{
						msg.add("data", "success");
					}
					return msg;
				}catch(Exception e){
					throw new SystemException(e,"importPallet4xls",log);
				}finally{
					if(is!=null)
						is.close();
					if(rwb!=null)
						rwb.close();
				}
			}
		}catch (Exception e){
			throw new SystemException(e,"importSeal4xls",log);
		} 
	}
	/**
	 * seal导出xls
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public String exportSeal(HttpServletRequest request) throws Exception{
		try{
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
	 		long psId=adminLoggerBean.getPs_id(); 
			SealWithStatusKey sealWithStatusKey = new SealWithStatusKey();
			String import_start_time = StringUtil.getString(request, "import_start_time");
			String import_end_time = StringUtil.getString(request, "import_end_time");
			String used_start_time = StringUtil.getString(request, "used_start_time");
			String used_end_time = StringUtil.getString(request, "used_end_time");
			String return_start_time = StringUtil.getString(request, "return_start_time");
			String return_end_time = StringUtil.getString(request, "return_end_time");
			long ps_id = StringUtil.getLong(request, "ps_id");
			int import_status = StringUtil.getInt(request,"import_status");
			
			ps_id = ps_id>0?ps_id:psId;
			if(!StringUtil.isBlank(import_start_time)){
				import_start_time = DateUtil.showUTCTime(import_start_time, ps_id);
			}
			if(!StringUtil.isBlank(import_end_time)){
				import_end_time = DateUtil.showUTCTime(import_end_time, ps_id);
			}
			if(!StringUtil.isBlank(used_start_time)){
				used_start_time = DateUtil.showUTCTime(used_start_time, ps_id);
			}
			if(!StringUtil.isBlank(used_end_time)){
				used_end_time = DateUtil.showUTCTime(used_end_time, ps_id);
			}
			if(!StringUtil.isBlank(return_start_time)){
				return_start_time = DateUtil.showUTCTime(return_start_time, ps_id);
			}
			if(!StringUtil.isBlank(return_end_time)){
				return_end_time = DateUtil.showUTCTime(return_end_time, ps_id);
			}
			DBRow para = new DBRow();
			para.add("import_start_time", import_start_time);
			para.add("import_end_time", import_end_time);
			para.add("used_start_time", used_start_time);
			para.add("used_end_time", used_end_time);
			para.add("return_start_time", return_start_time);
			para.add("return_end_time", return_end_time);
			para.add("ps_id", ps_id);
			para.add("import_status", import_status);
			DBRow[] rows = floorCheckInMgrWfh.getAllSealInventory(para);
			ps_id = ps_id>0?ps_id:psId;
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/check_in/ExportSealList.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			
			wb.setSheetName(0,"Seal Record");
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);  //得到Excel工作表的行 

			Font font = wb.createFont();
			font.setColor(HSSFColor.WHITE.index);    //白色字
			
			HSSFCellStyle titleStyle = wb.createCellStyle();
			titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			titleStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);   //设置标题背景

			titleStyle.setWrapText(true);
			
			HSSFCellStyle Style = wb.createCellStyle();
			Style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

			Style.setLocked(false);
			Style.setWrapText(true);
			
			//使用过的设置背景色为红色
		    HSSFCellStyle cellStyle = wb.createCellStyle();
			HSSFFont cellFont = wb.createFont();  
			cellFont.setColor(HSSFColor.RED.index);
			cellFont.setFontName("宋体"); 
	        cellStyle.setFont(cellFont); 
	        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
	        
	        for(int i=0;i<rows.length;i++){
	        	row = sheet.createRow(i+1); 	//创建行
				row.setHeightInPoints(17);    //设置行高
				
				row.createCell(0).setCellValue(rows[i].getString("seal_no"));
				row.createCell(1).setCellValue(rows[i].getString("title"));
				String import_time = rows[i].getString("import_time");
				String used_time = rows[i].getString("used_time");
				String assign_time = rows[i].getString("assign_time");
				String return_time = rows[i].getString("return_time");
				if(!StringUtil.isBlank(import_time)){
					import_time = DateUtil.showLocalparseDateTo24Hours(import_time,ps_id);
				}
				if(!StringUtil.isBlank(used_time)){
					used_time = DateUtil.showLocalparseDateTo24Hours(used_time,ps_id);
				}
				if(!StringUtil.isBlank(assign_time)){
					assign_time = DateUtil.showLocalparseDateTo24Hours(assign_time,ps_id);
				}
				if(!StringUtil.isBlank(return_time)){
					return_time = DateUtil.showLocalparseDateTo24Hours(return_time,ps_id);
				}
				row.createCell(2).setCellValue(import_time);
				row.createCell(3).setCellValue(rows[i].getString("import_user"));
				row.createCell(4).setCellValue(used_time);
				row.createCell(5).setCellValue(assign_time);
				row.createCell(6).setCellValue(rows[i].getString("assign_user"));
				row.createCell(7).setCellValue(return_time);
				row.createCell(8).setCellValue(rows[i].getString("return_user"));
				row.createCell(9).setCellValue(sealWithStatusKey.getSealWithStatusName(rows[i].get("seal_status",0)));
				if(rows[i].get("seal_status", 0)==2){
					row.getCell(9).setCellStyle(cellStyle);
				}
	        }
	        
			String path;
			path = "upl_excel_tmp/ExportSealList_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
			wb.write(fout);
			fout.close();
			return (path);
		}catch (Exception e){
			throw new SystemException(e,"exportSeal",log);
		}
	}
	

	/**
	 * 按照资源id 查找门的号码
	 * @param resources_id	资源id
	 * @return
	 * @throws SystemException
	 */
	@Override
	public DBRow findDoorByResourcesId(int resources_id) throws Exception {
		try {
			DBRow space_door = floorCheckInMgrWfh.findDoorByResourcesId(resources_id);
			return space_door;
		}catch(Exception e){
			throw new SystemException(e,"findDoorByResourcesId",log);
		}
	}
	/**
	 * 按照资源id 查找停车位的号码
	 * @param resources_id	资源id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow findSpotByResourcesId(int resources_id) throws Exception {
		try {
			DBRow space_spot = floorCheckInMgrWfh.findSpotByResourcesId(resources_id);
			return space_spot;
		}catch(Exception e){
			throw new SystemException(e,"findSpotByResourcesId",log);
		}
	}

	/**
	 * 当当前的detail所属的资源为占用或者保留则查询space表中本资源下所有的task
	 * @param resources_id	资源ID
	 * @param entry_id		mainID
	 * @param resources_type	资源类型（门、停车位）
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] selectSpaceBySpaceAndEntryId(int resources_id, long entry_id,int resources_type,int equipmentId) throws Exception {
		try {
			DBRow[] task = floorCheckInMgrWfh.selectSpaceBySpaceAndEntryId(resources_id,entry_id,resources_type, equipmentId);
			return task;
		}catch(Exception e){
			throw new SystemException(e,"selectSpaceBySpaceAndEntryId",log);
		}
	}
	/**
	 * 按照mainID查找设备（车头车尾  
	 * @param entry_id
	 * @param equipment_type  1----车头 2-----车尾
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findTractorByEntryId(long entry_id, int equipment_type) throws Exception {
		try {
			DBRow[] equiment = floorCheckInMgrWfh.findTractorByEntryId(entry_id, equipment_type);
			return equiment;
		}catch(Exception e){
			throw new SystemException(e,"findTractorByEntryId",log);
		}
	}
	/**
	 * 查找所有占用的或者未释放的门
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findOccupancySpaceByMainIdAndEquipmentId(long entry_id,int equipmentId) throws Exception {
		try{
			DBRow[] occupancyTaskSpace = floorCheckInMgrWfh.findOccupancyTaskSpaceByMainId(entry_id,equipmentId);
			/*DBRow[] occupancyEquipmentSpace = floorCheckInMgrWfh.findOccupancyEquipmentSpaceByMainId(entry_id,equipmentId);
			List<DBRow> list = new ArrayList<DBRow>();
			if(occupancyTaskSpace!=null){
				for(DBRow row:occupancyTaskSpace){
					list.add(row);
				}
			}
			if(occupancyEquipmentSpace!=null){
				one:for(DBRow row:occupancyEquipmentSpace){
					int resourcesId = row.get("resources_id", 0);
					int resourcesType = row.get("resources_type", 0);
					//因为两组数据可能会出现重复的所以判断以前有这个资源就跳过
					for(DBRow _row:list){
						int _resourcesId = _row.get("resources_id", 0);
						int _resourcesType = _row.get("resources_type", 0);
						if(resourcesId==_resourcesId&&_resourcesType==resourcesType)
							break one;
					}
					list.add(row);
				}
			}*/   //？设备只占用一个资源 且已经在设备号之后显示,如果这个资源上有task那么查询task表也会查找出来这个资源 
			return occupancyTaskSpace; //(DBRow[]) list.toArray(new DBRow[0]);
		}catch(Exception e){
			throw new SystemException(e,"findOccupancyDoorByMainId",log);
		}
	}

	
	/**
	 * 查找所有释放的门
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findReleaseSpaceByMainIdAndEquipmentId(long entry_id,int equipmentId) throws Exception {
		try{
			
			return floorCheckInMgrWfh.findReleaseSpaceByMainId(entry_id, equipmentId);
		}catch(Exception e){
			throw new SystemException(e,"findReleaseDoorByMainId",log);
		}
	}
	
	/**
	 * 查找设备占用的资源
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow findEquipmentOccupancySpace(int equipmentId) throws Exception {
		try{
			DBRow returnRow = null ;
			returnRow =  floorCheckInMgrWfh.findEquipmentOccupancySpace(equipmentId);
			return returnRow == null ?  new DBRow() :returnRow ;
		}catch(Exception e){
			throw new SystemException(e,"findEquipmentOccupancySpace",log);
		}
	}
	
	/**
	 * 查找checkOut时带走的entry
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findCheckOutIdByEntryId(long entry_id) throws Exception {
		try{
			
			DBRow[] ctnrs = floorCheckInMgrWfh.findCtnrByCheckOutId(entry_id);
			
			return ctnrs;
		}catch(Exception e){
			throw new SystemException(e,"findCtnrByCheckOutId",log);
		}
	}
	
	/**
	 * 查找关闭资源下的task
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findTaskClosedByEntry(long entry_id,int equipment_id) throws Exception {
		try{
			return floorCheckInMgrWfh.findTaskClosedByEntry(entry_id,equipment_id);
		}catch(Exception e){
			throw new SystemException(e,"findTaskClosedByEntry",log);
		}
	}
	
	

	/**
	 * 查找设备下所有的customer或者title
	 * @param entry_id
	 * @param equipmentId
	 * @param relType
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findCustomerOrTitleByEntryEquipment(long entry_id,int equipmentId, int relType) throws Exception {
		try{
			return floorCheckInMgrWfh.findCustomerOrTitleByEntryEquipment(entry_id,equipmentId);
		}catch(Exception e){
			throw new SystemException(e,"findCustomerOrTitleByEntryEquipment",log);
		}
	}
	
	/**
	 * 查找设备下的task个数
	 * @param equipmentId
	 * @param relType
	 * @return
	 * @throws Exception
	 */
	@Override
	public int findTaskCountByEquipmentId(long entry_id,int equipmentId) throws Exception {
		try{
			DBRow row = floorCheckInMgrWfh.findTaskCountByEquipmentId(entry_id,equipmentId);
			return row.get("count", 0);
		}catch(Exception e){
			throw new SystemException(e,"findTaskCountByEquipmentId",log);
		}
	}
	
	/**
	 * 添加loadbar
	 * @param para
	 * @return
	 * @throws Exception
	 */
	@Override
	public long saveLoadBar(DBRow para) throws Exception {
		try{
			return floorCheckInMgrWfh.saveLoadBar(para);
		}catch(Exception e){
			throw new SystemException(e,"saveLoadBar",log);
		}
	}
	
	/**
	 * 查询EntryAnd设备下的loadBar
	 * @param dloId
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findLoadBarByMainId(long dloId, int equipmentId)
			throws Exception {
		try{
			return floorCheckInMgrWfh.findLoadBarByMainId(dloId,equipmentId);
		}catch(Exception e){
			throw new SystemException(e,"findLoadBarByMainId",log);
		}
	}
	
	/**
	 * 查询所有的taskAppointment
	 * @param entry_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findAllTaskByMainId(long entry_id, long ps_id)
			throws Exception {
		try{
			ModuleKey  moduleKey = new ModuleKey();
			CheckInOrderComeStatusKey comeStatusKey = new CheckInOrderComeStatusKey();
			DBRow[] tasks = floorCheckInMgrWfh.findAllTaskByMainId(entry_id);
			if(ps_id>0){
				for (DBRow row : tasks) {
					String appointmentTime = row.get("appointment_date", "");
					if(!appointmentTime.equals("")){
						appointmentTime = DateUtil.showLocalparseDateTo24Hours(appointmentTime, ps_id);
					}
					row.add("number_type_name", moduleKey.getModuleName(row.get("number_type", 0)));
					row.add("come_status_name", comeStatusKey.geCheckInOrderComeStatusKeyValue(row.get("come_status", 0)));
					
					row.add("appointment_time", appointmentTime);
				}
			}
			return tasks;
		}catch(Exception e){
			throw new SystemException(e,"findAllTaskByMainId",log);
		}
	}
	
	/**
	 * 查询忘了checkout的设备
	 * @param ctnr
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findForgetCheckOutEquipmentByCtnr(String gate_container_no,String liscense_plate,long outByEntryId,long ps_id,String equipmentIds)
			throws Exception {
		try{
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
			DBRow[] equipments = null;
			if(!"".equals(equipmentIds)){
				equipments = floorCheckInMgrWfh.findForgetCheckOutEquipmentByEquipmentIds(equipmentIds,0L,ps_id);
			}else{
				equipments = floorCheckInMgrWfh.findForgetCheckOutEquipmentByCtnr(gate_container_no,liscense_plate,outByEntryId,ps_id);
			}
			for (DBRow row : equipments) {
				row.add("equipment_type_name", checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(row.get("equipment_type", 0)));
				if(row.get("resources_id", 0)!=0){
					DBRow space = null;
					int resources_type = row.get("resources_type", 0);
					row.add("resources_type_name", occupyTypeKey.getOccupyTypeKeyName(resources_type));
					if(resources_type==OccupyTypeKey.DOOR){
						space  = floorCheckInMgrWfh.findDoorByResourcesId(row.get("resources_id", 0));
						row.add("resources_id", space.get("doorId", 0));
					}else if(resources_type==OccupyTypeKey.SPOT){
						space  = floorCheckInMgrWfh.findSpotByResourcesId(row.get("resources_id", 0));
						row.add("resources_id", space.get("yc_no", 0));
					}
				}
			}
			return equipments;
		}catch(Exception e){
			throw new SystemException(e,"findAllWaitCheckOutEquipmentByCtnr",log);
		}
	}

	
	
	/**
	 * 关闭忘记checkout的设备
	 * @param request
	 * @throws Exception
	 */
	@Override
	public void checkOutEquipment(long outByEntryId,DBRow data) throws Exception {
		
		try{
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
			// 获取需要checkOut的设备
			long adid = data.get("adid", 0L);
			String gate_container_no=  data.get("gate_container_no","");
			String liscense_plate =  data.get("liscense_plate","");
			long ps_id = data.get("ps_id", 0L);
			String equipmentIds = data.get("equipmentIds", "");
			DBRow[] ctnrs = this.findForgetCheckOutEquipmentByCtnr(gate_container_no,liscense_plate,outByEntryId,ps_id,equipmentIds);
			
			// 遍历忘记checkOut的设备
	 		for(int i = 0 ; i < ctnrs.length ; i++)
			{
  				long equipment_id = ctnrs[i].get("equipment_id",0l); //设备ID
				String equipment_no = ctnrs[i].getString("equipment_number");
				String seal = "NA";
				int resources_type = ctnrs[i].get("resources_type",0); // 1door 2 spot
				long check_in_entry_id = ctnrs[i].get("check_in_entry_id",0L);
				int resources_id = ctnrs[i].get("resources_id",0);
				long entry_id = check_in_entry_id;	//用来判断是否被带走
				String levelHead = checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(ctnrs[i].get("equipment_type", 0));
			
				//记录设备checkout日志
				String anotherLog = "";
				
				anotherLog += levelHead + "【"+equipment_no+"】, -Release:" + occupyTypeKey.getOccupyTypeKeyName(resources_type) ;
				if(resources_id>0)
				{
					if(occupyTypeKey.DOOR==resources_type){	
						anotherLog += "【" + resources_id + "】" ;
					}else{
						anotherLog += "【" + resources_id + "】";
					}
				}
				anotherLog += ", -Seal【"+seal+"】   ";
				anotherLog += ", -Check Out By: E"+outByEntryId;
				
				// 调用记录日志方法 记录被带走设备的entry_id中
				checkInMgrZwb.addCheckInLog(adid, "Check Out InYard Same Equipment", entry_id,  CheckInLogTypeKey.GATEOUT, DateUtil.NowStr(),anotherLog);  //添加日志    车头车尾日志
				
				// 第一步 更新entry表的check_out_time和设备表的check_out_time，如果资源为door，则记录check_out_warehouse_time
				checkInMgrZwb.checkOutTime(equipment_id, adid, entry_id,resources_type+"",ctnrs[i].get("equipment_type", 0l));
				//将equipemtn的patrol_lost置为1
				DBRow para = new DBRow();
				para.add("patrol_lost", 1);
				floorCheckInMgrZyj.updateEquipment(equipment_id, para);
				// 第二步 释放资源记回task表
				checkInMgrZwb.checkOutReturnTheTask(equipment_id);
				// 第三步 查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true 
				checkInMgrZwb.checkOutTaskStatus(false,adid,1,check_in_entry_id,entry_id,equipment_id);
				// 第四步 调用詹洁的释放资源方法
				checkInMgrZwb.checkOutByZj(equipment_id, adid, entry_id);
			//	checkInMgrZwb.checkOutByZj(equipment_id, adid, outByEntryId);
				// 第五步 根据equipment_id记录seal
				checkInMgrZwb.checkOutTheSeal(equipment_id, seal);
			}
	 		
		}catch(Exception e){
			throw new SystemException(e,"checkOutEquipment",log);
		}
	}
	
	/**
	 * checkout其它相同设备时给这条entry添加日志
	 * @param request
	 * @param adid
	 * @param main_id  添加的entryID
	 * @throws Exception
	 */
	public void addCheckOutLog(DBRow data, long main_id) throws Exception {
		int checkOutFlag = data.get("checkOutFlag", 0);
		String gate_container_no= data.get("gate_container_no", "");
		String liscense_plate = data.get("liscense_plate", "");
		long adid = data.get("adid", 0L);
		long ps_id = data.get("ps_id", 0l);
		String equipmentIds = data.get("equipmentids", "");
		//查询需要CheckOut的数据liscense_plate
		OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
		CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
		DBRow[] equipments = this.findForgetCheckOutEquipmentByCtnr(gate_container_no,liscense_plate,main_id,ps_id,equipmentIds);
		
		//  END
		
		for(int j = 0 ; j < equipments.length ; j++)
		{
			long equipment_id = equipments[j].get("equipment_id",0l); //设备ID
			String equipment_no = equipments[j].getString("equipment_number");
			String seal = "NA";
			int resources_type = equipments[j].get("resources_type",0); // 1door 2 spot
			long check_in_entry_id = equipments[j].get("check_in_entry_id",0L);
			int resources_id = equipments[j].get("resources_id",0);
			long entry_id = check_in_entry_id;	//用来判断是否被带走
			String levelHead = checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipments[j].get("equipment_type", 0));
		//	if(checkOutFlag==1){
				String equipmentLog = "";
				equipmentLog += levelHead + "【"+equipment_no+"】, -Release:" + occupyTypeKey.getOccupyTypeKeyName(resources_type) ;
				if(resources_id>0)
				{
					
					if(occupyTypeKey.DOOR==resources_type){	
						equipmentLog += "【" + resources_id + "】" ;
					}else{
						equipmentLog += "【" + resources_id + "】";
					}
				}
				equipmentLog += ", -Seal【"+seal+"】   ";
				equipmentLog += ", -Check Out Entry: E"+check_in_entry_id;
				checkInMgrZwb.addCheckInLog(adid, "Check Out Inyard Same Equipment", main_id, CheckInLogTypeKey.GATEIN, DateUtil.NowStr(),equipmentLog); 
			
				///checkout 设备下面的任务
				String taskLog = "";
				//获得与设备关联的任务
				DBRow[] tasks = floorCheckInMgrZwb.getTaskByEquimentId(equipment_id,new int[0]);
				//释放任务占用的资源
				for (int i = 0; i < tasks.length; i++) 
				{
					if(!"".equals(tasks[i]) && tasks[i] != null)
					{
						String space = (tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.DOOR) ? "Door" : "Spot";
						String spaceNo = "";
						if(tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.DOOR){
							spaceNo = floorCheckInMgrZwb.findDoorById(tasks[i].get("rl_id", 0l)).get("doorId", ""); // doorId
						}
						else if(tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.SPOT)
						{
							spaceNo = floorCheckInMgrZwb.findStorageYardControl(tasks[i].get("rl_id", 0l)).get("yc_no",""); //yc_no
						}
						//查询该任务占用的资源
						DBRow task_row=this.floorSpaceResourcesRelationMgr.getSpaceResorceRelation(SpaceRelationTypeKey.Task, tasks[i].get("dlo_detail_id",0l));
						if(task_row != null && task_row.size() != 0 && !"".equals(spaceNo)){
							taskLog += "Task" +"【" + tasks[i].get("number", "") + "】Release:" + space +"【" + spaceNo + "】" + ",";
						}
						
						if(tasks[i].get("number_status", 0l) == CheckInChildDocumentsStatusTypeKey.PROCESSING || tasks[i].get("number_status", 0l) == CheckInChildDocumentsStatusTypeKey.UNPROCESS)
						{
							taskLog += "Task" +"【" + tasks[i].get("number", "") + "】ForgetTask";
						}
						taskLog +=",";
					}
				}
				if(!"".equals(taskLog))
					checkInMgrZwb.addCheckInLog(adid, "Check Out Task", main_id, CheckInLogTypeKey.GATEIN, DateUtil.NowStr(),taskLog); 
		//	}
		}
	}
	
	/**
	 * 查询通知
	 * @param entry_id
	 * @param windowCheckInTime
	 * @param scheduleType
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findScheduleByMainId(long entry_id, int scheduleType) throws Exception {
		try{
			DBRow[] schedule =  floorCheckInMgrWfh.findScheduleByMainId(entry_id, scheduleType);
				for (DBRow s : schedule) {
					DBRow[] scheduleUser = floorCheckInMgrWfh.findScheduleUserByScheduleId(s.get("schedule_id", 0));
					s.add("scheduleUser", scheduleUser);
				}
			return schedule;
		}catch(Exception e){
			throw new SystemException(e,"findScheduleByMainId",log);
		}
	}
	
	/**
	 * 按照通知ID查询通知
	 * @param scheduleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow findScheduleByScheduleId(int scheduleId) throws Exception {
		try{
			DBRow schedule = floorCheckInMgrWfh.findScheduleByScheduleId(scheduleId);
			DBRow[] scheduleUser = floorCheckInMgrWfh.findScheduleUserByScheduleId(scheduleId);
			schedule.add("scheduleUser", scheduleUser);
			return schedule;
		}catch(Exception e){
			throw new SystemException(e,"findScheduleByScheduleId",log);
		}
	}
	
	/**
	 * 按照taskId查询warehouse的通知
	 * @param detail_id
	 * @param scheduleType
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findScheduleByDetailId(int detail_id, int scheduleType)
			throws Exception {
		try{
			DBRow[] schedule = floorCheckInMgrWfh.findScheduleByDetailId(detail_id,scheduleType);
			return schedule;
		}catch(Exception e){
			throw new SystemException(e,"findScheduleByDetailId",log);
		}
	}	
	
	/**
	 * 更新seal
	 * @param pickUpSeal
	 * @param deliverySeal
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public long saveSealByEquipmentId(String pickUpSeal, String deliverySeal,
			int equipment_id) throws Exception {
		try{
			DBRow row = new DBRow();
			if(!StringUtil.isBlank(pickUpSeal)){
				row.add("seal_pick_up", pickUpSeal);
			}
			if(!StringUtil.isBlank(deliverySeal)){
				row.add("seal_delivery", deliverySeal);
			}
			ymsMgrAPI.updateEquipment(equipment_id,row);
			return 1L;
		}catch(Exception e){
			throw new SystemException(e,"findScheduleByDetailId",log);
		}
	}
	
	/**
	 * 按照设备ID查找设备
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow findEquipmentByEquipmentId(int equipmentId) throws Exception {
		try{
			return floorCheckInMgrWfh.findhEquipmentByEquipmentId(equipmentId);
		}catch(Exception e){
			throw new SystemException(e,"findhEquipmentByEquipmentId",log);
		}
	}
	/**
	 * 查询为关闭的task
	 * @param entry_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getNotCloseTaskByEquipmentId(long entry_id, int equipment_id)
			throws Exception {
		try{
			DBRow[] tasks = floorCheckInMgrWfh.findNotCloseTaskByEquipmentId(entry_id, equipment_id);
			return tasks==null||tasks.length==0?0:tasks.length;
		}catch(Exception e){
			throw new SystemException(e,"findhEquipmentByEquipmentId",log);
		}
	}
	
	/**
	 * 查找pallet
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow getPalletInfoByDetailId(long dlo_detail_id) throws Exception {
		try{
			DBRow returnRow = new DBRow();
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow != null){
				String number = detailRow.getString("number") ;
				int number_type = detailRow.get("number_type", 0);
				returnRow.add("number", number);
				returnRow.add("number_type",number_type );
				returnRow.add("ps_id",detailRow.get("ps_id", 0l) );
				if(number_type == ModuleKey.CHECK_IN_LOAD){
					addMasterBolInfo(returnRow, dlo_detail_id);
				}else{
					addOrderInfo(returnRow, dlo_detail_id);
				}
				
			}
			
			
			return returnRow ;
		}catch(Exception e){
  			throw new SystemException(e,"getPalletInfoByDetailId",log);
		}
	}

	@Override
	public DBRow getReceivePalletInfoByDetailId(long dlo_detail_id)
			throws Exception {
		try{
			DBRow returnRow = new DBRow();
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow != null){
				String number = detailRow.getString("number") ;
				int number_type = detailRow.get("number_type", 0);
				String receiptNo = detailRow.getString("receipt_no") ;
				String companyID = detailRow.getString("company_id") ;
				returnRow.add("number", number);
				returnRow.add("number_type",number_type );
				returnRow.add("ps_id",detailRow.get("ps_id", 0l) );
				DBRow receiptInfos = this.addReceiptInfo(receiptNo,companyID);
				if(receiptInfos != null){
					String receipt_no = receiptInfos.getString("receipt_no");
					returnRow.add("receipt_no", receipt_no);
					returnRow.add("total_pallets", receiptInfos.get("total_pallets",0l));
					returnRow.add("expected_qty", receiptInfos.get("expected_qty",0l));
					returnRow.add("received_qty", receiptInfos.get("received_qty",0l));
					returnRow.add("receipt_lines",addReceiptLinesAndPalletInfo(receiptNo,companyID));
				}
				
			}
			
			
			return returnRow ;
		}catch(Exception e){
  			throw new SystemException(e,"getReceivePalletInfoByDetailId",log);
		}
	}
	/**
	 * 添加Load 进来的信息
	 * @param returnRow
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author 
	 * @Date   2014年9月21日
	 */
	private void addMasterBolInfo(DBRow returnRow , long dlo_detail_id) throws Exception {
		try{
		//	DBRow[] masterBolInfos = wmsLoadMgrZr.getMasterBolBy(dlo_detail_id);
			//其他方法会用这个顾使用新的方法
			DBRow[] masterBolInfos = wmsLoadMgrZr.getMasterBolInfoByDetailId(dlo_detail_id);
			if(masterBolInfos != null){
				DBRow[] datas = new DBRow[masterBolInfos.length];
				for(int index = 0 , count = datas.length ; index < count ; index++ ){
					DBRow masterBol = masterBolInfos[index] ;
					String masterBolNo = masterBol.getString("master_bol_no");
					DBRow insertMasterBol = new DBRow();
					insertMasterBol.add("master", masterBolNo);
					insertMasterBol.add("pro_no", masterBol.getString("pro_no"));
					insertMasterBol.add("total_pallets", masterBol.get("total_pallets", 0));
					insertMasterBol.add("orders",addOrderAndPalletInfo(dlo_detail_id , masterBolNo));
					datas[index] = insertMasterBol ;
				}
				returnRow.add("datas", datas);
			}
		
		}catch(Exception e){
  			throw new SystemException(e,"addMasterBolInfo",log);
		}
	}
	/**
	 * 添加Receipt 进来的信息
	 * @param returnRow
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author 
	 * @Date   2014年9月21日
	 */
	private DBRow addReceiptInfo(String receiptNo, String companyID) throws Exception {
		try{
		
			DBRow[] receiptInfos = floorCheckInMgrWfh.getReceiptByDetailId(receiptNo,companyID);
			DBRow row = new DBRow();
			if(receiptInfos != null){
				
				long total_pallets = 0;
				long expected_qty = 0;
				long received_qty = 0;
				for(DBRow result: receiptInfos){
					total_pallets += result.get("total_pallets",0l);
					expected_qty += result.get("expected_qty",0l);
					received_qty += result.get("received_qty",0l);
					row.add("receipt_no", result.get("receipt_no",""));
					row.add("total_pallets", total_pallets);
					row.add("expected_qty", expected_qty);
					row.add("received_qty", received_qty);
					
				}
				
			}
			return row;
		
		}catch(Exception e){
  			throw new SystemException(e,"addReceiptInfo",log);
		}
	}
	/**
	 * 
	 * 
	 * @author
	 * @Date   2014年9月21日
	 */
	private void addOrderInfo(DBRow returnRow , long dlo_detail_id) throws Exception {
		try{
			DBRow masterRow = new DBRow();
			DBRow[] masterBols = new DBRow[1];
			masterBols[0] = masterRow ;
			masterRow.add("master", "0");
			masterRow.add("orders", addOrderAndPalletInfo(dlo_detail_id, "0"));
			returnRow.add("datas", masterBols);
		}catch(Exception e){
  			throw new SystemException(e,"addOrderInfo",log);
		}
	}
	
	private DBRow[] addOrderAndPalletInfo( long dlo_detail_id , String masterBolNo) throws Exception{
		try{
			DBRow[] insertOrders = null ;
	//		DBRow[] orders = wmsLoadMgrZr.getOrderInfoByDetailIdAndMasterBol(dlo_detail_id, masterBolNo);
			//其他的方法也会用到这个顾使用新得方法 
			DBRow[] orders = wmsLoadMgrZr.getOrderByDetailAndMasterBolIncludeZeroPallet(dlo_detail_id, masterBolNo);
			
			if(orders != null && orders.length > 0){
				insertOrders = new DBRow[orders.length];
				for(int index = 0 , count = orders.length ; index < count ;index++){
					DBRow inserOrder = new DBRow();
					inserOrder.add("order_no", orders[index].getString("order_numbers"));
					inserOrder.add("reference_no", orders[index].getString("reference_no"));
					inserOrder.add("po_no", orders[index].getString("po_no"));
					inserOrder.add("pro_no", orders[index].getString("pro_no"));
					inserOrder.add("pallets", orders[index].get("total_pallets", 0));
					
					long wms_order_id = orders[index].get("wms_order_id", 0l);
					DBRow[] palletInfo = wmsLoadMgrZr.getOrderPalletInfoByWmsOrderId(dlo_detail_id,wms_order_id);
					
					fixPalletInfo(palletInfo);
					inserOrder.add("pallet",palletInfo);
					insertOrders[index] = inserOrder;
				}
 			}
			return insertOrders ;
		}catch(Exception e){
  			throw new SystemException(e,"addOrderAndPalletInfo",log);
		}
	}
	private DBRow[] addReceiptLinesAndPalletInfo(String receiptNo, String companyID) throws Exception{
		try{
			DBRow[] insertReceiptLines = null ;
			ReceiptLinesStatusKey receiptLinesStatusKey = new ReceiptLinesStatusKey();
			DBRow[] receiptLines = floorCheckInMgrWfh.getReceiveLinesInfoByDetailId(receiptNo,companyID);
			if(receiptLines != null && receiptLines.length > 0){
				insertReceiptLines = new DBRow[receiptLines.length];
				for(int index = 0 , count = receiptLines.length ; index < count ;index++){
					DBRow lines = new DBRow();
					lines.add("item_id", receiptLines[index].getString("item_id"));
					lines.add("lot_no", receiptLines[index].getString("lot_no"));
					lines.add("is_check_sn", receiptLines[index].get("is_check_sn",0));
					lines.add("status", receiptLinesStatusKey.getReceiptLinesStatusKeyName(receiptLines[index].get("status",0)));
					lines.add("pallets", receiptLines[index].get("pallets",0l));
					lines.add("received_qty", receiptLines[index].get("received_qty",0l));
					lines.add("expected_qty", receiptLines[index].get("expected_qty",0l));
					lines.add("goods_total", receiptLines[index].get("goods_total",0l));
					lines.add("damage_total", receiptLines[index].get("damage_total",0l));
					lines.add("scan_start_time", receiptLines[index].getString("scan_start_time"));
					
					//break 掉的托盘  去掉不需要
				//	DBRow[] breakPalletInfo = floorCheckInMgrWfh.getReceiveBreakPalletsInfoByDetailId(receiptLines[index].get("receipt_line_id",0l));
//					for(DBRow pallet:breakPalletInfo){
//						pallet.add("plate_no", "(94)"+String.format("%012d",pallet.get("palte_no",0)));
//					}
					//正常的托盘
					DBRow[] palletInfo = floorCheckInMgrWfh.getReceivePalletsInfoByDetailId(receiptLines[index].get("receipt_line_id",0l));
					for(DBRow pallet:palletInfo){
						pallet.add("plate_no", "(94)"+String.format("%012d",pallet.get("plate_no",0)));
					}
				//	fixPalletInfo(palletInfo);
					lines.add("pallet",palletInfo);
				//	lines.add("break_pallet",breakPalletInfo);
					insertReceiptLines[index] = lines;
				}
 			}
			return insertReceiptLines ;
		}catch(Exception e){
  			throw new SystemException(e,"addReceiptLinesAndPalletInfo",log);
		}
	}
	private void fixPalletInfo(DBRow[] palletInfos){
		if(palletInfos != null && palletInfos.length > 0){
			for(DBRow pallet : palletInfos){
				pallet.remove("wms_order_id");
				pallet.remove("wms_order_type_id");
			}
		}
	}
	
	
	@Override
	public DBRow[] findTaskByNumber(String number,int numberType) throws Exception {
		try{
			
			return floorCheckInMgrWfh.findTaskByNumber(number,numberType);
		}catch(Exception e){
			throw new SystemException(e,"findTaskByNumber",log);
		}
	}
	
	/**
	 * 查看task是否已经开始，返回未开始的task
	 * @param tasks
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<HoldDoubleValue<Long, String>> findTaskIsStart(
			List<HoldDoubleValue<Long, String>> tasks) throws Exception {
		try{
			StringBuffer detailIds = new StringBuffer(); 
			for (HoldDoubleValue<Long, String> temp : tasks) {
				long detail_id = temp.a;
				detailIds.append(detail_id).append(",");
			}
			String str_detailsIds = detailIds.toString();
			str_detailsIds = str_detailsIds.substring(0,str_detailsIds.length()-1);
			DBRow[] task= floorCheckInMgrWfh.findTaskByIds(str_detailsIds);
			for(DBRow row:task){
				long detail_id = row.get("dlo_detail_id", 0L);
				int status = row.get("number_status", 0);
				for(int i=0;i<tasks.size();i++){
					long detailId = tasks.get(i).a;
					if(detailId == detail_id && status!=CheckInChildDocumentsStatusTypeKey.UNPROCESS){  //如果不是未开始那么就移除不需要再重复开始任务
						tasks.remove(i);
						break;
					}
				}
			}
			return tasks;
		}catch(Exception e){
			throw new SystemException(e,"findTaskByNumber",log);
		}
	}
	
	/**
	 * 得到所有的customer
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] getAllCustomer() throws Exception {
		try{
			DBRow[] customer = this.floorCheckInMgrWfh.getAllCustomer();
			Map<String,String> map = new HashMap<String, String>();
			List<DBRow> temp_customer = new ArrayList<DBRow>();
			for(DBRow row:customer){      //先将不含有 "," 的放入list
				if(row.get("customer_id", "").contains(",")){  
					String[] customers = row.get("customer_id", "").split(",");
					for (String s : customers) {
						map.put(s, s);
					}
					continue;
				}
				map.put(row.get("customer_id", ""), row.get("customer_id", ""));
			}
			for(String key:map.keySet()){
				DBRow row = new DBRow();
				row.add("customer_id", key);
				temp_customer.add(row);
			}
			
			customer = (DBRow[]) temp_customer.toArray(new DBRow[0]);
			return customer;
		}catch(Exception e){
			throw new SystemException(e,"findTaskByNumber",log);
		}
	}
	
	/**
	 * 分页查找sn
	 */
	@Override
	public DBRow[] findSNByConID(long con_id, int pageNo, int pageSize,long ps_id)
			throws Exception {
		try{
			PageCtrl pc = new PageCtrl();
			pc.setPageNo(pageNo);
			pc.setPageSize(pageSize);
			DBRow[] sn = floorCheckInMgrWfh.findSNByConID(con_id,pc);
			int i=1;
			for (DBRow s : sn) {
				if(!StringUtil.isBlank(s.get("update_date", ""))){
					s.add("update_date", DateUtil.showLocalparseDateToNoYear24Hours(s.get("update_date", ""), ps_id));
				}
				s.add("serial_id", i + (pageSize*(pageNo-1)));
				i++;
			}
			return sn;
		}catch(Exception e){
			throw new SystemException(e,"findSNByConID",log);
		}
	}
	/**
	 * 查找customer
	 */
	@Override
	public DBRow[] getCustomer() throws Exception {
		try {
			return floorCheckInMgrWfh.getAllCustomer();
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.getCustomer",log);
		}
	}
	/**
	 * 查找smallParcel
	 */
	@Override
	public DBRow[] getSmallParcelCarrier() throws Exception {
		try {
			return floorCheckInMgrWfh.getSmallParcelCarrier();
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.getSmallParcelCarrier",log);
		}
	}
	/**
	 * load 所有的order
	 */
	@Override
	public DBRow scanAllPalletOrTackNO(long detail_id,long adid) throws Exception {
		try {
			String updateTime = DateUtil.NowStr();
			DBRow detail =  floorCheckInMgrWfh.findTaskByIds(String.valueOf(detail_id))[0];
			DBRow[] space = floorCheckInMgrWfh.findTaskOccupanyResources(detail.get("dlo_detail_id", 0L));
			if(space.length == 0){
				space = new DBRow[]{new DBRow()};
			}
			long lr_id = detail.get("lr_id", 0l);
			long carrier_id = detail.get("small_parcel_carrier_id", 0L);
			DBRow equipment = floorCheckInMgrWfh.findhEquipmentByEquipmentId(detail.get("equipment_id", 0));
			long ps_id = equipment.get("ps_id", 0L);
			
			DBRow upRow = new DBRow();
			upRow.add("resources_type", space[0].get("resources_type", 0));
			upRow.add("resources_id", space[0].get("resources_id", 0));
			upRow.add("scan_time", updateTime);
			upRow.add("scan_adid", adid);
			upRow.add("dlo_detail_id", detail_id);
			StringBuffer palletIDs = new StringBuffer();
			DBRow[] orders = this.findOrdersAndPallet(ps_id, carrier_id,1);
			for (DBRow order : orders) {
				DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
				StringBuffer appenedTN = new StringBuffer();
				boolean flag = true;
				for (DBRow pallet : pallets) {
					long wms_order_type_id = pallet.get("wms_order_type_id", 0L);
					if(pallet.get("dlo_detail_id", 0L) != detail_id){
						flag = false;
						break;
					}
					if(pallet.get("scan_id", 0L)==0){
						appenedTN.append(wms_order_type_id).append(",");
					}
				}
				if(flag){
					palletIDs.append(appenedTN);
				}
			}
			if(orders.length>0){
				if(palletIDs.length()>0){
					String upPalletIds = palletIDs.toString();
					upPalletIds = upPalletIds.substring(0,upPalletIds.length()-1);
					floorCheckInMgrWfh.updateWMSLoadOrderPalletByIds(upPalletIds,upRow);
				}
			}else{
				long id = floorCheckInMgrWfh.updateWMSLoadOrderPalletBylrId(lr_id, upRow);
			}
			DBRow data = new DBRow();
//			if(id>0){
//				data.add("success", true);
//			}else{
//				data.add("success", false);
//			}
			return data;
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.loadAllOrder",log);
		}
	}
	/**
	 * 扫描pallet或者是trackNo
	 * @param wms_order_type_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow scanPalletOrTrackNo(String scan_numbers,long detail_id,long adid) throws Exception {
		try {
			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(String.valueOf(detail_id));
			DBRow[] spaces = floorCheckInMgrWfh.findTaskOccupanyResources(detail_id);
			DBRow space = new DBRow();
			if(spaces.length > 0){
				space = spaces[0];
			}
			long lr_id = task[0].get("lr_id", 0L);
			String updateTime = DateUtil.NowStr();
			
			DBRow upRow = new DBRow();
			upRow.add("resources_type", space.get("resources_type", 0));
			upRow.add("resources_id", space.get("resources_id", 0));
			upRow.add("scan_time", updateTime);
			upRow.add("scan_adid", adid);
			upRow.add("dlo_detail_id", detail_id);
			
			
			StringBuffer scanTNIDS = new StringBuffer();  //扫描的pallet&tn
			JSONArray scanNumbers = new JSONArray(scan_numbers);
			for(int i=0;i<scanNumbers.length();i++){
				 
				JSONObject scan_number = scanNumbers.getJSONObject(i);
				long order_id = StringUtil.getJsonLong(scan_number, "wms_order_id");
				long wms_order_type_id = StringUtil.getJsonLong(scan_number, "wms_order_type_id");
				//没有orderId，那就是什么都没有查出来扫描提交的数据
				if(order_id == 0){
					DBRow pallet = new DBRow();
					pallet.add("wms_scan_number_type", 3);
					pallet.add("scan_numer", StringUtil.getJsonString(scan_number, "scan_number"));
					wms_order_type_id = this.addWMSLoadOrderPalletReturnPalletID(detail_id, lr_id, pallet, 0);
					//添加完则算是扫描完成
					long id = floorCheckInMgrWfh.updateWMSLoadOrderPalletByWmsOrderTypeId(wms_order_type_id, upRow);
				}
				//记录下来所有的tn_id
				else{
					if(!scanTNIDS.toString().contains(String.valueOf(wms_order_type_id))){
						scanTNIDS.append(wms_order_type_id).append(",");
					}
				}
			}
			long rows = -1;
			//更新扫描的pallet&tn
			if(scanTNIDS.length()>0){
				rows = 0;
				String palletIDS = scanTNIDS.substring(0,scanTNIDS.length()-1);
				rows += floorCheckInMgrWfh.updateWMSLoadOrderPalletByIds(palletIDS, upRow);
			}
			DBRow data = new DBRow();
		//	data.add("status", rows);
			return data;
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.loadAllOrder",log);
		}
	}
	/**
	 * 去掉扫描的状态
	 */
	@Override
	public DBRow removeScanState(long wms_order_type_id, long detail_id,
			int wms_scan_number_type) throws Exception {
		try{
			DBRow upRow = new DBRow();
			upRow.add("scan_time", null);
			upRow.add("resources_type", null);
			upRow.add("resources_id", null);
			upRow.add("scan_adid", null);
			upRow.add("dlo_detail_id", null);
//			upRow.add("is_scanned", 0);
			if(wms_scan_number_type == 2){
				floorCheckInMgrWfh.updateWMSLoadOrderPalletByWmsOrderTypeId(wms_order_type_id, upRow);
			}else if(wms_scan_number_type == 3){
				floorCheckInMgrWfh.deleteWMSLoadOrderPalletCountByPalletID(String.valueOf(wms_order_type_id));
			}
			return null;
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.removeScanState",log);
		}
	}
	/**
	 * reload 需要变动的orders
	 */
	@Override
	public DBRow reloadOrder(long detail_id,int is_keep_order) throws Exception {
		try {
			DBRow result = new DBRow();
			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(detail_id+"");
			DBRow equipment = floorCheckInMgrWfh.findhEquipmentByEquipmentId(task[0].get("equipment_id", 0));
			long ps_id = equipment.get("ps_id", 0L);
			long lr_id = task[0].get("lr_id", 0L);
			long carrier_id = task[0].get("small_parcel_carrier_id", 0L);
			
			reloadOrder(detail_id, is_keep_order, ps_id, lr_id, carrier_id);
			
			DBRow[] data = this.findOrdersAndPallet(ps_id, carrier_id,0);
			List<DBRow> returnOrders = new ArrayList<DBRow>();
			for (DBRow order : data) {
				DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
				if(pallets.length == 0){
					returnOrders.add(order);
				}else if(pallets.length >0){
					boolean flag = true;
					for (DBRow pallet : pallets) {
						long scan_detail_id = pallet.get("dlo_detail_id", 0L);
						if(scan_detail_id!=0 && scan_detail_id!=detail_id){
							flag = false;
							break;
						}
					}
					if(flag){
						returnOrders.add(order);
					}
				}
			}
			if(data.length == 0){
				DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPalletByLRID(lr_id);
				List<DBRow> returnPallet = new ArrayList<DBRow>();
				for (DBRow pallet : pallets) {
					if(pallet.get("wms_scan_number_type", 0) == 3){
						String scan_time = pallet.get("scan_time", "");
						if(!StringUtil.isBlank(scan_time)){
							scan_time = DateUtil.showLocalparseDateTo24Hours(scan_time, ps_id);
						}
						pallet.add("scan_time", scan_time);
						returnPallet.add(pallet);
					}
				}
				result.add("data", returnPallet.toArray(new DBRow[0]));
				result.add("is_order", 0);
				return result;
			}
			result.add("data", returnOrders.toArray(new DBRow[0]));
			result.add("is_order", 1);
			return result;
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.reloadOrder",log);
		}
	}
	/**
	 * 重新载入order 
	 * @param detail_id
	 * @param is_keep_order
	 * @param ps_id
	 * @param lr_id
	 * @param carrier_id
	 * @throws Exception
	 */
	private void reloadOrder(long detail_id, int is_keep_order, long ps_id,
			long lr_id, long carrier_id) throws Exception {
		
		//得到所有不能删除的order
		List<DBRow> allOrders = new ArrayList<DBRow>();
		//得到所有不能删除的pallet
		List<DBRow> allTNs = new ArrayList<DBRow>();
		//查询wms的orders 与 pallet
		DBRow[] wms_orders = this.findWMSOrderAndPallet(carrier_id, ps_id);
		//将本地的所有的order都转为map 以orderNO+wms_company 为key  查询全部的order 数量应该与wms查询出来的一致
		DBRow[] orders = this.findOrdersAndPallet(ps_id, carrier_id,0);
		Map<String,DBRow> allOrderMap = new HashMap<String, DBRow>();
		this.convertOrderArray2MapForKeyOrderNo(allOrderMap, orders);
		//reload 全部的时候不需要得到交叉存在的部分
		if(is_keep_order == 1){
			//在两组数据间,互相标记交叉存在的部分
			for (DBRow wms_order : wms_orders) {
				String WMSOrderNO =wms_order.get("OrderNo", "");
				String wms_company_id = wms_order.get("CompanyID", "");
				//如果order在本地可以查到则表示这个order存在
				DBRow order = allOrderMap.get(WMSOrderNO+wms_company_id);
				if(order != null){
					wms_order.add("is_have", 1);
					allOrders.add(order);
					DBRow[] wms_trackNos = (DBRow[]) wms_order.get("tracking_nos", new DBRow[0]);
					//将sync中的tn转为map
					DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
					Map<String,DBRow> palletMap = new HashMap<String, DBRow>();
					convertPalletArray2Map(palletMap,pallets,WMSOrderNO,wms_company_id);
					//遍历wms order下的track看在sync中是否存在  存在则在wms的数组中加上标识，将sync中存在的track加到pallet list中
					for(DBRow wms_trackNo:wms_trackNos){
						String trackNo = wms_trackNo.get("TrackingNo", "");
						//看这个order下的pallet Or track在sync中是否存在
						DBRow tn = palletMap.get(trackNo+WMSOrderNO+wms_company_id);
						if(tn!=null){
							wms_trackNo.add("is_have", 1);
							allTNs.add(tn);
						}
					}
				}
			}
		}
		
		//删除mysql中在wms查询数据中不存在的部分  reload all mysql中order状态为open的order
		if(wms_orders.length>0 || (wms_orders.length==0 && is_keep_order!=1)){
			String order_id = "";
			String pallet_id = "";

			//将所有不能删除的order转为map
			Map<Long,DBRow> orderMap = new HashMap<Long, DBRow>();
			this.convertOrderArray2Map(orderMap, allOrders.toArray(new DBRow[0]));
			//本地已经close的不能删除
			List<DBRow> closedOrder = new ArrayList<DBRow>();
			for(DBRow order:orders){
				//close的托盘不能删除
				if(order.get("status", "").equals("Closed")){
					closedOrder.add(order);
					//将不能删除的托盘也添加到不能删除托盘的数组
					DBRow[] pallets = (DBRow[]) order.get("pallets",new DBRow[0]);
					for(DBRow pallet:pallets){
						allTNs.add(pallet);
					}
				}else{
					DBRow[] pallets = (DBRow[]) order.get("pallets",new DBRow[0]);
					//不是这个task扫描的order也不能删除
					boolean addFlag = false;
					for (DBRow pallet : pallets) {
						if(pallet.get("dlo_detail_id", 0L)!=0 && pallet.get("dlo_detail_id", 0L)!=detail_id){
							addFlag = true;
							break;
						}
					}
					//不能删除order将pallet也加到数组中
					if(addFlag){
						closedOrder.add(order);
						for(DBRow pallet:pallets){
							allTNs.add(pallet);
						}
					}
				}
			}
			this.convertOrderArray2Map(orderMap, closedOrder.toArray(new DBRow[0]));
			//本地已经close的不能删除的order加到allOrderMap中
			
			//将所有不能删除的TN转为map
			Map<Long,DBRow> TNMap = new HashMap<Long, DBRow>();
			this.convertPalletArray2MapKeyIsWmsOrderTypeID(TNMap, allTNs.toArray(new DBRow[0]));
			
			//遍历本地所有的order
			for(DBRow order:orders){
				long orderID = order.get("wms_order_id", 0L);
				DBRow haveOrder = orderMap.get(orderID);
				//如果在不能删除的order中没有找到 则删除
				if(haveOrder!=null){
					//拿到这个order下所有的pallet
					DBRow[] pallets = (DBRow[]) order.get("pallets",new DBRow[0]);
					for (DBRow pallet : pallets) {
						long wms_order_type_id = pallet.get("wms_order_type_id", 0L);
						DBRow havePallet = TNMap.get(wms_order_type_id);
						//如果在不能删除能找到对应的tn 则不能删除
						if(havePallet == null){
							pallet_id += pallet.get("wms_order_type_id", 0L)+",";
						}
					}
				}else{
					order_id += orderID+",";
				}
			}
			
			//什么都没进来全部删除TN
			if(wms_orders.length==0 && is_keep_order!=1){
				DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPalletByLRID(lr_id);
				for (DBRow pallet : pallets) {
					if(pallet.get("wms_scan_number_type", 0)!=3){
						continue;
					}
					pallet_id += pallet.get("wms_order_type_id", 0L)+",";
				}
			}
			if(!order_id.equals("")){
				order_id = order_id.substring(0,order_id.length()-1);
				floorCheckInMgrWfh.deleteWMSLoadOrderByOrderID(order_id);
				//将order下的pallet也尽数删除
				floorCheckInMgrWfh.deleteWMSLoadOrderPalletCountByOrderIDS(order_id);
			}
			if(!pallet_id.equals("")){
				pallet_id = pallet_id.substring(0,pallet_id.length()-1);
				floorCheckInMgrWfh.deleteWMSLoadOrderPalletCountByPalletID(pallet_id);
			}
		}
		//将wmsOrder中未标记的 添加到mysql中
		for(DBRow wms_order:wms_orders){
			String orderNo = wms_order.get("OrderNo", "");
			String company_id = wms_order.get("CompanyID", "");
			//这个order是否在sync中已经存在
			int is_have = wms_order.get("is_have", 0);
			if(wms_order.get("is_have", 0)!=1){
				//添加order  如果order存在则不添加
				long wms_order_id = 0L;
				if(is_have == 1){
					DBRow order = allOrderMap.get(orderNo+company_id); 
					wms_order_id = order.get("wms_order_id", 0L);
				}else{
					//在reload all的时候wms的数组中不会有is_have字段 。如果在不能删除的map能能查到则不添加
					DBRow order = allOrderMap.get(orderNo+company_id); 
					if(order != null){
						wms_order_id = order.get("wms_order_id", 0L);
						String status = order.get("status", "");
						//在sync中托盘已经关闭不能 reload all
						if(status.equals("Closed")){
							continue;
						}else{
							//不是当前任务扫描的order reload all
							DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
							boolean continueFlag = false;
							for (DBRow pallet : pallets) {
								if(pallet.get("dlo_detail_id", 0L)!=0 && pallet.get("dlo_detail_id", 0L) !=detail_id){
									continueFlag = true;
									break;
								}
							}
							if(continueFlag){
								continue;
							}else{
								//这个order关闭并且也是本task扫描的order之前已经删除需要重新添加order
								wms_order_id = addWMSLoadOrderReturnOrderID(lr_id, wms_order,ps_id);
							}
						}
					}else{
						wms_order_id = addWMSLoadOrderReturnOrderID(lr_id, wms_order,ps_id);
					}
				}
				//添加trackNO
				DBRow[]  wms_trackNos = (DBRow[]) wms_order.get("tracking_nos", new DBRow[0]);
				for (DBRow wms_trackNo : wms_trackNos) {
					//只有 wms的order在sync中存在才需要确认这个tn时候存在，order不存在tn则不会存在
					if(is_have==1 && wms_trackNo.get("is_have", 0) == 1  ){
						continue;
					}
					DBRow pallet = new DBRow();
					pallet.add("scan_numer", wms_trackNo.get("TrackingNo", ""));
					pallet.add("wms_scan_number_type",2);
					pallet.add("order_number", wms_order.get("OrderNo", ""));
				//	System.out.println(wms_order_id);
					this.addWMSLoadOrderPalletReturnPalletID(detail_id, lr_id, pallet, wms_order_id);
				}
			}
		}
	}
	
	/**
	 * 将pallet array转为map 以palletNumbet作为主键
	 * @param palletMap
	 * @param pallets
	 * @throws Exception
	 */
	private void convertPalletArray2Map(Map<String,DBRow> palletMap,DBRow[] pallets,String orderNo,String company_id) throws Exception{
		for (DBRow pallet : pallets) {
			String palletNO = pallet.get("wms_pallet_number", "");
			palletMap.put(palletNO+orderNo+company_id, pallet);
		}
	}
	/**
	 * 将pallet array转为map 以wms_order_type_id作为主键
	 * @param palletMap
	 * @param pallets
	 * @throws Exception
	 */
	private void convertPalletArray2MapKeyIsWmsOrderTypeID(Map<Long,DBRow> palletMap,DBRow[] pallets) throws Exception{
		for (DBRow pallet : pallets) {
			long wmsOrderTypeID = pallet.get("wms_order_type_id", 0L);
			palletMap.put(wmsOrderTypeID, pallet);
		}
	}
	/**
	 * 将order array转为map 以order_id作为主键
	 * @param palletMap
	 * @param pallets
	 * @throws Exception
	 */
	private void convertOrderArray2Map(Map<Long,DBRow> orderMap,DBRow[] orders) throws Exception{
		for (DBRow order : orders) {
			long order_id = order.get("wms_order_id", 0L);
			orderMap.put(order_id, order);
		}
	}
	/**
	 * 将order array转为map 以order_no+company作为主键
	 * @param palletMap
	 * @param pallets
	 * @throws Exception
	 */
	private void convertOrderArray2MapForKeyOrderNo(Map<String,DBRow> orderMap,DBRow[] orders) throws Exception{
		for (DBRow order : orders) {
			String order_no = order.get("order_numbers", "");
			String company_id = order.get("company_id", "");
			orderMap.put(order_no+company_id, order);
		}
	}

	/**
	 * 过滤samllParcel
	 */
	@Override
	public DBRow filterSmallParcel(long carrier_id, long customer_id,long detail_id)
			throws Exception {
		try {
			DBRow result = new DBRow();
			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(detail_id+"");
			long lr_id = task[0].get("lr_id", 0L);
			DBRow equipment = floorCheckInMgrWfh.findhEquipmentByEquipmentId(task[0].get("equipment_id", 0));
			long ps_id = equipment.get("ps_id", 0L);
			
			//每次都重新载入order ，但是保留上次的扫描的pallet
			this.reloadOrder(detail_id, 1, ps_id, lr_id, carrier_id);
			//更新detail
			DBRow upRow = new DBRow();
			upRow.add("small_parcel_carrier_id", carrier_id);
			floorCheckInMgrWfh.updateDetailByID(detail_id,upRow);
			//查询orders 返回
			DBRow[] orders = findOrdersAndPallet(ps_id, carrier_id,1);
			//只显示为扫描与本任务扫描的task
			List<DBRow> returnOrders = new ArrayList<DBRow>();
			for (DBRow order : orders) {
				DBRow[] pallets = (DBRow[]) order.get("pallets", new DBRow[0]);
				if(pallets.length == 0){
					returnOrders.add(order);
				}else if(pallets.length >0){
					boolean flag = true;
					for (DBRow pallet : pallets) {
						long scan_detail_id = pallet.get("dlo_detail_id", 0L);
						if(scan_detail_id!=0 && scan_detail_id!=detail_id){
							flag = false;
							break;
						}
					}
					if(flag){
						returnOrders.add(order);
					}
				}
			}
			//如果没有order则返回pallet
			if(orders.length == 0){
				DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPalletByLRID(lr_id);
				List<DBRow> returnPallet = new ArrayList<DBRow>();
				for (DBRow pallet : pallets) {
					if(pallet.get("wms_scan_number_type", 0) == 3){
						String scan_time = pallet.get("scan_time", "");
						if(!StringUtil.isBlank(scan_time)){
							scan_time = DateUtil.showLocalparseDateTo24Hours(scan_time, ps_id);
						}
						pallet.add("scan_time", scan_time);
						returnPallet.add(pallet);
					}
				}
				result.add("data", returnPallet.toArray(new DBRow[0]));
				result.add("is_order", 0);
				return result;
			}
			result.add("is_order", 1);
			result.add("data", returnOrders.toArray(new DBRow[0]));
			return result;
		}catch(Exception e){
			throw new SystemException(e,"checkInMgrWfh.filterSmallParcel",log);
		}
	}
	/**
	 * 清掉task其他carrier扫描的order
	 * @param carrier_id
	 * @param detail_id
	 * @throws Exception
	 */
	public void freshOtherCarrierOrder(long carrier_id,long detail_id) throws Exception{
		DBRow[] carrier_subs = floorCheckInMgrWfh.findSmallParcelCarrierSubByCarrierId(carrier_id);
		StringBuffer carrier_sub_name_sb = new StringBuffer();
		for (DBRow csub : carrier_subs) {
			carrier_sub_name_sb.append("'"+csub.get("carrier_name","")+"'").append(",");
		}
		String carrier_sub = carrier_sub_name_sb.toString();
		carrier_sub = carrier_sub.substring(1,carrier_sub.length()-2);
		
		DBRow[] freshOrders = floorCheckInMgrWfh.findTaskOtherCarrierOrder(carrier_sub,detail_id);
		DBRow upRow = new DBRow();
		upRow.add("dlo_detail_id", null);
		upRow.add("scan_time", null);
		upRow.add("resources_type", null);
		upRow.add("resources_id", null);
		upRow.add("scan_adid", null);
//		upRow.add("is_scanned", 0);
		DBRow upOrder = new DBRow();
		upOrder.add("status", "Open");
		for (DBRow order : freshOrders) {
			long orderId = order.get("wms_order_id", 0L);
			floorCheckInMgrWfh.updateWMSLoadOrderPalletCountByOrderID(orderId, upRow);
			
			floorCheckInMgrWfh.updateWMSLoadOrder(String.valueOf(orderId), upOrder);
		}
	}
	/**
	 * 添加wms_load_order_pallet_type_count   --pallet
	 * @param detail_id
	 * @param lr_id
	 * @param wms_order
	 * @param order_id
	 * @param pallet
	 * @return
	 * @throws Exception
	 */
	private long addWMSLoadOrderPalletReturnPalletID(long detail_id,long lr_id, DBRow pallet, long order_id) throws Exception {
		DBRow upRow = new DBRow();
		upRow.add("wms_pallet_number", pallet.get("scan_numer", ""));
		upRow.add("wms_scan_number_type", pallet.get("wms_scan_number_type", 3));
		upRow.add("order_number",  pallet.get("order_number", ""));
		upRow.add("wms_order_id", order_id);
		upRow.add("delivery_or_pick_up", 2);  //装货，出货
		upRow.add("lr_id", lr_id);
//		"INSERT INTO wms_load_order (wms_pallet_number,wms_scan_number_type,order_number,wms_order_id"
//		+ ",delivery_or_pick_up,lr_id) VALUES "
//		convetDBRow2InsertSql(upRow);
//		return convetDBRow2InsertSql(upRow);
		return floorCheckInMgrWfh.addWmsLoadOrderPalletTypeCount(upRow);
	}
	
	/**
	 * 主页面查看scan的order
	 */
	@Override
	public DBRow findLoadSmallParcelOrderByDetailId(long detail_id)
			throws Exception {
		try{
			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(detail_id+"");
			DBRow equipment = floorCheckInMgrWfh.findhEquipmentByEquipmentId(task[0].get("equipment_id", 0));
			long ps_id = equipment.get("ps_id", 0L);
			DBRow result = new DBRow();
			DBRow[] orders = floorCheckInMgrWfh.findWMSLoadOrder(String.valueOf(detail_id));
			for (DBRow order : orders) {
				long wms_order_id = order.get("wms_order_id", 0L);
				DBRow[] palletDbRows = floorCheckInMgrWfh.findwmsLoadOrderPallet(wms_order_id);
				for (DBRow pallet : palletDbRows) {
					pallet.add("scan_time", DateUtil.showLocalparseDateTo24Hours(pallet.get("scan_time", ""), ps_id));
				}
				order.add("pallets", palletDbRows);
				
				String shipped_date = order.get("shipped_date","");
				if(!StringUtil.isBlank(shipped_date)){
					order.add("shipped_date", DateUtil.showLocalparseDateTo24Hours(shipped_date, ps_id));
				}
			}
			if(orders.length>0){
				result.add("data", orders);
				result.add("is_order", 1);
			}else{
				DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPalletByDetailID(detail_id);
				List<DBRow> returnPallet = new ArrayList<DBRow>();
				for (DBRow pallet : pallets) {
					if(pallet.get("wms_scan_number_type", 0) == 3){
						String scan_time = pallet.get("scan_time", "");
						if(!StringUtil.isBlank(scan_time)){
							scan_time = DateUtil.showLocalparseDateTo24Hours(scan_time, ps_id);
						}
						pallet.add("scan_time", scan_time);
						returnPallet.add(pallet);
					}
				}
				result.add("data", returnPallet.toArray(new DBRow[0]));
				result.add("is_order", 0);
			}
			
			result.add("number", task[0].get("number", ""));
			result.add("number_type", task[0].get("number_type", 0));
			return result;
		}catch (Exception e){
			throw new SystemException(e,"checkInMgrWfh.findLoadSmallParcelOrderByDetailId",log);
		}
	}
	
	
	/*
	 * 添加wms_load_order
	 */
	private long addWMSLoadOrderReturnOrderID(long lr_id, DBRow wms_order,long ps_id)
			throws Exception {
		DBRow upRow = new DBRow();
		upRow.add("order_numbers", wms_order.get("OrderNo", ""));
		upRow.add("status", "Open");
		upRow.add("pallets", wms_order.get("pallets_count", 0)+wms_order.get("tracking_count", 0));
		upRow.add("staging_area_id", wms_order.get("StagingAreaID", ""));
		upRow.add("pro_no", wms_order.get("ProNo", ""));
		upRow.add("po_no", wms_order.get("PONo", ""));
		upRow.add("reference_no", wms_order.get("ReferenceNo", ""));
		upRow.add("note", wms_order.get("Note", ""));
		upRow.add("wms_is_close", 1);
		upRow.add("company_id", wms_order.get("CompanyID", ""));
		upRow.add("account_id", wms_order.get("AccountID", ""));
		upRow.add("ship_to_id", wms_order.get("ShipToID", ""));
		upRow.add("ship_to_name", wms_order.get("ShipToName", ""));
		upRow.add("ship_to_address1", wms_order.get("ShipToAddress1", ""));
		upRow.add("ship_to_address2", wms_order.get("ShipToAddress2", ""));
		upRow.add("ship_to_city", wms_order.get("ShipToCity", ""));
		upRow.add("ship_to_state", wms_order.get("ShipToState", ""));
		upRow.add("ship_to_zip_code", wms_order.get("ShipToZipCode", ""));
		upRow.add("ship_to_country", wms_order.get("ShipToCountry", ""));
		upRow.add("ship_to_phone", wms_order.get("ShipToPhone", ""));
		upRow.add("ship_to_extension", wms_order.get("ShipToExtension", ""));
		upRow.add("ship_to_fax", wms_order.get("ShipToFax", ""));
		upRow.add("ship_to_store_no", wms_order.get("ShipToStoreNo", ""));
		upRow.add("lr_id", lr_id);
		upRow.add("customer_id", wms_order.get("CustomerID", ""));
		upRow.add("carrier_id", wms_order.get("CarrierID", ""));
		upRow.add("shipped_date",DateUtil.showUTCTime( wms_order.get("ShippedDate", ""), ps_id));
		upRow.add("master_bol_no", 0);
//		"INSERT INTO wms_load_order (wms_is_close,status,order_numbers,pallets,staging_area_id"
//		+ ",pro_no,po_no,reference_no,note,company_id,account_id,ship_to_id,ship_to_name,ship_to_address1"
//		+ ",ship_to_address2,ship_to_city,ship_to_state,ship_to_zip_code,ship_to_country,ship_to_phone"
//		+ ",ship_to_extension,ship_to_fax,ship_to_store_no,lr_id,customer_id,carrier_id,shipped_date) VALUES "
		
//		upRow.add("case_total", wms_order.get("orderNo", ""));
//		upRow.add("supplier_id", wms_order.get("orderNo", ""));
//		upRow.add("pallet_type_id", wms_order.get("orderNo", ""));
//		convetDBRow2InsertSql(upRow);
//		return convetDBRow2InsertSql(upRow);
		return floorCheckInMgrWfh.addWmsLoadOrder(upRow);
	}
	/**
	 * 将DBRo 转为insert的数据部分 （'123','234'...）,
	 * @param upRow
	 * @return
	 */
	private String convetDBRow2InsertSql(DBRow upRow){
		StringBuffer sql = new StringBuffer();
		ArrayList<String> fileName = upRow.getFieldNames();
		sql.append("(");
		for (String key : fileName) {
			sql.append("'").append(upRow.get(key, "")).append("',");
		}
		sql.substring(0, sql.length()-1);
		sql.append("),");
		return sql.toString();
	}
	/**
	 * 查询wms的orders 与 之下的pallets
	 * @param carrier_id
	 * @param ps_id
	 * @param lr_id
	 * @return
	 * @throws Exception
	 */
	private DBRow[] findWMSOrderAndPallet(long carrier_id,long ps_id) throws Exception {
		DBRow[] carrier_subs = floorCheckInMgrWfh.findSmallParcelCarrierSubByCarrierId(carrier_id);
		StringBuffer carrier_sub_name_sb = new StringBuffer();
		for (DBRow csub : carrier_subs) {
			carrier_sub_name_sb.append(csub.get("carrier_name","")).append(",");
		}
		String carrier_sub = carrier_sub_name_sb.toString();
		carrier_sub = carrier_sub.substring(0,carrier_sub.length()-1);
		String date = DateUtil.NowStr();
		date = DateUtil.showLocalTime(date, ps_id);//  showLocalparseDateTo24Hours(date, ps_id);
		date = date.substring(0,date.length()-9);
		
	//	if(carrier_id == 1){
//			date = "2015-04-01";
	//	}
		//查询wms的order
		DBRow[] companyRow = floorCheckInMgrWfh.findCompanyIdByPsId(ps_id);
		List<String> companys = new ArrayList<String>();
		for (DBRow co : companyRow) {
			companys.add(co.get("COMPANY_ID", ""));
		}
		DBRow[] wms_orders= sqlServerMgrZJ.findOrderByCompanyCustomerCarrierDate(companys.toArray(new String[0]), null, carrier_sub.split(","), date);
		return wms_orders;
	}
	/**
	 * 按照lrid查找orders与下面的pallet或者trackNO
	 * @param ps_id
	 * @param lr_id
	 * @return
	 * @throws Exception
	 */
	private DBRow[] findOrdersAndPallet(long ps_id, long carrier_id,int search_open)
			throws Exception {
		DBRow[] carrier_subs = floorCheckInMgrWfh.findSmallParcelCarrierSubByCarrierId(carrier_id);
		StringBuffer carrier_sub_name_sb = new StringBuffer();
		for (DBRow csub : carrier_subs) {
			carrier_sub_name_sb.append("'"+csub.get("carrier_name","")+"'").append(",");
		}
		String carrier_sub = carrier_sub_name_sb.toString();
		carrier_sub = carrier_sub.substring(1,carrier_sub.length()-2);
		
		//将utc转成loca时间 以防跨天的问题
		String date = DateUtil.showLocalTime(DateUtil.NowStr(), ps_id);;
		date = date.substring(0,date.length()-9);
		String startTime = date+" 00:00:00";
//		String endTime = date+" 23:59:59";
	//	if(carrier_id == 1){
//			startTime = "2015-4-1 00:00:00";
	//	}
		DBRow[] companyRow = floorCheckInMgrWfh.findCompanyIdByPsId(ps_id);
		String companys = "";
		for (DBRow co : companyRow) {
			companys += "'"+co.get("COMPANY_ID", "")+"',";
		}
		// companys =  '123','213',   sql写的是  in('"+companys+"')  去掉前一位 后两位
		companys = companys.substring(1,companys.length()-2);
		
		DBRow[] orders = floorCheckInMgrWfh.findWMSLoadOrderByCarrierSub(carrier_sub,companys,startTime,"",search_open);
		for (DBRow order : orders) {
			long wms_order_id = order.get("wms_order_id", 0L);
			DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPallet(wms_order_id);
			for (DBRow pallet : pallets) {
				String scan_time = pallet.get("scan_time", "");
				if(!StringUtil.isBlank(scan_time)){
					scan_time = DateUtil.showLocalparseDateTo24Hours(scan_time, ps_id);
				}
				pallet.add("scan_time", scan_time);
			}
			String shipped_date = order.get("shipped_date","");
			if(!StringUtil.isBlank(shipped_date)){
				order.add("shipped_date", DateUtil.showLocalparseDateTo24Hours(shipped_date, ps_id));
			}
			order.add("pallets", pallets);
		}
		return orders;
	}
	
	/**
	 * check in/out数据查询接口
	 * @param ctnr
	 * @param type
	 * @param time
	 * @param check_in_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getCheckMessage(String ctnr, int type, String time, String check_in_code) throws Exception {
		try{
			
			DBRow result = new DBRow();
			//验证是否code 是否正确 ctnr+time+oso.com =md5
			String code = time+"arrest_1!";
			Md5 md5 = new Md5();
			code = md5.getMD5ofStr(code).toLowerCase();
			if(!code.equals(check_in_code)){
				result.add("success", false);
				result.add("data", "Code is error!!");
				return result;
			}
			//查询需要的设备信息
			DBRow message = floorCheckInMgrWfh.getCheckInMessage(ctnr);
			long entry_id = message.get("check_in_entry_id", 0L);
			DBRow[] tractor = floorCheckInMgrWfh.findTractorByEntryId(entry_id, CheckInTractorOrTrailerTypeKey.TRACTOR);
			
			result.add("CTNR", ctnr);
			result.add("TYPE", type);
			result.add("TIME", time);
			result.add("LP", tractor[0].get("equipment_number", ""));
			result.add("MC_DOC", message.get("mc_dot", ""));
			result.add("CARRIER", message.get("company_name", ""));
			result.add("DRIVER_NAME", message.get("gate_driver_name", ""));
			result.add("DRIVER_LICENSE", message.get("gate_driver_liscense", ""));
			result.add("SEAL", message.get("seal_pick_up", ""));
			result.add("SPOT", message.get("yc_no", ""));
			result.add("SOPT_AREA", message.get("mc_dot", ""));
			return result;
		}catch (Exception e){
			throw new SystemException(e,"checkInMgrWfh.getCheckMessage",log);
		}
	};
	
	@Override
	public DBRow[] findNoSpaceTaskByEntryId(long entry_id, long equipment_id)
			throws Exception {
		try{
			DBRow[] noSpaceTasks = floorCheckInMgrWfh.findNoSpaceTask(entry_id, equipment_id);
			
			return noSpaceTasks;
		}catch (Exception e){
			throw new SystemException(e,"checkInMgrWfh.findNoSpaceTaskByEntryId",log);
		}
	}
	/**
	 * 在check out的时候查询的equipment不存在，则提示是否新建，新建一条并返回数据，
	 */
	@Override
	public DBRow creatNewEquipmentOnCheckOut(HttpServletRequest request) throws Exception {
		try{
			CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			//使用parto接口创建新的记录
			long entry_id = checkInMgrXj.createEntryId(request);
			//查询记录并返回
			DBRow[] equipments = floorCheckInMgrWfh.findTractorByEntryId(entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
			
			DBRow equipment = equipments[0]==null?new DBRow():equipments[0];
			String loaction = "";
			loaction += occupyTypeKey.getOccupyTypeKeyName(equipment.get("resources_type", 0));
			if(!StringUtil.isBlank( occupyTypeKey.getOccupyTypeKeyName(equipment.get("resources_type", 0))) && !StringUtil.isBlank(equipment.getString("resources_id_name")))
					loaction += ": ";
			loaction += equipment.getString("resources_id_name");
			//新建的肯定没有任务，
			equipment.add("is_disabled", "false");
			equipment.add("equipment_type_msg", checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(checkInTractorOrTrailerTypeKey.TRAILER));
			equipment.add("is_search", 0);
			equipment.add("location", loaction);
			return equipments[0]==null?new DBRow():equipment;
		}catch (Exception e){
			throw new SystemException(e,"checkInMgrWfh.creatNewEquipmentOnCheckOut",log);
		}
	}
	
	/**
	 * 查询仓库的名称
	 */
	@Override
	public DBRow findPSNameByPsId(long ps_id) throws Exception {
		try{
			return floorCheckInMgrWfh.findPsNameByPsId(ps_id);
		}catch (Exception e){
			throw new SystemException(e,"checkInMgrWfh.findPSNameByPsId",log);
		}
	}
	
	@Override
	public DBRow getCheckInIndexJson(HttpServletRequest request) throws Exception {
		try {
		    ModuleKey moduleKey = new ModuleKey();
		    CheckInMainDocumentsStatusTypeKey checkInMainDocumentsStatusTypeKey = new CheckInMainDocumentsStatusTypeKey();
			CheckInChildDocumentsStatusTypeKey checkInChildDocumentsStatusTypeKey = new CheckInChildDocumentsStatusTypeKey();
			CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
			OccupyStatusTypeKey occupyStatusTypeKey = new OccupyStatusTypeKey();
			CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
			CheckInMainDocumentsRelTypeKey checkInMainDocumentsRelTypeKey = new CheckInMainDocumentsRelTypeKey();
			CheckInLogTypeKey checkInLogTypeKey = new CheckInLogTypeKey();
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
	 		long psId=adminLoggerBean.getPs_id(); 
			//long mainStatus = StringUtil.getLong(request,"mainStatus");
			String mainStatus = StringUtil.getString(request,"mainStatus");
			//	System.out.println(mainStatus);
			//long tractorStatus = StringUtil.getLong(request,"tractorStatus");
			String tractorStatus = StringUtil.getString(request,"tractorStatus");
			String entryType = StringUtil.getString(request,"entryType");
			//long numberStatus = StringUtil.getLong(request,"numberStatus");
			String numberStatus = StringUtil.getString(request,"numberStatus");
			long loadBar = StringUtil.getLong(request,"loadBar");
			//long doorStatus = StringUtil.getLong(request,"doorStatus");
			String doorStatus = StringUtil.getString(request,"doorStatus");
			//System.out.println("mainStatus---"+mainStatus+"tractorStatus"+tractorStatus+"numberStatus---"+numberStatus+"doorStatuss---"+doorStatus);
			
			
			
			String CLOSED = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.CLOSE);
			String EXCEPTION = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.EXCEPTION);
			String PARTIALLY = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.PARTIALLY);
			String RELEASED = occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RELEASED);
			
			
			long ps_id = StringUtil.getLong(request,"ps_id");
			long main_id  = StringUtil.getLong(request,"main_id");
			PageCtrl pc = new PageCtrl();
			pc.setPageNo(StringUtil.getInt(request,"pageNo"));
			pc.setPageSize(StringUtil.getInt(request,"pageSize"));
//			DBRow[] rows = checkInMgrZwb.selectAllMain(main_id,pc);
			if(ps_id==0){
				ps_id=psId;
			}
			String start_time=StringUtil.getString(request, "start_time");
			String end_time=StringUtil.getString(request, "end_time");	
			//if ("".equals(start_time)&&"".equals(end_time)){
			//	start_time = "2014-07-03 00:00";
			//	end_time = "2014-07-03 23:59";
	 		//	start_time = DateUtil.FormatDatetime("yyyy-MM-dd HH:mm");//获取当前时间
	 		//	end_time = DateUtil.FormatDatetime("yyyy-MM-dd")+" 23:59";//获取当天结束时间
			//}	
			DBRow[] rows = new DBRow[0];//checkInMgrZwb.filterCheckIn(entryType,start_time,end_time,mainStatus,tractorStatus,numberStatus,doorStatus,ps_id,loadBar,0L, pc);
			String key = StringUtil.getString(request,"key");
			String cmd = StringUtil.getString(request,"cmd");
			int search_mode = StringUtil.getInt(request,"search_mode");
			if(cmd.equals("search"))
			{
					rows = checkInMgrZwb.searchCheckInByNumber(key,search_mode,pc,adminLoggerBean);
			}
			
			DBRow datas = new DBRow();
	/* 		List<String> title = new ArrayList<String>();
			title.add(0,"Trailer");
			
			title.add(1,"Tractor");
			
			title.add(2,"Time");
			
			title.add(3,"Log");
			
			title.add(4,"Options");
			
			datas.add("thead",title); */
			
			//**********************封装所有的Key
			DBRow _key = new DBRow();
			datas.add("keys", _key);
			DBRow kk = new DBRow();
			DBRow kkk = new DBRow();
			_key.add("checkInMainDocumentsStatusTypeKey", kkk);
			kk.add("key", checkInMainDocumentsStatusTypeKey.INYARD);
			kk.add("value", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.INYARD));
			kkk.add("INYARD", kk);
			
			kk = new DBRow();
			kk.add("key", checkInMainDocumentsStatusTypeKey.LEAVING);
			kk.add("value", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.LEAVING));
			kkk.add("LEAVING", kk);
			
			kk = new DBRow();
			kk.add("key", checkInMainDocumentsStatusTypeKey.LEFT);
			kk.add("value", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.LEFT));
			kkk.add("LEFT", kk);
			
			kk = new DBRow();
			kk.add("key", checkInMainDocumentsStatusTypeKey.PROCESSING);
			kk.add("value", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.PROCESSING));
			kkk.add("PROCESSING", kk);
			
			kk = new DBRow();
			kk.add("key", checkInMainDocumentsStatusTypeKey.UNPROCESS);
			kk.add("value", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(checkInMainDocumentsStatusTypeKey.UNPROCESS));
			kkk.add("UNPROCESS", kk);
			
			
			kk = new DBRow();
			kkk = new DBRow();
			_key.add("CheckInChildDocumentsStatusTypeKey", kkk);
			kk.add("key", CheckInChildDocumentsStatusTypeKey.CLOSE);
			kk.add("value", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.CLOSE));
			kkk.add("CLOSE", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInChildDocumentsStatusTypeKey.EXCEPTION);
			kk.add("value", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.EXCEPTION));
			kkk.add("EXCEPTION", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInChildDocumentsStatusTypeKey.PARTIALLY);
			kk.add("value", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.PARTIALLY));
			kkk.add("PARTIALLY", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInChildDocumentsStatusTypeKey.PROCESSING);
			kk.add("value", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.PROCESSING));
			kkk.add("PROCESSING", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInChildDocumentsStatusTypeKey.UNPROCESS);
			kk.add("value", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(CheckInChildDocumentsStatusTypeKey.UNPROCESS));
			kkk.add("UNPROCESS", kk);
			
			
			
			
			
			kk = new DBRow();
			kkk = new DBRow();
			_key.add("CheckInMainDocumentsRelTypeKey", kkk);
			kk.add("key", CheckInMainDocumentsRelTypeKey.BOTH);
			kk.add("value", checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.BOTH));
			kkk.add("BOTH", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInMainDocumentsRelTypeKey.DELIVERY);
			kk.add("value", checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.DELIVERY));
			kkk.add("DELIVERY", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInMainDocumentsRelTypeKey.NONE);
			kk.add("value", checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.NONE));
			kkk.add("NONE", kk);
			
			kk = new DBRow();
			kk.add("key", CheckInMainDocumentsRelTypeKey.PICK_UP);
			kk.add("value", checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(CheckInMainDocumentsRelTypeKey.PICK_UP));
			kkk.add("PICK_UP", kk);
			
			
			
			
			
			kk = new DBRow();
			kkk = new DBRow();
			_key.add("CheckInDoorStatusTypeKey", kkk);
			kk.add("key", OccupyStatusTypeKey.OCUPIED);
			kk.add("value", occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.OCUPIED));
			kkk.add("OCUPIED", kk);
			
			kk = new DBRow();
			kk.add("key", OccupyStatusTypeKey.RELEASED);
			kk.add("value",occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RELEASED));
			kkk.add("RELEASED", kk);
			
			kk = new DBRow();
			kk.add("key", OccupyStatusTypeKey.RESERVERED);
			kk.add("value",occupyStatusTypeKey.getOccupyStatusValue(OccupyStatusTypeKey.RESERVERED));
			kkk.add("RESERVERED", kk);
			
			
			if(rows != null && rows.length > 0){
				int _i = 1;
				List mainRow = new ArrayList();
	   			datas.add("data",mainRow);			//总的数据加进去 只需要加这一次
	      		for(DBRow row : rows){
					DBRow one = new DBRow();	//第一列
	      			DBRow _row = new DBRow();	//行
	      			
	      			mainRow.add(_row);	
	      			_row.add("trailer", one);	//第一列的数据加进去
	      			_row.add("zone_id", row.getString("zone_id",""));	
	      			_row.add("FileWithType", FileWithTypeKey.OCCUPANCY_MAIN);	
	      			_row.add("load_count", row.get("load_count",0l));	
	      			_row.add("rel_type", row.get("rel_type",0l));	
	      			_row.add("in_seal", row.getString("in_seal",""));	
	      			_row.add("out_seal",  row.getString("out_seal",""));	
	      			_row.add("tractor_status",  row.getString("tractor_status"));	
	      			_row.add("status",  row.getString("status"));	
//	      			//system.out.println(row.getString("zone_id",""));
	      			
	      			
	      			_row.add("dlo_id",row.getString("dlo_id"));
	      			
					one.add("E_dlo_id", "E"+row.getString("dlo_id"));
	      			one.add("tractor_status", row.get("tractor_status",0));
	      			
	      			if(!row.getString("gate_container_no").equals("")){
		  				one.add("container_no", checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(CheckInTractorOrTrailerTypeKey.TRAILER)+":"+row.getString("gate_container_no"));
		  				
		  			}
					if(row.get("status",0)!=0 && row.get("status",0)!=11 && !row.getString("gate_container_no").equals("") ){
	  					if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){
	  						one.add("entryType", "Delivery:"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("status",0)));
	  					}else{
	  						one.add("entryType",checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(row.get("rel_type",0))+":"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("status",0)));
	  					}
	  				}
	  				one.add("isLiveKey",checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(row.get("isLive",0)));
	  				DBRow[] details = checkInMgrZwb.selectDetailsInfo(0,row.get("dlo_id",01));
	  				one.add("details",details);
		  			if(details.length > 0 ){
		  				for(int i=0;i< details.length ;i++){
		  					if(!details[i].getString("doorId").equals("")){
			  					DBRow[] numbers = checkInMgrZwb.selectDetailsInfo(details[i].get("rl_id",01),row.get("dlo_id",01));
			  					details[i].add("numbers",numbers);
			  					if(details[i].get("occupancy_status",01)==OccupyStatusTypeKey.RELEASED){
			  						details[i].add("doorStatusType","Resleased");
			  					}else{
			  						boolean numflag = false;
					  				boolean doorflag = false;
					  				if(numbers != null && numbers.length > 0 ){
					  					for(int j=0;j< numbers.length ;j++){
					  						if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY ){
					  						 	numflag = true;
					  						}
					  						if(numbers[j].get("occupancy_status",01)==OccupyStatusTypeKey.OCUPIED){
					  							doorflag=true;
					  						}
					  					}
					  				}
					  				if(doorflag==false){
					  					details[i].add("doorStatusType","Reserved");
									}else{
										details[i].add("doorStatusType","Occupied");
								 	}
						  			if(numflag==false){
						  				DBRow input = new DBRow();
						  				input.add("type", "button");
						  				input.add("name", "release");
						  				input.add("value", "Release");
						  				input.add("para", "3|"+details[i].getString("rl_id")+"|"+details[i].getString("dlo_id"));
						  				input.add("callBack","");
						  				details[i].add("release", input);
						  			}else{
						  				DBRow input = new DBRow();
						  				details[i].add("release", input);
						  			}
			  					}
			  					
			  					if(numbers != null && numbers.length > 0 ){
						      		for(int j=0;j< numbers.length ;j++){
								   		if(!numbers[j].getString("number").equals("")){
								   			
								   			numbers[j].add("numberType", moduleKey.getModuleName(numbers[j].get("number_type", "")));
								   			
											if(numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.CLOSE && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.EXCEPTION && numbers[j].get("number_status",01)!=CheckInChildDocumentsStatusTypeKey.PARTIALLY){
						  						List<DBRow> select = new ArrayList<DBRow>();
						  						DBRow input = new DBRow();
						  						input.add("type", "option");
						  						 input.add("name", "close");
						  						input.add("value", "Close");
						  						input.add("para",row.getString("dlo_id")+"|"+numbers[j].getString("dlo_detail_id")+"|"+CheckInChildDocumentsStatusTypeKey.CLOSE+"|"+numbers[j].getString("rl_id")+"|"+row.getString("rel_type")+"|"+row.get("isLive",0));
						  						input.add("callBack","");
						  						select.add(0, input);
						  						
						  						input = new DBRow();
						  						input.add("type", "option");
						  						input.add("name", "exception");
						  						input.add("value", "Exception");
						  						input.add("para",row.getString("dlo_id")+"|"+numbers[j].getString("dlo_detail_id")+"|"+CheckInChildDocumentsStatusTypeKey.EXCEPTION+"|"+numbers[j].getString("rl_id")+"|"+row.getString("rel_type")+"|"+row.get("isLive",0));
						  						input.add("callBack","");
						  						select.add(1, input); 
						  						
						  						if(numbers[j].get("number_type", 0l)==10 || numbers[j].get("number_type", 0l)==11 || numbers[j].get("number_type", 0l)==17 || numbers[j].get("number_type", 0l)==18){
							  						input = new DBRow();
							  						input.add("type", "option");
							  						input.add("name", "partially");     
							  						input.add("value", "Partially");
							  						input.add("para",row.getString("dlo_id")+"|"+numbers[j].getString("dlo_detail_id")+"|"+CheckInChildDocumentsStatusTypeKey.PARTIALLY+"|"+numbers[j].getString("rl_id")+"|"+row.getString("rel_type")+"|"+row.get("isLive",0));
							  						input.add("callBack","");
							  						select.add(2, input);
						  						}
						  						numbers[j].add("select", select);
						  						numbers[j].add("doucmentStatus", checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(numbers[j].get("number_status",01)));
						      				}
											if(numbers[j].get("number_status",01)==CheckInChildDocumentsStatusTypeKey.CLOSE ){
												numbers[j].add("doucmentStatus", CLOSED);
								      		}else if( numbers[j].get("number_status",01)==CheckInChildDocumentsStatusTypeKey.EXCEPTION){
								      				numbers[j].add("doucmentStatus", EXCEPTION);
								      		}else if( numbers[j].get("number_status",01)==CheckInChildDocumentsStatusTypeKey.PARTIALLY){
								 	   		 		numbers[j].add("doucmentStatus", PARTIALLY);
								      		}
						 	   		 	}
								   	}
								}
		  					}else{
		  						DBRow[] gateNumber = checkInMgrZwb.findAllLoadByMainId(row.get("dlo_id",01));
		  						List list = new ArrayList();
		  						list.add(gateNumber[0]);
		  						one.add("details", list);
		  						if(!gateNumber[0].getString("number").equals("")){
		  			  			  	DBRow[] rr = {new DBRow()};
		  							rr[0].add("doucmentStatus","");
		  							rr[0].add("number",gateNumber[0].getString("number"));
		  							rr[0].add("numberType",moduleKey.getModuleName(gateNumber[0].get("number_type", "")));
		  							gateNumber[0].add("numbers",rr);
		  						}
		  					}
		  				}
		  			}//details  end
	  				
	  				DBRow two = new DBRow(); //第二列
	  				_row.add("tractor", two);
	  				if(!row.getString("gate_liscense_plate").equals("")){
	  					two.add("tractor","Tractor:"+row.getString("gate_liscense_plate"));
	  				}
	  				if(row.get("tractor_status",0)!=0 && row.get("tractor_status",0)!=11){
		  				if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){
		  					two.add("delivery","Delivery:"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("tractor_status",0)));
		  				}else{
		  					two.add("delivery",checkInMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(row.get("rel_type",0))+":"+checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("tractor_status",0)));
		  				}
		  			}
	  				List<DBRow> chilDren = new ArrayList<DBRow>();
	  				DBRow cell = new DBRow();
	  				two.add("children", chilDren);
	  				chilDren.add(0, cell);
	  				cell.add("lable", "Driver License");
	  				cell.add("value", row.getString("gate_driver_liscense"));
	  				
	  				cell = new DBRow();
	  				chilDren.add(1, cell);
	  				cell.add("lable", "Driver Name");
	  				cell.add("value", row.getString("gate_driver_name"));

	  				cell = new DBRow();
	  				chilDren.add(2, cell);
	  				cell.add("lable", "Carrier");
	  				cell.add("value", row.getString("company_name"));

	  				cell = new DBRow();
	  				chilDren.add(3, cell);
	  				cell.add("lable", "GPS");
	  				cell.add("value", row.getString("imei"));
	  				
	  				DBRow input = new DBRow();
					input.add("type", "hidden");
					input.add("name", "initgpsId");
					input.add("value",row.getString("id"));
					input.add("callBack","");
					chilDren.add(4, input);
	  				
					cell = new DBRow();
	  				chilDren.add(5, cell);
	  				cell.add("lable", "Spot");
	  				cell.add("value", row.getString("yc_no"));
	  				
					cell = new DBRow();
	  				chilDren.add(6, cell);
	  				cell.add("lable", "MC/DOT");
	  				cell.add("value", row.getString("mc_dot"));
	  				
					cell = new DBRow();
	  				chilDren.add(7, cell);
	  				cell.add("lable", "Exiting CTNR");
	  				cell.add("value", row.getString("search_gate_container_no"));
					
					cell = new DBRow();
	  				chilDren.add(8, cell);
	  				cell.add("lable", "Delivery Seal");
	  				cell.add("value", row.getString("in_seal"));
	  				
					cell = new DBRow();
	  				chilDren.add(9, cell);
	  				cell.add("lable", "PickUp Seal");
	  				cell.add("value", row.getString("out_seal"));
	  				
	  				if(!row.getString("load_bar_id").equals("")){
	  					cell = new DBRow();
	  	  				chilDren.add(10, cell);
	  	  				cell.add("lable", "Load Bar");
	  	  				cell.add("value", row.getString("load_bar_id"));
	  				}else{
	  					cell = new DBRow();
	  	  				chilDren.add(10, cell);
	  	  				cell.add("lable", "Load Bar");
	  	  				cell.add("value", "NA");
	  				}
	  					
	  				DBRow three = new DBRow();//第三列
	  				_row.add("time", three);
	  				
	  				three.add("WHSE","WHSE:"+row.get("title", ""));
					String appointment_time = row.getString("appointment_time");
	  				String gate_check_in_operate_time = row.getString("gate_check_in_operate_time");
	  				String window_check_in_operate_time = row.getString("window_check_in_operate_time");
	  				String warehouse_check_in_operate_time = row.getString("warehouse_check_in_operate_time");
	  				String check_out_time = row.getString("check_out_time");       
	  				if(!appointment_time.equals("")){
	  					three.add("Appointment",appointment_time.substring(0,appointment_time.length()-5));
	  				}
	  				if(!gate_check_in_operate_time.equals("")){
	  					three.add("GateCheckIn",gate_check_in_operate_time.substring(0,gate_check_in_operate_time.length()-5));
	  				}
	  				DBRow[] windowResult = checkInMgrZwb.findAllLoadByMainId(row.get("dlo_id",0));
	  				three.add("window_check_in_operate_time", window_check_in_operate_time);
	  				three.add("warehouse_check_in_operate_time", window_check_in_operate_time);
	  				
	  				if(!window_check_in_operate_time.equals("")){
	  					
	  					three.add("windowResult",windowResult);
	  					three.add("window_check_in_operate_time", window_check_in_operate_time.substring(0,window_check_in_operate_time.length()-5));
						if(windowResult!=null&&windowResult.length>0){
		  					for(int i=0;i<windowResult.length;i++){
		  						
								DBRow[] window_notices_load = new DBRow[0];//checkInMgrZwb.finn(1,1,windowResult[i].get("dlo_detail_id",0),ProcessKey.CHECK_IN_WINDOW);
								
								windowResult[i].add("window_notices_load",window_notices_load);
								if(window_notices_load!=null&&window_notices_load.length>0){
							 		for(int a=0;a<window_notices_load.length;a++){					 				
									 	window_notices_load[a].add("Notified",window_notices_load[a].getString("employe_name"));
									 	DBRow detail = new DBRow();
									 	detail.add("type", "href");
									 	detail.add("name", "detail");
									 	detail.add("value", "Detail");
									 	detail.add("para", "windows"+window_notices_load[a].getString("employe_name")+windowResult[i].get("dlo_detail_id",0));
									 	
									 	window_notices_load[a].add("Detail",detail);
									}
							 		if(window_notices_load.length>2){
							 			DBRow more = new DBRow();
									 	more.add("type", "href");
									 	more.add("name", "more");
									 	more.add("value", "More...");
									 	more.add("para", row.get("dlo_id",0));
									 	
									 	windowResult[i].add("more",more);
							 		}
							 	}
		  					}
		  				}
	  				}
	  				
	   				if(!warehouse_check_in_operate_time.equals("")){
	  					three.add("warehouse_check_in_operate_time", warehouse_check_in_operate_time.substring(0,warehouse_check_in_operate_time.length()-5));
	  					
	  					if(windowResult!=null&&windowResult.length>0){
	  						for(int i=0;i<windowResult.length;i++){
	  							windowResult[i].add("WHSECheckIn","WHSECheckIn:"+warehouse_check_in_operate_time.substring(0,warehouse_check_in_operate_time.length()-5));
						 		DBRow[] warehouse_notices_load = new DBRow[0];// checkInMgrZwb.findWindowNotices(windowResult[i].get("dlo_detail_id",0),-1,ProcessKey.CHECK_IN_WAREHOUSE);
						 		windowResult[i].add("warehouse_notices_load", warehouse_notices_load);
						 		if(warehouse_notices_load!=null&&warehouse_notices_load.length>0){
					 				for(int a=0;a<warehouse_notices_load.length;a++){
						 				warehouse_notices_load[a].add("Notified",warehouse_notices_load[a].getString("employe_name"));
						 				DBRow detail = new DBRow();
								 		detail.add("type", "href");
								 		detail.add("name", "detail");
								 		detail.add("value", "Detail");
								 		detail.add("para", "warehouse"+warehouse_notices_load[a].getString("employe_name")+windowResult[i].get("dlo_detail_id",0));
								 		warehouse_notices_load[a].add("Detail",detail);
						 							
					 				}
					 				if(warehouse_notices_load.length>2){
						 				DBRow more = new DBRow();
									 	more.add("type", "href");
									 	more.add("name", "more");
									 	more.add("value", "More...");
									 	more.add("para", row.get("dlo_id",0));
									 	
									 	windowResult[i].add("more",more);
						 			}
						 		}
	  						 }
	  					}
	  				}
	  				if(!check_out_time.equals("")){
					 	three.add("GateCheckOut", check_out_time.substring(0,check_out_time.length()-5));
					} 
					
	  				DBRow[] checkInLog = checkInMgrZwb.findCheckInLog(row.get("dlo_id",0l)); //第四列
	  				DBRow _log = new DBRow();
	  				_log.add("log", checkInLog);
	  				
	  				_row.add("checkInLog", _log);			
	  				if(checkInLog.length>0 && checkInLog!=null){
		  				for(int mm=0;mm<checkInLog.length;mm++){
		  					
			  					DBRow log_type = new DBRow();
							 	log_type.add("type", "href");
							 	log_type.add("name", "log_type");
							 	log_type.add("value",checkInLogTypeKey.getCheckInLogTypeKeyValue(checkInLog[mm].getString("log_type")));
							 	log_type.add("para", row.get("dlo_id",0));
							 	
							 	checkInLog[mm].add("log_type",log_type);
		  						checkInLog[mm].add("operator_time", checkInLog[mm].getString("operator_time").substring(0,checkInLog[mm].getString("operator_time").length()-5));
		  						if(mm==2){
			  						break;
			  					}
		  				}
		  				if(checkInLog.length>3){
		  					DBRow more = new DBRow();
						 	more.add("type", "href");
						 	more.add("name", "more");
						 	more.add("value", "More...");
						 	more.add("para", row.getString("dlo_id"));
						 	
						 	_log.add("more",more);
		  					
		  				}
	  				}
	  				
	  				List<DBRow> five = new ArrayList<DBRow>();  //第五列
	  				_row.add("options",five);
	  				DBRow button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "edit");
				 	button.add("value", "Edit");
				 	button.add("para", row.getString("dlo_id")+"|"+row.getString("status"));
				 	
				 	five.add(0,button);
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "print");
				 	button.add("value", "Print");
				 	button.add("para", row.getString("dlo_id"));
				 	five.add(1,button);
				 	
/*				 	List<DBRow> bottom_row = new ArrayList<DBRow>();
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "photos");
				 	button.add("value", "Photos");
				 	button.add("para", FileWithTypeKey.OCCUPANCY_MAIN+"|"+row.getString("dlo_id"));
				 	bottom_row.add(button);
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "windowCheckIn");
				 	button.add("value", "WindowCheckIn");
				 	button.add("para", row.getString("dlo_id")+"|"+row.getString("zone_id")+"|"+row.getString("status")+"|"+row.getString("isLive"));
				 	bottom_row.add(button);
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "windowCheckOut");
				 	button.add("value", "WindowCheckOut");
				 	button.add("para", row.getString("dlo_id")+"|"+row.getString("status")+"|"+row.getString("isLive"));
				 	bottom_row.add(button);
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "warehouseCheckIn");
				 	button.add("value", "WarehouseCheckIn");
				 	button.add("para", row.getString("dlo_id")+"|"+row.getString("status")+"|"+(!row.getString("in_seal").equals("")?row.getString("in_seal"):"")+"|"+(!row.getString("out_seal").equals("")?row.getString("out_seal"):"")+"|"+row.get("load_count",0l)+"|"+row.get("rel_type",0l));
				 	bottom_row.add(button);
				 	
				 	
				 	button = new DBRow();
				 	button.add("type", "button");
				 	button.add("name", "gateCheckOut");
				 	button.add("value", "GateCheckOut");
				 	button.add("para", row.getString("dlo_id")+"|"+row.getString("tractor_status"));
				 	bottom_row.add(button);
				 	
					_row.add("tfoot", bottom_row);*/
				 	_i++;		
	      		}
	      		
	      	}



			datas.add("pageCtrl", pc);
			return datas;
		}catch (Exception e){
			throw new SystemException(e,"getCheckInIndexJson",log);
		}
	}
	
	@Override
	public void assignSchedule(HttpServletRequest request) throws Exception {
		try{
			String assignTo = StringUtil.getString(request, "assignTo");
			DBRow[] assignToRow = floorScheduleMgrZr.getAdminUserByAdid(assignTo+"");
			long adid = StringUtil.getLong(request, "adid");
			String scheduleJson = StringUtil.getString(request, "schedule");
			DBRow schedule = new DBRow();
			JSONObject jo = new JSONObject(scheduleJson);
			schedule.add("schedule_id", jo.get("schedule_id"));
			schedule.add("associate_type", jo.get("associate_type"));
			schedule.add("associate_process", jo.get("associate_process"));
			schedule.add("associate_id", jo.get("associate_id"));
			schedule.add("schedule_overview", jo.get("schedule_overview"));
			schedule.add("associate_main_id", jo.get("schedule_id"));
			schedule.add("schedule_detail", jo.get("schedule_detail"));
			sysNotifyZr.handleScheduleAddShortMessage(assignToRow, adid, schedule);
		}catch(Exception e){
			throw new SystemException(e,"assignSchedule",log);
		}
	}
	
	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}

	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}

	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}

	public void setWmsLoadMgrZr(WmsLoadMgrZrIface wmsLoadMgrZr) {
		this.wmsLoadMgrZr = wmsLoadMgrZr;
	}

	public void setFloorCheckInMgrZyj(FloorCheckInMgrZyj floorCheckInMgrZyj) {
		this.floorCheckInMgrZyj = floorCheckInMgrZyj;
	}

	public void setFloorScheduleMgrZr(FloorScheduleMgrZr floorScheduleMgrZr) {
		this.floorScheduleMgrZr = floorScheduleMgrZr;
	}
	
	public void setSysNotifyZr(SysNotifyIfaceZr sysNotifyZr) {
		this.sysNotifyZr = sysNotifyZr;
	}
	
	public void setSqlServerMgrZJ(SQLServerMgrZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}


	public void setCheckInMgrXj(CheckInMgrIfaceXj checkInMgrXj) {
		this.checkInMgrXj = checkInMgrXj;
	}
	
}

