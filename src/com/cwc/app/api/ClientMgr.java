package com.cwc.app.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.client.ClientIsExistException;
import com.cwc.app.exception.client.MarkOrderIsExistException;
import com.cwc.app.exception.client.NotDirectPayException;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.floor.api.FloorClientMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.iface.ClientMgrIFace;
import com.cwc.app.lucene.ClientIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class ClientMgr implements ClientMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorClientMgr fcm;
	private FloorOrderMgr fom ;
	
	public void addClients(HttpServletRequest request)
		throws ClientIsExistException,Exception
	{
		try
		{
			String email = StringUtil.getString(request,"email");
			String summary = StringUtil.getString(request,"summary");
			String post_date = DateUtil.NowStr();
			
			int clients_service = StringUtil.getInt(request,"clients_service");
			String note = StringUtil.getString(request, "note");
			
			AdminMgr am = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminInfo = am.getAdminLoginBean(request.getSession(true));
			
			if (fcm.getDetailClientByEmail(email)==null)
			{
				DBRow client = new DBRow();
				
				client.add("email",email);
				client.add("summary",summary);
				client.add("mod_date",post_date);
				
				long cid = fcm.addClients(client);
				ClientIndexMgr.getInstance().addIndexAsyn( String.valueOf(cid), email, summary,post_date);
				
				DBRow client_trace = new DBRow();
				
				client_trace.add("cid",cid);
				client_trace.add("post_date",post_date);
				client_trace.add("account",adminInfo.getAccount());		
				client_trace.add("clients_service",clients_service);
				client_trace.add("note",note+email);
		
				long ctid = fcm.addClientTrace(client_trace);
			}
			else
			{
				//客户已经存在
				throw new ClientIsExistException();
			}
	
		} 
		catch (ClientIsExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addClients",log);
		}
	}
	
	
	public DBRow[] getClients(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClients(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClients",log);
		}
	}
		

	
	public DBRow[] getSearchClients(String value,PageCtrl pc)
		throws Exception
	{
		try
		{
			String fields[] = new String[]{"cid","email"};
			
			DBRow row[] = ClientIndexMgr.getInstance().getSearchResults( value, pc);
			return(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchClients",log);
		}
	}
	
	public void modClient(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cid = StringUtil.getLong(request,"cid");
			String summary = StringUtil.getString(request, "summary");
			
			DBRow client = new DBRow();
			client.add("summary", summary);
			
			fcm.modClient(cid,client);
			
			DBRow detail = fcm.getDetailClientByCid(cid);
			ClientIndexMgr.getInstance().updateIndexAsyn( cid,detail.getString("email"),detail.getString("summary"),detail.getString("mod_date"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modClient",log);
		}
	}
	
	
	public DBRow[] getClientTraceByCount(long cid,int c)
		throws Exception
	{
		try
		{
			return(fcm.getClientTraceByCount(cid,c));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientTraceByCount",log);
		}
	}
	
	public void addClientTrace(HttpServletRequest request)
		throws NotDirectPayException,OrderNotExistException,MarkOrderIsExistException,Exception
	{
		try
		{
			long cid = StringUtil.getLong(request,"cid");
			String note = StringUtil.getString(request,"note");
			String post_date = DateUtil.NowStr();
			int clients_service = StringUtil.getInt(request,"clients_service");
			
			AdminMgr am = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
			
			long oid = StringUtil.getLong(request,"oid");
			double mc_gross = 0; 
			
			DBRow client_trace = new DBRow();
	
			client_trace.add("cid",cid);
			client_trace.add("post_date",post_date);
			client_trace.add("account",adminInfo.getAccount());		
			client_trace.add("oid",oid);
			client_trace.add("mc_gross",mc_gross);
			client_trace.add("clients_service",clients_service);
			client_trace.add("note",note);
	
			//成单
			if (clients_service == ClientServiceKey.Doneorder)
			{
				//检测订单是否存在
				//另外还需要检查订单状态是否正常  ----- 需要补充？
				DBRow detailorder = fom.getDetailPOrderByOid(oid);
				if (detailorder==null)
				{
					throw new OrderNotExistException();
				}
				
				//验证订单号是否已经被使用(以为之前已经有人重复录入了OID，所以这里就不适用DETAIL模式了，以免出错)
				DBRow cTraces[] = fcm.getClientsTraceByOid(oid);
				if (cTraces.length>0)
				{
					DBRow result = new DBRow();
					result.add("account",cTraces[0].getString("account"));
					result.add("oid",String.valueOf(oid));

					throw new MarkOrderIsExistException(result);
				}
				
				//检查订单是否为DIRECTPAY
//				if ( detailorder.getString("order_source").toLowerCase().equals("directpay")==false&&detailorder.getString("order_source").toLowerCase().equals("manual")==false )
//				{
//					throw new NotDirectPayException();
//				}
				
				mc_gross = detailorder.get("mc_gross", 0d);
				client_trace.add("note",note+String.valueOf(mc_gross));//把订单金额追加到备注
				client_trace.add("mc_gross",mc_gross);
			}
	
			fcm.addClientTrace(client_trace);
			
			//更新顾客信息主表信息更新时间
			DBRow client = new DBRow();
			client.add("mod_date",DateUtil.NowStr());
			fcm.modClient(cid, client);
		} 
		catch (OrderNotExistException e) 
		{
			throw e;
		}
		catch (MarkOrderIsExistException e) 
		{
			throw e;
		}
		catch (NotDirectPayException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addClientTrace",log);
		}
	}

	public int getClientTraceCountByCid(long cid)
		throws Exception
	{
		try
		{
			return(fcm.getClientTraceCountByCid(cid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientTraceCountByCid",log);
		}
	}
	
	public DBRow[] getClientTracesByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientTracesByCid(cid,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientTracesByCid",log);
		}
	}
	
	public DBRow getDetailClientByCid(long cid)
		throws Exception
	{
		try 
		{
			return(fcm.getDetailClientByCid(cid));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailClientByCid",log);
		}		
	}
	
	public DBRow[] getClientsSortBySumMcGross(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientsSortBySumMcGross( st, en, account,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsSortBySumMcGross",log);
		}
	}
	
	public DBRow[] getClientsSortBySumMcGrossDown(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientsSortBySumMcGrossDown( st, en, account,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsSortBySumMcGrossDown",log);
		}
	}
	
	public DBRow[] getClientsSortByModDate(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientsSortByModDate( st, en, account,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsSortByModDate",log);
		}
	}
	
	public DBRow[] getClientsSortByModDateDown(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientsSortByModDateDown( st, en, account,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsSortByModDateDown",log);
		}
	}
	
	public DBRow[] getClientsByCidRange(long st_cid,long en_cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getClientsByCidRange( st_cid, en_cid, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClients",log);
		}
	}
	
	public void addAsk(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String email = StringUtil.getString(request, "email");
			String ask_type = StringUtil.getString(request, "ask_type");
			long cid = 0;
			String post_date = DateUtil.NowStr();
			int clients_service = StringUtil.getInt(request,"clients_service");
			
			AdminMgr am = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
			
			DBRow detailClient = fcm.getDetailClientByEmail(email);
			
			//先判断邮件地址是否存在，如果不存在，则创建新客户
			if (detailClient==null)
			{
				DBRow client = new DBRow();
				
				client.add("email",email);
				client.add("mod_date",post_date);
				
				cid = fcm.addClients(client);
				
				ClientIndexMgr.getInstance().addIndexAsyn(String.valueOf(cid),email,"",post_date);
			}
			else
			{
				cid = detailClient.get("cid",0l);
			}
	
			
			DBRow client_trace = new DBRow();
	
			client_trace.add("cid",cid);
			client_trace.add("post_date",post_date);
			client_trace.add("account",adminInfo.getAccount());		
			client_trace.add("clients_service",clients_service);
			client_trace.add("note",ask_type);
	
			fcm.addClientTrace(client_trace);
			
			if (detailClient==null)
			{
				//更新顾客信息主表信息更新时间
				DBRow client = new DBRow();
				client.add("mod_date",DateUtil.NowStr());
				fcm.modClient(cid, client);				
			}
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addAsk",log);
		}
	}
	
	public DBRow[] getSearchServiceByFilter(String inService[],String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			//long stt = System.currentTimeMillis();
			DBRow rows[] = fcm.getSearchServiceByFilter(inService, st, en, account, pc);
			//long ent = System.currentTimeMillis();
			////system.out.println("getSearchServiceByFilter time:"+(ent-stt));
			
			return(rows);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchServiceByFilter",log);
		}
	}

	
	public int getSearchClientResultCount(HttpServletRequest request)
		throws Exception
	{
		try
		{
			PageCtrl pc = new PageCtrl();
			pc.setPageNo(1);
			pc.setPageSize(1);
			
			String search_key = StringUtil.getString(request, "search_key");
			
			DBRow result[] = this.getSearchClients(search_key, pc);
			
			return(result.length);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchClientResultCount",log);
		}
	}
	
	public void delClientByCid(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cid = StringUtil.getLong(request,"cid");		
			
			fcm.delClientByCid(cid);
			fcm.delClientTraceByCid(cid);
			
			ClientIndexMgr.getInstance().deleteIndexAsyn(cid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delClientByCid",log);
		}
	}
	
	
	public DBRow getClientsServiceStat(String st,String en,String account,int clients_service)
		throws Exception
	{
		try
		{
			DBRow serviceStat[] = fcm.getClientsServiceStat( st, en, account, clients_service);
			
			DBRow result = new DBRow();
			
			for (int i=0; i<serviceStat.length; i++)
			{
				result.add(serviceStat[i].getString("post_date").substring(0,10),serviceStat[i].get("c",0));
			}
			
			return(result);				
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsServiceStat",log);
		}
	}
	
	public DBRow[] getClientsServiceStatDate(String st,String en,String account)
		throws Exception
	{
		try
		{
			return(fcm.getClientsServiceStatDate( st, en,account));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsServiceStatDate",log);
		}
	}
	
	public double getClientsServiceOrderMcGrossStat(String date,String account)
		throws Exception
	{
		try
		{
			return(fcm.getClientsServiceOrderMcGrossStat(date,account));				
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getClientsServiceOrderMcGrossStat",log);
		}
	}

	public void setFcm(FloorClientMgr fcm)
	{
		this.fcm = fcm;
	}

	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}
}
