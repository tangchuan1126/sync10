package com.cwc.app.api.sbb;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.iface.sbb.DynamincDataAjaxMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

/**
 * 新框架实施后废除此类
 * 
 * */
public class DynamincDataAjaxMgrSbb implements DynamincDataAjaxMgrIFaceSbb{

	static Logger log = Logger.getLogger("ACTION");

	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;
	
	
	public DBRow[] getAreaByPsId(HttpServletRequest request) throws Exception{
		
		return floorFolderInfoMgrWp.getAreaByPsId(StringUtil.getString(request,"psId"));
	}
	
	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}
}
