package com.cwc.app.api.sbb;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.floor.api.sbb.FloorRoleMgrSbb;
import com.cwc.app.iface.sbb.RoleMgrIFaceSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;

public class RoleMgrSbb implements RoleMgrIFaceSbb{

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorRoleMgrSbb floorRoleMgr;
	private FloorAccountMgrSbb floorAccountMgr;

	public void setFloorRoleMgr(FloorRoleMgrSbb floorRoleMgr) {
		this.floorRoleMgr = floorRoleMgr;
	}
	
	public void setFloorAccountMgr(FloorAccountMgrSbb floorAccountMgr) {
		this.floorAccountMgr = floorAccountMgr;
	}
	
	/**
	 * 角色管理 >> 获取角色列表
	 * @param 分页
	 * @return 角色列表
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getSearchRoleList(Map<String,Object> parameter) throws Exception{
		
		try {
			
			DBRow[] roleList = floorRoleMgr.getSearchRoleList(parameter);
			
			return roleList;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getAllRoleList(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加角色
	 * @param 角色信息
	 * @return 角色信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow addRole(DBRow parameter) throws Exception{
		
		try {
			
			DBRow result = new DBRow();
			
			boolean validate = true;
			//验证
			DBRow roleRow = floorRoleMgr.getRoleNameByRoleName(parameter.getString("NAME"));
			
			if(roleRow != null){
				
				validate = false;
				result.add("error", "Role already exists");
			}
			
			if(validate){
				
				long roleId = floorRoleMgr.insertRole(parameter);
				result.add("id", roleId);
			}
			
			result.add("success",validate);
				
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.addRole(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 修改角色
	 * @param 角色信息
	 * @return 角色系你想
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow modifyRole(DBRow parameter) throws Exception{
		
		try {
			
			//验证
			DBRow result = new DBRow();
			
			boolean validate = true;
			//验证
			DBRow[] roleRow = floorRoleMgr.getRoleByRoleNameBesideSelf(parameter.get("ADGID", -1),parameter.getString("NAME"));
			
			if(roleRow != null && roleRow.length > 0){
				
				validate = false;
				result.add("error", "Role already exists");
			}
			
			if(validate){
				
				floorRoleMgr.updateRole(parameter);
			}
			
			result.add("success",validate);
				
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.modifyRole(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 删除角色
	 * @param 角色ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public Map<String,String> dropRole(long id) throws Exception{
		
		try {
			Map<String,String> result = new HashMap<String,String>();
			
			//验证角色是否可以删除
			if(floorRoleMgr.existDeptForAccount(id)){
				
				result.put("success", "false");
				result.put("error", "Cann't delete, Users with this role.");
				
			}else{
				
				//删除角色
				floorRoleMgr.deleteRole(id);
				
				//删除角色对应权限
				floorRoleMgr.deleteRolePermission(id);
				
				result.put("success", "true");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.dropRole(long id) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加修改角色 角色名重复验证
	 * @param 角色名,角色ID
	 * @return 是否通过验证
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public boolean roleValidator(Map<String,String> parameter) throws Exception {
		
		try {
			
			boolean result = false;
			
			//验证角色名是否重复
			if(parameter.get("roleName")!=null){
				
				DBRow roleRow = floorRoleMgr.getRoleNameByRoleName(parameter.get("roleName"));
				if(roleRow==null){
					result = true;
				}else if(roleRow.get("adgid","0").equals(parameter.get("adgid"))){
					result = true;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.roleValidator(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 权限管理  >> 获得角色权限
	 * @param 角色ID
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPermissionContextList(long id) throws Exception{
		
		try {
			
			DBRow[] allRolePermissionList = floorAccountMgr.getPermissionList();
			
			if(id!=-1){
				
				for(DBRow oneResult:allRolePermissionList){
					
					DBRow state = new DBRow();
					
					DBRow parameter = new DBRow();
					parameter.add("adgid", id);
					
					//菜单权限
					if(oneResult.getString("flag").equals("menu")){
						
						parameter.add("ctid", oneResult.get("original_id", -1));
						//是否拥有此权限
						if(floorRoleMgr.getRoleMenuPermission(parameter)!=null){
							
							//选中
							state.add("selected", true);
							//打开
							state.add("opened", false);
						}
					//页面权限
					}else{
						
						parameter.add("ataid", oneResult.get("original_id", -1));
						
						if(floorRoleMgr.getRolePagePermission(parameter)!=null){
							
							state.add("selected", true);
							state.add("opened", false);
						}
					}
					
					oneResult.add("state", state);
				}
			}
			
			return allRolePermissionList;
			
		} catch (Exception e) {
			throw new Exception("RoleMgrSbb.getPermissionContextList(long id) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 权限管理  >> 修改角色权限
	 * @param 权限信息
	 * @return DBRow
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow modifyRolePermission(DBRow parameter) throws Exception{
		
		try {
			
			DBRow resultValue = null;
			
			long adgid = parameter.get("adgid", -1);
			
			if(floorRoleMgr.getRoleById(adgid)!=null){
				
				//删除原有角色权限
				floorRoleMgr.deleteRolePermission(adgid);
				
				DBRow[] permissionArray = (DBRow[]) parameter.getValue("permission_collection");
				
				for(DBRow oneResult:permissionArray){
					
					DBRow param = new DBRow();
					param.add("adgid", adgid);
					//添加权限
					if(oneResult.getString("flag").equals("menu")){
						
						param.add("ctid", oneResult.get("original_id", -1));
						
						floorRoleMgr.insertRoleMenuPermission(param);
						
					}else{
						
						param.add("ataid", oneResult.get("original_id", -1));
						
						floorRoleMgr.insertRolePagePermission(param);
					}
				}
				
				resultValue = new DBRow();
				resultValue.add("adgid", adgid);
				
				//清空缓存权限,再下次访问时会重新查询
				AdminAuthenCenter.clearPermitPageHM();
				AdminAuthenCenter.clearPermitActionHM();
			}
			
			return resultValue;
			
		} catch (Exception e) {
			throw new Exception("RoleMgrSbb.modifyRolePermission(DBRow parameter) error:" + e);
		}
	}

	//yuanxinyu
	public DBRow[] getLefts(long adg_id) throws Exception {
		try{
			DBRow[] adpIdRows = floorRoleMgr.queryAdminGroupPostByAdgId(adg_id);
			
			DBRow[] adpRows = null;
			
			if(adpIdRows.length > 0){
				String params = "";
				for(int i = 0 ; i<adpIdRows.length ; i++){
					params += adpIdRows[i].getString("adp_id") + ",";
				}
				params = params.substring(0, params.length()-1);
				adpRows = floorRoleMgr.queryIdExistAdminPostByParams(params);
			}
			
			return adpRows;
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getRights(long adg_id) error:" + e);
		}
	}

	public DBRow[] getRights(long adg_id) throws Exception {
		try{
			DBRow[] lefts = floorRoleMgr.queryAdminGroupPostByAdgId(adg_id);
			
			DBRow[] rights = null;
			
			if(lefts.length > 0){
				String params = "";
				for(int i = 0 ; i<lefts.length ; i++){
					params += lefts[i].getString("adp_id") + ",";
				}
				params = params.substring(0, params.length()-1);
				rights = floorRoleMgr.queryAdminPostByParams(params);
			}else{
				rights = floorRoleMgr.queryAdminPostAll();
			}
			
			return rights;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getRights(long adg_id) error:" + e);
		}
	}

	public DBRow addGroupPost(JSONObject jsonData) throws Exception {
		DBRow returnValue = null;
		try{
			long adg_id = jsonData.getJSONObject("role").getLong("ADGID");
			String unhasTitle = jsonData.getString("unhasTitle");
			DBRow param = new DBRow();
			if(unhasTitle.length() > 1)
			{
				String[] unhasTits = unhasTitle.split(",");
				for(int i = 0; i < unhasTits.length ; i++){
					param.clear();
					param.add("adg_id", adg_id);
					param.add("adp_id", Long.parseLong(unhasTits[i]));
					floorRoleMgr.insertAdminGroupPost(param);
				}
			}
			else
			{
				param.clear();
				param.add("adg_id", adg_id);
				param.add("adp_id", Long.parseLong(unhasTitle));
				floorRoleMgr.insertAdminGroupPost(param);
			}
			returnValue = new DBRow();
			returnValue.add("adg_id", adg_id);
			
			return returnValue;
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.addGroupPost(DBRow parameter) error:" + e);
		}
		
	}

	public DBRow delGroupPost(String adg_id,String hasTitle) throws Exception {
		try{
			long adgId = Long.parseLong(adg_id);
			DBRow param = null;
			if(hasTitle.length() > 1)
			{
				String[] hasTits = hasTitle.split(",");
				for(int i = 0; i < hasTits.length ; i++){
					
					floorRoleMgr.deleteAdminGroupPostByAdpId(Long.parseLong(hasTits[i]),adgId);
				}
			}
			else
			{
				floorRoleMgr.deleteAdminGroupPostByAdpId(Long.parseLong(hasTitle),adgId);
			}
			
			param = new DBRow();
			param.add("adg_id", adg_id);
			return param;
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.delGroupPost(long adp_id, long adg_id) error:" + e);
		}
	}

	public DBRow getRoleById(long id) throws Exception {
		DBRow main = new DBRow();
		try{
			main = floorRoleMgr.getRoleById(id);
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getRoleById(long id) error:" + e);
		}
		return main;
	}

	public JSONObject getGpDetail(JSONObject jsonData) throws Exception {
		JSONObject output = new JSONObject();
		try{
			long id = jsonData.getJSONObject("role").getLong("ADGID");
			DBRow[] lefts = getLefts(id);
			DBRow[] rights = getRights(id);
			DBRow role = getRoleById(id);
			output.put("lefts",DBRowUtils.multipleDBRowArrayAsJSON(lefts));
			output.put("rights",DBRowUtils.multipleDBRowArrayAsJSON(rights));
			output.put("role", role);
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getRoleById(long id) error:" + e);
		}
		return output;
	}

	public JSONObject getGpDetailById(long adg_id) throws Exception {
		JSONObject output = new JSONObject();
		try{
			DBRow[] lefts = getLefts(adg_id);
			DBRow[] rights = getRights(adg_id);
			DBRow role = getRoleById(adg_id);
			
			output.put("lefts",DBRowUtils.multipleDBRowArrayAsJSON(lefts));
			output.put("rights",DBRowUtils.multipleDBRowArrayAsJSON(rights));
			output.put("role", role);
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getRoleById(long id) error:" + e);
		}
		return output;
	}

	/**
	 * 角色管理 >> 获取职务列表
	 * @param 分页
	 * @return 职务列表
	 * @since Sync10-ui 1.0
	 * @author Yuanxinyu
	**/
	public DBRow[] getSearchPostList(Map<String,Object> parameter) throws Exception{
		
		try {
			
			DBRow[] postList = floorRoleMgr.querySearchPostList(parameter);
			
			for(int i = 0 ; i < postList.length ; i ++){
				long adid = postList[i].get("creator", -1);
				if(adid != -1){
					DBRow admin = floorRoleMgr.getAdminByAdid(adid);
					postList[i].add("create_name", admin.get("employe_name"));
				}
			}
			
			return postList;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.getSearchPostList(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 修改职务
	 * @param 职务信息
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author Yuanxinyu
	**/
	public DBRow modifyPost(DBRow parameter) throws Exception{
		
		try {
			
			//验证
			DBRow result = new DBRow();
			
			boolean validate = true;
			//验证
			DBRow[] PostRow = floorRoleMgr.queryPostByNameNotId(parameter.get("id", -1),parameter.getString("name"));
			
			if(PostRow != null && PostRow.length > 0){
				
				validate = false;
				result.add("error", "Post already exists");
			}
			
			if(validate){
				
				parameter.remove("adpid");
				
				floorRoleMgr.updatePost(parameter);
			}
			
			result.add("success",validate);
				
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.modifyPost(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加修改职务 职务名重复验证
	 * @param 职务名,职务ID
	 * @return 是否通过验证
	 * @since Sync10-ui 1.0
	 * @author Yuanxinyu
	**/
	public boolean postValidator(Map<String,String> parameter) throws Exception {
		
		try {
			
			boolean result = false;
			
			//验证角色名是否重复
			if(parameter.get("postName")!=null){
				
				DBRow postRow = floorRoleMgr.getPostByName(parameter.get("postName"));
				if(postRow==null){
					result = true;
				}else if(postRow.get("id","0").equals(parameter.get("adpid"))){
					result = true;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.postValidator(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加职务
	 * @param 职务信息
	 * @return 职务信息
	 * @since Sync10-ui 1.0
	 * @author Yuanxinyu
	**/
	public DBRow addPost(DBRow parameter) throws Exception{
		
		try {
			
			DBRow result = new DBRow();
			
			boolean validate = true;
			//验证
			DBRow roleRow = floorRoleMgr.getPostByName(parameter.getString("NAME"));
			
			if(roleRow != null){
				
				validate = false;
				result.add("error", "Post already exists");
			}
			
			if(validate){
				DBRow maxId = floorRoleMgr.getAdminPostMaxId();
				long id = maxId.get("MAXID", -1) + 1;
				parameter.add("id", id);
				floorRoleMgr.insertPost(parameter);
				result.add("id", id);
			}
			
			result.add("success",validate);
				
			return result;
			
		}catch (Exception e){
			throw new Exception("RoleMgrSbb.addPost(DBRow parameter) error:" + e);
		}
	}
	
}
