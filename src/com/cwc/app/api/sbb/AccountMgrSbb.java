package com.cwc.app.api.sbb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import seamoonotp.seamoonapi;
import sun.misc.BASE64Decoder;

import com.cwc.app.api.zzq.OpenFireSetBean;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.sbb.AccountMgrIFaceSbb;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.lucene.zyj.AdminIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.ImageCut;
import com.cwc.app.util.StrUtil;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class AccountMgrSbb implements AccountMgrIFaceSbb{

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorAccountMgrSbb floorAccountMgr;
	private SystemConfigIFace systemConfig;
	private OpenFireSetBean openFireSetBean;

	public void setFloorAccountMgr(FloorAccountMgrSbb floorAccountMgr) {
		this.floorAccountMgr = floorAccountMgr;
	}
	public void setOpenFireSetBean(OpenFireSetBean openFireSetBean) {
		this.openFireSetBean = openFireSetBean;
	}
	/**
	 * 账号管理 >> 添加账号  >> 添加
	 * @param [DBRow parameter]
	 * @return DBRow 
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow addAccount(DBRow parameter)throws Exception{
		
		try {
			
			DBRow result = new DBRow();
			boolean validate = true;
			//验证[validate插件暂时无效] 验重
			DBRow account = floorAccountMgr.getAccountByAccount(parameter.getValue("ACCOUNT").toString());
			DBRow[] departRows = (DBRow[]) parameter.getValue("DEPARTMENTPOST");
			DBRow[] wareRows = (DBRow[]) parameter.getValue("WAREHOUSE");
			
			//验证：账号是否存在
			if(account != null ){
				
				validate = false;
				result.add("error", "Account already exists");
			}
			
			//获取仓库ID 用于转换utc时间
			String warehouseId = "";
			if(wareRows != null && wareRows.length > 0){
				
				warehouseId = wareRows[0].getString("WAREHOUSE_ID");
				
			}else{
				
				result.add("exception", "warehouse is null");
				validate = false;
			}
			
			if(validate){
				
				//添加账户
				parameter.remove("DEPARTMENTPOST");
				parameter.remove("WAREHOUSE");
				//加密密码
				String pwd = parameter.getString("pwd");
				parameter.add("pwd", StringUtil.getMD5(pwd));
				
				if(!parameter.getString("ENTRYTIME").equals("")){
					
					parameter.put("entrytime", DateUtil.utcTime(parameter.getString("ENTRYTIME"), Long.valueOf(warehouseId)));
				}
				
				if(StrUtil.isBlank(parameter.get("customer_id", "")))
				{
					parameter.remove("customer_id");
				}

				if(StrUtil.isBlank(parameter.get("corporation_id", "")))
				{
					parameter.remove("corporation_id");
				}
				if(StrUtil.isBlank(parameter.get("corporation_type", "")))
				{
					parameter.remove("corporation_type");
				}
				long id = floorAccountMgr.insertAccount(parameter);
				
				//添加部门职务
				for(DBRow oneResult:departRows){
					oneResult.add("adid", id);
				}
				floorAccountMgr.insertDepartmentAndPost(departRows);
				
				//添加仓库区域
				for(DBRow oneResult:wareRows){
					oneResult.add("adid", id);
				}
				floorAccountMgr.insertWarehouseAndArea(wareRows);
				this.editAdminIndex("ADD", id);
				
				result = floorAccountMgr.getAccountById(id);
			}
			
			result.add("success", validate);
			
			return result;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.addAccount(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 修改账号  >> 修改
	 * @param DBRow String
	 * @return DBRow 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow modifyAccount(DBRow parameter,String flag)throws Exception{
		
		try {
			long id = parameter.get("adid",-1L);
			//预处理[屏蔽账号][修改账户][修改令牌][修改密码]
			if(flag.equals("lock")){
				
				//转换屏蔽与解除屏蔽
				if(parameter.getString("llock").equals("0")){
					parameter.add("llock", 1);
				}else{
					parameter.add("llock", 0);
				}
				
			}else if(flag.equals("update")){
				
				DBRow[] departRows = (DBRow[]) parameter.getValue("DEPARTMENTPOST");
				DBRow[] wareRows = (DBRow[]) parameter.getValue("WAREHOUSE");
				parameter.remove("DEPARTMENTPOST");
				parameter.remove("WAREHOUSE");
				
				//添加部门职务
				for(DBRow oneResult:departRows){
					oneResult.add("adid", id);
				}
				floorAccountMgr.deleteDepartmentByAccountId(id);
				floorAccountMgr.insertDepartmentAndPost(departRows);
				
				//添加仓库区域
				for(DBRow oneResult:wareRows){
					oneResult.add("adid", id);
				}
				floorAccountMgr.deleteWarehouseByAccountId(id);
				floorAccountMgr.insertWarehouseAndArea(wareRows);
				
				//获取仓库ID 用于转换utc时间
				String warehouseId = "";
				if(wareRows != null && wareRows.length > 0){
					
					warehouseId = wareRows[0].getString("WAREHOUSE_ID");
					
					if(!parameter.getString("ENTRYTIME").equals("")){
						
						parameter.put("entrytime", DateUtil.utcTime(parameter.getString("ENTRYTIME"), Long.valueOf(warehouseId)));
					}
				}
				if(StrUtil.isBlank(parameter.get("corporation_id", "")))
				{
					parameter.put("corporation_id",null);
				}
				if(StrUtil.isBlank(parameter.get("corporation_type", "")))
				{
					parameter.put("corporation_type",null);
				}
				
			}else if(flag.equals("token")){
				
				
				
			}else if(flag.equals("password")){
				
				//加密
				String pwd = parameter.getString("pwd");
				parameter.add("pwd",StringUtil.getMD5(pwd));
			
			//安卓端：修改昵称和密码
			}else if(flag.equals("android")){
				
				String pwd = parameter.getString("pwd");
				
				parameter.add("pwd",StringUtil.getMD5(pwd));
				
			}else{
				
				return null;
			}
			
			if(StrUtil.isBlank(parameter.get("customer_id", "")))
			{
				parameter.remove("customer_id");
			}
			DBRow accountInfo = floorAccountMgr.updateAccount(parameter);
			
			this.editAdminIndex("UPDATE", id);
			
			return accountInfo;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.modifyAccount(DBRow parameter,String flag) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 删除账号
	 * @param [long id]
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public void dropAccount(long id)throws Exception{
		
		try {
			
			this.editAdminIndex("DELETE", id);
			
			//删除账号
			floorAccountMgr.deleteAccountById(id);
			
			//删除对应部门
			floorAccountMgr.deleteDepartmentByAccountId(id);
			
			//删除对应的仓库
			floorAccountMgr.deleteWarehouseByAccountId(id);
			
			//TODO 删除对应的权限
			
			//TODO 删除对应的TITLE
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.delAccount(long id) error:" + e);
		}
	}
	/**
	 * 账号管理 >>查询
	 * @param Map<String,Object>
	 * @return Map<String,DBRow[]>
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public Map<String,DBRow[]> getAccountList(Map<String,Object> parameter) throws Exception{
		
		try {
			DBRow[] accountList = new DBRow[0];
			if("search".equals(parameter.get("cmd"))
					&& !StringUtil.isBlank(parameter.get("searchConditions").toString()))
			{
				AdminIndexMgr adminIndexMgr = new AdminIndexMgr();
				PageCtrl pc = (PageCtrl)parameter.get("PageCtrl");
				int page_count = systemConfig.getIntConfigValue("page_count");
				accountList = adminIndexMgr.getSearchResults(parameter.get("searchConditions").toString(), 2, page_count, pc);
			}
			else
			{
				accountList = floorAccountMgr.getAccountList(parameter);
			}
				
			for(DBRow oneResult:accountList){
				
				long id = oneResult.get("adid", -1L);
				
				DBRow[] deptposts = floorAccountMgr.getAccountDepartmentAndPost(id);
				
				Map<String, DBRow> deptMap = new HashMap<String, DBRow>();
				String deptpost = "";
				
				for (int i = 0; i < deptposts.length; i++){
					
					String DEPTID = deptposts[i].getString("deptid");
					String POSTNAME = deptposts[i].getString("postname");
					
					if(deptMap.containsKey(DEPTID)){
						
						if(deptpost.indexOf(","+POSTNAME+",") == -1){
							deptpost += POSTNAME + ", " ;
						}
						
					}else{
						
						deptMap.put(DEPTID, deptposts[i]);
						deptpost = "," + POSTNAME+", ";
					}
					deptMap.get(DEPTID).add("postname_str", deptpost);
				}
				
				List<DBRow> list1 = new ArrayList<DBRow>();
				Iterator<Entry<String, DBRow>> it = deptMap.entrySet().iterator();
				
				while(it.hasNext()){
					
					Entry<String, DBRow> entry = it.next();
					DBRow row = entry.getValue();
					String postname = row.getString("postname_str");
					
					if(!StrUtil.isBlank(postname)){
						postname = postname.substring(1, postname.length()-2);
					}
					
					row.add("postname_str", postname);
					list1.add(row);
				}
				
				DBRow[] rows1 = (DBRow[])list1.toArray(new DBRow[0]);
				
				DBRow[] areaRows = floorAccountMgr.getAccountWarehouseAndArea(id);
				
				Map<String, DBRow> areaMap = new HashMap<String, DBRow>();
				List<String> areapost = new ArrayList<String>();
				
				for (int i = 0; i < areaRows.length; i++){
					
					String wareid = areaRows[i].getString("wareid");
					String area_name = areaRows[i].getString("areaname");
					
					if(areaMap.containsKey(wareid)){
						
						if(!areapost.contains(area_name)){
							
							areapost.add(area_name);
						}
						
					}else{
						
						areaMap.put(wareid, areaRows[i]);
						areapost = new ArrayList<String>();
						
						if(!StrUtil.isBlank(area_name)){
							
							areapost.add(area_name);
						}
					}
					areaMap.get(wareid).add("area_name_array", areapost);
				}
				
				List<DBRow> list2 = new ArrayList<DBRow>();
				Iterator<Entry<String, DBRow>> it1 = areaMap.entrySet().iterator();
				
				while(it1.hasNext()){
					
					Entry<String, DBRow> entry = it1.next();
					DBRow row = entry.getValue();
					@SuppressWarnings("unchecked")
					List<String> areaname = (List<String>)row.get("area_name_array", new ArrayList<String>());
					
					if(areaname.size() > 0){
						DBRow[] area = new DBRow[areaname.size()];
						
						for (int i = 0; i < areaname.size(); i++){
							
							area[i] = new DBRow();
							area[i].add("area", areaname.get(i));
						}
						row.add("area_name_array", area);
					}
					list2.add(row);
				}
				
				DBRow[] rows2 = (DBRow[])list2.toArray(new DBRow[0]);
				
				//转换utc时间
				if(areaRows != null && areaRows.length > 0 ){
					
					if(!oneResult.getString("last_login_date").equals("")){
						
						oneResult.put("last_login_date", DateUtil.showLocalTime(oneResult.getString("last_login_date"), Long.valueOf(areaRows[0].getString("WAREID"))).substring(0,10));
					}
					
					if(!oneResult.getString("Entrytime").equals("")){
						
						oneResult.put("Entrytime", DateUtil.showLocalTime(oneResult.getString("Entrytime"), Long.valueOf(areaRows[0].getString("WAREID"))).substring(0,10));
					}
				}
				
				oneResult.add("warehoused", areaRows);
				oneResult.add("deptpost", rows1);
				oneResult.add("warearea", rows2);
				oneResult.add("titles", floorAccountMgr.getAdminHasTitleList(id, ""));
				
				//办公地点全显示
				if(!oneResult.getString("office_location").equals("")){
					
					oneResult.add("office_all_name", floorAccountMgr.getOfficeLocationCascadeNameById(Long.valueOf(oneResult.getString("office_location"))));
				}
			}
			
			//获取所有部门
			DBRow[] deptmentList = floorAccountMgr.getDepartmentList();
			//获取所有职务
			DBRow[] postList = floorAccountMgr.getPostList();
			//获取所有仓库
			DBRow[] warehouseList = floorAccountMgr.getWarehouseList();
			//TODO
			
			//全部customer
			DBRow[] customerList = floorAccountMgr.getCustomerId();
			//获取树办公地点
			DBRow[] officeLocationList = floorAccountMgr.getOfficeLocationList(-1,0);
			
			Map<String,DBRow[]> result = new HashMap<String,DBRow[]>();
			result.put("accounts",accountList);
			result.put("deptments",deptmentList);
			result.put("posts",postList);
			result.put("warehouses",warehouseList);
			result.put("officelocations",officeLocationList);
			result.put("customers",customerList);
			return result;
			
		}catch (Exception e){
			
			e.printStackTrace();
			throw new Exception("AccountMgrSbb.getAccountList(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 添加账号	>> 查询账号上下文
	 * @param 账号ID
	 * @return Map<String,DBRow[]>
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public Map<String,DBRow[]> getAccountContext(long accountId) throws Exception{
		
		try {
			
			//全部部门/职务列表
			DBRow[] departmentList  = floorAccountMgr.getDepartmentList();
			
			for(DBRow oneResult:departmentList){
				
				oneResult.add("children", floorAccountMgr.getPostList(oneResult.get("adgid",-1L),accountId));
				oneResult.add("nocheck", true);
			}
			
			//全部办公地点列表
			DBRow[] officeLocationList = floorAccountMgr.getOfficeLocationList(accountId,0);
			
			//仓库类型
			StorageTypeKey storage = new StorageTypeKey();
			DBRow[] storageType = storage.getEnStorageTypeKey();
			
			//全部仓库/区域列表
			DBRow[] warehouseList = floorAccountMgr.getWarehouseList();
			
			//全部customer
			DBRow[] customerList = floorAccountMgr.getCustomerId();
			
			DBRow[] carrierList = floorAccountMgr.getCarrierId();
			/*for(DBRow oneResult:warehouseList){
				DBRow[] regionalList = floorAccountMgr.getRegionalList(oneResult.get("id",-1L),accountId);
				oneResult.add("nocheck", true);
				if(regionalList.length > 0){
					oneResult.add("children",regionalList);
				}
			}*/
			
			Map<String,DBRow[]> result = new HashMap<String,DBRow[]>();
			result.put("departments",departmentList);
			result.put("officelocations",officeLocationList);
			result.put("warehouses",warehouseList);
			result.put("storageType",storageType);
			result.put("customers",customerList);
			result.put("carriers",carrierList);
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountContext(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 验证
	 * @param Map<String,String>
	 * @return int
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public boolean accountValidator(Map<String,String> parameter) throws Exception {
		
		try {
			
			boolean result = false;
			
			
			//验证帐号是否重复
			if(parameter.get("account")!=null){
				
				if(floorAccountMgr.getAccountByAccount(parameter.get("account"))==null){
					result = true;
				}
			//验证令牌是否正确
			}else if(parameter.get("tokenId")!=null && parameter.get("tokenKey")==null){
				
				DBRow tokeRow = floorAccountMgr.getTokenById(parameter.get("tokenId"));
				
				if(tokeRow!=null){
					
					result = true;
						
				}
			//验证令牌是否正确
			}else if(parameter.get("tokenKey")!=null){
				
				if(parameter.get("tokenId")!=null){
					
					DBRow tokeRow = floorAccountMgr.getTokenById(parameter.get("tokenId"));
					
					if(tokeRow!=null){
						
						seamoonapi seamoon = new seamoonapi();
						
						String validateResult = seamoon.checkpassword(tokeRow.getString("sninfo"),parameter.get("tokenKey"));
						
						if(validateResult.length()>3){
							
							DBRow updateTokeRow = new DBRow();
							updateTokeRow.add("sn", parameter.get("tokenId"));
							updateTokeRow.add("sninfo", validateResult);
							//更新SNINFO
							floorAccountMgr.updateSninfo(updateTokeRow);
							result = true;
						}
					}
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountValidator(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 权限管理
	 * @param null
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPermissionContextList(long id) throws Exception{
		
		try {
			
			DBRow[] allAccountPermissionList = floorAccountMgr.getPermissionList();
			
			if(id!=-1){
				
				for(DBRow oneResult:allAccountPermissionList){
					
					DBRow state = new DBRow();
					
					DBRow parameter = new DBRow();
					parameter.add("adid", id);
					
					//菜单权限
					if(oneResult.getString("flag").equals("menu")){
						
						parameter.add("ctid", oneResult.get("original_id", -1));
						//是否拥有此权限
						if(floorAccountMgr.getAccountMenuPermission(parameter)!=null){
							
							//选中
							state.add("selected", true);
							//打开
							state.add("opened", false);
						}
						//所属角色是否拥有此权限
						if(floorAccountMgr.getAccountBelongsRolesMenuPermission(parameter).length>0){
							
							//选中
							state.add("selected", true);
							//禁用
							state.add("disabled", true);
							//打开
							state.add("opened", false);
						}
					
					//页面权限
					}else{
						
						parameter.add("ataid", oneResult.get("original_id", -1));
						
						if(floorAccountMgr.getAccountPagePermission(parameter)!=null){
							
							state.add("selected", true);
							state.add("opened", false);
						}
						
						if(floorAccountMgr.getAccountBelongsRolesPagePermission(parameter).length>0){
							
							state.add("selected", true);
							state.add("disabled", true);
							state.add("opened", false);
						}
					}
					
					oneResult.add("state", state);
					
				}
			}
			
			return allAccountPermissionList;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.getPermissionContextList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 权限管理  >> 修改权限
	 * @param DBRow
	 * @return DBRow
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow modifyAccountPermission(DBRow parameter) throws Exception{
		
		try {
			DBRow resultValue = null;
			
			long adid = parameter.get("adid", -1);
			
			DBRow account = floorAccountMgr.getAccountById(adid);
			
			if(account!=null){
				
				//删除原有账户权限
				floorAccountMgr.deleteAccountPermission(adid);
				
				DBRow[] permissionArray = (DBRow[]) parameter.getValue("permission_collection");
				
				for(DBRow oneResult:permissionArray){
					
					DBRow param = new DBRow();
					param.add("adid", adid);
					//添加权限
					if(oneResult.getString("flag").equals("menu")){
						
						param.add("ctid", oneResult.get("original_id", -1));
						//看看是否存在,所在组，或者自己
						if(!checkAccountMenu(adid, oneResult.get("original_id", -1)))
						{
							floorAccountMgr.insertAccountMenuPermission(param);
//							modifyAccountPermissionParentMenu(adid, oneResult.get("original_id", -1));
						}
						
					}else{
						
						param.add("ataid", oneResult.get("original_id", -1));
						if(!checkAccountAction(adid, oneResult.get("original_id", -1)))
						{
							floorAccountMgr.insertAccountPagePermission(param);
//							modifyAccountPermissionParentAction(adid, oneResult.get("original_id", -1));
						}
					}
				}
				
				resultValue = new DBRow();
				resultValue.add("adid", adid);
				
				//清空缓存权限,再下次访问时会重新查询
				AdminAuthenCenter.clearPermitPageHM();
				AdminAuthenCenter.clearPermitActionHM();
			}
			
			return resultValue;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.modifyAccountPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 添加账号菜单的父菜单权限
	 * @param id
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月21日 上午9:29:21
	 */
	public void modifyAccountPermissionParentMenu(long adid, long id) throws Exception
	{
		DBRow param = new DBRow();
		param.add("adid", adid);
		DBRow menu = floorAccountMgr.getTurboshopControlTreeById(id);
		if(menu.get("parentid", 0L) != 0)
		{
			param.add("ctid", menu.get("parentid", 0L));
			//看看是否存在,所在组，或者自己
			if(!checkAccountMenu(adid, menu.get("parentid", 0L)))
			{
				floorAccountMgr.insertAccountMenuPermission(param);
			}
			modifyAccountPermissionParentMenu(adid, menu.get("parentid", 0L));
		}
	}
	
	/**
	 * 添加账号action的菜单权限
	 * @param id
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月21日 上午9:29:35
	 */
	public void modifyAccountPermissionParentAction(long adid, long ataid) throws Exception
	{
		DBRow param = new DBRow();
		param.add("adid", adid);
		DBRow action = floorAccountMgr.getAuthenticationActionById(ataid);
		if(action.get("page", 0L) != 0)
		{
			param.add("ctid", action.get("page", 0L));
			//看看是否存在,所在组，或者自己
			if(!checkAccountMenu(adid, action.get("page", 0L)))
			{
				floorAccountMgr.insertAccountMenuPermission(param);
			}
			modifyAccountPermissionParentMenu(adid, action.get("page", 0L));
		}
	}
	
	/**
	 * 验证账号菜单权限
	 * @param adid
	 * @param ctid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月21日 上午9:23:00
	 */
	public boolean checkAccountMenu(long adid, long ctid) throws Exception
	{
		boolean flag = false;
		DBRow parameter = new DBRow();
		parameter.add("ctid", ctid);
		parameter.add("adid", adid);
		//所属角色是否拥有此权限
		if(floorAccountMgr.getAccountBelongsRolesMenuPermission(parameter).length>0){
			flag = true;
		}
		//账号是否拥有此权限
		if(floorAccountMgr.getAccountMenuPermission(parameter)!=null){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 验证账号action权限
	 * @param adid
	 * @param ataid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月21日 上午9:23:09
	 */
	public boolean checkAccountAction(long adid, long ataid) throws Exception
	{
		boolean flag = false;
		DBRow parameter = new DBRow();
		parameter.add("ataid", ataid);
		parameter.add("adid", adid);
		
		//所属角色是否拥有此权限
		if(floorAccountMgr.getAccountBelongsRolesPagePermission(parameter).length>0){
			flag = true;
		}
		//账号是否拥有此权限
		if(floorAccountMgr.getAccountPagePermission(parameter)!=null){
			flag = true;
		}
		return flag;
	}
	
	
	/**
	 * 账号管理 >> 上传图像  >> 图像剪辑
	 * @param Map<String,String>
	 * @return long
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public String croppedImage(Map<String,Object> parameter) throws Exception{
	 
		try {
			
			long seq = floorAccountMgr.getSequance("upload_image_seq");
			
			String inImagePath = (String)parameter.get("inImagePath");
			String outImagePath = (String)parameter.get("outImagePath")+String.valueOf(seq)+".jpg";
			String result = "/Sync10/upload/account/"+String.valueOf(seq)+".jpg";
			
			int x = Double.valueOf((String)parameter.get("x")).intValue();
			int y = Double.valueOf((String)parameter.get("y")).intValue();
			int w = Double.valueOf((String)parameter.get("w")).intValue();
			int h = Double.valueOf((String)parameter.get("h")).intValue();
			
			ImageCut imageCut=new ImageCut();
			
			imageCut.cutImage(outImagePath,inImagePath,x,y,w,h);
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String sysdate = sdf.format(date);
			
			DBRow row = new DBRow();
			row.put("adid", Long.valueOf((String)parameter.get("adid")));
			row.put("file_flag", (String)parameter.get("flag"));
			row.put("file_path", result);
			row.put("file_name", (String)parameter.get("file_name"));
			row.put("create_date",sysdate);
			row.put("activity", 0);
			
			//如果是初次添加某种类型 ,设置为默认
			DBRow param = new DBRow();
			param.put("adid", Long.valueOf((String)parameter.get("adid")));
			param.put("file_flag", (String)parameter.get("flag"));
			
			DBRow[] photo = floorAccountMgr.getAccountPhotoFromType(param);
			
			//插入数据库
			long id = floorAccountMgr.insertAccountImage(row);
			
			//如果不存在此类型图片,更新为默认.
			if( photo == null || photo.length <= 0 ){
				
				param.put("id", id);
				floorAccountMgr.updateAccountPhoto(param);
			}
			
			return result;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.croppedImage(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 上传图像  >> Android
	 * @param Map<String,String>
	 * @return long
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public long addAccountImage(DBRow parameter) throws Exception{
	 
		try {
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String sysdate = sdf.format(date);
			
			parameter.put("create_date",sysdate);
			parameter.put("activity", 0);
			
			//如果是初次添加某种类型 ,设置为默认
			DBRow param = new DBRow();
			param.put("adid", parameter.get("adid"));
			param.put("file_flag", (String)parameter.get("file_flag"));
			
			DBRow[] photo = floorAccountMgr.getAccountPhotoFromType(param);
			
			//插入数据库
			long id = floorAccountMgr.insertAccountImage(parameter);
			
			//如果不存在此类型图片,更新为默认.
			if( photo == null || photo.length <= 0 ){
				
				param.put("id", id);
				floorAccountMgr.updateAccountPhoto(param);
			}
			
			return id;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.addAccountImage(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> title管理  >> 获得title
	 * @param adid
	 * @return Map<String,DBRow[]>
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public Map<String,DBRow[]> getAccountTitle(long id,String has,String unHas) throws Exception{
		
		try {
			
			Map<String,DBRow[]> result = new HashMap<String,DBRow[]>();
			
			if(id!=-1){
				
				DBRow[] hasTitle = floorAccountMgr.getAdminHasTitleList(id,has);
				DBRow[] unhasTitle = floorAccountMgr.getAdminUnhasTitleList(id,unHas);
				
				result.put("hasTitle",hasTitle);
				result.put("unhasTitle",unhasTitle);
				
			}else{
				
				DBRow[] allTitle = floorAccountMgr.getAllTitleList(unHas);
				result.put("hasTitle",null);
				result.put("unhasTitle",allTitle);
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountTitle(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> title管理  >> 交换TITlE
	 * @param 账号ID 需要删除TITLEID 需要添加的TITLEID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] swapAccountTitle(DBRow parameter) throws Exception{
		
		try {
			
			long adid = parameter.get("adid", -1);
			
			if(parameter.getValue("hasTitle")!=null){
				
				for(DBRow oneResult:(DBRow[])parameter.getValue("hasTitle")){
					
					long titleId = oneResult.get("title_id", -1);
					floorAccountMgr.deleteAccountTitle(adid, titleId);
				}
			}
			
			if(parameter.getValue("unhasTitle")!=null){
				
				for(DBRow oneResult:(DBRow[])parameter.getValue("unhasTitle")){
					
					long titleId = oneResult.get("title_id", -1);
					floorAccountMgr.insertAccountTitle(adid, titleId);
				}
			}
			
			return floorAccountMgr.getAdminHasTitleList(adid, "");
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.swapAccountTitle(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理  >> 获取图像列表
	 * @param 账号ID
	 * @return 此账号的图像列表
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountPhotoList(long id) throws Exception{
		
		try {
			
			return floorAccountMgr.getAccountPhotoList(id);
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountPhotoList(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理  >> 修改图像
	 * @param 图像信息
	 * @return 图像信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public int modifyAccountPhoto(DBRow parameter) throws Exception{
		
		try {
			
			int result = 0;
			//清除默认
			floorAccountMgr.updateAccountPhotoDefault(parameter);
			
			if(parameter.getString("ACTIVITY").equals("1")){
				
				result = floorAccountMgr.updateAccountPhoto(parameter);
			}
			
			return result;
			
		}catch (Exception e){
			
			throw new Exception("AccountMgrSbb.modifyAccountPhoto(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理  >> 删除图像
	 * @param 图像ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void dropAccountPhoto(long id) throws Exception{
		
		try {
			
			//查询url
			DBRow row = floorAccountMgr.getAccountPhotoById(id);
			
			if(row!=null){
				
				//删除记录
				floorAccountMgr.deleteAccountPhoto(id);
				//删除本地图象
				String filePath = Environment.getHome().replace("\\", "/")+".";
				
				if(row.get("file_path", "-1").length() > 7){
					
					filePath += row.get("file_path", "-1").substring(7);
				}
				
				FileUtil.delFile(filePath);
			}
			
		}catch (Exception e){
			
			throw new Exception("AccountMgrSbb.dropAccountPhoto(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> TITLE导出
	 * @param 搜索参数
	 * @return path
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public String exportAccountTitleBySearchResult(Map<String,String> parameter) throws Exception{
		
		try{
			
			DBRow[] exportResultRow = floorAccountMgr.getAccountExportTitleList(parameter);
				
			long[] storageAddressBook = new long[exportResultRow.length];
			
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/admin/ExportAdminTitles.xls"));
//			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/admin/ExportAdminTitles.xlsm"));
			
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row = sheet.getRow(0);
			
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			style.setLocked(false);
			style.setWrapText(true);
			
			//style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
			//style.setFillPattern(HSSFCellStyle.BIG_SPOTS);  
			
			HSSFCellStyle stylelock = wb.createCellStyle();
			//创建一个锁定样式
			stylelock.setLocked(true); 
			stylelock.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			
			if(exportResultRow.length > 0){
				
				for(int i = 0 ; i< exportResultRow.length ; i++){
					
					row = sheet.createRow((int)i+1);
					sheet.setDefaultColumnWidth(30);
					DBRow exportAdminTitle = exportResultRow[i];
					
					storageAddressBook[i] = exportAdminTitle.get("adid",0L);
					row.createCell(0).setCellValue(exportAdminTitle.getString("account"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(exportAdminTitle.getString("title_name"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(exportAdminTitle.getString("title_admin_sort"));
					row.getCell(2).setCellStyle(style);
				}
			}

			String path = "upl_excel_tmp/ExportAdminTitles_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
			FileOutputStream os= new FileOutputStream(Environment.getHome()+path);
			wb.write(os);  
			os.close();  
			return (path);
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.exportAccountTitleBySearchResult(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> title排序
	 * @param title信息
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void modifyAccountTitleSort(DBRow parameter) throws Exception{
		
		try{
			
			long titleId = parameter.get("id",-1);
			long adId = parameter.get("adid",-1);
			String flag = parameter.get("flag","");
			
			DBRow titleRow = floorAccountMgr.getAccountTitleByTitle(adId,titleId);
			
			DBRow sortTitleRow = null;
			if(flag.equals("up")){
				
				sortTitleRow = floorAccountMgr.getAccountTitleUp(adId,titleId);
				
			}else if(flag.equals("down")){
				
				sortTitleRow = floorAccountMgr.getAccountTitleDown(adId,titleId);
			}
			
			DBRow updateRow = new DBRow();
			
			updateRow.add("title_admin_sort",sortTitleRow.get("title_admin_sort", -1));
			floorAccountMgr.updateAccountTitleSort(titleRow.get("title_admin_id", -1),updateRow);
			
			updateRow.add("title_admin_sort",titleRow.get("title_admin_sort", -1));
			floorAccountMgr.updateAccountTitleSort(sortTitleRow.get("title_admin_id", -1),updateRow);
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.exportAccountTitleBySearchResult(Map<String,String> parameter) error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >>处理账号的索引
	 * @param adid
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author zhangyanjie
	 */
	private void editAdminIndex(String type, long adid) throws Exception{
		
		try{
			
			type = type.toUpperCase();
			DBRow row = floorAccountMgr.getAccountById(adid);
			String account = row.getString("account");
			String employe_name = row.getString("employe_name");
			String email = row.getString("email");
			String mobilePhone = row.getString("mobilePhone");
			String register_token = row.getString("register_token");
			AdminIndexMgr indexMgr = new AdminIndexMgr();
			
			if("ADD".equals(type)){
				
				indexMgr.addIndex(adid, account, employe_name, email, mobilePhone, register_token);
			
			}else if("UPDATE".equals(type)){
				
				indexMgr.updateIndex(adid, account, employe_name, email, mobilePhone, register_token);
			
			}else if("DELETE".equals(type)){
				
				indexMgr.deleteIndex(adid);
			}
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.editAdminIndex() error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> 登记指纹
	 * @param 账号ID,特征码,base64码
	 * @return DBRow
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow registrationFingerprint(Long adid,String fingerCode,String fingerBase64) throws Exception{
		
		try{
			
			long seq = floorAccountMgr.getSequance("upload_image_seq");
			String fileName = String.valueOf(seq)+".png";
			String filePath = Environment.getHome().replace("\\", "/")+"."+ "/upload/account/"+fileName;
			
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String sysdate = sdf.format(date);
			
			//输出图片
			byte[] buffer = new BASE64Decoder().decodeBuffer(fingerBase64);
			FileOutputStream out = new FileOutputStream(filePath);
			out.write(buffer);
			out.close();
			
			//插入特征码记录
			DBRow codeParam = new DBRow();
			codeParam.add("fingerCode", fingerCode);
			DBRow result = floorAccountMgr.updateAccountFingerCode(adid,codeParam);
			
			//插入指纹图片记录
			DBRow imgParam = new DBRow();
			imgParam.add("adid", adid);
			imgParam.add("file_flag", "finger");
			imgParam.add("file_path", "/Sync10/upload/account/"+fileName);
			imgParam.add("file_name", fileName);
			imgParam.add("create_date",sysdate);
			imgParam.add("activity", 0);
			
			floorAccountMgr.insertAccountImage(imgParam);
			
			return result;
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.registrationFingerprint(Long adid,String fingerCode,String fingerBase64) error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> Excel登录
	 * @param 账号,密码
	 * @return 是,否
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public boolean accountLoginValidator(String account,String password) throws Exception{
		
		try{
			
			boolean result = false;
			
			DBRow admin = floorAccountMgr.getAccountByAccount(account);
			
			if(admin!=null){
				
				//密码正确 未被锁定
				if(admin.getString("pwd").equals(StringUtil.getMD5(password)) && admin.get("llock", 0)==0){
					
					result = true;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountLoginValidator(String account,String password) error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> 查询所有部门职务,仓库区域平铺列表
	 * @param null
	 * @return 所有部门职务,仓库区域平铺列表
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,DBRow[]> getAccountDeptPostAndWareArea() throws Exception{
		
		try{
			
			Map<String,DBRow[]> result = new HashMap<String,DBRow[]>();
			
			DBRow[] department = floorAccountMgr.getDepartmentList();

			DBRow[] post = floorAccountMgr.getPostList();
			
			DBRow[] wareArea = floorAccountMgr.getAccountAllWareAndAreaTileList();
			
			result.put("department", department);
			result.put("post", post);
			result.put("warehouseAndArea", wareArea);
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountDeptPostAndWareArea() error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> excel添加账号
	 * @param null
	 * @return 账号ID,错误信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,String> addAccountExcel(DBRow param) throws Exception{
		
		try{
			
			Map<String,String> result = new HashMap<String,String>();
			//数据处理
			param = this.accountDataProcessing(param);
			//验证[包含正确信息,错误信息,错误原因]
			result = this.accountExcelValidator(param);
			//插入账号返回Account
			if(result.get("success").equals("true")){
				
				DBRow accountRow = new DBRow();
				accountRow.put("account", param.getString("account"));
				accountRow.put("pwd",StringUtil.getMD5(param.getString("password")));
				accountRow.put("employe_name", param.getString("employeName"));
				accountRow.put("mobilePhone", param.getString("telephone"));
				accountRow.put("email", param.getString("email"));
				
				//插入账号
				long id = floorAccountMgr.insertAccount(accountRow);
				
				//查询部门ID 职务ID
				DBRow deptRow = floorAccountMgr.getDepartmentByName(param.getString("department"));
				DBRow postRow = floorAccountMgr.getPostByName(param.getString("post"));
				//插入部门
				DBRow iDeptRow = new DBRow();
				iDeptRow.put("adid", id);
				iDeptRow.put("department_id", deptRow.get("adgid", 0));
				iDeptRow.put("post_id",postRow.get("id", 0));
				
				DBRow[] iDeptRowArray = new DBRow[1];
				iDeptRowArray[0] = iDeptRow;
				
				floorAccountMgr.insertDepartmentAndPost(iDeptRowArray);
				
				//插入仓库
				String[] areaArray = param.getString("area").split(",");
				
				DBRow[] iWareAreaArray = new DBRow[areaArray.length];
				
				//查询仓库ID
				DBRow wareRow = floorAccountMgr.getWarehouseByName(param.getString("warehouse"));
				
				for(int i=0;i<areaArray.length;i++){
					
					//查询区域ID
					DBRow areaRow = floorAccountMgr.getWarehouseAreaByName(wareRow.get("id", 0), areaArray[i]);
					
					DBRow iWareArea = new DBRow();
					iWareArea.put("adid", id);
					iWareArea.put("warehouse_id", wareRow.get("id", 0));
					iWareArea.put("area_id", areaRow.get("area_id", 0));
					
					iWareAreaArray[i] = iWareArea;
				}
				
				floorAccountMgr.insertWarehouseAndArea(iWareAreaArray);
				
				//账号为空
				if(param.getString("account").equals("")){
					//更新账号,自动生成账号,[依据序列]
					DBRow row = null;
					long account = 0;
					
					do{
						account = floorAccountMgr.getSequance("account_auto_id");
						
						row = floorAccountMgr.getAccountByAccount(String.valueOf(account));
						
					}while(row!=null);
					
					DBRow updateAccount = new DBRow();
					updateAccount.put("adid", id);
					updateAccount.put("account", account);
					
					floorAccountMgr.updateAccount(updateAccount);
					
					result.put("success", String.valueOf(account));
				}else{
					
					result.put("success", param.getString("account"));
				}
			}else{
				
				result.remove("success");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.addAccountExcel() error:"+ e);
		}
	}
	
	private Map<String,String> accountExcelValidator(DBRow param) throws Exception{
		
		try{
			
			Map<String,String> result = new HashMap<String,String>();
			
			StringBuilder error = new StringBuilder();
			
			String account = param.getString("account");
			String password = param.getString("password");
			String employeName = param.getString("employeName");
			//String telephone = param.getString("telephone");
			String email = param.getString("email");
			
			String warehouse = param.getString("warehouse");
			String area = param.getString("area");
			String department = param.getString("department");
			String post = param.getString("post");
			
			//账号:账号是否重复,账号只能包含数字字母下划线,长度至少5位,最大12位
			if(!account.equals("")){
				if(floorAccountMgr.getAccountByAccount(account)!=null){
					
					error.append(",Account already exists");
				}
				if(!account.matches("^[a-zA-Z0-9_]+$")){
					
					error.append(",Account only letters (a-z) numbers and underscore");
				}
				if(account.length() < 5 || account.length() > 12){
					
					error.append(",Account use between 5 and 12 characters");
				}
			}
			
			//密码:长度至少5位,最大12位,密码只能包含数字字母特殊字符,不允许为空
			if(password.equals("")){
				
				error.append(",Password does not allow empty");
			}else{
				
				if(!password.matches("^[a-zA-Z0-9_.!@#$%^&*-]+$")){
					
					error.append(",Password only letters (a-z) numbers and special characters");
				}
				if(password.length() < 5 || password.length() > 12){
					
					error.append(",Password use between 5 and 12 characters");
				}
			}
			
			//姓名:不允许为空
			if(employeName.equals("")){
				
				error.append(",Full Name does not allow empty");
			}
			
			//邮件:格式验证,可以为空
			if((!email.equals("")) && !email.matches("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*")){
				
				error.append(",Email format is not recognized");
			}
			
			//仓库:查询是否存在此仓库
			if(warehouse.equals("")){
				
				error.append(",Warehouse does not allow empty");
				
			}else if(floorAccountMgr.getWarehouseByName(warehouse)==null){
				
				error.append(",Warehouse does not exist");
			}else{
				
				//区域:查询是否存在此区域,多区域
				if(!(area.equals(""))){
					
					String[] areaArray = area.split(",");
					
					DBRow wareRow = floorAccountMgr.getWarehouseByName(warehouse);
					
					for(String oneResutl:areaArray){
						
						if(floorAccountMgr.getWarehouseAreaByName(wareRow.get("id", 0),oneResutl)==null){
							
							error.append(",Area does not exist at this warehouse");
							break;
						}
					}
				}
			}
			
			//部门:查询是否存在此部门
			if(department.equals("")){
				
				error.append(",Department does not allow empty");
			}else if(floorAccountMgr.getDepartmentByName(department)==null){
				
				error.append(",Department does not exist");
			}
			
			//职位:查询是否存在此职位
			if(post.equals("")){
				
				error.append(",Post does not allow empty");
			}else if(floorAccountMgr.getPostByName(post)==null){
				
				error.append(",Post does not exist");
			}
			
			if(error.toString().length()>0){
				
				result.put("success", "false");
				result.put("error", error.toString().substring(1));
			}else{
				result.put("success", "true");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountExcelValidator(DBRow param) error:"+ e);
		}
	}
	
	private DBRow accountDataProcessing(DBRow param) throws Exception{
		
		try{
			
			DBRow result = new DBRow();
			
			Set<String> key = param.keySet();
			for (Iterator<String> it = key.iterator(); it.hasNext();) {
		        
				String oneKey = it.next().toString();
				//数据前后去空格
				result.put(oneKey, param.get(oneKey).toString().trim());
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountDataProcessing(DBRow param) error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> 导入title验证
	 * @param 文件流
	 * @return 验证结果
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,Object> validateImportTitle(Workbook workbook) throws Exception{
		
		try{
			
			DBRow[] rows = this.excelToDBRow(workbook);
			
			Map<String,Object> result = validateTitle(rows);
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.validateImportTitle(Workbook workbook) error:"+ e);
		}
	}
	
	/**
	 * 账号管理 >> 导入title
	 * @param 文件流
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void updateImportTitle(Workbook workbook) throws Exception{
		
		try{
			
			DBRow[] rows = this.excelToDBRow(workbook);
			
			for(DBRow oneResult:rows){
				
				String account = oneResult.getString("account");
				String title = oneResult.getString("title");
				
				if(floorAccountMgr.existAccountTitleRelevance(account,title) == null){
					
					DBRow titleRow = floorAccountMgr.getTitleByName(title);
					DBRow accountRow = floorAccountMgr.getAccountByAccount(account);
					
					floorAccountMgr.insertAccountTitle(accountRow.get("adid", -1), titleRow.get("title_id", -1));
				}
			}
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.updateImportTitle(Workbook workbook) error:"+ e);
		}
	}
	
	private DBRow[] excelToDBRow(Workbook workbook) throws Exception{
		
		try {
			
			List<DBRow> result = new ArrayList<DBRow>();
			
			Sheet rs = workbook.getSheet(0);
			int rsRows = rs.getRows();
			
			for(int i=1;i<rsRows;i++){
				
				String account = rs.getCell(0, i).getContents().trim();
				String title = rs.getCell(1, i).getContents().trim();
				
				if(!"".equals(account) || !"".equals(title)){
					
					DBRow row = new DBRow();
					row.add("account", account);
					row.add("title", title);
					row.add("number", i);
					result.add(row);
				}
			}
			
			return (result.toArray(new DBRow[0]));
			  
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.excelToDBRow(Workbook workbook) error:"+ e);
		}
	}
	
	private Map<String,Object> validateTitle(DBRow[] titles) throws Exception {
		
		try{
			
			Map<String,Object> result= new HashMap<String,Object>();
			boolean flag = true;
			String errorInfo = "";
			
			if(titles.length > 0){
				
				for(DBRow oneResult:titles){
					
					StringBuilder error = new StringBuilder();
					String status = "success";
					String account = oneResult.getString("account");
					String title = oneResult.getString("title");
					String number = oneResult.getString("number");
					
					//空值验证
					if(account.equals("")){
						
						error.append(",Enter account");
						status = "error";
					}else{
						
						if(title.equals("")){
							
							error.append(",Enter title");
							status = "error";
						}else{
							
							//重复验证
							for(DBRow secondRow:titles){
								
								if(account.equals(secondRow.getString("account")) && title.equals(secondRow.getString("title")) && !number.equals(secondRow.getString("number"))){
									
									error.append(",Repeat with line "+secondRow.getString("number"));
									status = "warn";
								}
							}
							
							//已经关联验证
							if(floorAccountMgr.existAccountTitleRelevance(account,title) != null){
								
								error.append(",Association exist");
								status = "warn";
							}
							
							//存在验证
							if(floorAccountMgr.getAccountByAccount(account) == null){
								
								error.append(",Account not found");
								status = "error";
							}
							
							if(floorAccountMgr.getTitleByName(title) == null){
								
								error.append(",Title not found");
								status = "error";
							}
						}
					}
					
					oneResult.add("status", status);
					
					if(error.toString().length()>0){
						
						oneResult.add("error", error.toString().substring(1));
					}else{
						oneResult.add("error", "");
					}
					
					if(status.equals("error")){
						flag = false;
					}
				}
				
				//如果全部是警告也不允许提交
				boolean warnAll = true;
				for(DBRow oneResult:titles){
					
					if(oneResult.getString("status").equals("error") || oneResult.getString("status").equals("success")){
						
						warnAll = false;
						break;
					}
				}
				
				if(warnAll){
					
					flag = false;
				}
				
			}else{
				flag = false;
				errorInfo = "Data is null";
			}
			
			result.put("data", titles);
			result.put("success", flag);
			result.put("error",errorInfo);
			
			return result;
		
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.validateTitle(DBRow[] titles) error:"+ e);
		}
	}
	
	/**
	 * 首页：正常登录
	 * @param 用户名,密码,验证码 
	 * @return {adminBean,success,errorInfo}
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,Object> accountLogin(Map<String,Object> params) throws Exception{
		
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			
			DBRow admin = floorAccountMgr.getAccountByAccount(params.get("account").toString());
			//验证：账号密码是否正确
			if(admin != null && admin.getString("pwd").equals(StringUtil.getMD5(params.get("pwd").toString()))){
				
				//验证：是否锁定
				if(admin.get("llock", 0) == 1){
					
					result.put("success", false);
					result.put("errorInfo","Account locked");
				}else{
					
					//设置值
					String lastLoginDate = DateUtil.NowStr();
					
					AdminLoginBean adminBean = this.getAdminLoginBean(params.get("adminBean"));
					
					adminBean.setAccount(admin.get("account", "-1"));
					adminBean.setAdid(admin.get("adid", 0l));
					adminBean.setEmail(admin.getString("email"));
					adminBean.setLoginDate(lastLoginDate);
					adminBean.setEmploye_name(admin.getString("employe_name"));
					adminBean.setTitles(floorAccountMgr.getAdminHasTitleList(admin.get("adid", 0l), ""));
					adminBean.setIsLogin();
					
					adminBean.setCorporationId(admin.get("corporation_id",0));
					adminBean.setCorporationType(admin.get("corporation_type",0));
					
					adminBean.setDepartment(floorAccountMgr.getAccountDepartmentAndPost(admin.get("adid", 0l)));
					adminBean.setWarehouse(floorAccountMgr.getAccountWarehouseAndArea(admin.get("adid", 0l)));
					
					if(adminBean.getWarehouse()!=null && adminBean.getWarehouse().length > 0){
						
						adminBean.setPs_id(adminBean.getWarehouse()[0].get("WAREID",-1));
					}
					
					//是否是管理员
					adminBean.setAdministrator(this.isAdministrator(adminBean));
					
					//更新最后登录日期
					DBRow updateRow = new DBRow();
					updateRow.add("last_login_date",lastLoginDate);
					updateRow.add("adid", admin.get("adid", 0l));
					//floorAccountMgr.updateAccount(updateRow);
					
					//返回值
					result.put("adminBean", adminBean);
					result.put("success", true);
					result.put("errorInfo","");
				}
				
			}else{
				result.put("success", false);
				result.put("errorInfo","Account or password error");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountLogin(Map<String,String> params) error:"+ e);
		}
	}
	
	/**
	 * 令牌验证
	 * @param 令牌ID,令牌口令
	 * @return boolean
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	private boolean validateToken(String tokenId,String tokenSn) throws Exception {
		
		try {
			boolean result = false;
			
			if(tokenId != null && tokenSn != null){
				
				DBRow tokeRow = floorAccountMgr.getTokenById(tokenId);
				
				if(tokeRow != null){
					
					seamoonapi seamoon = new seamoonapi();
					
					String validateResult = seamoon.checkpassword(tokeRow.getString("sninfo"),tokenSn);
					
					if(validateResult.length()>3){
						
						DBRow updateTokeRow = new DBRow();
						updateTokeRow.add("sn", tokenId);
						updateTokeRow.add("sninfo", validateResult);
						//更新SNINFO
						floorAccountMgr.updateSninfo(updateTokeRow);
						
						result = true;
					}
				}
				
			}
			
			return result;
			
		} catch (Exception e) {
			
			throw new Exception("AccountMgrSbb.validateToken(String tokenId,String tokenSn) error:"+ e);
		}
	}
	
	/**
	 * 首页：内部登录非第三方
	 * @param 用户名,动态口令
	 * @return {adminBean,success,errorInfo}
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,Object> accountInternaLoginNoThird(Map<String,Object> params) throws Exception{
		
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			
			DBRow admin = floorAccountMgr.getAccountByAccount(params.get("userName").toString());
			
			//验证：账号令牌是否正确
			if(admin != null ){
				
				//验证：令牌
				if(this.validateToken(admin.getString("register_token"),params.get("tokenSn").toString())){
					
					//验证：是否锁定
					if(admin.get("llock", 0) == 1){
						
						result.put("success", false);
						result.put("errorInfo","账号已被锁定");
					}else{
						
						//设置值
						String lastLoginDate = DateUtil.NowStr();
						
						AdminLoginBean adminBean = this.getAdminLoginBean(params.get("adminBean"));
						
						adminBean.setAccount(admin.get("account", "-1"));
						adminBean.setAdid(admin.get("adid", 0l));
						adminBean.setEmail(admin.getString("email"));
						adminBean.setLoginDate(lastLoginDate);
						adminBean.setEmploye_name(admin.getString("employe_name"));
						adminBean.setTitles(floorAccountMgr.getAdminHasTitleList(admin.get("adid", 0l), ""));
						adminBean.setIsLogin();
						
						adminBean.setCorporationId(admin.get("corporation_id",0));
						adminBean.setCorporationType(admin.get("corporation_type",0));
						
						adminBean.setDepartment(floorAccountMgr.getAccountDepartmentAndPost(admin.get("adid", 0l)));
						adminBean.setWarehouse(floorAccountMgr.getAccountWarehouseAndArea(admin.get("adid", 0l)));
						
						if(adminBean.getWarehouse()!=null && adminBean.getWarehouse().length > 0){
							
							adminBean.setPs_id(adminBean.getWarehouse()[0].get("WAREID",-1));
						}
						
						//是否是管理员
						adminBean.setAdministrator(this.isAdministrator(adminBean));
						
						//更新最后登录日期
						DBRow updateRow = new DBRow();
						updateRow.add("last_login_date",lastLoginDate);
						updateRow.add("adid", admin.get("adid", 0l));
						floorAccountMgr.updateAccount(updateRow);
						
						//返回值
						result.put("adminBean", adminBean);
						result.put("success", true);
						result.put("errorInfo","");
					}
					
				} else {
					result.put("success", false);
					result.put("errorInfo","令牌口令错误");
				}
			}else{
				result.put("success", false);
				result.put("errorInfo","用户名错误");
			}
			
			return result;
			
		}catch (Exception e){
			
			throw new Exception("AccountMgrSbb.accountInternaLoginNoThird(Map<String,String> params) error:"+ e);
		}
	}

	/**
	 * 首页：内部登录第三方
	 * @param 用户名,密码,见证人用户名,见证人口令
	 * @return {adminBean,success,errorInfo}
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,Object> accountInternaLoginThird(Map<String,Object> params) throws Exception{
		
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			
			DBRow admin = floorAccountMgr.getAccountByAccount(params.get("userName").toString());
			DBRow witness = floorAccountMgr.getAccountByAccount(params.get("witnessUserName").toString());
			
			//验证：账号密码是否正确
			if(admin != null && admin.getString("pwd").equals(StringUtil.getMD5(params.get("pwd_user").toString()))){
				
				if(witness != null && this.validateToken(witness.getString("register_token"),params.get("witnessTokenSn").toString())){
					
					//验证：是否锁定
					if(admin.get("llock", 0) == 1){
						
						result.put("success", false);
						result.put("errorInfo","账号已被锁定");
					}else{
						
						//设置值
						String lastLoginDate = DateUtil.NowStr();
						
						AdminLoginBean adminBean = this.getAdminLoginBean(params.get("adminBean"));
						
						adminBean.setAccount(admin.get("account", "-1"));
						adminBean.setAdid(admin.get("adid", 0l));
						adminBean.setEmail(admin.getString("email"));
						adminBean.setLoginDate(lastLoginDate);
						adminBean.setEmploye_name(admin.getString("employe_name"));
						adminBean.setTitles(floorAccountMgr.getAdminHasTitleList(admin.get("adid", 0l), ""));
						adminBean.setIsLogin();
						
						adminBean.setCorporationId(admin.get("corporation_id",0));
						adminBean.setCorporationType(admin.get("corporation_type",0));
						
						adminBean.setDepartment(floorAccountMgr.getAccountDepartmentAndPost(admin.get("adid", 0l)));
						adminBean.setWarehouse(floorAccountMgr.getAccountWarehouseAndArea(admin.get("adid", 0l)));
						
						if(adminBean.getWarehouse()!=null && adminBean.getWarehouse().length > 0){
							
							adminBean.setPs_id(adminBean.getWarehouse()[0].get("WAREID",-1));
						}
						
						//是否是管理员
						adminBean.setAdministrator(this.isAdministrator(adminBean));
						
						//更新最后登录日期
						DBRow updateRow = new DBRow();
						updateRow.add("last_login_date",lastLoginDate);
						updateRow.add("adid", admin.get("adid", 0l));
						floorAccountMgr.updateAccount(updateRow);
						
						//返回值
						result.put("adminBean", adminBean);
						result.put("success", true);
						result.put("errorInfo","");
					}
					
				}else{
					
					result.put("success", false);
					result.put("errorInfo","见证人用户名或口令错误");
				}
				
			}else{
				result.put("success", false);
				result.put("errorInfo","用户名或密码错误");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountInternaLoginThird(Map<String,String> params) error:"+ e);
		}
	}
	
	/**
	 * 首页：供应商登录
	 * @param 账号,密码
	 * @return {adminBean,success,errorInfo}
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public Map<String,Object> accountSupplierLogin(Map<String,Object> params) throws Exception{
		
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			
			DBRow admin = floorAccountMgr.getAccountByAccount(params.get("account").toString());
			//验证：账号密码是否正确
			if(admin != null && admin.getString("pwd").equals(StringUtil.getMD5(params.get("pwd").toString()))){
				
				//验证：是否锁定
				if(admin.get("llock", 0) == 1){
					
					result.put("success", false);
					result.put("errorInfo","账号已被锁定");
				}else{
					
					//设置值
					String lastLoginDate = DateUtil.NowStr();
					
					AdminLoginBean adminBean = this.getAdminLoginBean(params.get("adminBean"));
					
					adminBean.setAccount(admin.get("account", "-1"));
					adminBean.setAdid(admin.get("adid", 0l));
					adminBean.setEmail(admin.getString("email"));
					adminBean.setLoginDate(lastLoginDate);
					adminBean.setEmploye_name(admin.getString("employe_name"));
					adminBean.setTitles(floorAccountMgr.getAdminHasTitleList(admin.get("adid", 0l), ""));
					adminBean.setIsLogin();
					
					adminBean.setCorporationId(admin.get("corporation_id",0));
					adminBean.setCorporationType(admin.get("corporation_type",0));
					
					adminBean.setDepartment(floorAccountMgr.getAccountDepartmentAndPost(admin.get("adid", 0l)));
					adminBean.setWarehouse(floorAccountMgr.getAccountWarehouseAndArea(admin.get("adid", 0l)));
					
					if(adminBean.getWarehouse()!=null && adminBean.getWarehouse().length > 0){
						
						adminBean.setPs_id(adminBean.getWarehouse()[0].get("WAREID",-1));
					}
					
					//是否是管理员
					adminBean.setAdministrator(this.isAdministrator(adminBean));
					
					//更新最后登录日期
					DBRow updateRow = new DBRow();
					updateRow.add("last_login_date",lastLoginDate);
					updateRow.add("adid", admin.get("adid", 0l));
					floorAccountMgr.updateAccount(updateRow);
					
					//返回值
					result.put("adminBean", adminBean);
					result.put("success", true);
					result.put("errorInfo","");
				}
				
			}else{
				result.put("success", false);
				result.put("errorInfo","用户名或密码错误");
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.accountSupplierLogin(Map<String,String> params) error:"+ e);
		}
	}
	
	public boolean isAdministrator(AdminLoginBean adminBean){
		
		boolean result = false;
		
		for(DBRow oneResult : adminBean.getDepartment()){
			
			if(oneResult.get("deptid", 0) == 10000 ){
				
				result = true;
			}
		}
		
		return result;
	}

	public AdminLoginBean getAdminLoginBean(Object param){
		
		AdminLoginBean adminBean = new AdminLoginBean();
		
		if(param != null){
			
			if(param instanceof AdminLoginBean){
				
				adminBean = (AdminLoginBean)param;
				
			}else if(param instanceof Map){
				
				@SuppressWarnings("unchecked")
				Map<String,Object> adminMap = (Map<String,Object>)param;
				
				adminBean.setAccount(adminMap.get("account") == null ? null : adminMap.get("account").toString());
				adminBean.setAdid(adminMap.get("adid") == null ? null : Long.valueOf(adminMap.get("adid").toString()));
				adminBean.setCity(adminMap.get("city") == null ? null : Long.valueOf(adminMap.get("city").toString()));
				adminBean.setEmail(adminMap.get("email") == null ? null : adminMap.get("email").toString());
				adminBean.setEmploye_name(adminMap.get("employe_name") == null ? null : adminMap.get("employe_name").toString());
				adminBean.setLoginDate(adminMap.get("loginDate") == null ? null : adminMap.get("loginDate").toString());
				adminBean.setProvince(adminMap.get("province") == null ? null : Long.valueOf(adminMap.get("province").toString()));
				adminBean.setPs_id(adminMap.get("ps_id") == null ? null : Long.valueOf(adminMap.get("ps_id").toString()));
				adminBean.setAdministrator(adminMap.get("administrator") == null ? null : Boolean.valueOf(adminMap.get("administrator").toString()));
				
				adminBean.setCorporationId(adminMap.get("corporationId") == null ? 0 : Long.valueOf(adminMap.get("corporationId").toString()));
				adminBean.setCorporationType(adminMap.get("corporationType") == null ? 0 : Long.valueOf(adminMap.get("corporationType").toString()));
				
				if(Boolean.valueOf(adminMap.get("login") == null ? null : adminMap.get("login").toString())){
					
					adminBean.setIsLogin();
				}
				
				if(Boolean.valueOf(adminMap.get("loginRightPath") == null ? null : adminMap.get("loginRightPath").toString())){
					
					adminBean.setIsLoginRightPath();
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> titles = adminMap.containsKey("titles") ? (List<Map<String,Object>>)adminMap.get("titles") : null;
				
				if(titles != null){
					
					DBRow[] titlesArray = new DBRow[titles.size()];
					
					for(int i=0; i<titlesArray.length; i++){
						
						titlesArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: titles.get(i).entrySet()){
							
							titlesArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setTitles(titlesArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> depts = adminMap.containsKey("department") ? (List<Map<String,Object>>)adminMap.get("department") : null;
				
				if(depts != null){
					
					DBRow[] deptsArray = new DBRow[depts.size()];
					
					for(int i=0; i<deptsArray.length; i++){
						
						deptsArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: depts.get(i).entrySet()){
							
							deptsArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setDepartment(deptsArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> wares = adminMap.containsKey("warehouse") ? (List<Map<String,Object>>)adminMap.get("warehouse") : null;
				
				if(wares != null){
					
					DBRow[] waresArray = new DBRow[wares.size()];
					
					for(int i=0; i<waresArray.length; i++){
						
						waresArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: wares.get(i).entrySet()){
							
							waresArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setWarehouse(waresArray);
				}
			}
		}
		
		return adminBean;
	}
	
	public DBRow[] getAccountNavigationBar(long adid) throws Exception{
		
		try{
			DBRow[] ownUrlAndAction = getUserMenuRecordByAdid(adid);
			HashMap<String, DBRow> tileMenuTree = new HashMap<String, DBRow>();
			genTileMenuTreeFromMenuRecord(ownUrlAndAction, tileMenuTree);
			
			return getAccountNavigationBarFromTileMenuTree(tileMenuTree);
			
			//return floorAccountMgr.getAccountNavigationMenu(adid,0L);
//			return getAccountNavigationParentMenus(adid);
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountNavigationBar(long adid) error:"+ e);
		}
	}
	
	
	public DBRow[] getAccountNavigationParentMenus(long adid) throws Exception
	{
		Set<String> parentIds = new HashSet<String>();
		List<DBRow> parent_menus = new ArrayList<DBRow>();
		DBRow[] menus = floorAccountMgr.findAccountRoleAndSelfMenuRights(adid, -1, -1);
		for (int i = 0; i < menus.length; i++) 
		{
			DBRow parentMenu = getNavigationMenuTopParentByCtid(menus[i].get("id", 0L));
			if(parentMenu != null && !parentIds.contains(parentMenu.getString("id")))
			{
				parentIds.add(parentMenu.getString("id"));
				parent_menus.add(parentMenu);
			}
		}
		DBRow[] actions = floorAccountMgr.findAccountRoleAndSelfActionRights(adid, -1);
		for (int i = 0; i < actions.length; i++) 
		{
			DBRow parentMenu = getNavigationMenuTopParentByCtid(actions[i].get("page", 0L));
			if(parentMenu != null && !parentIds.contains(parentMenu.getString("id")))
			{
				parentIds.add(parentMenu.getString("id"));
				parent_menus.add(parentMenu);
			}
		}
		DBRow[] parentMenus = parent_menus.toArray(new DBRow[0]);
		for (int i = 0; i < parentMenus.length; i++)
		{
			DBRow[] subs = new DBRow[0];
			parentMenus[i].add("subMenu", subs);
			if(subs.length == 0)
			{
				continue;
			}
		}
		return parentMenus;
	}
	
	
	/**
	 * 获得账号菜单的父菜单
	 * @param adid
	 * @param ctid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月22日 下午3:10:43
	 */
	public DBRow getNavigationMenuTopParentByCtid(long ctid) throws Exception
	{
		try
		{
			//获取action信息，getAuthenticationActionById
			DBRow menu = floorAccountMgr.getTurboshopControlTreeById(ctid);
			long ctid_parent	= menu.get("parentid", 0L);
			if(ctid_parent != 0)
			{
				menu = getNavigationMenuTopParentByCtid(ctid_parent);
			}
			return menu;
		}
		catch (Exception e)
		{
			throw new Exception("AccountMgrSbb.getAccountNavigationMenus(long adid) error:"+ e);
		}
	}
	
	public String getAccountDefaultHomePage(long adid) throws Exception{
		
		try{
			String returnVal = "task/new_task_list.html";
			
			//查询个人主页
			DBRow result = floorAccountMgr.getAccountDefaultHomePage(adid);
			
			if(result != null){
				
				returnVal = result.getString("home_page");
			}else{
				
				//查询所属部门主页 取top1
				
				DBRow[] deptsRow = floorAccountMgr.getAccountDepartmentAndPost(adid);
				
				if(deptsRow!= null && deptsRow.length > 0){
					
					for(DBRow oneResult:deptsRow){
						
						DBRow dept = floorAccountMgr.getDepartmentById(oneResult.get("deptid", -1L));
						
						if(!dept.get("index_page","").equals("")){
							
							returnVal = dept.get("index_page","");
							
							break;
						}
					}
				}
			}
			
			return returnVal;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getAccountDefaultHomePage(long adid) error:"+ e);
		}
	}
	
	
	private DBUtilAutoTran dbUtilAutoTran;
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	public static final long MENU_ROOT = 0L;
	/**
	 * 通过adid获取用户权限的url和action
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public DBRow[] getUserMenuRecordByAdid(long adid) throws Exception {
		/*StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT DISTINCT * FROM (")
				 .append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("	      SELECT DISTINCT tctm.ctid FROM turboshop_control_tree_map AS tctm INNER JOIN(SELECT * FROM admin_department WHERE adid = "+ adid +") AS ad ON tctm.adgid = ad.department_id ")
				 .append("  UNION ALL ")
				 .append("		  SELECT ctid FROM turboshop_control_tree_map_extend WHERE adid = "+adid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ")
				 .append("UNION ALL  ")
				 .append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("	      SELECT DISTINCT aa.page ctid FROM admin_group_auth aga JOIN admin_department ad ON ad.department_id = aga.adgid JOIN authentication_action aa ON aa.ataid = aga.ataid WHERE ad.adid = "+ adid)
				 .append("	UNION ALL ")
				 .append("		  SELECT DISTINCT aa.page ctid FROM turboshop_admin_group_auth_extend tagae JOIN authentication_action aa ON aa.ataid = tagae.ataid WHERE tagae.adid = "+ adid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ")
				 .append(") temp ")
				 .append(" WHERE temp.control_type = 1 ")
				 .append(" ORDER BY parentid, sort");
				 //.append(" GROUP BY parentid ORDER BY parentid, sort");*/
		
		try
	    {
	      return this.dbUtilAutoTran.selectMutliple(getMenuSql(adid, 1));
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getAccountNavigationMenu error:" + e);
	    }
	}
		
	/**
	 * 根据adid获取移动端权限的平铺
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年2月10日
	 * @author  lixf
	 */
	@Override
	public DBRow[] getUserMobileMenuRecordByAdid(long adid) throws Exception {
		
		HashMap<String, DBRow> tileMenuTree = new HashMap<String, DBRow>();
		
		try
	    {
			DBRow[] menuRecord = this.dbUtilAutoTran.selectMutliple(getMenuSql(adid, 2));
			
			genTileMenuTreeFromMenuRecord(menuRecord, tileMenuTree);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getUserMobileMenuRecordByAdid error:" + e);
	    }
		
		DBRow[] permissions = tileMenuTree.values().toArray(new DBRow[tileMenuTree.values().size()]);
		if(permissions!=null && permissions.length>0){
			for(DBRow permission : permissions){
				permission.remove("control_type");
				permission.remove("sort");
				permission.remove("link");
			}
		}
		
		return permissions;
	}
	
	/**
	 * 获取查询权限sql
	 * @param adid
	 * @param control_type {1:web端权限，2：手持端权限}
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年2月10日
	 * @author  lixf
	 */
	private String getMenuSql(long adid, long control_type){
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT DISTINCT * FROM (")
				 .append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type, ")
				 .append("	     tct.enabled, ")
				 .append("	     tct.ico ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("	      SELECT DISTINCT tctm.ctid FROM turboshop_control_tree_map AS tctm INNER JOIN(SELECT * FROM admin_department WHERE adid = "+ adid +") AS ad ON tctm.adgid = ad.department_id ")
				 .append("  UNION ALL ")
				 .append("		  SELECT ctid FROM turboshop_control_tree_map_extend WHERE adid = "+adid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ")
				 .append("UNION ALL  ")
				 .append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type, ")
				 .append("	     tct.enabled, ")
				 .append("	     tct.ico ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("	      SELECT DISTINCT aa.page ctid FROM admin_group_auth aga JOIN admin_department ad ON ad.department_id = aga.adgid JOIN authentication_action aa ON aa.ataid = aga.ataid WHERE ad.adid = "+ adid)
				 .append("	UNION ALL ")
				 .append("		  SELECT DISTINCT aa.page ctid FROM turboshop_admin_group_auth_extend tagae JOIN authentication_action aa ON aa.ataid = tagae.ataid WHERE tagae.adid = "+ adid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ")
				 .append(") temp ")
				 .append(" WHERE temp.control_type = "+ control_type + " and temp.enabled=1 ")
				 .append(" ORDER BY parentid, sort"); 
		return sqlBuffer.toString();
	}
	
	/**
	 * 通过用户url和action记录生成平铺树
	 * @param ownUrlAndActions 用户url和action记录
	 * @param tileMenuTree 存放平铺树的map
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public void genTileMenuTreeFromMenuRecord(DBRow[] ownUrlAndActions, HashMap<String, DBRow> tileMenuTree){
		
		if(ownUrlAndActions != null){
			for(DBRow node : ownUrlAndActions){
				long id = node.get("id", -1);
				tileMenuTree.put(id+"", node);
				
				// TODO 向上查找父节点直到跟节点
				long parentId = node.get("parentid", -1);
				if(parentId!=-1 && parentId!=MENU_ROOT && !tileMenuTree.containsKey(parentId)){
					try {
						getNodeAncestor(parentId, tileMenuTree);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				// TODO 向下构建树 并放入Map
				try {
					getDescendant(id, tileMenuTree);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 向上查找父节点直到跟节点
	 * @param parentId
	 * @param tileMenuTree
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public void getNodeAncestor(long parentId, HashMap<String, DBRow> tileMenuTree) throws Exception {
		StringBuffer chkParentNodeSql = new StringBuffer();
		
		chkParentNodeSql.append("SELECT tct.id, ")
						.append("	    tct.parentid, ")
						.append("	    tct.sort, ")
						.append("	    tct.title, ")
						.append("	    tct.link, ")
						.append("	    tct.control_type, ")
						.append("	    tct.ico ")
						.append("  FROM turboshop_control_tree AS tct  ")
						.append(" WHERE tct.id = "+ parentId)
						.append("   AND tct.enabled=1");
		
		DBRow parentNode;
		
		try
	    {
			parentNode = this.dbUtilAutoTran.selectSingle(chkParentNodeSql.toString());
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getNodeAncestor error:" + e);
	    }
		
		if(parentNode != null){
			String id = parentNode.getString("id");
			tileMenuTree.put(id, parentNode);
			
			long grandParentId = parentNode.get("parentid", -1);
			if(grandParentId!=-1 && grandParentId!=MENU_ROOT && !tileMenuTree.containsKey(grandParentId)){
				getNodeAncestor(grandParentId, tileMenuTree);
			}
		}
	}
	
	/**
	 * 向下构建树
	 * @param id
	 * @param tileMenuTree
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public void getDescendant(long id, HashMap<String, DBRow> tileMenuTree) throws Exception {
		StringBuffer chkChildNodeSql = new StringBuffer();
		
		chkChildNodeSql.append("SELECT tct.id, ")
					   .append("	   tct.parentid, ")
					   .append("	   tct.sort, ")
					   .append("	   tct.title, ")
					   .append("	   tct.link, ")
					   .append("	   tct.control_type, ")
					   .append("	   tct.ico ")
					   .append("  FROM turboshop_control_tree AS tct  ")
					   .append(" WHERE tct.parentid = "+ id)
					   .append("   AND tct.enabled=1")
					   .append(" ORDER BY sort"); 
		
		DBRow[] childNodes;
		
		try
	    {
			childNodes = this.dbUtilAutoTran.selectMutliple(chkChildNodeSql.toString());
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getDescendant error:" + e);
	    }
		
		if(childNodes!=null && childNodes.length>0){
			for(DBRow childNode : childNodes){
				if(childNode != null){
					long childid = childNode.get("id", -1);
					if(childid!=-1 && !tileMenuTree.containsKey(childid)){
						tileMenuTree.put(childid+"", childNode);
						getDescendant(childid, tileMenuTree);
					}
				}
			}
		}
	}
	
	/**
	 * 通过adgid获取有权限的url和action
	 * @param adgid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public DBRow[] getRolePageRecord(long adgid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("	      SELECT DISTINCT tctm.ctid FROM turboshop_control_tree_map AS tctm WHERE tctm.adgid = "+ adgid)
				 .append("  UNION ALL ")
				 .append("	      SELECT DISTINCT aa.page ctid FROM admin_group_auth aga JOIN authentication_action aa ON aa.ataid = aga.ataid WHERE aga.adgid = "+ adgid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ");
		
		try
	    {
	      return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getAccountNavigationMenu error:" + e);
	    }
	}
	
	/**
	 * 通过adid获取用户扩展权限的url和action
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public DBRow[] getUserExtendPageRecord(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT tct.id, ")
				 .append("	     tct.parentid, ")
				 .append("	     tct.sort, ")
				 .append("	     tct.title, ")
				 .append("	     tct.link, ")
				 .append("	     tct.control_type ")
				 .append("  FROM turboshop_control_tree AS tct ")
				 .append(" INNER JOIN( ")
				 .append("		  SELECT ctid FROM turboshop_control_tree_map_extend WHERE adid = "+adid)
				 .append("	UNION ALL ")
				 .append("		  SELECT DISTINCT aa.page ctid FROM turboshop_admin_group_auth_extend tagae JOIN authentication_action aa ON aa.ataid = tagae.ataid WHERE tagae.adid = "+ adid)
				 .append(" )AS adtct ON tct.id = adtct.ctid ");
		
		try
	    {
	      return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorAccountMgrSbb.getAccountNavigationMenu error:" + e);
	    }
	}
	
	/**
	 * 将平铺树生成菜单树
	 *   【！】目前此方法只支持2层菜单树
	 * @param tileMenuTree
	 * @return
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	private DBRow[] getAccountNavigationBarFromTileMenuTree(HashMap<String, DBRow> tileMenuTree){
		
		//DBRow menu = new DBRow();
		TreeMap<Long, DBRow> firstLevel = new TreeMap<Long, DBRow>();
		//menu.add("first", firstLevel);
		
		for(DBRow node : tileMenuTree.values()){
			if(node.get("parentid", -1L) == 0){
				firstLevel.put(node.get("sort", -1L), node);
			}
		}
		
		if( firstLevel.size()>0 ){
			
			for(DBRow parentnode : firstLevel.values()){
				TreeMap<Long, DBRow> submenu = new TreeMap<Long, DBRow>();
				
				for(DBRow node : tileMenuTree.values()){
					if(node.get("parentid", -1L) == parentnode.get("id", -1L)){
						submenu.put(node.get("sort", -1L), node);
					}
				}
				
				if(submenu.size() > 0){
					parentnode.add("submenu", submenu.values().toArray(new DBRow[submenu.values().size()]));
				}
			}
		}
		
		return firstLevel.values().toArray(new DBRow[firstLevel.values().size()]);
	}
	
	/**
	 * 获取adgid或adid授权的需验证的Action
	 * @param sql
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年1月28日
	 * @author  lixf
	 */
	public DBRow[] getRoleOrUserNeed2AuthActions(String sql) throws Exception
	{
		try
		{
			DBRow authRows[] = dbUtilAutoTran.selectMutliple(sql);
			
			return(authRows);
		}
		catch (Exception e)
		{
			log.error("AccountMgrSbb.getRoleOrUserNeed2AuthActions(sql) error:" + e);
			throw new Exception("getRoleOrUserNeed2AuthActions(sql) error:" + e);
		}
	}
	
	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	/**
	 * 文件上传
	 * @param HttpServletRequest
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	/*public DBRow uploadFile(HttpServletRequest request)throws Exception{
		
		try {
			
			long seq = floorAccountMgr.getSequance("upload_image_seq");
			
			String path = "/Sync10/upload/account/"+String.valueOf(seq)+".jpg";
			
			String fileUploadPath = Environment.getHome().replace("\\", "/")+"."+ "/upload/account/";
			
	        ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
	        
	        @SuppressWarnings("unchecked")
			List<FileItem> items = uploadHandler.parseRequest(request);
	        
	        DBRow result = new DBRow();
	        
	        //只上传一张图片
	        for (FileItem item : items) {
	        	
	            if (!item.isFormField()) { 
	            	
	                File file = new File(fileUploadPath, String.valueOf(seq)+".jpg");
	                item.write(file);
	                
	                result.put("file_path", path);
	                result.put("file_name", item.getName());
	            }
	        }
	        
	        return result;
			
		} catch (Exception e) {
			throw new Exception("AccountMgrSbb.uploadFile() error:" + e);
		}
	}*/
	
	public String[] changeTofficialFile(DBRow param) throws Exception {
		
		String[] result = null;
		
		long seq = floorAccountMgr.getSequance("upload_image_seq");
		
		String accountPath = Environment.getHome().replace("\\", "/")+"."+ "/upload/account/";
		
		//文件夹路径
		String filePath = param.get("filePath", "-1");
		//读取文件夹下的文件
		String[] file = FileUtil.getFileList(filePath);
		
		if(file!=null&&file.length>0){
			
			result = new String[file.length];
			
			for(int i=0;i<file.length;i++){
				
				//复制文件到帐号目录
				FileUtil.copyFileAndRename(filePath+"/"+file[i],accountPath,String.valueOf(seq)+".jpg");
				
				result[i]=String.valueOf(seq)+".jpg";
			}
		}
		
		return result;
	}
	/**
	 * 接收android请求参数
	 * @param HttpServletRequest
	 * @return DBRow形式的请求参数
	 * @since Sync10-ui 1.0
	 * @author unknown
	**/
	public DBRow getRequestData(HttpServletRequest request) throws IOException,Exception{
		
		DBRow returnRow = new DBRow();
		boolean isMutilRequest = ServletFileUpload.isMultipartContent(request);
		
		if(isMutilRequest){
			
			ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
			@SuppressWarnings("unchecked")
		    List<FileItem> items = uploadHandler.parseRequest(request);
		    List<FileItem> files = new ArrayList<FileItem>();
		    for(FileItem item : items){
		    	if(item.isFormField()){
		    		returnRow.add(item.getFieldName(), item.getString());
		    	}else{
		    		files.add(item);
		    	}
		    	 //这里解析zip文件然后，保存在temp文件夹下面
		    	if(files.size() == 1){
		    		returnRow.add("filePath", FileUtil.saveZipFile(files.get(0).getInputStream(), "_portrait", "_portrait.zip"));
		    	}
		    }
		    
		    returnRow.add("files", files);
		    
		}else{
			
			Enumeration<String>  enumer = request.getParameterNames();
			while(enumer != null && enumer.hasMoreElements()){
				
				String key = enumer.nextElement();
				returnRow.add(key, StringUtil.getString(request, key));
			}
		}
		
 	    return returnRow;
	}
	
	
	/**
	 * 账号管理 >> 查询所属部门的职务
	 * @param 部门ID[deptId]
	 * @return
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getPostByDeptId(long deptId) throws Exception{
		
		try{
			
			DBRow[] result = floorAccountMgr.getAccountPostByDeptId(deptId);
			
			return result;
			
		}catch (Exception e){
			throw new Exception("AccountMgrSbb.getPostByDeptId(long deptId) error:"+ e);
		}
	}
	/**
	 * 刷新openFire人员
	 * @param adid
	 * @author chenchen
	 */
	public void refreshOpenfireMember(long adid, String account) throws Exception{
		try {
			DBRow[] rows = floorAccountMgr.getAccountDepartmentAndPost(adid);
			Set<String> deptSet = new HashSet<String>();
			for (DBRow r : rows) {
				deptSet.add(r.getString("deptname"));
			}
			String deptStr = deptSet.toString();
			deptStr = deptStr.substring(1, deptStr.length()-1).replaceAll(",\\s+", ",");
			String urlStr = "http://"+openFireSetBean.getAddress()+":9090/plugins/createduserinsertcache/insertusercache?userName="+account+"&groupNames="+deptStr;
			//仅向openfire发送刷新请求,不关心返回结果
			URL url = new URL(urlStr);
			URLConnection connection = url.openConnection();
			InputStream in  = connection.getInputStream();
			in.close();	
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过账号ID，查询账号角色和职务
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:28:52
	 */
	public DBRow[] findAdminDepartmentPosts(long adid) throws Exception
	{
		try
		{
			return floorAccountMgr.getAccountDepartmentAndPost(adid);
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.findAdminDepartmentPosts(long adid) error:"+ e);
		}
	}
	
	/**
	 * 通过账号ID查询账号仓库和区域
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:29:13
	 */
	public DBRow[] findAdminWarehouseAreas(long adid) throws Exception
	{
		try
		{
			return floorAccountMgr.getAccountWarehouseAndArea(adid);
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.findAdminWarehouseAreas(long adid) error:"+ e);
		}
	}
	/**
	 * 通过账号ID查询账号所有图像信息
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:29:13
	 */
	public DBRow[] findAccountPortrait(long adid) throws Exception
	{
		try
		{
			return floorAccountMgr.getPersonalPortrait(adid);
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.findAccountPortrait(long adid) error:"+ e);
		}
	}
	/**
	 * 通过账号ID查询账号信息
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:29:13
	 */
	public DBRow GetUserInfoByAccount(String account) throws Exception
	{
		try
		{
			return floorAccountMgr.getAccountByAccount(account);
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.GetUserInfoByAccount(long adid) error:"+ e);
		}
	}
	
	/**
	 * 查询customerID
	 */
	public DBRow[] getCustomerId() throws Exception {
		try
		{
			return floorAccountMgr.getCustomerId();
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.getCustomerId() error:"+ e);
		}
	}
	
	public DBRow[] getCustomerAddressByCustomerKey(long customer_key) throws Exception {
		try
		{
			return floorAccountMgr.getCustomerAddressByCustomerKey(customer_key);
		}
		catch (Exception e){
			throw new Exception("AccountMgrSbb.getCustomerAddressByCustomerKey(long customer_key) error:"+ e);
		}
	}
	
	public DBRow[] getCustomerTitleRelation(Map<String,Object> param) throws Exception {
		
		try {
			
			DBRow[] title = floorAccountMgr.getTitle();
			
			for(DBRow one : title){
				
				DBRow[] customer = floorAccountMgr.getCustomerByTitle(one.get("title_id",0));
				
				DBRow[] category = floorAccountMgr.getCategoryByTitle(one.get("title_id",0));
				
				DBRow[] line = floorAccountMgr.getLineByTitle(one.get("title_id",0));
				
				one.add("customer", customer);
				one.add("category", category);
				one.add("line", line);
			}
			
			return title;
		}
		catch (Exception e){
			
			throw new Exception("AccountMgrSbb.getCustomerTitleRelation(Map<String,Object> param) error:"+ e);
		}
	}
	
	/**
	 * 功能：根据warehouse_id查询仓库下所有的账号
	 * @param warehouse_id
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年5月12日
	 */
	public DBRow[] findAllAccountByWarehouseId(long warehouse_id) throws Exception {
		DBRow[] accountInfos = null;
		
		try{
			accountInfos = floorAccountMgr.findAllAccountByWarehouseId(warehouse_id);
			
			if(accountInfos!=null && accountInfos.length>0){
				DBRow tempCtn = new DBRow();
				
				for(DBRow accountInfo : accountInfos){
					tempCtn.add(accountInfo.getString("adid"), accountInfo);
				}
				
				accountInfos = tempCtn.values().toArray(new DBRow[tempCtn.values().size()]);
			}
		}catch(Exception e){
			throw new Exception("AccountMgrSbb.findAllAccountByWarehouseId(long warehouse_id) error:"+ e);
		}
		
		return accountInfos;
	}
	
	public DBRow getAccountByAccount(String account) throws Exception {
		
		return floorAccountMgr.getAccountByAccount(account);
	}
}
	
