package com.cwc.app.api.sbb;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.cwc.app.floor.api.sbb.FloorUserTitleMgrSbb;
import com.cwc.app.iface.sbb.UserTitleMgrIFaceSbb;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

/**
 * 新框架实施后废除此类
 * 
 * */
public class UserTitleMgrSbb implements UserTitleMgrIFaceSbb{

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorUserTitleMgrSbb floorUserTitleMgr;
	
	public void setFloorUserTitleMgr(FloorUserTitleMgrSbb floorUserTitleMgr) {
		this.floorUserTitleMgr = floorUserTitleMgr;
	}

	public void changeUserTitlePriority(HttpServletRequest request) throws Exception {
		
		try{
			
			String idPriority = StringUtil.getString(request, "idPriority");
			String direction = StringUtil.getString(request, "direction");
			
			String[] result = idPriority.split("_");
			
			int sort = Integer.parseInt(result[1]);
			
			if(direction.equals("up")){
				//查询上一个优先级
				sort--;
			}else{
				//查询下一个优先级
				sort++;
			}
			
			DBRow dbrow = floorUserTitleMgr.getAdminIdByTitleSort(result[0],sort);
			String id = dbrow.getString("title_admin_id");
			
			DBRow updateRow = new DBRow();
			
			updateRow.add("title_admin_sort", String.valueOf(sort));
			floorUserTitleMgr.updateUserTitlePriority(result[0],updateRow);
			
			updateRow.add("title_admin_sort", result[1]);
			floorUserTitleMgr.updateUserTitlePriority(id,updateRow);
			
		}catch (Exception e){
			throw new SystemException(e,"addProprietaryAdmins(HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] getAdminHasTitleList(long adid) throws Exception{
		try{
			
			return floorUserTitleMgr.getAdminHasTitleList(adid);
			
		}catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	
	public DBRow[] getAdminUnhasTitleList(long adid) throws Exception{
		try{
			return floorUserTitleMgr.getAdminUnhasTitleList(adid);
			
		}catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
}
