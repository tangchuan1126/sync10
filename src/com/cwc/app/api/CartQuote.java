package com.cwc.app.api;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductNotProfitException;
import com.cwc.app.iface.CartQuoteIFace;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.*;
import com.cwc.exception.SystemException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.util.*;

/**
 * 购物车相关操作
 * 
 * 购物车结构：
 * 用一个DBRow存放一个商品记录
 * ArrayList存放DBRow集合 
 * 
 * 一个购物车应该包含两大块信息：购物车信息和购物车商品信息
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CartQuote implements CartQuoteIFace
{
	static Logger log = Logger.getLogger("ACTION");
	
	private ProductMgr pm;
	private QuoteMgr quoteMgr;
	private CustomCartQuote customCart;
	private SystemConfig systemConfig ;
	private ExpressMgr expressMgr;

	private DBRow detailProducts[] = null;		//购物车详细商品信息
	private float sumQuantity = 0;			//商品总数
	private double productFee = 0;			//商品总价
	private double shippingFee = 0;			//商品总价
	private boolean hasLackingProduct;
	private double totalPrice = 0;
	private float totalWeight = 0;		//商品总重量
	private double systemDiscount = 0;
	private double finalDiscount = 0;	//系统折扣和客服折扣运算结果
	private double saving = 0;			//节省多少钱
	private double sumSpecialPrice = 0;		//特价商品总价
	private String orderCatalog;


	//商品类型
	public static int NORMAL = 1; 
	public static int UNION_STANDARD = 2; 
	public static int UNION_STANDARD_MANUAL = 3; 
	public static int UNION_CUSTOM = 4; 
	//商品类型（普通商品或特价商品）
	public static int NORMAL_PRICE = 1; 
	public static int SPECIAL_PRICE = 2; 

	/**
	 * 增加商品到购物车
	 * 
	 * @param session
	 * @param pid			商品ID
	 * @param quantity		商品数量
	 * @param product_type	商品类型
	 * @throws Exception
	 */
	public void put2Cart(HttpSession session,long pid,float quantity,int product_type,int price_type,long ps_id) 
		throws  Exception
	{
		ArrayList<DBRow> al = null;
		
		if ( pid != 0 )
		{
			if (quantity<=0)
			{
				quantity = 1;
			}
			
			if ( session.getAttribute(Config.cartQuoteSession) == null )
			{
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("cart_pid",pid);
				row.add("cart_quantity",quantity);
				row.add("cart_product_type",product_type);//商品类型
				row.add("price_type",price_type);
				
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
				dealWithRepeat(al,pid,quantity,product_type,price_type);
			}

			session.setAttribute(Config.cartQuoteSession,al);
			session.setAttribute(Config.cartQuoteSession+"_ps_id", ps_id);
		}
	}
	
	/**
	 * 修改购物车的主管折扣
	 * @param discount
	 * @throws Exception
	 */
	public void modifyCartManagerDiscount(HttpSession session,float discount) 
		throws  Exception
	{
		DBRow cartProducts[] = this.getSimpleProducts(session);
		if (cartProducts.length>0)
		{
			ArrayList newAL = new ArrayList();
			for (int i=0; i<cartProducts.length; i++)
			{
				cartProducts[i].add("manager_discount", discount);//每一行商品，增加主管折扣这个字段
				newAL.add(cartProducts[i]);
			}
			session.setAttribute(Config.cartQuoteSession,newAL);
		}
	}

	/**
	 * 提供给初始化购物车使用的接口
	 * @param session
	 * @param pid
	 * @param quantity
	 * @param product_type
	 * @param quote_price
	 * @throws Exception
	 */
	public void initCart(HttpSession session,long pid,float quantity,int product_type,float manager_discount,int price_type,double quote_price) 
		throws  Exception
	{
		ArrayList<DBRow> al = null;
		
		if ( pid != 0 )
		{
			if (quantity<=0)
			{
				quantity = 1;
			}
			
			if ( session.getAttribute(Config.cartQuoteSession) == null )
			{
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("cart_pid",pid);
				row.add("cart_quantity",quantity);
				row.add("cart_product_type",product_type);//商品类型
				row.add("manager_discount",manager_discount);
				row.add("price_type",price_type);
				row.add("quote_price",quote_price);
					
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
	
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("cart_pid",pid);
				row.add("cart_quantity",quantity);
				row.add("cart_product_type",product_type);//商品类型
				row.add("manager_discount",manager_discount);
				row.add("price_type",price_type);
				row.add("quote_price",quote_price);
					
				al.add(row);	
			}
	
			session.setAttribute(Config.cartQuoteSession,al);			
		}
	}

	/**
	 * 处理重复提交商品
	 * @param pid
	 * @param quantity
	 * @return		返回true：有旧商品，修改了数量  返回alse：新增加商品
	 * @throws Exception 
	 */
	private boolean dealWithRepeat(ArrayList<DBRow> al,long pid,float quantity,int product_type,int price_type) 
		throws  Exception
	{
		DBRow productOld;
		
		for ( int i=0;i<al.size(); i++ )
		{
			productOld = (DBRow)al.get(i);		//获得购物车一行记录
			
			if ( StringUtil.getLong(productOld.getString("cart_pid")) == pid&&StringUtil.getInt(productOld.getString("cart_product_type"))==product_type&&StringUtil.getInt(productOld.getString("price_type"))==price_type )		
			{
				float t = StringUtil.getFloat(productOld.getString("cart_quantity")) + quantity;
				productOld.add("cart_quantity",t);
				al.set(i,productOld);
				return(true);
			}
		}

		//没有重复商品，则作为新纪录
		DBRow row = new DBRow();
		row.add("cart_pid",pid);
		row.add("cart_quantity",quantity);
		row.add("cart_product_type",product_type);
		row.add("price_type",price_type);
		
		al.add(row);
		
		return(false);
	}

	/**
	 * 从session获得购物车商品
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getSimpleProducts(HttpSession session) 
		throws Exception
	{
		if ( session.getAttribute(Config.cartQuoteSession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow = (DBRow)al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}

	/**
	 * 清空购物车
	 */
	public void clearCart(HttpSession session)
	{
		if ( session.getAttribute(Config.cartQuoteSession) != null )
		{
			session.removeAttribute(Config.cartQuoteSession);
			session.removeAttribute(Config.cartQuoteSession+"_ps_id");
		}
		
		customCart.clearCart(session);
	}
	
	/**
	 * 判断购物车是否为空
	 * @return
	 * @throws Exception 
	 */
	public boolean isEmpty(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartQuoteSession) == null )
		{
			return(true);
		}
		else
		{
			return( getSimpleProducts(session).length==0 );
		}
	}
	
	/**
	 * 从购物车移除商品
	 * @throws Exception 
	 * @throws  
	 *
	 */
	public void removeProduct(HttpSession session,long pid,int product_type)
		throws Exception
	{
		if ( session.getAttribute(Config.cartQuoteSession) != null )
		{
			DBRow rowOld;
			ArrayList al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
			
			for ( int i=0;i<al.size(); i++ )
			{
				rowOld = (DBRow)al.get(i);
				if ( StringUtil.getLong(rowOld.getString("cart_pid")) == pid&&StringUtil.getInt(rowOld.getString("cart_product_type"))==product_type )
				{
					al.remove(i);
				}
			}
			
			session.setAttribute(Config.cartQuoteSession,al);
		}
	}

	/**
	 * 批量修改商品数量
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public void modQuantity(HttpSession session,String pids[],String quantitys[],String product_type[],String quote_prices[],String price_type[]) 
		throws  Exception
	{
		if ( session.getAttribute(Config.cartQuoteSession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
			DBRow product;
			float quantity;
			
			for (int i=0; i<pids.length; i++)
			{
				for (int j=0; j<al.size(); j++)
				{
					product = (DBRow)al.get(j);
					if ( product.getString("cart_pid").equals(pids[i])&&product.getString("cart_product_type").equals(product_type[i])&&product.getString("price_type").equals(price_type[i]) )//找到对应的商品记录进行数量修改
					{
						//更新商品数量
						quantity = StringUtil.getFloat(quantitys[i]);
						if ( quantity<=0 )
						{
							quantity = 1;
						}
						product.add("cart_quantity",quantity);
						//更新商品报价
						product.add("quote_price",StringUtil.getDouble(quote_prices[i]));
						
					}
					al.set(j,product);
				}
			}

			session.setAttribute(Config.cartQuoteSession,al);
		}
	}

	/**
	 * 为定制组合商品修改购物车
	 * @param session
	 * @param old_pid
	 * @param new_pid
	 * @throws Exception
	 */
	public void convert2CustomProduct(HttpSession session,long old_pid,long new_pid) 
		throws  Exception
	{
		ArrayList al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
		DBRow product;
		
		for (int j=0; j<al.size(); j++)
		{
			product = (DBRow)al.get(j);
			
			/**
			 * 把普通套装变成定制套装时，需要把现在的PID变成定制产生的新定制套装ID
			 */
			if ( product.getString("cart_pid").equals(String.valueOf(old_pid))&&product.getString("cart_product_type").equals(String.valueOf(CartQuote.UNION_STANDARD)) )
			{
				product.add("cart_pid",new_pid);
				product.add("cart_product_type",CartQuote.UNION_CUSTOM);
				al.set(j,product);
				break;
			}
		}
	}
	
	
	/**
	 * 获得购物车详细信息
	 * @param session
	 * @param sc_id		快递公司
	 * @param ccid		递送国家
	 * @throws Exception
	 */
	public void flush(HttpSession session,String client_id,long sc_id,long ccid,long pro_id,long ps_id)
		throws Exception
	{
		try
		{
				double zoom = systemConfig.getDoubleConfigValue("wholesell_price_zoom");//放大商品原始价格
			
				float sum_quantity = 0;
				double sum_price = 0;
				DBRow detailP;
				double total = 0;
				double discount_sum_price = 0;//用来累计优惠折扣的金额
				double discount_total_price = 0;
				totalWeight = 0;
				sumSpecialPrice = 0;
				
				double currency = systemConfig.getDoubleConfigValue("USD");//获得汇率
				
				ArrayList<DBRow> cartAl = (ArrayList<DBRow>)session.getAttribute(Config.cartQuoteSession);
				DBRow products[] = this.getSimpleProducts(session);
				
				Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
				this.orderCatalog = "";

				//把更详细的商品信息附加到购物车的商品记录中
				/**
				 * quote_price   批发报价
				 * lowest_price  零售报价
				 */
				for (int i=0; i<products.length; i++)
				{
					/**
					 * 计算得出最低报价和实际报价后，需要重新设置到session里面
					 * 这里处理主要防止客服把报价低于 系统要求最低报价
					 * 这里关键是要计算出每个商品最低报价
					 */
					
					//非定制商品
					if (products[i].get("cart_product_type",0)!=CartQuote.UNION_CUSTOM)
					{
						detailP = pm.getDetailProductByPcid(StringUtil.getLong(products[i].getString("cart_pid")));

						products[i].add("lowest_price", MoneyUtil.round(MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(products[i].getString("cart_pid")),ccid,1)*zoom, currency),2) );//最低报价
						
						////system.out.println("org price-> "+Double.parseDouble(products[i].getString("lowest_price"))*currency);
						//数据初始化使用
						if ( products[i].getString("quote_price").equals("") )
						{
							products[i].add("quote_price", products[i].getString("lowest_price"));
							cartAl.set(i, products[i]);//重新设置session购物车，把购物车增加两个字段
						}
						
						products[i].append(detailP);
					}
					else
					{
						//定制商品需要从另外的定制表获得商品详细信息
						detailP = pm.getDetailProductCustomByPcPcid(StringUtil.getLong(products[i].getString("cart_pid")));

						//因为新纪录的字段比原来少，所以需要把旧字段删掉
						String cart_pid = products[i].getString("cart_pid");
						String cart_quantity = products[i].getString("cart_quantity");
						String cart_product_type = products[i].getString("cart_product_type");
						String manager_discount = products[i].getString("manager_discount");
						String price_type = products[i].getString("price_type");
						
						products[i].clear();
						products[i].add("cart_pid", StringUtil.getLong(cart_pid));
						products[i].add("cart_quantity", StringUtil.getFloat(cart_quantity));
						products[i].add("cart_product_type", StringUtil.getInt(cart_product_type));
						products[i].add("manager_discount", StringUtil.getFloat(manager_discount,0f));
						products[i].add("price_type", StringUtil.getInt(price_type));

						//计算一套定制最低报价
						DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(session, StringUtil.getLong(cart_pid));
						double lowestPrice= 0;
						double custom_unit_price= 0;
						float weight = 0;
						//计算总重，原始总价
						for (int j=0; j<productsCustomInSet.length; j++)
						{
							custom_unit_price = MoneyUtil.add(custom_unit_price,MoneyUtil.mul(productsCustomInSet[j].get("unit_price", 0d), Double.parseDouble(productsCustomInSet[j].getString("cart_quantity"))));
							weight += productsCustomInSet[j].get("weight", 0f)*Float.parseFloat(productsCustomInSet[j].getString("cart_quantity"));
						}
						//获得零售价格
						lowestPrice = quoteMgr.getCustomProductPrice(session,StringUtil.getLong(products[i].getString("cart_pid")), ccid, 1);
						////system.out.println("org price-> "+lowestPrice);
						products[i].add("unit_price", custom_unit_price);//原始价格
						products[i].add("weight",weight);//原始重量
						////system.out.println("=> "+weight);

						products[i].add("lowest_price", MoneyUtil.round( MoneyUtil.div(lowestPrice*zoom, currency), 2));
						if ( products[i].getString("quote_price").equals("") )
						{
							products[i].add("quote_price", products[i].getString("lowest_price"));
						}

						products[i].append(detailP);
						cartAl.set(i, products[i]);//重新设置session购物车，把购物车增加两个字段
					}

					//计算一些数据
					sum_quantity += StringUtil.getFloat(products[i].getString("cart_quantity"));
					//附加 小计 字段
					
					total = MoneyUtil.round(MoneyUtil.mul(products[i].getString("cart_quantity"), products[i].getString("quote_price")), 2);
					discount_total_price = MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(products[i].getString("cart_pid")),ccid,1), currency)*StringUtil.getDouble(products[i].getString("cart_quantity"));//用来计算折扣
					
					sum_price = MoneyUtil.add(sum_price, total);//用用来计算折扣
					//计算折扣的金额，不能算特价商品
					if (products[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
					{
						discount_sum_price = MoneyUtil.add(discount_sum_price, discount_total_price);//用来计算折扣
					}
					////system.out.println("=====>"+discount_sum_price);
					
					products[i].add("total", total);
					
					//计算商品重量
					totalWeight += StringUtil.getFloat(products[i].getString("weight"))*StringUtil.getFloat(products[i].getString("cart_quantity"));
					////system.out.println( products[i].getString("weight")+" x "+ products[i].getString("cart_quantity") );
					
					/**
					 * 计算订单所属分类
					 * 如果一个订单内，存在两种以上不同类型商品，那么订单分类为“没法确定”
					 */
					DBRow catalogFather[] = tree.getAllFather(detailP.get("catalog_id", 0l));
					if(catalogFather.length!=0)//报价莫名出错临时解决方式需要仔细检查购物车
					{
						if (i==0)
						{
							this.orderCatalog = catalogFather[0].getString("title");
						}
						else
						{
							if (this.orderCatalog.equals(catalogFather[0].getString("title"))==false)
							{
								this.orderCatalog = "Pending";
							}
						}
					}
					
				}

				//每一个ITEM，增加2个字段
				double final_price,manager_discount=1;
				////system.out.println("discount_sum_price: "+discount_sum_price);
				systemDiscount = quoteMgr.getWholeSellDiscount(this.orderCatalog,client_id, discount_sum_price);//根据系统策略，获得一个基本批发折扣
				
				double lowest_price_saving = 0;
				double quote_price_saving = 0;
				sum_price = 0;
				
				//最后对商品在统筹一遍
				for (int i=0; i<products.length; i++)
				{
					if (products[i].getString("manager_discount").equals("")==false&&StringUtil.getDouble(products[i].getString("manager_discount"))!=0)
					{
						manager_discount = StringUtil.getDouble(products[i].getString("manager_discount"));
					}
					
					/**
					 * 最终折扣会在页面计算得到
					 * 由下拉选择框提供
					 */
					if (manager_discount==1)
					{
						finalDiscount = systemDiscount;
					}
					else
					{
						finalDiscount = manager_discount;
					}
					
					if (products[i].get("cart_product_type",0)==CartQuote.UNION_CUSTOM)
					{
						final_price = quoteMgr.getCustomProductPrice(session,StringUtil.getLong(products[i].getString("cart_pid")), ccid, finalDiscount);
					}
					else//普通商品计算方法
					{
						final_price = quoteMgr.getLowestProductPrice(StringUtil.getLong(products[i].getString("cart_pid")),ccid,finalDiscount);
					}
					////system.out.println(final_discount*manager_discount);
					////system.out.println("discount("+final_discount*manager_discount+") price-> "+final_price);
					final_price = MoneyUtil.round( MoneyUtil.div(final_price, currency), 2);
					
					if (products[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
					{
						products[i].add("discount",MoneyUtil.round(MoneyUtil.div(final_price, StringUtil.getDouble(products[i].getString("lowest_price")))*10, 1) );
					}
					else
					{
						products[i].add("discount",10 );
					}
					
					//不更新特价商品
					if (products[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
					{
						products[i].add("quote_price",final_price);//最终报价
					}

					//重新计算总价和小计
					total = MoneyUtil.round(MoneyUtil.mul(products[i].getString("cart_quantity"), products[i].getString("quote_price")), 2);
					products[i].add("total", total);
					//非特价商品才累计总金额
					if (products[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
					{
						sum_price = MoneyUtil.add(sum_price, total);//用商品报价来计算总价
					}
					else
					{
						this.sumSpecialPrice = MoneyUtil.add(this.sumSpecialPrice, total);
					}
					////system.out.println("@!!-->");
					
					cartAl.set(i, products[i]);
					
					//计算节省多少钱(不计算特价商品)
					if (products[i].get("price_type",0)==CartQuote.NORMAL_PRICE)
					{
						lowest_price_saving = MoneyUtil.add(lowest_price_saving, MoneyUtil.mul(products[i].getString("lowest_price"), products[i].getString("cart_quantity")));
						quote_price_saving = MoneyUtil.add(quote_price_saving, MoneyUtil.mul(products[i].getString("quote_price"), products[i].getString("cart_quantity")));
					}
				}
				
				//把增加了字段的购物车放回session
				session.setAttribute(Config.cartQuoteSession, cartAl);
				
				//需要乘以一个重量修正系数
				float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
				////system.out.print(totalWeight);
				totalWeight = totalWeight*weight_cost_coefficient;
				////system.out.println("#=> "+totalWeight);

				//计算运费
				try
				{
					shippingFee = 0;//重新初始化为0
					
					//这里先校验下传进来的sc_id是符合重量段要求的快递（因为页面有可能传进来的快递并不合适重新计算的重量）
					if (ps_id>0&&ccid>0)
					{
						boolean isDomesticFlag = expressMgr.isDomesticShipping(ccid, ps_id);
						DBRow express[];
						boolean matchExpressFlag = false;
						if (isDomesticFlag)//国内发货
						{
							express = expressMgr.getDomesticExpressCompanyByPsIdProId( ps_id, pro_id, totalWeight, null);
						}
						else
						{
							express = expressMgr.getInternationalExpressCompanyByPsIdProId(ps_id,ccid,totalWeight,null);
						}
						for (int i=0; i<express.length; i++)
						{
							if (express[i].get("sc_id", 0l)==sc_id)
							{
								matchExpressFlag = true;
							}
						}
						if (!matchExpressFlag)
						{
							sc_id = 0;
						}
						//----------------------------------- 校验完成 ---------------------------------------
					}
					
					ShippingInfoBean shippingInfoBean = expressMgr.getShippingFee(sc_id, totalWeight, ccid,pro_id);//需要改造报价单购物车
					if (shippingInfoBean!=null)
					{
						////system.out.println("shippingInfoBean.getShippingFee():"+shippingInfoBean.getShippingFee());
						this.shippingFee = MoneyUtil.round( MoneyUtil.div(quoteMgr.getLowestShippingPrice(shippingInfoBean.getShippingFee()), currency), 2);
					}
					
					////system.out.println(totalWeight+" - "+shippingInfoBean.getShippingFee()+" - "+shippingFee);
				}
				catch(Exception e)
				{
				}

				//设置购物车和购物车商品详细信息
				this.detailProducts =  products;
				this.productFee = MoneyUtil.round(sum_price, 2);
				this.sumQuantity = sum_quantity; 
				this.totalPrice = MoneyUtil.round(MoneyUtil.add(this.sumSpecialPrice,MoneyUtil.add(sum_price,shippingFee)), 0);
				this.saving = MoneyUtil.round( MoneyUtil.sub(lowest_price_saving, quote_price_saving) ,2);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"flush",log);
		}
	}
	
	
	/**
	 * 该接口提供给修改报价单时候加载已有数据
	 * @param session
	 * @param sc_id
	 * @param ccid
	 * @throws Exception
	 */
//	public void flushModify(HttpSession session,long qoid,long sc_id,long ccid)
//		throws Exception
//	{
//		try
//		{
//				this.hasLackingProduct = false;		//默认没有包含缺货商品
//				double total = 0;
//				DBRow detailItem;
//				
//				DBRow detailQuoteOrder = quoteMgr.getDetailQuoteByQoid(qoid);
//				
//				DBRow products[] = this.getSimpleProducts(session);
//				//把更详细的商品信息附加到购物车的商品记录中
//				for (int i=0; i<products.length; i++)
//				{
//					detailItem = quoteMgr.getDetailQuoteItemByQoidPid(qoid, StrUtil.getLong(products[i].getString("cart_pid")));
//					
//					products[i].append(detailItem);
//					//附加 小计 字段
//					total = MoneyUtil.round(MoneyUtil.mul(products[i].getString("cart_quantity"), products[i].getString("quote_price")), 2);
//					products[i].add("total", total);
//					products[i].add("p_name", detailItem.getString("name"));
//				}
//				
//				
//				//设置购物车和购物车商品详细信息
//				this.detailProducts =  products;
//				this.productFee = detailQuoteOrder.get("product_cost", 0d);
//				this.sumQuantity = detailQuoteOrder.get("quantity", 0f);
//				this.totalPrice = detailQuoteOrder.get("total_cost", 0d);
//				this.shippingFee = detailQuoteOrder.get("shipping_cost", 0d);
//		}
//		catch (Exception e)
//		{
//			throw new SystemException(e,"flush",log);
//		}
//	}
	

	/**
	 * 把商品添加到购物车
	 * @param request
	 * @throws Exception
	 */
	public void put2Cart(HttpServletRequest request)
		throws ProductNotProfitException,ProductNotCreateStorageException,ProductNotExistException,Exception
	{
		try
		{
			int product_type;
			
			String p_name = StringUtil.getString(request, "p_name");
			float quantity = StringUtil.getFloat(request, "quantity");
			long ps_id = StringUtil.getLong(request, "ps_id");
			long ccid = StringUtil.getLong(request, "ccid");//用来验证商品是否已经设置了利润
			int price_type = StringUtil.getInt(request, "price_type");
			
			long pid = 0;
			
			//先检测商品是否存在，存在则取得PID
			DBRow detailP = pm.getDetailProductByPname(p_name);
			if (detailP==null||detailP.get("alive", 0)==0)
			{
				throw new ProductNotExistException();
			}
			else
			{
				pid = detailP.get("pc_id", 0l);
			}

			/**
			 * 检测商品在发货仓库是否已经建库
			 * 套装商品：标准套装或者散件，至少其中一样需要建库
			 */
			DBRow product = new DBRow();
			product.add("cart_pid", pid);
			product.add("cart_product_type", 0);
			DBRow products[] = new DBRow[] {product} ;
			pm.validateProductsCreateStorage(ps_id,products);
			
			/**
			 * 从系统外部，用户只能添加普通和标准套装
			 * 拼装套装和定制套装，都是系统内部转换得来
			 */
			if (detailP.get("union_flag", 0)==0)
			{
				product_type = CartQuote.NORMAL;
			}
			else
			{
				product_type = CartQuote.UNION_STANDARD;
			}
			
			this.put2Cart(StringUtil.getSession(request),pid, quantity,product_type,price_type,ps_id);
			
//			//验证商品是否设置了毛利
//			if ( quoteMgr.getDetailProductProfitByPidCcid(pid, ccid)==null )
//			{
//				throw new ProductNotProfitException();//暂时屏蔽
//			}
		}
		catch (ProductNotProfitException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductNotCreateStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"put2Cart",log);
		}
	}
	@Override
	public void updateSessionPsId(HttpSession session, long ps_id)
			throws Exception {
		 try{
			 session.setAttribute(Config.cartQuoteSession+"_ps_id", ps_id);
		 }catch (Exception e){
				throw e;
		}
	}
	
	public DBRow[] getDetailProducts()
	{
		return detailProducts;
	}

	public float getSumQuantity()
	{
		return sumQuantity;
	}

	public boolean isHasLackingProduct()
	{
		return hasLackingProduct;
	}
	
	public void setPm(ProductMgr pm)
	{
		this.pm = pm;
	}

	public void setQuoteMgr(QuoteMgr quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}

	public void setCustomCart(CustomCartQuote customCart)
	{
		this.customCart = customCart;
	}

	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}

	public double getProductFee()
	{
		return productFee;
	}

	public void setExpressMgr(ExpressMgr expressMgr)
	{
		this.expressMgr = expressMgr;
	}

	public double getShippingFee()
	{
		return shippingFee;
	}

	public double getTotalPrice()
	{
		return totalPrice;
	}

	public float getTotalWeight() 
	{
		return totalWeight;
	}
	
	public double getSystemDiscount() 
	{
		return systemDiscount;
	}
	
	public double getFinalDiscount()
	{
		return finalDiscount;
	}
	
	public double getSaving() 
	{
		return saving;
	}
	
	public double getSumSpecialPrice()
	{
		return sumSpecialPrice;
	}
	
	public String getOrderCatalog() 
	{
		return orderCatalog;
	}
	
	
	
	public static void main(String args[])
	{
		float rate = 0.26f;
		float step = 0.05f;
		
		float i=step;
		for (;i<=rate; i=i+step)
		{
			////system.out.println(i);
		}
		////system.out.println("->"+i+" "+(rate+step));
	}



	
}






