package com.cwc.app.api;

import org.apache.log4j.Logger;

import com.cwc.app.iface.MemberMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.exception.SendMailException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * 会员相关操作类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MemberMgr implements MemberMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");

	/**
	 * 注册发送邮件
	 * @param tmail
	 * @param account
	 * @return
	 * @throws SendMailException
	 */
	public boolean sendMailForRegister(String tmail,String account)
		throws SendMailException
	{
		try
		{
			String template = Environment.getHome() + ConfigBean.getStringValue("email_template") + "/register.html";
			
			String subject = "";
			String content = "";
				
			String t[];
			content = FileUtil.readFile2String(template,"utf-8");
			t = content.split("<title>");
			subject = t[1];
			t = subject.split("</title>");
			subject = t[0];
			subject = StringUtil.replaceString(subject,"$account",account);
		
			content = StringUtil.replaceString(content,"$account",account);

			SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
			content = systemConfig.replaceHtmlImgSrc(content);
			
			//sendMail.send(tmail,subject,content);
		}
		catch (Exception e)
		{
			log.error("OrderMgr.sendMailForMakeOrder error:" + e);
			//throw new SendMailException("sendMailForDisbelieveOrder() error:" + e);
			return(false);
		}
		
		return(true);
	}
}








