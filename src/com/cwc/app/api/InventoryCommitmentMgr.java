package com.cwc.app.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Font;

import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorWMSOrdersMgr;
import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.iface.InventoryCommitmentMgrIFace;
import com.cwc.app.key.B2BOrderLogTypeKey;
import com.cwc.app.key.WmsOrderUpdateTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.fr.third.org.apache.poi.hssf.util.HSSFColor;
import com.mysql.jdbc.StringUtils;

/**
 * @author Liang Jie Created on 2014年12月16日
 * 
 */
public class InventoryCommitmentMgr implements InventoryCommitmentMgrIFace {

	private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
	private FloorOrderMgr floorOrderMgr;
	private FloorWMSOrdersMgr floorWMSOrdersMgr;
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	private WMSOrderMgr wmsOrderMgr;
	private String isAllCommited = "";
	
	private static final SimpleDateFormat usaFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
	@Override
	public String[] commitInventory(HttpServletRequest request) throws Exception {
		String companyId = request.getParameter("companyId");
		String customerId = request.getParameter("customerId");
		List<String> dnList = Arrays.asList(request.getParameterValues("dnList[]"));
		long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
		String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
		isAllCommited = "Y";
		POIFSFileSystem fs = null;
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/order/InventoryCommitment.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);//ALL
		HSSFSheet sheet1 = wb.getSheetAt(1);//No Commited
		HSSFSheet sheet2 = wb.getSheetAt(2);//Pending Inbounds
		HSSFSheet sheet3 = wb.getSheetAt(3);//End User
		HSSFSheet sheet4 = wb.getSheetAt(4);//New Order printed=N
		sheet.setDefaultColumnWidth(15);
		
		if(StringUtil.isNull(companyId)){		
			throw new Exception("CompanyID is required!");
		}else if(StringUtil.isNull(customerId)){
			throw new Exception("CustomerID is required!");
		}else if(dnList==null || dnList.size()<=0){
			throw new Exception("Please check the orders to be commited!");
		}else{
			int rownum = calculateInventory(companyId, customerId, dnList, sheet,sheet1,sheet2,sheet3,sheet4, 1, wb, logingUserId, loginUserName);
			if(rownum<2){
				throw new Exception("No inventory found for the orders selected!");
			}
		}

		//写文件  
		String path = "inventory_commitment_report/inventory_commitment_"+customerId+"_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";

		File f = new File(Environment.getHome()+"inventory_commitment_report");
		if (!f.exists()) {
            f.mkdirs();
        }
		FileOutputStream os= new FileOutputStream(Environment.getHome()+path);
		wb.write(os);  
		os.close(); 
		
		String[] arr = {path,isAllCommited};
		return (arr);
	}

	/**
	 * 计算库存
	 * @param companyId
	 * @param customerId
	 * @param orders
	 * @param sheet
	 * @param rownum
	 * @param style
	 * @return
	 * @throws Exception
	 */
	private int calculateInventory(String companyId, String customerId, List<String> dnList, 
			HSSFSheet sheet,HSSFSheet sheet1, HSSFSheet sheet2, HSSFSheet sheet3, HSSFSheet sheet4, 
			int rownum, HSSFWorkbook wb, long logingUserId, String logingUserName) throws Exception{
		//获取当前需要处理和计算的wms对应单号，用于数据同步
		List<String> orderNoList = floorOrderMgr.getWMSOrderNOsToCommited(companyId, customerId);
		//得到当前onHand的sku及其产品数量
		Map<String,Integer> productCountMap = getProductCountOnHand(companyId,customerId, orderNoList);
		if(productCountMap != null){
			//查询已生成BOL的订单,从参数订单列表中移除
			DBRow[] bookedOrderIds = floorOrderMgr.getBookedOrderId(companyId,customerId,dnList);
			
			//若Customer是Vizio,则对所选订单填入关联查询的运输时间及零售商优先级
			if(customerId.equals("4")){
				DBRow[] rowArr = floorOrderMgr.getOrdersListBySelection(companyId, customerId, dnList);
				DBRow comp = floorOrderMgr.getWmsCompanyID(companyId,customerId);
				inputPriorityConditions(rowArr, comp.get("ps_id", 0L));
			}
			
			//计算所选order的库存是否满足需求
			DBRow[] recsList = this.getReceivSchedule(companyId, customerId);
			rownum = commitInventorySelectedOrders(companyId, customerId, dnList, productCountMap, 
					bookedOrderIds, recsList, sheet,sheet1,sheet2,sheet3,sheet4, rownum, wb, logingUserId, logingUserName);
		}
		return rownum;
	}
	
	public DBRow[] getReceivSchedule(String companyID, String customerID) throws SQLException {
		DBRow[] rt = new DBRow[0];
		DBRow wmsCompanyIDRow = floorOrderMgr.getWmsCompanyID(companyID, customerID);
		DBRow customerBrandRow = floorOrderMgr.getCustomerBrandNumber(customerID);
		if(wmsCompanyIDRow!=null && wmsCompanyIDRow.get("company_id")!=null){
			if(customerBrandRow!=null && customerBrandRow.get("brand_number")!=null){
				String wmsCompanyID = (String)wmsCompanyIDRow.get("company_id");
				String wmsCustomerID = (String)customerBrandRow.get("brand_number");
				wmsCustomerID = wmsCustomerID(wmsCompanyID, wmsCustomerID);
				
				return floorSQLServerMgrZJ.getReceivSchedule(wmsCustomerID, wmsCompanyID);
			}
		}
		return rt;
	}
	
	@Override
	public Map<String,Integer> getProductCountOnHand(String companyID, String customerID, List<String> OrderNoList)
			throws Exception {
		DBRow wmsCompanyIDRow = floorOrderMgr.getWmsCompanyID(companyID, customerID);
		DBRow customerBrandRow = floorOrderMgr.getCustomerBrandNumber(customerID);
		if(wmsCompanyIDRow!=null && wmsCompanyIDRow.get("company_id")!=null){
			if(customerBrandRow!=null && customerBrandRow.get("brand_number")!=null){
				boolean hasLotno = true;
				String wmsCompanyID = (String)wmsCompanyIDRow.get("company_id");
				String wmsCustomerID = (String)customerBrandRow.get("brand_number");
				
				if(!StringUtil.isBlank(wmsCompanyID) && !StringUtil.isBlank(wmsCustomerID)){
					synchronized(wmsCompanyID+"|||"+wmsCustomerID) {
						wmsCustomerID = wmsCustomerID(wmsCompanyID, wmsCustomerID);
						//区库存数据
						if (WMSOrderMgr.isTF(Long.parseLong(customerID))) {
							hasLotno = false;
						}
						DBRow[] inResult = floorSQLServerMgrZJ.getInSkuSumCount(wmsCompanyID, wmsCustomerID, hasLotno);
						DBRow[] adjResult = floorSQLServerMgrZJ.getAdjSkuSumCount(wmsCompanyID, wmsCustomerID, hasLotno);
						//待后面同步数据用 by wangcr 2015/06/18
						DBRow[] commitPKs = floorSQLServerMgrZJ.getPickedOrderPK(wmsCompanyID, wmsCustomerID);
						DBRow[] outPKs = floorSQLServerMgrZJ.getOverOrderPK(wmsCompanyID, wmsCustomerID, OrderNoList);
						DBRow[] outResult = floorSQLServerMgrZJ.getOutSkuSumCount(wmsCompanyID, wmsCustomerID, hasLotno);
						//
						List<String> uSQL = new ArrayList<String>();
						List<String> delPks = floorSQLServerMgrZJ.getCloseOrders(wmsCompanyID, wmsCustomerID, OrderNoList);
						
						if (delPks.size()> 0) {
							//有删除的Order
							String ids = "";
							for (String OrderNo : delPks) {
								if (!"".equals(OrderNo)) {
									ids+=","+ OrderNo;
								}
							}
							if (ids.length()>1) {
								//canel order
								uSQL.add("update b2b_order t set t.b2b_order_status=9 where t.b2b_order_number in ("+ids.substring(1)+")");
							}
						}
						for (DBRow row : commitPKs) {
							uSQL.add("update b2b_order t set t.b2b_order_status=2 where t.b2b_order_number ='"+row.get("OrderNo","")+"' and t.customer_dn = '"+row.get("ReferenceNo","")+"'");
						}
						for (DBRow row : outPKs) {
							uSQL.add("update b2b_order t set t.b2b_order_status=8 where t.b2b_order_number ='"+row.get("OrderNo","")+"' and t.customer_dn = '"+row.get("ReferenceNo","")+"'");
						}
						//批量更新状态
						floorOrderMgr.updateOrderStatus(uSQL);
						
						//取wms 数据中的数据  by wangcr 2015/05/22
						//DBRow[] bookedResult = floorOrderMgr.getBookedSkuSumCount(companyID, customerID);
						
						//取同步数据后的数据 wangcr 2015/06/18
						//DBRow[] bookedResult = floorSQLServerMgrZJ.getBookedSkuSumCount(wmsCompanyID, wmsCustomerID);
						DBRow[] bookedResult = floorOrderMgr.getBookedSkuSumCount(companyID, customerID);
						
						Map<String,Integer> inResultMap = convertToMap(inResult);
						Map<String,Integer> onHandResultMap = calculateOnHand(calculateOnHand(calculateOnHand(inResultMap,outResult,"ShippedQty"),
								adjResult,"AdjustedQty"),bookedResult,"BookedQty");
						return onHandResultMap;
					}
				}
			}
		}		
		return null;
	}
	
	//VIZIO 在不同仓库有不同的 customerID by wangcr 05/20/2015
	private String wmsCustomerID(String wmsCompanyID, String wmsCustomerID) {
		if ("VIZIO".equals(wmsCustomerID)) {
				if ("W12".equals(wmsCompanyID)) return "VIZIO";
				if ("W14".equals(wmsCompanyID)) return "VZO";
				if ("W29".equals(wmsCompanyID)) return "VIZIO3";
		} else if(!"".equals(wmsCustomerID)) {
			//System.out.println(wmsCustomerID);
		}
		return wmsCustomerID;
	}
	
	public static String changTitle(String title, long customerID) {
		if (WMSOrderMgr.isTF(customerID)) {
			if ("Element".equalsIgnoreCase(title)) return "";
		}
		return title;
	}
	
	
	public int commitInventorySelectedOrders(String companyID, String customerID, 
			List<String> orders, Map<String,Integer> productCountMap, DBRow[] bookedOrders, DBRow[] recsList,
			HSSFSheet sheet, HSSFSheet sheet1, HSSFSheet sheet2, HSSFSheet sheet3, HSSFSheet sheet4, int rownum, HSSFWorkbook wb,
			long logingUserId, String logingUserName) throws Exception {
		DBRow[] orderArr = floorOrderMgr.getOrdersListWithPriority(companyID, customerID, orders);
		
		DBRow customerBrandRow = floorOrderMgr.getCustomerBrandNumber(customerID);
		String wmscustomerId = String.valueOf(customerBrandRow.get("brand_number"));
		
		List<DBRow> orderList = removeBookedOrders(orderArr, bookedOrders);
		if (orderList.size()==0) return 2;
		rownum = populateInventoryReport(orderList,productCountMap, recsList, sheet, sheet1, sheet2, sheet3, sheet4, rownum,wb,logingUserId,logingUserName, wmscustomerId);
		return rownum;
	}
	
	/**
	 * 生成库存确认报告
	 * @author Liang Jie
	 * @param orders
	 * @param productCountMap
	 * @param sheet
	 * @param rowNum
	 * @param wb
	 * @return
	 * @throws Exception
	 */
	private int populateInventoryReport(List<DBRow> orders, Map<String,Integer> productCountMap, DBRow[] recsList,
			HSSFSheet sheet, HSSFSheet sheet1, HSSFSheet sheet2, HSSFSheet sheet3, HSSFSheet sheet4, int rowNum, HSSFWorkbook wb, long logingUserId, String logingUserName, String wmscustomerId) throws Exception{

		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setLocked(false);
		style.setWrapText(true);
		Font font = wb.createFont();
		style.setFont(font);
		
		HSSFCellStyle styleError = wb.createCellStyle();
		styleError.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		styleError.setLocked(false);
		styleError.setWrapText(true);
		styleError.setFillBackgroundColor(HSSFColor.YELLOW.index);
		Font fontRed = wb.createFont();
		fontRed.setColor(HSSFColor.RED.index);
		styleError.setFont(fontRed);
		
		List<Map<String,Object>> errors = new ArrayList<Map<String,Object>>();
		//Map<String,List<Map<String,Object>>> errors = new HashMap<String,List<Map<String,Object>>>();
		String mindate = "";
		int rowNum3=1;
		int rowNum4=1;
		for(DBRow order : orders)
		{
			String orderNo = order.getString("b2b_oid");
			//String orderNo2 = order.getString("b2b_order_number", "");
			String companyId = order.getString("company_id");
			long customerID = order.get("customer_id",0L);
			String calcRequestedDate = order.getString("calc_requested_date");
			//得到每个order的sku列表，只有当前order下所有sku都可以satisfy时，此order才可以被book
			DBRow[] orderLines = floorOrderMgr.getOrderLinesList(order, companyId);
			Boolean isBooked = true;
			//用于暂存当前order下所有sku的report信息
			List<Map<String,Object>> itemsList = new ArrayList<Map<String,Object>>();
			if ("".equals(mindate)) {
				mindate = order.getString("requested_date");
			} else {
				
				if (mindate.compareTo(order.getString("requested_date"))>0) {
					mindate = order.getString("requested_date");
				}
			}
			int printed = 0;
			for(DBRow item : orderLines){
				String title = item.getString("title");
				String sku = item.getString("p_name");
				String skuWithTitle = sku.trim()+"/"+(title==null?"":WMSOrderMgr.changTitle(title, customerID));
				String itemId = sku;
				String lotNo = "";
				if (sku.indexOf("/")>0) {
					 itemId = sku.substring(0, sku.indexOf("/"));
					 lotNo = sku.substring(sku.indexOf("/")+1);
				}
//				String itemId = sku.substring(0, sku.indexOf("/")); 
//				String lotNo = sku.substring(sku.indexOf("/")+1);
				Integer orderedQty = ((Float)item.getValue("b2b_count")).intValue();
				
				Map<String,Object> itemMap = new HashMap<String,Object>();
				itemMap.put("companyId", companyId);
				itemMap.put("customerId", order.getString("customer_id"));
				itemMap.put("shipToParty", item.getString("ship_to_party"));
				itemMap.put("referenceNo", order.getString("customer_dn"));
				itemMap.put("referenceNo", order.getString("customer_dn"));
				itemMap.put("title", title);
				itemMap.put("orderNo", item.get("b2b_order_number",""));
				itemMap.put("freightTerm", order.getString("freight_term"));
				itemMap.put("status", order.getString("b2b_order_status"));
				itemMap.put("loadNo", order.getString("load_no"));
				itemMap.put("calcRequestedDate", calcRequestedDate);
				itemMap.put("lineNo", item.getString("b2b_detail_id"));
				itemMap.put("itemId", itemId);
				itemMap.put("lotNo", lotNo);
				itemMap.put("po", item.get("retail_po",""));
				itemMap.put("orderedQty", orderedQty);
				itemMap.put("skuWithTitle", skuWithTitle);
				itemMap.put("requestedDate", order.getString("requested_date"));
				itemMap.put("mabd", order.getString("mabd"));
				printed = item.get("tag", 0);
				if (printed==1) {
					itemMap.put("printed", "Y");
				} else {
					itemMap.put("printed", "N");
				}
				
				if(productCountMap.containsKey(skuWithTitle)){
					Integer availableQty = productCountMap.get(skuWithTitle);
					itemMap.put("availableQty", availableQty);
					if(availableQty-orderedQty>=0){
						itemMap.put("satisfied", "Y");
						itemMap.put("updatedQty", availableQty-orderedQty);
					}else{
						itemMap.put("satisfied", "N");
						isBooked = false;
						isAllCommited = "N";
						//errors.add(itemMap);
					}
				} else {
					itemMap.put("availableQty", 0);
					itemMap.put("satisfied", "N");
					isBooked = false;
					isAllCommited = "N";
					//errors.add(itemMap);
				}
				itemsList.add(itemMap);
			}
			//若所有sku的orderedQty都满足，则先更新onHandQty,再将report数据插入Excel
			if(isBooked){
				for(Map<String,Object> map : itemsList){
					String key = (String)map.get("skuWithTitle");
					Integer value = (Integer)map.get("updatedQty");
					productCountMap.put(key, value);
					
					HSSFRow row = sheet.createRow(rowNum++);
					setExcelRowValue(row, map, "Y", style);
					
					if (0==printed) {
						HSSFRow row_new = sheet4.createRow(rowNum4++);
						setExcelRowValue(row_new, map, "Y", style);
					}
					if ("END USER".equalsIgnoreCase(order.get("order_type", ""))) {
						HSSFRow row_new = sheet3.createRow(rowNum3++);
						setExcelRowValue(row_new, map, "Y", style);
					}
				}
				commitOrder(orderNo, logingUserId, logingUserName);//状态回写WMS
				updateWmsOrder(order.getString("b2b_order_number"),companyId,order.getString("customer_id"));
			}else{
				for(Map<String,Object> map : itemsList){
					HSSFRow row = sheet.createRow(rowNum++);
					setExcelRowValue(row, map, "N", styleError);
					errors.add(map);
					if (0==printed) {
						HSSFRow row_new = sheet4.createRow(rowNum4++);
						setExcelRowValue(row_new, map, "N", styleError);
					}
					if ("END USER".equalsIgnoreCase(order.get("order_type", ""))) {
						HSSFRow row_new = sheet3.createRow(rowNum3++);
						setExcelRowValue(row_new, map, "N", styleError);
					}
				}
			}
		}
		int rowNum1=1;
		Collections.sort(errors, new ComparatorDate());
		String tdate ="";
		for (Map<String,Object> map: errors) {
			if ("".equals(tdate)) {
				tdate = String.valueOf(map.get("requestedDate"));
			} else {
				String rqd = String.valueOf(map.get("requestedDate"));
				if (tdate.compareTo(rqd)!=0)  {
					sheet1.createRow(rowNum1++);
					tdate = String.valueOf(map.get("requestedDate"));
				}
			}
			HSSFRow row = sheet1.createRow(rowNum1++);
			if ("Y".equals(map.get("satisfied"))) {
				map.put("error", "Line");
				setExcelRowValue1(row, map, "N", styleError);
			} else {
				map.put("error", "Qty");
				setExcelRowValue1(row, map, "N", styleError);
			}
		}
		if (recsList!=null) {
			//Pending Inbounds Sheet
			int rowNum2 = 1;
			String sku ="";
			for(DBRow rrow: recsList) {
				if ("".equals(sku)) {
					sku = rrow.getString("ItemID", "")+"/"+ rrow.get("LotNo", "");
				} else {
					if (!sku.equals(rrow.getString("ItemID", "")+"/"+rrow.get("LotNo", ""))) {
						sku = rrow.getString("ItemID", "")+"/"+ rrow.get("LotNo", "");
						sheet2.createRow(rowNum2++);
					}
				}
				HSSFRow row = sheet2.createRow(rowNum2++);
				setExcelRowValue2(row, rrow, "N", style);
			}
		}
		return rowNum;
	}
	
	private void setExcelRowValue2(HSSFRow row, DBRow map, String isBooked, HSSFCellStyle style){
		int i = -1;
		row.createCell(++i).setCellValue(map.get("CustomerID",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("SupplierID",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ItemID",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("LotNo",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ReferenceNo",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ContainerNo",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ExpectedQty",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ScheduledDate",""));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("ReceiptNo",""));
		row.getCell(i).setCellStyle(style);
		
		if ("".equals(map.get("AppointmentDate",""))) {
			row.createCell(++i).setCellValue("");
		} else {
			row.createCell(++i).setCellValue(usaFormat.format(DateUtil.StringToDate(map.get("AppointmentDate",""))));
		}
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue(map.get("InYardDate",""));
		row.getCell(i).setCellStyle(style);
	}
	
	private void setExcelRowValue1(HSSFRow row, Map<String,Object> map, String isBooked, HSSFCellStyle style){
		int i = -1;
		row.createCell(++i).setCellValue((String)map.get("mabd"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("referenceNo"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("po"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((Integer)map.get("orderedQty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((Integer)map.get("availableQty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("shipToParty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("itemId"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("lotNo"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("title"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("error"));
		row.getCell(i).setCellStyle(style);
	}
	/**
	 * 输出一个order中一个sku的确认库存结果
	 * @author Liang Jie
	 * @param row
	 * @param map
	 * @param isBooked
	 * @param style
	 */
	private void setExcelRowValue(HSSFRow row, Map<String,Object> map, String isBooked, HSSFCellStyle style){
		int i = -1;
		row.createCell(++i).setCellValue((String)map.get("companyId"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("referenceNo"));
		row.getCell(i).setCellStyle(style);
		
		row.createCell(++i).setCellValue((String)map.get("orderNo"));
		row.getCell(i).setCellStyle(style);
		
		row.createCell(++i).setCellValue((String)map.get("itemId"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("lotNo"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("title"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((Integer)map.get("orderedQty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((Integer)map.get("availableQty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("satisfied"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("freightTerm"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("calcRequestedDate"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("shipToParty"));
		row.getCell(i).setCellStyle(style);
		row.createCell(++i).setCellValue((String)map.get("printed"));
		row.getCell(i).setCellStyle(style);
		//
	}
	
	/**
	 * 
	 * @param inResultMap
	 * @param rows
	 * @return
	 * @throws Exception 
	 */
	private Map<String,Integer> calculateOnHand(Map<String,Integer> inResultMap, DBRow[] rows, String valueField) throws Exception{
		for(DBRow row:rows){
//			String skuWithTitle = row.getString("LotNo")+"/"+row.getString("ItemID")+"/"+row.getString("SupplierID");
			String skuWithTitle = row.getString("ItemID").trim()+"/"+("".equals(row.get("LotNo",""))?"":row.get("LotNo","")+"/")+row.getString("SupplierID").trim();
			Integer calQty = 0;
			
			if(valueField.equals("BookedQty")){
				Double qty = (Double) row.getValue(valueField);
				calQty = qty.intValue();
			} else {
				calQty = (Integer) row.getValue(valueField);
			}

			if(inResultMap.containsKey(skuWithTitle) && !calQty.equals(0)){
				Integer calculatedQty = inResultMap.get(skuWithTitle).intValue();
				if(valueField.equals("ShippedQty") || valueField.equals("BookedQty") || valueField.equals("wBookedQty")){
					calculatedQty = calculatedQty - calQty;
				}else if(valueField.equals("AdjustedQty")){
					calculatedQty = calculatedQty + calQty;
				}
				inResultMap.put(skuWithTitle, calculatedQty);
			}
		}
		return inResultMap;
	}
		
	/**
	 * 
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	private Map<String,Integer> convertToMap(DBRow[] rows) throws Exception{
		Map<String, Integer> results = new HashMap<String, Integer>();
		for(DBRow row:rows){
			if(row.getString("ItemID")!=null && row.getString("LotNo")!=null 
					&& row.getString("SupplierID")!=null && row.getValue("ReceivedQty")!=null){
				results.put(row.getString("ItemID") +"/" +("".equals(row.get("LotNo",""))?"":row.get("LotNo","")+"/") + row.getString("SupplierID"), (Integer) row.getValue("ReceivedQty"));
			}			
		}
		//System.out.println("E55-C2/10248020245/WISTRON"+results.get("E55-C2/10248020245/WISTRON"));
		//所有Customer都只有Model#,没有LOT#
		/*
		for(DBRow row:rows){
			if(row.getString("ItemID")!=null && row.getString("SupplierID")!=null 
					&& row.getValue("ReceivedQty")!=null){
				results.put(row.getString("ItemID").trim()+"/"+row.getString("SupplierID").trim(), 
						(Integer) row.getValue("ReceivedQty"));
			}			
		}*/
		return results;
	}
		
	/**
	 * 
	 * @param orders
	 * @param bookedOrderList
	 * @return
	 */
	private List<DBRow> removeBookedOrders(DBRow[] orders, DBRow[] bookedOrders){
		List<DBRow> orderList = new ArrayList<DBRow>();
		for(DBRow row : orders){
			String orderNo = row.getString("b2b_oid");
			boolean isBooked = false;
			if(bookedOrders != null){
				for(DBRow bookedOrder : bookedOrders){
					if(orderNo.equals(bookedOrder.getString("b2b_oid"))){
						isBooked = true;
					}				
				}
			}			
			if(!isBooked){
				orderList.add(row);
			}
		}
		return orderList;
	}
	
	private void inputPriorityConditions(DBRow[] orderList, long psid) throws Exception{
		String retailerRank = null;
		String destCity = null;
		
		DBRow cityArr = floorOrderMgr.getStorageCityAndState(psid);
		String originCity = cityArr.get("send_city", "");
		String originState = cityArr.get("state", "");
		Map<String, Integer> stateTTimes = floorOrderMgr.getStateTransTime(originState, originCity);
		
		for(DBRow order:orderList){
			String orderId = order.getString("b2b_oid");
			String shipToId = order.getString("account_id");
			String freightTerm = order.getString("freight_term");
			//String mabd = order.getString("mabd");
			//String companyId = order.getString("company_id");
			
			//基于freightTerm和retailer得到其排序优先级
			//fix 大小写匹配  by wangcr 20/05/2015
			if(!StringUtil.isBlank(freightTerm)){
				if(freightTerm.equalsIgnoreCase("Collect")){
					retailerRank = "1-x";
				}else if(freightTerm.equalsIgnoreCase("Prepaid") || freightTerm.equals("Third Party")){
					retailerRank = "2-x";
				}
			}
			DBRow retailerRow = floorOrderMgr.getRetailerRank(freightTerm, shipToId);
			if(retailerRow!=null && !StringUtils.isNullOrEmpty(retailerRow.getString("rank"))){
				retailerRank = retailerRow.getString("rank");
			}
			DBRow rankRow = new DBRow();
			rankRow.put("retailer_rank", retailerRank);
			//将运输时间存入order表，用于madb-transit time计算后排序
			rankRow.put("calc_requested_date", order.get("mabd"));
			int transit = 9;
			String destState = order.get("deliver_state", "").replaceAll(" ", "").toUpperCase();
			if (!StringUtils.isNullOrEmpty(shipToId) && !shipToId.equals("-1") && !shipToId.equals("0")) {
				destCity = order.get("deliver_city", "");
				DBRow transit_time_row = floorOrderMgr.getTransitTime(shipToId, originCity, originState, destCity, destState);
				
				if (transit_time_row!=null && transit_time_row.get("transit_time", 0)>0) {
					transit = transit_time_row.get("transit_time", 0);
				} else {
					if (stateTTimes.containsKey(destState.toUpperCase())) {
						transit = stateTTimes.get(destState.toUpperCase());
					} else {
						transit = 0;
					}
				}
				
			} else {
				if ("-1".equals(shipToId)) {
					transit = 10;
				} else if ("0".equals(shipToId)) {
					if (stateTTimes.containsKey(destState.toUpperCase())) {
						transit = stateTTimes.get(destState.toUpperCase());
					} else {
						transit = 0;
					}
				} else {
					if (stateTTimes.containsKey(destState.toUpperCase())) {
						transit = stateTTimes.get(destState.toUpperCase());
					} else {
						transit = 0;
					}
				}
			}
			
			rankRow.put("transit_time", transit);
			order.put("retailer_rank", retailerRank);
			try {
				int i = floorOrderMgr.modB2bOrder(orderId, rankRow);
				if(i<0){
					throw new Exception("Failure: Update to retailer rank of OrderID: "+orderId+" has been failed!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	/**
	 * 对所选order列表计算retailer优先级和发货日期
	 * @author Liang Jie
	 * @param orderList
	 * @throws Exception
	 */
	private void inputPriorityConditions(DBRow[] orderList) throws Exception{
		String originCity = null;
		String destCity = null;
		String retailerRank = null;
		
		for(DBRow order:orderList){
			String orderId = order.getString("b2b_oid");
			String shipToId = order.getString("account_id");
			String freightTerm = order.getString("freight_term");
			String mabd = order.getString("mabd");
			String companyId = order.getString("company_id");
			
			//基于freightTerm和retailer得到其排序优先级
			//fix 大小写匹配  by wangcr 20/05/2015
			if(!StringUtil.isBlank(freightTerm)){
				if(freightTerm.equalsIgnoreCase("Collect")){
					retailerRank = "1-x";
				}else if(freightTerm.equalsIgnoreCase("Prepaid") || freightTerm.equals("Third Party")){
					retailerRank = "2-x";
				}
			}
			DBRow retailerRow = floorOrderMgr.getRetailerRank(freightTerm, shipToId);
			if(retailerRow!=null && !StringUtils.isNullOrEmpty(retailerRow.getString("rank"))){
				retailerRank = retailerRow.getString("rank");
			}
			DBRow rankRow = new DBRow();
			rankRow.put("retailer_rank", retailerRank);
			//取消madb-transit time计算后，按req。date by wangcr 20/05/2015
			rankRow.put("calc_requested_date", order.get("mabd"));
			order.put("retailer_rank", retailerRank);
			try {
				int i = floorOrderMgr.modB2bOrder(orderId, rankRow);
				if(i<0){
					throw new Exception("Failure: Update to retailer rank of OrderID: "+orderId+" has been failed!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//基于order关联得到发货城市、收货城市
			DBRow[] cityArr = floorOrderMgr.getCityNameByOrder(orderId);
			for(DBRow city : cityArr){
				if(!StringUtils.isNullOrEmpty(city.getString("origin"))){
					originCity = city.getString("origin");
				}else if(!StringUtils.isNullOrEmpty(city.getString("dest"))){
					destCity = city.getString("dest");
				}
			}
			//取消此逻辑，按req。date by wangcr 20/05/2015
			//将运输时间存入order表，用于madb-transit time计算后排序
			
			/*
			if(!StringUtils.isNullOrEmpty(originCity)&&!StringUtils.isNullOrEmpty(destCity)
					&&!StringUtils.isNullOrEmpty(shipToId) && !shipToId.equals("-1")){
				DBRow transit_time_row = floorOrderMgr.getTransitTime(shipToId, originCity, destCity);
				DBRow warehouseRow = floorWMSOrdersMgr.getConfigSearchWare(companyId);
				if(transit_time_row!=null && !StringUtils.isNullOrEmpty(transit_time_row.getString("transit_time"))
						&& warehouseRow!=null && !StringUtils.isNullOrEmpty(warehouseRow.getString("ps_id"))){
					String transitTime = transit_time_row.getString("transit_time");
					String shipFromLoalMabd = DateUtil.showLocalTime(mabd,Integer.parseInt(warehouseRow.getString("ps_id")));
					String calcRequestedDate = DateUtil.calcDate(shipFromLoalMabd,Integer.parseInt(transitTime),false);
					order.put("transit_time", transitTime);
					DBRow timeRow = new DBRow();
					timeRow.put("transit_time", transitTime);
					timeRow.put("calc_requested_date", order.get(key));
					try {
						int i = floorOrderMgr.modB2bOrder(orderId, timeRow);
						if(i<0){
							throw new Exception("Failure: Update to transit time of OrderID: "+orderId+" has been failed!");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}	
			}
			*/
		}		
	}
	
	/**
	 * 保留order库存
	 * @author Liang Jie
	 * @param orderId
	 */
	private void commitOrder(String orderId, long logingUserId, String logingUserName)  throws Exception{
		TDate tDate = new TDate();
		String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
		DBRow orderRow = new DBRow();
		orderRow.put("b2b_order_status", 2);
		orderRow.put("updatedate", currentTime);
		orderRow.put("updateby", logingUserId);
		orderRow.put("updatename", logingUserName);
		try {
			int i = floorOrderMgr.modB2bOrder(orderId, orderRow);
			if(i<0){
				throw new Exception("Failure: Update to status of OrderID- "+orderId+" has been failed!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//添加日志：库存确认
		DBRow logRow = new DBRow();
		logRow.add("b2b_oid", orderId);
		logRow.add("b2b_content", "commit inventory");
		logRow.add("b2ber_id", logingUserId);
		logRow.add("b2ber", logingUserName);
		logRow.add("b2b_type", B2BOrderLogTypeKey.Update);
		logRow.add("activity_id", "1");
		logRow.add("b2b_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		try {
			floorB2BOrderMgrZyj.addB2BOrderLog(logRow);
		}catch(Exception e){
			throw new Exception("Failure: Add order log!"+e);
		}
		
	}

	private void updateWmsOrder(String orderNo, String companyId, String customerId) throws Exception {
		DBRow wmsRow = new DBRow();
		wmsRow.add("company_id", companyId);
		wmsRow.add("b2b_order_number", orderNo);
		wmsRow.add("customer_id", customerId);
		
		try {
			wmsOrderMgr.update(wmsRow, WmsOrderUpdateTypeKey.COMMIT);
		}catch(Exception e){
			throw new Exception("Failure: Update WMS Order status!"+e);
		}		
	}
	/**
	 * @param floorSQLServerMgrZJ the floorSQLServerMgrZJ to set
	 */
	public void setFloorSQLServerMgrZJ(FloorSQLServerMgrZJ floorSQLServerMgrZJ) {
		this.floorSQLServerMgrZJ = floorSQLServerMgrZJ;
	}

	/**
	 * @param floorOrderMgr the floorOrderMgr to set
	 */
	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	/**
	 * @param floorWMSOrdersMgr the floorWMSOrdersMgr to set
	 */
	public void setFloorWMSOrdersMgr(FloorWMSOrdersMgr floorWMSOrdersMgr) {
		this.floorWMSOrdersMgr = floorWMSOrdersMgr;
	}

	/**
	 * @param floorB2BOrderMgrZyj the floorB2BOrderMgrZyj to set
	 */
	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}

	/**
	 * @param wmsOrderMgr the wmsOrderMgr to set
	 */
	public void setWmsOrderMgr(WMSOrderMgr wmsOrderMgr) {
		this.wmsOrderMgr = wmsOrderMgr;
	}
}

class ComparatorDate implements Comparator<Map<String,Object>>{
	public int compare(Map<String,Object> v1, Map<String,Object> v2) {
		try {
			String k1 = (String)v1.get("requestedDate");
			String k2 = (String)v2.get("requestedDate");
			return k1.compareTo(k2);
		} catch (Exception e) {
			return 0;
		}
	}
}
