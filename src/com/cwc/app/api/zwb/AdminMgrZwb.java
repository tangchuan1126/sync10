package com.cwc.app.api.zwb;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zwb.FloorAdminMgrZwb;
import com.cwc.app.iface.zwb.AdminMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class AdminMgrZwb implements AdminMgrIfaceZwb{
	
    private FloorAdminMgrZwb floorAdminMgrZwb;
    static Logger log = Logger.getLogger("ACTION");
    
    
    public DBRow[] androidLogin(String name,String pwd)throws Exception {
		try{
			String password=StringUtil.getMD5(pwd);
			return this.floorAdminMgrZwb.getAdmin(name,password);
		}catch(Exception e){
			throw new SystemException(e,"androidLogin",log);
		}	
    }
    
    public DBRow[] getAdminDetil(long psId)throws Exception{
    	try{
			return this.floorAdminMgrZwb.getAdminDetil(psId);
		}catch(Exception e){
			throw new SystemException(e,"getAdminDetil",log);
		}	
    }
    
	public DBRow[] getProprietaryByadminId(long adminId)throws Exception{
		try{
			return this.floorAdminMgrZwb.getProprietaryByadminId(adminId);
		}catch(Exception e){
			throw new SystemException(e,"getProprietaryByadminId",log);
		}
	}
	
	//判断登录用户的分组 是否为用户
	public Boolean findAdminGroupByAdminId(long adminId)throws Exception{
		try{
			DBRow row=this.floorAdminMgrZwb.findAdminGroupByAdminId(adminId);
			if(row==null){
				return false;   //不在客户分组里
			}else{
				return true;    //是客户
			}
		}catch(Exception e){
			throw new SystemException(e,"findAdminGroupByAdminId",log);
		}
	}
    
	/**
	 * 通过用户ID,查询此用户是否是客户
	 * zyj
	 */
	@Override
	public int findAdminByGroupClientAdid(long adminId) throws Exception {
		try{
			return floorAdminMgrZwb.findAdminByGroupClientAdid(adminId);
		}catch(Exception e){
			throw new SystemException(e,"findAdminByGroupClientAdid(long adminId)",log);
		}
	}
	
	public DBRow findAdminPs(long adminId)throws Exception{
		try{
			return this.floorAdminMgrZwb.findAdminPs(adminId);
		}catch(Exception e){
			throw new SystemException(e,"findAdminPs(long adminId)",log);
		}
	}
	
	
	
	public void setFloorAdminMgrZwb(FloorAdminMgrZwb floorAdminMgrZwb) {
		this.floorAdminMgrZwb = floorAdminMgrZwb;
	}
	
}
