package com.cwc.app.api.zwb;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.CheckinActionType;
import com.cwc.app.api.EntryTicketCheck;
import com.cwc.app.api.Equipment;
import com.cwc.app.api.GateCheckoutInfo;
import com.cwc.app.api.Warehouse;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.LoadNumberNotExistException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustHasTasksException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.ResourceHasUsedException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.exception.checkin.VerifyShippingCTNRException;
import com.cwc.app.floor.api.cc.FloorReportDataInterfaceMgrCc;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zj.FloorEquipmentMgr;
import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.floor.api.zr.FloorRelativeFileMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zwb.FloorWaybillMgrZwb;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.LoadPaperWorkMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.iface.zr.StorageYardControlIfaceZr;
import com.cwc.app.iface.zr.WmsLoadMgrZrIface;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.iface.zyj.OrderSystemMgrIFace;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInEntryWaitingOrNotKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInOrderComeStatusKey;
import com.cwc.app.key.CheckInSpotStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.ContainerImportStatusKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.OrderSystemTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ReturnReceiveCategoryKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.lucene.zwb.CheckInCarrierIndexMgr;
import com.cwc.app.lucene.zwb.CheckInIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.Md5;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.CheckInEntryPermissionUtil;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.service.file.action.zr.ImgToPdfMgr;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.XmlUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;
import com.fr.base.core.util.SerializationUtils;
import com.lowagie.text.DocumentException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Pattern;

public class CheckInMgrZwb implements CheckInMgrIfaceZwb {
	/*private static final String LOGIN_URL = "http://127.0.0.1:8000/Sync10/action/admin/AccountLoginMgrAction.action";
	private static final String CHECK_IN_URL = "http://127.0.0.1:8000/Sync10/action/administrator/checkin/gateCheckInAddAction.action";*/
	
    private FloorCheckInMgrZwb floorCheckInMgrZwb;
    private FloorFileMgrZr floorFileMgrZr;
    private SystemConfigIFace systemConfig;
    private ScheduleMgrIfaceZR scheduleMgrZr;
    private SQLServerMgrIFaceZJ sqlServerMgrZJ;
    private FloorWaybillMgrZwb floorWaybillMgrZwb;
    private TransactionTemplate sqlServerTransactionTemplate;
    private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
    private WmsLoadMgrZrIface wmsLoadMgrZr; // 在根据一个Load查询到数据的时候，把load 和
    
    // order的数据先保存在本地的数据库中
    private AdminMgrIFaceZJ adminMgrZJ;
    private LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr;
    private CheckInMgrIFaceZyj checkInMgrZyj;
    private StorageDoorMgrIfaceZr storageDoorMgrZr;
    private StorageYardControlIfaceZr storageYardControlZr;
    private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr; // 资源
    private FloorEquipmentMgr floorEquipmentMgr; // 设备
    private YMSMgrAPI ymsMgrAPI;
    private AdminMgr adminMgr;
    private OrderSystemMgrIFace orderSystemMgr;
    private FloorCheckInMgrWfh floorCheckInMgrWfh;
    private FloorRelativeFileMgrZr floorRelativeFileMgrZr;
    private FileMgrIfaceZr fileMgrZr;
    private ImgToPdfMgr imgToPdfMgr;
    private BASE64Decoder decoder;
    protected String downLoadFileUrl = "";
    protected String uploadFileUrl = "";
    protected String checkinUploadFileUrl = "";
    protected String ymsFileDownLoadUrl = "";
    protected UploadAndDownloadPDF uploadAndDownloadPDF;
    
    protected String rabbitMqServer ;
    protected String rabbitMqUserName ;
    protected String rabbitMqPassword ;
    protected String checkInQueue ;
    protected String checkOutQueue ;
    
    
    public String getRabbitMqServer() {
		return rabbitMqServer;
	}

	public void setRabbitMqServer(String rabbitMqServer) {
		this.rabbitMqServer = rabbitMqServer;
	}

	public String getRabbitMqUserName() {
		return rabbitMqUserName;
	}

	public void setRabbitMqUserName(String rabbitMqUserName) {
		this.rabbitMqUserName = rabbitMqUserName;
	}

	public String getRabbitMqPassword() {
		return rabbitMqPassword;
	}

	public void setRabbitMqPassword(String rabbitMqPassword) {
		this.rabbitMqPassword = rabbitMqPassword;
	}

	public String getCheckInQueue() {
		return checkInQueue;
	}

	public void setCheckInQueue(String checkInQueue) {
		this.checkInQueue = checkInQueue;
	}

	public String getCheckOutQueue() {
		return checkOutQueue;
	}

	public void setCheckOutQueue(String checkOutQueue) {
		this.checkOutQueue = checkOutQueue;
	}

	
    
  

	public void setUploadAndDownloadPDF(UploadAndDownloadPDF uploadAndDownloadPDF) {
		this.uploadAndDownloadPDF = uploadAndDownloadPDF;
	}

	public void setYmsFileDownLoadUrl(String ymsFileDownLoadUrl) {
		this.ymsFileDownLoadUrl = ymsFileDownLoadUrl;
	}

	public void setCheckinUploadFileUrl(String checkinUploadFileUrl) {
		this.checkinUploadFileUrl = checkinUploadFileUrl;
	}

    public static int LoadCloseNotifyStayOrLeave = 1; // 关闭load的时候提示停留或者的是离开 ，
    // 当前最后一个单据下最后一个子单据
    public static int LoadCloseNotifyReleaseDoor = 2; // 关闭load的时候提示是否relaeaseDoor
    // ,当前门下面最后一个子单据
    public static int LoadCloseNoifyNormal = 3; // 关闭load的时候 如果不是最后一个（door
    // 或者是Entry ）正常
    public static int LoadCloseNotifyInputSeal = 4; // 本单据下面最后一个子单据的时候提示输入Seal
    public static int LoadCloseForgateNotifyReleaseDoorOrStayOrLeave = 5; // 如果是ForgateClose
// 是门下面最后一个
// ，
// 或者是Entry最后一个的时候（不用返回2,1）

    private FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc;

    public void setFloorReportDataInterfaceMgrCc(
        FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc) {
        this.floorReportDataInterfaceMgrCc = floorReportDataInterfaceMgrCc;
    }

    public static int TimeSetOut = -1; // 大屏幕的时候-1 表示永远不过期
    static Logger log = Logger.getLogger("ACTION");
    // 保存的路径
    private static String PDFPath = Environment.getHome().replace("\\", "/")
        + "upl_pdf_tmp/";
    ModuleKey moduleKey = new ModuleKey();
    CheckInMainDocumentsStatusTypeKey checkInMainDocumentsStatusTypeKey = new CheckInMainDocumentsStatusTypeKey();
    CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
    CheckInLogTypeKey logTypeKey = new CheckInLogTypeKey();
    OccupyTypeKey occupyTypeKey = new OccupyTypeKey();

    // 根据id查询主表
    public DBRow selectMainByEntryId(long id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectMainByEntryId(id);
        } catch (Exception e) {
            throw new SystemException(e, "selectMainByEntryId", log);
        }
    }

    /**
     * 查询门是否存在或占用
     */
    @Override
    public DBRow selectDoorisExistByDoorName(long ps_id, String doorName)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectDoorisExistByDoorName(ps_id,
                doorName);
        } catch (Exception e) {
            throw new SystemException(e, "selectDoorisExistByDoorName", log);
        }
    }

    /**
     * 查询门列表
     */
    public DBRow[] selectAllDoor(long ps_id, String doorName, long dlo_id)
        throws Exception { // 要改张睿
        try {
            return this.floorCheckInMgrZwb.selectAllDoor(ps_id, doorName,
                dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectAllDoor", log);
        }
    }

    /**
     * 查询门位置关系
     */
    public DBRow[] findDoorAndLocation(long doorId, long psId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findDoorAndLocation(doorId, psId);
        } catch (Exception e) {
            throw new SystemException(e, "findDoorAndLocation", log);
        }
    }

    /**
     * 根据主单id 查询loding
     */
    public DBRow[] findloadingByInfoId(long dlo_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "findloadIngByInfoId", log);
        }
    }

    /**
     * 查询详细表去重 门
     */
    public DBRow[] selectDoorByInfoId(long infoId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectDoorByInfoId(infoId);
        } catch (Exception e) {
            throw new SystemException(e, "selectDoorByInfoId", log);
        }
    }

    /**
     * 根据门查询load数据和主单据id2014-11-25 主要是获取门下面的Task
     */
    public DBRow[] selectLotNumberByDoorId(long doorId, long infoId)
        throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectLotNumberByDoorId(
                doorId, infoId);
// for(int i=0;i<rows.length;i++){
// long associate_id=rows[i].get("dlo_detail_id", 0l);
// DBRow[] shRow=this.floorCheckInMgrZwb.findSchedule(associate_id);
// if(shRow.length>0 && shRow!=null){
//
// }
// }
//
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectLotNumberByDoorId", log);
        }
    }

    /**
     * 查询主单据功能 带分页
     *
     * @param pc
     * @return
     * @throws Exception
     */
    public DBRow[] selectAllMain(long id, PageCtrl pc) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectAllMain(id, pc);
        } catch (Exception e) {
            throw new SystemException(e, "selectAllMain", log);
        }
    }

    // 查询GPS根据id
    public DBRow findAssetByGpsId(long gps_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findAssetByGpsId(gps_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectAllMain", log);
        }
    }

    // 根据id查询主单据信息--打印用
    public DBRow findGateCheckInById(long id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findGateCheckInById(id);
        } catch (Exception e) {
            throw new SystemException(e, "findGateCheckInById", log);
        }
    }

    // android check out 扫描entryid
    public DBRow androidCheckOutSearchEntryId(HttpServletRequest request)
        throws Exception {
        try {
            long entry_id = StringUtil.getLong(request, "entry_id");
            DBRow result = new DBRow();
            DBRow row = this.findGateCheckInById(entry_id);
            if (row != null) {
                result.add("entry_id", row.get("dlo_id", 0l));
                result.add("liscense_plate", row.get("gate_liscense_plate", ""));
                result.add("container_no", row.get("gate_container_no", ""));
                result.add("driver_liscense",
                    row.get("gate_driver_liscense", ""));
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "androidCheckOutSearchEntryId", log);
        }
    }

    /**
     * 添加数据到detail表
     */
    private long windowCheckInMainId;

    @Override
    public long addOccupancyDetails(final long adid,
                                    final HttpServletRequest request) throws Exception {
        try {
            windowCheckInMainId = 0;
            sqlServerTransactionTemplate
                .execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(
                        TransactionStatus txStat) {
                        try {
                            String det_detail_id = StringUtil.getString(
                                request, "det_detail_id");// 删除的子单据ID，ID……
                            String str = StringUtil.getString(request,
                                "str");// 添加或修改的子单据数据
                            JSONObject json = new JSONObject(str);
                            String mainId = json.getString("mainId");
                            windowCheckInMainId = Long.parseLong(mainId);
                            String in_seal = json.getString("in_seal");
                            String out_seal = json.getString("out_seal");
                            String appointmentdate = json
                                .getString("appointmentdate");
                            String waitingOrNotStr = json
                                .getString("waiting");
                            int waitingOrNot = Integer
                                .parseInt(waitingOrNotStr);
                            int priority = StringUtil.getInt(request,
                                "priority");
                            int rel_type = json.getInt("rel_type");// StringUtil.getInt(request,
// "rel_type");
                            int is_live = json.getInt("is_live");// StringUtil.getInt(request,
// "is_live");
                            JSONArray items = json.getJSONArray("items");
// 获取主表信息,如果不是pick ctnr,且items长度为0，不能没有子单据
                            DBRow mRow = floorCheckInMgrZwb
                                .findGateCheckInById(Long
                                    .parseLong(mainId));
                            if (rel_type != CheckInMainDocumentsRelTypeKey.CTNR
                                && items.length() == 0) {
                                CheckInMainDocumentsRelTypeKey relTypeKey = new CheckInMainDocumentsRelTypeKey();
                                throw new CheckInMustHasTasksException(
                                    relTypeKey
                                        .getCheckInMainDocumentsRelTypeKeyValue(rel_type));
                            }
                            if (rel_type == CheckInMainDocumentsRelTypeKey.CTNR
                                && items.length() != 0) {
                                CheckInMainDocumentsRelTypeKey relTypeKey = new CheckInMainDocumentsRelTypeKey();
                                throw new CheckInMustNotHaveTasksException(
                                    relTypeKey
                                        .getCheckInMainDocumentsRelTypeKeyValue(rel_type));
                            } // 没有task时且未做过window的，将主表上占用的门或停车位释放
                            windowCheckInReleaseNoTaskDoorOrSpot(
                                windowCheckInMainId, items.length(),
                                mRow.getString("window_check_in_time"),
                                request, adid);
// 创建时间
                            Date date = (Date) Calendar.getInstance()
                                .getTime();
                            SimpleDateFormat dateformat = new SimpleDateFormat(
                                "yyyy-MM-dd HH:mm:ss");
                            String createTime = dateformat.format(date);
// 获得创建人
                            AdminMgr adminMgr = new AdminMgr();
                            AdminLoginBean adminLoginBean = adminMgr
                                .getAdminLoginBean(StringUtil
                                    .getSession(request));
                            long createManId = adminLoginBean.getAdid();
// 修改前占用的门和停车位，用来验证修改之后的门和停车位是否有问题
                            DBRow[] hasSavedDetails = floorCheckInMgrZwb
                                .findloadingByInfoId(Long
                                    .parseLong(mainId));
                            DBRow usedDoorOrSpots = windowCheckInHasUsedDoorOrSpots(hasSavedDetails);
                            List<DBRow> usedDoorDetails = (ArrayList<DBRow>) usedDoorOrSpots
                                .get("doors", new ArrayList<DBRow>());// 曾经占用的门和单据信息
                            List<DBRow> usedSpotDetails = (ArrayList<DBRow>) usedDoorOrSpots
                                .get("spots", new ArrayList<DBRow>());// 曾经占用的spot

// 处理删除的子单据，处理window任务和warehouse任务
                            windowCheckInHandleDeleteDetails(det_detail_id,
                                request, in_seal, out_seal);

                            long load_count = 0;
                            Boolean pickup = false;
                            Boolean delivery = false;
// 处理子单据
                            String ctn_number_first = "";
// 所有子表 的containerNo都为空时，将主表的ctnr写到所有子表中
                            for (int i = 0; i < items.length(); i++) {
                                DBRow row = windowCheckInSubmitJsonToDBRow(
                                    items.getJSONObject(i),
                                    Long.parseLong(mainId));
                                String detailId = row
                                    .getString("dlo_detail_id");
                                int bill_type = row.get("number_type", 0);
                                String number = row.getString("number");
                                String ctn_number = row
                                    .getString("ctn_number");
                                if (!StrUtil.isBlank(ctn_number)) {
                                    if (StrUtil.isBlank(ctn_number_first)) {
                                        ctn_number_first = ctn_number;
                                    }
                                } else {
                                    ctn_number = ctn_number_first;
                                }
                                row.add("ctn_number", ctn_number);
                                String ids = row.getString("ids");
                                String doorId = row.getString("rl_id");
                                int occupancy_type = row.get(
                                    "occupancy_type", 0);
                                String occupancyName = "";
                                if (occupancy_type == OccupyTypeKey.DOOR) {
                                    DBRow occupancyRow = floorCheckInMgrZwb
                                        .findDoorById(Long
                                            .parseLong(doorId));
                                    occupancyName = (null == occupancyRow) ? ""
                                        : occupancyRow
                                        .getString("doorId");
                                } else if (occupancy_type == OccupyTypeKey.SPOT) {
                                    DBRow occupancyRow = storageYardControlZr
                                        .findSpotDetaiBySpotId(Long
                                            .parseLong(doorId));
                                    occupancyName = (null == occupancyRow) ? ""
                                        : occupancyRow
                                        .getString("yc_no");
                                }
// 处理明细是PickUp还是Delivery，发邮件内容，load数量
                                DBRow loadTypeCountContent = windowCheckInHandleLoadTypeCountContent(
                                    bill_type, number, occupancy_type,
                                    occupancyName, row, mRow,
                                    createTime, adid, request);
                                String context = loadTypeCountContent
                                    .getString("context");
                                if (!pickup) {
                                    pickup = loadTypeCountContent.get(
                                        "pickup", 0) == 1 ? true
                                        : false;
                                }
                                if (!delivery) {
                                    delivery = loadTypeCountContent.get(
                                        "delivery", 0) == 1 ? true
                                        : false;
                                }
                                load_count = loadTypeCountContent.get(
                                    "load_count", 0);

// 根据主id查询 如果查到就更新没查到就反填
                                String emailTitle = "CHECK IN WINDOW";
                                Boolean yj = row.get("yj", 0) == 1 ? true
                                    : false;
                                Boolean dx = row.get("dx", 0) == 1 ? true
                                    : false;
                                Boolean ym = row.get("ym", 0) == 1 ? true
                                    : false;
// 根据主单据id 和子单据id 查询子单据 是否存在
                                long id = 0;
                                if (!detailId.equals("")) { // 如果有子表id 就更新子表
// 信息
                                    id = Long.parseLong(detailId);
// updateDetailByIsExist(detailId,Long.parseLong(doorId),1,1,zoneId,Long.parseLong(String.valueOf(bill_type)),number);
// //更新
                                    String loadData = "";
                                    if (bill_type == ModuleKey.CHECK_IN_LOAD) {
// 将load的MasterBolNo和OrderNo写到checkin明细表中
                                        loadData = sqlServerMgrZJ
                                            .findMasterBolNoOrderNoStrByLoadNo(
                                                number, adid);// ,request
                                    }
                                    row.add("master_order_nos", loadData);

// 单据号变了，子单据状态需要改,门需要改；如果门变了，需要改变门状态，子单据
                                    DBRow detailRowExist = floorCheckInMgrZwb
                                        .selectDetailByDetailId(id);
// TODO 在其他界面删除了
                                    if (null == detailRowExist) {
                                        throw new CheckInTaskHaveDeletedException();
                                    }
                                    long occupiedTypeId = detailRowExist
                                        .get("rl_id", 0L);
                                    int occupiedTypeKey = detailRowExist
                                        .get("occupancy_type", 0);
                                    String numberExist = detailRowExist
                                        .getString("number");
                                    int numberTypeExist = detailRowExist
                                        .get("number_type", 0);

// task或者门改变了，将单据状态为未处理，门占用状态改成预留，将wareHouse任务删除
                                    if (!numberExist.equals(number)
                                        || numberTypeExist != bill_type
                                        || occupiedTypeId != Long
                                        .parseLong(doorId)
                                        || occupiedTypeKey != occupancy_type) {
// 单据或者门改变，将单据状态为未处理，门占用状态改成预留
                                        row.add("number_status",
                                            CheckInChildDocumentsStatusTypeKey.UNPROCESS);// 改子单据状态
                                        if (occupancy_type == OccupyTypeKey.DOOR) {
                                            row.add("occupancy_status",
                                                OccupyStatusTypeKey.RESERVERED);
                                        } else if (occupancy_type == OccupyTypeKey.SPOT) {
                                            row.add("occupancy_status",
                                                CheckInSpotStatusTypeKey.OCUPIED);
                                        }
// 删除warehouse任务，通知
                                        delteTaskScheduleByDetailId(id,
                                            request);
                                    }

                                    row.remove("yj");
                                    row.remove("dx");
                                    row.remove("ym");
                                    row.remove("ids");
                                    row.remove("dlo_detail_id");
                                    floorCheckInMgrZwb
                                        .updateDetailByIsExist(Long
                                                .parseLong(detailId),
                                            row);
// 添加日志
                                    windowAddLog("update", number,
                                        bill_type, windowCheckInMainId,
                                        request, occupancy_type,
                                        occupancyName, ids, in_seal,
                                        out_seal);
// 删除通知，先查询通知
                                    DBRow[] shRows = floorCheckInMgrZwb
                                        .selectSchedule(
                                            id,
                                            ProcessKey.CHECK_IN_WINDOW);
                                    if (shRows.length > 0 && shRows != null) {
                                        scheduleMgrZr
                                            .deleteScheduleByScheduleId(
                                                shRows[0]
                                                    .get("schedule_id",
                                                        0l),
                                                request);
                                    }
                                } else {
                                    if (occupancy_type == OccupyTypeKey.DOOR) {
                                        row.add("occupancy_status",
                                            OccupyStatusTypeKey.RESERVERED);
                                    } else if (occupancy_type == OccupyTypeKey.SPOT) {
                                        row.add("occupancy_status",
                                            CheckInSpotStatusTypeKey.OCUPIED);
                                    }
                                    row.add("number_status", 1);
                                    row.remove("yj");
                                    row.remove("dx");
                                    row.remove("ym");
                                    row.remove("ids");
                                    row.remove("dlo_detail_id");
                                    id = floorCheckInMgrZwb
                                        .addOccupancyDetails(row); // 添加详细数据
                                    windowAddLog("add", number, bill_type,
                                        windowCheckInMainId, request,
                                        occupancy_type, occupancyName,
                                        ids, in_seal, out_seal);
                                }
// 发通知，上面删除，此处添加
                                if (!ids.equals("")) {
                                    scheduleMgrZr
                                        .addScheduleByExternalArrangeNow(
                                            createTime,
                                            "",
                                            createManId,
                                            ids,
                                            "",
                                            id,
                                            ModuleKey.CHECK_IN,
                                            emailTitle,
                                            context,
                                            ym,
                                            yj,
                                            dx,
                                            request,
                                            true,
                                            ProcessKey.CHECK_IN_WINDOW);
                                }
                            }
// 处理主单据
                            if (rel_type != CheckInMainDocumentsRelTypeKey.CTNR) {
                                rel_type = windowCheckInHandleMainRelType(
                                    pickup, delivery);
                            } else {
                                is_live = CheckInLiveLoadOrDropOffKey.PICK_UP;
                            }
                            DBRow mainRow = windowCheckInHandleMainRow(
                                mRow, in_seal, out_seal,
                                appointmentdate, adminLoginBean,
                                priority, Long.parseLong(mainId),
                                createTime, createManId, load_count,
                                rel_type, ctn_number_first,
                                waitingOrNot, is_live);
                            floorCheckInMgrZwb.modCheckIn(
                                windowCheckInMainId, mainRow);

// windowCheckInEntryTasksAllClosedCanntWaiting(windowCheckInMainId,
// waitingOrNot);//所有task关了，不能上waiting
                            windowCheckInUpdateCtnNumberByDloId(
                                windowCheckInMainId,
                                mainRow.getString("gate_container_no"));// 没有填过ctnr，为na；如果所有task都没有,用主表的;主表未填用第一个非na的task的ctnr
                            windowCheckIncheckTaskTypeAndTaskRepeat(
                                windowCheckInMainId,
                                mainRow.get("ps_id", 0L),
                                hasSavedDetails);// 处理单据是否重复,同entry或非同entry
                            editCheckInIndex(windowCheckInMainId, "update");
                            addFiles(request, windowCheckInMainId,
                                createManId);
                            updateReceiptsByCtnOrBols(windowCheckInMainId);
// 处理Door和Spot自身状态
                            windowCheckInHandleDoorOrSpotStatus(
                                usedDoorDetails, windowCheckInMainId,
                                OccupyTypeKey.DOOR, adid);
                            windowCheckInHandleDoorOrSpotStatus(
                                usedSpotDetails, windowCheckInMainId,
                                OccupyTypeKey.SPOT, adid);
                        } catch (DoorHasUsedException e1) {
                            throw new DoorHasUsedException(e1.getMessage());
                        } catch (SpotHasUsedException e) {
                            throw new SpotHasUsedException(e.getMessage());
                        } catch (CheckInTaskCanntDeleteException e) {
                            throw new CheckInTaskCanntDeleteException(e
                                .getMessage());
                        } catch (CheckInTaskRepeatToEntryException e) {
                            throw new CheckInTaskRepeatToEntryException(e
                                .getMessage());
                        } catch (CheckInTaskRepeatToOthersException e) {
                            throw new CheckInTaskRepeatToOthersException(e
                                .getMessage());
                        } catch (CheckInMustHasTasksException e) {
                            throw new CheckInMustHasTasksException(e
                                .getMessage());
                        } catch (CheckInAllTasksClosedException e) {
                            throw new CheckInAllTasksClosedException();
                        } catch (CheckInMustNotHaveTasksException e) {
                            throw new CheckInMustNotHaveTasksException(e
                                .getMessage());
                        } catch (CheckInTaskHaveDeletedException e) {
                            throw new CheckInTaskHaveDeletedException();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            return windowCheckInMainId;
        } catch (DoorHasUsedException e1) {
            throw new DoorHasUsedException(e1.getMessage());
        } catch (SpotHasUsedException e) {
            throw new SpotHasUsedException(e.getMessage());
        } catch (CheckInTaskCanntDeleteException e) {
            throw new CheckInTaskCanntDeleteException(e.getMessage());
        } catch (CheckInTaskRepeatToEntryException e) {
            throw new CheckInTaskRepeatToEntryException(e.getMessage());
        } catch (CheckInTaskRepeatToOthersException e) {
            throw new CheckInTaskRepeatToOthersException(e.getMessage());
        } catch (CheckInMustHasTasksException e) {
            throw new CheckInMustHasTasksException(e.getMessage());
        } catch (CheckInAllTasksClosedException e) {
            throw new CheckInAllTasksClosedException();
        } catch (CheckInMustNotHaveTasksException e) {
            throw new CheckInMustNotHaveTasksException(e.getMessage());
        } catch (CheckInTaskHaveDeletedException e) {
            throw new CheckInTaskHaveDeletedException();
        } catch (Exception e) {
            throw new SystemException(e, "addOccupancyDetails", log);
        }
    }

    /**
     * 释放主单据占用的门或停车位
     *
     * @param dlo_id
     * @param itemLen
     * @param windowCheckInTime
     * @param request
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月20日 下午12:22:29
     */
    public void windowCheckInReleaseNoTaskDoorOrSpot(long dlo_id, int itemLen,
                                                     String windowCheckInTime, HttpServletRequest request, long adid)
        throws Exception {
        if (itemLen != 0 && StrUtil.isBlank(windowCheckInTime)) {
            int occupancy_type = 0;
            String releaseName = "";
            DBRow[] useDoors = storageDoorMgrZr.getUseDoorByAssociateIdAndType(
                dlo_id, ModuleKey.CHECK_IN);
            if (useDoors.length == 1) {
                storageDoorMgrZr.freeDoor(useDoors[0].get("sd_id", 0L), dlo_id,
                    adid, ModuleKey.CHECK_IN);
                occupancy_type = OccupyTypeKey.DOOR;
                releaseName = useDoors[0].getString("doorId");
            }
            DBRow[] useSpots = storageYardControlZr
                .getUseSpotByAssociateIdAndType(dlo_id, ModuleKey.CHECK_IN);
            if (useSpots.length == 1) {
                storageYardControlZr.freeSpot(useSpots[0].get("yc_id", 0L),
                    dlo_id, adid, ModuleKey.CHECK_IN);
                occupancy_type = OccupyTypeKey.SPOT;
                releaseName = useSpots[0].getString("yc_no");
            }
            windowAddLog("release_main", "", 0, windowCheckInMainId, request,
                occupancy_type, releaseName, "", "", "");
        }
    }

    /**
     * entryt处理删除数据
     *
     * @param det_detail_id
     * @param request
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月18日 下午8:06:59
     */
    public void windowCheckInHandleDeleteDetails(String det_detail_id,
                                                 HttpServletRequest request, String in_seal, String out_seal)
        throws Exception {
        try {
            if (!det_detail_id.equals("")) {
                String[] det_detail_ids = det_detail_id.split(",");
                for (int i = 0; i < det_detail_ids.length; i++) {
                    long det_id = Long.parseLong(det_detail_ids[i]);
                    DBRow detailRow = floorCheckInMgrZwb
                        .selectDetailByDetailId(det_id);
                    if (detailRow.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY
                        || detailRow.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION
                        || detailRow.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.CLOSE) {
                        ModuleKey moduleKey = new ModuleKey();
                        String errorMsg = moduleKey.getModuleName(detailRow
                            .get("number_type", 0))
                            + ":"
                            + detailRow.getString("number");
                        if (!StrUtil.isBlank(detailRow.getString("company_id"))) {
                            errorMsg += "(" + detailRow.getString("company_id")
                                + ")";
                        }
                        throw new CheckInTaskCanntDeleteException(errorMsg);
                    }
                    detCheckinWindow(request, det_id);
                    String doorOrSpot = "";
                    if (detailRow.get("occupancy_type", 0) == OccupyTypeKey.DOOR) {
                        DBRow door = storageDoorMgrZr
                            .findDoorByDoorIdAssociation(
                                detailRow.get("rl_id", 0L),
                                ModuleKey.CHECK_IN,
                                detailRow.get("dlo_id", 0L));
                        doorOrSpot = null != door ? door.getString("doorId")
                            : "";
                    } else if (detailRow.get("occupancy_type", 0) == OccupyTypeKey.SPOT) {
                        DBRow spot = storageYardControlZr
                            .findSpotsBySpotIdAssociateId(
                                detailRow.get("rl_id", 0L),
                                ModuleKey.CHECK_IN,
                                detailRow.get("dlo_id", 0L));
                        doorOrSpot = null != spot ? spot.getString("yc_no")
                            : "";
                    }

                    DBRow[] joins = floorCheckInMgrZwb.selectSchedule(det_id,
                        ProcessKey.CHECK_IN_WINDOW);
                    String ids = "";
                    for (int j = 0; j < joins.length; j++) {
                        ids += "," + joins[j].get("adid", 0L);
                    }
                    if (!StrUtil.isBlank(ids)) {
                        ids = ids.substring(1);
                    }
                    windowAddLog("delete", detailRow.getString("number"),
                        detailRow.get("number_type", 0),
                        detailRow.get("dlo_id", 0L), request,
                        detailRow.get("occupancy_type", 0), doorOrSpot,
                        ids, in_seal, out_seal);
                }
            }
        } catch (CheckInTaskCanntDeleteException e) {
            throw new CheckInTaskCanntDeleteException(e.getMessage());
        }
    }

    /**
     * 批量更新子表的ctn_number
     *
     * @param dlo_id
     * @param ctn_number
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午7:46:47
     */
    public void windowCheckInUpdateCtnNumberByDloId(long dlo_id,
                                                    String ctn_number) throws Exception {
        if (dlo_id > 0) {
            if (StrUtil.isBlank(ctn_number)) {
                ctn_number = "NA";
            }
            DBRow[] detailsNow = floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
            for (int i = 0; i < detailsNow.length; i++) {
                if (StrUtil.isBlank(detailsNow[i].getString("ctn_number"))
                    || "NA".toUpperCase().equals(
                    detailsNow[i].getString("ctn_number"))) {
                    DBRow row = new DBRow();
                    row.add("ctn_number", ctn_number);
                    floorCheckInMgrZwb.updateDetail(
                        detailsNow[i].get("dlo_detail_id", 0L), row);
                }
            }
        }
    }

    /**
     * 查看entry所有明细是否都关了，如果都关了，不能waiting
     *
     * @param dlo_id
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月18日 下午8:00:23
     */
    public void windowCheckInEntryTasksAllClosedCanntWaiting(long dlo_id,
                                                             int waitingOrNot) throws Exception {
        try {
            DBRow[] detailsNow = floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
            boolean isCanWait = false;
            for (int i = 0; i < detailsNow.length; i++) {
                int status = detailsNow[i].get("number_status", 0);
                if (status == CheckInChildDocumentsStatusTypeKey.PROCESSING
                    || status == CheckInChildDocumentsStatusTypeKey.UNPROCESS) {
                    isCanWait = true;
                    break;
                }
            }
            if (!isCanWait
                && waitingOrNot == CheckInEntryWaitingOrNotKey.WAITING) {
                throw new CheckInAllTasksClosedException();
            }
        } catch (CheckInAllTasksClosedException e) {
            throw new CheckInAllTasksClosedException();
        }
    }

    /**
     * windowCheckIn验证task是否重复
     *
     * @param dlo_id
     * @param ps_id
     * @param detailsBefore
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午7:26:46
     */
    public void windowCheckIncheckTaskTypeAndTaskRepeat(long dlo_id,
                                                        long ps_id, DBRow[] detailsBefore) throws Exception {
        try {
            ModuleKey moduleKey = new ModuleKey();
            Set<String> setBefore = new HashSet<String>();
            for (int i = 0; i < detailsBefore.length; i++) {
                setBefore.add(detailsBefore[i].getString("number") + " | "
                    + detailsBefore[i].get("number_type", 0) + " | "
                    + detailsBefore[i].getString("company_id"));
            }

            DBRow[] detailsNow = floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
// 处理entry的task
            Set<String> detailNumberAndType = new HashSet<String>();
            for (int i = 0; i < detailsNow.length; i++) {
                String number = detailsNow[i].getString("number");
                int number_type = detailsNow[i].get("number_type", 0);
                String company_id = detailsNow[i].getString("company_id");
                String numberAndType = number + " | " + number_type + " | "
                    + company_id;
                String errorMsg = moduleKey.getModuleName(number_type) + ":"
                    + number;
                if (!StrUtil.isBlank(company_id)) {
                    errorMsg += "(" + company_id + ")";
                }
                if (!detailNumberAndType.add(numberAndType)) {
                    throw new CheckInTaskRepeatToEntryException(errorMsg);
                } else {
                    if (!setBefore.contains(numberAndType)) {
// 如果新加的单据之前没添加过，与其他添加的单据进行比较
                        DBRow[] otherEntryTask = findTaskByNumberTypePsCompanyNotDloId(
                            number, number_type, ps_id, company_id, dlo_id);
                        if (otherEntryTask.length > 0) {
                            throw new CheckInTaskRepeatToOthersException(
                                errorMsg);
                        }
                    }
                }
            }
// 处理entry的ctn_number
// CheckInCtnRepeatToOthersException
/*
* for(int i = 0; i < detailsNow.length; i++) { String ctn_number =
* detailsNow[i].getString("ctn_number"); int number_type =
* detailsNow[i].get("number_type", 0); String company_id =
* detailsNow[i].getString("company_id"); String numberAndType =
* ctn_number+" | "+number_type+" | "+company_id; DBRow[]
* otherEntryTask =
* findTaskByNumberTypePsCompanyNotDloId(ctn_number,
* ModuleKey.CHECK_IN_CTN, ps_id, company_id, dlo_id);
* if(otherEntryTask.length > 0) { throw new
* CheckInTaskRepeatToOthersException(numberAndType); } DBRow[]
* otherCtnNumber = findCtnNumberNotDloId(ctn_number, dlo_id,
* ps_id); if(otherCtnNumber.length > 0) { throw new
* CheckInTaskRepeatToOthersException(numberAndType); } }
*/
        } catch (CheckInTaskRepeatToEntryException e) {
            throw new CheckInTaskRepeatToEntryException(e.getMessage());
        } catch (CheckInTaskRepeatToOthersException e) {
            throw new CheckInTaskRepeatToOthersException(e.getMessage());
        }
    }

    /**
     * windowCheckIn修改前占用的门或停车位
     *
     * @param details
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午5:41:21
     */
    public DBRow windowCheckInHasUsedDoorOrSpots(DBRow[] details)
        throws Exception {
        DBRow result = new DBRow();
        List<DBRow> listDoor = new ArrayList<DBRow>();
        List<DBRow> listSpot = new ArrayList<DBRow>();
        for (int i = 0; i < details.length; i++) {
            DBRow row = new DBRow();
            row.add("id", details[i].get("rl_id", 0L));
            row.add("status", details[i].get("occupancy_status", 0));
            if (details[i].get("occupancy_type", 0) == OccupyTypeKey.DOOR) {
                listDoor.add(row);
            } else if (details[i].get("occupancy_type", 0) == OccupyTypeKey.SPOT) {
                listSpot.add(row);
            }
        }
        result.add("doors", listDoor);
        result.add("spots", listSpot);
        return result;
    }

    /**
     * windowCheckIn处理主表
     *
     * @param mRow
     * @param in_seal
     * @param out_seal
     * @param appointmentdate
     * @param adminLoginBean
     * @param priority
     * @param mainId
     * @param createTime
     * @param createManId
     * @param load_count
     * @param rel_type
     * @param ctn_first
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午6:08:05
     */
    public DBRow windowCheckInHandleMainRow(DBRow mRow, String in_seal,
                                            String out_seal, String appointmentdate,
                                            AdminLoginBean adminLoginBean, int priority, long mainId,
                                            String createTime, long createManId, long load_count, int rel_type,
                                            String ctn_first, int waitingOrNot, int isLive) throws Exception {

        DBRow mainRow = mRow;
        mainRow.add("in_seal", in_seal); // 更新封条号
        mainRow.add("out_seal", out_seal); // 更新封条号
        if (!appointmentdate.equals("")) {
            long a = adminLoginBean.getPs_id();
            mainRow.add(
                "appointment_time",
                DateUtil.showUTCTime(appointmentdate,
                    adminLoginBean.getPs_id()));
        } else {
            mainRow.add("appointment_time", null);
        }
// 添加优先级 wfh
        mainRow.add("priority", priority);
        if (mRow.getString("window_check_in_time").equals("")) {
            mainRow.add("window_check_in_time", createTime);
            mainRow.add("window_check_in_operator", createManId);
        }
        mainRow.add("window_check_in_operate_time", createTime);
        mainRow.add("dlo_id", windowCheckInMainId);
        mainRow.add("load_count", load_count); // 数量
        if (rel_type > 0) {
            mainRow.add("rel_type", rel_type);
        }
        if (isLive > 0) {
            mainRow.add("isLive", isLive);
        }
        mainRow.add("status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
        if (mRow.get("tractor_status", 0) != 5) {
            mainRow.add("tractor_status",
                CheckInMainDocumentsStatusTypeKey.UNPROCESS);
        }
        mainRow.add("waiting", waitingOrNot);
// 释放停车位
// 根据主单据查询停车位id
        if (mRow != null) {
            long yc_id = mRow.get("yc_id", 0l);
            if (yc_id != 0) {
                floorCheckInMgrZwb.updateParkingStatus(yc_id, 1);
                mainRow.add("yc_id", null);
            }
        }

// 修改gate货柜号，将第一个有值的container写到主表
        if (!StrUtil.isBlank(ctn_first)) {
            String phone_gate_container_no = StringUtil
                .convertStr2PhotoNumber(ctn_first);
            mainRow.add("gate_container_no", ctn_first);
            mainRow.add("phone_gate_container_no", phone_gate_container_no);
        }
        return mainRow;
    }

    /**
     * 通过entry明细删除明细的warehouse任务
     *
     * @param id
     * @param request
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午6:08:21
     */
    public void delteTaskScheduleByDetailId(long id, HttpServletRequest request)
        throws Exception {
        DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(id,
            ProcessKey.CHECK_IN_WAREHOUSE);
        for (int k = 0; k < ware1.length; k++) {
            long schedule_id = ware1[k].get("schedule_id", 0l);
            scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
        }
        DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(id,
            ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
        for (int k = 0; k < ware2.length; k++) {
            long schedule_id = ware2[k].get("schedule_id", 0l);
            scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
        }
    }

    /**
     * windowCheckIn判断主表的rel_type
     *
     * @param pickup
     * @param delivery
     * @return
     * @author:zyj
     * @date: 2014年11月17日 下午6:08:47
     */
    public int windowCheckInHandleMainRelType(boolean pickup, boolean delivery) {
        int rel_type = 0;
        if (pickup && delivery) {
            rel_type = CheckInMainDocumentsRelTypeKey.BOTH;
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// if(mRow.get("tractor_status",0)!=5 &&
// mRow.get("tractor_status",0)!=10){
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// }
        } else if (pickup) {
            rel_type = CheckInMainDocumentsRelTypeKey.PICK_UP;
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// if(mRow.get("tractor_status",0)!=5 &&
// mRow.get("tractor_status",0)!=10){
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// }
        } else if (delivery) {
            rel_type = CheckInMainDocumentsRelTypeKey.DELIVERY;
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// if(mRow.get("tractor_status",0)!=5 &&
// mRow.get("tractor_status",0)!=10){
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.UNPROCESS);
// }
        }
        return rel_type;
    }

    /**
     * windowCheckIn处理主表 是pickup、container、日志内容、load数量
     *
     * @param bill_type
     * @param number
     * @param occupancy_type
     * @param occupyName
     * @param row
     * @param mRow
     * @param createTime
     * @param adid
     * @param request
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午6:09:20
     */
    public DBRow windowCheckInHandleLoadTypeCountContent(int bill_type,
                                                         String number, int occupancy_type, String occupyName, DBRow row,
                                                         DBRow mRow, String createTime, long adid, HttpServletRequest request)
        throws Exception {
        DBRow resultRow = new DBRow();
        String context = "";
        Boolean pickup = false;
        Boolean delivery = false;
        int load_count = 0;
        String beizhu = row.getString("note");
        OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
        if (bill_type == ModuleKey.CHECK_IN_LOAD) {
/*
* maintype = "Outbound" ; detailType =
* moduleKey.getModuleName(bill_type) ;
*/
            load_count++;
// row.add("load_number",number);
            context += "Pick Up:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
            pickup = true;
// 将load的MasterBolNo和OrderNo写到checkin明细表中
            String loadData = sqlServerMgrZJ.findMasterBolNoOrderNoStrByLoadNo(
                number, adid);// ,request
            row.add("master_order_nos", loadData);
// 更新wms数据
            String shipTime = !""
                .equals(mRow.getString("window_check_in_time")) ? mRow
                .getString("window_check_in_time") : createTime;
// sqlServerMgrZJ.updateLoadMasterOrderShipDateSealVehicleDockCarrier(shipTime,
// out_seal,"", doorRow.getString("doorId"),
// mRow.getString("company_name"),number,adid,request);//更新wms数据
        } else if (bill_type == ModuleKey.CHECK_IN_ORDER) {
// maintype = "Outbound" ;
            load_count++;
            pickup = true;
            context += "Pick Up:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】From "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
        } else if (bill_type == ModuleKey.CHECK_IN_PONO) {
// maintype = "Outbound" ;
            load_count++;
            pickup = true;
            context += "Pick Up:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
        } else if (bill_type == ModuleKey.CHECK_IN_CTN) {
// maintype = "Inbound" ;
            context += "Delivery:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
            delivery = true;
        } else if (bill_type == ModuleKey.CHECK_IN_BOL) {
// maintype = "Inbound" ;
            context += "Delivery:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
            delivery = true;
        } else if (bill_type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
// maintype = "Other" ;
            context += "Delivery:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
            delivery = true;
        } else if (bill_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
// maintype = "Other" ;
            load_count++;
            pickup = true;
            context += "Pick Up:【" + moduleKey.getModuleName(bill_type) + ":"
                + number + "】To "
                + occupyTypeKey.getOccupyTypeKeyName(occupancy_type) + ":【"
                + occupyName + "】Note：" + beizhu;
        }
// else if(bill_type==ModuleKey.CHECK_IN_PICKUP_ORTHERS){
// context+="Delivery:【"+moduleKey.getModuleName(bill_type)+":"+number+"】To DOOR:【"+doorRow.getString("doorId")+"】Note："+beizhu;
// }
        resultRow.add("context", context);
        resultRow.add("pickup", pickup == true ? 1 : 2);
        resultRow.add("delivery", delivery == true ? 1 : 2);
        resultRow.add("load_count", load_count);
        return resultRow;
    }

    /**
     * windowCheckIn将json转成DBRow
     *
     * @param json
     * @param mainId
     * @return
     * @throws JSONException
     * @author:zyj
     * @date: 2014年11月17日 上午11:07:52
     */
    public DBRow windowCheckInSubmitJsonToDBRow(JSONObject json, long mainId)
        throws JSONException {
        if (null != json) {
            DBRow row = new DBRow();

            String number = json.get("number").toString().toUpperCase(); // load
            String doorId = json.get("doorId").toString(); // 门id
            String ids = json.get("tongzhi").toString(); // 要通知的人 名字 字符串
            String detailId = json.get("detailId").toString();
            String zoneId = json.get("zoneId").toString();
            String youjian = json.get("youjian").toString(); // 邮件
            String duanxin = json.get("duanxin").toString(); // 短信
            String yemian = json.get("yemian").toString(); // 页面
            String beizhu = json.get("beizhu").toString(); // 备注
            String number_type = json.get("number_type").toString();
            String customer_id = json.get("customer_id").toString();
            String company_id = json.get("company_id").toString();
            String account_id = json.get("account_id").toString();
            String order_no = json.get("order_no").toString();
            String po_no = json.get("po_no").toString();
            String supplier_id = json.get("supplier_id").toString();
            String staging_area_id = json.get("staging_area_id").toString();
            String freight_term = json.get("freight_term").toString();
            String occupancy_type = json.get("occupancy_type").toString();
            String ctn_number = json.getString("container_no").toUpperCase();

            row.add("dlo_id", mainId);
            row.add("dlo_detail_id", detailId);
            row.add("occupancy_type", occupancy_type);
            row.add("rl_id", doorId);
            row.add("zone_id", zoneId);

            int bill_type = 0;
            if (!"".equals(number_type)) {
                bill_type = Integer.parseInt(number_type);
            }

            row.add("number_type", bill_type);
            row.add("number", number);
            row.add("ctn_number", ctn_number);
            row.add("customer_id", customer_id);
            row.add("company_id", company_id);
            row.add("account_id", account_id);
            row.add("order_no", order_no);
            row.add("po_no", po_no);
            row.add("supplier_id", supplier_id);
            row.add("staging_area_id", staging_area_id);
            row.add("freight_term", freight_term);
            row.add("note", beizhu);

            Boolean yj = true;
            Boolean dx = true;
            Boolean ym = true;
            if (youjian.equals("0")) {
                yj = false;
            }
            if (duanxin.equals("0")) {
                dx = false;
            }
            if (yemian.equals("0")) {
                ym = false;
            }

            row.add("yj", yj == true ? 1 : 2);
            row.add("dx", dx == true ? 1 : 2);
            row.add("ym", ym == true ? 1 : 2);
            row.add("ids", ids);

            return row;
        }
        return null;
    }

    /**
     * 处理门或停车位的状态
     *
     * @param usedDetailDoorOrSpots
     * @param dlo_id
     * @param occupancy_type
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午6:10:35
     */
    public void windowCheckInHandleDoorOrSpotStatus(
        List<DBRow> usedDetailDoorOrSpots, long dlo_id, int occupancy_type,
        long adid) throws Exception {
        String errorDoorOrSpotName = "";
        try {
            DBRow[] entryDetails = floorCheckInMgrZwb
                .findloadingByInfoId(dlo_id);
            DBRow usingDoorOrSpotDetail = windowCheckInHasUsedDoorOrSpots(entryDetails);
            List<DBRow> usingDoorOrSpotList = new ArrayList<DBRow>();
            if (occupancy_type == OccupyTypeKey.DOOR) {
                usingDoorOrSpotList = (ArrayList<DBRow>) usingDoorOrSpotDetail
                    .get("doors", new ArrayList<DBRow>());// 曾经占用的门和单据信息
            } else if (occupancy_type == OccupyTypeKey.SPOT) {
                usingDoorOrSpotList = (ArrayList<DBRow>) usingDoorOrSpotDetail
                    .get("spots", new ArrayList<DBRow>());
                ;// 曾经占用的spot
            }

            DBRow[] usingDetailDoorOrSpots = usingDoorOrSpotList
                .toArray(new DBRow[0]);

            Map<Long, TreeSet<Integer>> usedDoorOrSpots = new HashMap<Long, TreeSet<Integer>>();// 之前占用的门或停车位
            Map<Long, TreeSet<Integer>> usingDoorOrSpots = new HashMap<Long, TreeSet<Integer>>();// 现在占用的门或停车位
// 将之前占用的门按门分组按照
            for (int i = 0; i < usedDetailDoorOrSpots.size(); i++) {
                long door_id = usedDetailDoorOrSpots.get(i).get("id", 0L);
                int door_status = usedDetailDoorOrSpots.get(i).get("status", 0);
                if (usedDoorOrSpots.containsKey(door_id)) {
                    usedDoorOrSpots.get(door_id).add(door_status);
                } else {
                    TreeSet<Integer> statusSet = new TreeSet<Integer>();
                    statusSet.add(door_status);
                    usedDoorOrSpots.put(door_id, statusSet);
                }
            }
// 将现在占用的门按门分组按照
            for (int i = 0; i < usingDetailDoorOrSpots.length; i++) {
                long n_door_id = usingDetailDoorOrSpots[i].get("id", 0L);
                int n_door_status = usingDetailDoorOrSpots[i].get("status", 0);
                if (usingDoorOrSpots.containsKey(n_door_id)) {
                    usingDoorOrSpots.get(n_door_id).add(n_door_status);
                } else {
                    TreeSet<Integer> statusSet = new TreeSet<Integer>();
                    statusSet.add(n_door_status);
                    usingDoorOrSpots.put(n_door_id, statusSet);
                }
            }

            List<Long> delDoorIdOrSpots = new ArrayList<Long>();
            Map<Long, Integer> sameDoorOrSpotIds = new HashMap<Long, Integer>();
            Set<Entry<Long, TreeSet<Integer>>> usedDoorOrSpotSet = usedDoorOrSpots
                .entrySet();
            for (Iterator iterator = usedDoorOrSpotSet.iterator(); iterator
                .hasNext(); ) {
                Entry<Long, TreeSet<Integer>> entry = (Entry<Long, TreeSet<Integer>>) iterator
                    .next();
                long doorId = entry.getKey();
                TreeSet<Integer> statusb = entry.getValue();
                if (usingDoorOrSpots.containsKey(doorId)) {
// 处理现在与之前的状态，以现在门占用状态
                    TreeSet<Integer> statusn = usingDoorOrSpots.get(doorId);
                    if (occupancy_type == OccupyTypeKey.DOOR) {
                        if (statusn.contains(OccupyStatusTypeKey.OCUPIED)) {
                            sameDoorOrSpotIds.put(doorId,
                                OccupyStatusTypeKey.OCUPIED);
                        } else {
                            sameDoorOrSpotIds.put(doorId,
                                OccupyStatusTypeKey.RESERVERED);
                        }
                    } else if (occupancy_type == OccupyTypeKey.SPOT) {
                        sameDoorOrSpotIds.put(doorId,
                            CheckInSpotStatusTypeKey.OCUPIED);
                    }
                }
// 需要删除
                else {
                    delDoorIdOrSpots.add(doorId);
                }
            }

            List<Long> addDoorOrSpotIds = new ArrayList<Long>();
            Set<Entry<Long, TreeSet<Integer>>> usingDoorOrSpotSet = usingDoorOrSpots
                .entrySet();
            for (Iterator iterator = usingDoorOrSpotSet.iterator(); iterator
                .hasNext(); ) {
                Entry<Long, TreeSet<Integer>> entry = (Entry<Long, TreeSet<Integer>>) iterator
                    .next();
                long doorId = entry.getKey();
                if (!sameDoorOrSpotIds.containsKey(doorId)) {
                    addDoorOrSpotIds.add(doorId);
                }
            }
// 添加时候考虑是否被其他entry占用
            for (int i = 0; i < addDoorOrSpotIds.size(); i++) {
                if (occupancy_type == OccupyTypeKey.DOOR) {
                    if (storageDoorMgrZr.checkDoorIsFree(addDoorOrSpotIds
                        .get(i))) {
                        storageDoorMgrZr.reserveredDoor(
                            addDoorOrSpotIds.get(i), ModuleKey.CHECK_IN,
                            dlo_id, adid);
                    } else {
                        DBRow doorRow = storageDoorMgrZr
                            .findDoorDetaiByDoorId(addDoorOrSpotIds.get(i));
                        errorDoorOrSpotName = null != doorRow ? doorRow
                            .getString("doorId") : "";
                        throw new DoorHasUsedException(errorDoorOrSpotName);
                    }
                } else if (occupancy_type == OccupyTypeKey.SPOT) {
                    if (storageYardControlZr
                        .isCanUseSpot(addDoorOrSpotIds.get(i),
                            ModuleKey.CHECK_IN, dlo_id)) {
                        storageYardControlZr.ocupiedSpot(
                            addDoorOrSpotIds.get(i), ModuleKey.CHECK_IN,
                            dlo_id, adid);
                    } else {
                        DBRow spotRow = storageYardControlZr
                            .findSpotDetaiBySpotId(addDoorOrSpotIds.get(i));
                        errorDoorOrSpotName = null != spotRow ? spotRow
                            .getString("yc_no") : "";
                        throw new SpotHasUsedException();
                    }
                }
            }
// 如果是此entry占用，释放
            for (int i = 0; i < delDoorIdOrSpots.size(); i++) {
                if (occupancy_type == OccupyTypeKey.DOOR) {
                    storageDoorMgrZr.freeDoor(delDoorIdOrSpots.get(i), adid,
                        dlo_id, ModuleKey.CHECK_IN);
                } else if (occupancy_type == OccupyTypeKey.SPOT) {
                    storageYardControlZr.freeSpot(delDoorIdOrSpots.get(i),
                        adid, dlo_id, ModuleKey.CHECK_IN);

                }
            }
// 如果一直被此entry占用，更新状态
            Set<Entry<Long, Integer>> sameDoorsSet = sameDoorOrSpotIds
                .entrySet();
            for (Iterator iterator = sameDoorsSet.iterator(); iterator
                .hasNext(); ) {
                Entry<Long, Integer> entry = (Entry<Long, Integer>) iterator
                    .next();
                long doorId = entry.getKey();
                int status = entry.getValue();
                if (occupancy_type == OccupyTypeKey.DOOR) {
                    if (status == OccupyStatusTypeKey.OCUPIED) {
                        storageDoorMgrZr.ocupiedDoor(doorId,
                            ModuleKey.CHECK_IN, dlo_id, adid);
                    } else if (status == OccupyStatusTypeKey.RESERVERED) {
                        storageDoorMgrZr.reserveredDoor(doorId,
                            ModuleKey.CHECK_IN, dlo_id, adid);
                    }
                } else if (occupancy_type == OccupyTypeKey.SPOT) {
                    if (status == CheckInSpotStatusTypeKey.OCUPIED) {
                        storageYardControlZr.ocupiedSpot(doorId,
                            ModuleKey.CHECK_IN, dlo_id, adid);
                    }
                }
            }

        } catch (DoorHasUsedException e) {
            throw new DoorHasUsedException(errorDoorOrSpotName);
        } catch (SpotHasUsedException e) {
            throw new SpotHasUsedException(errorDoorOrSpotName);
        }
    }

    // load生成标签
    public DBRow[] createLoadBiaoQian(long mainId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.createLoadBiaoQian(mainId);
        } catch (Exception e) {
            throw new SystemException(e, "createLoadBiaoQian", log);
        }
    }

    public DBRow[] findBolNoDetailsByEntryId(long EntryId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findBolNoDetailsByEntryId(EntryId);
        } catch (Exception e) {
            throw new SystemException(e, "findBolNoDetailsByEntryId", log);
        }
    }

    public DBRow[] findContainerNoDetailsByEntryId(long EntryId)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb
                .findContainerNoDetailsByEntryId(EntryId);
        } catch (Exception e) {
            throw new SystemException(e, "findContainerNoDetailsByEntryId", log);
        }
    }

    public DBRow[] findBolNoContainerNoNotDetailsByEntryId(long EntryId)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb
                .findBolNoContainerNoNotDetailsByEntryId(EntryId);
        } catch (Exception e) {
            throw new SystemException(e, "findContainerNoDetailsByEntryId", log);
        }
    }

    public void windowAddLog(String type, String number, int number_type,
                             long main_id, HttpServletRequest request, int occupyType,
                             String occupyName, String ids, String in_seal, String out_seal)
        throws Exception {
        try {
// 创建时间
            Date date = (Date) Calendar.getInstance().getTime();
            SimpleDateFormat dateformat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
            String createTime = dateformat.format(date);
// 获得创建人
            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long createManId = adminLoginBean.getAdid();
            String data = "";
            OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
            if (!occupyName.equals("")) {
                data += occupyTypeKey.getOccupyTypeKeyName(occupyType) + ":"
                    + occupyName + ",";
            }
            if (!ids.equals("")) {
                String operator = "";
                String[] str = ids.split(",");
                for (int i = 0; i < str.length; i++) {
                    DBRow row = this.floorWaybillMgrZwb.getPeople(Long
                        .parseLong(str[i]));
                    String name = row.get("employe_name", "");
                    if (i == str.length - 1) {
                        operator += name;
                    } else {
                        operator += name + "、";
                    }
                }
                data += "Operator:" + operator + ",";
            }

            data += "Delivery Seal:" + in_seal + ",";

            data += "PickUp Seal:" + out_seal + ",";
            if (!data.equals("")) {
                data = data.substring(0, data.length() - 1);
            }

            if (type.equals("add")) {
                this.addCheckInLog(createManId,
                    "Create " + moduleKey.getModuleName(number_type) + "【"
                        + number + "】", main_id,
                    CheckInLogTypeKey.WINDOW, createTime, data);
            } else if (type.equals("update")) {
                this.addCheckInLog(createManId,
                    "Update " + moduleKey.getModuleName(number_type) + "【"
                        + number + "】", main_id,
                    CheckInLogTypeKey.WINDOW, createTime, data);
            } else if (type.equals("delete")) {
                this.addCheckInLog(createManId,
                    "Delete " + moduleKey.getModuleName(number_type) + "【"
                        + number + "】", main_id,
                    CheckInLogTypeKey.WINDOW, createTime, data);
            } else if (type.equals("release_main")) {
                String content = "release "
                    + occupyTypeKey.getOccupyTypeKeyName(occupyType) + ":"
                    + occupyName;
                this.addCheckInLog(createManId, content, main_id,
                    CheckInLogTypeKey.WINDOW, createTime, content);
            }

        } catch (Exception e) {
            throw new SystemException(e, "windowAddLog", log);
        }
    }

    // 跟新子单据
    public long updateDetailByIsExist(String detailId, long doorId,
                                      long occupancy_type, long occupancy_status, String zoneId,
                                      long number_type, String number, long adid,
                                      HttpServletRequest request) throws Exception {
        try {
            String loadData = "";
            DBRow dlrow = new DBRow();
            if (number_type == ModuleKey.CHECK_IN_LOAD) {
// 将load的MasterBolNo和OrderNo写到checkin明细表中
                loadData = sqlServerMgrZJ.findMasterBolNoOrderNoStrByLoadNo(
                    number, adid);// request
            } else {
                dlrow.add("load_number", null);
            }
            dlrow.add("rl_id", doorId);
            dlrow.add("number_type", number_type);
            dlrow.add("number", number);
            dlrow.add("master_order_nos", loadData);
            dlrow.add("occupancy_type", occupancy_type);
            dlrow.add("occupancy_status", occupancy_status);
            return this.floorCheckInMgrZwb.updateDetailByIsExist(
                Long.parseLong(detailId), dlrow);
        } catch (Exception e) {
            throw new SystemException(e, "updateDetailByIsExist", log);
        }
    }

    /**
     * 查询通知warehouse
     */
    public DBRow[] findNotices(long id, long type) throws Exception {
        try {
            DBRow[] rows = null;
/**
 * if(type.equals("ctn")){ //查询ctn 通知
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_CTN,-1,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("load")){
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_LOAD,-1,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("bol")){
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_BOL, -1,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("other")){
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_ORTHERS,-1,ProcessKey.CHECK_IN_WAREHOUSE); }
 */
            rows = this.floorCheckInMgrZwb.findAllNoticeDedetail(id, type, -1,
                ProcessKey.CHECK_IN_WAREHOUSE);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "findNotice", log);
        }
    }

    /**
     * 查询通知
     */
    public DBRow[] findWindowNotices(long id, long window, long warehouse)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.findAllWindowNoticeDedetail(id,
                window, warehouse);
        } catch (Exception e) {
            throw new SystemException(e, "findNotice", log);
        }
    }

    // 查询所有通知
    public DBRow[] findAllNotices(long id, long type) throws Exception {
        try {
            DBRow[] rows = null;
/**
 * if(type.equals("ctn")){ //查询ctn 通知
 * //rows=scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(id,
 * ModuleKey.CHECK_IN_CTN, ProcessKey.CHECK_IN);
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_CTN,
 * ProcessKey.CHECK_IN_WINDOW,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("load")){ //查询load通知
 * //rows=scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(id,
 * ModuleKey.CHECK_IN_LOAD, ProcessKey.CHECK_IN);
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_LOAD,
 * ProcessKey.CHECK_IN_WINDOW,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("bol")){
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_BOL,
 * ProcessKey.CHECK_IN_WINDOW,ProcessKey.CHECK_IN_WAREHOUSE); }else
 * if(type.equals("other")){
 * rows=this.floorCheckInMgrZwb.findAllNoticeDedetail(id,
 * ModuleKey.CHECK_IN_PICKUP_ORTHERS,
 * ProcessKey.CHECK_IN_WINDOW,ProcessKey.CHECK_IN_WAREHOUSE); }
 */
            rows = this.floorCheckInMgrZwb.findAllNoticeDedetail(id, type,
                ProcessKey.CHECK_IN_WINDOW, ProcessKey.CHECK_IN_WAREHOUSE);

            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "findNotice", log);
        }
    }

    // 删除window 子单据
    public long detCheckinWindow(HttpServletRequest request, long detail_id)
        throws Exception {
        try {

            this.detOccupancyDetails(detail_id); // 删除子单据

// 删除 该单据的window任务
            DBRow[] shRow = this.floorCheckInMgrZwb.selectSchedule(detail_id,
                ProcessKey.CHECK_IN_WINDOW);
            if (shRow.length > 0 && shRow != null) {
                for (int i = 0; i < shRow.length; i++) {
                    long schedule_id = shRow[i].get("schedule_id", 0l);
                    this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id,
                        request);
                }
            }
// 删除warehouse任务
            DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(detail_id,
                ProcessKey.CHECK_IN_WAREHOUSE);
            for (int i = 0; i < ware1.length; i++) {
                long schedule_id = ware1[i].get("schedule_id", 0l);
                this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id,
                    request);
            }
            DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(detail_id,
                ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
            for (int i = 0; i < ware2.length; i++) {
                long schedule_id = ware2[i].get("schedule_id", 0l);
                this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id,
                    request);
            }
            return 01;
        } catch (Exception e) {
            throw new SystemException(e, "detCheckinWindow", log);
        }
    }

    // android用 查询通知 反填
    public DBRow[] androidfindNotice(long id, String type) throws Exception {
        try {
            DBRow[] rows = null;
            if (type.equals("ctn")) {
// 查询ctn 通知
// rows=scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(id,
// ModuleKey.CHECK_IN_CTN, ProcessKey.CHECK_IN);
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_CTN, ProcessKey.CHECK_IN);
            } else if (type.equals("load")) {
// 查询load通知
// rows=scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(id,
// ModuleKey.CHECK_IN_LOAD, ProcessKey.CHECK_IN);
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_LOAD, ProcessKey.CHECK_IN);
            } else if (type.equals("bol")) {
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_BOL, ProcessKey.CHECK_IN);
            } else if (type.equals("other")) {
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_ORTHERS, ProcessKey.CHECK_IN);
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "findNotice", log);
        }
    }

    // 根据mainId 查询该单据下所有load
    public DBRow[] findAllLoadByMainId(long mainId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findAllLoadByMainId(mainId);
        } catch (Exception e) {
            throw new SystemException(e, "findAllLoadByMainId", log);
        }
    }

    // TODO 根据entryId 查询ctnr 所占用的资源
    public DBRow[] findCTNRResourceByEntryId(long entry_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findCTNRResourceByEntryId(entry_id);
        } catch (Exception e) {
            throw new SystemException(e, "findAllLoadByMainId", log);
        }
    }

    // 根据主单据和门id查询 子单据号
    public DBRow[] findOrderByEntryIdAndDoorId(long entry_id, long door_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.findOrderByEntryIdAndDoorId(
                entry_id, door_id);
        } catch (Exception e) {
            throw new SystemException(e, "findOrderByEntryIdAndDoorId", log);
        }
    }

    // 根据主单据号 查询 子单据 被占用门
    public DBRow[] findUseDoorByMainId(long maind_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findUseDoorByMainId(maind_id);
        } catch (Exception e) {
            throw new SystemException(e, "findUseDoorByMainId", log);
        }
    }

    // window 签 查询门 不要load
    public DBRow[] findDoorNoRepeatNotPickUp(long mainId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findDoorNoRepeatNotPickUp(mainId);
        } catch (Exception e) {
            throw new SystemException(e, "findDoorNoRepeatNotPickUp", log);
        }
    }

    // 根据mainId 查询该单据下 去重门
    public DBRow[] findDoorNoRepeat(long mainId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findDoorNoRepeat(mainId);
        } catch (Exception e) {
            throw new SystemException(e, "findDoorNoRepeat", log);
        }
    }

    // 查询子单据是否存在
    public DBRow[] findBillIsExist(String order, String type, long ps_id,
                                   String company_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findBillIsExist(order, type, ps_id,
                company_id);
        } catch (Exception e) {
            throw new SystemException(e, "findBillIsExist", log);
        }
    }

    private DBRow adrdReleaseDoorsResult;

    // 跟新子单据状态
    public DBRow androidUpdateDetailsNumberStatus(final DBRow row,
                                                  final AdminLoginBean adminLoginBean) throws Exception {
        try {
            adrdReleaseDoorsResult = null;
            sqlServerTransactionTemplate
                .execute(new TransactionCallbackWithoutResult() {

                    @Override
                    protected void doInTransactionWithoutResult(
                        TransactionStatus txStat) {
                        try {
                            int main_id = row.get("main_id", 0);
                            String number = row.getString("order_id");
                            int door_id = row.get("door_id", 0);
                            int type = row.get("type", 0);
                            String status = row.getString("status"); // StringUtil.getString(request,"");
                            int occupancy_status = row
                                .get("door_status", 0);// StringUtil.getInt(request,"door_status");
                            int is_leave = row.get("is_leave", 0); // StringUtil.getInt(request,"is_leave");
                            long last_flag = row.get("last_flag", 0);
                            DBRow mainRow = new DBRow();
                            DBRow statusRow = floorCheckInMgrZwb
                                .findGateCheckInById(main_id);
                            if (last_flag == 1) {
                                mainRow.add(
                                    "status",
                                    CheckInMainDocumentsStatusTypeKey.UNPROCESS);
                                if (statusRow.get("tractor_status", 0l) != 5) {
                                    mainRow.add(
                                        "tractor_status",
                                        CheckInMainDocumentsStatusTypeKey.UNPROCESS);
                                }
                                mainRow.add("rel_type", 2);
                            }// 待修改
// int flag=0;
                            int num = 0;
                            if (status.equals("close")) {
                                num = 3;
                            } else if (status.equals("exception")) {
                                num = 4;
                            } else {
                                num = 5;
                            }
// if(type.equals("load")){
// flag=1;
// //mainRow.add("status",
// CheckInMainDocumentsStatusTypeKey.PICK_UP_LEAVING);
// }
// if(type.equals("ctn")){
// flag=2;
// //mainRow.add("status",
// CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING);
// }
// if(type.equals("bol")){
// flag=3;
// //mainRow.add("status",
// CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING);
// }
// if(type.equals("other")){
// flag=4;
// }

                            adrdReleaseDoorsResult = floorCheckInMgrZwb
                                .modOccupancyDetailsNumberStatus(
                                    main_id, number, 0, num,
                                    occupancy_status, door_id,
                                    adminLoginBean.getPs_id(), "");

// 更新主单据状态

// 根据主单据号查询 主单据状态

                            if (is_leave == 1) {
// if(statusRow.get("status",
// 0l)==CheckInMainDocumentsStatusTypeKey.UNPROCESS
// || statusRow.get("status",
// 0l)==CheckInMainDocumentsStatusTypeKey.PROCESSING
// ){
                                mainRow.add(
                                    "status",
                                    CheckInMainDocumentsStatusTypeKey.INYARD);
                                if (statusRow.get("tractor_status", 0l) != 5) {
                                    mainRow.add(
                                        "tractor_status",
                                        CheckInMainDocumentsStatusTypeKey.INYARD);
                                }
// }else{
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.INYARD);
// if(statusRow.get("tractor_status", 0l)!=5
// && statusRow.get("tractor_status",
// 0l)!=10){
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.DELIVERY_INYARD);
// }
// }
                            } else if (is_leave == 2) {
// if(statusRow.get("status",
// 0l)==CheckInMainDocumentsStatusTypeKey.PICK_UP_UNPROCESS
// || statusRow.get("status",
// 0l)==CheckInMainDocumentsStatusTypeKey.PICK_UP_PROCESSING
// ){
                                mainRow.add(
                                    "status",
                                    CheckInMainDocumentsStatusTypeKey.LEAVING);
                                if (statusRow.get("tractor_status", 0l) != 5) {
                                    mainRow.add(
                                        "tractor_status",
                                        CheckInMainDocumentsStatusTypeKey.LEAVING);
                                }
// }else{
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING);
// if(statusRow.get("tractor_status", 0l)!=5
// && statusRow.get("tractor_status",
// 0l)!=10){
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING);
// }
// }
                            }

                            floorCheckInMgrZwb.updateYcIdByDloId(main_id,
                                mainRow);
// 处理文件
                            String filePath = row.getString("filePath");
/*
* if(!StringUtil.isBlank(filePath)){ String
* fileDescBasePath = Environment.getHome() +
* "upload/check_in/" ; File dir = new
* File(filePath); if(dir.isDirectory()){ File[]
* files = dir.listFiles(); for(File file :
* files){ //String fileName =
* StringUtil.getFileName(file.getName());
* addFile
* ("dock_close_"+main_id+"_"+file.getName(),
* main_id, FileWithTypeKey.OCCUPANCY_MAIN, 0 ,
* adminLoginBean.getAdid(), DateUtil.NowStr());
* //FileUtil.copyProductFile(file,
* fileDescBasePath);
* FileUtil.checkInAndroidCopyFile(file,
* "dock_close_"+main_id+"_", fileDescBasePath);
* } } }
*/
                            androidSaveMoveFile(filePath, main_id,
                                "dock_close_" + main_id + "_",
                                row.getString("session_id"));

                            DBRow storage = floorCheckInMgrZwb
                                .findDoorById(door_id);
                            String door_name = storage.getString("doorId");

// 获得当前登录人
                            String createTime = DateUtil.NowStr();

                            String data = "";
                            String note = "";
                            if (occupancy_status > 0) {
                                note = "Released Door";
                                if (!door_name.equals("")) {
                                    data += "DOOR: " + door_name + ",";
                                }
// if(!number.equals("")) {
// if(flag == 1) {
// data += "loadNo: " + number + ",";
// } else {
// data += "CTNR/BOL: " + number + ",";
// }
// }
                                if (!data.equals("")) {
                                    data = data.substring(0,
                                        data.length() - 1);
                                }
                                addCheckInLog(
                                    adminLoginBean.getAdid(),
                                    note,
                                    main_id,
                                    CheckInLogTypeKey.ANDROID_DOCK_CLOSE,
                                    createTime, data);
                            } else {

// if(!door_name.equals("")) {
// data += "DOOR: " + door_name + ",";
// }
                                if (!number.equals("")) {

                                    note = "Closed";
                                    data += moduleKey.getModuleName(type)
                                        + ":" + number + ",";

                                }
                                if (!data.equals("")) {
                                    data = data.substring(0,
                                        data.length() - 1);
                                }
                                addCheckInLog(
                                    adminLoginBean.getAdid(),
                                    note,
                                    main_id,
                                    CheckInLogTypeKey.ANDROID_DOCK_CLOSE,
                                    createTime, data);
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            return adrdReleaseDoorsResult;
        } catch (Exception e) {
            throw new SystemException(e, "androidUpdateDetailsNumberStatus",
                log);
        }
    }

    /**
     * 查询loading表
     */
    @Override
    public DBRow[] getLoading(String area_name, String title_name, long type,
                              String search_number, long mainId, int mark, long ps_id)
        throws Exception {
        try {
            DBRow[] row = null;
            if (!area_name.equals("") || !title_name.equals("")) {
                row = floorCheckInMgrZwb.getLoading(area_name, title_name,
                    type, mainId, mark, ps_id);
            }
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "getLoading", log);
        }
    }

    @Override
    public DBRow getDoorByStagingOrTitle(long type, String search_number,
                                         long mainId, long ps_id, long adid, HttpServletRequest request)
        throws Exception {
        try {

            DBRow result = null;
            DBRow pickUp = null;
            DBRow[] local = this.floorCheckInMgrZwb.storageSearchLocal(ps_id);
            DBRow[] wms = this.floorCheckInMgrZwb.storageSearchWms(ps_id);
            DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
            int[] status = null;
            String[] company_id = null;
// int mark = 0;
            if (companyId.length > 0) {
                company_id = new String[companyId.length];
                for (int i = 0; i < companyId.length; i++) {
                    company_id[i] = companyId[i].get("company_id", "");
                }
            }

            if (type == GateCheckLoadingTypeKey.LOAD) {
                if (local.length > 0
                    && local[0].get("search_priority", 0l) == 1) {
// DBRow[] localResult = null;
                    status = new int[]{ContainerImportStatusKey.RECEIVED};

                    result = this.floorCheckInMgrZwb.findContainerInfoByNo("",
                        search_number, status);
                    if (result != null) {
                        result.add("accountid", result.get("account_id", ""));
                        result.add("order_type", ModuleKey.CHECK_IN_LOAD);
                        result.add("system_type", OrderSystemTypeKey.SYNC);
                    }
                    if (wms.length > 0
                        && wms[0].get("search_priority", 0l) == 2) {
                        if (result == null && companyId.length > 0) {
                            pickUp = sqlServerMgrZJ
                                .checkOrderTypeAndChangeOrderTypeByNo(
                                    search_number, company_id);
// WmsOrderStatusKey.PICKED.equals(orderInfos[i].getString("Status")
                            if (null != pickUp) {
                                DBRow[] orderLoads = (DBRow[]) pickUp.get(
                                    "infos", new DBRow[0]);

                                if (orderLoads.length == 1) {
                                    result = orderLoads[0];
                                    result.add("system_type",
                                        OrderSystemTypeKey.WMS);
                                }
                                if (orderLoads.length > 1) {
                                    result = new DBRow();
                                    result.add("pickup", pickUp);
                                    result.add("num", -1);
                                    result.add("system_type",
                                        OrderSystemTypeKey.WMS);
                                }

                            }
// if(result==null){
// mark=1;
// }

                        }
                    }

                } else if (wms.length > 0
                    && wms[0].get("search_priority", 0l) == 1) {
                    pickUp = sqlServerMgrZJ
                        .checkOrderTypeAndChangeOrderTypeByNo(
                            search_number, company_id);
                    if (null != pickUp) {
                        DBRow[] orderLoads = (DBRow[]) pickUp.get("infos",
                            new DBRow[0]);

                        if (orderLoads.length == 1) {
                            result = orderLoads[0];
                            result.add("system_type", OrderSystemTypeKey.WMS);
                        }
                        if (orderLoads.length > 1) {
                            result = new DBRow();
                            result.add("pickup", pickUp);
                            result.add("num", -1);
                            result.add("system_type", OrderSystemTypeKey.WMS);
                        }

                    }
// if(pickUp!=null && pickUp.length==1){
// result=pickUp;
// }
// if(result==null){
// mark=1;
// }

                }

            } else {
                if (local.length > 0
                    && local[0].get("search_priority", 0l) == 1) {
// DBRow[] localResult = null;
                    status = new int[]{ContainerImportStatusKey.IMPORTED};
                    result = this.findReceiptByContainerBolNoOrLoad(
                        search_number, status);
// TODO container_no
                    if (result != null) {
// result=localResult[0];
                        result.add("accountid", result.get("account_id", ""));
                        result.add("order_type", ModuleKey.CHECK_IN_CTN);
                        result.add("system_type", OrderSystemTypeKey.SYNC);
                    }
                    if (wms.length > 0
                        && wms[0].get("search_priority", 0l) == 2) {
                        if (result == null && companyId.length > 0) {
                            DBRow delivery = sqlServerMgrZJ
                                .findReceiptByContainerBolNo(search_number,
                                    adid, company_id);
                            if (null != delivery) {
                                DBRow[] deliverys = (DBRow[]) delivery.get(
                                    "infos", new DBRow[0]);
                                if (deliverys.length == 1) {
                                    result = deliverys[0];
                                    result.add("system_type",
                                        OrderSystemTypeKey.WMS);
                                }
                                if (deliverys.length > 1) {
                                    result = new DBRow();
                                    result.add("num", -1);
                                    result.add("pickup", delivery);
                                    result.add("system_type",
                                        OrderSystemTypeKey.WMS);
                                }
                            }
// if(result==null){
// mark=1;
// }
                        }
                    }
                } else if (wms.length > 0
                    && wms[0].get("search_priority", 0l) == 1) {
                    DBRow delivery = sqlServerMgrZJ
                        .findReceiptByContainerBolNo(search_number, adid,
                            company_id);
                    if (null != delivery) {
                        DBRow[] deliverys = (DBRow[]) delivery.get("infos",
                            new DBRow[0]);
                        if (deliverys.length == 1) {
                            result = deliverys[0];
                            result.add("system_type", OrderSystemTypeKey.WMS);
                        }
                        if (deliverys.length > 1) {
                            result = new DBRow();
                            result.add("num", -1);
                            result.add("pickup", delivery);
                            result.add("system_type", OrderSystemTypeKey.WMS);
                        }
                    }
// if(result==null){
// mark=1;
// }
                }
            }

            if (result != null) {
                if (result.get("system_type", 0) == OrderSystemTypeKey.WMS) {
                    if (result.get("num", 0l) != -1) {
                        String area_name = result.get("StagingAreaID", "");
                        String title_name = result.get("SupplierID", "");
                        DBRow door = autoAssignDoor(area_name, title_name,
                            type, search_number, mainId, ps_id);
                        if (door != null) {
                            result.add("door_id",
                                String.valueOf(door.get("door_id", 0l)));
                            result.add("door_name", door.get("door_name", ""));
                            result.add("area_id",
                                String.valueOf(door.get("area_id", 0l)));
                            result.add("area_name", door.get("area_name", ""));
                        } else {
                            if (type != GateCheckLoadingTypeKey.LOAD) {
                                DBRow titleRow = this.floorCheckInMgrZwb
                                    .selectTitleIdByTitleName(title_name);
/**
 * if(titleRow!=null){ DBRow
 * nearestDoor=this.floorCheckInMgrZwb
 * .selectNearestDoor(ps_id,
 * titleRow.get("title_id", 0l));
 *
 * if(!nearestDoor.get("door", "").equals("0")){
 * String[] doorStr = nearestDoor.get("door",
 * "").split(",");
 * result.add("door_id",doorStr[0]);
 * result.add("door_name",doorStr[1]);
 * result.add("area_id",doorStr[2]);
 * result.add("area_name",doorStr[3]); } }
 */

                            }
                        }
                        result.add("num", 1);
                    }

                } else {
                    if (type == GateCheckLoadingTypeKey.LOAD) {
                        result.add("appointmentdate",
                            result.get("ship_out_date", ""));
                    }
                    result.add("customerid", result.get("customer_id", ""));
                    result.add("num", 1);
                }

            }

            if (result == null) {
                result = new DBRow();
                result.add("num", 0);
                result.add("system_type", OrderSystemTypeKey.SYNC);

            }

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "getLoading", log);
        }
    }

    public DBRow changeEntryDetailsToWmsFormat(DBRow detail) throws Exception {
        try {
            DBRow result = new DBRow();
            DBRow main = floorCheckInMgrZwb.getEntryIdById(detail.get("dlo_id",
                0L));
            String doorOrSpot = "";
            if (detail.get("occupancy_type", 0) == OccupyTypeKey.DOOR) {
                DBRow door = storageDoorMgrZr.findDoorByDoorIdAssociation(
                    detail.get("rl_id", 0L), ModuleKey.CHECK_IN,
                    detail.get("dlo_id", 0L));
                doorOrSpot = null != door ? door.getString("doorId") : "";
            } else if (detail.get("occupancy_type", 0) == OccupyTypeKey.SPOT) {
                DBRow spot = storageYardControlZr.findSpotsBySpotIdAssociateId(
                    detail.get("rl_id", 0L), ModuleKey.CHECK_IN,
                    detail.get("dlo_id", 0L));
                doorOrSpot = null != spot ? spot.getString("yc_no") : "";
            }
            DBRow areaRow = floorCheckInMgrZwb
                .findStorageLocationAreaByAreaId(detail.get("zone_id", 0L));
            result.add("door_id", detail.get("rl_id", 0L));
            result.add("door_name", doorOrSpot);
            result.add("area_id", detail.get("zone_id", 0L));
            result.add("stagingareaid", detail.getString("staging_area_id"));
            result.add("area_name",
                null != areaRow ? areaRow.getString("area_name") : "");
            result.add("companyid", detail.getString("company_id"));
            result.add("customerid", detail.getString("customer_id"));
            result.add("accountid", detail.getString("account_id"));
            result.add("supplierid", detail.getString("supplier_id"));
            result.add("order_type", detail.get("number_type", 0));
            result.add("freightterm", detail.getString("freight_term"));
            result.add("appointmentdate", main.getString("appointment_time"));
            result.add("number_note", detail.getString("note"));
            result.add("occupancy_type", detail.get("occupancy_type", 0));
            result.add("container_no", detail.getString("ctn_number"));
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "changeEntryDetailsToWmsFormat", log);
        }
    }

    public DBRow findReceiptByContainerBolNoOrLoad(String search_number,
                                                   int[] status) throws Exception {
        try {

            DBRow result = this.floorCheckInMgrZwb.findContainerInfoByNo(
                search_number, "", status);
            if (result == null) {
                result = this.floorCheckInMgrZwb.findContainerInfoByNo("",
                    search_number, status);
            }

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "findReceiptByContainerBolNoOrLoad",
                log);
        }
    }

    public DBRow[] getNearestDocks(long ps_id, long id) throws Exception {
        DBRow dock = floorCheckInMgrZwb.queryDocksById(ps_id, id);
        double x1 = 0;
        double y1 = 0;
        if (!dock.isEmpty()) {
            x1 = Double.parseDouble(dock.getString("x"));
            y1 = Double.parseDouble(dock.getString("y"));
        }
        DBRow[] rows = floorCheckInMgrZwb.queryEmptyDocks(ps_id);// 获取所有未被占用的门
        double min_one = 100000, min_two = 100000;
        DBRow rt1 = null;
        DBRow rt2 = null;
        if (rows.length > 0) {
            for (int i = 0; i < rows.length; i++) {
                DBRow r = rows[i];
                long obj_id = r.get("sd_id", 0l);
                if (dock.get("sd_id", 0l) != obj_id) {
                    double x2 = Double.parseDouble(r.getString("x"));
                    double y2 = Double.parseDouble(r.getString("y"));
                    double len = Math.sqrt(Math.pow(x1 - x2, 2)
                        + Math.pow(y1 - y2, 2));
                    if (len < min_one) {
                        min_two = min_one;
                        min_one = len;
                        rt2 = rt1;
                        rt1 = r;
                    } else if (len < min_two) {
                        min_two = len;
                        rt2 = r;
                    }
                }

            }

        }
        return new DBRow[]{rt1, rt2};
    }

    @Override
// 自动指定门
    public DBRow autoAssignDoor(String area_name, String title_name, long type,
                                String search_number, long mainId, long ps_id) throws Exception {
        try {
            DBRow[] row = null;
            DBRow result = null;
            row = this.getLoading(area_name, title_name, type, search_number,
                mainId, 1, ps_id);
            if (row != null && row.length > 0) {
                if (type == GateCheckLoadingTypeKey.LOAD) {
                    long door_id = row[0].get("sd_id", 0l);
                    DBRow[] doors = this.floorCheckInMgrZwb.doorIsAvalible(
                        door_id, mainId);
                    if (doors.length == 0) {// 不可用
// doors =
// this.floorSpaceResourcesRelationMgr.getCanUseDoorByDoorName(mainId,
// ps_id, 0,
// String.valueOf(Long.parseLong(door_name)+1));
// if(doors.length==0){
// doors =
// this.floorSpaceResourcesRelationMgr.getCanUseDoorByDoorName(mainId,
// ps_id, 0,
// String.valueOf(Long.parseLong(door_name)-1));
// }
                        DBRow[] neareastDoor = this.getNearestDocks(ps_id,
                            door_id);
                        if (neareastDoor.length > 0) {
                            doors = this.floorCheckInMgrZwb.doorIsAvalible(
                                neareastDoor[0].get("sd_id", 0l), mainId);
                            if (doors.length == 0) {
                                doors = this.floorCheckInMgrZwb.doorIsAvalible(
                                    neareastDoor[1].get("sd_id", 0l),
                                    mainId);
                            }
                        }
                    }
                    if (doors.length > 0) {
                        result = new DBRow();
                        result.add("door_id",
                            String.valueOf(doors[0].get("sd_id", 0l)));
                        result.add("door_name", doors[0].get("doorId", ""));
                        result.add("area_id",
                            String.valueOf(doors[0].get("area_id", 0l)));
                        result.add("area_name", doors[0].get("area_name", ""));
                    }

                } else {
                    result = new DBRow();
                    result.add("door_id",
                        String.valueOf(row[0].get("sd_id", 0l)));
                    result.add("door_name", row[0].get("doorId", ""));
                    result.add("area_id",
                        String.valueOf(row[0].get("area_id", 0l)));
                    result.add("area_name", row[0].get("area_name", ""));
                }

            } else {
                if (type == GateCheckLoadingTypeKey.LOAD) {
                    row = this.getLoading(area_name, title_name, type,
                        search_number, mainId, 2, ps_id);
                    if (row != null && row.length > 0) {
                        result = new DBRow();
                        result.add("door_id",
                            String.valueOf(row[0].get("sd_id", 0l)));
                        result.add("door_name", row[0].get("doorId", ""));
                        result.add("area_id",
                            String.valueOf(row[0].get("area_id", 0l)));
                        result.add("area_name", row[0].get("area_name", ""));
                    } else {
                        row = this.getLoading(area_name, title_name, type,
                            search_number, mainId, 3, ps_id);
                        if (row != null && row.length > 0) {
                            result = new DBRow();
                            result.add("door_id",
                                String.valueOf(row[0].get("sd_id", 0l)));
                            result.add("door_name", row[0].get("doorId", ""));
                            result.add("area_id",
                                String.valueOf(row[0].get("area_id", 0l)));
                            result.add("area_name", row[0].get("area_name", ""));
                        }
                    }
                } else {
                    row = this.getLoading(area_name, title_name, type,
                        search_number, mainId, 2, ps_id);
                    if (row != null && row.length > 0) {
                        result = new DBRow();
                        result.add("door_id",
                            String.valueOf(row[0].get("sd_id", 0l)));
                        result.add("door_name", row[0].get("doorId", ""));
                        result.add("area_id",
                            String.valueOf(row[0].get("area_id", 0l)));
                        result.add("area_name", row[0].get("area_name", ""));
                    }
                }

            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "autoAssignDoor", log);
        }
    }

    @Override
    public DBRow[] findZoneByTitleName(String title_name, long ps_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.findZoneByTitleName(title_name,
                ps_id);
        } catch (Exception e) {
            throw new SystemException(e, "findZoneByTitleName", log);
        }
    }

    // android 验证用
    @Override
    public DBRow androidGetLoading(DBRow data, HttpServletRequest request)
        throws Exception {
        try {
            int number_type = data.get("number_type", 01);
            String search_number = data.get("search_number", "");
            long ps_id = data.get("ps_id", 01);
            DBRow result = this.getDoorByStagingOrTitle(number_type,
                search_number, 0, ps_id, 0L, request);

            result.add("time", result.get("AppointmentDate", "") + "");
            result.remove("AppointmentDate");

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "getLoading", log);
        }
    }

    private DBRow[] androidShuttleCtnr(DBRow[] equipments)
        throws NoRecordsException, EquipmentHadOutException {
        List<DBRow> equipmentArrays = new ArrayList<DBRow>();
        if (equipments != null && equipments.length > 0) {
            for (DBRow row : equipments) {
                if (row.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.LEFT) {
                    continue;
                } else {
                    equipmentArrays.add(row);
                }
            }
        } else {
            throw new NoRecordsException();
        }
        if (equipmentArrays.size() < 1) {
            throw new EquipmentHadOutException();
        }
        return equipmentArrays.toArray(new DBRow[0]);
    }

    /**
     * 只是检查这个Entry是否存在的，然后让他自己选择是移动门，还是停车位
     */
    @Override
    public DBRow[] androidGetDoorOrlocationByMainId(long info_id, long ps_id,
                                                    AdminLoginBean adminLoginBean, String ctnr)
        throws CheckInEntryIsLeftException, EquipmentHadOutException,
        Exception {
        try {
            DBRow[] resultRow = null;
            DBRow mainRow = null;
            if (!StringUtil.isNull(ctnr)) {
                resultRow = this.floorCheckInMgrZwb.getEquipmentBy(ctnr, ps_id);
                resultRow = androidShuttleCtnr(resultRow);
            } else {
// 校验是否有本仓库的操作权限
                mainRow = this.floorCheckInMgrZwb.findMainById(info_id);
                CheckInEntryPermissionUtil.checkEntry(mainRow, adminLoginBean); // 验证是否有权限
                CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                    .getBeanFromContainer("proxyCheckInMgrZr");

                checkInMgrZr.checkEntryIsLeft(info_id);
// 若此entry下车尾有未离开的设备，直接返回车尾未完成设备
                resultRow = floorCheckInMgrZwb.getEntryNotLeftTrailer(info_id);
                if (resultRow == null || resultRow.length < 1) {
                    resultRow = floorCheckInMgrZwb
                        .getEntryNotLeftTractor(info_id);
                }

            }
            return resultRow;
        } catch (EquipmentHadOutException e) {
            throw e;
        } catch (CheckInEntryIsLeftException e) {
            throw e;
        } catch (CheckInNotFoundException e) {
            throw e;
        } catch (NoPermiessionEntryIdException e) {
            throw e;
        } catch (NoRecordsException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "androidGetDoorOrlocationByMainId",
                log);
        }

    }

    public DBRow[] fixAndroidGateCheckOutOccupiedAndRDoorInfo(DBRow[] doors) {

        if (doors != null && doors.length > 0) {
            DBRow[] returRows = new DBRow[doors.length];
            for (int index = 0, count = doors.length; index < count; index++) {
                DBRow tempDoor = new DBRow();
                DBRow door = doors[index];
                tempDoor.add("door_id", door.get("sd_id", 0l));
                tempDoor.add("door_name", door.getString("doorId"));
                returRows[index] = tempDoor;
            }
            return returRows;
        }
        return null;
    }

    public DBRow[] fixAndroidGateCheckOutAccupiedAndSpotInfo(DBRow[] spots) {

        if (spots != null && spots.length > 0) {
            DBRow[] returRows = new DBRow[spots.length];
            for (int index = 0, count = spots.length; index < count; index++) {
                DBRow tempSpot = new DBRow();
                DBRow spot = spots[index];
                tempSpot.add("yc_id", spot.get("yc_id", 0l));
                tempSpot.add("yc_name", spot.getString("yc_no"));
                returRows[index] = tempSpot;
            }
            return returRows;
        }
        return null;
    }

    // 安卓获得checkout数据
    public DBRow androidGetCheckOutData(long info_id,
                                        AdminLoginBean adminLoginBean) throws Exception {
        try {
            DBRow resultRow = new DBRow();
            DBRow mainRow = this.floorCheckInMgrZwb.findMainById(info_id);
            CheckInEntryPermissionUtil.checkEntry(mainRow, adminLoginBean);

            if (mainRow != null) {
                resultRow.add("is_live", mainRow.get("isLive", 0l));
                resultRow.add("tractor_status",
                    mainRow.get("tractor_status", 0l)); // 张睿添加（是否是已经离开的字段）
                resultRow.add("seal", mainRow.get("out_seal", ""));
/*
* if(mainRow.get("yc_id",0l)!=0 && mainRow.get("yc_id",
* 0l)!=-1){ DBRow
* ycRow=this.floorCheckInMgrZwb.findStorageYardControl
* (mainRow.get("yc_id",0l));
* resultRow.add("yc_id",mainRow.get("yc_id", 0l));
* resultRow.add("yc_name",ycRow.get("yc_no", "")); }else{
* resultRow.add("yc_id",""); resultRow.add("yc_name",""); }
* 原有代码2014_11-14 _1
*/
                DBRow[] spots = storageYardControlZr
                    .getUseSpotByAssociateIdAndType(info_id,
                        ModuleKey.CHECK_IN);
                DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
                if (fixSpots != null) {
                    resultRow.add("spots", fixSpots);
                }
                resultRow.add("gate_liscense_plate",
                    mainRow.get("gate_liscense_plate", ""));

                if (mainRow.get("isLive", 0l) == CheckInLiveLoadOrDropOffKey.LIVE) {

                    DBRow[] doors = storageDoorMgrZr
                        .getUseDoorByAssociateIdAndType(info_id,
                            ModuleKey.CHECK_IN);
                    DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
                    if (fixDoors != null) {
                        resultRow.add("doors", fixDoors);
                    }
/*
* //查询子单据占用的门和停车位 long dlo_id=mainRow.get("dlo_id", 0l);
* DBRow[]
* detailRow=this.floorCheckInMgrZwb.findUseDoorByDloId
* (dlo_id); for(int i=0;i<detailRow.length;i++){ DBRow
* row=new DBRow();
*
* row.add("door_id",detailRow[i].get("rl_id", 0l));
* row.add("detail_id",detailRow[i].get("dlo_detail_id",
* 0l)); row.add("door_name",detailRow[i].get("doorId",""));
*
* boolean flag = true; for(int j=0;j<doorList.size();j++){
* flag = true;
*
* if(doorList.get(j).get("door_id",
* 0L)==detailRow[i].get("rl_id", 0l)){ flag = false; break;
* } } if(flag){ doorList.add(row); } }
*/// 2014-11-17 _2
                }
            }

// 张睿获取图片Gate Close图片的数据，主要有可能是，先采集了close图片数据，在真正close的时候在反填回去
            DBRow[] files = floorFileMgrZr
                .getAllFileByFileWithIdAndFileWithTypeAndFileWithClass(
                    info_id, FileWithTypeKey.OCCUPANCY_MAIN,
                    FileWithCheckInClassKey.PhotoGateCheckOut);
            if (files != null) {
                String[] fileNames = new String[files.length];
                for (int index = 0, count = files.length; index < count; index++) {
                    fileNames[index] = files[index].getString("file_name");
                }
                resultRow.add("files", fileNames);
            }
            return resultRow;
        } catch (CheckInNotFoundException e) {
            throw e;
        } catch (NoPermiessionEntryIdException e) {
            throw e;
        } catch (CheckInEntryIsLeftException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "androidGetCheckOutData", log);
        }
    }

    /**
     * 获得gatecheckin 日志数据
     *
     * @param gate_liscense_plate
     * @param gate_container_no
     * @param company_name
     * @param gate_driver_liscense
     * @param mc_dot
     * @param gps_tracker
     * @param number_type
     * @param add_checkin_number
     * @param yc_id
     * @param doorId
     * @param isLive
     * @return
     * @throws Exception
     */

    public String getGateCheckInLogData(String gate_liscense_plate,
                                        String gate_container_no, String company_name,
                                        String gate_driver_liscense, String mc_dot, String gps_tracker,
                                        int number_type, String add_checkin_number, long yc_id,
                                        long doorId, int isLive) throws Exception {
        try {
// 加log 数据--------------------
            String data = "";
            data += "License Plate【" + gate_liscense_plate + "】,";
            data += "Trailer/CTNR【" + gate_container_no + "】,";
            data += "Carrier【" + company_name + "】,";
            data += "Driver License【" + gate_driver_liscense + "】,";
            data += "MC/DOT【" + mc_dot + "】,";
            data += "GPS【" + gps_tracker + "】,";
            data += "TASK【" + moduleKey.getModuleName(number_type) + "："
                + add_checkin_number + "】,";

            if (yc_id != 0) {
                DBRow result = floorCheckInMgrZwb.findStorageYardControl(yc_id);
                data += "Spot【" + result.get("yc_no", "") + "】,";
            } else {
                data += "Spot【" + "" + "】,";
            }
            if (doorId != 0) {
                DBRow result = floorCheckInMgrZwb.findDoorById(doorId);
                data += "Door【" + result.get("doorId", "") + "】,";
            } else {
                data += "Door【" + "" + "】,";
            }
            if (isLive != 0) {
                data += "Type【"
                    + checkInLiveLoadOrDropOffKey
                    .getCheckInLiveLoadOrDropOffKey(isLive) + "】,";
            } else {
                data += "Type【" + "" + "】,";
            }
            if (!data.equals("")) {
                data = data.substring(0, data.length() - 1);
            }
            return data;
        } catch (Exception e) {
            throw new SystemException(e, "addGateCheckInLog", log);
        }
    }

    /**
     * gatecheckin添加设备
     *
     * @param check_in_entry_id
     * @param gate_container_no
     * @param isLive
     * @param create_time
     * @param search_container_no
     * @throws Exception
     */
    public void addEquipment(long check_in_entry_id, String gate_container_no,
                             int isLive, String gate_liscense_plate, String create_time,
                             String search_container_no, long door_id, long spot_id,
                             String in_seal, long detail_id, int rel_type, long ps_id,
                             boolean is_update) throws Exception {
        try {
            if (is_update) { // 如果是更新 时间要写成 第一次进来的时间
                DBRow mainRow = this.floorCheckInMgrZwb
                    .findGateCheckInById(check_in_entry_id);
                create_time = mainRow.getString("gate_check_in_time");
            }
            DBRow tractorRow = new DBRow(); // 车头的设备
            tractorRow.add("equipment_type",
                CheckInTractorOrTrailerTypeKey.TRACTOR); // 设备类型
            tractorRow.add("check_in_entry_id", check_in_entry_id); // 进门时的entry_id
            tractorRow.add("check_in_time", create_time); // 进门的时间
            tractorRow.add("equipment_number", gate_liscense_plate); // 设备号
            tractorRow.add("ps_id", ps_id);
            if (StrUtil.isBlank(in_seal)) {
                in_seal = "NA";
            }
            if (isLive == CheckInLiveLoadOrDropOffKey.PICK_UP
                || isLive == CheckInLiveLoadOrDropOffKey.SWAP) {
                tractorRow.add("equipment_purpose",
                    CheckInLiveLoadOrDropOffKey.PICK_UP);
            } else if (isLive == CheckInLiveLoadOrDropOffKey.TMS) {
// tms 类型 车头车尾都是tms
                tractorRow.add("equipment_purpose", isLive);
            } else {
                tractorRow.add("equipment_purpose",
                    CheckInLiveLoadOrDropOffKey.LIVE);
            }
            tractorRow.add("equipment_status",
                CheckInMainDocumentsStatusTypeKey.UNPROCESS); // 设备状态
            tractorRow.add("rel_type", rel_type);
            tractorRow.add("phone_equipment_number",
                StringUtil.convertStr2PhotoNumber(gate_liscense_plate)); // 把驾照
// 转换数字
// android用
            tractorRow.add("seal_delivery", in_seal);
            long return_id = this.floorEquipmentMgr.addEquipment(tractorRow); // 创建车头
// DBRow traRow=new DBRow();
// traRow.add("equipment_id",return_id);
// floorCheckInMgrZwb.updateDetail(detail_id, traRow);
// //把车头设备添加到task上
            if (door_id > 0) { // 添加占用资源
                this.addResourcesRelation(OccupyTypeKey.DOOR, door_id,
                    SpaceRelationTypeKey.Equipment, return_id,
                    OccupyStatusTypeKey.RESERVERED, check_in_entry_id);
            } else if (spot_id > 0) {
                this.addResourcesRelation(OccupyTypeKey.SPOT, spot_id,
                    SpaceRelationTypeKey.Equipment, return_id,
                    OccupyStatusTypeKey.OCUPIED, check_in_entry_id);
            }
            DBRow trailerRow = new DBRow(); // 车尾设备
            trailerRow.add("equipment_type",
                CheckInTractorOrTrailerTypeKey.TRAILER); // 设备类型
            trailerRow.add("check_in_entry_id", check_in_entry_id); // 进门时的entry_id
            trailerRow.add("check_in_time", create_time); // 进门的时间
            trailerRow.add("rel_type", rel_type);
            trailerRow.add("ps_id", ps_id);
            trailerRow.add("equipment_number", gate_container_no); // 设备号
            trailerRow.add("equipment_purpose", isLive == 0 ? 1 : isLive); // 车头都是live
// load
            trailerRow.add("equipment_status",
                CheckInMainDocumentsStatusTypeKey.UNPROCESS); // 设备状态
            trailerRow.add("phone_equipment_number",
                StringUtil.convertStr2PhotoNumber(gate_container_no)); // 把驾照
// 转换数字
// android用
            trailerRow.add("seal_delivery", in_seal);
            trailerRow.add("seal_pick_up", "NA");
            if (isLive == CheckInLiveLoadOrDropOffKey.PICK_UP) { // 不创建车尾设备
// 更新带走设备
// 还得判断是否查询到了带走的货柜
                if (!search_container_no.equals("")) {
                    DBRow[] rows = this.checkInMgrZyj
                        .findInYardDropEquipmentsByNumber(
                            search_container_no, ps_id,
                            CheckInTractorOrTrailerTypeKey.TRAILER);
                    if (rows.length > 0) {
                        for (int i = 0; i < rows.length; i++) {
                            long equipment_id = rows[i].get("equipment_id", 0l);
                            DBRow upRow = new DBRow();
                            upRow.add("check_out_entry_id", check_in_entry_id);
                            this.floorEquipmentMgr.updateEquipment(
                                equipment_id, upRow);
                        }
                    }
                }
                if (!gate_container_no.equals("")) {
                    trailerRow.add("equipment_purpose",
                        CheckInLiveLoadOrDropOffKey.LIVE);
                    trailerRow.add("rel_type", null);
                    long id = this.floorEquipmentMgr.addEquipment(trailerRow); // 创建车尾
                    if (door_id > 0) { // 添加占用资源
                        this.addResourcesRelation(OccupyTypeKey.DOOR, door_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.RESERVERED,
                            check_in_entry_id);
                    } else if (spot_id > 0) {
                        this.addResourcesRelation(OccupyTypeKey.SPOT, spot_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.OCUPIED, check_in_entry_id);
                    }
                }
            } else if (isLive == CheckInLiveLoadOrDropOffKey.SWAP) { // 创建车尾设备
// 更新带走设备还得判断是否查询到了带走的货柜
                if (!search_container_no.equals("")) {
                    DBRow[] rows = this.checkInMgrZyj
                        .findInYardDropEquipmentsByNumber(
                            search_container_no, ps_id,
                            CheckInTractorOrTrailerTypeKey.TRAILER);
                    if (rows.length > 0) {
                        long equipment_id = rows[0].get("equipment_id", 0l);
                        DBRow upRow = new DBRow();
                        upRow.add("check_out_entry_id", check_in_entry_id);
                        this.floorEquipmentMgr.updateEquipment(equipment_id,
                            upRow);
                    }
                }
                if (!gate_container_no.equals("")) {
                    trailerRow.add("equipment_purpose",
                        CheckInLiveLoadOrDropOffKey.DROP);
                    long id = this.floorEquipmentMgr.addEquipment(trailerRow); // 创建车尾
// DBRow row=new DBRow();
// row.add("equipment_id", id);
// floorCheckInMgrZwb.updateDetail(detail_id, row);
// //把车尾设备添加到task上
                    if (door_id > 0) { // 添加占用资源
                        this.addResourcesRelation(OccupyTypeKey.DOOR, door_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.RESERVERED,
                            check_in_entry_id);
                    } else if (spot_id > 0) {
                        this.addResourcesRelation(OccupyTypeKey.SPOT, spot_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.OCUPIED, check_in_entry_id);
                    }
                }
            } else {
                if (isLive == CheckInLiveLoadOrDropOffKey.DROP) {
                    trailerRow.add("equipment_purpose",
                        CheckInLiveLoadOrDropOffKey.DROP);
                }
                if (!gate_container_no.equals("")) {
                    long id = this.floorEquipmentMgr.addEquipment(trailerRow); // 创建车尾
// DBRow row=new DBRow();
// row.add("equipment_id", id);
// floorCheckInMgrZwb.updateDetail(detail_id, row);
// //把车尾设备添加到task上
                    if (door_id > 0) { // 添加占用资源
                        this.addResourcesRelation(OccupyTypeKey.DOOR, door_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.RESERVERED,
                            check_in_entry_id);
                    } else if (spot_id > 0) {
                        this.addResourcesRelation(OccupyTypeKey.SPOT, spot_id,
                            SpaceRelationTypeKey.Equipment, id,
                            OccupyStatusTypeKey.OCUPIED, check_in_entry_id);
                    }
                }
            }

        } catch (Exception e) {
            throw new SystemException(e, "addEquipment", log);
        }
    }

    /**
     * 修改gatecheckin时 重置设备
     *
     * @param check_in_entry_id
     * @param gate_container_no
     * @param isLive
     * @param gate_liscense_plate
     * @param create_time
     * @param search_container_no
     * @param door_id
     * @param spot_id
     * @param in_seal
     * @throws Exception
     */
    public void gateCheckInUpdateEquipment(long check_in_entry_id,
                                           String gate_container_no, int isLive, String gate_liscense_plate,
                                           String create_time, String search_container_no, long door_id,
                                           long spot_id, String in_seal, long detail_id, int rel_type,
                                           long ps_id) throws Exception {
        try {
// 删除 重新添加把。。
// 删除设备 和设备占用的资源
            this.floorEquipmentMgr
                .delateEquipmentByCheckInEntryId(check_in_entry_id);
            this.floorSpaceResourcesRelationMgr.delSpceResourcesByRelation_id(
                check_in_entry_id, ModuleKey.CHECK_IN,
                SpaceRelationTypeKey.Equipment); // 删除task 占用的资源关系
// 调重新添加设备方法
            this.addEquipment(check_in_entry_id, gate_container_no, isLive,
                gate_liscense_plate, create_time, search_container_no,
                door_id, spot_id, in_seal, detail_id, rel_type, ps_id, true);
        } catch (Exception e) {
            throw new SystemException(e, "gateCheckInUpdateEquipment", log);
        }
    }

    /**
     * 占用资源
     *
     * @param door_or_spot
     * @param door_spot_id
     * @param relation_type
     * @param relation_id
     * @param occupy_status
     * @throws Exception
     */

    public void addResourcesRelation(int door_or_spot, long door_spot_id,
                                     int relation_type, long relation_id, int occupy_status,
                                     long entry_id) throws Exception {
        try {
            DBRow row = new DBRow();
            row.add("resources_type", door_or_spot);
            row.add("resources_id", door_spot_id);
            row.add("relation_type", relation_type);
            row.add("relation_id", relation_id);
            row.add("occupy_status", occupy_status);
            row.add("module_id", entry_id);
            row.add("module_type", ModuleKey.CHECK_IN);
            this.floorSpaceResourcesRelationMgr.addSpaceResourcesRelation(row);

        } catch (Exception e) {
            throw new SystemException(e, "addResourcesRelation", log);
        }
    }

    /**
     * 根据entryId，货柜号。查询占用的资源信息
     *
     * @param trailerNo
     * @throws Exception
     * @author Yuanxinyu
     */
    public DBRow findResourceByEquipment(HttpServletRequest request,
                                         String trailerNo) throws Exception {
        try {
            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long ps_id = adminLoginBean.getPs_id();
            long equipment_type = -1;
            DBRow[] rows = this.checkInMgrZyj
                .findInYardDropOffEquipmentByNumberNotThisEntry(trailerNo,
                    ps_id, 0);
// DBRow[] rows =
// this.floorSpaceResourcesRelationMgr.findEquipmentWithResource(ps_id,
// "", trailerNo);
            DBRow detail = new DBRow();
            if (rows != null && rows.length > 0) {
// seal的处理，查询任务，> 0 则去除seal，并可以输入新的seal < 0 则disabled这个input
                DBRow[] sealRows = getTaskByEquipmentId(rows[0].get(
                    "equipment_id", 0l));
                if (sealRows.length > 0) {
                    rows[0].add("is_disabled", "false");
                    rows[0].add("now_seal",
                        sealRows[0].get("seal_pick_up", "NA"));
                } else {
                    rows[0].add("is_disabled", "true");
                }

                long resources_type = rows[0].get("resources_type", 0l);
                long resources_id = rows[0].get("resources_id", 0l);
                equipment_type = rows[0].get("equipment_type", 0l);

                String title = (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) ? "Tractor"
                    : "Trailer";
                rows[0].add("equipment_type_msg", title);

                if (resources_type == 1) {
                    detail = findDoorById(resources_id);
                    if (detail != null && detail.size() > 0) {
                        rows[0].add("location",
                            "DOOR:" + detail.getString("doorId"));
                    } else {
                        rows[0].add("location", "");
                    }
                } else {
                    detail = findStorageYardControl(resources_id);
                    if (detail != null && detail.size() > 0) {
                        rows[0].add("location",
                            "SPOT:" + detail.getString("yc_no"));
                    } else {
                        rows[0].add("location", "");
                    }
                }
                rows[0].add("is_search", 0);
            }
            DBRow defaultReturn = new DBRow();
            if (rows != null && rows.length > 0
                && equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
                defaultReturn = rows[0];
            }
            return defaultReturn;
        } catch (Exception e) {
            throw new SystemException(e, "findResourceByEquipment", log);
        }
    }

     /**
     * 根据entryId，货柜号。查询占用的资源信息
     *
     * @param trailerNo
     * @throws Exception
     * @author Yuanxinyu
     */
    public DBRow findResourceByEquipment(long ps_id,String trailerNo) throws Exception {
    	DBRow defaultReturn = null;
    	try {
            long equipment_type = -1;
            DBRow[] rows = this.checkInMgrZyj.findInYardDropOffEquipmentByNumberNotThisEntry(trailerNo,ps_id, 0);

            DBRow detail = new DBRow();
            if (rows != null && rows.length > 0) {
                DBRow[] sealRows = getTaskByEquipmentId(rows[0].get("equipment_id", 0l));
                if (sealRows.length > 0) {
                    rows[0].add("is_disabled", "false");
                    rows[0].add("now_seal",sealRows[0].get("seal_pick_up", "NA"));
                } else {
                    rows[0].add("is_disabled", "true");
                }

                long resources_type = rows[0].get("resources_type", 0l);
                long resources_id = rows[0].get("resources_id", 0l);
                equipment_type = rows[0].get("equipment_type", 0l);

                String title = (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) ? "Tractor": "Trailer";
                rows[0].add("equipment_type_msg", title);

                if (resources_type == 1) {
                    detail = findDoorById(resources_id);
                    if (detail != null && detail.size() > 0) {
                        rows[0].add("location","DOOR:" + detail.getString("doorId"));
                        rows[0].add("door_id",detail.getString("doorId"));
                    } else {
                        rows[0].add("location", "");
                    }
                } else {
                    detail = findStorageYardControl(resources_id);
                    if (detail != null && detail.size() > 0) {
                        rows[0].add("location","SPOT:" + detail.getString("yc_no"));
                        rows[0].add("spot_id",detail.getString("yc_no"));
                    } else {
                        rows[0].add("location", "");
                    }
                }
                rows[0].add("is_search", 0);
            }
            defaultReturn = new DBRow();
            if (rows != null && rows.length > 0
                && equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
                defaultReturn = rows[0];
            }
            return defaultReturn;
        } catch (Exception e) {
            e.printStackTrace();
        	//throw new SystemException(e, "findResourceByEquipment", log);
        }
    	return defaultReturn;
    }

    /**
     * 获得设备，及其关联的资源
     *
     * @param entry_id
     * @throws Exception
     * @author Yuanxinyu
     */
    public DBRow[] getEquipmentByEntryIdOccpuyResource(long entry_id)
        throws Exception {
        try {
// 根据entryId 和 占用状态1保留2占用 查询占用信息
            DBRow[] taskByEntryIdWithOccpuyStatus = this.floorSpaceResourcesRelationMgr
                .getEquipmentByEntryIdOccpuyResource(entry_id);
            return taskByEntryIdWithOccpuyStatus;
        } catch (Exception e) {
            throw new SystemException(e, "getEquipmentByEntryIdOccpuyResource",
                log);
        }
    }

    public DBRow[] getCheckOutEntryUseResource(long entry_id) throws Exception {
        try {
// 根据entryId查询设备占用信息
            DBRow[] rows = this.ymsMgrAPI.getCheckOutEntryUseResource(entry_id);
            DBRow detail = new DBRow();
            if (rows != null && rows.length > 0) {
                for (int i = 0; i < rows.length; i++) {
                    rows[i].remove("check_in_time");
// rows[i].remove("check_in_entry_id");
                    rows[i].remove("check_out_entry_id");
// rows[i].remove("equipment_purpose");
                    rows[i].remove("equipment_status");
                    rows[i].remove("seal_delivery");
                    rows[i].remove("phone_equipment_number");
                    rows[i].remove("srr_id");
                    rows[i].remove("relation_type");
                    rows[i].remove("relation_id");
                    rows[i].remove("occupy_status");
                    rows[i].remove("module_type");
                    long resources_type = rows[i].get("resources_type", 0l);
                    long resources_id = rows[i].get("resources_id", 0l);
                    long equipment_type = rows[i].get("equipment_type", 0l);

                    String title = (equipment_type == 1) ? "Tractor"
                        : "Trailer";
                    rows[i].add("equipment_type_msg", title);

                    long isSearch = (rows[i].get("check_in_entry_id", 0l) == entry_id) ? 1
                        : 0; // 1为本身的车尾 0为pickup 需要带走车尾（ 记录日志用）

                    rows[i].add("isSearch", isSearch);

                    if (resources_type == 1) {
                        detail = findDoorById(resources_id);
                        if (detail != null && detail.size() > 0) {
                            rows[i].add("location",
                                "DOOR:" + detail.getString("doorId"));
                        }
                    } else {
                        detail = findStorageYardControl(resources_id);
                        if (detail != null && detail.size() > 0) {
                            rows[i].add("location",
                                "SPOT:" + detail.getString("yc_no"));
                        }
                    }

// seal的处理，查询任务，> 0 则去除seal，并可以输入新的seal < 0 则disabled这个input
                    DBRow[] sealRows = getTaskByEquipmentId(rows[i].get(
                        "equipment_id", 0l));

                    if (sealRows.length > 0) {
                        rows[i].add("is_disabled", "false");
                        rows[i].add("now_seal",
                            sealRows[0].get("seal_pick_up", "NA"));
                    } else {
                        rows[i].add("is_disabled", "true");
                    }

                }
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "getCheckOutEntryUseResource", log);
        }
    }

    public String[] getAndroidCheckOutFiles(long info_id) throws Exception {
        try {
            DBRow[] files = floorFileMgrZr
                .getAllFileByFileWithIdAndFileWithTypeAndFileWithClass(
                    info_id, FileWithTypeKey.OCCUPANCY_MAIN,
                    FileWithCheckInClassKey.PhotoGateCheckOut);
            if (files != null) {
                String[] fileNames = new String[files.length];
                for (int index = 0, count = files.length; index < count; index++) {
                    fileNames[index] = files[index].getString("file_name");
                }
                return fileNames;
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "getCheckOutFiles", log);
        }
    }

    /**
     * android or web check out yxy 1.释放传入的资源的Id 2.添加日志 3.处理图片（Android和web处理不一样）
     *
     * @param isAndroid
     * @param request
     * @throws Exception
     */
    @Override
    public DBRow checkOut(boolean isAndroid, HttpServletRequest request)
        throws Exception {
        PostMethod post = null;
        try {
            DBRow result = new DBRow();
// 得到request中的admin对象
            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");

// checkout 清除schedule信息
            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                .getBeanFromContainer("proxyCheckInMgrZr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long adid = adminLoginBean.getAdid();
            long ps_id = adminLoginBean.getPs_id();
// 得到ajax传过来的参数
            String jsonString = StringUtil.getString(request, "jsonObject");
// 将string转成 jsonObject
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject
                .fromObject(jsonString);
            long entry_id = jsonObject.getLong("entry_id");
// 若带出的设备类型为TMS 则验证check in/out的driver与车头信息
            DBRow[] tractor = floorCheckInMgrWfh.findTractorByEntryId(entry_id,
                CheckInTractorOrTrailerTypeKey.TRACTOR);
            DBRow[] trailers = floorCheckInMgrWfh.findTractorByEntryId(
                entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
            int closeFlag = StringUtil.getInt(request, "close_flag");
            for (DBRow trailer : trailers) {
                int purpose = trailer.get("equipment_purpose", 0);
                if (purpose == checkInLiveLoadOrDropOffKey.TMS
                    && closeFlag == 0) {
                    HttpClient clien = new HttpClient();
                    post = new PostMethod(
                        "http://tms.gofuze.io/api/containerInOutCheck/");
                    String code = "";
                    String time = DateUtil.NowStr();
                    Md5 md5 = new Md5();
                    code = md5.getMD5ofStr(time + "arrest_1!").toLowerCase();
                    DBRow data = new DBRow();
                    data.add("CTNR", trailer.get("equipment_number", ""));
                    data.add("TIME", time);
                    data.add("API_KEY", "UvIxOvxDrdhV");
                    data.add("CHECK_CODE", code);
                    data.add("LOC_CODE", trailer.get("ps_id", 0L));// "1000009"
                    post.addParameter("data", StringUtil
                        .convertDBRowsToJsonStringKeyIsUpperCase(data));
                    int status_code = clien.executeMethod(post);
                    String body = post.getResponseBodyAsString();
                    post.releaseConnection();
                    JSONObject result1 = new JSONObject(body);
// Example response {"RESULT":1,"DRIVER_NAME":"Jose Flores"
// ,"DISPATCH_DATE":"2015-05-11 00:00:00","COMPLETED_DATE":null,"FROM_WARESHOUSE":"PORTUS"
// ,"TO_WAREHOUSE":"FRONTIER LOGISTICS "}

// Exception response
// {"RESULT":0,"ERROR":"No inbound-outbound info found for given container and location and date."}
                    int result_status = StringUtil
                        .getJsonInt(result1, "RESULT");
                    String driver_name = StringUtil.getJsonString(result1,
                        "DRIVER_NAME");
                    String dispatch_date = StringUtil.getJsonString(result1,
                        "DISPATCH_DATE");
                    String completed_date = StringUtil.getJsonString(result1,
                        "COMPLETED_DATE");
                    DateFormat df = new SimpleDateFormat("mm/dd/YY hh:MM");
                    DateFormat df1 = new SimpleDateFormat("YYY-mm-dd hh:MM:ss");
                    if (dispatch_date != null && !dispatch_date.equals("")
                        && !dispatch_date.equals("null")) {
                        Date date = df1.parse(dispatch_date);
                        dispatch_date = df.format(date);
                    }
                    if (completed_date != null && !completed_date.equals("")
                        && !completed_date.equals("null")) {
                        Date date = df1.parse(completed_date);
                        completed_date = df.format(date);
                    }
                    String from_wareshouse = StringUtil.getJsonString(result1,
                        "FROM_WARESHOUSE");
                    String to_warehouse = StringUtil.getJsonString(result1,
                        "TO_WARESHOUSE");
                    result.add("result", result_status);
                    result.add("driver_name", driver_name.equals("null") ? ""
                        : driver_name);
                    result.add("dispatch_date",
                        dispatch_date.equals("null") ? "" : dispatch_date);
                    result.add("completed_date",
                        completed_date.equals("null") ? "" : completed_date);
                    result.add("from_wareshouse", from_wareshouse
                        .equals("null") ? "" : from_wareshouse);
                    result.add("to_warehouse", to_warehouse.equals("") ? ""
                        : to_warehouse);
                    result.add("phone_number", "626-271-9805");
                    return result;
                }
            }

            String filePath = "";
// 如果是安卓则获取 filePath
            if (isAndroid)
                filePath = jsonObject.getString("filePath");
// 获取jsonObject中的 JSONArray对象
            net.sf.json.JSONArray jsonArray = jsonObject
                .getJSONArray("jsonArray");

            String allLog = "";
            boolean val = true;
// 查到entry进来时所有的车尾
            DBRow[] equipments = floorCheckInMgrWfh.findTractorByEntryId(
                entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
            DBRow[] tractors = floorCheckInMgrWfh.findTractorByEntryId(
                entry_id, CheckInTractorOrTrailerTypeKey.TRACTOR);
            int tractor_requipment_purpose = 0;
            int trailer_requipment_purpose = 0;
            boolean check_out_this_flag = false; // 这次带走的是否有check_in进来的车尾
            String live_ids = "";
            String drop_ids = "";
            DBRow check_out_equipment = new DBRow();
// 这次带走的是否有check_in进来的车尾
            for (int i = 0; i < jsonArray.size(); i++) {
                net.sf.json.JSONObject jsonObj = jsonArray.getJSONObject(i);
                long equipment_id = jsonObj.getLong("equipment_id"); // 设备ID
                long check_in_entry_id = jsonObj.getLong("check_in_entry_id");
                int equipment_type = jsonObj.getInt("equipment_type"); // 1车头
// 2车尾
// "Tractor"
// :
// "Trailer"
// ;
                if (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
                    continue;
                }
                if (check_in_entry_id == entry_id) {
                    check_out_this_flag = true;
                }
                check_out_equipment.add(equipment_id + "", check_in_entry_id);
            }
// 仅车头走了
            if (jsonArray.size() == 1) {
                tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.LIVE;
                if (tractors[0].get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.TMS) {
                    tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.TMS;
                }
// 有车尾但没有带走
                if (equipments.length > 0)
                    trailer_requipment_purpose = CheckInLiveLoadOrDropOffKey.DROP;
            }
// 带其他的车尾走了。
            else if (jsonArray.size() > 1) {

// 带走的车尾中没有checkIn进来的车尾
                if (!check_out_this_flag) {
                    tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.PICK_UP;
// checkIn的时候带进来车尾了
                    if (equipments.length > 0) {
                        trailer_requipment_purpose = CheckInLiveLoadOrDropOffKey.DROP;
                    }
                } else {
// 带走的车尾有部分自己也有其它的
                    for (DBRow equipment : equipments) {
                        long equipment_id = equipment.get("equipment_id", 0L);
                        long check_in_entry_id = check_out_equipment.get(
                            equipment_id + "", 0L);
                        if (check_in_entry_id != 0) {
                            if (equipment.get("equipment_purpose", 0) != CheckInLiveLoadOrDropOffKey.TMS) {
                                live_ids += equipment_id + ",";
                            }
                            check_out_equipment.remove("" + equipment_id);
                        } else {
                            drop_ids += equipment_id + ",";
                        }
                    }
                    if (check_out_equipment.size() > 0) {
                        tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.PICK_UP;
                    } else {
                        tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.LIVE;
                        if (tractors[0].get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.TMS) {
                            tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.TMS;
                        }
                    }
                }
            }
// 更新设备的purpose值
            if (tractor_requipment_purpose > 0) {
                DBRow tractor_row = new DBRow();
                tractor_row
                    .add("equipment_purpose", tractor_requipment_purpose);
                floorCheckInMgrWfh.updateEquipmentPurposeByEntryID(entry_id,
                    CheckInTractorOrTrailerTypeKey.TRACTOR, tractor_row);
            }
            if (trailer_requipment_purpose > 0) {
                DBRow trailer_row = new DBRow();
                trailer_row
                    .add("equipment_purpose", trailer_requipment_purpose);
                floorCheckInMgrWfh.updateEquipmentPurposeByEntryID(entry_id,
                    CheckInTractorOrTrailerTypeKey.TRAILER, trailer_row);
            }
            if (live_ids.length() > 0) {
                live_ids = live_ids.substring(0, live_ids.length() - 1);
                DBRow trailer_row = new DBRow();
                trailer_row.add("equipment_purpose",
                    CheckInLiveLoadOrDropOffKey.LIVE);
                floorCheckInMgrWfh.updateEquipmentPurposeByEquipmentID(
                    live_ids, CheckInTractorOrTrailerTypeKey.TRAILER,
                    trailer_row);

            }
            if (drop_ids.length() > 0) {
                drop_ids = drop_ids.substring(0, drop_ids.length() - 1);
                DBRow trailer_row = new DBRow();
                trailer_row.add("equipment_purpose",
                    CheckInLiveLoadOrDropOffKey.DROP);
                floorCheckInMgrWfh.updateEquipmentPurposeByEquipmentID(
                    drop_ids, CheckInTractorOrTrailerTypeKey.TRAILER,
                    trailer_row);

            }
// 遍历jsonArray
            for (int i = 0; i < jsonArray.size(); i++) {
                net.sf.json.JSONObject jsonObj = jsonArray.getJSONObject(i);
                int equipment_id = jsonObj.getInt("equipment_id"); // 设备ID
                String equipment_no = jsonObj.getString("equipment_no");
                String location = jsonObj.getString("location");
                String seal = jsonObj.getString("seal");
                long equipment_type = jsonObj.getLong("equipment_type"); // 1车头
// 2车尾
// "Tractor"
// :
// "Trailer"
// ;
                String resources_type = jsonObj.getString("resources_type"); // 1door
// 2
// spot
                long check_in_entry_id = jsonObj.getLong("check_in_entry_id");
                long isSearch = jsonObj.getLong("isSearch");
                String levelHead = "";
                if (check_in_entry_id != entry_id
                    && equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
                    levelHead += "Pick up ";
                }
                levelHead += (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) ? "Tractor"
                    : "Trailer";
// 当dropoff的时候，递交到后台一条记录，即为 车头 因其未占用资源，所以标记为No-Tractor-left
                if (jsonArray.size() == 1
                    && equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
                    String endPoint = "";
                    if ("".equals(location) || "Task".equals(location)) {
                        endPoint = ",";
                    }
                    String dropLog = "";
                    dropLog += levelHead + "【" + equipment_no + "】Left"
                        + endPoint;
                    if (!"".equals(location) && !"Task".equals(location)) {
                        dropLog += "-Release:" + location.substring(0, 4) + "【"
                            + location.substring(5, location.length())
                            + "】" + ",";
                    }
                    checkOutTheLog("Check Out Equipment", dropLog, isAndroid,
                        adid, check_in_entry_id);
                    val = false;
                }
// 带走别人的车尾 需要在其entry_id中记录日志
                else if (jsonArray.size() != 1 && isSearch == 0) {
                    String anotherLog = "";
                    if (!"Task".equals(location)) {
                        if (!"".equals(seal)) {
                            anotherLog += "Seal【" + seal + "】,";
                        }
                        String endPoint = "";
                        if ("".equals(location)) {
                            endPoint = ",";
                        }
                        anotherLog += levelHead + "【" + equipment_no + "】Left"
                            + endPoint;
                        if (!"".equals(location)) {
                            anotherLog += "-Release:"
                                + location.substring(0, 4) + "【"
                                + location.substring(5, location.length())
                                + "】" + ",";
                        }

// 调用记录日志方法 记录被带走车尾的entry_id中
                        checkOutTheLog("Check Out Equipment", anotherLog,
                            isAndroid, adid, check_in_entry_id);
                        val = true;
                    }
                }
                if (!"Task".equals(location)) {
                    if (!"".equals(seal)) {
                        allLog += "Seal【" + seal + "】,";
                    }
                    String endPoint = "";
                    if ("".equals(location)) {
                        endPoint = ",";
                    }
                    allLog += levelHead + "【" + equipment_no + "】Left"
                        + endPoint;
                    if (!"".equals(location)) {
                        allLog += "-Release:" + location.substring(0, 4) + "【"
                            + location.substring(5, location.length())
                            + "】" + ",";
                    }
                }

// 第一步
// 更新entry表的check_out_time和设备表的check_out_time，如果资源为door，则记录check_out_warehouse_time
                checkOutTime(equipment_id, adid, entry_id, resources_type,equipment_type);
// 第二步 释放资源记回task表
                checkOutReturnTheTask(equipment_id);
// 第三步
// 查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true
                checkOutTaskStatus(isAndroid, adid, isSearch,
                    check_in_entry_id, entry_id, equipment_id);
// 第四步 调用詹洁的释放资源方法
                checkOutByZj(equipment_id, adid, entry_id);
// 第五步 根据equipment_id记录seal
                checkOutTheSeal(equipment_id, seal);

// 清除与设备 相关的未完成的 schedule
                DBRow[] schedules = this.floorCheckInMgrZwb
                    .getScheduleByEquipmentId(equipment_id);
                for (int index = 0, len = schedules.length; index < len; index++) {
                    checkInMgrZr.finishSchedule(
                        schedules[index].get("schedule_id", 0l), adid);
                }
// 如果设备没有关联任务,但是却发布了schedule给window,当设备全部离开时,清除任务
                if (isAllLeft(check_in_entry_id)) {
                    DBRow[] schedulesWithoutTask = this.floorCheckInMgrZwb
                        .getNoTaskScheduleByEntryId(check_in_entry_id);
                    for (int index = 0, len = schedulesWithoutTask.length; index < len; index++) {
                        checkInMgrZr.finishSchedule(schedulesWithoutTask[index]
                            .get("schedule_id", 0l), adid);
                    }
                }
            }

            if (val) {
// 调用记录日志的方法
                checkOutTheLog("Check Out Equipment", allLog, isAndroid, adid,
                    entry_id);
            }
// 处理图片
            if (isAndroid) {
// androidSaveMoveFile(filePath, adid, entry_id,
// "Gate_check_out_");
                androidSaveMoveFile(filePath, entry_id, "Gate_check_out_",
                    request.getRequestedSessionId());
            } else {
                addFiles(request, entry_id, adid); // 照片
            }

// 更新索引
            editCheckInIndex(entry_id, "update");

// 向tms的接口push
            for (DBRow trailer : trailers) {
                int purpose = trailer.get("equipment_purpose", 0);
// if(purpose == CheckInLiveLoadOrDropOffKey.TMS){
                String time = DateUtil.NowStr();
                time = DateUtil.showLocalTime(time, ps_id);
                int resources_type = trailer.get("resources_type", 0);
                long yc_id = 0L;
                if (resources_type == occupyTypeKey.SPOT) {
                    yc_id = trailer.get("resources_id", 0L);
                }
                DBRow spot = new DBRow();
                if (yc_id > 0) {
                    spot = floorCheckInMgrWfh.findSpotBySpotID(yc_id);
                }
                String out_seal = trailer.get("seal_pick_up", "");
                String in_seal = trailer.get("seal_delivery", "");

                boolean isOcranContainer = false;
// 验证ctnr是否为海运集装箱号码 是的话验证是否正确
                String gate_container_no = trailer.get("equipment_number", "");
                if (Pattern.matches("[A-Za-z]{4}\\d{7}$", gate_container_no)) {
                    isOcranContainer = true;
/*
* String checkResult = getCntr(gate_container_no); if
* (!checkResult.equals("success")) { isOcranContainer =
* true; }
*/
                }
                if (!isOcranContainer) {
                    System.out.print("========="
                        + gate_container_no.toUpperCase()
                        + " is not a ocran container#==========");
                    continue;
                }

// push数据不管成功还是失败
                try {
                    Md5 md5 = new Md5();
                    HttpClient client = new HttpClient();
                    post = new PostMethod(
                        "http://tms.gofuze.io/api/containerInOutPush/");
                    DBRow data = new DBRow();
// data.add("API_KEY", "UvIxOvxDrdhV");
                    data.add("API_KEY", "arrest_1");
                    data.add("CTNR", trailer.get("equipment_number", "")
                        .toUpperCase());
                    data.add("WHS_ID",
                        getTmsWarehouseId(adminLoginBean.getPs_id()));
                    data.add("TYPE", "1"); // check out

                    data.add("SEAL", out_seal.equals("") ? in_seal : out_seal);
                    data.add("TIME", time);
                    data.add("CHECK_CODE", md5.getMD5ofStr(time + "arrest_1!")
                        .toLowerCase());
// 查询需要的数据
                    DBRow entry_info = floorCheckInMgrWfh
                        .findEntryInfoForTmsPush(trailer.get(
                            "equipment_id", 0L));
                    data.add("MC_DOC", entry_info.get("mc_dot", "")
                        .toUpperCase());
                    data.add("CARRIER", entry_info.get("company_name", "")
                        .toUpperCase());
                    data.add("DRIVER_NAME",
                        entry_info.get("gate_driver_name", "")
                            .toUpperCase());
                    data.add("DRIVER_LICENSE",
                        entry_info.get("gate_driver_liscense", "")
                            .toUpperCase());
                    data.add("SPOT", entry_info.get("yc_no", ""));
                    data.add("SOPT_AREA", entry_info.get("area_name", ""));
                    data.add("LP", tractor[0].get("equipment_number", ""));
                    String json = StringUtil
                        .convertDBRowsToJsonStringKeyIsUpperCase(data);
                    post.addParameter("data", json);
                    System.out.println("======post data :" + json
                        + "==========");
                    client.executeMethod(post);
                    String body = post.getResponseBodyAsString();
                    System.out.println("==========TMS Response:" + body
                        + "==============");
                } catch (Exception e) {
                    log.info(e);
                } finally {
                    if (post != null) {
                        post.releaseConnection();
                    }
                }
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "checkOut", log);

        }
    }

    /**
     * 判断某次entry下的所有设备是否已经全部离开
     *
     * @param check_in_entry_id
     * @return true:全部离开
     * @throws Exception
     */
    private boolean isAllLeft(long check_in_entry_id) throws Exception {
        boolean result = false;
        DBRow[] equipments = this.floorEquipmentMgr
            .getEquipmentByEntryId(check_in_entry_id);
        if (equipments.length > 0) {
            result = true;
            for (int i = 0, len = equipments.length; i < len; i++) {
                if (equipments[i].get("equipment_status", 0l) != CheckInMainDocumentsStatusTypeKey.LEFT) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 第一步 更新entry表的check_out_time和设备表的check_out_time
     * 如果资源为door，则记录check_out_warehouse_time
     *
     * @author yuanxinyu
     */
    public void checkOutTime(long equipment_id, long adid, long entry_id,
                             String resources_type, long equipment_type,String ...checkOutTimes) throws Exception {
        try {
            DBRow mainRow = new DBRow(); // 跟新主单据的row
            String createTime = DateUtil.NowStr(); // 当前时间
			if (checkOutTimes != null && checkOutTimes.length > 0  && checkOutTimes[0] != null && !"".equals(checkOutTimes[0])) {
            	createTime  = checkOutTimes[0];
            }
            mainRow.add("check_out_time", createTime);
            if (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
                this.floorCheckInMgrZwb.modCheckIn(entry_id, mainRow); // 更新entry表的check_out_time
            }
            // 更新设备表中的check_out_time
            this.floorEquipmentMgr.updateEquipment(equipment_id, mainRow);
            System.out.println("=======================" + equipment_id + ":" + mainRow.getString("check_out_time") + "=============================");
            // 如果此设备是door，则记录设备表中的check_out_warehouse_time
			if ("1".equals(resources_type)) {
				DBRow equipmentRow = new DBRow();
				equipmentRow.add("check_out_warehouse_time", createTime);
				this.floorEquipmentMgr.updateEquipment(equipment_id,equipmentRow);
			}

        } catch (Exception e) {
            throw new SystemException(e, "checkOutTheResource", log);
        }
    }
    

    

    /**
     * 第二步 把任务释放的资源记回task表
     *
     * @param equipment_id
     * @throws Exception
     * @author yuanxinyu
     */
    public void checkOutReturnTheTask(long equipment_id) throws Exception {
    	// 获得与设备关联的任务
        DBRow[] tasks = floorCheckInMgrZwb.getTaskByEquimentId(equipment_id, new int[0]);
        // 释放任务占用的资源
        for (int i = 0; i < tasks.length; i++) {
            long dlo_detail_id = tasks[i].get("dlo_detail_id", 0l);
            // 查询该任务占用的资源
            DBRow row = this.floorSpaceResourcesRelationMgr .getSpaceResorceRelation(SpaceRelationTypeKey.Task,dlo_detail_id);
            if (row != null && row.size() != 0) {
                DBRow detailRow = new DBRow();
                detailRow.add("rl_id", row.get("resources_id", 0l));
                detailRow.add("occupancy_type", row.get("resources_type", 0l));
                this.floorCheckInMgrZwb.updateDetailByIsExist(dlo_detail_id,detailRow); // 把要释放的资源更新回 task 表
            }
        }
    }

    /**
     * 第三步 查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true
     *
     * @param equipment_id
     * @throws Exception
     */
    public void checkOutTaskStatus(boolean isAndroid, long adid, long isSearch,
                                   long check_in_entry_id, long entry_id, long equipment_id)
        throws Exception {
        String taskLog = "";
// 获得与设备关联的任务
        DBRow[] tasks = floorCheckInMgrZwb.getTaskByEquimentId(equipment_id,
            new int[0]);
// 释放任务占用的资源
        for (int i = 0; i < tasks.length; i++) {
            if (!"".equals(tasks[i]) && tasks[i] != null) {
                String space = (tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.DOOR) ? "Door"
                    : "Spot";
                String spaceNo = "";
                if (tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.DOOR) {
                    spaceNo = floorCheckInMgrZwb.findDoorById(
                        tasks[i].get("rl_id", 0l)).get("doorId", ""); // doorId
                } else if (tasks[i].get("occupancy_type", 0l) == OccupyTypeKey.SPOT) {
                    spaceNo = floorCheckInMgrZwb.findStorageYardControl(
                        tasks[i].get("rl_id", 0l)).get("yc_no", ""); // yc_no
                }
// 查询该任务占用的资源
                DBRow row = this.floorSpaceResourcesRelationMgr
                    .getSpaceResorceRelation(SpaceRelationTypeKey.Task,
                        tasks[i].get("dlo_detail_id", 0l));
                if (row != null && row.size() != 0 && !"".equals(spaceNo)) {
                    taskLog += "Task" + "【" + tasks[i].get("number", "")
                        + "】Release:" + space + "【" + spaceNo + "】" + ",";
                }

                if (tasks[i].get("number_status", 0l) == CheckInChildDocumentsStatusTypeKey.PROCESSING
                    || tasks[i].get("number_status", 0l) == CheckInChildDocumentsStatusTypeKey.UNPROCESS) {
                    long dlo_detail_id = tasks[i].get("dlo_detail_id", 0l);
                    DBRow status = new DBRow();
                    status.add("is_forget_task", 1);
                    this.floorCheckInMgrZwb.updateDetailByIsExist(
                        dlo_detail_id, status);
                    taskLog += "Task" + "【" + tasks[i].get("number", "")
                        + "】ForgetTask" + ",";
                }
            }
        }
        if (!"".equals(taskLog)) {
            if (isSearch == 0 && entry_id != check_in_entry_id) {
                checkOutTheLog("Check Out Task", taskLog, isAndroid, adid,
                    check_in_entry_id);
            } else if (isSearch == 1 && entry_id == check_in_entry_id) {
// 记录task日志
                checkOutTheLog("Check Out Task", taskLog, isAndroid, adid,
                    entry_id);
            }
        }
    }

    /**
     * 第四步 调用詹洁的释放资源方法
     *
     * @param equipment_id
     * @throws Exception
     */
    public void checkOutByZj(long equipment_id, long adid, long entry_id,String ...checkoutTimes)
        throws Exception {
        this.ymsMgrAPI.checkOutReleaseResources(entry_id, equipment_id, adid,checkoutTimes); // 调用詹洁的方法，释放各种占用资源
    }

    /**
     * 第五步 根据equipment_id记录seal
     *
     * @param equipment_id
     * @throws Exception
     */
    public void checkOutTheSeal(long equipment_id, String seal)
        throws Exception {
        DBRow sealRow = new DBRow();
        sealRow.add("seal_pick_up", seal);
        this.floorEquipmentMgr.updateEquipment(equipment_id, sealRow);
    }

    /**
     * 记录日志方法
     *
     * @author yuanxinyu
     */
    @Override
    public void checkOutTheLog(String mark, String data, boolean isAndroid, long adid, long entry_id,String ...checkoutTimes) throws Exception {
        try {
            String createTime = DateUtil.NowStr(); // 当前时间
			if (checkoutTimes != null && checkoutTimes.length > 0 && checkoutTimes[0] != null && !"".equals(checkoutTimes[0])) {
				createTime = checkoutTimes[0];
			}
            int LogType = isAndroid ? CheckInLogTypeKey.ANDROID_GATEOUT : CheckInLogTypeKey.GATEOUT;
            addCheckInLog(adid, mark, entry_id, LogType, createTime, data); // 添加日志
            // 车头车尾日志

        } catch (Exception e) {
            throw new SystemException(e, "checkOutTheLog", log);
        }
    }

    /**
     * 更新资源表
     *
     * @param door_id
     * @param spot_id
     * @param entry_id
     * @param relation_id
     * @param relation_type
     * @throws Exception
     */

    public void updateResourcesRelation(long door_id, long spot_id,
                                        long entry_id, long relation_id, int relation_type,
                                        AdminLoginBean adminLoginBean) throws Exception {
        try {

            long createManId = adminLoginBean.getAdid();
            if (door_id > 0) {
                ymsMgrAPI.operationSpaceRelation(OccupyTypeKey.DOOR, door_id,
                    relation_type, relation_id, ModuleKey.CHECK_IN,
                    entry_id, OccupyStatusTypeKey.RESERVERED, createManId);
            }
            if (spot_id > 0) {
                ymsMgrAPI.operationSpaceRelation(OccupyTypeKey.SPOT, spot_id,
                    relation_type, relation_id, ModuleKey.CHECK_IN,
                    entry_id, OccupyStatusTypeKey.OCUPIED, createManId);
            }
        } catch (Exception e) {
            throw new SystemException(e, "updateResourcesRelation", log);
        }
    }

    /**
     * 添加gatecheckin数据
     *
     * @param row
     * @param request
     * @throws Exception
     */
    public long gateCheckInAddData(HttpServletRequest request, DBRow row,
                                   AdminLoginBean adminLoginBean, boolean isAndroid) throws Exception { // gateCheckIn
        // 时添加数据
        try {

            int isLive = row.get("live", 0);
            String gate_driver_name = row.get("driver_name", "").toUpperCase();
            String gate_liscense_plate = row.get("liscense_plate", "").toUpperCase();
            // String appointment_time = StringUtil.getString(request,
            // "appointment_time");
            String gate_driver_liscense = row.get("driver_liscense", "").toUpperCase();
            // String gate_container_no = StringUtil.getString(request,
            // "gate_container_no").toUpperCase();//货柜号
            String company_name = row.get("company_name", "").toUpperCase(); // 运输公司名
            String gps_tracker = row.get("gps_tracker", "");
            // long ps_id = StringUtil.getLong(request, "add_ps_id");
            long mark = row.get("mark", 0l);
            long gpsId = row.get("gpsId", 0l);
            long ps_id = row.get("ps_id", 0L) ;
            if(ps_id == 0){
            	ps_id = adminLoginBean.getPs_id();
            }
            long createManId = adminLoginBean.getAdid();
            String mc_dot = row.get("mc_dot", "").toUpperCase();
            int load_count = row.get("load_count", 0);
            // long rel_type = StringUtil.getLong(request, "rel_type");
            // String add_checkin_number = StringUtil.getString(request,
            // "add_checkin_number").toUpperCase();
            // long doorId=StringUtil.getLong(request,"checkInDoorId");
            // long yc_id = StringUtil.getLong(request, "yc_id");
            long main_id = row.get("main_id", 0l);
            String filePath = row.get("filePath", "");
            DBRow mainRow = new DBRow();

            // gate check in 优先级默认为0 ---BY CHENCHEN---
            mainRow.add("priority", 0);

            // ***************end
            mainRow.add("yms_entry_id", row.getString("yms_entry_id"));
            mainRow.add("gate_driver_name", gate_driver_name);
            mainRow.add("gate_driver_liscense", gate_driver_liscense);
            mainRow.add("company_name", company_name);
            mainRow.add("mc_dot", mc_dot);
            if (mark == 1) {
                long id = addAsset(gps_tracker, gate_liscense_plate, 0, "", "",
                    "");
                mainRow.add("gps_tracker", id);
            } else {
                if (gpsId > 0) {
                    DBRow updateRow = new DBRow();
                    updateRow.add("def", gate_liscense_plate);
                    floorCheckInMgrZwb.updateAsset(gpsId, updateRow);
                    mainRow.add("gps_tracker", gpsId);
                }
            }
            mainRow.add("ps_id", ps_id);
            mainRow.add("load_count", load_count);
            mainRow.add("gate_check_in_operate_time", DateUtil.NowStr());
            mainRow.add("gate_check_in_operator", createManId);
            mainRow.add("creator", createManId);
            mainRow.add("photo_gate_driver_liscense", StringUtil.convertStr2PhotoNumber(gate_driver_liscense)); // 张睿2015-01-15
            // gate_driver_liscense
            // to
            // phone
            // number
            // android
            // 数字键查询
            String create_time = DateUtil.NowStr();
            String remark = "";
            if (main_id == 0) {
               
                if(!row.get("checkInTime", "").equals("")){
                	create_time = row.get("checkInTime", "");
                }
                mainRow.add("create_time", create_time);
                mainRow.add("gate_check_in_time", create_time);
                main_id = floorCheckInMgrZwb.gateCheckInAdd(mainRow); // 添加
                // main表数据
                remark = "Add Driver Info";
            } else {
                floorCheckInMgrZwb.modCheckIn(main_id, mainRow);
                remark = "Update Driver Info";
            }

            if (isAndroid) {
                // 文件上传到文件服务器里面 --by chenchen
                String sessionId = request.getRequestedSessionId();
                DBRow r = new DBRow();
                r.add("file_with_id", main_id);
                r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
                r.add("file_with_class",
                    FileWithCheckInClassKey.PhotoGateCheckIn);
                fileMgrZr.uploadDirToFileServ(filePath, r, sessionId,
                    "gate_check_in_" + main_id + "_");
            } else {
                addFiles(request, main_id, createManId);
            }

            // 记录日志
            String logData = "";
            logData += "Driver License: " + gate_driver_liscense + ",";
            logData += "Driver Name: " + gate_driver_name + ",";
            logData += "MC/DOT: " + mc_dot + ",";
            logData += "Carrier: " + company_name + ",";
            logData += "GPS: " + gps_tracker + ",";

            if (!logData.equals("")) {
                logData = logData.substring(0, logData.length() - 1);
            }
            addCheckInLog(createManId, remark, main_id,CheckInLogTypeKey.GATEIN, create_time, logData);

            return main_id;
        } catch (Exception e) {
            throw new SystemException(e, "gateCheckInAddData", log);
        }
    }

    /**
     * 添加gatecheckin数据
     *
     * @param row
     * @throws Exception
     */
    public long gateCheckInAddData(DBRow row, AdminLoginBean adminLoginBean) throws Exception {
        // gateCheckIn
        // 时添加数据
        try {
            int isLive = row.get("live", 0);
            String gate_driver_name = row.get("driver_name", "").toUpperCase();
            String gate_liscense_plate = row.get("liscense_plate", "").toUpperCase();

            String gate_driver_liscense = row.get("driver_liscense", "").toUpperCase();

            String company_name = row.get("company_name", "").toUpperCase(); // 运输公司名
            String gps_tracker = row.get("gps_tracker", "");

            long mark = row.get("mark", 0l);
            long gpsId = row.get("gpsId", 0l);
            long ps_id = adminLoginBean.getPs_id();
            long createManId = adminLoginBean.getAdid();
            String mc_dot = row.get("mc_dot", "").toUpperCase();
            int load_count = row.get("load_count", 0);

            long main_id = row.get("main_id", 0l);
            String filePath = row.get("filePath", "");
            DBRow mainRow = new DBRow();

            // gate check in 优先级默认为0 ---BY CHENCHEN---
            mainRow.add("priority", 0);

            // ***************end
            mainRow.add("gate_driver_name", gate_driver_name);
            mainRow.add("gate_driver_liscense", gate_driver_liscense);
            mainRow.add("company_name", company_name);
            mainRow.add("mc_dot", mc_dot);
            if (mark == 1) {
                long id = addAsset(gps_tracker, gate_liscense_plate, 0, "", "",
                    "");
                mainRow.add("gps_tracker", id);
            } else {
                if (gpsId > 0) {
                    DBRow updateRow = new DBRow();
                    updateRow.add("def", gate_liscense_plate);
                    floorCheckInMgrZwb.updateAsset(gpsId, updateRow);
                    mainRow.add("gps_tracker", gpsId);
                }
            }
            mainRow.add("ps_id", ps_id);
            mainRow.add("load_count", load_count);
            mainRow.add("gate_check_in_operate_time", DateUtil.NowStr());
            mainRow.add("gate_check_in_operator", createManId);
            mainRow.add("creator", createManId);
            mainRow.add("photo_gate_driver_liscense", StringUtil.convertStr2PhotoNumber(gate_driver_liscense)); // 张睿2015-01-15

            String remark = "";
            if (main_id == 0) {
                String create_time = DateUtil.NowStr();
                mainRow.add("create_time", create_time);
                mainRow.add("gate_check_in_time", create_time);
                main_id = floorCheckInMgrZwb.gateCheckInAdd(mainRow); // 添加
                // main表数据
                remark = "Add Driver Info";
            } else {
                floorCheckInMgrZwb.modCheckIn(main_id, mainRow);
                remark = "Update Driver Info";
            }

            // 记录日志
            String logData = "";
            logData += "Driver License: " + gate_driver_liscense + ",";
            logData += "Driver Name: " + gate_driver_name + ",";
            logData += "MC/DOT: " + mc_dot + ",";
            logData += "Carrier: " + company_name + ",";
            logData += "GPS: " + gps_tracker + ",";

            if (!logData.equals("")) {
                logData = logData.substring(0, logData.length() - 1);
            }
            addCheckInLog(createManId, remark, main_id, CheckInLogTypeKey.GATEIN, DateUtil.NowStr(), logData);

            return main_id;
        } catch (Exception e) {
            throw new SystemException(e, "gateCheckInAddData", log);
        }
    }

    public void gateCheckInAddEquipment(DBRow row, AdminLoginBean adminLoginBean, boolean isUpdate) throws Exception {

        String gate_container_no = row.get("gate_container_no", "")
            .toUpperCase();// 货柜号
        String gate_liscense_plate = row.get("liscense_plate", "")
            .toUpperCase();
        String in_seal = row.get("in_seal", "");
        int rel_type = row.get("rel_type", 0);
        int isLive = row.get("live", 0);
        long yc_id = row.get("yc_id", 0l);
        long doorId = row.get("checkInDoorId", 0l);
        long main_id = row.get("main_id", 0l);
        long detail_id = row.get("detail_id", 0l);
        long ps_id = adminLoginBean.getPs_id();
        String logTime = DateUtil.NowStr();
        if(!row.get("checkInTime", "").equals("")){
        	logTime = row.get("checkInTime", "");
        }
        String search_container_no = row.get("search_ctnr", ""); // 搜索的货柜号
        String add_checkin_number = row.get("add_checkin_number", "")
            .toUpperCase();
        String equipment_number = "";
        int equipment_type = 0;
        if (!StrUtil.isBlank(gate_container_no)) {
            equipment_number = gate_container_no;
            equipment_type = EquipmentTypeKey.Container;
        } else {
            equipment_number = gate_liscense_plate;
            equipment_type = EquipmentTypeKey.Tractor;
        }
        System.out
            .println("============= start selectPickUpCTNRByEntryId===============");
        DBRow[] pickCtnrRow = this.floorCheckInMgrZwb
            .selectPickUpCTNRByEntryId(main_id);
        System.out
            .println("=============finsh selectPickUpCTNRByEntryId===============");
        if (pickCtnrRow.length > 0) {
            DBRow equipRow = new DBRow();
            equipRow.add("check_out_entry_id", null);
            this.ymsMgrAPI.updateEquipment(
                pickCtnrRow[0].get("equipment_id", 0l), equipRow);
        }
// 添加设备
        String remark = "";
        System.out.println("============= start Equipments===============");
        if (isUpdate) {
            this.gateCheckInUpdateEquipment(main_id, gate_container_no, isLive,
                gate_liscense_plate, logTime,
                search_container_no, doorId, yc_id, in_seal, detail_id,
                rel_type, ps_id);
            remark = "Update Equipments";
        } else {
            this.addEquipment(main_id, gate_container_no, isLive,
                gate_liscense_plate, logTime,
                search_container_no, doorId, yc_id, in_seal, detail_id,
                rel_type, ps_id, false);
            remark = "Add Equipments";

        }
        System.out.println("============= end Equipments===============");
        CheckInMainDocumentsRelTypeKey checkInMainDocumentsRelTypeKey = new CheckInMainDocumentsRelTypeKey();
        CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
        String logData = "";
        logData += "Tractor" + ": " + gate_liscense_plate + ",";
        logData += "Trailer" + ": " + gate_container_no + ",";
        logData += checkInMainDocumentsRelTypeKey
            .getCheckInMainDocumentsRelTypeKeyValue(rel_type) + ",";
        String pickUpCtnr = "";
        if (isLive == CheckInLiveLoadOrDropOffKey.PICK_UP) {
            pickUpCtnr = "【" + search_container_no + "】";
        }
        logData += checkInLiveLoadOrDropOffKey
            .getCheckInLiveLoadOrDropOffKey(isLive) + pickUpCtnr + ",";
        if (yc_id != 0) {
            DBRow result = floorCheckInMgrZwb.findStorageYardControl(yc_id);
            logData += "Spot【" + result.get("yc_no", "") + "】,";
        }
        if (doorId != 0) {
            DBRow result = floorCheckInMgrZwb.findDoorById(doorId);
            logData += "Door【" + result.get("doorId", "") + "】,";
        }
        logData += "Delivery Seal" + ": " + in_seal + ",";

        if (!logData.equals("")) {
            logData = logData.substring(0, logData.length() - 1);
        }
        DBRow[] detailRow = floorCheckInMgrZwb.findloadingByInfoId(main_id);
        System.out.println("============= start Send Message===============");
        if (detailRow.length > 0) {
// 删除之前的通知从新发
            DBRow[] shRows = floorCheckInMgrZwb.selectSchedule(
                detailRow[0].get("dlo_detail_id", 0l),
                ProcessKey.GateNotifyWareHouse);
            if (shRows.length > 0 && shRows != null) {
                scheduleMgrZr.deleteScheduleByScheduleId(
                    shRows[0].get("schedule_id", 0l), adminLoginBean);
            }
            DBRow[] sshRows = floorCheckInMgrZwb.selectSchedule(
                detailRow[0].get("dlo_detail_id", 0l),
                ProcessKey.GateHasTaskNotifyWindow);
            if (sshRows.length > 0 && sshRows != null) {
                scheduleMgrZr.deleteScheduleByScheduleId(
                    sshRows[0].get("schedule_id", 0l), adminLoginBean);
            }

        }
        System.out.println("============= end Send Message===============");
        System.out
            .println("============= start  ProcessKey.GateNoTaskNotifyWindow===============");
        DBRow[] noTaskNotice = floorCheckInMgrZwb.selectSchedule(main_id,
            ProcessKey.GateNoTaskNotifyWindow);
        System.out
            .println("============= start  ProcessKey.deleteScheduleByScheduleId===============");
        if (noTaskNotice.length > 0 && noTaskNotice != null) {
            scheduleMgrZr.deleteScheduleByScheduleId(
                noTaskNotice[0].get("schedule_id", 0l), adminLoginBean);
        }
        System.out
            .println("============= start  gateCheckInAddNotice===============");
        if (StrUtil.isBlank(add_checkin_number)
            && rel_type != CheckInMainDocumentsRelTypeKey.VISITOR) {
// 发通知
            logData += this.gateCheckInAddNotice(main_id, 0, doorId, yc_id,
                adminLoginBean, add_checkin_number.toUpperCase(), 0,
                equipment_number, equipment_type);

        }
        System.out.println("============start addCheckInLog===============");
        addCheckInLog(adminLoginBean.getAdid(), remark, main_id,
            CheckInLogTypeKey.GATEIN, logTime, logData);
        System.out.println("============= end addCheckInLog===============");

    }

    public long gateCheckInAddTask(DBRow row, AdminLoginBean adminLoginBean,
                                   boolean isUpdate) throws Exception {

        long yc_id = row.get("yc_id", 0l);
        long doorId = row.get("checkInDoorId", 0l);
        String add_checkin_number = row.get("add_checkin_number", "")
            .toUpperCase();
        int number_type = row.get("number_type", 0);
        String company_id = row.get("company_id", "");
        String customer_id = row.get("customer_id", "");
        String account_id = row.get("account_id", "");
        String order_no = row.get("order_no", "");
        String po_no = row.get("po_no", "");
        String supplier_id = row.get("supplier_id", "");
        String staging_area_id = row.get("staging_area_id", "");
        String freight_term = row.get("freight_term", "");
        long ic_id = row.get("ic_id", 0l);
        String appointment_time = row.get("appointment_time", "");
        String create_time = DateUtil.NowStr(); // 当前时间
        String number_order_status = row.get("number_order_status", "");
        int order_system_type = row.get("order_system_type", 0);
        long receipt_no = row.get("receipt_no", 0l);
        long ps_id = adminLoginBean.getPs_id();
        long main_id = row.get("main_id", 0l);
        long detail_id = 0;
        DBRow rowDetail = new DBRow();
        String dataStr = "";
        String remark = "";
        String equipment_number = "";
        int equipment_type = 0;
        if (!("").equals(appointment_time)) {

            rowDetail.add("appointment_date",
                DateUtil.showUTCTime(appointment_time, ps_id));
            long diffTime = (DateUtil.getDate2LongTime(create_time) - DateUtil
                .getDate2LongTime(DateUtil.showUTCTime(appointment_time,
                    ps_id))) / 1000 / 60;
            if (diffTime >= 30) {
                rowDetail.add("come_status", CheckInOrderComeStatusKey.TooLate);
            } else if (diffTime <= -30) {
                rowDetail
                    .add("come_status", CheckInOrderComeStatusKey.TooEarly);
            } else {
                rowDetail.add("come_status", CheckInOrderComeStatusKey.OnTime);
            }
        } else {
            rowDetail.add("appointment_date", null);
            rowDetail.add("come_status",
                CheckInOrderComeStatusKey.NoAppointment);
        }
        DBRow[] detailRow = floorCheckInMgrZwb.findloadingByInfoId(main_id);
// if(detailRow.length>0){
// //删除之前的通知从新发
// DBRow[]
// shRows=floorCheckInMgrZwb.selectSchedule(detailRow[0].get("dlo_detail_id",
// 0l), ProcessKey.GateNotifyWareHouse);
// if(shRows.length>0 && shRows!=null){
// scheduleMgrZr.deleteScheduleByScheduleId(shRows[0].get("schedule_id",
// 0l), adminLoginBean);
// }
// DBRow[]
// sshRows=floorCheckInMgrZwb.selectSchedule(detailRow[0].get("dlo_detail_id",
// 0l), ProcessKey.GateHasTaskNotifyWindow);
// if(sshRows.length>0 && sshRows!=null){
// scheduleMgrZr.deleteScheduleByScheduleId(sshRows[0].get("schedule_id",
// 0l), adminLoginBean);
// }
//
// }
// DBRow[] noTaskNotice=floorCheckInMgrZwb.selectSchedule(main_id,
// ProcessKey.GateNoTaskNotifyWindow);
// if(noTaskNotice.length>0 && noTaskNotice!=null){
// scheduleMgrZr.deleteScheduleByScheduleId(noTaskNotice[0].get("schedule_id",
// 0l), adminLoginBean);
// }
        if (!StringUtil.isBlank(add_checkin_number)) {
            DBRow[] equipment = this.floorCheckInMgrZwb
                .selectTrailerByEntryId(main_id);
            if (equipment.length == 0) {
                equipment = this.floorCheckInMgrZwb
                    .selectTractorByEntryId(main_id);
            }
            if (equipment != null && equipment.length > 0) {
                rowDetail.add("equipment_id",
                    equipment[0].get("equipment_id", 0l));
                equipment_number = equipment[0].get("equipment_number", "");
                equipment_type = equipment[0].get("equipment_type", 0);

            }

            rowDetail.add("dlo_id", main_id);
            rowDetail.add("number", add_checkin_number.toUpperCase());
            rowDetail.add("number_status",
                CheckInChildDocumentsStatusTypeKey.UNPROCESS);
            rowDetail.add("company_id", company_id);
            rowDetail.add("customer_id", customer_id);
            rowDetail.add("account_id", account_id);
            rowDetail.add("supplier_id", supplier_id);
            rowDetail.add("po_no", po_no);
            rowDetail.add("order_no", order_no);
            rowDetail.add("number_type", number_type);
            rowDetail.add("staging_area_id", staging_area_id);
            rowDetail.add("ic_id", ic_id);
            rowDetail.add("receipt_no", receipt_no);
            if (number_type == ModuleKey.CHECK_IN && ic_id > 0) {
                DBRow importRow = new DBRow();
                importRow.add("status", ContainerImportStatusKey.RECEIVED);
                checkInMgrZyj.updateContainerInfoByIcid(ic_id, importRow);
            }
            rowDetail.add("freight_term", freight_term);

            if (isUpdate) {

                if (detailRow != null && detailRow.length > 0) { // 如果输入了单据号
// 之前有task
// 就更新
                    detail_id = detailRow[0].get("dlo_detail_id", 0l);
                    floorCheckInMgrZwb.updateDetail(
                        detailRow[0].get("dlo_detail_id", 0l), rowDetail);

// String dataStr=this.gateCheckInAddNotice(main_id,
// detailRow[0].get("dlo_detail_id", 0l), doorId, yc_id,
// adminLoginBean, add_checkin_number.toUpperCase(),
// number_type); //添加通知
// // data+=dataStr;
                    this.floorSpaceResourcesRelationMgr
                        .delSpaceResourcesRelation(
                            SpaceRelationTypeKey.Task, detail_id);
                    remark = "Update Task";
                } else { // 如果输入了单据号 之前没有task 就添加
                    rowDetail.add("dlo_id", main_id);
                    detail_id = floorCheckInMgrZwb
                        .addOccupancyDetails(rowDetail); // 添加task
                    remark = "Add Task";
                }

            } else {
                rowDetail.add("create_time", DateUtil.NowStr());
                rowDetail.add("creator", adminLoginBean.getAdid());
                detail_id = floorCheckInMgrZwb.addOccupancyDetails(rowDetail); // 添加task
                remark = "Add Task";

            }

// 发通知
            dataStr = this.gateCheckInAddNotice(main_id, detail_id, doorId,
                yc_id, adminLoginBean, add_checkin_number.toUpperCase(),
                number_type, equipment_number, equipment_type);

            if (doorId > 0 && !StringUtil.isBlank(add_checkin_number)) {
// 添加占用资源
                this.addResourcesRelation(OccupyTypeKey.DOOR, doorId,
                    SpaceRelationTypeKey.Task, detail_id,
                    OccupyStatusTypeKey.RESERVERED, main_id);
            }
            if (yc_id > 0 && !StringUtil.isBlank(add_checkin_number)) {
// 添加占用资源
                this.addResourcesRelation(OccupyTypeKey.SPOT, yc_id,
                    SpaceRelationTypeKey.Task, detail_id,
                    OccupyStatusTypeKey.OCUPIED, main_id);
            }
        } else {
            if (detailRow != null && detailRow.length > 0) {
                number_type = detailRow[0].get("number_type", 0);
                add_checkin_number = detailRow[0].get("number", "");
                this.floorSpaceResourcesRelationMgr
                    .delSpceResourcesByRelation_id(main_id,
                        ModuleKey.CHECK_IN, SpaceRelationTypeKey.Task); // 删除task
// 占用的资源关系
                floorCheckInMgrZwb.detOccupancyDetails(detailRow[0].get(
                    "dlo_detail_id", 0l)); // 删除task
                remark = "Delete Task";

            }
        }
        DBRow orderSystem = orderSystemMgr.addOrUpdateOrderSystems(detail_id,
            ps_id, number_order_status, order_system_type, adminLoginBean);
        if (null != orderSystem) {
            DBRow updateDetail = new DBRow();
            updateDetail.add("lr_id", orderSystem.get("lr_id", 0));
            floorCheckInMgrZwb.updateDetailByIsExist(detail_id, updateDetail);
        }

        if (!StringUtil.isBlank(add_checkin_number)) {
            String logData = "";
            logData += moduleKey.getModuleName(number_type) + ": "
                + add_checkin_number + ",";
            if (yc_id != 0) {
                DBRow result = floorCheckInMgrZwb.findStorageYardControl(yc_id);
                logData += "Spot【" + result.get("yc_no", "") + "】,";
            }
            if (doorId != 0) {
                DBRow result = floorCheckInMgrZwb.findDoorById(doorId);
                logData += "Door【" + result.get("doorId", "") + "】,";
            }

            logData += dataStr;
            addCheckInLog(adminLoginBean.getAdid(), remark, main_id,
                CheckInLogTypeKey.GATEIN, DateUtil.NowStr(), logData);
        }

        return detail_id;
    }

    /**
     * gatecheckin发通知功能 如果占用停车位 就给window的人发通知，如果占用的是门就给warehouse的人发通知 zwb
     *
     * @param detail_id
     * @param door_id
     * @param spot_id
     * @throws Exception
     */
    @Override
    public String gateCheckInAddNotice(long entry_id, long detail_id,
                                       long door_id, long spot_id, AdminLoginBean adminLoginBean,
                                       String number, int number_type, String equipment_number,
                                       int equipment_type) throws Exception {
        try {
            CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
            long window_group_id = 1000003; // window 的分组
            long warehouse_group_id = 1000002; // warehouse 的分组
            String data = "";
            long createManId = adminLoginBean.getAdid();
            long ps_id = adminLoginBean.getPs_id();
            CheckInMainDocumentsRelTypeKey checkInMainDocumentsRelTypeKey = new CheckInMainDocumentsRelTypeKey();
// 如果停在停车位 就给window 的人发通知 如果是停在了门上就给warehouse 的 发通知
            String context = "No Data";
            DBRow[] equips = this.floorCheckInMgrZwb
                .findEquipmentByDloId(entry_id);
            DBRow[] rows = null;
            int key = 0;
            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                .getBeanFromContainer("proxyCheckInMgrZr");
            if (door_id != 0 || number_type == ModuleKey.SMALL_PARCEL) { // 如果是samll_parcel的也给warehouse的人发通知
// wfh
// 2015-4-29

                DBRow doorRow = this.floorCheckInMgrZwb.findDoorById(door_id);
                DBRow spotRow = this.floorCheckInMgrZwb
                    .findStorageYardControl(spot_id);
                String space = door_id > 0 ? doorRow.getString("doorId")
                    : spotRow.getString("yc_no"); // 因为如果是smallParcel
// 类型的不管是占用的车位还是门都会发给WHSE通知
                if (number_type > 0) {
                    rows = checkInMgrZr.getPresenceOfSuperior(ps_id,
                        warehouse_group_id);
                    key = ProcessKey.GateNotifyWareHouse; // ↓↓ 因为如果是smallParcel
// 类型的不管是占用的车位还是门都会发给WHSE通知
                    context = "【"
                        + moduleKey.getModuleName(number_type)
                        + "："
                        + number
                        + "】 To "
                        + (door_id > 0 ? "Door" : "Spot")
                        + "【"
                        + space
                        + "】 In 【"
                        + checkInTractorOrTrailerTypeKey
                        .getContainerTypeKeyValue(equipment_type)
                        + " : " + equipment_number + "】";
                } else {
                    rows = checkInMgrZr.getPresenceOfSuperior(ps_id,
                        window_group_id);
                    key = ProcessKey.GateNoTaskNotifyWindow;
                    context = " No Task To Door【"
                        + doorRow.getString("doorId")
                        + "】 In 【"
                        + checkInTractorOrTrailerTypeKey
                        .getContainerTypeKeyValue(equipment_type)
                        + " : " + equipment_number + "】";

                }
            } else if (spot_id != 0) {
                rows = checkInMgrZr.getPresenceOfSuperior(ps_id,
                    window_group_id);
                DBRow spotRow = this.floorCheckInMgrZwb
                    .findStorageYardControl(spot_id);
                if (number_type > 0) {
                    key = ProcessKey.GateHasTaskNotifyWindow;
                    context = "【"
                        + moduleKey.getModuleName(number_type)
                        + "："
                        + number
                        + "】 To Spot【"
                        + spotRow.getString("yc_no")
                        + "】In 【"
                        + checkInTractorOrTrailerTypeKey
                        .getContainerTypeKeyValue(equipment_type)
                        + " : " + equipment_number + "】";
                } else {
                    key = ProcessKey.GateNoTaskNotifyWindow;
                    context = " No Task To Spot【"
                        + spotRow.getString("yc_no")
                        + "】In 【"
                        + checkInTractorOrTrailerTypeKey
                        .getContainerTypeKeyValue(equipment_type)
                        + " : " + equipment_number + "】";

                }
            } else {
                rows = checkInMgrZr.getPresenceOfSuperior(ps_id,
                    window_group_id);
                key = ProcessKey.GateNoTaskNotifyWindow;
                context = "EntryID【"
                    + entry_id
                    + "】"
                    + checkInMainDocumentsRelTypeKey
                    .getCheckInMainDocumentsRelTypeKeyValue(equips[0]
                        .get("rel_type", 0));
            }
            String ids = "";
            String names = "";
            if (rows != null && rows.length > 0) {
                for (int i = 0; i < rows.length; i++) {
                    if (i == rows.length - 1) {
                        ids += rows[i].get("adid", 0l);
                        names += rows[i].getString("employe_name");
                    } else {
                        ids += rows[i].get("adid", 0l) + ",";
                        names += rows[i].getString("employe_name") + "、";
                    }
                }
            }

            if (!StringUtil.isNull(ids)) { // 发通知
                if (number_type > 0) {
                    scheduleMgrZr.addSchedule(ScheduleModel
                        .getCheckInScheduleModel(entry_id, detail_id, key,
                            createManId, ids, context, number,
                            number_type));

                } else {
                    scheduleMgrZr.addSchedule(ScheduleModel
                        .getCheckInNotifyWindowScheduleModel(entry_id, key,
                            createManId, ids, context));

                }
                data += ",Noticed: " + names;
            }
            return data;
        } catch (Exception e) {
            throw new SystemException(e, "gateCheckInAddNotice", log);
        }
    }

/**
 * gatecheckin 更新方法
 *
 * @param row
 * @param request
 * @param data
 * @throws Exception
 *
 *             public void gateCheckInUpdateData(HttpServletRequest
 *             request,DBRow row,AdminLoginBean adminLoginBean,boolean
 *             isAndroid)throws Exception{ try{
 *
 *             long createManId = adminLoginBean.getAdid(); int isLive =
 *             StringUtil.getInt(request, "live"); String
 *             gate_liscense_plate = StringUtil.getString(request,
 *             "liscense_plate").toUpperCase(); String gate_container_no =
 *             StringUtil.getString(request,
 *             "gate_container_no").toUpperCase();//货柜号 String create_time =
 *             DateUtil.NowStr(); long yc_id = StringUtil.getLong(request,
 *             "yc_id"); long ps_id = StringUtil.getLong(request,
 *             "add_ps_id"); long
 *             doorId=StringUtil.getLong(request,"checkInDoorId"); String
 *             add_checkin_number = StringUtil.getString(request,
 *             "add_checkin_number").toUpperCase(); // long status =
 *             StringUtil.getLong(request, "status"); long main_id =
 *             StringUtil.getLong(request, "main_id"); int rel_type =
 *             StringUtil.getInt(request, "rel_type"); int number_type =
 *             StringUtil.getInt(request, "number_type"); String
 *             phone_gate_liscense_plate
 *             =StringUtil.convertStr2PhotoNumber(gate_liscense_plate); //
 *             String
 *             phone_gate_container_no=StringUtil.convertStr2PhotoNumber
 *             (gate_container_no); String company_id =
 *             StringUtil.getString(request, "company_id"); String
 *             customer_id = StringUtil.getString(request, "customer_id");
 *             String account_id = StringUtil.getString(request,
 *             "account_id"); String order_no =
 *             StringUtil.getString(request, "order_no"); String po_no =
 *             StringUtil.getString(request, "po_no"); String supplier_id =
 *             StringUtil.getString(request, "supplier_id"); String
 *             staging_area_id = StringUtil.getString(request,
 *             "staging_area_id"); String freight_term =
 *             StringUtil.getString(request, "freight_term"); long ic_id =
 *             StringUtil.getLong(request, "ic_id"); // long spotZoneId =
 *             StringUtil.getLong(request, "spotZoneId"); String
 *             search_container_no
 *             =StringUtil.getString(request,"search_ctnr"); //搜索的货柜号 String
 *             in_seal = StringUtil.getString(request, "in_seal"); String
 *             appointment_time = StringUtil.getString(request,
 *             "appointment_time"); String number_order_status =
 *             StringUtil.getString(request,"number_order_status"); int
 *             order_system_type =
 *             StringUtil.getInt(request,"order_system_type"); long
 *             receipt_no = StringUtil.getLong(request, "receipt_no"); //
 *             if(isAndroid){ //android 提交 //
 *             number_type=androidRow.get("number_type", 0); //
 *             yc_id=androidRow.get("yc_id", 0l); //
 *             doorId=androidRow.get("door_id", 0l); //
 *             appointment_time=androidRow.get("appointmentdate", ""); //
 *             company_id = StringUtil.getString(request, "companyid"); //
 *             customer_id = StringUtil.getString(request, "customerid"); //
 *             account_id = StringUtil.getString(request, "accountid"); // }
 *             DBRow upDetailRow=new DBRow(); DBRow
 *             mainRow=floorCheckInMgrZwb.findGateCheckInById(main_id);
 *             String gate_check_in_time =
 *             mainRow.getString("gate_check_in_time");
 *             if(!("").equals(appointment_time)){
 *             upDetailRow.add("appointment_date",
 *             DateUtil.showUTCTime(appointment_time, ps_id)); long diffTime
 *             = (DateUtil.getDate2LongTime(gate_check_in_time)-DateUtil.
 *             getDate2LongTime(DateUtil.showUTCTime(appointment_time,
 *             ps_id)))/1000/60; if(diffTime >= 60){
 *             upDetailRow.add("come_status"
 *             ,CheckInOrderComeStatusKey.TooLate); }else if(diffTime <=
 *             -120){
 *             upDetailRow.add("come_status",CheckInOrderComeStatusKey
 *             .TooEarly); }else{
 *             upDetailRow.add("come_status",CheckInOrderComeStatusKey
 *             .OnTime); } }else{ upDetailRow.add("appointment_date", null);
 *             upDetailRow
 *             .add("come_status",CheckInOrderComeStatusKey.NoAppointment);
 *             }
 *
 *             floorCheckInMgrZwb.modCheckIn(main_id,row); //修改主单据
 *
 *             DBRow[] detailRow=
 *             floorCheckInMgrZwb.findloadingByInfoId(main_id); long
 *             return_detail_id=0; //更新task 关联设备用
 *             if(!StringUtil.isBlank(add_checkin_number)){ //如果修改时输入就修改
 *             没输入说明没有task 就删除 upDetailRow.add("number",
 *             add_checkin_number.toUpperCase());
 *             upDetailRow.add("number_status"
 *             ,CheckInChildDocumentsStatusTypeKey.UNPROCESS);
 *             upDetailRow.add("company_id",company_id);
 *             upDetailRow.add("customer_id",customer_id);
 *             upDetailRow.add("account_id",account_id);
 *             upDetailRow.add("po_no",po_no);
 *             upDetailRow.add("order_no",order_no);
 *             upDetailRow.add("number_type",number_type);
 *             upDetailRow.add("supplier_id",supplier_id);
 *             upDetailRow.add("staging_area_id", staging_area_id);
 *             upDetailRow.add("ic_id", ic_id);
 *             upDetailRow.add("freight_term", freight_term);
 *             upDetailRow.add("receipt_no", receipt_no); if(detailRow!=null
 *             && detailRow.length>0){ //如果输入了单据号 之前有task 就更新
 *             return_detail_id=detailRow[0].get("dlo_detail_id", 0l);
 *             floorCheckInMgrZwb
 *             .updateDetail(detailRow[0].get("dlo_detail_id", 0l),
 *             upDetailRow); //删除之前的通知从新发 DBRow[]
 *             shRows=floorCheckInMgrZwb.selectSchedule
 *             (detailRow[0].get("dlo_detail_id", 0l),
 *             ProcessKey.CHECK_IN_WINDOW); if(shRows.length>0 &&
 *             shRows!=null){
 *             scheduleMgrZr.deleteScheduleByScheduleId(shRows
 *             [0].get("schedule_id", 0l), request); } DBRow[]
 *             sshRows=floorCheckInMgrZwb
 *             .selectSchedule(detailRow[0].get("dlo_detail_id", 0l),
 *             ProcessKey.GateHasTaskNotifyWindow); if(sshRows.length>0 &&
 *             sshRows!=null){
 *             scheduleMgrZr.deleteScheduleByScheduleId(sshRows
 *             [0].get("schedule_id", 0l), request); } String
 *             dataStr=this.gateCheckInAddNotice(main_id,
 *             detailRow[0].get("dlo_detail_id", 0l), doorId, yc_id,
 *             adminLoginBean, add_checkin_number.toUpperCase(),
 *             number_type); //添加通知 // data+=dataStr; //
 *             this.updateResourcesRelation(doorId, yc_id, main_id,
 *             detailRow[0].get("dlo_detail_id", 0l),
 *             SpaceRelationTypeKey.Task,request); //修改task 占用资源 }else{
 *             //如果输入了单据号 之前没有task 就添加 upDetailRow.add("dlo_id", main_id);
 *             long
 *             returnId=floorCheckInMgrZwb.addOccupancyDetails(upDetailRow);
 *             //添加task //添加通知 String
 *             dataStr=this.gateCheckInAddNotice(main_id, returnId, doorId,
 *             yc_id, adminLoginBean, add_checkin_number.toUpperCase(),
 *             number_type); // data+=dataStr; return_detail_id=returnId;
 *             //添加task 占用资源 if(doorId >0){ //添加占用资源
 *             this.addResourcesRelation(OccupyTypeKey.DOOR, doorId,
 *             SpaceRelationTypeKey.Task, returnId,
 *             OccupyStatusTypeKey.RESERVERED,main_id); } if(yc_id > 0){
 *             //添加占用资源 this.addResourcesRelation(OccupyTypeKey.SPOT, yc_id,
 *             SpaceRelationTypeKey.Task, returnId,
 *             OccupyStatusTypeKey.OCUPIED,main_id); } } }else{
 *             if(detailRow!=null && detailRow.length>0){
 *             this.floorSpaceResourcesRelationMgr
 *             .delSpceResourcesByRelation_id
 *             (main_id,ModuleKey.CHECK_IN,SpaceRelationTypeKey.Task);
 *             //删除task 占用的资源关系
 *             floorCheckInMgrZwb.detOccupancyDetails(detailRow
 *             [0].get("dlo_detail_id", 0l)); //删除task } } DBRow orderSystem
 *             = orderSystemMgr.addOrUpdateOrderSystems(return_detail_id,
 *             adminLoginBean.getAdid(), number_order_status,
 *             order_system_type, adminLoginBean); if(null!=orderSystem) {
 *             DBRow updateDetail = new DBRow(); updateDetail.add("lr_id",
 *             orderSystem.get("lr_id",0));
 *             floorCheckInMgrZwb.updateDetailByIsExist
 *             (return_detail_id,updateDetail); } DBRow[] pickCtnrRow =
 *             this.floorCheckInMgrZwb.selectPickUpCTNRByEntryId(main_id);
 *             if(pickCtnrRow.length > 0 ){ DBRow equipRow = new DBRow();
 *             equipRow.add("check_out_entry_id", null);
 *             this.ymsMgrAPI.updateEquipment
 *             (pickCtnrRow[0].get("equipment_id", 0l), equipRow); }
 *
 *             //添加日志 // addCheckInLog(createManId,
 *             "Updated Entry ID【"+main_id+"】   driver info", main_id,
 *             CheckInLogTypeKey.GATEIN,create_time,data);
 *
 *             //修改设备 this.gateCheckInUpdateEquipment(main_id,
 *             gate_container_no, isLive, phone_gate_liscense_plate,
 *             create_time, search_container_no, doorId, yc_id,
 *             in_seal,return_detail_id,rel_type,ps_id);
 *
 *             // windowCheckIncheckTaskTypeAndTaskRepeat(main_id, ps_id,
 *             detailRow); //不知道对错？？？？？
 *
 *             //添加gate_check_in索引 editCheckInIndex(main_id, "update");
 *             addFiles(request,main_id,createManId); }catch(Exception e){
 *             throw new SystemException(e,"gateCheckInUpdateData",log); } }
 */

    /**
     * 将request转换成DBRow
     */

    @Override
    public DBRow gateCheckInAddDBRow(HttpServletRequest request) throws Exception {
        // 获得创建人
        AdminMgr adminMgr = (AdminMgr) MvcUtil.getBeanFromContainer("adminMgr");
        AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
        int isLive = StringUtil.getInt(request, "live");
        String in_seal = StringUtil.getString(request, "in_seal");
        String gate_driver_name = StringUtil.getString(request, "driver_name").toUpperCase();
        String gate_liscense_plate = StringUtil.getString(request, "liscense_plate").toUpperCase();
        String appointment_time = StringUtil.getString(request, "appointment_time");
        String gate_driver_liscense = StringUtil.getString(request, "driver_liscense").toUpperCase();
        String gate_container_no = StringUtil.getString(request, "gate_container_no").toUpperCase();// 货柜号
        String company_name = StringUtil.getString(request, "company_name").toUpperCase();// 运输公司名
        String gps_tracker = StringUtil.getString(request, "gps_tracker");
        long yc_id = StringUtil.getLong(request, "yc_id");
        long doorId = StringUtil.getLong(request, "checkInDoorId");
        String add_checkin_number = StringUtil.getString(request, "add_checkin_number").toUpperCase();
        long status = StringUtil.getLong(request, "status");
        long rel_type = StringUtil.getLong(request, "rel_type");
        long zone_id = StringUtil.getLong(request, "zone_id");
        long mark = StringUtil.getLong(request, "mark");
        long gpsId = StringUtil.getLong(request, "gpsId");
        String ctnr = StringUtil.getString(request, "ctnr").toUpperCase();
        long main_id = StringUtil.getLong(request, "main_id");
        String mc_dot = StringUtil.getString(request, "mc_dot").toUpperCase();
        int number_type = StringUtil.getInt(request, "number_type");
        int load_count = StringUtil.getInt(request, "load_count");
        String company_id = StringUtil.getString(request, "company_id");
        String customer_id = StringUtil.getString(request, "customer_id");
        String account_id = StringUtil.getString(request, "account_id");
        String order_no = StringUtil.getString(request, "order_no");
        String po_no = StringUtil.getString(request, "po_no");
        String supplier_id = StringUtil.getString(request, "supplier_id");
        String staging_area_id = StringUtil.getString(request, "staging_area_id");
        String freight_term = StringUtil.getString(request, "freight_term");
        long ic_id = StringUtil.getLong(request, "ic_id");
        long spotZoneId = StringUtil.getLong(request, "spotZoneId");
        String search_container_no = StringUtil.getString(request, "search_ctnr");
        String number_order_status = StringUtil.getString(request, "number_order_status");
        int order_system_type = StringUtil.getInt(request, "order_system_type");
        long receipt_no = StringUtil.getLong(request, "receipt_no");
        DBRow row = new DBRow();
        row.add("live", isLive);
        row.add("in_seal", in_seal);
        row.add("driver_name", gate_driver_name);
        row.add("liscense_plate", gate_liscense_plate);
        row.add("appointment_time", appointment_time);
        row.add("driver_liscense", gate_driver_liscense);
        row.add("gate_container_no", gate_container_no);
        row.add("company_name", company_name);
        row.add("gps_tracker", gps_tracker);
        row.add("yc_id", yc_id);
        row.add("checkInDoorId", doorId);
        row.add("add_checkin_number", add_checkin_number);
        row.add("status", status);
        row.add("rel_type", rel_type);
        row.add("zone_id", zone_id);
        row.add("mark", mark);
        row.add("gpsId", gpsId);
        row.add("main_id", main_id);
        row.add("mc_dot", mc_dot);
        row.add("number_type", number_type);
        row.add("load_count", load_count);
        row.add("company_id", company_id);
        row.add("customer_id", customer_id);
        row.add("account_id", account_id);
        row.add("po_no", po_no);
        row.add("order_no", order_no);
        row.add("supplier_id", supplier_id);
        row.add("staging_area_id", staging_area_id);
        row.add("freight_term", freight_term);
        row.add("ic_id", ic_id);
        row.add("spotZoneId", spotZoneId);
        row.add("search_ctnr", search_container_no);
        row.add("number_order_status", number_order_status);
        row.add("order_system_type", order_system_type);
        row.add("receipt_no", receipt_no);
        row.add("creatFlag", 1); // web 在jsp中以检查是否是标准的海运集装箱号码所以这里传1 即不在这里检查
        return this.gateCheckInAdd(request, row, adminLoginBean, false);
    }
    

  /*  public void ymsInnerGateCheckIn(String ymsEntryId) throws Exception {
    	System.out.println("===============begin inner check in :"+ymsEntryId+"====================");
    	String sessionId = PdfUtil.login(LOGIN_URL, "SystemNotify", "123456");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("yms_entry_id", ymsEntryId);
		PdfUtil.SentPostRequest(CHECK_IN_URL, map, sessionId);
    }*/
    
    
   /* public DBRow ymsGateCheckInAdd(HttpServletRequest request) throws Exception {
    	String yms_entry_id = StringUtil.getString(request, "yms_entry_id");
    	EntryTicketCheck ticketCheck =  getYmsEntryTicket(yms_entry_id);
		if (ticketCheck != null) {
			return  this.ymsGateCheckInAdd(request, ticketCheck,true);
		}
    	return null;
    }*/
    
    public DBRow ymsGateCheckInAdd(HttpServletRequest request,EntryTicketCheck entryTicketCheck) throws Exception {
    	AdminMgr adminMgr = (AdminMgr) MvcUtil.getBeanFromContainer("adminMgr");
        AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
        
        DBRow entryTicket = floorCheckInMgrZwb.getYmsCheckinRequest(entryTicketCheck.entryId, com.cwc.app.api.Warehouse.getWareHourseId(entryTicketCheck.warehouse), "checkout", "pending");
        if(entryTicket != null  ){
        	return null;
        }
        
        long ps_id = Warehouse.getWareHourseId(entryTicketCheck.warehouse);
        if(ps_id != adminLoginBean.getPs_id() ){
        	return null;
        }
        
        entryTicket = floorCheckInMgrZwb.getEntryByYmsId(entryTicketCheck.entryId,com.cwc.app.api.Warehouse.getWareHourseId(entryTicketCheck.warehouse));
        if(entryTicket != null){
        	return entryTicket;
        }
       
        DBRow spot = floorCheckInMgrWfh.findSpotByNo(ps_id,entryTicketCheck.spotName);
        DBRow door = floorCheckInMgrWfh.findDoorByDoorId(ps_id,entryTicketCheck.dockName);
        
        DBRow row = new DBRow();
        row.add("yms_entry_id", entryTicketCheck.entryId);
        row.add("in_seal", entryTicketCheck.seal);
        row.add("driver_name", entryTicketCheck.driverName);
        row.add("liscense_plate", entryTicketCheck.tractor);
        row.add("mc_dot", entryTicketCheck.mcDot);
        row.add("company_name", entryTicketCheck.carrierName);
        row.add("driver_liscense", entryTicketCheck.driverLicense);
        row.add("checkInTime", entryTicketCheck.checkInTime);
        if(entryTicketCheck.containerNOs!=null && entryTicketCheck.containerNOs.size()>0){
        	row.add("gate_container_no", entryTicketCheck.containerNOs.get(0));
        }
        row.add("status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
        row.add("photoId", entryTicketCheck.photoId);
        row.add("photoIds", entryTicketCheck.photoIds);
        row.add("rel_type", entryTicketCheck.actionType.getRelType());
        row.add("company_id", entryTicketCheck.companyName);
        row.add("customer_id", entryTicketCheck.customerName);
        
        if(door!=null){
        	row.add("checkInDoorId", door.get("sd_id",0));
            row.add("zone_id", door.get("area_id",0));
        }
        if(spot!=null){
        	 row.add("yc_id", spot.get("yc_id",0));
             row.add("spotZoneId",  spot.get("area_id",0));
        }
        row.add("creatFlag", 1); // web 在jsp中以检查是否是标准的海运集装箱号码所以这里传1 即不在这里检查
        row.add("ps_id", Warehouse.getWareHourseId(entryTicketCheck.warehouse));
        return this.ymsGateCheckInAdd(request, row, adminLoginBean);
    }
    
    public void batchCheckOut() throws Exception{
    	List<GateCheckoutInfo> list = getYmsCheckOutRequests("pending");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				try {
					ymsCheckOut(list.get(i));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
    }
    
    public void ymsCheckOut( GateCheckoutInfo checkOutInfo) throws SystemException {
        try {
            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");
            long adid = 0L;
            System.out.println("========warehouse :"+ checkOutInfo.warehouse +"==============");
            long psId = com.cwc.app.api.Warehouse.getWareHourseId(checkOutInfo.warehouse);
            System.out.println("========psId :"+ psId +"==============");
            DBRow entry = floorCheckInMgrZwb.getEntryByYmsId(checkOutInfo.entryId,psId);
            if(entry == null){
            	System.out.println("========"+checkOutInfo.entryId+": donse not existed!!==============");
            	return ;
            }
            System.out.println("========begin check out :"+checkOutInfo.entryId+"==============");
			long ps_id = entry.get("ps_id", 0L);
			long entry_id = entry.get("dlo_id", 0L);
      
            String allLog = "";
            boolean val = true;
			// 查到entry进来时所有的车尾
            DBRow[] equipments = floorCheckInMgrWfh.findTractorByEntryId(entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
            DBRow[] tractors = floorCheckInMgrWfh.findTractorByEntryId(entry_id, CheckInTractorOrTrailerTypeKey.TRACTOR);
            int tractor_requipment_purpose = 0;
            int trailer_requipment_purpose = 0;
            boolean check_out_this_flag = false; // 这次带走的是否有check_in进来的车尾
            String live_ids = "";
            String drop_ids = "";
            DBRow check_out_equipment = new DBRow();
			// 这次带走的是否有check_in进来的车尾
            
			for (int i = 0; i < checkOutInfo.equipments.size(); i++) {
				Equipment equipment = checkOutInfo.equipments.get(i);
				equipment.resource = this.findResourceByEquipment(ps_id,equipment.equipmentNo);
				if (equipment.equipmentType == CheckInTractorOrTrailerTypeKey.TRAILER && equipment.resource!=null) {
					equipment.equipmentId = equipment.resource.get("equipment_id", 0L);
					if(equipment.equipmentId == 0){
						checkOutInfo.equipments.remove(i);
						continue;
					}
				}
				
				DBRow row = floorCheckInMgrWfh.findTractorByEntryId(entry_id,checkOutInfo.equipments.get(i).equipmentNo);
				if (row == null) {
					continue;
				}
				equipment.equipmentId = row.get("equipment_id", 0L);
				equipment.checkInEntryId = row.get("check_in_entry_id", 0L);
				equipment.equipmentType = row.get("equipment_type", 0);
				
				if (equipment.equipmentType == CheckInTractorOrTrailerTypeKey.TRACTOR) {
					continue;
				}
				if (equipment.checkInEntryId == entry_id) {
					check_out_this_flag = true;
				}
				check_out_equipment.add(equipment.equipmentId + "",equipment.checkInEntryId);
			}
			// 仅车头走了
            if (checkOutInfo.equipments.size() == 1) {
                tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.LIVE;
                if (tractors[0].get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.TMS) {
                    tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.TMS;
                }
				// 有车尾但没有带走
                if (equipments.length > 0){
                	trailer_requipment_purpose = CheckInLiveLoadOrDropOffKey.DROP;		
                }
            }
			// 带其他的车尾走了。
            else if (checkOutInfo.equipments.size() > 1) {
				// 带走的车尾中没有checkIn进来的车尾
                if (!check_out_this_flag) {
                    tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.PICK_UP;
					// checkIn的时候带进来车尾了
                    if (equipments.length > 0) {
                        trailer_requipment_purpose = CheckInLiveLoadOrDropOffKey.DROP;
                    }
                } else {
					// 带走的车尾有部分自己也有其它的
                    for (DBRow equipment : equipments) {
                        long equipment_id = equipment.get("equipment_id", 0L);
                        long check_in_entry_id = check_out_equipment.get(equipment_id + "", 0L);
                        if (check_in_entry_id != 0) {
                            if (equipment.get("equipment_purpose", 0) != CheckInLiveLoadOrDropOffKey.TMS) {
                                live_ids += equipment_id + ",";
                            }
                            check_out_equipment.remove("" + equipment_id);
                        } else {
                            drop_ids += equipment_id + ",";
                        }
                    }
                    if (check_out_equipment.size() > 0) {
                        tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.PICK_UP;
                    } else {
                        tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.LIVE;
                        if (tractors[0].get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.TMS) {
                            tractor_requipment_purpose = CheckInLiveLoadOrDropOffKey.TMS;
                        }
                    }
                }
            }
			// 更新设备的purpose值
            if (tractor_requipment_purpose > 0) {
                DBRow tractor_row = new DBRow();
                tractor_row.add("equipment_purpose", tractor_requipment_purpose);
                floorCheckInMgrWfh.updateEquipmentPurposeByEntryID(entry_id,CheckInTractorOrTrailerTypeKey.TRACTOR, tractor_row);
            }
            if (trailer_requipment_purpose > 0) {
                DBRow trailer_row = new DBRow();
                trailer_row.add("equipment_purpose", trailer_requipment_purpose);
                floorCheckInMgrWfh.updateEquipmentPurposeByEntryID(entry_id,CheckInTractorOrTrailerTypeKey.TRAILER, trailer_row);
            }
            if (live_ids.length() > 0) {
                live_ids = live_ids.substring(0, live_ids.length() - 1);
                DBRow trailer_row = new DBRow();
                trailer_row.add("equipment_purpose",CheckInLiveLoadOrDropOffKey.LIVE);
                floorCheckInMgrWfh.updateEquipmentPurposeByEquipmentID(live_ids, CheckInTractorOrTrailerTypeKey.TRAILER,trailer_row);

            }
            if (drop_ids.length() > 0) {
                drop_ids = drop_ids.substring(0, drop_ids.length() - 1);
                DBRow trailer_row = new DBRow();
                trailer_row.add("equipment_purpose",CheckInLiveLoadOrDropOffKey.DROP);
                floorCheckInMgrWfh.updateEquipmentPurposeByEquipmentID(drop_ids, CheckInTractorOrTrailerTypeKey.TRAILER,trailer_row);
            }
			// 遍历jsonArray
            for (int i = 0; i < checkOutInfo.equipments.size(); i++) {
            	Equipment equipment = checkOutInfo.equipments.get(i);
            	if (equipment.equipmentType != CheckInTractorOrTrailerTypeKey.TRACTOR &&  equipment.equipmentType != CheckInTractorOrTrailerTypeKey.TRAILER) {
					continue;
				}
                //net.sf.json.JSONObject jsonObj = jsonArray.getJSONObject(i);
                long equipment_id = equipment.equipmentId; // 设备ID
                long equipment_type = equipment.equipmentType;
                long check_in_entry_id = equipment.checkInEntryId;
                String resources_type = equipment.resource.getString("resources_type");
                String equipment_no = equipment.equipmentNo;
                String location = equipment.resource.getString("location");
                String seal = equipment.sealNo;
              
                String levelHead = "";
                if (check_in_entry_id != entry_id && equipment_type == CheckInTractorOrTrailerTypeKey.TRAILER) {
                    levelHead += "Pick up ";
                }
                levelHead += (equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) ? "Tractor": "Trailer";
				// 当dropoff的时候，递交到后台一条记录，即为 车头 因其未占用资源，所以标记为No-Tractor-left
                if (checkOutInfo.equipments.size() == 1
                    && equipment_type == CheckInTractorOrTrailerTypeKey.TRACTOR) {
                    String endPoint = "";
                    if ("".equals(location) || "Task".equals(location)) {
                        endPoint = ",";
                    }
                    String dropLog = levelHead + "【" + equipment_no + "】Left"+ endPoint;
                    if (!"".equals(location) && !"Task".equals(location)) {
                        dropLog += "-Release:" + location.substring(0, 4) + "【"+ location.substring(5, location.length())+ "】" + ",";
                    }
                    checkOutTheLog("Check Out Equipment", dropLog, false,adid, check_in_entry_id,checkOutInfo.checkOutTime);
                    val = false;
                }
				// 带走别人的车尾 需要在其entry_id中记录日志
                else if (checkOutInfo.equipments.size() != 1 ) {
                    String anotherLog = "";
                    if (!"Task".equals(location)) {
                        if (!"".equals(seal)) {
                            anotherLog += "Seal【" + seal + "】,";
                        }
                        String endPoint = "";
                        if ("".equals(location)) {
                            endPoint = ",";
                        }
                        anotherLog += levelHead + "【" + equipment_no + "】Left" + endPoint;
                        if (!"".equals(location)) {
                            anotherLog += "-Release:"+ location.substring(0, 4) + "【" + location.substring(5, location.length()) + "】" + ",";
                        }

						// 调用记录日志方法 记录被带走车尾的entry_id中
                        checkOutTheLog("Check Out Equipment", anotherLog,false, adid, check_in_entry_id,checkOutInfo.checkOutTime);
                        val = true;
                    }
                }
                if (!"Task".equals(location)) {
                    if (!"".equals(seal)) {
                        allLog += "Seal【" + seal + "】,";
                    }
                    String endPoint = "";
                    if ("".equals(location)) {
                        endPoint = ",";
                    }
                    allLog += levelHead + "【" + equipment_no + "】Left" + endPoint;
                    if (!"".equals(location)) {
                        allLog += "-Release:" + location.substring(0, 4) + "【"+ location.substring(5, location.length()) + "】" + ",";
                    }
                }

				// 第一步
				// 更新entry表的check_out_time和设备表的check_out_time，如果资源为door，则记录check_out_warehouse_time
                
                checkOutTime(equipment_id, adid, entry_id, resources_type,equipment_type,checkOutInfo.checkOutTime);
				// 第二步 释放资源记回task表
                checkOutReturnTheTask(equipment_id);
				// 第三步
				// 查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true
                checkOutTaskStatus(false, adid, 0,check_in_entry_id, entry_id, equipment_id);
				// 第四步 调用詹洁的释放资源方法
                checkOutByZj(equipment_id, adid, entry_id,checkOutInfo.checkOutTime);
				// 第五步 根据equipment_id记录seal
                checkOutTheSeal(equipment_id, seal);

				// 清除与设备 相关的未完成的 schedule
                DBRow[] schedules = this.floorCheckInMgrZwb .getScheduleByEquipmentId(equipment_id);
                for (int index = 0, len = schedules.length; index < len; index++) {
                    checkInMgrZr.finishSchedule(schedules[index].get("schedule_id", 0l), adid);
                }
				// 如果设备没有关联任务,但是却发布了schedule给window,当设备全部离开时,清除任务
                if (isAllLeft(check_in_entry_id)) {
                    DBRow[] schedulesWithoutTask = this.floorCheckInMgrZwb .getNoTaskScheduleByEntryId(check_in_entry_id);
                    for (int index = 0, len = schedulesWithoutTask.length; index < len; index++) {
                        checkInMgrZr.finishSchedule(schedulesWithoutTask[index] .get("schedule_id", 0l), adid);
                    }
                }
            }

            if (val) {
			// 调用记录日志的方法
                checkOutTheLog("Check Out Equipment", allLog, false, adid,entry_id,checkOutInfo.checkOutTime);
            }
			// 处理图片
            //addFiles(request, entry_id, adid); // 照片

			// 更新索引
            editCheckInIndex(entry_id, "update");
            
            //close checkout request
            DBRow row = new DBRow();
            row.put("entryId", checkOutInfo.entryId);
            row.put("psId", psId);
            row.put("state", "closed");
            floorCheckInMgrZwb.closeYmsCheckOutRequest(row);
            
        } catch (Exception e) {
        	e.printStackTrace();
            throw new SystemException(e, "checkOut", log);

        }
    }




    // 得到海运集装中字母对应的数字
    private int changechar(String ch) {
        int return_value = -1000;
        switch (ch) {
            case "a":
                return_value = 10;
                break;
            case "A":
                return_value = 10;
                break;

            case "b":
                return_value = 12;
                break;
            case "B":
                return_value = 12;
                break;

            case "c":
                return_value = 13;
                break;
            case "C":
                return_value = 13;
                break;

            case "d":
                return_value = 14;
                break;
            case "D":
                return_value = 14;
                break;

            case "e":
                return_value = 15;
                break;
            case "E":
                return_value = 15;
                break;

            case "f":
                return_value = 16;
                break;
            case "F":
                return_value = 16;
                break;

            case "g":
                return_value = 17;
                break;
            case "G":
                return_value = 17;
                break;

            case "h":
                return_value = 18;
                break;
            case "H":
                return_value = 18;
                break;

            case "i":
                return_value = 19;
                break;
            case "I":
                return_value = 19;
                break;

            case "j":
                return_value = 20;
                break;
            case "J":
                return_value = 20;
                break;

            case "k":
                return_value = 21;
                break;
            case "K":
                return_value = 21;
                break;

            case "l":
                return_value = 23;
                break;
            case "L":
                return_value = 23;
                break;

            case "m":
                return_value = 24;
                break;
            case "M":
                return_value = 24;
                break;

            case "n":
                return_value = 25;
                break;
            case "N":
                return_value = 25;
                break;

            case "o":
                return_value = 26;
                break;
            case "O":
                return_value = 26;
                break;

            case "p":
                return_value = 27;
                break;
            case "P":
                return_value = 27;
                break;

            case "q":
                return_value = 28;
                break;
            case "Q":
                return_value = 28;
                break;

            case "r":
                return_value = 29;
                break;
            case "R":
                return_value = 29;
                break;

            case "s":
                return_value = 30;
                break;
            case "S":
                return_value = 30;
                break;

            case "t":
                return_value = 31;
                break;
            case "T":
                return_value = 31;
                break;

            case "u":
                return_value = 32;
                break;
            case "U":
                return_value = 32;
                break;

            case "v":
                return_value = 34;
                break;
            case "V":
                return_value = 34;
                break;

            case "w":
                return_value = 35;
                break;
            case "W":
                return_value = 35;
                break;

            case "x":
                return_value = 36;
                break;
            case "X":
                return_value = 36;
                break;

            case "y":
                return_value = 37;
                break;
            case "Y":
                return_value = 37;
                break;

            case "z":
                return_value = 38;
                break;
            case "Z":
                return_value = 38;
                break;
        }
        return return_value;
    }

    // 验证集装箱是否是海运集装箱
    private String getCntr(String ctnr) {
        String result = "success";
        int[] num = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // 初始化数组
        if (ctnr.length() != 11) {
            result = "请输入11位的集装箱号码。";
            return result;
        } else {
            if (!Pattern.matches("[A-Za-z]{4}\\d{7}$", ctnr)) {
                result = "号码错误，不是海运集装箱";
                return result;
            }
// 前四位字母部分
            num[0] = changechar(ctnr.substring(0, 1));
            num[1] = changechar(ctnr.substring(1, 2));
            num[2] = changechar(ctnr.substring(2, 3));
            num[3] = changechar(ctnr.substring(3, 4));

// 数字部分
            num[4] = Integer.parseInt(ctnr.substring(4, 5));
            num[5] = Integer.parseInt(ctnr.substring(5, 6));
            num[6] = Integer.parseInt(ctnr.substring(6, 7));
            num[7] = Integer.parseInt(ctnr.substring(7, 8));
            num[8] = Integer.parseInt(ctnr.substring(8, 9));
            num[9] = Integer.parseInt(ctnr.substring(9, 10));
// 最后一位验证
            num[10] = Integer.parseInt(ctnr.substring(10, 11));

            int sum = num[0] + num[1] * 2 + num[2] * 4 + num[3] * 8 + num[4]
                * 16 + num[5] * 32 + num[6] * 64 + num[7] * 128 + num[8]
                * 256 + num[9] * 512;
            int result1 = sum % 11;
            result1 = result1 % 10;
            if (result1 != num[10]) {
                result = "CTNR input is wrong,The last of the CTNR is "
                    + result1;
                return result;
            }
        }
        return result;
    }

    /**
     * 验证没有离开的 (车头 | 车尾) 相同的设备号 车头验证车头 车尾验证车尾
     *
     * @param data
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date 2015年1月22日
     */
    private HoldDoubleValue<Boolean, DBRow[]> validateSameNotLeftEquipment(
        DBRow data) throws Exception {
        try {
// System.out.println(StringUtil.convertDBRowsToJsonString(data));
// "liscense_plate": "ZHANTACTOR", 车头
// "gate_container_no": "ZHANTRAILER", 车尾
            String equipment_tractor = data.getString("liscense_plate");
            String equipment_trailer = data.getString("gate_container_no");

            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                .getBeanFromContainer("proxyCheckInMgrZr");

            DBRow[] tractor = null;
            DBRow[] trailers = null;
            Map<Long, List<DBRow>> returnMaps = new HashMap<Long, List<DBRow>>();

            if (!StringUtil.isBlank(equipment_tractor)) {
                tractor = checkInMgrZr.getEquipmentJoinResouces(
                    equipment_tractor, EquipmentTypeKey.Tractor);
            }
            if (!StringUtil.isBlank(equipment_trailer)) {
                trailers = checkInMgrZr.getEquipmentJoinResouces(
                    equipment_trailer, EquipmentTypeKey.Container);
            }
            if (tractor != null && tractor.length > 0) {
                for (DBRow temp : tractor) {
                    long entry_id = temp.get("entry_id", 0l);
                    List<DBRow> datas = returnMaps.get(entry_id);
                    datas = (datas == null) ? new ArrayList<DBRow>() : datas;
                    datas.add(temp);
                    returnMaps.put(entry_id, datas);
                }
            }

            if (trailers != null && trailers.length > 0) {
                for (DBRow temp : trailers) {
                    long entry_id = temp.get("entry_id", 0l);
                    List<DBRow> datas = returnMaps.get(entry_id);
                    datas = (datas == null) ? new ArrayList<DBRow>() : datas;
                    datas.add(temp);
                    returnMaps.put(entry_id, datas);
                }
            }
// 组织数据
            List<DBRow> returnArray = new ArrayList<DBRow>();
            Iterator<Entry<Long, List<DBRow>>> it = returnMaps.entrySet()
                .iterator();
            while (it.hasNext()) {
                Entry<Long, List<DBRow>> entry = it.next();
                DBRow temp = new DBRow();
                temp.add("entry_id", entry.getKey());
                temp.add("values", entry.getValue());
                returnArray.add(temp);
            }
// System.out.println(returnArray.size() + " ..... ");
            return new HoldDoubleValue<Boolean, DBRow[]>(
                (returnMaps.size() > 0), returnArray.toArray(new DBRow[0]));
        } catch (Exception e) {
            throw new SystemException(e, "validateSameNotLeftEquipment", log);
        }
    }

    /**
     * CheckOut 设备
     *
     * @param equipmentids
     * @throws Exception
     * @author zhangrui
     * @Date 2015年1月25日
     */
    private void checkOutEquipments(String equipmentids) throws Exception {
        try {

        } catch (Exception e) {
            throw new SystemException(e, "checkOutEquipments", log);
        }
    }

    /**
     * gatecheckin 添加数据 最后修改 zhouweibin
     */
    public final static int GateCheckInNeedCheckOutSameEquipment = 1; // 需要CheckOut相同的设备
    public final static int GateCheckInAddSuccess = 2; // 直接添加成功.
    private DBRow gateCheckMain;

    @Override
    public DBRow gateCheckInAdd(final HttpServletRequest request,
                                final DBRow row, final AdminLoginBean adminLoginBean,
                                final boolean isAndroid) throws Exception, RuntimeException {
        try {
            gateCheckMain = null;
            sqlServerTransactionTemplate
                .execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(
                        TransactionStatus txStat) {
                        PostMethod post = null;

                        try {
// System.out.println(StringUtil.convertDBRowsToJsonString(row));
                            long yc_id = row.get("yc_id", 0l);
                            long doorId = row.get("checkInDoorId", 0l);
                            int rel_type = row.get("rel_type", 0);
                            long ps_id = adminLoginBean.getPs_id();
// long rel_type = row.get("rel_type",0l);
                            long main_id = row.get("main_id", 0l);
                            String add_checkin_number = row.get(
                                "add_checkin_number", "").toUpperCase();
                            String company_name = row.get("company_name",
                                "").toUpperCase();
// String gate_liscense_plate =
// row.get("gate_liscense_plate","");
                            String gate_container_no = row.get(
                                "gate_container_no", "");
                            int creatFlag = row.get("creatFlag", 0); // 当验证海运集装出错时，继续做check
// in
// 需要传这个参数值为1
                            boolean isOcranContainer = false;
                            if (Pattern.matches("[A-Za-z]{4}\\d{7}$",
                                gate_container_no)) {
                                isOcranContainer = true;
                            }
// 验证ctnr是否为海运集装箱号码 是的话验证是否正确
                            if (Pattern.matches("[A-Za-z]{4}\\d{7}$",
                                gate_container_no) && creatFlag != 1) {
                                isOcranContainer = true;
                                String result = getCntr(gate_container_no);
                                if (!result.equals("success")) {
                                    throw new VerifyShippingCTNRException(
                                        result);
                                }
                            }
                            DBRow androidRow = null;
                            if (rel_type == CheckInMainDocumentsRelTypeKey.SMALL_PARCEL
                                && add_checkin_number.equals("")) {
                                add_checkin_number = "";
                                if (Pattern.matches("^UPS.*", company_name)) {
                                    add_checkin_number += "UPS";
                                } else if (Pattern.matches("^FEDEX.*",
                                    company_name)) {
                                    add_checkin_number += "FEDEX";
                                }

                                String nowTime = DateUtil.NowStr();
                                String year = DateUtil.getYear(nowTime)
                                    + "";
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss");
                                String month = DateUtil.getMonth(sdf
                                    .parse(nowTime)) + "";
                                String day = DateUtil.getMonthDay(nowTime)
                                    + "";
                                String hour = DateUtil.getDayHour(nowTime)
                                    + "";
                                String min = DateUtil.getMinute(sdf
                                    .parse(nowTime)) + "";
                                String sec = DateUtil.getSecond(nowTime)
                                    + "";
                                add_checkin_number += year.substring(2, 4)
                                    + (month.length() == 1 ? "0"
                                    + month : month)
                                    + (day.length() == 1 ? "0" + day
                                    : day)
                                    + (hour.length() == 1 ? "0" + hour
                                    : hour)
                                    + (min.length() == 1 ? "0" + min
                                    : min)
                                    + (sec.length() == 1 ? "0" + sec
                                    : sec);

                                row.add("number_type",
                                    ModuleKey.SMALL_PARCEL);
                                row.add("add_checkin_number",
                                    add_checkin_number);
                                row.add("order_system_type",
                                    OrderSystemTypeKey.SYNC);
                                row.add("num", 0);
// System.out.println(add_checkin_number);
                            }
                            if (isAndroid) { // android 提交
// zhangrui 2015-01-27
                                boolean needValidate = row.get(
                                    "needvalidate", 0) == 1;
                                if (needValidate) {
                                    HoldDoubleValue<Boolean, DBRow[]> same = validateSameNotLeftEquipment(row);
                                    boolean isValidate = same.a;
                                    if (isValidate) {
                                        gateCheckMain = new DBRow();
                                        gateCheckMain
                                            .add("flag",
                                                GateCheckInNeedCheckOutSameEquipment);
                                        gateCheckMain.add("datas", same.b);
                                        return;
                                    }
                                }
                                androidRow = androidAutoAssginResource(row,
                                    adminLoginBean);
                                if (androidRow != null) {
                                    row.add("appointment_time", androidRow
                                        .get("appointmentdate", ""));
                                    row.add("number_type", androidRow.get(
                                        "number_type", 0));
                                    row.add("company_id",
                                        androidRow.get("companyid", ""));
                                    row.add("customer_id", androidRow.get(
                                        "customerid", ""));
                                    row.add("supplier_id", androidRow.get(
                                        "supplier_id", ""));
                                    row.add("account_id",
                                        androidRow.get("accountid", ""));
                                    row.add("staging_area_id", androidRow
                                        .get("stagingareaid", ""));
                                    row.add("receipt_no",
                                        androidRow.get("receiptno", 0l));
                                    row.add("order_system_status",
                                        androidRow.get("status", ""));
                                    row.add("order_system_type", androidRow
                                        .get("system_type", 0));
                                    row.add("yc_id",
                                        androidRow.get("yc_id", 0l));
                                    row.add("checkInDoorId", androidRow
                                        .get("checkInDoorId", 0l));
                                    row.add("num", androidRow.get("num", 0));
                                }
                            }

                            int resources_type = 0;
                            long resources_id = 0l;
                            if (yc_id > 0) {
                                resources_type = OccupyTypeKey.SPOT;
                                resources_id = yc_id;
                            }
                            if (doorId > 0) {
                                resources_type = OccupyTypeKey.DOOR;
                                resources_id = doorId;
                            }
                            String resource_name = checkInMgrZyj
                                .windowCheckInHandleOccupyResourceName(
                                    resources_type, resources_id);

                            if (isAndroid
                                && androidRow.get("resource_id", 0l) > 0) {
                                if (!ymsMgrAPI.resourceJudgeCanUse(
                                    ModuleKey.CHECK_IN, main_id,
                                    androidRow.get("resource_type", 0),
                                    androidRow.get("resource_id", 0l))) {
                                    androidRow = androidAutoAssginResource(
                                        row, adminLoginBean);
                                    if (androidRow != null) {
                                        row.add("appointment_time",
                                            androidRow.get(
                                                "appointmentdate",
                                                ""));
                                        row.add("number_type", androidRow
                                            .get("number_type", 0));
                                        row.add("company_id", androidRow
                                            .get("companyid", ""));
                                        row.add("customer_id", androidRow
                                            .get("customerid", ""));
                                        row.add("supplier_id", androidRow
                                            .get("supplier_id", ""));
                                        row.add("account_id", androidRow
                                            .get("accountid", ""));
                                        row.add("staging_area_id",
                                            androidRow
                                                .get("stagingareaid",
                                                    ""));
                                        row.add("receipt_no", androidRow
                                            .get("receiptno", 0l));
                                        row.add("order_system_status",
                                            androidRow
                                                .get("status", ""));
                                        row.add("order_system_type",
                                            androidRow.get(
                                                "system_type", 0));
                                        row.add("yc_id",
                                            androidRow.get("yc_id", 0l));
                                        row.add("checkInDoorId", androidRow
                                            .get("checkInDoorId", 0l));
                                        row.add("num",
                                            androidRow.get("num", 0));
                                    }
                                }
                            }
                            if (!isAndroid && resources_id > 0) {
                                if (!ymsMgrAPI.resourceJudgeCanUse(
                                    ModuleKey.CHECK_IN, main_id,
                                    resources_type, resources_id))
                                    throw new ResourceHasUsedException(
                                        occupyTypeKey
                                            .getOccupyTypeKeyName(resources_type)
                                            + ":" + resource_name);
                            }

// 获得日志数据
// String
// data=getGateCheckInLogData(gate_liscense_plate,gate_container_no,
// company_name,gate_driver_liscense,mc_dot,gps_tracker,number_type,
// add_checkin_number,yc_id,doorId,isLive);
                            if ((CheckInMainDocumentsRelTypeKey.PICK_UP == row
                                .get("rel_type", 0))
                                && !StringUtil
                                .isBlank(add_checkin_number)) {
                                row.add("load_count", 1);
                            }
                            if (main_id == 0) {
                                main_id = gateCheckInAddData(request, row,
                                    adminLoginBean, isAndroid);
                                row.add("main_id", main_id);
                                gateCheckInAddEquipment(row,
                                    adminLoginBean, false);
// 添加gate_check_in索引
                                editCheckInIndex(main_id, "add");
                                if (row.get("num", 0) != -1
                                    && !StrUtil
                                    .isBlank(add_checkin_number)) {
                                    gateCheckInAddTask(row, adminLoginBean,
                                        false);
                                }

                            } else {
                                gateCheckInAddData(request, row,
                                    adminLoginBean, isAndroid);
                                System.out
                                    .println("==========update finished===========");
                                gateCheckInAddEquipment(row,
                                    adminLoginBean, true);
                                System.out
                                    .println("==========gateCheckInAddEquipment finished===========");
// 添加gate_check_in索引
                                editCheckInIndex(main_id, "update");
                                System.out
                                    .println("==========update index finished===========");
                                if (row.get("num", 0) != -1) {
                                    gateCheckInAddTask(row, adminLoginBean,
                                        true);
                                }
                                System.out
                                    .println("==========gateCheckInAddTask finished===========");

                            }
                            if (main_id > 0) {
                                gateCheckMain = new DBRow();
                                if (androidRow != null) {
                                    gateCheckMain.add("resource_name",
                                        androidRow.get("resource_name",
                                            ""));
                                    gateCheckMain
                                        .add("resource_type",
                                            occupyTypeKey
                                                .getOccupyTypeKeyName(androidRow
                                                    .get("resource_type",
                                                        0)));
                                    if (androidRow.get("num", 0) == -1) {
                                        gateCheckMain
                                            .add("prompt",
                                                "Found multiple data, please check in office");
                                    }
                                }
                                gateCheckMain.add("entry_id", main_id);
                            }
                            gateCheckMain
                                .add("flag", GateCheckInAddSuccess);
                            updateReceiptsByCtnOrBols(main_id);

// 将创建的信息 push到接口 tms
                            if (isOcranContainer) {
                                try {
                                    DBRow spot = new DBRow();
                                    if (yc_id > 0) {
                                        spot = floorCheckInMgrWfh .findSpotBySpotID(yc_id);
                                    }
                                    String time = DateUtil.NowStr();
                                    time = DateUtil.showLocalTime(time,
                                        ps_id);

                                    Md5 md5 = new Md5();
                                    HttpClient client = new HttpClient();

                                    DBRow data = new DBRow();
                                    post = new PostMethod(
                                        "http://tms.gofuze.io/api/containerInOutPush/");
                                    post.addRequestHeader(
                                        "user-agent",
                                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36");
                                    data.add("API_KEY", "arrest_1");
                                    data.add(
                                        "CTNR",
                                        row.get("gate_container_no", "")
                                            .toUpperCase());
                                    data.add(
                                        "WHS_ID",
                                        getTmsWarehouseId(adminLoginBean
                                            .getPs_id()));// "1000009"
                                    data.add("TYPE", "0");
                                    data.add("LP",
                                        row.get("liscense_plate", "")
                                            .toUpperCase());
                                    data.add("MC_DOC", row
                                        .get("mc_dot", "")
                                        .toUpperCase());
                                    data.add("CARRIER",
                                        row.get("company_name", "")
                                            .toUpperCase());
                                    data.add("DRIVER_NAME",
                                        row.get("driver_name", "")
                                            .toUpperCase());
                                    data.add("DRIVER_LICENSE",
                                        row.get("driver_liscense", "")
                                            .toUpperCase());
                                    data.add("SEAL", row.get("in_seal", ""));
                                    data.add("TIME", time);
                                    data.add(
                                        "CHECK_CODE",
                                        md5.getMD5ofStr(
                                            time + "arrest_1!")
                                            .toLowerCase());
                                    data.add("SPOT", spot.get("yc_no", ""));
                                    data.add("SOPT_AREA",
                                        spot.get("area_name", ""));
                                    String json = StringUtil
                                        .convertDBRowsToJsonStringKeyIsUpperCase(data);
                                    post.addParameter("data", json);
                                    System.out.println(json);
// push数据不管成功还是失败
                                    client.executeMethod(post);
                                    String body = post
                                        .getResponseBodyAsString();
                                    System.out
                                        .println("==========TMS Response:"
                                            + body
                                            + "==============");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out
                                    .print("========="
                                        + row.get(
                                        "gate_container_no",
                                        "").toUpperCase()
                                        + " is not a ocran container#==========");
                            }

                        } catch (ResourceHasUsedException e) {
                            throw new ResourceHasUsedException(e
                                .getMessage());
                        } catch (VerifyShippingCTNRException e) {
                            throw e;
                        } catch (Exception e2) {
                            throw new RuntimeException(e2);
                        } finally {
// 释放资源
                            if (post != null) {
                                post.releaseConnection();
                            }
                        }
                    }

                });
            return gateCheckMain;

        } catch (ResourceHasUsedException e) {
            throw new ResourceHasUsedException(e.getMessage());
        } catch (VerifyShippingCTNRException e) {
            throw new VerifyShippingCTNRException(e.getMessage());
        } catch (Exception e2) {
            throw new SystemException(e2, "GateCheckInAdd", log);
        }

    }

    public String getTmsWarehouseId(Long ps_id) {
        String timePsId = "";
        Map<Long, String> map = new HashMap<Long, String>();
        map.put(1000009L, "847");
        map.put(1000005L, "59166");
        map.put(1005696L, "59165");
        map.put(1006858L, "110041");
        map.put(1000015L, "59315");
        map.put(1000007L, "59349");
        if (map.containsKey(ps_id)) {
            timePsId = map.get(ps_id);
        }
        return timePsId;
    }

    public DBRow androidAutoAssginResource(DBRow row,
                                           AdminLoginBean adminLoginBean) throws Exception {

        long yc_id = row.get("yc_id", 0l);
        long doorId = row.get("checkInDoorId", 0l);
        int rel_type = row.get("rel_type", 0);
        long main_id = row.get("main_id", 0l);
        String add_checkin_number = row.get("add_checkin_number", "")
            .toUpperCase();
        DBRow androidRow = new DBRow();
        if (rel_type == CheckInMainDocumentsRelTypeKey.DELIVERY
            || rel_type == CheckInMainDocumentsRelTypeKey.PICK_UP) {
            if (!StrUtil.isBlank(add_checkin_number)) {
                androidRow = androidGetNumberType(add_checkin_number, rel_type,
                    main_id, adminLoginBean.getPs_id(), null);
                if (androidRow.get("resource_type", 0) == OccupyTypeKey.SPOT) {
                    yc_id = androidRow.get("resource_id", 0l);
                } else {
                    doorId = androidRow.get("resource_id", 0l);
                }
            } else {
                androidRow = new DBRow();
                DBRow[] spotArea = floorCheckInMgrZwb.selectSprotByType(
                    adminLoginBean.getPs_id(), 1, 2);
                if (spotArea.length > 0) {
                    DBRow[] rows = getVacancySpot(adminLoginBean.getPs_id(),
                        spotArea[0].get("area_id", 0l), 0); // 可用停车位
                    if (rows.length > 0) {
                        yc_id = rows[0].get("yc_id", 0l);
                        androidRow.add("resource_name",
                            rows[0].get("yc_no", ""));
                    } else {
                        rows = getVacancySpot(adminLoginBean.getPs_id(), 0, 0); // 可用停车位
                        if (rows.length > 0) {
                            yc_id = rows[0].get("yc_id", 0l);
                            androidRow.add("resource_name",
                                rows[0].get("yc_no", ""));
                        }

                    }

                    androidRow.add("resource_id", yc_id);
                    androidRow.add("resource_type", OccupyTypeKey.SPOT);
                }
            }
        } else if (rel_type == CheckInMainDocumentsRelTypeKey.CTNR) {
            androidRow = new DBRow();
            DBRow[] spotArea = floorCheckInMgrZwb.selectSprotByType(
                adminLoginBean.getPs_id(), 2, 1);
            if (spotArea.length > 0) {
                DBRow[] rows = getVacancySpot(adminLoginBean.getPs_id(),
                    spotArea[0].get("area_id", 0l), 0); // 可用停车位
                if (rows.length > 0) {
                    yc_id = rows[0].get("yc_id", 0l);
                    androidRow.add("resource_name", rows[0].get("yc_no", ""));
                } else {
                    rows = getVacancySpot(adminLoginBean.getPs_id(), 0, 0); // 可用停车位
                    if (rows.length > 0) {
                        yc_id = rows[0].get("yc_id", 0l);
                        androidRow.add("resource_name",
                            rows[0].get("yc_no", ""));
                    }

                }

                androidRow.add("resource_id", yc_id);
                androidRow.add("resource_type", OccupyTypeKey.SPOT);
            }

        } else if (rel_type == 0) {
            androidRow = new DBRow();
            DBRow[] spotArea = floorCheckInMgrZwb.selectSprotByType(
                adminLoginBean.getPs_id(), 1, 2);
            if (spotArea.length > 0) {
                DBRow[] rows = getVacancySpot(adminLoginBean.getPs_id(),
                    spotArea[0].get("area_id", 0l), 0); // 可用停车位
                if (rows.length > 0) {
                    yc_id = rows[0].get("yc_id", 0l);
                    androidRow.add("resource_name", rows[0].get("yc_no", ""));
                } else {
                    rows = getVacancySpot(adminLoginBean.getPs_id(), 0, 0); // 可用停车位
                    if (rows.length > 0) {
                        yc_id = rows[0].get("yc_id", 0l);
                        androidRow.add("resource_name",
                            rows[0].get("yc_no", ""));
                    }

                }

                androidRow.add("resource_id", yc_id);
                androidRow.add("resource_type", OccupyTypeKey.SPOT);
            }
        } else if (rel_type == CheckInMainDocumentsRelTypeKey.SMALL_PARCEL) {
// 如果是samllParce自动指定门 wfh 2015-5-12
            androidRow = new DBRow();
            DBRow[] rows = this.getVacancyDoor(adminLoginBean.getPs_id(), 0, 0); // areaId
// 暂不知道
            if (rows.length > 0) {
                doorId = rows[0].get("sd_id", 0l);
                String resource_name = rows[0].get("doorId", "");
                androidRow.add("resource_name", resource_name);
            }
            androidRow.add("resource_id", doorId);
            androidRow.add("resource_type", OccupyTypeKey.DOOR);
            androidRow.add("number_type", ModuleKey.SMALL_PARCEL);
            androidRow.add("system_type", OrderSystemTypeKey.SYNC);
        }
        androidRow.add("yc_id", yc_id);
        androidRow.add("checkInDoorId", doorId);
        return androidRow;
    }

    /**
     * android 根据 task 号 查询出类型 并指定 门 或者停车位 zwb
     *
     * @param number
     * @return
     * @throws Exception
     */
    public DBRow androidGetNumberType(String number, long type, long mainId,
                                      long ps_id, HttpServletRequest request) throws Exception {
        try {
            DBRow row = new DBRow();
            long search_type = 0;
            int number_type = 0;
            long resource_id = 0;
            int resource_type = 0;
            String resource_name = "";
            String appointmentdate = "";
            String companyid = "";
            String customerid = "";
            String supplier_id = "";
            String accountid = "";
            String stagingareaid = "";
            long receiptno = 0l;
            String status = "";
            int system_type = 0;
            if (type == CheckInMainDocumentsRelTypeKey.PICK_UP) {
                search_type = GateCheckLoadingTypeKey.LOAD;
            } else if (type == CheckInMainDocumentsRelTypeKey.DELIVERY) {
                search_type = GateCheckLoadingTypeKey.CTNR;
            }
            DBRow result = this.getDoorByStagingOrTitle(search_type, number,
                mainId, ps_id, 0l, request);
            if (result != null) {
                if (result.get("num", 0l) == 0) { // 没查询到
                    if (search_type == GateCheckLoadingTypeKey.CTNR) {
                        number_type = ModuleKey.CHECK_IN_DELIVERY_ORTHERS;
                    } else if (search_type == GateCheckLoadingTypeKey.LOAD) {
                        number_type = ModuleKey.CHECK_IN_PICKUP_ORTHERS;
                    }
                }
                if (result.get("num", 0l) == 1) { // 查询到一个
                    number_type = result.get("order_type", 0);
                    appointmentdate = result.get("appointmentdate", "");
                    if (!StrUtil.isBlank(appointmentdate)) {
                        long diffTime = (DateUtil.getDate2LongTime(DateUtil
                            .NowStr()) - DateUtil.getDate2LongTime(DateUtil
                            .showUTCTime(appointmentdate, ps_id))) / 1000 / 60;
                        if (diffTime > 120 || diffTime < -120) {
                            DBRow[] spotArea = this.floorCheckInMgrZwb
                                .selectSprotByType(ps_id, 1, 2);
                            if (spotArea.length > 0) {
                                DBRow[] rows = this.getVacancySpot(ps_id,
                                    spotArea[0].get("area_id", 0l), mainId); // 可用停车位
                                if (rows.length > 0) {
                                    resource_id = rows[0].get("yc_id", 0l);
                                    resource_name = rows[0].get("yc_no", "");
                                    resource_type = OccupyTypeKey.SPOT;
                                } else {
                                    rows = this
                                        .getVacancySpot(ps_id, 0, mainId); // 可用停车位
                                    if (rows.length > 0) {
                                        resource_id = rows[0].get("yc_id", 0l);
                                        resource_name = rows[0]
                                            .get("yc_no", "");
                                        resource_type = OccupyTypeKey.SPOT;
                                    }

                                }
                            }

                        } else {
                            if (result.get("door_id", 0l) > 0) {
                                resource_id = result.get("door_id", 0l);
                                resource_name = result.get("door_name", "");
                                resource_type = OccupyTypeKey.DOOR;
                            } else {
                                DBRow[] spotArea = this.floorCheckInMgrZwb
                                    .selectSprotByType(ps_id, 1, 2);
                                if (spotArea.length > 0) {
                                    DBRow[] rows = this.getVacancySpot(ps_id,
                                        spotArea[0].get("area_id", 0l),
                                        mainId); // 可用停车位
                                    if (rows.length > 0) {
                                        resource_id = rows[0].get("yc_id", 0l);
                                        resource_name = rows[0]
                                            .get("yc_no", "");
                                        resource_type = OccupyTypeKey.SPOT;
                                    } else {
                                        rows = this.getVacancySpot(ps_id, 0,
                                            mainId); // 可用停车位
                                        if (rows.length > 0) {
                                            resource_id = rows[0].get("yc_id",
                                                0l);
                                            resource_name = rows[0].get(
                                                "yc_no", "");
                                            resource_type = OccupyTypeKey.SPOT;
                                        }

                                    }
                                }
                            }
                        }
                    } else {
                        if (result.get("door_id", 0l) > 0) {
                            resource_id = result.get("door_id", 0l);
                            resource_name = result.get("door_name", "");
                            resource_type = OccupyTypeKey.DOOR;
                        } else {
                            DBRow[] spotArea = this.floorCheckInMgrZwb
                                .selectSprotByType(ps_id, 1, 2);
                            if (spotArea.length > 0) {
                                DBRow[] rows = this.getVacancySpot(ps_id,
                                    spotArea[0].get("area_id", 0l), mainId); // 可用停车位
                                if (rows.length > 0) {
                                    resource_id = rows[0].get("yc_id", 0l);
                                    resource_name = rows[0].get("yc_no", "");
                                    resource_type = OccupyTypeKey.SPOT;
                                } else {
                                    rows = this
                                        .getVacancySpot(ps_id, 0, mainId); // 可用停车位
                                    if (rows.length > 0) {
                                        resource_id = rows[0].get("yc_id", 0l);
                                        resource_name = rows[0]
                                            .get("yc_no", "");
                                        resource_type = OccupyTypeKey.SPOT;
                                    }

                                }
                            }
                        }
                    }

                } else {
                    DBRow[] spotArea = this.floorCheckInMgrZwb
                        .selectSprotByType(ps_id, 1, 2);
                    if (spotArea.length > 0) {
                        DBRow[] rows = this.getVacancySpot(ps_id,
                            spotArea[0].get("area_id", 0l), mainId); // 可用停车位
                        if (rows.length > 0) {
                            resource_id = rows[0].get("yc_id", 0l);
                            resource_name = rows[0].get("yc_no", "");
                            resource_type = OccupyTypeKey.SPOT;
                        } else {
                            rows = this.getVacancySpot(ps_id, 0, mainId); // 可用停车位
                            if (rows.length > 0) {
                                resource_id = rows[0].get("yc_id", 0l);
                                resource_name = rows[0].get("yc_no", "");
                                resource_type = OccupyTypeKey.SPOT;
                            }

                        }
                    }
                }
                companyid = result.get("companyid", "");
                customerid = result.get("customerid", "");
                supplier_id = result.get("supplierid", "");
                accountid = result.get("accountid", "");
                stagingareaid = result.get("stagingareaid", "");
                receiptno = result.get("receiptno", 0l);
                status = result.get("status", "");
                system_type = result.get("system_type", 0);

            }
            row.add("appointmentdate", appointmentdate);
            row.add("number_type", number_type);
            row.add("companyid", companyid);
            row.add("customerid", customerid);
            row.add("supplier_id", supplier_id);
            row.add("accountid", accountid);
            row.add("stagingareaid", stagingareaid);
            row.add("receiptno", receiptno);
            row.add("status", status);
            row.add("system_type", system_type);
            row.add("resource_id", resource_id);
            row.add("resource_type", resource_type);
            row.add("resource_name", resource_name);
            row.add("num", result.get("num", 0l));
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "androidGetNumberType", log);
        }
    }

    /**
     * gateCheckIn门或停车位是否可以占用
     *
     * @param doorId
     * @param spotId
     * @param dlo_id
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月20日 下午9:13:18
     */
    public void gateChekcInCheckDoorOrSpot(long doorId, long spotId, long dlo_id)
        throws Exception {
        try {
            if (doorId > 0) {
                if (!storageDoorMgrZr.isCanUseDoor(doorId, ModuleKey.CHECK_IN,
                    dlo_id)) {
                    DBRow door = storageDoorMgrZr.findDoorDetaiByDoorId(doorId);
                    throw new DoorHasUsedException(
                        door != null ? door.getString("doorId") : "");
                }
            }
            if (spotId > 0) {
                if (!storageYardControlZr.isCanUseSpot(spotId,
                    ModuleKey.CHECK_IN, dlo_id)) {
                    DBRow spot = storageYardControlZr
                        .findSpotDetaiBySpotId(spotId);
                    throw new SpotHasUsedException(
                        spot != null ? spot.getString("yc_no") : "");
                }
            }
        } catch (DoorHasUsedException e) {
            throw new DoorHasUsedException(e.getMessage());
        } catch (SpotHasUsedException e) {
            throw new SpotHasUsedException(e.getMessage());
        }
    }

    public long modStorageParking(long spot_id, long spot_status,
                                  int associate_type, long associate_id, String patrol_time)
        throws Exception {
        try {
            DBRow row = new DBRow();
            row.add("yc_status", spot_status);
            row.add("associate_type", associate_type);
            row.add("associate_id", associate_id);
            row.add("patrol_time", patrol_time);
            return this.floorCheckInMgrZwb.modStorageParking(spot_id, row);
        } catch (Exception e) {
            throw new SystemException(e, "modStorageParking", log);
        }
    }

    /**
     * 添加GPS设备
     *
     * @return
     * @throws Exception
     */
    public long addAsset(String gps_tracker, String gate_liscense_plate,
                         long groupId, String groupName, String assetName, String callnum)
        throws Exception {
        try {
            long time = System.currentTimeMillis();
            DBRow asset = new DBRow();
            if (!assetName.equals("")) {
                asset.add("name", assetName);
            } else {
                asset.add("name", gps_tracker);
            }
            asset.add("def", gate_liscense_plate);
            asset.add("callnum", callnum);
            asset.add("icon", 1);
            asset.add("assettype", 1);
            asset.add("regtime", time);
            asset.add("imei", gps_tracker);
            asset.add("createtime", time);
            asset.add("updatetime", time);
            long assetid = floorCheckInMgrZwb.addAsset(asset);
            DBRow result = new DBRow();
            if (!groupName.equals("")) {
                result = floorCheckInMgrZwb.findGroup(groupId);
                long groupid = 0;
                if (result == null) {

                    DBRow group = new DBRow();
                    group.add("parentid", 0);
                    group.add("name", groupName);
// group.add("def", gate_liscense_plate);
                    group.add("regtime", time);
                    groupid = floorCheckInMgrZwb.addGroup(group);

                }
                DBRow group_asset = new DBRow();
                if (groupId == 0) {
                    group_asset.add("groupid", groupid);
                } else {
                    group_asset.add("groupid", groupId);
                }
                group_asset.add("assetid", assetid);
                floorCheckInMgrZwb.addGroupAssetRelation(group_asset);
            }
            return assetid;
        } catch (Exception e) {
            throw new SystemException(e, "addAsset", log);
        }
    }

    // 查询GPS号是否存在
    public DBRow findAssetByGPSNumber(String gps_tracker) throws Exception {
        try {

            DBRow result = floorCheckInMgrZwb.findAssetByGPSNumber(gps_tracker);

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "findAssetByGPSNumber", log);
        }
    }

    // 安卓 gate check in
    private long gateAndroidCheckMainId;

    @Override
    public long gateAdnroidCheckInAdd(final DBRow data, final String filePath,
                                      final AdminLoginBean adminLoginBean) throws Exception {
        gateAndroidCheckMainId = 0L;
        sqlServerTransactionTemplate
            .execute(new TransactionCallbackWithoutResult() {

                @Override
                protected void doInTransactionWithoutResult(
                    TransactionStatus txStat) {
                    try {
                        int isLive = data.get("live", 0);
                        String in_seal = data.get("in_seal", "");
                        String gate_driver_name = data.get("driver_name",
                            "").toUpperCase();
                        String gate_liscense_plate = data.get(
                            "liscense_plate", "").toUpperCase();
                        String appointment_time = data.get(
                            "appointment_time", "");
                        String gate_driver_liscense = data.get(
                            "driver_liscense", "").toUpperCase();
                        String gate_container_no = data.get(
                            "gate_container_no", "").toUpperCase();// 货柜号
                        String company_name = data.get("company_name", "")
                            .toUpperCase();// 运输公司名
// long type = StringUtil.getLong(request, "type");
                        String create_time = DateUtil.NowStr();
// long creator = StringUtil.getLong(request,
// "creator");
                        String gps_tracker = data.get("gps_tracker", "");
                        long yc_id = data.get("yc_id", 0l);
                        long ps_id = adminLoginBean.getPs_id();
                        long doorId = data.get("checkInDoorId", 0l);
                        String add_checkin_number = data.get(
                            "add_checkin_number", "").toUpperCase();
                        long status = data.get("status", 0l);
                        long rel_type = data.get("rel_type", 0l);
                        long zone_id = data.get("zone_id", 0l);
                        long mark = data.get("mark", 0l);
                        long gpsId = data.get("gpsId", 0l);
                        String ctnr = data.get("ctnr", "").toUpperCase();
                        String mc_dot = data.get("mc_dot", "")
                            .toUpperCase();
                        int number_type = data.get("number_type", 0);
                        int load_count = data.get("load_count", 0);
                        String phone_gate_liscense_plate = StringUtil
                            .convertStr2PhotoNumber(gate_liscense_plate);
                        String phone_gate_container_no = StringUtil
                            .convertStr2PhotoNumber(gate_container_no);
                        String company_id = data.get("company_id", "");
                        String customer_id = data.get("customer_id", "");
                        String account_id = data.get("account_id", "");
                        String order_no = data.get("order_no", "");
                        String po_no = data.get("po_no", "");
                        String supplier_id = data.get("supplier_id", "");
                        String staging_area_id = data.get(
                            "staging_area_id", "");
                        String freight_term = data.get("freight_term", "");
                        long ic_id = data.get("ic_id", 0l);

                        DBRow row = new DBRow();
                        row.add("isLive", isLive);
                        row.add("in_seal", in_seal);
                        row.add("gate_driver_name", gate_driver_name);

                        row.add("gate_liscense_plate", gate_liscense_plate);
                        if (!("").equals(appointment_time)) {
                            row.add("appointment_time", DateUtil
                                .showUTCTime(appointment_time, ps_id));
                        } else {
                            row.add("appointment_time", null);
                        }

                        row.add("gate_driver_liscense",
                            gate_driver_liscense);
                        row.add("gate_container_no", gate_container_no);
                        row.add("company_name", company_name);
                        row.add("search_gate_container_no", ctnr);
                        row.add("mc_dot", mc_dot);
                        row.add("phone_gate_liscense_plate",
                            phone_gate_liscense_plate);
                        row.add("phone_gate_container_no",
                            phone_gate_container_no);

                        if (mark == 1) {
                            long id = addAsset(gps_tracker,
                                gate_liscense_plate, 0, "", "", "");
                            row.add("gps_tracker", id);
                        } else {
                            if (gpsId > 0) {
                                DBRow updateRow = new DBRow();
                                updateRow.add("def", gate_liscense_plate);
                                floorCheckInMgrZwb.updateAsset(gpsId,
                                    updateRow);
                                row.add("gps_tracker", gpsId);
                            }
                        }
                        if (yc_id > 0) {
                            row.add("yc_id", yc_id);
                        } else {
                            row.add("yc_id", null);
                        }
                        row.add("ps_id", ps_id);
                        row.add("gate_check_in_operate_time", create_time);
                        row.add("creator", adminLoginBean.getAdid());
                        if (!gate_container_no.equals("")) {
                            row.add("status", status);
                        } else {
                            row.add("status", null);
                        }

                        row.add("rel_type", rel_type);
                        row.add("load_count", load_count);
// 加log 数据--------------------
                        String log = "";
                        log += "License Plate:" + gate_liscense_plate + ",";
                        log += "Trailer/CTNR:" + gate_container_no + ",";
                        log += "Carrier:" + company_name + ",";
                        log += "Driver License:" + gate_driver_liscense
                            + ",";
                        log += "MC/DOT:" + mc_dot + ",";
                        log += "GPS:" + gps_tracker + ",";
                        log += moduleKey.getModuleName(number_type) + ":"
                            + add_checkin_number + ",";

                        if (yc_id != 0) {
                            DBRow result = floorCheckInMgrZwb
                                .findStorageYardControl(yc_id);
                            log += "Spot:" + result.get("yc_no", "") + ",";
                        } else {
                            log += "Spot:" + "" + ",";
                        }
                        if (doorId != 0) {
                            DBRow result = floorCheckInMgrZwb
                                .findDoorById(doorId);
                            log += "Door:" + result.get("doorId", "") + ",";
                        } else {
                            log += "Door:" + "" + ",";
                        }
                        if (isLive != 0) {
                            log += "Type:"
                                + checkInLiveLoadOrDropOffKey
                                .getCheckInLiveLoadOrDropOffKey(isLive)
                                + ",";
                        } else {
                            log += "Type:" + "" + ",";
                        }
                        if (!data.equals("")) {
                            log = log.substring(0, log.length() - 1);
                        }

                        row.add("create_time", create_time);
                        row.add("gate_check_in_time", create_time);
                        row.add("tractor_status", status);
                        long main_id = floorCheckInMgrZwb
                            .gateCheckInAdd(row);
// 添加数据到详细表

                        DBRow rowDetail = new DBRow();

                        if (doorId > 0) {
// rowDetail.add("occupancy_type",1);
                            rowDetail.add("occupancy_status", 1);
                            rowDetail.add("rl_id", doorId);
                            rowDetail.add("zone_id", zone_id);

                        }
                        if (doorId > 0
                            || !StringUtil.isBlank(add_checkin_number)) {
                            rowDetail.add("dlo_id", main_id);
                        }
                        if (!StringUtil.isBlank(add_checkin_number)) {

                            rowDetail.add("number",
                                add_checkin_number.toUpperCase());
                            rowDetail.add("number_status", 1);
                            rowDetail.add("company_id", company_id);
                            rowDetail.add("customer_id", customer_id);
                            rowDetail.add("account_id", account_id);
                            rowDetail.add("supplier_id", supplier_id);
                            rowDetail.add("occupancy_status", 1);
                            rowDetail.add("po_no", po_no);
                            rowDetail.add("order_no", order_no);
                            rowDetail.add("number_type", number_type);
                            rowDetail.add("staging_area_id",
                                staging_area_id);
                            rowDetail.add("ic_id", ic_id);
                            if (number_type == ModuleKey.CHECK_IN_CTN
                                && ic_id > 0) {
                                DBRow importRow = new DBRow();
                                importRow.add("status",
                                    ContainerImportStatusKey.RECEIVED);
                                checkInMgrZyj.updateContainerInfoByIcid(
                                    ic_id, importRow);
                            }
                            rowDetail.add("freight_term", freight_term);
                        }

                        floorCheckInMgrZwb.addOccupancyDetails(rowDetail);

// 更改停车位状态
                        if (yc_id > 0) {
                            floorCheckInMgrZwb
                                .updateParkingStatus(yc_id, 2);
                        }
// 添加gate_check_in索引
                        editCheckInIndex(main_id, "add");

                        addCheckInLog(adminLoginBean.getPs_id(),
                            "Created Entry ID【" + main_id + "】",
                            main_id, CheckInLogTypeKey.GATEIN,
                            create_time, log); // 添加log
/*
* //添加file
* 到file表里面,将filePath下面的所有的文件都复制到upload/check_in/ 中
* if(!StringUtil.isBlank(filePath)){ String
* fileDescBasePath = Environment.getHome() +
* "upload/check_in/" ; File dir = new
* File(filePath); if(dir.isDirectory()){ File[]
* files = dir.listFiles(); for(File file : files){
* addFile("gate_check_in_"+
* main_id+"_"+file.getName(), main_id,
* FileWithTypeKey.OCCUPANCY_MAIN, 0 ,
* adminLoginBean.getAdid(), DateUtil.NowStr());
* //FileUtil.copyProductFile(file,
* fileDescBasePath);
* FileUtil.checkInAndroidCopyFile
* (file,"gate_check_in_"+ main_id+"_" ,
* fileDescBasePath); } } }
*/
// 文件上传到文件服务器里面 --by chenchen
                        DBRow r = new DBRow();
                        r.add("file_with_id", main_id);
                        r.add("file_with_type",
                            FileWithTypeKey.OCCUPANCY_MAIN);
                        r.add("file_with_class", 0);
                        fileMgrZr.uploadDirToFileServ(filePath, r,
                            data.getString("session_id"),
                            "gate_check_in_" + main_id + "_");

                        gateAndroidCheckMainId = main_id;
                        updateReceiptsByCtnOrBols(main_id);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        return gateAndroidCheckMainId;
    }

    // 查询load与 order关系
    public DBRow[] findOrderByLoadId(String loadno) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findOrderByLoadId(loadno);
        } catch (Exception e) {
            throw new SystemException(e, "findOrderByLoadId", log);
        }
    }

    // 查询被占用的门
    public DBRow[] selectUseDoor(long ps_id, long area_id) throws Exception {
        try {
// return this.floorCheckInMgrZwb.selectUseDoor(ps_id, area_id);
// 原有的代码
            DBRow[] rows = this.floorSpaceResourcesRelationMgr.getCanUseDoor(
                0l, ps_id, area_id);

// for(int i=0;i<rows.length;i++){
// long equipment_id=rows[i].get("relation_id",0l); //获取关联id
// DBRow
// row=this.floorEquipmentMgr.getEquipmentByEquipmentId(equipment_id);
// int equipment_type=row.get("equipment_type", 0);
// String equipment_number=row.get("equipment_number","");
// rows[i].add("equipment_type", equipment_type);
// rows[i].add("equipment_number", equipment_number);
// }
            return rows;

        } catch (Exception e) {
            throw new SystemException(e, "selectUseDoor", log);
        }
    }

    // 查询被占用门的详细信息
    public DBRow[] selectUseDoorDetail(long doorId) throws Exception {
        try {
// return this.floorCheckInMgrZwb.selectUseDoorDetail(doorId); 原有的代码
            DBRow returnRow = storageDoorMgrZr.getCheckInDoorDetailInfo(doorId);
            if (returnRow != null) {
                return new DBRow[]{returnRow};
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "selectUseDoorDetail", log);
        }
    }

    // 添加传文件
    public long addFile(String fileName, long file_with_id, int file_with_type,
                        int file_with_class, long creator, String upload_time)
        throws Exception {
        try {
            DBRow file = new DBRow();
            file.add("file_name", fileName);
            file.add("file_with_id", file_with_id);
            file.add("file_with_type", file_with_type);
            file.add("file_with_class", file_with_class);
            file.add("upload_adid", creator);
            file.add("upload_time", upload_time);
            file.add("file_is_convert", 0);
            file.add("file_convert_file_path", "");
            return floorFileMgrZr.addFile(file);
        } catch (Exception e) {
            throw new SystemException(e, "addFile", log);
        }
    }
    
    public void addFile(String sn,long file_id,long file_with_id, int file_with_type, int file_with_class,long adid) throws Exception{
    	
    	 StringBuffer fileName = new StringBuffer(sn);
         fileName.append("_")
         .append(file_with_id)
         .append("_")
         .append(DateUtil.NowStr().replaceAll("[-|\\s|:]","") + 1);
         DBRow file = new DBRow();
         file.add("file_name", fileName.toString());
         file.add("file_with_id", file_with_id);
         file.add("file_with_type", file_with_type);
         file.add("file_with_class", file_with_class);
         file.add("expire_in_secs", 0);
         floorFileMgrZr.updateFileById(file_id, file);
         
    	/*StringBuffer logFileNameContent = new StringBuffer();
        String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
        String tempSuffix = getSuffix(fileName);
        String tempfileUpName = fileName.replace("."+ tempSuffix, "");
        StringBuffer realyFileName = new StringBuffer(sn);
        realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);

        int indexFile = floorFileMgrZr.getIndexOfFileByFileName(realyFileName.toString());
        if (indexFile != 0) {
            realyFileName.append("_").append(indexFile);
        }
        realyFileName.append(".").append(tempSuffix);
        logFileNameContent.append(",").append(realyFileName.toString());

        String tempUrl = baseTempUrl + fileName;
        //移动文件到指定的
        StringBuffer url = new StringBuffer();
        url.append(Environment.getHome()).append("upload/")
            .append(path).append("/")
            .append(realyFileName.toString());
        if (!new File(tempUrl).exists()) {
            throw new FileNotFoundException("Up File Not Found.");
        }
        FileUtil.moveFile(tempUrl, url.toString());
        return  this.addFile(realyFileName.toString(), file_with_id,file_with_type, file_with_class, adid,DateUtil.NowStr());*/
    }

    /**
     * 根据doorId和entryId查询详细表
     */
    @Override
    public DBRow[] selectDetails(long doorId, long main_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectDetails(doorId, main_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectDetails", log);
        }
    }

    public DBRow[] selectDetailsInfo(long doorId, long main_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectDetailsInfo(doorId, main_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectDetails", log);
        }
    }

    /**
     * 修改主表
     */
    @Override
    public long modCheckIn(HttpServletRequest request) throws Exception {
        try {
            long dlo_id = StringUtil.getLong(request, "main_id");

            String gate_driver_first_name = StringUtil.getString(request,
                "gate_driver_first_name");
            String gate_driver_last_name = StringUtil.getString(request,
                "gate_driver_last_name");
            String gate_liscense_plate = StringUtil.getString(request,
                "gate_liscense_plate");
            String appointment_time = StringUtil.getString(request,
                "appointment_time");
            String gate_driver_liscense = StringUtil.getString(request,
                "gate_driver_liscense");
            String gate_container_no = StringUtil.getString(request,
                "gate_container_no");
            String company_name = StringUtil.getString(request, "company_name");
            String gps_tracker = StringUtil.getString(request, "gps_tracker");
            long yc_id = StringUtil.getLong(request, "yc_id");
            long ps_id = StringUtil.getLong(request, "add_ps_id");
            long status = StringUtil.getLong(request, "status");
            long mark = StringUtil.getLong(request, "mark");
            long gpsId = StringUtil.getLong(request, "gpsId");
            long initgpsId = StringUtil.getLong(request, "initgpsId");
            long door_id = StringUtil.getLong(request, "checkInDoorId");
            long rel_type = StringUtil.getLong(request, "rel_type");

// 获得创建人
            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long createManId = adminLoginBean.getAdid();
            DBRow row = new DBRow();
            if (!gate_driver_first_name.equals("")) {
                row.add("gate_driver_first_name", gate_driver_first_name);
            }
            if (!gate_driver_last_name.equals("")) {
                row.add("gate_driver_last_name", gate_driver_last_name);
            }
            if (!gate_liscense_plate.equals("")) {
                row.add("gate_liscense_plate", gate_liscense_plate);
            }
            if (!appointment_time.equals("")) {
                row.add("appointment_time",
                    DateUtil.showUTCTime(appointment_time, ps_id));
            }
            if (!gate_driver_liscense.equals("")) {
                row.add("gate_driver_liscense", gate_driver_liscense);
            }
            if (!gate_container_no.equals("")) {
                row.add("gate_container_no", gate_container_no);
            }
            if (!company_name.equals("")) {
                row.add("company_name", company_name);
            }

            if (mark == 1) {
                long id = this.addAsset(gps_tracker, gate_liscense_plate, 0,
                    "", "", "");
                row.add("gps_tracker", id);
            } else {
                DBRow updateRow = new DBRow();
                updateRow.add("def", gate_liscense_plate);
                floorCheckInMgrZwb.updateAsset(gpsId, updateRow);
                row.add("gps_tracker", gpsId);

            }
            if (initgpsId > 0) {
                row.add("gps_tracker", initgpsId);
            }
            DBRow mainRow = this.findGateCheckInById(dlo_id);
            if (yc_id > 0) {
                row.add("yc_id", yc_id);
                this.floorCheckInMgrZwb.updateParkingStatus(
                    mainRow.get("yc_id", 0l), 1); // 释放停车位
                this.floorCheckInMgrZwb.updateParkingStatus(yc_id, 2); // 占用停车位
            }
            if (ps_id > 0) {
                row.add("ps_id", ps_id);
            }
            if (createManId > 0) {
                row.add("creator", createManId);
            }
            if (status > 0) {
                row.add("status", status);
                if (mainRow.get("tractor_status", 0) != 5) {
                    row.add("tractor_status", status);
                }
            }
            if (rel_type > 0) {
                row.add("rel_type", rel_type);
            }
            long id = floorCheckInMgrZwb.modCheckIn(dlo_id, row); // 修改主单据
// 修改子单据
            if (door_id != 0) {
                DBRow[] detailRow = this.floorCheckInMgrZwb
                    .findloadingByInfoId(dlo_id);
                DBRow upDetailRow = new DBRow();
                upDetailRow.add("rl_id", door_id);
                if (detailRow != null && detailRow.length > 0) {
                    long upId = this.floorCheckInMgrZwb.updateDetail(
                        detailRow[0].get("dlo_detail_id", 0l), upDetailRow);
                }
            }

// 添加gate_check_in索引
            this.editCheckInIndex(dlo_id, "update");
// 添加文件
            this.addFiles(request, dlo_id, createManId);
            return dlo_id;
        } catch (Exception e) {
            throw new SystemException(e,
                "modCheckIn(HttpServletRequest request)", log);
        }

    }

    @Override
    public long modPickUpSeal(HttpServletRequest request) throws Exception {
        try {
            long mainId = StringUtil.getLong(request, "mainId");
            long detail_id = StringUtil.getLong(request, "detail_id");
            String seal = StringUtil.getString(request, "seal");
            int load_bar = StringUtil.getInt(request, "load_bar");
            long equipment_id = StringUtil.getLong(request, "equipment_id"); //
            DBRow row = new DBRow();
            row.add("out_seal", seal);
            if (load_bar != -1) {
                row.add("load_bar_id", load_bar);
            }
            DBRow updateRow = new DBRow();
            updateRow.add("used_time", DateUtil.NowStr());
            updateRow.add("seal_status", 3);
            floorCheckInMgrZwb.updateSealUsed(seal, updateRow);

            DBRow[] details = getEntryDetailHasResources(mainId, equipment_id);
//
            DBRow currentLoadRow = floorCheckInMgrZwb
                .selectDetailByDetailId(detail_id);
            currentLoadRow.add("seal", 1);
            this.floorCheckInMgrZwb.modCheckIn(mainId, row);
            return this.closeDetailReturnFlag(equipment_id, mainId, detail_id);// (currentLoadRow.get("number",
// ""),
// details,
// currentLoadRow);
        } catch (Exception e) {
            throw new SystemException(e, "modPickUpSeal", log);
        }
    }

    private String getSuffix(String filename) {
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1) {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }

    /**
     * 添加文件
     *
     * @param request
     * @param dlo_id
     * @param createManId
     * @throws Exception
     */
    public void addFiles(HttpServletRequest request, long dlo_id,long createManId) throws FileNotFoundException, Exception {

        try {

            String fileNames = StringUtil.getString(request, "file_names");
            String sn = StringUtil.getString(request, "sn");
            String path = StringUtil.getString(request, "path");
            int file_with_type = StringUtil.getInt(request, "file_with_type");
            int file_with_class = StringUtil.getInt(request, "file_with_class");
            boolean is_in_file_serv = StringUtil.getBooleanType(StringUtil .getInt(request, "is_in_file_serv")); // 文件是否已上传至文件服务器

            String[] fileNameArray = null;
            if (fileNames.trim().length() > 0) {
                fileNameArray = fileNames.trim().split(",");
                if (fileNameArray == null || fileNameArray.length == 0) {
                    return;
                }
            } else {
                return;
            }

            if (!is_in_file_serv) {
                StringBuffer logFileNameContent = new StringBuffer();
                String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
                for (String tempFileName : fileNameArray) {
                    String tempSuffix = getSuffix(tempFileName);
                    String tempfileUpName = tempFileName.replace("."+ tempSuffix, "");
                    StringBuffer realyFileName = new StringBuffer(sn);
                    realyFileName.append("_").append(dlo_id).append("_").append(tempfileUpName);

                    int indexFile = floorFileMgrZr.getIndexOfFileByFileName(realyFileName.toString());
                    if (indexFile != 0) {
                        realyFileName.append("_").append(indexFile);
                    }
                    realyFileName.append(".").append(tempSuffix);
                    logFileNameContent.append(",").append(realyFileName.toString());

                    String tempUrl = baseTempUrl + tempFileName;
// 移动文件到指定的
                    StringBuffer url = new StringBuffer();

                    url.append(Environment.getHome()).append("upload/")
                        .append(path).append("/")
                        .append(realyFileName.toString());
                    if (!new File(tempUrl).exists()) {
                        throw new FileNotFoundException("Up File Not Found.");
                    }
                    FileUtil.moveFile(tempUrl, url.toString());
                    this.addFile(realyFileName.toString(), dlo_id,file_with_type, file_with_class, createManId,DateUtil.NowStr());
                }
            } else {
                int count = 0;
                for (String fileIdStr : fileNameArray) {
                    count++;
                    StringBuffer fileName = new StringBuffer(sn);
                    fileName.append("_")
                    .append(dlo_id)
                    .append("_")
                    .append(DateUtil.NowStr().replaceAll("[-|\\s|:]","") + count);
                    long file_id = Long.parseLong(fileIdStr);
                    DBRow file = new DBRow();
                    file.add("file_name", fileName.toString());
                    file.add("file_with_id", dlo_id);
                    file.add("file_with_type", file_with_type);
                    file.add("file_with_class", file_with_class);
                    file.add("expire_in_secs", 0);
                    floorFileMgrZr.updateFileById(file_id, file);
                }
            }
        } catch (FileNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "addFile", log);
        }
    }

    /**
     * 根据id和type查询上传文件
     */
    @Override
    public DBRow[] getAllFileByFileWithIdAndFileWithType(long file_with_id,
                                                         int file_with_type) throws Exception {
        try {
            return floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(
                file_with_id, file_with_type);
        } catch (Exception e) {
            throw new SystemException(e,
                "getAllFileByFileWithIdAndFileWithType", log);
        }

    }

    /**
     * 操作check_in索引
     *
     * @param dlo_id entryID
     * @param type   操作类型(add,update)
     * @throws Exception
     */
    public void editCheckInIndex(long dlo_id, String type) throws Exception {
        DBRow row = floorCheckInMgrZwb.selectMainByEntryId(dlo_id);
        long ps_id = row.get("ps_id", 0l);
// DBRow[] equipment =
// floorCheckInMgrZwb.findEquipmentByDloId(dlo_id);???
// findEquipmentByDloIdOrOutId
        DBRow[] equipment = floorCheckInMgrZwb
            .findEquipmentByDloIdOrOutId(dlo_id);

        String gate_container_no = "";
        String gate_driver_liscense = row.get("gate_driver_liscense", "");
        String gate_liscense_plate = "";
        String company_name = row.get("company_name", "");
        String ymsEntryId = row.get("yms_entry_id", "");
        DBRow[] numbers = this.floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
        String loadNo = "";
        if (numbers != null) {
            for (int i = 0; i < numbers.length; i++) {
// if(numbers[i].get("number_type", 0l)==ModuleKey.CHECK_IN_LOAD
// || numbers[i].get("number_type", 0l)==ModuleKey.CHECK_IN_PONO
// || numbers[i].get("number_type",
// 0l)==ModuleKey.CHECK_IN_ORDER ){
                loadNo += numbers[i].get("number", "") + " ";
// }
            }
        }
        if (equipment != null) {
            for (DBRow temp : equipment) {
                int eqType = temp.get("equipment_type", 0);
                if (eqType == 1) {// 车头
                    gate_liscense_plate += temp.get("equipment_number", "")
                        + " ";
                } else if (eqType == 2) { // 车尾
                    gate_container_no += temp.get("equipment_number", "") + " ";
                }
            }
        }
        if (type.equals("add")) {
            CheckInIndexMgr.getInstance().addIndex(dlo_id, ps_id,
                gate_container_no, gate_driver_liscense,
                gate_liscense_plate, company_name, loadNo,ymsEntryId);
        } else if (type.equals("update")) {
            CheckInIndexMgr.getInstance().updateIndex(dlo_id, ps_id,
                gate_container_no, gate_driver_liscense,
                gate_liscense_plate, company_name, loadNo);
        }
    }

    public void editCheckInCarrierIndex(long dlo_id, String company_name,
                                        String type) throws Exception {
        if (type.equals("add")) {
            CheckInCarrierIndexMgr.getInstance().addIndex(dlo_id, company_name);
        } else if (type.equals("update")) {
            CheckInCarrierIndexMgr.getInstance().updateIndex(dlo_id,
                company_name);
        }
    }

    /**
     * 查询停车位信息
     */
    @Override
    public DBRow[] getParkingOccupancy(String yc, long ps_id, long flag,
                                       long spotArea, long mainId) throws Exception {
        try {
            return floorCheckInMgrZwb.getParkingOccupancy(yc, ps_id, flag,
                spotArea, mainId);

        } catch (Exception e) {
            throw new SystemException(e, "getParkingOccupancy", log);
        }

    }

    /**
     * 根据entryID搜索主单据
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] searchCheckInByNumber(String search_key, int search_mode, PageCtrl pc, AdminLoginBean adminLoginBean) throws Exception {
        /*try {
            if (!search_key.equals("") && !search_key.equals("\"")) {
                search_key = search_key.replaceAll("\"", "");
                search_key = search_key.replaceAll("'", "");
                search_key = search_key.replaceAll("\\*", "");
                // 	search_key +="*";这个索引是IK分词器，不要最后+*
            }
            int page_count = systemConfig.getIntConfigValue("page_count");
            
            DBRow[] rows = CheckInIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(), search_mode, page_count, pc,adminLoginBean);
            
			if ((rows == null || rows.length == 0) && search_key.startsWith("ET-")) {
				EntryTicketCheck ticketCheck =  getYmsEntryTicket(search_key);
				
			}
            return  rows ;
            //return CheckInIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(), search_mode, page_count, pc,adminLoginBean);
        } catch (Exception e) {
            throw new SystemException(e, "", log);
        }*/
    	return searchCheckInByNumber(search_key,search_mode,pc,adminLoginBean,null);
    }
    
    @Override
    public DBRow[] searchCheckInByNumber(String search_key, int search_mode, PageCtrl pc, AdminLoginBean adminLoginBean,HttpServletRequest request) throws Exception {
        try {
            if (!search_key.equals("") && !search_key.equals("\"")) {
                search_key = search_key.replaceAll("\"", "");
                search_key = search_key.replaceAll("'", "");
                search_key = search_key.replaceAll("\\*", "");
                // 	search_key +="*";这个索引是IK分词器，不要最后+*
            }
            int page_count = systemConfig.getIntConfigValue("page_count");
            
            if(search_key.startsWith("ET-")){
            	search_key = translateIntoLincEntryId(search_key,adminLoginBean.getPs_id());
            }
            
            DBRow[] rows = CheckInIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(), search_mode, page_count, pc,adminLoginBean);
			if ((rows == null || rows.length == 0) && search_key.startsWith("ET-")) {
				EntryTicketCheck ticketCheck =  getYmsEntryTicket(search_key,adminLoginBean.getPs_id());
				if (ticketCheck != null) {
					this.ymsGateCheckInAdd(request, ticketCheck);
					return CheckInIndexMgr.getInstance().getSearchResults(translateIntoLincEntryId(search_key,adminLoginBean.getPs_id()), search_mode, page_count, pc,adminLoginBean);
				}
			}
            return  rows ;
        } catch (Exception e) {
            throw new SystemException(e, "", log);
        }
    }
    
    private String translateIntoLincEntryId(String ymsEntryId,long psId) throws Exception{
    	String lincEntryId = ymsEntryId;
    	DBRow row = floorCheckInMgrZwb.getEntryByYmsId(ymsEntryId,psId);
    	if(row!=null){
    		lincEntryId =  row.getString("dlo_id").toLowerCase();
    	}
    	return lincEntryId;
    }

    @Override
    public DBRow[] findOccupancyDetails(long main_id) throws Exception {
        try {
            return floorCheckInMgrZwb.findOccupancyDetails(main_id);
        } catch (Exception e) {
            throw new SystemException(e, "findOccupancyDetails", log);
        }
    }

    // 添加check in等待信息
    public long addCheckInWait(HttpServletRequest request) throws Exception {
        try {
            long zone_id = StringUtil.getLong(request, "zone_id");
            long entry_id = StringUtil.getLong(request, "entry_id");
            String order_id = StringUtil.getString(request, "order_id");
            String order_type = StringUtil.getString(request, "order_type");
            String beizhu = StringUtil.getString(request, "beizhu");

            DBRow row = new DBRow();
            if (!order_id.equals("")) {
                row.add("bill_id", order_id);
                row.add("bill_type", order_type);
            }
            row.add("zone_id", zone_id);
            row.add("entry_id", entry_id);
            row.add("reason", beizhu);
            return this.floorCheckInMgrZwb.addCheckInWait(row);
        } catch (Exception e) {
            throw new SystemException(e, "addCheckInWait", log);
        }
    }

    @Override
    public DBRow[] findSpotArea(long ps_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findSpotArea(ps_id);
        } catch (Exception e) {
            throw new SystemException(e, "findSpotArea", log);
        }
    }

    /**
     * 1.如果有最小的单数，返回单数 2.如果都没有单数的，返回一个最小的双数
     *
     * @param dataList
     * @return
     */
    private DBRow getAndroidSingleSpot(List<DBRow> dataList) {
/**
 * 1.查询一个最小的单数，如果一个单数都没有了，那么返回一个最小的双数, 2.首先让整个DBRow进行一个排序 3.然后再进行查找,
 * 排序过后然后查找最小的单数，没有单数，那么返回index = 0 的数据，他是双数
 */

        if (dataList != null && dataList.size() > 0) {
            Collections.sort(dataList, new Comparator<DBRow>() {
                @Override
                public int compare(DBRow o1, DBRow o2) {
                    int returnInt = 0;
                    if (o1.get("yc_no", 0) > o2.get("yc_no", 0)) {
                        returnInt = 1;
                    }
                    ;
                    if (o1.get("yc_no", 0) < o2.get("yc_no", 0)) {
                        returnInt = -1;
                    }
                    if (o1.get("yc_id", 0) == o2.get("yc_no", 0)) {
                        returnInt = 0;
                    }
                    return returnInt;
                }
            });
            DBRow returnRow = null;
            boolean isSingle = false;
            for (DBRow row : dataList) {
                if (row.get("yc_no", 0) % 2 == 1) {
                    returnRow = row;
                    isSingle = true;
                    break;
                }
            }
            returnRow = isSingle ? returnRow : dataList.get(0);
            return returnRow;
        }
        return new DBRow();
    }

// android 用返回停车位 的区域 与停车位

    /**
     * request_type (1:ALL. 2:Free+自己可用的)
     */
    public DBRow androidGetSpotArea(long ps_id, long entry_id, int request_type)
        throws Exception {
        return null;
    }

    // 添加checkin等待信息
    public DBRow[] selectAllCheckInWait(long zoneId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectAllCheckInWait(zoneId);
        } catch (Exception e) {
            throw new SystemException(e, "selectAllCheckInWait", log);
        }
    }

    // 根据 id 查询等待信息
    public DBRow findWaitById(long id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findWaitById(id);
        } catch (Exception e) {
            throw new SystemException(e, "findWaitById", log);
        }
    }

    // 区域可用门
    public DBRow[] zoneReadyDoor(long zone_id, long dlo_id, String doorName)
        throws Exception { // 要改张睿
        try {
            return this.floorCheckInMgrZwb.zoneReadyDoor(zone_id, dlo_id,
                doorName);
        } catch (Exception e) {
            throw new SystemException(e, "zoneReadyDoor", log);
        }
    }

    // android 区域可用门
    public DBRow[] androidZoneReadyDoor(long dlo_id, long zone_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.zoneReadyDoor(zone_id, dlo_id, "");
        } catch (Exception e) {
            throw new SystemException(e, "zoneReadyDoor", log);
        }
    }

    // android区域占用门
    public DBRow[] androidZoneUseDoor(long zone_id) throws Exception {
        try {
            List<DBRow> dataList = new ArrayList<DBRow>(); // 返回的
            DBRow[] rows = this.floorCheckInMgrZwb.zoneUseDoor(zone_id);
            if (rows != null && rows.length > 0) {
                for (int i = 0; i < rows.length; i++) {
                    DBRow row = new DBRow();
                    row.add("sd_id", rows[i].get("sd_id", 0l));
                    row.add("doorId", rows[i].getString("doorId"));
                    row.add("gate_liscense_plate",
                        rows[i].getString("gate_liscense_plate"));
                    dataList.add(row);
                }
            }
            return dataList.toArray(new DBRow[dataList.size()]);
        } catch (Exception e) {
            throw new SystemException(e, "zoneUseDoor", log);
        }
    }

    // 区域占用门
    public DBRow[] zoneUseDoor(long zone_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.zoneUseDoor(zone_id);
        } catch (Exception e) {
            throw new SystemException(e, "zoneUseDoor", log);
        }
    }

    // 根据门id 查询门信息
    public DBRow findDoorById(long id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findDoorById(id);
        } catch (Exception e) {
            throw new SystemException(e, "findDoorById", log);
        }
    }

    // 根据主单据id 查询单据
    public DBRow findMainById(long dlo_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findMainById(dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "findMainById", log);
        }
    }

    @Override
    public void modOccupancyDetails(DBRow row) throws Exception {
        try {
            floorCheckInMgrZwb.modOccupancyDetails(row);
        } catch (Exception e) {
            throw new SystemException(e, "modOccupancyDetails", log);
        }
    }

    // checkin wahouse
    public long wahouseCheckIn(final HttpServletRequest request)
        throws Exception {
        try {
// 创建时间
            Date date = (Date) Calendar.getInstance().getTime();
            SimpleDateFormat dateformat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
            String createTime = dateformat.format(date);
// 获得创建人
            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long createManId = adminLoginBean.getAdid();

            long infoId = StringUtil.getLong(request, "id");
            String in_seal = StringUtil.getString(request, "in_seal");
            String out_seal = StringUtil.getString(request, "out_seal");
// long islive = StringUtil.getLong(request,"islive");
            DBRow mainRow = new DBRow();
// if(!in_seal.equals("")){ //如果封条号不为空 更新封条号
            mainRow.add("in_seal", in_seal);
// }
// if(!out_seal.equals("")){ //如果封条号不为空 更新封条号
            mainRow.add("out_seal", out_seal);
// }
// 根据主单据id 查询主单据状态
            DBRow infoRow = floorCheckInMgrZwb.findMainById(infoId);
// long rel_type=infoRow.get("rel_type", 0l);//主单据类型
            long tractor_status = infoRow.get("tractor_status", 0l);
            mainRow.add("status", CheckInMainDocumentsStatusTypeKey.PROCESSING);
            if (tractor_status != 5) {
                mainRow.add("tractor_status",
                    CheckInMainDocumentsStatusTypeKey.PROCESSING);
            }

// 更新主单据 时间 状态
            mainRow.add("warehouse_check_in_time", createTime);
            mainRow.add("warehouse_check_in_operator", createManId);
            mainRow.add("warehouse_check_in_operate_time", createTime);

            mainRow.add("dlo_id", infoId); // status
            floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新主表

            addFiles(request, infoId, createManId);
            addCheckInLog(createManId, "Update Entry Status", infoId,
                CheckInLogTypeKey.WAREHOUSE, createTime, "");

            return 01;
        } catch (Exception e) {
            throw new SystemException(e, "wahouseCheckIn", log);
        }
    }

    private boolean isAllTaskFinish(long dlo_id, long sd_id) throws Exception {
        try {

            DBRow[] rows = floorCheckInMgrZwb.getDetailsBy(dlo_id, sd_id);
            if (rows != null && rows.length > 0) {
                for (DBRow temp : rows) {
                    if (!isEntryDetailIsFinish(temp)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new SystemException(e, "dockCheckInFreeHasOccupeidDoors", log);
        }
    }

    /**
     * 是否这个门下的所有的子单据上面门的状态都是RESERVERED
     *
     * @param rowsArray
     * @return
     * @author zhangrui
     * @Date 2014年11月17日
     */
    private boolean isThisDoorDetailAllRESERVERED(Iterable<DBRow> rowsArray) {
        for (DBRow temp : rowsArray) {
            if (temp.get("occupancy_status", 0) != OccupyStatusTypeKey.RESERVERED) {
                return false;
            }
        }
        return true;
    }

    private boolean isDetailOccupiedDoor(DBRow detail) {
        if (detail != null) {
            return detail.get("occupancy_type", 1) == 1;
        }
        return false;
    }

    /**
     * 过滤出这个门下面所有的子任务
     *
     * @return
     * @author zhangrui
     * @Date 2014年11月17日
     */
    private List<DBRow> filterCurrentDoorDetails(DBRow[] details, long sd_id) {
        List<DBRow> returnRows = new ArrayList<DBRow>();
        if (details != null && details.length > 0) {
            for (DBRow temp : details) {
                if (isDetailOccupiedDoor(temp)
                    && temp.get("rl_id", 0l) == sd_id) {
                    returnRows.add(temp);
                }
            }
        }
        return returnRows;
    }

    /**
     * 根据门的ID 获取门的名字 如果没有查询到返回"";
     *
     * @param sd_id
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date 2014年11月25日
     */
    private String getDoorName(long sd_id) throws Exception {
        try {
            if (sd_id != 0l) {
                DBRow doorRow = floorCheckInMgrZwb.findDoorById(sd_id);
                if (doorRow != null) {
                    return doorRow.getString("doorId");
                }
            }
            return "";
        } catch (Exception e) {
            throw new SystemException(e, "getDoorName", log);
        }
    }

    // android checkin wahouse
    public long androidWahouseCheckIn(final long infoId, final long adid,
                                      final String delivery_seal, final String pickup_seal,
                                      final String filePath) throws Exception {
        try {
// 创建时间
            Date date = (Date) Calendar.getInstance().getTime();
            SimpleDateFormat dateformat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
            String createTime = dateformat.format(date);
            DBRow mainRow = new DBRow();
            if (!delivery_seal.equals("")) {
                mainRow.add("in_seal", delivery_seal);
            }
            if (!pickup_seal.equals("")) {
                mainRow.add("out_seal", pickup_seal);
            }
// 根据主单据id 查询主单据状态
            floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新主表
            addCheckInLog(adid, "Update Entry Status", infoId,
                CheckInLogTypeKey.ANDROID_DOCK_CHECKIN, createTime, "");
            androidSaveMoveFile(filePath, adid, infoId, "dock_check_in_"
                + infoId + "_");
            return 01;
        } catch (Exception e) {
            throw new SystemException(e, "wahouseCheckIn", log);
        }
    }

    /**
     * android,如果提交数据的时候有包含图片，那么记得调用这个方法(文件保存在web服务器) -----如需上传至文件服务器请用
     * androidSaveMoveFile(String dirPath, long dlo_id , String prefix, String
     * sessionId)方法 --by chenchen
     *
     * @param filePath
     * @param dlo_id
     * @throws Exception
     */
    @Override
    public void androidSaveMoveFile(String filePath, long adid, long dlo_id,
                                    String prefix) throws Exception {
        if (!StringUtil.isBlank(filePath)) {
            String fileDescBasePath = Environment.getHome()
                + "upload/check_in/";
            File dir = new File(filePath);
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (File file : files) {
// String fileName = StringUtil.getFileName(file.getName());
                    addFile(prefix + file.getName(), dlo_id,
                        FileWithTypeKey.OCCUPANCY_MAIN, 0, adid,
                        DateUtil.NowStr());
// FileUtil.copyProductFile(file, fileDescBasePath);
                    FileUtil.checkInAndroidCopyFile(file, prefix,
                        fileDescBasePath);
                }
            }
        }
    }

    @Override
    public void androidSaveMoveFile(String dirPath, long dlo_id, String prefix,
                                    String sessionId) throws Exception {
        DBRow row = new DBRow();
        row.add("file_with_id", dlo_id);
        row.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
        row.add("file_with_class", 0);
        fileMgrZr.uploadDirToFileServ(dirPath, row, sessionId, prefix);
    }

    /**
     * 批量释放门
     *
     * @param door
     * @throws Exception
     */

    public void batchReleaseDoor(String door) throws Exception {
        DBRow updateRow = new DBRow();
        updateRow.add("occupied_status", OccupyStatusTypeKey.RELEASED);
        updateRow.add("associate_id", 0l);
        updateRow.add("associate_type", 0);
        this.floorCheckInMgrZwb.batchReleaseDoor(door, updateRow);
    }

    /**
     * 根据主单据号获得占用门的字符串
     *
     * @param main_id
     * @return <doorName,doorId>
     * @throws Exception
     */
    private HoldDoubleValue<String, String> getDoorNamesAndDoorIds(long main_id)
        throws Exception {
        try {
            StringBuffer str_door_names = new StringBuffer("");
            StringBuffer str_door_ids = new StringBuffer("");
            DBRow[] doors = storageDoorMgrZr.getUseDoorByAssociateIdAndType(
                main_id, ModuleKey.CHECK_IN);
            if (doors != null) {
                for (DBRow temp : doors) {
                    str_door_ids.append(",").append(temp.getString("sd_id"));
                    str_door_names.append(",").append(temp.getString("doorId"));

                }
            }
            String fixDoorNames = str_door_names.length() > 1 ? str_door_names
                .substring(1) : "";
            String fixDoorIds = str_door_ids.length() > 1 ? str_door_ids
                .substring(1) : "";
            return new HoldDoubleValue<String, String>(fixDoorNames, fixDoorIds);
        } catch (Exception e) {
            throw new SystemException(e, "getDoorsName", log);
        }
    }

    /**
     * 根据主单据号获得停车位的字符串
     *
     * @return
     * @throws Exception
     */
    private HoldDoubleValue<String, String> getSpotNamesAndSpotIds(long main_id)
        throws Exception {
        try {
            StringBuffer str_spot_names = new StringBuffer("");
            StringBuffer str_spot_ids = new StringBuffer("");
            DBRow[] spots = storageYardControlZr
                .getUseSpotByAssociateIdAndType(main_id, ModuleKey.CHECK_IN);
            if (spots != null) {
                for (DBRow temp : spots) {
                    str_spot_ids.append(",").append(temp.getString("yc_id"));
                    str_spot_names.append(",").append(temp.getString("yc_no"));

                }
            }
            String fixSpotNames = str_spot_names.length() > 1 ? str_spot_names
                .substring(1) : "";
            String fixSpotIds = str_spot_ids.length() > 1 ? str_spot_ids
                .substring(1) : "";
            return new HoldDoubleValue<String, String>(fixSpotNames, fixSpotIds);

/*
* StringBuffer str_spots= new StringBuffer(); DBRow[] spots =
* storageYardControlZr.getUseSpotByAssociateIdAndType(main_id,
* ModuleKey.CHECK_IN); if(spots != null){ for(DBRow temp : spots){
* str_spots.append(",").append(temp.getString("yc_id")); } } return
* str_spots.length() > 1 ? str_spots.substring(1) :"" ;
*/
        } catch (Exception e) {
            throw new SystemException(e, "getSpot", log);
        }
    }

    /**
     * 根据主单据号获得停车位的名字字符串
     *
     * @return
     * @throws Exception
     */
    private String getSpotNames(long main_id) throws Exception {
        try {
            StringBuffer str_spots = new StringBuffer();
            DBRow[] spots = storageYardControlZr
                .getUseSpotByAssociateIdAndType(main_id, ModuleKey.CHECK_IN);
            if (spots != null) {
                for (DBRow temp : spots) {
                    str_spots.append(",").append(temp.getString("yc_no"));
                }
            }
            return str_spots.length() > 1 ? str_spots.substring(1) : "";
        } catch (Exception e) {
            throw new SystemException(e, "getSpot", log);
        }
    }

    /**
     * 根据货柜号查询 该货柜所在的主单据号
     *
     * @param ctnr_no
     * @return
     * @throws Exception
     */

    private long getMainIdByContainerNo(String ctnr_no, long ps_id)
        throws Exception {
        try {
            long main_id = 0;
// 获得仓库
            DBRow searchRow = this.findSpotByCtnNo(ctnr_no, ps_id);
            if (searchRow != null) {
                main_id = searchRow.get("dlo_id", 0l);
            }
            return main_id;
        } catch (Exception e) {
            throw new SystemException(e, "getMainIdByContainerNo", log);
        }
    }

    /**
     * 释放门 更新 门的表
     *
     * @param main_id
     * @throws Exception
     */

    private void updateStorageDoor(long main_id, long adid) throws Exception {
        try {
            DBRow[] doors = storageDoorMgrZr.getUseDoorByAssociateIdAndType(
                main_id, ModuleKey.CHECK_IN);
            if (doors.length > 0) {
                for (int i = 0; i < doors.length; i++) {
                    long door_id = doors[i].get("sd_id", 0l);
                    storageDoorMgrZr.freeDoor(door_id, adid, main_id,
                        ModuleKey.CHECK_IN);
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "updateStorageDoor", log);
        }

    }

    /**
     * 释放停车位 更新 yard的表
     *
     * @param main_id
     * @throws Exception
     */
    private void updateYard(long main_id, long adid) throws Exception {
        try {
            DBRow[] spots = storageYardControlZr
                .getUseSpotByAssociateIdAndType(main_id, ModuleKey.CHECK_IN);
            if (spots.length > 0) {
                for (int i = 0; i < spots.length; i++) {
                    long spot_id = spots[i].get("yc_id", 0l);
                    storageYardControlZr.freeSpot(spot_id, adid, main_id,
                        ModuleKey.CHECK_IN);
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "updateStorageDoor", log);
        }
    }

    /**
     * 获得 checkout 的日志
     *
     * @param search_ctnr
     * @param seal
     * @param doors
     * @param parks
     * @return
     * @throws Exception
     */
    private String getCheckOutLog(String search_ctnr, String seal,
                                  String doors, String parks) throws Exception {
        try {
            String data = "";
            data += "Exiting CTNR:" + search_ctnr + ",";
            data += "PickUp Seal:" + seal + ",";
            if (!doors.equals("")) {
                data += "Door: " + doors + ",";
            }
            if (!parks.equals("")) {
                data += "Spot: " + parks + ",";
            }
            if (!data.equals("")) {
                data = data.substring(0, data.length() - 1);
            }
            return data;
        } catch (Exception e) {
            throw new SystemException(e, "getCheckOutLog", log);
        }
    }

    // check out
// liveLoad 释放占用的门和停车位 更改 车头 车尾 的 状态---------------------更新1个entry的单据
// 车头和车尾 分开 车头自己走 不释放门和停车位 更改车头的状态------------------------ 更新1个entry的单据
// 车头和车尾 分开 车头带车尾走 释放车尾占用的门和停车位 更改车头本身的状态 -----和带走车尾的状态------更新2个entry的单据
// 日志记录在详细一点
// drop off 头 不释放门和停车位 更改车头状态 更新 1个entry 单据 只能车头离开 如果有货柜 联系window
// swap ctnr 车头 车尾 释放车尾占用的门和停车位 更改车头本身的状态 -----和带走车尾的状态------更新2个entry的单据
// pickup ctnr 车头 车尾 释放车尾占用的门和停车位 更改车头本身的状态 -----和带走车尾的状态------更新2个entry的单据
    @Override
    public void checkInOut(long infoId, String ctnr_no, String isLive,
                           String seal, long adid, long ps_id, boolean isAndroid,
                           HttpServletRequest request) throws Exception {
        try {
// 获得创建人

            String door_names = "";
            String spot_names = "";
            DBRow mainRow = new DBRow(); // 跟新主单据的row
            String createTime = DateUtil.NowStr(); // 当前时间

            if (isLive.equals("liveLoad")) {
// 释放车头的停车位和门
                HoldDoubleValue<String, String> spotNamesAndIds = this
                    .getSpotNamesAndSpotIds(infoId); // 根据主单据id查询占用的Spot的字符串
                spot_names = spotNamesAndIds.a;
                String spots = spotNamesAndIds.b;

                HoldDoubleValue<String, String> doorNamesAndIds = this
                    .getDoorNamesAndDoorIds(infoId); // 根据主单据id查询占用的门的字符串
                door_names = doorNamesAndIds.a;
                String doors = doorNamesAndIds.b;

                this.updateStorageDoor(infoId, adid); // 释放门 门表
                this.updateYard(infoId, adid); // 释放 停车位
                if (!doors.equals("")) {
                    floorCheckInMgrZwb.openDoorByInfoId(infoId, doors); // 释放子表门
                }
                if (!spots.equals("")) {
                    floorCheckInMgrZwb.openDoorByInfoId(infoId, spots); // 释放子停车位
                }
                mainRow.add("status", CheckInMainDocumentsStatusTypeKey.LEFT); // 更新车尾状态
// 同entry
                mainRow.add("tractor_status",
                    CheckInMainDocumentsStatusTypeKey.LEFT); // 更新车头状态
// 同entry

                String data = this.getCheckOutLog(ctnr_no, seal, door_names,
                    spot_names); // 获得 日志的更新数据
                int LogType = isAndroid ? CheckInLogTypeKey.ANDROID_GATEOUT
                    : CheckInLogTypeKey.GATEOUT;
                addCheckInLog(adid, "Completed", infoId, LogType, createTime,
                    data); // 添加日志 车头车尾日志

            } else {
// 如果带走货柜 需要释放货柜的门和停车位 如果车头自己走 什么都不释放更改车头的状态
                if (!ctnr_no.equals("")) {
                    long search_main_id = this.getMainIdByContainerNo(ctnr_no,
                        ps_id);

                    HoldDoubleValue<String, String> spotNamesAndIds = this
                        .getSpotNamesAndSpotIds(search_main_id); // 根据主单据id查询占用的Spot的字符串
                    spot_names = spotNamesAndIds.a;
                    String spots = spotNamesAndIds.b;

                    HoldDoubleValue<String, String> doorNamesAndIds = this
                        .getDoorNamesAndDoorIds(search_main_id); // 根据主单据id查询占用的门的字符串
                    door_names = doorNamesAndIds.a;
                    String doors = doorNamesAndIds.b;

                    this.updateStorageDoor(search_main_id, adid); // 释放门 门表
                    this.updateYard(search_main_id, adid); // 释放 停车位
                    if (!doors.equals("")) {
                        floorCheckInMgrZwb.openDoorByInfoId(search_main_id,
                            doors); // 释放子表门
                    }
                    if (!spots.equals("")) {
                        floorCheckInMgrZwb.openDoorByInfoId(search_main_id,
                            spots); // 释放子停车位
                    }
                    mainRow.add("search_gate_container_no", ctnr_no); // 更新主表
// 记录要带走的货柜

                    mainRow.add("tractor_status",
                        CheckInMainDocumentsStatusTypeKey.LEFT); // 更新车头状态
// 车头的entry
                    DBRow containerRow = new DBRow();
                    containerRow.add("status",
                        CheckInMainDocumentsStatusTypeKey.LEFT);
                    floorCheckInMgrZwb.modCheckIn(search_main_id, containerRow); // 更新要带走的货柜的状态
// 货柜的entry

                    String data = this.getCheckOutLog(ctnr_no, seal, "", ""); // 获得
// 日志的更新数据
                    int LogType = isAndroid ? CheckInLogTypeKey.ANDROID_GATEOUT
                        : CheckInLogTypeKey.TRACTOROUT;
                    addCheckInLog(adid, "Completed", infoId, LogType,
                        createTime, data); // 添加日志 车头车尾日志

                    String tractorData = this.getCheckOutLog("", seal,
                        door_names, spot_names); // 获得 日志的更新数据
                    int LogTypeTractor = isAndroid ? CheckInLogTypeKey.ANDROID_GATEOUT
                        : CheckInLogTypeKey.TRAILEROUT;
                    addCheckInLog(adid, "Completed", search_main_id,
                        LogTypeTractor, createTime, tractorData); // 添加日志
// 车头车尾日志

                } else {
                    mainRow.add("tractor_status",
                        CheckInMainDocumentsStatusTypeKey.LEFT); // 车头自己走
// 什么都不释放
// 只更开
// 车头状态

                    String data = this.getCheckOutLog("", seal, "", ""); // 获得
// 日志的更新数据
                    int LogType = isAndroid ? CheckInLogTypeKey.ANDROID_GATEOUT
                        : CheckInLogTypeKey.TRACTOROUT;
                    addCheckInLog(adid, "Completed", infoId, LogType,
                        createTime, data); // 添加日志 车头日志
                }
            }

            mainRow.add("out_seal", seal);
            mainRow.add("check_out_time", createTime);

            floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新车头表

            if (!isAndroid) {
                addFiles(request, infoId, adid); // 照片
            }

        } catch (Exception e) {
            throw new SystemException(e, "checkInOut", log);
        }

// try{
//
// // 获得创建人
// AdminMgr adminMgr = new AdminMgr();
// AdminLoginBean adminLoginBean =
// adminMgr.getAdminLoginBean(StringUtil.getSession(request));
// long createManId = adminLoginBean.getAdid();
// long ps_id=adminLoginBean.getPs_id();
//
//
//
// long infoId = StringUtil.getLong(request, "id");
// // long search_dlo_id = StringUtil.getLong(request, "search_dlo_id");
// String search_ctnr = StringUtil.getString(request, "ctnrVal");
// String isLive = StringUtil.getString(request, "isLive");
// String seal = StringUtil.getString(request, "seal");
//
// DBRow searchRow=this.findSpotByCtnNo(search_ctnr,ps_id);
// String door="";
// String parks ="";
// long search_dlo_id=0;
// if(searchRow != null){
// search_dlo_id = searchRow.get("dlo_id", 0l);
//
// DBRow[] doors =
// storageDoorMgrZr.getUseDoorByAssociateIdAndType(search_dlo_id,
// ModuleKey.CHECK_IN) ;
// DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
// if(fixDoors != null){
// result.add("doors",fixDoors);
// }
//
// DBRow[] spots =
// storageYardControlZr.getUseSpotByAssociateIdAndType(search_dlo_id,
// ModuleKey.CHECK_IN);
// DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
// if(fixSpots != null){
// result.add("spots",fixSpots);
// }
//
// // 更新主表
// String createTime = DateUtil.NowStr();
//
// if(!door.equals("")) {
// // 不为 0 就是 查询到有效的货柜 需要带走
// if(!search_ctnr.equals("")) {
// floorCheckInMgrZwb.openDoorByInfoId(search_dlo_id,door); // 释放门
// // 清空主单据 占用门
// DBRow row = new DBRow();
// row.add("door_id", null);
// floorCheckInMgrZwb.modCheckIn(search_dlo_id, row);
// } else {
// floorCheckInMgrZwb.openDoorByInfoId(infoId, door); // 释放门
// // 清空主单据 占用门
// DBRow row = new DBRow();
// row.add("door_id", null);
// floorCheckInMgrZwb.modCheckIn(infoId, row);
// }
// this.batchReleaseDoor(door);
// }
// DBRow mainRow = new DBRow();
// mainRow.add("out_seal", seal);
// // 根据主单据 id 查询主单据状态
// mainRow.add("check_out_time", createTime);
//
//
// if(!search_ctnr.equals("")){
// mainRow.add("search_gate_container_no",search_ctnr);
// }
//
// //确定主单据状态
// if(isLive.equals("liveLoad")){
// mainRow.add("status",CheckInMainDocumentsStatusTypeKey.LEFT);
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.LEFT);
//
// }else{
// mainRow.add("tractor_status",CheckInMainDocumentsStatusTypeKey.LEFT);
//
// }
//
// if(search_dlo_id!=0){
// DBRow containerRow=new DBRow();
// containerRow.add("status",CheckInMainDocumentsStatusTypeKey.LEFT);
// floorCheckInMgrZwb.modCheckIn(search_dlo_id,containerRow); //更新货柜表
// getcheckoutByCTNR(request,StringUtil.getLong(request,
// "search_dlo_id"),StringUtil.getString(request,
// "ctnrVal"),StringUtil.getLong(request, "ps_id"));
// }
//
// mainRow.add("dlo_id", infoId);
// String ctnr = search_ctnr;
// // status
// if(!parks.equals("") && !parks.equals("undefined")) {
// String[] str = parks.split(",");
// for(int i = 0; i < str.length; i++){
// long park = Long.parseLong(str[i]);
// DBRow upRow = floorCheckInMgrZwb.findMainBySpot(park,ctnr,infoId);
// long dlo_id = upRow.get("dlo_id", 0l);
// if(dlo_id != infoId) {
// floorCheckInMgrZwb.updateParkingStatus(park, 1); // 释放停车位
// DBRow row = new DBRow();
// row.add("yc_id", null);
// floorCheckInMgrZwb.modCheckIn(dlo_id, row); // 更新主表
// floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新主表
// } else{
// mainRow.add("yc_id", null);
// floorCheckInMgrZwb.updateParkingStatus(park, 1); // 释放停车位
// floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新主表
// }
// }
// } else{
// floorCheckInMgrZwb.modCheckIn(infoId, mainRow); // 更新主表
// }
//
// String data = "";
// data += "Exiting CTNR:" + search_ctnr + ",";
// data += "PickUp Seal:" + seal + ",";
// if(!parks.equals("") && !parks.equals("undefined")){ // 解释清楚
//
// String[] str = parks.split(",");
// data += "Spot: ";
// for(int i = 0; i < str.length; i++){
// long park = Long.parseLong(str[i]);
// DBRow result = floorCheckInMgrZwb.findStorageYardControl(park);
// data += result.get("yc_no", "") + "、";
// }
// if(!data.equals("")){
// data = data.substring(0, data.length()-1);
// }
// data += ",";
// }
// if(!door.equals("") && !door.equals("undefined")){
// String[] str = door.split(",");
// data += "Door: ";
// for(int i = 0; i < str.length; i++){
// long door_name = Long.parseLong(str[i]);
// DBRow result = floorCheckInMgrZwb.findDoorById(door_name);
// data += result.get("doorId", "") + "、";
// }
// if(!data.equals("")){
// data = data.substring(0, data.length()-1);
// }
// data += ",";
// }
// if(!data.equals("")){
// data = data.substring(0, data.length()-1);
// }
//
// addFiles(request, infoId, createManId);
// addCheckInLog(createManId, "Completed", infoId,
// CheckInLogTypeKey.GATEOUT, createTime,data);
//
// }catch(Exception e){
// throw new SystemException(e,"checkInOut",log);
// }
    }

    // 查询出同一个ctnr多余的单据并关闭 wfh
    public void getcheckoutByCTNR(HttpServletRequest request, long dlo_id,
                                  String ctnr, long ps_id) throws Exception {
        try {
            DBRow[] rows = floorCheckInMgrZwb.findSpotByCtnNo(ctnr, ps_id,
                dlo_id);
            if (rows.length > 0) {
                for (DBRow row : rows) {

//
// List<DBRow> dataList = new ArrayList<DBRow>(); //返回的
// DBRow spotRow=new DBRow();
//
// if(row.get("yc_id", 0l)!=0 && row.get("yc_id", 0l)!=-1){
// DBRow ycRow=this.findStorageYardControl(row.get("yc_id",
// 0l));
// spotRow.add("yc_no",ycRow.get("yc_no", ""));
// spotRow.add("yc_id",row.get("yc_id", 0l));
//
//
// }
// DBRow[] door=this.findUseDoorByDloId(row.get("dlo_id",
// 0l));
// for(int i=0;i<door.length;i++){
// DBRow doorRow=new DBRow();
// doorRow.add("door_name",door[i].get("doorId", ""));
// doorRow.add("door_id",door[i].get("sd_id", 0l));
// doorRow.add("dlo_detail_id",door[i].get("dlo_detail_id",
// 0l));
// dataList.add(doorRow);
// }

                    DBRow[] doors = storageDoorMgrZr
                        .getUseDoorByAssociateIdAndType(
                            row.get("dlo_id", 0l), ModuleKey.CHECK_IN);
                    DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);

                    if (fixDoors != null) {
// result.add("doors",fixDoors);
                        row.add("doors", fixDoors);
                    }

                    DBRow[] spots = storageYardControlZr
                        .getUseSpotByAssociateIdAndType(
                            row.get("dlo_id", 0l), ModuleKey.CHECK_IN);
                    DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
                    if (fixSpots != null) {
// result.add("spots",fixSpots);
                        row.add("spots", fixSpots);
                    }

// 查询货货柜占的门
// 调用关闭的方法
                    this.checkOutCTNR(request, row);

                }
            }

        } catch (Exception e) {
            throw new SystemException(e, "getcheckoutByCTNR", log);
        }

    }

    // 关闭ctnr其他的多余的单据
    @Override
    public void checkOutCTNR(final HttpServletRequest request, DBRow data)
        throws Exception {
        try {
            final DBRow _row = data;
            sqlServerTransactionTemplate
                .execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(
                        TransactionStatus txStat) {
                        try {
// 获得创建人
                            AdminMgr adminMgr = (AdminMgr) MvcUtil
                                .getBeanFromContainer("adminMgr");
                            AdminLoginBean adminLoginBean = adminMgr
                                .getAdminLoginBean(StringUtil
                                    .getSession(request));
                            long createManId = adminLoginBean.getAdid();

                            DBRow[] doors = (DBRow[]) _row
                                .getValue("doors");
                            DBRow[] spots = (DBRow[]) _row
                                .getValue("spots");

                            long dloId = _row.get("dlo_id", 0L);
// int relType =_row.get("rel_type", 0);
// String search_ctnr
// =_row.get("gate_container_no", "");

                            DBRow mainRow = new DBRow();
                            mainRow.add("status",
                                CheckInMainDocumentsStatusTypeKey.LEFT);
                            mainRow.add("tractor_status",
                                CheckInMainDocumentsStatusTypeKey.LEFT);

                            if (doors != null && doors.length > 0) {
                                for (DBRow row : doors) {
// floorCheckInMgrZwb.openDoorByInfoId(dloId,row.get("door_id",
// "").equals("")?row.get("sd_id",
// ""):row.get("door_id", "")); // 释放门
                                    DBRow _row = new DBRow();
                                    _row.add("occupancy_status",
                                        OccupyStatusTypeKey.RELEASED);
                                    floorCheckInMgrZwb
                                        .updateDetailsDoorOrSpot(dloId,
                                            OccupyTypeKey.DOOR,
                                            row.get("DOOR_ID", 0L),
                                            _row);
                                    storageDoorMgrZr.freeDoor(
                                        row.get("DOOR_ID", 0L),
                                        createManId, dloId,
                                        ModuleKey.CHECK_IN);
                                }
                            }
                            if (spots != null && spots.length > 0) {
// mainRow.add("yc_id", null);
                                for (DBRow row : spots) {
// 释放停车位
                                    DBRow _row = new DBRow();
                                    _row.add(
                                        "occupancy_status",
                                        CheckInSpotStatusTypeKey.RELEASED);
                                    floorCheckInMgrZwb
                                        .updateDetailsDoorOrSpot(dloId,
                                            OccupyTypeKey.SPOT,
                                            row.get("YC_ID", 0L),
                                            _row);
// floorCheckInMgrZwb.updateParkingStatus(park.get("yc_id",
// 0L), 1);
                                }
                            }

                            String createTime = DateUtil.NowStr();
                            mainRow.add("check_out_time", createTime);
                            floorCheckInMgrZwb.modCheckIn(dloId, mainRow);

// 日志
                            String data = "";
                            if (spots != null && spots.length > 0) {
                                data += "Spot: ";
// data += park.get("yc_no", 0L);
// data += ",";
                                for (DBRow row : spots) {
                                    data += row.get("YC_NAME", "");
                                    data += "、";
                                }

                                data = data.substring(0, data.length() - 1);
                                data += ",";

                            }

                            if (doors != null && doors.length > 0) {
                                data += "Door: ";
                                for (DBRow row : doors) {
                                    data += row.get("DOOR_NAME", "");
                                    data += "、";
                                }

                                data = data.substring(0, data.length() - 1);
                                data += ",";

                            }

                            if (!data.equals("")) {
                                data = data.substring(0, data.length() - 1);
                            }

                            addFiles(request, dloId, createManId);
                            addCheckInLog(createManId, "Completed", dloId,
                                CheckInLogTypeKey.ANDROID_GATEOUT,
                                createTime, data);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
        } catch (Exception e) {
            throw new SystemException(e, "checkOutCTNR", log);
        }

    }

    @Override
    public DBRow[] findAllZone(long psId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findAllZone(psId);
        } catch (Exception e) {
            throw new SystemException(e, "findAllZone", log);
        }
    }

    /**
     * 整个方法是通过zoneId 去查询这个Zone下面可以用的门 如果没有zoneId 那么查询所有的
     * <p>
     * flag = 0 ,1,2 1:返回最小的一个单数. 2:表示返回最小的一个双数.
     * <p>
     * 如果没有单数最小的那么就返回一个双数最小的 DBRow[] returnArrays = null ;
     * <p>
     * if(zoneId == 0){ returnArrays = all( free + 单据可用的) ; } returnArrays =
     * query(zoneId) (当前zoneId可用的 + 单据可用的); if(flag != 0 && returnArrays.length
     * > 0 ){ if(flag == 1){ 返回一条数据(单数最小的 ？单数最小的:双数最小的) }else{
     * 返回一条数据(双数最小的？双数最小的：单数最小的) } } zhangrui 2014-11-15
     */
    @Override
    public DBRow[] findDoorsByZoneId(long zoneId, long mainId, long ps_id,
                                     long flag) throws Exception {
        try {
/**
 * return
 * this.floorCheckInMgrZwb.findDoorsByZoneId(zoneId,mainId,ps_id
 * ,flag) ;(原有代码)
 */
            DBRow[] returnArray = storageDoorMgrZr.getAvailableDoorsByZoneId(
                ps_id, zoneId, mainId, ModuleKey.CHECK_IN);
            if (flag != 0 && returnArray != null && returnArray.length > 0) { // 那么肯定有一个奇数最小，或者是奇数最大的
                HoldDoubleValue<DBRow, DBRow> minSingleAndMinDouble = storageDoorMgrZr
                    .getMinSinleAndMinDoubleBySortedArray(returnArray);
                if (flag == 1) {
                    return new DBRow[]{minSingleAndMinDouble.a != null ? minSingleAndMinDouble.a
                        : minSingleAndMinDouble.b};
                }
                if (flag == 2) {
                    return new DBRow[]{minSingleAndMinDouble.a != null ? minSingleAndMinDouble.b
                        : minSingleAndMinDouble.a};
                }
            }
            return returnArray;
        } catch (Exception e) {
            throw new SystemException(e, "findDoorsByZoneId", log);
        }
    }

    @Override
    public long addCheckInLog(long operator_id, String note, long dlo_id,
                              long log_type, String operator_time, String data) throws Exception {
        try {
            DBRow row = new DBRow();
            row.add("operator_id", operator_id);
            row.add("note", note);
            row.add("dlo_id", dlo_id);
            row.add("log_type", log_type);
            if(operator_time == null || "".equals(operator_time)){
            	 row.add("operator_time", DateUtil.NowStr());
            }
            else{
            	 row.add("operator_time", operator_time);
            }
            row.add("data", data);

            return this.floorCheckInMgrZwb.addCheckInLog(row);
        } catch (Exception e) {
            throw new SystemException(e, "addCheckInLog", log);
        }
    }

    @Override
    public DBRow[] findCheckInLog(long dlo_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findCheckInLog(dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "findCheckInLog", log);
        }
    }

    // web 根据货柜号查询单据
    public DBRow getUseDoorOrSpot(String container_no, long ps_id)
        throws Exception {
        try {

            DBRow row = this.findSpotByCtnNo(container_no, ps_id);
            if (row != null) {
                long mainId = row.get("dlo_id", 0l);
                DBRow result = new DBRow();
                DBRow[] doors = storageDoorMgrZr
                    .getUseDoorByAssociateIdAndType(mainId,
                        ModuleKey.CHECK_IN);
                DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
                if (fixDoors != null) {
                    result.add("doors", fixDoors);
                }

                DBRow[] spots = storageYardControlZr
                    .getUseSpotByAssociateIdAndType(mainId,
                        ModuleKey.CHECK_IN);
                DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
                if (fixSpots != null) {
                    result.add("spots", fixSpots);
                }

                return result;
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "getUseDoorOrSpot", log);
        }
    }

    // web 根据货柜号查询单据所有单据
    public DBRow[] getAllDoorOrSpot(String container_no, long ps_id)
        throws Exception {
        try {

            DBRow[] rows = floorCheckInMgrZwb.findAllSpotByCtnNo(container_no,
                ps_id);

            if (rows.length > 0) {

                for (DBRow row : rows) {
                    List<DBRow> dataList = new ArrayList<DBRow>(); // 返回的
                    DBRow spotRow = new DBRow();
                    if (row.get("yc_id", 0l) != 0 && row.get("yc_id", 0l) != -1) {
                        DBRow ycRow = this.findStorageYardControl(row.get(
                            "yc_id", 0l));
                        spotRow.add("yc_no", ycRow.get("yc_no", ""));
                        spotRow.add("yc_id", row.get("yc_id", 0l));

                    }

                    DBRow[] door = this.findUseDoorByDloId(row
                        .get("dlo_id", 0l));

                    for (int i = 0; i < door.length; i++) {
                        long sdId = door[i].get("sd_id", 0L);
                        DBRow doorRow = new DBRow();
                        doorRow.add("door_name", door[i].get("doorId", ""));
                        doorRow.add("door_id", door[i].get("sd_id", 0l));
                        doorRow.add("dlo_detail_id",
                            door[i].get("dlo_detail_id", 0l));
                        boolean flag = true;
                        for (int j = 0; j < dataList.size(); j++) {
                            flag = true;
                            if (dataList.get(j).get("door_id", 0L) == sdId) {
                                flag = false;
                                break;
                            }
                        }
                        if (flag) {
                            dataList.add(doorRow);
                        }
                    }
// 查询货货柜占的门
                    row.add("spot", spotRow);
                    row.add("door",
                        dataList.toArray(new DBRow[dataList.size()]));
                }
            }

            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "getAllDoorOrSpot", log);
        }
    }

    // android 根据货柜号查询占用的门和停车位
    public DBRow androidGetUseDoorOrSpot(String container_no, long ps_id)
        throws Exception {
        try {
            DBRow result = new DBRow();
            DBRow row = this.findSpotByCtnNo(container_no, ps_id);
            List<DBRow> dataList = new ArrayList<DBRow>(); // 返回的
// DBRow spotRow=new DBRow();
            if (row != null) {
                long dlo_id = row.get("dlo_id", 0l);
                DBRow[] doors = storageDoorMgrZr
                    .getUseDoorByAssociateIdAndType(dlo_id,
                        ModuleKey.CHECK_IN);
                DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
                if (fixDoors != null) {
                    result.add("doors", fixDoors);
                }

                DBRow[] spots = storageYardControlZr
                    .getUseSpotByAssociateIdAndType(dlo_id,
                        ModuleKey.CHECK_IN);
                DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
                if (fixSpots != null) {
                    result.add("spots", fixSpots);
                }
                result.add("dlo_id", dlo_id);
/*
* 原有代码2014-11-17 if(row.get("yc_id", 0l)!=0 && row.get("yc_id",
* 0l)!=-1){ DBRow
* ycRow=this.findStorageYardControl(row.get("yc_id", 0l));
* spotRow.add("yc_no",ycRow.get("yc_no", ""));
* spotRow.add("yc_id",row.get("yc_id", 0l));
*
* } spotRow.add("dlo_id",row.get("dlo_id", 0l)); DBRow[]
* rows=this.findUseDoorByDloId(row.get("dlo_id", 0l));
*
* for(int i=0;i<rows.length;i++){ long sdId =
* rows[i].get("sd_id", 0L); DBRow doorRow=new DBRow();
* doorRow.add("door_name",rows[i].get("doorId", ""));
* doorRow.add("door_id",rows[i].get("sd_id", 0l));
* doorRow.add("dlo_detail_id",rows[i].get("dlo_detail_id",
* 0l)); boolean flag = true; for(int
* j=0;j<dataList.size();j++){ flag = true;
*
* if(dataList.get(j).get("door_id", 0L)==sdId){ flag = false;
* break; } } if(flag){ dataList.add(doorRow); } }
*/
            }
// 查询货货柜占的门
/*
* 2014-11-17 原有代码 result.add("spots",spotRow);
* result.add("doors",dataList.toArray(new DBRow[dataList.size()]));
*/
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "androidGetUseDoorOrSpot", log);
        }
    }

    // android check out
    public void androidCheckInOut(final HttpServletRequest request, long ps_id,
                                  long infoId, String isLive, String seal, long adid, String ctnr_no,
                                  String filePath) throws Exception {
        try {
// 这里调用zwb的方法。文件的android 单独的处理
            boolean isAndroid = true;
            checkInOut(infoId, ctnr_no, isLive, seal, adid, ps_id, isAndroid,
                request);
            if (!StringUtil.isBlank(filePath)) {
                String fileDescBasePath = Environment.getHome()
                    + "upload/check_in/";
                File dir = new File(filePath);
                if (dir.isDirectory()) {
                    File[] files = dir.listFiles();
                    for (File file : files) {
                        addFile("gate_check_out_" + infoId + "_"
                                + file.getName(), infoId,
                            FileWithTypeKey.OCCUPANCY_MAIN, 0, adid,
                            DateUtil.NowStr());
// FileUtil.copyProductFile(file, fileDescBasePath);
                        FileUtil.checkInAndroidCopyFile(file, "gate_check_out_"
                            + infoId + "_", fileDescBasePath);

                    }
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "checkInOut", log);
        }
    }

/*
* private DBRow webReleaseDoorsResult;
*
* @Override public DBRow modOccupancyDetailsNumberStatus(final
* HttpServletRequest request)throws Exception{ try { webReleaseDoorsResult
* = null; sqlServerTransactionTemplate .execute(new
* TransactionCallbackWithoutResult() {
*
* @Override protected void doInTransactionWithoutResult(TransactionStatus
* txStat) { try { long main_id = StringUtil.getLong(request, "main_id");
* String number = StringUtil.getString(request, "number"); int flag =
* StringUtil.getInt(request, "flag"); int status =
* StringUtil.getInt(request, "status"); int occupancy_status =
* StringUtil.getInt(request, "occupancy_status"); long doorId =
* StringUtil.getLong(request, "doorId"); String exceptionNote =
* StringUtil.getString(request, "note"); DBRow[] details =
* floorCheckInMgrZwb.selectDetails(doorId, main_id); AdminLoginBean
* adminLoginBean =
* (AdminLoginBean)request.getSession().getAttribute(Config.adminSesion);
* long ps_id = adminLoginBean.getPs_id(); // 在单子已经被关闭的情况，不再因为多次请求再次提交关闭日志
* boolean relFlag = true; for (int i = 0; i < details.length; i++) {
*
* if(flag==1 && number.equals(details[i].getString("number")) &&
* (details[i].get("number_status", 0)==3 || details[i].get("number_status",
* 0)==4 || details[i].get("number_status", 0)==5)) { relFlag = false; }
* if(flag!=1 && number.equals(details[i].getString("number")) &&
* (details[i].get("number_status", 0)==3 || details[i].get("number_status",
* 0)==4 || details[i].get("number_status", 0)==5)) { relFlag = false; } }
* webReleaseDoorsResult =
* floorCheckInMgrZwb.modOccupancyDetailsNumberStatus
* (main_id,number,flag,status,occupancy_status,doorId,ps_id,exceptionNote);
* if(occupancy_status>0) { DBRow main =
* floorCheckInMgrZwb.selectMainByEntryId(main_id); if(main != null &&
* main.get("door_id", 0) == doorId) { DBRow row = new DBRow();
* row.add("door_id", null); floorCheckInMgrZwb.modCheckIn(main_id, row); }
* } DBRow noticeEntryId = findWaitingListByDoorId(doorId);
*
* // 判断是否有人在等在门 if(noticeEntryId != null) { long entry_id =
* noticeEntryId.get("entry_id", 0l); // 创建时间 Date date =
* (Date)Calendar.getInstance().getTime(); SimpleDateFormat dateformat = new
* SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String createTime =
* dateformat.format(date); // 获得当前登录人 long createManId =
* adminLoginBean.getAdid(); String userName=adminLoginBean.getAccount(); //
* 发通知通知门已经好了 entry_id String emailTitle = "CHECK IN WINDOW WAITING"; String
* context = "DOOR:【" + doorId + "】Ready,单据" + entry_id + "可以去该门操作"; //
* scheduleMgrZr.addScheduleByExternalArrangeNow(createTime, "",
* createManId, Long.toString((createManId)), "", entry_id,
* ModuleKey.CHECK_IN_WAITING, emailTitle, context, true, true, true,
* request, true, ProcessKey.CHECK_IN); } // 添加日志 // 创建时间 Date date =
* (Date)Calendar.getInstance().getTime(); SimpleDateFormat dateformat = new
* SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); String createTime =
* dateformat.format(date); DBRow storage =
* floorCheckInMgrZwb.findDoorById(doorId); String door_name =
* storage.getString("doorId"); // 获得当前登录人 long createManId =
* adminLoginBean.getAdid(); String data = ""; String note = "";
* if(occupancy_status > 0) { note = "Released Door";
* if(!door_name.equals("")) { data += "DOOR: " + door_name + ","; }
* if(!data.equals("")) { data = data.substring(0, data.length()-1); }
* addCheckInLog(createManId, note, main_id, CheckInLogTypeKey.DOCK_CLOSE,
* createTime, data); } else { if(relFlag) {
*
* if(!number.equals("")) { if(flag == 1) { note = "Closed LOAD"; data +=
* "LOAD: " + number + ","; } else { note = "Closed CTNR/BOL"; data +=
* "CTNR/BOL: " + number + ","; } } if(!data.equals("")) { data =
* data.substring(0, data.length()-1); } addCheckInLog(createManId, note,
* main_id, CheckInLogTypeKey.DOCK_CLOSE, createTime, data); } } } catch
* (Exception e) { throw new RuntimeException(e); } } }); return
* webReleaseDoorsResult; }catch (Exception e) { throw new
* SystemException(e, "modOccupancyDetailsNumberStatus", log); } }
*/

    /**
     * 回写receipts的inYard时间
     *
     * @param entryId
     * @throws Exception
     */
    public void updateReceiptsByCtnOrBols(long entryId) throws Exception {

        DBRow[] bols = floorCheckInMgrZwb.findEntryDetailByEntryIdType(entryId,
            ModuleKey.CHECK_IN_BOL);
        DBRow entryRow = floorCheckInMgrZwb.findGateCheckInById(entryId);
        for (int i = 0; i < bols.length; i++) {
            String bol = bols[i].getString("number");
            String gateCheckInTime = null != entryRow ? entryRow
                .getString("gate_check_in_time") : "";
            String gateCheckInDate = DateUtil.showLocalTime(gateCheckInTime,
                null != entryRow ? entryRow.get("ps_id", 0L) : 0L);
            sqlServerMgrZJ.updateReceiptInYardDateByBolOrCtnrNo(
                gateCheckInDate, bol, null,
                bols[i].getString("supplier_id"),
                bols[i].getString("company_id"), 0L, null);
        }
        DBRow[] ctns = floorCheckInMgrZwb.findEntryDetailByEntryIdType(entryId,
            ModuleKey.CHECK_IN_CTN);
// .findContainerNoDetailsByEntryId(entryId);
        for (int i = 0; i < ctns.length; i++) {
            String ctn = ctns[i].getString("number");
            String gateCheckInTime = null != entryRow ? entryRow
                .getString("gate_check_in_time") : "";
            String gateCheckInDate = DateUtil.showLocalTime(gateCheckInTime,
                null != entryRow ? entryRow.get("ps_id", 0L) : 0L);
            sqlServerMgrZJ.updateReceiptInYardDateByBolOrCtnrNo(
                gateCheckInDate, null, ctn,
                ctns[i].getString("supplier_id"),
                ctns[i].getString("company_id"), 0L, null);
        }
    }

    /**
     * 条件过滤
     */
    @Override
    public DBRow[] filterCheckIn(String entryType, String start_time,
                                 String end_time, String mainStatus, String tractorStatus,
                                 String numberStatus, String waitingType, String comeStatus,
                                 String purposeStatus, long ps_id, long loadBar, long priority,
                                 PageCtrl pc, String customer) throws Exception {

        try {

            if (!start_time.equals("")) {
                start_time = DateUtil.showUTCTime(
                    DateUtil.StrtoYYMMDDHHMMSS(start_time), ps_id);
            }
            if (!end_time.equals("")) {
                end_time = DateUtil.showUTCTime(
                    DateUtil.StrtoYYMMDDHHMMSS(end_time), ps_id);
            }
            return floorCheckInMgrZwb.filterCheckIn(entryType, start_time,
                end_time, mainStatus, tractorStatus, numberStatus,
                waitingType, comeStatus, purposeStatus, ps_id, loadBar,
                priority, customer, pc);
        } catch (Exception e) {
            throw new SystemException(e, "filterCheckIn", log);
        }
    }

    // gateCheckOut 查询没有释放的停车位和门
    @Override
    public DBRow findParkAndDoorByEntryId(long mainId) throws Exception {
        try {
            DBRow result = new DBRow();
            DBRow[] doors = storageDoorMgrZr.getUseDoorByAssociateIdAndType(
                mainId, ModuleKey.CHECK_IN);
            DBRow[] fixDoors = fixAndroidGateCheckOutOccupiedAndRDoorInfo(doors);
            if (fixDoors != null) {
                result.add("doors", fixDoors);
            }

            DBRow[] spots = storageYardControlZr
                .getUseSpotByAssociateIdAndType(mainId, ModuleKey.CHECK_IN);
            DBRow[] fixSpots = fixAndroidGateCheckOutAccupiedAndSpotInfo(spots);
            if (fixSpots != null) {
                result.add("spots", fixSpots);
            }

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "filterCheckIn", log);
        }
    }

// >>>>>>>>>>>>>>>>>>>>>>>>>>张睿添加<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public DBRow countDloId(long dlo_id, AdminLoginBean loginBean)
        throws Exception {
        try {
            DBRow row = new DBRow();
            DBRow mainRow = this.floorCheckInMgrZwb.findGateCheckInById(dlo_id);

            CheckInEntryPermissionUtil.checkEntry(mainRow, loginBean);
            boolean isLeft = CheckInEntryPermissionUtil.isEntryLeft(mainRow);
            if (isLeft) {
                throw new CheckInEntryIsLeftException();
            }
            DBRow[] rows = this.floorCheckInMgrZwb.findloadingByInfoId(dlo_id);
            if (rows != null && rows.length > 0) {
                for (int i = 0; i < rows.length; i++) {
// if(!rows[i].getString("load_number").equals("")){
// rows[i].add("bill",rows[i].getString("load_number"));
// rows[i].add("type","load");
// rows[i].remove("load_number");
// }else if(!rows[i].getString("ctn_number").equals("")){
// rows[i].add("bill",rows[i].getString("ctn_number"));
// rows[i].remove("ctn_number");
// rows[i].add("type","ctn");
// }else if(!rows[i].getString("bol_number").equals("")){
// rows[i].add("bill",rows[i].getString("bol_number"));
// rows[i].remove("bol_number");
// rows[i].add("type","bol");
// }else if(!rows[i].getString("others_number").equals("")){
// rows[i].add("bill",rows[i].getString("others_number"));
// rows[i].remove("others_number");
// rows[i].add("type","other");
// }
                    if (rows[i].get("rl_id", 0l) != 0) {
                        DBRow doorRow = this.floorCheckInMgrZwb
                            .findDoorById(rows[i].get("rl_id", 0l));
                        if (doorRow != null) {
                            rows[i].add("door_name", doorRow.get("doorId", ""));
                        }
                    }
                }
                row.add("rel_type", mainRow.get("rel_type", 0l));
                row.add("status", mainRow.get("status", 0l));
                row.add("tractor_status", mainRow.get("tractor_status", 0l));
                row.add("is_live", mainRow.get("isLive", 0l));
                row.add("detail", rows);
                long count = floorCheckInMgrZwb
                    .countDoorOrLocationOccupancyMainBy(dlo_id);
                row.add("count", count);
                row.add("wash_time",
                    mainRow.get("warehouse_check_in_operate_time", ""));
            }
            return row;
        } catch (CheckInEntryIsLeftException e) {
            throw e;
        } catch (CheckInNotFoundException e) {
            throw e;
        } catch (NoPermiessionEntryIdException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "countDloId(long dlo_id)", log);
        }
    }

// 扫描主单据id查询为释放的门
// public DBRow[]

    @Override
    public DBRow getDoorOrLocationOccupancyMainById(long dlo_id,
                                                    HttpServletRequest request) throws Exception {
        try {
            DBRow mainRecord = floorCheckInMgrZwb
                .getDoorOrLocationOccupancyMainBy(dlo_id);
            if (mainRecord != null) {
                mainRecord.add("appointment_time",
                    mainRecord.getString("appointment_time") + "");
                mainRecord
                    .add("gate_check_in_operate_time",
                        mainRecord
                            .getString("gate_check_in_operate_time")
                            + "");
                mainRecord.add("window_check_in_operate_time",
                    mainRecord.getString("window_check_in_operate_time")
                        + "");
                mainRecord.add("window_check_in_time",
                    mainRecord.getString("window_check_in_time") + "");
                mainRecord.add("create_time", mainRecord
                    .getString("create_time" + ""));
                mainRecord.add("warehouse_check_in_time", mainRecord
                    .getString("warehouse_check_in_time" + ""));
                mainRecord.add("warehouse_check_in_operate_time",
                    mainRecord.getString("warehouse_check_in_operate_time"
                        + ""));

            }
// 查询detail中详细,详细要包含 notify的信息
            if (mainRecord != null && mainRecord.get("dlo_id", 0l) != 0l) {
                DBRow[] details = floorCheckInMgrZwb
                    .getDetailByMainRecordId(dlo_id);
                if (details != null && details.length > 0) {
                    for (DBRow detail : details) {

                        String doorName = detail.getString("doorId");
                        long sd_id = detail.get("sd_id", 0l);
                        detail.add("doorId", sd_id);
                        detail.add("doorName", doorName);
                        detail.add("yc_id", detail.get("yc_id", 0l));
// detail.add("type",type);
                        detail.remove("occupancy_type");
                        detail.remove("dlo_detail_id");
                        detail.remove("dlo_id");
                        detail.remove("rl_id");
                        detail.remove("sd_id");
                    }
                    mainRecord.add("windowCheckInitems", details);
                }
            }
            DBRow[] rows = this.findAllLoadByMainId(dlo_id);
            List<DBRow> detaillist = new ArrayList<DBRow>();
            if (rows != null && rows.length > 0) {
                for (int i = 0; i < rows.length; i++) {
                    DBRow[] row = null;

                    row = this.findWindowNotices(
                        rows[i].get("dlo_detail_id", 0l), -1,
                        ProcessKey.CHECK_IN_WAREHOUSE);

                    for (int a = 0; a < row.length; a++) {
                        DBRow detail = new DBRow();
                        detail.add("name", row[a].getString("employe_name"));
                        detail.add("detail",
                            row[a].getString("schedule_detail"));
                        detaillist.add(detail);
                    }
                }
                mainRecord.add("warehouseCheckInitems",
                    detaillist.toArray(new DBRow[detaillist.size()]));
            }

// 添加Close的信息
// mainRecord.add("closeInfos",
// setCheckInDetalCloseInfos(mainRecord));
            return mainRecord;
        } catch (Exception e) {
            throw new SystemException(e,
                "getDoorOrLocationOccupancyMainById(long dlo_id)", log);
        }
    }

    private DBRow[] setCheckInDetalCloseInfos(DBRow row) throws Exception {
        DBRow[] details = this.selectDetails(0, row.get("dlo_id", 01));
        if (details != null && details.length > 0) {
            flag:
            for (int i = 0; i < details.length; i++) {
                for (int a = 0; a < i; a++) {
                    if (details[i].getString("doorId").equals(
                        details[a].getString("doorId"))) {
                        continue flag;
                    }
                }
                if (!details[i].getString("doorId").equals("")) {
// DBRow[] numbers =
// selectDetails(details[i].get("rl_id",01),row.get("dlo_id",01));
// details[i].add("numbers", numbers);
                }
            }
        }
        return details;
    }

    public void setNotifyValue(DBRow detail, DBRow[] notifys) {
        StringBuffer userIds = new StringBuffer();
        StringBuffer userNames = new StringBuffer();
        int emailFlag = 0;
        int snsFlag = 0;
        int pagerFlag = 0;
        String comment = "";

        if (notifys != null && notifys.length > 0) {
            for (DBRow notify : notifys) {
                userIds.append(",").append(notify.getString("adid"));
                userNames.append(",").append(notify.getString("employe_name"));
                if (!StringUtil.isNull(notify.getString("schedule_detail"))) {
                    String[] arrays = notify.getString("schedule_detail")
                        .split("Note：");
                    if (arrays.length > 1) {
                        comment = arrays[1];
                    }
                }
                emailFlag = StringUtil.getInt(
                    notify.getString("sms_email_notify"), 0);
                snsFlag = StringUtil.getInt(
                    notify.getString("sms_short_notify"), 0);
                pagerFlag = StringUtil.getInt(
                    notify.getString("schedule_is_note"), 0);
            }
            detail.add("userIds", userIds.length() > 1 ? userIds.substring(1)
                : "");
            detail.add("userNames",
                userNames.length() > 1 ? userNames.substring(1) : "");
            detail.add("email", emailFlag);
            detail.add("sns", snsFlag);
            detail.add("pager", pagerFlag);
            detail.add("comment", comment);

        }
    }

    // 验证gps 是否存在
    public DBRow androidVerificationGps(String gps_tracker) throws Exception {
        try {
            DBRow resuleRow = new DBRow();
            DBRow row = floorCheckInMgrZwb.findAssetByGPSNumber(gps_tracker);
            if (row == null) {
                resuleRow.add("gps", "no");
            } else {
                resuleRow.add("gps", "yes");
            }
            return resuleRow;
        } catch (Exception e) {
            throw new SystemException(e, "androidVerificationGps", log);
        }

    }

    // android 查询被占用门
    @Override
    public DBRow[] getCheckInGetOccupyDoor(long ps_id) throws Exception {
        try {
            DBRow[] useDoors = selectUseDoor(ps_id, 0);
            if (useDoors != null && useDoors.length > 0) {
                for (DBRow door : useDoors) {
                    door.remove("dlo_detail_id");
                    door.remove("occupancy_type");
                    door.remove("rl_id");
                    door.remove("occupancy_status");
                    door.remove("dlo_id");
                    door.remove("ps_id");
                    DBRow[] detailes = selectUseDoorDetail(door
                        .get("sd_id", 0l));
                    door.add("detailes", detailes);
                    if (detailes != null) {
                        for (DBRow t : detailes) {
                            t.remove("dlo_detail_id");
                            t.remove("occupancy_type");
                            t.remove("rl_id");
                            t.remove("occupancy_status");
                            t.remove("dlo_id");
                            t.add("appointment_time",
                                t.getString("appointment_time"));
                            t.add("gate_check_in_operate_time",
                                t.getString("gate_check_in_operate_time"));
                        }
                    }
                }
            }
            return useDoors;
        } catch (Exception e) {
            throw new SystemException(e, "getCheckInGetOccupyDoor(long ps_id)",
                log);
        }
    }

    @Override
    public void addWindowCheckInByAndroid(DBRow dataRow,
                                          HttpServletRequest request) throws Exception {
        try {
// 创建时间
            Date date = (Date) Calendar.getInstance().getTime();
            SimpleDateFormat dateformat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
            String createTime = dateformat.format(date);

            AdminMgr adminMgr = (AdminMgr) MvcUtil
                .getBeanFromContainer("adminMgr");
            AdminLoginBean adminLoginBean = adminMgr
                .getAdminLoginBean(StringUtil.getSession(request));
            long createManId = adminLoginBean.getAdid();

            long dlo_id = dataRow.get("dlo_id", 0l);
            DBRow mainRow = new DBRow();

            mainRow.add("window_check_in_time", createTime);
            mainRow.add("window_check_in_operator", createManId);
            mainRow.add("window_check_in_operate_time", createTime);
            mainRow.add("dlo_id", dlo_id);
            this.floorCheckInMgrZwb.modCheckIn(dlo_id, mainRow); // 更新主表
            List<DBRow> items = (ArrayList<DBRow>) dataRow.get("details",
                new ArrayList<DBRow>());
            for (DBRow detail : items) {
                String ids = detail.getString("userIds"); // 要通知的人 名字 字符串
                String beizhu = detail.getString("comment"); // 备注
                DBRow doorRow = this.floorCheckInMgrZwb.findDoorById(detail
                    .get("doorId", 0l));

                DBRow row = new DBRow();
                row.add("occupancy_type", 1);
                row.add("rl_id", detail.get("doorId", 0l));
                row.add("occupancy_status", 1);
                row.add("dlo_id", dlo_id);
                row.add("number_status", 1);
                String context = "";
// 因为android都是一个字段loadId
                long loadId = detail.get("loadId", 0l);
                String type = detail.getString("type");
                if (type.trim().equalsIgnoreCase("ctn")) {
                    row.add("ctn_number", detail.get("loadId", 0l));
                    context += "Delivery:【CTNR:" + detail.get("loadId", 0l)
                        + "】From DOOR:【" + doorRow.getString("doorId")
                        + "】DOOR Note：" + beizhu;
                } else if (type.trim().equalsIgnoreCase("other")) {
                    row.add("others_number", detail.get("loadId", 0l));
                    context += "Delivery:【OTHER:" + detail.get("loadId", 0l)
                        + "】From DOOR:【" + doorRow.getString("doorId")
                        + "】DOOR Note：" + beizhu;
                } else if (type.trim().equalsIgnoreCase("bol")) {
                    row.add("bol_number", detail.get("loadId", 0l));
                    context += "Delivery:【BOL:" + detail.get("loadId", 0l)
                        + "】From DOOR:【" + doorRow.getString("doorId")
                        + "】DOOR Note：" + beizhu;
                } else if (type.trim().equalsIgnoreCase("load")) {
                    row.add("load_number", detail.get("loadId", 0l));
                    context += "Pick Up:【LOAD:" + detail.get("loadId", 0l)
                        + "】From DOOR:【" + doorRow.getString("doorId")
                        + "】DOOR Note：" + beizhu;
                }
                long id = this.floorCheckInMgrZwb.addOccupancyDetails(row); // 添加详细数据
                String emailTitle = "CHECK IN WINDOW";

                Boolean yj = true;
                Boolean dx = true;
                Boolean ym = true;
                if (detail.get("email", 0) == 0) {
                    yj = false;
                }
                if (detail.get("sns", 0) == 0) {
                    dx = false;
                }
                if (detail.get("pager", 0) == 0) {
                    ym = false;
                }
                if (type.trim().equalsIgnoreCase("ctn")) {
// scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,ids,"",loadId,
// ModuleKey.CHECK_IN_CTN, emailTitle, context, ym, yj,
// dx,request,true,ProcessKey.CHECK_IN);
                } else if (type.trim().equalsIgnoreCase("load")) {
// scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,ids,"",loadId,
// ModuleKey.CHECK_IN_LOAD, emailTitle, context, ym, yj,
// dx,request,true,ProcessKey.CHECK_IN);
                } else if (type.trim().equalsIgnoreCase("other")) {
// scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,ids,"",loadId,
// ModuleKey.CHECK_IN_ORTHERS, emailTitle, context, ym, yj,
// dx,request,true,ProcessKey.CHECK_IN);
                } else if (type.trim().equalsIgnoreCase("bol")) {
// scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,ids,"",loadId,
// ModuleKey.CHECK_IN_BOL, emailTitle, context, ym, yj,
// dx,request,true,ProcessKey.CHECK_IN);
                }
            }
        } catch (Exception e) {
            throw new SystemException(e,
                "addWindowCheckInByAndroid(DBRow dataRow)", log);
        }
    }

    // 查询通知 反填(张睿)
    public DBRow[] findNotice(long id, String type) throws Exception {
        try {
            DBRow[] rows;
            if (type.equals("ctn")) {
// 查询ctn 通知
// rows=scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(id,
// ModuleKey.CHECK_IN_CTN, ProcessKey.CHECK_IN);
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_CTN, ProcessKey.CHECK_IN);
            } else if (type.equals("load")) {
// 查询load通知
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_LOAD, ProcessKey.CHECK_IN);
            } else if (type.equals("bol")) {
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_BOL, ProcessKey.CHECK_IN);
            } else {
// rows=this.floorCheckInMgrZwb.findNoticeDedetail(id,
// ModuleKey.CHECK_IN_ORTHERS, ProcessKey.CHECK_IN);
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "findNotice", log);
        }
    }

    private static SimpleDateFormat dateformat = new SimpleDateFormat(
        "MM-dd HH:mm");

    private String formateTime(String dateString) {
        String returnValue = "";
        try {
            if (StringUtil.isNull(dateString)) {
                return returnValue;
            }
            Date d = DateFormat.getDateTimeInstance().parse(dateString);
            returnValue = dateformat.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    @Override
    public DBRow findWaitingListByDoorId(long doorId) throws Exception {
        try {
            return floorCheckInMgrZwb.findWaitingListByDoorId(doorId);
        } catch (Exception e) {
            throw new SystemException(e, "findWaitingListByDoorId", log);
        }
    }

    // 释放门的时候 查询如果有等待的门 通知他该门可以用
    public long findZoneByDoorId(HttpServletRequest request) throws Exception { // 留着android
// 用
        try {
            long door_id = StringUtil.getLong(request, "door_id");
            DBRow row = this.floorCheckInMgrZwb.findZoneByDoorId(door_id);
            if (row != null) { // 如果查到 就通知
// 创建时间
                Date date = (Date) Calendar.getInstance().getTime();
                SimpleDateFormat dateformat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
                String createTime = dateformat.format(date);

                AdminMgr adminMgr = (AdminMgr) MvcUtil
                    .getBeanFromContainer("adminMgr");
                AdminLoginBean adminLoginBean = adminMgr
                    .getAdminLoginBean(StringUtil.getSession(request));
                long createManId = adminLoginBean.getAdid();
                String ids = String.valueOf(createManId);
                String emailTitle = "";
                String context = "";
                Boolean yemian = true;
                Boolean youjian = true;
                Boolean duanxin = true;
// 发通知
                scheduleMgrZr.addScheduleByExternalArrangeNow(createTime, "",
                    createManId, ids, "", row.get("waiting_id", 0l),
                    ModuleKey.CHECK_IN_WAITING, emailTitle, context,
                    yemian, youjian, duanxin, request, true,
                    ProcessKey.CHECK_IN_WINDOW);
            }
            return 01;
        } catch (Exception e) {
            throw new SystemException(e, "findZoneByDoorId", log);
        }
    }

    // android 扫门 选择停车位
    public long androidCheckInWindowStop(HttpServletRequest request)
        throws Exception {
        try {
            long dlo_id = StringUtil.getLong(request, "dlo_id");
            long door_id = StringUtil.getLong(request, "door_id");
            long parking_id = StringUtil.getLong(request, "parking_id");
            long type = StringUtil.getLong(request, "type"); // type 0 1
            DBRow row = this.floorCheckInMgrZwb.selectMainByEntryId(dlo_id);
            if (type == 0) {
                if (row != null) {
                    DBRow ro = new DBRow();
                    ro.add("yc_id", parking_id);

                    DBRow rr = new DBRow();
                    rr.add("occupancy_status", 3);

                    this.floorCheckInMgrZwb.updateCloseDoor(door_id, rr); // 释放门
                    this.floorCheckInMgrZwb.updateYcIdByDloId(dlo_id, ro); // 更新主表停车位
                    this.floorCheckInMgrZwb.updateParkingStatus(parking_id, 2); // 更新停车位表
                    return 02; // 成功
                } else {
                    return 01; // 失败
                }
            } else {
                if (row != null) {
                    DBRow ro = new DBRow();
                    ro.add("yc_id", null);

                    DBRow rr = new DBRow();
                    rr.add("occupancy_status", 2);

                    this.floorCheckInMgrZwb.updateCloseDoor(door_id, rr); // 占用门
                    this.floorCheckInMgrZwb.updateYcIdByDloId(dlo_id, ro); // 更新主表停车位
                    this.floorCheckInMgrZwb.updateParkingStatus(parking_id, 1); // 释放停车位
                    return 02; // 成功
                } else {
                    return 01; // 失败
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "androidCheckInWindowStop", log);
        }
    }

    // 根据停车位id 查询停车位
    public DBRow findStorageYardControl(long yc_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findStorageYardControl(yc_id);
        } catch (Exception e) {
            throw new SystemException(e, "findStorageYardControl", log);
        }
    }

    // 更新主表类型
    public DBRow updateRelTypeMain(HttpServletRequest request) throws Exception {
        try {
            long dlo_id = StringUtil.getLong(request, "dlo_id");
            long rel_val = StringUtil.getLong(request, "rel_val");
            long live_val = StringUtil.getLong(request, "live_val");
            DBRow row = new DBRow();
            row.add("status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
            row.add("tractor_status",
                CheckInMainDocumentsStatusTypeKey.UNPROCESS);
            row.add("rel_type", rel_val);

            if (live_val == CheckInLiveLoadOrDropOffKey.LIVE) {
                row.add("isLive", CheckInLiveLoadOrDropOffKey.LIVE);
            } else if (live_val == CheckInLiveLoadOrDropOffKey.DROP) {
                row.add("isLive", CheckInLiveLoadOrDropOffKey.DROP);
            } else if (live_val == CheckInLiveLoadOrDropOffKey.SWAP) {
                row.add("isLive", CheckInLiveLoadOrDropOffKey.SWAP);
            } else if (live_val == CheckInLiveLoadOrDropOffKey.PICK_UP) {
                row.add("isLive", CheckInLiveLoadOrDropOffKey.PICK_UP);
            }

            long num = this.floorCheckInMgrZwb.updateYcIdByDloId(dlo_id, row);
            DBRow mainRow = null;
            if (num > 0) {
                mainRow = this.floorCheckInMgrZwb.findGateCheckInById(dlo_id);
            }
            return mainRow;
        } catch (Exception e) {
            throw new SystemException(e, "updateRelTypeMain", log);
        }
    }

    // 删除子单据
    public long detOccupancyDetails(long detail_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.detOccupancyDetails(detail_id);
        } catch (Exception e) {
            throw new SystemException(e, "updateRelTypeMain", log);
        }
    }

    // 根据货柜号查询停车位
    public DBRow findSpotByCtnNo(String container_no, long ps_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.findSpotByCtnNo(container_no, ps_id);
        } catch (Exception e) {
            throw new SystemException(e, "findSpotByCtnNo", log);
        }
    }

    // 根据主单据号查询被占用的门
    public DBRow[] findUseDoorByDloId(long dlo_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findUseDoorByDloId(dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "findUseDoorByDloId", log);
        }
    }

    // 查询是否有通知
    public DBRow[] findSchedule(long associate_id, long typeKey)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.findSchedule(associate_id, typeKey);
        } catch (Exception e) {
            throw new SystemException(e, "findUseDoorByDloId", log);
        }
    }

    // 查询门下是否有包含没发通知的单据
    public Boolean findScheduleByDoor(long dlo_id, long door_id)
        throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectLotNumberByDoorId(
                door_id, dlo_id);
            Boolean is = false;
            for (int i = 0; i < rows.length; i++) {
                DBRow[] shRows = floorCheckInMgrZwb.findSchedule(
                    rows[i].get("dlo_detail_id", 0l),
                    ProcessKey.CHECK_IN_WAREHOUSE);
                if (shRows.length > 0) {

                } else {
                    is = true;
                }
            }
            return is;
        } catch (Exception e) {
            throw new SystemException(e, "findScheduleByDoor", log);
        }
    }

    // 如果任务是处理Unprocessed 那么就是表示是没有发送任务的 2014-11-24
    public boolean isEntryDetailHasAssign(DBRow entryDetail) {
        if (entryDetail != null) {
            if (entryDetail.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.UNPROCESS) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    // 查询门下是否有包含没发通知的单据 2014-11-24 如果任务是处理Unprocessed 那么就是表示是没有发送任务的
    public Boolean androidFindScheduleByDoor(long dlo_id, long door_id,
                                             long ProcessKey) throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectLotNumberByDoorId(
                door_id, dlo_id); // 他实际是查询dlo_detail_id某个门下面的
            Boolean is = false;
            for (int i = 0; i < rows.length; i++) {
                DBRow[] shRows = floorCheckInMgrZwb.findSchedule(
                    rows[i].get("dlo_detail_id", 0l), ProcessKey);
                if (shRows.length > 0) {

                } else {
                    is = true;
                }
            }
            return is;
        } catch (Exception e) {
            throw new SystemException(e, "findScheduleByDoor", log);
        }
    }

    // window 查询通知
    public DBRow[] windowFindSchedule(long schedule_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectSchedule(schedule_id,
                ProcessKey.CHECK_IN_WINDOW);
        } catch (Exception e) {
            throw new SystemException(e, "windowFindSchedule", log);
        }
    }

    /**
     * 通过associate，process查询schedule
     *
     * @param associate_id
     * @param processKeys
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年1月5日 上午10:08:22
     */
    public DBRow[] windowFindSchedule(long associate_id, int[] processKeys)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectSchedule(associate_id,
                processKeys);
        } catch (Exception e) {
            throw new SystemException(e, "windowFindSchedule", log);
        }
    }

    @Override
    public DBRow[] rtOccupiedSpot(long ps_id, long spot_area, long spotStatus,
                                  PageCtrl pc) throws Exception {
        try {
            return this.floorCheckInMgrZwb.rtOccupiedSpot(ps_id, spot_area,
                spotStatus, pc);
        } catch (Exception e) {
            throw new SystemException(e, "rtOccupiedSpot", log);
        }
    }

    @Override
    public DBRow[] findMainMesByCondition(long ps_id, long entryId,
                                          String license_plate, String trailerNo) throws Exception {
        try {
            DBRow row[] = this.floorCheckInMgrZwb.findMainByCondition(ps_id,
                entryId, license_plate, trailerNo);
            if (row != null && row.length > 0) {
                String spot = "";
                String dock = "";
                DBRow[] spots = this.floorCheckInMgrZwb
                    .findSpotByEntryId(row[0].get("dlo_id", 0l));
                DBRow[] doors = this.floorCheckInMgrZwb
                    .findDoorByEntryId(row[0].get("dlo_id", 0l));
                for (int i = 0; i < spots.length; i++) {
                    if (!StrUtil.isBlank(spots[i].get("yc_no", ""))) {
                        spot += spots[i].get("yc_no", "") + ",";
                    }

                }
                for (int j = 0; j < doors.length; j++) {

                    if (!StrUtil.isBlank(doors[j].get("doorId", ""))) {
                        dock += doors[j].get("doorId", "") + ",";
                    }
                }
                if (spot.length() > 0) {
                    spot = spot.substring(0, spot.length() - 1);
                }
                if (dock.length() > 0) {
                    dock = dock.substring(0, dock.length() - 1);
                }

                row[0].add("spot", spot);
                row[0].add("dock", dock);
                if (!StrUtil.isBlank(row[0].getString("gate_check_in_time"))) {
                    row[0].add("gate_check_in_time", DateUtil
                        .showLocalparseDateTo24Hours(
                            row[0].getString("gate_check_in_time"),
                            ps_id));
                }
                if (!StrUtil.isBlank(row[0]
                    .getString("warehouse_check_in_time"))) {
                    row[0].add(
                        "warehouse_check_in_time",
                        DateUtil.showLocalparseDateTo24Hours(
                            row[0].getString("warehouse_check_in_time"),
                            ps_id));
                }

            }

/**
 * for (int i = 0; i < rows.length; i++) {
 * if(!StringUtil.isBlank(rows[i].get("doorId", ""))){
 * rows[i].add("flag", "door"); }else{ rows[i].add("flag", "spot");
 * } } fixFindMainMesByCondition(rows, ps_id);
 */
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "findMainMesByCondition", log);
        }
    }

    private void fixFindMainMesByCondition(DBRow[] rows, long ps_id)
        throws Exception {
        try {
            if (rows != null && rows.length > 0) {
                for (DBRow temp : rows) {

                    String gate_check_in_operate_time = temp
                        .getString("gate_check_in_operate_time");
                    if (!StringUtil.isNull(gate_check_in_operate_time)) {
                        gate_check_in_operate_time = DateUtil
                            .showLocalparseDateTo24Hours(
                                gate_check_in_operate_time, ps_id);
                        temp.add("gate_check_in_operate_time",
                            gate_check_in_operate_time);
                    }

                    String patrol_last_update_time = temp
                        .getString("patrol_last_update_time");
                    if (!StringUtil.isNull(patrol_last_update_time)) {
                        patrol_last_update_time = DateUtil
                            .showLocalparseDateTo24Hours(
                                patrol_last_update_time, ps_id);
                        temp.add("patrol_last_update_time",
                            patrol_last_update_time);
                    }
                }

            }
        } catch (Exception e) {
            throw new SystemException(e, "fixFindMainMesByCondition", log);
        }
    }

    public DBRow[] findDockMainMesByCondition(long ps_id, long entryId,
                                              String license_plate, String trailerNo) throws Exception {
        try {
// DBRow[] rows =
// this.floorCheckInMgrZwb.findDockMainMesByCondition(ps_id,entryId,license_plate,trailerNo);
// // if(rows.length==0){
// // rows =
// this.floorCheckInMgrZwb.findDoubtDockMainMesByCondition(ps_id,
// entryId, license_plate, trailerNo);
// // }
// for (int i = 0; i < rows.length; i++) {
// if(!StringUtil.isBlank(rows[i].get("yc_no", ""))){
// rows[i].add("flag", "spot");
// }else{
// rows[i].add("flag", "door");
// }
// }
// fixDockMainMesByCondition(rows,ps_id);
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "findMainMesByCondition", log);
        }
    }

    private void fixDockMainMesByCondition(DBRow[] rows, long ps_id)
        throws Exception {
        if (rows != null && rows.length > 0) {
            for (DBRow temp : rows) {
                String warehouse_check_in_operate_time = temp
                    .getString("warehouse_check_in_operate_time");
                if (!StringUtil.isNull(warehouse_check_in_operate_time)) {
                    warehouse_check_in_operate_time = DateUtil
                        .showLocalparseDateTo24Hours(
                            warehouse_check_in_operate_time, ps_id);
                    temp.add("warehouse_check_in_operate_time",
                        warehouse_check_in_operate_time);
                }

                String patrol_last_update_time = temp
                    .getString("patrol_last_update_time");
                if (!StringUtil.isNull(patrol_last_update_time)) {
                    patrol_last_update_time = DateUtil
                        .showLocalparseDateTo24Hours(
                            patrol_last_update_time, ps_id);
                    temp.add("patrol_last_update_time", patrol_last_update_time);
                }
            }

        }
    }

    public long modStorageDoor(long door_id, long door_status,
                               int associate_type, long associate_id, String patrol_time)
        throws Exception {
        try {
            DBRow row = new DBRow();
            row.add("occupied_status", door_status);
            row.add("associate_type", associate_type);
            row.add("associate_id", associate_id);
            row.add("patrol_time", patrol_time);
            return this.floorCheckInMgrZwb.modStorageDoor(door_id, row);

        } catch (Exception e) {
            throw new SystemException(e, "modStorageDoor", log);
        }
    }

    @Override
    public DBRow[] rtOccupiedDoor(long ps_id, long zone_area, long doorStatus,
                                  PageCtrl pc) throws Exception {
        try {
            return this.floorCheckInMgrZwb.rtOccupiedDoor(ps_id, zone_area,
                doorStatus, pc);
        } catch (Exception e) {
            throw new SystemException(e, "rtOccupiedSpot", log);
        }
    }

    /**
     * pickup swap 时查询带走的货柜占用资源
     */
    @Override
    public DBRow[] findSpotOrDoorByCtnr(String ctnr, long ps_id)
        throws Exception {
        try {

            DBRow[] rows = this.checkInMgrZyj.findInYardDropEquipmentsByNumber(
                ctnr, ps_id, CheckInTractorOrTrailerTypeKey.TRAILER);
            if (rows.length > 0) {
// long resources_id=rows[0].get("resources_id",0l);
// int resources_type=rows[0].get("resources_type",0);
// String door_name="";
// String yc_no="";
// 根据主单据查询详细
                DBRow mainRow = this.floorCheckInMgrZwb
                    .findGateCheckInById(rows[0].get("check_in_entry_id",
                        0l));
                DBRow[] tractorRow = this.floorCheckInMgrZwb
                    .selectTractorByEntryId(rows[0].get(
                        "check_in_entry_id", 0l));
                if (tractorRow.length > 0) {
                    rows[0].add("gate_liscense_plate",
                        tractorRow[0].getString("equipment_number"));
                }
                rows[0].add("gate_driver_liscense",
                    mainRow.getString("gate_driver_liscense"));
// if(resources_type==OccupyTypeKey.DOOR){
// door_name=rows[0].get("doorId","");
// //查询door 的area
// DBRow
// row=this.floorCheckInMgrZwb.getAreaByDoorId(resources_id);
// rows[0].add("zone_id",row.getString("area_id"));
// rows[0].add("doorid",door_name);
// }
// if(resources_type==OccupyTypeKey.SPOT){
// yc_no=rows[0].get("yc_no","");
// //查询spot 的area
// DBRow
// row=this.floorCheckInMgrZwb.getAreaByDoorSpotId(resources_id);
// rows[0].add("zone_id",row.getString("spot_id"));
// rows[0].add("yc_no",yc_no);
// }

            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "findSpotOrDoorByCtnr", log);
        }
    }

    // pick up 搜索 ctnr方法 得到 门或 停车位方法
    public DBRow androidFindSpotOrDoorByCtnr(String ctnr, long ps_id)
        throws Exception {
        try {
            DBRow result = new DBRow();
            DBRow[] rows = this.floorCheckInMgrZwb.findSpotOrDoorByCtnr(ctnr,
                ps_id);
            if (rows.length > 0 && rows != null) {
                result.add("door_name", rows[0].get("doorId", ""));
                result.add("yc_name", rows[0].get("yc_no", ""));
                result.add("zone_id", rows[0].get("zone_id", ""));
                result.add("spot_area", rows[0].get("spot_area", ""));
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "findSpotOrDoorByCtnr", log);
        }
    }

    /**
     * shuttle 的时候移动设备 现在shuttle是不会移动任务，只会移动当前的设备 释放当前的设备占用的地方， 占用新的地方
     *
     * @param equipment_id
     * @author zhangrui
     * @Date 2014年11月21日
     * @author zhangrui
     * @Date 2014年11月21日
     */
    private void moveEquipment(long entry_id, long equipment_id,
                               long resources_id, int resources_type, long adid) throws Exception {
        ymsMgrAPI
            .operationSpaceRelation(resources_type, resources_id,
                SpaceRelationTypeKey.Equipment, equipment_id,
                ModuleKey.CHECK_IN, entry_id,
                OccupyStatusTypeKey.OCUPIED, adid);

    }

    // 巡逻人员用释放门 占用停车位
    public DBRow androidMoveToSpot(long info_id, long equipment_id,
                                   long spot_id, long adid, String spot_name) throws Exception {
        try {
            DBRow result = new DBRow();
// 首先判断当前停车位是否是 可用的 现在没有添加这个判断
/*
* if(isCanUseSpot(spot_id, ModuleKey.CHECK_IN, info_id)){
* result.add("flag", "1"); return result ; }
*/
            moveEquipment(info_id, equipment_id, spot_id, OccupyTypeKey.SPOT,
                adid);
            result.add("result", "ok");
            this.addCheckInLog(adid, "Move To Spot【" + spot_name + "】",
                info_id, CheckInLogTypeKey.ANDROID_SHUTTLE,
                DateUtil.NowStr(), "");
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "androidPatrolReleaseDoor", log);
        }
    }

    /**
     * 更新 detail 字表, 通过 dlo_id . where processing | unprocess
     *
     * @param updateRow
     * @throws Exception
     * @author zhangrui
     * @Date 2014年11月19日
     */
    public void updateUnProcessAndProcessingDetail(long dlo_id, DBRow updateRow)
        throws Exception {
        try {
            if (dlo_id > 0l) {
                floorCheckInMgrZwb.updateUnProcessAndProcessingDetail(dlo_id,
                    updateRow);
            }
        } catch (Exception e) {
            throw new SystemException(e,
                "updateCheckInModuleUnProcessAndProcessingDetail", log);
        }
    }

    /**
     * 移动设备到门,那么移动当前设备在Spot(unprocess || processing)上面的任务到当前的门 同时给当前选择的人添加任务，
     * 把这些设备
     */
    public DBRow androidMoveToDoor(HttpServletRequest request, long login_id)
        throws Exception {
        try {
// 参数door_name

// 占用们的时候要查询一下当前门是否是可用的
            long infoId = StringUtil.getLong(request, "infoId");
            long door_id = StringUtil.getLong(request, "door_id");
            long equipment_id = StringUtil.getLong(request, "equipment_id");
            String door_name = StringUtil.getString(request, "door_name");
/*
* 现在没有添加 if(isCanUseDoor(door_id, ModuleKey.CHECK_IN, infoId)){
* DBRow returnRow = new DBRow(); returnRow.add("flag", "1");
* //1表示停车位已经被占用了 return returnRow; }
*/
            moveEquipment(infoId, equipment_id, door_id, OccupyTypeKey.DOOR,
                login_id);
            DBRow[] detailRow = this.floorCheckInMgrZwb
                .findloadingByInfoId(infoId);
            if (detailRow != null) {
                String ids = StringUtil.getString(request, "ids");
                long mail = StringUtil.getLong(request, "mail");
                long page = StringUtil.getLong(request, "page");
                long sms = StringUtil.getLong(request, "sms");
                String beizhu = StringUtil.getString(request, "beizhu");

// 创建时间
                Date date = (Date) Calendar.getInstance().getTime();
                SimpleDateFormat dateformat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
                String createTime = dateformat.format(date);
                String emailTitle = "ANDROID CHECK IN";
                String context = "";

                Boolean duanxin = false;
                Boolean youjian = false;
                Boolean yemian = false;
                if (sms == 1) {
                    duanxin = true;
                }
                if (mail == 1) {
                    youjian = true;
                }
                if (page == 1) {
                    yemian = true;
                }
                String entryType = "";
                if (detailRow[0].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN
                    || detailRow[0].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL
                    || detailRow[0].get("number_type", 0l) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    entryType = "Delivery:";

                } else {
                    entryType = "Pick Up:";
                }
                context += entryType
                    + "【"
                    + moduleKey.getModuleName(detailRow[0].get(
                    "number_type", 0)) + ":"
                    + detailRow[0].get("number", "") + "】To DOOR:【"
                    + door_name + "】Note：" + beizhu;
// 发通知
                for (DBRow row : detailRow) {
                    if (row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.UNPROCESS
                        || row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PROCESSING) {
                        long dlo_detail_id = row.get("dlo_detail_id", 0l);
                        scheduleMgrZr.addScheduleByExternalArrangeNow(
                            createTime, "", login_id, ids, "",
                            dlo_detail_id, ModuleKey.CHECK_IN_ANDROID,
                            emailTitle, context, yemian, youjian, duanxin,
                            request, true,
                            ProcessKey.CHECK_IN_ANDROID_PARROL);
                    }
                }
                this.addCheckInLog(login_id, "Move To Door【" + door_name + "】",
                    infoId, CheckInLogTypeKey.ANDROID_SHUTTLE, createTime,
                    "");
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "androidPatrolReleaseSpot", log);
        }
    }

    // 根据entryId查询loadnum
    @Override
    public DBRow[] findLoadNumberByEntryId(long entryId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findLoadNumberByEntryId(entryId);
        } catch (Exception e) {
            throw new SystemException(e, "findLoadNumberByEntryId", log);
        }
    }

    // 查找 打印标签title
    public DBRow[] findTitleByAdminId(long adminId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findTitleByAdminId(adminId);
        } catch (Exception e) {
            throw new SystemException(e, "findTitleByAdminId", log);
        }
    }

    // 根据 titleid 查询 title 与 模版关系表e
    public DBRow[] selectTitleLableTemplate(long title_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectTitleLableTemplate(title_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectTitleLableTemplate", log);
        }
    }

    // 导出时获取wms title
    public String getWmsTitleExport(String loadNo, String ctnrNo, long adid,
                                    HttpServletRequest request) throws Exception {
        try {
            String titles = "";
            String loadNoTitles = "";
            String ctnrTitles = "";
            if (!loadNo.equals("")) {
                DBRow[] rows = sqlServerMgrZJ
                    .findSupplierIDByLoadNosBolNosCtnrs(loadNo, "load",
                        adid, request);
                if (rows.length > 0 && rows != null) {
                    for (int i = 0; i < rows.length; i++) {
                        String title = rows[i].getString("SupplierID");
                        if (i == rows.length - 1) {
                            loadNoTitles += title;
                        } else {
                            loadNoTitles += title + ",";
                        }
                    }
                }
            }
            if (!ctnrNo.equals("")) {
                DBRow[] rows = sqlServerMgrZJ
                    .findSupplierIDByLoadNosBolNosCtnrs(ctnrNo, "delivery",
                        adid, request);
                if (rows.length > 0 && rows != null) {
                    for (int i = 0; i < rows.length; i++) {
                        String title = rows[i].getString("SupplierID");
                        if (i == rows.length - 1) {
                            ctnrTitles += title;
                        } else {
                            ctnrTitles += title + ",";
                        }
                    }
                }
            }
            if (!loadNoTitles.equals("") && !ctnrTitles.equals("")) {
                titles = loadNoTitles + "," + ctnrTitles;
            } else if (!loadNoTitles.equals("")) {
                titles = loadNoTitles;
            } else if (!ctnrTitles.equals("")) {
                titles = ctnrTitles;
            }

            return titles;
        } catch (Exception e) {
            throw new SystemException(e, "getWmsTitleExport", log);
        }
    }

    // TODO 导出
    @Override
    public String ajaxDownEntryExport(long adid, HttpServletRequest request)
        throws Exception {

        String start_time = StringUtil.getString(request, "start_time");
        String end_time = StringUtil.getString(request, "end_time");
        String mainStatus = StringUtil.getString(request, "mainStatus");
        String tractorStatus = StringUtil.getString(request, "tractorStatus");
        String numberStatus = StringUtil.getString(request, "numberStatus");
        String entryType = StringUtil.getString(request, "entryType");
        String waitingType = StringUtil.getString(request, "waitingType");
        String comeStatus = StringUtil.getString(request, "comeStatus");
        long ps_id = StringUtil.getLong(request, "ps_id");
        long priority = StringUtil.getLong(request, "priority");
        POIFSFileSystem fs = null;// 获得模板
        fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
            + "/administrator/check_in/ExportCheckInList.xls"));
        HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件

        wb.setSheetName(0, "General Gate Record");
        wb.setSheetName(1, "Score card Outbound");
        wb.setSheetName(2, "Score card Inbound");
        HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
        HSSFSheet sheet1 = wb.getSheetAt(1);// 获得模板的第二个sheet
        HSSFSheet sheet2 = wb.getSheetAt(2);// 获得模板的第三个sheet
        HSSFRow row = sheet.getRow(0); // 得到Excel工作表的行
// HSSFCell cell = row.createCell(0);
// cell.setCellValue("ID");
        Font font = wb.createFont();
        font.setColor(HSSFColor.WHITE.index); // 白色字

        HSSFCellStyle titleStyle = wb.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
// style.setFillBackgroundColor(HSSFColor.BLUE.index);
        titleStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index); // 设置标题背景
// titleStyle.setFont(font);
// titleStyle.setLocked(false);
        titleStyle.setWrapText(true);

        HSSFCellStyle Style = wb.createCellStyle();
        Style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
// Style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
// style.setFillBackgroundColor(HSSFColor.BLUE.index);
// Style.setFillForegroundColor(HSSFColor.BLUE_GREY.index); //设置标题背景
        Style.setLocked(false);
        Style.setWrapText(true);

// 迟到的设置背景色为红色
        HSSFCellStyle cellStyle = wb.createCellStyle();
        HSSFFont cellFont = wb.createFont();
        cellFont.setColor(HSSFColor.RED.index);
        cellFont.setFontName("宋体");
        cellStyle.setFont(cellFont);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        DBRow[] mainRows = this.filterCheckIn(entryType, start_time, end_time,
            mainStatus, tractorStatus, numberStatus, waitingType, 0L,
            priority, ps_id);

        for (int i = 0; i < mainRows.length; i++) {
            String load_number = "";
            String po_number = "";
            String order_number = "";
            String outOther_number = "";
            String inOther_number = "";
            String bol_number = "";
            String ctnr_number = "";
            String supplier_id = "";
            String customer_id = "";
            String door = "";
            String rel_type = relTypeToBound(mainRows[i].get("rel_type", 1));// 转换为inbound/outbound
            long dlo_id = mainRows[i].get("dlo_id", 0l);
            DBRow[] detailRows = this.floorCheckInMgrZwb
                .findAllLoadByMainId(dlo_id);
            for (int b = 0; b < detailRows.length; b++) {
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    load_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    po_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    order_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    outOther_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN) {
                    ctnr_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                    bol_number += detailRows[b].getString("number") + ",";
                }
                if (detailRows[b].get("number_type", 0l) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    inOther_number += detailRows[b].getString("number") + ",";
                }
                if (!detailRows[b].getString("doorId").equals("")) {
                    if (detailRows[b].get("occupancy_status", 0l) != 3) {
                        door += detailRows[b].getString("doorId") + ",";
                    }
                }
                if (!detailRows[b].getString("supplier_id").equals("")) {
                    supplier_id += detailRows[b].getString("supplier_id") + ",";
                }
                if (!detailRows[b].getString("customer_id").equals("")) {
                    customer_id += detailRows[b].getString("customer_id") + ",";
                }

            }

            row = sheet.createRow(i + 1); // 创建行
            row.setHeightInPoints(17); // 设置行高

            if (!load_number.equals("")) {
                load_number = load_number
                    .substring(0, load_number.length() - 1);
            }
            if (!po_number.equals("")) {
                po_number = po_number.substring(0, po_number.length() - 1);
            }
            if (!order_number.equals("")) {
                order_number = order_number.substring(0,
                    order_number.length() - 1);
            }
            if (!outOther_number.equals("")) {
                outOther_number = outOther_number.substring(0,
                    outOther_number.length() - 1);
            }
            if (!ctnr_number.equals("")) {
                ctnr_number = ctnr_number
                    .substring(0, ctnr_number.length() - 1);
            }
            if (!bol_number.equals("")) {
                bol_number = bol_number.substring(0, bol_number.length() - 1);
            }
            if (!inOther_number.equals("")) {
                inOther_number = inOther_number.substring(0,
                    inOther_number.length() - 1);
            }
            if (!door.equals("")) {
                door = door.substring(0, door.length() - 1);
            }
            if (!supplier_id.equals("")) {
                supplier_id = supplier_id
                    .substring(0, supplier_id.length() - 1);
            }
            if (!customer_id.equals("")) {
                customer_id = customer_id
                    .substring(0, customer_id.length() - 1);
            }
            row.createCell(0).setCellValue(mainRows[i].getString("dlo_id"));
            row.getCell(0).setCellStyle(Style);
            row.createCell(1).setCellValue(load_number);
            row.createCell(2).setCellValue(po_number);
            row.createCell(3).setCellValue(order_number);
            row.createCell(4).setCellValue(outOther_number);
            row.createCell(5).setCellValue(ctnr_number);
            row.createCell(6).setCellValue(bol_number);
            row.createCell(7).setCellValue(inOther_number);
            row.createCell(8).setCellValue(
                mainRows[i].getString("gate_container_no"));
            row.createCell(9).setCellValue(
                mainRows[i].getString("gate_driver_liscense"));
            row.createCell(10).setCellValue(
                mainRows[i].getString("gate_liscense_plate"));
            row.createCell(11).setCellValue(door); // 门
            row.createCell(12).setCellValue(mainRows[i].getString("yc_no"));
            row.getCell(12).setCellStyle(Style);
            row.createCell(13).setCellValue(
                mainRows[i].getString("company_name"));
            row.createCell(14).setCellValue(
                DateUtil.parseDateFormat(
                    mainRows[i].getString("appointment_time"),
                    "MM/dd/yyyy hh:mm:ss"));
            row.createCell(15).setCellValue(
                DateUtil.parseDateFormat(
                    mainRows[i].getString("create_time"),
                    "MM/dd/yyyy hh:mm:ss"));
            row.createCell(16).setCellValue("Y");

            row.createCell(17).setCellValue(
                DateUtil.parseDateFormat(
                    mainRows[i].getString("window_check_in_time"),
                    "MM/dd/yyyy hh:mm:ss"));
            row.createCell(18).setCellValue(
                DateUtil.parseDateFormat(
                    mainRows[i].getString("warehouse_check_in_time"),
                    "MM/dd/yyyy hh:mm:ss"));
            row.createCell(19).setCellValue(
                DateUtil.parseDateFormat(
                    mainRows[i].getString("check_out_time"),
                    "MM/dd/yyyy hh:mm:ss"));
// String title=this.getWmsTitleExport(load_number, ctnr_bol);
            row.createCell(20).setCellValue(supplier_id);
            row.createCell(21).setCellValue(customer_id);
            row.createCell(22).setCellValue(rel_type);
            row.createCell(23).setCellValue(
                mainRows[i].getString("gate_driver_name"));
            row.createCell(24).setCellValue(
                mainRows[i].getString("load_bar_name"));
            row.createCell(25).setCellValue(
                mainRows[i].getString("search_gate_container_no"));
// 判断是否迟到
            String gate_check_in_time = mainRows[i].getString("create_time");
            String appointment_time = mainRows[i].getString("appointment_time");
            if (!"".equals(appointment_time)) {
                long gate_check_in_min = DateUtil
                    .getDate2LongTime(gate_check_in_time);// 毫秒
                long appointment_min = DateUtil
                    .getDate2LongTime(appointment_time);
// System.out.println("a="+gate_check_in_min+"   b="+appointment_min+"   b-a="+(appointment_min
// - gate_check_in_min));
                if ((gate_check_in_min - appointment_min) > 600000) {
                    row.getCell(15).setCellStyle(cellStyle);
                    row.createCell(16).setCellValue("N");
                    row.getCell(16).setCellStyle(cellStyle);
                }
            }
        }

        setLoadNoSheet(sheet1, adid, request);// 写入第二个sheet，写入迟到的load
        setReceiptBolSheet(sheet2, adid, request);// 写入第二个sheet，写入迟到的ReceiptBol

        String path;
        path = "upl_excel_tmp/ExportCheckInList_"
            + DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date())
            + ".xls";
        FileOutputStream fout = new FileOutputStream(Environment.getHome()
            + path);
        wb.write(fout);
        fout.close();
        return (path);
    }

    // TODO　sheet2，导出当天 预约的loadNo等信息，红色显示迟到的，绿色显示未到的
    public void setLoadNoSheet(HSSFSheet sheet, long adid,
                               HttpServletRequest request) throws Exception {
        long ps_id = StringUtil.getLong(request, "ps_id");
        DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
        String[] company_id = null;
        if (companyId.length > 0) {
            company_id = new String[companyId.length];
            for (int i = 0; i < companyId.length; i++) {
                company_id[i] = companyId[i].get("company_id", "");
            }
        }
        HSSFRow row = sheet.getRow(0);
// 迟到的设置字体色为红色
        HSSFCellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        HSSFFont cellFont = sheet.getWorkbook().createFont();
        cellFont.setColor(HSSFColor.RED.index);
        cellFont.setFontName("宋体");
        cellStyle.setFont(cellFont);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

// 未到的设置字体色为绿色
        HSSFCellStyle style = sheet.getWorkbook().createCellStyle();
        HSSFFont font = sheet.getWorkbook().createFont();
        font.setColor(HSSFColor.GREEN.index);
        font.setFontName("宋体");
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        String time = DateUtil.FormatDatetime("yyyy-MM-dd");
// String time = "2013-07-12";
// DBRow[] appointDBRows =
// floorSQLServerMgrZJ.findLoadNoAppointTimeBetweenTime(time);
        DBRow[] appointDBRows = sqlServerMgrZJ
            .findLoadNoAppointTimeBetweenTime(time, time, company_id, adid,
                request);
        DBRow[] dbRows = this.floorCheckInMgrZwb.selectCreateTimeForLoadNo(
            time, ps_id);// 查询今天到的信息
        for (int i = 0; i < appointDBRows.length; i++) {
            String load_number = appointDBRows[i].getString("LoadNo");
            String appointment_time = appointDBRows[i]
                .getString("AppointmentDate");
            String create_time = "";
            String carrier_name = "";
            String rel_type = "";
// System.out.println("load_number: "+load_number);
            for (int j = 0; j < dbRows.length; j++) {
                if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    if (load_number.equals(dbRows[j].getString("number"))) {
                        create_time = dbRows[j].getString("create_time");
                        carrier_name = dbRows[j].getString("company_name");
                        rel_type = relTypeToBound(dbRows[j].get("rel_type", 1));// 转换为inbound/outbound
                        break;
                    }
                }

            }
// int count = ((i+1)*2)-1;//从第二行开始，隔行显示
// sheet.createRow(count+1).setHeightInPoints(15);//设置空行的高度
            row = sheet.createRow(i + 1);
            row.setHeightInPoints(17);// 设置行高
            row.createCell(0).setCellValue(load_number);
            row.createCell(1).setCellValue(
                DateUtil.parseDateTo12HourNoYear(appointment_time));
            row.createCell(2).setCellValue(
                DateUtil.parseDateTo12HourNoYear(create_time));
            row.createCell(3).setCellValue(carrier_name);
// row.createCell(3).setCellValue(title);
// row.createCell(4).setCellValue(rel_type);
// row.createCell(5).setCellValue(driverName);
            if ("".equals(create_time)) {
                row.getCell(1).setCellStyle(style);
            } else if (!"".equals(appointment_time)) {
                long gate_check_in_min = DateUtil.getDate2LongTime(create_time);// 毫秒
                long appointment_min = DateUtil
                    .getDate2LongTime(appointment_time);
                if ((gate_check_in_min - appointment_min) > 600000) {
                    row.getCell(1).setCellStyle(cellStyle);
                }
            }

        }
    }

    /**
     * 报表查找 load
     *
     * @param request
     * @param adid
     * @return
     * @throws SystemException
     */
    @Override
    public List<DBRow> getLoadNo(HttpServletRequest request, long adid,
                                 String startTime, String endTime) throws SystemException {
        try {
            long ps_id = StringUtil.getLong(request, "ps_id");
            String time = DateUtil.FormatDatetime("yyyy-MM-dd");
            DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
            String[] company_id = null;
            if (companyId.length > 0) {
                company_id = new String[companyId.length];
                for (int i = 0; i < companyId.length; i++) {
                    company_id[i] = companyId[i].get("company_id", "");
                }
            }
// String time = "2013-07-12";
// DBRow[] appointDBRows =
// floorSQLServerMgrZJ.findLoadNoAppointTimeBetweenTime(time);
            DBRow[] appointDBRows = sqlServerMgrZJ
                .findLoadNoAppointTimeBetweenTime(startTime, endTime,
                    company_id, adid, request);
            DBRow[] dbRows = this.floorCheckInMgrZwb
                .getLoadNoByGateCheckInTime(startTime, endTime, ps_id);
            List<DBRow> mains = new ArrayList<DBRow>();

            for (int i = 0; i < dbRows.length; i++) {
                if (!dbRows[i].get("number_type", "").equals(
                    "" + ModuleKey.CHECK_IN_LOAD)) {
                    continue;
                }
                String number = dbRows[i].get("number", "");
                DBRow row = new DBRow();
                mains.add(row);
                row.add("carrier", dbRows[i].get("company_name", ""));
                row.add("number", dbRows[i].get("number", ""));
                row.add("numberType", moduleKey.getModuleName(dbRows[i].get(
                    "number_type", "")));
                row.add("WHSE", dbRows[i].get("title", ""));
                row.add("dlo_id", dbRows[i].get("dlo_id", ""));
                row.add("customId", dbRows[i].get("custom_id", ""));
                row.add("title", dbRows[i].get("supplier_id", ""));
                row.add("gateCheckInTime",
                    dbRows[i].get("gate_check_in_time", ""));

                for (int j = 0; j < appointDBRows.length; j++) {

                    String load_number = appointDBRows[j].getString("LoadNo");
                    String appointment_time = appointDBRows[j]
                        .getString("AppointmentDate");
                    String suppilerId = appointDBRows[j].get("SUPPLIERIDS", "");
                    if (!StringUtil.isNull(suppilerId)) {
                        suppilerId = suppilerId.substring(1);
                    }
                    row.add("AppointmentTime", "");
                    if (load_number.equals(number)) {
                        row.add("AppointmentTime", appointment_time);
                        row.add("WHSE", appointDBRows[j].get("COMPANYID", ""));
                        row.add("carrier",
                            appointDBRows[j].get("CARRIERID", ""));
                        row.add("number", load_number);
                        row.add("numberType", moduleKey
                            .getModuleName(ModuleKey.CHECK_IN_LOAD));
                        row.add("WHSE", appointDBRows[j].get("COMPANYID", ""));
                        row.add("customId",
                            appointDBRows[j].get("CUSTOMERID", ""));
                        row.add("ReferenceNo",
                            appointDBRows[j].get("ReferenceNo", ""));
                        row.add("title", suppilerId);

                        appointDBRows[j] = new DBRow();
                        break;
                    }

                }
            }
            DBRow row = null;
            for (int j = 0; j < appointDBRows.length; j++) {
                String load_number = appointDBRows[j].getString("LoadNo");
                String appointment_time = appointDBRows[j]
                    .getString("AppointmentDate");
                String suppilerId = appointDBRows[j].get("SUPPLIERIDS", "");
                if (!StringUtil.isNull(suppilerId)) {
                    suppilerId = suppilerId.substring(1);
                }

                if (!StringUtil.isNull(load_number)) {
                    row = new DBRow();
                    mains.add(row);

                    row.add("carrier", appointDBRows[j].get("CARRIERID", ""));
                    row.add("number", load_number);
                    row.add("numberType",
                        moduleKey.getModuleName(ModuleKey.CHECK_IN_LOAD));
                    row.add("WHSE", appointDBRows[j].get("COMPANYID", ""));
                    row.add("customId", appointDBRows[j].get("CUSTOMERID", ""));
                    row.add("ReferenceNo",
                        appointDBRows[j].get("ReferenceNo", ""));
                    row.add("title", suppilerId);
                    row.add("AppointmentTime", appointment_time);
                    appointDBRows[j] = new DBRow();
                }
            }

            return mains;
        } catch (Exception e) {
            throw new SystemException(e, "HttpServletRequest", log);
        }

    }

    /**
     * 报表查找 order Po load
     *
     * @param request
     * @param adid
     * @return
     * @throws SystemException
     */
    @Override
    public List<DBRow> getOrderAndPoNo(HttpServletRequest request, long adid,
                                       String startTime, String endTime, long ps_id)
        throws SystemException {

        try {
// long ps_id = StringUtil.getLong(request, "ps_id");
            DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
            String[] company_id = null;
            if (companyId.length > 0) {
                company_id = new String[companyId.length];
                for (int i = 0; i < companyId.length; i++) {
                    company_id[i] = companyId[i].get("company_id", "");
                }
            }
            String time = DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss");
// String time = "2013-07-12";
            time = DateUtil.showLocalTime(time, ps_id);
            String queryEndTime = "";
            String[] dataArrays = time.split(" ");
            if (dataArrays.length == 2) {
                time = dataArrays[0];
                queryEndTime = dataArrays[0];
            }
            String utcTime = DateUtil.FormatDatetime("yyyy-MM-dd");
            DBRow[] appointDBRowsLoad = sqlServerMgrZJ
                .findLoadNoAppointTimeBetweenTime(time, queryEndTime,
                    company_id, adid, request);
            DBRow[] appointDBRows = sqlServerMgrZJ
                .findOrderNoAppointTimeBetweenTime(time, queryEndTime,
                    company_id, adid, request);
            DBRow[] dbRows = this.floorCheckInMgrZwb
                .getLoadNoByGateCheckInTime(utcTime, utcTime, ps_id);// 查询今天到的信息
            List<DBRow> mains = new ArrayList<DBRow>();

            if (appointDBRows.length > 0) {
                for (int i = 0; i < appointDBRows.length; i++) {

                    String order_number = appointDBRows[i].getString("OrderNo");
                    String po_number = appointDBRows[i].getString("PONo");
                    String suppilerId = appointDBRows[i].get("SUPPLIERIDS", "");
                    if (!StringUtil.isNull(suppilerId)) {
                        suppilerId = suppilerId.substring(1);
                    }

                    appointDBRows[i].add("PONumber", po_number);
                    appointDBRows[i].add("OrderNo", order_number);
                    appointDBRows[i].add("WHSE",
                        appointDBRows[i].get("COMPANYID", ""));
                    appointDBRows[i].add("customId",
                        appointDBRows[i].get("CUSTOMERID", ""));
                    appointDBRows[i].add("title", suppilerId);
                    appointDBRows[i].add("gateCheckInTime", "");
                    appointDBRows[i].add("AppointmentTime",
                        appointDBRows[i].get("AppointmentDate", ""));
                    for (int j = 0; j < dbRows.length; j++) {
                        if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_PONO
                            || dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                            if (order_number.equals(dbRows[j]
                                .getString("number"))
                                || po_number.equals(dbRows[j]
                                .getString("number"))) {
                                appointDBRows[i]
                                    .add("gateCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("gate_check_in_time"),
                                            ps_id));
                                appointDBRows[i].add("carrier",
                                    dbRows[j].getString("company_name"));
                                appointDBRows[i]
                                    .add("dockCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("handle_time"),
                                            ps_id));
                                break;
                            }
                        }

                    }
                    mains.add(appointDBRows[i]);

                }
            }
            if (appointDBRowsLoad.length > 0) {
                for (int i = 0; i < appointDBRowsLoad.length; i++) {

                    String load_number = appointDBRowsLoad[i]
                        .getString("LoadNo");

                    String suppilerId = appointDBRowsLoad[i].get("SUPPLIERIDS",
                        "");
                    if (!StringUtil.isNull(suppilerId)) {
                        suppilerId = suppilerId.substring(1);
                    }

                    appointDBRowsLoad[i].add("LoadNumber", load_number);
                    appointDBRowsLoad[i].add("WHSE",
                        appointDBRowsLoad[i].get("COMPANYID", ""));
                    appointDBRowsLoad[i].add("customId",
                        appointDBRowsLoad[i].get("CUSTOMERID", ""));
                    appointDBRowsLoad[i].add("title", suppilerId);
                    appointDBRowsLoad[i].add("gateCheckInTime", "");
                    appointDBRowsLoad[i].add("AppointmentTime",
                        appointDBRowsLoad[i].get("AppointmentDate", ""));
                    for (int j = 0; j < dbRows.length; j++) {
                        if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD
                            || dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                            if (load_number.equals(dbRows[j]
                                .getString("number"))) {
                                appointDBRowsLoad[i]
                                    .add("gateCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("gate_check_in_time"),
                                            ps_id));
                                appointDBRowsLoad[i].add("carrier",
                                    dbRows[j].getString("company_name"));
                                appointDBRowsLoad[i]
                                    .add("dockCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("handle_time"),
                                            ps_id));

                            }
                        }

                    }
                    mains.add(appointDBRowsLoad[i]);

                }
            }

            return mains;
        } catch (Exception e) {
            throw new SystemException(e, "HttpServletRequest", log);
        }

    }

    /**
     * 报表查找 receiptBol
     *
     * @param request
     * @param adid
     * @return
     * @throws SystemException
     */
    @Override
    public List<DBRow> getReceiptBol(HttpServletRequest request, long adid,
                                     String startTime, String endTime, long ps_id)
        throws SystemException {
        try {
// long ps_id = StringUtil.getLong(request, "ps_id"); zhangrui
            DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
            String[] company_id = null;
            if (companyId.length > 0) {
                company_id = new String[companyId.length];
                for (int i = 0; i < companyId.length; i++) {
                    company_id[i] = companyId[i].get("company_id", "");
                }
            }
            String time = DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss");
// String time = "2013-07-12";
            time = DateUtil.showLocalTime(time, ps_id);
            String queryEndTime = "";
            String[] dataArrays = time.split(" ");
            if (dataArrays.length == 2) {
                time = dataArrays[0];
                queryEndTime = dataArrays[0];
            }
            String utcTime = DateUtil.FormatDatetime("yyyy-MM-dd");

            DBRow[] appointDBRows = sqlServerMgrZJ
                .findReceiptBolCtnAppointTimeBetweenTime(time,
                    queryEndTime, company_id, adid, null); // sqlServerMgrZJ.findReceiptBolCtnAppointTimeBetweenTime(startTime,
// endTime,adid,
// request);
// zhangrui
            DBRow[] dbRows = this.floorCheckInMgrZwb
                .getLoadNoByGateCheckInTime(utcTime, utcTime, ps_id);// 查询今天到的信息
            List<DBRow> mains = new ArrayList<DBRow>();

            if (appointDBRows.length > 0) {
                for (int i = 0; i < appointDBRows.length; i++) {

                    String bol_number = appointDBRows[i].getString("BOLNo");
                    String ctn_number = appointDBRows[i]
                        .getString("ContainerNo");
                    String suppilerId = appointDBRows[i].get("SupplierID", "");

                    appointDBRows[i].add("BOLNumber", bol_number);
                    appointDBRows[i].add("CTNRNumber", ctn_number);
                    appointDBRows[i].add("WHSE",
                        appointDBRows[i].get("COMPANYID", ""));
                    appointDBRows[i].add("customId",
                        appointDBRows[i].get("CUSTOMERID", ""));
                    appointDBRows[i].add("title", suppilerId);
                    appointDBRows[i].add("gateCheckInTime", "");
                    appointDBRows[i].add("AppointmentTime",
                        appointDBRows[i].get("AppointmentDate", ""));
                    for (int j = 0; j < dbRows.length; j++) {
                        if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN
                            || dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                            if (bol_number
                                .equals(dbRows[j].getString("number"))
                                || ctn_number.equals(dbRows[j]
                                .getString("number"))) {
                                appointDBRows[i]
                                    .add("gateCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("gate_check_in_time"),
                                            ps_id));
                                appointDBRows[i].add("carrier",
                                    dbRows[j].getString("company_name"));
                                appointDBRows[i]
                                    .add("dockCheckInTime",
                                        DateUtil.showLocalparseDateTo24Hours(
                                            dbRows[j]
                                                .getString("handle_time"),
                                            ps_id));
                                break;
                            }
                        }

                    }
                    mains.add(appointDBRows[i]);

                }
            }

            return mains;
        } catch (Exception e) {
            throw new SystemException(e, "HttpServletRequest", log);
        }

    }

    // rel_type转化为inbound或者outbound
    public String relTypeToBound(int rel_type) {
        String result = "";
        if (rel_type == CheckInMainDocumentsRelTypeKey.PICK_UP) {
            result = "Outbound";
        } else if (rel_type == CheckInMainDocumentsRelTypeKey.NONE) {
            result = "None";
        } else {
            result = "Inbound";
        }

        return result;
    }

    // TODO　导出设置sheet，写入迟到的ReceiptBol
    public void setReceiptBolSheet(HSSFSheet sheet, long adid,
                                   HttpServletRequest request) throws Exception {
        long ps_id = StringUtil.getLong(request, "ps_id");
        DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
        String[] company_id = null;
        if (companyId.length > 0) {
            company_id = new String[companyId.length];
            for (int i = 0; i < companyId.length; i++) {
                company_id[i] = companyId[i].get("company_id", "");
            }
        }
        HSSFRow row = sheet.getRow(0);
// 迟到的设置字体色为红色
        HSSFCellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        HSSFFont cellFont = sheet.getWorkbook().createFont();
        cellFont.setColor(HSSFColor.RED.index);
        cellFont.setFontName("宋体");
        cellStyle.setFont(cellFont);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

// 未到的设置字体色为绿色
        HSSFCellStyle style = sheet.getWorkbook().createCellStyle();
        HSSFFont font = sheet.getWorkbook().createFont();
        font.setColor(HSSFColor.GREEN.index);
        font.setFontName("宋体");
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        String time = DateUtil.FormatDatetime("yyyy-MM-dd");
// String time = "2013-07-12";
        DBRow[] appointDBRows = sqlServerMgrZJ
            .findReceiptBolCtnAppointTimeBetweenTime(time, time,
                company_id, adid, request);
        DBRow[] dbRows = this.floorCheckInMgrZwb.selectCreateTimeForLoadNo(
            time, ps_id);

        for (int i = 0; i < appointDBRows.length; i++) {
            String bol_number = appointDBRows[i].getString("BOLNo");
            String ctn_number = appointDBRows[i].getString("ContainerNo");
            String appointment_time = appointDBRows[i]
                .getString("AppointmentDate");
            String create_time = "";
            String rel_type = "";
            String carrier_name = "";
            for (int j = 0; j < dbRows.length; j++) {
                if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN) {
                    if (ctn_number.equals(dbRows[j].getString("number"))) {
                        create_time = dbRows[j].getString("create_time");
                        rel_type = relTypeToBound(dbRows[j].get("rel_type", 1));// 转换为inbound/outbound
                        carrier_name = dbRows[j].getString("company_name");
                        break;
                    }
                }
                if (dbRows[j].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                    if (bol_number.equals(dbRows[j].getString("number"))) {
                        create_time = dbRows[j].getString("create_time");
                        rel_type = relTypeToBound(dbRows[j].get("rel_type", 1));// 转换为inbound/outbound
                        carrier_name = dbRows[j].getString("company_name");
                        break;
                    }
                }

            }
// int count = ((i+1)*2)-1;//从第二行开始，隔行显示
// sheet.createRow(count+1).setHeightInPoints(15);//设置空行的高度
            row = sheet.createRow(i + 1);
            row.setHeightInPoints(17);// 设置行高
            row.createCell(0).setCellValue(bol_number);
            row.createCell(1).setCellValue(ctn_number);
            row.createCell(2).setCellValue(
                DateUtil.parseDateTo12HourNoYear(appointment_time));
            row.createCell(3).setCellValue(
                DateUtil.parseDateTo12HourNoYear(create_time));
            row.createCell(4).setCellValue(carrier_name);
// row.createCell(4).setCellValue(title);
// row.createCell(5).setCellValue(rel_type);
// row.createCell(6).setCellValue(driver name);
            if ("".equals(create_time)) {
                row.getCell(2).setCellStyle(style);
            } else if (!"".equals(appointment_time)) {
                long gate_check_in_min = DateUtil.getDate2LongTime(create_time);// 毫秒
                long appointment_min = DateUtil
                    .getDate2LongTime(appointment_time);
                if ((gate_check_in_min - appointment_min) > 600000) {
                    row.getCell(2).setCellStyle(cellStyle);
                }
            }

        }
    }

    // 条件过滤
    public DBRow[] filterCheckIn(String entryType, String start_time,
                                 String end_time, String mainStatus, String tractorStatus,
                                 String numberStatus, String doorStatus, long loadBar,
                                 long priority, long ps_id) throws Exception {
        try {
            if (!start_time.equals("")) {
                start_time = DateUtil.showUTCTime(
                    DateUtil.StrtoYYMMDDHHMMSS(start_time), ps_id);
            }
            if (!end_time.equals("")) {
                end_time = DateUtil.showUTCTime(
                    DateUtil.StrtoYYMMDDHHMMSS(end_time), ps_id);
            }
            return floorCheckInMgrZwb.filterCheckIn(entryType, start_time,
                end_time, mainStatus, tractorStatus, numberStatus,
                doorStatus, loadBar, priority, ps_id);
        } catch (Exception e) {
            throw new SystemException(e, "filterCheckIn", log);
        }
    }

    // 根据lable name查询lable详细
    public DBRow[] findLableByName(DBRow[] rows, long number_type)
        throws SystemException {
        try {
            for (int i = 0; i < rows.length; i++) {
// DBRow lable,String masterBolformat, String Bolformat,String
// AccountID
                rows[i] = floorCheckInMgrZwb.findLableByName(rows[i],
                    rows[i].get("MasterBOLFormat", ""),
                    rows[i].get("BOLFormat", "error"), "", number_type);
// floorCheckInMgrZwb.findLableByName(rows[i],
// rows[i].get("MasterBOLFormat", ""),
// rows[i].get("BOLFormat","error"), ""); //
// floorCheckInMgrZwb.findLableByName(rows[i],rows[i].get("MasterBOLFormat",
// ""),rows[i].get("BOLFormat","error"),rows[i].getString("AccountID"),"");
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "filterCheckIn", log);
        }
    }

    /*
* @Override public DBRow fixCompanyIdAndCustomerId(String load_number, long
* adid, HttpServletRequest request) throws Exception { DBRow returnRow =
* new DBRow(); try{ DBRow[] rows =
* sqlServerMgrZJ.findLoadCompanyIdCustomerIdByLoadNo(load_number, adid,
* request); if(rows != null && 0 == rows.length){ rows =
* sqlServerMgrZJ.findOrderCompanyIdCustomerIdByLoadNo(load_number, adid,
* request); } List<DBRow> arrays = new ArrayList<DBRow>(); for(int i = 0
* ;rows != null && i < rows.length; i++){ DBRow cRow = new DBRow();
* cRow.add("load_no",rows[i].getString("LoadNo"));
* cRow.add("company_id",rows[i].getString("CompanyID"));
* cRow.add("customer_id",rows[i].getString("CustomerID"));
* arrays.add(cRow); } returnRow.add("count", arrays.size());
* returnRow.add("datas", arrays.toArray(new DBRow[0])); return returnRow ;
* }catch (Exception e) { throw new
* SystemException(e,"fixCompanyIdAndCustomerId",log); } }
*/
// 安卓链接web 端实现打印功能接口zr
    @Override
    public DBRow[] androidLinkWebPrint(String number, long entry_id,
                                       String companyId, String customerId, long number_type,
                                       long order_no, long adid, HttpServletRequest request)
        throws Exception {
        try {
// String number, String companyId, String customerId,long
// entry_id,long number_type,long order_no,long adid ,
// HttpServletRequest request
            DBRow[] rows = this.androidFindTemplate(number, companyId,
                customerId, entry_id, number_type, order_no, adid, request);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "androidLinkWebPrint", log);
        }
    }

    // android 根据load companyId customerId获取打印的url
    @Override
    public DBRow androidLinkWebPrintGetPath(String number, String companyId,
                                            String customerId, long entry_id, int number_type, long order_no,
                                            long adid, HttpServletRequest request) throws Exception {
        try {
            DBRow result = new DBRow();
            DBRow[] rows = this.androidFindTemplate(number, companyId,
                customerId, entry_id, number_type, order_no, adid, request);
            DBRow wmsRow = this.getWmsRow(number, companyId, customerId,
                entry_id, adid, request);
            result.add("datas", rows);
            result.add("loading_ticket", wmsRow);
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "androidLinkWebPrintGetPath", log);
        }
    }

    // 安卓 用 用load查询模版
    public DBRow[] androidFindTemplate(String number, String companyId,
                                       String customerId, long entry_id, long number_type, long order_no,
                                       long adid, HttpServletRequest request) throws Exception {
        try {
            DBRow[] wmsRows = null;
            if (number_type == ModuleKey.CHECK_IN_LOAD) {
                wmsRows = sqlServerMgrZJ.findBillOfLadingTemplateByLoad(number,
                    companyId, customerId, adid, request);
            } else if (number_type == ModuleKey.CHECK_IN_ORDER) {
                wmsRows = sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(
                    Long.parseLong(number), companyId, customerId, adid,
                    request);
            } else if (number_type == ModuleKey.CHECK_IN_PONO) {
                wmsRows = sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(
                    order_no, companyId, customerId, adid, request);
            }

            this.findLableByName(wmsRows, number_type);
// System.out.println("entry_id"+entry_id);
            List<DBRow> masterList = new ArrayList<DBRow>();
            for (int i = 0; i < wmsRows.length; i++) {

                DBRow bolRow = new DBRow();
                String masterBolNo = wmsRows[i].getString("master_bol_nos");
                String orderNo = wmsRows[i].getString("order_nos");

                customerId = wmsRows[i].getString("CustomerID");
                companyId = wmsRows[i].getString("CompanyID");

                if (number_type == ModuleKey.CHECK_IN_LOAD) {
                    DBRow masterRow = new DBRow();
                    DBRow masterBol = (DBRow) wmsRows[i].get(
                        "MasterBOLFormatBAK", new DBRow());
                    if (!masterBol.get("template_path", "").equals("")) {
                        String path = masterBol.get("template_path", "");
                        String name = wmsRows[i]
                            .get("MasterBOLFormat", "error");

                        masterRow.add("master_bol_no", masterBolNo);
                        masterRow.add("order_no", orderNo);
                        masterRow.add("path", path);
                        masterRow.add("name", name);
                        masterRow.add("load_no", number);
                        masterRow.add("number", number);
                        masterRow.add("number_type", number_type);
                        masterRow.add("type", "MasterBOL");
                        masterRow.add("companyId", companyId);
                        masterRow.add("customerId", customerId);
                    }
                    masterList.add(masterRow);
                }
                DBRow Bol = (DBRow) wmsRows[i].get("BOLFormatBAK", new DBRow());

                if (!Bol.get("template_path", "").equals("")) {
                    String path = Bol.get("template_path", "");
                    String name = wmsRows[i].get("BOLFormat", "error");
                    bolRow.add("master_bol_no", masterBolNo);
                    bolRow.add("order_no", order_no);
                    bolRow.add("path", path);
                    bolRow.add("name", name);
                    bolRow.add("type", "BOL");
                    bolRow.add("number", number);
                    bolRow.add("number_type", number_type);
                    bolRow.add("load_no", number);
                    bolRow.add("companyId", companyId);
                    bolRow.add("customerId", customerId);
                }

                masterList.add(bolRow);
            }
            return masterList.toArray(new DBRow[masterList.size()]);
        } catch (Exception e) {
            throw new SystemException(e, "androidFindTemplate", log);
        }
    }

    // 查询货柜号android
    public DBRow androidGetCtnr(long entry_id, String number, int number_type)
        throws Exception {
        try {
            DBRow cnRow = new DBRow();
            DBRow eqRow = this.floorCheckInMgrZwb.getEquimentNoByNumber(
                entry_id, number, (int) number_type);
            return eqRow;
        } catch (Exception e) {
            throw new SystemException(e, "androidGetCtnr", log);
        }
    }

    // wmsRow 标签wenti
    public DBRow getWmsRow(String loadNo, String companyId, String customerId,
                           long entry_id, long adid, HttpServletRequest request)
        throws Exception {
        try {
            DBRow wmsRow = new DBRow();
            int masterCount = sqlServerMgrZJ.findMasterBolNoCountByLoadNo(
                loadNo, companyId, customerId, adid, request);
            String wmsUrl = "";
            if (masterCount != 0) {
                wmsUrl = "check_in/print_order_master_wms.html";
            } else {
                wmsUrl = "check_in/print_order_no_master_wms.html";
            }
            DBRow mainRow = this.findGateCheckInById(entry_id);
            if (mainRow != null) {
                wmsRow.add("entryId", entry_id);
                wmsRow.add("loadNo", loadNo);
                wmsRow.add("window_check_in_time",
                    mainRow.get("window_check_in_time", ""));
// wmsRow.add("DockID", "测试");
                wmsRow.add("company_name", mainRow.get("company_name", ""));
                wmsRow.add("gate_container_no",
                    mainRow.get("gate_container_no", ""));
// wmsRow.add("seal", mainRow.get("out_seal", ""));
                wmsRow.add("CompanyID", companyId);
                wmsRow.add("CustomerID", customerId);
                wmsRow.add("path", wmsUrl);

            }
            return wmsRow;
        } catch (Exception e) {
            throw new SystemException(e, "getWmsRow", log);
        }
    }

    // 安卓链接web 端实现打印功能接口 送货标签
    public DBRow androidCtnrWebPrint(String ctnr_number, String bol_number,
                                     long entry_id, long adid, HttpServletRequest request)
        throws Exception {
        try {
            String number = "";
            if (!ctnr_number.equals("")) {
                number = ctnr_number;
            } else {
                number = bol_number;
            }
            DBRow result = new DBRow();
            DBRow ctnRow = sqlServerMgrZJ
                .findReceiptsrCompanyIdCustomerIdTypeByBolNoOrContainerNo(
                    number, adid, request);
            if (((DBRow[]) ctnRow.get("rows", new DBRow[0])).length == 1) {
                result.add("path",
                    "check_in/print_receipts_wms_by_bol_container.html");
                result.add("count", 1);
            } else if (((DBRow[]) ctnRow.get("rows", new DBRow[0])).length > 1) {
                DBRow[] rows = sqlServerMgrZJ.findReceiptsByBolNoOrContainerNo(
                    bol_number, ctnr_number, "", "", adid, request);
                List<DBRow> companyList = new ArrayList<DBRow>();
                for (int i = 0; i < rows.length; i++) {
                    DBRow row = new DBRow();
                    row.add("CompanyID", rows[i].getString("CompanyID"));
                    row.add("BOLNo", rows[i].getString("BOLNo"));
                    row.add("ContainerNo", rows[i].getString("ContainerNo"));
                    row.add("CustomerID", rows[i].getString("CustomerID"));
                    companyList.add(row);
                }
                result.add("value",
                    companyList.toArray(new DBRow[companyList.size()]));
                result.add("path",
                    "check_in/print_receipts_wms_by_bol_container.html");
                result.add("count", 2);
            }

            return result;
        } catch (Exception e) {
            throw new SystemException(e, "androidCtnrWebPrint", log);
        }
    }

    @Override
    public String searchEquipmentByEquipmentType(long ps_id,
                                                 String containerNo, int equipment_type) throws Exception {
        try {
            if (StringUtil.isNumber(containerNo)) { // 数字查询
// DBRow[] datas =
// floorCheckInMgrZwb.queryContainerNoByPhoneNumber(ps_id,containerNo,CheckInTractorOrTrailerTypeKey.TRAILER);
                DBRow[] datas = floorCheckInMgrZwb.queryEquipmentByPhoneNumber(
                    ps_id, containerNo, equipment_type);// 2014/1213 周卫斌改
                if (datas != null && datas.length > 0) {
                    return convertContainerNoArray(datas);
                }
            } else { // 字母查询
// DBRow[] datas =
// floorCheckInMgrZwb.getSearchCheckInCTNRJSON(ps_id,
// containerNo,
// CheckInTractorOrTrailerTypeKey.TRACTOR);
                DBRow[] datas = floorCheckInMgrZwb
                    .queryEquipmentByEquipmentNumber(ps_id, containerNo,
                        equipment_type);// 2014/1213 周卫斌改
                if (datas != null && datas.length > 0) {
                    return convertContainerNoArray(datas);
                }
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e,
                "searchContainerNoByAndroidAutoComplete", log);
        }
    }

    @Override
    public String searchEquipment(long ps_id, String licensePlate)
        throws Exception {
        try {
            if (StringUtil.isNumber(licensePlate)) { // 数字查询
                DBRow[] datas = floorCheckInMgrZwb.queryEquipmentByPhoneNumber(
                    ps_id, licensePlate, 0);
                if (datas != null && datas.length > 0) {
                    return convertContainerNoArray(datas);
                }
            } else { // 字母查询
                DBRow[] datas = floorCheckInMgrZwb
                    .queryEquipmentByEquipmentNumber(ps_id, licensePlate, 0);
                if (datas != null && datas.length > 0) {
                    return convertContainerNoArray(datas);
                }
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e,
                "searchLicensePlateByAndroidAutoComplete", log);
        }
    }

    private String convertLicencePlateArray(DBRow[] datas) {
        StringBuilder builder = new StringBuilder();
        if (datas != null) {
            for (DBRow row : datas) {
                builder.append(",")
                    .append(row.getString("gate_liscense_plate"));
            }
        }
        return builder.toString().substring(1);
    }

    private String convertContainerNoArray(DBRow[] rows) {
        StringBuilder builder = new StringBuilder();
        if (rows != null) {
            for (DBRow row : rows) {
                builder.append(",").append(row.getString("equipment_number"));
            }
        }
        return builder.toString().substring(1);
    }

    // 根据live load 、drop off、swap CNTR 查询停车位
    @Override
    public DBRow[] getAreaBydropOffKey(long ps_id, int dropOffKey, int type,
                                       long mainId) throws Exception {
        try {
            DBRow[] rows = floorCheckInMgrZwb.selectSprotByType(ps_id,
                dropOffKey, type);
            if (type == 1 | type == 2) {
                boolean flag = true;
                for (DBRow row : rows) {
                    DBRow[] result = this.getVacancySpot(ps_id,
                        row.get("area_id", 0), mainId);
                    if (result.length > 0) {
                        row.add("selected", "selected");
                        flag = false;
                        break;
                    }
                }
                if (flag & rows.length > 0)
                    rows[0].add("selected", "selected");
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "getAreaBydropOffKey", log);
        }
    }

    @Override
    public void updatePhoneNumbers() throws Exception {
        try {
            DBRow[] rows = floorCheckInMgrZwb.getYardEntry();
            if (rows != null) {
                for (DBRow row : rows) {
                    DBRow updateRow = new DBRow();
                    updateRow.add("phone_gate_container_no", StringUtil
                        .convertStr2PhotoNumber(row
                            .getString("gate_container_no")));
                    updateRow.add("phone_gate_liscense_plate", StringUtil
                        .convertStr2PhotoNumber(row
                            .getString("gate_liscense_plate")));
                    floorCheckInMgrZwb.modCheckIn(row.get("dlo_id", 0l),
                        updateRow);
                }
            }

        } catch (Exception e) {
            throw new SystemException(e, "updatePhoneNumbers", log);
        }
    }

    @Override
    public DBRow[] getEntryDetailsByEntryId(long entryId) throws Exception {
        try {
            return floorCheckInMgrZwb
                .selectDoorOrLocationOccupancyDetails(entryId);
        } catch (Exception e) {
            throw new SystemException(e, "getEntryDetailsByEntryId", log);
        }
    }

    /**
     * 1.处理装货的过程中请求数据，然后保存在sync数据 2.因为这个接口都是通过master_bol 来的 3.首先解析数据 然后取出Load
     * 建立Load数据(有可能是在MasterBol上带有Load的数据) 4.然后遍历这个Load下面的masterBol 中order的数据
     */
    @Override
    public DBRow[] handFindOrderPalletForLoading(int order_type,
                                                 String master_bol, String CompanyID, String CustomerID, long adid,
                                                 HttpServletRequest request) throws Exception {
        try {

            DBRow[] masterBolInfos = sqlServerMgrZJ.findOrderPalletForLoading(
                order_type, master_bol, CompanyID, CustomerID, adid,
                request);
            if (masterBolInfos != null && masterBolInfos.length > 0) {
                for (DBRow masterBolInfo : masterBolInfos) {
                    String load_no = masterBolInfo.getString("load_no");
                    handWmsLoadOrder(new DBRow[]{masterBolInfo}, CustomerID,
                        CompanyID, load_no);
                }
            }
            return masterBolInfos;
        } catch (Exception e) {
            throw new SystemException(e, "handFindOrderPalletForLoading", log);
        }
    }

    /**
     * 如果是有customerId,companyId 那么就直接调用张艳杰的接口查询
     * 没有的情况那么就是先去查询有几个Company的数据，如果是只有一个那么也返回，如果是有多个那么就返回company的数据让他
     * <p>
     * <p>
     * 因为android 处理关闭Order ,Load
     * 的时候是不能去关闭wms的数据，所以现在处理关闭的情况是先在Sync上面建立2张表wms_load_load
     * ,wms_load_order去记录这个状态 这样在Partially 情况下，就可以处理某些已经关闭了的order，取出两边的数据比较一下
     */
    @Override
    public DBRow findOrderPalletByLoadNo(int order_type, String orderNumber,
                                         String customerId, String companyId, long adid,
                                         HttpServletRequest request) throws Exception, NoRecordsException {
        try {
            if (!StringUtil.isNull(customerId) && !StringUtil.isNull(companyId)) {
                DBRow ordersDetail = new DBRow();
                DBRow[] loadInfos = sqlServerMgrZJ.findOrderPalletForLoading(
                    order_type, orderNumber, companyId, customerId, adid,
                    request);

// handWmsLoadOrder(loadInfos,customerId,companyId,loadNumber);

                ordersDetail.add("customer_id", customerId);
                ordersDetail.add("company_id", companyId);
                ordersDetail.add("load_info", loadInfos);
                return ordersDetail;
            }
            return null;
/*
* 以前是有根据Load去查询的时候有多个customer_id 的情况，先过滤出Company_id,customer_id
* DBRow fixCompanyName = fixCompanyIdAndCustomerId(orderNumber,
* adid, request); int count = fixCompanyName.get("count", 0) ;
* if(count == 0){ throw new NoRecordsException(); } if(count == 1){
* DBRow[] rows = (DBRow[])fixCompanyName.get("datas", new
* DBRow[0]); if(rows != null && rows.length == 1){ DBRow
* ordersDetail = new DBRow(); ordersDetail.add("count", 1); String
* customer_id = rows[0].getString("customer_id") ; String
* company_id = rows[0].getString("company_id") ;
*
* ordersDetail.add("customer_id",customer_id );
* ordersDetail.add("company_id", company_id); DBRow loadInfos[] =
* handFindOrderPalletForLoading(order_type, orderNumber,
* company_id, customer_id, adid, request);
* ordersDetail.add("load_info", loadInfos); return ordersDetail ; }
* } return fixCompanyName ;
*/
        } catch (NoRecordsException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "getEntryDetailsByEntryId", log);
        }
    }

    /**
     * 处理wms过来的数据, 1.插入到本地的数据库，如果本地数据库不存在的情况 2.如果是本地数据库已经存在了，那么查询数据 . 处理
     * 如果是关闭了的就不用返回数据了
     *
     * @throws Exception
     */
    private void handWmsLoadOrder(DBRow[] masterBolInfos, String customer_id,
                                  String company_id, String load_number) throws Exception {
        try {
            DBRow queryRow = wmsLoadMgrZr.queryLoadBy(load_number, customer_id,
                company_id);
            if (queryRow != null) {
                handLoadNumberExits(masterBolInfos,
                    queryRow.get("wms_load_id", 0l), customer_id,
                    company_id);
            } else {
                DBRow loadRow = new DBRow();
                loadRow.add("load_number", load_number);
                loadRow.add("customer_id", customer_id);
                loadRow.add("company_id", company_id);
                loadRow.add("status", WmsOrderStatusKey.OPEN);
                handLoadNumberNotExits(masterBolInfos, loadRow);
            }
        } catch (Exception e) {
            throw new SystemException(e, "handWmsLoadOrder", log);
        }
    }

    private HoldDoubleValue<Boolean, Long> isSyncMasterBolNoInWms(
        DBRow syncMasterBolInfo, DBRow[] wmsMasterBolInfos) {
        boolean flag = false;
        long master_bol_no = syncMasterBolInfo.get("master_bol_no", 0l);
        long sync_master_bol_id = syncMasterBolInfo
            .get("wms_master_bol_id", 0l);
        for (DBRow wmsMasterBol : wmsMasterBolInfos) {
            if (wmsMasterBol.get("master_bol_no", 0l) == master_bol_no) {
                flag = true;
                break;
            }
        }
        return new HoldDoubleValue<Boolean, Long>(flag, sync_master_bol_id);
    }

/**
 * 首先删除我们本地是数据，然后再添加新的数据
 *
 * @param wms_master_bol_id
 * @param row
 * @throws Exception
 */
/*
* private void handMasterBolInfoNotFound(long wms_master_bol_id, DBRow
* masterBolInfo, long wms_load_id)throws Exception{ try{
*
* DBRow logRow = wmsLoadMgrZr.getWmsLoadInfo(wms_load_id);
* wmsLoadMgrZr.deleteWmsMasterBolInfo(wms_master_bol_id); //删除
* handLoadNumberNotExits(new DBRow[]{masterBolInfo}, logRow);
*
* }catch (Exception e) { throw new
* SystemException(e,"handMasterBolInfoNotFound",log); } }
*/

    /**
     * 如果存在的时候 1.因为他们有可能调整Load上面的Order，所以我会和我本地的数据比较一下，如果只要有一个OrderNo对应不上，
     * 如果对应不上那么我会删除我本地load关联的所有的数据 重新添加一次
     * <p>
     * 2.同时加上WmsOrderId,我们系统当中的
     *
     * @param load_id
     * @throws Exception
     */
    private void handLoadNumberExits(DBRow[] wmsMasterBolInfos, long load_id,
                                     String customer_id, String company_id) throws Exception {
        try {
            DBRow[] exitsMasterBols = wmsLoadMgrZr
                .getMastBolRowByLoadId(load_id); // sync 数据库中 的数据 删除
            for (DBRow syncMasterBols : exitsMasterBols) {
                HoldDoubleValue<Boolean, Long> returnHoldValue = isSyncMasterBolNoInWms(
                    syncMasterBols, wmsMasterBolInfos);
                if (returnHoldValue.a) {
// 如果存在那么删除
                    wmsLoadMgrZr.deleteWmsMasterBolInfo(returnHoldValue.b); // 删除
                }
            }
// 删除有的数据，然后再添加
            adWmsMasterBolInfo(wmsMasterBolInfos, company_id, customer_id,
                load_id);
        } catch (Exception e) {
            throw new SystemException(e, "handLoadNumberExits", log);
        }
    }

    /**
     * 首先判断masterbols 如果相等 再去判断 order 是否都相等
     *
     */
    private void handMasterBolIsOrderSame(DBRow[] exitsMasterBols,
                                          DBRow[] wmsMasterInfos) throws Exception {
        try {

/*
* System.out.println(" exitsMasterBols length : " +
* exitsMasterBols.length );
* System.out.println(" wmsMasterInfos length : " +
* wmsMasterInfos.length );
*/
        } catch (Exception e) {
            throw new SystemException(e, "handIsOrderSame", log);
        }
    }

    /**
     * 查询Sync数据库中的数据是否已经Close掉了
     *
     * @param exitsOrders
     * @return
     */
    private HoldDoubleValue<Boolean, Long> isOrderClose(String order_numbers,
                                                        DBRow[] exitsOrders) throws Exception {
        try {
            boolean flag = false;
            long order_id = 0l;
            for (int index = 0, count = exitsOrders.length; index < count; index++) {
                DBRow order = exitsOrders[index];
                if (order.getString("order_numbers").equalsIgnoreCase(
                    order_numbers)) {
                    order_id = order.get("wms_order_id", 0l); // 添加wms_order_id
                    if (order.getString("status").equals(
                        WmsOrderStatusKey.CLOSED)) {
                        flag = true;
                        break;
                    }
                }
            }
            return new HoldDoubleValue<Boolean, Long>(flag, order_id);
        } catch (Exception e) {
            throw new SystemException(e, "isOrderClose", log);
        }
    }

// [{"orderno":"98471","pallets":"3","od_status":"Closed"

    /**
     * 1.对于没有Load数据的情况，首先插入到数据库 2.order 的状态都是Open
     *
     * @param loadRow
     * @throws Exception
     */
    private void handLoadNumberNotExits(DBRow[] mastbolInfos, DBRow loadRow)
        throws Exception {
        try {

            String company_id = loadRow.getString("company_id");
            String customer_id = loadRow.getString("customer_id");
            long wms_load_id = wmsLoadMgrZr.addLoad(loadRow);
            adWmsMasterBolInfo(mastbolInfos, company_id, customer_id,
                wms_load_id);
        } catch (Exception e) {
            throw new SystemException(e, "handLoadNumberNotExits", log);
        }
    }

    private void adWmsMasterBolInfo(DBRow[] mastbolInfos, String company_id,
                                    String customer_id, long wms_load_id) throws Exception {
        try {
            if (mastbolInfos != null && mastbolInfos.length > 0) {
                for (int index = 0, count = mastbolInfos.length; index < count; index++) {
                    DBRow mastbolInfo = mastbolInfos[index];
                    DBRow masbolRow = fixMastbolDBRow(mastbolInfo, company_id,
                        customer_id, wms_load_id);
                    long wms_mastbol_id = wmsLoadMgrZr
                        .addWmsMastBolRow(masbolRow);
                    addLoadOrderInfos(mastbolInfo, wms_mastbol_id);
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "adWmsMasterBolInfo", log);
        }
    }

    /**
     * 遍历master_bol 里面Order的信息 ,然后插入到我们系统中的load_order 表
     *
     * @param mastbolInfo
     * @throws Exception
     */
    private void addLoadOrderInfos(DBRow mastbolInfo, long wms_mastbol_id)
        throws Exception {
        try {
            DBRow[] orderInfos = (DBRow[]) mastbolInfo.get("order_info",
                new DBRow[]{});
            if (orderInfos != null && orderInfos.length > 0) {
                for (DBRow row : orderInfos) {
                    DBRow insertOrderRow = new DBRow();

                    insertOrderRow.add("master_bol_id", wms_mastbol_id);
                    insertOrderRow.add("status", WmsOrderStatusKey.OPEN);
                    insertOrderRow.add("order_numbers",
                        row.getString("orderno"));
                    insertOrderRow.add("pallets", row.get("pallets", 0));
                    insertOrderRow.add("staging_area_id",
                        row.getString("stagingareaid"));
                    insertOrderRow.add("pallet_type_id",
                        row.getString("mpallettypeid"));
                    long wms_order_id = wmsLoadMgrZr
                        .addWmsLoadOrder(insertOrderRow);
// row.add("wms_order_id", wms_order_id);不需要返回了
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "addLoadOrderInfos", log);
        }
    }

    /**
     * 组织成master_bol_的数据
     *
     * @param mastbolInfo
     * @param company_id
     * @param customer_id
     * @param wms_load_id
     * @return
     * @throws Exception
     */
    private DBRow fixMastbolDBRow(DBRow mastbolInfo, String company_id,
                                  String customer_id, long wms_load_id) throws Exception {
        try {
            DBRow returnRow = new DBRow();
            returnRow.add("master_bol_no",
                mastbolInfo.getString("master_bol_no"));
            returnRow.add("wms_load_id", wms_load_id);
            returnRow.add("status", WmsOrderStatusKey.OPEN);
            returnRow.add("company_id", company_id);
            returnRow.add("customer_id", customer_id);
            returnRow.add("staging_area_id",
                mastbolInfo.getString("staging_area_id"));
            returnRow.add("total_pallets", mastbolInfo.get("total_pallets", 0));
            return returnRow;
        } catch (Exception e) {
            throw new SystemException(e, "fixMastbolDBRow", log);
        }
    }

    private DBRow[] fixLoadDatas(DBRow[] arrays) {
        CheckInChildDocumentsStatusTypeKey typeKey = new CheckInChildDocumentsStatusTypeKey();
        List<DBRow> arrayList = new ArrayList<DBRow>();
        Map<String, List<DBRow>> maps = new HashMap<String, List<DBRow>>();
        if (arrays != null && arrays.length > 0) {
            for (DBRow row : arrays) {

                String number = row.getString("number");
                int numberType = row.get("number_type", 0);

                if (numberType == ModuleKey.CHECK_IN_LOAD
                    || numberType == ModuleKey.CHECK_IN_PONO
                    || numberType == ModuleKey.CHECK_IN_ORDER) {

                    DBRow insertRow = new DBRow();
                    String doorName = row.getString("doorId");
                    List<DBRow> innerLoadNumbers = maps.get(doorName);

                    innerLoadNumbers = innerLoadNumbers == null ? new ArrayList<DBRow>()
                        : innerLoadNumbers;
                    insertRow.add("number", number);
                    insertRow.add("status", typeKey
                        .getContainerTypeKeyValue(row.get("number_status",
                            -1)));
                    insertRow.add("status_int", row.get("number_status", -1));

// 徐佳升级过后执行的东西
                    insertRow.add("customer_id", row.getString("customer_id"));
                    insertRow.add("company_id", row.getString("company_id"));
                    insertRow.add("number_type", row.get("number_type", 0));
                    insertRow.add("account_id", row.getString("account_id"));
                    insertRow
                        .add("dlo_detail_id", row.get("dlo_detail_id", 0l));
                    if (numberType == ModuleKey.CHECK_IN_ORDER) {
                        insertRow.add("append_number", row.getString("po_no"));
                        insertRow.add("append_type", ModuleKey.CHECK_IN_PONO);
                    }
                    if (numberType == ModuleKey.CHECK_IN_PONO) {
                        insertRow.add("append_number",
                            row.getString("order_no"));
                        insertRow.add("append_type", ModuleKey.CHECK_IN_ORDER);
                    }

                    innerLoadNumbers.add(insertRow);
                    maps.put(doorName, innerLoadNumbers);
                }
            }
        }
        Iterator<Entry<String, List<DBRow>>> it = maps.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, List<DBRow>> entry = it.next();
            DBRow inner = new DBRow();
            inner.add("loads", entry.getValue().toArray(new DBRow[0]));
            inner.add("door", entry.getKey());
            arrayList.add(inner);
        }
        return arrayList.toArray(new DBRow[0]);
    }

    /**
     * 过滤出只是Load PO,Order 的单子
     *
     * @param arrays
     * @return
     * @author zhangrui
     * @Date 2014年9月16日
     */
    private DBRow[] filterLoadDetail(DBRow[] arrays) {
        List<DBRow> arrayList = new ArrayList<DBRow>();
        if (arrays != null && arrays.length > 0) {
            for (DBRow row : arrays) {
                int numberType = row.get("number_type", 0);
                if (numberType == ModuleKey.CHECK_IN_LOAD
                    || numberType == ModuleKey.CHECK_IN_ORDER
                    || numberType == ModuleKey.CHECK_IN_PONO) {
                    arrayList.add(row);
                }
            }
        }
        return arrayList.toArray(new DBRow[0]);
    }

    /**
     * 1.目前只是处理Load ,order po.子单据的数据。 2.如果是当前当前Entry下最后一个Load那么提示是否 停留 或者 离开
     * 3.如果是当前门下最后一个单子，那么提示 是否 释放这个门。
     * <p>
     * 最后一个表示的：number_status 当前最门的状态是 1.未处理 2.处理中3.关闭.4异常 4.添加日志 。 5.close
     * 过后那么直接关闭Wms系统中的数据 6.子单据添加Finish time 7.Load 有多个Master_bol
     * 的时候，只有最后一个Master_bol关闭的时候那么才会执行Close
     * <p>
     * <p>
     * 多个master_bol 关闭第一个 肯定是 正常关闭， 关闭第二个的时候那么会真真的调用Close 这个时候要计算放回的flag
     */
    private int returnFlag = 0;

    @Override
    public int closeLoad(final DBRow row, final AdminLoginBean adminLoginBean)
        throws Exception {
        try {
            returnFlag = 0;
            sqlServerTransactionTemplate
                .execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(
                        TransactionStatus txStat) {
                        try {

                            long entryId = row.get("entry_id", 0l);
                            long dlo_detail_id = row.get("dlo_detail_id",
                                0l);
                            int closeType = row.get("close_type", 0); // close,
// exception,partially
                            long adid = adminLoginBean.getAdid();
                            String masterBolNo = row
                                .getString("master_bol_no"); // 如果一个Load
// 有多个MasterBol的时候，只有所有的Master_bol都关闭了才会走这个方法
                            String pro_no = row.getString("pro_no");
                            String filePath = row.getString("filePath");
                            long ps_id = adminLoginBean.getPs_id();
                            long equipment_id = row.get("equipment_id", 0l);
                            int resources_type = row.get("resources_type",
                                0);
                            long resources_id = row.get("resources_id", 0l);
                            String note = row.getString("note");
                            String option_file_param = row
                                .getString("option_file_param");
                            String session_id = row.getString("session_id");

                            DBRow entry = floorCheckInMgrZwb
                                .getEntryIdById(entryId); // 主单据
                            DBRow currentLoadRow = floorCheckInMgrZwb
                                .selectDetailByDetailId(dlo_detail_id);
                            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                                .getBeanFromContainer("proxyCheckInMgrZr");

                            DBRow equipmentSeals = checkInMgrZr
                                .getEquipmentSeals(equipment_id);
                            String seals = "";
                            if (equipmentSeals != null) {
                                seals = equipmentSeals
                                    .getString("out_seal");
                            }
                            seals = StringUtil.isBlank(seals) ? "NA"
                                : seals;
                            if (currentLoadRow == null || entry == null) {
                                throw new LoadNumberNotExistException();
                            }
                            currentLoadRow
                                .add("doorId",
                                    resources_type == OccupyTypeKey.DOOR ? checkInMgrZr
                                        .getDoorName(resources_id)
                                        : "Spot:"
                                        + checkInMgrZr
                                        .getSpotName(resources_id));
                            int number_type = currentLoadRow.get(
                                "number_type", 0);
                            String shipDate = DateUtil.NowStr();
// 张睿修改 shipDate 时间，Elwood
// 在wms系统中是存放的Elwood的时间，所以我需要装换一下时间
                            try {
                                shipDate = DateUtil.showLocalTime(shipDate,
                                    ps_id); // 怕时间出错，影响正常的流程

                            } catch (Exception e) {
                                log.error("zhangrui close detail..."
                                    + shipDate + ps_id);
                            }
                            loadPaperWorkMgrZr.closeSyncDetailInfo(
                                currentLoadRow.get("LR_ID", 0l),
                                masterBolNo, pro_no, currentLoadRow); // 在我们系统中写入Pro_no，close的状态

                            if (number_type == ModuleKey.CHECK_IN_LOAD) {
                                if (loadPaperWorkMgrZr
                                    .isLoadMasterBolAllClose(currentLoadRow
                                        .get("LR_ID", 0l))) { // 现在是改成如果Load有多个MasterBol的时候只有当两个MasterBol关闭了，那么才会关闭我们的。同时会循环的关闭他们的MasterBol
// 修改子明细
                                    updateDetailByFinishTimeAndCloseTypeNote(
                                        currentLoadRow, closeType,
                                        DateUtil.NowStr(), note,
                                        resources_type, resources_id,
                                        adid);
                                    returnFlag = closeWmsAndSyncLoad(
                                        filePath, pro_no, shipDate,
                                        seals, currentLoadRow, entry,
                                        adminLoginBean, equipment_id,
                                        closeType,
                                        row.getString("config"),
                                        option_file_param, session_id);
                                } else {
                                    returnFlag = LoadCloseNoifyNormal;
                                }
                            } else {
// 修改子明细
                                updateDetailByFinishTimeAndCloseTypeNote(
                                    currentLoadRow, closeType,
                                    DateUtil.NowStr(), note,
                                    resources_type, resources_id, adid);
                                returnFlag = closeWmsAndSyncOrder(filePath,
                                    pro_no, shipDate, seals,
                                    currentLoadRow, entry,
                                    adminLoginBean, equipment_id,
                                    row.getString("config"),
                                    option_file_param, session_id); // 因为PO
// 和
// Order
// 都是保存的Order
// 在wms_order
// 表中
                            }
                            if (returnFlag == 0) {
                                throw new RuntimeException();
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            return returnFlag;
        } catch (Exception e) {
            throw new SystemException(e, "closeLoad", log);
        }
    }

    /**
     * Load 时候异常关闭的情况 。 不会关闭Wms,Exception 假如有Load的数据那么删除
     *
     * @param row
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date 2014年11月3日
     */
    @Override
    public int exceptionLoad(DBRow row, AdminLoginBean adminLoginBean)
        throws Exception {
        try {

/*
* String detail_ids = data.getString("detail_id"); String filePath
* = data.getString("filePath"); long adid =
* adminLoggerBean.getAdid(); long entry_id = data.get("entry_id",
* 0l); long rl_id = data.get("resources_id", 0l); long equipment_id
* = data.get("equipment_id", 0l); int resourcesType =
* data.get("resources_type", 0); number_status
*/

            long entryId = row.get("entry_id", 0l);
            long dlo_detail_id = row.get("dlo_detail_id", 0l);
            long equipment_id = row.get("equipment_id", 0l);
            int closeType = row.get("close_type", 0); // close,
// exception,partially
            long adid = adminLoginBean.getAdid();
            String note = row.getString("note");
            DBRow entry = floorCheckInMgrZwb.getEntryIdById(entryId); // 主单据
            DBRow currentLoadRow = floorCheckInMgrZwb
                .selectDetailByDetailId(dlo_detail_id);
            int resources_type = row.get("resources_type", 0);
            long resources_id = row.get("resources_id", 0l);

            if (currentLoadRow == null || entry == null) {
// 当前LoadNumber不在 entry子单据中存在
                throw new LoadNumberNotExistException();
            }
            long lr_id = currentLoadRow.get("lr_id", 0l);
            int number_type = currentLoadRow.get("number_type", 0);

            String shipDate = DateUtil.NowStr();
            String masterBolNo = row.getString("master_bol_no");
            loadPaperWorkMgrZr.closeSyncDetailInfo(lr_id, masterBolNo, "",
                currentLoadRow); // 在我们系统中写入Pro_no，close的状态

            if (number_type == ModuleKey.CHECK_IN_LOAD) {
                if (loadPaperWorkMgrZr.isLoadMasterBolAllClose(lr_id)) { // 现在是改成如果Load有多个MasterBol的时候只有当两个MasterBol关闭了，那么才会关闭我们的。同时会循环的关闭他们的MasterBol
// 修改子明细
                    returnFlag = closeDetailReturnFlag(equipment_id, entryId,
                        dlo_detail_id);
                    updateDetailByFinishTimeAndCloseTypeNote(currentLoadRow,
                        closeType, shipDate, note, resources_type,
                        resources_id, adid);
                } else {
                    returnFlag = LoadCloseNoifyNormal;
                }
            } else {
// 修改子明细
                returnFlag = closeDetailReturnFlag(equipment_id, entryId,
                    dlo_detail_id);
                updateDetailByFinishTimeAndCloseTypeNote(currentLoadRow,
                    closeType, shipDate, note, resources_type,
                    resources_id, adid);
            }
            if (returnFlag == 0) {
                throw new RuntimeException();
            }

/**
 * 因为Load和inputseal loadBar
 * 都是放在一起了，所以在close的的时候就应该插入LoadBar，和seal到我们的数据库中 2014-12-20 ?
 */
/*
* if(row.get("is_last", 0) == 1 && !StringUtil.isNull(seals)){
* DBRow updateRow = new DBRow(); updateRow.add("used_time",
* DateUtil.NowStr()); updateRow.add("seal_status",2);
* floorCheckInMgrZwb.updateSealUsed(seals, updateRow); }
*/

// int checkInLogType = (closeType ==
// CheckInChildDocumentsStatusTypeKey.EXCEPTION ?
// CheckInLogTypeKey.AndroidExceptionTask :
// CheckInLogTypeKey.AndroidPartiallyTask);
// 记录日志
// String closeFlag = getCloseFlag(number_type);
// addCheckInLog(adid,
// logTypeKey.getCheckInLogTypeKeyValue(checkInLogType), entryId,
// checkInLogType, DateUtil.NowStr(), closeFlag+" :" +number );
// 移动文件，

            DBRow fileRow = new DBRow();
            fileRow.add("file_with_class",
                FileWithCheckInClassKey.PhotoTaskProcessing);
            fileRow.add("file_with_id", entryId);
            fileRow.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
            fileRow.add("file_with_detail_id", dlo_detail_id);
            fileRow.add("filePath", row.getString("filePath"));
/*
* if(closeType == CheckInChildDocumentsStatusTypeKey.EXCEPTION){
* //如果是Exception ，那么是表示货物没有装走
* wmsLoadMgrZr.deleteScanPalletType(dlo_detail_id); }
*/
            fileMgrZr.androidUpZipPictrue(fileRow, adminLoginBean);
            return returnFlag;
        } catch (Exception e) {
            throw new SystemException(e, "exceptionLoad", log);
        }

    }

    private int closeWmsAndSyncLoad(String filePath, String pro_no,
                                    String shipDate, String seals, DBRow currentLoadRow, DBRow entry,
                                    AdminLoginBean adminLoginBean, long equipment_id, int closeType,
                                    String fileConfig, String option_file_param, String sessionId)
        throws Exception {
        try {
            long adid = adminLoginBean.getAdid();
            long entryId = entry.get("dlo_id", 0l);
            String fixCloseFlag = getCloseFlag(currentLoadRow.get(
                "number_type", 0));
            DBRow equipment = getEquipmentByEquipmentId(equipment_id);
            String vehicle = equipment != null ? equipment
                .getString("equipment_number") : ""; // entry.getString("gate_container_no");
            String dockName = currentLoadRow.getString("doorId");
            String carrierName = entry.getString("company_name");
            String userName = adminLoginBean.getEmploye_name();
            int nofityFlag = closeDetailReturnFlag(equipment_id, entryId,
                currentLoadRow.get("dlo_detail_id", 0l));
            loadPaperWorkMgrZr.closeWmsMasterBol(currentLoadRow, pro_no, adid,
                shipDate, seals, vehicle, dockName, carrierName, userName,
                closeType);
// addCheckInLog(adid, "Closed "+fixCloseFlag, entryId,
// CheckInLogTypeKey.ANDROID_DOCK_CLOSE, DateUtil.NowStr(),
// fixCloseFlag+" :" +number );
            loadCloseUpFiles(filePath, adid, entryId, fixCloseFlag + "_close_",
                currentLoadRow.get("dlo_detail_id", 0l), fileConfig,
                option_file_param, sessionId);
            return nofityFlag;
        } catch (Exception e) {
            throw new SystemException(e, "closeWmsAndSyncLoad", log);
        }
    }

    private int closeWmsAndSyncOrder(String filePath, String pro_no,
                                     String shipDate, String seals, DBRow currentLoadRow, DBRow entry,
                                     AdminLoginBean adminLoginBean, long equipment_id,
                                     String fileConfig, String option_file_param, String sessionId)
        throws Exception {
        try {
            long adid = adminLoginBean.getAdid();
            long entryId = entry.get("dlo_id", 0l);
// 关闭wms系统中的数据，关闭Sync系统中的数据，写回数据，finish time
            String fixCloseFlag = getCloseFlag(currentLoadRow.get(
                "number_type", 0)); // number_type);
            DBRow equipment = getEquipmentByEquipmentId(equipment_id);
            String vehicle = equipment != null ? equipment
                .getString("equipment_number") : ""; // entry.getString("gate_container_no");
            String dockName = currentLoadRow.getString("doorId");
            String carrierName = entry.getString("company_name");
            String userName = adminLoginBean.getEmploye_name();
            loadPaperWorkMgrZr.closeWmsOrder(currentLoadRow, pro_no, adid,
                shipDate, seals, vehicle, dockName, carrierName, userName);
            int nofityFlag = closeDetailReturnFlag(equipment_id, entryId,
                currentLoadRow.get("dlo_detail_id", 0l)); // dlo_detail_id);
// 添加图片
// androidSaveMoveFile(filePath, adid, entryId,
// fixCloseFlag+"_close_");
            loadCloseUpFiles(filePath, adid, entryId, fixCloseFlag + "_close_",
                currentLoadRow.get("dlo_detail_id", 0l), fileConfig,
                option_file_param, sessionId);
            return nofityFlag;
        } catch (Exception e) {
            throw new SystemException(e, "closeWmsAndSyncOrder", log);
        }
    }

    private int getFileWithClassAndFileWithDetailId(JSONArray jsonArray,
                                                    String fileName) throws Exception {
        if (jsonArray != null && jsonArray.length() > 0) {
            for (int index = 0, count = jsonArray.length(); index < count; index++) {
                JSONObject obj = jsonArray.getJSONObject(index);
                if (obj.getString("name").equalsIgnoreCase(fileName)) {
                    if (!StringUtil.isBlank(StringUtil.getJsonString(obj,
                        "file_with_class"))) {
                        return Integer.parseInt(obj
                            .getString("file_with_class"));
                    }
                }
            }
        }
        return FileWithCheckInClassKey.PhotoDockClose;
    }

    /**
     * 在Closetask的时候有上传CountingSheet 的时候重新对图片做
     *
     * @author zhangrui
     * @Date 2015年3月4日
     */
    private void loadCloseUpFiles(String filePath, long adid, long dlo_id,
                                  String prefix, long detail_id, String config,
                                  String option_file_param, String session_id) throws Exception {
        if (!StringUtil.isBlank(filePath)) {
// String fileDescBasePath = Environment.getHome() +
// "upload/check_in/" ;
            JSONArray jsonArray = null;
            if (!StringUtil.isNull(config)) {
                jsonArray = new JSONArray(config);
            }
            File dir = new File(filePath);
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (File file : files) {
// PhotoCountingSheet图片前缀修改
                    String _prefix = prefix;

                    int fileWithClass = getFileWithClassAndFileWithDetailId(
                        jsonArray, file.getName());
                    if (fileWithClass == FileWithCheckInClassKey.PhotoCountingSheet) {
                        _prefix = "CountSheet_";
                    }
/*
* DBRow fileRow = new DBRow();
* fileRow.add("file_name",_prefix+file.getName());
* fileRow.add("file_with_id",dlo_id);
* fileRow.add("file_with_type"
* ,FileWithTypeKey.OCCUPANCY_MAIN);
* fileRow.add("file_with_class",fileWithClass);
* fileRow.add("upload_adid",adid);
* fileRow.add("upload_time",DateUtil.NowStr());
* fileRow.add("file_is_convert", 0);
* fileRow.add("file_convert_file_path", "");
* fileRow.add("file_with_detail_id", detail_id);
* fileRow.add("option_file_param", option_file_param);
* floorFileMgrZr.addFile(fileRow);
* FileUtil.checkInAndroidCopyFile(file,_prefix ,
* fileDescBasePath);
*/

// 改为文件服务器上传 --by chenchen
                    DBRow[] rows = fileMgrZr.uploadToFileServ(
                        file.getAbsolutePath(), session_id);
                    if (rows != null && rows.length > 0) {
                        DBRow f = rows[0];
                        long file_id = f.get("file_id", 0l);
// 更新file表
                        DBRow r = new DBRow();
                        r.add("file_name", _prefix + file.getName());
                        r.add("file_with_id", dlo_id);
                        r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
                        r.add("file_with_class", fileWithClass);
                        r.add("file_with_detail_id", detail_id);
                        r.add("expire_in_secs", 0);
                        r.add("option_file_param", option_file_param);
                        fileMgrZr.updateFile(r, file_id);
                    }
                }
            }
        }
    }

    private String getCloseFlag(int number_type) {
        String closeFlag = "Load";
        if (number_type == ModuleKey.CHECK_IN_ORDER) {
            closeFlag = "Order";
        } else if (number_type == ModuleKey.CHECK_IN_PONO) {
            closeFlag = "Pono";
        }
        return closeFlag;
    }

    private int returnNotifyFlag(long dlo_detail_id, DBRow[] details,
                                 DBRow currentLoadRow, DBRow equipmentResources) {
        int nofityFlag = LoadCloseNoifyNormal;
        if (isLastOfEntry(dlo_detail_id, details)) {
            nofityFlag = LoadCloseNotifyStayOrLeave;
        } else if (isLastOfResources(currentLoadRow, details)) {
// 如果某个资源下最后一个单据关闭时，与当前设备占用的资源不一致则不会提示是否release当前设备的资源
// ，即不将flag设置为2（LoadCloseNotifyReleaseDoor）即可 wfh
            if (equipmentResources != null
                && equipmentResources.get("resources_type", 0) == currentLoadRow
                .get("resources_type", 0)
                && equipmentResources.get("resources_id", 0) == currentLoadRow
                .get("resources_id", 0)) {
                nofityFlag = LoadCloseNotifyReleaseDoor;
            }
        }
        return nofityFlag;
    }

    /**
     * 设备停留或者是离开的时候 如果这个设备是车头 && 已经离开 ，那么就不要更新了 停留 或者
     * 离开的时候要把设备的check_out_warehouse_time写入
     *
     * @param equipment_id
     * @throws Exception
     * @author zhangrui
     * @Date 2014年12月3日
     */
    @Override
    public void leavingOrStay(long equipment_id, int status, long adid)
        throws Exception {
        try {
            DBRow equipment = ymsMgrAPI.getDetailEquipment(equipment_id);
            if (equipment != null && status > 0) {
                if (equipment.get("equipment_type", 0) == EquipmentTypeKey.Tractor
                    && equipment.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.LEFT) {
                    return;
                }
                DBRow updateRow = new DBRow();
                updateRow.add("equipment_status", status);
                if (status == CheckInMainDocumentsStatusTypeKey.LEAVING) {
                    updateRow
                        .add("check_out_warehouse_time", DateUtil.NowStr()); // 写入check_out_time
// 离开 释放门
                    ymsMgrAPI.releaseResourcesByRelation(
                        SpaceRelationTypeKey.Equipment, equipment_id, adid);
                }
                ymsMgrAPI.updateEquipment(equipment_id, updateRow);
            }

        } catch (Exception e) {
            throw new SystemException(e, "leavingOrStay", log);
        }
    }

    @Override
    public void closeLoadStayOrLeave(DBRow data, DBRow loginRow)
        throws LoadNumberNotExistException, Exception {
        try {
            int is_leave = data.get("is_leave", 0); // 2 leave , 1 : 停留
            long dlo_detail_id = data.get("dlo_detail_id", 0l);
            long entryId = data.get("entry_id", 0l);
            long equipment_id = data.get("equipment_id", 0l);
            DBRow[] details = getEntryDetailHasResources(entryId, equipment_id);
            DBRow currentLoadRow = filterCurrentLoadRow(dlo_detail_id, details);
            if (currentLoadRow == null) {
// 当前LoadNumber不在 entry子单据中存在
                throw new LoadNumberNotExistException();
            }
            int fixIsleave = (is_leave == 2 ? CheckInMainDocumentsStatusTypeKey.LEAVING
                : CheckInMainDocumentsStatusTypeKey.INYARD);
            leavingOrStay(equipment_id, fixIsleave, loginRow.get("adid", 0l));
        } catch (LoadNumberNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "closeLoadStayOrLeave", log);
        }
    }

    /**
     * 完成单据的时候更新数据
     *
     * @param detailRow
     * @param closeType
     * @param finishTime
     * @param resources_type
     * @param resources_id
     * @param adid
     * @throws Exception
     * @author zhangrui
     * @Date 2015年1月3日
     */
    private void updateDetailByFinishTimeAndCloseType(DBRow detailRow,
                                                      int closeType, String finishTime, int resources_type,
                                                      long resources_id, long adid) throws Exception {
        try {
            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                .getBeanFromContainer("proxyCheckInMgrZr");
            DBRow updateDetail = new DBRow();
            updateDetail.add("number_status", closeType);
            updateDetail.add("finish_time", finishTime);
            checkInMgrZr.finishDetail(detailRow, resources_id, updateDetail,
                adid, resources_type);
        } catch (Exception e) {
            throw new SystemException(e, "updateLoadDetail", log);
        }
    }

    private void updateDetailByFinishTimeAndCloseTypeNote(DBRow detailRow,
                                                          int closeType, String finishTime, String note, int resources_type,
                                                          long resources_id, long adid) throws Exception {
        try {
            CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                .getBeanFromContainer("proxyCheckInMgrZr");
            DBRow updateDetail = new DBRow();
            updateDetail.add("number_status", closeType);
            updateDetail.add("finish_time", finishTime);
            updateDetail.add("note", note);
            checkInMgrZr.finishDetail(detailRow, resources_id, updateDetail,
                adid, resources_type);

        } catch (Exception e) {
            throw new SystemException(e,
                "updateDetailByFinishTimeAndCloseTypeNote", log);
        }
    }

    /**
     * 这个方法可能会改，因为新加入了 PO,Order
     *
     * @param arrays
     * @return
     */
    private DBRow filterCurrentLoadRow(long dlo_detail_id, DBRow[] arrays) {
        for (DBRow row : arrays) {
            long otherDloDetailId = row.get("dlo_detail_id", 0l);
            if (dlo_detail_id == otherDloDetailId) {
                return row;
            }
        }
        return null;
    }

    /**
     * 当前的Entry是否是 门下面的最后一个子单据 是否是某个resouce下面的最后一个 currentLoadRow 一定要通过detail
     * left join space表中的数据
     *
     * @return
     */
    private boolean isLastOfResources(DBRow currentLoadRow, DBRow[] details) {
        HoldDoubleValue<Long, Integer> returnFixResouces = returnFixResourcesIdAndResourcesType(currentLoadRow);
        DBRow[] idThisDoor = filterDetailsInDoor(returnFixResouces.a,
            returnFixResouces.b, details);
        boolean flag = true;
        for (DBRow row : idThisDoor) {
            String otherLoadNumber = row.getString("number").trim();
            if (!StringUtil.isNull(otherLoadNumber)
                && row.get("dlo_detail_id", 0l) != currentLoadRow.get(
                "dlo_detail_id", 0l) && !isEntryDetailIsFinish(row)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 返回 resources_id，resources_type currentRow 必须是 Detail left join space表的记录
     *
     * @param currentLoadRow
     * @return
     * @author zhangrui
     * @Date 2014年12月16日
     */
    @Override
    public HoldDoubleValue<Long, Integer> returnFixResourcesIdAndResourcesType(
        DBRow currentLoadRow) {
        int resources_type = currentLoadRow.get("resources_type", 0);
        long resources_id = currentLoadRow.get("resources_id", 0l);

        if (resources_type == 0) {
            resources_type = currentLoadRow.get("occupancy_type", 0);
        }
        if (resources_id == 0) {
            resources_id = currentLoadRow.get("rl_id", 0l);
        }
        currentLoadRow.add("resources_id", resources_id);
        currentLoadRow.add("resources_type", resources_type);
        currentLoadRow.add("relation_type", SpaceRelationTypeKey.Task);
        return new HoldDoubleValue<Long, Integer>(resources_id, resources_type);
    }

    /**
     * 把过滤出这个 所有子单据的 当前门下的 所有的单据
     *

     * @return
     */
    @Override
    public DBRow[] filterDetailsInDoor(long resources_id, int resources_type,
                                       DBRow[] details) {
        List<DBRow> returnArray = new ArrayList<DBRow>();
        for (DBRow row : details) {
            HoldDoubleValue<Long, Integer> tempValue = returnFixResourcesIdAndResourcesType(row);
            if (tempValue.equals(new HoldDoubleValue<Long, Integer>(
                resources_id, resources_type))) {
                returnArray.add(row);
            }
        }
        return returnArray.toArray(new DBRow[0]);
    }

    /**
     * 判断是子单据是不是最后一个，所以门下的最后一个。zhangrui 需要改
     *
     * @param loadNumber
     * @param details
     * @return
     */
    private boolean isLastOfEntry(String loadNumber, DBRow[] details) {
        boolean flag = true;
        for (DBRow row : details) {
            String otherLoadNumber = row.getString("number").trim();

            if (!StringUtil.isNull(otherLoadNumber)
                && !row.getString("number").equalsIgnoreCase(loadNumber)
                && !isEntryDetailIsFinish(row)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 判断是最后一个load
     *
     * @param details
     * @return
     */
    private boolean isLastOfEntry(long dlo_detail_id, DBRow[] details) {
        boolean flag = true;
        for (DBRow row : details) {
            String otherLoadNumber = "";
            otherLoadNumber = row.getString("number").trim();
            if (!StringUtil.isNull(otherLoadNumber)
                && dlo_detail_id != row.get("dlo_detail_id", 0l)
                && !isEntryDetailIsFinish(row)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /**
     * 盘点子单据是否完成了 number_status 当前最门的状态是 1.未处理 2.处理中3.关闭.4异常 ,5 partially
     *
     * @return
     */
    @Override
    public boolean isEntryDetailIsFinish(DBRow row) {
        int numberStatus = row.get("number_status", -1);
        if (numberStatus == 3 || numberStatus == 4 || numberStatus == 5) {
            return true;
        }
        return false;
    }

    @Override
    public void submitLoadPalletInfo(DBRow data) throws Exception {
        try {
            String company_id = data.getString("company_id");
            String customer_id = data.getString("customer_id");
            String master_bol_no = data.getString("master_bol_no");

// 这里应该根据master_bol_no,company_id,customer_id去查询
            DBRow row = wmsLoadMgrZr.queryMasterBolBy(master_bol_no,
                customer_id, company_id);
            if (row == null) {
                throw new SystemException();
            }
            long wms_master_bol_id = row.get("wms_master_bol_id", 0l);
            DBRow[] orderInfos = (DBRow[]) data.get("order_infos",
                new DBRow[]{});
            for (DBRow orderInfo : orderInfos) {
                long orderno = orderInfo.get("orderno", 0l);
// 根据orderno 和 master_bol_id 去查询order_id
                DBRow orderRow = wmsLoadMgrZr.queryOrderBy(wms_master_bol_id,
                    String.valueOf(orderno));
                if (orderRow == null) {
                    throw new SystemException();
                }
                DBRow[] palletInfoRows = (DBRow[]) orderInfo.get("pallet_info",
                    new DBRow[]{});
                for (DBRow palletRow : palletInfoRows) {
                    palletRow.add("wms_order_id",
                        orderRow.get("wms_order_id", 0l));
// insert
// wmsLoadMgrZr.addPalletTypeRow(palletRow) ;

                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "submitLoadPalletInfo", log);
        }
    }

    @Override
    public void loadEntryLastStayOrLeave(DBRow row,
                                         AdminLoginBean adminLoginBean) throws CheckInNotFoundException,
        Exception {
        try {
            long entry_id = row.get("entry_id", 0l);
            int is_leave = row.get("is_leave", 0);
            long dlo_detail_id = row.get("dlo_detail_id", 0l);
            long equipment_id = row.get("equipment_id", 0l);
            long adid = adminLoginBean.getAdid();
            DBRow detailRow = floorCheckInMgrZwb
                .selectDetailByDetailId(dlo_detail_id);
            if (detailRow == null) {
                throw new SystemException();
            }
            DBRow statusRow = floorCheckInMgrZwb.findGateCheckInById(entry_id);
            if (statusRow == null) {
                throw new CheckInNotFoundException();
            }
            int fixisLeave = (is_leave == 1 ? CheckInMainDocumentsStatusTypeKey.INYARD
                : CheckInMainDocumentsStatusTypeKey.LEAVING);
            leavingOrStay(equipment_id, fixisLeave, adid);
        } catch (CheckInNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "loadEntryLastStayOrLeave", log);
        }
    }

    // releaseDoor zhangrui
    @Override
    public void loadEntryLastReleaseDoor(DBRow row,
                                         AdminLoginBean adminLoginBean) throws Exception {
        try {
// zhangrui
            long entryId = row.get("entry_id", 0l);
            long dlo_detail_id = row.get("dlo_detail_id", 0l);
            long equipment_id = row.get("equipment_id", 0l);
            long adid = adminLoginBean.getAdid();
            DBRow currentLoadRow = floorCheckInMgrZwb
                .selectDetailByDetailId(dlo_detail_id);
            if (currentLoadRow == null) {
// 当前LoadNumber不在 entry子单据中存在
                throw new LoadNumberNotExistException();
            }
// 释放当前设备占用的资源
            ymsMgrAPI.releaseResourcesByRelation(
                SpaceRelationTypeKey.Equipment, equipment_id, adid);

        } catch (Exception e) {
            throw new SystemException(e, "loadEntryLastReleaseDoor", log);
        }
    }

    public DBRow[] selectLoadBar() throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectLoadBar();
        } catch (Exception e) {
            throw new SystemException(e, "selectLoadBar", log);
        }
    }

    @Override
    public DBRow[] selectGateCheckIn(long ps_id, PageCtrl pc) throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectGateCheckIn(ps_id, pc);
            DBRow psTimeSet = floorCheckInMgrZwb.selectPsTimeSet(ps_id);
            int timeOut = psTimeSet != null ? psTimeSet.get("timeout_one",
                TimeSetOut) : TimeSetOut; // -1表示永远不过期
            String nowTime = DateUtil.NowStr();
            CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
            for (int i = 0; i < rows.length; i++) {
                String gate_check_in_operate_time = rows[i].get(
                    "gate_check_in_operate_time", "");
                int is_live = rows[i].get("isLive", 0);
                if (!StringUtil.isBlank(gate_check_in_operate_time)) {
                    Long occupiedTime = ((DateUtil.getDate2LongTime(nowTime) - DateUtil
                        .getDate2LongTime(gate_check_in_operate_time)) / 60000);
                    rows[i].add("occupied_time", occupiedTime);
// 张睿添加一个参数，直接显示是否是应该标红
                    rows[i].add("is_late", occupiedTime > timeOut
                        && timeOut != TimeSetOut ? 1 : 0);

                }
                rows[i].add("rel_type",
                    relTypeToBound(rows[i].get("rel_type", 0)));

                rows[i].add("is_live", checkInLiveLoadOrDropOffKey
                    .getCheckInLiveLoadOrDropOffKey(is_live));
                rows[i].remove("gate_check_in_operate_time");

            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectGateCheckIn", log);
        }
    }

    @Override
    public DBRow[] selectWareHouseCheckIn(long ps_id, PageCtrl pc)
        throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectWareHouseCheckIn(
                ps_id, pc);
            for (int i = 0; i < rows.length; i++) {
                String window_check_in_operate_time = rows[i].get(
                    "window_check_in_operate_time", "");
                String nowTime = DateUtil.NowStr();
                DBRow psTimeSet = floorCheckInMgrZwb.selectPsTimeSet(ps_id);
                int timeOut = psTimeSet != null ? psTimeSet.get("timeout_two",
                    TimeSetOut) : TimeSetOut; // -1表示永远不过期
                if (!StringUtil.isBlank(window_check_in_operate_time)) {
                    Long occupied_time = (DateUtil.getDate2LongTime(nowTime) - DateUtil
                        .getDate2LongTime(window_check_in_operate_time)) / 60000;
                    rows[i].add("occupied_time", occupied_time);
                    rows[i].add("is_late", occupied_time > timeOut
                        && timeOut != TimeSetOut ? 1 : 0);
                }
                rows[i].add("rel_type",
                    relTypeToBound(rows[i].get("rel_type", 0)));
/**
 * else{ String gate_check_in_operate_time=rows[i].get(
 * "gate_check_in_operate_time","");
 * if(!StringUtil.isBlank(window_check_in_operate_time)){
 * rows[i]
 * .add("occupied_time",(DateUtil.getDate2LongTime(nowTime
 * )-DateUtil
 * .getDate2LongTime(gate_check_in_operate_time))/60000); } }
 */
                rows[i].remove("window_check_in_operate_time");

            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectWareHouseCheckIn", log);
        }
    }

    @Override
    public DBRow[] selectWareHouseNumberProcessing(long ps_id, PageCtrl pc)
        throws Exception {
        try {

            DBRow[] rows = this.floorCheckInMgrZwb
                .selectWareHouseNumberProcessing(ps_id, pc);
            DBRow psTimeSet = floorCheckInMgrZwb.selectPsTimeSet(ps_id);
            int timeOut = psTimeSet != null ? psTimeSet.get("timeout_three",
                TimeSetOut) : TimeSetOut; // -1表示永远不过期
            String date = DateUtil.NowStr();
            for (int i = 0; i < rows.length; i++) {
                String load_time = rows[i].get("handle_time", "");
                long dlo_detail_id = rows[i].get("dlo_detail_id", 0l);
                if (!StringUtil.isBlank(load_time)) {
                    Long loadTimeInt = (DateUtil.getDate2LongTime(date) - DateUtil
                        .getDate2LongTime(load_time)) / 60000;
                    rows[i].add("load_time", loadTimeInt);
                    rows[i].add("is_late", loadTimeInt > timeOut
                        && timeOut != TimeSetOut ? 1 : 0);

                }
                int load_count = this.floorCheckInMgrZwb
                    .loadDoneConut(dlo_detail_id);
                int total_count = this.floorCheckInMgrZwb
                    .totalConut(dlo_detail_id);
                if (total_count > 0) {
                    double doneCount = load_count / (total_count * 1.0d);
                    int fixCount = (int) (doneCount * 100);
                    rows[i].add("doneCount", fixCount >= 100 ? 100 : fixCount);
                } else {
                    rows[i].add("doneCount", 0);
                }

                if (rows[i].get("number_type", 0) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS
                    || rows[i].get("number_type", 0) == ModuleKey.CHECK_IN_CTN
                    || rows[i].get("number_type", 0) == ModuleKey.CHECK_IN_BOL) {
                    rows[i].add("EntryType", "Delivery");
                } else {
                    rows[i].add("EntryType", "Pick Up");
                }

                rows[i].remove("dlo_detail_id");
            }
// List<DBRow> sortDBRow = Arrays.asList(rows);
// Collections.sort(sortDBRow, new Comparator<DBRow>() {
// @Override
// public int compare(DBRow o1, DBRow o2) {
// int time1 = o1.get("load_time",0);
// int time2 = o1.get("load_time",0);
// return time1 > time2 ? 1 : -1;
// }
//
// });
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectWareHouseNumberProcessing", log);
        }
    }

    @Override
    public DBRow[] selectSpotSituation(int ps_id, PageCtrl pc) throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectSpotSituation(ps_id,
                pc);
            String nowTime = DateUtil.NowStr();
// CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new
// CheckInLiveLoadOrDropOffKey();
            for (int i = 0; i < rows.length; i++) {
                String gate_check_in_operate_time = rows[i].get(
                    "gate_check_in_operate_time", "");
// int is_live = rows[i].get("isLive",0);
                if (!StringUtil.isBlank(gate_check_in_operate_time)) {
                    rows[i].add(
                        "occupied_time",
                        (DateUtil.getDate2LongTime(nowTime) - DateUtil
                            .getDate2LongTime(gate_check_in_operate_time)) / 60000);

                }
// rows[i].add("is_live",checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(is_live));
                rows[i].remove("gate_check_in_operate_time");
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectSpotSituation", log);
        }
    }

    @Override
    public DBRow[] selectDockSituation(int ps_id, PageCtrl pc) throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectDockSituation(ps_id,
                pc);
            String nowTime = DateUtil.NowStr();
// CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new
// CheckInLiveLoadOrDropOffKey();
            for (int i = 0; i < rows.length; i++) {
                String gate_check_in_operate_time = rows[i].get(
                    "gate_check_in_operate_time", "");
                String window_check_in_operate_time = rows[i].get(
                    "window_check_in_operate_time", "");
                String warehouse_check_in_operate_time = rows[i].get(
                    "warehouse_check_in_operate_time", "");
// int is_live = rows[i].get("isLive",0);
                if (!StringUtil.isBlank(warehouse_check_in_operate_time)) {
                    rows[i].add(
                        "occupied_time",
                        (DateUtil.getDate2LongTime(nowTime) - DateUtil
                            .getDate2LongTime(warehouse_check_in_operate_time)) / 60000);

                } else {
                    if (!StringUtil.isBlank(window_check_in_operate_time)) {
                        rows[i].add(
                            "occupied_time",
                            (DateUtil.getDate2LongTime(nowTime) - DateUtil
                                .getDate2LongTime(window_check_in_operate_time)) / 60000);
                    } else {
                        if (!StringUtil.isBlank(gate_check_in_operate_time)) {
                            rows[i].add(
                                "occupied_time",
                                (DateUtil.getDate2LongTime(nowTime) - DateUtil
                                    .getDate2LongTime(gate_check_in_operate_time)) / 60000);
                        }
                    }
                }
                String ss = null;
                Object obj = rows[i].getValue("num_type");
                if (obj != null) {
                    if (obj instanceof String) {
                        ss = (String) obj;
                    } else {
                        ss = byteArrayToString((byte[]) obj);
                    }
                }

                rows[i].add("num_type", ss);
// rows[i].add("is_live",checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(is_live));
                rows[i].remove("warehouse_check_in_operate_time");
                rows[i].remove("window_check_in_operate_time");
                rows[i].remove("gate_check_in_operate_time");
            }

            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectDockSituation", log);
        }
    }

    @Override
    public DBRow[] selectDockCloseNoCheckOut(int ps_id, PageCtrl pc)
        throws Exception {
        try {
            DBRow[] rows = this.floorCheckInMgrZwb.selectDockCloseNoCheckOut(
                ps_id, pc);

            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "selectDockCloseNoCheckOut", log);
        }
    }

    // 报表
    @Override
    public DBRow[] checkInExcel(String start_time, String end_time, long ps_id)
        throws Exception {
        try {
// String entryType=StringUtil.getString(request,"entryType");
// String start_time=StringUtil.getString(request,"start_time");
// String end_time=StringUtil.getString(request,"end_time");
// String mainStatus=StringUtil.getString(request,"mainStatus");
// String
// tractorStatus=StringUtil.getString(request,"tractorStatus");
// String numberStatus=StringUtil.getString(request,"numberStatus");
// long ps_id=StringUtil.getLong(request,"ps_id");
            start_time = DateUtil.showUTCTime(start_time, ps_id);
            end_time = DateUtil.showUTCTime(end_time, ps_id);
            DBRow[] mainRows = floorCheckInMgrZwb.excelFilterCheckIn(
                start_time, end_time, ps_id);
            DBRow[] returnRows = returnRows = new DBRow[mainRows.length];
            CheckInChildDocumentsStatusTypeKey checkInChildDocumentsStatusTypeKey = new CheckInChildDocumentsStatusTypeKey();
            for (int i = 0; i < mainRows.length; i++) {

                String rel_type = relTypeToBound(mainRows[i].get("rel_type", 1));// 转换为inbound/outbound
                DBRow temp = new DBRow();
                long dlo_id = mainRows[i].get("dlo_id", 0l);
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    temp.add("LOAD", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    temp.add("ORDER", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    temp.add("PO", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    temp.add("OUTBOUND OTHERS", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN) {
                    temp.add("CTNR", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                    temp.add("BOL", mainRows[i].get("number", ""));
                }
                if (mainRows[i].get("number_type", 0l) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    temp.add("INBOUND OTHERS", mainRows[i].get("number", ""));
                }

                temp.add("Entry Id", dlo_id);
                temp.add("Container No",
                    mainRows[i].getString("gate_container_no"));
                temp.add("Driver License",
                    mainRows[i].getString("gate_driver_liscense"));
                temp.add("License Plate",
                    mainRows[i].getString("gate_liscense_plate"));
                temp.add("Driver Name",
                    mainRows[i].getString("gate_driver_name"));
                temp.add("Carrier", mainRows[i].getString("company_name"));
                if (mainRows[i].get("occupancy_status", 0) != 3) {
                    temp.add("Door", mainRows[i].getString("doorId"));
                }

                temp.add("Spot", mainRows[i].getString("yc_no"));
                temp.add("Appointment Time", DateUtil
                    .showLocalparseDateTo24Hours(
                        mainRows[i].getString("appointment_time"),
                        ps_id));

                temp.add("Gate Check In Time", DateUtil
                    .showLocalparseDateTo24Hours(
                        mainRows[i].getString("gate_check_in_time"),
                        ps_id));
                temp.add("Window Check In Time", DateUtil
                    .showLocalparseDateTo24Hours(
                        mainRows[i].getString("window_check_in_time"),
                        ps_id));
                temp.add("Warehouse Check In Time", DateUtil
                    .showLocalparseDateTo24Hours(mainRows[i]
                        .getString("warehouse_check_in_time"), ps_id));
                temp.add(
                    "Check Out Time",
                    DateUtil.showLocalparseDateTo24Hours(
                        mainRows[i].getString("check_out_time"), ps_id));
                temp.add("STitle", mainRows[i].getString("supplier_id"));
                temp.add("CustomerID", mainRows[i].getString("customer_id"));
                temp.add("Load Bar", mainRows[i].getString("load_bar_name"));
                temp.add("Inbound/Outbound", rel_type);
                temp.add("Tractor Status", checkInMainDocumentsStatusTypeKey
                    .getContainerTypeKeyValue(mainRows[i]
                        .getString("tractor_status")));
                temp.add("Trailer Status", checkInMainDocumentsStatusTypeKey
                    .getContainerTypeKeyValue(mainRows[i]
                        .getString("status")));
                temp.add("WHSE", mainRows[i].getString("title"));
                temp.add("On Time Y/N", "Y");
                temp.add("number_status", mainRows[i].get("number_status", 0));
                temp.add("companyId", mainRows[i].getString("company_id"));
                returnRows[i] = temp;

// 判断是否迟到
                String gate_check_in_time = mainRows[i]
                    .getString("create_time");
                String appointment_time = mainRows[i]
                    .getString("appointment_time");
                if (!"".equals(appointment_time)) {
                    long gate_check_in_min = DateUtil
                        .getDate2LongTime(gate_check_in_time);// 毫秒
                    long appointment_min = DateUtil
                        .getDate2LongTime(appointment_time);
// System.out.println("a="+gate_check_in_min+"   b="+appointment_min+"   b-a="+(appointment_min
// - gate_check_in_min));
                    if ((gate_check_in_min - appointment_min) > 600000) {
                        temp.add("On Time Y/N", "N");
                    }
                }

            }
            return returnRows;
        } catch (Exception e) {
            throw new SystemException(e, "selectLoadBar", log);
        }
    }

    public DBRow[] CheckInExcelLogin(HttpServletRequest request)
        throws Exception {
        InputStream inputStream = request.getInputStream(); // 用二进制流接收数据
        String xml = getPost(inputStream);
// xml = xml.replaceAll("\"","\\\"");
        DBRow row = XmlUtil.initDataXmlStringToDBRow(xml, new String[]{
            "Sheet", "Account", "Password"});
        String sheet = row.getString("sheet");
        String account = row.getString("account");
        String password = row.getString("password");

        DBRow login = adminMgrZJ.MachineLogin(account, password);
        DBRow[] result = null;

        if (login != null) {
            AdminLoginBean adminLoggerBean = new AdminLoginBean();
            adminLoggerBean.setAccount(account);
            adminLoggerBean.setAdgid(login.get("adgid", 0l));
            adminLoggerBean.setAdid(login.get("adid", 0l));
            adminLoggerBean.setPs_id(login.get("ps_id", 0l));
            adminLoggerBean.setEmploye_name(login.getString("employe_name"));
            String startTime = "";
            String endTime = "";
            long ps_id = 0;
            if (!sheet.equals("Login")) {
// if(sheet.equals("SCORECARDREPORT-OUTBOUND") ||
// sheet.equals("SCORECARDREPORT-INBOUND") ||
// sheet.equals("OccupiedSpot") || sheet.equals("OccupiedDock"))
// {
// String[] nodeNames = {"psId"};
// DBRow nodeRow = XmlUtil.initDataXmlStringToDBRow(xml,
// nodeNames);
// ps_id = Long.parseLong(nodeRow.getString("psId"));
// }
// else
// {
                String[] nodeNames = {"startTime", "endTime", "psId"};
                DBRow nodeRow = XmlUtil.initDataXmlHasSpaceStringToDBRow(xml,
                    nodeNames);
                startTime = nodeRow.getString("startTime");
                endTime = nodeRow.getString("endTime");
                ps_id = Long.parseLong(nodeRow.getString("psId"));
// }

            }

            switch (sheet) {
                case "Login":
                    result = new DBRow[1];
                    DBRow logintemp = new DBRow();
                    logintemp.add("result", "Login Success");
                    result[0] = logintemp;
                    break;
                case "LoadBar":
                    result = this.getLoadBarReport(startTime, endTime, ps_id);
                    break;

                case "SealInventory":
                    result = this.getSealReport(startTime, endTime, ps_id);
                    break;
                case "AssignTask":
                    result = this.getTask(startTime, endTime, ps_id);
                    break;
                case "PalletInventory":
                    result = checkInMgrZyj.findPlateNoInOutInfo(startTime, endTime,
                        ps_id);
                    break;
                case "OccupiedSpot":
                    result = this.getOccupyYard(ps_id);
                    break;
                case "OccupiedDock":
                    result = this.getOccupyDoor(ps_id);
                    break;
                case "GateRecord":
                    result = this.checkInExcel(startTime, endTime, ps_id);
                    break;
                case "SCORECARDREPORT-OUTBOUND":

// List<DBRow> dataOut = this.getOrderAndPoNo(request,
// login.get("adid",0l), startTime, endTime,ps_id);
                    result = this.scordOutBoundReport(ps_id, startTime, endTime);
                    break;
                case "SCORECARDREPORT-INBOUND":
// List<DBRow> dataIn = this.getReceiptBol(request,
// login.get("adid",0l), startTime, endTime, ps_id);
// ymsMgrAPI.scordInBoundReport(ps_id,startTime,endTime,
// null);//(DBRow[]) dataIn.toArray(new DBRow[0]);
                    result = this.scordInBoundReport(ps_id, startTime, endTime);
                    break;
                case "GATEACTIVITY":
                    result = ymsMgrAPI
                        .gateActivityReport(ps_id, startTime, endTime);
                    break;
                case "TASKACTIVITY":
                    result = ymsMgrAPI
                        .taskActivityReport(ps_id, startTime, endTime);
                    break;
                case "EQUIPMENTREPORT":
                    result = ymsMgrAPI.equipmentReport(ps_id, startTime, endTime);
                    break;
                case "PalletTypeCount":
                    result = this.newFindWMSOrderPalletCount(ps_id, startTime,
                        endTime);
                    break;
                case "SCHEDULE-INBOUND":
                    result = ymsMgrAPI.scheduleInBound(ps_id, startTime, endTime,
                        null);
                    break;
                case "SCHEDULE-OUTBOUND":
                    result = ymsMgrAPI.scheduleOutBound(ps_id, startTime, endTime,
                        null);
                    break;
                case "RECEIVE-SCHEDULE-INBOUND":
                    result = ymsMgrAPI.receiveScheduleInbound(ps_id, startTime,
                        endTime);
                    break;
                case "CTNRINYARD":
                    result = this.findCTNRInYard(ps_id);
                    break;
            }
        } else {

            result = new DBRow[1];
            DBRow temp = new DBRow();
            temp.add("result", "User name or password is wrong");
            result[0] = temp;

        }

        return result;
    }

    /**
     * 看load是否是exception并且装过货
     *
     * @param ps_id
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    private DBRow[] scordOutBoundReport(long ps_id, String startTime,
                                        String endTime) throws Exception {
        DBRow[] orders = ymsMgrAPI.scordOutBoundReport(ps_id, startTime,
            endTime);
        CheckInChildDocumentsStatusTypeKey checkInChildDocumentsStatusTypeKey = new CheckInChildDocumentsStatusTypeKey();
        for (DBRow order : orders) {
            String load = order.get("load_no", "");
// 看能否查出来这个load 状态exception并且有扫托盘的
            if (order.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION) {
                DBRow[] loads = floorCheckInMgrWfh.findLoadOnLoadingByLoadNo(
                    load, CheckInChildDocumentsStatusTypeKey.EXCEPTION);
                if (loads.length > 0) {
                    order.add("exception", 0);
                } else {
                    order.add("exception", 1); // 代表missed
                }
            } else {
                order.add("exception", 0);
            }
            order.add("number_status", checkInChildDocumentsStatusTypeKey
                .getContainerTypeKeyValue(order.get("number_status", 0)));
        }
        return orders;
    }

    /**
     * 看rn是否是exception并且收过
     *
     * @param ps_id
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    private DBRow[] scordInBoundReport(long ps_id, String startTime,
                                       String endTime) throws Exception {
        DBRow[] RNS = ymsMgrAPI.scordInBoundReport(ps_id, startTime, endTime);
        CheckInChildDocumentsStatusTypeKey checkInChildDocumentsStatusTypeKey = new CheckInChildDocumentsStatusTypeKey();

        for (DBRow rn : RNS) {
            String rn_no = rn.get("receipt_no", "");
            if (rn.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION) {
                DBRow[] rns = floorCheckInMgrWfh.findReceivedRNByRn(rn_no,
                    CheckInChildDocumentsStatusTypeKey.EXCEPTION);
                if (rns.length > 0) {
                    rn.add("exception", 0);
                } else {
                    rn.add("exception", 1); // 代表missed
                }
            } else {
                rn.add("exception", 0);
            }
            rn.add("number_status", checkInChildDocumentsStatusTypeKey
                .getContainerTypeKeyValue(rn.get("number_status", 0)));

        }
        return RNS;
    }

    /**
     * 查询yard内的货柜
     *
     * @param ps_id
     * @return
     * @throws Exception
     */
    public DBRow[] findCTNRInYard(long ps_id) throws Exception {
        try {
            CheckInMainDocumentsRelTypeKey checkInMainDocumentsRelTypeKey = new CheckInMainDocumentsRelTypeKey();
            DBRow[] result = floorCheckInMgrWfh.findYardCtnr(ps_id);
            for (DBRow ctnr : result) {
                int full_empty = 1; // 1:empty 2:full 3:Partial
                int rel_type = ctnr.get("equipment_type", 0);
                int status = ctnr.get("equipment_status", 0);
// 如果是送货并且未离开则还是满柜 1:empty

                if ((rel_type == CheckInMainDocumentsRelTypeKey.DELIVERY || rel_type == CheckInMainDocumentsRelTypeKey.BOTH)
                    && status == CheckInMainDocumentsStatusTypeKey.UNPROCESS) {

                    full_empty = 2;
                } else if (rel_type == CheckInMainDocumentsRelTypeKey.DELIVERY
                    && (status == CheckInMainDocumentsStatusTypeKey.LEAVING || status == CheckInMainDocumentsStatusTypeKey.INYARD)) {
                    full_empty = 1;
                }
// 如果是装货 并且未离开则还是空柜 2：full
                else if ((rel_type == CheckInMainDocumentsRelTypeKey.PICK_UP || rel_type == CheckInMainDocumentsRelTypeKey.SMALL_PARCEL)
                    && status == CheckInMainDocumentsStatusTypeKey.UNPROCESS) {
                    full_empty = 1;
                } else if ((rel_type == CheckInMainDocumentsRelTypeKey.PICK_UP
                    || rel_type == CheckInMainDocumentsRelTypeKey.BOTH || rel_type == CheckInMainDocumentsRelTypeKey.SMALL_PARCEL)
                    && (status == CheckInMainDocumentsStatusTypeKey.LEAVING || status == CheckInMainDocumentsStatusTypeKey.INYARD)) {
                    full_empty = 2;
                } else if (rel_type == CheckInMainDocumentsRelTypeKey.NONE
                    || rel_type == CheckInMainDocumentsRelTypeKey.VISITOR
                    || rel_type == CheckInMainDocumentsRelTypeKey.PATROL) {
                    full_empty = 0;
                } else if (status == CheckInMainDocumentsStatusTypeKey.PROCESSING) {
                    full_empty = 3;
                }
                if (full_empty == 3) {
                    ctnr.add("full_empty", "Partial");
                } else if (full_empty == 2) {
                    ctnr.add("full_empty", "Full");
                } else if (full_empty == 1) {
                    ctnr.add("full_empty", "Empty");
                } else if (full_empty == 0) {
                    ctnr.add("full_empty", "??");
                }
                String resources_name = ctnr.get("resources_name", "");
                String resource = occupyTypeKey.getOccupyTypeKeyName(ctnr.get(
                    "resources_type", 0));
                if (!resources_name.equals("")) {
                    resource += ": " + resources_name;
                }
                ctnr.add("resources_type_name", resource);
                ctnr.add("equipment_type_name", checkInMainDocumentsRelTypeKey
                    .getCheckInMainDocumentsRelTypeKeyValue(ctnr.get(
                        "equipment_type", 0)));
                ctnr.add("equipment_status_name",
                    checkInMainDocumentsStatusTypeKey
                        .getContainerTypeKeyValue(ctnr.get(
                            "equipment_status", 0)));
                ctnr.add(
                    "gate_check_in_time",
                    DateUtil.showLocalparseDateTo24Hours(
                        ctnr.get("gate_check_in_time", ""), ps_id));
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "findCTNRInYard", log);
        }
    }

    /**
     * 查找每个order对应的palletCount 新版本的系统使用这个方法查找palletTypeCount
     *
     * @param ps_id
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] newFindWMSOrderPalletCount(long ps_id, String startTime,
                                              String endTime) throws Exception {
        try {
            String wmsStartTime = startTime;
            String wmsEndTime = endTime;
            boolean flag = true; // 新系统打开 旧系统关闭(旧系统不需要转换时间
            if (flag) {
                if (!StringUtil.isBlank(startTime)) {
                    startTime = DateUtil.showUTCTime(startTime, ps_id);
                }
                if (!StringUtil.isBlank(endTime)) {
                    endTime = DateUtil.showUTCTime(endTime, ps_id);
                }
            }
// DBRow[] orders =
// floorCheckInMgrWfh.findWMSOrderPalletCountOld(ps_id, startTime,
// endTime);
            DBRow[] orders = floorCheckInMgrWfh.findWMSOrderPalletCount(ps_id,
                startTime, endTime);
            if (orders == null)
                return null;
            DBRow[] companyIds = floorCheckInMgrWfh.findCompanyIdByPsId(ps_id);
            StringBuffer ss = new StringBuffer();
            for (DBRow companyId : companyIds) {
                ss.append("'").append(companyId.get("company_id", ""))
                    .append("',");
            }
            String companyid = "";
            if (companyIds.length > 0)
                companyid = ss.toString().substring(0, ss.length() - 1);

            DBRow para = new DBRow();
            para.add("startTime", wmsStartTime);
            para.add("endTime", wmsEndTime);
            para.add("company_id", companyid);
// long time = System.currentTimeMillis();
            DBRow[] wmsData = sqlServerMgrZJ
                .findShipToInfoByNumberAndType(para);
// System.out.println("use "+(System.currentTimeMillis()-time)/1000+"s"
// +"----- "+wmsData.length +"条");
            List<DBRow> list = new ArrayList<DBRow>();

            for (DBRow order : wmsData) {
                list.add(order);
            }

            for (DBRow order : orders) {
                order.add("userUpdated", order.get("employe_name", ""));
                if (order.get("number_type", 0) != ModuleKey.CHECK_IN_LOAD) {
                    order.add("number", "");
                }
                if (flag) {
                    String finishTime = order.get("finish_time", "");
                    if (!StringUtil.isBlank(finishTime)) {
                        finishTime = DateUtil.showLocalparseDateTo24Hours(
                            finishTime, ps_id);
                    }
                    order.add("finish_time", finishTime);
                }
            }
// time = System.currentTimeMillis();
            this.PalletCompletaData(orders, wmsData, 0, orders.length, flag);
// System.out.println("each "+(System.currentTimeMillis()-time)/1000+"s");
// System.out.println(new JsonObject(orders).toString());
            return orders;
        } catch (Exception e) {
            throw new SystemException(e, "newFindWMSOrderPalletCount", log);
        }
    }

    // 将wms数据反填到data中
    private void PalletCompletaData(DBRow[] row, DBRow[] wmsData, int index,
                                    int count, boolean flag) throws Exception {
        try {
            Map<String, DBRow> data = new HashMap<String, DBRow>();

            data = this.OrderArrayToMap(wmsData); // 将wms数据转成map提供快速的查找
            String defaultOrder = "";
            for (int i = index; i < count; i++) {
                long orderNumber = row[i].get("order_numbers", 0L);
                String company_id = row[i].get("company_id", "");
// String customer_id = row[i].get("customer_id", "");
                StringBuffer key = new StringBuffer();
                key.append("'" + orderNumber + "'+").append(
                    "'" + company_id + "'"); // 拼出key值 将格式拼成这样便于直接在sql中当做条件。

                DBRow wmsDateTemp = data.get(key.toString()); // 取出对应的反填值
                if (wmsDateTemp == null) { // 如果在这个时间段内 在wms系统中没有查出对应的order就记录下来
// ，用于以后直接查询使用
                    defaultOrder += key.toString() + ",";
                    wmsDateTemp = new DBRow();
                }
                if (!flag) { // 新系统 （true 不需要进行反填
                    row[i].add("ship_to", wmsDateTemp.get("ShipToName", ""));
                    row[i].add("address", wmsDateTemp.get("ShipToAddress1", "")
                        + " " + wmsDateTemp.get("ShipToAddress2", ""));
                    row[i].add("city", wmsDateTemp.get("ShipToCity", ""));
                    row[i].add("state", wmsDateTemp.get("ShipToState", ""));
                    row[i].add("postal_code",
                        wmsDateTemp.get("ShipToZipCode", ""));
                    row[i].add("country", wmsDateTemp.get("ShipToCountry", ""));
                    row[i].add("supplier_id", wmsDateTemp.get("SupplierID", ""));
                    row[i].add("account_id", wmsDateTemp.get("AccountID", ""));
                }
                row[i].add("ItemId", wmsDateTemp.get("ItemId", ""));
// 反填 end
            }
            if (!StringUtil.isBlank(defaultOrder)) { // 如果上面记录的order条件不为空
// 则进行一次查询补全
                defaultOrder = defaultOrder.substring(0,
                    defaultOrder.length() - 1);
                DBRow[] deOrder = floorSQLServerMgrZJ
                    .findOrderLinePlatesByOrderNo(defaultOrder, null);
                data = this.OrderArrayToMap(deOrder); // 将后来查找的数据转成map 供快速查找
                for (DBRow orderRow : row) {
                    long orderNumber = orderRow.get("order_numbers", 0L);
                    String company_id = orderRow.get("company_id", "");
// String customer_id = orderRow.get("customer_id", "");
                    StringBuffer key = new StringBuffer();
                    key.append("'" + orderNumber + "'+").append(
                        "'" + company_id + "'"); // 拼出key值

                    DBRow wmsDateTemp = data.get(key.toString()); // 取出对应的反填值
                    if (wmsDateTemp == null) {
                        continue;
                    }
                    if (!flag) {
                        orderRow.add("ship_to",
                            wmsDateTemp.get("ShipToName", ""));
                        orderRow.add("address",
                            wmsDateTemp.get("ShipToAddress1", "") + " "
                                + wmsDateTemp.get("ShipToAddress2", ""));
                        orderRow.add("city", wmsDateTemp.get("ShipToCity", ""));
                        orderRow.add("state",
                            wmsDateTemp.get("ShipToState", ""));
                        orderRow.add("postal_code",
                            wmsDateTemp.get("ShipToZipCode", ""));
                        orderRow.add("country",
                            wmsDateTemp.get("ShipToCountry", ""));
                        orderRow.add("supplier_id",
                            wmsDateTemp.get("SupplierID", ""));
                        orderRow.add("account_id",
                            wmsDateTemp.get("AccountID", ""));
                    }
                    orderRow.add("ItemId", wmsDateTemp.get("ItemId", ""));
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "findWMSOrderPalletCount", log);
        }
    }

    // 将wms查出来的order数组转为map 并建立key 值可以快速查找
    private Map<String, DBRow> OrderArrayToMap(DBRow[] wmsData) {
        Map<String, DBRow> data = new HashMap<String, DBRow>();
        for (DBRow wmsOrder : wmsData) {
            String wms_company_id = wmsOrder.get("CompanyID", "");
// String wms_customer_id = wmsOrder.get("CustomerID", "");
            long wmsOrderId = wmsOrder.get("OrderNo", 0L);
            StringBuffer key = new StringBuffer();
            key.append("'" + wmsOrderId + "'+").append(
                "'" + wms_company_id + "'"); // 存入map key值
            DBRow order = data.get(key.toString()); // 将重复的itemId过滤出来 拼成字符串
            if (order != null) {
                String item = order.get("ItemId", "");
                String item_order = wmsOrder.get("ItemId", "");
                if (!item_order.contains(item)) {
                    wmsOrder.add("ItemId", wmsOrder.get("ItemId", "") + ","
                        + item);
                }
            }
            data.put(key.toString(), wmsOrder);
        }
        return data;
    }

    private String getPost(InputStream inputStream) throws Exception {
        BufferedInputStream input = null; // 输入流,用于接收请求的数据
        byte[] buffer = new byte[1024]; // 数据缓冲区
        int count = 0; // 每个缓冲区的实际数据长度
        ByteArrayOutputStream streamXML = new ByteArrayOutputStream(); // 请求数据存放对象
        byte[] iXMLData = null;
        try {
            input = new BufferedInputStream(inputStream);
            while ((count = input.read(buffer)) != -1) {
                streamXML.write(buffer, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception f) {
                    f.printStackTrace();
                }
            }
        }
        iXMLData = streamXML.toByteArray();

        return (new String(iXMLData));
    }

    /**
     * 查找每个order对应的palletCount 不在使用？
     *
     * @param ps_id
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] findWMSOrderPalletCount(long ps_id, String startTime,
                                           String endTime) throws Exception {
        try {
/*
* // if(!StringUtil.isBlank(startTime)){ // startTime =
* DateUtil.showUTCTime(startTime, ps_id); // } //
* if(!StringUtil.isBlank(endTime)){ // endTime =
* DateUtil.showUTCTime(endTime, ps_id); // } // DBRow[] orders =
* floorCheckInMgrWfh.findWMSOrderPalletCount(ps_id, startTime,
* endTime); //新系统 DBRow[] orders =
* floorCheckInMgrWfh.findWMSOrderPalletCountOld(ps_id, startTime,
* endTime); //旧系统 if(orders!=null&&orders.length>0){ int total =
* 300; //一次查询的个数 int index = 0; index = (int)
* Math.ceil((double)orders.length/total); //分几次查询 wms StringBuffer
* indexs = new StringBuffer(); for(int j=0;j<index;j++){ int
* orderLength = orders.length-(j)*total; int length = orderLength
* >=total ? (j+1)*total : orders.length; //得到每次需要遍历剩余的次数 for(int i
* = j*total;i<length;i++){ //用来拼 参数 String finishTime =
* orders[i].get("finish_time",""); //
* if(!StringUtil.isBlank(finishTime)){ // finishTime =
* DateUtil.showLocalparseDateTo24Hours(finishTime, ps_id); // } //
* orders[i].add("finish_time", finishTime);
* orders[i].add("userUpdated", orders[i].get("employe_name",""));
*
* // 'orderNumber'+'companyId'+'customerId',
* 'orderNumber'+'companyId'+'customerId' 联合主键查询使用
* indexs.append("'") .append(orders[i].get("order_numbers", ""))
* .append("'+'") .append(orders[i].get("company_id", ""))
* .append("'+'") .append(orders[i].get("customer_id",""))
* .append("',"); } String para =
* indexs.toString().substring(0,indexs.toString().length()-1);
* DBRow[] wmsData = null; // wmsData = new DBRow[1]; wmsData[0] =
* new DBRow(); long timeTemp = System.currentTimeMillis(); //
* wmsData = sqlServerMgrZJ.findShipToInfoByNumberAndType(para);
* System
* .out.println((j+1)+"次"+(System.currentTimeMillis()-timeTemp)
* /(1000
* )+"s"+"--查出("+wmsData.length+"条"+"--in("+(length-j*total)+"条");
* // PalletCompletaData(orders,wmsData,j*total,length); //反填数据 } }
*
* // System.out.println(new JsonObject(orders).toString()); return
* orders;
*/
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "findWMSOrderPalletCount", log);
        }
    }

    /**
     * 查询自有仓库
     *
     * @return
     * @throws Exception
     */
    public DBRow[] findSelfStorage() throws Exception {
        try {
            DBRow[] storages = this.floorCheckInMgrZwb.findSelfStorage();
            for (int i = 0; i < storages.length; i++) {
                long ps_id = storages[i].get("ps_id", 0l);
                storages[i].add("jet_lag", DateUtil.getStorageRawOffset(ps_id));
            }
            return storages;
        } catch (Exception e) {
            throw new SystemException(e, "findSelfStorage", log);
        }
    }

    @Override
    public DBRow[] selectSpotSituation(long ps_id, PageCtrl pc)
        throws Exception {
        try {
            DBRow[] result = this.floorCheckInMgrZwb.selectSpotSituation(ps_id,
                pc);
            String date = DateUtil.NowStr();

            for (int i = 0; i < result.length; i++) {
                result[i].add("gate_check_in_operate_time", (DateUtil
                    .getDate2LongTime(date) - DateUtil
                    .getDate2LongTime(result[i].get(
                        "gate_check_in_operate_time", ""))) / 60000);

            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "selectSpotSituation", log);
        }
    }

    @Override
    public DBRow[] selectDockSituation(long ps_id, PageCtrl pc)
        throws Exception {
        try {
            DBRow[] result = this.floorCheckInMgrZwb.selectDockSituation(ps_id,
                pc);
            String date = DateUtil.NowStr();

            for (int i = 0; i < result.length; i++) {
                result[i]
                    .add("warehouse_check_in_operate_time",
                        (DateUtil.getDate2LongTime(date) - DateUtil.getDate2LongTime(result[i]
                            .get("warehouse_check_in_operate_time",
                                ""))) / 60000);

            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "selectDockSituation", log);
        }
    }

    public void loadSubmitPhoto(DBRow data, DBRow loginRow) throws Exception {
        try {
            long dlo_detail_id = data.get("dlo_detail_id", 0l);
            DBRow detailRow = floorCheckInMgrZwb
                .selectDetailByDetailId(dlo_detail_id);
            long adid = loginRow.get("adid", 0l);
            long entry_id = detailRow.get("dlo_id", 0l);
// 添加日志
            addCheckInLog(adid, "Update Entry Status", entry_id,
                CheckInLogTypeKey.ANDROID_LOAD, DateUtil.NowStr(), "");
// androidSaveMoveFile(data.getString("filePath"),
// loginRow.get("adid", 0l),entry_id, "Load_");
            androidSaveMoveFile(data.getString("filePath"), entry_id, "Load_",
                data.getString("session_id"));
        } catch (Exception e) {
            throw new SystemException(e, "loadSubmitPhoto", log);
        }
    }

    /**
     */
    @Override
    public DBRow selectDetailByDetailId(long dlo_detail_id) throws Exception {
        try {
            return floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectDetailByDetailId", log);
        }
    }

    /**
     * DBRow[]{seal_id}
     *
     * @return
     * @throws Exception
     */
    private DBRow handleXml(InputStream inputStream) throws Exception {
        DBRow returnRow = new DBRow();
        DocumentBuilderFactory docBuilderFactory = null;
        DocumentBuilder docBuilder = null;
        Document doc = null;
        try {
            docBuilderFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(inputStream);
            Element root = doc.getDocumentElement();
// NodeList nodeList = root.getElementsByTagName("Seal");
            NodeList nodeListid = root.getElementsByTagName("SealId");
// account
            NodeList accountNode = doc.getElementsByTagName("Account");
            if (accountNode != null && accountNode.getLength() > 0) {
                returnRow.add("account", accountNode.item(0).getTextContent());
            }
// password
            NodeList passwordNode = doc.getElementsByTagName("Password");
            if (passwordNode != null && passwordNode.getLength() > 0) {
                returnRow
                    .add("password", passwordNode.item(0).getTextContent());
            }

// sheet
            NodeList sheetNode = doc.getElementsByTagName("Sheet");
            if (sheetNode != null && sheetNode.getLength() > 0) {
                returnRow.add("sheet", sheetNode.item(0).getTextContent());
            }
// flag
            NodeList flagNode = doc.getElementsByTagName("Flag");
            if (flagNode != null && flagNode.getLength() > 0) {
                returnRow.add("flag", flagNode.item(0).getTextContent());
            }
// startTime
            NodeList ps_id = doc.getElementsByTagName("WHSEID");
            if (ps_id != null && ps_id.getLength() > 0) {
                returnRow.add("start_time", ps_id.item(0).getTextContent());
            }
/**
 * //endTime NodeList endTimeNode =
 * doc.getElementsByTagName("EndTime"); if(endTimeNode != null &&
 * endTimeNode.getLength() > 0){ returnRow.add("end_time",
 * endTimeNode.item(0).getTextContent()); } if(nodeList != null &&
 * nodeList.getLength() > 0 ){ DBRow[] seals = new
 * DBRow[nodeList.getLength()]; for(int index = 0 , count =
 * nodeList.getLength() ; index < count ; index++ ){ Element temp
 * =(Element)nodeList.item(index); if(temp != null){ DBRow tempRow =
 * new DBRow(); tempRow.add("seal", temp.getTextContent());
 * seals[index] = tempRow ; } } returnRow.add("seals", seals); }
 */
            if (nodeListid != null && nodeListid.getLength() > 0) {
                DBRow[] sealIds = new DBRow[nodeListid.getLength()];
                for (int index = 0, count = nodeListid.getLength(); index < count; index++) {
                    Element temp = (Element) nodeListid.item(index);
                    if (temp != null) {
                        DBRow tempRow = new DBRow();
                        tempRow.add("sealId", temp.getTextContent());
                        sealIds[index] = tempRow;
                    }
                }
                returnRow.add("sealIds", sealIds);
            }
            return returnRow;
        } catch (Exception e) {
            throw new RuntimeException();
        } finally {
            doc = null;
            docBuilder = null;
            docBuilderFactory = null;
        }

    }

    @Override
    public String checkInImportSeals(HttpServletRequest request)
        throws Exception {
        InputStream inputStream = request.getInputStream(); // 用二进制流接收数据
        String xml = getPost(inputStream);
        DBRow row = XmlUtil.initDataXmlStringToDBRow(xml, new String[]{
            "Sheet", "Account", "Password"});
        String sheet = row.getString("sheet");
        String account = row.getString("account");
        String password = row.getString("password");

        DBRow login = adminMgrZJ.MachineLogin(account, password);

        String result = null;

        if (login != null) {
            AdminLoginBean adminLoggerBean = new AdminLoginBean();
            adminLoggerBean.setAccount(account);
            adminLoggerBean.setAdgid(login.get("adgid", 0l));
            adminLoggerBean.setAdid(login.get("adid", 0l));
            adminLoggerBean.setPs_id(login.get("ps_id", 0l));
            adminLoggerBean.setEmploye_name(login.getString("employe_name"));

            switch (sheet) {
                case "Login":
                    result = "Login Success";
                    break;
                case "IMPORTSEAL":
                    result = handleSealsInfo(xml, adminLoggerBean);
                    break;

            }
        } else {
            result = "User name or password is wrong";

        }

        return result;
    }

    @Override
    public DBRow[] checkInCheckSeals(HttpServletRequest request)
        throws Exception {
        InputStream inputStream = request.getInputStream(); // 用二进制流接收数据
        DBRow row = this.handleXml(inputStream);
        String sheet = row.getString("sheet");
        String account = row.getString("account");
        String password = row.getString("password");
        long ps_id = row.get("ps_id", 0l);
        String flag = row.getString("flag", "");
        String success = "";

        DBRow login = adminMgrZJ.MachineLogin(account, password);

        DBRow[] result = null;

        if (login != null) {
            AdminLoginBean adminLoggerBean = new AdminLoginBean();
            adminLoggerBean.setAccount(account);
            adminLoggerBean.setAdgid(login.get("adgid", 0l));
            adminLoggerBean.setAdid(login.get("adid", 0l));
            adminLoggerBean.setPs_id(login.get("ps_id", 0l));
            adminLoggerBean.setEmploye_name(login.getString("employe_name"));

            switch (sheet) {
                case "Login":
                    result = new DBRow[1];
                    DBRow logintemp = new DBRow();
                    logintemp.add("result", "Login Success");
                    result[0] = logintemp;
                    break;

                case "CHECK SEAL":
                    if (flag.equals("search")) {
                        result = selectSealsByTime(ps_id);
                    }
                    if (flag.equals("update")) {
                        DBRow[] sealIds = (DBRow[]) row.get("sealIds",
                            new DBRow[]{});
                        String nowDate = DateUtil.NowStr();
                        DBRow checkRow = new DBRow();
                        checkRow.add("return_time", nowDate);
                        checkRow.add("seal_status", 1);
                        checkRow.add("return_user", login.get("adid", 0l));
                        success = unusedSealsReturn(sealIds, checkRow);
                        if (success == "Success") {
                            result = selectSealsByTime(ps_id);
                        }
                    }
                    if (flag.equals("assign")) {
                        DBRow[] sealIds = (DBRow[]) row.get("sealIds",
                            new DBRow[]{});
                        String nowDate = DateUtil.NowStr();
                        DBRow assignRow = new DBRow();
                        assignRow.add("assign_time", nowDate);
                        assignRow.add("return_time", null);
                        assignRow.add("seal_status", 2);
                        assignRow.add("assign_user", login.get("adid", 0l));
                        success = unusedSealsReturn(sealIds, assignRow);
                        if (success == "Success") {
                            result = selectSealsByTime(ps_id);
                        }
                    }
                    break;

            }
        } else {
            result = new DBRow[1];
            DBRow temp = new DBRow();
            temp.add("result", "User name or password is wrong");
            result[0] = temp;
        }

        return result;
    }

    /**
     * 导入seal信息
     *
     * @return
     * @throws Exception
     */
    public String handleSealsInfo(String xml, AdminLoginBean adminLoggerBean)
        throws Exception {
        String result;
        try {
            String[] nodeNames = {"Seal", "WHSEID"};
            DBRow row = XmlUtil
                .initDataXmlHasSpaceStringToDBRow(xml, nodeNames);
            String nowDate = DateUtil.NowStr();

            DBRow sealRow = new DBRow();
            String seal_no = row.getString("seal");
            long ps_id = row.get("WHSEID", 0l);
            DBRow[] reRow = floorCheckInMgrZwb.selectBySealNo(seal_no, ps_id);
            if (reRow.length > 0) {
                result = "Seal No is repeat";
            } else {
                sealRow.add("seal_no", seal_no);
                sealRow.add("import_user", adminLoggerBean.getAdid());
                sealRow.add("ps_id", ps_id);
                sealRow.add("import_time", nowDate);
                sealRow.add("seal_status", 1);
                floorCheckInMgrZwb.addImportSeals(sealRow);

                result = "Success";
            }

        } catch (Exception e) {
            result = "System Error";
        }

        return result;
    }

    /**
     * 未使用的seal退回
     *
     * @return
     * @throws Exception
     */
    public String unusedSealsReturn(DBRow[] sealIds, DBRow row)
        throws Exception {
        String result;
        try {

// floorCheckInMgrZwb.updateImportSeals();

            for (int i = 0; i < sealIds.length; i++) {
                floorCheckInMgrZwb.unusedSealsReturn(
                    sealIds[i].get("sealId", 0l), row);
            }
            result = "Success";

        } catch (Exception e) {
            result = "System Error";
        }

        return result;
    }

    /**
     * 未使用的seal退回
     *
     * @return
     * @throws Exception
     */
    public DBRow[] selectSealsByTime(long ps_id) throws Exception {
        DBRow[] result = null;
        try {
            String time = DateUtil.FormatDatetime("yyyy-MM-dd");
            result = floorCheckInMgrZwb.selectSealsByTime(time, time, ps_id);
            for (int i = 0; i < result.length; i++) {
                if (result[i].get("seal_status", 0) == 1) {
                    result[i].add("status", "Available");
                } else if (result[i].get("seal_status", 0) == 2) {
                    result[i].add("status", "Assigned");
                } else if (result[i].get("seal_status", 0) == 3) {
                    result[i].add("status", "Used");
                }

            }

        } catch (Exception e) {

        }

        return result;
    }

    public String byteArrayToString(byte[] a) {
        StringBuffer sb = new StringBuffer();
        for (int i : a) {
            sb.append((char) i);
        }
        return sb.toString();
    }

    /**
     * 根据checkout时间查询entrys 报表用 load seal
     */
    @Override
    public DBRow[] getLoadBarReport(String startTime, String endTime, long ps_id)
        throws Exception {
        try {
            startTime = DateUtil.showUTCTime(startTime, ps_id);
            endTime = DateUtil.showUTCTime(endTime, ps_id);

            DBRow[] mains = floorCheckInMgrZwb.getLoadBarReport(startTime,
                endTime, ps_id);
            DBRow[] loadCount = floorCheckInMgrZwb.getLoadBarCountreport(
                startTime, endTime, ps_id);// 每种loadBar的数量
            List<DBRow> list = new ArrayList<DBRow>();
            int i = 0;
            ModuleKey moduleKey = new ModuleKey();
            for (DBRow main : mains) {
                DBRow row = new DBRow();
                DBRow[] result = null;
                // if(main.get("dlo_id", 0L)==120332){
                // System.out.println(1);
                // }
                if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    // row.add("LoadNumber",main.get("number",""));
                    result = sqlServerMgrZJ.findOrderInfosByLoadNo(
                        main.get("number", ""), main.get("company_id", ""),
                        main.get("customer_id", ""), null);

                    for (int j = 0; j < result.length; j++) {
                        result[j].add("number_type", moduleKey
                            .getModuleName(main.get("number_type", 0)));
                        result[j].add("number", main.get("number", ""));
                        result[j].add("OrderNumber",
                            result[j].get("OrderNo", ""));
                        result[j].add("ReferenceNo",
                            result[j].get("ReferenceNo", ""));
                        result[j]
                            .add("load_bar", main.get("load_bar_name", ""));
                        result[j].add("dlo_id", main.get("dlo_id", 0L));
                        result[j]
                            .add("checkOutTime",
                                DateUtil.showLocalparseDateTo24Hours(
                                    main.get(
                                        "check_out_warehouse_time",
                                        ""), ps_id));
                        result[j].add("carrier", main.get("company_name", ""));
                        result[j].add("custom_id", main.get("customer_id", ""));
                        result[j].add("company_id", main.get("company_id", ""));
                        result[j].add("freight_term",
                            main.get("freight_term", ""));
                        result[j].add("equipment_number",
                            main.get("equipment_number", ""));
                        result[j].add("dlo_detail_id",
                            main.get("dlo_detail_id", ""));
                        result[j].add("load_bar_count",
                            main.get("load_bar_count", ""));
                        list.add(result[j]);
                    }
                    if (result.length == 0) {
                        row.add("number_type", moduleKey.getModuleName(main
                            .get("number_type", 0)));
                        row.add("number", main.get("number", ""));
                        row.add("OrderNumber", "");
                        row.add("ReferenceNo", "");
                        row.add("load_bar", main.get("load_bar_name", ""));
                        row.add("dlo_id", main.get("dlo_id", 0L));
                        row.add("checkOutTime", DateUtil
                            .showLocalparseDateTo24Hours(main.get(
                                "check_out_warehouse_time", ""), ps_id));
                        row.add("carrier", main.get("company_name", ""));
                        row.add("custom_id", main.get("customer_id", ""));
                        row.add("company_id", main.get("company_id", ""));
                        row.add("freight_term", main.get("freight_term", ""));
                        row.add("equipment_number",
                            main.get("equipment_number", ""));
                        row.add("dlo_detail_id", main.get("dlo_detail_id", ""));
                        row.add("load_bar_count",
                            main.get("load_bar_count", ""));
                        list.add(row);
                    }

                } else if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    row.add("number_type",
                        moduleKey.getModuleName(main.get("number_type", 0)));
                    row.add("number", main.get("number", ""));
                    DBRow order = sqlServerMgrZJ.findOrderInfoByOrderNo(
                        main.get("number", 0l), main.get("company_id", ""),
                        main.get("customer_id", ""), null);
                    row.add("LoadNumber", main.get("number", ""));
                    row.add("OrderNumber", order.get("OrderNo", ""));
                    row.add("ReferenceNo", order.get("ReferenceNo", ""));
                    row.add("load_bar", main.get("load_bar_name", ""));
                    row.add("dlo_id", main.get("dlo_id", 0L));
                    row.add("checkOutTime", DateUtil
                        .showLocalparseDateTo24Hours(
                            main.get("check_out_warehouse_time", ""),
                            ps_id));
                    row.add("carrier", main.get("company_name", ""));
                    row.add("custom_id", main.get("customer_id", ""));
                    row.add("company_id", main.get("company_id", ""));
                    row.add("freight_term", main.get("freight_term", ""));
                    row.add("equipment_number",
                        main.get("equipment_number", ""));
                    row.add("dlo_detail_id", main.get("dlo_detail_id", ""));
                    row.add("load_bar_count", main.get("load_bar_count", ""));
                    list.add(row);
                } else if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    result = sqlServerMgrZJ.findOrderInfoByPoNo(
                        main.get("number", ""), main.get("company_id", ""),
                        main.get("customer_id", ""), null);

                    for (int j = 0; j < result.length; j++) {
                        result[j].add("number_type", moduleKey
                            .getModuleName(main.get("number_type", 0)));
                        result[j].add("number", main.get("number", ""));
                        result[j].add("PoNumber", main.get("number", ""));
                        result[j].add("OrderNumber",
                            result[j].get("OrderNo", ""));
                        result[j].add("ReferenceNo",
                            result[j].get("ReferenceNo", ""));
                        result[j]
                            .add("load_bar", main.get("load_bar_name", ""));
                        result[j].add("dlo_id", main.get("dlo_id", 0L));
                        result[j]
                            .add("checkOutTime",
                                DateUtil.showLocalparseDateTo24Hours(
                                    main.get(
                                        "check_out_warehouse_time",
                                        ""), ps_id));
                        result[j].add("carrier", main.get("company_name", ""));
                        result[j].add("custom_id", main.get("customer_id", ""));
                        result[j].add("company_id", main.get("company_id", ""));
                        result[j].add("freight_term",
                            main.get("freight_term", ""));
                        result[j].add("equipment_number",
                            main.get("equipment_number", ""));
                        result[j].add("dlo_detail_id",
                            main.get("dlo_detail_id", ""));
                        result[j].add("load_bar_count",
                            main.get("load_bar_count", ""));
                        list.add(result[j]);
                    }
                    if (result.length == 0) {
                        row.add("number_type", moduleKey.getModuleName(main
                            .get("number_type", 0)));
                        row.add("number", main.get("number", ""));
                        row.add("PoNumber", main.get("number", ""));
                        row.add("OrderNumber", "");
                        row.add("ReferenceNo", "");
                        row.add("load_bar", main.get("load_bar_name", ""));
                        row.add("dlo_id", main.get("dlo_id", 0L));
                        row.add("checkOutTime", DateUtil
                            .showLocalparseDateTo24Hours(main.get(
                                "check_out_warehouse_time", ""), ps_id));
                        row.add("carrier", main.get("company_name", ""));
                        row.add("custom_id", main.get("customer_id", ""));
                        row.add("company_id", main.get("company_id", ""));
                        row.add("freight_term", main.get("freight_term", ""));
                        row.add("equipment_number",
                            main.get("equipment_number", ""));
                        row.add("dlo_detail_id", main.get("dlo_detail_id", ""));
                        row.add("load_bar_count",
                            main.get("load_bar_count", ""));
                        list.add(row);
                    }
                } else if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    row.add("number_type",
                        moduleKey.getModuleName(main.get("number_type", 0)));
                    row.add("number", main.get("number", ""));
                    row.add("PickOthersNumber", main.get("number", ""));
                    row.add("load_bar", main.get("load_bar_name", ""));
                    row.add("dlo_id", main.get("dlo_id", 0L));
                    row.add("checkOutTime", DateUtil
                        .showLocalparseDateTo24Hours(
                            main.get("check_out_warehouse_time", ""),
                            ps_id));
                    row.add("carrier", main.get("company_name", ""));
                    row.add("custom_id", main.get("customer_id", ""));
                    row.add("company_id", main.get("company_id", ""));
                    row.add("freight_term", main.get("freight_term", ""));
                    row.add("equipment_number",
                        main.get("equipment_number", ""));
                    row.add("dlo_detail_id", main.get("dlo_detail_id", ""));
                    row.add("load_bar_count", main.get("load_bar_count", ""));
                    list.add(row);
                } else {
                    if (main.get("rel_type", 0) == CheckInMainDocumentsRelTypeKey.DELIVERY) {
                        row.add("number_type", moduleKey.getModuleName(main
                            .get("number_type", 0)));
                        row.add("number", main.get("number", ""));
                        row.add("PickOthersNumber", main.get("number", ""));
                        row.add("load_bar", main.get("load_bar_name", ""));
                        row.add("dlo_id", main.get("dlo_id", 0L));
                        row.add("checkOutTime", DateUtil
                            .showLocalparseDateTo24Hours(main.get(
                                "check_out_warehouse_time", ""), ps_id));
                        row.add("carrier", main.get("company_name", ""));
                        row.add("custom_id", main.get("customer_id", ""));
                        row.add("company_id", main.get("company_id", ""));
                        row.add("freight_term", main.get("freight_term", ""));
                        row.add("equipment_number",
                            main.get("equipment_number", ""));
                        row.add("dlo_detail_id", main.get("dlo_detail_id", ""));
                        row.add("load_bar_count",
                            main.get("load_bar_count", ""));
                        list.add(row);
                    }

                }

                i++;

            }

            DBRow data = new DBRow();
            data.add("load_bar_sum", loadCount);
            data.add("data", (DBRow[]) list.toArray(new DBRow[0]));
            DBRow[] datas = {data};
            return datas;
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }
    }

    /**
     * 根据checkout时间查询entrys 报表用 load
     */
    @Override
    public DBRow[] getSealReport(String startTime, String endTime, long ps_id)
        throws Exception {
        try {
            startTime = DateUtil.showUTCTime(startTime, ps_id);
            endTime = DateUtil.showUTCTime(endTime, ps_id);
            DBRow[] mains = floorCheckInMgrZwb.getSealReport(startTime,
                endTime, ps_id);
            DBRow[] mainss = new DBRow[mains.length];
            int i = 0;
            for (DBRow main : mains) {

                DBRow row = new DBRow();
                row.add("seal", main.get("out_seal", ""));
                row.add("dlo_id", main.get("dlo_id", 0L));
                row.add("checkOutTime",
                    DateUtil.showLocalparseDateTo24Hours(
                        main.get("check_out_time", ""), ps_id));
// row.add("load_bar_name", main.get("load_bar_name",""));
                row.add("carrier", main.get("company_name", ""));

                if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    row.add("LoadNumber", main.get("number", ""));
                }
                if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    row.add("OrderNumber", main.get("number", ""));
                }
                if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    row.add("PoNumber", main.get("number", ""));
                }
                if (main.get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    row.add("PickOthersNumber", main.get("number", ""));
                }

                row.add("custom_id", main.get("customer_id", ""));
                row.add("company_id", main.get("company_id", ""));
                mainss[i] = row;
                i++;

            }
            return mainss;
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }
    }

    /**
     * 根据finishTime 时间查询 task
     *
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */

    @Override
    public DBRow[] getTask(String startTime, String endTime, long ps_id)
        throws Exception {

        try {
            startTime = DateUtil.showUTCTime(startTime, ps_id);
            endTime = DateUtil.showUTCTime(endTime, ps_id);
            DBRow[] mains = floorCheckInMgrZwb.getTaskReportByCheckOutTime(
                startTime, endTime, ps_id);
            for (DBRow row : mains) {
                int type = row.get("number_type", 0);
                if (type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    row.add("number_type", "Inbound not found");
                } else if (type == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    row.add("number_type", "Outbound not found");
                } else {
                    row.add("number_type", moduleKey.getModuleName(type));
                }
                row.add("finish_time",
                    DateUtil.showLocalparseDateTo24Hours(
                        row.get("finish_time", ""), ps_id));
                row.add("handle_time",
                    DateUtil.showLocalparseDateTo24Hours(
                        row.get("handle_time", ""), ps_id));
            }
            return mains;
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }
    }

    /**
     * 报表 查找占用的门的task
     * @return
     */
    @Override
    public DBRow[] getOccupyDoor(long ps_id) throws SystemException {
        try {
            DBRow[] mains = floorCheckInMgrZwb.selectDockSituationReport(ps_id);
            DBRow[] datas = new DBRow[mains.length];

            for (int i = 0; i < mains.length; i++) {

                DBRow row = new DBRow();

                row.add("entryId", mains[i].get("dlo_id", 0l));
                row.add("door", mains[i].get("doorId", ""));
                row.add("LP", mains[i].get("gate_liscense_plate", ""));
                row.add("gate_container_no",
                    mains[i].get("gate_container_no", ""));
                row.add("WareHouseCheckInTime", DateUtil
                    .showLocalparseDateTo24Hours(
                        mains[i].get("warehouse_check_in_time", ""),
                        ps_id));

                row.add("rel_type", relTypeToBound(mains[i].get("rel_type", 0)));
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    row.add("LoadNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    row.add("OrderNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    row.add("PONumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    row.add("OutOthersNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN) {
                    row.add("CTNRNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                    row.add("BOLNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    row.add("InOthersNumber", mains[i].get("number", ""));
                }
                row.add("companyId", mains[i].get("company_id", ""));
                datas[i] = row;

            }
            return datas;
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }

    }

    /**
     * 报表 查找占用的停车位的task
     *
     * @return
     */
    @Override
    public DBRow[] getOccupyYard(long ps_id) throws SystemException {
        try {
            DBRow[] mains = floorCheckInMgrZwb.selectSpotSituationReport(ps_id);
            DBRow[] datas = new DBRow[mains.length];
            for (int i = 0; i < mains.length; i++) {

                DBRow row = new DBRow();
// datas[i] = row;
                row.add("entryId", mains[i].get("dlo_id", ""));
                row.add("yard", mains[i].get("yc_no", ""));
                row.add("LP", mains[i].get("gate_liscense_plate", ""));
                row.add("gate_container_no",
                    mains[i].get("gate_container_no", ""));
                row.add("GateCheckInTime",
                    DateUtil.showLocalparseDateTo24Hours(
                        mains[i].get("gate_check_in_time", ""), ps_id));

                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_LOAD) {
                    row.add("LoadNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_ORDER) {
                    row.add("OrderNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PONO) {
                    row.add("PONumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_PICKUP_ORTHERS) {
                    row.add("OutOthersNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_CTN) {
                    row.add("CTNRNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_BOL) {
                    row.add("BOLNumber", mains[i].get("number", ""));
                }
                if (mains[i].get("number_type", 0l) == ModuleKey.CHECK_IN_DELIVERY_ORTHERS) {
                    row.add("InOthersNumber", mains[i].get("number", ""));
                }
                row.add("companyId", mains[i].get("company_id", ""));
                datas[i] = row;
            }
            return datas;
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }
    }

    @Override
    public boolean isDetailLastOfEntry(long dlo_detail_id, long equipment_id)
        throws Exception {
        if (dlo_detail_id != 0l) {
            DBRow detailRow = floorCheckInMgrZwb
                .selectDetailByDetailId(dlo_detail_id);
            long entryId = detailRow.get("dlo_id", 0l);
            String loadNumber = detailRow.getString("number");
            DBRow[] details = getEntryDetailHasResources(entryId, equipment_id); // 子单据明细
            details = filterLoadDetail(details);
            return isLastOfEntry(loadNumber, details);
        }
        return false;
    }

    public DBRow findLoadBar(long load_bar_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.findLoadBar(load_bar_id);
        } catch (Exception e) {
            throw new SystemException(e, "findLoadBar", log);
        }
    }

    public DBRow[] getUseSpot(long ps_id) throws Exception {
        try {
            return this.floorCheckInMgrZwb.getUseSpot(ps_id);
        } catch (Exception e) {
            throw new SystemException(e, "getUseSpot", log);
        }
    }

    @Override
    public DBRow[] selectTaskBy(long rl_id, long main_id, int occupancy_type)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb.selectTaskBy(rl_id, main_id,
                occupancy_type);
        } catch (Exception e) {
            throw new SystemException(e, "getEntrys", log);
        }
    }

    /**
     * 判断task与其他未处理或处理中的task是否重
     *
     * @param order
     * @param number_type
     * @param ps_id
     * @param company_id
     * @param dlo_id
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午6:56:05
     */
    public DBRow[] findTaskByNumberTypePsCompanyNotDloId(String order,
                                                         int number_type, long ps_id, String company_id, long dlo_id)
        throws Exception {
        try {
            return floorCheckInMgrZwb.findTaskByNumberTypePsCompanyNotDloId(
                order, number_type, ps_id, company_id, dlo_id);
        } catch (Exception e) {
            throw new SystemException(e,
                "findTaskByNumberTypePsCompanyNotDloId", log);
        }
    }

    /**
     * ctn_number与其他entry的ctn_nubmer不能重
     *
     * @param ctn_number
     * @param dlo_id
     * @param ps_id
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年11月17日 下午7:14:37
     */
    public DBRow[] findCtnNumberNotDloId(String ctn_number, long dlo_id,
                                         long ps_id) throws Exception {
        try {
            return floorCheckInMgrZwb.findCtnNumberNotDloId(ctn_number, dlo_id,
                ps_id);
        } catch (Exception e) {
            throw new SystemException(e,
                "findTaskByNumberTypePsCompanyNotDloId", log);
        }
    }

    public DBRow[] selectLocationByEntryId(long dlo_id) throws Exception {
        try {
            return floorCheckInMgrZwb.selectLocationByEntryId(dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectLocationByEntryId", log);
        }
    }

    // 查询可用门spot
    public DBRow[] selectUnavailableSpot(long ps_id, long area_id, long dlo_id)
        throws Exception {
        try {
            return floorCheckInMgrZwb.selectUnavailableSpot(ps_id, area_id,
                dlo_id);
        } catch (Exception e) {
            throw new SystemException(e, "selectUnavailableSpot", log);
        }
    }

    /**
     * 下面一个方法的重载
     *
     * @param equipment
     * @param entry_id
     * @param dlo_detail_id
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date 2014年12月20日
     */
    @Override
    public int closeDetailReturnFlag(DBRow equipment, long entry_id,
                                     long dlo_detail_id) throws Exception {
        try {
            long equipment_id = equipment.get("equipment_id", 0l);
            DBRow[] details = getEntryDetailHasResources(entry_id, equipment_id);
            if (details != null && details.length > 0) {
                DBRow currentRow = filterCurrentLoadRow(dlo_detail_id, details);
                DBRow equipmentResources = floorCheckInMgrWfh
                    .findEquipmentOccupancySpace(equipment.get(
                        "equipment_id", 0));
                int returnFlag = returnNotifyFlag(dlo_detail_id, details,
                    currentRow, equipmentResources);
                if ((returnFlag == LoadCloseNotifyStayOrLeave || returnFlag == LoadCloseNotifyReleaseDoor)) {
                    CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr) MvcUtil
                        .getBeanFromContainer("proxyCheckInMgrZr");
                    if (checkInMgrZr.isEquipmentCheckOut(equipment)) {
                        returnFlag = LoadCloseForgateNotifyReleaseDoorOrStayOrLeave;
                    }
                }
                return returnFlag;
            }

        } catch (Exception e) {
            throw new SystemException(e, "closeDetailReturnFlag", log);
        }
        return LoadCloseNoifyNormal;
    }

    /**
     * close detail 的时候计算是否是最后一个 | 是门下面最后一个
     *
     * @param equipment_id
     * @param entry_id
     * @param dlo_detail_id
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date 2014年12月16日
     */
    @Override
    public int closeDetailReturnFlag(long equipment_id, long entry_id,
                                     long dlo_detail_id) throws Exception {
        try {
            return closeDetailReturnFlag(
                ymsMgrAPI.getDetailEquipment(equipment_id), entry_id,
                dlo_detail_id);
        } catch (Exception e) {
            throw new SystemException(e, "closeDetailReturnFlag", log);
        }

    }

    /**
     * 主要是 修改数据库结构过后，以前调用这个方法的地方都调用这个 过滤出来有资源的 关闭 或者是为关闭的
     */
    @Override
    public DBRow[] getEntryDetailHasResources(long entry_id, long equipment_id)
        throws Exception {
        try {
            DBRow[] results = floorSpaceResourcesRelationMgr
                .getTaskByEntryIdWithOccpuyStatus(entry_id, 0, equipment_id);
            if (results != null) {
                List<DBRow> returnArray = new ArrayList<DBRow>();
// 过滤出门的 + 然后添加门的信息
                Map<HoldDoubleValue<Long, Integer>, String> hashMap = new HashMap<HoldDoubleValue<Long, Integer>, String>();
                for (DBRow temp : results) {
                    HoldDoubleValue<Long, Integer> resoucesValues = returnFixResourcesIdAndResourcesType(temp);
                    if (resoucesValues.a > 0l && resoucesValues.b > 0) {

                        if (StringUtil.isNull(hashMap.get(resoucesValues.a))) {
                            String resouces_name = getDoorName(temp.get(
                                "resources_id", 0l));
                            hashMap.put(resoucesValues, resouces_name);
                        }
                        String resouces_name = hashMap.get(resoucesValues);
/*
* temp.add("doorId",doorId); temp.add("rl_id", rl_id);
*/
                        temp.add("resources_id", resoucesValues.a);
                        temp.add("resources_name", resouces_name);
                        temp.add("resources_type", resoucesValues.b);
                        returnArray.add(temp);

                    }
                }
                return returnArray.toArray(new DBRow[0]);
            }
            return null;
        } catch (Exception e) {
            throw new SystemException(e, "getEntryDetailHasDoor", log);
        }
    }

    /**
     * TODO 记录打印的标签————转PDF用
     */
    public void addPrintedLabel(HttpServletRequest request) throws Exception {
        try {

// long entry_id = StringUtil.getLong(request,"entry_id");
// long master_bol_no = StringUtil.getLong(request,"master_bol_no");
// String number = StringUtil.getString(request,"number");
// long type = StringUtil.getLong(request,"type");
// DBRow
// countRow=this.floorCheckInMgrZwb.selectCountPrintedLabel(entry_id,
// number, master_bol_no,type);
//
// if(countRow==null){
// String url = StringUtil.getString(request,"url");
//
// // int status = ReturnOrderStatusKey.RECEIVING;
//
// DBRow row = new DBRow();
// row.add("number",number);
// row.add("master_bol_no",master_bol_no);
// row.add("url",url);
// row.add("type",type);
// row.add("entry_id",entry_id);
// this.floorCheckInMgrZwb.addPrintedLabel(row);
// }

        } catch (Exception e) {
            throw new SystemException(e, "findLoadBar", log);
        }
    }

    public long windowCheckInUpdateEntryInfos() throws Exception {
        try {
            return 0;
        } catch (Exception e) {
            throw new SystemException(e, "findLoadBar", log);
        }
    }

    /**
     * 查询可用门
     *
     * @param ps_id
     * @param area_id
     * @param entry_id
     * @return
     * @throws Exception
     */
    public DBRow[] getVacancyDoor(long ps_id, long area_id, long entry_id)
        throws Exception {
        try {
            return this.floorSpaceResourcesRelationMgr.getCanUseDoor(entry_id,
                ps_id, area_id);
        } catch (Exception e) {
            throw new SystemException(e,
                "floorSpaceResourcesRelationMgr getVacancyDoor", log);
        }
    }

    /**
     * 根据doorName等条件模糊查询可用门
     *
     * @param ps_id
     * @param area_id
     * @param entry_id
     * @param door_name
     * @return
     * @throws Exception
     * @author geqingling 2014/12/2
     */
    public DBRow[] getCanUseDoorByDoorName(long entry_id, long ps_id,
                                           long area_id, String door_name) throws Exception {
        try {
            return this.floorSpaceResourcesRelationMgr.getCanUseDoorByDoorName(
                entry_id, ps_id, area_id, door_name);
        } catch (Exception e) {
            throw new SystemException(e,
                "floorSpaceResourcesRelationMgr getVacancyDoor", log);
        }
    }

    /**
     * 根据doorName等条件精确查询可用门
     *
     * @param ps_id
     * @param area_id
     * @param entry_id
     * @param door_name
     * @return
     * @throws Exception
     * @author geqingling 2014/12/2
     */
    public DBRow[] getDoorByDoorName(long entry_id, long ps_id, long area_id,
                                     String door_name) throws Exception {
        try {
            return this.floorSpaceResourcesRelationMgr.getDoorByDoorName(
                entry_id, ps_id, area_id, door_name);
        } catch (Exception e) {
            throw new SystemException(e,
                "floorSpaceResourcesRelationMgr getVacancyDoor", log);
        }
    }

    /**
     * 查询不可用的门
     *
     * @author geqingling 2014/12/2
     */
    public DBRow[] getUnavailableDoor(long ps_id, long area_id)
        throws Exception {
        try {
            DBRow[] rows = this.floorSpaceResourcesRelationMgr
                .getUnavailableDoor(ps_id, area_id);

            for (int i = 0; i < rows.length; i++) {
                long resources_id = rows[i].get("sd_id", 0l);
                DBRow[] equipments = this.floorSpaceResourcesRelationMgr
                    .getEquipmentRelation(OccupyTypeKey.DOOR, resources_id,
                        SpaceRelationTypeKey.Equipment);
                String equipment_number = "";
                for (int a = 0; a < equipments.length; a++) {
                    if (!StringUtil.isBlank(equipments[a]
                        .getString("equipment_number")))
                        equipment_number += equipments[a]
                            .getString("equipment_number") + ",";
                }
                DBRow taskRow = this.floorSpaceResourcesRelationMgr
                    .getCountRelation(OccupyTypeKey.DOOR, resources_id,
                        SpaceRelationTypeKey.Task);
                long num = taskRow.get("task", 0l);
                String taskStr = "Task[" + num + "]";
                rows[i].add("detail", equipment_number + taskStr);
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getVacancyDoor", log);
        }
    }

    /**
     * 模糊查询不可用的门
     *
     * @author geqingling 2014/12/11
     */
    public DBRow[] getUnavailableDoorByDoorName(long ps_id, long area_id,
                                                String door_name) throws Exception {
        try {
            DBRow[] rows = this.floorSpaceResourcesRelationMgr
                .getUnavailableDoorByDoorName(ps_id, area_id, door_name);
            for (int i = 0; i < rows.length; i++) {
                long resources_id = rows[i].get("sd_id", 0l);
                DBRow[] equipments = this.floorSpaceResourcesRelationMgr
                    .getEquipmentRelation(OccupyTypeKey.DOOR, resources_id,
                        SpaceRelationTypeKey.Equipment);
                String equipment_number = "";
                for (int a = 0; a < equipments.length; a++) {
                    equipment_number += equipments[a]
                        .getString("equipment_number") + ",";
                }
                DBRow taskRow = this.floorSpaceResourcesRelationMgr
                    .getCountRelation(OccupyTypeKey.DOOR, resources_id,
                        SpaceRelationTypeKey.Task);
                long num = taskRow.get("task", 0l);
                String taskStr = "Task[" + num + "]";
                rows[i].add("detail", equipment_number + taskStr);
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e,
                "CheckInMgrZwb getUnavailableDoorByDoorName", log);
        }
    }

    /**
     * 精确查询不可用的门
     *
     * @author geqingling 2014/12/16
     */
    public DBRow[] findUnavailableDoorByDoorName(long ps_id, long area_id,
                                                 String door_name) throws Exception {
        try {
            DBRow[] rows = this.floorSpaceResourcesRelationMgr
                .findUnavailableDoorByDoorName(ps_id, area_id, door_name);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e,
                "CheckInMgrZwb getUnavailableDoorByDoorName", log);
        }
    }

    /**
     * window check in 时查找被占用的门
     *
     * @author geqingling 2014/12/3 现在不用了，改为gate check in 是查找门的方法了 2014/12/11
     */
    public DBRow[] getOccupiedDoorOfWindow(long ps_id, long area_id)
        throws Exception {
        try {
            return this.floorSpaceResourcesRelationMgr.getOccupiedDoorOfWindow(
                ps_id, area_id);
        } catch (Exception e) {
            throw new SystemException(e,
                "floorSpaceResourcesRelationMgr getVacancyDoor", log);
        }
    }

    /**
     * 查询可用停车位
     *
     * @param ps_id
     * @param area_id
     * @param entry_id
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] getVacancySpot(long ps_id, long area_id, long entry_id)
        throws Exception {
        try {
            return floorSpaceResourcesRelationMgr.getCanUseSpot(entry_id,
                ps_id, area_id);
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getVacancySpot", log);
        }

    }

    /**
     * 根据yc_no模糊查询可用的停车位 gql
     */
    public DBRow[] getVacancySpotByYcno(long entry_id, long area_id,
                                        long ps_id, String yc_no) throws Exception {
        try {
            DBRow[] rows = floorSpaceResourcesRelationMgr.getCanUseSpotByYcno(
                entry_id, ps_id, area_id, yc_no);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getVacancySpotByYcno",
                log);
        }
    }

    /**
     * 根据yc_no精确查询可用的停车位 zwb
     *
     * @param entry_id
     * @param area_id
     * @param ps_id
     * @param yc_no
     * @return
     * @throws Exception
     */
    public DBRow[] getVacancySpotByYcnoAccurate(long entry_id, long area_id,
                                                long ps_id, String yc_no) throws Exception {
        try {
            DBRow[] rows = floorSpaceResourcesRelationMgr
                .getCanUseSpotByYcnoAccurate(entry_id, ps_id, area_id,
                    yc_no);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getVacancySpotByYcno",
                log);
        }
    }

    /**
     * 查询被占用的停车位
     *
     * @param ps_id
     * @param area_id
     * @param entry_id
     * @return
     * @throws Exception
     */
    public DBRow[] getOccupiedSpot(long ps_id, long area_id, long entry_id)
        throws Exception {
        try {
            DBRow[] rows = floorSpaceResourcesRelationMgr.getUnavailableSpot(
                ps_id, area_id, 0); // 查询所有占用资源 设备和task
            for (int i = 0; i < rows.length; i++) {
                long resources_id = rows[i].get("yc_id", 0l);
                DBRow[] equipments = this.floorSpaceResourcesRelationMgr
                    .getEquipmentRelation(OccupyTypeKey.SPOT, resources_id,
                        SpaceRelationTypeKey.Equipment);
                String equipment_number = "";
                for (int a = 0; a < equipments.length; a++) {
                    if (!StringUtil.isBlank(equipments[a]
                        .getString("equipment_number")))
                        equipment_number += equipments[a]
                            .getString("equipment_number") + ",";
                }
                DBRow taskRow = this.floorSpaceResourcesRelationMgr
                    .getCountRelation(OccupyTypeKey.SPOT, resources_id,
                        SpaceRelationTypeKey.Task);
                long num = taskRow.get("task", 0l);
                String taskStr = "Task[" + num + "]";
                rows[i].add("detail", equipment_number + taskStr);
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getOccupiedSpot", log);
        }
    }

    @Override
    public void checkEntryPermission(long entry_id,
                                     AdminLoginBean adminLoginBean) throws CheckInNotFoundException,
        NoPermiessionEntryIdException, SystemException {
        try {
            DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
            CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
        } catch (CheckInNotFoundException e) {
            throw e;
        } catch (NoPermiessionEntryIdException e) {
            throw e;
        } catch (Exception e) {
            throw new SystemException(e, "checkEntryPermission", log);
        }

    }

    @Override
    public DBRow[] getTaskByEquimentId(long equipment_id, int number_status[])
        throws Exception {
        try {

            return this.floorCheckInMgrZwb.getTaskByEquimentId(equipment_id,
                number_status);
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb getTaskByEquimentId",
                log);
        }
    }

    @Override
    public long updateDetailByIsExist(long detail_id, DBRow row)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb
                .updateDetailByIsExist(detail_id, row);
        } catch (Exception e) {
            throw new SystemException(e, "CheckInMgrZwb updateDetailByIsExist",
                log);
        }
    }

    /**
     * 根据主单据号查询占用的信息
     *
     * @param module_id
     * @return
     * @throws Exception
     */
    public DBRow getResourcesByModuleId(long module_id) throws Exception {
        try {
            return this.floorSpaceResourcesRelationMgr.getResourcesByModuleId(
                module_id, ModuleKey.CHECK_IN);
        } catch (Exception e) {
            throw new SystemException(e, "getResourcesByModuleId", log);
        }
    }

    /**
     * 根据主单据id查询 设备占用的停车位 gatecheckin 标签用 zwb
     *
     * @param module_id
     * @return
     * @throws Exception
     */
    public DBRow getEquipmentUseSpot(long module_id) throws Exception {
        try {
            DBRow[] rows = this.floorSpaceResourcesRelationMgr
                .getEquipmentUseSpot(module_id);
            DBRow row = null;
            String yc_no = "";
            if (rows != null && rows.length > 0) {
                row = rows[0];
                for (DBRow r : rows) {
                    yc_no += "," + r.getString("yc_no").trim();
                }
            }
            if (row != null) {
                row.remove("yc_no");
                row.add("yc_no", yc_no.substring(1));
            }
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "getEquipmentUseSpot", log);
        }
    }

    /**
     * 根据主单据id查询 task占用的门 gatecheckin 标签用 zwb
     *
     * @param module_id
     * @return
     * @throws Exception
     */
    public DBRow[] getTaskUseDoor(long module_id) throws Exception {
        try {
            DBRow[] rows = this.floorSpaceResourcesRelationMgr
                .getTaskUseDoor(module_id);
            ArrayList<DBRow> list = new ArrayList<DBRow>();
            if (rows != null && rows.length > 0) {
                DBRow temp = rows[0];
                String doorId = rows[0].get("doorId", "");
                String number = "";
                for (int i = 0; i < rows.length; i++) {
                    DBRow row = rows[i];
                    if (doorId == row.get("doorId", "")) {
                        number += "," + row.getString("number").trim();
                    } else {
                        temp = rows[i - 1];
                        temp.remove("number");
                        temp.add("number", number.substring(1));
                        list.add(temp);
                        temp = row;
                        doorId = row.get("doorId", "");
                        number = "," + row.getString("number").trim();
                    }
                }

                temp.remove("number");
                temp.add("number", number.substring(1));
                list.add(temp);

            }

            return (DBRow[]) (list.toArray(new DBRow[list.size()]));
        } catch (Exception e) {
            throw new SystemException(e, "getTaskUseDoor", log);
        }
    }

    /**
     * 根据设备id 查询任务
     *
     * @return
     * @throws Exception
     */
    public DBRow[] getTaskByEquipmentId(long equipmentId) throws Exception {
        try {
            return this.floorCheckInMgrZwb.getTaskByEquipmentId(equipmentId);
        } catch (Exception e) {
            throw new SystemException(e, "getTaskByEquipmentId", log);
        }
    }

    /**
     * 根据entry_id和task查询设备表信息 geql
     *
     * @return
     * @throws Exception
     */
    public DBRow getEquipmentByTask(long entry_id, String number)
        throws Exception {
        try {
            DBRow row = new DBRow();
            DBRow[] rows = this.floorCheckInMgrZwb.getEquipmentByTask(entry_id,
                number);
            if (rows != null && rows.length > 0) {
                row = rows[0];
            }
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "getTaskByEquipmentId", log);
        }
    }

    /**
     * 通过entryId查询车头信息
     *
     * @param entry_id
     * @return
     * @throws Exception
     * @author xujia
     */
    @Override
    public DBRow selectTractorByEntryId(long entry_id) throws Exception {
        try {
            DBRow[] gateRow = this.floorCheckInMgrZwb
                .selectTractorByEntryId(entry_id);
            DBRow result = null;
            if (gateRow.length > 0) {
                result = gateRow[0];
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "selectGateByEntryId", log);
        }
    }

    /**
     * 通过entryId查询车尾信息
     *
     * @param entry_id
     * @return
     * @throws Exception
     * @author xujia
     */
    @Override
    public DBRow selectTrailerByEntryId(long entry_id) throws Exception {
        try {
            DBRow[] gateRow = this.floorCheckInMgrZwb
                .selectTrailerByEntryId(entry_id);
            DBRow result = null;
            if (gateRow.length > 0) {
                result = gateRow[0];
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "selectGateByEntryId", log);
        }
    }

    /**
     * 通过entryId查询带走的设备
     *
     * @param entry_id
     * @return
     * @throws Exception
     * @author xujia
     */
    @Override
    public DBRow selectPickUpCTNRByEntryId(long entry_id) throws Exception {
        try {
            DBRow[] row = this.floorCheckInMgrZwb
                .selectPickUpCTNRByEntryId(entry_id);
            DBRow result = null;
            if (row.length > 0) {
                result = row[0];
            }
            return result;
        } catch (Exception e) {
            throw new SystemException(e, "selectGateByEntryId", log);
        }
    }

    // 去除数组中重复的记录
    public String array_unique(DBRow[] a, String fieldNm) {
// array_unique
        List<String> list = new LinkedList<String>();
        String str = "";
        for (int i = 0; i < a.length; i++) {
            if (!"".equals(a[i].getString(fieldNm))
                && !list.contains(a[i].getString(fieldNm))) {
                list.add(a[i].getString(fieldNm));
                str += "'" + a[i].getString(fieldNm);
            }
        }
        return str;
    }

    /**
     * 根据主单据id查询 设备表 gatecheckin 标签用 zwb
     *
     * @param entry_id
     * @return
     * @throws Exception
     */
    public DBRow[] getEquipmentByEntryId(long entry_id) throws Exception {
        try {

            DBRow[] detailRow = this.findAllLoadByMainId(entry_id);// 根据entry_id查询详细信息
            String title = array_unique(detailRow, "supplier_id");
            String customer_id = array_unique(detailRow, "customer_id");
            long rl_id = 0L;// 门或位置ID
// if(detailRow!=null&&detailRow.length>0){
// rl_id = detailRow[0].get("rl_id", 0L);
// for(DBRow row : detailRow){
// if(!"".equals(row.getString("supplier_id"))){
// title += "'"+row.getString("supplier_id");
// }
// if(!"".equals(row.getString("customer_id"))){
// customer_id += "'"+row.getString("customer_id");
// }
// }
// }
            if ("".equals(title)) {
                title = "'";
            }
            if ("".equals(customer_id)) {
                customer_id = "'";
            }

            int live_flag = YesOrNotKey.YES;// 是否是live load 的标记 ,默认是live load
            DBRow[] rows = this.floorEquipmentMgr
                .getEquipmentByEntryId(entry_id);// 根据entry_id查询设备信息
            int rel_type = 0;// 类型
            String rel_type_value = "";
            if (rows != null && rows.length > 0) {
                for (DBRow row : rows) {
                    long purpose = row.get("equipment_purpose", 0l);// 设备目标
                    long type = row.get("equipment_type", 0l);// 设备类型
// 若车尾全部是live load 判断 如果车尾都是liveload 出一张标签--车头车尾 混合)
                    if (type == EquipmentTypeKey.Container) {
                        if (purpose != CheckInLiveLoadOrDropOffKey.LIVE) {
                            live_flag = YesOrNotKey.NO;
                        }
                    } else {
                        rel_type = row.get("rel_type", 0);
                    }
                }

// live load 的情况
                if (live_flag == YesOrNotKey.YES) {
                    rel_type_value = new CheckInMainDocumentsRelTypeKey()
                        .getCheckInMainDocumentsRelTypeKeyValue(rel_type);
                    for (DBRow row : rows) {
                        row.add("live_flag", live_flag); // 将 是否是live load
// 的标记加入到rows中
                        int purpose = row.get("equipment_purpose", 0);
                        String equipment_purpose_value = new CheckInLiveLoadOrDropOffKey()
                            .getCheckInLiveLoadOrDropOffKey(purpose);
                        row.add("equipment_purpose_value",
                            equipment_purpose_value);
                        row.add("rel_type_value", rel_type_value); // rel_type标记为车头的rel_type状态的
// key对应的value，
                        row.add("title", title.substring(1)); // ，分割组合后的title
                        row.add("customer_id", customer_id.substring(1)); // ，分割组合后的customer_id
                        row.add("rl_id", rl_id);// 添加详情表中门或位置ID
                    }
                } else {
                    for (DBRow row : rows) {
                        row.add("live_flag", live_flag); // 将 是否是live load
// 的标记加入到rows中
                        int purpose = row.get("equipment_purpose", 0);
                        rel_type = row.get("rel_type", 0);
                        String equipment_purpose_value = new CheckInLiveLoadOrDropOffKey()
                            .getCheckInLiveLoadOrDropOffKey(purpose);
                        rel_type_value = new CheckInMainDocumentsRelTypeKey()
                            .getCheckInMainDocumentsRelTypeKeyValue(rel_type);
                        row.add("equipment_purpose_value",
                            equipment_purpose_value);
                        row.add("rel_type_value", rel_type_value); // rel_type标记为各自rel_type的
// key对应的value
                        row.add("title", title.substring(1)); // ，分割组合后的title
                        row.add("customer_id", customer_id.substring(1)); // ，分割组合后的customer_id
                        row.add("rl_id", rl_id); // 添加详情表中门或位置ID
                    }
                }
            }
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "getTaskByEquipmentId", log);
        }
    }

    public DBRow getEntryPrint(long entry_id, AdminLoginBean adminLoggerBean)
        throws Exception {

        try {
            DBRow row = new DBRow();
            loadPaperWorkMgrZr.getEntryPrint(row, entry_id, adminLoggerBean);
// System.out.println(StringUtil.convertDBRowsToJsonString(row));
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "getTaskByEquipmentId", log);
        }

    }

    /**
     * check in index 显示通知
     *
     * @param entry_id
     * @param associate_process
     * @return
     * @throws Exception
     * @xujia
     */
    @Override
    public DBRow[] findNoticesByAssociateProcess(long entry_id,
                                                 int associate_process) throws Exception {
        try {
            DBRow[] detailsRow = this.findloadingByInfoId(entry_id);
            List<DBRow> notices = new ArrayList<DBRow>();
            for (int i = 0; i < detailsRow.length; i++) {
                DBRow[] result = this.floorCheckInMgrZwb
                    .findNoticesByAssociateProcess(
                        detailsRow[i].get("dlo_detail_id", 0l),
                        ModuleKey.CHECK_IN, associate_process);
                for (int j = 0; j < result.length; j++) {
                    notices.add(result[j]);
                }
            }
            return (DBRow[]) notices.toArray(new DBRow[0]);
        } catch (Exception e) {
            throw new SystemException(e, "findNoticesByAssociateProcess", log);
        }
    }

    /**
     * 查询gate发的所有通知详细
     *
     * @return
     * @throws Exception
     */
    public DBRow[] findGateotices(long entry_id) throws Exception {
        try {

            DBRow[] detailsRow = this.findloadingByInfoId(entry_id);
            List<DBRow> notices = new ArrayList<DBRow>();
            if (detailsRow != null && detailsRow.length > 0) {
                DBRow[] result = this.floorCheckInMgrZwb.findGateotices(
                    detailsRow[0].get("dlo_detail_id", 0l),
                    ModuleKey.CHECK_IN);
                for (int j = 0; j < result.length; j++) {
                    notices.add(result[j]);
                }
            }
            return (DBRow[]) notices.toArray(new DBRow[0]);
        } catch (Exception e) {
            throw new SystemException(e, "findNGateoticesByAssociateProcess",
                log);
        }
    }

    /**
     * 查询一条通知详细
     *
     * @param schedule_sub_id
     * @return
     * @throws Exception
     * @xujia
     */
    public DBRow findNoticeByscheduleSubId(long schedule_sub_id)
        throws Exception {
        try {
            return this.floorCheckInMgrZwb
                .findNoticeByscheduleSubId(schedule_sub_id);
        } catch (Exception e) {
            throw new SystemException(e, "findNoticeByscheduleSubId", log);
        }
    }

    /**
     * 更新gps
     *
     * @param gps
     * @param row
     * @throws Exception
     * @author:zyj
     * @date: 2014年12月15日 下午9:03:11
     */
    public void updateGpsInfos(long gps, DBRow row) throws Exception {
        try {
            floorCheckInMgrZwb.updateAsset(gps, row);
        } catch (Exception e) {
            throw new SystemException(e, "updateGpsInfos", log);
        }
    }

    /**
     * 查询资源
     *
     * @param relation_type
     * @param relation_id
     * @throws Exception
     * @author:gql
     * @date: 2014年12月19日
     */
    public DBRow getSpaceResorceRelation(int relation_type, long relation_id)
        throws Exception {
        try {
            DBRow row = this.floorSpaceResourcesRelationMgr
                .getSpaceResorceRelation(relation_type, relation_id);
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "updateGpsInfos", log);
        }
    }

    /**
     * 验证单据类型
     *
     * @param type
     * @param search_number
     * @param mainId
     * @param ps_id
     * @param adid
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2014年12月22日 下午5:25:36
     */
    public DBRow checkOrderTypeSystem(long type, String search_number,
                                      long mainId, long ps_id, long adid) throws Exception {

        DBRow result = null;
        DBRow pickUp = null;
        DBRow[] local = this.floorCheckInMgrZwb.storageSearchLocal(ps_id);
        DBRow[] wms = this.floorCheckInMgrZwb.storageSearchWms(ps_id);
        DBRow[] companyId = this.floorCheckInMgrZwb.findWmsStorage(ps_id);
        int[] status = null;
        String[] company_id = null;
// int mark = 0;
        if (companyId.length > 0) {
            company_id = new String[companyId.length];
            for (int i = 0; i < companyId.length; i++) {
                company_id[i] = companyId[i].get("company_id", "");
            }
        }

        if (type == GateCheckLoadingTypeKey.LOAD) {
            if (local.length > 0 && local[0].get("search_priority", 0l) == 1) {
// DBRow[] localResult = null;
                status = new int[]{ContainerImportStatusKey.RECEIVED};

                result = this.floorCheckInMgrZwb.findContainerInfoByNo("",
                    search_number, status);
                if (result != null) {
                    result.add("accountid", result.get("account_id", ""));
                    result.add("order_type", ModuleKey.CHECK_IN_LOAD);
                    result.add("system_type", OrderSystemTypeKey.SYNC);
                }
                if (wms.length > 0 && wms[0].get("search_priority", 0l) == 2) {
                    if (result == null && companyId.length > 0) {
                        pickUp = sqlServerMgrZJ
                            .checkOrderTypeAndChangeOrderTypeByNo(
                                search_number, company_id);

                        if (null != pickUp) {
                            DBRow[] orderLoads = (DBRow[]) pickUp.get("infos",
                                new DBRow[0]);

                            if (orderLoads.length == 1) {
                                result = orderLoads[0];
                                result.add("system_type",
                                    OrderSystemTypeKey.WMS);
                            }
                            if (orderLoads.length > 1) {
                                result = new DBRow();
                                result.add("pickup", pickUp);
                                result.add("num", -1);
                                result.add("system_type",
                                    OrderSystemTypeKey.WMS);
                            }

                        }
// if(result==null){
// mark=1;
// }

                    }
                }

            } else if (wms.length > 0 && wms[0].get("search_priority", 0l) == 1) {
                pickUp = sqlServerMgrZJ.checkOrderTypeAndChangeOrderTypeByNo(
                    search_number, company_id);
                if (null != pickUp) {
                    DBRow[] orderLoads = (DBRow[]) pickUp.get("infos",
                        new DBRow[0]);

                    if (orderLoads.length == 1) {
                        result = orderLoads[0];
                        result.add("system_type", OrderSystemTypeKey.WMS);
                    }
                    if (orderLoads.length > 1) {
                        result = new DBRow();
                        result.add("pickup", pickUp);
                        result.add("num", -1);
                        result.add("system_type", OrderSystemTypeKey.WMS);
                    }

                }
// if(pickUp!=null && pickUp.length==1){
// result=pickUp;
// }
// if(result==null){
// mark=1;
// }

            }

        } else {
            if (local.length > 0 && local[0].get("search_priority", 0l) == 1) {
// DBRow[] localResult = null;
                status = new int[]{ContainerImportStatusKey.IMPORTED};
                result = this.findReceiptByContainerBolNoOrLoad(search_number,
                    status);
// TODO container_no
                if (result != null) {
// result=localResult[0];
                    result.add("accountid", result.get("account_id", ""));
                    result.add("order_type", ModuleKey.CHECK_IN_CTN);
                    result.add("system_type", OrderSystemTypeKey.SYNC);
                }
                if (wms.length > 0 && wms[0].get("search_priority", 0l) == 2) {
                    if (result == null && companyId.length > 0) {
                        DBRow delivery = sqlServerMgrZJ
                            .findReceiptByContainerBolNo(search_number,
                                adid, company_id);
                        if (null != delivery) {
                            DBRow[] deliverys = (DBRow[]) delivery.get("infos",
                                new DBRow[0]);
                            if (deliverys.length == 1) {
                                result = deliverys[0];
                                result.add("system_type",
                                    OrderSystemTypeKey.WMS);
                            }
                            if (deliverys.length > 1) {
                                result = new DBRow();
                                result.add("num", -1);
                                result.add("pickup", delivery);
                                result.add("system_type",
                                    OrderSystemTypeKey.WMS);
                            }
                        }
// if(result==null){
// mark=1;
// }
                    }
                }
            } else if (wms.length > 0 && wms[0].get("search_priority", 0l) == 1) {
                DBRow delivery = sqlServerMgrZJ.findReceiptByContainerBolNo(
                    search_number, adid, company_id);
                if (null != delivery) {
                    DBRow[] deliverys = (DBRow[]) delivery.get("infos",
                        new DBRow[0]);
                    if (deliverys.length == 1) {
                        result = deliverys[0];
                        result.add("system_type", OrderSystemTypeKey.WMS);
                    }
                    if (deliverys.length > 1) {
                        result = new DBRow();
                        result.add("num", -1);
                        result.add("pickup", delivery);
                        result.add("system_type", OrderSystemTypeKey.WMS);
                    }
                }
// if(result==null){
// mark=1;
// }
            }
        }

        if (result != null) {
            if (result.get("system_type", 0) == OrderSystemTypeKey.WMS) {
                if (result.get("num", 0l) != -1) {
                    String area_name = result.get("StagingAreaID", "");
                    String title_name = result.get("SupplierID", "");
                    DBRow door = autoAssignDoor(area_name, title_name, type,
                        search_number, mainId, ps_id);
                    if (door != null) {
                        result.add("resources_id",
                            String.valueOf(door.get("door_id", 0l)));
                        result.add("resources_name", door.get("door_name", ""));
                        result.add("resources_type", OccupyTypeKey.DOOR);
                        result.add("area_id",
                            String.valueOf(door.get("area_id", 0l)));
                        result.add("area_name", door.get("area_name", ""));
                    } else {
                        if (type != GateCheckLoadingTypeKey.LOAD) {
                            DBRow titleRow = this.floorCheckInMgrZwb
                                .selectTitleIdByTitleName(title_name);
                            if (titleRow != null) {
                                DBRow nearestDoor = this.floorCheckInMgrZwb
                                    .selectNearestDoor(ps_id,
                                        titleRow.get("title_id", 0l));

                                if (!nearestDoor.get("door", "").equals("0")) {
                                    String[] doorStr = nearestDoor.get("door",
                                        "").split(",");
                                    result.add("resources_id", doorStr[0]);
                                    result.add("resources_name", doorStr[1]);
                                    result.add("resources_type",
                                        OccupyTypeKey.DOOR);
                                    result.add("area_id", doorStr[2]);
                                    result.add("area_name", doorStr[3]);
                                }
                            }

                        }
                    }
                    result.add("num", 1);
                }

            } else {
                if (type == GateCheckLoadingTypeKey.LOAD) {
                    result.add("appointmentdate",
                        result.get("ship_out_date", ""));
                }
                result.add("customerid", result.get("customer_id", ""));
                result.add("num", 1);
            }

        }

        if (result == null) {
            result = new DBRow();
            result.add("num", 0);
            result.add("system_type", OrderSystemTypeKey.SYNC);
            int search_number_type = ModuleKey.CHECK_IN_DELIVERY_ORTHERS;
            if (type == GateCheckLoadingTypeKey.LOAD) {
                search_number_type = ModuleKey.CHECK_IN_PICKUP_ORTHERS;
            }
            DBRow result1 = new DBRow();
            result1.add("search_number_type", search_number_type);
            result1.add("search_number", search_number);
            result1.add("order_type", search_number_type);
            result1.add("number", search_number);
            result1.add("order_type_value",
                moduleKey.getModuleName(search_number_type));

            DBRow[] result2 = new DBRow[1];
            result2[0] = result1;
            DBRow result3 = new DBRow();
            result3.add("infos", result2);
            result.add("pickup", result3);
        } else if (result.get("num", 0) == 1) {
            DBRow[] result2 = new DBRow[1];
            result2[0] = result;
            DBRow result3 = new DBRow();
            result3.add("infos", result2);
            DBRow result1 = new DBRow();

            result1.add("num", 1);
            result1.add("system_type", result.get("system_type", 0));
            result1.add("pickup", result3);
            result = result1;
        }

        return result;

    }

    // bol标签用根据entry id 查询 车头的设备zwb
    public DBRow findTractorByEntryId(long entry_id) throws Exception {
        try {
            DBRow row = new DBRow();
            DBRow[] rows = this.floorEquipmentMgr
                .getEquipmentByEntryId(entry_id);
            for (int i = 0; i < rows.length; i++) {
                if (rows[i].get("equipment_type", 0) == CheckInTractorOrTrailerTypeKey.TRACTOR) {
                    row.add("tractor_num",
                        rows[i].getString("equipment_number"));
                }
            }
            return row;
        } catch (Exception e) {
            throw new SystemException(e, "findTractorByEntryId", log);
        }
    }

    /**
     * 根据entryId查询去重的所有dn对应的文件id，下载pdf用 gql
     *
     * @param EntryId
     * @return
     * @throws Exception
     */
    public DBRow[] getFileIdByEntryId(long EntryId, HttpServletRequest request)
        throws Exception {
        try {
            ArrayList<DBRow> resultList = new ArrayList<DBRow>();

            DBRow[] loadNos = floorSpaceResourcesRelationMgr
                .getTaskByEntryIdWithOccpuyStatus(EntryId, 0, 0);// 查询该entry下所有loadNo
            for (int i = 0; i < loadNos.length; i++) {
                DBRow row = loadNos[i];
                String loadNo = row.getString("number");
                int numberType = row.get("number_type", 0);
                String companyId = row.getString("company_id");
                long detailId = row.get("dlo_detail_id", 0L);
                String[] customerIds = row.getString("customer_id").split(",");

                if (numberType == ModuleKey.CHECK_IN_LOAD
                    || numberType == ModuleKey.CHECK_IN_PONO
                    || numberType == ModuleKey.CHECK_IN_ORDER) {
                    if (customerIds != null && customerIds.length > 0) {
                        for (String customerId : customerIds) {
                            DBRow result = new DBRow();
                            ArrayList<DBRow> dnList = new ArrayList<DBRow>();
                            String dnFlag = "fail";
                            String signFlag = "fail";
                            String counting = "fail";
                            AdminLoginBean adminLoggerBean = adminMgr
                                .getAdminLoginBean(StringUtil
                                    .getSession(request));
                            long adid = adminLoggerBean.getAdid();
                            DBRow[] dns = null;
                            if (numberType == ModuleKey.CHECK_IN_ORDER) {
                                DBRow dnRow = sqlServerMgrZJ
                                    .findOrderSomeInfoByOrderNo(
                                        Long.valueOf(loadNo),
                                        companyId, adid, request);
                                if (dnRow != null) {
                                    dns = new DBRow[]{dnRow};
                                }
                            } else if (numberType == ModuleKey.CHECK_IN_LOAD) {
                                dns = floorSQLServerMgrZJ
                                    .findOrderPonoInfosByLoadNoMasterBolNo(
                                        loadNo, new Long[0], companyId,
                                        customerId, null);// 查询该load下所有的dn
                            } else {
                                dns = floorSQLServerMgrZJ
                                    .findOrderPonoInfosByPoNo(loadNo,
                                        companyId, customerId, null);// 查询该PoNo下所有的dn
                            }

                            String strDebug = "LoadNo:[" + loadNo + "],DN:";
                            if (dns != null) {
                                strDebug += dns.length;
                            }
                            System.out.println("*********************"
                                + strDebug + "*******************");
/*
* 查找满足条件的laod，条件如下：
* load类型为Load、ordre、pono，且load下有dn, 将信息组合到result列表中
*/
                            if (dns != null && dns.length > 0) {

                                DBRow[] detailRows = this.floorCheckInMgrZwb
                                    .getFileIdByEntryId(EntryId, loadNo,
                                        "", "");// 查询该entry某个load所有生成pdf的dn的详细信息，

/*
* 1、单个order的bol，load_no字段中存放order_no
* 2、根据load生成的签，load_no字段中存放number
* 3、其他情况，load_no字段中存放number。
*
* if(numberType==ModuleKey.CHECK_IN_PONO||
* numberType==ModuleKey.CHECK_IN_ORDER){ loadNo
* = row.get("order_no",""); }
*/

/*
* 1、生成新的temp来存储Dn、file_id信息，
* 2、给所有的dn都添加file_id字段，默认值为"",
* 3、若有对应的pdf，file_id为detailRows中的file_id的值
* 4、将temp[]放到load的dbrow中。
*/
                                for (int m = 0; m < dns.length; m++) {

                                    String dn = dns[m].getString("ReferenceNo")
                                        .trim();
                                    DBRow drBundle = this.findOrderByBundle(
                                        dns[m].getString("OrderNo").trim(),
                                        companyId);
                                    if (drBundle != null) {
                                        dn = drBundle.getString("ReferenceNo");
                                    }
                                    String file_id = "";
                                    DBRow temp = new DBRow();
                                    temp.add("load_no", loadNo);
                                    temp.add("entry_id", EntryId);
                                    temp.add("dn", dn);
                                    for (int n = 0; n < detailRows.length; n++) {
                                        String dnOfDetail = detailRows[n]
                                            .getString("dn");
                                        if (dn.equals(dnOfDetail)) {
                                            file_id = detailRows[n]
                                                .getString("file_id");
                                        }
                                    }
                                    temp.add("file_id", file_id);
                                    if (!"".equals(file_id)) {
                                        dnList.add(0, temp);
                                    } else {
                                        dnList.add(temp);
                                    }
                                }

// 判断是否有dn未生成pdf
                                if (dnList != null && dnList.size() > 0) {
                                    int dnSize = dnList.size() - 1;
                                    if (dnList.get(dnSize).getString("file_id")
                                        .length() > 0) {
                                        dnFlag = "success";
                                    }
                                }

// 判断masterBol是否签字
                                DBRow[] signRows = sqlServerMgrZJ
                                    .findBillOfLadingTemplateByLoad(loadNo,
                                        companyId, customerId, 0,
                                        request);// 查询该customer_id条件下的
                                if (signRows != null && signRows.length > 0) {
                                    String master_bol_nos = signRows[0]
                                        .getString("master_bol_nos");
                                    String relative_value = EntryId + "_"
                                        + loadNo + "_" + master_bol_nos;
                                    DBRow[] relativeFileRows = floorRelativeFileMgrZr
                                        .getRelativeFile(relative_value, 2);
                                    if (relativeFileRows != null
                                        && relativeFileRows.length > 0) {
                                        signFlag = "success";
                                    }
                                } else {
                                    String relative_value = EntryId + "_"
                                        + loadNo + "_";
                                    DBRow[] relativeFileRows = floorRelativeFileMgrZr
                                        .getRelativeFile(relative_value, 2);
                                    if (relativeFileRows != null
                                        && relativeFileRows.length > 0) {
                                        signFlag = "success";
                                    }

                                }

// 是否有counting sheet
                                DBRow[] cRows = fileMgrZr
                                    .getCheckInCountingSheet(EntryId,
                                        detailId, customerId);
                                if (cRows != null && cRows.length > 0) {
                                    counting = "success";
                                }

                                result.add("dnFlag", dnFlag);
                                result.add("signFlag", signFlag);
                                result.add("counting", counting);

                                result.add("number", loadNo);
                                result.add("number_type", numberType);
                                result.add("company_id", companyId);
                                result.add("customer_id", customerId);
                                result.add("dlo_detail_id", detailId);
                                result.add("details", dnList
                                    .toArray(new DBRow[dnList.size()]));
                                resultList.add(result);
                            }
                        }
                    }

                }

            }

            return (DBRow[]) resultList.toArray(new DBRow[resultList.size()]);
        } catch (Exception e) {
// e.printStackTrace();
            throw new SystemException(e, "getFileIdByEntryId", log);
        }
    }

    @Override
    public String handleHtmlToPDF(HttpServletRequest request) throws Exception {
        String message = "false";
        try {
            String sessionId = request.getRequestedSessionId();
            String URL = uploadFileUrl;
            String getURL = downLoadFileUrl;

            String loadNo = StringUtil.getString(request, "loadNo", null);
            String customerId = StringUtil.getString(request, "customerId",
                null);
            String companyId = StringUtil.getString(request, "companyId", null);
            String type = StringUtil.getString(request, "type", null);
            Long entryId = StringUtil.getLong(request, "entryId");
            String pdfContent = StringUtil.getString(request, "template", null);
            UUID uuid = UUID.randomUUID();
// 区分MBOL和BOL
            String dn = StringUtil.getString(request, "dn", null);
            String strIsManual = StringUtil.getString(request, "isManual", "");
            boolean manual = false;
            if ("true".equals(strIsManual)) {
                manual = true;
            }
            boolean is = false;// 标示

// 创建目录
            File pdfFilePath = new File(PDFPath);
            if (!pdfFilePath.exists()) {
                pdfFilePath.mkdirs();
            }

            String filePath = "";
            String fileName = uuid.toString() + ".html";
            filePath = pdfFilePath + File.separator + fileName;
            FileUtil.createFile(filePath, "utf8",
                pdfContent.replaceAll("nbsp", "#160"));
            String ip = "127.0.0.1";
            if (!request.getLocalAddr().equals("0:0:0:0:0:0:0:1")) {
                ip = request.getLocalAddr();
            }
// ip= ip+":"+request.getLocalPort();
// String url =
// "http://"+request.getServerName()+":"+request.getLocalPort()+"/Sync10/upl_pdf_tmp/"+fileName;
            String url = "http://" + ip + "/Sync10/upl_pdf_tmp/" + fileName;
            System.out.println(url);

            if (dn != null) {
// BOL
                if (loadNo != null && entryId != null && customerId != null
                    && companyId != null && type != null && url != null
                    && !"".equals(url)) {
                    is = handlePDFByDnname(sessionId, URL, getURL, loadNo,
                        type, entryId, is, url, dn.trim(), customerId,
                        manual);
                }
            } else {
// MBOL
                if (loadNo != null
                    && !loadNo.equalsIgnoreCase("undefined")
                    && entryId != null
                    && !String.valueOf(entryId).equalsIgnoreCase(
                    "undefined") && customerId != null
                    && !customerId.equalsIgnoreCase("undefined")
                    && companyId != null
                    && !companyId.equalsIgnoreCase("undefined")
                    && type != null && !type.equalsIgnoreCase("undefined")) {
// 再生成所有的pdf
                    DBRow[] dns = floorSQLServerMgrZJ
                        .findOrderPonoInfosByLoadNoMasterBolNo(loadNo,
                            new Long[0], companyId, customerId, null);
                    if (dns != null && dns.length > 0) {
                        for (DBRow dbRow : dns) {
                            String dnname = dbRow.get("ReferenceNo", null);
                            DBRow drBundle = this.findOrderByBundle(
                                dbRow.getString("OrderNo"), companyId);
                            if (drBundle != null) {
                                dnname = drBundle.getString("ReferenceNo");
                            }
                            is = handlePDFByDnname(sessionId, URL, getURL,
                                loadNo, type, entryId, is, url,
                                dnname.trim(), customerId, manual);
                        }
                    }
                }
            }
            if (is) {
                message = "true";
                File ftTmpFile = new File(filePath);
                if (ftTmpFile.exists()) {
                    ftTmpFile.delete();
                }
            }
        } catch (Exception e) {
            throw new SystemException(e, "handleImageToPDF", log);
        }
        return message;
    }

    /**
     * 处理image转成PDF
     */
    @Override
    public String handleImageToPDF(HttpServletRequest request) throws Exception {
        String message = "false";
        try {
            String sessionId = request.getRequestedSessionId();
            String URL = uploadFileUrl;
            String getURL = downLoadFileUrl;

            String base64StringOfImage = StringUtil.getString(request,
                "imageBuffer", null);
            String loadNo = StringUtil.getString(request, "loadNo", null);
            String customerId = StringUtil.getString(request, "customerId",
                null);
            String companyId = StringUtil.getString(request, "companyId", null);
            String type = StringUtil.getString(request, "type", null);
            Long entryId = StringUtil.getLong(request, "entryId");

// 区分MBOL和BOL
            String dn = StringUtil.getString(request, "dn", null);

            boolean is = false;// 标示

// 创建目录
            File pdfFilePath = new File(PDFPath);
            if (!pdfFilePath.exists()) {
                pdfFilePath.mkdirs();
            }

// 转换图片流
            BufferedImage bi1 = null;
            if (base64StringOfImage != null) {
                byte[] bt = decoder.decodeBuffer(base64StringOfImage);
                ByteArrayInputStream bais = new ByteArrayInputStream(bt);
                bi1 = ImageIO.read(bais);
            }

            if (dn != null) {
// BOL
                if (loadNo != null && entryId != null && customerId != null
                    && companyId != null && type != null && bi1 != null) {
                    is = handlePDFByDnname(sessionId, URL, getURL, loadNo,
                        type, entryId, is, bi1, dn.trim(), customerId);
                }
            } else {
// MBOL
                if (loadNo != null
                    && !loadNo.equalsIgnoreCase("undefined")
                    && entryId != null
                    && !String.valueOf(entryId).equalsIgnoreCase(
                    "undefined") && customerId != null
                    && !customerId.equalsIgnoreCase("undefined")
                    && companyId != null
                    && !companyId.equalsIgnoreCase("undefined")
                    && type != null && !type.equalsIgnoreCase("undefined")
                    && bi1 != null
                    && !String.valueOf(bi1).equalsIgnoreCase("undefined")) {
// 再生成所有的pdf
                    DBRow[] dns = floorSQLServerMgrZJ
                        .findOrderPonoInfosByLoadNoMasterBolNo(loadNo,
                            new Long[0], companyId, customerId, null);
                    if (dns != null && dns.length > 0) {
                        for (DBRow dbRow : dns) {
                            String dnname = dbRow.get("ReferenceNo", null);
                            is = handlePDFByDnname(sessionId, URL, getURL,
                                loadNo, type, entryId, is, bi1,
                                dnname.trim(), customerId);
                        }
                    }
                }
            }
            if (is) {
                message = "true";
            }
        } catch (Exception e) {
            throw new SystemException(e, "handleImageToPDF", log);
        }
        return message;
    }

    // TODO 删除load下的所有的pdf
/*
* 删除load下的所有的pdf 1、 先删除文件服务器上的pdf 2、 然后再printed_label清表
*/
    public void deletPdfByLoad(HttpServletRequest request, long entryId,
                               String loadNo, String customerId) {

// 先删除load下的所有的pdf 1 先删除文件服务器上的pdf 2 然后再printed_label清表
        try {
            String DeleteURL = downLoadFileUrl;
            String sessionId = request.getRequestedSessionId();

            DBRow[] fileids = floorCheckInMgrZwb.selectPrintedLabel(entryId,
                String.valueOf(loadNo), customerId);
            if (fileids != null && fileids.length > 0) {
// 删除文件服务器上的文件
                for (int i = 0; i < fileids.length - 1; i++) {
                    DeleteURL = DeleteURL + fileids[i].getString("file_id")
                        + ",";
                }
                DeleteURL = DeleteURL
                    + fileids[fileids.length - 1].getString("file_id");
// 删除printed_label上的数据
// uploadAndDownloadPDF.deleteFile(DeleteURL, sessionId);
// floorCheckInMgrZwb.deletePrintedLabelByLoad(entryId,String.valueOf(loadNo),customerId);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private boolean handlePDFByDnname(String sessionId, String URL,
                                      String getURL, String loadNo, String type, Long entryId,
                                      boolean is, BufferedImage bi1, String dnname, String customerId)
        throws Exception, ClientProtocolException, IOException,
        DocumentException {
        String file_id;
        DBRow db;
        File file;
        if (dnname != null) {
            DBRow[] existInData = floorCheckInMgrZwb.selectCountPrintedLabel(
                entryId, null, dnname, customerId);
            if (existInData != null && existInData.length > 0) {
// 该pdf存在
                file_id = existInData[0].getString("file_id");
                if (file_id == null) {
                    for (DBRow fileid_db : existInData) {
                        file_id = fileid_db.getString("file_id");
                        if (file_id != null) {
                            break;
                        }
                    }
                }
                if (file_id != null) {
// 再查询一下--判断是否追加过
                    existInData = floorCheckInMgrZwb.selectCountPrintedLabel(
                        entryId, type, dnname, customerId);
                    if (existInData == null || existInData.length == 0) {
                        File serverFile = new File(PDFPath + dnname + ".pdf");
                        if (serverFile != null && serverFile.exists()) {
// 存在
// 先压缩图片
                            file = imgToPdfMgr.compressImg(PDFPath, bi1, 0.5f,
                                false);
// 追加内容
                            File returnfile = imgToPdfMgr.addContentToPDF(
                                serverFile.getAbsolutePath(),
                                ImageIO.read(new FileInputStream(file)),
                                false, false);
                            if (returnfile.exists()) {
                                is = true;
                            }
                        }
                    }
                }
            } else {
// 该pdf不存在--重新创建
                String subpdfpath = PDFPath + dnname + ".pdf";
// 先压缩图片

                file = imgToPdfMgr.compressImg(PDFPath, bi1, 0.5f, false);

                File returnfile = imgToPdfMgr.handleImage(
                    ImageIO.read(new FileInputStream(file)),
                    String.valueOf(entryId), subpdfpath);
                if (returnfile != null && returnfile.exists()) {
                    is = true;
                }
            }
// 如果操作成功插入数据库
            if (is) {
                String create_time = DateUtil.NowStr();
                db = new DBRow();
                db.add("load_no", loadNo);
                db.add("entry_id", entryId);
                db.add("type", type);
                db.add("dn", dnname);
                db.add("create_time", create_time);
                db.add("customer_id", customerId);
                floorCheckInMgrZwb.addPrintedLabel(db);
            }
        }
        return is;
    }

    private boolean handlePDFByDnname(String sessionId, String URL,
                                      String getURL, String loadNo, String type, Long entryId,
                                      boolean is, String contentUrl, String dnname, String customerId,
                                      boolean manual) throws Exception, ClientProtocolException,
        IOException, DocumentException {
        String file_id;
        DBRow db;
        File file;
        if (dnname != null) {
            DBRow[] existInData = floorCheckInMgrZwb.selectCountPrintedLabel(
                entryId, null, dnname, customerId);
            if (existInData != null && existInData.length > 0) {
// 该pdf存在
                file_id = existInData[0].getString("file_id");
                if (file_id == null) {
                    for (DBRow fileid_db : existInData) {
                        file_id = fileid_db.getString("file_id");
                        if (file_id != null) {
                            break;
                        }
                    }
                }
                if (file_id != null) {
// 再查询一下--判断是否追加过
                    boolean existed = false;
                    existInData = floorCheckInMgrZwb.selectCountPrintedLabel(
                        entryId, type, dnname, customerId);
                    if (existInData != null && existInData.length > 0) {
                        if (!manual) {// 如果不是手动则跳过已经存在的记录
                            existed = true;
                        } else { // 如果是手动生成则删除已经存在的记录
                            floorCheckInMgrZwb.deletePrintedLabelByTypeDname(
                                entryId, type, dnname, loadNo, customerId);
                        }
                    }

                    if (!existed) {
                        File serverFile = new File(PDFPath + dnname + ".pdf");
                        if (serverFile != null && serverFile.exists()) {
// 存在
// 先压缩图片
// file=imgToPdfMgr.compressImg(PDFPath, bi1, 0.5f,
// false);
// 追加内容
// File
// returnfile=imgToPdfMgr.addContentToPDF(serverFile.getAbsolutePath(),ImageIO.read(new
// FileInputStream(file)),false,false);
                            UUID uuid = UUID.randomUUID();
                            String tmpFileName = uuid.toString() + ".pdf";
                            String tmpFilePath = PDFPath + File.pathSeparator
                                + tmpFileName;
                            File returnfile = imgToPdfMgr.handleHtml(
                                contentUrl, tmpFilePath);
                            returnfile = imgToPdfMgr.addContentToPDF(
                                serverFile.getAbsolutePath(), tmpFilePath,
                                false, false);
                            if (returnfile.exists()) {
                                is = true;
                            }

                        } else {
                            String subpdfpath = PDFPath + dnname + ".pdf";
                            File returnfile = imgToPdfMgr.handleHtml(
                                contentUrl, subpdfpath);
                            if (returnfile != null && returnfile.exists()) {
                                is = true;
                            }
                        }
                    }
                }
            } else {
// 该pdf不存在--重新创建
                String subpdfpath = PDFPath + dnname + ".pdf";
// 先压缩图片

                File returnfile = imgToPdfMgr
                    .handleHtml(contentUrl, subpdfpath);
                if (returnfile != null && returnfile.exists()) {
                    is = true;
                }
            }
// 如果操作成功插入数据库
            if (is) {
                String create_time = DateUtil.NowStr();
                db = new DBRow();
                db.add("load_no", loadNo);
                db.add("entry_id", entryId);
                db.add("type", type);
                db.add("dn", dnname);
                db.add("create_time", create_time);
                db.add("customer_id", customerId);
                floorCheckInMgrZwb.addPrintedLabel(db);
            }
        }
        return is;
    }

    @Override
    public boolean isTaskScanPallet(long detail_id) throws Exception {
        return floorCheckInMgrZwb.isTaskScanPallet(detail_id);
    }

    @Override
    public DBRow[] getFileBy(long fileWithId, int fileWithType,
                             int fileWithClass) throws Exception {
        return floorFileMgrZr
            .getFileBy(fileWithId, fileWithType, fileWithClass);
    }

    /**
     * 根据 设备id 查询设备详细信息 zwb
     *
     * @param equipment_id
     * @return
     * @throws Exception
     */
    public DBRow getEquipmentByEquipmentId(long equipment_id) throws Exception {
        try {
            return this.floorEquipmentMgr
                .getEquipmentByEquipmentId(equipment_id);
        } catch (Exception e) {
            throw new SystemException(e, "getEquipmentByEquipmentId", log);
        }
    }

    /**
     * 判断是否显示Time keeper 信息。a4print.jsp签使用 accountId等于BESTBUY、BEST
     * BUY、BESTBUY-CAN 并且carrierId等于ABF FREIGHT，ABFL，ABFT，ABFF 显示Time keeper
     */
// TODO
    public boolean showTimeKeeper(String accountId, String carrierId) {
        boolean flag = false;
        if (!StrUtil.isBlank(accountId) && !StrUtil.isBlank(carrierId)) {
            accountId = accountId.toUpperCase();
            carrierId = carrierId.toUpperCase();

            List<String> accountList = new LinkedList<String>();
            accountList.add("BESTBUY");
            accountList.add("BEST BUY");
            accountList.add("BESTBUY-CAN");
            List<String> carrierList = new LinkedList<String>();
            carrierList.add("ABF FREIGHT");
            carrierList.add("ABFL");
            carrierList.add("ABFT");
            carrierList.add("ABFF");
            if (accountList.contains(accountId)
                && carrierList.contains(carrierId)) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 安卓打印Tlp标签
     *
     * @param PalletIds
     * @return
     * @throws Exception
     */
    public DBRow[] queryPalletsByLineAndId(String PalletIds) throws Exception {
        try {
            String PalletId = PalletIds;
            DBRow[] rows = floorCheckInMgrZwb
                .queryPalletsByLineAndId(PalletIds);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "queryPalletsByLineAndId", log);
        }
    }

    /**
     * android 收货 tn label 2015年4月3日
     *
     * @param receipt_no
     * @param company_id
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] queryLinesByRNForTicketPrint(long receipt_no,
                                                String company_id) throws Exception {
        try {
            DBRow[] rows = floorCheckInMgrZwb.queryLinesByRNForTicketPrint(
                receipt_no, company_id);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e, "queryLinesByRNForTicketPrint", log);
        }
    }

    /**
     * android 收货 tn label 2015年4月3日
     *
     * @param receipt_line_id
     * @return
     * @throws Exception
     */
    @Override
    public DBRow[] queryPalletsInfoByLineIdForTicketPrint(long receipt_line_id)
        throws Exception {
        try {
            DBRow[] rows = floorCheckInMgrZwb
                .queryPalletsInfoByLineIdForTicketPrint(receipt_line_id);
            return rows;
        } catch (Exception e) {
            throw new SystemException(e,
                "queryPalletsInfoByLineIdForTicketPrint", log);
        }
    }

    /**
     * 组合counting sheet页面的显示的数据 a4_counting_sheet.jsp页面使用
     *
     * @author gql
     */
    public DBRow[] getOrderItemRowOfCountingSheet(String loadNo,
                                                  String companyId, String customerId, long adid,
                                                  HttpServletRequest request) throws Exception {
        try {
            ArrayList<DBRow> list = new ArrayList<DBRow>();
            DBRow[] loadOrderNos = null;
            if (!StringUtil.isBlank(loadNo)) {
                String[] status = null;
                loadOrderNos = floorSQLServerMgrZJ.findMasterBolLinesByLoadNo(
                    loadNo, companyId, customerId, status);// 根据load查询order信息
                for (int k = 0; k < loadOrderNos.length; k++) {
                    long orderNo = loadOrderNos[k].get("OrderNo", 0L);
                    String refNo = loadOrderNos[k].get("ReferenceNo", "");
                    String companyIdOr = loadOrderNos[k].getString("CompanyID");
                    DBRow[] orderItemRows = floorSQLServerMgrZJ
                        .findOrderItemsInfoForCountingByOrderNo(orderNo,
                            companyIdOr, status);// 获取order的pallet信息
                    ArrayList<DBRow> temp = new ArrayList<DBRow>();

                    for (int i = 0; i < orderItemRows.length; i++) {
                        double palletCount = orderItemRows[i].get(
                            "palletCount", 0d);// 获取item的pallet数
                        int palletCountToInt = MoneyUtil
                            .formatDoubleUpInt(palletCount);// 得到接近的较大值（向上取整）
                        int orderLineCase = orderItemRows[i].get(
                            "orderLineCase", 0);// 获取item的orderLineCase数

                        DBRow row = orderItemRows[i];
                        row.add("orderno", orderNo);
                        row.add("refNo", refNo);
/*
* 拆分pallet：将一个拥有超过一个的pallet拆分为，多条只有一个pallet的数据
* 1、拆分的row的palletCount设为1.
* 2、最后一个orderLineCase的值为原来的值，其余拆分的row的orderLineCase值为0。
*/
                        if (palletCountToInt > 1) {
                            row.add("palletCount", 1);// 设置palletCount为1.
                            row.add("orderLineCase", 0);// 设置orderLineCase值为0。
                            for (int palletNum = palletCountToInt; palletNum > 0; palletNum--) {

                                if (palletNum == 1) {
                                    DBRow r = new DBRow();
                                    r.add("orderno", orderNo);
                                    r.add("refNo", refNo);
                                    r.add("palletCount", 1);
                                    r.add("orderLineCase", orderLineCase);// 最后一个orderLineCase的值为原来的值
                                    temp.add(r);
                                } else {
                                    temp.add(row);
                                }
                            }
                        } else {
                            temp.add(row);
                        }
                    }

// ArrayList<DBRow> temp = new
// ArrayList<DBRow>(Arrays.asList(orderItemRows));
                    list.addAll(temp);
                }
            }
            return (DBRow[]) list.toArray(new DBRow[list.size()]);
        } catch (Exception e) {
            throw new SystemException(e,
                "queryPalletsInfoByLineIdForTicketPrint", log);
        }
    }

    public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
        this.sqlServerMgrZJ = sqlServerMgrZJ;
    }

    public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
        this.floorCheckInMgrZwb = floorCheckInMgrZwb;
    }

    public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr) {
        this.floorFileMgrZr = floorFileMgrZr;
    }

    public void setSystemConfig(SystemConfigIFace systemConfig) {
        this.systemConfig = systemConfig;
    }

    public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
        this.scheduleMgrZr = scheduleMgrZr;
    }

    public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
        this.adminMgrZJ = adminMgrZJ;
    }

    public void setFloorWaybillMgrZwb(FloorWaybillMgrZwb floorWaybillMgrZwb) {
        this.floorWaybillMgrZwb = floorWaybillMgrZwb;
    }

    public void setSqlServerTransactionTemplate(
        TransactionTemplate sqlServerTransactionTemplate) {
        this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
    }

    public void setFloorSQLServerMgrZJ(FloorSQLServerMgrZJ floorSQLServerMgrZJ) {
        this.floorSQLServerMgrZJ = floorSQLServerMgrZJ;
    }

    public void setWmsLoadMgrZr(WmsLoadMgrZrIface wmsLoadMgrZr) {
        this.wmsLoadMgrZr = wmsLoadMgrZr;
    }

    public void setLoadPaperWorkMgrZr(LoadPaperWorkMgrIfaceZr loadPaperWorkMgrZr) {
        this.loadPaperWorkMgrZr = loadPaperWorkMgrZr;
    }

    @Override
    public DBRow[] loadSearchEntryDetail(long entry_id, String door_name)
        throws Exception {
// TODO Auto-generated method stub
        return null;
    }

    @Override
    public DBRow[] findPOTypeOfDN(Long ps_id) throws Exception {
        DBRow[] rows = null;
        DBRow[] companies = floorReportDataInterfaceMgrCc
            .getCompanyIdByPsId(ps_id);
        String companyIds = "";

        if (companies != null && companies.length > 0) {
            for (int i = 0; i < companies.length; i++) {
                DBRow row = companies[i];
                String spit = companyIds.equals("") ? "" : ",";
                String companyId = row.getString("company_id");
                companyIds += spit + "'" + companyId + "'";
            }

        }
        rows = sqlServerMgrZJ.findPOTypeOfDN(companyIds);
        return rows;
    }

    public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
        this.checkInMgrZyj = checkInMgrZyj;
    }

    public void setStorageDoorMgrZr(StorageDoorMgrIfaceZr storageDoorMgrZr) {
        this.storageDoorMgrZr = storageDoorMgrZr;
    }

    public void setStorageYardControlZr(
        StorageYardControlIfaceZr storageYardControlZr) {
        this.storageYardControlZr = storageYardControlZr;
    }

    public void setFloorSpaceResourcesRelationMgr(
        FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
        this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
    }

    public void setFloorEquipmentMgr(FloorEquipmentMgr floorEquipmentMgr) {
        this.floorEquipmentMgr = floorEquipmentMgr;
    }

    public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
        this.ymsMgrAPI = ymsMgrAPI;
    }

    public void setAdminMgr(AdminMgr adminMgr) {
        this.adminMgr = adminMgr;
    }

    public void setOrderSystemMgr(OrderSystemMgrIFace orderSystemMgr) {
        this.orderSystemMgr = orderSystemMgr;
    }

    public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
        this.floorCheckInMgrWfh = floorCheckInMgrWfh;
    }

    public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
        this.fileMgrZr = fileMgrZr;
    }

    public void setImgToPdfMgr(ImgToPdfMgr imgToPdfMgr) {
        this.imgToPdfMgr = imgToPdfMgr;
    }

  

    public void setDecoder(BASE64Decoder decoder) {
        this.decoder = decoder;
    }

    public void setDownLoadFileUrl(String downLoadFileUrl) {
        this.downLoadFileUrl = downLoadFileUrl;
    }

    public void setUploadFileUrl(String uploadFileUrl) {
        this.uploadFileUrl = uploadFileUrl;
    }

    public void setFloorRelativeFileMgrZr(
        FloorRelativeFileMgrZr floorRelativeFileMgrZr) {
        this.floorRelativeFileMgrZr = floorRelativeFileMgrZr;
    }

    public DBRow[] findOrderLineByBundle(String comanyId, DBRow[] lines)
        throws Exception {
        DBRow[] orderItems = null;
        List<DBRow> listBundle = new ArrayList<DBRow>();
        List<String> orderList = new ArrayList<String>();
        HashMap<String, DBRow> map = new HashMap<String, DBRow>();
        try {
            if (lines == null || lines.length == 0) {
                return lines;
            }
            String orderNos = "";
            for (int i = 0; i < lines.length; i++) {
                DBRow line = lines[i];
                String orderNo = line.getString("OrderNo");
                String spit = orderNos.equals("") ? "" : ",";
                orderNos += spit + "'" + orderNo + "'";
            }
            DBRow[] rows = floorCheckInMgrZwb.findOrderLineByBundle(comanyId,
                orderNos);
            if (rows == null || rows.length == 0) {
                return lines;
            }

            for (int i = 0; i < rows.length; i++) {
                DBRow row = rows[i];
                String orderNo = row.getString("order_no");
                row.add("CommodityDescription",
                    row.getString("customer_material"));
                row.add("CommodityDescription2",
                    row.getString("customer_material"));
                String strShippedQty = row.getString("shipped_qty");
                if (strShippedQty != null && !strShippedQty.equals("")) {
                    row.add("orderLineCase",
                        (int) Float.parseFloat(strShippedQty));
                    row.add("OrderedQty", (int) Float.parseFloat(strShippedQty));
                }
                String strWeight = row.getString("total_weight");
                if (strWeight != null && !strWeight.equals("")) {
                    row.add("weightItemAndPallet",
                        (int) Float.parseFloat(strWeight));
                }

                String strPalletCount = row.getString("total_pallets");
                if (strPalletCount != null && !strPalletCount.equals("")) {
                    row.add("palletCount",
                        (int) Float.parseFloat(strPalletCount));
                }
                listBundle.add(row);
                orderList.add(orderNo);
            }

            for (int i = 0; i < lines.length; i++) {
                DBRow line = lines[i];
                DBRow row = null;
                double palletCount = line.get("palletCount", 0d);
                int caseQty = line.get("orderLineCase", 0);
                int weightLine = line.get("weightItemAndPallet", 0);

                if (!orderList.contains(line.getString("OrderNo"))) {
                    String itemId = line.getString("ItemID");
                    if (map.containsKey(itemId)) {
                        row = map.get(itemId);
                        row.add("palletCount", row.get("palletCount", 0d)
                            + palletCount);
                        row.add("orderLineCase", row.get("orderLineCase", 0)
                            + caseQty);
                        row.add("weightItemAndPallet",
                            row.get("weightItemAndPallet", 0) + weightLine);
                    } else {
                        row = line;
                        map.put(itemId, row);
                    }
// listBundle.add(line);
                }
            }
            Object[] keys = map.keySet().toArray();
            for (int i = 0; i < keys.length; i++) {
                listBundle.add(map.get(keys[i]));
            }
            orderItems = new DBRow[listBundle.size()];
            for (int i = 0; i < listBundle.size(); i++) {
                DBRow line = listBundle.get(i);

                orderItems[i] = line;

            }

        } catch (Exception e) {
            throw new SystemException(e, "findloadIngByInfoId", log);
        }
        return orderItems;

    }


    public DBRow findOrderByBundle(String comanyId, DBRow line)
        throws Exception {
        DBRow row = null;
        DBRow[] rows = new DBRow[]{line};
        rows = this.findOrderByBundle(comanyId, rows);
        if (rows != null && rows.length > 0) {
            row = rows[0];
        }

        return row;
    }

    public DBRow findOrderByBundle(String orderNo, String companyId)
        throws Exception {
        return floorCheckInMgrZwb.findOrderByBundle(companyId, orderNo);
    }

    public DBRow[] findOrderByBundle(String companyId, DBRow[] lines)
        throws Exception {
        DBRow[] orderItems = null;
        HashMap<String, DBRow> map = new HashMap<String, DBRow>();
        HashMap<String, String> map1 = new HashMap<String, String>();
        HashMap<String, String> map2 = new HashMap<String, String>();
        HashMap<String, String> map3 = new HashMap<String, String>();
        HashMap<String, String> map4 = new HashMap<String, String>();
        List<DBRow> listBundle = new ArrayList<DBRow>();
        try {
            String orderNos = "";

            for (int i = 0; i < lines.length; i++) {
                DBRow line = lines[i];
                String orderNo = line.getString("OrderNo");
                String spit = orderNos.equals("") ? "" : ",";
                orderNos += spit + "'" + orderNo + "'";
                map1.put(orderNo, line.getString("ReferenceNo"));
                map2.put(orderNo, line.getString("PONo"));
                map3.put(orderNo, line.getString("DeptNo"));
                map4.put(orderNo, line.getString("ShipToStoreNo"));

            }
            DBRow[] rows = floorCheckInMgrZwb.findOrderLineByBundle(companyId,
                orderNos);

            if (rows == null || rows.length == 0) {

                return lines;
            }

            for (int i = 0; i < rows.length; i++) {
                DBRow line = null;
                DBRow row = rows[i];
                String orderNo = row.getString("order_no");
                int totalShippedQty = 0;
                int totalWeight = 0;
                String strShippedQty = row.getString("shipped_qty");
                if (strShippedQty != null && !strShippedQty.equals("")) {
                    totalShippedQty = (int) Float.parseFloat(strShippedQty);
                }
                String strWeight = row.getString("total_weight");
                if (strWeight != null && !strWeight.equals("")) {
                    totalWeight = (int) Float.parseFloat(strWeight);
                }
                if (map.containsKey(orderNo)) {
                    line = map.get(orderNo);
                    int shippedQty = line.get("orderLineCaseSum", 0);
                    int weight = line.get("weightItemAndPallet", 0);
                    totalShippedQty = totalShippedQty + shippedQty;
                    totalWeight = totalWeight + weight;
                } else {
                    line = row;
                    line.add("CommodityDescription",
                        row.getString("customer_material"));
                    line.add("CommodityDescription2",
                        row.getString("customer_material"));
// line.add("ReferenceNo", map1.get(orderNo));
                    line.add("PONo", map2.get(orderNo));
                    line.add("DeptNo", map3.get(orderNo));
                    line.add("ShipToStoreNo", map4.get(orderNo));

                }
                line.add("orderLineCaseSum", totalShippedQty);
                line.add("weightItemAndPallet", totalWeight);
                map.put(orderNo, line);

            }

            for (Map.Entry<String, DBRow> MapEnt : map.entrySet()) {
                DBRow row = map.get(MapEnt.getKey());
                listBundle.add(row);
            }

            for (int i = 0; i < lines.length; i++) {
                DBRow line = lines[i];
                if (!map.containsKey(line.getString("OrderNo"))) {
                    listBundle.add(line);
                }
            }
            orderItems = new DBRow[listBundle.size()];
            for (int i = 0; i < listBundle.size(); i++) {
                DBRow line = listBundle.get(i);
                orderItems[i] = line;

            }

        } catch (Exception e) {
            throw new SystemException(e, "findloadIngByInfoId", log);
        }
        return orderItems;
    }
    
    @Override
	public void addYmsCheckOutRequest(String entryId,long psId,String json) throws Exception {
		DBRow row = new DBRow();
		row.put("entryId", entryId);
		row.put("type", "checkout");
		row.put("state", "pending");
		row.put("psId", psId);
		row.put("req_entity", json);
		floorCheckInMgrZwb.addYmsCheckOutRequest(row);
	}
	
	@Override
	public void addYmsCheckInRequest(String entryId,long psId,String json) throws Exception {
		DBRow row = new DBRow();
		row.put("entryId", entryId);
		row.put("type", "checkin");
		row.put("state", "pending");
		row.put("psId", psId);
		row.put("req_entity", json);
		floorCheckInMgrZwb.addYmsCheckOutRequest(row);
	}
	
	private String getSaftValue(String key, net.sf.json.JSONObject json){
		String value = null;
		if(json.containsKey(key)){
			value = json.getString(key);
		}
		return value;
	}
	@Override
	public List<GateCheckoutInfo> getYmsCheckOutRequests(String status) throws Exception {
		List<GateCheckoutInfo> gateCheckoutInfos = new ArrayList<>();
		DBRow[] rows = floorCheckInMgrZwb.getYmsCheckOutRequests();
		if(rows!=null){
			for (int i=0 ; i < rows.length; i++){
				String strJson = rows[i].getString("req_entity");
				net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(strJson);
				GateCheckoutInfo gateCheckoutInfo = new GateCheckoutInfo();
				
				gateCheckoutInfo.equipments = new ArrayList<Equipment>();
				gateCheckoutInfo.entryId = getSaftValue("entryId", json);
				gateCheckoutInfo.warehouse = getSaftValue("warehouse", json);
				if(json.containsKey("checkOutDriver")){
					net.sf.json.JSONObject checkOutDriver = json.getJSONObject("checkOutDriver");
					gateCheckoutInfo.driverLicense  = getSaftValue("driverLicense", checkOutDriver);
					gateCheckoutInfo.driverName  = getSaftValue("driverName", checkOutDriver);
					gateCheckoutInfo.mcDot  = getSaftValue("mcDot", checkOutDriver);
					gateCheckoutInfo.tractor = getSaftValue("driverLicense", checkOutDriver);
				} 
				if(json.containsKey("equipments")){
					net.sf.json.JSONArray  equipments = json.getJSONArray("equipments");
					for(int j = 0;j<equipments.size();j++){
						Equipment equipment = new Equipment();
						equipment.equipmentNo = getSaftValue("equipmentNo",equipments.getJSONObject(j));
						String equipmentType = getSaftValue("type", equipments.getJSONObject(j)).toLowerCase();
						if("chassis".equals(equipmentType)){
							continue;
						}
						if("tractor".equals(equipmentType)){
							equipment.equipmentType = 1;
						}
						else {
							equipment.equipmentType = 2;
						}
						if(gateCheckoutInfo.checkOutTime == null || "".equals(gateCheckoutInfo.checkOutTime)){
							gateCheckoutInfo.checkOutTime =  getSaftValue("checkOutTime",equipments.getJSONObject(j));
						}
						equipment.checkInEntry = getSaftValue("checkInEntry",equipments.getJSONObject(j));
						System.out.println("=======equipment.checkInEntry:"+equipment.checkInEntry+"==========");
						gateCheckoutInfo.equipments.add(equipment);
					}
				}
				gateCheckoutInfos.add(gateCheckoutInfo);
			}
		}
		return gateCheckoutInfos;
	}
	
	@Override
	public List<EntryTicketCheck> getYmsCheckinRequests(String status) throws Exception {
		List<EntryTicketCheck> entryTicketChecks = new ArrayList<>();
		DBRow[] rows = floorCheckInMgrZwb.getYmsCheckinRequests("checkin",status);
		if(rows!=null){
			for (int i=0 ; i < rows.length; i++){
				byte[] bytes = (byte[])rows[i].getValue("req_entity");
				EntryTicketCheck info = (EntryTicketCheck)SerializationUtils.deserialize(bytes);
				entryTicketChecks.add(info);
			}
		}
		return entryTicketChecks;
	}
	
	@Override
	public EntryTicketCheck getYmsEntryTicket(String entryId,long psId) throws Exception {
		EntryTicketCheck entryTicketCheck = null;
		DBRow row = floorCheckInMgrZwb.getYmsCheckinRequest(entryId, psId,"checkin", "pending");
		if(row!=null){
			entryTicketCheck = new EntryTicketCheck();
			String strJson = row.getString("req_entity");
			net.sf.json.JSONObject json = net.sf.json.JSONObject.fromObject(strJson);
		    entryTicketCheck.entryId = getSaftValue("entryId", json);
	        entryTicketCheck.warehouse  = getSaftValue("warehouse", json);
	        entryTicketCheck.photoIds = new ArrayList<Integer>();
	        if(json.containsKey("entryCheckin")){
	        	net.sf.json.JSONObject entryCheckin = json.getJSONObject("entryCheckin");
	        	entryTicketCheck.driverName = getSaftValue("driverName", entryCheckin);
	 	        entryTicketCheck.driverLicense = getSaftValue("driverLicense", entryCheckin);
	 	        entryTicketCheck.mcDot = getSaftValue("mcDot", entryCheckin);
	 	        entryTicketCheck.carrierName = getSaftValue("carrierName", entryCheckin);
	 	        entryTicketCheck.tractor = getSaftValue("tractor", entryCheckin);
	 	        
	 	        String actionType = getSaftValue("actionType", entryCheckin);
	 	        if(actionType!=null){
	 	        	if(actionType.equals("Load")){
	 	        		entryTicketCheck.actionType = CheckinActionType.DELIVERY;
	 	        	}
	 	        	if(actionType.equals("Delivery")){
	 	        		entryTicketCheck.actionType = CheckinActionType.DELIVERY;
	 	        	}
	 	        	else if(actionType.equals("Pickup Container")){
	 	        		entryTicketCheck.actionType = CheckinActionType.PICK_UP;
	 	        	}
	 	        	else if(actionType.equals("Drop Off Container")){
	 	        		entryTicketCheck.actionType = CheckinActionType.DELIVERY;
	 	        	}
	 	        	else if(actionType.equals("Parking")){
	 	        		entryTicketCheck.actionType = CheckinActionType.VISITOR;
	 	        	}
	 	        	else if(actionType.equals("Visitor")){
	 	        		entryTicketCheck.actionType = CheckinActionType.VISITOR;
	 	        	}
	 	        	else if(actionType.equals("Other")){
	 	        		entryTicketCheck.actionType = CheckinActionType.NONE;
	 	        	}
	 	        	
	 	        }
	        }
	        if(json.containsKey("equipments")){
	        	net.sf.json.JSONArray array =  json.getJSONArray("equipments");
	        	for(int i = 0 ;i<array.size();i++){
	        		net.sf.json.JSONObject equipment = array.getJSONObject(i);
	        		if("tractor".equals(getSaftValue("type", equipment).toLowerCase())){
	        			net.sf.json.JSONObject currentLocationInfo = equipment.getJSONObject("currentLocationInfo");
	        			if(currentLocationInfo!=null){
	        				String type = getSaftValue("type", currentLocationInfo);
	        				if("SPOT".equals(type)){
	        					entryTicketCheck.spotName = getSaftValue("name", currentLocationInfo);
	        				}
	        				else if("DOCK".equals(type)){
	        					entryTicketCheck.dockName= getSaftValue("name", currentLocationInfo);
	        				}
	        			}
	        		}
	        		else if(!"chassis".equals(getSaftValue("type", equipment).toLowerCase())){
	        			  entryTicketCheck.seal = getSaftValue("sealNo", equipment);
	        			  List<String> containers = new ArrayList<>();
	        		      containers.add(getSaftValue("equipmentNo", equipment));
	        		      entryTicketCheck.containerNOs = containers; 
	        		}
	        		/*else if("Trailer".equals(getSaftValue("type", equipment))){
	        			  entryTicketCheck.seal = getSaftValue("sealNo", equipment);
	        			  List<String> containers = new ArrayList<>();
	        		      containers.add(getSaftValue("equipmentNo", equipment));
	        		      entryTicketCheck.containerNOs = containers; 
	        		}*/
	        		
	        		if(equipment.containsKey("photoIds")){
        				net.sf.json.JSONArray photoIds =  equipment.getJSONArray("photoIds");
	        			for(int j =0;j<photoIds.size();j++){
	        				if(photoIds.get(j)!=null && !"".equals(photoIds.get(j))){
	        					entryTicketCheck.photoIds.add(Integer.parseInt(photoIds.get(j).toString()));
	        				}
	        			}

        			}
	        		if(entryTicketCheck.checkInTime == null || "".equals(entryTicketCheck.checkInTime)){
	        			entryTicketCheck.checkInTime = getSaftValue("checkInTime", equipment);
	        		}
        			
	        	}
	        }
			/*byte[] bytes = (byte[])row.getValue("req_entity");
			info = (EntryTicketCheck)SerializationUtils.deserialize(bytes);*/
		}
		return entryTicketCheck;
	}
	
	
    public DBRow ymsGateCheckInAdd(final HttpServletRequest request, final DBRow row, final AdminLoginBean adminLoginBean) throws Exception, RuntimeException {
        try {
            gateCheckMain = null;
            sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(TransactionStatus txStat) {

                        try {
                            long yc_id = row.get("yc_id", 0l);
                            long doorId = row.get("checkInDoorId", 0l);
                            int rel_type = row.get("rel_type", 0);

                            long main_id = row.get("main_id", 0l);
                            String add_checkin_number = row.get("add_checkin_number", "").toUpperCase();
                            String company_name = row.get("company_name","").toUpperCase();
                            String gate_container_no = row.get("gate_container_no", "");
                            int creatFlag = row.get("creatFlag", 0); // 当验证海运集装出错时，继续做check
                            if (Pattern.matches("[A-Za-z]{4}\\d{7}$", gate_container_no) && creatFlag != 1) {
                                String result = getCntr(gate_container_no);
                                if (!result.equals("success")) {
                                    throw new VerifyShippingCTNRException(result);
                                }
                            }

                            if (rel_type == CheckInMainDocumentsRelTypeKey.SMALL_PARCEL && add_checkin_number.equals("")) {
                                add_checkin_number = "";
                                if (Pattern.matches("^UPS.*", company_name)) {
                                    add_checkin_number += "UPS";
                                } else if (Pattern.matches("^FEDEX.*",
                                    company_name)) {
                                    add_checkin_number += "FEDEX";
                                }

                                String nowTime = DateUtil.NowStr();
                                String year = DateUtil.getYear(nowTime)+ "";
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String month = DateUtil.getMonth(sdf.parse(nowTime)) + "";
                                String day = DateUtil.getMonthDay(nowTime)+ "";
                                String hour = DateUtil.getDayHour(nowTime)+ "";
                                String min = DateUtil.getMinute(sdf.parse(nowTime)) + "";
                                String sec = DateUtil.getSecond(nowTime)+ "";
                                add_checkin_number += year.substring(2, 4)
                                    + (month.length() == 1 ? "0"+ month : month)+ (day.length() == 1 ? "0" + day: day)
                                    + (hour.length() == 1 ? "0" + hour: hour)
                                    + (min.length() == 1 ? "0" + min: min)
                                    + (sec.length() == 1 ? "0" + sec: sec);

                                row.add("number_type",ModuleKey.SMALL_PARCEL);
                                row.add("add_checkin_number",add_checkin_number);
                                row.add("order_system_type",OrderSystemTypeKey.SYNC);
                                row.add("num", 0);
                            }

                            int resources_type = 0;
                            long resources_id = 0l;
                            if (yc_id > 0) {
                                resources_type = OccupyTypeKey.SPOT;
                                resources_id = yc_id;
                            }
                            if (doorId > 0) {
                                resources_type = OccupyTypeKey.DOOR;
                                resources_id = doorId;
                            }
                            
                            if ( resources_id > 0 && !ymsMgrAPI.resourceJudgeCanUse(ModuleKey.CHECK_IN, main_id, resources_type, resources_id)){
                            	DBRow[] spots = floorSpaceResourcesRelationMgr.getSpaceResourceUse(2,resources_id);
									for (int i = 0; i < spots.length; i++) {
										floorSpaceResourcesRelationMgr.delSpaceResourcesRelation(spots[i].get("srr_id", 0L));
									}
                            }

                            if ((CheckInMainDocumentsRelTypeKey.PICK_UP == row .get("rel_type", 0)) 
                            		&& !StringUtil.isBlank(add_checkin_number)) {
                                row.add("load_count", 1);
                            }
                            if (main_id == 0) {
                                main_id = gateCheckInAddData(request, row, adminLoginBean, false);
                                row.add("main_id", main_id);
                                gateCheckInAddEquipment(row, adminLoginBean, false);
                                if (row.get("num", 0) != -1&& !StrUtil .isBlank(add_checkin_number)) {
                                    gateCheckInAddTask(row, adminLoginBean, false);
                                }
                                //int photoId = row.get("photoId", 0);
                                if(row.get("photoIds")!=null){
                                	
                                	String sessionId = request.getSession().getId();
                                    List<Integer>photoIds = (List<Integer>)row.get("photoIds");
                                    for (int j=0;j<photoIds.size();j++){
                                    	
                                    	String savePath= Environment.getHome() + "upl_imags_tmp/"+ photoIds.get(j) + ".png";
                                    	System.out.println("==========save path : "+savePath+"=============");
                                        File file = null;
                                        if(ymsFileDownLoadUrl.indexOf("https")!=-1){
                                        	file = FileUtil.downloadFromSShFileServ(photoIds.get(j), ymsFileDownLoadUrl,savePath);
                                        }
                                        else{
                                        	FileUtil.downloadFromFileServ(photoIds.get(j), ymsFileDownLoadUrl,savePath);
                                        }
                                        if(file!=null){
                                        	String fileId = UploadAndDownloadPDF.upLoadPDF(checkinUploadFileUrl, file.getAbsolutePath(), sessionId, 0);
                                            if(fileId!=null && !"".equals(fileId)){
                                            	addFile("gate_check_in",Long.parseLong(fileId), main_id, FileWithTypeKey.OCCUPANCY_MAIN, FileWithCheckInClassKey.PhotoGateCheckIn, adminLoginBean.getAdgid());
                                            }
                                        }
                                    }
                                    
                                }
                                
                                editCheckInIndex(main_id, "add");
                            } 
                            if (main_id > 0) {
                                gateCheckMain = new DBRow();
                                gateCheckMain.add("entry_id", main_id);
                            }
                            gateCheckMain.add("flag", GateCheckInAddSuccess);
                            updateReceiptsByCtnOrBols(main_id);
                            
                            DBRow drRequest = new DBRow();
                            drRequest.put("entryId", row.getString("yms_entry_id"));
                            drRequest.put("state", "closed");
                            drRequest.put("psId", adminLoginBean.getPs_id());
                            floorCheckInMgrZwb.closeYmsCheckInRequest(drRequest);
                            
                        } catch (ResourceHasUsedException e) {
                            throw new ResourceHasUsedException(e .getMessage());
                        } catch (VerifyShippingCTNRException e) {
                            throw e;
                        } catch (Exception e2) {
                        	e2.printStackTrace();
                            throw new RuntimeException(e2);
                        }
                    }

                });
            return gateCheckMain;

        } catch (ResourceHasUsedException e) {
            throw new ResourceHasUsedException(e.getMessage());
        } catch (VerifyShippingCTNRException e) {
            throw new VerifyShippingCTNRException(e.getMessage());
        } catch (Exception e2) {
        	e2.printStackTrace();
            throw new SystemException(e2, "GateCheckInAdd", log);
        }

    }


    
    
}
