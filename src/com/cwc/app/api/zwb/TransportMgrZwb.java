package com.cwc.app.api.zwb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zwb.FloorTransportMgrZwb;
import com.cwc.app.floor.api.zwb.FloorWaybillMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.OutboundOrderMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zwb.TransportMgrIfaceZwb;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.fr.chart.Title;

public class TransportMgrZwb implements TransportMgrIfaceZwb {

	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrZwb floorTransportMgrZwb;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private AdminMgrIFace adminMgr;
	private TransportMgrIFaceZJ transportMgrZJ;
	private FloorWaybillMgrZwb floorWaybillMgrZwb;
	private OutboundOrderMgrIFaceZJ outboundOrderMgrZJ;
	
	
	//根据转运单id查 关联的采购单id
	@Override
	public DBRow getTransport(long transportId) throws Exception {
		
		return this.floorTransportMgrZwb.getTransport(transportId);
	}
	
	public DBRow[] getTransferlogs(long transferId)throws Exception{
		try{
			return this.floorTransportMgrZwb.getTransferlogs(transferId);
		}catch(Exception e){
			throw new SystemException(e,"getTransferlogs",log);
		}
	}
	public DBRow[] getTransportProductByName(long transport_id,String name)throws Exception{
		try{
			return this.floorTransportMgrZwb.getTransportProductByName(transport_id,name);
		}catch(Exception e){
			throw new SystemException(e,"getTransportProductByName",log);
		}
	}
	//根据转运单号 查出该转运单下的商品
	public DBRow[] getTransportProduct(long transportId) throws Exception{
		return this.floorTransportMgrZwb.getTransportProduct(transportId);
	}
	public long addTransferLog(HttpServletRequest request)throws Exception{
		try{
			long association_type_id=StringUtil.getLong(request,"association_type_id");
			long association_id=StringUtil.getLong(request,"association_id");
			long apply_money_id=StringUtil.getLong(request,"apply_money_id");
			long transfer_id	= StringUtil.getLong(request,"apply_transfer_id");
			int categoryId		= StringUtil.getInt(request, "categoryId");
			int apply_money_status = StringUtil.getInt(request, "apply_money_status");//资金的状态，如果资金完成，任务完成
		    String context		= StringUtil.getString(request, "context");
		    String time =StringUtil.getString(request,"eta");
		    HttpSession session = request.getSession();
		    AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
		    DBRow row=new DBRow();
			row.add("transfer_id", transfer_id);
			row.add("user_name", adminLoggerBean.getEmploye_name());
			row.add("user_name_id",adminLoggerBean.getAdid() );
			row.add("context", context);
			row.add("createDate",DateUtil.NowStr());	
			String followupContent = "";
			boolean isFinish = false;
			//采购单定金
			if(association_type_id==4 && 100009 == categoryId){
				followupContent = adminLoggerBean.getEmploye_name()+"确认了资金[F"+apply_money_id
								+"]的转账申请[W"+transfer_id+"],关联"
								+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(association_type_id+"")
								+"["+association_id+"]";
				if(FundStatusKey.FINISH_PAYMENT == apply_money_status){
			    	isFinish = true;
				}
				scheduleMgrZr.addScheduleReplayExternal(apply_money_id, ModuleKey.APPLY_MONEY, ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS, followupContent, isFinish, request, "");
			}
			//交货单货款
			else if(association_type_id==5 && 100001 == categoryId){
				followupContent = adminLoggerBean.getEmploye_name()+"确认了资金[F"+apply_money_id
				+"]的转账申请[W"+transfer_id+"],关联"
				+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(association_type_id+"")
				+"["+association_id+"]";
				if(FundStatusKey.FINISH_PAYMENT == apply_money_status){
			    	isFinish = true;
				}
				scheduleMgrZr.addScheduleReplayExternal(apply_money_id, ModuleKey.APPLY_MONEY, ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS, followupContent, isFinish, request, "");
			}
			//交货单或者转运单的货款，任务取决于其运费所对应的交货单或转运单的运费状态
			else if((5 == association_type_id || 6 == association_type_id) && 10015 == categoryId)
			{
				DBRow transportRow		= transportMgrZJ.getDetailTransportById(association_id);
		    	int stock_in_set		= null==transportRow?0:transportRow.get("stock_in_set", 0);
		    	isFinish				= TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH==stock_in_set?true:false;
				followupContent = adminLoggerBean.getEmploye_name()+"确认了资金[F"+apply_money_id
				+"]的转账申请[W"+transfer_id+"],关联"
				+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(association_type_id+"")
				+"["+association_id+"]";
				scheduleMgrZr.addScheduleReplayExternal(association_id, ModuleKey.TRANSPORT_ORDER,5, followupContent, isFinish, request, "transport_stock_in_set_period");
			}
			return this.floorTransportMgrZwb.addTransferLog(row);
		}catch(Exception e){
			throw new SystemException(e,"addTransferLog",log);
		}

	}
	
	//查询根据出库仓库 到货仓库转运单
	public DBRow[] findTransport(long send_psid,long receive_psid)throws Exception{
		try{
			return this.floorTransportMgrZwb.findTransport(send_psid, receive_psid);
		}catch(Exception e){
			throw new SystemException(e,"findTransport",log);
		}
	}
	
	//创建出库单  并更新转运单  //出货仓库send_psid  HttpServletRequest request
	public DBRow addOutUpdateTransport(HttpServletRequest request)throws Exception{
		try{
			String transportIds=StringUtil.getString(request,"ids");
			long send_psid=StringUtil.getLong(request,"send_psid");
			DBRow row=new DBRow();
			//获取当前时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			
			row.add("create_time", createTime);  //创建时间
			row.add("ps_id",send_psid);   //出货仓库
			row.add("state", 0);
			row.add("out_for_type",ProductStoreBillKey.TRANSPORT_ORDER);
			//创建出库单返回id
			long id=this.floorWaybillMgrZwb.addOutOrder(row);
			//更新转运单 out_id
			DBRow transportRow=new DBRow();
			transportRow.add("out_id", id);
			this.floorTransportMgrZwb.updateTransportByOutId(transportIds, transportRow);  //更新转运单
			//创建拣货单详细
			DBRow[] outDetail=this.floorTransportMgrZwb.findTransportCreateOut(transportIds, ProductStoreBillKey.TRANSPORT_ORDER);
			this.createOutDetail(outDetail,id);
            //outboundOrderMgrZJ.summarizeTransportOutListDetail(id);
			DBRow outRow=floorWaybillMgrZwb.selectOutOrder(id);
			return outRow;
		}catch(Exception e){
			throw new SystemException(e,"addOutUpdateTransport",log);
		}
	}
	
	//创建拣货单详细
	public void createOutDetail(DBRow[] rows,long outId)throws Exception{
		try{
			for(int i=0;i<rows.length;i++){
				DBRow row=new DBRow();
				row.add("out_list_pc_id",rows[i].get("out_list_pc_id",0l));
				row.add("out_list_slc_id",rows[i].get("out_list_slc_id",0l));
				row.add("out_list_area_id",rows[i].get("out_list_area_id",0l));
				row.add("out_list_serial_number",rows[i].getString("out_list_serial_number"));
				row.add("pick_up_quantity",rows[i].get("sum_quantity",0d));
				row.add("need_execut_quantity",rows[i].get("sum_quantity",0d));
				row.add("system_bill_id",rows[i].get("system_bill_id",0l));
				row.add("system_bill_type",rows[i].get("system_bill_type",0l));
				row.add("create_adid",rows[i].get("create_adid",0l));
				row.add("create_time",rows[i].getString("create_time"));
				row.add("ps_location_id",rows[i].get("ps_location_id",0l));
				row.add("from_container_type",rows[i].get("from_container_type",0l));
				row.add("from_container_type_id",rows[i].get("from_container_type_id",0l));
				row.add("from_con_id",rows[i].get("from_con_id",0l));
				row.add("pick_container_type",rows[i].get("pick_container_type",0l));
				row.add("pick_container_type_id",rows[i].get("pick_container_type_id",0l));
				row.add("pick_con_id",rows[i].get("pick_con_id",0l));
				row.add("pick_container_quantity",rows[i].get("count",0l));
				row.add("out_storebill_order",outId);
				row.add("title_id",rows[i].get("title_id",0l));
				this.floorTransportMgrZwb.addOutOrderDetail(row);
			}
		}catch(Exception e){
			throw new SystemException(e,"createOutDetail",log);
		}
	}
	
	 //根据拣货单id 查询转运单
	 public DBRow[] selectTransportByOutId(long out_id)throws Exception{
		 try{
			 return this.floorTransportMgrZwb.selectTransportByOutId(out_id);
		 }catch(Exception e){
			 throw new SystemException(e,"selectTransportByOutId",log);
		 }
	 }
	 
	//根据拣货单id  查询拣货单里所有货物
	public DBRow[] selectOutDetail(long out_id)throws Exception{
		try{
			return floorWaybillMgrZwb.selectOutDetail(out_id);
		}catch(Exception e){
			throw new SystemException(e,"selectOutDetail",log);
		}
	}
	
	//查询货物位置
	public DBRow[] findSlcId(long out_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findSlcId(out_id);
		}catch(Exception e){
			throw new SystemException(e,"findSlcId",log);
		}
	}
	//根据区域id查询位置 去重
	public DBRow[] findSlcByAreaId(long areaId,long out_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findSlcByAreaId(areaId,out_id);
		}catch(Exception e){
			throw new SystemException(e,"findSlcByAreaId",log);
		}
	}
	//查询区域是2d 还是3d
	public DBRow findCollocationDetail(String areaName)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findCollocationDetail(areaName);
		}catch(Exception e){
			throw new SystemException(e,"findCollocationDetail",log);
		}
	}
	//查询区域
	public DBRow[] findArea(long out_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findArea(out_id);
		}catch(Exception e){
			throw new SystemException(e,"findArea",log);
		}
	}
	
	//查询转运单总数量 根据出库仓库和到货仓库
	public DBRow findTransportCount(long send_psid,long receive_psid)throws Exception{
		try{
			return this.floorTransportMgrZwb.findTransportCount(send_psid, receive_psid);
		}catch(Exception e){
			throw new SystemException(e,"findTransportCount",log);
		}
	}
	
	
	public void setFloorTransportMgrZwb(FloorTransportMgrZwb floorTransportMgrZwb) {
		this.floorTransportMgrZwb = floorTransportMgrZwb;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public static void setLog(Logger log) {
		TransportMgrZwb.log = log;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setFloorWaybillMgrZwb(FloorWaybillMgrZwb floorWaybillMgrZwb) {
		this.floorWaybillMgrZwb = floorWaybillMgrZwb;
	}

	public void setOutboundOrderMgrZJ(OutboundOrderMgrIFaceZJ outboundOrderMgrZJ) {
		this.outboundOrderMgrZJ = outboundOrderMgrZJ;
	}
	
	
	
	

}
