package com.cwc.app.api.zwb;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.floor.api.zwb.FloorQuestionCatalogMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zr.ConvertFileToSwfMgrIfaceZr;
import com.cwc.app.iface.zr.FileIndexMgrIfaceZr;
import com.cwc.app.iface.zr.QuestionMgrIfaceZr;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.lucene.zr.QuestionIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.PathUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;

public class QuestionCatalogMgrZwb implements QuestionCatalogMgrIfaceZwb {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorQuestionCatalogMgrZwb floorQuestionCatalogMgrZwb;
	private AdminMgrIFace adminMgr;   
	private QuestionMgrIfaceZr questionMgrZr;
	private FloorFileMgrZr floorFileMgrZr;
	private ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr;
	private ProductLineMgrIFaceTJH productLineMgrTJH; 
	private PathUtil pathUtil;
	
	//删除问题时 取消所有用户对该问题的提醒
	public long detQuestionCallAllByQuestionId(long questionId)throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.detQuestionCallAllByQuestionId(questionId);
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb detQuestionCallAllByQuestionId"+e);
		}
	}
	
	//标记为已经阅读
	public long detQuestionCallById(long userId,long questionId)throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.detQuestionCallById(userId, questionId);
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb detQuestionCallById"+e);
		}
	}
	
	//查询某用户下的问题提醒详细
	public DBRow[] getAllQuestionCallById(long userId)throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getAllQuestionCallById(userId);
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb getAllQuestionCallById"+e);
		}
	}
	
	//查询是否有问题需要提示
	public DBRow[] getQuestionCallByUserId(long userId)throws Exception{
		try{
			DBRow[] row=this.floorQuestionCatalogMgrZwb.getQuestionCallByUserId(userId);
			return row;
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb getQuestionCallByUserId"+e);
		}
	}
	
	//添加问题时 想问题提醒表添加数据
	public void addQuestionCall(long questionId,long productLineId)throws Exception{
		try{
			DBRow[] row=this.floorQuestionCatalogMgrZwb.getQuestionCallUserList();//获取要提醒的用户列表
			for(int i=0;i<row.length;i++){
				DBRow questionRow=new DBRow();
				long userId=row[i].get("adid", 0l);
				DBRow dbrow=this.floorQuestionCatalogMgrZwb.getQuestionCallByOne(userId, questionId);
				if(dbrow==null){
					String userName=row[i].getString("employe_name");
					questionRow.add("user_id",userId);
					questionRow.add("user_name",userName);
					questionRow.add("question_id",questionId);
					questionRow.add("product_line_id",productLineId);
					this.floorQuestionCatalogMgrZwb.addQuestionCall(questionRow);
				}
			}
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb addQuestionCall"+e);
		}
	}
	
	//查询问题下的附件内容
	public String selectAppendixContent(String fileName)throws Exception{
		try{
			String INDEX_FILE_PATH = Environment.getHome()+ "upload/"+fileName;
			File file = new File(INDEX_FILE_PATH);
			String fileContent=FileUtil.readOfficeFile(file);
			return fileContent;
		}catch(Exception e){
			throw new Exception("FloorQuestionCstalogMgrZwb selectAppendixContent"+e);
		}
	}

	//查询所有为回答的问题总数 
	public DBRow getQuestionCountByStatus()throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getQuestionCountByStatus();
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCountByStatus"+e);
		}
	}
	//查询某条产品线下为回答问题详细
	public DBRow[] getQuestionproductLineStatus(long productLineId,PageCtrl pc)throws Exception{
		try{
			String ids=this.getAllSubclassByCatalogId(productLineId);
			return this.floorQuestionCatalogMgrZwb.getQuestionproductLineStatus(ids,pc);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionBproductLineStatus"+e);
		}
	}
	
	//查询某条产品线下为回答问题的总数
	public DBRow[] getQuestionCountByProductLine(HttpServletRequest request)throws Exception{
		try{
			DBRow [] pLines = productLineMgrTJH.getAllProductLine();
			List<DBRow> arrayDBRow = new ArrayList<DBRow>();	    
		    for(int i=0;i<pLines.length;i++){
		    	String productLineName=pLines[i].getString("name");
		    	long productLineId=pLines[i].get("id", 0l);
		    	String ids=this.getAllSubclassByCatalogId(productLineId);
		    	DBRow row=this.floorQuestionCatalogMgrZwb.getQuestionCountByProductLine(ids);
		    	long count=row.get("count", 0l);
		    	if(count!=0){
		    		DBRow temp =new DBRow();
			    	temp.add("productLineName",productLineName);
			    	temp.add("questionCount", count);
			    	temp.add("productLineId", productLineId);
			    	arrayDBRow.add(temp);
		    	}	
		    }
		    return  arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCountByProductLine"+e);
		}
	}
	
	//转移问题时用根据产品线id查询该产品线下所有分类详细
	@Override
	public DBRow[] moveGetQuestionCatalogByLineId(long question_line_id)throws Exception{
		try{
			 String ids=this.getAllSubclassByCatalogId(question_line_id);
			 DBRow[] row=this.floorQuestionCatalogMgrZwb.getAllCatalogIdByAllParentId(ids);
			 return row;
		}catch(Exception e){
			throw new SystemException(e,"moveGetQuestionCatalogByLineId",log);
		}
	}
	
	@Override
	public String moveQuestionCatalog(HttpServletRequest request)throws Exception{
		try{
			String message="";
			long questionId=StringUtil.getLong(request,"questionId");
			long moveCatalogId=StringUtil.getLong(request,"moveCatalogId");
			String path = StringUtil.getString(request, "path");
			DBRow row=new DBRow();
			row.add("question_catalog_id", moveCatalogId);
			row.add("discriminate", 1);  //1为问题分类下问题 0为产品线下问题
			long id=this.floorQuestionCatalogMgrZwb.moveUpdateQuestion(questionId, row);//得到更新后的问题id
			if(id==1){
				//读取附件文件
				StringBuffer sb = new StringBuffer("");
				 DBRow[] files = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(questionId, FileWithTypeKey.KNOWLEDGE);
			 	 for(int index = 0 , count = files.length ; index < count ; index++ ){
			 		 String realFilePath =  Environment.getHome()+"upload/"+path+"/"+files[index].getString("file_name");
			 		 File fileTemp = new File(realFilePath);
			 		 sb.append(FileUtil.readOfficeFile(fileTemp));
	 		 	 }
			 	 DBRow questionRow=this.floorQuestionCatalogMgrZwb.getQuestionByCatlogId(questionId);
			 	 long question_id=questionRow.get("question_id", 0l);
			 	 String question_title=questionRow.getString("question_title");
			 	 String content=questionRow.getString("contents");
			 	 long product_id=questionRow.get("product_id", 0l);
			 	 long product_catalog_id=questionRow.get("product_catalog_id", 0l);
			 	 long question_catalog_id=questionRow.get("question_catalog_id", 0l);
			 	 //调更新索引方法
			 	 QuestionIndexMgr.getInstance().updateIndex(question_id, question_title, content, sb.toString(), product_id, product_catalog_id, question_catalog_id); 
			     message="ok";
			}
			return message;
		}catch(Exception e){
			throw new SystemException(e,"moveQuestionCatalog",log);
		}
		
	}
   
   //转义特殊字符
   public String transformSolrMetacharactor(String input){
	        StringBuffer sb = new StringBuffer();
	        String regex = "[+\\-&|!(){}\\[\\]^\"~*?:(\\)]";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(input);
	        while(matcher.find()){
	            matcher.appendReplacement(sb, "\\\\"+matcher.group());
	        }
	        matcher.appendTail(sb);
	        return sb.toString();
	}
	
	//添加问题的时候查询是否存在问题------------------
	@Override
	public DBRow[] searchHint(HttpServletRequest request) throws Exception{
       try{
    	   
    	    List<DBRow> arrayDBRow = new ArrayList<DBRow>();
    	   
    	    Analyzer analyzer = new IKAnalyzer();   //实例化
    	    String questionTitle=StringUtil.getString(request,"questionTitle");
   		    long question_catalog_id=StringUtil.getLong(request,"productLineId");
   		    String question_title=transformSolrMetacharactor(questionTitle);
   		    //根据传进来的catalog_id查询所有子类id返回数组
   		    long[] question_catalog_ids=this.getAllQuestionByQid(question_catalog_id);
   	
			String INDEX_FILE_PATH = Environment.getHome()+ "/WEB-INF/index/question_index/";
			File file = new File(INDEX_FILE_PATH);
			if(!file.exists() || ( file.exists() && file.list().length < 1 )){
				return null ;
			}
			Directory directory = FSDirectory.open(file);
			DirectoryReader ireader = DirectoryReader.open(directory);
			IndexSearcher isearcher = new IndexSearcher(ireader);
			//设置多个query组合
			BooleanQuery booleanQuery = new BooleanQuery(false);
			
			//添加问题分类Id条件 必须
			if(question_catalog_ids != null && question_catalog_ids.length > 0 ){
				MultiPhraseQuery queryQuestionCatalogId  = new MultiPhraseQuery();
				Term[] terms = new Term[question_catalog_ids.length] ;
				for(int index = 0 , count = question_catalog_ids.length ; index < count ; index++ ){
					Term temp = new Term("question_catalog_id",question_catalog_ids[index]+"");
					terms[index] = temp ;
				}
				queryQuestionCatalogId.add(terms);
				booleanQuery.add(queryQuestionCatalogId,Occur.MUST);
			}
			
            //添加内容的query
			if(question_title!=null && question_title.length()>0){
				QueryParser queryParser = new QueryParser(Version.LUCENE_40,"question_title",analyzer);
				Query query=queryParser.parse(question_title);
				booleanQuery.add(query, Occur.MUST);
			}
			ScoreDoc[] scoreDoc=isearcher.search(booleanQuery, 1).scoreDocs;
			for(int i=0;i<scoreDoc.length;i++){
				 DBRow temp =new DBRow();
				 Document neirong=isearcher.doc(scoreDoc[i].doc);
				 String str[]=neirong.getValues("question_title");
				 String ids[]=neirong.getValues("question_id");
				 String cat[]=neirong.getValues("question_catalog_id");
				 for(int j=0;j<str.length;j++){
					 temp.add("question_id",ids[j]);
					 temp.add("question_title", str[j]);
					 temp.add("question_catalog_id",cat[j]);
				 }
				 arrayDBRow.add(temp);				 
			}
			   
			 return  arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
        }catch(Exception e){
    	   throw new SystemException(e,"search",log);
        }  
	}
	
	//添加问题在问题分类上
	@Override
	public long addQuestionByCatalogId(HttpServletRequest request)throws Exception{
		try{
			
			DBRow row=new DBRow();
			String contents=StringUtil.getString(request,"ajaxContent");
			String question_title=StringUtil.getString(request,"question_title");
			long product_id=StringUtil.getLong(request,"product_id");
			long productLine=StringUtil.getLong(request,"productLine");
			long catalogId=StringUtil.getLong(request,"catalogId");
		
			String file_name=StringUtil.getString(request,"file_names");
			long productCatalogId=StringUtil.getLong(request,"filter_pcid");
			//创建时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			
			//获得创建人
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoggerBean.getAdid();
			String userName=adminLoggerBean.getAccount();
			
			row.add("contents", contents);
			row.add("question_title", question_title);
			row.add("product_id",product_id);
			row.add("question_catalog_id",catalogId);
			row.add("question_create_time",createTime);
			row.add("product_catalog_id", productCatalogId);
			row.add("questioner", userName);
			row.add("product_line_id",productLine );
		    long id= floorQuestionCatalogMgrZwb.addQuestionQ(row);
		    //添加问题提醒功能
		    this.addQuestionCall(id, productLine);
		    //调添加文件的方法
		    this.questionMgrZr.addQuestionIndex(request, id, product_id, productCatalogId, catalogId);
			return id;
			
		}catch(Exception e){
			throw new SystemException(e,"addQuestionByCatalogId",log);
		}
	}
	
	

	//添加问题在产品线上
	@Override
	public long addQuestionByProductLine(HttpServletRequest request)throws Exception{
		try{
			DBRow row=new DBRow();
			String contents=StringUtil.getString(request,"ajaxContent");
			String question_title=StringUtil.getString(request,"question_title");
			long product_id=StringUtil.getLong(request,"product_id");
			long productLine=StringUtil.getLong(request,"productLine");
			String productLineName=StringUtil.getString(request,"productLineName");
			String file_name=StringUtil.getString(request,"file_names");
			long productCatalogId=StringUtil.getLong(request,"filter_pcid");
			//创建时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			//获得创建人
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoggerBean.getAdid();
			String userName=adminLoggerBean.getAccount();
			
			row.add("contents", contents);
			row.add("question_title", question_title);
			row.add("product_id",product_id);
			row.add("question_catalog_id",productLine);
			row.add("question_create_time",createTime);
			row.add("product_catalog_id", productCatalogId);
			row.add("questioner", userName);
			row.add("product_line_id",productLine );
			row.add("discriminate", 0);  //区别 该问题是产品线下问题
			
			row.add("product_id",product_id);
			long id= floorQuestionCatalogMgrZwb.addQuestionQ(row);
			 //添加问题提醒功能
		    this.addQuestionCall(id, productLine);
		    //调下添加文件方法
		    this.questionMgrZr.addQuestionIndex(request, id, product_id, productCatalogId, productLine);
		   
			return id;
		}catch(Exception e){
			throw new SystemException(e,"addQuestionByProductLine",log);
		}
		
	}
	
	//根据树等级查询问题分类
	@Override
	public DBRow[] getQuestionCatalog(long grade) throws Exception {
		try{
			return this.floorQuestionCatalogMgrZwb.getQuestionCatalog(grade);
		}catch(Exception e){
			throw new SystemException(e,"getQuestionCatalog",log);
		}
	} 
	
	//根据等级和产品线查询
	public DBRow[] getQuestionCatalogByPid(long productlineId) throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getQuestionCatalogByPid(productlineId);
		}catch(Exception e){
			throw new SystemException(e,"getQuestionCatalog",log);
		}
	}
	
	//查询所有问题分类数据
	public DBRow[] getAllQuestionCatalog() throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getAllQuestionCatalog();
		}catch(Exception e){
			throw new SystemException(e,"getAllQuestionCatalog",log);
		}
	}
	//添加问题的时候提示查询
	public DBRow[] getQuestionSingle(long question_id) throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getQuestionSingle(question_id);
		}catch(Exception e){
			throw new SystemException(e,"getQuestionSingle",log);
		}
	}
	
	//添加问题分类
	public long addQuestionCatalog(HttpServletRequest request)throws Exception{
		try{
			long productLineId=StringUtil.getLong(request,"productLineId");
			String catalogName=StringUtil.getString(request,"catalogName");
			String productLineName=StringUtil.getString(request,"productLineName");
			//创建时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			//获得创建人
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoggerBean.getAdid();
			
			DBRow row=new DBRow();
			row.add("create_man",createManId);
			row.add("create_time", createTime);
			row.add("product_line_name", productLineName);
			row.add("product_line_id", productLineId);
			row.add("parent_id", productLineId);
			row.add("catalog_title", catalogName);
			return this.floorQuestionCatalogMgrZwb.addQuestionCatalog(row);
		}catch(Exception e){
			throw new SystemException(e,"addQuestionCatalog",log);
		}
	}
	//添加子分类
	public long addQuestionCatalogSmall(HttpServletRequest request)throws Exception{
		try{
			long productLineId=StringUtil.getLong(request,"productLineId");
			String catalogName=StringUtil.getString(request,"catalogName");
			String productLineName=StringUtil.getString(request,"productLineName");
			long catalogId=StringUtil.getLong(request,"catalogId");
			//创建时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			DBRow row=new DBRow();
			//获得创建人
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoggerBean.getAdid();
			row.add("create_man",createManId);
			row.add("create_time", createTime);
			row.add("product_line_name", productLineName);
			row.add("product_line_id", productLineId);
			row.add("parent_id", catalogId);
			row.add("catalog_title", catalogName);
			row.add("grade", 2);
			return this.floorQuestionCatalogMgrZwb.addQuestionCatalog(row);
		}catch(Exception e){
			throw new SystemException(e,"addQuestionCatalogSmall",log);
		}
	}
	
	//修改问题的时候 查询所在目录
	public String getTitleByUpdateQuestion(long question_id)throws Exception{
		try{
			String title="";
			 DBRow row=this.floorQuestionCatalogMgrZwb.getQuestionQById(question_id);
			 if(row.get("discriminate", 0l)==0){
				 title=this.floorQuestionCatalogMgrZwb.getProductLineNameById(row.get("question_catalog_id",0l));
			 }else{
				 title=this.getParenttitle(row.get("question_catalog_id",0l));
			 }
			return title;
		}catch(Exception e){
			throw new SystemException(e,"getTitleByUpdateQuestion",log);
		}
	}
	
	
	//根据子类id 查出所有父类名字返回字符串
	public String getParenttitle(long catalogId)throws Exception{
		try{
		String message="";	
		ArrayList list = new ArrayList();
		DBRow row=this.getQuestionCatalogById(catalogId);
		list.add(row.getString("catalog_title"));
		while (row.get("grade", 0l)!=1) {
			row=this.getQuestionCatalogById(row.get("parent_id", 0l));
			list.add(row.getString("catalog_title"));
		}	
		  list.add(row.getString("product_line_name"));
		  for(int i=list.size()-1;i>=0;i--){
			  if(i==0){
				 message+=list.get(i).toString();
			  }else{
			     message+=list.get(i).toString()+" >> ";
			  }
		  }
		return message;
		}catch(Exception e){
			throw new SystemException(e,"getParenttitle",log);
		}
	}
	//根据id查询 问题分类
	public DBRow getQuestionCatalogById(long catalogId) throws Exception{
		try{
			return this.floorQuestionCatalogMgrZwb.getQuestionCatalogById(catalogId);
		}catch(Exception e){
			throw new SystemException(e,"getQuestionCatalogById",log);
		}
	}
	//修改问题分类
	public void updateQuestionCatalogSmall(HttpServletRequest request) throws Exception{
		try {
			long catalogId=StringUtil.getLong(request,"catalogId");
			String catalogName=StringUtil.getString(request,"catalogName");
			//更新时间
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String updateTime = dateformat.format(date);
			DBRow row=new DBRow();
			row.add("update_time", updateTime);
			row.add("catalog_title", catalogName);
			this.floorQuestionCatalogMgrZwb.updateQuestionCatalogSmall(catalogId, row);
		} catch (Exception e) {
			throw new SystemException(e,"updateQuestionCatalogSmall",log);
		}
	}	
	
	//通过问题分类查询问题 数据库查询
	@Override
	public DBRow[] getAllQuestionByFlool(long questionCatalogId,String op,PageCtrl pc)throws Exception{
		try{
			//long questionCatalogId=StrUtil.getLong(request,"catalogId");
			//根据商品线id 查处下所有 问题分类id
			String ids= this.getAllSubclassByCatalogId(questionCatalogId);
			DBRow[] row = this.floorQuestionCatalogMgrZwb.getAllQuestionByFlool(ids,op,pc);
			return row;
		}catch(Exception e){
			throw new SystemException(e,"getAllQuestionByFlool",log);
		}
	}
	
    //通过问题分类ID查询各分类下的所有子类id
	@Override
	public long[] getAllQuestionByQid(long catalogId)
			throws Exception {
		try{
         	long[] allCatalogIdsUnderCatalogId =null;
        	//获取当前子类下的所有子类id 然后根据子类的id 查询出 所有问题
        	String ids=this.getAllSubclassByCatalogId(catalogId);
            if(ids != null && ids.length() > 0){
            	String[] tempIds = ids.split(",");
            	if(tempIds != null && tempIds.length > 0 ){
            		allCatalogIdsUnderCatalogId = new long[tempIds.length];
            		for(int index = 0 ,count = tempIds.length  ; index < count ; index++){
            			long idTemp = Long.parseLong(tempIds[index]);
            			allCatalogIdsUnderCatalogId[index] = idTemp;
            		}
            		 
            	}
            }
          
            return allCatalogIdsUnderCatalogId ;
		}catch(Exception e){
			throw new SystemException(e,"getQuestionCatalogById",log);
		}
	}
	
	//根据当前子类id查询该分类下的所有子类
	private String getAllSubclassByCatalogId(long catalogId)throws Exception{
		try{
			String ids=String.valueOf(catalogId);
			DBRow[] row=this.floorQuestionCatalogMgrZwb.getAllSubclassByCatalogId(catalogId);
			while(row.length>0){
			   String str=this.getStringIdItems(row);
			   ids+=","+str;
			   row=this.floorQuestionCatalogMgrZwb.getAllCatalogIdByAllParentId(str);
			}
			return ids;
		}catch(Exception e){
			throw new SystemException(e,"getAllSubclassByCatalogId",log);
		}
	}
	
	private String getStringIdItems(DBRow[] row)throws Exception{
		String str="";
		for(int i=0;i<row.length;i++){
			if(row.length==1 || i== row.length-1){
				str+=row[i].get("question_catalog_id", 0l);
			}else{
				str+=row[i].get("question_catalog_id", 0l)+",";
			}
		}
		return str;
	}
	
	public long[] getAllProductCatalog(long productCatalogId)throws Exception{
		try{	
			DBRow[] row=this.floorQuestionCatalogMgrZwb.getAllProductCatalog(productCatalogId);
			long[] num={row.length};
			for(int i=0;i<row.length;i++){
				num[i]=row[i].get("pc_id", 0l);
				////system.out.println(num[i]);
			}
			return num;
		}catch(Exception e){
			throw new SystemException(e,"getAllProductCatalog",log);
		}
	}
	//通过问题ID查询问题信息
	public DBRow getQuestionByCatlogId(long QuestionId)throws Exception {
		try {
			DBRow row=this.floorQuestionCatalogMgrZwb.getQuestionQById(QuestionId);
			return row;
		} catch (Exception e) {
			throw new SystemException(e,"getQuestionByCatlogId", log);
		}
	}
	//回答问题
	public DBRow addQuestionAnswer(HttpServletRequest request)throws Exception{
		try {
			long question_id = StringUtil.getLong(request,"question_id");
			String answer_content = StringUtil.getString(request,"answer_content");
			 String fileNames = StringUtil.getString(request, "file_names");
			 String sn = StringUtil.getString(request, "sn");
			 String path = StringUtil.getString(request, "path");
			 int fileWithType =  FileWithTypeKey.ANSWER;
			 
			AdminMgr adminMgr= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			// 创建时间
			Date date = Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			DBRow row = new DBRow();
			row.add("question_id", question_id);
			row.add("answer_content",answer_content);
			row.add("create_time", createTime);
			row.add("answerer", adminLoggerBean.getAccount());
			long answer_id = this.floorQuestionCatalogMgrZwb.addQuestionAnswer(row);
			DBRow ansRow=this.floorQuestionCatalogMgrZwb.getAnswerById(answer_id);
			//修改问题回答状态
			this.modQuestionAnswerState(question_id, 2);
			//调添加文件的方法
			this.handleNewFileConvertMove(fileNames, sn, answer_id, fileWithType, path, adminLoggerBean);
			//回答完问题后把该问题标记为 未读
			DBRow questionRow=this.getQuestionByCatlogId(question_id);
		    this.addQuestionCall(question_id, questionRow.get("product_line_id", 0l));
		    
		    return ansRow;
		} catch (Exception e) {
			throw new SystemException(e, "addQuestionAnswer", log);
		}
	}
	//更改问题回答状态为已回答
	public void modQuestionAnswerState(long question_id,int state)throws Exception{
		try{
			DBRow row = new DBRow();
			row.add("question_status", state);
			this.floorQuestionCatalogMgrZwb.modQuertionAnswerState(question_id, row);
		}catch(Exception e) {
			throw new SystemException(e, "updateQuestionAnswerState", log);
		}
	}
	//上传文件
	//文件重新命名，文件复制到指定文件夹
	private String handleNewFileConvertMove(String fileNames,String sn , long answer_id , int fileWithType , String path ,AdminLoginBean adminLoggerBean) throws Exception {
		StringBuffer sb = new StringBuffer("");
		try{
 
			//在文件中添加file.把文件从临时文件夹中移动到正式的文件夹中
			//注意文件名不能重复
			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
			if(fileNames.length() > 0 ){
				String[] fileNameArray = fileNames.split(",");
				if(fileNameArray != null && fileNameArray.length > 0 ){
					
					for(String fileNameTemp : fileNameArray){	
						StringBuffer realyFileName = new StringBuffer(sn);
	 					String suffix = StringUtil.getSuffix(fileNameTemp);
	 					realyFileName.append("_").append(answer_id).append("_").append(fileNameTemp.replace("."+suffix, ""));
	 					int indexFile = floorFileMgrZr.getIndexOfFileByFileName(realyFileName.toString());
	 					if(indexFile != 0){
	 						realyFileName.append("_").append(indexFile);
	 					}
	 					
						String desFilePath = realyFileName.append(".")+"swf";
						realyFileName.append(suffix);
	 					
						long fileId = this.addFile(realyFileName.toString(), answer_id, fileWithType, 0, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 					String realFilePath =  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString();
	 					FileUtil.moveFile(baseUrl+fileNameTemp,  realFilePath);
	 					sb.append(FileUtil.readOfficeFile(new File(realFilePath)));
	 					//转换文件 
	 					convertFileToSwfMgrZr.conver(realFilePath,Environment.getHome() +"upload/"+path+"/"+ desFilePath, Environment.getHome()+"upload/"+path+"/");
	 					
	 					//update file 把file的file_is_convert == 1.保存文件保存的路径
	 					DBRow fileTempRow = new DBRow() ;
	 					fileTempRow.add("file_is_convert", 1);
	 					fileTempRow.add("file_convert_file_path", "upload/"+path+"/"+ desFilePath);
	 					floorFileMgrZr.updateFileById(fileId, fileTempRow);
					}
  				}
			}
			return sb.toString();
		}catch (Exception e) {
			 throw new SystemException(e,"updateKnowledge",log); 
		}
	}
	//添加传文件
	private long addFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception {
		try{
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			file.add("file_is_convert", 0);
			file.add("file_convert_file_path", "");
			return floorFileMgrZr.addFile(file);
		}catch (Exception e) {
			throw new SystemException(e,"addFile",log);
		}
	}
	//根据问题ID查询回答内容
	public DBRow getAnswerByQuestionId(long question_id) throws Exception{
		try {
			return this.floorQuestionCatalogMgrZwb.getAnswerByQuestionId(question_id);
		} catch (Exception e) {
			throw new SystemException(e,"getAnswerByQuestionId",log);
		}
	}
	
	
	//删除答案
	public void deleteQuestionAnswer(long answer_id,long question_id)throws Exception{
		
		try {
			//删除file表中的记录,文件不删除
			
			DBRow[] fillRows = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(answer_id, FileWithTypeKey.ANSWER);
			if(fillRows != null && fillRows.length > 0 ){
				for(int index = 0 , count = fillRows.length ; index < count ; index++ ){
						long fileId = fillRows[index].get("file_id", 0l);
						
						floorFileMgrZr.deleteFile(fileId);	
				}
			}
			//删除答案
			this.floorQuestionCatalogMgrZwb.deleteQuestionAnswer(answer_id);
			//修改回答状态
			this.modQuestionAnswerState(question_id, 1);
			
		} catch (Exception e) {
			throw new SystemException(e,"deleteQuestionAnswer",log);
		}
	}
	//根据答案ID查询答案详情
	public DBRow getAnswerById(long answer_id)throws Exception{
		try {
			DBRow row=this.floorQuestionCatalogMgrZwb.getAnswerById(answer_id);
			return row;
		} catch (Exception e) {
			throw new SystemException(e,"getQuestionByCatlogId", log);
		}
	}
	//修改答案
	public DBRow modAnswer(HttpServletRequest request)throws Exception{
		try{
			 
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			 
			 String content = StringUtil.getString(request, "answer_content"); 
			 String fileNames = StringUtil.getString(request, "file_names");
			 String sn = StringUtil.getString(request, "sn");
			 String path = StringUtil.getString(request, "path");
			 int fileWithType =  FileWithTypeKey.ANSWER;
			 
			 long answer_id = StringUtil.getLong(request, "answer_id");
		 	 
		 	// 修改时间
				Date date = Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String updateTime = dateformat.format(date);
				
		 	 StringBuffer sb = new StringBuffer("");
		 	
		 	 DBRow[] files = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(answer_id, FileWithTypeKey.ANSWER);
		 	 for(int index = 0 , count = files.length ; index < count ; index++ ){
		 		 String realFilePath =  Environment.getHome()+"upload/"+path+"/"+files[index].getString("file_name");
		 		 File fileTemp = new File(realFilePath);
		 		 sb.append(FileUtil.readOfficeFile(fileTemp));
		 	 }
		 	 if(fileNames != null && fileNames.trim().length() > 0 ){
		 		 //处理新上传的文件
		 		 sb.append(this.handleNewFileConvertMove(fileNames, sn, answer_id, fileWithType, path, adminLoggerBean));
		 	 }
			 DBRow modKnowledge = new DBRow();
			 modKnowledge.add("answer_content",content);
			 modKnowledge.add("update_time", updateTime);
			 modKnowledge.add("answerer", adminLoggerBean.getAccount());
			 
			 floorQuestionCatalogMgrZwb.modAnswerById(answer_id, modKnowledge);
			 return modKnowledge;
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"updateKnowledge",log);
		}
		
	}
	
	public static void setLog(Logger log) {
		QuestionCatalogMgrZwb.log = log;
	}

	public void setFloorQuestionCatalogMgrZwb(
			FloorQuestionCatalogMgrZwb floorQuestionCatalogMgrZwb) {
		this.floorQuestionCatalogMgrZwb = floorQuestionCatalogMgrZwb;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setQuestionMgrZr(QuestionMgrIfaceZr questionMgrZr)
	{
		this.questionMgrZr = questionMgrZr;
	}
	

	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr) {
		this.floorFileMgrZr = floorFileMgrZr;
	}
	public void setConvertFileToSwfMgrZr(ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr)
	{
		this.convertFileToSwfMgrZr = convertFileToSwfMgrZr;
	}

	public void setProductLineMgrTJH(ProductLineMgrIFaceTJH productLineMgrTJH) {
		this.productLineMgrTJH = productLineMgrTJH;
	}

	public void setPathUtil(PathUtil pathUtil) {
		this.pathUtil = pathUtil;
	}

 
	 
	

	
}
