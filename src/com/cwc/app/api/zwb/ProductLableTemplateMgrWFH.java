package com.cwc.app.api.zwb;

import com.cwc.app.floor.api.zwb.FloorProductLableTemplateMgrWFH;
import com.cwc.app.iface.zwb.ProductLableTempMgrIfaceWFH;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class ProductLableTemplateMgrWFH implements ProductLableTempMgrIfaceWFH {
	private FloorProductLableTemplateMgrWFH productLableTemplate;
	
	public FloorProductLableTemplateMgrWFH getProductLableTemplate() {
		return productLableTemplate;
	}
	public void setProductLableTemplate(
			FloorProductLableTemplateMgrWFH productLableTemplate) {
		this.productLableTemplate = productLableTemplate;
	}
	
	@Override
	public DBRow[] findAllLable(String lableName,String type,int templateType,PageCtrl pc) throws Exception {
		try{
			DBRow[] rows = productLableTemplate.findAllLable(lableName,type,templateType,pc);
			return rows;
		}catch(Exception e){
			throw new Exception("ProductLableTemplateMgrWFH.findAllLable error:" + e);
		}
	}

	//新建基础标签模板信息
	public long addLableTemp(DBRow row) throws Exception
	{
		try {
			return this.productLableTemplate.addLableTemp(row);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	//根据id查询基础标签模板信息
	public DBRow getLableTempById(long id)throws Exception
	{
		try {
			return this.productLableTemplate.getLableTempById(id);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	//根据id修改
	public void modifyLableTemp(long id,DBRow row) throws Exception{
		try {
			this.productLableTemplate.modifyLableTemp(id,row);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	//根据id删除
	public void delLabelTemp(long id)throws Exception{
		try {
			this.productLableTemplate.delLabelTemp(id);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
}
