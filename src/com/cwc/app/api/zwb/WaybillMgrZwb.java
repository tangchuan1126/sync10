package com.cwc.app.api.zwb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zwb.FloorTransportMgrZwb;
import com.cwc.app.floor.api.zwb.FloorWaybillMgrZwb;
import com.cwc.app.iface.zj.OutboundOrderMgrIFaceZJ;
import com.cwc.app.iface.zwb.WaybillMgrIfaceZwb;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class WaybillMgrZwb implements WaybillMgrIfaceZwb{
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorWaybillMgrZwb floorWaybillMgrZwb;
	private OutboundOrderMgrIFaceZJ outboundOrderMgrZJ;
	private ShortMessageMgrZyjIFace shortMessageMgrZyjIFace;
	private FloorTransportMgrZwb floorTransportMgrZwb;

	//查询商品在该产品线下的运单数量
	@Override
	public DBRow getWaybillByCountLineId(long lineId) throws Exception {
		try{
			return this.floorWaybillMgrZwb.getWaybillByCountLineId(lineId);
		}catch(Exception e){
			throw new SystemException(e,"getWaybillByCountLineId",log);
		}	
	}
	
	//包含件数  
	public DBRow seachPkcount(long num)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachPkcount(num);
		}catch(Exception e){
			throw new SystemException(e,"seachPkcount",log);
		}
	}
	
	//包含多件数  
	public DBRow seachPkcountMuch(long num)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachPkcountMuch(num);
		}catch(Exception e){
			throw new SystemException(e,"seachPkcountMuch",log);
		}
	}
	
	/**
	 * @param lineId 	 产品线id
	 * @param sc_id  	 快递id
	 * @param ps_id   	 仓库id
	 * @param all_weight 重量
	 * @param pkcount    包含数量
	 * @return			 返回条件下 该产品线的运单数
	 * @throws Exception
	 */
	public DBRow getSeachWaybillByLineId(long lineId,long sc_id,long ps_id,long all_weight,long pkcount) throws Exception{
		try{
			return this.floorWaybillMgrZwb.getSeachWaybillByLineId(lineId, sc_id, ps_id, all_weight, pkcount);
		}catch(Exception e){
			throw new SystemException(e,"getSeachWaybillByLineId",log);
		}
	}
	
	//条件查询包含数量
	public DBRow getCountPkcountMuch(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			return this.floorWaybillMgrZwb.getCountPkcountMuch(lineId, sc_id, ps_id, all_weight, pkcount);
		}catch(Exception e){
			throw new SystemException(e,"getCountPkcountMuch",log);
		}
	}
	
	//条件查询包含数量 123
	public DBRow getCountPkcount(long sc_id,long ps_id,long all_weight,long pkcount) throws Exception{
		try{
			return this.floorWaybillMgrZwb.getCountPkcount(sc_id, ps_id, all_weight, pkcount);
		}catch(Exception e){
			throw new SystemException(e,"getCountPkcount",log);
		}
	}
	
	//条件查询包含数量 大于4   没有产品线
	public DBRow getCountPkcountMuchNoLine(long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			return this.floorWaybillMgrZwb.getCountPkcountMuchNoLine(sc_id, ps_id, all_weight, pkcount);
		}catch(Exception e){
			throw new SystemException(e,"getCountPkcountMuchNoLine",log);
		}
	}
	
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLine(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachWaybillDetailedNoLine(sc_id, ps_id, all_weight, pkcount,pc);
		}catch(Exception e){
			throw new SystemException(e,"seachWaybillDetailedNoLine",log);
		}
	}
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailed(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachWaybillDetailed(lineId, sc_id, ps_id, all_weight, pkcount,pc);
		}catch(Exception e){
			throw new SystemException(e,"seachWaybillDetailed",log);
		}
	}
	
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLineFour(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachWaybillDetailedNoLineFour(sc_id, ps_id, all_weight, pkcount,pc);
		}catch(Exception e){
			throw new SystemException(e,"seachWaybillDetailedNoLineFour",log);
		}
	}
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailedFour(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			return this.floorWaybillMgrZwb.seachWaybillDetailedFour(lineId, sc_id, ps_id, all_weight, pkcount,pc);
		}catch(Exception e){
			throw new SystemException(e,"seachWaybillDetailedFour",log);
		}
	}
	
	//创建拣货单、 返回当前拣货单id
	public DBRow addOutOrder(HttpServletRequest request)throws Exception{
		try{
			DBRow row=new DBRow();
			long ps_id=StringUtil.getLong(request,"ps_id");  //仓库id
			long sc_id=StringUtil.getLong(request,"sc_id");   //快递
			long line_id=StringUtil.getLong(request,"line");   // 产品线
			long allWeight=StringUtil.getLong(request,"allWeight");  //重量
			long pkcount=StringUtil.getLong(request,"pkcount");
			String adminIds=StringUtil.getString(request,"ids");
			DBRow[] waybill=null;
			if(line_id==0 && pkcount!=4){
				waybill=this.floorWaybillMgrZwb.seachWaybillDetailedNoLine(sc_id,ps_id,allWeight,pkcount);
			}else if(line_id==0 && pkcount==4){
				waybill=this.floorWaybillMgrZwb.seachWaybillDetailedNoLineFour(sc_id,ps_id,allWeight,pkcount);
			}else if(line_id!=0 && pkcount!=4){
				waybill=this.floorWaybillMgrZwb.seachWaybillDetailed(line_id,sc_id,ps_id,allWeight,pkcount);
			}else if(line_id!=0 && pkcount==4){
				waybill=this.floorWaybillMgrZwb.seachWaybillDetailedFour(line_id,sc_id,ps_id,allWeight,pkcount);
			}
			String ids="";
			for(int i=0;i<waybill.length;i++){
				if(i==waybill.length-1){
					ids+=waybill[i].getString("waybill_id");
				}else{
					ids+=waybill[i].getString("waybill_id")+",";
				}
			}
			if(ids!=""){
				//获取当前时间
				Date date=(Date)Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String createTime = dateformat.format(date);
				
				row.add("create_time", createTime);
				row.add("ps_id", ps_id);
				row.add("state", 0);
				row.add("out_for_type", ProductStoreBillKey.WAYBILL_ORDER);
				long outOrderId=this.floorWaybillMgrZwb.addOutOrder(row);
				//更新运单到 当前拣货单上
				DBRow outrow=new DBRow();
				outrow.add("out_id",outOrderId);
				this.floorWaybillMgrZwb.updateWaybillOutId(outrow, ids);
				
				DBRow outOrderRow=this.floorWaybillMgrZwb.selectOutOrder(outOrderId);
				
				//创建拣货单详细
				DBRow[] outDetail=this.floorTransportMgrZwb.findTransportCreateOut(ids, ProductStoreBillKey.WAYBILL_ORDER);
				this.createOutDetail(outDetail,outOrderId);
				//outboundOrderMgrZJ.summarizeWaybillOutListDetail(outOrderId);

				//根据区域通知负责人，发信息
//				String[] items=adminIds.split(",");
//				DBRow[] receivers = null ;
//				for(int a=0;a<items.length;a++){
//					receivers=new DBRow[items.length];
//				   //调发短信接口
//					DBRow tempReceiver=this.floorWaybillMgrZwb.getPeople(Long.parseLong(items[a]));
//					receivers[a]=tempReceiver;
//				}
//				String title="请准备去拣货";
//				this.shortMessageMgrZyjIFace.addShortMessage(outOrderId, ProductStoreBillKey.WAYBILL_ORDER, title, receivers, request);
		        
				
				return outOrderRow;
			}else{
				return null;
			}	
		}catch(Exception e){
			throw new SystemException(e,"addOutOrder",log);
		}
	}
	//创建拣货单详细
	public void createOutDetail(DBRow[] rows,long outId)throws Exception{
		try{
			for(int i=0;i<rows.length;i++){
				DBRow row=new DBRow();
				row.add("out_list_pc_id",rows[i].get("out_list_pc_id",0l));
				row.add("out_list_slc_id",rows[i].get("out_list_slc_id",0l));
				row.add("out_list_area_id",rows[i].get("out_list_area_id",0l));
				row.add("out_list_serial_number",rows[i].getString("out_list_serial_number"));
				row.add("pick_up_quantity",rows[i].get("sum_quantity",0d));
				row.add("system_bill_id",rows[i].get("system_bill_id",0l));
				row.add("system_bill_type",rows[i].get("system_bill_type",0l));
				row.add("create_adid",rows[i].get("create_adid",0l));
				row.add("create_time",rows[i].getString("create_time"));
				row.add("ps_location_id",rows[i].get("ps_location_id",0l));
				row.add("from_container_type",rows[i].get("from_container_type",0l));
				row.add("from_container_type_id",rows[i].get("from_container_type_id",0l));
				row.add("from_con_id",rows[i].get("from_con_id",0l));
				row.add("pick_container_type",rows[i].get("pick_container_type",0l));
				row.add("pick_container_type_id",rows[i].get("pick_container_type_id",0l));
				row.add("pick_con_id",rows[i].get("pick_con_id",0l));
				row.add("out_storebill_order",outId);
				this.floorTransportMgrZwb.addOutOrderDetail(row);
			}
		}catch(Exception e){
			throw new SystemException(e,"createOutDetail",log);
		}
	}
	
	//根据拣货单id查询运单
	public DBRow[] findWaybillByOutId(long outId)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findWaybillByOutId(outId);
		}catch(Exception e){
			throw new SystemException(e,"findWaybillByOutId",log);
		}
	}
	
	//查询出库单 先查所有
	public DBRow[] selectAllOutOrder(long out_id,long ps_id,long outRorType,PageCtrl pc)throws Exception{
		try{
			DBRow[] row= this.floorWaybillMgrZwb.selectAllOutOrder(out_id, ps_id,outRorType,pc);
			for(int i=0;i<row.length;i++){
				long outId=row[i].get("out_id", 0l);
				String message=this.printStatusOutOrder(outId);
				row[i].add("message", message);
			}
			return row;
		}catch(Exception e){
			throw new SystemException(e,"selectAllOutOrder",log);
		}
	}
	//扫描货物打印1票1件
	public DBRow printScanProduct(HttpServletRequest request)throws Exception{
		try{
			long scanId=StringUtil.getLong(request,"scanId");    //扫描的货码
			long outId=StringUtil.getLong(request,"outId");
			DBRow waybillRow=null;
			DBRow retuRow=null;
			//判断 当前出库单 下 包含该货物的 运单
			DBRow[] row=this.floorWaybillMgrZwb.findWaybillByProduct(scanId, outId);
			for(int i=0;i<row.length;i++){
				long waybill_id=row[i].get("waybill_id", 0l);  //运单id
				long sc_id=row[i].get("sc_id", 0l);           //快递id
				 waybillRow=this.floorWaybillMgrZwb.findProductCountByWaybillId(waybill_id);
				 //判断运单下包含的 件数与数量
				 long id=waybillRow.get("waybill_order_id", 0l);       //运单
				 long quantity=(long)waybillRow.get("quantity", 0.0d);
				 long count=waybillRow.get("count", 0l); 
				 if(quantity==1 && count==1){
					 retuRow=waybillRow;
					 retuRow.add("sc_id",sc_id);
					 break;
				 }
			}
			return retuRow;
		}catch(Exception e){
			throw new SystemException(e,"printScanProduct",log);
		}
	}
	//扫描货物打印1票多件
	public DBRow[] printScanProductMuch(HttpServletRequest request)throws Exception{
		try{
			long scanId=StringUtil.getLong(request,"scanId");    //扫描的货码
			long outId=StringUtil.getLong(request,"outId");
			DBRow waybillRow=null;
			DBRow retuRow=null;
			//判断 当前出库单 下 包含该货物的 运单
			DBRow[] row=this.floorWaybillMgrZwb.findWaybillByProduct(scanId, outId);
			for(int i=0;i<row.length;i++){
				long waybill_id=row[i].get("waybill_id", 0l);  //运单id
				  waybillRow=this.floorWaybillMgrZwb.findProductCountByWaybillId(waybill_id);
				 //判断运单下包含的 件数与数量
				 long id=waybillRow.get("waybill_order_id", 0l);       //运单
				 long quantity=(long)waybillRow.get("quantity", 0.0d);
				 long count=waybillRow.get("count", 0l); 
				 if(quantity > 1 || count > 1){
					 retuRow=waybillRow;
					 break;
				 }
			}
			if(retuRow!=null){
				return this.floorWaybillMgrZwb.findProductMuchByWaybillId(retuRow.get("waybill_order_id", 0l));
			}else{
				return null;
			}
		}catch(Exception e){
			throw new SystemException(e,"printScanProductMuch",log);
		}
	}
	
	//查快递详细信息
	public DBRow findExpressDetailed(long sc_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findExpressDetailed(sc_id);
		}catch(Exception e){
			throw new SystemException(e,"findExpressDetailed",log);
		}
	}
	
	//计算出库单状态
	public String printStatusOutOrder(long out_id)throws Exception{
		try{
			DBRow sumRow=this.floorWaybillMgrZwb.sumCountWaybillByOutId(out_id); //总数量
			DBRow printRow=this.floorWaybillMgrZwb.sumCountPrint(out_id);        //打印的数量
			long sumCount=sumRow.get("count", 0l);
			long printCount=printRow.get("count", 0l);
			String message="";
			if(printCount==0){
				message="都未打印";
			}else if(printCount!=0 && sumCount > printCount){
				message="部分打印";
			}else if(printCount!=0 && sumCount == printCount){
				message="全部打印";
			}
			return message;
		}catch(Exception e){
			throw new SystemException(e,"printStatusOutOrder",log);
		}
	}
	
	//根据id查询 拣货单
	public DBRow selectOutOrder(long out_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.selectOutOrder(out_id);
		}catch(Exception e){
			throw new SystemException(e,"selectOutOrder",log);
		}
	}
	
	//商品
	public DBRow findProductById(long pc_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findProductById(pc_id);
		}catch(Exception e){
			throw new SystemException(e,"findProductById",log);
		}
	}
	//查询位置
	public DBRow findStorageLocationCatalogById(long slc_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findStorageLocationCatalogById(slc_id);
		}catch(Exception e){
			throw new SystemException(e,"findStorageLocationCatalogById",log);
		}
	}
	//查询区域
	public DBRow findStorageLocationAreaById(long area_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.findStorageLocationAreaById(area_id);
		}catch(Exception e){
			throw new SystemException(e,"findStorageLocationAreaById",log);
		}
	}
	//拣完货，需要放到的门和位置  根据类型 是拣货单  和拣货单id
	public DBRow[] getStorageOrDoorByOutId(long rel_type,long rel_id)throws Exception{
		try{
			DBRow[] row=this.floorWaybillMgrZwb.getStorageOrDoorByOutId(rel_type, rel_id);
			List<DBRow> arrayDBRow = new ArrayList<DBRow>();
			for(int i=0;i<row.length;i++){
				DBRow dbr=new DBRow();
				long type=row[i].get("occupancy_type", 0l);
				long id=row[i].get("rl_id", 0l);
				if(type==1){   //type 1 门  2位置
					DBRow doorRow=this.floorWaybillMgrZwb.getDoorByRlId(id);
					String door=doorRow.getString("doorId");
					dbr.add("door", door);
					dbr.add("location","  ");
				}else if(type==2){
					DBRow locationRow=this.floorWaybillMgrZwb.getStorageByRlId(id);
					String location=locationRow.getString("location_name");
					dbr.add("location", location);
					dbr.add("door","  ");
				}
				arrayDBRow.add(dbr);	
			}
			return arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
		}catch(Exception e){
			throw new SystemException(e,"getStorageOrDoorByOutId",log);
		}
	}
	
	//运单创建拣货单分配人
	public DBRow[] getPeopleList(long ps_id)throws Exception{
		try{
			return this.floorWaybillMgrZwb.getPeopleList(ps_id);
		}catch(Exception e){
			throw new SystemException(e,"getPeopleList",log);
		}
	}

	public void setFloorWaybillMgrZwb(FloorWaybillMgrZwb floorWaybillMgrZwb) {
		this.floorWaybillMgrZwb = floorWaybillMgrZwb;
	}

	public void setOutboundOrderMgrZJ(OutboundOrderMgrIFaceZJ outboundOrderMgrZJ) {
		this.outboundOrderMgrZJ = outboundOrderMgrZJ;
	}

	public void setFloorTransportMgrZwb(FloorTransportMgrZwb floorTransportMgrZwb) {
		this.floorTransportMgrZwb = floorTransportMgrZwb;
	}

	
}
