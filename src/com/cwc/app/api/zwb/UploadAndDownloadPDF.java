package com.cwc.app.api.zwb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.antlr.grammar.v3.ANTLRParser.finallyClause_return;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class UploadAndDownloadPDF {
	static Logger log = Logger.getLogger("ACTION");
	private static int MAX_RETRY_TIMES =5;
	/**
	 * 文件上传到文件服务器
	 * 
	 * @param url
	 * @param destFileName
	 * @param sessionId
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String upLoadPDF(String url, String fullPath, String sessionId)  {
		String fileId = null;
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("Cookie", "JSESSIONID=" + sessionId);
			FileBody bin = new FileBody(new File(fullPath));

			MultipartEntity reqEntity = new MultipartEntity();

			reqEntity.addPart("file", bin);// file1为请求后台的File upload;属性
			httppost.setEntity(reqEntity);

			HttpResponse response = httpClient.execute(httppost);

			int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				JSONArray json = JSONArray.fromObject(resp);
				JSONObject jo = json.getJSONObject(0);
				fileId = jo.getString("file_id");
				EntityUtils.consume(resEntity);
			}else{
				log.error("PDF upload params"+url+" "+fullPath+" "+sessionId);
				log.error("PDF upload error"+statusCode+" "+response.getStatusLine().toString());

			}
			return fileId;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				httpClient.getConnectionManager().shutdown();
			} catch (Exception ignore) {
				ignore.printStackTrace();
			}
		}
		return fileId;
	}
	
	public static synchronized String upLoadPDF(String url, String fullPath, String sessionId,int retTryTimes) throws Exception {
		String fileId = null;
		HttpClient httpClient = new DefaultHttpClient();
		boolean shutDowned = false;
		try {
			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("Cookie", "JSESSIONID=" + sessionId);
			FileBody bin = new FileBody(new File(fullPath));
			MultipartEntity reqEntity = new MultipartEntity();

			reqEntity.addPart("file", bin);// file1为请求后台的File upload;属性
			httppost.setEntity(reqEntity);

			HttpResponse response = httpClient.execute(httppost);

			int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				JSONArray json = JSONArray.fromObject(resp);
				JSONObject jo = json.getJSONObject(0);
				fileId = jo.getString("file_id");
				EntityUtils.consume(resEntity);
			}else{
				log.error("PDF upload params"+url+" "+fullPath+" "+sessionId);
				log.error("PDF upload error"+statusCode+" "+response.getStatusLine().toString());

			}
			return fileId;
		} catch (IOException e) {
			e.printStackTrace();
			httpClient.getConnectionManager().shutdown();
			shutDowned = true;
			if( retTryTimes < MAX_RETRY_TIMES){
				upLoadPDF(url, fullPath, sessionId, ++retTryTimes);
			}
		} finally {
			if(!shutDowned){
				try {
					httpClient.getConnectionManager().shutdown();
				} catch (Exception ignore) {
					ignore.printStackTrace();
				}
			}
		}
		return fileId;
	}

	
	/**
	 * 在文件服务上获取文件
	 * @param url
	 * @param destFileName
	 * @param sessionId
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public File getFile(String url, String destFileName, String sessionId)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);

		httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
		HttpResponse response = httpClient.execute(httpget);

		StatusLine statusLine = response.getStatusLine();
		int httpStatus=statusLine.getStatusCode();
		File file=null;
		if (httpStatus == 200) {
			HttpEntity entity = response.getEntity();
			
			InputStream in = entity.getContent();
			file = new File(destFileName);

			try {
				FileOutputStream fout = new FileOutputStream(file);
				int l = -1;
				byte[] tmp = new byte[1024];
				while ((l = in.read(tmp)) != -1) {
					fout.write(tmp, 0, l);
				}
				fout.flush();
				fout.close();
			} finally {
				// 关闭低层流。
				in.close();
			}
			//把底层的流给关闭
			EntityUtils.consume(entity);
		}else{
			log.error("PDF Get params"+url+" "+destFileName+" "+sessionId);
			log.error("PDF Get error"+httpStatus+" "+response.getStatusLine().toString());
			//system.out.println("PDF Get params"+url+" "+destFileName+" "+sessionId);
			//system.out.println("PDF Get error"+httpStatus+" "+response.getStatusLine().toString());
		}
		
		httpClient.getConnectionManager().shutdown();
		return file;
	}
	/**
	 * 在文件服务上删除
	 * @param url
	 * @param destFileName
	 * @param sessionId
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public boolean deleteFile(String url, String sessionId)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpDelete httpdelete = new HttpDelete(url);

		httpdelete.setHeader("Cookie", "JSESSIONID=" + sessionId);
		HttpResponse response = httpClient.execute(httpdelete);

		StatusLine statusLine = response.getStatusLine();
		int httpStatus=statusLine.getStatusCode();
		boolean success=false;
		if (httpStatus == 200) {
			success=true;
		}else{
			log.error("PDF delete params"+url+" "+sessionId);
			log.error("PDF delete error"+httpStatus+" "+response.getStatusLine().toString());
		}
		httpClient.getConnectionManager().shutdown();
		return success;
	}
	
}
