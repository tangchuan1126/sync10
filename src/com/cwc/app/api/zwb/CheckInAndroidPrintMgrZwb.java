package com.cwc.app.api.zwb;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInAndroidPrintMgrIfaceZwb;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;




public class CheckInAndroidPrintMgrZwb implements CheckInAndroidPrintMgrIfaceZwb{

	private FloorCheckInMgrZwb floorCheckInMgrZwb;
    private SQLServerMgrIFaceZJ sqlServerMgrZJ;
    private CheckInMgrIfaceZr checkInMgrZr ;
    private CheckInMgrIfaceZwb checkInMgrZwb;
    
     static Logger log = Logger.getLogger("ACTION");
    ModuleKey moduleKey = new ModuleKey();
    
    //android 打印bill of loading 多个 返回列表
  	@Override
  	public DBRow androidSelectLabel(long adid,long entry_id,long detail_id,HttpServletRequest request)throws Exception{
  		try{
  			int  resources_type   = StringUtil.getInt(request, "resources_type");
  			int  resources_id   = StringUtil.getInt(request, "resources_id");
  			String  resources_name   = StringUtil.getString(request, "resources_name");

  			DBRow result=new DBRow();
  			DBRow row=this.floorCheckInMgrZwb.selectDetailByDetailId(detail_id);
  			long equipment_id = row.get("equipment_id", 0l);
  		//	DBRow door=this.floorCheckInMgrZwb.findDoorById(row.get("rl_id", 0l));
  		//	DBRow entry = this.floorCheckInMgrZwb.findMainById(entry_id);
  			String door_name=  resources_name;
  			String number=row.getString("number");
  			int number_type=row.get("number_type",0);
  			//在Load 和Po的是时候重新添加OrderNo zhangrui
  			long order_no = 0l;
  			if(number_type == ModuleKey.CHECK_IN_ORDER){
  				  order_no= row.get("number", 0l);
  			}
  			if(number_type == ModuleKey.CHECK_IN_PONO){
  				order_no= row.get("order_no", 0l);
  			}
  			String customer_id=row.getString("customer_id");
  			String company_id=row.getString("company_id");
  			String account_id=row.getString("account_id");
  			entry_id =  row.get("dlo_id", 0l);
  			if(ModuleKey.CHECK_IN_CTN==number_type || number_type==ModuleKey.CHECK_IN_BOL){
  				//加载送货ticket
  				this.androidDeliveryLabel(result,customer_id,company_id, number_type, number, entry_id, adid, request,door_name);
  				
  			}else{
	            //加载 bill of lading
	  			this.androidGetPrintList(result,number, company_id, customer_id,entry_id,number_type,order_no,adid,request,door_name,equipment_id);
	  			//加载load ticket
	  			this.getWmsRow(result, number_type, number, company_id, customer_id, entry_id, adid, request);
	  			//加载counting sheet
	  			this.getCountingSheet(result, number_type);
	  			//加载packing list
	  			this.getPackingList(result, order_no, number, number_type, company_id, customer_id, adid);
	  			//公用的数据
	  			this.getData(result,number,number_type,customer_id,company_id,account_id,door_name,order_no,  checkInMgrZr.getEquipmentSeals(equipment_id).getString("out_seal"));
  			}

  			return result;
  		}catch(Exception e){
  			throw new SystemException(e,"androidLinkWebPrintGetPath",log);
  		}
  	}
  	
  	//公用的数据
  	public void getData(DBRow results,String number,int number_type,String customer_id,String company_id,String account_id,String door_name,long order_no , String pickup_seal)throws Exception{
  		try{
  			DBRow data=new DBRow();
  			data.add("number", number);
  			data.add("number_type", number_type);
  			data.add("customer_id", customer_id);
  			data.add("company_id", company_id);
  			data.add("account_id", account_id);
  			data.add("door_name", door_name);
  			data.add("order_no", order_no);
  			data.add("out_seal", pickup_seal);
  			results.add("data",data);
  		}catch(Exception e){
  			throw new SystemException(e,"getData",log);
  		}
  	}
  	
    //安卓链接web 端实现打印功能接口    送货标签
  	public void androidDeliveryLabel(DBRow results,String customer_id,String company_id, int number_type,String number,long entry_id,long adid ,HttpServletRequest request,String door_name)throws Exception{
  		try{
  			
  			String bol_number="";
  			String ctnr_number="";
  			if(number_type==moduleKey.CHECK_IN_BOL){
  				bol_number=number;
  			}
  			if(number_type==moduleKey.CHECK_IN_CTN){
  				ctnr_number=number;
  			}
  			DBRow result=new DBRow();
  		//	DBRow ctnRow=sqlServerMgrZJ.findReceiptsrCompanyIdCustomerIdTypeByBolNoOrContainerNo(number,adid,request);
  			//if(((DBRow[])ctnRow.get("rows", new DBRow[0])).length==1){
  				result.add("path","check_in/print_receipts_wms_by_bol_container.html");
  				result.add("CompanyID", company_id);
  				result.add("CustomerID", customer_id);
  				result.add("bol_number", bol_number);
  				result.add("ctnr_number", ctnr_number);
  				result.add("entry_id", entry_id);
  				result.add("adid", adid);
  				result.add("door_name", door_name);
  		//	}
//  			else if(((DBRow[])ctnRow.get("rows", new DBRow[0])).length>1){
//  				DBRow[] rows = sqlServerMgrZJ.findReceiptsByBolNoOrContainerNo(bol_number, ctnr_number, "", "",adid,request);
//  				List<DBRow> companyList = new ArrayList<DBRow>();
//  				for(int i=0;i<rows.length;i++){
//  					DBRow row=new DBRow();
//  					row.add("CompanyID",rows[i].getString("CompanyID"));
//  					row.add("BOLNo",rows[i].getString("BOLNo"));
//  					row.add("ContainerNo",rows[i].getString("ContainerNo"));
//  					row.add("CustomerID",rows[i].getString("CustomerID"));
//  					companyList.add(row);
//  				}
//  				result.add("value",companyList.toArray(new DBRow[companyList.size()]));
//  				result.add("path","check_in/print_receipts_wms_by_bol_container.html");
//  				result.add("count",2);
//  			}
  			
  			results.add("delivery_label",result);
  		}catch(Exception e){
  			throw new SystemException(e,"androidCtnrWebPrint",log);
  		}
  	}
  	
  	//packing list
    public void getPackingList(DBRow result,long order_no,String number,int number_type,String company_id,String customer_id,long adid)throws SystemException{
    	try{
    		DBRow[] orders=null;
    		if(ModuleKey.CHECK_IN_LOAD == number_type){
	    		orders = sqlServerMgrZJ.findMasterBolLinesByMasterBolStr(number,"",company_id,customer_id,adid, null);
				if(0 == orders.length){
					orders = sqlServerMgrZJ.findOrdersNoAndCompanyByLoadNoOrderStr(number,"",company_id,customer_id,adid, null);
				}
	    	}else{
	    		orders = new DBRow[1];
				DBRow row = new DBRow();
				if(ModuleKey.CHECK_IN_ORDER == number_type){
					row.add("OrderNo", number);
				}else if(ModuleKey.CHECK_IN_PONO == number_type){
					row.add("OrderNo", order_no);
				}
				row.add("CompanyID", company_id);
				row.add("customerId", customer_id);
				orders[0] = row;
	    	}
    		result.add("packing_list",orders);
    	}catch(Exception e){
    		throw new SystemException(e,"getPackingList",log);
    	}
    }
  	
  	//counting sheet
  	public void getCountingSheet(DBRow result,int number_type)throws SystemException{
  		try{
  			DBRow countingRow=new DBRow();
  			if(number_type==ModuleKey.CHECK_IN_LOAD){
  				countingRow.add("path","check_in/a4_counting_sheet.html");
  			}else{
  				countingRow.add("path","check_in/a4_counting_sheet_order.html");
  			}
  			result.add("counting_sheet",countingRow);
  		}catch(Exception e){
  			throw new SystemException(e,"getCountingSheet",log);
  		}
  		
  	}
  	
  	//安卓 打印 返回BillOfLoading 集合
  	public void androidGetPrintList(DBRow result,String number,String company_id,String customer_id,long entry_id,int number_type,long order_no,long adid,HttpServletRequest request,String door_name,long equipment_id)throws Exception{
  		try{
			DBRow[] wmsRows=null;
			if(number_type==ModuleKey.CHECK_IN_LOAD){
				wmsRows=sqlServerMgrZJ.findBillOfLadingTemplateByLoad(number,company_id,customer_id,adid,request);
			}else if(number_type==ModuleKey.CHECK_IN_ORDER){
				wmsRows=sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(Long.parseLong(number), company_id, customer_id,adid,request);
			}else if(number_type==ModuleKey.CHECK_IN_PONO){
				wmsRows=sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(order_no, company_id, customer_id,adid,request);
			}
			this.findLableByName(wmsRows,number_type);
			
			//DBRow eqRow=this.floorCheckInMgrZwb.getEquimentNoByNumber(entry_id, number, number_type);
			DBRow eqRow=this.checkInMgrZwb.getEquipmentByEquipmentId(equipment_id);
			if(eqRow!=null){
				result.add("container_no",eqRow.getString("equipment_number"));
			}
	
			List<DBRow> masterList = new ArrayList<DBRow>(); 
			for(int i=0; wmsRows != null && wmsRows.length > 0 && i<wmsRows.length;i++){
				
				String masterBolNo=wmsRows[i].getString("master_bol_nos");
				String orderNo=wmsRows[i].getString("order_nos");

				String shipToName = wmsRows[i].getString("SHIPTONAME");
				String shipToID = wmsRows[i].getString("SHIPTOID");
				
				customer_id=wmsRows[i].getString("customer_id");
				company_id=wmsRows[i].getString("company_id");
				
				Boolean isSuit = false;
				DBRow targetRow=new DBRow();
				if(number_type==ModuleKey.CHECK_IN_LOAD){
					
					DBRow masterBol =(DBRow) wmsRows[i].get("MasterBOLFormatBAK",new DBRow());
					if(!masterBol.get("template_path", "").equals("")){
						String path=masterBol.get("template_path", "");
						String name=wmsRows[i].get("MasterBOLFormat", "error");
						
						targetRow.add("masterBolPath",path);
						targetRow.add("masterBolName",name);
						isSuit = true;
					}
				}
				DBRow Bol =(DBRow) wmsRows[i].get("BOLFormatBAK",new DBRow());
				
				if(!Bol.get("template_path", "").equals("")){
					String path=Bol.get("template_path", "");
					String name=wmsRows[i].get("BOLFormat", "error");
					targetRow.add("BolPath", path);
					targetRow.add("BolName", name);
					isSuit = true;
				}
				
				if(isSuit){
					targetRow.add("master_bol_no",masterBolNo);
					targetRow.add("order_no",orderNo);
					targetRow.add("number",number);
					targetRow.add("company_id", company_id);
					targetRow.add("number_type", number_type);
					targetRow.add("customer_id", customer_id);
					targetRow.add("door_name", door_name);
					targetRow.add("ship_to_name" , shipToName);
					targetRow.add("ship_to_id" , shipToID);
					
					masterList.add(targetRow);
				}
			}	
			result.add("bill_of_lading",masterList.toArray(new DBRow[masterList.size()])); 
		}catch(Exception e){
			throw new SystemException(e,"androidFindTemplate",log); 
		}
  	}
  	
  	 //load ticket
 	public void getWmsRow(DBRow result,int number_type,String number,String companyId, String customerId,long entry_id,long adid , HttpServletRequest request)throws Exception{
 		try{
 			DBRow wmsRow=new DBRow();
 			String wmsUrl="";
 			if(number_type==ModuleKey.CHECK_IN_LOAD){
 				int masterCount=sqlServerMgrZJ.findMasterBolNoCountByLoadNo(number, companyId, customerId,adid,request);
 	 		    if(masterCount!=0){
 	 		    	wmsUrl = "check_in/print_order_master_wms.html";
 	 			}else{
 	 				wmsUrl = "check_in/print_order_no_master_wms.html";
 	 		    }
 			}else{
 				wmsUrl = "print_order_no_master_wms_order.html";
 			}
 			
 		    DBRow mainRow=this.findGateCheckInById(entry_id);
 		    if(mainRow!=null){
 		    	wmsRow.add("entryId",entry_id);
 			    wmsRow.add("loadNo", number);
 			    wmsRow.add("order_no", number);
 			    wmsRow.add("window_check_in_time", mainRow.get("window_check_in_time", ""));
 			    wmsRow.add("company_name", mainRow.get("company_name", ""));
 			    wmsRow.add("gate_container_no", mainRow.get("gate_container_no", ""));
// 			    wmsRow.add("CompanyID", companyId);
// 			    wmsRow.add("CustomerID",customerId);
 			    wmsRow.add("path",wmsUrl);
 				   
 		    }
 		   result.add("loading_ticket",wmsRow);
 		}catch(Exception e){
 			throw new SystemException(e,"getWmsRow",log); 
 		}
 	}
  	
	//根据lable name查询lable详细
	public DBRow[] findLableByName(DBRow[] rows,long number_type) throws SystemException{
		try{
			for(int i=0; rows != null && i<rows.length;i++){		
				rows[i] =  floorCheckInMgrZwb.findLableByName(rows[i], rows[i].get("MasterBOLFormat", ""), rows[i].get("BOLFormat","error"), "", number_type); 
			}
			return rows;
		}catch (Exception e) {
			throw new SystemException(e,"filterCheckIn",log);
		}
	}
	
	//根据id查询主单据信息--打印用
	public DBRow findGateCheckInById(long id)throws Exception{
		try{
			return this.floorCheckInMgrZwb.findGateCheckInById(id);
		}catch(Exception e){
			throw new SystemException(e,"findGateCheckInById",log);
		}
	}
    
    
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
	
	
	
}

