package com.cwc.app.api.zwb;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.api.zj.PurchaseMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorApplyImagesMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyMoneyLogsMgrLL;
import com.cwc.app.floor.api.ll.FloorPurchaseMgrLL;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zwb.FloorPreparePurchaseFundsMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zwb.PreparePurchaseMgrIfaceZwb;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.PurchaseMoneyKey;
import com.cwc.app.lucene.zr.ApplyMoneyIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class PreparePurchaseMgrZwb implements PreparePurchaseMgrIfaceZwb{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorPreparePurchaseFundsMgrZwb floorPreparePuchasefunds;
	private AdminMgrIFace adminMgr;
	private SystemConfig systemConfig;
	private FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL;
	private FloorPurchaseMgrLL floorPurchaseMgrLL;
	private FloorPurchaseMgr fpm;
	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	private FloorProductMgr floorProductMgr;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private SupplierMgrIFaceTJH supplierMgrTJH;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorApplyImagesMgrLL floorApplyImagesMgrLL;
	private PurchaseMgr purchaseMgr ;
	private FloorTransportMgrZr floorTransportMgrZr;
	private AccountMgrIfaceZr accountMgrZr;
	//上传
	public String uploadImage(HttpServletRequest request) throws Exception{
			try {
				String permitFile = "jpg,gif,bmp";
				
				TUpload upload = new TUpload();
				upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));//文件名
				upload.setRootFolder("/upl_imags_tmp/");//文件保存路径
				upload.setPermitFile(permitFile);
 
				int flag = upload.upload(request);
				String fileName=upload.getFileName();
				StringBuffer fileNameSb = new StringBuffer();
				
				
				
				if(flag == 0){
					fileNameSb.append( upload.getRequestRow().getString("fileName")).append(",").append(fileName);
				}
				String returnUrl = upload.getRequestRow().getString("backurl")+fileNameSb.toString();
				returnUrl += "&flag="+flag;
				return returnUrl;
			} catch (Exception e) {
				throw new SystemException(e,"uploadImage",log);
			}
	}
	
	//确认可以添加申请定金
	public void affirmTransferPurchaseDeposit(HttpServletRequest request)
			throws Exception {

		{
			try {
				long purchase_id = StringUtil.getLong(request, "purchase_id");
		 
				String remark = StringUtil.getString(request, "remark");

				double amount = StringUtil.getDouble(request, "amount");
				
				//接收接口参数
				String admin_id=StringUtil.getString(request,"admin_id");
				String mail=StringUtil.getString(request,"mail");
				String pageMessage=StringUtil.getString(request,"pageMessage");
				String shortMessage=StringUtil.getString(request,"shortMessage");
				boolean ma=false;
				boolean pm=false;
				boolean sm=false;
				if(mail.equals("true")){
					ma=true;
				}
				if(pageMessage.equals("true")){
					pm=true;
				}
				if(shortMessage.equals("true")){
					sm=true;
				}			  
				String lastTime=StringUtil.getString(request,"last_time");
				lastTime+=" 23:59:59";
				Date date=(Date)Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String startTime = dateformat.format(date);
				
				String payee = StringUtil.getString(request, "payee");
				String bank_information = StringUtil.getString(request,"payment_information");
				
				String title="申请定金:"+amount+",关联采购单:"+purchase_id;
				String content=title+",最迟转款时间:"+lastTime+",收款人:"+payee;
				
				DBRow param = new DBRow();

				DBRow purchaseRow = floorPurchaseMgrLL.getPurchaseById(Long.toString(purchase_id));
				TDate tDate = new TDate();
				param.add("apply_money_over", tDate.getDiffDate(purchaseRow.getString("purchase_date"), "dd")- purchaseRow.get("price_affirm_over", 0));
				
				param.add("money_status", PurchaseMoneyKey.NOTTRANSFER);

				param.add("updatetime", DateUtil.NowStr());
				fpm.updatePurchase(purchase_id, param);

				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				String follower = adminLoggerBean.getEmploye_name();
				long follower_id = adminLoggerBean.getAdid();

				//添加资金的跟进日志
				addPurchasefollowuplogs(2, purchase_id, "申请定金:"+amount+","+remark, follower_id,follower);
				
				DBRow applyTemp = this.addPurchaseDeposit(lastTime,String.valueOf(adminLoggerBean.getAdid()), 100009,purchase_id, amount, payee, bank_information, remark,100019, 0, FinanceApplyTypeKey.PURCHASE_ORDER, 1, 0, 0, 0, 0, StringUtil.getString(request, "currency"), adminLoggerBean);// 采购单申请资金，成本中心，产品线暂时为0
				//在account表中添加记录
				accountMgrZr.addAccount(applyTemp.get("applyId", 0l), request);
			
				
				//添加关联文件重新命名文件名称.将文件移动到新的文件夹中
				String fileName = StringUtil.getString(request, "file_names");
				String sn = StringUtil.getString(request, "sn");
				String folder = StringUtil.getString(request, "folder");
				
				String[] arrayFile = null ;
				if(fileName.trim().length() > 0 ){
					arrayFile = fileName.split(",");
				}
				if(arrayFile != null && arrayFile.length > 0){
					for(String name : arrayFile){
						// 保存数据。移动文件
						StringBuffer realFileName = new StringBuffer(sn);
						realFileName.append("_").append(applyTemp.get("applyId", 0l)).append("_");
						String suffix = getSuffix(name);
						
					 
						realFileName.append(name.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfApplyMoneyImageByFileName(realFileName.toString());
						if(indexFileName != 0){
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						
						DBRow fileRow = new DBRow();
						fileRow.add("association_id", applyTemp.get("applyId", 0l));
						fileRow.add("association_type", FinanceApplyTypeKey.NO_ASOCIATE);
						fileRow.add("path", realFileName.toString());
						floorApplyImagesMgrLL.insertApplyImages(fileRow);
						String temp_url = Environment.getHome()+"upl_imags_tmp/"+name;
						String url = Environment.getHome()+"upload/"+folder+"/"+realFileName.toString();
						FileUtil.moveFile(temp_url,url);
						
					}	
				}
				//通知
				if(admin_id.trim().length() > 0 ){
					scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), admin_id, "", applyTemp.get("applyId", 0l), ModuleKey.APPLY_MONEY, title, content, pm, ma, sm, request, true, ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS);
				}

			} catch(Exception e){
				throw new SystemException(e,"affirmTransferPurchase",log);
			}
		}

	}
	
	
	//根据采购单id 查询是否申请了定金
	public long findPuerchaseDeposit(long purchaseId)throws Exception{
		return this.floorPreparePuchasefunds.findPuerchaseDeposit(purchaseId);
	}
	
	
	//添加定金
	public DBRow addPurchaseDeposit(String lastTime,String adid,
			long categoryId, long association_id, double amount, String payee,
			String paymentInformation, String remark, long center_account_id,
			long product_line_id, int association_type_id,
			long center_account_type_id, long center_account_type1_id,
			long payee_type_id, long payee_type1_id, long payee_id,
			String currency, AdminLoginBean adminLoggerBean) throws Exception {
		try 
		{
			double amount_transfer = 0d;
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid))
					.getString("account");
			DBRow row = new DBRow();
			row.add("last_time", lastTime);
			row.add("types", categoryId);
			row.add("association_id", association_id);
			row.add("amount", amount);
			row.add("amount_transfer", amount_transfer);
			row.add("payee", payee);
			row.add("payment_information", paymentInformation);
			row.add("create_time", DateUtil.NowStr());
			row.add("remark", remark);
			row.add("creater_id", adid);
			row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("association_type_id", association_type_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			if (currency.equals("RMB"))
			{
				row.add("standard_money", amount);
			}
			else
			{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			DBRow returnValue = new DBRow();
			
			long apply_id = floorPreparePuchasefunds.addPuerchaseDeposit(row);
			//申请资金添加索引
			applyMoneyMgrZZZ.editApplyMoneyIndex(apply_id, "add");
			
			
			returnValue.add("applyId", apply_id);
			returnValue.add("adid", adid);
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id),"添加资金申请:单号" + apply_id, adminLoggerBean.getAdid(),adminLoggerBean.getEmploye_name());
			purchaseMgr.addPurchasefollowuplogs(2, 0, association_id, "采购:申请定金:"+amount+currency+",资金单ID:"+apply_id, Long.valueOf(adid), userName);
			
			
			//这里要新添加账户信息在account表中.表示为资金的
			
			
			return (returnValue);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"prepareAddApplyMoney",log);
		}
	}
	
	//根据采购单id查询 预申请表 是否申请过   预申请只能申请一次
	public DBRow getPreparePurchaseFunds(long purchaseId)throws Exception{
		try{
			return this.floorPreparePuchasefunds.getPreparePurchaseFunds(purchaseId);
		}catch(Exception e){
			throw new SystemException(e,"getPreparePurchaseFunds",log);
		}
	}
	
	// 添加预申请
	public void affirmTransferPurchase(HttpServletRequest request)
			throws Exception {

		{
			try {
				long purchase_id = StringUtil.getLong(request, "purchase_id");
				long supplier_id = StringUtil.getLong(request, "supplier_id");
				String remark = StringUtil.getString(request, "remark");
				
				//接收接口参数
				String admin_id=StringUtil.getString(request,"admin_id");
				String mail=StringUtil.getString(request,"mail");
				String pageMessage=StringUtil.getString(request,"pageMessage");
				String shortMessage=StringUtil.getString(request,"shortMessage");
				boolean ma=false;
				boolean pm=false;
				boolean sm=false;
				if(mail.equals("true")){
					ma=true;
				}
				if(pageMessage.equals("true")){
					pm=true;
				}
				if(shortMessage.equals("true")){
					sm=true;
				}			  
				String lastTime=StringUtil.getString(request,"last_time");
				lastTime+=" 23:59:59";
				Date date=(Date)Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String startTime = dateformat.format(date);
				
				double amount = StringUtil.getDouble(request, "amount");
				String payee = StringUtil.getString(request, "payee");
				String bank_information = StringUtil.getString(request,"payment_information");
				
				String title="预申请资金:"+amount+",关联采购单:"+purchase_id;
				String content=title+",最迟转款时间:"+lastTime+",收款人:"+payee;
				
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				String follower = adminLoggerBean.getEmploye_name();
				long follower_id = adminLoggerBean.getAdid();

				// 预申请
				DBRow supplier = supplierMgrTJH.getDetailSupplier(supplier_id);
				
				
		        //调通知
				//scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
//				adid, 人的id字符串, "",
//				采购单id, MoudleKey.PURCHASE_ORDER, title, content,
//				页面, 邮件, 短信, request,“是否立即安排任务”, ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS);
				
				 DBRow row=this.prepareAddApplyMoney(lastTime,String.valueOf(adminLoggerBean.getAdid()), 100001l,purchase_id, amount, payee, bank_information, remark,100019, 0, 4, 1, 0, 0, 0, 0, "RMB", adminLoggerBean);// 采购单申请资金，成本中心，产品线暂时为0
				 
				//在account表中添加记录
				 accountMgrZr.addAccount(row.get("applyId", 0l), request);
				 
				//通知
				 if(admin_id != null && admin_id.length() > 0 ){
					 scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), admin_id, "", row.get("applyId", 0l), ModuleKey.PURCHASE_ORDER, title, content, pm, ma, sm, request, true, ProcessKey.PURCHASE_FRONT_MONEY);
				 }
	

			} catch(Exception e){
				throw new SystemException(e,"affirmTransferPurchase",log);
			}
		}

	}
		

	/**
	 * @param followup_type 日志记录类型1跟进日志2转账日志3上传excel
	 */
	private long addPurchasefollowuplogs(int followup_type,long purchase_id,String followup_content,long follower_id,String follower)
		throws Exception
	{
		DBRow dbrow=new DBRow();
		dbrow.add("followup_type", followup_type);
		dbrow.add("purchase_id", purchase_id);
		dbrow.add("followup_content",followup_content);
		dbrow.add("follower_id",follower_id);
		dbrow.add("follower",follower);
		dbrow.add("followup_date",DateUtil.NowStr());
		return (fpm.addPurchasefollowuplogs(dbrow));
	}

	// 添加预申请
	public DBRow prepareAddApplyMoney(String lastTime, String adid,
			long categoryId, long association_id, double amount, String payee,
			String paymentInformation, String remark, long center_account_id,
			long product_line_id, int association_type_id,
			long center_account_type_id, long center_account_type1_id,
			long payee_type_id, long payee_type1_id, long payee_id,
			String currency, AdminLoginBean adminLoggerBean) throws Exception {
		try 
		{
			double amount_transfer = 0d;
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid))
					.getString("account");
			DBRow row = new DBRow();
			row.add("last_time", lastTime);
			row.add("types", categoryId);
			row.add("association_id", association_id);
			row.add("amount", amount);
			row.add("amount_transfer", amount_transfer);
			row.add("payee", payee);
			row.add("payment_information", paymentInformation);
			row.add("create_time", DateUtil.NowStr());
			row.add("remark", remark);
			row.add("creater_id", adid);
			row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("association_type_id", association_type_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			if (currency.equals("RMB"))
				row.add("standard_money", amount);
			else
				row.add("standard_money", amount
						* Double.parseDouble(systemConfig
								.getStringConfigValue(currency)));
			DBRow returnValue = new DBRow();
			long apply_id = floorPreparePuchasefunds.addApplyMoney(row);
			

			
			returnValue.add("applyId", apply_id);
			returnValue.add("adid", adid);
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id),
					"添加资金申请:单号" + apply_id, adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name());
			return (returnValue);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"prepareAddApplyMoney",log);
		}
	}
	
	
	 /**
     * 获取所有的资金申请单     
     * @param pc
     * @return
     * @throws Exception
     */	
	public DBRow[] getAllPreparePurchase(PageCtrl pc) 
	throws Exception 
	{
		try
		{
			return floorPreparePuchasefunds.getAllPreparePurchase(pc);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAllApplyMoney",log);
		}
		
	}
	/**
	 * 根据条件过滤资金申请
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
    public DBRow[] getPreparePurchase(HttpServletRequest request,PageCtrl pc)
    throws Exception
    {
    	try
    	{
	    	String startTime=StringUtil.getString(request, "st");	
	    	String endTime=StringUtil.getString(request,"en");
	    	int types=StringUtil.getInt(request, "category");
	    	int status=StringUtil.getInt(request, "status");
	    	long productLineId=StringUtil.getLong(request, "productLine");
	    	long centerAccountId=StringUtil.getLong(request, "center_account_id");
	    	long association_type_id=StringUtil.getLong(request, "association_type_id");
	    	
	    	return floorPreparePuchasefunds.getPreparePurchase(startTime,endTime,types,status,productLineId,centerAccountId,association_type_id,pc);
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getPreparePurchase",log);
    	}
    }
    
  //根据id删除预申请
	public long detApplyMoney(long id)throws Exception{
		return floorPreparePuchasefunds.detApplyMoney(id);
	}
	
	//根据id 查询预申请
	public DBRow getByApplyMoney(long id)throws Exception{
		return this.floorPreparePuchasefunds.getByApplyMoney(id);
	}
	
	//根据id更新预申请
	public void updateApplyMoney(long apply_id,String adid,long categoryId,String association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,String association_type_id, long center_account_type_id, long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean) 
	throws Exception{
		try
		{
			SystemConfig systemConfig = new SystemConfig();   
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid)).getString("account");
			DBRow row=new DBRow();

			row.add("types",categoryId);
			row.add("association_id", association_id);
			row.add("association_type_id", association_type_id);
			row.add("amount", amount);
			row.add("payee",payee);
			row.add("payment_information", paymentInformation);					
			//row.add("create_time",DateUtil.NowStr());
			row.add("remark", remark);
			//row.add("creater_id", adid);
			//row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			if(currency.equals("RMB"))
				row.add("standard_money", amount);
			else
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			
			this.floorPreparePuchasefunds.updateApplyMoney(apply_id, row);
			
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id), "修改资金申请:单号"+apply_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());

			
			return;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoney",log);
		}
	}
	
	/**
	 * 交货单申请货款
	 */
	public DBRow transportApplyMoney(HttpServletRequest request)
			throws Exception
	{

		{
			DBRow result = new DBRow();
			try {
				long purchase_id = StringUtil.getLong(request, "purchase_id");
				long transport_id = StringUtil.getLong(request,"transport_id");
				String remark = StringUtil.getString(request, "remark");

				double amount = StringUtil.getDouble(request, "amount");
				
				//接收接口参数
				String admin_id=StringUtil.getString(request,"admin_id");
				String mail=StringUtil.getString(request,"mail");
				String pageMessage=StringUtil.getString(request,"pageMessage");
				String shortMessage=StringUtil.getString(request,"shortMessage");
				String currency = StringUtil.getString(request, "currency").trim();
				String fixCurrency = currency.toUpperCase();
				double fixAmount = amount ;
				if(!fixCurrency.equals("RMB")){
					fixAmount = MoneyUtil.round(amount * Double.parseDouble(systemConfig.getStringConfigValue(fixCurrency)),2);
				} 
				if(this.isTransportApplyMoneyAllow(purchase_id, fixAmount)){
					result.add("money_flag", "allow");
				}else{
					result.add("money_flag", "notallow");
					result.add("flag", "success");
					return result;
				}
				boolean ma=false;
				boolean pm=false;
				boolean sm=false;
				if(mail.equals("true")){
					ma=true;
				}
				if(pageMessage.equals("true")){
					pm=true;
				}
				if(shortMessage.equals("true")){
					sm=true;
				}			  
				String lastTime=StringUtil.getString(request,"last_time");
				lastTime+=" 23:59:59";
				Date date=(Date)Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String startTime = dateformat.format(date);
				
				String payee = StringUtil.getString(request, "payee");
				String bank_information = StringUtil.getString(request,"payment_information");
				
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				String follower = adminLoggerBean.getEmploye_name();
				long follower_id = adminLoggerBean.getAdid();

				TDate tDate = new TDate();
				String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
				DBRow row = new DBRow();
				row.add("transport_id", transport_id);
				row.add("transport_content", follower+"申请了T"+transport_id+"货款:"+amount+currency);
				row.add("transporter_id", follower_id);
				row.add("transporter", follower);
				row.add("transport_type", 2);
				row.add("activity_id", 0);
				row.add("transport_date", currentTime);
				floorTransportMgrZr.addTransportLog(row);
 
				DBRow applyTemp = this.addPurchaseDeposit(lastTime,String.valueOf(adminLoggerBean.getAdid()), 100001,transport_id, amount, payee, bank_information, remark,100019, 0, FinanceApplyTypeKey.DELIVERY_ORDER, 1, 0, 0, 0, 0, currency, adminLoggerBean);// 采购单申请资金，成本中心，产品线暂时为0
				
				//改变商品原有价格与该采购单价格一致
				DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id,null,null);
				for(int i=0;i<purchaseDetails.length;i++)
				{
					DBRow product = floorProductMgr.getDetailProductByPname(purchaseDetails[i].getString("purchase_name"));
					
					if (product!=null) //预防找不到商品报错
					{
						DBRow updateparam = new DBRow();
						updateparam.add("unit_price", purchaseDetails[i].get("price", 0.00));
						floorProductMgr.modProduct(product.get("pc_id", 0l), updateparam);
						
						//根据商品ID获得商品信息,记录商品修改日志
						DBRow product_log = floorProductMgr.getDetailProductByPcid(product.get("pc_id", 0l));
						
						product_log.add("account",adminLoggerBean.getAdid());
						product_log.add("edit_type",ProductEditTypeKey.UPDATE);
						product_log.add("edit_reason",ProductEditReasonKey.PURCHASE);
						product_log.add("post_date",DateUtil.NowStr());
						floorProductLogsMgrZJ.addProductLogs(product_log);
					}
				}
				
 				accountMgrZr.addAccount(applyTemp.get("applyId", 0l), request);
				
				
				String title="申请货款:F"+applyTemp.get("applyId", 0l)+",关联转运单:"+transport_id;
				String content=title+",最迟转款时间:"+lastTime+",收款人:"+payee+",申请金额:"+amount+currency;
				
				//添加关联文件重新命名文件名称.将文件移动到新的文件夹中
				String fileName = StringUtil.getString(request, "file_names");
				String sn = StringUtil.getString(request, "sn");
				String folder = StringUtil.getString(request, "folder");
				
				String[] arrayFile = null ;
				if(fileName.trim().length() > 0 ){
					arrayFile = fileName.split(",");
				}
				if(arrayFile != null && arrayFile.length > 0){
					for(String name : arrayFile){
						// 保存数据。移动文件
						StringBuffer realFileName = new StringBuffer(sn);
						realFileName.append("_").append(applyTemp.get("applyId", 0l)).append("_");
						String suffix = getSuffix(name);
						realFileName.append(name.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfApplyMoneyImageByFileName(realFileName.toString());
						if(indexFileName != 0){
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						DBRow fileRow = new DBRow();
						fileRow.add("association_id", applyTemp.get("applyId", 0l));
						fileRow.add("association_type", FinanceApplyTypeKey.NO_ASOCIATE);
						fileRow.add("path", realFileName.toString());
						floorApplyImagesMgrLL.insertApplyImages(fileRow);
						String temp_url = Environment.getHome()+"upl_imags_tmp/"+name;
						String url = Environment.getHome()+"upload/"+folder+"/"+realFileName.toString();
						FileUtil.moveFile(temp_url,url);
						
					}	
				}
				//通知
				if(admin_id.trim().length() > 0 ){
					scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), admin_id, "", applyTemp.get("applyId", 0l), ModuleKey.APPLY_MONEY, title, content, pm, ma, sm, request, true, ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS);
				}
				result.add("flag", "success");	
				return result ;
			} catch(Exception e){
				throw new SystemException(e,"transportApplyMoney",log);
			}
		}

	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	@Override
	public boolean isTransportApplyMoneyAllow(long purchase_id, double money)
			throws Exception
	{
 		//查询所有该purchase 申请的交货单转运的金额。然后和现在的金额比较
		//用standard_money 进行计算
		double purchaseDetailMoneny = purchaseMgr.getPurchasePrice(purchase_id);
		DBRow[] rows = floorPreparePuchasefunds.getApplyMoneysOfPurchase(purchase_id);
		DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,purchase_id,FinanceApplyTypeKey.PURCHASE_ORDER);
		double purchaseDeposit = 0.0;
		if(applyMoneys.length > 0)
		{
			purchaseDeposit = applyMoneys[0].get("standard_money", 0.0);
		}
		double sumOfApplyMoney = 0.0 ;
		if(rows != null && rows.length>0 )
		{
			for(DBRow row : rows)
			{
				sumOfApplyMoney += row.get("standard_money", 0.0d);
			}
		}
		double total = sumOfApplyMoney + money + purchaseDeposit;
		boolean result = true;
		if(purchaseDetailMoneny < total && Math.abs((purchaseDetailMoneny - total)) > (Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*purchaseDetailMoneny*0.01)){
			result = false;
		}
		return result;
 		
	}

	@Override
	public DBRow[] getApplyMoneyByAssociationIdAndTypeHasApply(long association_id,
			long type) throws Exception
	{
		try
		{
			return floorPreparePuchasefunds.getApplyMoneyByAssIdAndTypeHasApply(association_id, type);
		}catch (Exception e) 
		{
			throw new SystemException(e,"getApplyMoneyByAssociationIdAndType",log);
		}
	}

	public static void setLog(Logger log) {
		PreparePurchaseMgrZwb.log = log;
	}

	public void setFloorPreparePuchasefunds(
			FloorPreparePurchaseFundsMgrZwb floorPreparePuchasefunds) {
		this.floorPreparePuchasefunds = floorPreparePuchasefunds;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorApplyMoneyLogsMgrLL(
			FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL) {
		this.floorApplyMoneyLogsMgrLL = floorApplyMoneyLogsMgrLL;
	}

	public void setFloorPurchaseMgrLL(FloorPurchaseMgrLL floorPurchaseMgrLL) {
		this.floorPurchaseMgrLL = floorPurchaseMgrLL;
	}

	public void setFpm(FloorPurchaseMgr fpm) {
		this.fpm = fpm;
	}

	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) {
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}

	public void setSupplierMgrTJH(SupplierMgrIFaceTJH supplierMgrTJH) {
		this.supplierMgrTJH = supplierMgrTJH;
	}


	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setFloorApplyImagesMgrLL(FloorApplyImagesMgrLL floorApplyImagesMgrLL)
	{
		this.floorApplyImagesMgrLL = floorApplyImagesMgrLL;
	}

	public void setPurchaseMgr(PurchaseMgr purchaseMgr)
	{
		this.purchaseMgr = purchaseMgr;
	}

	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr)
	{
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	
	
	
	

}
