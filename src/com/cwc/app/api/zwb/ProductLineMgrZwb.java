package com.cwc.app.api.zwb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.zwb.FloorAdminMgrZwb;
import com.cwc.app.floor.api.zwb.FloorProductLineMgrZwb;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.iface.zwb.AdminMgrIfaceZwb;
import com.cwc.app.iface.zwb.ProductLineMgrIfaceZwb;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.ImportTitleRelatedExcelErrorKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import freemarker.log.Logger;

public class ProductLineMgrZwb implements ProductLineMgrIfaceZwb {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorProductLineMgrZwb floorProductLineMgrZwb;
	private FloorAdminMgrZwb floorAdminMgrZwb;
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	private AdminMgrIfaceZwb adminMgrZwb;
	
	//查询所有生产线
	@Override
	public DBRow[] getAllProductLine() throws Exception {
		
		return floorProductLineMgrZwb.getAllProductLine();
	}
	
	//查询所有供应商
	@Override
	public DBRow[] getAllSupplier() throws Exception {
		
		return floorProductLineMgrZwb.getAllSupplier();
	}
	
	//查询所有采购单
	@Override
	public DBRow[] getAllPurchase() throws Exception {
		
		return floorProductLineMgrZwb.getAllPurchase();
	}

	//根据生产线id查询供应商
	public DBRow[] getSupplier(long id) throws Exception {
		return floorProductLineMgrZwb.getSupplier(id);
	}
	
	public DBRow[] getPurchase(String supplierId) throws Exception {
		return floorProductLineMgrZwb.getPurchase(supplierId);
	}
	
	//根据商品id 采购单id查询 工厂类型
	public DBRow getFactorType(long purchaseId,long productId)throws Exception{
		DBRow dbrow=floorProductLineMgrZwb.getFactorType(purchaseId, productId);
		return dbrow;
	}
	
	//按搜索的值查询
	public DBRow[] seachProductBySeachValue(String type,String seachValue)throws Exception{
		return floorProductLineMgrZwb.seachProductBySeachValue(type, seachValue);
	}
	
	//android某分类下的所有商品信息
	public DBRow[] seachProductByCatalogIdType(long catalogId,String type,String seachValue)throws Exception{
		return this.floorProductLineMgrZwb.selectAllProductByProductCatalogId(catalogId, type, seachValue);
	}
	
	//根据产品线查询下面所有商品
	public DBRow[] selectProductByLineId(long productLineId,String type,String seachValue)throws Exception{
		return this.floorProductLineMgrZwb.selectAllProductByProductLineId(productLineId, type, seachValue);
	}
	
	//根据产品线id查询产品分类android用
	public DBRow[] getProductCatalogByProductLine(long productId) throws Exception{
		return this.floorProductLineMgrZwb.getProductLineIdInCatalog(productId);
	}
	
	//根据父id查询产品分类
	public DBRow[] getProductCatalogByParentId(long id)throws Exception{
		return this.floorProductLineMgrZwb.getProductCatalogByProductLine(id);
	}
	
	public DBRow selectProductByProductId(long productId)throws Exception{
		return this.floorProductLineMgrZwb.selectProductByProductId(productId);
	}
	
	//添加title 产品线-子分类-商品
	public void addProductLineTitle(DBRow row)throws Exception
	{
		//先验证此关系是否存在
		 long productLineId=row.get("tpl_product_line_id", 0l);
		 long titleId= row.get("tpl_title_id",0l);
		 if(0 == floorProprietaryMgrZyj.findProprietaryPcLineCountByTitleIdPcId(titleId, productLineId))
		 {
			 this.floorProductLineMgrZwb.addProductLineTitle(row);
		 
			 //查找所有子分类 并添加title
			 //根据产品线id 查询所有产品分类
			 DBRow[] catalogRow=floorProductLineMgrZwb.selectAllProductCatalogByLineId(productLineId);
			 for(int i=0;i<catalogRow.length;i++){
				 DBRow catalogTitleRow=new DBRow();
				 catalogTitleRow.add("tpc_product_catalog_id",catalogRow[i].get("id",0l));
				 catalogTitleRow.add("tpc_title_id",titleId);
				 //先验证关系是否存在
				 if(0 == floorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(titleId, catalogRow[i].get("id",0l)))
				 {
					 floorProductLineMgrZwb.addProductCatalogTitle(catalogTitleRow);
				 }
			 }
			 //根据产品线 id 查询出所有商品 添加title关系
			 DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductLineId(productLineId,"","");
			 for(int a=0;a<productRow.length;a++){
				DBRow productTitle=new DBRow();
				productTitle.add("tp_pc_id",productRow[a].get("pc_id", 0l));
				productTitle.add("tp_title_id",titleId);
				//先验证关系是否存在
				if(0 == floorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(titleId, productRow[a].get("pc_id", 0l)))
				{
					this.floorProductLineMgrZwb.addProductTitle(productTitle);
				}
			 }
		 }
	}
	
	//查询titleg根据产品线id
	public DBRow[] selectTitleByProductLine(long productId)throws Exception{
		return this.floorProductLineMgrZwb.selectTitleByProductLine(productId);
	}
	
	//添加title 产品分类-子分类-商品
	public void addProductCatalogTitle(long productCatalogId,long titleId)throws Exception{
		//添加title关系 所有子分类极其本身
		DBRow[] catalogRow=this.floorProductLineMgrZwb.selectAllProductCatalogByCatalogId(productCatalogId);
		for(int i=0;i<catalogRow.length;i++){
			DBRow row=new DBRow();
			row.add("tpc_product_catalog_id",catalogRow[i].get("pc_id", 0l));
			row.add("tpc_title_id",titleId);
			if(0 == floorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(titleId, catalogRow[i].get("pc_id", 0l)))
			{
				this.floorProductLineMgrZwb.addProductCatalogTitle(row);
			}
		}
		//子分类-商品 添加title关系
		DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductCatalogId(productCatalogId,"","");
		for(int a=0;a<productRow.length;a++){
			DBRow productTitle=new DBRow();
			productTitle.add("tp_pc_id",productRow[a].get("pc_id", 0l));
			productTitle.add("tp_title_id",titleId);
			if(0 == floorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(titleId, productRow[a].get("pc_id", 0l)))
			{
				this.floorProductLineMgrZwb.addProductTitle(productTitle);
			}
		}		
	}
	
	//根据产品分类id 查询title关系
	public DBRow[] selectTitleByProductCatalog(long productCatalogId)throws Exception{
		return this.floorProductLineMgrZwb.selectTitleByProductCatalog(productCatalogId);
	}
	
	//删除产品线 title关系 -产品分类title关系-商品title关系
	public void detTitleByProductLine(long productLineId,long titleId)throws Exception{
		  int value=this.floorProductLineMgrZwb.detTitleByProductLine(productLineId,titleId);
		  //查询产品线下的 子分类并删除 title 关系
		  //根据产品线id 查询所有产品分类
		  DBRow[] catalogRow=floorProductLineMgrZwb.selectAllProductCatalogByLineId(productLineId);
		  for(int i=0;i<catalogRow.length;i++){
			  long catalogid= catalogRow[i].get("id",0l);
			  //删除分类与title 关系
			  this.floorProductLineMgrZwb.detTitleByProductCatalog(catalogid,titleId);
		  }
		  DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductLineId(productLineId,"","");
		  for(int a=0;a<productRow.length;a++){
			 long productId=productRow[a].get("pc_id", 0l);
			 this.floorProductLineMgrZwb.detTitleByProduct(productId,titleId);
		  }
	}
	
	//删除产品分类 title关系    子产品分类-商品
	public int detTitleByProductCatalog(long productCatalogId,long titleId)throws Exception{
		//判断父级是否包含该title 如果包含不让删除
		String str=this.selectProductCatalogParent(productCatalogId);
		String ids=str.substring(0, str.length()-1);
		if(ids.equals("0")){  //可以删除
			DBRow[] dd=this.floorProductLineMgrZwb.selectProductLineTitleByCatalogId(productCatalogId);
			if(dd.length>0 && dd!=null){
				return 1;
			}else{
				DBRow[] catalogRow=this.floorProductLineMgrZwb.selectAllProductCatalogByCatalogId(productCatalogId);
				for(int i=0;i<catalogRow.length;i++){
					long catalogId=catalogRow[i].get("pc_id", 0l);
					//删除分类与title 关系
					this.floorProductLineMgrZwb.detTitleByProductCatalog(catalogId,titleId);
				}
				DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductCatalogId(productCatalogId,"","");
				for(int a=0;a<productRow.length;a++){
					long productId=productRow[a].get("pc_id", 0l);
					this.floorProductLineMgrZwb.detTitleByProduct(productId,titleId);
				}
				return 0;
			}
		}else{
			DBRow[] pd=this.floorProductLineMgrZwb.selectProductCatalogTitleByParentCatalogId(titleId, ids);
			if(pd.length > 0 && pd!=null){ //不可以删除有关系
				return 1;
			}else{
				DBRow[] dd=this.floorProductLineMgrZwb.selectProductLineTitleByCatalogId(productCatalogId);
				if(dd.length>0 && dd!=null){
					return 1;
				}else{
					DBRow[] catalogRow=this.floorProductLineMgrZwb.selectAllProductCatalogByCatalogId(productCatalogId);
					for(int i=0;i<catalogRow.length;i++){
						long catalogId=catalogRow[i].get("pc_id", 0l);
						//删除分类与title 关系
						this.floorProductLineMgrZwb.detTitleByProductCatalog(catalogId,titleId);
					}
					DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductCatalogId(productCatalogId,"","");
					for(int a=0;a<productRow.length;a++){
						long productId=productRow[a].get("pc_id", 0l);
						this.floorProductLineMgrZwb.detTitleByProduct(productId,titleId);
					}
					return 0;
				}
			}
		}
        
	}
	
	//根据产品分类id查询 该分类的所有父级并返回字符串
	public String selectProductCatalogParent(long productCatalogId)throws Exception{
		DBRow[] row=this.floorProductLineMgrZwb.selectProductCatalogParentByCatalogId(productCatalogId);
		String ids="";
		if(row.length > 0 && row!=null){
			long parentId=row[0].get("parentid", 0l);
			ids=parentId+",";
			while(parentId!=0){
				DBRow[] ro=this.floorProductLineMgrZwb.selectProductCatalogParentByCatalogId(parentId);
				if(ro.length > 0 && ro!=null){
					parentId=ro[0].get("parentid", 0l);
					ids+=parentId+",";
				}
			}
		}
		return ids;
	}
	
	//变更产品线与title关系
	public void updateTitleByProductLine(long productLineId,long titleId,long toTitleId)throws Exception{
		DBRow liRow=new DBRow();
		liRow.add("tpl_title_id",toTitleId);
		DBRow caRow=new DBRow();
		caRow.add("tpc_title_id",toTitleId);
		DBRow prRow=new DBRow();
		prRow.add("tp_title_id",toTitleId);
		if(0 == floorProprietaryMgrZyj.findProprietaryPcLineCountByTitleIdPcId(toTitleId, productLineId))
		{
			this.floorProductLineMgrZwb.updateTitleProductLine(productLineId,titleId,liRow);  
		}
		DBRow[] catalogRow=floorProductLineMgrZwb.selectAllProductCatalogByLineId(productLineId);
		for(int i=0;i<catalogRow.length;i++){
			  long catalogid= catalogRow[i].get("id",0l);
			  //变更分类与title 关系
			  if(0 == floorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(toTitleId, catalogid))
			  {
				  this.floorProductLineMgrZwb.updateTitleProductCatalog(catalogid,titleId,caRow);
			  }
		} 
		//根据产品线 id 查询出所有商品 添加title关系
		DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductLineId(productLineId,"","");
		for(int a=0;a<productRow.length;a++){
			long productId=productRow[a].get("pc_id", 0l);
		    //变更商品与title关系
			if(0 == floorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(toTitleId, productId))
			{
				this.floorProductLineMgrZwb.updateTitleProduct(productId,titleId,prRow);
			}
		} 	
	}
	
	//更变产品分类与title关系
	public void updateTitleByProductCatalog(long productCatalogId,long titleId,long toTitleId)throws Exception{
		DBRow caRow=new DBRow();
		caRow.add("tpc_title_id",toTitleId);
		DBRow prRow=new DBRow();
		prRow.add("tp_title_id",toTitleId);
	
		//添加title关系 所有子分类极其本身
		DBRow[] catalogRow=this.floorProductLineMgrZwb.selectAllProductCatalogByCatalogId(productCatalogId);
		for(int i=0;i<catalogRow.length;i++){
			long catalogId=catalogRow[i].get("pc_id", 0l);
			//变更分类与title 关系
			if(0 == floorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(toTitleId, catalogId))
			{
				this.floorProductLineMgrZwb.updateTitleProductCatalog(catalogId,titleId,caRow);
			}
		}
		//子分类-商品 添加title关系
		DBRow[] productRow=this.floorProductLineMgrZwb.selectAllProductByProductCatalogId(productCatalogId,"","");
		for(int a=0;a<productRow.length;a++){
		   long productId=productRow[a].get("pc_id", 0l);
		   //变更商品与title关系
		   if(0 == floorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(toTitleId, productId))
		   {
			   this.floorProductLineMgrZwb.updateTitleProduct(productId,titleId,prRow);
		   }
		}		
		
	}
	
	//根据产品id查询 商品与title关系表
	public DBRow[] selectProductTitleByProductId(long productId)throws Exception{
		return this.floorProductLineMgrZwb.selectProductTitleByProductId(productId);
	}
	
	//查询title下  可用总数量
	public DBRow selectTitleCount(long titleId,long productId)throws Exception{
		return this.floorProductLineMgrZwb.selectTitleCount(titleId, productId);
	}
	
	public DBRow[] selectLotNumberCount(long titleId,long productId,long ps_id,long lotNumber)throws Exception{
		return this.floorProductLineMgrZwb.selectLotNumberCount(titleId, productId,ps_id,lotNumber);
	}
	
	//商品 title 下某批次的 位置数量
	public DBRow[] selectLotNumberLocationCount(long pcId,long titleId,long lot_number,long ps_id)throws Exception{
		return this.floorProductLineMgrZwb.selectLotNumberLocationCount(pcId, titleId, lot_number,ps_id);
	}
	
	//商品title 位置下所有托盘 数量
	public DBRow[] selectLotNuberLpCount(long productId,long titleId,long locationId,long lotNumber)throws Exception{
		return this.floorProductLineMgrZwb.selectLotNuberLpCount(productId, titleId, locationId, lotNumber);
	}
	
	//容器类型总数
	public DBRow[] selectContainerTypeNumber(long productId,long titleId,long lotNumberId)throws Exception{
		return this.floorProductLineMgrZwb.selectContainerTypeNumber(productId, titleId, lotNumberId); 
	}
	
	//查询根据容器类型id0
	public DBRow[] selectContainerDetail(long productId,long titleId,long lotNumberId,long containerType)throws Exception{
		return this.floorProductLineMgrZwb.selectContainerDetail(productId, titleId, lotNumberId, containerType); 
	}
	
	//分仓库查库存
	public DBRow[] selectWarehouse(long productId,long titleId,long lot_number)throws Exception{
		return this.floorProductLineMgrZwb.selectWarehouse(productId, titleId, lot_number);
	}
	
	//根据托盘id查询托盘
	public DBRow selectLpByLpId(long lpId)throws Exception{
		return this.floorProductLineMgrZwb.selectLpByLpId(lpId);
	}
	
	//导出
	public String ajaxDownTitleList(HttpServletRequest request)throws Exception{
		
		String productLine=StringUtil.getString(request, "productLine");
		String productCatalog=StringUtil.getString(request,"productCatalog");
		long admin_user_id=StringUtil.getLong(request, "admin_user_id");
		String search_key=StringUtil.getString(request,"search_key");
		
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/proprietary/Title.xlsm"));//根据模板创建工作文件Title.xlsm
		
		wb.setSheetName(0,"Number One");
		XSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		XSSFRow row = sheet.getRow(0);  //得到Excel工作表的行 
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adminId = adminLoggerBean.getAdid();
		//如果是 用户登录 连带 把title插入到  用户下
		Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
		DBRow[] titles;
		
		if("".equals(search_key)){
			
			if(bl){
				
				titles=this.proprietaryMgrZyj.findProprietaryAllRelatesByAdminProductNameLineCatagory(true, admin_user_id,productLine,productCatalog,productCatalog,search_key, 0,0,null, request);
			}else{
				
				titles=this.proprietaryMgrZyj.findProprietaryAllRelatesByAdminProductNameLineCatagory(false, admin_user_id,productLine,productCatalog,productCatalog,search_key, 0,0,null, request);
			}
			
		}else{
			
			//索引搜索
			//titles = proprietaryMgrZyj.searchTitleByNumber(search_key, 1, null, 0, 4, request);
			DBRow title = floorProprietaryMgrZyj.getTitleCount();
			
			titles = proprietaryMgrZyj.getTitleByEso(search_key,title.get("count",0));
		}
		
		for(int i=0;i<titles.length;i++){
			row = sheet.createRow(i+1);
			row.createCell(1).setCellValue(titles[i].getString("title_name"));
		}
		String path;
		path = "upl_excel_tmp/ExportTitle_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsm";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	
	//上传
	public DBRow[] ajaxUploadTitleList(String filename,long adminId)throws Exception,FileTypeException{
		String path = "";
		InputStream is;
		Workbook wrb;
		List<DBRow> arrayDBRow = new ArrayList<DBRow>();
		path = Environment.getHome() + "upl_imags_tmp/" + filename;
		is = new FileInputStream(path);
		wrb = Workbook.getWorkbook(is);
		Sheet rs = wrb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		for(int i=1;i<rsRows;i++){
			 String titleName = rs.getCell(0,i).getContents().trim();
			 if(!titleName.equals("")){
				    //判断title是否存在  
					DBRow[] titleRow= this.floorProductLineMgrZwb.selectTitleByTitleName(titleName);
					//如果是 用户登录 连带 把title插入到  用户下
				    Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
				    if(bl){
				    	 //是用户登录    存在查询id 建立关系  不存在 建立title 建立关系
				    	if(titleRow!=null && titleRow.length>0){
				    		long titleId=titleRow[0].get("title_id", 0l);
				    		//验证用户与title下 是否重复  如果不重复 添加
							DBRow[] adRow=this.floorProductLineMgrZwb.findAdminTItleByTitleName(adminId,titleId);
								if(adRow!=null && adRow.length>0){
									DBRow errorRow=new DBRow();
									errorRow.add("titleName",titleName);
									errorRow.add("error","失败！该用户下已经包含该TITLE关系");
									arrayDBRow.add(errorRow);
								}else{
						    		//查询 最大排序
									DBRow sortRow=this.floorProductLineMgrZwb.findMaxSort();
									long maxSort=sortRow.get("sort",0l);
									DBRow adminRow=new DBRow();
									adminRow.add("title_admin_adid",adminId );
									adminRow.add("title_admin_title_id",titleId );
									adminRow.add("title_admin_sort",maxSort+1);
									this.floorProductLineMgrZwb.insertAdminTitle(adminRow);
									
									DBRow errorRow=new DBRow();
									errorRow.add("titleName",titleName);
									errorRow.add("error","成功");
									arrayDBRow.add(errorRow);
								}						    		
				    	}else{
								//可以添加
								DBRow row=new DBRow();
								row.add("title_name",titleName);
								long addTitleId=this.floorProprietaryMgrZyj.addProprietary(row);
								//查询 最大排序
								DBRow sortRow=this.floorProductLineMgrZwb.findMaxSort();
								long maxSort=sortRow.get("sort",0l);
								DBRow adminRow=new DBRow();
								adminRow.add("title_admin_adid",adminId );
								adminRow.add("title_admin_title_id",addTitleId );
								adminRow.add("title_admin_sort",maxSort+1);
								this.floorProductLineMgrZwb.insertAdminTitle(adminRow);
								
								DBRow errorRow=new DBRow();
								errorRow.add("titleName",titleName);
								errorRow.add("error","成功");
								arrayDBRow.add(errorRow);				    		
				    	}
				    }else{
						if(titleRow!=null && titleRow.length>0){
							//已经存在 把当前title 保存下来提示 失败
							DBRow errorRow=new DBRow();
							errorRow.add("titleName",titleName);
							errorRow.add("error","失败！title已经存在");
							arrayDBRow.add(errorRow);	
						}else{
							DBRow errorRow=new DBRow();
							errorRow.add("titleName",titleName);
							errorRow.add("error","成功");
							arrayDBRow.add(errorRow);
							//可以添加
							DBRow row=new DBRow();
							row.add("title_name",titleName);
							long addTitleId=this.floorProprietaryMgrZyj.addProprietary(row);
						}
				    }		
			 }
		}
		return arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
	}
	
	//产品线导入title 关系
	public DBRow[] ajaxUploadProductLineAndTitle(String filename,long adminId)throws Exception,FileTypeException{
		String path = "";
		InputStream is;
		Workbook wrb;
		List<DBRow> arrayDBRow = new ArrayList<DBRow>();
		path = Environment.getHome() + "upl_imags_tmp/" + filename;
		is = new FileInputStream(path);
		wrb = Workbook.getWorkbook(is);
		Sheet rs = wrb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		for(int i=1;i<rsRows;i++){
			 String titleName = rs.getCell(1,i).getContents().trim();
			 String productLineName=rs.getCell(0,i).getContents().trim();
			 if(!titleName.equals("") && !productLineName.equals("")){
				 //都不空 然后验证
				DBRow[] lineRow=this.floorProductLineMgrZwb.selectProductLineByName(productLineName);
				DBRow[] titleRow=this.floorProductLineMgrZwb.selectTitleByTitleName(titleName);
				if(lineRow!=null && lineRow.length>0 && titleRow!=null && titleRow.length>0){    //都存在可以添加关系
				    //获取lineid 获取titleid
					long titleId=titleRow[0].get("title_id", 0l);
					long productLineId=lineRow[0].get("id", 0l);
					//都存在 在验证关系是否重复
					DBRow[] lineTitleRow=this.floorProductLineMgrZwb.selectProductLineTitleByTitleIdAndLineId(titleId, productLineId);
					if(lineTitleRow!=null && lineTitleRow.length>0){
						//说明已经存在不能添加
						 DBRow errorRow=new DBRow();
							errorRow.add("titleName",titleName);
							errorRow.add("productLineName",productLineName);
							errorRow.add("error","失败！title和产品线的关系已经存在");
							arrayDBRow.add(errorRow);
					}else{
							//判断是否为客户登录  如果是客户登录 添加关系的时候 验证该用户下是否存在该title
							Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
						    if(bl){
						    	//客户登录时 验证 用户下是否有该title
						    	DBRow[] adRow=this.floorProductLineMgrZwb.findAdminTitleByTitleIdAndAdid(adminId,titleId);
						    	if(adRow!=null && adRow.length>0){
						    		 DBRow errorRow=new DBRow();
										errorRow.add("titleName",titleName);
										errorRow.add("productLineName",productLineName);
										errorRow.add("error","成功");
										arrayDBRow.add(errorRow);	
									
									DBRow row= new DBRow();
									row.add("tpl_product_line_id",productLineId);
									row.add("tpl_title_id",titleId);
									//可以添加
									this.addProductLineTitle(row);
						    	}else{
						    		 //说明已该用户下不包含该title在不能添加
									 DBRow errorRow=new DBRow();
									 errorRow.add("titleName",titleName);
									 errorRow.add("productLineName",productLineName);
									 errorRow.add("error","失败！登录用户下没有该TITLE");
									 arrayDBRow.add(errorRow);
						    	}
						    }else{
						    	 DBRow errorRow=new DBRow();
									errorRow.add("titleName",titleName);
									errorRow.add("productLineName",productLineName);
									errorRow.add("error","成功");
									arrayDBRow.add(errorRow);	
								
								DBRow row= new DBRow();
								row.add("tpl_product_line_id",productLineId);
								row.add("tpl_title_id",titleId);
								//可以添加
								this.addProductLineTitle(row);
						    }			
					}
				 }else{
					    DBRow errorRow=new DBRow();
						errorRow.add("titleName",titleName);
						errorRow.add("productLineName",productLineName);
						errorRow.add("error","失败！title或者产品线不存在");
						arrayDBRow.add(errorRow);	
				 }
			 }
		}
		return arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
	}
	
	//产品线导出title关系
	public String ajaxDownProductLineAndTitle(HttpServletRequest request)throws Exception{
		long title_id=StringUtil.getLong(request, "title_id");
		POIFSFileSystem fs = null;//获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product_line/ExportProductLineTitle.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
		wb.setSheetName(0,"第一张表单");
		HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		HSSFRow row = sheet.getRow(0);  //得到Excel工作表的行 
		
//		HSSFCell cell = row.createCell(0); 
//		HSSFCell cell1 = row.createCell(1); 
//		cell.setCellValue("ID");
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adminId = adminLoggerBean.getAdid();
		
		DBRow[] lineRow;
		Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
		if(bl){
			lineRow=this.floorProductLineMgrZwb.seachProductLineTitleBySeachValue(title_id, adminId);
		}else{
			lineRow=this.floorProductLineMgrZwb.selectProductLineTitleBySeachAdmin(title_id); //非客户登录
		}
		
		for(int i=0;i<lineRow.length;i++){
			row = sheet.createRow(i+1);
			row.createCell(0).setCellValue(lineRow[i].getString("name"));
			row.createCell(1).setCellValue(lineRow[i].getString("title_name"));
		}
		String path;
		path = "upl_excel_tmp/ExportProductLineTitle_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	
	//产品分类导出title的关系
	public String ajaxDownCatalogAndTitle(HttpServletRequest request)throws Exception{
		long title_id=StringUtil.getLong(request, "title_id");
//		POIFSFileSystem fs = null;//获得模板
//		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/Category.xlsm"));
		 
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product/Category.xlsm"));//根据模板创建工作文件
		wb.setSheetName(0,"NumberOne");
		XSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		XSSFRow row = sheet.getRow(0);  //得到Excel工作表的行 
		
//		AdminMgr adminMgr = new AdminMgr();
//		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
//		long adminId = adminLoggerBean.getAdid();
//		Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
//		DBRow[] catalogRow;
//		if(bl){
//			catalogRow=this.floorProductLineMgrZwb.seachProductCatalogTitleBySeachValue(title_id, adminId);
//		}else{
//			catalogRow=this.floorProductLineMgrZwb.selectProductCatalogTitleBySeachAdmin(title_id);
//		}
		DBRow[] catalogRow = floorProductLineMgrZwb.getAllProductCategoryDetail();
		
		for(int i=0;i<catalogRow.length;i++){
			row = sheet.createRow(i+1);
			row.createCell(1).setCellValue(catalogRow[i].getString("level1"));
			row.createCell(2).setCellValue(catalogRow[i].getString("level2"));
			row.createCell(3).setCellValue(catalogRow[i].getString("level3"));
		}
		String path;
		path = "upl_excel_tmp/ExportProductCatalog_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsm";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	public static void main(String[] args) {
		DBRow level1 = new DBRow();
		level1.add("", "");
	}
	//产品分类与title关系导入
	public DBRow[] ajaxUploadCatalogLineAndTitle(String filename,long adminId)throws Exception,FileTypeException{
		String path = "";
		InputStream is;
		Workbook wrb;
		List<DBRow> arrayDBRow = new ArrayList<DBRow>();
		path = Environment.getHome() + "upl_imags_tmp/" + filename;
		is = new FileInputStream(path);
		wrb = Workbook.getWorkbook(is);
		Sheet rs = wrb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		
		for(int i=1;i<rsRows;i++){
			 String titleName = rs.getCell(1,i).getContents().trim();
			 String catalogName=rs.getCell(0,i).getContents().trim();
			 if(!titleName.equals("") && !catalogName.equals("")){
				DBRow[] titleRow=this.floorProductLineMgrZwb.selectTitleByTitleName(titleName);
				DBRow[] catalogRow=this.floorProductLineMgrZwb.selectProductCatalogByCatalogName(catalogName);
				if(catalogRow!=null && catalogRow.length>0 && titleRow!=null && titleRow.length>0){    //都存在可以添加关系
					long titleId=titleRow[0].get("title_id", 0l);
					long catalogId=catalogRow[0].get("id", 0l);
					//都存在 在验证关系是否重复
					DBRow[] catalogTitleRow=this.floorProductLineMgrZwb.selectProductCatalogTitleByTitleIdAndCatalogId(titleId, catalogId);
					if(catalogTitleRow!=null && catalogTitleRow.length>0){
						//说明已经存在不能添加
						 DBRow errorRow=new DBRow();
							errorRow.add("titleName",titleName);
							errorRow.add("productCatalogName",catalogName);
							errorRow.add("error","失败！title和产品分类的关系已经存在");
							arrayDBRow.add(errorRow);
					}else{
						 //判断是否为客户登录  如果是客户登录 添加关系的时候 验证该用户下是否存在该title
						 Boolean bl=adminMgrZwb.findAdminGroupByAdminId(adminId);    //如果bl 是true 说明是客户登录
					     if(bl){
					    	//客户登录时 验证 用户下是否有该title
						    	DBRow[] adRow=this.floorProductLineMgrZwb.findAdminTitleByTitleIdAndAdid(adminId,titleId);
						    	if(adRow!=null && adRow.length>0){
						    		 DBRow errorRow=new DBRow();
										errorRow.add("titleName",titleName);
										errorRow.add("productCatalogName",catalogName);
										errorRow.add("error","成功");
										arrayDBRow.add(errorRow);	
												
									//可以添加
									this.addProductCatalogTitle(catalogId, titleId); 
						    	}else{
						    		//说明已经存在不能添加
									 DBRow errorRow=new DBRow();
										errorRow.add("titleName",titleName);
										errorRow.add("productLineName",catalogName);
										errorRow.add("error","失败！登录用户下没有该TITLE");
										arrayDBRow.add(errorRow);
						    	}
					     }else{
					    	 DBRow errorRow=new DBRow();
								errorRow.add("titleName",titleName);
								errorRow.add("productCatalogName",catalogName);
								errorRow.add("error","成功");
								arrayDBRow.add(errorRow);	
										
							//可以添加
							this.addProductCatalogTitle(catalogId, titleId); 
					     } 
					}
				}else{
					  DBRow errorRow=new DBRow();
						errorRow.add("titleName",titleName);
						errorRow.add("productCatalogName",catalogName);
						errorRow.add("error","失败！title或者产品分类不存在");
						arrayDBRow.add(errorRow);	
				}
			 }
			 
		}
		return arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
	}
	
	//导出产品与title的关系
	public String ajaxDownProductgAndTitle(HttpServletRequest request)throws Exception {
		
		long catalogId = StringUtil.getLong(request,"catalogId");
		long lineId = StringUtil.getLong(request,"lineId");
		long titleId = StringUtil.getLong(request, "titleId");
		int productType = StringUtil.getInt(request,"productType");
		String cmd = StringUtil.getString(request,"cmd");
		
		String key = StringUtil.getString(request,"key");
		
		/*int product_file_types = StringUtil.getInt(request, "product_file_types");
		int product_upload_status = StringUtil.getInt(request, "product_upload_status");*/
		
		POIFSFileSystem fs = null;//获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportProductTitles.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
		wb.setSheetName(0,"NumberOne");
		HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		HSSFRow row = sheet.getRow(0);  //得到Excel工作表的行 
		
		DBRow[] rows = new DBRow[0];
		
		if (cmd.equals("filter")) {
			
			rows = proprietaryMgrZyj.filterExportProductTitlesByPcLineCategoryTitleId(true, catalogId, lineId, productType, 0, 0, titleId, 0L, null, request);
		
		} else if (cmd.equals("name")) {
			
			rows = proprietaryMgrZyj.findExportDetailProductLikeSearch(true, key, titleId, 0L, null, request);
		
		} else {
			
			rows = proprietaryMgrZyj.findExportAllProductsByTitleAdid(true, titleId, 0L, null, request);
		}
		
		for(int i=0;i<rows.length;i++){
			
			row = sheet.createRow(i+1);
			row.createCell(0).setCellValue(rows[i].getString("p_code"));
			row.createCell(1).setCellValue(rows[i].getString("title_name"));
		}
		
		String path = "upl_excel_tmp/ExportProductTitles_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
		
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	
	//根据title查询产品下
	public DBRow[] selectProductLinewByTitleId(long titleId)throws Exception{
		return this.floorProductLineMgrZwb.selectProductLinewByTitleId(titleId);
	}
	
	//查询所有title不区分帐号
	public DBRow[] selectAllTitle()throws Exception{
		return this.floorProductLineMgrZwb.selectAllTitle();
	}
	
	//admin登录 根据titleid 查询产品线
	public DBRow[] selectProductLineAllByTitleId(String titleId)throws Exception{
		List<Long> titleIdList = new ArrayList<Long>();
		String[] titieIdArr = null;
		if(!StringUtil.isBlank(titleId))
		{
			titieIdArr = titleId.split(",");
			for (int i = 0; i < titieIdArr.length; i++)
			{
				if(!StringUtil.isBlankAndCanParseLong(titieIdArr[i]))
				{
					titleIdList.add(Long.valueOf(titieIdArr[i]));
				}
			}
		}
		return this.floorProductLineMgrZwb.selectProductLineAllByTitleId(titleIdList.toArray(new Long[0]));
	}
	
	//非客户 根据title查询产品分类
	public DBRow[] selectProductCatalogByTitleId(long titleId)throws Exception{
		return this.floorProductLineMgrZwb.selectProductCatalogByTitleId(titleId);
	}
	
	
    
	public void setFloorProductLineMgrZwb(
			FloorProductLineMgrZwb floorProductLineMgrZwb) {
		this.floorProductLineMgrZwb = floorProductLineMgrZwb;
	}

	public void setFloorAdminMgrZwb(FloorAdminMgrZwb floorAdminMgrZwb) {
		this.floorAdminMgrZwb = floorAdminMgrZwb;
	}

	public void setFloorProprietaryMgrZyj(
			FloorProprietaryMgrZyj floorProprietaryMgrZyj) {
		this.floorProprietaryMgrZyj = floorProprietaryMgrZyj;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public void setAdminMgrZwb(AdminMgrIfaceZwb adminMgrZwb) {
		this.adminMgrZwb = adminMgrZwb;
	}
	
	

}
