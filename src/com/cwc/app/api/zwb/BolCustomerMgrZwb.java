package com.cwc.app.api.zwb;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.zj.FloorLableTemplateMgrZJ;
import com.cwc.app.iface.zj.LableTemplateMgrIFaceZJ;
import com.cwc.app.iface.zwb.BolCustomerIfaceZwb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.json.JsonUtils;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.googlecode.jsonplugin.JSONUtil;

public class BolCustomerMgrZwb implements BolCustomerIfaceZwb{

		static Logger log = Logger.getLogger("ACTION");
		
		private LableTemplateMgrIFaceZJ lableTemplateMgrZJ;
		private FloorLableTemplateMgrZJ floorLableTemplateMgrZJ;
		
		
		/**
		 * 获得bol列表	 zwb
		 */
		public void getBolList(HttpServletRequest request,HttpServletResponse response,PageCtrl pc) throws Exception {     
			DBRow[] rows=lableTemplateMgrZJ.getAllTemplateLabel(pc);
			response.getWriter().print(JsonUtils.toJsonArray(rows));
		}
		
		/**
		 * 添加bol 模版 zwb
		 */
		public void addBol(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			 DBRow data=this.getParamFromRequest(request);
			 String template_name= data.get("TEMPLATE_NAME","");
			 int lable_type=data.get("LABLE_TYPE",0);
			 String printer=data.get("PRINTER", "");
			 String paper=data.get("PAPER", "");
			 float print_range_width=data.get("PRINT_RANGE_WIDTH", 0f);
			 float print_range_height=data.get("PRINT_RANGE_HEIGHT", 0f);
			 String template_path=data.get("TEMPLATE_PATH", "");
			 String img_path=data.get("IMG_PATH", "");
			 
				DBRow dbrow = new DBRow();
				dbrow.add("template_name",template_name);
				dbrow.add("lable_type",lable_type);
				dbrow.add("printer",printer);
				dbrow.add("paper",paper);
				dbrow.add("print_range_width",print_range_width);
				dbrow.add("print_range_height",print_range_height);
				dbrow.add("template_path",template_path);
				dbrow.add("img_path",img_path);
				long num=floorLableTemplateMgrZJ.addLableTemplate(dbrow);
				JSONObject json=new JSONObject();
				if(num>0){
					json.put("result", "success");
				}else{
					json.put("result", "error");
				}
				response.getWriter().print(json);
		}

		/**
		 * 编辑bol 模版 zwb
		 */
		public void editBol(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			
			 DBRow data=this.getParamFromRequest(request);
			 
			 long lable_template_id = data.get("LABLE_TEMPLATE_ID",0l);
			 
			 String template_name= data.get("TEMPLATE_NAME","");
			 int lable_type=data.get("LABLE_TYPE",0);
			 String printer=data.get("PRINTER", "");
			 String paper=data.get("PAPER", "");
			 float print_range_width=data.get("PRINT_RANGE_WIDTH", 0f);
			 float print_range_height=data.get("PRINT_RANGE_HEIGHT", 0f);
			 String template_path=data.get("TEMPLATE_PATH", "");
			 String img_path=data.get("IMG_PATH", "");
			
			DBRow para = new DBRow();
			para.add("template_name",template_name);
			para.add("lable_type",lable_type);
			para.add("printer",printer);
			para.add("paper",paper);
			para.add("print_range_width",print_range_width);
			para.add("print_range_height",print_range_height);
			para.add("template_path",template_path);
			para.add("img_path",img_path);
			
			floorLableTemplateMgrZJ.modLableTemplate(lable_template_id, para);
			JSONObject json=new JSONObject();
			
			json.put("result", "success");
			
			response.getWriter().print(json);
			
		}

		/**
		 * 删除bol 模版 zwb
		 */
		public void detBol(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			 DBRow data=this.getParamFromRequest(request);
			 long lable_template_id = data.get("LABLE_TEMPLATE_ID",0l);
			 floorLableTemplateMgrZJ.delLableTemplate(lable_template_id);
			 JSONObject json=new JSONObject();
			 json.put("result", "success");	
			 response.getWriter().print(json);
			
		}
		
		
	

		/**
		 * response 设置
		 * @response
		 */
		private void setResponse(HttpServletResponse response){
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		}
		
		/**
		 * 从request中取出参数值
		 * @param request
		 * @throws IOException 
		 * @throws JSONException 
		 */
		private DBRow getParamFromRequest(HttpServletRequest request) throws Exception{
			DBRow dbRow=new DBRow();
			//Step 1
			 String result=IOUtils.toString(request.getReader());
			 if(!StringUtil.isBlank(result)){
				 JSONObject jsonData = new JSONObject(result);
				 dbRow=DBRowUtils.convertToDBRow(jsonData);
			 }
			//Step 2
			Enumeration<String> paramEnum=request.getParameterNames();
			while (paramEnum != null &&paramEnum.hasMoreElements()) {
				String key = (String) paramEnum.nextElement();
				dbRow.add(key, StringUtil.getString(request, key));
			}
			return dbRow;
		}


		public void setLableTemplateMgrZJ(LableTemplateMgrIFaceZJ lableTemplateMgrZJ) {
			this.lableTemplateMgrZJ = lableTemplateMgrZJ;
		}

		public void setFloorLableTemplateMgrZJ(
				FloorLableTemplateMgrZJ floorLableTemplateMgrZJ) {
			this.floorLableTemplateMgrZJ = floorLableTemplateMgrZJ;
		}


		

}
