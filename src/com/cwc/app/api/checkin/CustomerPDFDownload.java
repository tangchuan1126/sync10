package com.cwc.app.api.checkin;


import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.checkin.CustomerPDFDownloadIFace;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CustomerPDFDownload implements CustomerPDFDownloadIFace {
    private static Logger log = Logger.getLogger("ACTION");
    private final static List<String> ALLOW_STORAGES = Arrays.asList("Valley", "Elwood", "Dallas", "DallasWildlife","Portage");
    private final static List<String> ALLOW_CUSTOMER_IDS = Arrays.asList("VIZIO", "VZB", "VZO", "VIZIO3", "VIZIO2","VIZIO1");

    private FloorCheckInMgrZwb floorCheckInMgrZwb;
    private FloorCheckInMgrWfh floorCheckInMgrWfh;

    @Override
    public DBRow[] findAllStorage() throws Exception {
        List<DBRow> results = new ArrayList<>();
        try {
            DBRow[] storages = this.floorCheckInMgrZwb.findSelfStorage();
            for (DBRow storage : storages) {
                String storageName = storage.get("ps_name", null);
                if (!ALLOW_STORAGES.contains(storageName)) {
                    continue;
                }

                long ps_id = storage.get("ps_id", 0l);
                storage.add("jet_lag", DateUtil.getStorageRawOffset(ps_id));
                results.add(storage);
            }

            DBRow[] resultArray = new DBRow[results.size()];
            return results.toArray(resultArray);
        } catch (Exception e) {
            throw new SystemException(e, "findSelfStorage", log);
        }
    }

    @Override
    public DBRow[] getAllCustomer() throws Exception {
        List<DBRow> results = new ArrayList<>();
        List<String> customerIdList = new ArrayList<>();

        try {
            DBRow[] allCustomers = this.floorCheckInMgrWfh.getAllCustomer();

            for (DBRow row : allCustomers) {
                if (row.get("customer_id", "").contains(",")) {
                    String[] customerIds = row.get("customer_id", "").split(",");
                    for (String customerId : customerIds) {
                        if (!ALLOW_CUSTOMER_IDS.contains(customerId)) {
                            continue;
                        }
                        if (!customerIdList.contains(customerId)) {
                            DBRow newRow = new DBRow();
                            newRow.add("customer_id", customerId);
                            results.add(newRow);
                            customerIdList.add(customerId);
                        }
                    }
                } else {
                    if (ALLOW_CUSTOMER_IDS.contains(row.get("customer_id", ""))) {
                        if (!customerIdList.contains(row.get("customer_id", ""))) {
                            results.add(row);
                            customerIdList.add(row.get("customer_id", ""));
                        }
                    }
                }
            }

            return results.toArray(new DBRow[results.size()]);
        } catch (Exception e) {
            throw new SystemException(e, "findTaskByNumber", log);
        }
    }


    public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
        this.floorCheckInMgrZwb = floorCheckInMgrZwb;
    }

    public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
        this.floorCheckInMgrWfh = floorCheckInMgrWfh;
    }
}

