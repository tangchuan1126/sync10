package com.cwc.app.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorSystemConfig;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.page.core.VelocityTemplate;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.cache.CacheMgr;
import com.cwc.cache._l1l1l1l1l1l1l1l1l;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.web.TextPara;
import com.cwc.web.WebRequest;

/**
 * 系统操作相关
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SystemConfig implements SystemConfigIFace
{
	static Logger log = Logger.getLogger("PLATFORM");
	private static DBRow cache = new DBRow();
	String dbRoot = "WEB-INF/backup/";
	String emailTemplate = "/";
	String templateRoot = "user_case/";

	private FloorSystemConfig fsc;
	private CacheMgr cacheMgr;

	/**
	 * 加载系统配置到内存（系统启动时调用）
	 * @throws Exception
	 */
	public void loadAppSystemConfig()
		throws Exception
	{
		try 
		{
			DBRow conf[] = getAllConfig(null);
			for (int i=0; i<conf.length; i++)
			{
				cache.add(conf[i].getString("confname"),conf[i]);
			}
			log.info("Load Visionari Syncing Config Parameters from Database successful!");
			
		}
		catch (Exception e)
		{
			log.error("SystemConfig loadAppSystemConfig error:"+e);
		}		
		
	}
	
	/**
	 * 清空内存系统配置（系统停止时调用）
	 *
	 */
	public static void cleanCache()
	{
		cache.clear();
		cache = null;
		log.info("Clean AppSystemConfig successful!");
	}
	
	/**
	 * 通过变量名称获得系统配置值
	 * @param confname
	 * @return
	 * @throws Exception
	 */
	public DBRow getConfigValueByName(String confname)
		throws Exception
	{
		try
		{
			Object obj = cache.get(confname,new Object());
						
			if ((obj instanceof DBRow)==false)		//有时候获得对象不一定为NULL，所以需要通过类型来判断
			{
				obj = fsc.getConfigValueByName(confname);
				cache.add(confname,obj);			//	把值放到内存
			}
			
			return((DBRow)obj);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getConfigValueByName",log);
		}
	}
	
	/**
	 * 通过变量名获得系统配置整形值
	 * @param confname
	 * @return
	 * @throws Exception
	 */
	public int getIntConfigValue(String confname)
		throws Exception
	{	
		try 
		{
			return(StringUtil.getInt( getStringConfigValue(confname) ));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getIntConfigValue",log);
		}
	}
	
	public float getFloatConfigValue(String confname)
		throws Exception
	{	
		try 
		{
			if (getStringConfigValue(confname).equals(""))
			{
				return(0);
			}
			else
			{
				return(Float.parseFloat( getStringConfigValue(confname) ));
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getIntConfigValue",log);
		}
	}
	
	public double getDoubleConfigValue(String confname)
		throws Exception
	{	
		try 
		{
			if (getStringConfigValue(confname).equals(""))
			{
				return(0);
			}
			else
			{
				return(Double.parseDouble( getStringConfigValue(confname) ));
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getDoubleConfigValue",log);
		}
	}
	
	/**
	 * 通过变量名获得系统配置字符串型值
	 * @param confname
	 * @return
	 * @throws Exception
	 */
	public String getStringConfigValue(String confname)
		throws Exception
	{
		DBRow row = getConfigValueByName(confname);
		if ( row==null )
		{
			//log.error("SystemConfig.getStringConfigValue(" + confname + ") error:result is NULL!");
			return("");
		}
		
		return(row.getString("confvalue"));
	}

	/**
	 * 通过变量名获得系统配置描述
	 * @param confname
	 * @return
	 * @throws Exception
	 */
	public String getStringConfigDescription(String confname)
		throws Exception
	{
		DBRow row = getConfigValueByName(confname);
		if ( row==null )
		{
			log.error("SystemConfig.getStringConfigDescription(" + confname + ") error:result is NULL!");
			return("");
		}
		
		return(row.getString("description"));
	}
	
	/**
	 * 获得所有系统配置
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllConfig(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			DBRow row[] = fsc.getAllConfig(pc);
			if ( row==null )
			{
				return(new DBRow[0]);
			}
			return(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllConfig",log);
		}
	}
	
	/**
	 * 修改系统配置
	 * @param request
	 * @throws Exception
	 */
	public void modifySystemConfig(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String confname = StringUtil.getString(request,"confname");
			String confvalue = StringUtil.getString(request,"confvalue");
			
			DBRow row = new DBRow();
			row.add("confvalue",confvalue);
			
			fsc.modifySystemConfig(confname,row);
			
			cache.remove(confname);		//	修改后需要把该缓存清除，以便重新使用的时候直接从数据库读取
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modifySystemConfig",log);
		}
	}

	/**
	 * 批量修改系统配置
	 * @param request
	 * @throws Exception
	 */
	public void batchModifySystemConfig(HttpServletRequest request)
		throws Exception
	{
		try
		{
			
			Enumeration variable = request.getParameterNames();
			String name;
			while( variable.hasMoreElements() )
			{
				name = (String)variable.nextElement();
				DBRow row = new DBRow();
				row.add("confvalue",StringUtil.getString(request,name));
				fsc.modifySystemConfig(name,row);
				
				cache.remove(name);//清空内存缓冲
			}	
			
			//如果商城LOGO有修改，删除旧的LOGO					
			if(!StringUtil.getString(request, "logo").equals(""))
			{
				FileUtil.moveImg(Environment.getHome()+"."+ConfigBean.getStringValue("upload_pro_img_tmp")+StringUtil.getString(request, "logo"), Environment.getHome()+"."+ConfigBean.getStringValue("upload_pro_img")+StringUtil.getString(request, "logo"));
				
				if (getConfigValueByName("logo").equals(StringUtil.getString(request, "logo")))
				{
					FileUtil.delFile(Environment.getHome()+"."+ConfigBean.getStringValue("upload_pro_img")+StringUtil.getString(request, "logo"));
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModifySystemConfig",log);
		}
	}

	/**
	 * 备份商城数据库
	 * @throws Exception
	 */
	public void backupDB()
		throws Exception
	{
		String need2BackUpFolder[] = new String[]		//需要备份的系统文件夹
		                                        {
													//Environment.getHome() + "WEB-INF/clients_index",
													//Environment.getHome() + "WEB-INF/index"
		                                        };
		/**
		//分解当前日期
		String date = DateUtil.NowStr();
		date = StrUtil.replaceString(date,"-","");
		date = StrUtil.replaceString(date," ","");
		date = StrUtil.replaceString(date,":","");

		String bkDateFolderPath = Environment.getHome()+dbRoot+date;
		String bkFolderPath = Environment.getHome()+dbRoot+date+"/database";
		
		try 
		{
			FileUtil.createDirectory(bkDateFolderPath);		//创建日期目录
			FileUtil.createDirectory(bkFolderPath);			//创建数据库SQL日期目录
		} 
		catch (Exception e1)
		{
			log.error("SystemConfig.backupDB("+Environment.getHome()+dbRoot+date+") error:" + e1);
		}

		
		
		//备份数据库，生成SQL脚本
		String tbname;
		ArrayList al = fsc.getAllTables();		//获得所有表名
		
		for (int i=0; i<al.size(); i++)
		{
			tbname = al.get(i).toString();
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(bkFolderPath+"/"+tbname+".sql"),"UTF-8"));
				
			try 
			{
				fsc.getTableDataSQL(tbname,out);	//把表的创建和表的数据写入文件
			} 
			catch (IOException e) 
			{
				log.error("SystemConfig.backupDB() error:" + e);
			}
			finally
			{
				if ( out!=null )
				{
					out.close();
				}
			}
		}
		
		try 
		{
			CompressFile.zipFolder(bkFolderPath, bkFolderPath);			//创建数据库备份压缩包
			FileUtil.delFolder(bkFolderPath);							//删除临时文件夹
			
			//备份系统文件夹
			for (int i=0; i<need2BackUpFolder.length; i++)
			{
				FileUtil.copy2zip(need2BackUpFolder[i], bkDateFolderPath);
			}
		} 
		catch (Exception e1)
		{
			log.error("SystemConfig.backupDB("+Environment.getHome()+dbRoot+date+") error:" + e1);
		}
		**/
	}
	
	//Runtime.getRuntime().exec
	public void backupDBByMysqlCommand() 
		throws Exception
	{
		try
		{
			String cmd = "cmd /c mysqldump -u root -proot oscommerce > d:/1.sql";
			cmd = "cmd /c dir";
			Process proc = Runtime.getRuntime().exec(cmd);
		    StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "Error");           
	        StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "Output");
	        errorGobbler.start();
	        outputGobbler.start();
	        //proc.waitFor();
		}
		catch(Exception e)
		{
				throw new SystemException(e,"backupDBByMysqlCommand",log);
		}
	}
	
	/**
	 * 检查当前数据库备份数是否超过系统配置
	 * 如果当前备份数等于系统配置数，则删除最早一个备份
	 * @throws Exception
	 */
	public void checkCount()
		throws Exception
	{
		//从系统配置获得数据库备份数
		int c = getIntConfigValue("backup_db_count");
		
		String f[] = FileUtil.getFolderFromFolder(Environment.getHome()+dbRoot);
		Arrays.sort(f);		
		if (f.length==c)
		{
			FileUtil.delFolder(Environment.getHome()+dbRoot+f[0]);
		}
	}
	
	/**
	 * 恢复数据库备份
	 * @param tbname
	 * @throws Exception
	 */
	public void restoreTable(String tbname)
		throws Exception
	{
		try
		{
			String sql[] = FileUtil.readFile2Array(tbname,"UTF-8");
			fsc.batchUpdate(sql);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"restoreTable",log);
		}
	}
	
	/**
	 * 获得所有备份数据库
	 * @return
	 * @throws Exception
	 */
	public String[] getAllDBBK()
		throws Exception
	{
		String f[] = FileUtil.getFolderFromFolder(Environment.getHome()+dbRoot);
		Arrays.sort(f);		//	排序
		convertArray(f);	//反序
		return(f);
	}
	
	/**
	 * 获得一个备份文件夹里面的备份文件
	 * @param folder
	 * @return 
	 * @throws Exception
	 */
	public DBRow[] getAllDBBKTableSql(String folder)
		throws Exception
	{
		String f[] = FileUtil.getFileList(Environment.getHome()+dbRoot+folder);

		ArrayList al = new ArrayList();
		for (int i=0; i<f.length; i++)
		{
			DBRow row = new DBRow();
			row.add("name",f[i]);
			row.add("size",(new File(Environment.getHome()+dbRoot+folder+"/"+f[i])).length());
			
			al.add(row);
		}
		
		return((DBRow[])al.toArray(new DBRow[0]));
	}
	
	/**
	 * 反序
	 * @param tmp
	 */	
	private void convertArray(String tmp[])
	{
		String ttt;
		for (int i=0; i<tmp.length/2; i++)
		{
			ttt = tmp[i];
			tmp[i] = tmp[tmp.length-i-1];
			tmp[tmp.length-i-1] = ttt;
		}
	}
	
	/**
	 * 从备份文件恢复数据库
	 * @param request
	 * @throws Exception
	 */
	public void importDB(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String folder = StringUtil.getString(request,"folder");
			String tablename[] = request.getParameterValues("tablename");		//需要恢复哪一份数据库
			
			for (int i=0; tablename!=null&&i<tablename.length; i++)
			{
				restoreTable(Environment.getHome()+dbRoot+folder+"/"+tablename[i]);					//导入
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importDB",log);
		}
	}
	
	/**
	 * 删除备份
	 * @param request
	 * @throws Exception
	 */
	public void delDataBaseBK(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String folder = StringUtil.getString(request,"folder");
			FileUtil.delFolder(Environment.getHome()+dbRoot+folder);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delDataBaseBK",log);
		}
	}
	
	/**
	 * 执行数据库备份
	 * @throws Exception
	 */
	public void backupDBNow()
		throws Exception
	{
		try
		{
			checkCount();
			backupDB();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"backupDBNow",log);
		}
	}
	
	/**
	 * 不在使用
	 * 把备份数据库脚本输出到流
	 * @param file
	 * @param out
	 * @throws Exception
	 */
	public void getTableSqlString(String file,JspWriter out)
		throws Exception
	{		
		try 
		{
			String fileStr;
			File f = new File(Environment.getHome()+dbRoot+"/"+file);
			
			InputStreamReader read = new InputStreamReader (new FileInputStream(f),"utf-8");
			BufferedReader buf = new BufferedReader(read);
			while ( (fileStr=buf.readLine())!=null )
			{
				out.write(fileStr);
				out.write("\n\r");
			}
			buf.close();
			f = null;
		}
		catch (IOException e)
		{
			throw new SystemException(e,"getTableSqlString",log);
		}
	}
	
	/**
	 * 获得所有邮件模板
	 * @return
	 * @throws Exception
	 */
	public String[] getAllEmailTemplate()
		throws Exception
	{
		String f[] = FileUtil.getFileList(Environment.getHome()+ConfigBean.getStringValue("email_template")+emailTemplate);
		return(f);
	}
	
	/**
	 * 获得邮件模板详细内容
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String getDetailEmailTemplate(String name)
		throws Exception
	{
		String content = null;
		
		try
		{
			content = FileUtil.readFile2String(Environment.getHome()+ConfigBean.getStringValue("email_template")+emailTemplate+name,"utf-8");
		}
		catch (IOException e)
		{
			throw new SystemException(e,"getDetailEmailTemplate",log);
		}
		
		return(content);
	}
	
	/**
	 * 修改邮件模板
	 * @param request
	 * @throws Exception
	 */
	public void modEmailTemplate(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String name = StringUtil.getString(request,"name");
			String title = StringUtil.getString(request,"title");
			String content = StringUtil.getString(request,"content");
			content = "<title>"+title+"</title>"+content;
			FileUtil.createFile(Environment.getHome()+ConfigBean.getStringValue("email_template")+emailTemplate+name,"utf-8",content);
		}
		catch (IOException e)
		{
			throw new SystemException(e,"modEmailTemplate",log);
		}
	}
	
	/**
	 * 转换邮件模板中图片的URL
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String replaceHtmlImgSrc(String content)
		throws Exception
	{
		content = StringUtil.regReplace(content,"src=\\s*[\"']((/{1}.+-*)+[.]{1}(gif|jpg|jpeg|jpe|png|bmp|swf))[\"']","src=\""+getStringConfigValue("url")+"$1\"");
		content = StringUtil.replaceString(content, "../../upl_imags",getStringConfigValue("url")+ConfigBean.getStringValue("systenFolder")+"upl_imags");
		
		return(content);
	}
	
	/**
	 * 预览邮件模板
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String getReviewEmailTemplate(String name)
		throws Exception
	{
		String content = null;
		
		try
		{
			content = FileUtil.readFile2String(Environment.getHome()+ConfigBean.getStringValue("email_template")+emailTemplate+name,"utf-8");
			content = this.replaceHtmlImgSrc(content);
		}
		catch (IOException e)
		{
			throw new SystemException(e,"getReviewEmailTemplate",log);
		}
		
		return(content);
	}

	/**
	 * 刷新网站模板
	 * 新增加了网站模板，需要刷新，以便把模板数据同步到数据库中
	 * @throws Exception
	 */
	public void flushTemplate()
		throws Exception
	{
		try
		{
			ArrayList al = new ArrayList();
			
			DBRow usedTemplate = fsc.getUsedTemplate();		//当前正在使用的模板
			String template[] = FileUtil.getFolderFromFolder(Environment.getHome()+templateRoot);		//从模板目录中获得所有模板
			
			for (int i=0; i<template.length; i++)
			{
				al.add(template[i]);
			}

			//为了容错，如果当前数据库配置的网站模板在实际的模板目录中不存在，则使用默认模板
			if (!al.contains(usedTemplate.getString("name")))
			{
				usedTemplate.add("name","default");
			}
			
			//清空数据库记录的网站模板
			fsc.delAllTemplate();
			
			for (int i=0; i<template.length; i++)
			{
				DBRow newTemplate = new DBRow();
				newTemplate.add("name",template[i]);
				if (template[i].equals( usedTemplate.getString("name") ))
				{
					newTemplate.add("used","1");		//当前使用模板			
				}
				else
				{
					newTemplate.add("used","0");
				}
				fsc.addTemplate(newTemplate);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"SystemConfig.flushTemplate() error:",log);
		}
	}

	/**
	 * 把模板设置为当前使用模板
	 * @param request
	 * @throws Exception
	 */
	public void markUsedTemplate(HttpServletRequest request)
		throws Exception
	{
		try
		{
			
			//需要使用的模板名称
			String name = StringUtil.getString(request,"name");
			
			//当前正在使用的模板
			DBRow usedTemplate = fsc.getUsedTemplate();
			
			//把之前模板标记为不在使用
			DBRow r1 = new DBRow();
			r1.add("used","0");
			fsc.modTemplateByName(usedTemplate.getString("name"), r1);
			
			//把目标模板标记为正在使用
			DBRow r2 = new DBRow();
			r2.add("used","1");
			fsc.modTemplateByName(name, r2);
			
			//因为修改了模板目录，重新初始化模板配置
			VelocityTemplate.changeTemplate();
			
			//清空所有静态缓存和内存缓存
			flushAllHtmlCache();
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"markUsedTemplate",log);
		}
		
		
	}

	/**
	 * 获得所有模板
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTemplate()
		throws Exception
	{
		try 
		{
			
			return(fsc.getAllTemplate());
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getAllTemplate",log);
		}
	}
	
	/**
	 * 获得正在使用的模板
	 * @return
	 * @throws Exception
	 */
	public String getUsedTemplateName()
		throws Exception
	{
		try 
		{
			return(fsc.getUsedTemplate().getString("name"));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getUsedTemplateName",log);
		}
	}

	/**
	 * 清空内存cache和HTML cache
	 * @throws Exception
	 */
	public void flushAllHtmlCache()
		throws Exception
	{
		cacheMgr.flushAll();
		
		_l1l1l1l1l1l1l1l1l l1l1l1l1l1l1l1l = new _l1l1l1l1l1l1l1l1l();
		l1l1l1l1l1l1l1l.flushAll();
	}
		
	/**
	 * 根据一个日期，计算距离现在多少天
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public int getNowSubDay(String date)
		throws Exception
	{
		TDate tDate = new TDate();
		long currentDateTime = tDate.getDateTime();
		long datetime = tDate.getDateTime( date );
		
		return( (int)( (currentDateTime-datetime)/1000/86400 ) );
	}
	
	/**
	 * 清空多余日志
	 *
	 */
	public void delOutDayLogs()
	{
		String path = Environment.getHome()+"."+"/WEB-INF/logs";
		String logFiles[] = FileUtil.getFileList(path);
		int dayOut = 10;		//只保留10天系统日志
			
		try 
		{
			/**
			 * 根据日志文件名的日期，判断是否超过10天，如果超过了
			 * 则删掉
			 */			
			for (int i=0; i<logFiles.length; i++)
			{
				if ( logFiles[i].indexOf("log_")>=0 )
				{
					if (getNowSubDay(logFiles[i].split("_")[1])>dayOut)
					{
						FileUtil.delFile(path+"/"+logFiles[i]);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("SystemConfig.delOutDayLogs() error:" + e);
		}
	}

	/**
	 * 记录全系统所有操作日志（page和action）
	 * @param adminLoggerBean
	 * @param opertate_type
	 * @param operation
	 * @param operate_para
	 * @param ip
	 * @param ref
	 * @param visitor
	 * @throws Exception
	 */
	public void addSysLog(AdminLoginBean adminLoggerBean,int operate_type,String operation,String operate_para,String ip,String ref,String visitor)
		throws Exception
	{
		try
		{
			TDate td = new TDate();
			long datemark = td.getDateTime();
			
			String account = "null";
			long adid = 0;
			long adgid = 0;
			
			if (adminLoggerBean!=null)
			{
				account = adminLoggerBean.getAccount();
				adid = adminLoggerBean.getAdid();
				adgid = adminLoggerBean.getAdgid();
			}
			
			//防止字符串超长
			if (operation.length()>900)
			{
				operation = StringUtil.cutString(operation, 900, "......");
			}
			
			if (operate_para.length()>900)
			{
				operate_para = StringUtil.cutString(operate_para, 900, "......");
			}
			
			if (ip==null)
			{
				ip = "NULL";
			}
			
			if (ref==null)
			{
				ref = "NULL";
			}
			
			if (visitor==null)
			{
				visitor = "NULL";
			}
			
			DBRow log = new DBRow();
			
			log.add("account", account);
			log.add("adid", adid);
			log.add("adgid", adgid);
			log.add("operate_type", operate_type);
			log.add("operation", operation);
			log.add("operate_para", operate_para);
			log.add("post_date", DateUtil.NowStr());
			log.add("datemark", datemark);
			log.add("ip", ip);
			log.add("ref", ref);
			log.add("visitor", visitor);
			
			fsc.addSysLog(log);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addSysLog("+operation+")",log);
		}
	}
	
	/**
	 * 通过管理员ID和日期，获得系统日志
	 * @param st_datemark
	 * @param en_datemark
	 * @param adid
	 * @param opertate_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSysLogsByAdidDate(long st_datemark,long en_datemark,long adid,int opertate_type,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fsc.getSysLogsByAdidDate( st_datemark, en_datemark, adid, opertate_type,pc));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getSysLogsByAdidDate",log);
		}
	}

	/**
	 * 通过管理员角色和日期，获得系统日志
	 * @param st_datemark
	 * @param en_datemark
	 * @param adgid
	 * @param opertate_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSysLogsByAdgidDate(long st_datemark,long en_datemark,long adgid,int opertate_type,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fsc.getSysLogsByAdgidDate( st_datemark, en_datemark, adgid, opertate_type,pc));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getSysLogsByAdgidDate",log);
		}
	}

	/**
	 * 
	 * @param st_datemark
	 * @param en_datemark
	 * @param opertate_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllSysLogsByDate(long st_datemark,long en_datemark,int opertate_type,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fsc.getAllSysLogsByDate( st_datemark, en_datemark, opertate_type,pc));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getAllSysLogsByDate",log);
		}
	}
	
	public DBRow getDetailSysLogBySlid(long sl_id)
		throws Exception
	{
		return( fsc.getDetailSysLogBySlid(sl_id) );
	}
	
	/**
	 * 获得当前货币实时汇率
	 * @param currency
	 * @return
	 * @throws Exception
	 */
	public double getCurrencyRate(String currency)
		throws Exception
	{
		String strUrl = "https://www.google.com/finance?q="+currency+"CNY";
	  
	    String str = this.getURLOutput(strUrl);
	    String cc[] = str.split("<span class=bld>");
		String bb[] = cc[1].split("CNY</span>");
	    double c = Double.valueOf(bb[0]);
		return(MoneyUtil.round(c,2));
	}
	
	/**
	 * 批量更新汇率
	 * @throws Exception
	 */
	public void batchUpdateCurrencyRate()
		throws Exception
	{
		try
		{
			String currency[] = new String[]{"USD","AUD","EUR","CAD","CHF","CZK","GBP","NOK"};
			
			for (int i=0; i<currency.length; i++)
			{
				double rate = this.getCurrencyRate(currency[i]);
				if (rate>0)
				{
					DBRow row = new DBRow();
					row.add("confvalue",rate);
					fsc.modifySystemConfig(currency[i],row);
					cache.remove(currency[i]);		//	修改后需要把该缓存清除，以便重新使用的时候直接从数据库读取
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchUpdateCurrencyRate",log);
		}
	}
		
	
	
	
	
	
	public void setFsc(FloorSystemConfig fsc)
	{
		this.fsc = fsc;
	}

	public void setCacheMgr(CacheMgr cacheMgr)
	{
		this.cacheMgr = cacheMgr;
	}
	
	class StreamGobbler extends Thread {
		 private InputStream is;
		 private String type;

		 StreamGobbler(InputStream is, String type)
		 {
			  this.is = is;
			  this.type = type;
		 }

		 public void run() 
		 {
			  try 
			  {
				   InputStreamReader isr = new InputStreamReader(is);
				   BufferedReader br = new BufferedReader(isr);
				   String line = null;
				   while ((line = br.readLine()) != null)
				   {
					    if (type.equals("Error"))
					    {
					    	////system.out.println(line);
					    	log.error(line);
					    }
					    else
					    {
					    	////system.out.println(line);
					    	log.info(line);
					    }
				   }
			  } 
			  catch (IOException ioe)
			  {
				  log.error(this.getName()+":"+ioe);
			  }
		 }
	}

	public String getURLOutput(String strUrl) 
	{
		String strRead = null;
		HttpURLConnection jconn = null;

		try 
		{
			URL url = new URL(strUrl);
	
			jconn = (HttpURLConnection) url.openConnection();
			jconn.setDoOutput(true);
			jconn.setDoInput(true);
			jconn.connect();
	
			InputStream in = jconn.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(in, "UTF-8"));
	
			String line;
			while ((line = bufferedReader.readLine()) != null) 
			{
				strRead += line;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			jconn.disconnect();	
		}

		return strRead;
		
		
	}
	
	
	public static void main(String args[]) throws Exception
	{
		SystemConfig cc = new SystemConfig();
//		
		String currency[] = new String[]{"USD","AUD","EUR","CAD","CHF","CZK","GBP","NOK"};
		
		for (int i=0; i<currency.length; i++)
		{
			////system.out.println(cc.getCurrencyRate(currency[i]));
		}
		
	}
	
}











