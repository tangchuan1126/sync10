package com.cwc.app.api;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.iface.CartWarrantyIFace;
import com.cwc.app.util.Config;
import com.cwc.db.*;
import com.cwc.exception.SystemException;
import com.cwc.util.*;

/**
 * 购物车相关操作
 * 
 * 购物车结构：
 * 用一个DBRow存放一个商品记录
 * ArrayList存放DBRow集合 
 * 
 * 一个购物车应该包含两大块信息：购物车信息和购物车商品信息
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CartWarranty implements CartWarrantyIFace
{
	static Logger log = Logger.getLogger("ACTION");

	private ProductMgr pm;

	private DBRow detailProducts[] = null;		//购物车详细商品信息
	private float sumQuantity = 0;			//商品总数
	private double sumPrice = 0;			//总价
	private boolean hasLackingProduct;

	//商品类型
	public static int NORMAL = 1; 
	public static int UNION_STANDARD = 2; 
	public static int UNION_STANDARD_MANUAL = 3; 
	public static int UNION_CUSTOM = 4; 

	/**
	 * 增加商品到购物车
	 * @param session
	 * @param pid			商品ID
	 * @param quantity		商品数量
	 * @param product_type	商品类型
	 * @throws Exception
	 */
	public void put2Cart(HttpSession session,long pid,float quantity,int product_type) 
		throws  Exception
	{
		ArrayList<DBRow> al = null;
		
		if ( pid != 0 )
		{
			if (quantity<=0)
			{
				quantity = 1;
			}
			
			if ( session.getAttribute(Config.cartWarrantySession) == null )
			{
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("cart_pid",pid);
				row.add("cart_quantity",quantity);
				row.add("cart_product_type",product_type);//商品类型
				
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				al = (ArrayList)session.getAttribute(Config.cartWarrantySession);
				dealWithRepeat(al,pid,quantity,product_type);
			}

			session.setAttribute(Config.cartWarrantySession,al);			
		}
	}

	/**
	 * 处理重复提交商品
	 * @param pid
	 * @param quantity
	 * @return		返回true：有旧商品，修改了数量  返回alse：新增加商品
	 * @throws Exception 
	 */
	private boolean dealWithRepeat(ArrayList<DBRow> al,long pid,float quantity,int product_type) 
		throws  Exception
	{
		DBRow productOld;
		
		for ( int i=0;i<al.size(); i++ )
		{
			productOld = (DBRow)al.get(i);		//获得购物车一行记录
			
			if ( StringUtil.getLong(productOld.getString("cart_pid")) == pid&&StringUtil.getInt(productOld.getString("cart_product_type"))==product_type )		
			{
				float t = StringUtil.getFloat(productOld.getString("cart_quantity")) + quantity;
				productOld.add("cart_quantity",t);
				al.set(i,productOld);
				return(true);
			}
		}

		//没有重复商品，则作为新纪录
		DBRow row = new DBRow();
		row.add("cart_pid",pid);
		row.add("cart_quantity",quantity);
		row.add("cart_product_type",product_type);
		
		al.add(row);
		
		return(false);
	}

	/**
	 * 从session获得购物车商品
	 * 从session出来的数据，切记要克隆，否则对引用的修改会直接修改session
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartWarrantySession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartWarrantySession);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow = (DBRow)al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}

	/**
	 * 清空购物车
	 */
	public void clearCart(HttpSession session)
	{
		if ( session.getAttribute(Config.cartWarrantySession) != null )
		{
			session.removeAttribute(Config.cartWarrantySession);
		}
	}
	
	/**
	 * 判断购物车是否为空
	 * @return
	 * @throws Exception 
	 */
	public boolean isEmpty(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartWarrantySession) == null )
		{
			return(true);
		}
		else
		{
			return( getSimpleProducts(session).length==0 );
		}
	}
	
	/**
	 * 从购物车移除商品
	 * @throws Exception 
	 * @throws  
	 *
	 */
	public void removeProduct(HttpSession session,long pid,int product_type)
		throws Exception
	{
		if ( session.getAttribute(Config.cartWarrantySession) != null )
		{
			DBRow rowOld;
			ArrayList al = (ArrayList)session.getAttribute(Config.cartWarrantySession);
			
			for ( int i=0;i<al.size(); i++ )
			{
				rowOld = (DBRow)al.get(i);
				if ( StringUtil.getLong(rowOld.getString("cart_pid")) == pid&&StringUtil.getInt(rowOld.getString("cart_product_type"))==product_type )
				{
					al.remove(i);
				}
			}
			
			session.setAttribute(Config.cartWarrantySession,al);
		}
	}

	/**
	 * 批量修改商品数量
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public void modQuantity(HttpSession session,String pids[],String quantitys[],String product_type[]) 
		throws  Exception
	{
		if ( session.getAttribute(Config.cartWarrantySession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartWarrantySession);
			DBRow product;
			float quantity;
			
			for (int i=0; i<pids.length; i++)
			{
				for (int j=0; j<al.size(); j++)
				{
					product = (DBRow)al.get(j);
					if ( product.getString("cart_pid").equals(pids[i])&&product.getString("cart_product_type").equals(product_type[i]) )//找到对应的商品记录进行数量修改
					{
						quantity = StringUtil.getFloat(quantitys[i]);
						if ( quantity<=0 )
						{
							quantity = 1;
						}
						product.add("cart_quantity",quantity);
					}
					al.set(j,product);
				}
			}

			session.setAttribute(Config.cartWarrantySession,al);
		}
	}
	
	
	
	/**
	 * 获得购物车详细信息
	 * @param request
	 * @return  DBRow,包含购物车内商品详细信息和购物车信息
	 * @throws Exception
	 */
	public void flush(HttpSession session)
		throws Exception
	{
		try
		{
				float sum_quantity = 0;
				double sum_price = 0;
				this.hasLackingProduct = false;		//默认没有包含缺货商品
				DBRow detailP;

				DBRow products[] = this.getSimpleProducts(session);
				//必须深度克隆，否则使用的只是引用，会把大量数据加到session中
				//数组不能直接克隆，需要一个个对象克隆，否则也是克隆地址引用
				DBRow newProducts[] = null;
				ArrayList newProductsAL = new ArrayList();
				for (int i=0; i<products.length; i++)
				{
					DBRow newProduct = new DBRow();
					newProduct.append(products[i]);
					newProductsAL.add(newProduct);
				}
				newProducts = (DBRow[])newProductsAL.toArray(new DBRow[0]);
				
				//把更详细的商品信息附加到购物车的商品记录中
				for (int i=0; i<newProducts.length; i++)
				{
					//非定制商品
					if (newProducts[i].get("cart_product_type",0)!=CartWarranty.UNION_CUSTOM)
					{
						detailP = pm.getDetailProductByPcid(StringUtil.getLong(newProducts[i].getString("cart_pid")));
						newProducts[i].append(detailP);
					}
					else
					{
						//定制商品需要从另外的定制表获得商品详细信息
						detailP = pm.getDetailProductCustomByPcPcid(StringUtil.getLong(newProducts[i].getString("cart_pid")));

						//因为新纪录的字段比原来少，所以需要把旧字段删掉
						String cart_pid = newProducts[i].getString("cart_pid");
						String cart_quantity = newProducts[i].getString("cart_quantity");
						String cart_product_type = newProducts[i].getString("cart_product_type");
						newProducts[i].clear();
						newProducts[i].add("cart_pid", StringUtil.getLong(cart_pid));
						newProducts[i].add("cart_quantity", StringUtil.getFloat(cart_quantity));
						newProducts[i].add("cart_product_type", StringUtil.getInt(cart_product_type));
						newProducts[i].append(detailP);
					}
					//计算一些数据
					sum_quantity += StringUtil.getFloat(newProducts[i].getString("cart_quantity"));
					
					//计算商品重量
					
					
					
				}
				////system.out.println(newProducts.length);
				//设置购物车和购物车商品详细信息
				this.detailProducts =  newProducts;
				this.sumPrice = sum_price;
				this.sumQuantity = sum_quantity;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"flush",log);
		}
	}
	

	/**
	 * 把商品添加到购物车
	 * @param request
	 * @throws Exception
	 */
	public void put2Cart(HttpServletRequest request)
		throws ProductNotCreateStorageException,ProductNotExistException,Exception
	{
		try
		{
			int product_type;
			
			String p_name = StringUtil.getString(request, "p_name");
			float quantity = StringUtil.getFloat(request, "quantity");
			long pid = 0;
			
			//先检测商品是否存在，存在则取得PID(禁止抄单商品也认为不存在)
			DBRow detailP = pm.getDetailProductByPname(p_name);
			if (detailP==null||detailP.get("alive", 0)==0)
			{
				throw new ProductNotExistException();
			}
			else
			{
				pid = detailP.get("pc_id", 0l);
			}

			/**
			 * 从系统外部，用户只能添加普通和标准套装
			 * 拼装套装和定制套装，都是系统内部转换得来
			 */
			if (detailP.get("union_flag", 0)==0)
			{
				product_type = CartWarranty.NORMAL;
			}
			else
			{
				product_type = CartWarranty.UNION_STANDARD;
			}
			
			this.put2Cart(StringUtil.getSession(request),pid, quantity,product_type);
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductNotCreateStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"put2Cart",log);
		}
	}
	
	public void convert2CustomProduct(HttpSession session,long old_pid,long new_pid) 
		throws  Exception
	{
		ArrayList al = (ArrayList)session.getAttribute(Config.cartWarrantySession);
		DBRow product;
		
		for (int j=0; j<al.size(); j++)
		{
			product = (DBRow)al.get(j);
			
			/**
			 * 把普通套装变成定制套装时，需要把现在的PID变成定制产生的新定制套装ID
			 */
			if ( product.getString("cart_pid").equals(String.valueOf(old_pid))&&product.getString("cart_product_type").equals(String.valueOf(CartWarranty.UNION_STANDARD)) )
			{
				product.add("cart_pid",new_pid);
				product.add("cart_product_type",CartWarranty.UNION_CUSTOM);
				al.set(j,product);
				break;
			}
		}
	}
	
		
	
	
	
	
	
	public DBRow[] getDetailProducts()
	{
		return detailProducts;
	}

	public float getSumQuantity()
	{
		return sumQuantity;
	}

	public double getSumPrice()
	{
		return sumPrice;
	}

	public boolean isHasLackingProduct()
	{
		return hasLackingProduct;
	}

	public void setPm(ProductMgr pm)
	{
		this.pm = pm;
	}

	
	
	
	
	
	



	
}






