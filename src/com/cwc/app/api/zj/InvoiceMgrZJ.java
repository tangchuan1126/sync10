package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorInvoiceMgrZJ;
import com.cwc.app.iface.zj.InvoiceMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class InvoiceMgrZJ implements InvoiceMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorInvoiceMgrZJ floorInvoiceMgrZJ;
	
	/**
	 * 添加发票轮循
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addRoundInvoice(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long invoice_template_id = StringUtil.getLong(request,"invoice_template_id");
			String round_invoice_dog = StringUtil.getString(request,"round_invoice_dog");
			String round_invoice_tv = StringUtil.getString(request,"round_invoice_tv");
			String round_invoice_uv = StringUtil.getString(request,"round_invoice_uv");
			String round_invoice_ref = StringUtil.getString(request,"round_invoice_ref");
			String round_invoice_hs_code = StringUtil.getString(request,"round_invoice_hs_code");
			String round_invoice_material = StringUtil.getString(request,"round_invoice_material");
			String proMaterialChinese = StringUtil.getString(request,"proMaterialChinese");
			String proTextRfeChinese = StringUtil.getString(request,"proTextRfeChinese");
			String proTextDogChinese = StringUtil.getString(request,"proTextDogChinese");
			
			DBRow roundInvoice = new DBRow();
			
			roundInvoice.add("round_invoice_material_chinese",proMaterialChinese);
			roundInvoice.add("round_invoice_rfe_chinese",proTextRfeChinese);
			roundInvoice.add("round_invoice_dog_chinese",proTextDogChinese);
			
			roundInvoice.add("invoice_template_id",invoice_template_id);
			roundInvoice.add("round_invoice_dog",round_invoice_dog);
			roundInvoice.add("round_invoice_tv",round_invoice_tv);
			roundInvoice.add("round_invoice_uv",round_invoice_uv);
			roundInvoice.add("round_invoice_ref",round_invoice_ref);
			roundInvoice.add("round_invoice_hs_code",round_invoice_hs_code);
			roundInvoice.add("round_invoice_material",round_invoice_material);
			return (floorInvoiceMgrZJ.addRoundInvoice(roundInvoice));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addRoundInvoice",log);
		}
	}
	
	/**
	 * 修改模板样式
	 * @param request
	 * @throws Exception
	 */
	public void modRoundInvoiceById(HttpServletRequest request)
		throws Exception
	{
		try {
			long round_invoice_id = StringUtil.getLong(request,"round_invoice_id");
			String round_invoice_dog = StringUtil.getString(request,"round_invoice_dog");
			String round_invoice_tv = StringUtil.getString(request,"round_invoice_tv");
			String round_invoice_uv = StringUtil.getString(request,"round_invoice_uv");
			String round_invoice_ref = StringUtil.getString(request,"round_invoice_ref");
			String round_invoice_hs_code = StringUtil.getString(request,"round_invoice_hs_code");
			String round_invoice_material = StringUtil.getString(request,"round_invoice_material");
			
			String proMaterialChinese = StringUtil.getString(request,"proMaterialChinese");
			String proTextRfeChinese = StringUtil.getString(request,"proTextRfeChinese");
			String proTextDogChinese = StringUtil.getString(request,"proTextDogChinese");
			
			DBRow roundInvoice = new DBRow();
			
			roundInvoice.add("round_invoice_material_chinese",proMaterialChinese);
			roundInvoice.add("round_invoice_rfe_chinese",proTextRfeChinese);
			roundInvoice.add("round_invoice_dog_chinese",proTextDogChinese);
			
			roundInvoice.add("round_invoice_dog",round_invoice_dog);
			roundInvoice.add("round_invoice_tv",round_invoice_tv);
			roundInvoice.add("round_invoice_uv",round_invoice_uv);
			roundInvoice.add("round_invoice_ref",round_invoice_ref);
			roundInvoice.add("round_invoice_hs_code",round_invoice_hs_code);
			roundInvoice.add("round_invoice_material",round_invoice_material);
			floorInvoiceMgrZJ.modRoundInvoiceById(round_invoice_id, roundInvoice);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modRoundInvoiceById",log);
		}
	}
	/**
	 * 根据选择模板获得发件人
	 */
	public DBRow getDetailDeliveryInfoByInvoiceId(long invoice_id) 
		throws Exception 
	{
		try 
		{
			DBRow invoice = floorInvoiceMgrZJ.getDetailInvoiceTemplate(invoice_id);
			int round_di = invoice.get("round_di",0);//发票模板轮循发件人的顺序
			
			DBRow delivery_info = null;
			
			if(invoice.get("di_id",0l)==0)//如果模板发件人无指定发件人，发件人为轮循，根据轮循使用发票模板仓库内的发件人信息
			{
				DBRow[] deliveryInfos = floorInvoiceMgrZJ.getAllDelivererInfoByPsid(invoice.get("ps_id",0l));
				
				if(deliveryInfos==null||!(deliveryInfos.length>0))
				{
					throw new Exception("查询不到发件人信息");
				}
				
				if(round_di<deliveryInfos.length)//轮循值小于发件人数组，可以轮循
				{
					delivery_info = deliveryInfos[round_di];
					
					round_di = round_di+1;//使用后轮循值+1；不用考虑超出，因再次轮循时，轮循值大于则自动回0；
				}
				else//轮循值大于发件人数组长度
				{
					delivery_info = deliveryInfos[0];
					
					round_di = 0;
				}
				
				DBRow para = new DBRow();
				para.add("round_di",round_di);
				floorInvoiceMgrZJ.modInvoice(invoice_id, para);
			}
			else//设定了发件人
			{
				delivery_info = floorInvoiceMgrZJ.getDetailDelivererInfo(invoice.get("di_id",0l));
			}
			
			return delivery_info;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailDeliveryInfoByInvoiceId",log);
		}
	}
	
	/**
	 * 根据发票模板获得发票信息(附带轮循)
	 * @param invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailInvoiceForRound(long invoice_id)
		throws Exception
	{
		try 
		{
			DBRow invoiceTemplate = floorInvoiceMgrZJ.getDetailInvoiceTemplate(invoice_id);
			
			int round_invoice = invoiceTemplate.get("round_invoice",0);//发票内容轮循值
			
			DBRow invoice = null;
			
//			if(invoiceTemplate.get("is_round_invoice",0)==1)//发票信息是随机轮循的
//			{
				DBRow[] roundInvoices = floorInvoiceMgrZJ.getAllRoundInvoiceByInvoiceId(invoice_id);
				
				if(roundInvoices==null||!(roundInvoices.length>0))
				{
					throw new Exception("没有对应的发票轮循信息");
				}
				
				if(round_invoice<roundInvoices.length)//轮循值小于可选发票信息数组，可以轮循
				{
					invoice = new DBRow();
					
					invoice.add("dog",roundInvoices[round_invoice].getString("round_invoice_dog"));
					invoice.add("rfe",roundInvoices[round_invoice].getString("round_invoice_ref"));
					invoice.add("uv",roundInvoices[round_invoice].getString("round_invoice_uv"));
					invoice.add("tv",roundInvoices[round_invoice].getString("round_invoice_tv"));
					invoice.add("hs_code",roundInvoices[round_invoice].getString("round_invoice_hs_code"));
					invoice.add("material",roundInvoices[round_invoice].getString("round_invoice_material"));
					
					String dog_chinese = roundInvoices[round_invoice].getString("round_invoice_dog_chinese");
					String ref_chinese = roundInvoices[round_invoice].getString("round_invoice_rfe_chinese");
					String material_chinese = roundInvoices[round_invoice].getString("round_invoice_material_chinese");
					invoice.add("dog_chinese",dog_chinese);
					invoice.add("rfe_chinese",ref_chinese);
					invoice.add("material_chinese",material_chinese);
					
					invoice.add("invoice_id",invoiceTemplate.get("invoice_id",0l));
					invoice.add("di_id",invoiceTemplate.get("di_id",0l));
					invoice.add("ps_id",invoiceTemplate.get("ps_id",0l));
					invoice.add("t_name",invoiceTemplate.getString("t_name"));		
					
					round_invoice = round_invoice+1;//使用后轮循值+1；不用考虑超出，因再次轮循时，轮循值大于则自动回0；
				}
				else//轮循值大于发件人数组长度
				{
					round_invoice = 0;
					
					invoice = new DBRow();
					
					invoice.add("dog",roundInvoices[round_invoice].getString("round_invoice_dog"));
					invoice.add("rfe",roundInvoices[round_invoice].getString("round_invoice_ref"));
					invoice.add("uv",roundInvoices[round_invoice].getString("round_invoice_uv"));
					invoice.add("tv",roundInvoices[round_invoice].getString("round_invoice_tv"));
					invoice.add("hs_code",roundInvoices[round_invoice].getString("round_invoice_hs_code"));
					invoice.add("material",roundInvoices[round_invoice].getString("round_invoice_material"));
					
					String dog_chinese = roundInvoices[round_invoice].getString("round_invoice_dog_chinese");
					String ref_chinese = roundInvoices[round_invoice].getString("round_invoice_rfe_chinese");
					String material_chinese = roundInvoices[round_invoice].getString("round_invoice_material_chinese");
					invoice.add("dog_chinese",dog_chinese);
					invoice.add("rfe_chinese",ref_chinese);
					invoice.add("material_chinese",material_chinese);
					
					invoice.add("invoice_id",invoiceTemplate.get("invoice_id",0l));
					invoice.add("di_id",invoiceTemplate.get("di_id",0l));
					invoice.add("ps_id",invoiceTemplate.get("ps_id",0l));
					invoice.add("t_name",invoiceTemplate.getString("t_name"));
					
				}
				
				DBRow para = new DBRow();
				para.add("round_invoice",round_invoice);
				floorInvoiceMgrZJ.modInvoice(invoice_id, para);
//			}
//			else//发票信息不需轮循，直接取模板上的信息
//			{
//				invoice = new DBRow();
//				invoice.add("dog",invoiceTemplate.getString("dog"));
//				invoice.add("rfe",invoiceTemplate.getString("rfe"));
//				invoice.add("uv",invoiceTemplate.getString("uv"));
//				invoice.add("tv",invoiceTemplate.getString("tv"));
//				invoice.add("invoice_id",invoiceTemplate.get("invoice_id",0l));
//				invoice.add("di_id",invoiceTemplate.get("di_id",0l));
//				invoice.add("ps_id",invoiceTemplate.get("ps_id",0l));
//				invoice.add("t_name",invoiceTemplate.getString("t_name"));
//				invoice.add("hs_code",invoiceTemplate.getString("hs_code"));
//				invoice.add("material",invoiceTemplate.getString("material"));
//			}
			
			return invoice;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailInvoice",log);
		}
	}
	
	/**
	 * 根据发票模板Id获得完整的发票信息（包括发件人信息，发票信息）
	 * @param invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFullInvoiceByInvoiceId(long invoice_id)
		throws Exception
	{
		try 
		{
			DBRow delivery_info = this.getDetailDeliveryInfoByInvoiceId(invoice_id);
			
			DBRow invoice = this.getDetailInvoiceForRound(invoice_id);
			
			invoice.add("di_id",delivery_info.get("di_id",0l));
			invoice.add("CompanyName",delivery_info.getString("CompanyName"));
			invoice.add("City",delivery_info.getString("City"));
			invoice.add("DivisionCode",delivery_info.getString("DivisionCode"));
			invoice.add("PostalCode",delivery_info.getString("PostalCode"));
			invoice.add("CountryCode",delivery_info.getString("CountryCode"));
			invoice.add("CountryName",delivery_info.getString("CountryName"));
			invoice.add("AddressLine1",delivery_info.getString("AddressLine1"));
			invoice.add("AddressLine2",delivery_info.getString("AddressLine2"));
			invoice.add("AddressLine3",delivery_info.getString("AddressLine3"));
			invoice.add("PhoneNumber",delivery_info.getString("PhoneNumber"));
			
			return invoice;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getFullInvoiceByInvoiceId",log);
		}
	}
	
	/**
	 * 根据发票模板ID 
	 */
	public DBRow[] getAllRoundInvoicesByInvoiceId(long invoice_id)
		throws Exception 
	{
		try 
		{
			return floorInvoiceMgrZJ.getAllRoundInvoiceByInvoiceId(invoice_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllRoundInvoicesByInvoiceId",log);
		}
	}
	
	/**
	 * 删除模板内的轮循样式
	 * @param request
	 * @throws Exception
	 */
	public void delRoundInvoice(HttpServletRequest request)
		throws Exception
	{
		long round_invoice_id = StringUtil.getLong(request,"round_invoice_id");
		long invoice_id = StringUtil.getLong(request,"invoice_id");
		
		floorInvoiceMgrZJ.delRoundInvoiceById(round_invoice_id);
		
		DBRow[] roundInvoices = floorInvoiceMgrZJ.getAllRoundInvoiceByInvoiceId(invoice_id);
		
		if(roundInvoices==null||!(roundInvoices.length>0))//如果模板轮循样式全被删除，模板改为不样式随机
		{
			DBRow para = new DBRow();
			para.add("is_round_invoice",2);
			
			floorInvoiceMgrZJ.modInvoice(invoice_id, para);
		}
	}
	
	/**
	 * 根据ID获得发票模板样式
	 * @param round_invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoundInvoiceById(long round_invoice_id)
		throws Exception
	{
		try 
		{
			return (floorInvoiceMgrZJ.getDetailRoundInvoiceById(round_invoice_id));
		}
		catch (Exception e) 
		{
			throw new SystemException();
		}
	}

	
	
	

	public void setFloorInvoiceMgrZJ(FloorInvoiceMgrZJ floorInvoiceMgrZJ) {
		this.floorInvoiceMgrZJ = floorInvoiceMgrZJ;
	}

	
}
