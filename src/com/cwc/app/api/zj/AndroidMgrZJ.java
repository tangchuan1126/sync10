package com.cwc.app.api.zj;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.log4j.Logger;
import org.apache.tools.ant.types.selectors.DifferentSelector;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.storageApprove.HasWaitApproveAreaException;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStoreLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStoreMgrZJ;
import com.cwc.app.floor.api.zyj.FloorContainerMgrZyj;
import com.cwc.app.iface.zj.AndroidMgrIFaceZJ;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.mongodb.util.Hash;

public class AndroidMgrZJ implements AndroidMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStoreLocationMgrZJ floorProductStoreLocationMgrZJ;
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private StorageApproveMgrIFaceZJ storageApproveMgrZJ;
	private FloorProductStoreMgrZJ floorProductStoreMgrZJ;
	private FloorProductStoreMgr productStoreMgr;
	
	public byte[] downLoadProductStoreLocationBaseData(String xml,String machine)
		throws Exception 
	{
		String details = StringUtil.getSampleNode(xml,"Details");
		String efficacy = StringUtil.getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(StringUtil.getSampleNode(xml,"Length"));

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		long area_id = Long.parseLong(StringUtil.getSampleNode(xml, "Area"));
		
		String storeLocationCatalog = getResponseXML(this.xmlGetStoreLocationCatalog(area_id));
		String productStoreLocation = getResponseXML(this.xmlGetProductStoreLocationByArea(area_id));
		String productStoreContainer = getResponseXML(this.xmlGetProductStoreContainerByArea(area_id));
		String storeLocationProduct = getResponseXML(this.xmlGetProductByArea(area_id));
		String storeLocationProductCode = getResponseXML(this.xmlGetProductCodeByArea(area_id));
		String storeLocationSerialProduct = getResponseXML(this.xmlGetSerialProductByArea(area_id));
		
		
		InputStream isStoreLocationCatalog = new ByteArrayInputStream(storeLocationCatalog.getBytes());
		InputStream isProductStoreLocation = new ByteArrayInputStream(productStoreLocation.getBytes());
		InputStream isProductStoreContainer = new ByteArrayInputStream(productStoreContainer.getBytes());
		InputStream isStoreLocationProduct = new ByteArrayInputStream(storeLocationProduct.getBytes());
		InputStream isStoreLocationProductCode = new ByteArrayInputStream(storeLocationProductCode.getBytes());
		InputStream isStoreLocationSerialProduct = new ByteArrayInputStream(storeLocationSerialProduct.getBytes());
		
		
		ArrayList<InputStream> list = new ArrayList<InputStream>();
		list.add(isStoreLocationCatalog);
		list.add(isProductStoreLocation);
		list.add(isProductStoreContainer);
		list.add(isStoreLocationProduct);
		list.add(isStoreLocationProductCode);
		list.add(isStoreLocationSerialProduct);
		
		FileUtil.delFile(Environment.getHome()+"/bcs_down_zip/"+machine+"ProductLocation.zip");
		ZipFile zipFile = new ZipFile(Environment.getHome()+"/bcs_down_zip/"+machine+"ProductLocation.zip");
		
		for (int i = 0; i <list.size();i++)
		{
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			
			switch (i) 
			{
				case 0:
					parameters.setFileNameInZip("storeLocationCatalog.txt");
				break;
				
				case 1:
					parameters.setFileNameInZip("productStoreLocation.txt");
				break;
				
				case 2:
					parameters.setFileNameInZip("productStoreContainer.txt");
				break;
				
				case 3:
					parameters.setFileNameInZip("storeLocationProduct.txt");
				break;
				
				case 4:
					parameters.setFileNameInZip("storeLocationProductCode.txt");
				break;
				
				case 5:
					parameters.setFileNameInZip("storeLocationSerialProduct.txt");
				break;
			}
			
			parameters.setSourceExternalStream(true);
			
			zipFile.addStream(list.get(i), parameters);
			list.get(i).close();
		}
		
		File file=new File(Environment.getHome()+"/bcs_down_zip/"+machine+"ProductLocation.zip");
		InputStream fis=new FileInputStream(file);
		int fileLength =(int)file.length();
		

		byte[] image=new byte[fileLength];
		fis.read(image);
		fis.close();
		
		return image;
	}
	
	/**
	 * 根据区域获得仓库位置的XML
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public String xmlGetStoreLocationCatalog(long area_id)
		throws Exception
	{
		try 
		{
			DBRow[] storeLocationCatalog = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id, null, null, null);
			
			StringBuffer returnXML = new StringBuffer("");
			for (int i = 0; i < storeLocationCatalog.length; i++) 
			{
				returnXML.append("<Details>");
				returnXML.append("<Location>"+storeLocationCatalog[i].getString("slc_position_all")+"</Location>");
				returnXML.append("<Sort>"+(i+1)+"</Sort>");
				returnXML.append("<LocationId>"+storeLocationCatalog[i].get("slc_id",0l)+"</LocationId>");
				returnXML.append("</Details>");
			}
			
			return returnXML.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlStoreLocationCatalog",log);
		}
	}
	
	/**
	 * 根据区域获得商品的物理库存
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public String xmlGetProductStoreLocationByArea(long area_id)
		throws Exception
	{
		DBRow[] storeLocationProduct = floorProductStoreLocationMgrZJ.getProductStoreLocationByArea(area_id);
		
		try 
		{
			StringBuffer details = new StringBuffer("");
			for (int i = 0; i < storeLocationProduct.length; i++) 
			{
				details.append("<Details>");
				details.append("<SlcId>"+storeLocationProduct[i].get("slc_id",0l)+"</SlcId>");
				details.append("<Location>"+storeLocationProduct[i].getString("slc_position_all")+"</Location>");
				details.append("<PcId>"+storeLocationProduct[i].get("pc_id",0l)+"</PcId>");
				details.append("<PhysicalQty>"+storeLocationProduct[i].get("physical_quantity",0f)+"</PhysicalQty>");
				details.append("</Details>");
			}
			
			return details.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlGetProductStoreLocationByArea",log);
		}
	}
	
	public String xmlGetProductStoreLocationBySlc(long slc_id)
		throws Exception
	{
		DBRow[] storeLocationProduct = floorProductStoreLocationMgrZJ.getProductStoreLocationBySlcId(slc_id);
		
		try 
		{
			StringBuffer details = new StringBuffer("");
			for (int i = 0; i < storeLocationProduct.length; i++) 
			{
				details.append("<Details>");
				details.append("<SlcId>"+storeLocationProduct[i].get("slc_id",0l)+"</SlcId>");
				details.append("<Location>"+storeLocationProduct[i].getString("slc_position_all")+"</Location>");
				details.append("<PcId>"+storeLocationProduct[i].get("pc_id",0l)+"</PcId>");
				details.append("<PhysicalQty>"+storeLocationProduct[i].get("physical_quantity",0f)+"</PhysicalQty>");
				details.append("</Details>");
			}
			
			return details.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlGetProductStoreLocationByArea",log);
		}
	}
	
	/**
	 * 根据区域获得库位上的商品信息
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public String xmlGetProductByArea(long area_id)
		throws Exception
	{
		try 
		{
			DBRow[] products = floorProductStoreLocationMgrZJ.getProductStoreLocationProduct(area_id); 
			
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < products.length; i++)
			{
				re.append("<Details>");				
				re.append("<Name>"+products[i].getString("p_name")+"</Name>");
				re.append("<PID>"+products[i].get("pc_id",0l)+"</PID>");
				re.append("<Length>"+products[i].get("length",0f)+"</Length>");
				re.append("<Width>"+products[i].get("width",0f)+"</Width>");
				re.append("<Heigth>"+products[i].get("heigth",0f)+"</Heigth>");
				re.append("<Weight>"+products[i].get("weight",0f)+"</Weight>");
				re.append("</Details>");
			}
			
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlGetProductByArea",log);
			
		}
	}
	
	public String xmlGetProductCodeByArea(long area_id)
		throws Exception
	{
		try 
		{
			DBRow[] productCodes = floorProductStoreLocationMgrZJ.getProductStoreLocationProductCode(area_id);
			
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < productCodes.length; i++) 
			{
				re.append("<Details>");	
				re.append("<PCID>"+productCodes[i].get("pc_id",0l)+"</PCID>");
				re.append("<Barcode>"+productCodes[i].getString("p_code")+"</Barcode>");
				re.append("<CodeType>"+productCodes[i].get("code_type",0)+"</CodeType>"); //zhangrui
				re.append("<PName>"+productCodes[i].getString("p_name")+"</PName>"); //zhangrui

				if(productCodes[i].get("code_type",0)==CodeTypeKey.Main)
				{
					re.append("<Show>1</Show>");
				}
				else
				{
					re.append("<Show>0</Show>");
				}
				re.append("</Details>");	
			}
			
			DBRow[] products = floorProductStoreLocationMgrZJ.getProductStoreLocationProduct(area_id);
			
			for (int i = 0; i < products.length; i++) 
			{
				re.append("<Details>");	
				re.append("<PCID>"+products[i].get("pc_id",0l)+"</PCID>");
				re.append("<Barcode>"+products[i].get("pc_id",0l)+"</Barcode>");
				re.append("<CodeType>0</CodeType>");
				re.append("<PName>"+products[i].getString("p_name")+"</PName>");
				re.append("</Details>");
			}
			
			return (re.toString());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlGetProductCodeByArea",log);
		}
	}
	
	public String xmlGetSerialProductByArea(long area_id)
		throws Exception
	{
		try 
		{
			DBRow[] serialProducts = floorProductStoreLocationMgrZJ.getProductStoreLocationSerialNumber(area_id);
			StringBuffer re = new StringBuffer("");
			
			for (int i = 0; i < serialProducts.length; i++) 
			{
				re.append("<Details>");	
				re.append("<PCID>"+serialProducts[i].get("pc_id",0l)+"</PCID>");
				re.append("<SN>"+serialProducts[i].getString("serial_number")+"</SN>");
				re.append("</Details>");
			}
			
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlGetSerialProductByArea",log);
		}
	}
	/**
	 * 根据区域获得容器库存
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public String xmlGetProductStoreContainerByArea(long area_id)
		throws Exception
	{
		StringBuffer re = new StringBuffer();
		DBRow area = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
		long ps_id = area.get("area_psid",0l);
		
		Map<String,DBRow> productStoreContainerPhysicals = productStoreMgr.physicalCount(ps_id,area_id,0);
		Set<String> productStoreContainerPhysicalKey = productStoreContainerPhysicals.keySet();
		for (String key : productStoreContainerPhysicalKey) 
		{
			DBRow productStoreContainer = productStoreContainerPhysicals.get(key);
			long slc_id = productStoreContainer.get("slc_id",0l);
			DBRow storageLocationCatalog = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
			
			re.append("<Details>");
			re.append("<Pcid>"+productStoreContainer.get("pc_id",0l)+"</Pcid>");
			re.append("<PhysicalQty>"+productStoreContainer.get("quantity",0f)+"</PhysicalQty>");
			re.append("<Con_Id>"+productStoreContainer.get("con_id",0l)+"</Con_Id>");
			re.append("<ContainerType>"+productStoreContainer.get("container_type",0)+"</ContainerType>");
			re.append("<ContainerTypeId>"+productStoreContainer.get("type_id",0)+"</ContainerTypeId>");
			re.append("<Title>"+productStoreContainer.get("title_id",0l)+"</Title>");
			re.append("<SlcId>"+slc_id+"</SlcId>");
			re.append("<Location>"+storageLocationCatalog.getString("slc_position_all")+"</Location>");
			re.append("</Details>");
		}
		
		return re.toString();
	}

	public String xmlGetProductStoreContainerBySlc(long slc_id)
		throws Exception
	{
		DBRow[] productStoreContainers = floorProductStoreMgrZJ.getProductStoreContainerBySlc(slc_id);
		StringBuffer re = new StringBuffer();
		
		for (int i = 0; i < productStoreContainers.length; i++) 
		{
			re.append("<Details>");
			re.append("<Pcid>"+productStoreContainers[i].get("pc_id",0l)+"</Pcid>");
			re.append("<PhysicalQty>"+productStoreContainers[i].get("physical_count",0f)+"</PhysicalQty>");
			re.append("<Con_Id>"+productStoreContainers[i].get("con_id",0l)+"</Con_Id>");
			re.append("<ContainerType>"+productStoreContainers[i].get("container_type",0)+"</ContainerType>");
			re.append("<ContainerTypeId>"+productStoreContainers[i].get("container_type_id",0)+"</ContainerTypeId>");
			re.append("<Title>"+productStoreContainers[i].get("title_id",0l)+"</Title>");
			re.append("<SlcId>"+productStoreContainers[i].get("slc_id",0l)+"</SlcId>");
			re.append("<Location>"+productStoreContainers[i].getString("slc_position_all")+"</Location>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	public String getProductStoreLocationBySlc(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = StringUtil.getSampleNode(xml,"Details");
			String efficacy = StringUtil.getSampleNode(xml,"Efficacy");
			int length = Integer.parseInt(StringUtil.getSampleNode(xml,"Length"));

			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			long slc_id = Long.parseLong(StringUtil.getSampleNode(xml, "Location"));
			
//			DBRow storeCatalogLocation = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(adminLoggerBean.getPs_id(),location);
			
//			return this.xmlGetProductStoreLocationBySlc(slc_id);
			
			return this.xmlGetProductStoreContainerBySlc(slc_id);
		} 
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreLocationBySlc",log);
		}
	}
	
	public int storageTakeStockApprove(long area_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			int differnetCount = storageApproveMgrZJ.storageTakeStockNew(area_id,adminLoggerBean);
			return differnetCount;
		}
		catch (HasWaitApproveAreaException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"storageTakeStockApprove",log);
		}
	}
			
	/**
	 * 盘点申请审核
	 * @param xml
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public String storageTakeStockApprove(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = StringUtil.getSampleNode(xml,"Details");
			String efficacy = StringUtil.getSampleNode(xml,"Efficacy");
			int length = Integer.parseInt(StringUtil.getSampleNode(xml,"Length"));

			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			long area_id = Long.parseLong(StringUtil.getSampleNode(xml, "Area"));
			int differnetCount = storageApproveMgrZJ.storageTakeStockNew(area_id,adminLoggerBean);
			
			StringBuffer re = new StringBuffer("<DifferentCount>"+differnetCount+"</DifferentCount>");
			return re.toString();
		}
		catch (HasWaitApproveAreaException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"storageTakeStockOver",log);
		}
	}
	
	public DBRow[] storageTakeStockDifferents(String xml,AdminLoginBean adminBean)
		throws Exception
	{
		String details = StringUtil.getSampleNode(xml,"Details");
		String efficacy = StringUtil.getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(StringUtil.getSampleNode(xml,"Length"));

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		long area_id = Long.parseLong(StringUtil.getSampleNode(xml, "Area"));
		
		DBRow[] differents = storageApproveMgrZJ.storageContainerProductCountDifferentByArea(area_id);
		
//		StringBuffer re = new StringBuffer("");
//		for (int i = 0; i < differents.length; i++)
//		{
//
//			re.append("<Details>");
//			re.append("<SlcId>"+differents[i].get("slc_id",0l)+"</SlcId>");
//			re.append("<Location>"+differents[i].getString("location")+"</Location>");
//			re.append("<PcId>"+differents[i].get("pc_id",0l)+"</PcId>");
//			re.append("<Pname>"+differents[i].getString("p_name")+"</Pname>");
//			re.append("<PQty>"+differents[i].get("physical_count",0f)+"</PQty>");
//			re.append("<TQty>"+differents[i].get("take_store_count",0f)+"</TQty>");
//			re.append("</Details>");
//		}
		
		return differents;
	}
	
	public DBRow[] storageTakeStockDifferents(long area_id)
		throws Exception
	{
		try 
		{
			return storageApproveMgrZJ.storageContainerProductCountDifferentByArea(area_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"storageTakeStockDifferents",log);
		}
	}
	
	public void takeStoreStorageLocation(String floorContainers,long slc_id)
		throws Exception
	{
		try 
		{
			//先解除容器与位置的关系
			Map<String,Object> catalogNodeProps =new HashMap<String, Object>();
			catalogNodeProps.put("slc_id",slc_id);
			productStoreMgr.removeRelations(0,NodeType.Container,NodeType.StorageLocationCatalog,new HashMap<String,Object>(),catalogNodeProps,new HashMap<String,Object>());
			
			//像临时仓库添加位置节点
			DBRow storageLocationCatalog = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
			Map<String,Object> storageLocation = new HashMap<String, Object>();
			storageLocation.put("is_three_dimensional",storageLocationCatalog.get("is_three_dimensional",0));
			storageLocation.put("slc_area",storageLocationCatalog.get("slc_area",0l));
			storageLocation.put("slc_id",slc_id);
			storageLocation.put("slc_position",storageLocationCatalog.getString("slc_position"));
			storageLocation.put("slc_position_all",storageLocationCatalog.getString("slc_position_all"));
			storageLocation.put("slc_type",storageLocationCatalog.getString("slc_type"));
			storageLocation.put("slc_x",storageLocationCatalog.get("slc_x",0f));
			storageLocation.put("slc_y",storageLocationCatalog.get("slc_y",0f));
			productStoreMgr.addNode(0,NodeType.StorageLocationCatalog,storageLocation);
			
			String[] floorContainersString = floorContainers.split(",");
			
			for (int i = 0; i < floorContainersString.length; i++) 
			{
				long con_id = Long.parseLong(floorContainersString[i]);
				Map<String,Object> containerNode = new HashMap<String, Object>();
				containerNode.put("con_id",con_id);
				
				Map<String,Object> storageLocationCatalogNode = new HashMap<String, Object>();
				storageLocationCatalogNode.put("slc_id",slc_id);
				
				////system.out.println(con_id);
				
				JSONObject containerTree = productStoreMgr.containerTree(0, con_id, null, 0L, 0l, null, null);
				
				////system.out.println(containerTree.toString());
				JSONArray products = containerTree.getJSONArray("products");
				
				for (int j = 0; j < products.length(); j++) 
				{
					JSONObject product = products.getJSONObject(j);
					long pc_id = product.getLong("pc_id");
					long title_id = product.getLong("title_id");
					
					Map<String,Object> locates = new HashMap<String, Object>();
					locates.put("pc_id",pc_id);
					locates.put("title_id",title_id);
					locates.put("lot_number","");
					locates.put("time_number",0);
					
					productStoreMgr.addRelation(0,NodeType.Container,NodeType.StorageLocationCatalog,containerNode,storageLocationCatalogNode, locates);
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"takeStoreStorageLocation",log);
		}
	}
	
	private String getResponseXML(String details)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		return boxQtyXml.toString();
	}
	public void setFloorProductStoreLocationMgrZJ(
			FloorProductStoreLocationMgrZJ floorProductStoreLocationMgrZJ) {
		this.floorProductStoreLocationMgrZJ = floorProductStoreLocationMgrZJ;
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setStorageApproveMgrZJ(StorageApproveMgrIFaceZJ storageApproveMgrZJ) {
		this.storageApproveMgrZJ = storageApproveMgrZJ;
	}

	public void setFloorProductStoreMgrZJ(
			FloorProductStoreMgrZJ floorProductStoreMgrZJ) {
		this.floorProductStoreMgrZJ = floorProductStoreMgrZJ;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
