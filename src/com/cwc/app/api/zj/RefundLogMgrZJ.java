package com.cwc.app.api.zj;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorRefundLogMgrZJ;
import com.cwc.app.iface.zj.RefundLogMgrIFaceZJ;
import com.cwc.app.key.RefundLogKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class RefundLogMgrZJ implements RefundLogMgrIFaceZJ {

	private FloorRefundLogMgrZJ floorRefundLogMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	
	/**
	 * 添加退款记录
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addRefundLog(String refund_delivery_code,String refund_people,int refund_status) 
		throws Exception 
	{
		try 
		{
			String refund_time = DateUtil.NowStr();
			
			DBRow dbrow = new DBRow();
			dbrow.add("refund_delivery_code",refund_delivery_code);
			dbrow.add("refund_people",refund_people);
			dbrow.add("refund_status",refund_status);
			dbrow.add("refund_time",refund_time);
			
			return floorRefundLogMgrZJ.addRefundLog(dbrow);
		} catch (Exception e) 
		{
			throw new SystemException(e,"addRefundLog",log);
		}
	}

	
	/**
	 * 获得退款记录
	 * @param begintime
	 * @param endtime
	 * @param refund_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRefundLogs(String begin, String end,int refund_status, PageCtrl pc) 
		throws Exception 
	{
		
		try 
		{
			return floorRefundLogMgrZJ.getRefundLogs(DateUtil.StrtoDate(begin),DateUtil.StrtoDate(end), refund_status, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRefundLogs",log);
		}
	}

	/**
	 * 修改退款记录
	 * @param request
	 * @throws Exception
	 */
	public void modRefundLog(long refund_id,int refund_status,String refund_people) 
		throws Exception 
	{
		try 
		{
			String refund_time = DateUtil.NowStr();
			
			DBRow refundlog = new DBRow();
			refundlog.add("refund_people",refund_people);
			refundlog.add("refund_status",refund_status);
			refundlog.add("refund_time",refund_time);
			floorRefundLogMgrZJ.modRefundLog(refund_id,refundlog);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modRefundLog",log);
		}
	}
	
	/**
	 * 根据运单号模糊查询退款记录
	 * @param refund_delivery_code
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchRefundLogByDeliveryCode(String refund_delivery_code,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorRefundLogMgrZJ.searchRefundLogByDeliveryCode(refund_delivery_code, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"searchRefundLogByDeliveryCode",log);
		}
	}
	
	/**
	 * 申请退款
	 * @param request
	 * @throws Exception
	 */
	public void applyForRefundLog(HttpServletRequest request)
		throws Exception
	{
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		String refund_people = adminLoggerBean.getAccount();//操作人
		
		String errorDeliveryCode = StringUtil.getString(request,"errorDeliveryCode");
		String[] errorDeliveryCodes = errorDeliveryCode.split(",");
		for (int i = 0; i < errorDeliveryCodes.length; i++) 
		{
			int refund_status;//退款记录状态
			
			DBRow refundlog = floorRefundLogMgrZJ.searchDetailRefundLogByDeliveryCode(errorDeliveryCodes[i]);
			if(refundlog!=null)
			{
				if(refundlog.get("refund_status",0) == RefundLogKey.YES)
				{
					continue;
				}
				
				refund_status = applyForRefund(errorDeliveryCodes[i]);
				long refund_id = refundlog.get("refund_id",0l);
				
				
				modRefundLog(refund_id, refund_status, refund_people);
			}
			else
			{
				refund_status = applyForRefund(errorDeliveryCodes[i]);
				addRefundLog(errorDeliveryCodes[i], refund_people, refund_status);
			}
		}
	}

	/**
	 * 
	 * @param errorDeliveryCode
	 * 运行出错返回false，算作退款失败
	 * @return
	 */
	public int applyForRefund(String errorDeliveryCode)
	{
		int status;
		try 
		{
			////system.out.println("调用web service");//调用web service
			Random ra = new Random();
			int aa = ra.nextInt();
			if(aa%2==0)
			{
				status = RefundLogKey.YES;
			}
			else
			{
				status = RefundLogKey.NO;
			}
		} 
		catch (Exception e) 
		{
			status = RefundLogKey.NO;
		}
		return status;
	}
	
	public void setFloorRefundLogMgrZJ(FloorRefundLogMgrZJ floorRefundLogMgrZJ) 
	{
		this.floorRefundLogMgrZJ = floorRefundLogMgrZJ;
	}

}
