package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorTransportOutboundMgrZJ;
import com.cwc.app.iface.zj.TransportOutboundMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class TransportOutboundMgrZJ implements TransportOutboundMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportOutboundMgrZJ floorTransportOutboundMgrZJ;
	
	/**
	 * 
	 * @param dbs
	 * @param transport_id
	 * @throws Exception
	 */
	public void addTransportOutboundSub(DBRow[] dbs, long transport_id,String machine_id)
		throws Exception 
	{
		try
		{
			delTransportOutboundByTransportId(transport_id,machine_id);
			for (int i = 0; i < dbs.length; i++) 
			{
				floorTransportOutboundMgrZJ.addTransportOutBound(dbs[i]);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 添加出庫，無需刪除
	 * @param dbs
	 * @param transport_id
	 * @throws Exception
	 */
	public void addTransportOutboundSubNoDel(DBRow[] dbs)
		throws Exception 
	{
		try
		{
			for (int i = 0; i < dbs.length; i++) 
			{
				floorTransportOutboundMgrZJ.addTransportOutBound(dbs[i]);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	public DBRow[] compareTransportOutboundByTransportId(long transport_id)
		throws Exception 
	{
		
		try
		{
			return (floorTransportOutboundMgrZJ.compareTransportDetailOutByTransportId(transport_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 删除转运出货
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransportOutboundByTransportId(long transport_id,String machine_id)
		throws Exception 
	{
		try 
		{
			floorTransportOutboundMgrZJ.delTransportOutBoundByTransportId(transport_id,machine_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运计划与转运发货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportDetailOutByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportOutboundMgrZJ.compareTransportDetailOutByTransportId(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compareTransportDetailOutByTransportId",log);
		}
	}

	@Override
	public DBRow[] getTransportOutboundByTransportId(long transport_id)
		throws Exception 
	{
		try 
		{
			return floorTransportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportOutboundByTransportId",log);
		}
	}
	
	/**
	 * 转运发货数量比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] transportOutboundCompareCount(long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportOutboundMgrZJ.transportDetailOutCompareCountByTransportId(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"transportDetailOutCompareCountByTransportId",log);
		}
	}
	
	/**
	 *  验证同张单据 不同机器序列号相同
	 * @param sn
	 * @param machine_id
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportOutboundBySNNotMachine(String sn,String machine_id,long transport_id)
		throws Exception
	{
		try
		{
			return floorTransportOutboundMgrZJ.getTransportOutboundBySNNotMachine(sn, machine_id, transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportOutboundBySNNotMachine",log);
		}
	}

	public void setFloorTransportOutboundMgrZJ(FloorTransportOutboundMgrZJ floorTransportOutboundMgrZJ) 
	{
		this.floorTransportOutboundMgrZJ = floorTransportOutboundMgrZJ;
	}
}
