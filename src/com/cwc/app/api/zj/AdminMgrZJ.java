package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class AdminMgrZJ implements AdminMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorAdminMgr fam ;
	private FloorAccountMgrSbb floorAccountMgrSbb ;
	/**
	 * @param machieId
	 * @param password
	 */
	public DBRow MachineLogin(String account, String password)
		throws AccountOrPwdIncorrectException,AccountNotPermitLoginException, Exception 
	{
		DBRow detailAdmin = null;
		try 
		{
			detailAdmin = fam.getDetailAdminByAccount(account);
			
			if ( detailAdmin != null )
			{
				if(!detailAdmin.getString("pwd").equals( StringUtil.getMD5(password))){
					throw new AccountOrPwdIncorrectException();
				}
				//先检查下角色是否存在，因为删除角色并不会删除帐号
				//如果角色不存在，则不让登录，以免出错
				//改动人员的 表的结构过后， 多查询多数据
				/*DBRow row = fam.getDetailRoleByAdgid(detailAdmin.get("adgid",0l)) ;
				if(fam.getDetailRoleByAdgid(detailAdmin.get("adgid",0l))==null)
				{
					throw new AccountOrPwdIncorrectException();
				}
				 */
				DBRow[] departments =  floorAccountMgrSbb.getAccountDepartmentAndPost(detailAdmin.get("adid", 0l));
				DBRow[]  warehouses =  floorAccountMgrSbb.getAccountWarehouseAndArea(detailAdmin.get("adid", 0l));
				detailAdmin.add("departments", departments);
				detailAdmin.add("warehouses", warehouses);
				//获取所有帐户的头像
				DBRow[] portraits =  floorAccountMgrSbb.getAccountPortrait();
				for (int i = 0; i < portraits.length; i++) 
				{
					DBRow[]  warehouses1 =  floorAccountMgrSbb.getAccountWarehouseAndArea(portraits[i].get("adid", 0l));
					portraits[i].add("warehouses", warehouses1);
				}
				
				detailAdmin.add("portraits",portraits);
				
				//获取个人所有头像
				DBRow[] perPortraits =  floorAccountMgrSbb.getPersonalPortrait(detailAdmin.get("adid", 0l));
				detailAdmin.add("perPortraits", perPortraits);

				if(departments == null || departments.length < 1){
					throw new AccountOrPwdIncorrectException();
				}
				if ( detailAdmin.get("llock",0)==1)
				{
					throw new AccountNotPermitLoginException();
				}
			}
		} 
		catch (AccountOrPwdIncorrectException e) 
		{
			detailAdmin = null;
		}
		catch (AccountNotPermitLoginException e) 
		{
			detailAdmin = null;
		}
		return (detailAdmin);
	}
	public void setFam(FloorAdminMgr fam) {
		this.fam = fam;
	}
	public void setFloorAccountMgrSbb(FloorAccountMgrSbb floorAccountMgrSbb) {
		this.floorAccountMgrSbb = floorAccountMgrSbb;
	}
	

}
