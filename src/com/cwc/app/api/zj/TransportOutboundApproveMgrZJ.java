package com.cwc.app.api.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.transport.NoExistTransportDetailException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportOutboundApproveMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportOutboundMgrZJ;
import com.cwc.app.iface.zj.TransportOutboundApproveMgrIFaceZJ;
import com.cwc.app.key.ApproveTransportOutboundKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class TransportOutboundApproveMgrZJ implements TransportOutboundApproveMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportOutboundApproveMgrZJ floorTransportOutboundApproveMgrZJ;
	private FloorTransportOutboundMgrZJ floorTransportOutboundMgrZJ;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorTransportMgrLL floorTransportMgrLL;
	
	public void addTransportOutbound(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long transport_id = StringUtil.getLong(request,"transport_id");
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			this.addTransportOutboundApproveSub(transport_id, adminLoggerBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运发货审核提交
	 * @param transport_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addTransportOutboundApproveSub(long transport_id,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		DBRow[] differents = floorTransportOutboundMgrZJ.transportDetailOutCompareCountByTransportId(transport_id);
		DBRow[] differentsSN = floorTransportOutboundMgrZJ.compareTransportDetailOutByTransportId(transport_id);
		DBRow para = new DBRow();
		
		if(differents.length>0||differentsSN.length>0)//无差异就不创建审核了
		{
			String commit_account = adminLoggerBean.getEmploye_name();
			String commit_date = DateUtil.NowStr();
			int different_count = differents.length;
			int different_sn_count = differentsSN.length;
			
			DBRow dbrow = new DBRow();
			dbrow.add("transport_id",transport_id);
			dbrow.add("commit_account",commit_account);
			dbrow.add("commit_date",commit_date);
			dbrow.add("different_count",different_count);
			dbrow.add("different_sn_count",different_sn_count);
			
			long tsa_id = floorTransportOutboundApproveMgrZJ.addSendApprove(dbrow);
			
			for (int i = 0; i < differents.length; i++) 
			{
				long pc_id = differents[i].get("pc_id",0l);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				String product_name = product.getString("p_name");
				String product_code = product.getString("p_code");
				
				float plan_count = differents[i].get("plan_count",0f);
				float out_count = differents[i].get("out_count",0f);
				
				DBRow difference = new DBRow();
				difference.add("product_name",product_name);
				difference.add("product_code",product_code);
				difference.add("product_id",pc_id);
				difference.add("transport_count",plan_count);
				difference.add("transport_send_count",out_count);
				difference.add("tsa_id",tsa_id);
				
				floorTransportOutboundApproveMgrZJ.addSendApproveDetail(difference);
			}
			
			for (int i = 0; i < differentsSN.length; i++) 
			{
				long pc_id = differentsSN[i].get("pc_id",0l);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				String product_name = product.getString("p_name");
				String product_code = product.getString("p_code");
				
				float plan_count = differentsSN[i].get("plan_count",0f);
				float out_count = differentsSN[i].get("out_count",0f);
				String serial_number = differentsSN[i].getString("serial_number");
				
				DBRow differenceSN = new DBRow();
				differenceSN.add("product_name",product_name);
				differenceSN.add("product_code",product_code);
				differenceSN.add("tsads_pc_id",pc_id);
				differenceSN.add("transport_count",plan_count);
				differenceSN.add("transport_send_count",out_count);
				differenceSN.add("tsa_id",tsa_id);
				differenceSN.add("serial_number",serial_number);
				
				floorTransportOutboundApproveMgrZJ.addTransportSendApproveDetailSN(differenceSN);
			}
			
			para.add("transport_status",TransportOrderKey.INTRANSIT);//正常运输
			
		}
		else
		{
			para.add("transport_status",TransportOrderKey.INTRANSIT);//正常运输
		}
		
		floorTransportMgrZJ.modTransport(transport_id, para);
	}

	/**
	 * 审核发货不同
	 * @param tsa_id
	 * @param tad_ids
	 * @param notes
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void approveTransportOutboundDifferent(long tsa_id, long[] tsad_ids,String[] notes,long[] tsads_ids,String[] noteSNs,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		try 
		{
			for (int i = 0; i < tsad_ids.length; i++) 
			{
				DBRow modpara = new DBRow();
				modpara.add("note",notes[i]);
				modpara.add("approve_account",adminLoggerBean.getEmploye_name());
				modpara.add("approve_status",ApproveTransportOutboundKey.APPROVE);
				
				floorTransportOutboundApproveMgrZJ.modSendApproveDetail(tsad_ids[i], modpara);
			}
			
			for (int i = 0; i < tsads_ids.length; i++) 
			{
				DBRow modpara = new DBRow();
				modpara.add("note",noteSNs[i]);
				modpara.add("approve_account",adminLoggerBean.getEmploye_name());
				modpara.add("approve_status",ApproveTransportOutboundKey.APPROVE);
				
				floorTransportOutboundApproveMgrZJ.modTransportSendApproveDetailSN(tsads_ids[i], modpara);
			}
			
			DBRow[] sendDifferent = floorTransportOutboundApproveMgrZJ.getApproveTransportDetailByTsa(tsa_id);
			
			long transport_id = floorTransportOutboundApproveMgrZJ.getDetailTransportSendApprove(tsa_id).get("transport_id",0l);
			DBRow transport =floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			//扣除库存
//			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
//			deInProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
//			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
//			deInProductStoreLogBean.setOid(transport_id);
//			deInProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
//			deInProductStoreLogBean.setOperation(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT_AUDIT));
/*			
			for (int j = 0; j < sendDifferent.length; j++) 
			{
				if(sendDifferent[j].get("different_count",0f)>0)//出库时扣的有可能是散件回退库存不知回退散件还是套装，所以只扣，不回退
				{
					floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,transport.get("send_psid",0l),sendDifferent[j].get("product_id",0l),sendDifferent[j].get("different_count",0f));
				}
			}
			
*/			DBRow[] noApprove = floorTransportOutboundApproveMgrZJ.getApproveTransportDetailWihtStatus(tsa_id,ApproveTransportOutboundKey.WAITAPPROVE);//查找未审核的不同
			DBRow[] hasApprove = floorTransportOutboundApproveMgrZJ.getApproveTransportDetailWihtStatus(tsa_id,ApproveTransportOutboundKey.APPROVE);//查找已审核的不同
			DBRow[] noApproveSN = floorTransportOutboundApproveMgrZJ.getTransportSendApproveDetailSNWithStatus(tsa_id, ApproveTransportOutboundKey.WAITAPPROVE);
			DBRow[] hasApproveSN = floorTransportOutboundApproveMgrZJ.getTransportSendApproveDetailSNWithStatus(tsa_id, ApproveTransportOutboundKey.APPROVE);
			
			DBRow modApproveTransportSend = new DBRow();
			
			if(noApprove.length==0&&noApproveSN.length==0)//差异都已审核过，变更交货单审核状态
			{
				modApproveTransportSend.add("approve_status",ApproveTransportOutboundKey.APPROVE);
				modApproveTransportSend.add("approve_account",adminLoggerBean.getEmploye_name());
				modApproveTransportSend.add("approve_date",DateUtil.NowStr());

			}
			modApproveTransportSend.add("difference_approve_count",hasApprove.length);
			modApproveTransportSend.add("difference_sn_approve_count",hasApproveSN.length);
			floorTransportOutboundApproveMgrZJ.modSendApprove(tsa_id,modApproveTransportSend);
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "出库审核:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}


	/**
	 * 转运发货审核
	 * @param tsa_id
	 * @return
	 * @throws Exception
	 */
	public long approveTransportOutboundForJbpm(long tsa_id) 
		throws Exception 
	{
		return (tsa_id);
	}

	
	public DBRow[] fillterTransportOutboundApprove(long send_psid,long receive_psid, PageCtrl pc, int approve_status, String sorttype)
		throws Exception 
	{
		try 
		{
			return floorTransportOutboundApproveMgrZJ.fillterTransportOutboundApprove(send_psid, receive_psid, pc, approve_status, sorttype);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 根据转运发货审核ID获得审核明细
	 * @param tsa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportOutboundApproverDetailsByTsaid(long tsa_id,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorTransportOutboundApproveMgrZJ.getApproveTransportDetailByTsa(tsa_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public DBRow[] getTransportOutboundApproveDetailSNsByTsaid(long tsa_id,PageCtrl pc)
		throws Exception
	{
		return floorTransportOutboundApproveMgrZJ.getTransportSendApproveDetailSNByTsaId(tsa_id,pc);
	}
	
	/**
	 * 审核检查库存
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkApproveStorage(long tsa_id,long transport_id)
		throws Exception
	{
		try 
		{
			DBRow[] transportSendDetails = floorTransportOutboundApproveMgrZJ.getApproveTransportDetailByTsa(tsa_id);
			if(transport_id==0)
			{
				transport_id = floorTransportOutboundApproveMgrZJ.getDetailTransportSendApprove(tsa_id).get("transport_id",0l);
			}
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			long ps_id = transport.get("send_psid",0l);
			DBRow productCount = new DBRow();
			
			ArrayList<DBRow> result = new ArrayList<DBRow>();
			
			boolean orderIsLacking = false;
			for (int j=0; j<transportSendDetails.length; j++)
			{
					long pid = StringUtil.getLong(transportSendDetails[j].getString("product_id"));
					DBRow product = floorProductMgr.getDetailProductByPcid(pid);//每个商品信息，为了获得商品
	
					float quantity = StringUtil.getFloat(transportSendDetails[j].getString("different_count"));
	
					if (product.get("union_flag",0)==0)//0散件
					{
						int lacking = 0;
	
						DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,pid);
	
						if (detailPro==null)
						{
							lacking = 1;
							orderIsLacking = true;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity+this.getCumProductQuantity(productCount, pid))
							{
								lacking = 1;
								orderIsLacking = true;
							}
						}
						this.addCumProductQuantity(productCount, pid, quantity);
						
						//对原来的购物车中的item增加一个缺货标记字段
						transportSendDetails[j].add("lacking", lacking);
					}
					else if (product.get("union_flag",0)==1)//1套装
					{
						int lacking = 0;
						boolean standardUnionLacking = false;
						boolean manualUnionLacking = false;
						
						DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,pid);
							
						if (detailPro==null)
						{
							lacking = 1;
							standardUnionLacking = true;
						}
						else
						{
	
							if (detailPro.get("store_count", 0f)<quantity)
							{
								lacking = 1;
								standardUnionLacking = true;
							}
						}
						//标准套装有货
						if (lacking==0)
						{
							transportSendDetails[j].add("lacking", lacking);
						}
						else//否则，再检查散件
						{
							lacking = 0;
							
							DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid( pid );
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2==null)	//	商品还没进库，当然缺货
								{
									lacking = 1;
									manualUnionLacking = true;
								}
								else
								{
	
									if (detailPro2.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*quantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = 1;
										manualUnionLacking = true;
									}
								}
								
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*quantity);
							}
	
							
							if (standardUnionLacking&&manualUnionLacking)
							{
								orderIsLacking = true;
							}
							transportSendDetails[j].add("lacking", lacking);
						}
						
						
					}
					
//					if(transportDetails[j].get("lacking",0)==1)//缺货加入缺货列表
//					{
						result.add(transportSendDetails[j]);//缺货不缺货都加入返回集合，页面上显示
//					}
					
				}
			
				return result.toArray(new DBRow[0]);
		} 
		catch(NoExistTransportDetailException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 累计商品需要的数量
	 * 累加一个订单某个商品需要的累计数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @return
	 */
	private float getCumProductQuantity(DBRow productCount,long pid)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			return(0);
		}
		else
		{
			return( StringUtil.getFloat(productCount.getString(String.valueOf(pid))) );
		}
	}

	/**
	 * 累计订单某个商品需要的数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addCumProductQuantity(DBRow productCount,long pid,float quantity)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			productCount.add(String.valueOf(pid), quantity);
		}
		else
		{
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}

	public void setFloorTransportOutboundMgrZJ(
			FloorTransportOutboundMgrZJ floorTransportOutboundMgrZJ) {
		this.floorTransportOutboundMgrZJ = floorTransportOutboundMgrZJ;
	}

	public void setFloorTransportOutboundApproveMgrZJ(
			FloorTransportOutboundApproveMgrZJ floorTransportOutboundApproveMgrZJ) {
		this.floorTransportOutboundApproveMgrZJ = floorTransportOutboundApproveMgrZJ;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
}
