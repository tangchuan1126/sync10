package com.cwc.app.api.zj;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.location.AreaHasExitsException;
import com.cwc.app.exception.location.AreaHasLocationCatalogException;
import com.cwc.app.exception.location.AreaNotExitsException;
import com.cwc.app.exception.location.LocationCatalogHasExitsException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class LocationMgrZJ implements LocationMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private FloorProductStoreMgr productStoreMgr;
	
	/**
	 * 添加区域
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addLocationArea(HttpServletRequest request) 
		throws AreaHasExitsException,Exception 
	{
		try 
		{
			String permitFile = "jpg,gif,bmp";
			
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/administrator/storage_location/uploadImage/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
						
			String area_name = upload.getRequestRow().getString("area_name");
			long area_psid = Long.parseLong(upload.getRequestRow().getString("area_psid"));
			String area_img = upload.getFileName();
			String attrStoragedoor = upload.getRequestRow().getString("attrStoragedoor");
			String attrUnloadLocation = upload.getRequestRow().getString("attrUnloadLocation");
			
			DBRow area = floorLocationMgrZJ.getDetailLocationArea(area_psid, area_name);
			
			if(area != null)
			{
				throw new AreaHasExitsException();
			}
			
			DBRow dbrow = new DBRow();
			dbrow.add("area_name",area_name);
			dbrow.add("area_psid",area_psid);
			dbrow.add("area_img",area_img);
			long areaId= this.addLocationAreaSub(dbrow);
			
			String[] itemsDoor = null;
			 DBRow drow = new DBRow();				
	         if(attrStoragedoor.length()!=0 && !attrStoragedoor.equals("")){
	       	  itemsDoor = attrStoragedoor.split(",");
		          for(int i = 0 ; i<itemsDoor.length ; i++){
		        	  long doorId = Long.parseLong(itemsDoor[i]);
		        	  	drow.add("area_id", areaId);
					  	drow.add("rel_dl_id", doorId);
					  	drow.add("dl_type", 1);
					 	floorLocationMgrZJ.addAreaDoorOrLocation(drow);
		          }
	         }
	         String[] itemsLocation = null;
			 DBRow locationRow = new DBRow();				
	         if(attrUnloadLocation.length() !=0 && !attrUnloadLocation.equals("")){
	        	 itemsLocation = attrUnloadLocation.split(",");
		          for(int i = 0 ; i<itemsLocation.length ; i++){
		        	  long locationId = Long.parseLong(itemsLocation[i]);
		        	  locationRow.add("area_id", areaId);
		        	  locationRow.add("rel_dl_id", locationId);
		        	  locationRow.add("dl_type", 2);
					  floorLocationMgrZJ.addAreaDoorOrLocation(locationRow);
		          }
	         }		
		
			return areaId;
		}
		catch(AreaHasExitsException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addLocationArea",log);
		}
	}
	
	public long addLocationAreaSub(DBRow row)
			throws Exception
	{
		try 
		{
			return floorLocationMgrZJ.addLocationArea(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addLocationAreaSub",log);
		}
	}
	
	/**
	 * 删除区域
	 * @param request
	 * @throws Exception
	 */
	public void delLocationArea(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long area_id = StringUtil.getLong(request,"area_id");
			
			DBRow[] locations = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id,null,null,null);
			
			if(locations.length>0)//判断区域下是否有位置
			{
				throw new AreaHasLocationCatalogException();
			}
			DBRow oldArea = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			
			FileUtil.delFile(Environment.getHome()+"administrator/storage_location/uploadImage/"+oldArea.getString("area_img"));
			
			floorLocationMgrZJ.delLocationArea(area_id);
			floorLocationMgrZJ.deleteAllAreaDoorAndLocation(area_id);
		} 
		catch(AreaHasLocationCatalogException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delLocationArea",log);
		}
	}

	/**
	 * 根据仓库ID获得区域
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationAreaByPsid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long psid = StringUtil.getLong(request,"ps_id");
			
			return (floorLocationMgrZJ.getLocationAreaByPsid(psid));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLocationAreaByPsid",log);
		}
	}

	public DBRow[] getLocationAreaByPsid(long psid)
		throws Exception
	{
		try 
		{
			return (floorLocationMgrZJ.getLocationAreaByPsid(psid));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLocationAreaByPsid",log);
		}
	}
	
	public DBRow[] getLocationAreaByPsidAndTitleId(long psid, long title_id)
			throws Exception
		{
			try 
			{
				return (floorLocationMgrZJ.getLocationAreaByPsidAndTitleId(psid,title_id));
			}
			catch (Exception e) 
			{
				throw new SystemException(e,"getLocationAreaByPsid",log);
			}
		}
	/**
	 * 修改区域不修改图片
	 * @param request
	 * @throws Exception
	 */
	public void modLocationArea(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long area_id = StringUtil.getLong(request,"area_id");
			
			String area_name = StringUtil.getString(request,"area_name");
			long area_psid = StringUtil.getLong(request,"area_psid");
			String attrStoragedoor = StringUtil.getString(request,"attrStoragedoor");
			String attrUnloadLocation = StringUtil.getString(request,"attrUnloadLocation");
			
			DBRow area = floorLocationMgrZJ.getDetailLocationArea(area_psid, area_name);
			
			if(area != null&&area_id!=area.get("area_id",0l))//多判断下是否自己本身
			{
				throw new AreaHasExitsException();
			}
			
			DBRow oldArea = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			if(oldArea == null)
			{
				throw new AreaNotExitsException();
			}
						
			DBRow para = new DBRow();
			
			para.add("area_name",area_name);
			para.add("area_psid",area_psid);
			
			floorLocationMgrZJ.modLocationArea(area_id, para);
			
			String[] itemsDoor = null;
			 DBRow drow = new DBRow();				
	         if(attrStoragedoor.length()!=0 && !attrStoragedoor.equals("")){
	        	 floorLocationMgrZJ.deleteAreaDoorOrLocation(area_id, 1);
	       	     itemsDoor = attrStoragedoor.split(",");
		          for(int i = 0 ; i<itemsDoor.length ; i++){
		        	  long doorId = Long.parseLong(itemsDoor[i]);
		        	  	drow.add("area_id", area_id);
					  	drow.add("rel_dl_id", doorId);
					  	drow.add("dl_type", 1);
					 	floorLocationMgrZJ.addAreaDoorOrLocation(drow);
		          }
	         }
	         String[] itemsLocation = null;
			 DBRow locationRow = new DBRow();				
	         if(attrUnloadLocation.length() !=0 && !attrUnloadLocation.equals("")){
	        	 floorLocationMgrZJ.deleteAreaDoorOrLocation(area_id, 2);
	        	 itemsLocation = attrUnloadLocation.split(",");
		          for(int i = 0 ; i<itemsLocation.length ; i++){
		        	  long locationId = Long.parseLong(itemsLocation[i]);
		        	  locationRow.add("area_id", area_id);
		        	  locationRow.add("rel_dl_id", locationId);
		        	  locationRow.add("dl_type", 2);
					  floorLocationMgrZJ.addAreaDoorOrLocation(locationRow);
		          }
	         }		
		}
		catch(AreaHasExitsException e)
		{
			throw e;
		}
		catch(AreaNotExitsException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modLocationArea",log);
		}
	}
	
	/**
	 * 修改区域并且修改区域图片
	 * @param request
	 * @throws Exception
	 */
	public void modLocationAreaWithImg(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String permitFile = "jpg,gif,bmp";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/administrator/storage_location/uploadImage/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
						
			String area_name = upload.getRequestRow().getString("area_name");
			long area_psid = Long.parseLong(upload.getRequestRow().getString("area_psid"));
			String area_img = upload.getFileName();
			long area_id = Long.parseLong(upload.getRequestRow().getString("area_id"));
			String attrStoragedoor = upload.getRequestRow().getString("attrStoragedoor");
			String attrUnloadLocation = upload.getRequestRow().getString("attrUnloadLocation");
			
			DBRow area = floorLocationMgrZJ.getDetailLocationArea(area_psid, area_name);
			if(area != null&&area_id!=area.get("area_id",0l))//多判断下是否自己本身
			{
				throw new AreaHasExitsException();
			}
			
			DBRow oldArea = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			if(oldArea == null)
			{
				throw new AreaNotExitsException();
			}
			FileUtil.delFile(Environment.getHome()+"administrator/storage_location/uploadImage/"+oldArea.getString("area_img"));
			
			DBRow para = new DBRow();
			
			para.add("area_name",area_name);
			para.add("area_psid",area_psid);
			para.add("area_img",area_img);
			
			floorLocationMgrZJ.modLocationArea(area_id, para);
			
			String[] itemsDoor = null;
			 DBRow drow = new DBRow();				
	         if(attrStoragedoor.length()!=0 && !attrStoragedoor.equals("")){
	        	 floorLocationMgrZJ.deleteAreaDoorOrLocation(area_id, 1);
	       	     itemsDoor = attrStoragedoor.split(",");
		          for(int i = 0 ; i<itemsDoor.length ; i++){
		        	  long doorId = Long.parseLong(itemsDoor[i]);
		        	  	drow.add("area_id", area_id);
					  	drow.add("rel_dl_id", doorId);
					  	drow.add("dl_type", 1);
					 	floorLocationMgrZJ.addAreaDoorOrLocation(drow);
		          }
	         }
	         String[] itemsLocation = null;
			 DBRow locationRow = new DBRow();				
	         if(attrUnloadLocation.length() !=0 && !attrUnloadLocation.equals("")){
	        	 floorLocationMgrZJ.deleteAreaDoorOrLocation(area_id, 2);
	        	 itemsLocation = attrUnloadLocation.split(",");
		          for(int i = 0 ; i<itemsLocation.length ; i++){
		        	  long locationId = Long.parseLong(itemsLocation[i]);
		        	  locationRow.add("area_id", area_id);
		        	  locationRow.add("rel_dl_id", locationId);
		        	  locationRow.add("dl_type", 2);
					  floorLocationMgrZJ.addAreaDoorOrLocation(locationRow);
		          }
	         }
			
		} 
		catch(AreaHasExitsException e)
		{
			throw e;
		}
		catch(AreaNotExitsException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modLocationAreaWithImg",log);
		}
	}
	
	/**
	 * 根据区域ID获得区域详细
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationAreaById(long area_id) 
		throws Exception 
	{
		try 
		{
			DBRow dbrow = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailLocationAreaById",log);
		}
	}

	/**
	 * 根据区域获得位置
	 * @param area_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllLocationCatalogByAreaId(long area_id,PageCtrl pc,String position,String type) 
		throws Exception 
	{
		
		try 
		{
			return (floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id, pc,position,type));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllLocationCatalogByAreaId",log);
		}
	}
	
	
	/**
	 * 添加位置
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addLocationCatalog(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long area_id = StringUtil.getLong(request,"area_id");
			DBRow area = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			
			String type = StringUtil.getString(request,"type");
			float slc_x = StringUtil.getFloat(request,"slc_x");
			float slc_y = StringUtil.getFloat(request,"slc_y");
			String position = StringUtil.getString(request,"position");
			String slc_ps_title = area.getString("title");
			
			DBRow location_catalog = floorLocationMgrZJ.getDetailLocationCatalog(area_id,position);
			if(location_catalog!=null)
			{
				throw new LocationCatalogHasExitsException();
			}
			DBRow dbrow = new DBRow();
			dbrow.add("slc_area",area_id);
			dbrow.add("slc_type",type);
			dbrow.add("slc_psid",area.get("area_psid",0l));
			dbrow.add("slc_x",slc_x);
			dbrow.add("slc_y",slc_y);
			dbrow.add("slc_position",position);
			dbrow.add("slc_position_all",slc_ps_title+area.getString("area_name")+type+position);
			dbrow.add("slc_ps_title",slc_ps_title);
			
			return floorLocationMgrZJ.addLocationCatalog(dbrow);
		} 
		catch(LocationCatalogHasExitsException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addLocationCatalog",log);
		}
	}
	
	/**
	 * 添加位置提交，neo4j图库创建节点
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addLocationCatalogSub(DBRow row)
			throws Exception
	{
		try 
		{
			long slc_id = floorLocationMgrZJ.addLocationCatalog(row);
			long ps_id = row.get("slc_psid",0l);
			
			Map<String,Object> storageLocation = new HashMap<String, Object>();
			storageLocation.put("is_three_dimensional",row.get("is_three_dimensional",0));
//			storageLocation.put("ps_id",ps_id);
			storageLocation.put("slc_area",row.get("slc_area",0l));
			storageLocation.put("slc_id",slc_id);
			storageLocation.put("slc_position",row.getString("slc_position"));
			storageLocation.put("slc_position_all",row.getString("slc_position_all"));
			storageLocation.put("slc_type",row.getString("slc_type"));
			storageLocation.put("slc_x",row.get("slc_x",0f));
			storageLocation.put("slc_y",row.get("slc_y",0f));
			
//			//system.out.println(storageLocation.toString());
			
		   //productStoreMgr.addNode(ps_id,NodeType.StorageLocationCatalog,storageLocation);
			
			return slc_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addLocationCatalogSub",log);
		}
	}
	

	/**
	 * 根据ID获得位置详细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationCatalogById(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long slc_id = StringUtil.getLong(request,"slc_id");
			
			return floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailLocationCatalogById",log);
		}
	}
	
	
	public DBRow getDetailLocationCatalogById(long slc_id)
		throws Exception
	{
		try 
		{
			return floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailLocationCatalogById",log);
		}
	}
	
	/**
	 * 删除位置
	 * @param request
	 * @throws Exception
	 */
	public void delLocationCatalog(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long slc_id = StringUtil.getLong(request,"slc_id");
			floorLocationMgrZJ.delLocationCatalog(slc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delLocationCatalog",log);
		}
	}
	
	
	/**
	 * 修改位置
	 * @param request
	 * @throws Exception
	 */
	public void modLocationCatalog(HttpServletRequest request)
		throws Exception
	{
		long area_id = StringUtil.getLong(request,"area_id");
		
		long slc_id = StringUtil.getLong(request,"slc_id");
		
		DBRow area = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
		DBRow oldlocation_catalog = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
		
		String type = StringUtil.getString(request,"type");
		float slc_x = StringUtil.getFloat(request,"slc_x");
		float slc_y = StringUtil.getFloat(request,"slc_y");
		String position = StringUtil.getString(request,"position");
		String slc_ps_title = area.getString("title");
		
		DBRow location_catalog = floorLocationMgrZJ.getDetailLocationCatalog(area_id,position);
		
		if(location_catalog!=null&&oldlocation_catalog.get("slc_id",0l)!=location_catalog.get("slc_id",0l))
		{
			throw new LocationCatalogHasExitsException();
		}
		
		DBRow para = new DBRow();
		para.add("slc_type",type);
		para.add("slc_x",slc_x);
		para.add("slc_y",slc_y);
		para.add("slc_position",position);
		para.add("slc_position_all",slc_ps_title+area.getString("area_name")+type+position);
		
		floorLocationMgrZJ.modLocationCatalog(slc_id, para);
	}
	
	/**
	 * 批量删除
	 * @param request
	 * @throws Exception
	 */
	public void delLocationCatalogs(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String slc_ids = StringUtil.getString(request,"slc_id");
			floorLocationMgrZJ.delLocationCatalogs(slc_ids);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 获取区域所对应的卸货门或装卸位置
	 */
	public DBRow[] getAreaDoorOrLocationByAreaId(long area_id, long type)
			throws Exception {
			try {
				return floorLocationMgrZJ.getAreaDoorOrLocationByAreaId(area_id,type);
			} catch (Exception e) {
				throw new SystemException(e,"getAreaDoorOrLocationByAreaId",log);
			}
	}
	
	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
