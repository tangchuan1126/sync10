package com.cwc.app.api.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.MoreLessOrEqualKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class LPTypeMgrZJ implements LPTypeMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	private FloorProductMgr floorProductMgr;
	
	/**
	 * 通过商品与目的仓库获得BLP类型,若无对应卖场关系，获得该商品可使用的所有BLP
	 */
	public DBRow[] getBLPTypeForSkuToShipNotNull(long pc_id, long to_ps_id)
		throws Exception 
	{
		try 
		{
			DBRow[] boxTypes = floorLPTypeMgrZJ.getBLPTypeForSkuToShip(pc_id, to_ps_id);
			
			if (boxTypes.length==0) 
			{
				boxTypes = floorLPTypeMgrZJ.getBLPTypeForSku(pc_id);
			}
			return boxTypes;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getBLPTypeForSkuToShipNotNull",log);
		}
	}

	/**
	 * 通过SKU与目的仓库获得CLP
	 */
	public DBRow[] getCLPTypeForSkuToShip(long pc_id, long title_id,long to_ps_id) 
		throws Exception 
	{
		try 
		{
			return floorLPTypeMgrZJ.getCLPTypeForSkuToShip(pc_id, title_id, to_ps_id); 
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getCLPTypeForSkuToShip",log);
		}
	}
	
	public DBRow[] getCLPTypeForSKUShipTo(long pc_id,long ship_to_id)
		throws Exception
	{
		return floorLPTypeMgrZJ.getCLPTypeForSKUShipTo(pc_id, ship_to_id);
	}
	
	public String getILPName(long ibt_id)
		throws Exception
	{
		DBRow ilpType = floorLPTypeMgrZJ.getDetailILPType(ibt_id);
		String ILPName = "";
		if (ilpType != null)
		{
			ILPName = ilpType.getString("ibt_total_length")+"X"+ilpType.getString("ibt_total_width")+"X"+ilpType.getString("ibt_total_height");
		}
		
		return ILPName;
	}
	
	/**
	 * 获得BLP名称
	 * @param box_type_id
	 * @return
	 * @throws Exception
	 */
	public String getBLPName(long box_type_id)
		throws Exception
	{
		try 
		{
			DBRow boxType = floorLPTypeMgrZJ.getDetailBLPType(box_type_id);
			String BLPName = "";
			if (boxType !=null)
			{
				if (boxType.get("box_inner_type",0)!=0) 
				{
					BLPName ="{"+this.getILPName(boxType.get("box_inner_type",0))+"}"+boxType.getString("box_total_length")+"X"+boxType.getString("box_total_width")+"X"+boxType.getString("box_total_height");
				}
				else
				{
					BLPName = boxType.getString("box_total_length")+"X"+boxType.getString("box_total_width")+"X"+boxType.getString("box_total_height");
				}
			}
			
			return BLPName;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getBLPName",log);
		}
	}
	
	public String getCLPName(long clp_type_id)
		throws Exception
	{
		try 
		{
			String CLPName = "";
			DBRow clpType = floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
			if (clpType !=null)
			{
				long box_type_id = clpType.get("sku_lp_box_type",0l);			
				if (box_type_id !=0) 
				{
					String BLPName = this.getBLPName(box_type_id);
					CLPName = "["+BLPName+"]"+clpType.getString("sku_lp_box_length")+"X"+clpType.getString("sku_lp_box_width")+"X"+clpType.getString("sku_lp_box_height");
				}
				else
				{
					CLPName = clpType.getString("sku_lp_box_length")+"X"+clpType.getString("sku_lp_box_width")+"X"+clpType.getString("sku_lp_box_height");
				}
			}
			return CLPName;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getCLPName",log);
		}
	}
	
	/**
	 * 获得jqgrid使用的BLP的select数据
	 */
	public String getBLPSelectForJqgrid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long pc_id = StringUtil.getLong(request,"pc_id");
			long to_ps_id = StringUtil.getLong(request,"to_ps_id");
			
			StringBuffer selectString = new StringBuffer("0:无");
			
			DBRow[] blps = this.getBLPTypeForSkuToShipNotNull(pc_id, to_ps_id);
			
			for (int i = 0; i < blps.length; i++) 
			{
				selectString.append(";");
				long blp_type_id = blps[i].get("box_type_id",0l);
				String BLPName = this.getBLPName(blp_type_id);
				selectString.append(blp_type_id+":"+BLPName);
			}
			return selectString.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getBLPSelectForJqgrid",log);
		}
	}
	
	/**
	 * 获得jqgrid使用的CLP的select数据
	 */
	public String getCLPSelectForJqgrid(HttpServletRequest request)
		throws Exception 
	{
		try
		{
			long pc_id = StringUtil.getLong(request,"pc_id");
			long to_ps_id = StringUtil.getLong(request,"to_ps_id");
			long title_id = StringUtil.getLong(request,"title_id");
			
			StringBuffer selectString = new StringBuffer("0:无");
			DBRow[] clps = this.getCLPTypeForSkuToShip(pc_id, title_id, to_ps_id);
			for (int i = 0; i < clps.length; i++) 
			{
				selectString.append(";");
				long clp_type_id = clps[i].get("sku_lp_type_id",0l);
				String CLPName = this.getCLPName(clp_type_id);
				
				selectString.append(clp_type_id+":"+CLPName);
				
			}
			return selectString.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getCLPSelectForJqgrid",log);
		}
	}
	
	public String getLPTypeSelectForJqgrid()
		throws Exception
	{
		ContainerTypeKey containerTypeKey = new ContainerTypeKey();
		ArrayList list = containerTypeKey.getContainerTypeKeys();
		StringBuffer selectString = new StringBuffer("-1:无");
		for (int i = 0; i < list.size(); i++)
		{
			String rowId = (String) list.get(i);

			selectString.append(";");
				
			String LPName = containerTypeKey.getContainerTypeKeyValue(rowId);
				
			selectString.append(rowId+":"+LPName);
		}
		
		return selectString.toString();
	}
	
	/**
	 * 计算商品需要的容器形式
	 * @param pc_id
	 * @param count
	 * @param clp_type_id
	 * @param blp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow calculateProductLPCount(long pc_id,int count,long clp_type_id,long blp_type_id)
		throws Exception
	{
		try 
		{
			int needCLP = 0;
			int needBLP = 0;
			int needILP = 0;
			int needOriginal = 0;
			String clp_name = "";
			String blp_name = "";
			String ilp_name = "";
			int clp_total_piece = 0;
			int blp_total_piece = 0;
			int all_count = count;
			//计算需要的CLP数量
			if(clp_type_id!=0) 
			{
				DBRow clp = floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
				clp_total_piece = clp.get("sku_lp_total_piece",0);
				blp_type_id = clp.get("sku_lp_box_type",0l);
				
				needCLP = count/clp_total_piece;
				
				count = count-(needCLP*clp_total_piece);
				clp_name = clp.getString("clp_name");
			}
			//计算需要的BLP数量
			if(blp_type_id!=0) 
			{
				DBRow blp = floorLPTypeMgrZJ.getDetailBLPType(blp_type_id);
				
				blp_total_piece = blp.get("box_total_piece",0);
				
				needBLP = count/blp_total_piece;
				
				count = count-(needBLP*blp_total_piece);
				blp_name = blp.getString("box_name");
			}
			
			//剩余需要的原样产品
			needOriginal = count;
			
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			DBRow result = new DBRow();
			result.add("needCLP",needCLP);
			result.add("clp_name",clp_name);
			result.add("clp_type_id",clp_type_id);
			result.add("clp_total_piece",clp_total_piece);
			result.add("needBLP",needBLP);
			result.add("blp_name",blp_name);
			result.add("blp_type_id",blp_type_id);
			result.add("blp_total_piece",blp_total_piece);
			result.add("needILP",needILP);
			result.add("needOriginal",needOriginal);
			result.add("p_name",product.getString("p_name"));
			result.add("all_count",all_count);

			return result;
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"calculateProductLPCount",log);
		}
	}
	
	public DBRow getDetailCLPType(long clp_type_id) 
		throws Exception 
	{
		try 
		{
			return floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailCLPType",log);
		}
	}
	
	public DBRow[] getCLPNameSelectForJqgridByPname(long title_id,String p_name)
		throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		long pc_id = product.get("pc_id",0l);
		DBRow[] clpTypes = floorLPTypeMgrZJ.getCLPTypeForSku(pc_id, title_id);
		
		ArrayList<DBRow> result = new ArrayList<DBRow>();
		DBRow r1 = new DBRow();
		r1.add("value",-1);
		r1.add("text","请选择");
		result.add(r1);
		DBRow r2 = new DBRow();
		r2.add("value",0);
		r2.add("text","无");
		result.add(r2);
		
		for (int i = 0; i < clpTypes.length; i++) 
		{
			long clp_type_id = clpTypes[i].get("sku_lp_type_id",0l);
			String clpName = this.getCLPName(clp_type_id);
			
			DBRow row = new DBRow();
			row.add("value",clp_type_id);
			row.add("text",clpName);
			result.add(row);
		}
		
		return result.toArray(new DBRow[0]);
	}
	
	public DBRow[] getBLPNameSelectForJqgridByPname(String p_name)
		throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		long pc_id = product.get("pc_id",0l);
		DBRow[] blpTypes = floorLPTypeMgrZJ.getBLPTypeForSku(pc_id);
		
		ArrayList<DBRow> result = new ArrayList<DBRow>();
		DBRow r1 = new DBRow();
		r1.add("value",-1);
		r1.add("text","请选择");
		result.add(r1);
		DBRow r2 = new DBRow();
		r2.add("value",0);
		r2.add("text","无");
		result.add(r2);
		
		for (int i = 0; i < blpTypes.length; i++) 
		{
			long blp_type_id = blpTypes[i].get("box_type_id",0l);
			String blpName = this.getBLPName(blp_type_id);
			
			DBRow row = new DBRow();
			row.add("value",blp_type_id);
			row.add("text",blpName);
			result.add(row);
		}
		
		return result.toArray(new DBRow[0]);
	}
	
	public DBRow[] getILPNameSelectForJqgridByPname(String p_name)
		throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		long pc_id = product.get("pc_id",0l);
		DBRow[] ilpTypes = floorLPTypeMgrZJ.getILPTypeForSku(pc_id);
		
		ArrayList<DBRow> result = new ArrayList<DBRow>();
		DBRow r1 = new DBRow();
		r1.add("value",-1);
		r1.add("text","请选择");
		result.add(r1);
		DBRow r2 = new DBRow();
		r2.add("value",0);
		r2.add("text","无");
		result.add(r2);
		
		for (int i = 0; i < ilpTypes.length; i++) 
		{
			long ilp_type_id = ilpTypes[i].get("ibt_id",0l);
			String ilpName = this.getILPName(ilp_type_id);

			DBRow row = new DBRow();
			row.add("value",ilp_type_id);
			row.add("text",ilpName);
			result.add(row);
		}
		
		return result.toArray(new DBRow[0]);
		
	}
	
	public DBRow getDetailCLP(long clp_type_id)
		throws Exception
	{
		try 
		{
			return floorLPTypeMgrZJ.getDetailCLP(clp_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailCLP",log);
		}
	}
	
	public DBRow getDetailBLPType(long blp_type_id)
		throws Exception
	{
		try 
		{
			return floorLPTypeMgrZJ.getDetailBLPType(blp_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailBLPType",log);
		}
	}
	
	public DBRow getDetailILPType(long ilp_type_id)
		throws Exception
	{
		try 
		{
			return floorLPTypeMgrZJ.getDetailILPType(ilp_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailILPType",log);
		}
	}

	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	/**zyj
	 * 通过商品ID，查询BOX TYPE or  SHIP　TO
	 * @param pcid
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public DBRow findClpTypesOrShiptoByPcId(long pcid, int limit) throws Exception {
		try{
			DBRow result = null;
			DBRow[] box_re = new DBRow[0];
			DBRow[] bps_re = new DBRow[0];
			int is_more = MoreLessOrEqualKey.EQUAL;
			String ship_to_name = "";
			//查询出box容器类型
			DBRow[] boxs = floorLPTypeMgrZJ.findClpTypeWithoutShipTo(pcid, 0L);
			//是否存在去某个shipTo恰巧有limit个
			DBRow[] shipToCounts = floorLPTypeMgrZJ.findClpShipToInfo(pcid, 0, 0);
			if(boxs.length > limit || shipToCounts.length > 1 || (boxs.length > 0 && shipToCounts.length > 0))
			{
				is_more = MoreLessOrEqualKey.MORE;
			}
			//如果存在非特供的box容器类型，最多显示limit个
			if(boxs.length > 0)
			{
				if(boxs.length > limit)
				{
					box_re = new DBRow[limit];
					System.arraycopy(boxs, 0, box_re, 0, limit);
				}
				else
				{
					box_re = new DBRow[boxs.length];
					System.arraycopy(boxs, 0, box_re, 0, boxs.length);
				}
			}
			//如果不存在非特供，显示特供box容器类型
			else
			{
				//是否存在去某个shipTo恰巧有limit个
				DBRow[] shipToCountEquals = floorLPTypeMgrZJ.findClpShipToInfo(pcid, limit, MoreLessOrEqualKey.EQUAL);
				//存在，取一个
				if(shipToCountEquals.length > 0)
				{
					DBRow shipToCountEqual = shipToCountEquals[0];
					long ship_to_id = shipToCountEqual.get("ship_to_id", 0L);
					DBRow[] boxPsIndex = floorLPTypeMgrZJ.getCLPTypeForSKUShipTo(pcid,ship_to_id);
					bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
					System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
					ship_to_name = shipToCountEqual.getString("ship_to_name");
					if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
					{
						is_more = MoreLessOrEqualKey.MORE;
					}
				}
				//不存在，取小于limit个的
				else
				{
					DBRow[] shipToCountLesses = floorLPTypeMgrZJ.findClpShipToInfo(pcid, limit, MoreLessOrEqualKey.LESS);
					//存在小于limit个的，取一个
					if(shipToCountLesses.length > 0)
					{
						DBRow shipToCountLess = shipToCountLesses[0];
						long ship_to_id = shipToCountLess.get("ship_to_id", 0L);
						DBRow[] boxPsIndex = floorLPTypeMgrZJ.getCLPTypeForSKUShipTo(pcid,ship_to_id);
						bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
						System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
						ship_to_name = shipToCountLess.getString("ship_to_name");
						if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
						{
							is_more = MoreLessOrEqualKey.MORE;
						}
					}
					//不存在小于limit个的，取大于的
					else
					{
						DBRow[] shipToCountMores = floorLPTypeMgrZJ.findClpShipToInfo(pcid, limit, MoreLessOrEqualKey.MORE);
						if(shipToCountMores.length > 0)
						{
							DBRow shipToCountMore = shipToCountMores[0];
							long ship_to_id = shipToCountMore.get("clp_ship_to_id", 0L);
							DBRow[] boxPsIndex = floorLPTypeMgrZJ.getCLPTypeForSKUShipTo(pcid,ship_to_id);
							bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
							System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
							ship_to_name = shipToCountMore.getString("ship_to_name");
							if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
							{
								is_more = MoreLessOrEqualKey.MORE;
							}
						}
					}
				}
			}
			if(box_re.length > 0 || bps_re.length > 0)
			{
				result = new DBRow();
				result.add("ship_to_name", ship_to_name);
				result.add("box_types", box_re);
				result.add("box_ship_tos", bps_re);
				result.add("is_more", is_more);
			}
			return result;
		}catch (Exception e) {
			throw new SystemException(e, "findClpTypesOrShiptoByPcId", log);

 		}
 	}

}
