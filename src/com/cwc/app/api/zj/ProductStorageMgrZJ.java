package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.exception.productStorage.NoExistStorageException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.tjh.FloorOrderProcessMgrTJH;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zj.FloorProductStorageMgrZJ;
import com.cwc.app.iface.zj.ProductStorageMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class ProductStorageMgrZJ implements ProductStorageMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStorageMgrZJ floorProductStorageMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorOrderProcessMgrTJH floorOrderProcessMgrTJH;
	private FloorProductLineMgrTJH floorProductLineMgrTJH;
	private FloorCatalogMgr floorCatalogMgr;
	
	public DBRow[] getAllProductStorageByPsid(String[] ps_ids, long catalog_id, long pro_line_id) throws Exception {
		return floorProductStorageMgrZJ.getAllProductStorageByPsid(ps_ids, catalog_id, pro_line_id);
	}
	
	public DBRow[] getAllProductStorage(long catalog_id, long pro_line_id) throws Exception {
		return floorProductStorageMgrZJ.getAllProductStorage(catalog_id, pro_line_id);
	}
	
	public DBRow[] getAllAliveProductStorage(long catalog_id, long pro_line_id) throws Exception {
		return floorProductStorageMgrZJ.getAllAliveProductStorage(catalog_id, pro_line_id);
	}
	
	/**
	 * 下载仓库库存
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downLoadProductStorage(HttpServletRequest request) throws Exception {
		String sheetName = "所有商品";
		
		long ps_id = StringUtil.getLong(request, "ps_id");
		
		Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
		ArrayList al = tree.getAllSonNode(ps_id);
		al.add(String.valueOf(ps_id));
		
		long catalog_id = StringUtil.getLong(request, "catalog_id");
		long product_line_id = StringUtil.getLong(request, "product_line");
		
		if (product_line_id != 0)// 为0则不查询子分类
		{
			DBRow product_line = floorProductLineMgrTJH.getProductLineById(product_line_id);
			
			sheetName = "产品线-" + product_line.getString("name");
		}
		
		String catalog_ids = "";
		// if(product_line_id.contains("-p"))//选择的是产品线,产品线第一层
		// {
		// long pro_line_id = Long.parseLong(product_line_id.substring(0,product_line_id.indexOf("-")));
		//
		//
		// }
		// else if(!product_line_id.contains("-p")&&!product_line_id.equals(""))//产品线下拉框第二层
		// {
		// if(Long.parseLong(product_line_id)!=0)
		// {
		// catalog_id = Long.parseLong(product_line_id);
		// }
		//
		// }
		// 选择产品线第二层，实际上选择的是商品分类
		if (catalog_id != 0) {
			DBRow catalog = floorCatalogMgr.getDetailProductCatalogById(catalog_id);
			sheetName = sheetName + "商品分类-" + catalog.getString("title");
		}
		
		DBRow storage = floorProductStorageMgrZJ.getProductStorageCatalogByPsid(ps_id);
		
		DBRow[] product_storages;
		
		if (ps_id == 0) {
			product_storages = this.getAllProductStorage(catalog_id, product_line_id);
		} else {
			product_storages = this.getAllProductStorageByPsid((String[]) al.toArray(new String[0]), catalog_id,
					product_line_id);
		}
		
		POIFSFileSystem fs = null;// 获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
				+ "/administrator/product/ExportProductStorage.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件
		
		wb.setSheetName(0, sheetName);
		HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
		
		HSSFRow row = sheet.getRow(0);
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 创建一个居中格式
		style.setLocked(false);// 设置不锁定
		style.setWrapText(true);
		
		DBRow[] productStoreCatalogs = floorCatalogMgr.getAllSendProductStoreCatalog();
		if (ps_id == 0) {
			for (int i = 0; i < productStoreCatalogs.length; i++) {
				row.createCell((5 + i)).setCellValue(productStoreCatalogs[i].getString("title"));
			}
		}
		
		for (int i = 0; i < product_storages.length; i++) {
			
			DBRow productStorage = product_storages[i];
			long pc_id = productStorage.get("pc_id", 0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			row = sheet.createRow((int) i + 1);
			if (product == null) {
				continue;
			}
			
			row.createCell(0).setCellValue(productStorage.getString("pc_id"));
			row.getCell(0).setCellStyle(style);
			
			row.createCell(1).setCellValue(productStorage.getString("cid"));
			row.getCell(1).setCellStyle(style);
			
			row.createCell(2).setCellValue(product.getString("p_name"));
			row.getCell(2).setCellStyle(style);
			
			row.createCell(3).setCellValue(productStorage.getString("store_count_alert", "全库不支持"));
			row.getCell(3).setCellStyle(style);
			
			row.createCell(4).setCellValue(productStorage.getString("store_count"));
			row.getCell(4).setCellStyle(style);
			
			if (ps_id == 0) {
				for (int j = 0; j < productStoreCatalogs.length; j++) {
					DBRow productStore = floorProductMgr.getDetailProductStorageByRscIdPcid(
							productStoreCatalogs[j].get("id", 0l), productStorage.get("pc_id", 0l));
					
					if (productStore == null) {
						productStore = new DBRow();
					}
					
					row.createCell((5 + j)).setCellValue(productStore.get("store_count", 0f));
				}
			}
		}
		String path;
		if (ps_id == 0) {
			path = "upl_excel_tmp/全库.xls";
		} else {
			path = "upl_excel_tmp/" + storage.getString("title") + ".xls";
		}
		
		FileOutputStream fout = new FileOutputStream(Environment.getHome() + path);
		wb.write(fout);
		fout.close();
		
		return (path);
	}
	
	public DBRow[] xmlProductStore(long product_line_id) throws Exception {
		DBRow[] productStoreCatalogs = floorCatalogMgr.getAllSendProductStoreCatalog();
		DBRow[] product_storages = this.getAllProductStorage(0, product_line_id);
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < product_storages.length; i++) {
			DBRow productStorage = product_storages[i];
			DBRow product = floorProductMgr.getDetailProductByPcid(productStorage.get("pc_id", 0l));
			
			DBRow returnProductStore = new DBRow();
			
			returnProductStore.add("sku", product.getString("p_name"));
			returnProductStore.add("price", product.get("unit_price", 0d));
			returnProductStore.add("weight", product.get("weight", product.get("weight", 0f)));
			returnProductStore.add("volume", product.get("volume", 0f));
			returnProductStore.add("store_all", productStorage.get("store_count", 0f));
			
			for (int j = 0; j < productStoreCatalogs.length; j++) {
				DBRow productStore = floorProductMgr.getDetailProductStorageByRscIdPcid(
						productStoreCatalogs[j].get("id", 0l), productStorage.get("pc_id", 0l));
				
				if (productStore == null) {
					productStore = new DBRow();
				}
				returnProductStore.add(productStoreCatalogs[j].getString("title").toLowerCase(),
						productStore.get("store_count", 0f));
			}
			
			list.add(returnProductStore);
		}
		
		return list.toArray(new DBRow[0]);
		
	}
	
	public String downloadStorageCost(HttpServletRequest request) throws Exception, FileTypeException {
		return "";
	}
	
	public String[] importStorageAlert(HttpServletRequest request) throws Exception, FileTypeException {
		try {
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String msg[] = new String[2];
			
			upload.setFileName("In_Storage_Alert_" + DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			upload.setFileSize(1024 * 1024 * 5);
			
			int flag = upload.upload(request);
			
			String storage_title = upload.getRequestRow().getString("storage_title");
			msg[1] = storage_title;
			
			if (flag == 2) {
				msg[0] = "只能上传:" + permitFile;
				////system.out.println(msg);
				throw new FileTypeException();// 采购单文件上传格式不对
			} else if (flag == 1) {
				msg[0] = "上传出错";
				////system.out.println(msg);
			} else {
				msg[0] = upload.getFileName();
			}
			return msg;
		} catch (FileTypeException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "importProduct", log);
		}
	}
	
	/**
	 * 检查导入警戒数据
	 * 
	 * @param filename
	 * @param storage_title
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkImportStorageAlert(String filename, String storage_title) throws Exception {
		InputStream is = null;
		Workbook rwb = null;
		
		try {
			DBRow product_storage = floorProductStorageMgrZJ.getDetailProductStorageCatalogByTitle(storage_title);
			
			if (product_storage == null) {
				throw new NoExistStorageException();
			}
			
			long ps_id = product_storage.get("id", 0l);
			
			String path = "";
			
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			DBRow[] store_alerts = storeAlertData(rwb);
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; i < store_alerts.length; i++) {
				boolean error = false;
				StringBuffer error_message = new StringBuffer("<br/><font color='red'>错误行号：" + i + "<br/>");
				try {
					DBRow product = floorProductMgr.getDetailProductByPname(store_alerts[i].getString("p_name"));
					
					if (product.get("pc_id", 0l) != Long.parseLong(store_alerts[i].getString("pc_id")))// 商品名与商品ID不对应
					{
						error = true;
						error_message.append("商品名异常，必须使用导出文件修改<br/>");
					}
					
					if (Long.parseLong(store_alerts[i].getString("storage_id")) != ps_id)// 仓库ID不对应
					{
						error = true;
						error_message.append("对应仓库异常，必须使用导出文件修改<br/>");
					}
					
					float alert = Float.parseFloat(store_alerts[i].getString("store_count_alert"));
					
					if (!(alert > 0))// 警戒值不大于0
					{
						error = true;
						error_message.append("警戒值范围异常，必须大于0<br/>");
					}
				} catch (NumberFormatException e) {
					error = true;
					error_message.append("未使用导出文件修改或警戒值非数字<br/>");
				}
				
				if (error) {
					error_message.append("</font>");
					store_alerts[i].add("error_line", error_message);
					list.add(store_alerts[i]);
				}
			}
			return (list.toArray(new DBRow[0]));
		} catch (NoExistStorageException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "checkImportStorageAlert", log);
		}
	}
	
	/**
	 * 保存上传库存警戒值
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void saveImportStorageAlert(HttpServletRequest request) throws Exception {
		InputStream is = null;
		Workbook rwb = null;
		
		try {
			String filename = StringUtil.getString(request, "filename");
			
			String path = "";
			
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			DBRow[] store_alerts = storeAlertData(rwb);
			
			for (int i = 0; i < store_alerts.length; i++) {
				long pc_id = Long.parseLong(store_alerts[i].getString("pc_id"));
				long ps_id = Long.parseLong(store_alerts[i].getString("storage_id"));
				float store_alert_count = Float.parseFloat(store_alerts[i].getString("store_count_alert"));
				
				floorProductStorageMgrZJ.modProductStorageAlert(pc_id, ps_id, store_alert_count);
			}
		} catch (Exception e) {
			throw new SystemException(e, "saveImportStorageAlert", log);
		} finally {
			rwb.close();
			is.close();
			
		}
	}
	
	/**
	 * 上传文件商品信息转换DBRow[]
	 * 
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] storeAlertData(Workbook rwb) throws Exception {
		HashMap productFiled = new HashMap();
		productFiled.put(0, "pc_id");
		productFiled.put(1, "storage_id");
		productFiled.put(2, "p_name");
		productFiled.put(3, "store_count_alert");
		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns(); // excel表字段数
		int rsRows = rs.getRows(); // excel表记录行数
		
		for (int i = 1; i < rsRows; i++) {
			DBRow pro = new DBRow();
			
			boolean result = false;// 判断是否纯空行数据
			for (int j = 0; j < rsColumns; j++) {
				////system.out.println("i" + i + "---j:" + j);
				String content = rs.getCell(j, i).getContents().trim();
				if (j == 2) {
					content = content.toUpperCase();
				}
				pro.add(productFiled.get(j).toString(), content); // 转换成DBRow数组输出,去掉空格
				
				ArrayList proName = pro.getFieldNames();// DBRow 字段名
				for (int p = 0; p < proName.size(); p++) {
					if (!pro.getString(proName.get(p).toString()).trim().equals(""))// 去掉空格
					{
						result = true;
					}
				}
			}
			
			if (result)// 不是纯空行就加入到DBRow
			{
				al.add(pro);
			}
		}
		
		return (al.toArray(new DBRow[0]));
	}
	
	public void setFloorProductStorageMgrZJ(FloorProductStorageMgrZJ floorProductStorageMgrZJ) {
		this.floorProductStorageMgrZJ = floorProductStorageMgrZJ;
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	
	public void setFloorOrderProcessMgrTJH(FloorOrderProcessMgrTJH floorOrderProcessMgrTJH) {
		this.floorOrderProcessMgrTJH = floorOrderProcessMgrTJH;
	}
	
	public DBRow[] getAllProductStorageByPcid(long catalog_id, long psid, int type, long product_line_id,
			int union_flag, PageCtrl pc) throws Exception {
		Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
		ArrayList al = tree.getAllSonNode(psid);
		al.add(String.valueOf(psid));
		
		return floorProductStorageMgrZJ.getProductStorageInCids(product_line_id, catalog_id,
				(String[]) al.toArray(new String[0]), type, union_flag, pc);
	}
	
	public DBRow[] getProductStorageByName(long catalog_id, long ps_id, long product_line_id, String name,
			int union_flag, PageCtrl pc) throws Exception {
		try {
			String catalog_ids = "";
			
			return floorProductStorageMgrZJ.getProductStorageByName(catalog_ids, ps_id, name, union_flag, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getProductStorageByName", log);
		}
	}
	
	/**
	 * 获得某仓库下某商品的采购数量
	 */
	public DBRow[] getPurchaseCount(long ps_id, long pc_id, int[] purchase_status, boolean do_group) throws Exception {
		try {
			return (floorProductStorageMgrZJ.getPurchaseCount(ps_id, pc_id, purchase_status, do_group));
		} catch (Exception e) {
			throw new SystemException(e, "getPurchaseCount", log);
		}
	}
	
	/**
	 * 查询某仓库下的在途商品数量
	 * 
	 * @param ps_id
	 * @param pc_id
	 * @param delivery_status
	 * @param transport_status
	 * @param do_group
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryingCount(long ps_id, long pc_id, int[] delivery_status, int[] transport_status,
			boolean do_group) throws Exception {
		try {
			long[] pcids;
			
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			if (null != product && product.get("orignal_pc_id", 0l) != 0) {
				
				DBRow[] productUnion = floorProductMgr.getProductUnionsBySetPid(pc_id);
				
				pcids = new long[productUnion.length];
				
				for (int i = 0; i < productUnion.length; i++) {
					pcids[i] = productUnion[i].get("pid", 0l);
				}
			} else {
				pcids = new long[1];
				pcids[0] = pc_id;
			}
			
			return (floorProductStorageMgrZJ.getDeliveryingCount(ps_id, pcids, delivery_status, transport_status,
					do_group));
		} catch (Exception e) {
			throw new SystemException(e, "getDeliveryingCount", log);
		}
	}
	
	/**
	 * 搜索仓库
	 * 
	 * @param def_flag
	 * @param return_flag
	 * @param sample_flag
	 * @param storage_type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchProductStorageCatalog(int def_flag, int return_flag, int sample_flag, int storage_type,
			PageCtrl pc) throws Exception {
		try {
			return floorProductStorageMgrZJ.searchProductStorageCatalog(def_flag, return_flag, sample_flag,
					storage_type, pc);
		} catch (Exception e) {
			throw new SystemException(e, "searchProductStorageCatalog", log);
		}
	}
	
	public String loadProductStorageForNexSearch() throws Exception {
		DBRow[] storageCatalogs = this.searchProductStorageCatalog(0, 0, 0, 0, null);
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++) {
			DBRow storage = new DBRow();
			storage.add("id", storageCatalogs[i].get("id", 0l));
			storage.add("name", storageCatalogs[i].getString("title"));
			
			list.add(storage);
		}
		
		String c = new JsonObject(list.toArray(new DBRow[0])).toString();
		return c;
	}
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo(int storage_type, long storage_type_id) throws Exception {
		try {
			return floorProductStorageMgrZJ.getProductStorageCatalogsByTypeShipTo(storage_type, storage_type_id, null);
		} catch (Exception e) {
			throw new SystemException(e, "getProductStorageCatalogsByTypeShipTo", log);
		}
	}
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo0(int storage_type, long storage_type_id) throws Exception {
		try {
			return floorProductStorageMgrZJ.getProductStorageCatalogsByTypeShipTo0(storage_type, storage_type_id, null);
		} catch (Exception e) {
			throw new SystemException(e, "getProductStorageCatalogsByTypeShipTo0", log);
		}
	}
	
	/**
	 * 
	 * 获取单条仓库信息
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-29下午2:35:32<br>
	 * @author: cuicong
	 */
	public DBRow getProductStorageCatalogByPsid(int ps_id) throws Exception {
		
		return floorProductStorageMgrZJ.getProductStorageCatalogByPsid(ps_id);
		
	}
	
	public void setFloorProductLineMgrTJH(FloorProductLineMgrTJH floorProductLineMgrTJH) {
		this.floorProductLineMgrTJH = floorProductLineMgrTJH;
	}
	
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
	public FloorProductStorageMgrZJ getFloorProductStorageMgrZJ() {
		return floorProductStorageMgrZJ;
	}
	
	public FloorProductMgr getFloorProductMgr() {
		return floorProductMgr;
	}
	
	public FloorOrderProcessMgrTJH getFloorOrderProcessMgrTJH() {
		return floorOrderProcessMgrTJH;
	}
	
	public FloorProductLineMgrTJH getFloorProductLineMgrTJH() {
		return floorProductLineMgrTJH;
	}
	
	public FloorCatalogMgr getFloorCatalogMgr() {
		return floorCatalogMgr;
	}
	
	@Override
	public DBRow[] getProductStorageCatalogLikeName(String title) throws Exception {
		return floorProductStorageMgrZJ.getProductStorageCatalogLikeTitle(title);
	}
}
