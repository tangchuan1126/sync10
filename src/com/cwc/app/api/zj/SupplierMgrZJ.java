package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.supplier.SupplierLoginTimeOutException;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.floor.api.zj.FloorSupplierMgrZJ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.SupplierMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class SupplierMgrZJ implements SupplierMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorSupplierMgrZJ floorSupplierMgrZJ;
	private AdminMgrIFace adminMgr;
	private FloorSupplierMgrTJH floorSupplierMgrTJH;
	/**
	 * 供应商登录
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow supplierLogin(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request,"account");
			String password = StringUtil.getString(request,"pwd");
			
			return (floorSupplierMgrZJ.SupplierLogin(id,StringUtil.getMD5(password)));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 供应商修改密码
	 * @param request
	 * @throws Exception
	 */
	public void modSupplierPwd(HttpServletRequest request)
		throws SupplierLoginTimeOutException,OldPwdIncorrectException,Exception
	{
		try
		{
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			DBRow supplier = adminLoggerBean.getAttach();
			if(supplier == null)
			{
				throw new SupplierLoginTimeOutException();
			}
			long id = supplier.get("id",0l);
			String pwd = StringUtil.getString(request,"pwd");
			String oldpwd = StringUtil.getString(request,"oldpwd");
			
			DBRow modSupplier = floorSupplierMgrTJH.getDetailSupplier(id);
			
			if(modSupplier.getString("password").equals(StringUtil.getMD5(oldpwd)))
			{
				DBRow para = new DBRow();
				para.add("password",StringUtil.getMD5(pwd));
				floorSupplierMgrTJH.modSupplier(para, id);
				//重新登录
				adminMgr.adminLogout(request);
			}
			else
			{
				throw new OldPwdIncorrectException();
			}
		}
		catch(SupplierLoginTimeOutException e)
		{
			throw e;
		}
		catch(OldPwdIncorrectException e)
		{
			throw e;
		}
		catch(Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void passwordInit()throws Exception
	{
		DBRow[] suppliers = floorSupplierMgrTJH.getAllSupplier(null);
		
		for (int j = 0; j < suppliers.length; j++) 
		{
			long sid = suppliers[j].get("id",0l);
			String p = StringUtil.getMD5(suppliers[j].getString("id"));
			DBRow para = new DBRow();
			para.add("password",p);
			floorSupplierMgrTJH.modSupplier(para, sid);
		}
	}

	public void setFloorSupplierMgrZJ(FloorSupplierMgrZJ floorSupplierMgrZJ) {
		this.floorSupplierMgrZJ = floorSupplierMgrZJ;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setFloorSupplierMgrTJH(FloorSupplierMgrTJH floorSupplierMgrTJH) {
		this.floorSupplierMgrTJH = floorSupplierMgrTJH;
	}
}
