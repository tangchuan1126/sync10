package com.cwc.app.api.zj;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.zj.FloorFileMgrZJ;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
public class FileMgrZJ implements com.cwc.app.iface.zj.FileMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorFileMgrZJ floorFileMgrZJ;
	
	public DBRow[] getFileByWithIdAndType(long with_id, int with_type)
		throws Exception 
	{
		try 
		{
			return (floorFileMgrZJ.getFilesByWithAndType(with_id, with_type));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getFileByWithIdAndType",log);
		}
	}

	/**
	 * 上传文件（交货单,转运单用）
	 */
	public void addFile(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String permitFile = "jpg,gif,bmp,jpeg,png,xls,doc,xlsx,docx,rar";
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_imags_tmp/");
			upload.setPermitFile(permitFile);
			int flag = upload.upload(request);

			long file_with_id = upload.getRequestRow().get("association_id",0l);
			int association_type = upload.getRequestRow().get("association_type",0);
			int file_with_class = upload.getRequestRow().get("file_with_class",0);
			String sn = upload.getRequestRow().getString("sn");
			String path = upload.getRequestRow().getString("path");
			if (flag==2)
			{
				throw new FileTypeException();//�ɹ����ļ��ϴ���ʽ����
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{	
				String temp_url = Environment.getHome()+"upl_imags_tmp/"+upload.getFileName();
				String[] temp = upload.getFileName().split("\\."); 
				String fileType = temp[temp.length-1];
				String fileName = sn + file_with_id+"_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+"."+fileType;
				String url = Environment.getHome()+"upload/"+path+"/"+fileName;
				FileUtil.moveFile(temp_url,url);
				AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
				DBRow file = new DBRow();
				file.add("file_name",fileName);
				file.add("file_with_id",file_with_id);
				file.add("file_with_type",association_type);
				file.add("file_with_class",file_with_class);
				file.add("upload_adid",adminLoggerBean.getAdid());
				file.add("upload_time",DateUtil.NowStr());
				floorFileMgrZJ.addFile(file);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"returnProductForFile",log);
		}
	}
	
	public void delFile(long file_id)
		throws Exception
	{
		try 
		{
			floorFileMgrZJ.delFile(file_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delFile",log);
		}
	}
	public void setFloorFileMgrZJ(FloorFileMgrZJ floorFileMgrZJ) {
		this.floorFileMgrZJ = floorFileMgrZJ;
	}

}
