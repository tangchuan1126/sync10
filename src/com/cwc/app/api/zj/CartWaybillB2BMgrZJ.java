package com.cwc.app.api.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.Cart;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.iface.zj.CartWaybillB2BMgrIFaceZJ;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class CartWaybillB2BMgrZJ implements CartWaybillB2BMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	private DBRow[] cartWaybillB2bProduct = null;
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	/**
	 * 清空购物车
	 */
	public void cleanCart(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			HttpSession session = StringUtil.getSession(request);
			
			if ( session.getAttribute(Config.cartWaybillB2B) != null )
			{
				session.removeAttribute(Config.cartWaybillB2B);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"cleanCart",log);
		}
	}

	/**
	 * 放入购物车
	 */
	public void putToCart(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			HttpSession session = StringUtil.getSession(request);
			
			long b2b_detail_id = StringUtil.getLong(request,"b2b_detail_id");
			
			int clp_count = StringUtil.getInt(request,"clp_count");
			int blp_count = StringUtil.getInt(request,"blp_count");
			int ilp_count = StringUtil.getInt(request,"ilp_count");
			int orignail_count = StringUtil.getInt(request,"count");
			
			//先将B2B订单明细移出购物车
			this.removeProduct(session,b2b_detail_id);
			//明细放入购物车
			this.put2Cart(session,b2b_detail_id,clp_count,blp_count,ilp_count, orignail_count);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"putToCart",log);
		}
	}
	
	/**
	 * 放入session
	 * @param session
	 * @param transport_detail_id
	 * @param clp_type
	 * @param clp_count
	 * @param blp_type
	 * @param blp_count
	 * @param ilp_type
	 * @param ilp_count
	 * @param orignail_count
	 * @throws Exception
	 */
	private void put2Cart(HttpSession session,long b2b_detail_id,int clp_count,int blp_count,int ilp_count,int orignail_count) 
		throws Exception 
	{
		try 
		{
			DBRow b2bOrderItem = floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_detail_id);
			
			long clp_type = b2bOrderItem.get("clp_type_id",0l);
			long blp_type = b2bOrderItem.get("blp_type_id",0l);
			long ilp_type = b2bOrderItem.get("ilp_type_id",0l);
			long pc_id = b2bOrderItem.get("b2b_pc_id",0l);
			String lot_number = b2bOrderItem.getString("");
			
			DBRow row = new DBRow();
			row.add("b2b_detail_id",b2b_detail_id);
			row.add("pc_id",pc_id);
			row.add("clp_type",clp_type);
			row.add("clp_count",clp_count);
			row.add("blp_type",blp_type);
			row.add("blp_count",blp_count);
			row.add("ilp_type",ilp_type);
			row.add("ilp_count",ilp_count);
			row.add("orignail_count",orignail_count);
			row.add("lot_number",lot_number);
			
			DBRow clpType = floorLPTypeMgrZJ.getDetailCLP(clp_type);
			if (clpType==null)
			{
				clpType = new DBRow();
			}
			
			DBRow blpType = floorLPTypeMgrZJ.getDetailBLPType(blp_type);
			if (blpType==null)
			{
				blpType = new DBRow();
			}
			
			DBRow ilpType = floorLPTypeMgrZJ.getDetailILPType(ilp_type);
			if (ilpType == null)
			{
				ilpType = new DBRow();
			}
			
			int clpSumPiece = clpType.get("sku_lp_total_piece",0)*clp_count;
			int blpSumPiece = blpType.get("box_total_piece",0)*blp_count;
			int ilpSumPiece = ilpType.get("ibt_total",0)*ilp_count;
			row.add("product_count",clpSumPiece+blpSumPiece+ilpSumPiece+orignail_count);
			
			ArrayList<DBRow> al = null;
			if ( session.getAttribute(Config.cartWaybillB2B) == null )
			{
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				al = (ArrayList)session.getAttribute(Config.cartWaybillB2B);
				al.add(row);
			}
			session.setAttribute(Config.cartWaybillB2B,al);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"put2Cart",log);
		}
	}

	/**
	 * 从session中移除
	 * @param session
	 * @param transport_detail_id
	 * @throws Exception
	 */
	private void removeProduct(HttpSession session, long transport_detail_id)
		throws Exception 
	{
		if (session.getAttribute(Config.cartWaybillB2B)!= null)
		{
			DBRow rowOld = new DBRow();
			ArrayList al = (ArrayList)session.getAttribute(Config.cartWaybillB2B);
			
			for(int i=0;i<al.size();i++)
			{
				Object value =  al.get(i);
				if (value instanceof Map)
				{
					Map<String,Object> alMap = (Map<String,Object>)value;
					rowOld = DBRowUtils.mapConvertToDBRow(alMap);
				}
				else if (value instanceof DBRow)
				{
					rowOld = (DBRow)value;
				}
				
				
				if ( StringUtil.getLong(rowOld.getString("b2b_detail_id")) == transport_detail_id)
				{
					al.remove(i);
				}
			}
			
			session.setAttribute(Config.cartWaybillB2B,al);
			
			Object c = session.getAttribute(Config.cartWaybillB2B);
		}
	}

	/**
	 * 从购物车内移除
	 */
	public void removeProduct(HttpServletRequest request) 
		throws Exception 
	{
		try
		{
			long b2b_detail_id = StringUtil.getLong(request,"b2b_detail_id");//order_item的主键
			
			this.removeProduct(StringUtil.getSession(request),b2b_detail_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"removeProduct",log);
		}
	}
	
	public DBRow[] copyCartItem(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartWaybillB2B) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartWaybillB2B);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				Object value = al.get(i);
				
				DBRow alRow = new DBRow();
				if (value instanceof Map)
				{
					Map<String,Object> alMap = (Map<String,Object>)value;
					alRow = DBRowUtils.mapConvertToDBRow(alMap);
				}
				else if (value instanceof DBRow)
				{
					alRow = (DBRow) value;
				}
				
				
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}
	
	public void flush(HttpSession session)
		throws Exception
	{
		try 
		{
			DBRow products[] = this.copyCartItem(session);
			//必须深度克隆，否则使用的只是引用，会把大量数据加到session中
			//数组不能直接克隆，需要一个个对象克隆，否则也是克隆地址引用
			DBRow newProducts[] = null;
			ArrayList<DBRow> newProductsAL = new ArrayList<DBRow>();
			for (int i=0; i<products.length; i++)
			{
				DBRow newProduct = new DBRow();
				newProduct.append(products[i]);
				newProductsAL.add(newProduct);
			}
			newProducts = (DBRow[])newProductsAL.toArray(new DBRow[0]);
			
			//设置购物车和购物车商品详细信息
			this.cartWaybillB2bProduct =  newProducts;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"flush",log);
		}
	}


	public DBRow[] getCartWaybillB2bProduct() {
		return cartWaybillB2bProduct;
	}

	public void setCartWaybillB2bProduct(DBRow[] cartWaybillB2bProduct) {
		this.cartWaybillB2bProduct = cartWaybillB2bProduct;
	}

	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}

	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}
}
