package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorSetupLogMgr;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.iface.zj.ProductCodeMgrIFaceZJ;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProductCodeMgrZJ implements ProductCodeMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductCodeMgrZJ floorProductCodeMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	
	private FloorSetupLogMgr floorSetupLogMgr;
	
	public void setFloorSetupLogMgr(FloorSetupLogMgr floorSetupLogMgr) {
		this.floorSetupLogMgr = floorSetupLogMgr;
	}
	
	/**
	 * 添加商品条码
	 */
	public DBRow addProductCode(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow ret = new DBRow();
			long pc_id = StringUtil.getLong(request,"pc_id");
			String p_code = StringUtil.getString(request,"p_code");
			int code_type = StringUtil.getInt(request,"code_type");
			p_code = p_code.toUpperCase();
			
			DBRow productCodeIsHas = floorProductCodeMgrZJ.getDetailProductCode(p_code);
			
			if(productCodeIsHas !=null)
			{
				int hasCodeType = productCodeIsHas.get("code_type", 0);
				if(hasCodeType != 4)
				{
					throw new ProductCodeIsExistException();
				}
				else
				{
					long pcode_id = productCodeIsHas.get("pcode_id", 0l);
					delProductCode(pcode_id);
				}
			}
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			long pcode_id = addProductCodeSub(pc_id, p_code, code_type, adminLoggerBean);
			DBRow[] codes = floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
			
			String amozonCode = "";
		 	String upcCode = "";
			for(DBRow code : codes){
				int codeType = code.get("code_type", 0);
				// 3 amazon 2 upc
				if(codeType == 3){
					amozonCode = code.getString("p_code");
				}else if(codeType == 2){
					upcCode = code.getString("p_code");
				}
			}
			
			String p_codes_str = "";
			if(!StringUtil.isBlank(amozonCode))
			{
				p_codes_str += "Amazon:"+amozonCode+"<br/>";
			}
			if(!StringUtil.isBlank(upcCode))
			{
				p_codes_str += "UPC:"+upcCode;
			}
			ret.add("status", "success");
			ret.add("html", p_codes_str);
			
			return ret;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductCode",log);
		}
	}

	/**
	 * 添加商品条码提交
	 * @param pc_id
	 * @param p_code
	 * @param code_type
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public long addProductCodeSub(long pc_id,String p_code,int code_type,AdminLoginBean adminLoggerBean) throws Exception {
		
		//验证条码是否重复
		DBRow oldProductCode = floorProductCodeMgrZJ.getProductCodeByPcidAndCodeType(pc_id, code_type);
		
		if(oldProductCode!=null) {
			
			DBRow oldProductCodePara = new DBRow();
			oldProductCodePara.add("code_type",CodeTypeKey.Old);
			floorProductCodeMgrZJ.modProductCode(oldProductCode.get("pcode_id",0l),oldProductCodePara);
			
			/*******************************************************************************************************/
			/** 
			 * 创建的时候, 在创建完成后执行此方法
			 * 更新的时候, 在更新前执行此方法
			 */
			DBRow logRow = new DBRow();
			//create/update/delete
			logRow.put("event", "update");
			//更新的表名
			logRow.put("table", "product_code");
			//主键字段名
			logRow.put("column", "pcode_id");
			//主键ID
			logRow.put("pri_key", oldProductCode.get("pcode_id",0l));
			//依赖表
			logRow.put("ref_table", "product");
			//依赖表主键字段名
			logRow.put("ref_column", "pc_id");
			//依赖表主键ID
			logRow.put("ref_pri_key", pc_id);
			//账号ID
			logRow.put("create_by", adminLoggerBean.getAdid());
			//设备类型//请求的来源
			logRow.put("device", ProductDeviceTypeKey.Web);
			//描述
			logRow.put("describe", "update product_code "+oldProductCode.get("pcode_id",0l));
			
			//具体更新的字段内容
			//将DBRow转换为数组
			DBRow[] content = DBRowUtils.dbRowConvertToArray(oldProductCodePara);
			logRow.put("content", content);
			
			floorSetupLogMgr.addSetupLog(logRow);
			
			/*******************************************************************************************************/
		}
		
		//新加条码
		DBRow product_code = new DBRow();
		product_code.add("pc_id",pc_id);
		product_code.add("p_code",p_code);
		product_code.add("code_type",code_type);
		product_code.add("create_adid",adminLoggerBean.getAdid());
		product_code.add("create_time",DateUtil.NowStr());
		
		long pcode_id = floorProductCodeMgrZJ.addProductCode(product_code);
		
		/*******************************************************************************************************/
		/** 
		 * 创建的时候, 在创建完成后执行此方法
		 * 更新的时候, 在更新前执行此方法
		 */
		DBRow _logRow = new DBRow();
		//create/update/delete
		_logRow.put("event", "create");
		//更新的表名
		_logRow.put("table", "product_code");
		//主键字段名
		_logRow.put("column", "pcode_id");
		//主键ID
		_logRow.put("pri_key", pcode_id);
		//依赖表
		_logRow.put("ref_table", "product");
		//依赖表主键字段名
		_logRow.put("ref_column", "pc_id");
		//依赖表主键ID
		_logRow.put("ref_pri_key", pc_id);
		//账号ID
		_logRow.put("create_by", adminLoggerBean.getAdid());
		//设备类型//请求的来源
		_logRow.put("device", ProductDeviceTypeKey.Web);
		//描述
		_logRow.put("describe", "create product_code "+pcode_id);
		
		//具体更新的字段内容
		//将DBRow转换为数组
		DBRow[] _content = DBRowUtils.dbRowConvertToArray(product_code);
		_logRow.put("content", _content);
		
		floorSetupLogMgr.addSetupLog(_logRow);
		
		/*******************************************************************************************************/
		
		//商品修改日志
		DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
		
		DBRow[] pcodes = getProductCodesByPcid(pc_id);
		
		for (int i = 0; i < pcodes.length; i++){
			
			if(pcodes[i].get("code_type",0)==CodeTypeKey.Main){
				
				product_log.add("p_code",pcodes[i].getString("p_code"));
			}
			
			if(pcodes[i].get("code_type",0)==CodeTypeKey.Amazon){
				
				product_log.add("p_code2",pcodes[i].getString("p_code"));
			}
			
			if(pcodes[i].get("code_type",0)==CodeTypeKey.UPC){
				
				product_log.add("upc",pcodes[i].getString("p_code"));
			}
		}
		
		product_log.add("account",adminLoggerBean.getAdid());
		product_log.add("edit_type",ProductEditTypeKey.UPDATE);
		product_log.add("edit_reason",ProductEditReasonKey.SELF);
		product_log.add("post_date",DateUtil.NowStr());
		floorProductLogsMgrZJ.addProductLogs(product_log);
		
		return (pcode_id);
	}
	
	/**
	 * 添加商品条码，不记录商品修改日志（添加商品专用）
	 * @param pc_id
	 * @param p_code
	 * @param code_type
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public long addProductCodeSubNoLog(long pc_id,String p_code,int code_type,AdminLoginBean adminLoggerBean) throws Exception {
		
		try {
			
			//验证条码是否重复
			DBRow oldProductCode = floorProductCodeMgrZJ.getProductCodeByPcidAndCodeType(pc_id, code_type);
			
			//条码类型相同，条码内容相同，不做任何操作（商品导入）
			if(oldProductCode!=null&&!oldProductCode.getString("p_code").equals(p_code)){
				
				DBRow oldProductCodePara = new DBRow();
				oldProductCodePara.add("code_type",CodeTypeKey.Old);
				floorProductCodeMgrZJ.modProductCode(oldProductCode.get("pcode_id",0l),oldProductCodePara);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow logRow = new DBRow();
				//create/update/delete
				logRow.put("event", "update");
				//更新的表名
				logRow.put("table", "product_code");
				//主键字段名
				logRow.put("column", "pcode_id");
				//主键ID
				logRow.put("pri_key", oldProductCode.get("pcode_id",0l));
				//依赖表
				logRow.put("ref_table", "product");
				//依赖表主键字段名
				logRow.put("ref_column", "pc_id");
				//依赖表主键ID
				logRow.put("ref_pri_key", pc_id);
				//账号ID
				logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				logRow.put("describe", "update product_code "+oldProductCode.get("pcode_id",0l));
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] content = DBRowUtils.dbRowConvertToArray(oldProductCodePara);
				logRow.put("content", content);
				
				floorSetupLogMgr.addSetupLog(logRow);
				
				/*******************************************************************************************************/
			}
			
			long pcode_id = 0;
			
			if(oldProductCode==null || !oldProductCode.getString("p_code").equals(p_code)){
				
				//新加条码
				DBRow product_code = new DBRow();
				product_code.add("pc_id",pc_id);
				product_code.add("p_code",p_code);
				product_code.add("code_type",code_type);
				product_code.add("create_adid",adminLoggerBean.getAdid());
				product_code.add("create_time",DateUtil.NowStr());
				
				pcode_id = floorProductCodeMgrZJ.addProductCode(product_code);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow logRow = new DBRow();
				//create/update/delete
				logRow.put("event", "create");
				//更新的表名
				logRow.put("table", "product_code");
				//主键字段名
				logRow.put("column", "pcode_id");
				//主键ID
				logRow.put("pri_key", pcode_id);
				//依赖表
				logRow.put("ref_table", "product");
				//依赖表主键字段名
				logRow.put("ref_column", "pc_id");
				//依赖表主键ID
				logRow.put("ref_pri_key", pc_id);
				//账号ID
				logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				logRow.put("describe", "create product_code "+pcode_id);
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] content = DBRowUtils.dbRowConvertToArray(product_code);
				logRow.put("content", content);
				
				floorSetupLogMgr.addSetupLog(logRow);
				
				/*******************************************************************************************************/
			}
			
			return (pcode_id);
		}catch (Exception e) {
			throw new SystemException(e,"addProductCodeSubNoLog",log);
		}
	}
	
	public void delProductCodeByPcid(long pc_id)
		throws Exception
	{
		try
		{
			floorProductCodeMgrZJ.delProductCodeByPcid(pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delProductCodeByPcid",log);
		}
	}
	
	public void delProductCode(long pcode_id)
			throws Exception
		{
			try
			{
				floorProductCodeMgrZJ.delProductCode(pcode_id);
			}
			catch (Exception e) 
			{
				throw new SystemException(e,"delProductCodeByPcid",log);
			}
		}
		
	
	/**
	 * 获得商品条码
	 */
	public DBRow getDetailProductCode(String p_code) 
		throws Exception 
	{
		try 
		{
			return floorProductCodeMgrZJ.getDetailProductCode(p_code);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailProductCode",log);
		}
	}

	
	/**
	 * 获得商品ID
	 */
	public DBRow[] getProductCodesByPcid(long pc_id) 
		throws Exception 
	{
		try 
		{
			return floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCodesByPcid",log);
		}
	}
	
	/**
	 * 
	 * @param pc_id
	 * @param code_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductCodeByPcidAndCodeType(long pc_id,int code_type)
		throws Exception
	{
		try 
		{
			return floorProductCodeMgrZJ.getProductCodeByPcidAndCodeType(pc_id, code_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCodeByPcidAndCodeType",log);
		}
	}
	
	
	public void initProductCode(HttpServletRequest request)
		throws Exception
	{
		DBRow[] products = floorProductMgr.getAllProductsOld(null);
		
		for (int i = 0; i < products.length; i++) 
		{
			String main = products[i].getString("p_code");
			String upc = products[i].getString("upc");
			String amazon = products[i].getString("p_code2");
			long pc_id = products[i].get("pc_id",0l);
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			if(!main.equals(""))
			{
				addProductCodeSub(pc_id, main, CodeTypeKey.Main,adminLoggerBean);
			}
			
			if(!upc.equals(""))
			{
				addProductCodeSub(pc_id, upc, CodeTypeKey.UPC,adminLoggerBean);
			}
			
			if(!amazon.equals(""))
			{
				addProductCodeSub(pc_id, amazon, CodeTypeKey.Amazon,adminLoggerBean);
			}
		}
	}
	
	/**
	 * 获得商品使用中的条码
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUseProductCodes(long pc_id)
		throws Exception
	{
		try
		{
			return floorProductCodeMgrZJ.getUseProductCodes(pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getUseProductCodes",log);
		}
	}
	
	
	
	/**
	 * 通过pcid、codeType、code查询code
	 * @param codeType
	 * @param pcCode
	 * @param pc_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月5日 上午9:59:08
	 */
	public DBRow[] getProductCodeByTypeAndCodePcid(int codeType, String pcCode, long pc_id) throws Exception
	{
		try
		{
			return floorProductCodeMgrZJ.getProductCodeByTypeAndCodePcid(codeType, pcCode, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCodeByTypeAndCodePcid",log);
		}
	}

	public void setFloorProductCodeMgrZJ(FloorProductCodeMgrZJ floorProductCodeMgrZJ) {
		this.floorProductCodeMgrZJ = floorProductCodeMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}
}
