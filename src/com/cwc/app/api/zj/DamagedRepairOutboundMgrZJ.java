package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorDamagedRepairOutboundMgrZJ;
import com.cwc.app.iface.zj.DamagedRepairOutboundMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class DamagedRepairOutboundMgrZJ implements DamagedRepairOutboundMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ;
	
	
	public void addDamagedRepairOutboundSub(DBRow[] dbs, long repair_id)
		throws Exception 
	{
		try 
		{
			delDamagedRepairOutboundByDamagedRepairId(repair_id);
			
			for (int i = 0; i < dbs.length; i++) 
			{
				floorDamagedRepairOutboundMgrZJ.addDamagedRepairOutbound(dbs[i]);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addDamagedRepairOutboundSub",log);
		}

	}

	/**
	 * 返修单发货比较
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareDamagedRepairOutboundByDamagedRepairId(long repair_id)
		throws Exception 
	{
		try 
		{
			return floorDamagedRepairOutboundMgrZJ.compareDamagedRepairPacking(repair_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compareDamagedRepairOutboundByDamagedRepairId",log);
		}
	}


	public void delDamagedRepairOutboundByDamagedRepairId(long repair_id)
		throws Exception 
	{
		try 
		{
			floorDamagedRepairOutboundMgrZJ.delDamagedRepairOutbound(repair_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delDamagedRepairOutboundByDamagedRepairId",log);
		}

	}

	/**
	 * 获得返修发货明细
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairOutboundByDamagedRepairId(long repair_id)
		throws Exception 
	{
		try 
		{
			return floorDamagedRepairOutboundMgrZJ.getDamagedRepairOutboundByRepairId(repair_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDamagedRepairOutboundByDamagedRepairId",log);
		}
	}

	public void setFloorDamagedRepairOutboundMgrZJ(
			FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ) {
		this.floorDamagedRepairOutboundMgrZJ = floorDamagedRepairOutboundMgrZJ;
	}

}
