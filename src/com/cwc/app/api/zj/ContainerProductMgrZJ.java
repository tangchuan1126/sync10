package com.cwc.app.api.zj;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorContainerProductMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.iface.zj.ContainerProductMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ContainerProductMgrZJ implements ContainerProductMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorContainerProductMgrZJ floorContainerProductMgrZJ;
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	
	public void addContainerProduct(long cp_pc_id, String cp_sn,float cp_quantity, long cp_lp_id) 
		throws Exception 
	{
		DBRow containerProduct = new DBRow();
		containerProduct.add("cp_pc_id",cp_pc_id);
		containerProduct.add("cp_quantity",cp_quantity);
		containerProduct.add("cp_lp_id",cp_lp_id);
		
		if(cp_sn!=null&&!cp_sn.equals(""))
		{
			DBRow containerProductSN = floorContainerProductMgrZJ.getContainerProductBySN(cp_sn);
			
			if(containerProductSN != null)
			{
				floorContainerProductMgrZJ.delContainerProduct(containerProductSN.get("cp_id",0l));
			}
			
			containerProduct.add("cp_sn",cp_sn);
		}
		
		floorContainerProductMgrZJ.addContainerProduct(containerProduct);
	}
	
	/**
	 * 清空托盘
	 * @param lp_id
	 * @throws Exception
	 */
	public void clearContainerProductByLP(long lp_id)
		throws Exception
	{
		try 
		{
			floorContainerProductMgrZJ.delContainerProductByLP(lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"clearContainerProductByLP",log);
		}
	}
	
	/**
	 * 打印托盘
	 * @param lp_id
	 * @param containerProducts
	 * @throws Exception
	 */
	public void printLicensePlate(long lp_id,ArrayList<DBRow> containerProducts)
		throws Exception
	{
		clearContainerProductByLP(lp_id);//先清空托盘
		
		for(int i = 0;i<containerProducts.size();i++)
		{
			this.addContainerProduct(containerProducts.get(i).get("pc_id",0l),containerProducts.get(i).getString("sn",""),containerProducts.get(i).get("quantity",0f),containerProducts.get(i).get("lp_id",0l));
		}
	}
	
	/**
	 * 获得托盘
	 * @param container
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainerByContainer(String container)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getDetailContainerByContainer(container);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailContainerByContainer",log);
		}
	}
	
	public DBRow getDetailContainer(String searchKey)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getDetailContai(searchKey);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailContai",log);
		}
	}
	
	/**
	 * 添加容器（打托盘时使用）
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addContainer(DBRow row)
		throws Exception
	{
		try 
		{
			return  floorContainerMgrZYZ.addContainer(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addContainer",log);
		}
	}
	/**
	 * 获得托盘上的商品
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContainerProductByContainer(long lp_id)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getContainerProductByContainer(lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getContainerProductByContainer",log);
		}
	}
	
	/**
	 * 根据托盘ID获得托盘上商品的基础信息
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductForContainer(long lp_id)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getProductForContainer(lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductForContainer",log);
		}
	}
	
	/**
	 * 根据托盘ID获得托盘商品的商品条码
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeForContainer(long lp_id)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getProductCodeForContainer(lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCodeForContainer",log);
		}
	}
	
	/**
	 * 根据托盘ID获得托盘上商品的序列号
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductForContainer(long lp_id)
		throws Exception
	{
		try 
		{
			return floorContainerProductMgrZJ.getSerialProductForContainer(lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getSerialProductForContainer",log);
		}
	}

	public DBRow getDetailContainer(long lpId) 
		throws Exception 
	{
		try 
		{
			return floorContainerProductMgrZJ.getDetailContainerByContainerId(lpId);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailContainer ById",log);
		}
	}
	@Override
	public void delContainerProductByLP(long lpId) throws Exception {
		try 
		{
			  floorContainerProductMgrZJ.delContainerProductByLP(lpId);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delContainerProductByLP ById",log);
		}
	}
	
	public void setFloorContainerProductMgrZJ(FloorContainerProductMgrZJ floorContainerProductMgrZJ) 
	{	
		this.floorContainerProductMgrZJ = floorContainerProductMgrZJ;
	}

	public void setFloorContainerMgrZYZ(FloorContainerMgrZYZ floorContainerMgrZYZ) {
		this.floorContainerMgrZYZ = floorContainerMgrZYZ;
	}
}
