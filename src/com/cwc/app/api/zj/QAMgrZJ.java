package com.cwc.app.api.zj;

import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.qa.ProductNotFindException;
import com.cwc.app.floor.api.zj.FloorAnswerMgrZJ;
import com.cwc.app.floor.api.zj.FloorQuestionMgrZJ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.QAMgrIFaceZJ;
import com.cwc.app.key.QAKey;
import com.cwc.app.lucene.zj.QuestionIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class QAMgrZJ implements QAMgrIFaceZJ 
{
	private FloorQuestionMgrZJ floorQuestionMgrZJ;
	private ProductMgrIFace productMgr;
	private FloorAnswerMgrZJ floorAnswerMgrZJ;
	private AdminMgrIFace adminMgr;
	static Logger log = Logger.getLogger("ACTION");
	
	
	/**
	 * 添加问题
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addQuestion(HttpServletRequest request) 
		throws ProductNotFindException,Exception
	{
		try 
		{
			long product_id = StringUtil.getLong(request,"product_id");
			DBRow product = productMgr.getDetailProductByPcid(product_id);
			
			if(product ==null)
			{
				throw new ProductNotFindException();//根据商品ID找不到商品，禁止添加问题
			}
			
			String question_title = StringUtil.getString(request,"question_title");
			String content = StringUtil.getString(request,"content");
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String questioner = adminLoggerBean.getAccount();
			
			
			String question_time = DateUtil.NowStr();
			String question_update_time = DateUtil.NowStr();
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1); 
				if(img.indexOf("_QA")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/customerservice_qa/upload_img/"+img;
					FileUtil.moveImg(ulr,url);
				}	
			}
			
			DBRow dbrow = new DBRow();
			dbrow.add("product_id",product_id);
			dbrow.add("question_title",question_title);
			dbrow.add("content",content.replaceAll("upl_imags_tmp","administrator/customerservice_qa/upload_img"));
			dbrow.add("questioner",questioner);
			dbrow.add("question_time",question_time);
			dbrow.add("question_update_time",question_update_time);
			
			long question_id = floorQuestionMgrZJ.addQuestion(dbrow);
			
			//添加索引
			String text=this.Html2Text(content);
			QuestionIndexMgr.getInstance().addIndex(question_id,question_title, text, question_update_time,questioner,product.getString("p_name"));
			
			return (question_id);
		} 
		catch(ProductNotFindException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addQuestion",log);
		}
	}

	
	/**
	 * 获得所有问题
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllQuestion(PageCtrl pc,int question_status) 
		throws Exception 
	{
		try 
		{
			return (floorQuestionMgrZJ.getAllQuestion(pc,question_status));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllQuestion",log);
		}
	}
	
	
	/**
	 * 根据问题ID获得问题详细
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailQuestionById(Long question_id)
		throws Exception
	{
		try 
		{
			return (floorQuestionMgrZJ.getDetailQuestionById(question_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailQuestionById",log);
		}
	}
	
	
	/**
	 * 修改问题
	 * @param request
	 * @throws Exception
	 */
	public void modQuestionById(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long question_id = StringUtil.getLong(request,"question_id");
			String question_title = StringUtil.getString(request,"question_title");
			
			String content = StringUtil.getString(request,"content");
			
			DBRow old = floorQuestionMgrZJ.getDetailQuestionById(question_id);
			String oldcontent = old.getString("content");
			
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			
			String oldimgs[] = StringUtil.regMatchers(oldcontent, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			HashMap newimg=new HashMap();
			
			//移动文章新图片
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1);
				if(img.indexOf("_QA")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/customerservice_qa/upload_img/"+img;
					newimg.put(img,img);
					FileUtil.moveImg(ulr,url);
				}
			}
			
			//修改后原本有，现在没有引用的图片删除
			for(int j=0;oldimgs!=null&&j<oldimgs.length;j++)
			{
				String oldimg=oldimgs[j].substring(oldimgs[j].lastIndexOf("/")+1);
				if(!newimg.containsKey(oldimg))
				{
					FileUtil.delFile(Environment.getHome()+"administrator/customerservice_qa/upload_img/"+oldimg);
				}
			}
			
			String question_update_time = DateUtil.NowStr();
			
			DBRow para =  new DBRow();
			para.add("question_update_time",question_update_time);
			para.add("content",content.replaceAll("upl_imags_tmp","administrator/customerservice_qa/upload_img"));
			para.add("question_title",question_title);
			
			floorQuestionMgrZJ.modQuestionById(question_id, para);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String questioner = adminLoggerBean.getAccount();
			//修改索引
			String text=this.Html2Text(content);
			DBRow product = productMgr.getDetailProductByPcid(old.get("product_id",0l));
			QuestionIndexMgr.getInstance().updateIndex(question_id, question_title, text, question_update_time,questioner,product.getString("p_name"));
		} 
		catch(Exception e) 
		{
			throw new SystemException(e,"modQuestionById",log);
		}
	}
	
	
	/**
	 * 修改已回答问题
	 * @param request
	 * @throws Exception
	 */
	public long modQuestionHasAnswer(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long question_id = StringUtil.getLong(request,"question_id");
			String question_title = StringUtil.getString(request,"question_title");
			
			String content = StringUtil.getString(request,"content");
			
			DBRow old = floorQuestionMgrZJ.getDetailQuestionById(question_id);
			String oldcontent = old.getString("content");
			String questioner = old.getString("questioner");
			
			
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			
			String oldimgs[] = StringUtil.regMatchers(oldcontent, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			HashMap newimg=new HashMap();
			
			//移动文章新图片
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1);
				if(img.indexOf("_QA")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/customerservice_qa/upload_img/"+img;
					newimg.put(img,img);
					FileUtil.moveImg(ulr,url);
				}
			}
			
			//修改后原本有，现在没有引用的图片删除
			for(int j=0;oldimgs!=null&&j<oldimgs.length;j++)
			{
				String oldimg=oldimgs[j].substring(oldimgs[j].lastIndexOf("/")+1);
				if(!newimg.containsKey(oldimg))
				{
					FileUtil.delFile(Environment.getHome()+"administrator/customerservice_qa/upload_img/"+oldimg);
				}
			}
			
			String question_update_time = DateUtil.NowStr();
			
			DBRow para =  new DBRow();
			para.add("question_update_time",question_update_time);
			para.add("content",content.replaceAll("upl_imags_tmp","administrator/customerservice_qa/upload_img"));
			para.add("question_title",question_title);
			
			floorQuestionMgrZJ.modQuestionById(question_id, para);
			
			//修改索引
			String text=this.Html2Text(content);
			DBRow product = productMgr.getDetailProductByPcid(old.get("product_id",0l));
			QuestionIndexMgr.getInstance().updateIndex(question_id, question_title, text, question_update_time,questioner,product.getString("p_name"));
			
			return (question_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modQuestionHasAnswer",log);
		}
	}
	
	
	
	/**
	 * 根据问题ID检索该问题的答案
	 * @param question_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAnswersByQuestionId(long question_id)
		throws Exception
	{
		try 
		{
			return floorAnswerMgrZJ.getAnswersByQuestionId(question_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAnswersByQuestionId",log);
		}
	}
	
	
	
	/**
	 * 添加答案
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addAnswer(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String answerer = adminLoggerBean.getAccount();
			
			String answer_content = StringUtil.getString(request,"answer_content");
			String answer_time = DateUtil.NowStr();
			
			long question_id = StringUtil.getLong(request,"question_id"); 
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(answer_content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1); 
				if(img.indexOf("_QA")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/customerservice_qa/upload_img/"+img;
					FileUtil.moveImg(ulr,url);
				}	
			}
			
			DBRow answer = new DBRow();
			answer.add("answerer",answerer);
			answer.add("answer_content",answer_content.replaceAll("upl_imags_tmp","administrator/customerservice_qa/upload_img"));
			answer.add("answer_time",answer_time);
			answer.add("question_id",question_id);
			
			long answer_id = floorAnswerMgrZJ.addAnswer(answer);
			
			((QAMgrIFaceZJ)AopContext.currentProxy()).addAnswerForJbpm(question_id);
			
			DBRow para = new DBRow();
			para.add("question_status",QAKey.HASANSWERED);
			
			floorQuestionMgrZJ.modQuestionById(question_id, para);//修改问题状态
			
			return (answer_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addAnswer",log);
		}
		
	}
	
	/**
	 * 提供工作流拦截
	 */
	public String addAnswerForJbpm(long question_id)
		throws Exception
	{
		try 
		{
			DBRow question = getDetailQuestionById(question_id);
			String account = question.getString("questioner");

			DBRow admin = adminMgr.getDetailAdminByAccount(account);
			long adid = 0;
			if(admin!=null)
			{
				adid = admin.get("adid",0l);
			}
			return (question_id+"-"+adid);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"addAnswerForJbpm",log);
		}
	}
	
	/**
	 * 修改答案
	 * @param request
	 * @throws Exception
	 */
	public long modAnswer(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long answer_id = StringUtil.getLong(request,"answer_id");
			String answer_content = StringUtil.getString(request,"answer_content");
			String answer_time = DateUtil.NowStr();
			
			DBRow old = floorAnswerMgrZJ.getDetailAnswerById(answer_id);
			String oldcontent = old.getString("answer_content");

			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String answerer = adminLoggerBean.getAccount();
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(answer_content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			
			String oldimgs[] = StringUtil.regMatchers(oldcontent, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			HashMap newimg=new HashMap();
			
			//移动文章新图片
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1);
				if(img.indexOf("_QA")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/customerservice_qa/upload_img/"+img;
					newimg.put(img,img);
					FileUtil.moveImg(ulr,url);
				}
			}
			
			//修改后原本有，现在没有引用的图片删除
			for(int j=0;oldimgs!=null&&j<oldimgs.length;j++)
			{
				String oldimg=oldimgs[j].substring(oldimgs[j].lastIndexOf("/")+1);
				if(!newimg.containsKey(oldimg))
				{
					FileUtil.delFile(Environment.getHome()+"administrator/customerservice_qa/upload_img/"+oldimg);
				}
			}
			
			
			DBRow para = new DBRow();
			
			para.add("answerer",answerer);
			para.add("answer_content",answer_content.replaceAll("upl_imags_tmp","administrator/customerservice_qa/upload_img"));
			para.add("answer_time",answer_time);
			
			floorAnswerMgrZJ.modAnswer(answer_id, para);
			
			return (answer_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modAnswer",log);
		}
	}
	
	
	/**
	 * 根据商品分类搜索问题
	 * @param catalog_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchQuestionByCatalog(long catalog_id,String pname,PageCtrl pc,int question_status)
	throws Exception
	{
		try 
		{
			return floorQuestionMgrZJ.searchQuestionByCatalogId(catalog_id,pname, pc,question_status);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"searchQuestionByCatalog",log);
		}
	}
	
	
	
	/**
	 * 根据商品ID搜索问题
	 * @param product_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchQuestionByProductId(long product_id,PageCtrl pc,int question_status)
	throws Exception
	{
		try 
		{
			return floorQuestionMgrZJ.searchQuestionByProductId(product_id, pc,question_status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"searchQuestionByProductId",log);
		}
	}
	
	
	
	
	
	
	
	/**
	 * 上传文件
	 * @param request
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public String uploadQuestionImage(HttpServletRequest request,HttpServletResponse response)
		throws Exception
	{
		String msg = "";
	
		try
		{	
			String callback = request.getParameter("CKEditorFuncNum");//CKEditor回调函数参数，必须有
			String permitFile = "gif,jpg";
			
			response.setCharacterEncoding("utf-8");
			//PrintWriter out = response.getWriter();
			
			TUpload upload = new TUpload();
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String author=adminLoggerBean.getAccount();
			
			upload.setFileName(String.valueOf(new Date().getTime())+"_"+author+"_QA");
			upload.setRootFolder("/upl_imags_tmp/");
			upload.setPermitFile(permitFile);
			
			String url="";
			int flag = upload.upload(request);
			
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
			}
			else if (flag==1)
			{
				msg = "上传出错";
			}
			else
			{
				 
				String path = request.getContextPath();  
				url="../../upl_imags_tmp/"+upload.getFileName();
				msg ="";
			}
			String result="<script type=\"text/javascript\">"+"window.parent.CKEDITOR.tools.callFunction("+callback + ",'"+url+ "','"+msg+"'" + ")"+"</script>";
			return(result);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"uploadTopicImage",log);
		}
	}
	
	/**
	 * 根据索引搜索问题
	 */
	public DBRow[] searchQuestionByIndexKey(String key,long catalog_id,String pname,PageCtrl pc,int question_status)//
		throws Exception
	{
		if(!key.equals("")&&pname.equals(""))
		{
			return QuestionIndexMgr.getInstance().getSearchResults(key, pc,question_status,catalog_id);
		}
		else if(!pname.equals("")&&key.equals(""))
		{
			return QuestionIndexMgr.getInstance().getSearchResults(catalog_id, pname, pc, question_status);
		}
		else
		{
			return (QuestionIndexMgr.getInstance().getSearchResults(key, catalog_id, pname, pc,question_status));
		}
	}
	
	/**
	 * 删除问题
	 */
	public void delQuestionById(HttpServletRequest request)
		throws Exception
	{
		long question_id = StringUtil.getLong(request,"question_id");
		floorQuestionMgrZJ.delQuestionById(question_id);
		floorAnswerMgrZJ.delAnswerByQuestionId(question_id);
		QuestionIndexMgr.getInstance().deleteIndex(question_id);
	}
	/**
	 * 过滤html标签
	 * @param inputString
	 * @return
	 */
	public String Html2Text(String inputString) 
	{
        String htmlStr = inputString; //含html标签的字符串
        String textStr ="";
	      java.util.regex.Pattern p_script;
	      java.util.regex.Matcher m_script;
	      java.util.regex.Pattern p_style;
	      java.util.regex.Matcher m_style;
	      java.util.regex.Pattern p_html;
	      java.util.regex.Matcher m_html;
  
	      try 
	      {
	    	  String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; //定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script> }
	    	  String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; //定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style> }
	          String regEx_html = "<[^>]+>"; //定义HTML标签的正则表达式
	     
	          p_script = Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
	          m_script = p_script.matcher(htmlStr);
	          htmlStr = m_script.replaceAll(""); //过滤script标签
	
	          p_style = Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
	          m_style = p_style.matcher(htmlStr);
	          htmlStr = m_style.replaceAll(""); //过滤style标签
	     
	          p_html = Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
	          m_html = p_html.matcher(htmlStr);
	          htmlStr = m_html.replaceAll(""); //过滤html标签
	     
	          StringBuffer txt = new StringBuffer(htmlStr);
	          repaceEntities(txt,"&amp;","&");
	          repaceEntities(txt,"&lt;","<");       
	          repaceEntities(txt,"&gt;",">");
	          repaceEntities(txt,"&quot;","\"");
	          repaceEntities(txt,"&nbsp;","");      
	          String content=txt.toString();
	          
	          Pattern pattern = Pattern.compile("[\\s\\p{Zs}]");//过滤空格换行
	          Matcher re = pattern.matcher(content);
	          content=re.replaceAll("");
	          
	          textStr = htmlStr;
	     
	      }
	      catch(Exception e) 
	      {
	               System.err.println("Html2Text: " + e.getMessage());
	      }
	  
	      return textStr;//返回文本字符串
       }
	
	/**
	 * 清除文本内特定形式字符
	 * @param txt
	 * @param entity
	 * @param replace
	 */
	private void repaceEntities ( StringBuffer txt,String entity,String replace)
    {
        int pos=-1;
        while(-1!=(pos=txt.indexOf(entity)))
        {
            txt.replace(pos,pos+entity.length(),replace);
        }
    }

	public void setFloorQuestionMgrZJ(FloorQuestionMgrZJ floorQuestionMgrZJ) {
		this.floorQuestionMgrZJ = floorQuestionMgrZJ;
	}


	public void setFloorAnswerMgrZJ(FloorAnswerMgrZJ floorAnswerMgrZJ) {
		this.floorAnswerMgrZJ = floorAnswerMgrZJ;
	}


	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}


	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

}
