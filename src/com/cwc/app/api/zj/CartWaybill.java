package com.cwc.app.api.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.Cart;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.iface.zj.CartWaybillIFace;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class CartWaybill implements CartWaybillIFace 
{
	private DBRow[] detailProduct = null;
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorOrderMgr floorOrderMgr;
	private FloorProductMgr floorProductMgr;
	
	/**
	 * 往session里增加运单明细
	 */
	public void putToWaybillCart(HttpServletRequest request)
		throws Exception 
	{
		int product_type;
		
		long order_item_id = StringUtil.getLong(request,"order_item_id");//order_item的主键
		float quantity = StringUtil.getFloat(request,"quantity");
		
		removeProduct(StringUtil.getSession(request),order_item_id);//放入前先从session中清除这个order_item
		
		DBRow order_item = floorOrderMgr.getDetailOrderItem(order_item_id);
		
		
		product_type = order_item.get("product_type",0);
		
		this.put2Cart(StringUtil.getSession(request),order_item_id,order_item.get("pid",0l), quantity,product_type);
	}
	
	/**
	 * 向session内保存运单明细
	 * @param session
	 * @param order_item_id
	 * @param pid
	 * @param quantity
	 * @param product_type
	 * @throws Exception
	 */
	public void put2Cart(HttpSession session,long order_item_id,long pid,float quantity,int product_type) 
		throws  Exception
	{
		ArrayList<DBRow> al = null;
		
		if ( pid != 0 &&quantity>0)//if ( pid != 0)
		{
//			if (quantity<=0)
//			{
//				quantity = 1;
//			}
			
			//购物车每条记录属性
			DBRow row = new DBRow();
			row.add("order_item_id",order_item_id);
			row.add("cart_pid",pid);//商品ID
			row.add("cart_quantity",quantity);
			row.add("cart_product_type",product_type);//商品类型
			
			if ( session.getAttribute(Config.wayBillSession) == null )
			{
				
				
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				al = (ArrayList)session.getAttribute(Config.wayBillSession);
				al.add(row);
			}
			session.setAttribute(Config.wayBillSession,al);			
		}
	}
	
	/**
	 * 从运单Session中移除（接口版本）
	 * @param request
	 * @throws Exception
	 */
	public void removeProduct(HttpServletRequest request)
		throws Exception
	{
		long order_item_id = StringUtil.getLong(request,"order_item_id");//order_item的主键
		
		this.removeProduct(StringUtil.getSession(request),order_item_id);
	}
	
	public void removeProduct(HttpSession session,long order_item_id)
		throws Exception
	{
		if ( session.getAttribute(Config.wayBillSession) != null )
		{
			DBRow rowOld;
			ArrayList al = (ArrayList)session.getAttribute(Config.wayBillSession);
			
			for ( int i=0;i<al.size(); i++ )
			{
				rowOld = (DBRow)al.get(i);
				if ( StringUtil.getLong(rowOld.getString("order_item_id")) == order_item_id)
				{
					al.remove(i);
				}
			}
			
			session.setAttribute(Config.wayBillSession,al);
		}
	}
	
	public void flush(HttpSession session)
		throws Exception
	{
		try
		{
				float sum_quantity = 0;
				double sum_price = 0;
				DBRow detailP;
	
				DBRow products[] = this.getSimpleProducts(session);
				//必须深度克隆，否则使用的只是引用，会把大量数据加到session中
				//数组不能直接克隆，需要一个个对象克隆，否则也是克隆地址引用
				DBRow newProducts[] = null;
				ArrayList newProductsAL = new ArrayList();
				for (int i=0; i<products.length; i++)
				{
					DBRow newProduct = new DBRow();
					newProduct.append(products[i]);
					newProductsAL.add(newProduct);
				}
				newProducts = (DBRow[])newProductsAL.toArray(new DBRow[0]);
				
				//把更详细的商品信息附加到购物车的商品记录中
				for (int i=0; i<newProducts.length; i++)
				{
					//非定制商品
					if (newProducts[i].get("cart_product_type",0)!=ProductTypeKey.UNION_CUSTOM)
					{
						detailP = floorProductMgr.getDetailProductByPcid(StringUtil.getLong(newProducts[i].getString("cart_pid")));
						newProducts[i].append(detailP);
					}
					else
					{
						//定制商品需要从另外的定制表获得商品详细信息
						detailP = floorProductMgr.getDetailProductCustomByPcPcid(StringUtil.getLong(newProducts[i].getString("cart_pid")));
	
						//因为新纪录的字段比原来少，所以需要把旧字段删掉
						String cart_pid = newProducts[i].getString("cart_pid");
						String cart_quantity = newProducts[i].getString("cart_quantity");
						String cart_product_type = newProducts[i].getString("cart_product_type");
						String order_item_id = newProducts[i].getString("order_item_id");
						newProducts[i].clear();
						newProducts[i].add("order_item_id", StringUtil.getLong(order_item_id));
						newProducts[i].add("cart_pid", StringUtil.getLong(cart_pid));
						newProducts[i].add("cart_quantity", StringUtil.getFloat(cart_quantity));
						newProducts[i].add("cart_product_type", StringUtil.getInt(cart_product_type));
						newProducts[i].append(detailP);
					}
	
					//计算一些数据
					sum_quantity += StringUtil.getFloat(newProducts[i].getString("cart_quantity"));
					
					//计算商品重量	
				}
				
				//设置购物车和购物车商品详细信息
				this.detailProduct =  newProducts;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"flush",log);
		}
	}
	
	/**
	 * 从session获得购物车商品
	 * 从session出来的数据，切记要克隆，否则对引用的修改会直接修改session
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.wayBillSession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.wayBillSession);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow = (DBRow)al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}
	
	/**
	 * 清空购物车
	 */
	public void clearCart(HttpSession session)
		throws Exception
	{
		if ( session.getAttribute(Config.wayBillSession) != null )
		{
			session.removeAttribute(Config.wayBillSession);
		}
	}
	
	/**
	 * 获得运单购物车内商品重量
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public float getCartWeight(HttpSession session)
		throws Exception
	{
		this.flush(session);
		DBRow[] waybill = this.getDetailProduct();
		
		DBRow cartProduct;
		float weight=0;
		for (int i = 0; i < waybill.length; i++) 
		{
			cartProduct = floorProductMgr.getDetailProductByPcid(waybill[i].get("cart_pid",0l));
			
			weight +=  cartProduct.get("weight",0f)*waybill[i].get("cart_quantity",0f);
		}
		
		return (weight);
	}
	public float getCartWeight(DBRow[] rows) throws Exception{
		float weight=0f;
		DBRow cartProduct;
		for (int i = 0; i < rows.length; i++) {
			cartProduct = floorProductMgr.getDetailProductByPcid(rows[i].get("cart_pid",0l));
			weight +=  cartProduct.get("weight",0f) * rows[i].get("cart_quantity",0f);
		}
	
		return weight;
	}
	
	
	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public DBRow[] getDetailProduct() {
		return detailProduct;
	}

}
