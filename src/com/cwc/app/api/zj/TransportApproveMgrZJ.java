package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zj.FloorTranposrtApproveMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.iface.zj.ProductStoreEditLogMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zj.TransportApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportWarehouseMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.key.ApproveTransportKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.RepairLogTypeKey;
import com.cwc.app.key.RepairOrderKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class TransportApproveMgrZJ implements TransportApproveMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorTranposrtApproveMgrZJ floorTranposrtApproveMgrZJ;
	private FloorTransportMgrLL floorTransportMgrLL;
	private TransportWarehouseMgrIFaceZJ transportWarehouseMgrZJ;
	private TransportMgrZJ transportMgrZJ;
	private ProductMgr productMgr;
	private FloorProductMgr floorProductMgr;
	private ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ;
	private PurchaseIFace purchaseMgr;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ;

	/**
	 * 转运单申请审核
	 * @param request
	 * @throws Exception
	 */
	public void addTransportApprove(HttpServletRequest request)
			throws Exception 
	{
		long transport_id = StringUtil.getLong(request,"transport_id");
		
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport.get("transport_status",0)==TransportOrderKey.INTRANSIT)//判断是否是可申请完成状态
		{
			addTransportApproveSub(transport_id, adminLoggerBean);
		}
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "到货申请完成", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
	}

	/**
	 * 转运单审核提交
	 * @param transport_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addTransportApproveSub(long transport_id,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		try 
		{
			DBRow[] differences = transportWarehouseMgrZJ.transportReceiveCompareCount(transport_id);
			DBRow[] differencesSN = transportWarehouseMgrZJ.compareTransportOutWareByTransportId(transport_id);
			
			if (differences.length>0||differencesSN.length>0) 
			{
				String commit_account = adminLoggerBean.getEmploye_name();
				String commit_date = DateUtil.NowStr();
				int different_count = differences.length;
				int different_sn_count = differencesSN.length;
				DBRow dbrow = new DBRow();
				dbrow.add("transport_id", transport_id);
				dbrow.add("commit_account", commit_account);
				dbrow.add("commit_date", commit_date);
				dbrow.add("different_count", different_count);
				dbrow.add("different_sn_count",different_sn_count);
				long ta_id = floorTranposrtApproveMgrZJ.addTransportApprove(dbrow);
				
				for (int i = 0; i < differences.length; i++)
				{
					long pc_id = differences[i].get("pc_id", 0l);
					DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
					
					String product_name = product.getString("p_name");
					String product_code = product.getString("p_code");
					
					float transport_count = differences[i].get("send_count", 0f);
					float transport_reap_count = differences[i].get("receive_count", 0f);

					DBRow difference = new DBRow();
					difference.add("product_name", product_name);
					difference.add("product_code", product_code);
					difference.add("product_id", pc_id);
					difference.add("transport_count", transport_count);
					difference.add("transport_reap_count", transport_reap_count);
					difference.add("ta_id", ta_id);

					floorTranposrtApproveMgrZJ.addTransportApproveDetail(difference);
				}
				
				for (int i = 0; i < differencesSN.length; i++)
				{
					long pc_id = differencesSN[i].get("pc_id", 0l);
					DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
					
					String product_name = product.getString("p_name");
					String product_code = product.getString("p_code");
					
					float send_count = differencesSN[i].get("send_count", 0f);
					float receive_count = differencesSN[i].get("receive_count", 0f);
					String serial_number = differencesSN[i].getString("serial_number");

					DBRow differenceSN = new DBRow();
					differenceSN.add("product_name", product_name);
					differenceSN.add("product_code", product_code);
					differenceSN.add("tads_pc_id", pc_id);
					differenceSN.add("transport_out_count", send_count);
					differenceSN.add("transport_reap_count",receive_count);
					differenceSN.add("ta_id", ta_id);
					differenceSN.add("serial_number",serial_number);

					floorTranposrtApproveMgrZJ.addTransportApproveDetailSN(differenceSN);
				}
				
				//提供工作流审核
				((TransportApproveMgrIFaceZJ)AopContext.currentProxy()).approveTransportOrderForJbpm(ta_id);
				
				DBRow para = new DBRow();
				para.add("transport_status", TransportOrderKey.APPROVEING);
				floorTransportMgrZJ.modTransport(transport_id, para);//修改转运单为待审核状态
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	
	/**
	 * 审核转运单
	 * @param ta_id
	 * @param tad_ids
	 * @param notes
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void approveTransportDifferent(long ta_id, long[] tad_ids,String[] notes,long[] tads_ids,String[] noteSNs,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		for (int i = 0; i < tad_ids.length; i++) 
		{
			DBRow modpara = new DBRow();
			modpara.add("note",notes[i]);
			modpara.add("approve_account",adminLoggerBean.getEmploye_name());
			modpara.add("approve_status",ApproveTransportKey.APPROVE);
			
			floorTranposrtApproveMgrZJ.modTransportApproveDetail(tad_ids[i], modpara);
		}
		
		for (int i = 0; i < tads_ids.length; i++) 
		{
			DBRow modpara = new DBRow();
			modpara.add("note",noteSNs[i]);
			modpara.add("approve_account",adminLoggerBean.getEmploye_name());
			modpara.add("approve_status",ApproveTransportKey.APPROVE);
			
			floorTranposrtApproveMgrZJ.modTransportApproveDetailSN(tads_ids[i], modpara);
		}
		
		DBRow[] noApprove = floorTranposrtApproveMgrZJ.getApproveTransportDetailWihtStatus(ta_id,ApproveTransportKey.WAITAPPROVE);//查找未审核的不同
		DBRow[] hasApprove = floorTranposrtApproveMgrZJ.getApproveTransportDetailWihtStatus(ta_id,ApproveTransportKey.APPROVE);//查找已审核的不同
		DBRow[] noApproveSN = floorTranposrtApproveMgrZJ.getApproveTransportDetailSNWihtStatus(ta_id, ApproveTransportKey.WAITAPPROVE);
		DBRow[] hasApproveSN = floorTranposrtApproveMgrZJ.getApproveTransportDetailSNWihtStatus(ta_id, ApproveTransportKey.APPROVE);
		
		DBRow modApproveTransport = new DBRow();
		long transport_id = floorTranposrtApproveMgrZJ.getDetailApproveTransportById(ta_id).get("transport_id",0l);
		
		if(noApprove.length==0&&noApproveSN.length==0)//差异都已审核过，变更交货单审核状态
		{
			modApproveTransport.add("approve_status",ApproveTransportKey.APPROVE);
			modApproveTransport.add("approve_account",adminLoggerBean.getEmploye_name());
			modApproveTransport.add("approve_date",DateUtil.NowStr());
			
			//转运单更改状态
			DBRow updatepara = new DBRow();
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);//获得转运单
			TDate tDate = new TDate();
			TDate endDate = new TDate(transport.getString("transport_date")+" 00:00:00");
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			updatepara.add("all_over", diffDay);//获取结束天数
			updatepara.add("transport_status",TransportOrderKey.FINISH);		
			
			floorTransportMgrZJ.modTransport(transport_id, updatepara);
			
			//默认为转运单入库
			int product_store_operation = ProductStoreOperationKey.IN_STORE_TRANSPORT_AUDIT;
			int bill_type = ProductStoreBillKey.TRANSPORT_ORDER;
			
			long purchase_id = transport.get("purchase_id",0l);
			if(purchase_id!=0)//如果是采购交货转运，库存操作日志记录为交货单入库
			{
				product_store_operation = ProductStoreOperationKey.IN_STORE_DELIVERY_AUDIT;
				bill_type = ProductStoreBillKey.DELIVERY_ORDER;
			}
			
			//stockIn(transport_id, adminLoggerBean,bill_type,product_store_operation);
			
			//更新运费到入库批次上
			transportMgrZJ.updateTransportShippingFeeToProductStoreLogDetail(transport_id);
		}
		modApproveTransport.add("difference_approve_count",hasApprove.length);
		modApproveTransport.add("difference_sn_approve_count",hasApproveSN.length);
		floorTranposrtApproveMgrZJ.modTransportApprove(ta_id, modApproveTransport);
		
		
		DBRow transportRow = transportMgrZJ.getDetailTransportById(transport_id);
		
		DBRow[] transportDetailsWarehouse = transportWarehouseMgrZJ.getTransportWareHouseByTransportId(transport_id);
		long purchase_id = transportRow.get("purchase_id",0l);
		
		if(purchase_id!=0)//如果是采购交货转运，需要对采购单进行入库
		{
			purchaseMgr.incomingSub(transportDetailsWarehouse,transportRow.get("purchase_id",0l), "", adminLoggerBean);//采购单入库
		}
		
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "转运单审核:"+(noApprove.length == 0?"完全审核":"部分审核"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "转运单审核:"+transport_id+","+(noApprove.length == 0?"完全审核":"部分审核"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods);
		scheduleMgrZr.addScheduleReplayExternal(transport_id , ModuleKey.TRANSPORT_ORDER,4,"转运单:"+transport_id+","+(noApprove.length == 0?"完全审核":"部分审核"),transportRow.get("transport_status",0)==RepairOrderKey.FINISH?true:false, adminLoggerBean, "transport_intransit_period","true");
	}

	/**
	 * 拦截转运单需要审核
	 */
	public long approveTransportOrderForJbpm(long ta_id) 
		throws Exception 
	{
		return (ta_id);
	}

	/**
	 * 过滤转运单审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype) 
		throws Exception 
	{
		try
		{
			return (floorTranposrtApproveMgrZJ.fillterTransportApprove(send_psid, receive_psid, pc, approve_status, sorttype));
		} 
		catch (Exception e) 
		{	
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 根据转运单审核ID获得转运审核明细
	 * @param ta_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportApproverDetailsByTaid(long ta_id, PageCtrl pc)
		throws Exception 
	{
		try
		{
			return floorTranposrtApproveMgrZJ.getApproveDetailByTaid(ta_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}

	public void stockIn(long transport_id,AdminLoginBean adminLoggerBean,int bill_type,int product_storep_oeration) throws Exception {
		try 
		{
			DBRow[] transportDetailsWarehouse = transportWarehouseMgrZJ.getTransportWareHouseByTransportId(transport_id);
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			long adid = adminLoggerBean.getAdid();
			
			for (int i = 0; i < transportDetailsWarehouse.length; i++) 
			{
				
				float count = Float.valueOf(transportDetailsWarehouse[i].getString("tw_count"));//获得交货数量
				ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
				inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
				inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				inProductStoreLogBean.setOid(transport_id);
				inProductStoreLogBean.setBill_type(bill_type);
				inProductStoreLogBean.setOperation(product_storep_oeration);//转运入库
				
				
				DBRow product = floorProductMgr.getDetailProductByPcid(transportDetailsWarehouse[i].get("tw_product_id",0l));//为了获得商品ID
				
				if (product!=null) //以防商品被删除
				{
					DBRow transportDetail = floorTransportMgrZJ.getTransportDetailByTPId(transport_id,product.get("pc_id", 0l));
					
					//转运明细存在，库存操作为交货单或者交货单审核记录采购价格
					if(transportDetail!=null&&(product_storep_oeration==ProductStoreOperationKey.IN_STORE_DELIVERY||product_storep_oeration==ProductStoreOperationKey.IN_STORE_DELIVERY_AUDIT))
					{
//						inProductStoreLogBean.setPurchase_unit_price(transportDetail.get("transport_send_price",0d));
					}
					if(count>0)
					{
						long psl_id = floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,transport.get("receive_psid",0l),product.get("pc_id", 0l),count,0);//库存添加
						
						if(transportDetailsWarehouse[i].getFieldNames().size()==4)
						{
							String barcode = transportDetailsWarehouse[i].getString("product_barcod");
							String position = transportDetailsWarehouse[i].getString("storage");
//							productMgr.addStorageLocation(transport.get("receive_psid",0l),product.get("pc_id",0l),barcode,count,position,transportDetailsWarehouse[i].getString("machine_id"),null,adminLoggerBean);
							
//							productStoreLocationMgrZJ.putProductToLocation(transport.get("receive_psid",0l),product.get("pc_id",0l),count,transport_id,bill_type,adminLoggerBean.getAdid(),position,"",0,"wu",inProductStoreLogBean);
						}
					}
				}
			}
			//填写入库人和入库时间
			DBRow transportRow = new DBRow();
			transportRow.add("warehousinger_id", adid);
			transportRow.add("warehousing_date", DateUtil.NowStr());
			floorTransportMgrZJ.modTransport(transport_id, transportRow);
			
//			transportWarehouseMgrZJ.delTransportWareHouseByTransportId(transport_id);//清除虚拟交货数据
			
			long purchase_id = transport.get("purchase_id",0l);
			
			if(purchase_id!=0)//如果是采购交货转运，需要对采购单进行入库
			{
				purchaseMgr.incomingSub(transportDetailsWarehouse,transport.get("purchase_id",0l), "", adminLoggerBean);//采购单入库
			}
			
			
		   for (int i = 0; i < transportDetailsWarehouse.length; i++)
		   {
			   DBRow product = floorProductMgr.getDetailProductByPcid(transportDetailsWarehouse[i].get("tw_product_id",0l));//为了获得商品ID
			   productStoreEditLogMgrZJ.addProductStoreLogForPcidAndPsid(product.get("pc_id",0l), transport.get("receive_psid",0l), adminLoggerBean, 1);
		   }
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"stockIn",log);
		}
	}
	
	public DBRow[] getTransportApproveDetailsSNByTaid(long ta_id, PageCtrl pc)
		throws Exception 
	{
		try 
		{
			return floorTranposrtApproveMgrZJ.getTransportApproveDetailSNByTaId(ta_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportApproveDetailsSNByTaid",log);
		}
	}
	
	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public void setFloorTranposrtApproveMgrZJ(
			FloorTranposrtApproveMgrZJ floorTranposrtApproveMgrZJ) {
		this.floorTranposrtApproveMgrZJ = floorTranposrtApproveMgrZJ;
	}
	public void setTransportWarehouseMgrZJ(
			TransportWarehouseMgrIFaceZJ transportWarehouseMgrZJ) {
		this.transportWarehouseMgrZJ = transportWarehouseMgrZJ;
	}
	public void setTransportMgrZJ(TransportMgrZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
	public void setProductStoreEditLogMgrZJ(
			ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ) {
		this.productStoreEditLogMgrZJ = productStoreEditLogMgrZJ;
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
}
