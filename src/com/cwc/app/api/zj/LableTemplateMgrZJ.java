package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;



import com.cwc.app.floor.api.zj.FloorLableTemplateMgrZJ;
import com.cwc.app.iface.zj.LableTemplateMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class LableTemplateMgrZJ implements LableTemplateMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorLableTemplateMgrZJ floorLableTemplateMgrZJ;
	
	/**
	 * 添加标签模板
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addLableTemplate(HttpServletRequest request) 
		throws Exception 
	{
		try
		{
			String template_name = StringUtil.getString(request, "template_name");
			int lable_type = StringUtil.getInt(request,"lable_type");
			String printer = StringUtil.getString(request,"printer");
			String paper = StringUtil.getString(request,"paper");
			float print_range_width = StringUtil.getFloat(request,"print_range_width");
			float print_range_height = StringUtil.getFloat(request,"print_range_height");
			String template_path = StringUtil.getString(request,"template_path");
			String img_path = StringUtil.getString(request,"img_path");
			
			DBRow dbrow = new DBRow();
			dbrow.add("template_name",template_name);
			dbrow.add("lable_type",lable_type);
			dbrow.add("printer",printer);
			dbrow.add("paper",paper);
			dbrow.add("print_range_width",print_range_width);
			dbrow.add("print_range_height",print_range_height);
			dbrow.add("template_path",template_path);
			dbrow.add("img_path",img_path);
			
			return floorLableTemplateMgrZJ.addLableTemplate(dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addLableTemplate",log);
		}
	}
	
	
	/**
	 * 删除标签模板
	 * @param request
	 * @throws Exception
	 */
	public void delLableTemplate(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long lable_template_id = StringUtil.getLong(request,"lable_template_id");
			
			floorLableTemplateMgrZJ.delLableTemplate(lable_template_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delLableTemplate",log);
		}
	}


	/**
	 * 获得所有标签模板
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllLableTemplate(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorLableTemplateMgrZJ.getAllLableTemplate(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllLableTemplate",log);
		}
	}
	
	/**
	 * 获得全部模板
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTemplateLabel(PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorLableTemplateMgrZJ.getAllTemplateLabel(null);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	
	/**
	 * 修改标签模板
	 * @param request
	 * @throws Exception
	 */
	public void modLableTemplate(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long lable_template_id = StringUtil.getLong(request,"lable_template_id");
			
			String template_name = StringUtil.getString(request, "template_name");
			int lable_type = StringUtil.getInt(request,"lable_type");
			String printer = StringUtil.getString(request,"printer");
			String paper = StringUtil.getString(request,"paper");
			float print_range_width = StringUtil.getFloat(request,"print_range_width");
			float print_range_height = StringUtil.getFloat(request,"print_range_height");
			String template_path = StringUtil.getString(request,"template_path");
			String img_path = StringUtil.getString(request,"img_path");
			
			DBRow para = new DBRow();
			para.add("template_name",template_name);
			para.add("lable_type",lable_type);
			para.add("printer",printer);
			para.add("paper",paper);
			para.add("print_range_width",print_range_width);
			para.add("print_range_height",print_range_height);
			para.add("template_path",template_path);
			para.add("img_path",img_path);
			
			floorLableTemplateMgrZJ.modLableTemplate(lable_template_id, para);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modLableTemplate",log);
		}
	}



	/**
	 * 获得标签模板详细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLableTemplateById(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long lable_template_id = StringUtil.getLong(request,"lable_template_id");
			return (floorLableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailLableTemplateById",log);
		}
	}

	/**
	 * 获得标签模板详细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLableTemplateById(long lable_template_id)
		throws Exception
	{
		try 
		{
			return (floorLableTemplateMgrZJ.getDetailLableTemplateById(lable_template_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailLableTemplateById",log);
		}
	}
	
	/**
	 * 获得所有商品标签
	 */
	public DBRow[] getAllProductTemplate(PageCtrl pc) 
		throws Exception 
	{
		try
		{
			return floorLableTemplateMgrZJ.getAllProductTemplate(null);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getAllProductTemplate",log);
		}
	}
	
	public void setFloorLableTemplateMgrZJ(FloorLableTemplateMgrZJ floorLableTemplateMgrZJ) {
		this.floorLableTemplateMgrZJ = floorLableTemplateMgrZJ;
	}
}
