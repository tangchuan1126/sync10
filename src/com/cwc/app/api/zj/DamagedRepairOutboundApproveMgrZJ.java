package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorDamagedRepairMgrZJ;
import com.cwc.app.floor.api.zj.FloorDamagedRepairOutboundMgrZJ;
import com.cwc.app.floor.api.zj.FloorDamagedRepairSendApproveMgrZJ;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.DamagedRepairOutboundApproveMgrIFaceZJ;
import com.cwc.app.key.ApproveDamagedRepairOutboundKey;
import com.cwc.app.key.DamagedRepairKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class DamagedRepairOutboundApproveMgrZJ implements DamagedRepairOutboundApproveMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ;
	private FloorDamagedRepairSendApproveMgrZJ floorRepairSendApproveMgrZJ;
	private FloorDamagedRepairMgrZJ floorDamagedRepairMgrZJ;
	private FloorProductMgr floorProductMgr;
	private ProductMgrIFace productMgr;
	
	public void addDamagedRepairOutboundApprove(HttpServletRequest request)
		throws Exception
	{
		long repair_id = StringUtil.getLong(request,"repair_id");
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		
		addDamagedRepairOutboundApproveSub(repair_id, adminLoggerBean);
	}
	
	
	/**
	 * 返修审核提交
	 * @param repair_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addDamagedRepairOutboundApproveSub(long repair_id,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		DBRow[] differents = floorDamagedRepairOutboundMgrZJ.compareDamagedRepairPacking(repair_id);
		
		DBRow para = new DBRow();
		
		if(differents.length>0)//无差异就不创建审核了
		{
			String commit_account = adminLoggerBean.getAccount();
			String commit_date = DateUtil.NowStr();
			int different_count = differents.length;
			
			DBRow dbrow = new DBRow();
			dbrow.add("repair_id",repair_id);
			dbrow.add("commit_account",commit_account);
			dbrow.add("commit_date",commit_date);
			dbrow.add("different_count",different_count);
			
			long rsa_id = floorRepairSendApproveMgrZJ.addDamagedRepairSendApprove(dbrow);
			
			for (int i = 0; i < differents.length; i++) 
			{
				String product_name = differents[i].getString("p_name");
				String product_code = differents[i].getString("p_code");
				long product_id = differents[i].get("pc_id",0l);
				float repair_count = differents[i].get("repair_count",0f);
				float repair_send_count = differents[i].get("real_send_count",0f);
				
				DBRow difference = new DBRow();
				difference.add("product_name",product_name);
				difference.add("product_code",product_code);
				difference.add("product_id",product_id);
				difference.add("repair_count",repair_count);
				difference.add("repair_send_count",repair_send_count);
				difference.add("rsa_id",rsa_id);
				
				floorRepairSendApproveMgrZJ.addDamagedRepairSendApproveDetail(difference);
				
				//提供给工作流拦截
				((DamagedRepairOutboundApproveMgrIFaceZJ)AopContext.currentProxy()).approveDamagedRepairOutboundForJbpm(rsa_id);
			}
			
			
			para.add("repair_status",DamagedRepairKey.ERRORINTRANSIT);//状态差异运输
			
		}
		else
		{
			para.add("repair_status",DamagedRepairKey.INTRANSIT);//正常运输
		}
		
		floorDamagedRepairMgrZJ.modDamagedRepairOrder(repair_id, para);
	}


	/**
	 *	返修明细审核 
	 * @param rsa_id
	 * @param rsad_ids
	 * @param notes
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void approveDamagedRepairOutboundDifferent(long rsa_id,long[] rsad_ids, String[] notes, AdminLoginBean adminLoggerBean)
	throws Exception 
	{
		for (int i = 0; i < rsad_ids.length; i++) 
		{
			DBRow modpara = new DBRow();
			modpara.add("note",notes[i]);
			modpara.add("approve_account",adminLoggerBean.getAccount());
			modpara.add("approve_status",ApproveDamagedRepairOutboundKey.APPROVE);
			
			floorRepairSendApproveMgrZJ.modDamagedRepairSendApproveDetail(rsad_ids[i],modpara);
		}
		
		DBRow[] sendDifferent = floorRepairSendApproveMgrZJ.getDamagedRepairDetailsByRsa_id(rsa_id, 0);
		
		long repair_id = floorRepairSendApproveMgrZJ.getDetailDamagedRepairSendApprove(rsa_id).get("repair_id",0l);
		
		DBRow damagedRepair = floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
		
		long ps_id = damagedRepair.get("send_psid",0l);
		
		for (int i = 0; i < sendDifferent.length; i++) 
		{
			DBRow storage = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,sendDifferent[i].get("product_id",0l));
			
			long pid = storage.get("pid",0l);
			float damaged_count = storage.get("damaged_count",0f);
			float damaged_package_count = storage.get("damaged_package_count",0f);
			
			damaged_count = damaged_count-sendDifferent[i].get("different_count",0f);//先用残损值减去返修量
			
			DBRow para = new DBRow();
			
			if(damaged_count>0)
			{
				para.add("damaged_count",damaged_count);
			}
			else
			{
				para.add("damaged_count",0f);
				para.add("damaged_package_count",(damaged_package_count+damaged_count));
			}
			//修改残损库存
			floorProductMgr.modProductStorage(pid,para);
			
			//记录残损件出库日志
			
			ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
			deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
			deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
			deProductDamagedCountLogBean.setOid(repair_id);
			deProductDamagedCountLogBean.setPs_id(ps_id);
			deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR_AUDIT);
			deProductDamagedCountLogBean.setQuantity(-sendDifferent[i].get("different_count",0f));
			deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
			deProductDamagedCountLogBean.setPc_id(sendDifferent[i].get("product_id",0l));
			productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
			//floorRepairSendApproveMgrZJ.returnDamagedCount(ps_id, sendDifferent[i].get("product_id",0l), sendDifferent[i].get("different_count",0f));
		}
		
		
		DBRow[] noApprove = floorRepairSendApproveMgrZJ.getDamagedRepairDetailsByRsa_id(rsa_id,ApproveDamagedRepairOutboundKey.WAITAPPROVE);//查找未审核的不同
		DBRow[] hasApprove = floorRepairSendApproveMgrZJ.getDamagedRepairDetailsByRsa_id(rsa_id,ApproveDamagedRepairOutboundKey.APPROVE);//查找已审核的不同
		
		
		DBRow modRepairSendApprove = new DBRow();
		
		if(noApprove.length == 0)//差异都已审核过，变更交货单审核状态
		{
			modRepairSendApprove.add("approve_status",ApproveDamagedRepairOutboundKey.APPROVE);
			modRepairSendApprove.add("approve_account",adminLoggerBean.getAccount());
			modRepairSendApprove.add("approve_date",DateUtil.NowStr());
			
			

		}
		modRepairSendApprove.add("difference_approve_count",hasApprove.length);
		floorRepairSendApproveMgrZJ.modDamagedRepairSendApprove(rsa_id, modRepairSendApprove);

	}

	/**
	 * 过滤返修发货审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterDamagedRepairOutboundApprove(long send_psid,long receive_psid, PageCtrl pc, int approve_status, String sorttype)
	throws Exception 
	{
		try
		{
			return floorRepairSendApproveMgrZJ.fillterDamagedRepairOutboundApprove(send_psid, receive_psid, pc, approve_status, sorttype);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"fillterDamagedRepairOutboundApprove",log);
		}
	}

	/**
	 * 根据返修单审核ID获得返修审核明细
	 * @param rsa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairOutboundApproverDetailsByTsaid(long rsa_id,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorRepairSendApproveMgrZJ.getDamagedRepairDetailsByRsa_id(rsa_id, 0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDamagedRepairOutboundApproverDetailsByTsaid",log);
		}
	}
	
	/**
	 *返修单发货需审核，提供工作流拦截
	 */
	public long approveDamagedRepairOutboundForJbpm(long rsa_id)
		throws Exception 
	{
		return (rsa_id);
	}

	public void setFloorDamagedRepairOutboundMgrZJ(
			FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ) {
		this.floorDamagedRepairOutboundMgrZJ = floorDamagedRepairOutboundMgrZJ;
	}

	public void setFloorRepairSendApproveMgrZJ(
			FloorDamagedRepairSendApproveMgrZJ floorRepairSendApproveMgrZJ) {
		this.floorRepairSendApproveMgrZJ = floorRepairSendApproveMgrZJ;
	}

	public void setFloorDamagedRepairMgrZJ(
			FloorDamagedRepairMgrZJ floorDamagedRepairMgrZJ) {
		this.floorDamagedRepairMgrZJ = floorDamagedRepairMgrZJ;
	}


	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}


}
