package com.cwc.app.api.zj;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.CatalogMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.product.CatalogNotExistProductException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductDateChangeException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.exception.product.UPCExistException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.FloorSetupLogMgr;
import com.cwc.app.floor.api.tjh.FloorOrderProcessMgrTJH;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zj.FloorBackUpStorageMgrZJ;
import com.cwc.app.floor.api.zj.FloorImportProductMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductMgrZJ;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.ProductCustomer;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.UploadEditFileKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class ProductMgrZJ implements ProductMgrIFaceZJ {

	private FloorProductMgrZJ floorProductMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductMgr floorProductMgr;
	private ProductMgr productMgr;
	private PurchaseMgr purchaseMgr;
	
	private CatalogMgrIFace catalogMgr;
	private HashMap checkUploadPName;//上传文件内商品名
	private HashMap checkUploadPCode;//上传文件内商品条码
	private HashMap<String,String> checkUploadPCidandPCode;//上传文件内商品ID与商品条码组合
	private HashMap<String,DBRow[]> unionMap;//套装关系Map
	
	private HashMap<String,DBRow[]> unionDBMap;//不存在商品表中的套装关系
	
	private ArrayList<DBRow> updateProductIndex;
	
	private ArrayList<DBRow> addProductIndex;
	
	private FloorImportProductMgrZJ floorImportProductMgrZJ;
	
	private FloorOrderProcessMgrTJH floorOrderProcessMgrTJH;
	
	private FloorOrderMgr floorOrderMgr;
	
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	
	private ProductCodeMgrZJ productCodeMgrZJ;
	
	private FloorBackUpStorageMgrZJ floorBackUpStorageMgrZJ;
	
	private BackUpStorageMgrZJ backUpStorageMgrZJ;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	private FloorProductLineMgrTJH floorProductLineMgr;
	
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	
	private FloorCatalogMgr floorCatalogMgr;
	
	private	FloorProductStoreMgr productStoreMgr;
	
	private FloorSetupLogMgr floorSetupLogMgr;
	
	public void setFloorSetupLogMgr(FloorSetupLogMgr floorSetupLogMgr) {
		this.floorSetupLogMgr = floorSetupLogMgr;
	}
	
	public void setFloorProprietaryMgrZyj( FloorProprietaryMgrZyj floorProprietaryMgrZyj) {
		this.floorProprietaryMgrZyj = floorProprietaryMgrZyj;
	}
	
	public void setFloorCatalogMgr( FloorCatalogMgr floorCatalogMgr ) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
	public void setFloorProductLineMgr(FloorProductLineMgrTJH floorProductLineMgr) {
		this.floorProductLineMgr = floorProductLineMgr;
	}
	
	public void setFloorProductMgrZJ(FloorProductMgrZJ floorProductMgrZJ) {
		this.floorProductMgrZJ = floorProductMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	public void setPurchaseMgr(PurchaseMgr purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

	public void setFloorImportProductMgrZJ( FloorImportProductMgrZJ floorImportProductMgrZJ) {
		this.floorImportProductMgrZJ = floorImportProductMgrZJ;
	}

	public void setFloorOrderProcessMgrTJH( FloorOrderProcessMgrTJH floorOrderProcessMgrTJH) {
		this.floorOrderProcessMgrTJH = floorOrderProcessMgrTJH;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}

	public void setProductCodeMgrZJ(ProductCodeMgrZJ productCodeMgrZJ) {
		this.productCodeMgrZJ = productCodeMgrZJ;
	}
	
	public void setBackUpStorageMgrZJ(BackUpStorageMgrZJ backUpStorageMgrZJ) {
		this.backUpStorageMgrZJ = backUpStorageMgrZJ;
	}

	public void setFloorBackUpStorageMgrZJ( FloorBackUpStorageMgrZJ floorBackUpStorageMgrZJ) {
		this.floorBackUpStorageMgrZJ = floorBackUpStorageMgrZJ;
	}
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	/**
	 * 商品导出前数据预览
	 * @param catalog_id
	 * @param union_flag
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] exportProductShow(long catalog_id,long pro_line_id,int union_flag,PageCtrl pc)
		throws Exception
	{
		return floorProductMgrZJ.getProductInCids(catalog_id,pro_line_id, union_flag,0,0, pc);
		//return (floorProductMgrZJ.getExportProductShow(catalog_id,pc));
	}
	

	/**
	 * 导出商品（此方法从写）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportProduct(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String catalog_id = StringUtil.getString(request,"pcid");
			String cmd = StringUtil.getString(request,"cmd");
			String key = StringUtil.getString(request,"key");
			int union_flag = StringUtil.getInt(request,"union_flag");
			String product_line_id = StringUtil.getString(request,"pro_line_id");
			int product_file_type = StringUtil.getInt(request, "product_file_types");
			int product_upload_status = StringUtil.getInt(request, "product_upload_status");
			String title_id = StringUtil.getString(request, "title_id");
			String adid_null_to_login = StringUtil.getString(request, "adid_null_to_login");
			int active = StringUtil.getInt(request, "active", -1);
			boolean adidNullToLogin = false;
			if("true".equals(adid_null_to_login))
			{
				adidNullToLogin = true;
			}
			
			DBRow[] exportProduct = new DBRow[0];
			if("search".equals(cmd))
			{
				exportProduct = proprietaryMgrZyj.findDetailProductLikeSearch(true, key, title_id, 0L,-1,0,0,active, null, request);
			}
			else
			{
				exportProduct = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(adidNullToLogin, catalog_id, product_line_id, union_flag, product_file_type, product_upload_status, title_id, 0L,0,0,active, null, request);
			}
			long[] products = new long[exportProduct.length];
			
			if (exportProduct.length>0) 
			{
				XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product/sku-title-customer.xlsx"));//根据模板创建工作文件
				XSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
				
				
				
				XSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); //创建一个锁定样式
				
				int row_index = 0;
				
				for (int i = 0; i < exportProduct.length; i++) 
				{

					sheet.setDefaultColumnWidth(26);
					DBRow product = exportProduct[i];
					
					products[i] = product.get("pc_id",0l);
					
					//查询product信息，lot_no  sku
					
					DBRow productInfo = proprietaryMgrZyj.getProductDetailByPcId(product.get("pc_id",0l));
					DBRow[] snRows = productMgr.getProductSnByPcId(product.get("pc_id",0l));
					String sn_len = "";
					for (int j = 0; j < snRows.length; j++) {
						sn_len += ","+snRows[j].get("sn_size", 0);
					}
					if(sn_len.length() > 0){sn_len = sn_len.substring(1);}
					productInfo.add("sn_len", sn_len);
					
					List<ProductCustomer> title_customers = proprietaryMgrZyj.getByProduct(product.get("pc_id", 0));
					//商品与title 、customer的关系
					if(title_customers.size() > 0)
					{
						for (int j = 0; j < title_customers.size(); j++) 
						{
							List<Customer> customers = title_customers.get(j).getCustomers();
							if(customers.size() > 0)
							{
								for (int k = 0; k < customers.size(); k++)
								{
									row_index ++;
									xlsRow(product, productInfo, sheet, title_customers.get(j).getTitle().getName(), customers.get(k).getName(),row_index);
								}
							}
							else
							{
								row_index ++;
								xlsRow(product, productInfo, sheet, title_customers.get(j).getTitle().getName(), "",row_index);
							}
						}
					}
					else
					{
						row_index ++;
						xlsRow(product, productInfo, sheet, "", "",row_index);
					}
				}
				
				String path = "upl_excel_tmp/sku_title_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xlsx";
				FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
				wb.write(fout);
				fout.close();
				return (path);
			}
			else
			{
				return "0";
			}
        } 
		catch (Exception e) 
		{
			throw new SystemException(e,"exportProduct",log);
	    }
	}
	
	
	private XSSFSheet xlsRow(DBRow product, DBRow productInfo, XSSFSheet sheet, String title, String customer, int row_index)
	{
		XSSFRow row = sheet.createRow(row_index);
		row.createCell(0).setCellValue(product.getString("p_name"));//Product Name
//		row.createCell(1).setCellValue(productInfo.getString("sku"));//sku
//		row.createCell(2).setCellValue(productInfo.getString("lot_no"));//Lot
		row.createCell(1).setCellValue(product.getString("p_code"));//keycode 主条码
		row.createCell(2).setCellValue(product.getString("title"));//catalog
		row.createCell(3).setCellValue(title);//title
		row.createCell(4).setCellValue(customer);//customer
		row.createCell(5).setCellValue(product.getString("upc"));//upc
		row.createCell(6).setCellValue(product.get("length",0f));//length
		row.createCell(7).setCellValue(product.get("width",0f));//width
		row.createCell(8).setCellValue(product.get("heigth",0f));//height
		row.createCell(9).setCellValue(product.getString("weight"));//weight
		row.createCell(10).setCellValue(product.get("unit_price", 0d));//price
		row.createCell(11).setCellValue(productInfo.get("freight_class", 0));//class
		row.createCell(12).setCellValue(productInfo.getString("nmfc_code"));//nmfc code
		row.createCell(13).setCellValue(productInfo.getString("sn_len"));//sn length
		row.createCell(14).setCellValue(new LengthUOMKey().getLengthUOMKey(productInfo.get("length_uom", 0)));//lengthUom
		row.createCell(15).setCellValue(new WeightUOMKey().getWeightUOMKey(productInfo.get("weight_uom", 0)));//weightUom
		row.createCell(16).setCellValue(new PriceUOMKey().getMoneyUOMKey(productInfo.get("price_uom", 0)));//priceUom
		return sheet;
	}
	
	
	/**
	 * 导出套装关系
	 * @throws Exception
	 */
	public String exportProductUnion(HttpServletRequest request)
		throws Exception
	{
		
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long product_line_id = StringUtil.getLong(request,"pro_line_id");
		
		
		POIFSFileSystem fs = null;//获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportProductUnion.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
		/**
		 * 获得导出关系
		 */
		HSSFFont font = wb.createFont();
		font.setColor(HSSFColor.WHITE.index);//设置字体白色
		HSSFCellStyle ss = wb.createCellStyle();
		ss.setFont(font);
		HSSFSheet sheet3 = wb.getSheetAt(0);
		HSSFRow row3 = sheet3.getRow(0);
		
		
		
		DBRow[] unions = floorProductMgrZJ.getExportProdutUnion(catalog_id, product_line_id);
		for(int q = 0;q<unions.length;q++)
		{
			row3 = sheet3.createRow((int)q+1);
			sheet3.setDefaultColumnWidth(26);
			DBRow union = unions[q];
			
			
			row3.createCell(0).setCellValue(union.getString("product"));
			if(q>0&&union.getString("product").equals(unions[q-1].getString("product")))//同一套装只第一条关系显示套装条码
			{
				row3.getCell(0).setCellStyle(ss);//设定字体白色
			}
			row3.createCell(1).setCellValue(union.getString("accessories"));
			HSSFCell c = row3.createCell(2);
			c.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			c.setCellValue(union.get("quantity",0f));
		}
		
		String path = "upl_excel_tmp/Out_product_union_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	
	/**
	 * 导出商品文件
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportProductFile(HttpServletRequest request)
		throws Exception
	{
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long product_line_id = StringUtil.getLong(request,"pro_line_id");
		int union_flag = StringUtil.getInt(request,"union_flag");
		
		
		POIFSFileSystem fs = null;//获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportProductFile.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
		HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		HSSFRow row = sheet.getRow(0);
		
		DBRow[] exportProduct = floorProductMgrZJ.getProductInCids(catalog_id, product_line_id, union_flag,0,0,null);
		
		for (int i = 0; i < exportProduct.length; i++) 
		{
			DBRow[] productFiles = floorProductMgrZJ.getProductFileByPcid(exportProduct[i].get("pc_id",0l), true, true);
			
			row = sheet.createRow((int) i + 1);
			
			row.createCell(0).setCellValue(exportProduct[i].get("pc_id",0l));
			row.createCell(1).setCellValue(exportProduct[i].getString("p_name"));
			
			for (int j = 0; j < productFiles.length; j++)
			{
				row.createCell(i+2).setCellValue(productFiles[j].getString("file_name"));
			}
		}
		
		String path = "upl_excel_tmp/Out_product_file_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();
		return (path);
	}
	
	
	/**
	 * 导出打印条码商品
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportProductBarcode(HttpServletRequest request) 
		throws Exception 
	{
		FileOutputStream fout = null;
		try 
		{
			long catalog_id = StringUtil.getLong(request,"catalog_id"); 
			
			DBRow[] exportProduct = floorProductMgrZJ.getExportProduct(catalog_id);
			
			if(exportProduct==null)
			{
				throw new CatalogNotExistProductException();
			}
			
			if (exportProduct.length>0) 
			{
				
				POIFSFileSystem fs = null;//获得模板
				fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportBarcodeModel.xls"));
				HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
				HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
				HSSFRow row = sheet.getRow(0);
				
				HSSFCellStyle style = wb.createCellStyle();
				style.setLocked(false); //创建一个非锁定样式
				style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				style.setWrapText(true); 
				
				for (int i = 0; i < exportProduct.length; i++) 
				{
					row = sheet.createRow((int) i + 1);

					sheet.setDefaultColumnWidth(26);
					DBRow product = exportProduct[i];
					
					//创建单元格，并设置值
					
					row.createCell(0).setCellValue(product.getString("p_name"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(product.getString("p_code"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(1);
					row.getCell(2).setCellStyle(style);
								
					row.createCell(3).setCellValue(1);
					row.getCell(3).setCellStyle(style);
				}
				
				String path = "upl_excel_tmp/Out_product_barcode_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
				fout = new FileOutputStream(Environment.getHome()+ path);
				wb.write(fout);
				fout.close();
				return (path);
			}
			else
			{
				return "0";
			}
		}
		catch(CatalogNotExistProductException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"exportProductBarcode",log);
		}
		
	}
	
	/**
	 * 根据采购单号生成需批量打印条码文件
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String exportProductBarcodeByPurchase(HttpServletRequest request)
		throws Exception
	{
		FileOutputStream fout = null;
		
		try 
		{
			String id = StringUtil.getString(request,"id");
			
			String regEx="[^0-9]";  
			Pattern p = Pattern.compile(regEx);  
			Matcher m = p.matcher(id);   
			long purchase_id = 0;
			if(!m.replaceAll("").equals(""))
			{
				purchase_id = Long.parseLong(m.replaceAll(""));
			}
			
			
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportBarcodeModel.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			
			HSSFCellStyle style = wb.createCellStyle();
			style.setLocked(false); //创建一个非锁定样式
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			style.setWrapText(true);
			
			DBRow[] purchaseDetails = purchaseMgr.getPurchaseDetailByPurchaseid(purchase_id, null, null);
			
			if(purchaseDetails!=null&&purchaseDetails.length>0)
			{
				for (int i = 0; i < purchaseDetails.length; i++)
				{
					DBRow product = productMgr.getDetailProductByPcid(purchaseDetails[i].get("product_id",0l));
					
					row = sheet.createRow((int) i + 1);

					sheet.setDefaultColumnWidth(26);
					
					//创建单元格，并设置值
					
					row.createCell(0).setCellValue(product.getString("p_name"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(product.getString("p_code"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(1);
					row.getCell(2).setCellStyle(style);
								
					row.createCell(3).setCellValue(1);
					row.getCell(3).setCellStyle(style);
				}
				
				String path = "upl_excel_tmp/Out_product_barcode_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
				fout = new FileOutputStream(Environment.getHome()+ path);
				wb.write(fout);
				fout.close();
				return (path);
			}
			else
			{
				return "0";
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"purchaseProductBarcode",log);
		}
		
	}
	/**
	 * 商品上传
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String[] importProduct(HttpServletRequest request)
		throws Exception,FileTypeException
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String[] msg = new String[2];
			
			upload.setFileName("In_product_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			upload.setFileSize(1024*1024*5);
			
			int flag = upload.upload(request);
			
			msg[1] = upload.getRequestRow().getString("upload_type");
				
			if (flag==2)
			{
				msg[0] = "只能上传:"+permitFile;
				////system.out.println(msg);
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				msg[0] = "上传出错";
			}
			else
			{		  
				msg[0] = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importProduct",log);
		}
	}
	
	
	/**
	 * 商品条码上传
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String importProductBarcode(HttpServletRequest request)
		throws Exception,FileTypeException
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String msg=new String();
			
			upload.setFileName("In_product_barcode_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			String lable_template_id = upload.getRequestRow().getString("lable_template_id");
				
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
				////system.out.println(msg);
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				msg = "上传出错";
				////system.out.println(msg);
			}
			else
			{		  
				msg = upload.getFileName();
			}
			msg += ","+lable_template_id;
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importProductBarcode",log);
		}
	}
	
	/**
	 * 2003版excel文件转换DBRow
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
//	public HashMap<String,DBRow[]> excelshow(String filename,String type)
//	throws Exception
//	{
//		try 
//		{
//			String path = "";
//			InputStream is;
//			Workbook rwb;  
//			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
//			if (type.equals("show")) 
//			{
//				path = Environment.getHome() + "upl_excel_tmp/" + filename;
//				is = new FileInputStream(path);
//				rwb = Workbook.getWorkbook(is);
//				//1商品、关系转换DBRow【】，2关系构建map，3，检查商品，4，检查关系
//				DBRow[] products = productData(rwb);//商品DBRow[]
//				DBRow[] unions = unionData(rwb);//关系DBRow[]
//				unionMap = unionMap(unions);//关系构建map
//				DBRow[] errorProducts = checkProduct(products,unionMap);//检查商品
//				DBRow[] errorUnions = checkUnion(unions);//检查关系
//				
//				
//				resultMap.put("errorProducts",errorProducts);
//				resultMap.put("errorUnions",errorUnions);
//			}
//			else if(type.equals("data"))
//			{
//				path = Environment.getHome() + "upl_excel_tmp/" + filename;
//				is = new FileInputStream(path);
//				rwb = Workbook.getWorkbook(is);
//				productData(rwb);
//				
//				DBRow[] products = productData(rwb);//商品DBRow[]
//				DBRow[] unions = unionData(rwb);//关系DBRow[]
//				checkUploadPCode = checkUploadPCodeSet(products);
//				unionMap = unionMap(unions,checkUploadPCode);//关系构建map
//				
//				resultMap.put("products",products);
//				resultMap.put("unions",unions);
//			}
//			
//			return resultMap;
//		} 
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"excelshow",log);
//		}
//	}
	
	/**
	 * 获得商品条码Map
	 * @param products
	 * @return
	 * @throws Exception
	 */
	private HashMap checkUploadPCodeSet(DBRow[] products)
		throws Exception
	{
		try 
		{
			checkUploadPCode = new HashMap();
			for (int i = 0; i < products.length; i++) 
			{
				checkUploadPCode.put(products[i].getString("p_code"),products[i].getString("p_code"));
			}
			
			return checkUploadPCode;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkUploadPCodeSet",log);
		}
	}
	
	/**
	 * 商品条码文件转换成DBRow
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productBarcode(String filename)
		throws Exception
	{
		try 
		{
			HashMap productFiled = new HashMap();
			productFiled.put(0, "p_name");
			productFiled.put(1, "p_code");
			productFiled.put(2,"count");
			productFiled.put(3,"printcount");
			
			String path = Environment.getHome() + "upl_excel_tmp/" + filename;
			InputStream is;
			Workbook rwb;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
			
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();

			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
				   	String content = rs.getCell(j,i).getContents().trim();

					pro.add(productFiled.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
			    
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   DBRow product = floorProductMgrZJ.getDetailProductByPnamePcode(pro.getString("p_name"),pro.getString("p_code"));
				   if(product!=null)
				   {
					   pro.add("pc_id",product.get("pc_id",0l));
				   }
				   else
				   {
					   pro.add("error","error");
				   }
				   al.add(pro);   
			   }
			  }
			  
			  return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"productBarcode",log);
		}
	}
	
	/**
	 * 上传文件商品信息转换DBRow[]
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productData(Workbook rwb) 
		throws Exception
	{
		HashMap productFiled = new HashMap();
		productFiled.put(0, "pc_id");
		productFiled.put(1, "oldPrice");
		productFiled.put(2, "oldWeight");
		productFiled.put(3, "catalog_id");
		productFiled.put(4, "catalog_name");
		productFiled.put(5, "alive");
		productFiled.put(6, "union");
		productFiled.put(7, "p_name");
		
		
		productFiled.put(8, "unit_name");
		
		
		productFiled.put(9, "length");
		productFiled.put(10, "width");
		productFiled.put(11, "heigth");
		
		productFiled.put(12, "weight");
		productFiled.put(13, "unit_price");
		productFiled.put(14,"volume");
		
		productFiled.put(15, "p_code");
		productFiled.put(16, "p_code2");
		productFiled.put(17, "upc");

		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		
		  for(int i=1;i<rsRows;i++)
		  {
		   DBRow pro = new DBRow();

		   
		   boolean result = false;//判断是否纯空行数据
		   for (int j=0;j<rsColumns;j++)
		   {
			   	String content = rs.getCell(j,i).getContents().trim();
			   	if(j==7||j==15)
			   	{
			   		content = content.toUpperCase();
			   	}
				pro.add(productFiled.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
		    
			    ArrayList proName=pro.getFieldNames();//DBRow 字段名
			    for(int p=0;p<proName.size();p++)
			    {
			    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
			    	{
			    		result = true;
			    	}
			    }
		   }
		   
		   if(result)//不是纯空行就加入到DBRow
		   {
			   al.add(pro);   
		   }
		  }
		  
		  return (al.toArray(new DBRow[0]));
	}
	

	/**
	 * 上传文件关系转换DBRow[]
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] unionData(Workbook rwb) 
		throws Exception
	{
		HashMap unionFiled = new HashMap();
		unionFiled.put(0,"product");
		unionFiled.put(1,"accessories");
		unionFiled.put(2,"quantity");
		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		
		  for(int i=1;i<rsRows;i++)
		  {
		   DBRow pro = new DBRow();
		   
		   boolean result = false;//判断是否纯空行数据
		   for (int j=0;j<rsColumns;j++)
		   {
			   if(rs.getCell(j,i).getContents().trim()!=null&&!rs.getCell(j,i).getContents().trim().equals(""))
			   {
				   pro.add(unionFiled.get(j).toString(),rs.getCell(j,i).getContents().trim());  //转换成DBRow数组输出,去掉空格
				    
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
		   }
		   
		   if(result)//不是纯空行就加入到DBRow
		   {
			   al.add(pro);   
		   }
		  }
		  
		  return (al.toArray(new DBRow[0]));
	}
	
	/**
	 * 上传商品文件转换DBRow
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productFileData(Workbook rwb)
		throws Exception
	{
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		Sheet rs = rwb.getSheet(0);
		int rsRows = rs.getRows();    //excel表记录行数
		
		 for(int i=1;i<rsRows;i++)
		  {
			 Cell[] columns = rs.getRow(i);
			 for (int j = 2; j < columns.length; j++) 
			 {
				DBRow product_file = new DBRow();
				product_file.add("pc_id",columns[0].getContents());
				product_file.add("p_name",columns[1].getContents());
				product_file.add("file_name",columns[j].getContents());
				product_file.add("number",i);
				
				al.add(product_file);
			 }
		  }
		  return (al.toArray(new DBRow[0]));
	}
	
	
	/**
	 * 导入数据库转换关系
	 * @param uploadUnions
	 * @param checkUploadPCode
	 * @return
	 * @throws Exception
	 */
	private HashMap unionMap(DBRow[] uploadUnions,HashMap checkUploadPCode)
		throws Exception
	{
		unionMap = new HashMap<String,DBRow[]>();
		unionDBMap = new HashMap<String, DBRow[]>();
		ArrayList<DBRow> accessoriesList = new ArrayList<DBRow>();
		for (int i = 0; i < uploadUnions.length; i++) 
		{
			
			DBRow uploadUnion = uploadUnions[i];
			if(checkUploadPCode.containsKey(uploadUnion.getString("product")))//套装条码是否在上传excel内
			{
				//第一个数据或者判定是同一套装只添加配件条码到list里
				if(i==0||i>0&&uploadUnion.getString("product").equals(uploadUnions[i-1].getString("product")))
				{
					accessoriesList.add(uploadUnion);
				}
				else if(i>0)//先将上一套组合关系放入map，清空关系记录，加入当前记录
				{
					unionMap.put(uploadUnions[i-1].getString("product"),accessoriesList.toArray(new DBRow[0]));
					accessoriesList = new ArrayList<DBRow>();
					accessoriesList.add(uploadUnion);
				}
				
				if(i==uploadUnions.length-1)//最后一组关系放入Map
				{
					unionMap.put(uploadUnions[i].getString("product"),accessoriesList.toArray(new DBRow[0]));
				}
			}
			else
			{
				//第一个数据或者判定是同一套装只添加配件条码到list里
				if(i==0||i>0&&uploadUnion.getString("product").equals(uploadUnions[i-1].getString("product")))
				{
					accessoriesList.add(uploadUnion);
				}
				else if(i>0)//先将上一套组合关系放入map，清空关系记录，加入当前记录
				{
					unionMap.put(uploadUnions[i-1].getString("product"),accessoriesList.toArray(new DBRow[0]));
					unionDBMap.put(uploadUnions[i-1].getString("product"),accessoriesList.toArray(new DBRow[0]));//放入商品表没有的关系Map中
					accessoriesList = new ArrayList<DBRow>();
					accessoriesList.add(uploadUnion);
				}
				
				if(i==uploadUnions.length-1)//最后一组关系放入Map
				{
					unionMap.put(uploadUnions[i].getString("product"),accessoriesList.toArray(new DBRow[0]));
					unionDBMap.put(uploadUnions[i].getString("product"),accessoriesList.toArray(new DBRow[0]));//放入商品表没有的关系Map中
				}
			}
			
		}
		
		return unionMap;
	}
	
	/**
	 * 上传商品文件修改商品
	 * @param request
	 * @throws Exception
	 */
	public void productByUpload(HttpServletRequest request)
	throws Exception
	{
		try 
		{
			updateProductIndex = new ArrayList<DBRow>();
			addProductIndex = new ArrayList<DBRow>();
			String tempfilename = StringUtil.getString(request,"tempfilename");
//			HashMap<String,DBRow[]> excelDBRow = excelshow(tempfilename,"data");//excel导出结果集
			
//			DBRow[] products = excelDBRow.get("products");
			
//			DBRow[]union_products = importScatterProduct(products,request);	

//			importSuitProduct(union_products,request);
			
			addIndex();
			updateIndex();
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"updateProductByUpload",log);
		}
		
	}
	
	private void addIndex()
		throws Exception
	{
		try 
		{
			String[] pc_id = new String[addProductIndex.size()];
			String[] p_name = new String[addProductIndex.size()];
			String[] p_code = new String[addProductIndex.size()];
			String[] catalog_id = new String[addProductIndex.size()];
			String[] unit_name = new String[addProductIndex.size()];
			String[] alive = new String[addProductIndex.size()];
			for (int i = 0; i < addProductIndex.size(); i++)
			{
				pc_id[i] = addProductIndex.get(i).getString("pc_id");
				p_name[i] = addProductIndex.get(i).getString("p_name");
				
				catalog_id[i] = addProductIndex.get(i).getString("catalog_id");
				unit_name[i] = addProductIndex.get(i).getString("unit_name");
				alive[i] = addProductIndex.get(i).getString("alive");
				
				DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(Long.parseLong(pc_id[i]));
				String pcodeString = "";
				for (int j = 0; j < pcodes.length; j++) 
				{
					pcodeString += pcodes[j].getString("p_code")+" ";
				}
				
				p_code[i] = pcodeString;
			}
			
			
			
			ProductIndexMgr.getInstance().batchAddIndexAsyn(pc_id, p_name, p_code, catalog_id, unit_name, alive);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addIndex",log);
		}
		
	}
	
	private void updateIndex()
		throws Exception
	{
		
		try 
		{
			String[] pc_id = new String[updateProductIndex.size()];
			String[] p_name = new String[updateProductIndex.size()];
			String[] p_code = new String[updateProductIndex.size()];
			String[] catalog_id = new String[updateProductIndex.size()];
			String[] unit_name = new String[updateProductIndex.size()];
			String[] alive = new String[updateProductIndex.size()];
			for (int i = 0; i < updateProductIndex.size(); i++)
			{
				pc_id[i] = updateProductIndex.get(i).getString("pc_id");
				p_name[i] = updateProductIndex.get(i).getString("p_name");
				catalog_id[i] = updateProductIndex.get(i).getString("catalog_id");
				unit_name[i] = updateProductIndex.get(i).getString("unit_name");
				alive[i] = updateProductIndex.get(i).getString("alive");
				
				DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(Long.parseLong(pc_id[i]));
				String pcodeString = "";
				for (int j = 0; j < pcodes.length; j++) 
				{
					pcodeString += pcodes[j].getString("p_code")+" ";
				}
				
				p_code[i] = pcodeString;
			}
			
			ProductIndexMgr.getInstance().batchUpdateIndexAsyn(pc_id, p_name, p_code, catalog_id, unit_name, alive);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateIndex",log);
		}
	}
	
	/**
	 * 导入套装商品
	 * @param union_products
	 * @throws Exception
	 */
	/*  商品条码可不惟一 zyj
	public void importSuitProduct(DBRow[] union_products,HttpServletRequest request)
		throws Exception
	{
		try 
		{
			if(union_products!=null&&union_products.length>0)
			{
				for (int i = 0; i < union_products.length; i++) 
				{
					DBRow unionRow = union_products[i];
					unionRow.remove("catalog_name");//excel的分类名称，对数据库没用去掉
					unionRow.remove("union");//excel显示的商品拼装散件，对数据库没用去掉
					unionRow.remove("oldPrice");
					unionRow.remove("oldWeight");
					
					
					long catalog_id = Long.valueOf(unionRow.getString("catalog_id"));
					String p_name = unionRow.getString("p_name");
					String p_code = unionRow.getString("p_code");
					String unit_name = unionRow.getString("unit_name");
					int alive = Integer.valueOf(unionRow.getString("alive"));
					
					unionRow.add("weight",0);//套装不检查重量，但是插入时需要
					unionRow.add("unit_price",0);//套装不检查产品单价，但是插入时需要
					
					DBRow[] unionlist = unionMap.get(p_code);//根据组合关系Map中的键获得对应的商品ID配件组合关系
					
					DBRow paraIndex = new DBRow();
					paraIndex.add("p_name",p_name);
					paraIndex.add("p_code",p_code);
					paraIndex.add("catalog_id",catalog_id);
					paraIndex.add("unit_name",unit_name);
					paraIndex.add("alive",alive);
					
					if(unionRow.getString("pc_id").equals(""))//根据ID找不到对应商品，新加
					{
						unionRow.remove("pc_id");
						unionRow.add("union_flag",1);
						
						long newsuit_pc_id = floorProductMgr.addProduct(unionRow);
						
						paraIndex.add("pc_id",newsuit_pc_id);
						addProductIndex.add(paraIndex);
						
						combinationProductUnion(newsuit_pc_id, unionlist,request);//套装ID,添加配件组合关系
						
						//根据商品ID获得商品信息（此处为添加套装，对添加套装与修改套装记录成一条）
						
						DBRow product_log = floorProductMgr.getDetailProductByPcid(newsuit_pc_id);
						
						AdminMgr am = new AdminMgr();
						AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
						
						product_log.add("account",adminLoggerBean.getAdid());
						product_log.add("edit_type",ProductEditTypeKey.ADD);
						product_log.add("edit_reason",ProductEditReasonKey.SELF);
						product_log.add("post_date",DateUtil.NowStr());
						floorProductLogsMgrZJ.addProductLogs(product_log);

						
					}
					else
					{
						long pc_id = Long.valueOf(unionRow.getString("pc_id"));
						
						floorProductMgr.modProduct(pc_id,unionRow);
						
						paraIndex.add("pc_id",pc_id);
						updateProductIndex.add(paraIndex);
						
						cancelProductUnion(pc_id);//取消原有关系
						
						combinationProductUnion(pc_id, unionlist,request);//根据上传组合关系从新创建组合关系
						
						//根据商品ID获得商品信息（此处为添加套装，对添加套装与修改套装记录成一条）,记录商品修改日志
						
						DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
						
						AdminMgr am = new AdminMgr();
						AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
						
						product_log.add("account",adminLoggerBean.getAdid());
						product_log.add("edit_type",ProductEditTypeKey.UPDATE);
						product_log.add("edit_reason",ProductEditReasonKey.SELF);
						product_log.add("post_date",DateUtil.NowStr());
						floorProductLogsMgrZJ.addProductLogs(product_log);
					}
				}
			}
			
			 // excel没有商品的关系
			 
			Set<String> unionDB = unionDBMap.keySet();
			if (unionDB.size()>0) 
			{
				for (String pcode : unionDB) 
				{
					DBRow[] unionDBlist = unionDBMap.get(pcode);

					long unpc_id = floorProductMgr.getDetailProductByPcode(pcode).get("pc_id", 0l);

					cancelProductUnion(unpc_id);//取消原有关系

					combinationProductUnion(unpc_id, unionDBlist,request);//根据上传组合关系从新创建组合关系
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"importSuitProduct",log);
		}
	}*/
	
	/**
	 * 散件商品导入，返回套装商品DBRow[]
	 * @param products
	 * @return
	 * @throws Exception
	 */
	public DBRow[] importScatterProduct(DBRow[] products,HttpServletRequest request)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> unionporducts = new ArrayList<DBRow>();
			for (int i = 0; i <products.length; i++) 
			{
				if(unionMap.containsKey(products[i].getString("p_code")))//套装商品先不添加或修改
				{
					unionporducts.add(products[i]);
				}
				else//散件商品
				{
					DBRow product = products[i];
					
					product.remove("catalog_name");//excel的分类名称，对数据库没用去掉
					product.remove("union");//excel显示的商品拼装散件，对数据库没用去掉
					
					long catalog_id = Long.valueOf(product.getString("catalog_id"));
					String p_name = product.getString("p_name");
					String p_code = product.getString("p_code");
					String unit_name = product.getString("unit_name");
					int alive = Integer.valueOf(product.getString("alive"));
					
					DBRow paraIndex = new DBRow();
					paraIndex.add("p_name",p_name);
					paraIndex.add("p_code",p_code);
					paraIndex.add("catalog_id",catalog_id);
					paraIndex.add("unit_name",unit_name);
					paraIndex.add("alive",alive);
					
					if(product.getString("pc_id").equals(""))//数据库内找不到对应商品,新建商品
					{
						product.remove("pc_id");
						product.remove("oldPrice");
						product.remove("oldWeight");
						
						product.add("union_flag",0);
						
						long new_pc_id = floorProductMgr.addProduct(product);
						
						paraIndex.add("pc_id",new_pc_id);
						addProductIndex.add(paraIndex);
						
						//记录商品添加日志
						DBRow product_log = floorProductMgr.getDetailProductByPcid(new_pc_id);
						
						AdminMgr am = new AdminMgr();
						AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
						
						product_log.add("account",adminLoggerBean.getAdid());
						product_log.add("edit_type",ProductEditTypeKey.ADD);
						product_log.add("edit_reason",ProductEditReasonKey.SELF);
						product_log.add("post_date",DateUtil.NowStr());
						floorProductLogsMgrZJ.addProductLogs(product_log);
						
						//ProductIndexMgr.getInstance().addIndexAsyn(new_pc_id,p_name, p_code, catalog_id,unit_name,alive);//新加商品加索引
					}
					
					else//商品ID有对应商品,该商品为套装删除关系,修改商品，并重算已此商品为配件的套装
					{
						long pc_id = Long.valueOf(product.getString("pc_id"));
						
						double oldPrice = Double.valueOf(product.getString("oldPrice"));
						float oldWeight = Float.valueOf(product.getString("oldWeight"));
						double newPrice = Double.valueOf(product.getString("unit_price"));
						float newWeight = Float.valueOf(product.getString("weight"));
						
						product.remove("oldPrice");
						product.remove("oldWeight");
						cancelProductUnion(pc_id);//删除原有套装关系,更改商品为散件
						
						floorProductMgr.modProduct(pc_id,product);
						
						paraIndex.add("pc_id",pc_id);
						updateProductIndex.add(paraIndex);
						
						//记录商品修改日志，改的散件，是自身修改
						DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
						
						AdminMgr am = new AdminMgr();
						AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
						
						product_log.add("account",adminLoggerBean.getAdid());
						product_log.add("edit_type",ProductEditTypeKey.UPDATE);
						product_log.add("edit_reason",ProductEditReasonKey.SELF);
						product_log.add("post_date",DateUtil.NowStr());
						floorProductLogsMgrZJ.addProductLogs(product_log);
						
						
						//ProductIndexMgr.getInstance().updateIndexAsyn(pc_id, p_name, p_code, catalog_id,unit_name,alive);
						
						productMgr.updateSetPriceAndWeightByPid(pc_id,oldPrice,newPrice,oldWeight,newWeight,adminLoggerBean);
						
					}
				}
			}
			return unionporducts.toArray(new DBRow[0]);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importScatterProduct",log);
		}
	}
	/**
	 * 根据商品ID取消套装，恢复商品为散件
	 * @param set_pid
	 * @throws Exception
	 */
	public void cancelProductUnion(long set_pid)
		throws Exception
	{
		try 
		{
			floorProductMgrZJ.delProductUnionBySetPid(set_pid);//根据商品ID删除其作为套装的关系
			
			DBRow para = new DBRow();
			para.add("union_flag",0);
				
			//此处可不记录日志
			floorProductMgr.modProduct(set_pid,para);//修改商品为散件
		}
		catch (Exception e)
		{
			throw new SystemException(e,"cancelProductUnion",log);
		}
	}
	
	/**
	 * 组合商品套装
	 * @param pc_id
	 * @param unionlist,组合关系DBRow[]
	 * @throws Exception
	 */
	/*  商品条码可不惟一 zyj
	public void combinationProductUnion(long pc_id,DBRow[] unionlist,HttpServletRequest request)
		throws Exception
	{
		String set_pid = "";
		String pid = "";
		String quantity = "";
		try 
		{
			if(unionlist.length>0)
			{
				for (int j = 0; j < unionlist.length; j++) 
				{
					DBRow union = unionlist[j];
					DBRow accessories = floorProductMgr.getDetailProductByPcode(union.getString("accessories"));
					
					DBRow unionpara = new DBRow();
					
					set_pid = pc_id+"";
					pid = accessories.getString("pc_id");
					quantity = union.getString("quantity");
					
					unionpara.add("set_pid",pc_id);
					unionpara.add("pid",accessories.getString("pc_id"));
					unionpara.add("quantity",union.getString("quantity"));
					
					floorProductMgr.addProductUnion(unionpara);
				}
				
				DBRow para = floorProductMgrZJ.UpdateSuitPara(pc_id);
				para.add("union_flag",1);
				
				floorProductMgr.modProduct(pc_id,para);
				
				
				//记录商品修改日志
				
				DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
				
				AdminMgr am = new AdminMgr();
				AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"combinationProductUnion",log);
		}
		
	}*/
	
	/**
	 * 根据采购单ID获得采购单内所有商品信息
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductsByPurchaseId(long purchase_id,PageCtrl pc) 
		throws Exception
	{
		
		try 
		{
			return (floorProductMgrZJ.getProductsByPurchaseId(purchase_id,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 在采购单内商品中检索
	 * @param purchase_id
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchProductsByPurchaseIdKey(long purchase_id,String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorProductMgrZJ.searchProductsByPurchaseIdKey(purchase_id, key, pc);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * Jqgrid用修改商品信息
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modProductWithJqgrid(HttpServletRequest request) throws Exception {
		
		try {
			
			long pc_id = StringUtil.getLong(request, "id");
			Map parameter = request.getParameterMap();
			String p_name;
			String p_code;
			String p_code2;
			long catalog_id;
			String unit_name;
			double unit_price;
			float gross_profit;
			float weight;
			float volume;
			
			float length;
			float width;
			float heigth;
			int sn_size;
			String upc;
			String nmfc_code;
			int freight_class;
			
			DBRow para = new DBRow();
			DBRow oldDetail = floorProductMgr.getDetailProductByPcid(pc_id);
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			if(parameter.containsKey("p_name"))
			{
				p_name = StringUtil.getString(request, "p_name");
				p_name = p_name.toUpperCase();
				
				if (!oldDetail.getString("p_name").equals(p_name))
				{
					DBRow detail = floorProductMgr.getDetailProductByPname(p_name);
					if (detail!=null)
					{
						throw new ProductNameIsExistException();
					}
				}
				para.add("p_name",p_name);
			}
			if(parameter.containsKey("p_code"))
			{
				p_code = StringUtil.getString(request, "p_code");
				p_code = p_code.toUpperCase();
				
				/* 商品条码可不惟一 zyj
				if (!oldDetail.getString("p_code").equals(p_code))
				{
					DBRow detail = floorProductMgrZJ.checkPcodeExist(p_code);
					if (detail!=null)
					{
						throw new ProductCodeIsExistException();
					}
				}*/
				
				productCodeMgrZJ.addProductCodeSub(pc_id,p_code,CodeTypeKey.Main,adminLoggerBean);
			}
			
			if(parameter.containsKey("p_code2"))
			{
				p_code2 = StringUtil.getString(request, "p_code2");
				p_code2 = p_code2.toUpperCase();
				
				if (!oldDetail.getString("p_code2").equals(p_code2)&&!p_code2.trim().equals(""))
				{
					DBRow detail = floorProductMgrZJ.checkPcodeExist(p_code2);
					if (detail!=null)
					{
						throw new ProductCodeIsExistException();
					}
				}
				
				productCodeMgrZJ.addProductCodeSub(pc_id,p_code2,CodeTypeKey.Amazon,adminLoggerBean);
			}
			
			if(parameter.containsKey("catalog_text"))
			{
				catalog_id = StringUtil.getLong(request, "catalog_text");
				
				para.add("catalog_id",catalog_id);
			}
			
			if(parameter.containsKey("nmfc_code"))
			{
				nmfc_code = StringUtil.getString(request, "nmfc_code");
				
				para.add("nmfc_code",nmfc_code);
			}
			
			if(parameter.containsKey("freight_class"))
			{
				freight_class = StringUtil.getInt(request, "freight_class");
				
				para.add("freight_class",freight_class);
			}
			if(parameter.containsKey("unit_name"))
			{
				unit_name = StringUtil.getString(request, "unit_name");
				unit_name = unit_name.toUpperCase();
				
				para.add("unit_name",unit_name);
			}
			if(parameter.containsKey("unit_price"))
			{
				unit_price = StringUtil.getDouble(request, "unit_price");
				
				para.add("unit_price",unit_price);
			}
			if(parameter.containsKey("weight"))
			{
				weight = StringUtil.getFloat(request, "weight");
				
				para.add("weight",weight);
			}
			
			if(parameter.containsKey("length"))
			{
				length = StringUtil.getFloat(request,"length");
				
				para.add("length",length);
			}
			
			if(parameter.containsKey("width"))
			{
				width = StringUtil.getFloat(request,"width");
				
				para.add("width",width);
			}
			
			if(parameter.containsKey("heigth"))
			{
				heigth = StringUtil.getFloat(request,"heigth");
				
				para.add("heigth",heigth);
			}
			
			if(parameter.containsKey("sn_size"))
			{
				sn_size = StringUtil.getInt(request, "sn_size");
				
				para.add("sn_size", sn_size);
			}
			
			if(parameter.containsKey("upc"))
			{
				upc = StringUtil.getString(request,"upc");
				
				if (!oldDetail.getString("upc").equals(upc)&&!upc.trim().equals(""))
				{
					DBRow detail = floorProductMgrZJ.checkPcodeExist(upc);
					
					if(detail!=null)
					{
						throw new UPCExistException();
					}
				}
				
				productCodeMgrZJ.addProductCodeSub(pc_id,upc,CodeTypeKey.UPC,adminLoggerBean);
			}
			String length_uom = "";
			if(parameter.containsKey("length_uom_name"))
			{
				length_uom = StringUtil.getString(request, "length_uom_name");
				para.add("length_uom", length_uom);
			}
			String weigth_uom = "";
			if(parameter.containsKey("weight_uom_name"))
			{
				weigth_uom = StringUtil.getString(request, "weight_uom_name");
				para.add("weight_uom", weigth_uom);
			}
			String price_uom = "";
			if(parameter.containsKey("price_uom_name"))
			{
				price_uom = StringUtil.getString(request, "price_uom_name");
				para.add("price_uom", price_uom);
			}
			
			//修改条码后不修改product表，修改para里没有参数
			if(para.getFieldNames().size()>0)
			{
				floorProductMgr.modProduct(pc_id, para);

				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow logRow = new DBRow();
				//create/update/delete
				logRow.put("event", "update");
				//更新的表名
				logRow.put("table", "product");
				//主键字段名
				logRow.put("column", "pc_id");
				//主键ID
				logRow.put("pri_key", pc_id);
				//依赖表
				logRow.put("ref_table", "");
				//依赖表主键字段名
				logRow.put("ref_column", "");
				//依赖表主键ID
				logRow.put("ref_pri_key", "");
				//账号ID
				logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				logRow.put("describe", "update product "+pc_id);
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] content = DBRowUtils.dbRowConvertToArray(para);
				logRow.put("content", content);
				
				floorSetupLogMgr.addSetupLog(logRow);
				
				/*******************************************************************************************************/
				
				//根据商品ID获得商品信息,记录商品修改日志
				DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			}
			
			DBRow newProduct = floorProductMgr.getDetailProductByPcid(pc_id);
			
			/**
			 * 如果长宽高修改过，重算体积
			 */
			if(parameter.containsKey("length")||parameter.containsKey("width")||parameter.containsKey("heigth"))
			{
				volume = newProduct.get("length",0f)*newProduct.get("width",0f)*newProduct.get("heigth",0f);
				
				DBRow volumePara = new DBRow();
				volumePara.add("volume",volume);
				floorProductMgr.modProduct(pc_id,volumePara);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow _logRow = new DBRow();
				//create/update/delete
				_logRow.put("event", "update");
				//更新的表名
				_logRow.put("table", "product");
				//主键字段名
				_logRow.put("column", "pc_id");
				//主键ID
				_logRow.put("pri_key", pc_id);
				//依赖表
				_logRow.put("ref_table", "");
				//依赖表主键字段名
				_logRow.put("ref_column", "");
				//依赖表主键ID
				_logRow.put("ref_pri_key", "");
				//账号ID
				_logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				_logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				_logRow.put("describe", "update product "+pc_id);
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] _content = DBRowUtils.dbRowConvertToArray(volumePara);
				_logRow.put("content", _content);
				
				floorSetupLogMgr.addSetupLog(_logRow);
				
				/*******************************************************************************************************/
			}
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			
			ProductIndexMgr.getInstance().updateIndexAsyn(pc_id,newProduct.getString("p_name"),pcodeString,newProduct.get("catalog_id",0l),newProduct.getString("unit_name"),oldDetail.get("alive", 0));
			
			//如果修改了价格或者重量，则更新所有包含该商品的套装价格和重量
			productMgr.updateSetPriceAndWeightByPid(pc_id,oldDetail.get("unit_price", 0d),newProduct.get("unit_price", 0d),oldDetail.get("weight", 0f),newProduct.get("weight", 0f),adminLoggerBean);
			
			DBRow fintProductNode = floorProductMgr.getDetailProductByPcid(pc_id);

			Map<String,Object> productNode = new HashMap<String, Object>();
			productNode.put("pc_id", fintProductNode.get("pc_id",0l));
			productNode.put("p_name",fintProductNode.getString("p_name"));
			productNode.put("unit_name",fintProductNode.getString("unit_name"));
			productNode.put("unit_price",fintProductNode.getString("unit_price"));
			productNode.put("catalog_id",fintProductNode.getString("catalog_id"));
			productNode.put("orignal_pc_id", fintProductNode.get("orignal_pc_id",0l));
			productNode.put("union_flag",fintProductNode.getString("union_flag"));
			productNode.put("width",fintProductNode.getString("width"));
			productNode.put("weight",fintProductNode.getString("weight"));
			productNode.put("heigth", fintProductNode.getString("heigth"));
			productNode.put("length", fintProductNode.getString("length"));
			productNode.put("length_nom",fintProductNode.get("length_uom",0l));
			productNode.put("weight_nom",fintProductNode.get("weight_uom",0l));
			productNode.put("price_uom",fintProductNode.getString("price_uom"));
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("pc_id", pc_id);
			
			productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//修改商品节点alive属性
		} 
		catch(ProductNameIsExistException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch(UPCExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	//修改商品提供接口
	public void modProductSub(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,
	double unit_price,float length,float width,float heigth,float weight, int sn_size,AdminLoginBean adminLoggerBean)
	throws ProductNameIsExistException,ProductCodeIsExistException,Exception{
		try 
		{
			float volume;
			
			DBRow para = new DBRow();
			DBRow oldDetail = floorProductMgr.getDetailProductByPcid(pc_id);
			
			//如果商品名称做了修改需要检查是否重复
				if (!oldDetail.getString("p_name").toUpperCase().equals(p_name))
				{
					DBRow detail = floorProductMgr.getDetailProductByPname(p_name);
					if (detail!=null)
					{
						throw new ProductNameIsExistException();
					}
				}
				para.add("p_name",p_name);
			
				//如果主条码做了修改需要检查是否重复
				
				if (!oldDetail.getString("p_code").toUpperCase().equals(p_code))
				{
					DBRow detail = floorProductMgrZJ.checkPcodeExist(p_code);
					if (detail!=null)
					{
						throw new ProductCodeIsExistException();
					}
					productCodeMgrZJ.addProductCodeSub(pc_id,p_code,CodeTypeKey.Main,adminLoggerBean);
				}
				
				// 如果长宽高修改了要重算体积
				volume = length*width*heigth/1000;
					
				para.add("volume",volume);
				
				para.add("catalog_id",catalog_id);
				para.add("unit_name",unit_name);
				para.add("unit_price",unit_price);
				para.add("weight",weight);
				para.add("length",length);
				para.add("width",width);
				para.add("heigth",heigth);
				//张睿 sn_size
				para.add("sn_size", sn_size);
				
			//修改条码后不修改product表，修改para里没有参数
			if(para.getFieldNames().size()>0)
			{
				floorProductMgr.modProduct(pc_id, para);
				//根据商品ID获得商品信息,记录商品修改日志
				
				DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			}
			
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			
			ProductIndexMgr.getInstance().updateIndexAsyn(pc_id, p_name,pcodeString, catalog_id,unit_name,oldDetail.get("alive", 0));
			//如果修改了价格或者重量，则更新所有包含该商品的套装价格和重量
			productMgr.updateSetPriceAndWeightByPid(pc_id,oldDetail.get("unit_price", 0d),unit_price,oldDetail.get("weight", 0f),weight,adminLoggerBean);
		} 
		catch(ProductNameIsExistException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modProductSub",log);
		}
	}
	/**
	 * 根据配件ID获得套装
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductInfoUnionsByPid(long pid)
		throws Exception
	{
		try 
		{
			return floorProductMgr.getProductInfoUnionsByPid(pid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * jqgrid添加商品
	 * @param request
	 * @throws ProductCodeIsExistException
	 * @throws ProductNameIsExistException
	 * @throws Exception
	 */
	public void gridAddProduct(HttpServletRequest request)
	throws ProductCodeIsExistException,ProductNameIsExistException,Exception
	{
		try {
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
			
			String p_name = StringUtil.getString(request, "p_name").toUpperCase();
			String p_code = StringUtil.getString(request, "p_code").toUpperCase();
			long catalog_id = StringUtil.getLong(request, "catalog_text");
			
			String unit_name = StringUtil.getString(request, "unit_name");
			double unit_price = StringUtil.getDouble(request, "unit_price");
			
			float length = StringUtil.getFloat(request,"length");
			float width = StringUtil.getFloat(request,"width");
			float heigth = StringUtil.getFloat(request,"heigth");
			//float gross_profit = StrUtil.getFloat(request, "gross_profit");
			float weight = StringUtil.getFloat(request, "weight");
			int sn_size = StringUtil.getInt(request, "sn_size");
			
			gridAddProductSub(p_name,p_code,catalog_id,unit_name,unit_price,length,width,heigth,weight,sn_size,adminLoggerBean);
		
		} catch (Exception e) {
			throw new SystemException(e,"gridAddProduct",log);
		}
	}
	

	//添加商品基础数据接口提交 
	public long gridAddProductSub(String p_name, String p_code,
			long catalog_id, String unit_name, double unit_price, float length,
			float width, float heigth, float weight,int sn_size,
			AdminLoginBean adminLoggerBean)
		throws ProductCodeIsExistException,ProductNameIsExistException,Exception
	{
		try
		{
			float volume = length*width*heigth/1000;//添加商品体积
			
			DBRow detailPro = floorProductMgr.getDetailProductByPname(p_name);
			
			if (detailPro!=null)
			{
				throw new ProductNameIsExistException();	//商品名称已经存在
			}
		/*	 商品条码可不惟一 zyj
			detailPro = floorProductMgr.getDetailProductByPcode(p_code);
			
			if (detailPro!=null)
			{
				throw new ProductCodeIsExistException();	//商品条码已经存在
			}
			*/
			unit_name = unit_name.toUpperCase();
			
			DBRow product = new DBRow();
			product.add("p_name",p_name);
			//product.add("p_code",p_code)
			product.add("catalog_id",catalog_id);
			product.add("unit_name",unit_name);
			product.add("unit_price",unit_price);
			//product.add("gross_profit",(gross_profit/100));
			product.add("weight",weight);
			product.add("union_flag",0);
			product.add("alive",1);//允许抄单
			product.add("volume",volume);//设定商品体积zhanjie
			product.add("length",length);
			product.add("width",width);
			product.add("heigth",heigth);
			product.add("sn_size", sn_size);
			long pc_id = floorProductMgr.addProduct(product);
			
			productCodeMgrZJ.addProductCodeSubNoLog(pc_id,p_code,CodeTypeKey.Main,adminLoggerBean);
			
			//记录商品添加日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.ADD);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			//把新商品在所有仓库建库
			CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
			DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
			for ( int i=0; i<treeRows.length; i++ )
			{
				if (treeRows[i].get("parentid",0)==0)
				{
					DBRow store = new DBRow();
					store.add("store_station", null);
					store.add("post_date",DateUtil.NowStr());
					store.add("cid",treeRows[i].get("id",0));
					store.add("pc_id",pc_id);
					store.add("store_count",0);
					store.add("store_count_alert",0);
					floorProductMgr.addProductStorage(store);
				}
			}
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			//加商品与title之间的关系
			proprietaryMgrZyj.addProprietaryProductByMaxPriority(pc_id, adminLoggerBean);
			ProductIndexMgr.getInstance().addIndexAsyn(pc_id, p_name,pcodeString, catalog_id,unit_name,1);
			return pc_id ;
		}
		catch (ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (ProductNameIsExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"gridAddProductSub",log);
		}
	}

	/**
	 * grid删除商品
	 * @param request
	 * @throws ProductInUnionException
	 * @throws ProductHasRelationStorageException
	 * @throws Exception
	 */
	public void gridDelProduct(HttpServletRequest request) 
	throws ProductInUnionException,ProductHasRelationStorageException,Exception
	{
		try {
			
			long pcid = StringUtil.getLong(request, "id");
			gridDelProductSub(pcid);
			
		} catch(ProductInUnionException e)
		{
			throw new ProductInUnionException();
		}
		catch (Exception e) {
			throw new SystemException(e,"gridDelProduct",log);
		}
	}
	//删除商品提交
	public void gridDelProductSub(long pcid)
		throws ProductInUnionException,ProductHasRelationStorageException,Exception
	{
		try
		{	
			
			//商品有组合关系，不能删除
			if (floorProductMgr.getProductUnionsById(pcid).length>0)
			{
				throw new ProductInUnionException();
			}
			
			floorProductMgr.delProductStorageByPcId(pcid);//先删除库存
			floorProductMgr.delProduct(pcid);
			productCodeMgrZJ.delProductCodeByPcid(pcid);
			ProductIndexMgr.getInstance().deleteIndexAsyn(pcid);
		}
		catch (ProductInUnionException e)
		{
			throw e;
		}
		catch (ProductHasRelationStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductSub",log);
		}
	}
	
	public void addUnionJagrid(HttpServletRequest request)
		throws Exception
	{
		long set_pid = StringUtil.getLong(request,"set_pid");
		String p_name = StringUtil.getString(request,"p_name");
		float quantity = StringUtil.getFloat(request,"quantity");
	}
	
	/**
	 * 套装关系Grid修改
	 * @param request
	 * @throws Exception
	 */
	public void modUnionWithJqgrid(HttpServletRequest request)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		try 
		{
			Map parameter = request.getParameterMap();
			
			long pid = StringUtil.getLong(request,"id");
			long set_pid = StringUtil.getLong(request,"set_pid");
			long old_pid = pid;

			if(parameter.containsKey("p_name")||parameter.containsKey("quantity"))//修改套装关系或数量
			{
				modUnion(request, set_pid, old_pid);
			}
			else if (parameter.containsKey("unit_price")||parameter.containsKey("weight"))//修改了商品单价与重量，套装信息需要刷新
			{
				modProductNeedRefUnion(request, old_pid);
			}
			else
			{
				modProductNotNeedRefUnion(request, old_pid);
			}
			
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
		
	}
	

	//套装关系Grid修改提交
	public void modUnionWithJqgridSub(String p_name,long set_pid,long pid,float quantity,double unit_price,float weight,String unit_name,String p_code,AdminLoginBean adminLoggerBean)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		try 
		{
				long old_pid = pid;
				DBRow product_log=null;
			
				DBRow oldDetail = floorProductMgr.getDetailProductByPcid(old_pid);
				DBRow pUnion = new DBRow();
				if (!oldDetail.getString("p_name").toUpperCase().equals(p_name))
				{
					//验证通过后，获得商品ID，验证失败会抛出相应异常
						pid = productMgr.validateProductUnion(set_pid,p_name);		
						pUnion.add("pid", pid);	
						floorProductMgr.modProductUnion(set_pid,old_pid,pUnion);
				
					productMgr.updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
					//根据商品ID获得商品信息,记录商品修改日志
					 product_log = floorProductMgr.getDetailProductByPcid(set_pid);
				}else{
					
					if(!oldDetail.getString("quantity").equals(quantity)){
						
						pUnion.add("quantity",quantity);
						
						floorProductMgr.modProductUnion(set_pid,old_pid,pUnion);
						
						productMgr.updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
						//根据商品ID获得商品信息,记录商品修改日志
						 product_log = floorProductMgr.getDetailProductByPcid(set_pid);
					}
					
					DBRow para = new DBRow();
					
					para.add("unit_price", unit_price);  //修改了商品单价与重量，套装信息需要刷新
					para.add("weight", weight);
					para.add("unit_name",unit_name);
					floorProductMgr.modProduct(old_pid, para);
					
					//修改条码
					if (!oldDetail.getString("p_code").toUpperCase().equals(p_code))
					{
						/* 商品条码可不惟一 zyj
						DBRow detailPro = floorProductMgr.getDetailProductByPcode(p_code);
						
						if (detailPro!=null)
						{
							throw new ProductCodeIsExistException();	//商品条码已经存在
						}
						*/
						productCodeMgrZJ.addProductCodeSub(old_pid,p_code,CodeTypeKey.Main,adminLoggerBean);
					}
					//根据商品ID获得商品信息,记录商品修改日志
				    product_log = floorProductMgr.getDetailProductByPcid(old_pid);
					DBRow newProduct = floorProductMgr.getDetailProductByPcid(old_pid);
	
					productMgr.updateSetPriceAndWeightByPid(old_pid, oldDetail.get("unit_price", 0d), newProduct.get("unit_price", 0d),oldDetail.get("weight", 0f), newProduct.get("weight",0f),adminLoggerBean);
			

				//根据商品ID获得商品信息,记录商品修改日志
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			}
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
		
	}
	
	/**
	 * 删除套装关系
	 * @param request
	 * @throws Exception
	 */
	public void delProductUnion(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request,"set_pid");
			long pid = StringUtil.getLong(request,"id");
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			delProductUnionSub(set_pid,pid,adminLoggerBean);
			
			//system.out.println("del1");
			
		}
		catch (ProductNotExistException e)
		{
			throw new ProductNotExistException();
		}
	}
	//删除套装关系提交
	public void delProductUnionSub(long set_pid,long pid,AdminLoginBean adminLoggerBean)
	throws Exception{
		
		try {
			
			//system.out.println("del2");
			if ( set_pid>0&&pid>0 )
			{
				floorProductMgr.delProductUnion(set_pid,pid);
			}

			//删除后，需要对组合商品进行一次统计，如果其下没有子商品，需要修改他的组合标志为普通商品
			DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(set_pid);
			if (productsInSet.length==0)
			{
				DBRow markNoralPro = new DBRow();
				markNoralPro.add("union_flag","0");
				floorProductMgr.modProduct(set_pid, markNoralPro);
				
				Map<String,Object> searchFor = new HashMap<String, Object>();
				searchFor.put("pc_id", set_pid);
				Map<String,Object> productNode = new HashMap<String, Object>();
				productNode.put("pc_id", set_pid);
				productNode.put("union_flag",0);
				productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//修改商品节点
				////system.out.println("定位del");
			}
			
			productMgr.updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(set_pid);
			if(null == product_log)
			{
				throw new ProductNotExistException();
			}
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
		} catch (ProductNotExistException e) {
			throw e;
		}
	}
	
	/**
	 * 修改套装中配件关系或配件数量
	 * @throws Exception
	 */
	private void modUnion(HttpServletRequest request,long set_pid,long old_pid)
		throws Exception
	{
		try 
		{
			Map parameter = request.getParameterMap();
			String p_name;
			float quantity;
			
			DBRow pUnion = new DBRow();
			
			if(parameter.containsKey("p_name"))
			{
				p_name = StringUtil.getString(request,"p_name");
				
				long pid = productMgr.validateProductUnion(set_pid,p_name);		//验证通过后，获得商品ID，验证失败会抛出相应异常
				
				pUnion.add("pid", pid);
			}
			
			if(parameter.containsKey("quantity"))
			{
				quantity = StringUtil.getFloat(request,"quantity");
				
				pUnion.add("quantity",quantity);
			}
			
			floorProductMgr.modProductUnion(set_pid,old_pid,pUnion);
			
			productMgr.updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(set_pid);
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
		} 
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 修改商品单价或重量，需修改套装重量或价格
	 * @param request
	 * @param old_pid
	 * @throws Exception
	 */
	private void modProductNeedRefUnion(HttpServletRequest request,long old_pid)
		throws Exception
	{
		try 
		{
			Map parameter = request.getParameterMap();
			double unit_price;
			float weight;
			
			DBRow oldDetail = floorProductMgr.getDetailProductByPcid(old_pid);
			
			DBRow para = new DBRow();
			
			if (parameter.containsKey("unit_price")) 
			{
				unit_price = StringUtil.getDouble(request, "unit_price");

				para.add("unit_price", unit_price);
			}
			if (parameter.containsKey("weight")) 
			{
				weight = StringUtil.getFloat(request, "weight");

				para.add("weight", weight);
			}
			floorProductMgr.modProduct(old_pid, para);
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(old_pid);
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			
			DBRow newProduct = floorProductMgr.getDetailProductByPcid(old_pid);
			productMgr.updateSetPriceAndWeightByPid(old_pid, oldDetail.get("unit_price", 0d), newProduct.get("unit_price", 0d),oldDetail.get("weight", 0f), newProduct.get("weight",0f),adminLoggerBean);
		} 
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 修改商品信息，不需刷新套装
	 * @param request
	 * @param old_pid
	 * @throws Exception
	 */
	private void modProductNotNeedRefUnion(HttpServletRequest request,long old_pid)
		throws Exception
	{
		try 
		{
			
			Map parameter = request.getParameterMap();
			String unit_name;
			String p_code;
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			//float gross_profit;
			
			float volume;
			
			DBRow para = new DBRow();
			
			if (parameter.containsKey("unit_name")) 
			{
				unit_name = StringUtil.getString(request, "unit_name");

				para.add("unit_name", unit_name);
			}
			if (parameter.containsKey("volume"))
			{
				volume = StringUtil.getFloat(request, "volume");

				para.add("volume", volume);
			}
			if (parameter.containsKey("p_code")) 
			{
				p_code = StringUtil.getString(request, "p_code");
				/* 商品条码可不惟一 zyj
				DBRow detailPro = floorProductMgr.getDetailProductByPcode(p_code);
				
				if (detailPro!=null)
				{
					throw new ProductCodeIsExistException();	//商品条码已经存在
				}*/

				productCodeMgrZJ.addProductCodeSub(old_pid,p_code,CodeTypeKey.Main,adminLoggerBean);
			}
			
			if(para.getFieldNames().size()>0)
			{
				floorProductMgr.modProduct(old_pid, para);		
				
				//根据商品ID获得商品信息,记录商品修改日志
				DBRow product_log = floorProductMgr.getDetailProductByPcid(old_pid);
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			}
		} 
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 根据配件ID获得套装信息
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUnionProduct(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long pid = StringUtil.getLong(request,"pid");
			return floorProductMgr.getProductInfoUnionsByPid(pid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 审核文件改变商品
	 */
	public void approveImportProduct(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long ipa_id = StringUtil.getLong(request,"ipa_id");
			int upload_type = StringUtil.getInt(request,"upload_type");
			int approve_status = StringUtil.getInt(request,"approve_status");
			
			DBRow approveImportProduct = floorImportProductMgrZJ.getDetailImportProductApprove(ipa_id);
			String filename = approveImportProduct.getString("filename");
			
			
			if(approve_status==2)
			{
				DBRow[] error = null;
				if(upload_type == UploadEditFileKey.CommodityData)
				{
					error = checkImportProduct(filename);
					
				}
				else if(upload_type == UploadEditFileKey.UnionRelation)
				{
					
					error = checkImportProductUnion(filename);
				}
				
				if(error==null||error.length>0)
				{
					throw new ProductDateChangeException();
				}
				
				if(upload_type == UploadEditFileKey.CommodityData)
				{
					importProductDB(filename,approveImportProduct.get("import_adid",0l));//商品导入数据库
				}
				else if(upload_type == UploadEditFileKey.UnionRelation)
				{
					importProductUnionDB(filename,approveImportProduct.get("import_adid",0l));//套装关系导入数据库
				}
			}
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			DBRow approveImportProductPara = new DBRow();
			approveImportProductPara.add("approve_adid",adminLoggerBean.getAdid());
			approveImportProductPara.add("approve_date",DateUtil.NowStr());
			approveImportProductPara.add("approve_status",approve_status);
			
			floorImportProductMgrZJ.modImportProductApprove(ipa_id, approveImportProductPara);
		} 
		catch(ProductDateChangeException e)
		{
			throw e;
		}
		catch(Exception e) 
		{
			throw new SystemException(e,"approveImportProduct",log);
		}
		
	}
	
	/**
	 * 检查导入商品信息
	 */
	public HashMap<String,DBRow[]> importCheckProduct(String filename,int checkType) 
		throws Exception
	{
		InputStream is = null;
		Workbook rwb = null; 
		HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
		try 
		{	
			String path = "";
			
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			if(checkType == UploadEditFileKey.CommodityData)
			{
				DBRow[] products = productData(rwb);//商品DBRow[]
				
				addImportProducts(products,filename);
				
				DBRow[] errorProducts = checkImportProduct(filename);
				resultMap.put("errorProducts",errorProducts);
			}
			else if(checkType == UploadEditFileKey.UnionRelation)
			{
				DBRow[] unions = unionData(rwb);//关系DBRow[]
				addImportProductUnions(unions,filename);
				
				DBRow[] errorUnions = checkImportProductUnion(filename);
				resultMap.put("errorUnions",errorUnions);
			}
			else if(checkType == UploadEditFileKey.ProductFile)
			{
				DBRow[] productFile = productFileData(rwb);
				
				DBRow[] errorProductFile = checkImportProductFile(productFile);
				
				resultMap.put("errorFileNoExits",errorProductFile);
			}	
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
		finally
		{
			is.close();
			rwb.close();
		}
		
		return resultMap;
	}
	
	
	
	/**
	 * 临时表导入商品
	 * @param importProducts
	 * @throws Exception
	 */
	public void addImportProducts(DBRow[] importProducts,String filename)
		throws Exception
	{
		for (int i = 0; i < importProducts.length; i++) 
		{
			importProducts[i].remove("catalog_name");//excel的分类名称，对数据库没用去掉
			importProducts[i].remove("union");//excel显示的商品拼装散件，对数据库没用去掉
			importProducts[i].remove("oldPrice");
			importProducts[i].remove("oldWeight");
			
			String p_code = importProducts[i].getString("p_code");
			String p_code2 = importProducts[i].getString("p_code2");
			String upc = importProducts[i].getString("upc");
			
			importProducts[i].remove("p_code");
			importProducts[i].remove("p_code2");
			importProducts[i].remove("upc");
			
			importProducts[i].add("filename",filename);
			importProducts[i].add("number",i+2);
			
			long pc_id = floorImportProductMgrZJ.addImportProduct(importProducts[i]);
			
			if(!p_code.trim().equals(""))
			{
				addImportProductCode(pc_id, p_code,CodeTypeKey.Main,filename);
			}
			
			if(!p_code2.trim().equals(""))
			{
				addImportProductCode(pc_id, p_code2,CodeTypeKey.Amazon,filename);
			}
			
			if(!upc.trim().equals(""))
			{
				addImportProductCode(pc_id,upc,CodeTypeKey.UPC,filename);
			}
		}
	}
	
	/**
	 * 添加临时导入条码
	 * @param pc_id
	 * @param p_code
	 * @param code_type
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public long addImportProductCode(long pc_id,String p_code,int code_type,String filename)
		throws Exception
	{
		try {
			DBRow importProductCode = new DBRow();
			
			importProductCode.add("pc_id",pc_id);
			importProductCode.add("p_code",p_code);
			importProductCode.add("code_type",code_type);
			importProductCode.add("filename",filename);
			
			long import_pcode_id = floorImportProductMgrZJ.addImportProductCode(importProductCode);
			
			return (import_pcode_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addImportProductCode",log);
		}
	}
	
	/**
	 * 临时表导入关系
	 * @param importProductUnions
	 * @param filename
	 * @throws Exception
	 */
	public void addImportProductUnions(DBRow[] importProductUnions,String filename)
		throws Exception
	{
		for (int i = 0; i < importProductUnions.length; i++) 
		{
			DBRow product = floorProductMgr.getDetailProductByPname(importProductUnions[i].getString("product"));
			if(product==null)
			{
				product = floorImportProductMgrZJ.getDetailTemporaryProductByPname(importProductUnions[i].getString("product"),filename);
				
				if(product==null)
				{
					product = new DBRow();
					if(importProductUnions[i].getString("product").trim().equals(""))
					{
						product.add("pc_id",-1l);
					}
					else
					{
						product.add("pc_id",0l);
					}
					
				}
			}
			
			DBRow accessories = floorProductMgr.getDetailProductByPname(importProductUnions[i].getString("accessories"));
			if(accessories==null)
			{
				accessories = floorImportProductMgrZJ.getDetailTemporaryProductByPname(importProductUnions[i].getString("accessories"),filename);
				
				if(accessories==null)
				{
					accessories = new DBRow();
					if(importProductUnions[i].getString("accessories").trim().equals(""))//若配件条码为空赋值-1与条码错误区分
					{
						accessories.add("pc_id",-1l);
					}
					else
					{
						accessories.add("pc_id",0l);
					}
					
				}
			}
			
			long set_pid = Long.parseLong(product.getString("pc_id"));
			long pid = Long.parseLong(accessories.getString("pc_id"));
			
			importProductUnions[i].add("set_pid",set_pid);
			importProductUnions[i].add("pid",pid);
			importProductUnions[i].add("number",i+2);
			importProductUnions[i].add("filename",filename);
			floorImportProductMgrZJ.addImportUnion(importProductUnions[i]);
		}
	}
	
	/**
	 * 检查导入商品错误
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkImportProduct(String filename)
		throws Exception
	{
		try 
		{
			DBRow[] errors = floorImportProductMgrZJ.checkImportProduct(filename);//视图检查出的商品错误
			
			ArrayList<DBRow> errorlist = new ArrayList<DBRow>();//整理好的错误信息集合
			HashMap<String,String> map = new HashMap<String,String>();
			DBRow error = new DBRow();
			for (int i = 0; i < errors.length; i++) 
			{
				if(map.containsKey(errors[i].getString("number")))//如果同一行有多个错误信息，错误信息用，分割组合
				{
					String errorMessage = error.getString("errorMessage");
					errorMessage +=","+errors[i].getString("errortype");
					error.add("errorMessage",errorMessage);
				}
				else
				{
					map.put(errors[i].getString("number"),errors[i].getString("pc_id"));
					if(i>0)
					{
						errorlist.add(error);
					}
					error = new DBRow();
					error.add("p_name",errors[i].getString("p_name"));
					
					DBRow[] importProductCodes = floorImportProductMgrZJ.getImportProductCodeByPcid(errors[i].get("pc_id",0l),filename); 
					
					for (int j = 0; j < importProductCodes.length; j++) 
					{
						if (importProductCodes[j].get("code_type",0)==CodeTypeKey.Main) 
						{
							error.add("p_code",importProductCodes[j].getString("p_code"));
						}
						
						if (importProductCodes[j].get("code_type",0)==CodeTypeKey.Amazon) 
						{
							error.add("p_code2",importProductCodes[j].getString("p_code"));
						}
						
						if (importProductCodes[j].get("code_type",0)==CodeTypeKey.UPC) 
						{
							error.add("upc",importProductCodes[j].getString("p_code"));
						}
					}
					error.add("errorProductRow",errors[i].getString("number"));
					error.add("errorMessage",errors[i].getString("errortype"));
				}
				//最后一条记录加入错误集合中
				if(i==errors.length-1)
				{
					errorlist.add(error);
				}
			}
			return errorlist.toArray(new DBRow[0]);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 检查导入关系错误
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkImportProductUnion(String filename)
		throws Exception
	{
		DBRow[] errors = floorImportProductMgrZJ.checkImportProductUnion(filename);
		
		ArrayList<DBRow> errorlist = new ArrayList<DBRow>();//整理好的错误信息集合
		HashMap<String,String> map = new HashMap<String,String>();
		DBRow error = new DBRow();
		//普通错误信息整合
		for (int i = 0; i < errors.length; i++) 
		{
			if(map.containsKey(errors[i].getString("number")))//如果同一行有多个错误信息，错误信息用，分割组合
			{
				String errorMessage = error.getString("errorMessage");
				errorMessage +=","+errors[i].getString("errortype");
				error.add("errorMessage",errorMessage);
			}
			else
			{
				map.put(errors[i].getString("number"),errors[i].getString("pc_id"));
				if(i>0)
				{
					errorlist.add(error);
				}
				error = new DBRow();
				error.add("product",errors[i].getString("product"));
				error.add("accessories",errors[i].getString("accessories"));
				error.add("errorPUnionRow",errors[i].getString("number"));
				error.add("errorMessage",errors[i].getString("errortype"));
			}
			if(i==errors.length-1)
			{
				errorlist.add(error);
			}
		}
		
		return errorlist.toArray(new DBRow[0]);
	}
	
	/**
	 * 检查上传文件
	 * @param productFiles
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkImportProductFile(DBRow[] productFiles)
		throws Exception
	{
		try {
			String url = Environment.getHome()+"/upload/product/";
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			for (int i = 0; i < productFiles.length; i++) 
			{
				File file = new File(url+productFiles[i].getString("file_name"));
				if(!file.exists())
				{
					errorList.add(productFiles[i]);
				}
			}
			
			return errorList.toArray(new DBRow[0]);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 根据Ipa_id获得商品导入商品
	 */
	public DBRow[] getImportProductByIpaId(long ipa_id)
		throws Exception
	{
		try 
		{
			return (floorImportProductMgrZJ.getImportProductByIpaId(ipa_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getImportProductByIpaId",log);
		}
	}
	
	/**
	 * 数据库检查形式导入商品
	 * @param filename
	 * @throws Exception
	 */
	public void importProductDB(String filename,long adid)
		throws Exception
	{
		addProductIndex = new ArrayList<DBRow>();
		updateProductIndex = new ArrayList<DBRow>();
		DBRow[] products = floorImportProductMgrZJ.getImportProductByFilename(filename);
		
		AdminLoginBean adminLoggerBean = new AdminLoginBean();
		adminLoggerBean.setAdid(adid);
		
		for (int i = 0; i < products.length; i++) 
		{
			
			long pc_id = Long.parseLong(products[i].getString("pc_id"));
			
			products[i].remove("number");
			products[i].remove("filename");
			products[i].remove("errortype");
			products[i].remove("ipa_id");
			
			float volume = products[i].get("length",0f)*products[i].get("width",0f)*products[i].get("heigth",0f);
			volume = volume/1000;			
			products[i].add("volume",volume);
			
			DBRow paraIndex = new DBRow();
			paraIndex.add("pc_id",products[i].getString("pc_id"));
			paraIndex.add("p_name",products[i].getString("p_name"));
			paraIndex.add("p_code",products[i].getString("p_code"));
			paraIndex.add("catalog_id",Long.parseLong(products[i].getString("catalog_id")));
			paraIndex.add("unit_name",products[i].getString("unit_name"));
			paraIndex.add("alive",Integer.parseInt(products[i].getString("alive")));
			
			DBRow oldProduct = floorProductMgr.getDetailProductByPcid(pc_id);
			
			if(oldProduct!=null)//修改商品
			{
				
				this.modProduct(pc_id,products[i],adminLoggerBean);
				
				updateProductIndex.add(paraIndex);
				
				
				
				//重新计算以此商品为配件的套装重量与价值
				productMgr.updateSetPriceAndWeightByPid(pc_id,oldProduct.get("unit_price",0d),Double.parseDouble(products[i].getString("unit_price")),oldProduct.get("weight",0f),Float.parseFloat(products[i].getString("weight")),adminLoggerBean);
			}
			else//添加商品
			{
				this.addProduct(pc_id, products[i], adminLoggerBean);
				addProductIndex.add(paraIndex);
				
				//把新商品在所有仓库建库
				CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
				DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
				for ( int j=0; j<treeRows.length; j++ )
				{
					if (treeRows[j].get("parentid",0)==0)
					{
						DBRow store = new DBRow();
						store.add("store_station","");
						store.add("post_date",DateUtil.NowStr());
						store.add("cid",treeRows[j].get("id",0));
						store.add("pc_id",pc_id);
						store.add("store_count",0);
						store.add("store_count_alert",0);
						floorProductMgr.addProductStorage(store);
					}
				}
			}
		}
		
		addIndex();//添加索引
		updateIndex();//修改索引
	}
	
	/**
	 * 根据IpaId获得导入套装关系
	 */
	public DBRow[] getImportUnionByIpaId(long ipa_id)
		throws Exception
	{
		try 
		{
			return (floorImportProductMgrZJ.getImportProductUnionSetPidByIpaId(ipa_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getImportUnionByIpaId",log);
		}
	}
	
	public DBRow[] getImportProductUnionByIpaIdWithSetpid(long ipa_id,long set_pid)
		throws Exception
	{
		try 
		{
			return floorImportProductMgrZJ.getImportProductUnionByIpaIdWithSetpid(ipa_id, set_pid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getImportProductUnionByIpaIdWithSetpid",log);
		}
	}
	/**
	 * 数据库检查导入关系
	 * @param filename
	 * @throws Exception
	 */
	public void importProductUnionDB(String filename,long adid)
		throws Exception
	{
		//取消文件内已有的套装关系
		DBRow[] set_pids = floorImportProductMgrZJ.getImportProductUnionSetPidByFilename(filename);
		for (int i = 0; i < set_pids.length; i++) 
		{
			long set_pid = set_pids[i].get("set_pid",0l);
			cancelProductUnion(set_pid);
			
			DBRow unionpara = new DBRow();
			unionpara.add("set_pid",set_pid);
			
			DBRow[] unions = floorImportProductMgrZJ.getImportProductUnionByFilenameWithSetpid(filename, set_pid);
			for (int j = 0; j < unions.length; j++) 
			{
				unionpara.add("pid",unions[j].getString("pid"));
				unionpara.add("quantity",unions[j].getString("quantity"));
				
				floorProductMgr.addProductUnion(unionpara);
			}
			
			//套装关系更新好后刷新下套装信息
			DBRow para = floorProductMgrZJ.UpdateSuitPara(set_pid);
			para.add("union_flag",1);

			floorProductMgr.modProduct(set_pid,para);
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(set_pid);
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAdid(adid);
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
		}
	}
	
	/**
	 * 数据库检查后导入
	 * @param request
	 * @throws Exception
	 */
	public void importDB(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String filename = StringUtil.getString(request,"tempfilename");
			int upload_type = StringUtil.getInt(request,"upload_type");
			
			if(upload_type == UploadEditFileKey.CommodityData)
			{
//				importProductDB(filename,request);//商品导入数据库
			}
			else if(upload_type == UploadEditFileKey.UnionRelation)
			{
//				importProductUnionDB(filename,request);//套装关系导入数据库
			}
			
			
			
			
			//delImportDB(filename);//清除商品、套装关心临时信息
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 清除导入检查时创建的数据
	 * @param filename
	 * @throws Exception
	 */
	public void delImportDB(String filename)
		throws Exception
	{
		try 
		{
			floorImportProductMgrZJ.delImportProduct(filename);//清除临时商品信息
			floorImportProductMgrZJ.delImportUnion(filename);//清楚临时套装关系
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 删除全部导入信息
	 * @throws Exception
	 */
	public void delAllImportDB()
		throws Exception
	{
		try 
		{
			floorImportProductMgrZJ.delAllImportDB();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void addedProductStorage(long ps_id)
		throws Exception
	{
		DBRow[] products = floorProductMgr.getAllProducts(null);
		
		long cid = ps_id;
		
		float store_count = 0f;
		
		for (int i = 0; i < products.length; i++) 
		{
			long pc_id = products[i].get("pc_id",0l);
			//检查同一个跟父下是否有相同商品
			DBRow productStorages[] = floorProductMgrZJ.getProductStorageByPcidPsid(ps_id, pc_id);
			if (productStorages.length>0)
			{
				continue;
			}
			else
			{
				DBRow row = new DBRow();
				row.add("post_date",DateUtil.NowStr());
				row.add("cid",cid);
				row.add("pc_id",pc_id);
				row.add("store_count",store_count);
				row.add("store_count_alert",0);
				

				long pid = floorProductMgr.addProductStorage(row);
			}
		}
		
	}
	
	/**
	 * 商品过滤
	 * @param catalog_id
	 * @param product_line_id
	 * @param union_flag
	 * @param pc
	 * @return
	 * @throws Exception
	 */
//	public DBRow[] filterProductOld(long catalog_id,String product_line_id,int union_flag,PageCtrl pc)
//		throws Exception
//	{
//		
//		String catalog_ids = "";
//		long search_id = 0;
//		String function_name = "";
//		if(product_line_id.contains("-p"))//选择的是产品线,产品线第一层
//		{
//			long pro_line_id = Long.parseLong(product_line_id.substring(0,product_line_id.indexOf("-")));
//			search_id = pro_line_id;
//			function_name = "product_line_child_list";
//			if(pro_line_id !=0)//为0则不查询子分类
//			{
//				DBRow catalogs = floorOrderProcessMgrTJH.getProductLineIds(pro_line_id);
//				catalog_ids = catalogs.getString("line_ids");
//			}
//			
//		}
//		else if(!product_line_id.contains("-p")&&!product_line_id.equals(""))//产品线下拉框第二层
//		{
//			if(Long.parseLong(product_line_id)!=0)
//			{
//				catalog_id = Long.parseLong(product_line_id);
//			}
//			
//		}
//		//选择产品线第二层，实际上选择的是商品分类
//		if(catalog_id!=0)
//		{
//			function_name = "product_catalog_child_list";
//			search_id = catalog_id;
//			DBRow ids = floorOrderProcessMgrTJH.getCatalogResultSet(catalog_id);
//			catalog_ids = ids.getString("catalog_ids");
//		}
//		
//		return (floorProductMgrZJ.getProductInCids(catalog_ids, union_flag, pc));
//	}
	
	public DBRow[] filterProduct(long catalog_id, long product_line_id, int union_flag, int product_file_types, int product_upload_status, PageCtrl pc)
		throws Exception
	{
		
//		if(product_line_id !=0&&catalog_id==0)//为0则不查询子分类
//		{
//			return floorProductMgrZJ.getProductByProductLineId(product_line_id, union_flag, pc);
//		}
			
		//选择产品线第二层，实际上选择的是商品分类
		return (floorProductMgrZJ.getProductInCids(catalog_id, product_line_id, union_flag, product_file_types, product_upload_status, pc));
	}
	
	/**
	 * 根据国家编码查询所属省份
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProvinceByCountryCode(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String country_code = StringUtil.getString(request,"country_code");
			
			return (floorProductMgrZJ.getProvinceByCountryCode(country_code));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProvinceByCountryCode",log);
		}
	}
	
	/************************************************************************************/
	
	private void nowLineAndTitleRelation(long lineId,DBRow[] titles) throws Exception{
		
		if(lineId != 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductLine(lineId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title_id",0L));
					row.put("tpl_product_line_id", lineId);
					//添加
					floorCatalogMgr.insertTitleProductLine(row);
				}
			}
		}
	}

	private void beforeLineAndTitleRelation(long lineId) throws Exception{
		
		if(lineId != 0){
			
			//查询产品线下产品分类与title的关系
			DBRow[] title1 = floorCatalogMgr.getTitleForLineCategoryRelation(lineId);
			//查询产品线与title的关系
			DBRow[] title2 = floorCatalogMgr.getTitleForLineByLineId(lineId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//删除不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					floorCatalogMgr.deleteTitleProductLine(lineId,one.get("title",0L));
				}
			}
		}
	}
	
	private void nowParentCategoryAndTitleRelation(long categoryId,DBRow[] titles) throws Exception{
		
		//如果存在分类
		if(categoryId != 0 && titles.length > 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductCatalog(categoryId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpc_title_id", one.get("title_id",0L));
					row.put("tpc_product_catalog_id",categoryId);
					//添加
					floorCatalogMgr.insertTitleProductCatalog(row);
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.nowParentCategoryAndTitleRelation(parentCategory.get("id",0L),titles);
			}
		}
	}
	
	private void beforeParentCategoryAndTitleRelation(long categoryId)throws Exception{
		
		//如果存在分类
		if(categoryId != 0){
			
			//查询此分类关联的title
			DBRow[] title1 = floorCatalogMgr.getTitleForCategory(categoryId);
			
			//查询原父分类下的分类关联的title//查询原父分类下商品关联的title
			DBRow[] title4 = floorCatalogMgr.getTitleForCategoryProductAndSubCatagory(categoryId);
			
			//对比title
			for(DBRow one:title4){
				
				for(DBRow two:title1){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			
			//删除不存在的title
			for(DBRow one:title1){
				
				if(one.get("exist",0)==0){
					
					floorCatalogMgr.deleteTitleProductCatalog(categoryId,one.get("title",-1L));
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.beforeParentCategoryAndTitleRelation(parentCategory.get("id",0L));
			}
		}
	}
	
	/**
	 * 修改商品分类后影响分类与title的关系和产品线与title的关系
	 * 
	 * 1.原分类以及父分类与title的关系[是否应该删除]
	 * 2.现分类以及父分类与title的关系[是否应该添加]
	 * 3.原产品线与title的关系[是否需要删除]
	 * 4.现产品线与title的关系[是否需要添加]
	*/
	@SuppressWarnings("unused")
	private void updateCategoryAndLineForTitleRelation(long pcId,long categoryId) throws Exception{
		
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		
		//1.原分类以及父分类与title的关系[是否应该删除]
		if(categoryId != 0){
			this.beforeParentCategoryAndTitleRelation(categoryId);
		}
		
		//2.现分类以及父分类与title的关系[是否应该添加]
		DBRow[] titles = floorProprietaryMgrZyj.getProudctHasTitleList(pcId);
		
		this.nowParentCategoryAndTitleRelation(product.get("catalog_id", 0L),titles);
		
		//3.原产品线与title的关系[是否需要删除]
		if(categoryId != 0){
			
			DBRow category = floorCatalogMgr.getProductCatalogById(categoryId);
			
			this.beforeLineAndTitleRelation(category.get("product_line_id",0L));
		}
		
		//4.现产品线与title的关系[是否需要添加]
		DBRow catalog = floorCatalogMgr.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(catalog != null ){
			
			this.nowLineAndTitleRelation(catalog.get("product_line_id",0L), titles);
		}
	}
	
	/************************************************************************************/
	
	/**
	 * 修改商品分类后
	 * 
	 * 1.原分类以及父分类与customer/title的关系[是否应该删除]
	 * 2.现分类以及父分类与customer/title的关系[是否应该添加]
	 * 3.原产品线与customer/title的关系[是否需要删除]
	 * 4.现产品线与customer/title的关系[是否需要添加]
	*/
	public void updateCustomerTitleAndCategoryLineRelation(long pcId,long categoryId) throws Exception{
		
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		
		//1.原分类以及父分类与title的关系[是否应该删除]
		if(categoryId != 0){
			proprietaryMgrZyj.customerTitleAndCategory(categoryId);
		}
		
		//2.现分类以及父分类与title的关系[是否应该添加]
		proprietaryMgrZyj.customerTitleAndCategory(product.get("catalog_id", 0L));
		
		//3.原产品线与title的关系[是否需要删除]
		if(categoryId != 0){
			
			DBRow category = floorCatalogMgr.getProductCatalogById(categoryId);
			
			proprietaryMgrZyj.customerTitleAndLine(category.get("product_line_id",0L));
		}
		
		//4.现产品线与title的关系[是否需要添加]
		DBRow catalog = floorCatalogMgr.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(catalog != null ){
			
			proprietaryMgrZyj.customerTitleAndLine(catalog.get("product_line_id",0L));
		}
	}
	
	/************************************************************************************/
	
	/**
	 * 基础数据>>商品>>修改商品分类
	 * 
	 * @author subin
	 */
	public DBRow modProductCatalog( HttpServletRequest request) throws Exception {
		
		try {
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
			
			DBRow result = new DBRow();
			
			long pc_id = StringUtil.getLong(request,"pc_id");
			long catalog_id = StringUtil.getLong(request,"catalog_id");
			
			//维护title关系
			StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
			
			//验证此商品分类是否是最后一级
			DBRow[] subRow = floorCatalogMgr.getProductCatalogByParentId(catalog_id, null);
			
			if(subRow != null && subRow.length > 0){
				
				result.add("category_id", catalog_id);
				result.add("html",catalogText.toString());
				result.add("status", "error");
				
				return result;
			}
			
			DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pc_id);
			
			//修改商品分类等
			this.modProductCatalogSub(pc_id,catalog_id);
			
			/*******************************************************************************************************/
			/** 
			 * 创建的时候, 在创建完成后执行此方法
			 * 更新的时候, 在更新前执行此方法
			 */
			DBRow logRow = new DBRow();
			//create/update/delete
			logRow.put("event", "update");
			//更新的表名
			logRow.put("table", "product");
			//主键字段名
			logRow.put("column", "pc_id");
			//主键ID
			logRow.put("pri_key", pc_id);
			//依赖表
			logRow.put("ref_table", "");
			//依赖表主键字段名
			logRow.put("ref_column", "");
			//依赖表主键ID
			logRow.put("ref_pri_key", "");
			//账号ID
			logRow.put("create_by", adminLoggerBean.getAdid());
			//设备类型//请求的来源
			logRow.put("device", ProductDeviceTypeKey.Web);
			//描述
			logRow.put("describe", "update product "+pc_id);
			
			//具体更新的字段内容
			//将DBRow转换为数组
			DBRow catRow = new DBRow();
			catRow.put("catalog_id", catalog_id);
			
			DBRow[] content = DBRowUtils.dbRowConvertToArray(catRow);
			logRow.put("content", content);
			
			floorSetupLogMgr.addSetupLog(logRow);
			
			/*******************************************************************************************************/
			
			//修改商品分类后影响分类与title的关系和产品线与title的关系
			this.updateCustomerTitleAndCategoryLineRelation(pc_id,product.get("catalog_id",0L));
			
			if( 0 != catalog_id ){
				
				Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
				
				DBRow catalog = catalogMgr.getDetailProductCatalogById(catalog_id);
			  	
				if(null != catalog) {
					
			  		DBRow allFather[] = tree.getAllFather(catalog_id);
				  	
			  		for (int jj=0; jj<allFather.length-1; jj++) {
			  			
				  		catalogText.append("<img src='img/folderopen.gif'/><a class='nine4' href='javascript:afilter("+allFather[jj].getString("id")+")'>"+allFather[jj].getString("title")+"</a><br/>");
				  		String s = (jj==0) ? "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
				  		catalogText.append(s);
				  	}
				 	catalogText.append("<img  src='img/page.gif'/><a href='javascript:afilter("+catalog_id+")'>"+catalog.getString("title")+"</a>");
			  	} 
			  	
				result.add("category_id", catalog_id);
				result.add("html",catalogText.toString());
				result.add("status", "success");
				
			} else {
				
				result.add("category_id", 0);
				result.add("html",catalogText.append("</span>").toString());
				result.add("status", "success");
			}
			
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("pc_id", pc_id);
			Map<String,Object> productNode = new HashMap<String, Object>();
			productNode.put("pc_id", pc_id);
			productNode.put("catalog_id",catalog_id);
			productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//修改商品节点
			
			return result;
			
		} catch (Exception e) {
			throw new SystemException(e,"modProductCatalog",log);
		}
	}
	
	//修改商品分类等
	public void modProductCatalogSub(long pc_id,long catalog_id) throws Exception{
		
		try {
			
			floorProductMgr.modProductCatalog(pc_id, catalog_id);
			
		} catch (Exception e) {
			
			throw new SystemException(e,"modProductCatalogSub",log);
		}
	}
	
	public DBRow[] getDetailProductLikeSearch(String key,PageCtrl pc)
		throws Exception
	{
		return floorProductMgrZJ.getDetailProductLikeSearch(key, pc);
	}
	
	public DBRow[] getLackingProductByPsid(long[] pids)
		throws Exception
	{
		ArrayList<DBRow> lackingList = new ArrayList<DBRow>();
		
		HashMap<String,DBRow> lackingProductMap = new HashMap<String,DBRow>();
		
		for (int i = 0; i < pids.length; i++) 
		{
			DBRow productStore = floorProductMgr.getDetailProductStorageByPid(pids[i]);
			
			long pc_id = productStore.get("pc_id",0l);
			long ps_id = productStore.get("cid",0l);
			float count = productStore.get("store_count",0f);
			
			if(count>0)//库存数量大于不算
			{
				continue;
			}
			
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			if(product.get("union_flag",0)==1)
			{
				DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(product.get("pc_id",0l));
				for (int j=0; j<productsInSet.length; j++)
				{
					float productsInSetCount = productsInSet[j].get("quantity", 0f)*Math.abs(count);
					DBRow productStorage = floorProductMgr.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
					if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
					{
						DBRow lackingProduct;
						if(lackingProductMap.containsKey(productStorage.getString("pc_id")))
						{
							lackingProduct = lackingProductMap.get(productStorage.getString("pc_id"));
							
							float lackingCount = lackingProduct.get("lackingCount",0f);
							
							lackingCount += productsInSetCount;
							
							lackingProduct.add("lackingCount",lackingCount);
						}
						else
						{
							DBRow productNormal = floorProductMgr.getDetailProductByPcid(productStorage.get("pc_id",0l));
							
							lackingProduct = new DBRow();
							lackingProduct.add("pc_id",productNormal.get("pc_id",0l));
							lackingProduct.add("p_name",productNormal.getString("p_name"));
							lackingProduct.add("p_code",productNormal.getString("p_code"));
							lackingProduct.add("lackingCount",Math.abs(productStorage.get("store_count",0f)-productsInSetCount));
						}
						
						lackingProductMap.put(productStorage.getString("pc_id"),lackingProduct);
					}
				}
			}
			else
			{
				synchronized(String.valueOf(ps_id)+"_"+pc_id)
				{
					//检查库存
					//orderIsLacking
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,pc_id);
					float needCount = Math.abs(count);
					if (detailPro==null||detailPro.get("store_count", 0f)<needCount)
					{
						DBRow lackingProduct;
						if(lackingProductMap.containsKey(product.getString("pc_id")))
						{
							lackingProduct = lackingProductMap.get(String.valueOf(pc_id));
								
							float lackingCount = lackingProduct.get("lackingCount",0f);
								
							lackingCount += needCount;
								
							lackingProduct.add("lackingCount",lackingCount);
						}
						else
						{
							lackingProduct = new DBRow();
							lackingProduct.add("pc_id",product.get("pc_id",0l));
							lackingProduct.add("p_name",product.getString("p_name"));
							lackingProduct.add("p_code",product.getString("p_code"));
							lackingProduct.add("lackingCount",Math.abs(detailPro.get("store_count", 0f)-needCount));
						}
						
						lackingProductMap.put(String.valueOf(pc_id),lackingProduct);
					}
				}
			}
		}
		
		for(String key:lackingProductMap.keySet())
		{
			lackingList.add(lackingProductMap.get(key));
		}
		
		return lackingList.toArray(new DBRow[0]);
	}
	             
	/**
	 * 获得散件式缺货
	 * @param pc_id
	 * @param ps_id
	 * @param count
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingProduct(long pc_id,long ps_id,float count)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> lackingList = new ArrayList<DBRow>();
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			if(product.get("union_flag",0)==1)
			{
				DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(product.get("pc_id",0l));
				for (int j=0; j<productsInSet.length; j++)
				{
					float productsInSetCount = productsInSet[j].get("quantity", 0f)*count;
					DBRow productStorage = floorProductMgr.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
					if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
					{
						DBRow productNormal = floorProductMgr.getDetailProductByPcid(productStorage.get("pc_id",0l));
						
						DBRow lackingProduct = new DBRow();
						lackingProduct.add("pc_id",productNormal.get("pc_id",0l));
						lackingProduct.add("p_name",productNormal.getString("p_name"));
						lackingProduct.add("p_code",productNormal.getString("p_code"));
						lackingProduct.add("lackingCount",Math.abs(productStorage.get("store_count",0f)-productsInSetCount));
						
//						if(productStorage.get("store_count",0f)>0)//商品库存非负数
//						{
//							
//						}
//						else
//						{
//							lackingProduct.add("lackingCount",productsInSetCount-productStorage.get("store_count",0f));
//						}
						
						lackingList.add(lackingProduct);
					}
				}
			}
			else
			{
				synchronized(String.valueOf(ps_id)+"_"+pc_id)
				{
					//检查库存
					//orderIsLacking
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,pc_id);
					
					if (detailPro==null)	//	商品还没进库，当然缺货
					{
						DBRow lackingProduct = new DBRow();
						lackingProduct.add("pc_id",product.get("pc_id",0l));
					}
					else
					{
						if (detailPro.get("store_count", 0f)<count)
						{
							DBRow lackingProduct = new DBRow();
							lackingProduct.add("pc_id",product.get("pc_id",0l));
							lackingProduct.add("p_name",product.getString("p_name"));
							lackingProduct.add("p_code",product.getString("p_code"));
							lackingProduct.add("lackingCount",count);
							lackingList.add(lackingProduct);
						}
					}
				}
			}
			
			return lackingList.toArray(new DBRow[0]);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getLackingProduct",log);
		}
	}
	
	/**
	 * 获得拆货建议商品
	 * @param pc_ids
	 * @return
	 * @throws Exception
	 */
	public DBRow[] splitAdviceProduct(long[] pc_ids,long ps_id,long waybill_id)
		throws Exception
	{
		try 
		{
			return (floorProductMgrZJ.getProductByPids(pc_ids,ps_id,waybill_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"splitAdviceProduct",log);
		}
	}
	
	/**
	 * 获得不完整套装建议商品
	 * @param pc_ids
	 * @param ps_id
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] splitAdviceBackUpProduct(long[] pc_ids,long ps_id,long waybill_id)
		throws Exception
	{
		try 
		{
			return (floorProductMgrZJ.getBackUpProductByPids(pc_ids, ps_id, waybill_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"splitAdviceBackUpProduct",log);
		}
	}
	
	/**
	 * 建议拆散商品包含需要散件
	 * @param pc_ids
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] adviceNeedProductUnion(long [] pc_ids,long set_pid)
		throws Exception
	{
		ArrayList<DBRow> returnList = new ArrayList<DBRow>();
		
		for (int i = 0; i < pc_ids.length; i++) 
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_ids[i]);
			DBRow productUnion = floorProductMgr.getDetailProductCustomUnionBySetPidPid(set_pid, pc_ids[i]);
			productUnion.add("p_name",product.getString("p_name"));
			
			returnList.add(productUnion);
		}
		
		return returnList.toArray(new DBRow[0]);
	}
	
	public void returnProduct(HttpServletRequest request)
		throws Exception
	{
		String return_union_flag  = StringUtil.getString(request,"return_union_flag");
		
		String[] pcids = request.getParameterValues(return_union_flag+"_pc_id");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		long oid = StringUtil.getLong(request,"oid");
		
		AdminMgr am = new AdminMgr();
		AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
		
		long batch_id = 0;
		for (int i = 0; i < pcids.length; i++) 
		{
			long pc_id = Long.parseLong(pcids[i]);
			float quality_nor = StringUtil.getFloat(request,pcids[i]+"_nor");
			float quality_damage = StringUtil.getFloat(request,pcids[i]+"_damage");
			float quality_package_damage = StringUtil.getFloat(request,pcids[i]+"_package_damage");
			
			if (quality_nor>0)
			{
				ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
				inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				if(oid>0)
				{
					inProductStoreLogBean.setOid(oid);
					inProductStoreLogBean.setBill_type(ProductStoreBillKey.ORDER);
				}
				
				inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RETURN);
				
				floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,ps_id,pc_id, quality_nor,0);
			}
			//功能残损进库
			if (quality_damage>0)
			{
				//增加残损件数目
				floorProductMgr.incProductDamagedCountByPcid(ps_id,pc_id,quality_damage);
				
				//记录残损件入库日志
				ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
				inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				if(oid>0)
				{
					inProductDamagedCountLogBean.setOid(oid);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.ORDER);
				}
				
				inProductDamagedCountLogBean.setPs_id(ps_id);
				inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
				inProductDamagedCountLogBean.setQuantity(quality_damage);
				inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				inProductDamagedCountLogBean.setPc_id(pc_id);
				productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
			}
			//包装残损进库
			if (quality_package_damage>0)
			{
				//增加残损件数目
				floorProductMgr.incProductPackageDamagedCountByPcid(ps_id,pc_id,quality_package_damage);
				
				ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
				inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				if(oid>0)
				{
					inProductDamagedCountLogBean.setOid(oid);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.ORDER);
				}
				inProductDamagedCountLogBean.setPs_id(ps_id);
				inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
				inProductDamagedCountLogBean.setQuantity(quality_package_damage);
				inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				inProductDamagedCountLogBean.setPc_id(pc_id);
				productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
			}
		}
	}
	
	/**
	 * 修改商品，适应新的条码结构（包含条码时，修改条码与最后商品更新记录商品日志）
	 * @param product
	 * @throws Exception
	 */
	public void modProduct(long pc_id,DBRow product,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			if(!product.getString("p_code").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("p_code"),CodeTypeKey.Main,adminLoggerBean);
				
				product.remove("p_code");
			}
			
			if(!product.getString("p_code2").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("p_code2"),CodeTypeKey.Amazon,adminLoggerBean);
				product.remove("p_code2");
			}
			
			if(!product.getString("upc").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("upc"),CodeTypeKey.UPC,adminLoggerBean);
				product.remove("upc");
			}
			
			product.remove("union_flag");
			product.remove("volume");
			floorProductMgr.modProduct(pc_id,product);
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modProduct",log);
		}
	}
	
	/**
	 * 添加商品，适应新的条码结构
	 * @param pc_id
	 * @param product
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addProduct(long pc_id,DBRow product,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try {
			if(!product.getString("p_code").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("p_code"),CodeTypeKey.Main,adminLoggerBean);
				
				product.remove("p_code");
			}
			
			if(!product.getString("p_code2").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("p_code2"),CodeTypeKey.Amazon,adminLoggerBean);
				product.remove("p_code2");
			}
			
			if(!product.getString("upc").equals(""))
			{
				productCodeMgrZJ.addProductCodeSubNoLog(pc_id,product.getString("upc"),CodeTypeKey.UPC,adminLoggerBean);
				product.remove("upc");
			}
			
			floorProductMgrZJ.addProductForDB(product);
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.ADD);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addProduct",log);
		}
	}
	
	/**
	 * 申请上传文件审核
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addImportProductApprove(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String filename = StringUtil.getString(request,"tempfilename");
			int upload_type = StringUtil.getInt(request,"upload_type");
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			long ipa_id = 0;
			if(upload_type ==UploadEditFileKey.ProductFile)//上传的是商品文件，不创建审核
			{	
				this.addProductFile(filename);
			}
			else//商品基础数据或套装关系需要创建审核
			{
				DBRow importProductApprove = new DBRow();
				importProductApprove.add("import_adid",adminLoggerBean.getAdid());
				importProductApprove.add("import_date",DateUtil.NowStr());
				importProductApprove.add("filename",filename);
				importProductApprove.add("upload_type",upload_type);
				
				ipa_id = floorImportProductMgrZJ.addImportProductApprove(importProductApprove);
				
				floorImportProductMgrZJ.hasProductApprove(filename,ipa_id);
			}
			
			
			return (ipa_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addImportProductApprove",log);
		}
	}
	
	private void addProductFile(String filename)
		throws Exception
	{
		try {
			InputStream is = null;
			Workbook rwb = null; 
			String path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			DBRow[] productFiles = this.productFileData(rwb);
			
			for (int i = 0; i < productFiles.length; i++) 
			{
				if(i==0||productFiles[i].get("pc_id",0l)==productFiles[i-1].get("pc_id",0l))
				{
					floorProductMgrZJ.delProductFileByPcid(productFiles[i].get("pc_id",0l));
				}
			}
			
			
			for (int i = 0; i < productFiles.length; i++)
			{	
				productFiles[i].remove("p_name");
				productFiles[i].remove("number");
				floorProductMgrZJ.addProductFile(productFiles[i]);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}		
	}
	
	/**
	 * 过滤导入文件审核
	 * @param import_adid
	 * @param approve_adid
	 * @param sorttype
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterImportProductApprove(long import_adid,long approve_adid,String sort_colume,String sort_type,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorImportProductMgrZJ.filterImportProductApprove(import_adid,approve_adid,sort_colume,sort_type,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterImportProductApprove",log);
		}
	}
	
	/**
	 * 根据商品Id获得商品文件
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductFileByPcid(long pc_id, boolean isBarrierPurchase, boolean isBarrierTransport)
		throws Exception
	{
		try 
		{
			return floorProductMgrZJ.getProductFileByPcid(pc_id, isBarrierPurchase, isBarrierTransport);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 获得缺货商品
	 * @param ps_id
	 * @param product_line_id
	 * @param catalog_id
	 * @param status
	 * @param p_name
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingProductByPs(long ps_id,long product_line_id,long catalog_id,int[] status,String p_name,PageCtrl pc)
		throws Exception
	{
		try 
		{
			long pc_id = 0;
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product!=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return floorProductMgrZJ.getLackingProductByPs(ps_id, product_line_id, catalog_id, status, pc_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLackingProductByPs",log);
		}
	}
	
	/**
	 * 从订单拆套装
	 * @param request
	 * @throws Exception
	 */
	public void splitProductForWaybill(HttpServletRequest request)
		throws Exception
	{
		long waybill_id = StringUtil.getLong(request,"waybill_id");
		String store_type = StringUtil.getString(request,"store_type");
		long split_union_pc_id = StringUtil.getLong(request,"split_union_pc_id");//拆散套装ID
		DBRow original_product = floorProductMgr.getDetailProductByPcid(split_union_pc_id);
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		
		String[] usePcId = request.getParameterValues("use_pc_id");
		
		Map<String,String> productUnion = new HashMap<String, String>();
		
		//记录拆分要求
		for (int i = 0; i < usePcId.length; i++) 
		{
			float use_quantity = StringUtil.getFloat(request,"use_quantity_"+usePcId[i]);
			
			DBRow splitRequireDetail = new DBRow();
			splitRequireDetail.add("waybill_id",waybill_id);
			splitRequireDetail.add("need_pc_id",usePcId[i]);
			splitRequireDetail.add("need_quantity",use_quantity);
			splitRequireDetail.add("split_pc_id",split_union_pc_id);
			splitRequireDetail.add("post_date",DateUtil.NowStr());
			splitRequireDetail.add("post_adid",adminLoggerBean.getAdid());
			
			floorBackUpStorageMgrZJ.addSplitRequireDetail(splitRequireDetail);
		}
		
		/**
		 * 开始释放库存
		 */
		//减少主套装数量
		ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
		inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
		inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
		inProductStoreLogBean.setOid(0);
		inProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SPLIT_UNION_STANDARD);
		long from_psl_id = 0;
		if(store_type.equals("store"))
		{
			from_psl_id = floorProductMgr.deIncProductStoreCountByPcid(inProductStoreLogBean, ps_id,split_union_pc_id,1);
		}
		else if (store_type.equals("back_up_store"))
		{
			from_psl_id = backUpStorageMgrZJ.deIncBackUpProductStoreCountByPcid(inProductStoreLogBean, ps_id, split_union_pc_id, 1);
		}
		
		//增加散件进库
		DBRow inSetProducts[] = floorProductMgr.getProductsInSetBySetPid(split_union_pc_id);
		for (int i=0; i<inSetProducts.length; i++)
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(inSetProducts[i].get("pc_id", 0l));
			
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(0);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL);
//			deInProductStoreLogBean.setPurchase_unit_price(product.get("unit_price",0d));
			
			floorProductMgr.incProductStoreCountByPcid(deInProductStoreLogBean, ps_id, inSetProducts[i].get("pc_id", 0l),inSetProducts[i].get("quantity", 0f),from_psl_id);
			
			productUnion.put(inSetProducts[i].getString("pc_id"),inSetProducts[i].getString("quantity"));
			
		}
		/**
		 * 释放库存结束
		 */
		

		/**
		 * 构建拆散不完整套装的套装关系
		 */
		for (int i = 0; i < usePcId.length; i++) 
		{
			float use_quantity = StringUtil.getFloat(request,"use_quantity_"+usePcId[i]);
			
			float quantity = Float.parseFloat(productUnion.get(usePcId[i]));
			
			quantity = quantity - use_quantity;
			
			if(quantity>0)
			{
				productUnion.put(usePcId[i],String.valueOf(quantity));
			}
			else
			{
				productUnion.remove(usePcId[i]);
			}
		}
		
		Set<String> productUnionSet = productUnion.keySet();
		
		if (productUnionSet.size()>0) 
		{
			DBRow productCustom = new DBRow();
			productCustom.add("unit_name",original_product.getString("unit_name"));
			productCustom.add("length",original_product.get("length",0f));
			productCustom.add("width",original_product.get("width",0f));
			productCustom.add("heigth",original_product.get("heigth",0f));
			productCustom.add("orignal_pc_id",split_union_pc_id);
			productCustom.add("p_name",original_product.getString("p_name")+" ▲");
			productCustom.add("catalog_id",original_product.get("catalog_id", 0l));
			
			long pc_pc_id = floorProductMgr.addProductCustom(productCustom);
			
			//修改拆货要求里的结果商品ID
			DBRow splitRequireDetailPara = new DBRow();
			splitRequireDetailPara.add("to_pc_id",pc_pc_id);
			floorBackUpStorageMgrZJ.modSplitRequireDetail(split_union_pc_id,splitRequireDetailPara);
			
			//修改文件名为商品ID以区分
			DBRow pnamePara = new DBRow();
			pnamePara.add("p_name",pc_pc_id);
			floorProductMgr.modProduct(pc_pc_id,pnamePara);
			
			for (String pcid : productUnionSet) 
			{
				DBRow customPro = new DBRow();
				customPro.add("set_pid",pc_pc_id);
				customPro.add("pid",pcid);
				customPro.add("quantity",productUnion.get(pcid));
				floorProductMgr.addProductCustomUnion(customPro);
			}
			//非完整套装创建库存及拼装
			backUpStorageMgrZJ.addBackUpStore(pc_pc_id, ps_id, adminLoggerBean);
		}
		
	}
	
	public void initVolume()
		throws Exception
	{
		DBRow[] products = floorProductMgr.getAllProducts(null);
		
		float volume = 0;
		for (int i = 0; i < products.length; i++) 
		{
			volume = products[i].get("length",0f)*products[i].get("width",0f)*products[i].get("heigth",0f)/1000;
			
			DBRow para = new DBRow();
			para.add("volume",volume);
			floorProductMgr.modProduct(products[i].get("pc_id",0l),para);
		}
	}
	
	public void updatePairToItem()
		throws Exception
	{
		DBRow[] product = floorProductMgrZJ.getProductByUnitName("PAIR");
		
		for (int i = 0; i < product.length; i++) 
		{
			////system.out.println(product[i].getString("p_name")+"|"+i+"/"+product.length);
			long pc_id = product[i].get("pc_id",0l); 
			//采购单详细
			floorProductMgrZJ.executeSQL("update purchase_detail set purchase_count = purchase_count*2,reap_count = reap_count*2,backup_count = backup_count*2,order_count = order_count*2,price = price/2 where product_id = "+pc_id);
			//采购单审核
			floorProductMgrZJ.executeSQL("update purchase_detail_difference set purchase_count = purchase_count*2,reap_count = reap_count*2 where product_id = "+pc_id);
			//退货明细
			floorProductMgrZJ.executeSQL("update return_product_items set quantity = quantity*2,unit_name ='ITEM'  where pid = "+pc_id);
			//退货明细提交
			floorProductMgrZJ.executeSQL("update return_product_sub_items set quantity = quantity*2,unit_name ='ITEM',quality_nor = quality_nor*2,quality_damage = quality_damage*2,quality_package_damage = quality_package_damage*2  where pid = "+pc_id);
			//转运单明细
			floorProductMgrZJ.executeSQL("update transport_detail set transport_delivery_count = transport_delivery_count*2,transport_count = transport_count*2,transport_send_count = transport_send_count*2,transport_reap_count = transport_reap_count*2  where transport_pc_id = "+pc_id);
			//转运审核明细
			floorProductMgrZJ.executeSQL("update transport_approve_detail set transport_count = transport_count*2,transport_reap_count = transport_reap_count*2 where product_id = "+pc_id);
			//运单明细
			floorProductMgrZJ.executeSQL("update waybill_order_item set quantity = quantity*2,unit_name='ITEM' where pc_id = "+pc_id);
			//套装组合关系
			floorProductMgrZJ.executeSQL("update product_union set quantity = quantity*2 where pid = "+pc_id);
			//订单明细
			floorProductMgrZJ.executeSQL("update porder_item  set quantity = quantity*2,unit_name='ITEM' where pid = "+pc_id);
			//报价明细
			floorProductMgrZJ.executeSQL("update quote_order_item set quantity = quantity*2,unit_name='ITEM' where pid = "+pc_id);
			//账单明细
			floorProductMgrZJ.executeSQL("update bill_order_item set quantity = quantity*2,unit_name='ITEM',unit_price = unit_price/2 where pc_id = "+pc_id);
			//退货理由
			floorProductMgrZJ.executeSQL("update return_product_reason set quantity = quantity*2 where pc_id = "+pc_id);
			//库存
			String sql = "update product_storage set store_count = store_count*2,damaged_count = damaged_count*2,damaged_package_count = damaged_package_count*2 where pc_id ="+pc_id;
			
			floorProductMgrZJ.executeSQL(sql);
			////system.out.println(sql);
			log.error(sql);
			//商品本身
			floorProductMgrZJ.executeSQL("update product set unit_name = 'ITEM',unit_price = unit_price/2,weight = weight/2,width = width/2 where pc_id = "+pc_id);
			//重新依据长宽高计算体积
			floorProductMgrZJ.executeSQL("update product set volume = length*width*heigth/1000  where pc_id = "+pc_id);
		}
		
		////system.out.println("PAIR to ITEM OK");
	}

	@Override
	public void updateProductByDBRow(DBRow row, long id) throws Exception {
		try{
			floorProductMgrZJ.updateProduct(row, id);
 		}catch(Exception e){
 			throw new SystemException(e,"getLackingProductByPs",log);

		}
	}
	
	/**
	 * 查询套装商品所含的子商品数量
	 * @param set_pid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月7日 下午12:20:27
	 */
	 public int getProductUnionCountByPid(long set_pid) throws Exception
	 {
		 try{
			 return floorProductMgr.getProductUnionCountByPid(set_pid);
	 		}catch(Exception e){
	 			throw new SystemException(e,"getProductUnionCountByPid",log);

			}
	 }
}
