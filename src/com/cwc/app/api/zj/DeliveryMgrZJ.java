package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.iface.zj.DeliveryApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.key.DeliveryOrderKey;
import com.cwc.app.key.DeliveryOrderSaveKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.lucene.zr.DeliveryOrderIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class DeliveryMgrZJ implements DeliveryMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorDeliveryMgrZJ floorDeliveryMgrZJ;
	private FloorPurchaseMgr floorPurchaseMgr;
	private ProductMgr productMgr;
	private FloorProductMgr floorProductMgr;
	private PurchaseIFace purchaseMgr;
	private DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ;
	private FloorCatalogMgr floorCatalogMgr;
	
	/**
	 * 生成交货单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryOrder(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long delivery_purchase_id = StringUtil.getLong(request,"purchase_id");
			String filename = StringUtil.getString(request,"filename");
			
			String waybill_number = StringUtil.getString(request,"waybill_number");
			String waybill_name = StringUtil.getString(request,"waybill_name");
			String arrival_eta = StringUtil.getString(request,"arrival_eta");
			
			
			
			
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(delivery_purchase_id);
			
			long delivery_supplier_id;
			try 
			{
				delivery_supplier_id = Long.parseLong(purchase.getString("supplier"));
			}
			catch (NumberFormatException e)
			{
				delivery_supplier_id = 0;
			}
			//StrUtil.getLong(request,"supplier_id");
			
			DBRow[] delivery_orders = floorDeliveryMgrZJ.getDelveryOrdersByPurchaseId(delivery_purchase_id);
			String numbers = delivery_orders[delivery_orders.length-1].getString("delivery_order_number").split("-")[1];
			int number = Integer.parseInt(numbers);
			
			DBRow delivery_order = new DBRow();
			delivery_order.add("delivery_purchase_id", delivery_purchase_id);
			delivery_order.add("delivery_supplier_id",delivery_supplier_id);
			delivery_order.add("delivery_address",purchase.getString("delivery_address"));
			delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
			delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
			delivery_order.add("delivery_order_number","P"+delivery_purchase_id+"-"+number);
			delivery_order.add("delivery_date",DateUtil.NowStr());
			delivery_order.add("waybill_number",waybill_number);
			delivery_order.add("waybill_name",waybill_name);
			delivery_order.add("arrival_eta",arrival_eta);
			delivery_order.add("delivery_creat_account_id",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());
			delivery_order.add("delivery_creat_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());
			
			
			long delivery_order_id = floorDeliveryMgrZJ.addDelveryOrder(delivery_order);//创建交货单
			
			/**
			 * 根据采购单未到货创建交货单详细
			 */
			DBRow[] needDelivery;
			
			if(filename.equals(""))
			{
				needDelivery = floorPurchaseMgr.checkPurchaseStats(delivery_purchase_id);
			}
			else
			{
				needDelivery = excelShow(filename);
			}
				
				
			
			for (int i = 0; i < needDelivery.length; i++)
			{
				DBRow dbrow = new DBRow();//needDelivery[i];
				dbrow.add("product_id",needDelivery[i].get("product_id",0l));
				
				if(filename.equals(""))
				{
					dbrow.add("product_name",needDelivery[i].getString("purchase_name"));
					dbrow.add("product_barcode",needDelivery[i].getString("proudct_barcod"));
					dbrow.add("delivery_count",(needDelivery[i].get("purchase_count",0f)-needDelivery[i].get("reap_count",0f)));
				}
				else
				{
					dbrow.add("product_name",needDelivery[i].getString("product_name"));
					dbrow.add("product_barcode",needDelivery[i].getString("product_barcode"));
					dbrow.add("delivery_count",needDelivery[i].getString("delivery_count"));
					dbrow.add("delivery_box",needDelivery[i].getString("delivery_box"));
				}
				
				dbrow.add("delivery_reap_count",0f);
				dbrow.add("delivery_order_id",delivery_order_id);
				
				floorDeliveryMgrZJ.addDeliveryOrderDetail(dbrow);
			}
			
			/**
			 *修改采购单为交货中状态 
			 */
//			DBRow para = new DBRow();
//			para.add("purchase_status",PurchaseKey.DELIVERYING);
//			floorPurchaseMgr.updatePurchase(delivery_purchase_id, para);
			
			return (delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public long createDeliveryOrder(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long delivery_purchase_id = StringUtil.getLong(request,"purchase_id");
			
			//获得对应采购单的未保存的交货单
			DBRow[] noSaveDeliveryOrder = floorDeliveryMgrZJ.getNoSaveDeliveryOrderByPurchaseID(delivery_purchase_id);
			
			for (int i = 0; i < noSaveDeliveryOrder.length; i++) 
			{
				long delivery_order_id = noSaveDeliveryOrder[i].get("delivery_order_id",0l);
				floorDeliveryMgrZJ.delDeliveryOrder(delivery_order_id);//删除交货单
				floorDeliveryMgrZJ.delDeliveryOrderDetailByDeliveryOrderId(delivery_order_id);//删除交货单的
			}
			
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(delivery_purchase_id);
			
			DBRow productStorageCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(purchase.get("ps_id",0l));
			
			long delivery_supplier_id;
			try 
			{
				delivery_supplier_id = Long.parseLong(purchase.getString("supplier"));
			}
			catch (NumberFormatException e)
			{
				delivery_supplier_id = 0;
			}
			//StrUtil.getLong(request,"supplier_id");
			
			DBRow[] delivery_orders = floorDeliveryMgrZJ.getDelveryOrdersByPurchaseId(delivery_purchase_id);
			
			
			int number;
			
			try 
			{
				String numbers = delivery_orders[delivery_orders.length-1].getString("delivery_order_number").split("-")[1];
				number = Integer.parseInt(numbers);
			} 
			catch (ArrayIndexOutOfBoundsException e) 
			{
				number = 0;
			}
			
			DBRow delivery_order = new DBRow();
			delivery_order.add("delivery_purchase_id", delivery_purchase_id);
			delivery_order.add("delivery_supplier_id",delivery_supplier_id);
			delivery_order.add("delivery_address",purchase.getString("delivery_address"));
			delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
			delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
			
			delivery_order.add("deliver_address1",productStorageCatalog.getString("deliver_address1"));
			delivery_order.add("deliver_address2",productStorageCatalog.getString("deliver_address2"));
			delivery_order.add("deliver_address3",productStorageCatalog.getString("deliver_address3"));
			
			delivery_order.add("ccid",productStorageCatalog.getString("ccid"));
			delivery_order.add("pro_id",productStorageCatalog.getString("pro_id"));
			delivery_order.add("deliver_zip_code",productStorageCatalog.getString("deliver_zip_code"));
			delivery_order.add("deliver_city",productStorageCatalog.getString("city"));
			
			delivery_order.add("delivery_order_number","P"+delivery_purchase_id+"-"+(number+1));
			delivery_order.add("delivery_date",DateUtil.NowStr());
			delivery_order.add("save_status",1);
			delivery_order.add("delivery_create_account_id",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());
			delivery_order.add("delivery_create_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());
			delivery_order.add("updatedate",DateUtil.NowStr());
			delivery_order.add("updateby",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());
			delivery_order.add("updatename",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());
			delivery_order.add("delivery_psid", purchase.get("ps_id", 0l));
			
			long delivery_order_id = floorDeliveryMgrZJ.addDelveryOrder(delivery_order);//创建交货单
			
			/**
			 * 根据采购单未到货创建交货单详细
			 */
			DBRow[] needDelivery = floorPurchaseMgr.checkPurchaseStats(delivery_purchase_id);
			

			for (int i = 0; i < needDelivery.length; i++)
			{
				DBRow dbrow = new DBRow();//needDelivery[i];
				dbrow.add("product_id",needDelivery[i].get("product_id",0l));
				
				dbrow.add("product_name",needDelivery[i].getString("purchase_name"));
				dbrow.add("product_barcode",needDelivery[i].getString("proudct_barcod"));
				dbrow.add("delivery_count",(needDelivery[i].get("purchase_count",0f)-needDelivery[i].get("reap_count",0f)));
				
				dbrow.add("delivery_reap_count",0f);
				dbrow.add("delivery_order_id",delivery_order_id);
				
				floorDeliveryMgrZJ.addDeliveryOrderDetail(dbrow);
			}
			/**
			 *修改采购单为交货中状态 
			 */
//			DBRow para = new DBRow();
//			para.add("purchase_status",PurchaseKey.DELIVERYING);
//			floorPurchaseMgr.updatePurchase(delivery_purchase_id, para);
			
			return (delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 添加交货单详细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryOrderDetail(HttpServletRequest request)
		throws Exception
	{

		try 
		{
			long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
			String product_name = StringUtil.getString(request,"product_name");
			float delivery_count = StringUtil.getFloat(request,"delivery_count");
			String delivery_box = StringUtil.getString(request,"delivery_box");
			
			DBRow product = floorProductMgr.getDetailProductByPname(product_name);
			if(product == null)
			{
				throw new ProductNotExistException();
			}
			
			long product_id = product.get("pc_id",0l);
			
			DBRow productDelivery = floorDeliveryMgrZJ.getDeliveryOrderDetailByPcId(delivery_order_id, product_id);
			if(productDelivery !=null)
			{
				throw new RepeatProductException();
			}
			
			DBRow dbrow = new DBRow();
			dbrow.add("product_id",product.get("pc_id",0l));
			
			dbrow.add("delivery_order_id",delivery_order_id);
			dbrow.add("product_name",product_name);
			dbrow.add("product_barcode",product.getString("p_name"));
			dbrow.add("delivery_count",delivery_count);
			dbrow.add("delivery_reap_count",0f);
			dbrow.add("delivery_box",delivery_box);
			
			long delivery_order_detail_id = floorDeliveryMgrZJ.addDeliveryOrderDetail(dbrow);
			return delivery_order_detail_id;
		} 
		catch (ProductNotExistException e) 
		{
			throw e;
		}
		catch(RepeatProductException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 根据采购单获得交货单列表
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrders(long purchase_id) 
		throws Exception 
	{
		
		try 
		{
			return floorDeliveryMgrZJ.getDelveryOrdersByPurchaseId(purchase_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 点击交货生成虚拟交货单
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] virtualDeliveryOrderDetailsByPurchaseId(long purchase_id)
		throws Exception
	{
		DBRow[] needDelivery = floorPurchaseMgr.checkPurchaseStats(purchase_id);
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < needDelivery.length; i++)
		{
			DBRow product = productMgr.getDetailProductByPcid(needDelivery[i].get("product_id",0l));
			DBRow dbrow = new DBRow();//needDelivery[i];

			dbrow.add("product_id",product.get("pc_id",0l));
			dbrow.add("product_name",product.getString("p_name"));
			dbrow.add("product_barcode",product.getString("p_code"));
			dbrow.add("delivery_count",(needDelivery[i].get("purchase_count",0f)-needDelivery[i].get("reap_count",0f)));
			dbrow.add("unit_name",product.getString("unit_name"));
			dbrow.add("purchase_count",needDelivery[i].get("purchase_count",0f));
			dbrow.add("reap_count",needDelivery[i].get("reap_count",0f));
			
			list.add(dbrow);
		}
		
		return (list.toArray(new DBRow[0]));	
	}
	
	/**
	 * 根据交货单ID获得交货单明细
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderDetails(long delivery_order_id,long purchase_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception 
	{
		try 
		{
			return (floorDeliveryMgrZJ.getDeliveryOrderDetails(delivery_order_id,purchase_id,pc,sidx,sord,fillterBean));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}

	
	/**
	 * 根据交货单ID获得交货单详细
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDeliveryOrder(long delivery_order_id)
		throws Exception 
	{
		
		try 
		{
			return floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 下载交货单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downloadDeliveryOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String number = "";
			long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
			DBRow delivery_order= floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			DBRow[] delivery_order_details = null;
			if(delivery_order != null)
			{
				//delivery_order.get("delivery_purchase_id",0l);
				delivery_order_details = floorDeliveryMgrZJ.getDeliveryOrderDetails(delivery_order_id, purchase_id,null,null,null,null);
				number = delivery_order.getString("delivery_order_number");
			}
			else
			{
				number = "P"+purchase_id+"-"+(floorDeliveryMgrZJ.getDelveryOrdersByPurchaseId(purchase_id).length+1);
				delivery_order_details = virtualDeliveryOrderDetailsByPurchaseId(purchase_id);
			}
			
			
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/delivery/delivery_order_template/deliveryOrder_template.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); //创建一个居中格式
			style.setLocked(false);//设置不锁定
			style.setWrapText(true); 
			
			for (int i = 0; i < delivery_order_details.length; i++) 
			{
				DBRow delivery_order_detail = delivery_order_details[i];
				long pid = delivery_order_detail.get("product_id",0l);
				DBRow product = productMgr.getDetailProductByPcid(pid);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(product.getString("p_code"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(product.getString("unit_name"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(delivery_order_detail.getString("reap_count"));
				row.getCell(3).setCellStyle(style);
				
				row.createCell(4).setCellValue(delivery_order_detail.getString("purchase_count"));
				row.getCell(4).setCellStyle(style);
				
				row.createCell(5).setCellValue(delivery_order_detail.getString("delivery_count"));
				row.getCell(5).setCellStyle(style);
				
				row.createCell(6).setCellValue("");
				row.getCell(6).setCellStyle(style);
			}
			String path = "upl_excel_tmp/J"+number+".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
			wb.write(fout);
			fout.close();
			return (path);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 保存交货单(页面保存按钮，修改)
	 * @param request
	 * @throws Exception
	 */
	public void saveDeliveryOrder(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
			String waybill_number = StringUtil.getString(request,"waybill_number");
			String waybill_name = StringUtil.getString(request,"waybill_name");
			String arrival_eta = StringUtil.getString(request,"arrival_eta");
			
			DBRow para = new DBRow();
			para.add("waybill_number",waybill_number);
			para.add("waybill_name",waybill_name);
			para.add("arrival_eta",arrival_eta);
			para.add("save_status",DeliveryOrderSaveKey.HASSAVE);
			
			floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, para);
			
			DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);//为了获得采购单ID
			long delivery_purchase_id = deliveryOrder.get("delivery_purchase_id",0l);
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 交货单明细上传
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String[] uploadDeliveryOrderDetail(HttpServletRequest request)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String[] msg=new String[3];
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long delivery_order_id = Long.parseLong(upload.getRequestRow().getString("delivery_order_id"));
			long purchase_id = Long.parseLong(upload.getRequestRow().getString("purchase_id"));
			
			msg[1] = String.valueOf(delivery_order_id);
			msg[2] = String.valueOf(purchase_id);
				
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg[0] = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadPurchaseDetail",log);
		}
	}
	
	
	/**
	 * 将excel文件转换成DBRow[]
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelShow(String filename)
		throws Exception
	{
		try 
		{
			HashMap<String,String> filedName = new HashMap<String, String>();
			filedName.put("0","product_name");
			filedName.put("1","product_barcode");
			filedName.put("2","unit_name");
			filedName.put("3","reap_count");
			filedName.put("4","purchase_count");
			filedName.put("5","delivery_count");
			filedName.put("6","delivery_box");
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_excel_tmp/" + filename;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);

			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
  
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();
			   
			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
			    
				   pro.add(filedName.get(String.valueOf(j)),rs.getCell(j,i).getContents().trim());
				   
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
			   DBRow product = new DBRow();
			   //商品条码可不惟一 zyj
					   //productMgr.getDetailProductByPcode(pro.getString("product_barcode"));
			   if(product!=null)
			   {
				   pro.add("product_id",product.get("pc_id",0l));
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  
			  return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelshow",log);
		}
	}
	
	/**
	 * 保存上传的交货单详细
	 * @param request
	 * @throws Exception
	 */
	public void saveDeliveryOrderDetails(HttpServletRequest request)
		throws Exception
	{
		long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
		String filename = StringUtil.getString(request,"filename");
		floorDeliveryMgrZJ.clearDeliveryOrderDetail(delivery_order_id);
		
		DBRow[] delivery_order_details = excelShow(filename);
		
		for (int i = 0; i < delivery_order_details.length; i++)
		{
			DBRow product = new DBRow();
					 //商品条码可不惟一 zyj
					//productMgr.getDetailProductByPcode(delivery_order_details[i].getString("product_barcode"));
			
			DBRow dbrow = new DBRow();
			dbrow.add("product_id",product.get("pc_id",0l));
			dbrow.add("product_name",product.getString("p_name"));
			dbrow.add("product_barcode",product.getString("p_code"));
			dbrow.add("delivery_count",delivery_order_details[i].getString("delivery_count"));
			dbrow.add("delivery_order_id",delivery_order_id);
			dbrow.add("delivery_box",delivery_order_details[i].getString("delivery_box"));
			
			floorDeliveryMgrZJ.addDeliveryOrderDetail(dbrow);
		}
		
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
	}
	
	/**
	 * 获得全部交货单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)
		throws Exception 
	{
		
		try 
		{
			return floorDeliveryMgrZJ.getAllDeliveryOrder(pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 过滤交货单
	 * @param supplier
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterDeliveryOrders(long supplier, int status,long ps_id,PageCtrl pc,int declaration, int clearance,int invoice, int drawback, int day, long productline_id,int stock_in_set,long delivery_create_account_id)
		throws Exception 
	{
		try 
		{
			return (floorDeliveryMgrZJ.getDeliveryOrder(supplier,status,ps_id,pc,declaration, clearance,invoice, drawback, day, productline_id,stock_in_set,delivery_create_account_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 检索交货单
	 * @param number
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchDeliveryOrders(String number, PageCtrl pc,long supplier_id)
		throws Exception 
	{
		try 
		{
			 //return (floorDeliveryMgrZJ.searchDeliveryOrder(number, pc,supplier_id));
			return DeliveryOrderIndexMgr.getInstance().getSearchResults(number, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 交货单入库（文件上传方式）
	 * @param request
	 * @throws Exception
	 */
	public void deliveryWarehousing(HttpServletRequest request)
		throws Exception
	{
		String filename = StringUtil.getString(request,"filename");
		DBRow[] deliveryOrderDetails = null;//csvDBRow(filename);
		long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
		String machine_id = "";
		
		AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		
		
		this.deliveryWarehousingSub(delivery_order_id,deliveryOrderDetails,adminLoggerBean,machine_id);
	}
		
	/**
	 * 交货单入库提交
	 * @param delivery_order_id
	 * @param deliveryOrderDetails
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void deliveryWarehousingSub(long delivery_order_id,DBRow[] deliveryOrderDetails,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{		
		try {
			DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);//获得交货单
			DBRow deliveryOrderpara = new DBRow();
			
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(deliveryOrder.get("delivery_purchase_id",0l));//确定对应的采购单
			
			for (int i = 0; i < deliveryOrderDetails.length; i++) 
			{
					float count = Float.valueOf(deliveryOrderDetails[i].getString("dw_count"));//获得交货数量
					if(count > 0f)
					{
						DBRow deliveryOrderDetail = floorDeliveryMgrZJ.getDeliveryOrderDetailsByPDId(deliveryOrderDetails[i].get("dw_deliveryOrder_id",0l),deliveryOrderDetails[i].get("dw_product_id",0l));
						
						if(deliveryOrderDetail !=null)
						{
							float reap_count = deliveryOrderDetail.get("delivery_reap_count",0f);
							reap_count = reap_count + count;//到货数量累加
							
							deliveryOrderDetail.add("delivery_reap_count",reap_count);
							floorDeliveryMgrZJ.modDeliveryOrderDetailById(deliveryOrderDetail.get("delivery_order_detail_id",0l),deliveryOrderDetail);//修改到货量
						}
						else
						{
							DBRow noInDeliveryOrderproduct = productMgr.getDetailProductByPcid(deliveryOrderDetails[i].get("dw_product_id",0l));
							if(noInDeliveryOrderproduct == null)
							{
								throw new ProductNotExistException();
							}
							
							deliveryOrderDetail = new DBRow();
							
							deliveryOrderDetail.add("product_id",noInDeliveryOrderproduct.get("pc_id",0l));
							deliveryOrderDetail.add("product_name",noInDeliveryOrderproduct.getString("p_name"));
							deliveryOrderDetail.add("product_barcode",noInDeliveryOrderproduct.getString("p_code"));
							deliveryOrderDetail.add("delivery_count",0);
							deliveryOrderDetail.add("delivery_reap_count",count);
							deliveryOrderDetail.add("delivery_order_id",delivery_order_id);
							
							floorDeliveryMgrZJ.addDeliveryOrderDetail(deliveryOrderDetail);
						}
						
						//入库
					}
					
			}
			//检查交货单是否已完成
				DBRow[] checkApprove = floorDeliveryMgrZJ.checkDeliveryOrderApprove(delivery_order_id);
				if(checkApprove!=null&&checkApprove.length>0)
				{
					deliveryApproveMgrZJ.addDeliveryApproveSub(delivery_order_id, adminLoggerBean);					
				}
				else
				{
					TDate tDate = new TDate();
					TDate endDate = new TDate(deliveryOrder.getString("delivery_date")+" 00:00:00");
					double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
					
					deliveryOrderpara.add("all_over", diffDay);
					deliveryOrderpara.add("delivery_order_status",DeliveryOrderKey.FINISH);
					floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, deliveryOrderpara);//修改交货状态	
					
					deliveryApproveMgrZJ.stockIn(delivery_order_id, adminLoggerBean,ProductStoreOperationKey.IN_STORE_DELIVERY);//完成状态需要入库
				}
				
				
			
			DeliveryOrderKey deliveryOrderKey = new DeliveryOrderKey();
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 交货单路途中
	 * @param request
	 * @throws Exception
	 */
	public void IntransitDelivery(HttpServletRequest request)
		throws Exception
	{
		long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
		
		DBRow para = new DBRow();
		para.add("delivery_order_status",DeliveryOrderKey.INTRANSIT);
		
		floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, para);
		
		DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);
		DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(deliveryOrder.get("delivery_purchase_id",0l));
		((DeliveryMgrIFaceZJ)AopContext.currentProxy()).deliveryOrderIntransitForJbpm(delivery_order_id,purchase.get("ps_id",0l));//提供工作流拦截交货单已起运
	}
	
	
	/**
	 * 提供工作流拦截交货单已起运
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public String deliveryOrderIntransitForJbpm(long delivery_order_id,long ps_id)
		throws Exception
	{
		return (delivery_order_id+","+ps_id);
	}
	
	/**
	 * 根据交货单状态、仓库过滤交货单
	 * @param status
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrdersByPsStatus(int status, long ps_id)
		throws Exception 
	{
		
		try 
		{
			return (floorDeliveryMgrZJ.getDeliveryOrderDetailsByPsidStatus(ps_id,status));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 根据交货单号得到交货单详细
	 * @param delivery_order_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDeliveryOrderByDeliveryNumber(String delivery_order_number) 
		throws Exception 
	{
		try 
		{
			return (floorDeliveryMgrZJ.getDetailDeliveryOrderByDeliveryNumber(delivery_order_number));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 修改交货单详细
	 * @param request
	 * @throws Exception
	 */
	public void modDeliveryOrderDetail(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long delivery_order_detail_id = StringUtil.getLong(request,"id");//grid默认参数是ID
			Map parameter = request.getParameterMap();
			float delivery_count;
			String delivery_box;
			String product_name;
			DBRow para = new DBRow();
			if(parameter.containsKey("delivery_count"))
			{
				delivery_count = StringUtil.getFloat(request,"delivery_count");
				para.add("delivery_count",delivery_count);
			}
			
			if(parameter.containsKey("delivery_box"))
			{
				delivery_box = StringUtil.getString(request,"delivery_box");
				para.add("delivery_box",delivery_box);
			}
			if(parameter.containsKey("product_name"))
			{
				product_name = StringUtil.getString(request,"product_name");
				
				DBRow product = floorProductMgr.getDetailProductByPname(product_name);
				if(product == null)
				{
					throw new ProductNotExistException();
				}
				
				long product_id = product.get("pc_id",0l);
				DBRow deliveryOrderDetail = floorDeliveryMgrZJ.getDeliveryOrderDetail(delivery_order_detail_id);
				long delivery_order_id = deliveryOrderDetail.get("delivery_order_id",0l);
				
				DBRow productDelivery = floorDeliveryMgrZJ.getDeliveryOrderDetailByPcId(delivery_order_id, product_id);
				if(productDelivery !=null)
				{
					throw new RepeatProductException();
				}
				para.add("product_id",product.get("pc_id",0l));
				para.add("product_name",product_name);
				para.add("product_barcode",product.getString("p_code"));
			}
			
			
			floorDeliveryMgrZJ.modDeliveryOrderDetailById(delivery_order_detail_id, para);
		} 
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(RepeatProductException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 删除交货单明细
	 * @param request
	 * @throws Exception
	 */
	public void delDelvieryOrderDetail(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long delivery_order_detail_id = StringUtil.getLong(request,"id");
			floorDeliveryMgrZJ.delDelvieryOrderDetail(delivery_order_detail_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 根据交货单明细ID获得交货单明细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliveryOrderDetail(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long delivery_order_detail_id = StringUtil.getLong(request,"delivery_order_detail_id");
			return (floorDeliveryMgrZJ.getDeliveryOrderDetail(delivery_order_detail_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 获得采购单未保存的交货单
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNoSaveDeliveryOrderByPurchaseID(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			return (floorDeliveryMgrZJ.getNoSaveDeliveryOrderByPurchaseID(purchase_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 获得供应商未完成的交货单
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderNoFinishBySupplier(long supplier_id)
		throws Exception
	{
		try 
		{
			return (floorDeliveryMgrZJ.getDeliveryOrderNoFinishBySupplier(supplier_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 删除交货单
	 * @param delivery_order_id
	 * @throws Exception
	 */
	public void delDeliveryOrder(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
			floorDeliveryMgrZJ.delDeliveryOrder(delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 采购单完成删除未完成的交货单
	 * @param purchase_id
	 * @throws Exception
	 */
	public void delDeliveryOrderNoFinishByPurchaseFinish(long purchase_id)
		throws Exception
	{
		try 
		{
			DBRow[] rows = floorDeliveryMgrZJ.getDeliveryOrderNoFinishByPurchaseId(purchase_id);
			for(int i=0;i<rows.length;i++)
			{
				long delivery_order_id = rows[i].get("delivery_order_id",0l);
				
				floorDeliveryMgrZJ.delDeliveryOrder(delivery_order_id);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public float getDeliveryOrderWeight(long delivery_order_id)
		throws Exception
	{
		DBRow[] deliveryProducts = floorDeliveryMgrZJ.getDeliverOrderDetailsByDeliveryId(delivery_order_id);
		
		float weight = 0;
		for (int i = 0; i < deliveryProducts.length; i++)
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(deliveryProducts[i].get("product_id",0l));
			
			weight += product.get("weight",0f)*deliveryProducts[i].get("delivery_count",0f);
		}
		
		return weight;
	}
	
	/**
	 *根据交货单ID获得交货单上商品 
	 */
	public DBRow[] getDeliveryOrderDetailsById(long delivery_order_id)
		throws Exception
	{
		try 
		{
			return floorDeliveryMgrZJ.getDeliverOrderDetailsByDeliveryId(delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void printDeliveryWayBill(String airWayBillNumber,long ccid,long pro_id,long sc_id,long id,String send_name,String send_zip_code,String send_address1,String send_address2,String send_address3,String send_linkman_phone,String send_city,String deliver_name,String deliver_zip_code,String deliver_address1,String deliver_address2,String deliver_address3,String deliver_linkman_phone,String deliver_city,float weight,String shippingName,String hs_code,int pkcount,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			DBRow deliveryPara = new DBRow();
			
			deliveryPara.add("send_name",send_name);
			deliveryPara.add("send_zip_code",send_zip_code);
			deliveryPara.add("send_address1",send_address1);
			deliveryPara.add("send_address2",send_address2);
			deliveryPara.add("send_address3",send_address3);
			deliveryPara.add("send_linkman_phone",send_linkman_phone);
			deliveryPara.add("send_city",send_city);
			deliveryPara.add("delivery_linkman",deliver_name);
			deliveryPara.add("deliver_zip_code",deliver_zip_code);
			deliveryPara.add("deliver_address1",deliver_address1);
			deliveryPara.add("deliver_address2",deliver_address2);
			deliveryPara.add("deliver_address3",deliver_address3);
			deliveryPara.add("delivery_linkman_phone",deliver_linkman_phone);
			deliveryPara.add("deliver_city",deliver_city);
			
			deliveryPara.add("waybill_number",airWayBillNumber);
			deliveryPara.add("ccid",ccid);
			deliveryPara.add("pro_id",pro_id);
			deliveryPara.add("sc_id",sc_id);
			deliveryPara.add("waybill_name",shippingName);
			deliveryPara.add("hs_code",hs_code);
			deliveryPara.add("pkcount",pkcount);
			deliveryPara.add("weight",weight);
			
			floorDeliveryMgrZJ.modDelveryOrder(id,deliveryPara);
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printDeliveryWayBill",log);
		}
	}
	
	/**
	 * 上传发票文件
	 * @param request
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public void uploadDeliveryInvoice(HttpServletRequest request)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String permitFile = "xls,doc,xlsx,docx";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long delivery_order_id = Long.parseLong(upload.getRequestRow().getString("delivery_order_id"));
			
				
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				String[] temp = upload.getFileName().split("\\."); 
				String fileType = temp[temp.length-1];
				
				String fileName = "D"+delivery_order_id+"."+fileType;
				String temp_url = Environment.getHome()+"upl_excel_tmp/"+upload.getFileName();
				String invoice_path = "invoice_file/"+fileName;
				String url = Environment.getHome()+invoice_path;
				FileUtil.moveFile(temp_url,url);
				
				DBRow para  = new DBRow();
				para.add("invoice_path",invoice_path);
				
				floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, para);
				
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				
			}
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadDeliveryInvoice",log);
		}
	}
	
	public void setFloorDeliveryMgrZJ(FloorDeliveryMgrZJ floorDeliveryMgrZJ) {
		this.floorDeliveryMgrZJ = floorDeliveryMgrZJ;
	}

	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


	public void setDeliveryApproveMgrZJ(
			DeliveryApproveMgrIFaceZJ deliveryApproveMgrZJ) {
		this.deliveryApproveMgrZJ = deliveryApproveMgrZJ;
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
}
