package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.tjh.FloorOrderProcessMgrTJH;
import com.cwc.app.floor.api.zj.FloorReturnStepMgrZJ;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.ReturnStepMgrIFaceZJ;
import com.cwc.app.key.BatchOperationTypeKey;
import com.cwc.app.key.BatchTypeKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ReturnStepMgrZJ implements ReturnStepMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorReturnStepMgrZJ floorReturnStepMgrZJ;
	private FloorOrderProcessMgrTJH floorOrderProcessMgrTJH;
	private FloorProductMgr floorProductMgr;
	private ProductMgrIFace productMgr;
	
	/**
	 * 过滤退件
	 * @param ps_id
	 * @param catalog_id
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterReturnSteps(long ps_id, long catalog_id,String product_line_id, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String catalog_ids = "";
			
			if(product_line_id.contains("-p"))//选择的是产品线,产品线第一层
			{
				long pro_line_id = Long.parseLong(product_line_id.substring(0, product_line_id.indexOf("-")));
				if(pro_line_id !=0)//为0则不查询子分类
				{
					DBRow catalogs = floorOrderProcessMgrTJH.getProductLineIds(pro_line_id);
					catalog_ids = catalogs.getString("line_ids");
				}
				
			}
			else if(!product_line_id.contains("-p")&&!product_line_id.equals(""))//产品线下拉框第二层
			{
				if(Long.parseLong(product_line_id)!=0)
				{
					catalog_id = Long.parseLong(product_line_id);
				}
				
			}
			//选择产品线第二层，实际上选择的是商品分类
			if(catalog_id!=0)
			{
				DBRow ids = floorOrderProcessMgrTJH.getCatalogResultSet(catalog_id);
				catalog_ids = ids.getString("catalog_ids");
			}
			
			return (floorReturnStepMgrZJ.filterReturnStep(ps_id, catalog_ids, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"filterReturnSteps",log);
		}
	}
	
	/**
	 * 根据商品名或条码查询残损退件
	 * @param catalog_id
	 * @param ps_id
	 * @param product_line_id
	 * @param name
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchReturnStepByName(long catalog_id, long ps_id,String product_line_id, String name, PageCtrl pc)
		throws Exception
	{
		try 
		{
			String catalog_ids = "";
			
			if(product_line_id.contains("-p"))//选择的是产品线,产品线第一层
			{
				long pro_line_id = Long.parseLong(product_line_id.substring(0, product_line_id.indexOf("-")));
				if(pro_line_id !=0)//为0则不查询子分类
				{
					DBRow catalogs = floorOrderProcessMgrTJH.getProductLineIds(pro_line_id);
					catalog_ids = catalogs.getString("line_ids");
				}
				
			}
			else if(!product_line_id.contains("-p")&&!product_line_id.equals(""))//产品线下拉框第二层
			{
				if(Long.parseLong(product_line_id)!=0)
				{
					catalog_id = Long.parseLong(product_line_id);
				}
				
			}
			//选择产品线第二层，实际上选择的是商品分类
			if(catalog_id!=0)
			{
				DBRow ids = floorOrderProcessMgrTJH.getCatalogResultSet(catalog_id);
				catalog_ids = ids.getString("catalog_ids");
			}
			
			return (floorReturnStepMgrZJ.getReturnStepByName(catalog_ids, ps_id, name, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"searchReturnStepByName",log);
		}
	}
	
	/**
	 * 获得残损件存放位置
	 * @param ps_id
	 * @param pc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnStepLocationByPcid(long ps_id, long pc_id,PageCtrl pc) 
	throws Exception 
	{
		try 
		{
			return (floorReturnStepMgrZJ.getReturnStepLocationByPcid(ps_id, pc_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnStepLocationByPcid",log);
		}
	}
	
	/**
	 * 残损件拆分套装
	 * @param request
	 * @throws Exception
	 */
	public void spiltReturnStepUnion(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long ps_id = StringUtil.getLong(request,"ps_id");
			long pc_id = StringUtil.getLong(request,"pc_id");
			String pids = StringUtil.getString(request,"pids");
			String[] pid = pids.split(",");
			
			float split_quantity = StringUtil.getFloat(request,"split_quantity");
			float split_package_quantity = StringUtil.getFloat(request,"split_package_quantity");
			long s_pid = StringUtil.getLong(request,"s_pid");
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow damaged_store = floorProductMgr.getDetailProductStorageByPid(s_pid);
			//减功能残损库存
			if(split_quantity>0f)
			{
				floorProductMgr.deIncProductDamagedCountByPid(s_pid,split_quantity);
				
				//记录残损件出库日志
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(0);
				deProductDamagedCountLogBean.setPs_id(ps_id);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SPLIT_DAMDGED_UNION_STANDARD);
				deProductDamagedCountLogBean.setQuantity(-split_quantity);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(damaged_store.get("pc_id",0l));
				productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			//减外观残损库存
			if(split_package_quantity>0f)
			{
				floorProductMgr.deIncProductPackageDamagedCountByPid(s_pid,split_package_quantity);
				
				//记录残损件出库日志
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(0);
				deProductDamagedCountLogBean.setPs_id(ps_id);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SPLIT_DAMDGED_UNION_STANDARD);
				deProductDamagedCountLogBean.setQuantity(-split_quantity);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(damaged_store.get("pc_id",0l));
				productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			
			for (int i = 0; i < pid.length; i++) 
			{
				float whole = StringUtil.getFloat(request,pid[i]+"_whole");
				float damaged = StringUtil.getFloat(request,pid[i]+"_damaged");
				float appearance = StringUtil.getFloat(request,pid[i]+"_appearance");
				
				DBRow product_storage = floorProductMgr.getDetailProductStorageByRscIdPcid(ps_id,Long.parseLong(pid[i]));
				
				if(whole>0f)//完好
				{
					this.spiltUnionWhole(adminLoggerBean, ps_id,product_storage.get("pc_id",0l),whole,0);
				}
				
				if(damaged>0f)// 功能残损
				{
					floorProductMgr.incProductDamagedCountByPid(product_storage.get("pid",0l),damaged);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(0);
					inProductDamagedCountLogBean.setPs_id(ps_id);
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
					inProductDamagedCountLogBean.setQuantity(split_quantity);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(product_storage.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				
				if(appearance>0f)//包装残损
				{
					floorProductMgr.incProductPackageDamagedCountByPid(product_storage.get("pid",0l),appearance);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(0);
					inProductDamagedCountLogBean.setPs_id(ps_id);
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
					inProductDamagedCountLogBean.setQuantity(split_quantity);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(product_storage.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"spiltReturnStepUnion",log);
		}
	}
	
	/**
	 * 残损套装拆散完好散件
	 * @param adminLoggerBean
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @throws Exception
	 */
	private void spiltUnionWhole(AdminLoginBean adminLoggerBean,long ps_id,long pc_id,float quantity, long from_psl_id)
		throws Exception
	{
		try 
		{
			TDate td = new TDate();
			long datemark = td.getDateTime();

			//进库
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(0);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_NORMAL);
			floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean, ps_id, pc_id, quantity,from_psl_id);

			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			productMgr.addInProductLog(product.getString("p_code"), ps_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_DAMAGED_SPILT);
//			batchMgrLL.stockInOne(batch_id, 0l, product.get("pc_id", 0l), ps_id, product.get("pc_id", 0l), quantity, product.get("unit_price", 0f), 0, 0,  BatchOperationTypeKey.NORMAL);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"spiltUnionWhole",log);
		}
	}

	public void setFloorReturnStepMgrZJ(FloorReturnStepMgrZJ floorReturnStepMgrZJ) {
		this.floorReturnStepMgrZJ = floorReturnStepMgrZJ;
	}

	public void setFloorOrderProcessMgrTJH(
			FloorOrderProcessMgrTJH floorOrderProcessMgrTJH) {
		this.floorOrderProcessMgrTJH = floorOrderProcessMgrTJH;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

}
