package com.cwc.app.api.zj;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;


































import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AddInProductBean;
import com.cwc.app.beans.AddShipProductBean;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.android.ContainerNotFullException;
import com.cwc.app.exception.bcs.CanNotPutOverException;
import com.cwc.app.exception.bcs.CanNotPutProductException;
import com.cwc.app.exception.bcs.LpNotFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.NoExistLocationException;
import com.cwc.app.exception.bcs.PickUpCountMoreException;
import com.cwc.app.exception.bcs.ReceiveProductSNRepeatException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.damagedRepair.DamagedRepairOrderCanNotOutboundException;
import com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderCanNotWareHouseException;
import com.cwc.app.exception.deliveryOrder.DeliveryOrderHasWareHouseException;
import com.cwc.app.exception.deliveryOrder.NoExistDeliveryOrderException;
import com.cwc.app.exception.location.LocationCatalogNotExitsException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.exception.purchase.NoPurchaseException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportOrderCanNotOutboundException;
import com.cwc.app.exception.transport.TransportOrderCanNotWareHouseException;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutboundOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.BCSMgrIFaceZJ;
import com.cwc.app.iface.zj.ContainerProductMgrIFaceZJ;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.app.iface.zj.DamagedRepairOutboundApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.DamagedRepairOutboundMgrIFaceZJ;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.DeliveryWarehouseMgrIFaceZJ;
import com.cwc.app.iface.zj.OrderMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportWarehouseMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zyj.ContainerMgrIFaceZyj;
import com.cwc.app.key.BillTypeKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.DamagedRepairKey;
import com.cwc.app.key.DeliveryOrderKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.TransportCompareKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportPutKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class BCSMgrZJ implements BCSMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private ProductMgrIFace productMgr;
	private CatalogMgrIFace catalogMgr;
	private PurchaseIFace purchaseMgr;
	private FloorOrderMgr floorOrderMgr;
	private ExpressMgrIFace expressMgr;
	private OrderMgrIFaceZJ orderMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	private DeliveryWarehouseMgrIFaceZJ deliveryWarehouseMgrZJ;
	private TransportMgrIFaceZJ transportMgrZJ;
	private TransportWarehouseMgrIFaceZJ transportWarehouseMgrZJ;
	private TransportOutboundMgrIFaceZJ transportOutboundMgrZJ;
	private TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ;
	private DamagedRepairMgrIFaceZJ damagedRepairMgrZJ;
	private DamagedRepairOutboundMgrIFaceZJ damagedRepairOutboundMgrZJ;
	private DamagedRepairOutboundApproveMgrIFaceZJ damagedRepairOutboundApproveMgrZJ;
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private FloorProductCodeMgrZJ floorProductCodeMgrZJ;
	private FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ;
	private ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ;
	private ContainerProductMgrIFaceZJ containerProductMgrZJ;
	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	private ContainerMgrIFaceZyj containerMgrZyj;
	private AndroidMgrIfaceZr androidMgrZr ;
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	/**
	 * 条码机上传定位信息数据处理
	 * @param xml
	 * @param ps_id
	 * @param machine
	 * @throws Exception
	 */
	public int productLocation(String xml,AdminLoginBean adminLoggerBean,String machine)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			String[] inproducts = details.split("\\|");
			for (int i = 0; i < inproducts.length; i++) 
			{
				String[] inproduct = inproducts[i].split(",");
				if(inproducts[i].trim().equals("")||inproduct.length!=3)
				{
					throw new XMLDataErrorException();
				}
				DBRow inpara = new DBRow();
//				inpara.add("barcode", inproduct[0]);
//				inpara.add("pc_id",inproduct[1]);
//				inpara.add("quantity",inproduct[2]);
//				inpara.add("position",inproduct[3]);
				
				inpara.add("pc_id",inproduct[0]);
				inpara.add("quantity",inproduct[1]);
				inpara.add("position",inproduct[2]);
				list.add(inpara);
			}
			return (bcsStorageLocation(adminLoggerBean,list.toArray(new DBRow[0]),machine));
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(OperationNotPermitException e)
		{
			throw e;
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"productLocation",log);
		}
	}
	
	/**
	 * 条码机上传商品定位信息
	 * @param ps_name
	 * @param inproduct,上传信息DBRow[]
	 * @param
	 * @throws Exception
	 */
	private int bcsStorageLocation(AdminLoginBean adminLoggerBean,DBRow[] inproduct,String machine_id)
		throws Exception
	{
		long pc_id = 0;
		int i =0;
		try 
		{
			long ps_id = adminLoggerBean.getPs_id();
			floorProductMgr.cleanStorageLocationByPsidMachineId(ps_id, machine_id);//清除当前仓库原有机器数据
			for(; i<inproduct.length; i++)
			{
				pc_id = inproduct[i].get("pc_id",0l);
				
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				if(product==null)
				{
					log.info(pc_id+"----无此商品");
					continue;
					//throw new ProductNotExistException();
				}
				String p_code = product.getString("p_code");
//暂时不适用				productMgr.addStorageLocation(ps_id,pc_id,p_code,Float.valueOf(inproduct[i].getString("quantity")),inproduct[i].getString("position"), machine_id,null,adminLoggerBean);
				productMgr.addStorageLocation(ps_id, pc_id,inproduct[i].get("position",0l),Float.valueOf(inproduct[i].getString("quantity")),machine_id,inproduct[i].getString("sn",null),inproduct[i].get("con_id",0l),inproduct[i].get("title_id",0l),adminLoggerBean);
			}
			return i;
		}
		catch(ProductNotExistException e)
		{
			
			throw e;
		}
		catch(OperationNotPermitException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			log.info("error_pcid:"+pc_id);
			throw new SystemException(e,"bcsStorageLocation",log);
		}
	}
	
	public int productTakeStock(String xml,AdminLoginBean adminLoggerBean,String machine)
		throws Exception
	{
		long pc_id = 0 ;
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String position = getSampleNode(xml,"Location");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			DBRow location = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(adminLoggerBean.getPs_id(),position);
			if (location==null)
			{
				throw new LocationCatalogNotExitsException();
			}
			
		
			//清空位置上的盘点数据
			long slc_id = location.get("slc_id",0l);
			floorProductMgr.cleanStorageLocationBySlc(slc_id);
			
			if (!details.equals("NULL")) 
			{
				String[] inproducts = details.split("\\|");
				for (int i = 0; i < inproducts.length; i++) 
				{
					String[] inproduct = inproducts[i].split(",");
					if(inproducts[i].trim().equals("")||inproduct.length!=5)
					{
						throw new XMLDataErrorException();
					}
					
					pc_id = Long.parseLong(inproduct[0]);
					float quantity = Float.parseFloat(inproduct[1]);
					long con_id = Long.parseLong(inproduct[2]);
					long title_id = Long.parseLong(inproduct[3]);
					String serialNumber = inproduct[4];
					
					productMgr.addStorageLocation(adminLoggerBean.getPs_id(), pc_id, slc_id, quantity, machine,serialNumber, con_id, title_id, adminLoggerBean);
					
					serialNumberMgrZJ.serialProductPositioning(serialNumber, slc_id);
				}
				
				return inproducts.length;
			}
			else
			{
				return 0;
			}
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(ProductNotExistException e)
		{
			
			throw e;
		}
		catch(OperationNotPermitException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (LocationCatalogNotExitsException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			log.info("error_pcid:"+pc_id);
			throw new SystemException(e,"bcsStorageLocation",log);
		}
	}
	
	/**
	 * 条码机比较库存
	 * @param xml
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public String compareProductStorage(String xml,AdminLoginBean adminLoggerBean,String machine)
		throws Exception
	{
		try 
		{
			String ware = getSampleNode(xml,"Ware");
			long ps_id = adminLoggerBean.getPs_id();
			long cid = Long.parseLong(getSampleNode(xml,"Category"));
			DBRow[] differentStorage = productMgr.getIncorrectStorageByCidPsid(cid, ps_id,null);
			StringBuffer re = new StringBuffer("");
			for(int i = 0;i<differentStorage.length ;i++)
			{
//				re.append(differentStorage[i].getString("barcode"));
				re.append(differentStorage[i].get("pc_id",0l));
				re.append(",");
				re.append(differentStorage[i].getString("quantity").equals("")?0:differentStorage[i].getString("quantity"));
				re.append(",");
				re.append(differentStorage[i].getString("merge_count"));
				re.append(",");
				DBRow locations[] = productMgr.getStorageLocationByBarcode(ps_id,differentStorage[i].get("pc_id",0l),null);
				for(int j = 0;j<locations.length;j++)
				{
					re.append(locations[j].getString("position")+":"+locations[j].get("quantity",0f));
					if(j<locations.length-1)
					{
						re.append("+");
					}
				}
				if(!(locations.length>0))
				{
					re.append("NULL");
				}
				re.append("|");
			}
			return (re.toString());
		} 
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compareProductStorage",log);
		}
	}
	
	/**
	 * 条码机下载商品分类
	 * @return
	 * @throws Exception
	 */
	public String category()
		throws Exception
	{
		try 
		{
			StringBuffer re = new StringBuffer();
			DBRow[] catalog = catalogMgr.getProductCatalogTree();
			for (int i = 0; i < catalog.length; i++) 
			{
				re.append("<Details>");
				re.append("<Parent>"+catalog[i].getString("parentid")+"</Parent>");
				re.append("<Category>"+catalog[i].getString("title")+"</Category>");
				re.append("<ID>"+catalog[i].getString("id")+"</ID>");
				re.append("</Details>");
			}
			return (re.toString());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"category",log);
		}
	}
	
	/**
	 * 条码机下载仓库信息
	 * @return
	 * @throws Exception
	 */
	public String ware()
		throws Exception
	{	
		try 
		{
			StringBuffer re = new StringBuffer("<Details>");
			
			DBRow[] ware = catalogMgr.getProductStorageCatalogTree();
			for (int i = 0; i < ware.length; i++) 
			{
				re.append(ware[i].getString("title"));
				re.append("|");
			}
			re.append("</Details>");
			return (re.toString());
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ware",log);
		}
	}
	
	/**
	 * 获得使用者仓库位置
	 */
	public String location(long ps_id)
		throws Exception
	{
		try
		{
			StringBuffer re = new StringBuffer("");
			DBRow[] location = floorLocationMgrZJ.getLocationCatalogAreaByPsid(ps_id);
			
			for (int i = 0; i < location.length; i++)
			{
				re.append("<Details>");
				re.append("<Area>"+location[i].getString("area")+"</Area>");
				re.append("<Location>"+location[i].getString("slc_position_all")+"</Location>");
				re.append("</Details>");
			}
			return (re.toString());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"location",log);
		}
	}
	
	/**
	 * 获得所有商品信息
	 */
	public String product()
		throws Exception
	{
		try 
		{
			DBRow[] products = productMgr.getAllProducts(null);
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < products.length; i++)
			{
				re.append("<Details>");				
				
				String[] pnames = products[i].getString("p_name").split("/");
				if(pnames.length!=3)
				{
					re.append("<Name>"+products[i].getString("p_name")+"</Name>");
					re.append("<Specif>NULL</Specif>");
				}
				else
				{
					re.append("<Name>"+pnames[0]+"/"+pnames[1]+"</Name>");
					re.append("<Specif>"+pnames[2]+"</Specif>");
				}
				re.append("<Location>NULL</Location>");
				re.append("<PID>"+products[i].get("pc_id",0l)+"</PID>");
				re.append("<Length>"+products[i].get("length",0f)+"</Length>");
				re.append("<Width>"+products[i].get("width",0f)+"</Width>");
				re.append("<Heigth>"+products[i].get("heigth",0f)+"</Heigth>");
				re.append("<Weight>"+products[i].get("weight",0f)+"</Weight>");
				re.append("</Details>");
			}
			return (re.toString());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"product",log);
		}
	}
	
	/**
	 * 获得所有商品条码
	 * @return
	 * @throws Exception
	 */
	public String productCode()
		throws Exception
	{
		DBRow[] pcode = floorProductCodeMgrZJ.getAllProductCode();
		
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < pcode.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+pcode[i].get("pc_id",0l)+"</PCID>");
			re.append("<Barcode>"+pcode[i].getString("p_code")+"</Barcode>");
			if(pcode[i].get("code_type",0)==CodeTypeKey.Main)
			{
				re.append("<Show>1</Show>");
			}
			else
			{
				re.append("<Show>0</Show>");
			}
			
			re.append("</Details>");	
		}
		
		return (re.toString());
	}
	
	/**
	 * 商品入库
	 */
	public int inbondScan(String xml,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			ArrayList<AddInProductBean> list = new ArrayList<AddInProductBean>();
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			String[] inproducts = details.split("\\|");

			for (int i = 0; i < inproducts.length; i++) 
			{
				String[] inproduct = inproducts[i].split(",");
				if(inproducts[i].trim().equals("")||inproduct.length!=3)
				{
					throw new XMLDataErrorException();
				}
				AddInProductBean addInProductBean = new AddInProductBean();
				addInProductBean.setAdid(adminLoggerBean.getAdid());
				addInProductBean.setLogType(InOutStoreKey.IN_STORE_UPLOAD);
				addInProductBean.setPCode(inproduct[0]);
				addInProductBean.setPsId(adminLoggerBean.getPs_id());
				addInProductBean.setQuantity(Float.valueOf(inproduct[1]));
				addInProductBean.setStatFlag(0);
				if(!inproduct[2].equals("NULL"))
				{
					addInProductBean.setPosition(inproduct[2]);
				}
				list.add(addInProductBean);
			}
			
			TDate td = new TDate();
			long datemark = td.getDateTime();
			productMgr.addInProductSub(adminLoggerBean.getPs_id(), adminLoggerBean,list.toArray(new AddInProductBean[0]), datemark,machine_id);
			return (list.size());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	/**
	 * 采购单商品入库
	 * @param xml
	 * @param ps_id
	 * @param machine_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public int purchaseInbondScan(String xml,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			long purchase_id = Long.parseLong(getSampleNode(xml,"Purchase"));
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			String[] inproducts = details.split("\\|");
			for (int i = 0; i < inproducts.length; i++) 
			{
				String[] inproduct = inproducts[i].split(",");
				if(inproducts[i].trim().equals("")||inproduct.length!=3)
				{
					throw new XMLDataErrorException();
				}
				DBRow inpara = new DBRow();
				inpara.add("product_barcod", inproduct[0]);
				inpara.add("reap_count",inproduct[1]);
				if(!inproduct[2].equals("NULL"))
				{
					inpara.add("storage",inproduct[2]);
				}
				list.add(inpara);
			}
			
			purchaseMgr.incomingSub(list.toArray(new DBRow[0]),purchase_id,machine_id,adminLoggerBean);
			return (list.size());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NoPurchaseException e)
		{
			throw e;
		}
		catch(CanNotInbondScanException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"purchaseInbondScan",log);
		}
	}
	
	/**
	 * 上传待发货订单
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public HashMap waitingForDelivery(String xml,AdminLoginBean adminLoggerBean) 
		throws Exception
	{
		try 
		{
			String fc = "";
			boolean sub = true;
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			String[] orders = details.split("\\|");
			int count = 0;
			StringBuffer error = new StringBuffer(""); 
			long[] oids = new long[orders.length];
			for (int i = 0; i < orders.length; i++) 
			{
				count = i+1;
				String[] order = orders[i].split(",");
				
				long oid = Long.valueOf(order[0]);
				
				oids[i] = oid;
				
				DBRow paraOrder = floorOrderMgr.getDetailPOrderByOid(oid);
				if(paraOrder == null)
				{
					sub = false;
					error.append(oid+"|");
					fc = "noOrder";
				}
				
				if(paraOrder != null&&!paraOrder.getString("handle").equals(String.valueOf(HandleKey.PRINTED)))
				{
					sub = false;
					error.append(oid+"|");
					fc = "handle";
				}
			}
			
			if(sub)
			{
				orderMgrZJ.modOrdersHandle(oids,HandleKey.WAITING_DELIVERY);
			}
			
			HashMap map = new HashMap();
			map.put("postcount",count);
			map.put("error",error);
			map.put("fc",fc);
			
			return (map);
		} 
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"waitingForDelivery",log);
		}
	}
	
	
	/**
	 * 上传已发货订单
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 * @throws OrderNotPrintedException
	 */
	public HashMap shipped(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			String fc = "";
			boolean sub = true;
			TDate td = new TDate();
			long datemark = td.getDateTime();
			String dev_date = DateUtil.NowStr();
			String detail = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			ArrayList<AddShipProductBean> list = new ArrayList<AddShipProductBean>();
			StringBuffer error = new StringBuffer(""); 
			String[] orders = detail.split("\\|");
			
			if(!efficacy.equals(StringUtil.HashBase64(detail)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=detail.length())
			{
				throw new XMLDataLengthException();
			}
			
			for(int i = 0;i<orders.length;i++)
			{
				String[] order = orders[i].split(",");
				
				if(orders[i].trim().equals("")||order.length!=3)
				{
					throw new XMLDataErrorException();
				}
				long waybill_id = Long.valueOf(order[0]);
				
				DBRow paraOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
				
				if(paraOrder == null)
				{
					sub = false;
					error.append(waybill_id+"|");
					fc = "noOrder";
				}
				
				else if(paraOrder != null&&!paraOrder.getString("status").equals(String.valueOf(WayBillOrderStatusKey.PERINTED)))
				{
					sub = false;
					error.append(waybill_id+"|");
					fc = "handle";
				}
				
				else
				{
					String delivery_code = paraOrder.getString("tracking_number");
					long sc_id = paraOrder.get("sc_id",0l);  //expressMgr.getDetailCompanyByName(paraOrder.getString("shipping_name")).get("sc_id",0l);
					
					float weight = Float.parseFloat(order[1]);
					
					DBRow zone = expressMgr.getDetailZone(sc_id,paraOrder.get("ccid",0l),paraOrder.get("pro_id", 0l));
					String zoneName = zone.getString("name");
					
					AddShipProductBean addShipProductBean = new AddShipProductBean();
					addShipProductBean.setOid(waybill_id);
					addShipProductBean.setProductWeight(weight);
					addShipProductBean.setDeliveryAccount(adminLoggerBean.getAccount());
					addShipProductBean.setDeliveryCode(delivery_code);
					addShipProductBean.setDeliveryOperator(order[2]);
					addShipProductBean.setDeliveryto(zoneName);
					addShipProductBean.setScid(sc_id);
					
					
					list.add(addShipProductBean);
				}
			}
			
			if(sub)
			{
				productMgr.addShipProductSub(adminLoggerBean.getPs_id(), dev_date, datemark, adminLoggerBean,list.toArray(new AddShipProductBean[0]));	
			}
			
			HashMap map = new HashMap();
			map.put("postcount",list.size());
			map.put("error",error);
			map.put("fc",fc);
			
			return (map);
		} 
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"shipped",log);
		}
	}
	
	/**
	 * 下载该仓库下已起运交货单
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String deliveryDownload(String xml,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		
		try 
		{
			DBRow[] deliveryOrderDetails = deliveryMgrZJ.getDeliveryOrdersByPsStatus(DeliveryOrderKey.INTRANSIT,adminLoggerBean.getPs_id());
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < deliveryOrderDetails.length;i++)
			{
				re.append("<Details>");
				re.append("<Delivery>"+deliveryOrderDetails[i].getString("number")+"</Delivery>");
//				re.append("<Barcode>"+deliveryOrderDetails[i].getString("product_barcode")+"</Barcode>");
				re.append("<PID>"+deliveryOrderDetails[i].getString("product_id")+"</PID>");
				re.append("<Qty>"+deliveryOrderDetails[i].getString("delivery_count")+"</Qty>");
			    re.append("</Details>");
			}
			return (re.toString());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 交货比较
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String compareDelivery(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			String machine_id = getSampleNode(xml,"Machine");
			String delivery_order_number = getSampleNode(xml,"Delivery");
			DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrderByDeliveryNumber(delivery_order_number);
			
			if(delivery_order == null)
			{
				throw new NoExistDeliveryOrderException();
			}
			else
			{
				if(delivery_order.get("delivery_order_status",0) != DeliveryOrderKey.INTRANSIT)
				{
					throw new DeliveryOrderCanNotWareHouseException();
				}
			}
			
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入伪交货
			String[] deliveryWarehouses = details.split("\\|");
			
			for (int i = 0; i < deliveryWarehouses.length; i++) 
			{
				String[] deliveryWarehouse = deliveryWarehouses[i].split(",");
				
				if(deliveryWarehouses[i].trim().equals("")||deliveryWarehouse.length!=3)
				{
					throw new XMLDataErrorException();
				}
				
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(deliveryWarehouse[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				DBRow db = new DBRow();
				db.add("dw_deliveryOrder_number",delivery_order_number);
				db.add("dw_product_id",product.get("pc_id",0l));
				db.add("dw_product_name",product.getString("p_name"));
				db.add("dw_product_barcode",product.getString("p_code"));
				db.add("dw_count",deliveryWarehouse[1]);
				db.add("dw_deliveryOrder_id",delivery_order.get("delivery_order_id",0l));
				db.add("machine_id",machine_id);
				if(!deliveryWarehouse[2].equals("NULL"))
				{
					db.add("dw_storage_location",deliveryWarehouse[3]);
				}
				
				
				list.add(db);
			}
			
			deliveryWarehouseMgrZJ.addDeliveryWareHouseSub(list.toArray(new DBRow[0]), delivery_order_number);
			
			//伪交货比较
			DBRow[] compare = deliveryWarehouseMgrZJ.compareDeliveryByDeliveryOrderNumber(delivery_order_number);
			
			//返回比较结果
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < compare.length; i++) 
			{
				
				re.append("<Details>");
				re.append("<Barcode>"+compare[i].getString("dw_product_barcode")+"</Barcode>");
				re.append("<Name>"+compare[i].getString("dw_product_name")+"</Name>");
				re.append("<DeliveryCount>"+compare[i].getString("dcount")+"</DeliveryCount>");
				re.append("<ReapCount>"+compare[i].getString("dw_count")+"</ReapCount>");
				re.append("</Details>");
			}
			
			return (re.toString());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(DeliveryOrderCanNotWareHouseException e)
		{
			throw e;
		}
		catch(NoExistDeliveryOrderException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 交货确认
	 * @param xml
	 * @throws Exception
	 */
	public void deliverySuccess(String xml,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{
		try 
		{
			String delivery_number = getSampleNode(xml,"Delivery");
			
			DBRow delivery_order = deliveryMgrZJ.getDetailDeliveryOrderByDeliveryNumber(delivery_number);
			
			if(delivery_order.get("delivery_order_status",0) != DeliveryOrderKey.INTRANSIT)
			{
				throw new DeliveryOrderHasWareHouseException();
			}
				
			DBRow[] deliveryOrderDetails = deliveryWarehouseMgrZJ.getDeliveryByDeliveryOrderId(delivery_order.get("delivery_order_id",0l));
			
			deliveryMgrZJ.deliveryWarehousingSub(delivery_order.get("delivery_order_id",0l), deliveryOrderDetails,adminLoggerBean,machine_id);
			
			//deliveryWarehouseMgrZJ.delDeliveryWareHouseByDeliveryOrderNumber(delivery_number);//清除虚拟交货数据
			
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(DeliveryOrderHasWareHouseException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运发货比较
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String compareTransportPacking(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			String machine_id = getSampleNode(xml,"Machine");
			
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
			else
			{
				if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
				{
					throw new TransportOrderCanNotOutboundException();
				}
			}
			
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入发货明细
			String[] transportOutbounds = details.split("\\|");
			
			for (int i = 0; i < transportOutbounds.length; i++) 
			{
				String[] transportOutbound = transportOutbounds[i].split(",");
				
				if(transportOutbounds[i].trim().equals("")||transportOutbound.length!=4)
				{
					throw new XMLDataErrorException();
				}
				
				String lp = transportOutbound[3];
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				String sn = transportOutbound[2];
				
				long lp_id = 0;
				if(contianer!=null)
				{
					lp_id = contianer.get("con_id",0l);
				}
				
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(transportOutbound[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				DBRow db = new DBRow();
				db.add("to_transport_id",transport_id);
				db.add("to_pc_id",product.get("pc_id",0l));
				db.add("to_p_name",product.getString("p_name"));
				db.add("to_p_code",product.getString("p_code"));
				db.add("to_count",transportOutbound[1]);
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				db.add("to_machine_id",machine_id);
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				
				if(lp_id!=0)
				{
					db.add("to_lp_id",lp_id);
				}
				
				if(!sn.equals(""))
				{
					db.add("to_serial_number",sn);
				}
				
				list.add(db);
			}
			
			transportOutboundMgrZJ.addTransportOutboundSub(list.toArray(new DBRow[0]),transport_id,machine_id);
			
			//发货比较
			DBRow[] compare = transportOutboundMgrZJ.compareTransportOutboundByTransportId(transport_id);
			
			//返回比较结果
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < compare.length; i++) 
			{
				
				re.append("<Details>");
//				re.append("<Barcode>"+compare[i].getString("p_code")+"</Barcode>");
				re.append("<PID>"+compare[i].getString("pc_id")+"</PID>");
//				re.append("<Name>"+compare[i].getString("p_name")+"</Name>");
				re.append("<TransportCount>"+compare[i].getString("transport_count")+"</TransportCount>");
				re.append("<SendCount>"+compare[i].getString("real_send_count")+"</SendCount>");
				re.append("</Details>");
			}
			
			return (re.toString());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(TransportOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch(NoExistTransportOrderException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运交货比较
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String compareTransport(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
//			String ware = getSampleNode(xml,"Ware");
			String machine_id = getSampleNode(xml,"Machine");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
			else
			{
				if((transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL))
				{
					throw new TransportOrderCanNotWareHouseException();
				}
			}
			
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入伪交货
			String[] transportWarehouses = details.split("\\|");
			
			for (int i = 0; i < transportWarehouses.length; i++) 
			{
				String[] transportWarehouse = transportWarehouses[i].split(",");
				
				if(transportWarehouses[i].trim().equals("")||transportWarehouse.length!=4)
				{
					throw new XMLDataErrorException();
				}
				
				//通过条码机上传的ID查询商品
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(transportWarehouse[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				String lp = transportWarehouse[1];
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				
				
				String sn = transportWarehouse[2];
				
				long lp_id = 0;
				if(contianer!=null)
				{
					lp_id = contianer.get("con_id",0l);
				}
				else
				{
					lp_id = Long.parseLong(lp);
				}
				
				DBRow db = new DBRow();
				db.add("tw_transport_id",transport_id);
				db.add("tw_product_id",product.get("pc_id",0l));
				db.add("tw_product_name",product.getString("p_name"));
				db.add("tw_product_barcode",product.getString("p_code"));
				db.add("tw_count",transportWarehouse[3]);
				db.add("machine_id",machine_id);
				if(lp_id!=0)
				{
					db.add("tw_lp_id",lp_id);
				}
				
				if(!sn.equals("NULL"))
				{
					db.add("tw_serial_number",sn);
				}
				
				list.add(db);
			}
			
			transportWarehouseMgrZJ.addTransportWareHouseSub(list.toArray(new DBRow[0]),transport_id,machine_id);
			
			//伪交货比较
			DBRow[] compare = transportWarehouseMgrZJ.compareTransportByTransportId(transport_id);
			
			//返回比较结果
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < compare.length; i++) 
			{
				
				re.append("<Details>");
//				re.append("<Barcode>"+compare[i].getString("p_code")+"</Barcode>");
				re.append("<PID>"+compare[i].getString("pc_id")+"</PID>");//新加商品ID返回
				re.append("<Name>"+compare[i].getString("p_name")+"</Name>");
				re.append("<SendCount>"+compare[i].getString("transport_send_count")+"</SendCount>");
				re.append("<ReapCount>"+compare[i].getString("real_count")+"</ReapCount>");
				re.append("</Details>");
			}
			
			return (re.toString());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(TransportOrderCanNotWareHouseException e)
		{
			throw e;
		}
		catch(NoExistTransportOrderException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public int transportReceiveAndroid(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
	//		String ware = getSampleNode(xml,"Ware");
			String machine_id = getSampleNode(xml,"Machine");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
			else
			{
				if((transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0) != TransportOrderKey.RECEIVEING))
				{
					throw new TransportOrderCanNotWareHouseException();
				}
			}
			
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入伪交货
			String[] transportWarehouses = details.split("\\|");
			
			for (int i = 0; i < transportWarehouses.length; i++) 
			{
				String[] transportWarehouse = transportWarehouses[i].split(",");
				
				if(transportWarehouses[i].trim().equals("")||transportWarehouse.length!= 5)
				{
					throw new XMLDataErrorException();
				}
				String sn = transportWarehouse[3];
				
				DBRow[] repeatSN =  transportWarehouseMgrZJ.getTransportWareHouseBySNNotMachine(sn, machine_id, transport_id);
				
				if(repeatSN.length>0)
				{
					throw new ReceiveProductSNRepeatException();
				}
			}
			
			for (int i = 0; i < transportWarehouses.length; i++) 
			{
				String[] transportWarehouse = transportWarehouses[i].split(",");
				
				if(transportWarehouses[i].trim().equals("")||transportWarehouse.length!=5)
				{
					throw new XMLDataErrorException();
				}
				//pcid,qty,lp,sn
				//通过条码机上传的ID查询商品
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(transportWarehouse[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				String lp = transportWarehouse[2];
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				
				
				String sn = transportWarehouse[3];
				
				long lp_id = 0;
				if(contianer!=null)
				{
					lp_id = contianer.get("con_id",0l);
				}
//				else
//				{
//					lp_id = Long.parseLong(lp);
//				}
				
				DBRow db = new DBRow();
				db.add("tw_transport_id",transport_id);
				db.add("tw_product_id",product.get("pc_id",0l));
				db.add("tw_product_name",product.getString("p_name"));
				db.add("tw_product_barcode",product.getString("p_code"));
 				db.add("tw_count",transportWarehouse[1]);
 				db.add("tw_lot_number",transportWarehouse[4]);
				db.add("machine_id",machine_id);
				if(lp_id!=0)
				{
					db.add("tw_lp_id",lp_id);
				}
				
				if(!sn.equals("NULL"))
				{
					db.add("tw_serial_number",sn);
					
					//序列号放货
					//serialNumberMgrZJ.putSerialProduct(sn, product.get("pc_id",0l));
				}
				
				list.add(db);
			}
			
			transportWarehouseMgrZJ.addTransportWareHouseSub(list.toArray(new DBRow[0]),transport_id,machine_id);
			
			//张睿(添加在收货完成的时候自动的释放门的操作)
			androidMgrZr.clearDoorAndLocation(transport_id, TransportRegistrationTypeKey.DELEIVER);

			return list.size();
			
		}
		catch (ReceiveProductSNRepeatException e) 
		{
			throw e;
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(TransportOrderCanNotWareHouseException e)
		{
			throw e;
		}
		catch(NoExistTransportOrderException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运收货序列号重复
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String transportReciveSerialNumberRepeat(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
//		String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if((transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0) != TransportOrderKey.RECEIVEING))
			{
				throw new TransportOrderCanNotWareHouseException();
			}
		}
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		ArrayList<String> list = new ArrayList<String>();
		//存入伪交货
		String[] transportWarehouses = details.split("\\|");
		
		for (int i = 0; i < transportWarehouses.length; i++) 
		{
			String[] transportWarehouse = transportWarehouses[i].split(",");
			
			if(transportWarehouses[i].trim().equals("")||transportWarehouse.length!=5)
			{
				throw new XMLDataErrorException();
			}
			String sn = transportWarehouse[3];
			
			DBRow[] repeatSN =  transportWarehouseMgrZJ.getTransportWareHouseBySNNotMachine(sn, machine_id, transport_id);
			if (repeatSN.length>0) 
			{
				list.add(sn);
			}
			
		}
		
		StringBuffer re = new StringBuffer("<RSN>");
		for (int i = 0; i < list.size();i++) 
		{
			if(i!=0)
			{
				re.append("|");
			}
			re.append(list.get(i));
		}
		re.append("</RSN>");
		
		return re.toString();
	}
	
	public String transportReciveCompare(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
//		String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if((transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0)!=TransportOrderKey.RECEIVEING))
			{
				throw new TransportOrderCanNotWareHouseException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		//收货比较
		StringBuffer re = new StringBuffer();
		DBRow[] compare = transportWarehouseMgrZJ.compareTransportOutWareByTransportId(transport_id);
		for (int i = 0; i < compare.length; i++) 
		{
				
			re.append("<Different>");
			re.append("<PID>"+compare[i].getString("pc_id")+"</PID>");//新加商品ID返回
			re.append("<MachineId>"+compare[i].getString("machine_id")+"</MachineId>");
			re.append("<DifferentType>"+compare[i].getString("different_type")+"</DifferentType>");
			re.append("<SN>"+compare[i].getString("serial_number")+"</SN>");
			re.append("</Different>");
		}
		
		//单据整体收货
		DBRow[] warehouses = transportWarehouseMgrZJ.getTransportWareHouseByTransportId(transport_id);
		
		for (int i = 0; i < warehouses.length; i++) 
		{
			re.append("<WareHouse>");
			re.append("<PID>"+warehouses[i].getString("tw_product_id")+"</PID>");
			re.append("<SN>"+warehouses[i].getString("tw_serial_number","NULL")+"</SN>");
			re.append("<Qty>"+warehouses[i].get("tw_count",0f)+"</Qty>");
			re.append("<Machine>"+warehouses[i].getString("machine_id")+"</Machine>");
			re.append("</WareHouse>");
		}
		
		return re.toString();
	}
	
	public String transportReceiveCompleteResult(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
//		String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if((transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0) != TransportOrderKey.RECEIVEING))
			{
				throw new TransportOrderCanNotWareHouseException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		int compareResult = TransportCompareKey.Consistent;
		
		DBRow[] compareSN = transportWarehouseMgrZJ.compareTransportOutWareByTransportId(transport_id);
		DBRow[] compareCount = transportWarehouseMgrZJ.transportReceiveCompareCount(transport_id);
		
		if (compareSN.length>0&&compareCount.length==0)
		{
			compareResult = TransportCompareKey.SnDifferent;
		}
		else if(compareSN.length==0&&compareCount.length>0)
		{
			compareResult = TransportCompareKey.CountDifferent;
		}
		else if(compareSN.length>0&&compareCount.length>0)
		{
			compareResult = TransportCompareKey.AllDifferent;
		}
		
		String re = new String("<ReceiveResult>"+compareResult+"</ReceiveResult>");

		return re;
	}
	
	/**
	 * 收货放货差异比较
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String transportReceivePutLocationCompare(String xml)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
//			String ware = getSampleNode(xml,"Ware");
			String machine_id = getSampleNode(xml,"Machine");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
//			else
//			{
//				if((transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0) != TransportOrderKey.AlREADYRECEIVE)&&(transport.get("transport_status",0) != TransportOrderKey.RECEIVEING))
//				{
//					throw new TransportOrderCanNotWareHouseException();
//				}
//			}
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			DBRow[] transportReceivePutCompare = transportWarehouseMgrZJ.getTransportWareHousePutLocationCompare(transport_id);
			
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < transportReceivePutCompare.length; i++) 
			{
					re.append("<Different>");
					re.append("<PID>"+transportReceivePutCompare[i].getString("pc_id")+"</PID>");//新加商品ID返回
					re.append("<MachineId>"+transportReceivePutCompare[i].getString("machine_id")+"</MachineId>");
					re.append("<DifferentType>"+transportReceivePutCompare[i].getString("different_type")+"</DifferentType>");
					re.append("<SN>"+transportReceivePutCompare[i].getString("serial_number","NULL")+"</SN>");
					re.append("</Different>");
			}
			
			DBRow[] transportReceivePutLog = productStoreLocationMgrZJ.getProductStorePhysicaLogLayUpForTransport(transport_id);
			
			for (int i = 0; i < transportReceivePutLog.length; i++) 
			{
				re.append("<PutLog>");
				re.append("<PID>"+transportReceivePutLog[i].getString("pc_id")+"</PID>");//新加商品ID返回
				re.append("<MachineId>"+transportReceivePutLog[i].getString("operation_machine_id")+"</MachineId>");
				re.append("<Qty>"+transportReceivePutLog[i].getString("quantity")+"</Qty>");
				re.append("<SN>"+transportReceivePutLog[i].getString("serial_number")+"</SN>");
				re.append("</PutLog>");
			}
			
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"transportReceivePutLocationCompare",log);
		}
	}
	
	
	/**
	 * 下载目的仓库是登录账户所属仓库的运输中的转运单
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String transportDownload(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		long receive_psid = adminLoggerBean.getPs_id();//获得仓库ID
		DBRow[] transportOrderDetails = transportMgrZJ.getTransportDetailByPsWithStatus(0, receive_psid,TransportOrderKey.CANINSTORE);
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < transportOrderDetails.length; i++)
		{
			re.append("<Details>");
			re.append("<Transport>"+transportOrderDetails[i].getString("transport_id")+"</Transport>");
//			re.append("<Barcode>"+transportOrderDetails[i].getString("transport_p_code")+"</Barcode>");
			re.append("<PID>"+transportOrderDetails[i].getString("transport_pc_id")+"</PID>");
			re.append("<SendQty>"+transportOrderDetails[i].getString("transport_send_count")+"</SendQty>");
		    re.append("</Details>");
		}
		return (re.toString());
	}
	
	/**
	 * 下载转运仓库是登录账户所属仓库的装箱中的转运单
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String transportPacking(String xml,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		long send_psid = adminLoggerBean.getPs_id();//获得仓库ID
		DBRow[] transportOrderDetails = transportMgrZJ.getTransportDetailByPsWithStatus(send_psid,0,TransportOrderKey.PACKING);
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < transportOrderDetails.length; i++)
		{
			re.append("<Details>");
			re.append("<Transport>"+transportOrderDetails[i].getString("transport_id")+"</Transport>");
//			re.append("<Barcode>"+transportOrderDetails[i].getString("transport_p_code")+"</Barcode>");
			re.append("<PID>"+transportOrderDetails[i].getString("transport_pc_id")+"</PID>");
			re.append("<Qty>"+transportOrderDetails[i].getString("transport_count")+"</Qty>");
		    re.append("</Details>");
		}
		return (re.toString());
	}
	
	/**
	 * 确认转运到货
	 * @param xml
	 * @param adminLoggerBean
	 * @param machine_id
	 * @throws Exception
	 */
	public void transportSuccess(String xml,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport.get("transport_status",0) != TransportOrderKey.INTRANSIT&&transport.get("transport_status",0)!=TransportOrderKey.AlREADYARRIAL)
		{
			throw new DeliveryOrderHasWareHouseException();
		}
			
		DBRow[] transportDetailsWarehouse = transportWarehouseMgrZJ.getTransportWareHouseByTransportId(transport_id);
		
		transportMgrZJ.transportWarehousingSub(transport_id, transportDetailsWarehouse, adminLoggerBean, machine_id);
		
		//transportWarehouseMgrZJ.delTransportWareHouseByTransportId(transport_id);//清除虚拟交货数据
		
	}
	

	/**
	 * 确认转运起运
	 * @param xml
	 * @param adminLoggerBean
	 * @param machine_id
	 * @throws Exception
	 */
	public void transportDelivery(String xml, AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception 
	{
		
		try 
		{
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
			{
				throw new TransportOrderCanNotOutboundException();
			}
				
			transportMgrZJ.deliveryTransport(transport_id,adminLoggerBean);//转运单已起运
			
			transportOutboundApproveMgrZJ.addTransportOutboundApproveSub(transport_id, adminLoggerBean);//创建转运出库审核
			
			//差异回退库存
//			transportMgrZJ.rebackDiffTransport(transport_id, adminLoggerBean);
			//差异装箱
//			transportMgrZJ.packingDiffTransport(transport_id, adminLoggerBean);
//			
//			transportOutboundMgrZJ.delTransportOutboundByTransportId(transport_id);//清除转运发货数据
		} 
		catch(TransportOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 根据登录账号所属仓库下载该仓库下所有装箱中的返修单明细
	 */
	public String damagedRepairPacking(String xml,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		long send_psid = adminLoggerBean.getPs_id();//获得仓库ID
		DBRow[] damagedRepairDetails = damagedRepairMgrZJ.getDamagedRepairDetailByPsWithStatus(send_psid, 0,DamagedRepairKey.PACKING);
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < damagedRepairDetails.length; i++)
		{
			re.append("<Details>");
			re.append("<Return>"+damagedRepairDetails[i].getString("repair_id")+"</Return>");
			re.append("<Barcode>"+damagedRepairDetails[i].getString("repair_p_code")+"</Barcode>");
			re.append("<PID>"+damagedRepairDetails[i].getString("repair_pc_id")+"</PID>");
			re.append("<Qty>"+damagedRepairDetails[i].getString("repair_count")+"</Qty>");
		    re.append("</Details>");
		}
		return (re.toString());
	}
	
	/**
	 * 返修装箱比较（附带上传）
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public String compareDamagedRepairPacking(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String ware = getSampleNode(xml,"Ware");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			
			long repair_id = Long.parseLong(getSampleNode(xml,"Return"));
			DBRow damagedRepair = damagedRepairMgrZJ.getDetailDamagedRepair(repair_id);
			
			if(damagedRepair == null)
			{
				throw new NoExistDamagedRepairOrderException();
			}
			else
			{
				if(damagedRepair.get("repair_status",0) != DamagedRepairKey.PACKING)
				{
					throw new DamagedRepairOrderCanNotOutboundException();
				}
			}
			
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			//damagedRepairOutboundMgrZJ.delDamagedRepairOutboundByDamagedRepairId(repair_id);//清除以前的发货明细
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入发货明细
			String[] damagedRepairOutbounds = details.split("\\|");
			
			for (int i = 0; i < damagedRepairOutbounds.length; i++) 
			{
				String[] damagedRepairOutbound = damagedRepairOutbounds[i].split(",");
				
				if(damagedRepairOutbounds[i].trim().equals("")||damagedRepairOutbound.length!=2)
				{
					throw new XMLDataErrorException();
				}
				
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(damagedRepairOutbound[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				DBRow db = new DBRow();
				db.add("dro_repair_id",repair_id);
				db.add("dro_pc_id",product.get("pc_id",0l));
				db.add("dro_p_name",product.getString("p_name"));
				db.add("dro_p_code",product.getString("p_code"));
				db.add("dro_count",damagedRepairOutbound[1]);
				
				list.add(db);
			}
			
			damagedRepairOutboundMgrZJ.addDamagedRepairOutboundSub(list.toArray(new DBRow[0]), repair_id);
			
			//transportOutboundMgrZJ.addTransportOutboundSub(list.toArray(new DBRow[0]), transport_id);
			
			//发货比较
			DBRow[] compare = damagedRepairOutboundMgrZJ.compareDamagedRepairOutboundByDamagedRepairId(repair_id);
			
			//返回比较结果
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < compare.length; i++) 
			{
				
				re.append("<Details>");
				re.append("<Barcode>"+compare[i].getString("p_code")+"</Barcode>");
				re.append("<Name>"+compare[i].getString("p_name")+"</Name>");
				re.append("<ReturnCount>"+compare[i].getString("repair_count")+"</ReturnCount>");
				re.append("<SendCount>"+compare[i].getString("real_send_count")+"</SendCount>");
				re.append("</Details>");
			}
			
			return (re.toString());
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(DamagedRepairOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch(NoExistDamagedRepairOrderException e)
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 返修单确认起运
	 * @param xml
	 * @param adminLoggerBean
	 * @param machine_id
	 * @throws Exception
	 */
	public void damagedRepairDelivery(String xml, AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception 
	{
		
		try 
		{
			long repair_id = Long.parseLong(getSampleNode(xml,"Return"));
			
			DBRow damagedRepair = damagedRepairMgrZJ.getDetailDamagedRepair(repair_id);
			
			if(damagedRepair.get("repair_status",0) != DamagedRepairKey.PACKING)
			{
				throw new DamagedRepairOrderCanNotOutboundException();
			}
				
			damagedRepairMgrZJ.deliveryDamagedRepair(repair_id);
			
			damagedRepairOutboundApproveMgrZJ.addDamagedRepairOutboundApproveSub(repair_id, adminLoggerBean);//创建返修出库审核
			
			damagedRepairOutboundMgrZJ.delDamagedRepairOutboundByDamagedRepairId(repair_id);//清除返修发货数据
		} 
		catch(DamagedRepairOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	//----------------------------------------------------------------------------
	/**
	 * 获得节点数据（只适合唯一名称节点）
	 * @return
	 */
	public String getSampleNode(String xml,String name)
		throws Exception
	{
		try 
		{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");

			return(xmlSplit2[0]);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new NetWorkException();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getSampleNode",log);
		}
	}
	
	/**
	 * 根据仓库ID获得拣货单详细，返回接口内容
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutStoreBillOrderDetailByPsid(long ps_id)
		throws Exception
	{
		StringBuffer returnXML = new StringBuffer("");
		DBRow[] outStoreBillOrders = floorOutboundOrderMgrZJ.getOutStoreOrderByPsid(ps_id);
		//
	
		
		for (int i = 0; i < outStoreBillOrders.length; i++) 
		{	
			returnXML.append("<Details>");
			String door="";
			String location="";
			DBRow[] doorOrLocation=floorOutboundOrderMgrZJ.getDoorLocationByOutId(outStoreBillOrders[i].get("out_id",0l));
			for(int a=0;a<doorOrLocation.length;a++)
			{
				if(doorOrLocation[a].get("occupancy_type", 0l)==1)
				{
					DBRow doorRow=floorOutboundOrderMgrZJ.getDoorName(doorOrLocation[a].get("rl_id", 0l));
						door+=doorRow.getString("doorId")+",";
				}
				else
				{
					DBRow locationRow=floorOutboundOrderMgrZJ.getLocationName(doorOrLocation[a].get("rl_id", 0l));
					location+=locationRow.getString("location_name")+",";

				}
			}
			if(door!=""){
				door=door.substring(0,door.length()-1);
			}
			if(location!=""){
				location=location.substring(0,location.length()-1);
			}
			
			returnXML.append("<Out>"+outStoreBillOrders[i].get("out_id",0l)+"</Out>");
			returnXML.append("<DoorName>"+door+"</DoorName>");
			returnXML.append("<LocationName>"+location+"</LocationName>");
			returnXML.append("</Details>");
		}
		
		
		return returnXML.toString();
	}
	
	/**
	 * 拣货单拣货
	 * @param xml
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public String pickUpOut(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			String machine_id = getSampleNode(xml,"Machine");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			

			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			long out_id = Long.parseLong(getSampleNode(xml,"Out"));
			String locationXML = StringUtil.getSampleNode(xml,"Location");
			String xmlPickUpProduct = StringUtil.getSampleNode(xml,"PickUpProduct");
			String foType = StringUtil.getSampleNode(xml,"FOType");
			String returnNeed = StringUtil.getSampleNode(xml,"RN");
			
			
			
			DBRow outOrder = floorOutboundOrderMgrZJ.getDetailOutboundById(out_id);
			long ps_id = outOrder.get("ps_id",0l);
			DBRow location = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(ps_id,locationXML);
			long slc_id = location.get("slc_id",0l);
			
			
			String[] pickUpProductsString = xmlPickUpProduct.split("\\|");
			
			for (int i = 0; i < pickUpProductsString.length; i++)
			{
				String[] pickUpProductString = pickUpProductsString[i].split(",");
				
				DBRow pickUpProduct = new DBRow();
				
				long pc_id = Long.parseLong(pickUpProductString[0]);
				float pick_up_quantity = Float.parseFloat(pickUpProductString[1]);
				
				pickUpProduct.add("pc_id",pc_id);
				pickUpProduct.add("pick_quantity",pick_up_quantity);
				
				long lp_id = 0;
				if(!pickUpProductString[2].equals("NULL"))
				{	
					lp_id= Long.parseLong(pickUpProductString[2]);
				}
				
				String sn = "";
				if(!pickUpProductString[3].equals("NULL"))
				{	
					 sn = pickUpProductString[3];
				}
				
				int from_container_type = Integer.parseInt(pickUpProductString[4]);
				long from_container_type_id = Long.parseLong(pickUpProductString[5]);
				long from_con_id = Long.parseLong(pickUpProductString[6]);
				int pick_container_type = Integer.parseInt(pickUpProductString[7]);
				long pick_container_type_id = Long.parseLong(pickUpProductString[8]);
				long pick_con_id = Long.parseLong(pickUpProductString[9]);
				long operation_con_id = Long.parseLong(pickUpProductString[10]);
				productStoreMgrZJ.pickUpPhysical(ps_id,slc_id,pc_id,pick_up_quantity,adminLoggerBean,out_id,sn,machine_id,foType,from_container_type,from_container_type_id,from_con_id,pick_container_type,pick_container_type_id,pick_con_id,operation_con_id);
				//productStoreLocationMgrZJ.pickUpPhysical(ps_id,slc_id,pc_id,pick_up_quantity,adminLoggerBean.getAdid(),out_id,sn,lp_id,machine_id,foType,from_container_type,from_container_type_id,from_con_id,pick_container_type,pick_container_type_id,pick_con_id);
			}
			
			
			StringBuffer re = new StringBuffer();
			if (returnNeed.equals("1")) 
			{
				DBRow[] locationProducts = floorOutboundOrderMgrZJ.getLocationProductForOut(location.get("slc_id",0l), out_id);
				for (int i = 0; i < locationProducts.length; i++)
				{
					re.append("<Details>");
					re.append("<PcId>"+locationProducts[i].get("out_list_pc_id",0l)+"</PcId>");
					re.append("<SlcId>"+locationProducts[i].get("slc_id",0l)+"</SlcId>");
					re.append("<NeedPickQuantity>"+locationProducts[i].get("need_execut_quantity",0f)+"</NeedPickQuantity>");
					re.append("<Location>"+locationProducts[i].getString("slc_position_all")+"</Location>");
					re.append("<From_Container_Type>"+locationProducts[i].get("from_container_type",0)+"</From_Container_Type>");
					re.append("<From_Container_Type_id>"+locationProducts[i].get("from_container_type_id",0l)+"</From_Container_Type_id>");
					re.append("<From_Con_id>"+locationProducts[i].get("from_con_id",0l)+"</From_Con_id>");
					re.append("<Pick_Container_Type>"+locationProducts[i].get("Pick_Container_Type",0)+"</Pick_Container_Type>");
					re.append("<Pick_Container_Type_id>"+locationProducts[i].get("Pick_Container_Type_id",0l)+"</Pick_Container_Type_id>");
					re.append("<Pick_Con_id>"+locationProducts[i].get("Pick_Con_id",0l)+"</Pick_Con_id>");
					re.append("</Details>");
				}
			}

			return re.toString();
		} 
		catch (PickUpCountMoreException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"pickUpOut",log);
		}
	}
	
	/**
	 * 放货
	 */
	public int putProductToLocation(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		String machine_id = getSampleNode(xml,"Machine");

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		long system_bill_id = Long.parseLong(StringUtil.getSampleNode(xml, "Bill"));
		int system_bill_type = Integer.parseInt(StringUtil.getSampleNode(xml, "BillType"));
		String locationXML = StringUtil.getSampleNode(xml,"Location");
		String xmlPutProduct = StringUtil.getSampleNode(xml,"PutProduct");
		
		String[] putProductsString = xmlPutProduct.split("\\|");
		
		for (int i = 0; i < putProductsString.length; i++)
		{
			String[] putProductString = putProductsString[i].split(",");
			
			long pc_id = Long.parseLong(putProductString[0]);
			float put_quantity = Float.parseFloat(putProductString[1]);
			
			long lp_id = 0;
			String serial_number = "";
			if(!putProductString[2].equals("NULL"))
			{
				serial_number = putProductString[2];
			}
			
//			if(!putProductString[3].equals("NULL"))
//			{	
//				DBRow container = containerProductMgrZJ.getDetailContainer(putProductString[3]);
//				if(container!=null)
//				{
//					lp_id = container.get("con_id",0l);
//				}
//			}
			
			lp_id = Long.parseLong(putProductString[3]);
			
			long title_id = Long.parseLong(putProductString[4]);
			String lot_number = putProductString[5];
			
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(system_bill_id);
			
			
			int operation = 0;
			
			if(system_bill_type == ProductStoreBillKey.TRANSPORT_ORDER)
			{
				DBRow transport = transportMgrZJ.getDetailTransportById(system_bill_id);
				
				if(transport.get("put_status",0)!=TransportPutKey.Puting)
				{
					throw new CanNotPutProductException();
				}
					
				
				if (transport.get("purchase_id",0l)==0) 
				{
					system_bill_type = ProductStoreBillKey.DELIVERY_ORDER;
					operation = ProductStoreOperationKey.IN_STORE_DELIVERY;
				}
				else
				{
					operation = ProductStoreOperationKey.IN_STORE_TRANSPORT;
				}
			}
			
			inProductStoreLogBean.setBill_type(system_bill_type);
			inProductStoreLogBean.setOperation(operation);
			
			productStoreMgrZJ.putProductToLocation(adminLoggerBean.getPs_id(), pc_id,put_quantity, system_bill_id, system_bill_type,adminLoggerBean,locationXML,serial_number,lp_id,title_id,lot_number,machine_id,inProductStoreLogBean);
		}
		
		return putProductsString.length;
	}
	
	public String getTransportWareHouse(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
//		String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if((transport.get("transport_status",0) != TransportOrderKey.APPROVEING)&&(transport.get("transport_status",0) != TransportOrderKey.RECEIVEING)&&(transport.get("transport_status",0) != TransportOrderKey.INTRANSIT)&&(transport.get("transport_status",0)!=TransportOrderKey.AlREADYARRIAL)&&(transport.get("transport_status",0)!= TransportOrderKey.AlREADYRECEIVE))
			{
				throw new TransportOrderCanNotWareHouseException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		StringBuffer re = new StringBuffer("");
		//单据整体收货
		DBRow[] warehouses = transportWarehouseMgrZJ.getTransportWareHouseByTransportId(transport_id);
		
		for (int i = 0; i < warehouses.length; i++) 
		{
			re.append("<WareHouse>");
			re.append("<PID>"+warehouses[i].getString("tw_product_id")+"</PID>");
			re.append("<SN>"+warehouses[i].getString("tw_serial_number","NULL")+"</SN>");
			re.append("<Qty>"+warehouses[i].get("tw_count",0f)+"</Qty>");
			//张睿添加其他的信息
			re.append("<ToId>"+warehouses[i].get("tw_id",0l)+"</ToId>") ;
			re.append("<ToPName>"+warehouses[i].getString("tw_product_name")+"</ToPName>") ;
			re.append("<ToPCode>"+warehouses[i].getString("tw_product_barcode")+"</ToPCode>") ;
			//end
 //			re.append("<Machine>"+warehouses[i].getString("machine_id")+"</Machine>");
			re.append("</WareHouse>");
		}
		
		return re.toString();
	}
	
	public String alreadyPutTransport(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		String machine_id = getSampleNode(xml,"Machine");

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		long system_bill_id = Long.parseLong(StringUtil.getSampleNode(xml, "Bill"));
		int system_bill_type = Integer.parseInt(StringUtil.getSampleNode(xml, "BillType"));
		
		DBRow[] transportReceivePutLog = productStoreLocationMgrZJ.getProductStorePhysicaLogLayUpForTransport(system_bill_id);
		
		StringBuffer re = new StringBuffer();
		for (int i = 0; i < transportReceivePutLog.length; i++) 
		{
			re.append("<PutLog>");
			re.append("<PID>"+transportReceivePutLog[i].getString("pc_id")+"</PID>");//新加商品ID返回
			re.append("<Location>"+transportReceivePutLog[i].getString("slc_position_all")+"</Location>");
			re.append("<MachineId>"+transportReceivePutLog[i].getString("operation_machine_id")+"</MachineId>");
			re.append("<Qty>"+transportReceivePutLog[i].getString("quantity")+"</Qty>");
			re.append("<SN>"+transportReceivePutLog[i].getString("serial_number","NULL")+"</SN>");
			re.append("</PutLog>");
		}
		
		return re.toString();
	}
	
	
	/**
	 * 下载拣货单对应位置上需拣货商品
	 * @param xml
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public String downLoadOutBillLocationPoroduct(String xml,long ps_id)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		long out_id = Long.parseLong(getSampleNode(xml,"Out"));
		String location = StringUtil.getSampleNode(xml,"Location");
		
		DBRow locationCatalog = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(ps_id, location);
		
		if(locationCatalog ==null)
		{
			throw new NoExistLocationException();
		}
		
		DBRow[] locationProducts = floorOutboundOrderMgrZJ.getLocationProductForOut(locationCatalog.get("slc_id",0l), out_id);
		
		StringBuffer re = new StringBuffer();
		for (int i = 0; i < locationProducts.length; i++) 
		{
			re.append("<Details>");
			re.append("<PcId>"+locationProducts[i].get("out_list_pc_id",0l)+"</PcId>");
			re.append("<SlcId>"+locationProducts[i].get("slc_id",0l)+"</SlcId>");
			re.append("<NeedPickQuantity>"+locationProducts[i].get("need_execut_quantity",0f)+"</NeedPickQuantity>");
			re.append("<Location>"+locationProducts[i].getString("slc_position_all")+"</Location>");
			re.append("<From_Container_Type>"+locationProducts[i].get("from_container_type",0)+"</From_Container_Type>");
			re.append("<From_Container_Type_id>"+locationProducts[i].get("from_container_type_id",0l)+"</From_Container_Type_id>");
			re.append("<From_Con_id>"+locationProducts[i].get("from_con_id",0l)+"</From_Con_id>");
			re.append("<Pick_Container_Type>"+locationProducts[i].get("Pick_Container_Type",0)+"</Pick_Container_Type>");
			re.append("<Pick_Container_Type_id>"+locationProducts[i].get("Pick_Container_Type_id",0l)+"</Pick_Container_Type_id>");
			re.append("<Pick_Con_id>"+locationProducts[i].get("Pick_Con_id",0l)+"</Pick_Con_id>");
			re.append("<SN>"+locationProducts[i].getString("serial_number","NULL")+"</SN>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	/**
	 * 下载拣货单基础数据
	 */
	public byte[] downLoadOutStoreBillBasicData(String xml,String machine)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		long out_id = Long.parseLong(getSampleNode(xml,"Out"));
		
		String outboundLocation = getResponseXML(this.xmlOutStoreBillOrderLocation(out_id));
		String outboundLocationProduct = getResponseXML(this.xmlOutStoreBillOrderLocationProduct(out_id));
		String outboundProduct = getResponseXML(this.xmlOutStoreBillOrderProduct(out_id));
		String outboundProductCode = getResponseXML(this.xmlOutStoreBillOrderProductCode(out_id));
		String outboundSerialProduct = getResponseXML(this.xmlOutSerialProduct(out_id));
		
		InputStream isOutboundLocation = new ByteArrayInputStream(outboundLocation.getBytes());
		InputStream isOutboundLocationProduct = new ByteArrayInputStream(outboundLocationProduct.getBytes());
		InputStream isOutboundProduct = new ByteArrayInputStream(outboundProduct.getBytes());
		InputStream isOutboundProductCode = new ByteArrayInputStream(outboundProductCode.getBytes());
		InputStream isOutboundSerialProduct = new ByteArrayInputStream(outboundSerialProduct.getBytes());
		
		
		ArrayList<InputStream> list = new ArrayList<InputStream>();
		list.add(isOutboundLocation);
		list.add(isOutboundLocationProduct);
		list.add(isOutboundProduct);
		list.add(isOutboundProductCode);
		list.add(isOutboundSerialProduct);
		
		FileUtil.delFile(Environment.getHome()+"/bcs_down_zip/"+machine+"OutList.zip");
		ZipFile zipFile = new ZipFile(Environment.getHome()+"/bcs_down_zip/"+machine+"OutList.zip");
		
		for (int i = 0; i <list.size();i++)
		{
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			
			switch (i) 
			{
				case 0:
					parameters.setFileNameInZip("outlist.txt");
				break;
				
				case 1:
					parameters.setFileNameInZip("outlistLocationProduct.txt");
				break;
				
				case 2:
					parameters.setFileNameInZip("outlistProduct.txt");
				break;
				
				case 3:
					parameters.setFileNameInZip("outlistProductCode.txt");
				break;
				
				case 4:
					parameters.setFileNameInZip("outlistSerialProduct.txt");
				break;
			}
			
			parameters.setSourceExternalStream(true);
			
			zipFile.addStream(list.get(i), parameters);
			list.get(i).close();
		}
		
		File file=new File(Environment.getHome()+"/bcs_down_zip/"+machine+"OutList.zip");
		InputStream fis=new FileInputStream(file);
		int fileLength =(int)file.length();
		

		byte[] image=new byte[fileLength];
		fis.read(image);
		fis.close();
		
		return image;
	}
	
	private String getResponseXML(String details)
		throws Exception
	{
		StringBuilder boxQtyXml = new StringBuilder();
		boxQtyXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		boxQtyXml.append("<data>");
		boxQtyXml.append("<Detail>");
		if(!details.equals(""))
		{
			boxQtyXml.append(details);
		}
		boxQtyXml.append("</Detail>");	
		
		boxQtyXml.append("<ServiceTime>"+DateUtil.NowStr()+"</ServiceTime>");
		boxQtyXml.append("</data>");
		
		return boxQtyXml.toString();
	}
	
	/**
	 * 根据拣货单ID获得拣货单位置
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutStoreBillOrderLocation(long out_id)
		throws Exception
	{
		
		StringBuffer returnXML = new StringBuffer("");
		DBRow[] outStoreBillOrderDetails = floorOutboundOrderMgrZJ.getOutStoreBillLocation(out_id);
		
		for (int i = 0; i < outStoreBillOrderDetails.length; i++) 
		{
			returnXML.append("<Details>");
			returnXML.append("<Out>"+out_id+"</Out>");
			returnXML.append("<Location>"+outStoreBillOrderDetails[i].getString("slc_position_all")+"</Location>");
			returnXML.append("<Sort>"+(i+1)+"</Sort>");
			returnXML.append("<LocationId>"+outStoreBillOrderDetails[i].get("out_list_slc_id",0l)+"</LocationId>");
			returnXML.append("</Details>");
		}
		return returnXML.toString();
	}
	
	/**
	 * 根据拣货单获得拣货单商品信息
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutStoreBillOrderProduct(long out_id)
		throws Exception
	{
		DBRow[] products = floorOutboundOrderMgrZJ.getProductForOut(out_id);
		
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < products.length; i++)
		{
			re.append("<Details>");				
			re.append("<Name>"+products[i].getString("p_name")+"</Name>");
			re.append("<Location>NULL</Location>");
			re.append("<PID>"+products[i].get("pc_id",0l)+"</PID>");
			re.append("<Length>"+products[i].get("length",0f)+"</Length>");
			re.append("<Width>"+products[i].get("width",0f)+"</Width>");
			re.append("<Heigth>"+products[i].get("heigth",0f)+"</Heigth>");
			re.append("<Weight>"+products[i].get("weight",0f)+"</Weight>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	/**
	 * 获得拣货单商品条码
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutStoreBillOrderProductCode(long out_id)
		throws Exception
	{
		DBRow[] productCodes = floorOutboundOrderMgrZJ.getProductCodeForOut(out_id);
		
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < productCodes.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+productCodes[i].get("pc_id",0l)+"</PCID>");
			re.append("<Barcode>"+productCodes[i].getString("p_code")+"</Barcode>");
			re.append("<PName>"+productCodes[i].getString("p_name")+"</PName>");
			re.append("<CodeType>"+productCodes[i].get("code_type",0)+"</CodeType>");
			if(productCodes[i].get("code_type",0)==CodeTypeKey.Main)
			{
				re.append("<Show>1</Show>");
			}
			else
			{
				re.append("<Show>0</Show>");
			}
			re.append("</Details>");	
		}
		 
	
		DBRow[] products = floorOutboundOrderMgrZJ.getProductForOut(out_id);
		
		for (int i = 0; i < products.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+products[i].get("pc_id",0l)+"</PCID>");
			re.append("<Barcode>"+products[i].get("pc_id",0l)+"</Barcode>");
			re.append("<PName>"+productCodes[i].getString("p_name")+"</PName>");
			re.append("<CodeType>0</CodeType>");
			re.append("<Show>0</Show>");
			re.append("</Details>");
		}
		
		return (re.toString());
	}
	
	/**
	 * 获得拣货单所需序列号
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutSerialProduct(long out_id)
		throws Exception
	{
		DBRow[] serialProducts = floorOutboundOrderMgrZJ.getSerialProductForOut(out_id);
		
		StringBuffer re = new StringBuffer("");
		
		for (int i = 0; i < serialProducts.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+serialProducts[i].get("pc_id",0l)+"</PCID>");
			re.append("<SN>"+serialProducts[i].getString("serial_number")+"</SN>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	/**
	 * 打托盘
	 * @param xml
	 * @throws Exception
	 */
	public int printLicensePlate(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));

		String localbase64 = StringUtil.HashBase64(details);
		if(!efficacy.equals(localbase64))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		String[] licensePlateProducts = details.split("\\|");
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		long lp_id = 0;
		for (int i = 0; i < licensePlateProducts.length; i++) 
		{
			String[] licensePlateProduct = licensePlateProducts[i].split(",");
			long pc_id = Long.parseLong(licensePlateProduct[0]);
			String lpNumber = licensePlateProduct[1];
			
			DBRow container = containerProductMgrZJ.getDetailContainer(lpNumber);
			if(container!=null)
			{
				lp_id = container.get("con_id",0l);
			}
			else
			{
				container = new DBRow();
				container.add("container",lpNumber);
				
				lp_id = containerProductMgrZJ.addContainer(container);
			}
			
			String sn = "";
			if(!licensePlateProduct[2].toLowerCase().equals("null"))
			{
				sn = licensePlateProduct[2];
			}
			float quantity = Float.parseFloat(licensePlateProduct[3]);
			
			DBRow licensePlateProductRow = new DBRow();
			licensePlateProductRow.add("pc_id",pc_id);
			licensePlateProductRow.add("lp_id",lp_id);
			licensePlateProductRow.add("sn",sn);
			licensePlateProductRow.add("quantity",quantity);
			
			list.add(licensePlateProductRow);
		}
		
		containerProductMgrZJ.printLicensePlate(lp_id, list);
		
		return list.size();
	}
	
/*	*//**
	 * 获取容器的包含关系ContainerList的
	 * 目前最多只支持两级的	TLP->CLP->Product,CLP-BLP-Product
	 * 不支持类似 TLP->BLP->ILP->Product,CLP-BLP--Product
	 * @return
	 * @throws Exception
	 *//*
	private String xmlContainerList() throws Exception{
		StringBuffer str = new StringBuffer();
		 try{
			 return str.toString();
		 }catch (Exception e) {
			throw new SystemException(e,"xmlContainerType",log);
 		}
	}
	*/
	/**
	 * 托盘类型XML
	 * @param lp_id
	 * @return
	 * @throws Exception
	 *  <container>CLP10121212</container>		<!-- 编号 -->
		<containerId>100001</containerId>		<!-- ID -->
		<containerHasSn></containerHasSn>		<!-- container 是否含有SN-->
		<containerType>CLP</containerType>		<!-- 本身的类型 (TLP,CLP,BLP,ILP)-->
		<subContainerType>BLP</subContainerType>	<!-- 子级包含的东西(BLP,ILP,Product,ALL:TLP 包含的就是ALL)-->
		<subContainerTypeId>10002</subContainerTypeId>  <!-- 子级包含的类型的ID(BLP的类型ID)-->
		<subCount>10</subCount>				<!-- 子级包含的个数-->
		<containerPcId>1000030</containerPcId>		<!-- 限制的PCID类型-->
		<containerPcName>BALLAST</containerPcName>	<!-- 限制的p_name-->
		<containerPcCount>30.0</containerPcCount>	<!-- 一共可以放多少商品 (TLP 就是-1) -->
	 */
	public String xmlContainerType(DBRow container , int container_type, long type_id) throws Exception
	{
		try 
		{
			String str = "";
			/*
			if(ContainerTypeKey.BLP == container_type)
			{
				str = this.xmlFindBoxTypeRelateInfoByTypeId(container , type_id, container_type);
			}
			else*///去掉ILP和BLP
			if(ContainerTypeKey.CLP == container_type)
			{
				str = this.xmlFindClpTypeRelateInfoByTypeId(container,type_id, container_type);
			}
			/*
			else if(ContainerTypeKey.ILP == container_type)
			{
				
				str = this.xmlFindInnerBoxTypeRelateInfoByTypeId(container,type_id, container_type);
			}
			else*///去掉ILP和BLP
			 if(ContainerTypeKey.TLP == container_type)
			{
				str = this.xmlFindTlpTypeRelateInfoByTypeId( container , type_id, container_type);
			}
			return str;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlContainerType",log);
		}
	}
	
	/**
	 * 将BLP_type-->xml
	 * @param type_id
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public String xmlFindBoxTypeRelateInfoByTypeId(DBRow container ,long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow containerTypeInfo = containerMgrZyj.findBoxTypeRelateInfoByTypeId(type_id);
			StringBuffer re = new StringBuffer();
			if(containerTypeInfo != null){
				/**
				 * BOX 
				 * 
			 
				 */
				container.add("containerId", container.get("con_id", 0l));
				container.add("containerHasSn", container.get("is_has_sn", 0));
				container.add("containerType", "BLP");
				container.add("subContainerType",  containerTypeInfo.get("box_inner_type", 0) == 0 ? "Product":"ILP");
				container.add("subContainerTypeId",  containerTypeInfo.get("box_inner_type", 0l));
				if(containerTypeInfo.get("box_inner_type", 0) == 0){
					//直接放的是product
					container.add("subCount", 0);
				}else{
					container.add("subCount",  containerTypeInfo.get("box_total", 0) );
				}
				container.add("containerPcId", containerTypeInfo.get("box_pc_id", 0l));
				container.add("containerPcName", containerTypeInfo.getString("p_name"));
				container.add("containerPcCount",containerTypeInfo.get("box_total_piece", 0));
		 
				
				re.append(this.xmlContainerTypeIdPcCount(container));
			}
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlFindBoxTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	
	/**
	 * 将CLP_type-->xml
	 * @param type_id
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public String xmlFindClpTypeRelateInfoByTypeId(DBRow container , long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow containerTypeInfo = containerMgrZyj.findClpTypeRelateInfoByTypeId(type_id);
			StringBuffer re = new StringBuffer();
			if(containerTypeInfo != null)
			{	
		 
				container.add("containerId", container.get("con_id", 0l));
				container.add("containerHasSn", container.get("is_has_sn", 0));
				container.add("containerType", "CLP");
				if(containerTypeInfo.get("sku_lp_box_type", 0) == 0){
					container.add("subContainerType", "Product");
					container.add("subContainerTypeId", 0);
					container.add("subCount", 0 );

				}else{
					container.add("subContainerType", "BLP");
					container.add("subContainerTypeId", containerTypeInfo.get("sku_lp_box_type", 0));
					container.add("subCount",  containerTypeInfo.get("sku_lp_total_box", 0) );
				}
				container.add("containerPcId",  containerTypeInfo.get("sku_lp_pc_id", 0l));
				container.add("containerPcName", containerTypeInfo.getString("p_name") );
				container.add("containerPcCount", containerTypeInfo.get("sku_lp_total_piece", 0));
				
				re.append(this.xmlContainerTypeIdPcCount(container));
			}
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlFindClpTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	/**
	 * 将ILP_type-->xml
	 * @param type_id
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public String xmlFindInnerBoxTypeRelateInfoByTypeId(DBRow container ,long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow containerTypeInfo = containerMgrZyj.findInnerBoxTypeRelateInfoByTypeId(type_id);
			StringBuffer re = new StringBuffer();
			if(containerTypeInfo != null)
			{
				container.add("containerId", container.get("con_id", 0l));
 				container.add("containerHasSn", container.get("is_has_sn", 0));
				container.add("containerType", "ILP");
				container.add("subContainerType", "Product");
				container.add("subContainerTypeId", 0);
				container.add("subCount", 0);
				container.add("containerPcId", containerTypeInfo.get("ibt_pc_id", 0l));
				container.add("containerPcName", containerTypeInfo.getString("p_name"));
				container.add("containerPcCount", containerTypeInfo.get("ibt_total", 0));
				
				re.append(this.xmlContainerTypeIdPcCount(container));
			}
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlFindInnerBoxTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	/**
	 * 将TLP_type-->xml
	 * TLP只返回类型及类型ID
	 * @param type_id
	 * @param container_type
	 * @return
	 * @throws Exception
	 * 		re.append("<container>").append(row.getString("container")).append("</container>");					// <!-- container 编号-->	
			re.append("<containerId>").append(row.get("con_id", 0l)).append("</containerId>");				// <!-- container Id-->	
			re.append("<containerHasSn>").append(row.get("isHasSn", 0)).append("</containerHasSn>");		//<!-- container 是否含有SN-->	
			re.append("<containerType>").append(row.getString("containerType")).append("</containerType>");			//<!-- 本身的类型 (TLP,CLP,BLP,ILP)-->
			re.append("<subContainerType>").append(row.getString("subContainerType")).append("</subContainerType>");	//<!-- 子级包含的东西(BLP,ILP,Product,ALL:TLP 包含的就是ALL)-->
			re.append("<subContainerTypeId>").append(row.getString("subContainerTypeId")).append("</subContainerTypeId>");//<!-- 子级包含的个数 子容器 容器类型 TLP 为-1-->	
			re.append("<subCount>").append(row.get("subCount", 0)).append("</subCount>");					//<!-- 子级包含的个数  子容器包含的数量 TLP为-1-->	
			re.append("<containerPcId>").append(row.get("containerPcId", 0l)).append("</containerPcId>");		//<!-- 限制的PCID类型 TLP ""-->		
			re.append("<containerPcName>").append(row.getString("containerPcName")).append("</containerPcName>");	//<!-- 限制的p_name TLP 为""-->
 			re.append("<containerPcCount>").append(row.get("containerPcCount",0)).append("</containerPcCount>"); //<!-- 一共可以放多少商品 (TLP 就是-1) -->
	 */
	public String xmlFindTlpTypeRelateInfoByTypeId(  DBRow container , long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow  containerTypeInfo = containerMgrZyj.findTlpTypeRelateInfoByTypeId(type_id);
			StringBuffer re = new StringBuffer();
			if (containerTypeInfo != null )
			{
				//TLP
				 
				container.add("containerId", container.get("con_id", 0l));
				 
				container.add("containerHasSn", container.get("isHasSn", 0));
				container.add("containerType", "TLP");
				container.add("subContainerType", "ALL");
				container.add("subContainerTypeId", -1);
				container.add("subCount", -1);
				container.add("containerPcId", -1);
				container.add("containerPcName", "");
				container.add("containerPcCount", -1);
		 
				re.append(this.xmlContainerTypeIdPcCount(container));
			}
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlFindTlpTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	
	/**
	<container>CLP10121212</container>		<!-- 编号 -->
	<containerId>100001</containerId>		<!-- ID -->
	<containerHasSn></containerHasSn>		<!-- container 是否含有SN-->
	
	<containerType>CLP</containerType>		<!-- 本身的类型 (TLP,CLP,BLP,ILP)-->
	<subContainerType>BLP</subContainerType>	<!-- 子级包含的东西(BLP,ILP,Product,ALL:TLP 包含的就是ALL)-->
	<subContainerTypeId>10002</subContainerTypeId>  <!-- 子级包含的类型的ID(BLP的类型ID)-->
	<subCount>10</subCount>				<!-- 子级包含的个数-->
	<containerPcId>1000030</containerPcId>		<!-- 限制的PCID类型-->
	<containerPcName>BALLAST</containerPcName>	<!-- 限制的p_name-->
 
	<containerPcCount>30.0</containerPcCount>	<!-- 一共可以放多少商品 (TLP 就是-1) -->
	 * @return
	 * @throws Exception
	 */
	public String xmlContainerTypeIdPcCount(DBRow row) throws Exception
	{
		try
		{
			StringBuffer re = new StringBuffer();
			re.append("<Details>");
			re.append("<container>").append(row.getString("container")).append("</container>");					// <!-- container 编号-->	
			re.append("<containerId>").append(row.get("con_id", 0l)).append("</containerId>");				// <!-- container Id-->	
			re.append("<containerHasSn>").append(row.get("is_has_sn", 0)).append("</containerHasSn>");		//<!-- container 是否含有SN-->	
			re.append("<isFull>").append(row.get("is_full", 0)).append("</isFull>");							//<!-- container 是否是满的-->	
			re.append("<containerTypeId>").append(row.get("type_id", 0l)).append("</containerTypeId>");
			re.append("<containerType>").append(row.getString("containerType")).append("</containerType>");			//<!-- 本身的类型 (TLP,CLP,BLP,ILP)-->
			re.append("<subContainerType>").append(row.getString("subContainerType")).append("</subContainerType>");	//<!-- 子级包含的东西(BLP,ILP,Product,ALL:TLP 包含的就是ALL)-->
			re.append("<subContainerTypeId>").append(row.getString("subContainerTypeId")).append("</subContainerTypeId>");//<!-- 子级包含的个数 子容器 容器类型 TLP 为-1-->	
			re.append("<subCount>").append(row.get("subCount", 0)).append("</subCount>");					//<!-- 子级包含的个数  子容器包含的数量 TLP为-1-->	
			re.append("<containerPcId>").append(row.get("containerPcId", 0l)).append("</containerPcId>");		//<!-- 限制的PCID类型 TLP ""-->		
			re.append("<containerPcName>").append(row.getString("containerPcName")).append("</containerPcName>");	//<!-- 限制的p_name TLP 为""-->
 			re.append("<containerPcCount>").append(row.get("containerPcCount",0)).append("</containerPcCount>"); //<!-- 一共可以放多少商品 (TLP 就是-1) -->
 			//添加title titleName , lotNumber
 			re.append("<titleId>").append(row.get("title_id",0)).append("</titleId>"); 
 			re.append("<titleName>").append(row.getString("title_name")).append("</titleName>"); 
 			re.append("<lotNumber>").append(row.getString("lot_number")).append("</lotNumber>"); 
 			re.append("</Details>");
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlContainerTypeIdPcCount(long containerTypeId, long containerPcId, String containerPcName,"+
					"double containerChildrenCount, double containerPcCount, int containerLpType)",log);
		}
	}
	public String xmlContainer(DBRow row) throws Exception{
		if(row != null){
			StringBuffer re = new StringBuffer("");
			re.append("<Details>");
			re.append("<LpName>"+row.getString("container")+"</LpName>");
			re.append("<LpId>"+row.get("con_id", 0l)+"</LpId>");
			re.append("<BillId>"+row.get("bill_id", 0l)+"</BillId>");

			
			re.append("</Details>");
			return re.toString();
		}
		return "" ;
	}
	/**
	 * 托盘商品XML
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public String xmlContainerProduct(long lp_id)
		throws Exception
	{
		try 
		{
			DBRow[] containerProducts = containerProductMgrZJ.getContainerProductByContainer(lp_id);
			
			StringBuffer re = new StringBuffer("");
			for (int i = 0; i < containerProducts.length; i++) 
			{
				long pc_id = containerProducts[i].get("cp_pc_id",0l);
				String sn = containerProducts[i].getString("cp_sn");
				String p_name = containerProducts[i].getString("p_name");
				String lpName = containerProducts[i].getString("cp_lp_name");
				if (sn.equals(""))
				{
					sn = "NULL";
				}
				float quantity = containerProducts[i].get("cp_quantity",0f);
				
				re.append("<Details>");
				re.append("<PCID>"+pc_id+"</PCID>");
				re.append("<Pname>"+p_name+"</Pname>");
				re.append("<SN>"+sn+"</SN>");
				re.append("<Qty>"+quantity+"</Qty>");
				re.append("<LP>"+lp_id+"</LP>");
				re.append("<LpName>").append(lpName).append("</LpName>");
				re.append("</Details>");
			}
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlContainerProduct",log);
		}
	}
	
	/**
	 * 根据拣货单获得拣货单商品信息
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlLicensePlateProduct(long lp_id)
		throws Exception
	{
		DBRow[] products = containerProductMgrZJ.getProductForContainer(lp_id);
		
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < products.length; i++)
		{
			re.append("<Details>");				
			re.append("<Name>"+products[i].getString("p_name")+"</Name>");
			re.append("<Location>NULL</Location>");
			re.append("<PID>"+products[i].get("pc_id",0l)+"</PID>");
			re.append("<Length>"+products[i].get("length",0f)+"</Length>");
			re.append("<Width>"+products[i].get("width",0f)+"</Width>");
			re.append("<Heigth>"+products[i].get("heigth",0f)+"</Heigth>");
			re.append("<Weight>"+products[i].get("weight",0f)+"</Weight>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	/**
	 * 获得拣货单商品条码
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlLicensePlateProductCode(long lp_id)
		throws Exception
	{
		DBRow[] productCodes = containerProductMgrZJ.getProductCodeForContainer(lp_id);
		
		StringBuffer re = new StringBuffer("");
		for (int i = 0; i < productCodes.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+productCodes[i].get("pc_id",0l)+"</PCID>");
			re.append("<Barcode>"+productCodes[i].getString("p_code")+"</Barcode>");
			if(productCodes[i].get("code_type",0)==CodeTypeKey.Main)
			{
				re.append("<Show>1</Show>");
			}
			else
			{
				re.append("<Show>0</Show>");
			}
			re.append("</Details>");	
		}
		
		DBRow[] products = containerProductMgrZJ.getProductForContainer(lp_id);
		
		for (int i = 0; i < products.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+products[i].get("pc_id",0l)+"</PCID>");
			re.append("<Barcode>"+products[i].get("pc_id",0l)+"</Barcode>");
			re.append("<Show>0</Show>");
			re.append("</Details>");
		}
		
		return (re.toString());
	}
	
	/**
	 * 获得拣货单所需序列号
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlLicensePlateSerialProduct(long lp_id)
		throws Exception
	{
		DBRow[] serialProducts = containerProductMgrZJ.getSerialProductForContainer(lp_id);
		
		StringBuffer re = new StringBuffer("");
		
		for (int i = 0; i < serialProducts.length; i++) 
		{
			re.append("<Details>");	
			re.append("<PCID>"+serialProducts[i].get("pc_id",0l)+"</PCID>");
			re.append("<SN>"+serialProducts[i].getString("serial_number")+"</SN>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	/**
	 * 下载托盘基础数据
	 */
	public byte[] downLoadContainerProductData(String xml,String machine)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
	
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		String lp = getSampleNode(xml,"LP");
		
		DBRow container = containerMgrZyj.findContainerByNumber(lp);//扫描的是托拍的编号
			//containerProductMgrZJ.getDetailContainer(lp);
		
		long lp_id = 0 ;
		if (container!=null)
		{
			lp_id = container.get("con_id",0l);
		}
		else
		{
			try 
			{
				lp_id = Long.parseLong(lp);
			}
			catch (NumberFormatException e) 
			{
				lp_id = 0;
			} 
		}
		if(container == null)
		{
			throw new ContainerNotFullException();
		}
		
		String containerType = getResponseXML(this.xmlContainerType(container,container.get("container_type", 0), container.get("type_id", 0L)));//容器类型
		
		String containerProduct = getResponseXML(this.xmlContainerProduct(lp_id));
		String licensePlateProduct = getResponseXML(this.xmlLicensePlateProduct(lp_id));
		String licensePlateProductCode = getResponseXML(this.xmlLicensePlateProductCode(lp_id));
		String licensePlateSerialProduct = getResponseXML(this.xmlLicensePlateSerialProduct(lp_id));
		
		InputStream isContainerType = new ByteArrayInputStream(containerType.getBytes());
		InputStream isContainerProduct = new ByteArrayInputStream(containerProduct.getBytes());
		InputStream isLicensePlateProduct = new ByteArrayInputStream(licensePlateProduct.getBytes());
		InputStream isLicensePlateProductCode = new ByteArrayInputStream(licensePlateProductCode.getBytes());
		InputStream isLicensePlateSerialProduct = new ByteArrayInputStream(licensePlateSerialProduct.getBytes());
		
		
		ArrayList<InputStream> list = new ArrayList<InputStream>();
		list.add(isContainerProduct);
		list.add(isLicensePlateProduct);
		list.add(isLicensePlateProductCode);
		list.add(isLicensePlateSerialProduct);
		list.add(isContainerType);
		
		FileUtil.delFile(Environment.getHome()+"/bcs_down_zip/"+machine+"LicensePlate.zip");
		ZipFile zipFile = new ZipFile(Environment.getHome()+"/bcs_down_zip/"+machine+"LicensePlate.zip");
		
		for (int i = 0; i <list.size();i++)
		{
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			
			switch (i) 
			{
				case 0:
					parameters.setFileNameInZip("containerProduct.txt");
				break;
				
				case 1:
					parameters.setFileNameInZip("licensePlateProduct.txt");
				break;
				
				case 2:
					parameters.setFileNameInZip("licensePlateProductCode.txt");
				break;
				
				case 3:
					parameters.setFileNameInZip("licensePlateSerialProduct.txt");
				break;
				
				case 4:
					parameters.setFileNameInZip("containerType.txt");
				break;
			}
			
			parameters.setSourceExternalStream(true);
			
			zipFile.addStream(list.get(i), parameters);
			list.get(i).close();
		}
		
		File file=new File(Environment.getHome()+"/bcs_down_zip/"+machine+"LicensePlate.zip");
		InputStream fis=new FileInputStream(file);
		int fileLength =(int)file.length();
		
	
		byte[] image=new byte[fileLength];
		fis.read(image);
		fis.close();
		
		return image;
	}
	
	/**
	 * 转运单出库安卓
	 */
	public int transportOutboundAndroid(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			String details = getSampleNode(xml,"Details");
			String efficacy = getSampleNode(xml,"Efficacy");
			int length = Integer.parseInt(getSampleNode(xml,"Length"));
			String machine_id = getSampleNode(xml,"Machine");
			
			long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
			DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
			else
			{
				if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
				{
					throw new TransportOrderCanNotOutboundException();
				}
			}
			
			if(!efficacy.equals(StringUtil.HashBase64(details)))
			{
				throw new XMLDataErrorException();
			}
			if(length!=details.length())
			{
				throw new XMLDataLengthException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			//存入发货明细
			String[] transportOutbounds = details.split("\\|");
			
			for (int i = 0; i < transportOutbounds.length; i++) 
			{
				String[] transportOutbound = transportOutbounds[i].split(",");
				
				if(transportOutbounds[i].trim().equals("")||transportOutbound.length!=4)
				{
					throw new XMLDataErrorException();
				}
				
				String lp = transportOutbound[2];
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				String sn = transportOutbound[3];
				
				DBRow[] repeatSN = transportOutboundMgrZJ.getTransportOutboundBySNNotMachine(sn, machine_id, transport_id);  
				
				if(repeatSN.length>0)
				{
					throw new ReceiveProductSNRepeatException();
				}
				
				long lp_id = 0;
				if(contianer!=null)
				{
					lp_id = contianer.get("con_id",0l);
				}
				
				DBRow product = productMgr.getDetailProductByPcid(Long.parseLong(transportOutbound[0]));
				
				if(product ==null)
				{
					throw new ProductNotExistException();
				}
				
				DBRow db = new DBRow();
				db.add("to_transport_id",transport_id);
				db.add("to_pc_id",product.get("pc_id",0l));
				db.add("to_p_name",product.getString("p_name"));
				db.add("to_p_code",product.getString("p_code"));
				db.add("to_count",transportOutbound[1]);
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				db.add("to_machine_id",machine_id);
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				
				if(lp_id!=0)
				{
					db.add("to_lp_id",lp_id);
				}
				
				if(!sn.equals("NULL"))
				{
					db.add("to_serial_number",sn);
				}
				
				list.add(db);
			}
			
			transportOutboundMgrZJ.addTransportOutboundSub(list.toArray(new DBRow[0]),transport_id,machine_id);
			
			return list.size();
		} 
		catch (ReceiveProductSNRepeatException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"outboundTransportAndroid",log);
		}
	}
	
	/**
	 * 转运单出库比较
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public String transportOutboundCompare(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
	//	String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
			{
				throw new TransportOrderCanNotOutboundException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		//发货
		StringBuffer re = new StringBuffer();
		DBRow[] compare = transportOutboundMgrZJ.compareTransportDetailOutByTransportId(transport_id);
		for (int i = 0; i < compare.length; i++) 
		{
				
			re.append("<Different>");
			re.append("<PID>"+compare[i].getString("pc_id")+"</PID>");//新加商品ID返回
			re.append("<MachineId>"+compare[i].getString("machine_id")+"</MachineId>");
			re.append("<DifferentType>"+compare[i].getString("different_type")+"</DifferentType>");
			re.append("<SN>"+compare[i].getString("serial_number")+"</SN>");
			re.append("</Different>");
		}
		
		//单据整体发货
		DBRow[] outbounds = transportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
		
		for (int i = 0; i < outbounds.length; i++) 
		{
			re.append("<Outbound>");
			re.append("<PID>"+outbounds[i].getString("to_pc_id")+"</PID>");
			re.append("<SN>"+outbounds[i].getString("to_serial_number","NULL")+"</SN>");
			re.append("<Qty>"+outbounds[i].get("to_count",0f)+"</Qty>");
			re.append("<Machine>"+outbounds[i].getString("to_machine_id")+"</Machine>");
			re.append("</Outbound>");
		}
		
		return re.toString();
	}
	
	public String transportOutboundCompleteResult(String xml)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
	//	String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
			{
				throw new TransportOrderCanNotOutboundException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		int compareResult = TransportCompareKey.Consistent;
		
		DBRow[] compareSN = transportOutboundMgrZJ.compareTransportOutboundByTransportId(transport_id);
		DBRow[] compareCount = transportOutboundMgrZJ.transportOutboundCompareCount(transport_id);
		
		if (compareSN.length>0&&compareCount.length==0)
		{
			compareResult = TransportCompareKey.SnDifferent;
		}
		else if(compareSN.length==0&&compareCount.length>0)
		{
			compareResult = TransportCompareKey.CountDifferent;
		}
		else if(compareSN.length>0&&compareCount.length>0)
		{
			compareResult = TransportCompareKey.AllDifferent;
		}
		
		String re = new String("<OutboundResult>"+compareResult+"</OutboundResult>");
	
		return re;
	}
	
	public void transportReceiveOver(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
	//	String ware = getSampleNode(xml,"Ware");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if(transport.get("transport_status",0) != TransportOrderKey.AlREADYARRIAL&&transport.get("transport_status",0) != TransportOrderKey.RECEIVEING)
			{
				throw new TransportOrderCanNotWareHouseException();
			}
		}
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		transportMgrZJ.transportReceiveOver(transport_id, adminLoggerBean);
	}
	
	public void transportOutboundOver(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		String machine_id = getSampleNode(xml,"Machine");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		long transport_id = Long.parseLong(getSampleNode(xml,"Transport"));
		DBRow transport = transportMgrZJ.getDetailTransportById(transport_id);
		if(transport == null)
		{
			throw new NoExistTransportOrderException();
		}
		else
		{
			if(transport.get("transport_status",0) != TransportOrderKey.PACKING)
			{
				throw new TransportOrderCanNotOutboundException();
			}
		}
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		//记录序列号出库时间
		DBRow[] transportOutboundDetails = transportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
		for (int i = 0; i < transportOutboundDetails.length; i++) 
		{
			//装货时有扫描序列号，记录序列号出库时间
			if (!transportOutboundDetails[i].getString("to_serial_number").equals(""))
			{
				serialNumberMgrZJ.outSerialProduct(transportOutboundDetails[i].getString("to_serial_number"),transportOutboundDetails[i].get("to_pc_id",0l));
			}	
		}
		
		transportOutboundApproveMgrZJ.addTransportOutboundApproveSub(transport_id, adminLoggerBean);
	}
	
	
	public void putOver(String xml, AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		String machine_id = getSampleNode(xml,"Machine");

		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		long system_bill_id = Long.parseLong(StringUtil.getSampleNode(xml, "Bill"));
		int system_bill_type = Integer.parseInt(StringUtil.getSampleNode(xml, "BillType"));
		
		if(system_bill_type == ProductStoreBillKey.TRANSPORT_ORDER||system_bill_type == ProductStoreBillKey.DELIVERY_ORDER)
		{
			DBRow transport = transportMgrZJ.getDetailTransportById(system_bill_id);
			
			//收货未完成，无法放货完成
			if (transport.get("transport_status",0)!=TransportOrderKey.FINISH&&transport.get("transport_status",0)!=TransportOrderKey.APPROVEING&&transport.get("transport_status",0)!=TransportOrderKey.AlREADYRECEIVE)
			{
				throw new CanNotPutOverException();
			}
			
			transportMgrZJ.transportPutOver(system_bill_id, adminLoggerBean);
		}
	}
	
	/**
	 * 拣货提交异常
	 * @param xml
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public String pickUpException(String xml,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		String machine_id = getSampleNode(xml,"Machine");
		
		long out_id = Long.parseLong(getSampleNode(xml,"Out"));
		String locationXML = StringUtil.getSampleNode(xml,"Location");
		long pc_id = Long.parseLong(getSampleNode(xml,"Pcid"));
		String foType = StringUtil.getSampleNode(xml,"FOType");
		
		DBRow outOrder = floorOutboundOrderMgrZJ.getDetailOutboundById(out_id);
		long ps_id = outOrder.get("ps_id",0l);
		DBRow location = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(ps_id,locationXML);
		long slc_id = location.get("slc_id",0l);
		
		
		if(!efficacy.equals(StringUtil.HashBase64(details)))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		
		String[] pickUpErrorsString = details.split("\\|");
		
		DBRow[] newPickUp = new DBRow[0];
		for (int i = 0; i < pickUpErrorsString.length; i++)
		{
			String[] pickUpErrorString = pickUpErrorsString[i].split(",");
			int from_container_type = Integer.parseInt(pickUpErrorString[0]);
			long from_container_type_id = Long.parseLong(pickUpErrorString[1]);
			long from_con_id = Long.parseLong(pickUpErrorString[2]);
			int pick_container_type = Integer.parseInt(pickUpErrorString[3]);
			long pick_container_type_id = Long.parseLong(pickUpErrorString[4]);
			long pick_con_id = Long.parseLong(pickUpErrorString[5]);
			int error_quantity = Integer.parseInt(pickUpErrorString[6]);
			
			newPickUp = productStoreMgrZJ.pickUpException(ps_id, slc_id, pc_id, error_quantity, out_id, "",machine_id, foType, from_container_type,from_container_type_id, from_con_id,pick_container_type,pick_container_type_id, pick_con_id,pick_con_id, adminLoggerBean);
		}
		
		StringBuffer re = new StringBuffer();
		for (int i = 0; i < newPickUp.length; i++) 
		{
			re.append("<Details>");
			re.append("<PcId>"+newPickUp[i].get("out_list_pc_id",0l)+"</PcId>");
			re.append("<SlcId>"+newPickUp[i].get("slc_id",0l)+"</SlcId>");
			re.append("<NeedPickQuantity>"+newPickUp[i].get("need_execut_quantity",0f)+"</NeedPickQuantity>");
			re.append("<Location>"+newPickUp[i].getString("slc_position_all")+"</Location>");
			re.append("<From_Container_Type>"+newPickUp[i].get("from_container_type",0)+"</From_Container_Type>");
			re.append("<From_Container_Type_id>"+newPickUp[i].get("from_container_type_id",0l)+"</From_Container_Type_id>");
			re.append("<From_Con_id>"+newPickUp[i].get("from_con_id",0l)+"</From_Con_id>");
			re.append("<Pick_Container_Type>"+newPickUp[i].get("Pick_Container_Type",0)+"</Pick_Container_Type>");
			re.append("<Pick_Container_Type_id>"+newPickUp[i].get("Pick_Container_Type_id",0l)+"</Pick_Container_Type_id>");
			re.append("<Pick_Con_id>"+newPickUp[i].get("Pick_Con_id",0l)+"</Pick_Con_id>");
			re.append("</Details>");
		}
		
		return re.toString();
	}
	
	public String getOperatorArea(long ps_id)
		throws Exception
	{
		try 
		{
			DBRow[] areas = floorLocationMgrZJ.getLocationAreaByPsid(ps_id);
			
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < areas.length; i++) 
			{
				re.append("<Area>");
				re.append("<AreaName>"+areas[i].getString("title")+areas[i].getString("area_name")+"</AreaName>");
				re.append("<AreaId>"+areas[i].getString("area_id")+"</AreaId>");
				re.append("</Area>");
			}
	 
			
			return re.toString();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"operatorArea",log);
		}
	}
	
	/**
	 * 拣货单明细转换XML
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public String xmlOutStoreBillOrderLocationProduct(long out_id)
		throws Exception
	{
		try {
			DBRow[] locationProducts = floorOutboundOrderMgrZJ.getOutStoreBillOrderDetailByOutId(out_id);
			
			StringBuffer re = new StringBuffer();
			for (int i = 0; i < locationProducts.length; i++) 
			{
				re.append("<Details>");
				re.append("<PcId>"+locationProducts[i].get("out_list_pc_id",0l)+"</PcId>");
				re.append("<SlcId>"+locationProducts[i].get("out_list_slc_id",0l)+"</SlcId>");
				re.append("<NeedPickQuantity>"+locationProducts[i].get("need_execut_quantity",0f)+"</NeedPickQuantity>");
				re.append("<From_Container_Type>"+locationProducts[i].get("from_container_type",0)+"</From_Container_Type>");
				re.append("<From_Container_Type_id>"+locationProducts[i].get("from_container_type_id",0l)+"</From_Container_Type_id>");
				re.append("<From_Con_id>"+locationProducts[i].get("from_con_id",0l)+"</From_Con_id>");
				re.append("<Pick_Container_Type>"+locationProducts[i].get("Pick_Container_Type",0)+"</Pick_Container_Type>");
				re.append("<Pick_Container_Type_id>"+locationProducts[i].get("Pick_Container_Type_id",0l)+"</Pick_Container_Type_id>");
				re.append("<Pick_Con_id>"+locationProducts[i].get("Pick_Con_id",0l)+"</Pick_Con_id>");
				re.append("<SN>"+locationProducts[i].getString("serial_number","NULL")+"</SN>");
				
				re.append("<Pick_Container_Qty>"+getPickUpContainerQty(locationProducts[i].get("Pick_Container_Type",0),
						locationProducts[i].get("Pick_Container_Type_id",0l), locationProducts[i].get("need_execut_quantity",0.0d))+"</Pick_Container_Qty>");

				re.append("</Details>");
			}
			
			return re.toString();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"xmlOutStoreBillOrderLocationProduct",log);
		}
	}
	//张睿修改添加一个字段处理如果是拣Container的情况
	//那么告诉<Pick_Container_Qty></Pick_Container_Qty>这种类型的Container数量为多少
	private int getPickUpContainerQty(int containerType , long containerTypeId , double pickupProductTotal) throws Exception  {
		try{
			int count =  0 ;
//			if(containerType == ContainerTypeKey.BLP || containerType == ContainerTypeKey.CLP || containerType == ContainerTypeKey.ILP)
			if(containerType == ContainerTypeKey.CLP)//去掉ILP和BLP
			{
				double totalPiece =  androidMgrZr.getContainerTypeTotalPiece(containerType, containerTypeId);
				count =  (int)(pickupProductTotal / totalPiece);
			}
			return count ;
		}catch (Exception e) {
			throw new SystemException(e,"getPickUpContainerQty",log);
		}
	
	}
	 
	/**
	 * 
	 */
	@Override
	public int printTransportLicensePlate(String xml) throws Exception {
		String details = getSampleNode(xml,"Details");
		String efficacy = getSampleNode(xml,"Efficacy");
		String lpIdStr = getSampleNode(xml, "LpId");
		int length = Integer.parseInt(getSampleNode(xml,"Length"));
		
		String localbase64 = StringUtil.HashBase64(details);
		if(!efficacy.equals(localbase64))
		{
			throw new XMLDataErrorException();
		}
		if(length!=details.length())
		{
			throw new XMLDataLengthException();
		}
		long lp_id = 0;
		if(lpIdStr != null || lpIdStr.trim().length() > 0){
			lp_id = Long.parseLong(lpIdStr) ;
			DBRow container = containerProductMgrZJ.getDetailContainer(lp_id);
			if(container == null){
				throw new LpNotFindException("" +lpIdStr + " Not Found ");
			}
			containerProductMgrZJ.delContainerProductByLP(lp_id);
		}
		String[] licensePlateProducts = details.split("\\|");
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		
		//首先获取LpId  .查询是否存在。如果不存在的话。那么报错
		for (int i = 0; i < licensePlateProducts.length; i++) 
		{
			String[] licensePlateProduct = licensePlateProducts[i].split(",");
			long pc_id = Long.parseLong(licensePlateProduct[0]);
 			
		 
			
			String sn = "";
			if(!licensePlateProduct[2].toLowerCase().equals("null"))
			{
				sn = licensePlateProduct[2];
			}
			float quantity = Float.parseFloat(licensePlateProduct[3]);
			
			DBRow licensePlateProductRow = new DBRow();
			licensePlateProductRow.add("pc_id",pc_id);
			licensePlateProductRow.add("lp_id",lp_id);
			licensePlateProductRow.add("sn",sn);
			licensePlateProductRow.add("quantity",quantity);
			
			list.add(licensePlateProductRow);
		}
		
		containerProductMgrZJ.printLicensePlate(lp_id, list);
		
		return list.size();
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setExpressMgr(ExpressMgrIFace expressMgr) {
		this.expressMgr = expressMgr;
	}

	public void setOrderMgrZJ(OrderMgrIFaceZJ orderMgrZJ) {
		this.orderMgrZJ = orderMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setDeliveryWarehouseMgrZJ(
			DeliveryWarehouseMgrIFaceZJ deliveryWarehouseMgrZJ) {
		this.deliveryWarehouseMgrZJ = deliveryWarehouseMgrZJ;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setTransportWarehouseMgrZJ(
			TransportWarehouseMgrIFaceZJ transportWarehouseMgrZJ) {
		this.transportWarehouseMgrZJ = transportWarehouseMgrZJ;
	}

	public void setTransportOutboundMgrZJ(
			TransportOutboundMgrIFaceZJ transportOutboundMgrZJ) {
		this.transportOutboundMgrZJ = transportOutboundMgrZJ;
	}

	public void setTransportOutboundApproveMgrZJ(
			TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ) {
		this.transportOutboundApproveMgrZJ = transportOutboundApproveMgrZJ;
	}

	public void setDamagedRepairOutboundMgrZJ(
			DamagedRepairOutboundMgrIFaceZJ damagedRepairOutboundMgrZJ) {
		this.damagedRepairOutboundMgrZJ = damagedRepairOutboundMgrZJ;
	}

	public void setDamagedRepairMgrZJ(DamagedRepairMgrIFaceZJ damagedRepairMgrZJ) {
		this.damagedRepairMgrZJ = damagedRepairMgrZJ;
	}

	public void setDamagedRepairOutboundApproveMgrZJ(
			DamagedRepairOutboundApproveMgrIFaceZJ damagedRepairOutboundApproveMgrZJ) {
		this.damagedRepairOutboundApproveMgrZJ = damagedRepairOutboundApproveMgrZJ;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setFloorProductCodeMgrZJ(FloorProductCodeMgrZJ floorProductCodeMgrZJ) {
		this.floorProductCodeMgrZJ = floorProductCodeMgrZJ;
	}

	public void setFloorOutboundOrderMgrZJ(
			FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ) {
		this.floorOutboundOrderMgrZJ = floorOutboundOrderMgrZJ;
	}

	public void setContainerProductMgrZJ(
			ContainerProductMgrIFaceZJ containerProductMgrZJ) {
		this.containerProductMgrZJ = containerProductMgrZJ;
	}

	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ) {
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}

	public void setProductStoreLocationMgrZJ(
			ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ) {
		this.productStoreLocationMgrZJ = productStoreLocationMgrZJ;
	}

	public void setContainerMgrZyj(ContainerMgrIFaceZyj containerMgrZyj) {
		this.containerMgrZyj = containerMgrZyj;
	}

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}

	
}
