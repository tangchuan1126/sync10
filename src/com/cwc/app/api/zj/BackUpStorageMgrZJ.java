package com.cwc.app.api.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorBackUpStorageMgrZJ;
import com.cwc.app.iface.zj.BackUpStorageMgrIFaceZJ;
import com.cwc.app.key.PerformStatusKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class BackUpStorageMgrZJ implements BackUpStorageMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductMgr floorProductMgr;
	private FloorBackUpStorageMgrZJ floorBackUpStorageMgrZJ;
	private ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ;
	
	public void addBackUpStore(long back_up_pc_id,long ps_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		addBackUpStorage(back_up_pc_id, ps_id);
		
		combinationBackUpUnion(ps_id, back_up_pc_id, adminLoggerBean);
	}
	
	public void addBackUpStorage(long back_up_pc_id,long ps_id)
		throws Exception
	{
		try 
		{
			DBRow backUpStorage = new DBRow();
			backUpStorage.add("ps_id",ps_id);
			backUpStorage.add("post_date",DateUtil.NowStr());
			backUpStorage.add("pc_id",back_up_pc_id);
			
			floorBackUpStorageMgrZJ.addBackUpStorage(backUpStorage);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addBackUpStorage",log);
		}
	}
	
	public DBRow getDetailBackUpStore(long pc_id,long ps_id)
		throws Exception
	{
		try 
		{
			return floorBackUpStorageMgrZJ.getDetailBackUpStore(pc_id, ps_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailBackUpStore",log);
		}
	}
	
	/**
	 * 不完整套装减库存
	 * @param deInProductStoreLogBean
	 * @param ps_id
	 * @param split_back_up_union_pc_id
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public long deIncBackUpProductStoreCountByPcid(ProductStoreLogBean deInProductStoreLogBean,long ps_id,long split_back_up_union_pc_id,double value)
		throws Exception
	{	
		try 
		{
			long from_psl_id = 0;//floorBackUpStorageMgrZJ.deIncBackUpProductStoreCountByPcid(deInProductStoreLogBean,ps_id,split_back_up_union_pc_id,1);
			return from_psl_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deIncBackUpProductStoreCountByPcid",log);
		}
	}
	
	/**
	 * 组合不完整套装
	 */
	public void combinationBackUpUnion(long ps_id, long back_up_pc_id,AdminLoginBean adminLoggerBean) 
		throws Exception
	{
		try 
		{
			//减少散件
			DBRow inSetProducts[] = floorProductMgr.getProductsInSetBySetPid(back_up_pc_id);
			long[] fromPslIds = new long[inSetProducts.length];
			for (int i=0; i<inSetProducts.length; i++)
			{
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(0);
//			deInProductStoreLogBean.setBill_type(bill_type);
				
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_BACK_UP_COMBINATION);
				long from_psl_id = 0;//floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id, inSetProducts[i].get("pc_id", 0l),inSetProducts[i].get("quantity", 0f));
				
				fromPslIds[i] = from_psl_id;
			}
			
			//增加非完整套装数量
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(0);
//		inProductStoreLogBean.setBill_type(bill_type);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_BACK_UP_COMBINATION);
			
			long to_psl_id = 0;//floorBackUpStorageMgrZJ.incBackUpProductStoreCountByPcid(inProductStoreLogBean,ps_id,back_up_pc_id,1,0);
			
			//记录批次关系(屏蔽)
			productStoreLogsDetailMgrZJ.combinationToProdcutStoreUnionLog(fromPslIds,to_psl_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"combinationBackUpUnion",log);
		}
	}
	
	/**
	 * 拆散不完整套装
	 * @param request
	 * @throws Exception
	 */
	public void splitBackUpProductForWaybill(HttpServletRequest request)
		throws Exception
	{
		//记录拆分要求
		
		//开始操作库存
		long split_union_pc_id = StringUtil.getLong(request,"split_union_pc_id");//拆散套装ID
		DBRow original_product = floorProductMgr.getDetailProductByPcid(split_union_pc_id);
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		
		String[] usePcId = request.getParameterValues("use_pc_id");
		
		Map<String,String> productUnion = new HashMap<String, String>();
		/**
		 * 开始释放库存
		 */
		//减少主套装数量
		ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
		inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
		inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
		inProductStoreLogBean.setOid(0);
		inProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SPLIT_UNION_STANDARD);
		long from_psl_id = 0;//floorBackUpStorageMgrZJ.deIncBackUpProductStoreCountByPcid(inProductStoreLogBean, ps_id,split_union_pc_id,1);
		//增加散件进库
		DBRow inSetProducts[] = floorProductMgr.getProductsInSetBySetPid(split_union_pc_id);
		for (int i=0; i<inSetProducts.length; i++)
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(inSetProducts[i].get("pc_id", 0l));
			
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(0);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL);
//			deInProductStoreLogBean.setPurchase_unit_price(product.get("unit_price",0d));
			
			//floorProductMgr.incProductStoreCountByPcid(deInProductStoreLogBean, ps_id, inSetProducts[i].get("pc_id", 0l),inSetProducts[i].get("quantity", 0f),from_psl_id);
			
			productUnion.put(inSetProducts[i].getString("pc_id"),inSetProducts[i].getString("quantity"));
			
		}
		/**
		 * 释放库存结束
		 */
		
		DBRow productCustom = new DBRow();
		productCustom.add("unit_name",original_product.getString("unit_name"));
		productCustom.add("length",original_product.get("length",0f));
		productCustom.add("width",original_product.get("width",0f));
		productCustom.add("heigth",original_product.get("heigth",0f));
		productCustom.add("orignal_pc_id",split_union_pc_id);
		productCustom.add("p_name",original_product.getString("p_name")+" ▲");
		productCustom.add("catalog_id",original_product.get("catalog_id", 0l));
		
		long pc_pc_id = floorProductMgr.addProductCustom(productCustom);
		//修改文件名为商品ID以区分
		DBRow pnamePara = new DBRow();
		pnamePara.add("p_name",pc_pc_id);
		floorProductMgr.modProduct(pc_pc_id,pnamePara);

		/**
		 * 构建拆散不完整套装的套装关系
		 */
		for (int i = 0; i < usePcId.length; i++) 
		{
			float use_quantity = StringUtil.getFloat(request,"use_quantity_"+usePcId[i]);
			
			float quantity = Float.parseFloat(productUnion.get(usePcId[i]));
			
			quantity = quantity - use_quantity;
			
			if(quantity>0)
			{
				productUnion.put(usePcId[i],String.valueOf(quantity));
			}
			else
			{
				productUnion.remove(usePcId[i]);
			}
		}
		
		Set<String> productUnionSet = productUnion.keySet();
		
		if (productUnionSet.size()>0) 
		{
			for (String pcid : productUnionSet) 
			{
				DBRow customPro = new DBRow();
				customPro.add("set_pid",pc_pc_id);
				customPro.add("pid",pcid);
				customPro.add("quantity",productUnion.get(pcid));
				floorProductMgr.addProductCustomUnion(customPro);
			}
		}
		
		//非完整套装创建库存及拼装
		this.addBackUpStore(pc_pc_id, ps_id, adminLoggerBean);
	}
	
	/**
	 * 返回拆分需求
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public Map<String,DBRow> splitRequire(long out_id)
		throws Exception
	{
		DBRow[] splitRequireDetails = floorBackUpStorageMgrZJ.getSplitRequireDetailSplitAndToPcidByOut(out_id);
		
		Map<String,String> alreadFor = new HashMap<String, String>();
		Map<String,DBRow> result = new HashMap<String,DBRow>();
		
		for(int i = 0;i<splitRequireDetails.length;i++)
		{
			long split_pc_id = splitRequireDetails[i].get("split_pc_id",0l);
			long to_pc_id = splitRequireDetails[i].get("to_pc_id",0l);

			if(!alreadFor.containsKey(String.valueOf(to_pc_id)))
			{
				DBRow oneAllProcessProduct = new DBRow();
				long lastToPcid = 0;
				boolean isWhile = true;
				do
				{
					alreadFor.put(String.valueOf(to_pc_id),String.valueOf(to_pc_id));
					
					DBRow[] splitReauire = floorBackUpStorageMgrZJ.getSplitRequireDetailBySplitPcid(split_pc_id, to_pc_id);
					if(splitReauire.length>0)
					{
						for (int j = 0; j < splitReauire.length; j++) 
						{
							addAllProcessProductQuantity(oneAllProcessProduct,splitReauire[j].get("need_pc_id",0l),splitReauire[j].get("need_quantity",0f));
						}
						
						split_pc_id = splitReauire[0].get("to_pc_id",0l);
						to_pc_id = floorBackUpStorageMgrZJ.getToPcIdBySplitPcidAndOut(split_pc_id, out_id);
						
						if(to_pc_id==-1)
						{
							lastToPcid = split_pc_id;
							isWhile = false;
						}
					}
				}
				while(isWhile);
				
				result.put(splitRequireDetails[i].getString("split_pc_id")+","+lastToPcid,oneAllProcessProduct);
			}
		}
		
		return result;
	}
	
	/**
	 * 记录起始商品到完成提取商品的数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addAllProcessProductQuantity(DBRow productCount,long pid,float quantity)
		throws Exception
	{
		if ( productCount.getString(String.valueOf(pid)).equals(""))
		{
			productCount.add(String.valueOf(pid), quantity);
		}
		else
		{
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}
	
	/**
	 * 根据出库单执行拆货要求
	 * @param out_id
	 * @throws Exception
	 */
	public void performSplitRequireDetail(HttpServletRequest request)
		throws Exception
	{
		long out_id = StringUtil.getLong(request,"out_id");
		
		DBRow[] splitRequireDetails = floorBackUpStorageMgrZJ.getSplitRequireDetailsByOut(out_id);
		
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		
		for (int i = 0; i < splitRequireDetails.length; i++) 
		{
			long srd_id = splitRequireDetails[i].get("srd_id",0l);
			DBRow para = new DBRow();
			para.add("is_perform",PerformStatusKey.Executed);
			para.add("perform_adid",adminLoggerBean.getAdid());
			para.add("perform_date",DateUtil.NowStr());
			
			floorBackUpStorageMgrZJ.modSplitRequireDetail(srd_id, para);
		}
	}
	
	/**
	 * 判断出库单是否有拆货要求
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public boolean existsSplitRequireDetailForOut(long out_id)
		throws Exception
	{
		DBRow[] splitRequireDetails = floorBackUpStorageMgrZJ.getSplitRequireDetailSplitAndToPcidByOut(out_id);
		
		if(splitRequireDetails.length>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean existsSplitRequireDetailForWaybill(long waybill_id)
		throws Exception
	{
		DBRow[] splitRequireDetails = floorBackUpStorageMgrZJ.getSplitRequireDetailByWaybillId(waybill_id);
		
		if(splitRequireDetails.length>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * 根据出库单获得最后商品ID
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getToPcidsByOut(long out_id)
		throws Exception
	{
		try 
		{
			Map<String,DBRow> splitRequire = this.splitRequire(out_id);
			
			Set<String> ids = splitRequire.keySet();
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (String id : ids) 
			{
				long to_pc_id = Long.parseLong(id.split(",")[1]);
				
				if (to_pc_id !=0)
				{
					DBRow row = new DBRow();
					row.add("to_pc_id",to_pc_id);
					list.add(row);
				}
			}
			
			return list.toArray(new DBRow[0]);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getToPcidsByOut",log);
		}
	}
	

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorBackUpStorageMgrZJ(
			FloorBackUpStorageMgrZJ floorBackUpStorageMgrZJ) {
		this.floorBackUpStorageMgrZJ = floorBackUpStorageMgrZJ;
	}

	public void setProductStoreLogsDetailMgrZJ(
			ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ) {
		this.productStoreLogsDetailMgrZJ = productStoreLogsDetailMgrZJ;
	}
	
	

}
