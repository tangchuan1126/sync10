package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorDoubtGoodsMgrZJ;
import com.cwc.app.floor.api.zj.FloorFileMgrZJ;
import com.cwc.app.iface.zj.DoubtGoodsMgrIFaceZJ;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class DoubtGoodsMgrZJ implements DoubtGoodsMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorDoubtGoodsMgrZJ floorDoubtGoodsMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorFileMgrZJ floorFileMgrZJ;
	
	public long addDoubtProduct(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			long product_line_id = StringUtil.getLong(request,"filter_productLine");
			String doubt_goods_img = StringUtil.getString(request,"file_names");
			
			DBRow dbrow = new DBRow();
			
			dbrow.add("create_time",DateUtil.NowStr());
			dbrow.add("create_adid",adminLoggerBean.getAdid());
			dbrow.add("product_line_id",product_line_id);
			dbrow.add("doubt_goods_img",doubt_goods_img);
			
			long dg_id = floorDoubtGoodsMgrZJ.addDoubtGoods(dbrow);
			
			String[] doubtGoodsImgFiles = doubt_goods_img.split(",");
			
			for (int i = 0; i < doubtGoodsImgFiles.length; i++) 
			{
				String temp_url = Environment.getHome()+"upl_imags_tmp/"+doubtGoodsImgFiles[i];
				String invoice_path = "upload/doubt_goods/"+doubtGoodsImgFiles[i];
				String url = Environment.getHome()+invoice_path;
				FileUtil.moveFile(temp_url,url);
				
				DBRow file = new DBRow();
				file.add("file_name",doubtGoodsImgFiles[i]);
				file.add("file_with_id",dg_id);
				file.add("file_with_type",FileWithTypeKey.DOUBTGOODS);
				file.add("upload_adid",adminLoggerBean.getAdid());
				file.add("upload_time",DateUtil.NowStr());
				floorFileMgrZJ.addFile(file);
			}
			
			return dg_id;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
		
	}

	/**
	 * 过滤疑问商品
	 */
	public DBRow[] filterDoubtProduct(long product_line_id,int is_answerd,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			return floorDoubtGoodsMgrZJ.filterDoubtGoods(product_line_id,is_answerd,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterDoubtProduct",log);
		}
	}

	/**
	 * 根据ID获得疑问商品详细
	 */
	public DBRow getDetailDoubtProduct(long dg_id) 
		throws Exception 
	{
		try 
		{
			return floorDoubtGoodsMgrZJ.getDetailDoubtGoods(dg_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailDoubtProduct",log);
		}
	}
	
	/**
	 * 解答疑问商品
	 */
	public void answerDoubtProduct(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			String p_name = StringUtil.getString(request,"p_name");
			long dg_id = StringUtil.getLong(request,"dg_id");
			int unable_identify = StringUtil.getInt(request,"unable_identify",0);
			
			
			
			
			DBRow para = new DBRow();
			if(unable_identify==1)
			{
				p_name = "无法识别";
			}
			else
			{
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				long pc_id = 0;
				if (product !=null)
				{
					pc_id = product.get("pc_id",0l);
					
					DBRow productCatalog = floorCatalogMgr.getDetailProductCatalogById(product.get("catalog_id",0l));
					
					para.add("pc_id",pc_id);
					para.add("product_line_id",productCatalog.get("product_line_id",0l));
				}
			}
			para.add("answer_time",DateUtil.NowStr());
			para.add("answer_adid",adminLoggerBean.getAdid());
			para.add("p_name",p_name);
			floorDoubtGoodsMgrZJ.modDoubtGoods(dg_id, para);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"answerDoubtProduct",log);
		}
		
	}
	
	public int waitAnswerDoubtGoodCount()
		throws Exception
	{
		try 
		{
			return floorDoubtGoodsMgrZJ.waitAnswerDoubtGoodCount();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"waitAnswerDoubtGoodCount",log);
		}
	}
	
	public void initDoubtGoodImg()
		throws Exception
	{
		DBRow[] doubtgoods = floorDoubtGoodsMgrZJ.filterDoubtGoods(0,0,null);
		for (int i = 0; i < doubtgoods.length; i++) 
		{
			DBRow file = new DBRow();
			file.add("file_name",doubtgoods[i].getString("doubt_goods_img"));
			file.add("file_with_id",doubtgoods[i].get("dg_id",0l));
			file.add("file_with_type",FileWithTypeKey.DOUBTGOODS);
			file.add("upload_adid",doubtgoods[i].get("create_adid",0l));
			file.add("upload_time",doubtgoods[i].getString("create_time"));
			floorFileMgrZJ.addFile(file);
		}
	}

	public void setFloorDoubtGoodsMgrZJ(FloorDoubtGoodsMgrZJ floorDoubtGoodsMgrZJ) {
		this.floorDoubtGoodsMgrZJ = floorDoubtGoodsMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setFloorFileMgrZJ(FloorFileMgrZJ floorFileMgrZJ) {
		this.floorFileMgrZJ = floorFileMgrZJ;
	}

	

}
