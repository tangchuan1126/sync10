package com.cwc.app.api.zj;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;





import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.dhl.DHLClient;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorWaybillLogMgrZJ;
import com.cwc.app.iface.zj.WaybillLogMgrIFaceZJ;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.WaybillInternalOperKey;
import com.cwc.app.key.WaybillInternalTrackingKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.fedex.FedexClient;
import com.cwc.fedex.TrackWebServiceClient;
import com.cwc.util.StringUtil;

public class WaybillLogMgrZJ implements WaybillLogMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	static Logger tracklog = Logger.getLogger("TRACK");
	private FloorWaybillLogMgrZJ floorWaybillLogMgrZJ;
	private FloorExpressMgr floorExpressMgr;
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private FloorOrderMgr floorOrderMgr;
	
	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void updateArtificialWaybillTracking(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			int internal_tracking_status = StringUtil.getInt(request,"internal_tracking_status");
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow waybillLog = new DBRow();
			waybillLog.add("waybill_id",waybill_id);
			waybillLog.add("waybill_internal_status",WaybillInternalOperKey.ArtificialModification);
			waybillLog.add("trackingNumber",waybill.getString("tracking_number"));
			waybillLog.add("sc_id",waybill.get("sc_id",0l));
			waybillLog.add("operator_adid",adminLoggerBean.getAdid());
			waybillLog.add("operator_time",DateUtil.NowStr());
			waybillLog.add("from_waybill_id",waybill.getString("parent_waybill_id"));
			waybillLog.add("waybill_external_status",internal_tracking_status);
			floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
			
			modificationTrackingWaybill(internal_tracking_status,waybill_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateWaybillTracking",log);
		}
	}
	
	/**
	 * 运单日志过滤
	 */
	public DBRow[] filterWayBillLog(String trackingNumber, String st,String en, int waybill_internal_status,int waybill_external_status,long adid,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			return floorWaybillLogMgrZJ.filterWayBillLog(trackingNumber, st, en, waybill_internal_status,waybill_external_status,adid,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterWayBillLog",log);
		}
	}
	
	/**
	 * 查询已发货但是尚未妥投的运单日志
	 */
	public DBRow[] needTrackingWayBillLog(String st, String en)
		throws Exception 
	{
		
		try 
		{
			return floorWaybillLogMgrZJ.needTrackingWaybillLog(st, en);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"needTrackingWayBillLog",log);
		}
	}
	
	/**
	 * 运单追踪
	 * @param waybill_log_id
	 * @throws Exception
	 */
	public void trackingWaybill(long waybill_log_id)
		throws Exception
	{
		DBRow waybillLog = floorWaybillLogMgrZJ.getDetailWayBillLog(waybill_log_id);
		
		DBRow shipCompany = floorExpressMgr.getDetailCompany(waybillLog.get("sc_id",0l));
		try 
		{			
			Map result = null;
			String shippingCompany = "";
			
			if(shipCompany.getString("name").trim().toLowerCase().contains("dhl"))
			{
				result = null;
				shippingCompany = "DHL";
				DHLClient dhlClient = new DHLClient();
				result = dhlClient.commitTracking(waybillLog.getString("trackingNumber"));
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("usps"))
			{
				
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("fedex"))
			{
				result = null;
				shippingCompany = "FEDEX";
				
				TrackWebServiceClient trackWebServiceClient = new TrackWebServiceClient();
				result = trackWebServiceClient.trackCommit(waybillLog.getString("trackingNumber"));
			}
			
//			long start = System.currentTimeMillis();
			if(result != null)
			{
				DBRow[] shippments = (DBRow[]) result.get("shippments");
				
				
				int finalStatus = WaybillInternalTrackingKey.NormalTransport;//整体状态标志判断预先设置为运输中
					
				//判断传回来的是否有妥投，如果有妥投，最终状态设置为妥投，并且整体状态标志为妥投
				for (int i = 0; i < shippments.length; i++) 
				{
					DBRow waybillTracingCode = floorWaybillLogMgrZJ.getWayBillTrackingStatusCode(shippments[i].getString("status_code"),shippingCompany);
					if(waybillTracingCode!=null&&waybillTracingCode.get("tracking_status",0)==WaybillInternalTrackingKey.DeliveryCompleted)
					{
						finalStatus = WaybillInternalTrackingKey.DeliveryCompleted;
						
						updateTrackingWaybill(shippments[i].getString("status_code"),Float.valueOf(result.get("weight").toString()), finalStatus,shippments[i].getString("description"),waybillLog.get("waybill_id",0l));
					}
				}

				//默认整体状态标志有没有被改动过，如果未被改动，说明没有出现妥投的情况，循环判断是否有派送特例，如果有派送特例，最终状态设置为派送特例，并且整体状态标志为派送特例
				if(finalStatus==WaybillInternalTrackingKey.NormalTransport)
				{
					for (int i = 0; i < shippments.length; i++) 
					{
						DBRow waybillTracingCode = floorWaybillLogMgrZJ.getWayBillTrackingStatusCode(shippments[i].getString("status_code"),shippingCompany);
						if(waybillTracingCode!=null&&waybillTracingCode.get("tracking_status",0)==WaybillInternalTrackingKey.AddressError)
						{
							finalStatus = WaybillInternalTrackingKey.AddressError;
							
							updateTrackingWaybill(shippments[i].getString("status_code"),Float.valueOf(result.get("weight").toString()), finalStatus,shippments[i].getString("description"),waybillLog.get("waybill_id",0l));
						}
					}
				}
				
				//默认整体状态标志有没有被改动过，如果未被改动，说明没有出现妥投 或 派送特例的情况，循环判断是否有清关特例，如果有清关特例，最终状态设置为清关特例，并且整体状态标志为清关特例
				if(finalStatus==WaybillInternalTrackingKey.NormalTransport)
				{
					for (int i = 0; i < shippments.length; i++) 
					{
						DBRow waybillTracingCode = floorWaybillLogMgrZJ.getWayBillTrackingStatusCode(shippments[i].getString("status_code"),shippingCompany);
						if(waybillTracingCode!=null&&waybillTracingCode.get("tracking_status",0)==WaybillInternalTrackingKey.CustomsClearanceError)
						{
							finalStatus = WaybillInternalTrackingKey.CustomsClearanceError;
							
							updateTrackingWaybill(shippments[i].getString("status_code"),Float.valueOf(result.get("weight").toString()), finalStatus,shippments[i].getString("description"),waybillLog.get("waybill_id",0l));
						}
					}
				}
				
				//默认整体状态标志有没有被改动过，如果未被改动，说明没有出现妥投 或 派送特例或清关特例的情况，判断是否只有一条流程信息，如果只有一条，说明货物未提，最终状态设置为未提，并且整体状态标志为未提
				if(finalStatus==WaybillInternalTrackingKey.NormalTransport)
				{
					if(shippments.length==1)//Fedex有一条记录就是待取货，DHL没有记录是待取货
					{
						finalStatus = WaybillInternalTrackingKey.WaitPickUp;
						
						if(shippments.length>0)
						{
							updateTrackingWaybill(shippments[0].getString("status_code"),Float.valueOf(result.get("weight").toString()), finalStatus,shippments[0].getString("description"),waybillLog.get("waybill_id",0l));
						}
						else
						{
							updateTrackingWaybill("",Float.valueOf(result.get("weight").toString()), finalStatus,"",waybillLog.get("waybill_id",0l));
						}
					}
				}
				
				if(finalStatus==WaybillInternalTrackingKey.NormalTransport)
				{
					if(shippments.length>0)
					{
						if(waybillLog.get("waybill_external_status",0)!=WaybillInternalTrackingKey.CustomsClearanceError&&waybillLog.get("waybill_external_status",0)!=WaybillInternalTrackingKey.AddressError)
						{
							updateTrackingWaybill(shippments[0].getString("status_code"),Float.valueOf(result.get("weight").toString()), finalStatus,shippments[0].getString("description"),waybillLog.get("waybill_id",0l));
						}
						
					}
				}
			}
//			long end = System.currentTimeMillis();
//			//system.out.println(shippingCompany+":"+waybillLog.getString("trackingNumber")+"---time:"+(end-start));
		}
		catch (Exception e) 
		{
			if(!e.toString().contains("java.net.")||!e.getMessage().contains("timeout"))
			{
				tracklog.error(shipCompany.getString("name")+":"+waybillLog.getString("trackingNumber")+" "+e.toString());
				e.printStackTrace();
			}
			
			log.error("JmsError:"+shipCompany.getString("name")+":"+waybillLog.getString("trackingNumber")+e);
			throw e;
		}
	}
	
	/**
	 * 更新运单日志与运单的追踪状态
	 * @param status_code
	 * @param weight
	 * @param finalStatus
	 * @param description
	 * @param waybill_id
	 * @throws Exception
	 */
	private void updateTrackingWaybill(String status_code,float weight,int finalStatus,String description,long waybill_id)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("tracking_status_code",status_code);
			para.add("tracking_weight",weight);
			para.add("waybill_external_status",finalStatus);
			para.add("tracking_definition",description);
			
			floorWaybillLogMgrZJ.modWayBIllLogWayBillId(waybill_id, para);
			
			DBRow waybillPara = new DBRow();
			waybillPara.add("internal_tracking_status",finalStatus);
			waybillPara.add("tracking_weight",weight);
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, waybillPara);
			
			this.updateOrderTracking(waybill_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateTrackingWaybill",log);
		}
	}
	
	/**
	 * 手工跟踪运单
	 * @param finalStatus
	 * @param waybill_id
	 * @throws Exception
	 */
	private void modificationTrackingWaybill(int finalStatus,long waybill_id)
		throws Exception
	{
		try {
			DBRow para = new DBRow();
			para.add("waybill_external_status",finalStatus);
			
			floorWaybillLogMgrZJ.modWayBIllLogWayBillId(waybill_id, para);
			
			DBRow waybillPara = new DBRow();
			waybillPara.add("internal_tracking_status",finalStatus);
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, waybillPara);
			
			this.updateOrderTracking(waybill_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modificationTrackingWaybill",log);
		}
	}
	
	/**
	 * 根据运单ID获得日志
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillLogByWaybillId(long waybill_id)
		throws Exception
	{
		try
		{
			return (floorWaybillLogMgrZJ.getWaybillLogByWaybillId(waybill_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillLogByWaybillId",log);
		}
	}
	
	public void initWayBillLog(int waybill_status,String st,String en) 
		throws Exception 
	{
		DBRow[] rows = floorWaybillLogMgrZJ.initWaybillLog(waybill_status,st,en);
		
		for (int i = 0; i < rows.length; i++) 
		{
			DBRow[] row = floorWaybillLogMgrZJ.getWaybillLogByWaybillId(rows[i].get("waybill_id",0l));
			
			if(row.length>0)
			{
				continue;
			}
			else
			{
				DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(rows[i].get("waybill_id",0l));
				
				DBRow waybillLog = new DBRow();
				waybillLog.add("waybill_id",waybill.get("waybill_id",0l));
				waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Send);
				waybillLog.add("trackingNumber",waybill.getString("tracking_number"));
				waybillLog.add("sc_id",waybill.get("sc_id",0l));
				waybillLog.add("operator_adid",waybill.getString("delivery_account"));
				waybillLog.add("operator_time",waybill.getString("delivery_date").equals("")?waybill.getString("print_date"):waybill.getString("delivery_date"));
				waybillLog.add("from_waybill_id",waybill.get("parent_waybill_id",0l));
				floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
			}
			////system.out.println(i+"/"+rows.length);
		}
		
	}
	
	public void updateOrderTracking(long waybill_id)
		throws Exception
	{
		DBRow[] orders = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		for (int i = 0; i < orders.length; i++) 
		{	
			DBRow[] checkWaybills = floorWayBillOrderMgrZJ.getWayBillByOid(orders[i].get("oid",0l),null);
			
			int finalStatus = WaybillInternalTrackingKey.NormalTransport;//整体状态标志判断预先设置为运输中
			
			//查询订单下所有运单是否都妥投，都妥投则标记订单为已妥投
			for (int j = 0; j < checkWaybills.length; j++) 
			{
				if (checkWaybills[j].get("internal_tracking_status",0)==WaybillInternalTrackingKey.DeliveryCompleted) 
				{
					finalStatus = WaybillInternalTrackingKey.DeliveryCompleted;
				}
				else
				{
					finalStatus = WaybillInternalTrackingKey.NormalTransport;//未妥投，更新回初始状态
					break;//有一个不妥投，跳出循环
				}
			}
			
			//默认状态未被改动，订单非已妥投，判断是否清关例外
			if(finalStatus == WaybillInternalTrackingKey.NormalTransport)
			{
				for (int j = 0; j < checkWaybills.length; j++) 
				{
					if(checkWaybills[j].get("internal_tracking_status",0)==WaybillInternalTrackingKey.CustomsClearanceError) 
					{
						finalStatus = WaybillInternalTrackingKey.CustomsClearanceError;//有清关例外，跳出循环
						break;
					}
				}
			}
			
			//默认状态未被改动，订单非已妥投、清关例外，判断是否派送例外
			if(finalStatus == WaybillInternalTrackingKey.NormalTransport)
			{
				for (int j = 0; j < checkWaybills.length; j++) 
				{
					if(checkWaybills[j].get("internal_tracking_status",0)==WaybillInternalTrackingKey.AddressError) 
					{
						finalStatus = WaybillInternalTrackingKey.AddressError;//有派送例外，跳出循环
						break;
					}
				}
			}
			
			//默认状态未被改动，订单非已妥投、清关例外、派送例外，判断是否等待
			if(finalStatus == WaybillInternalTrackingKey.NormalTransport)
			{
				for (int j = 0; j < checkWaybills.length; j++) 
				{
					if(checkWaybills[j].get("internal_tracking_status",0)==WaybillInternalTrackingKey.WaitPickUp) 
					{
						finalStatus = WaybillInternalTrackingKey.WaitPickUp;//有等待提货，跳出循环
						break;
					}
				}
			}
			
			//修改订单的追踪状态
			DBRow para = new DBRow();
				
			para.add("internal_tracking_status",finalStatus);
			
			if(finalStatus == WaybillInternalTrackingKey.DeliveryCompleted)
			{
				para.add("trace_flag",0);
			}
			
			floorOrderMgr.modPOrder(orders[i].get("oid",0l),para);
			
			
			//如果是派送例外，先检查订单的跟进日志里是否有过派送例外的跟进，如果没有，系统自动跟进一次
			if(finalStatus == WaybillInternalTrackingKey.AddressError)
			{
				DBRow[] deliveryException = floorOrderMgr.getDeliveryExceptionTraceNoteByOid(orders[i].get("oid",0l));
				
				if(deliveryException.length==0)
				{
					DBRow porder = floorOrderMgr.getDetailPOrderByOid(orders[i].get("oid",0l));

					DBRow delivererInfo = new DBRow();
					delivererInfo.add("oid",orders[i].get("oid",0l));
					delivererInfo.add("note","第一次发生派送例外,系统自动跟进");
					delivererInfo.add("account","admin");
					delivererInfo.add("adid",100198);
					delivererInfo.add("trace_type",TracingOrderKey.WAYBILL_TRACKING);
					delivererInfo.add("post_date", DateUtil.NowStr());
					delivererInfo.add("rel_id",0);
					delivererInfo.add("trace_child_type",porder.get("internal_tracking_status",0));
					floorOrderMgr.addPOrderNote(delivererInfo);
					
					DBRow paraOrder = new DBRow();
					paraOrder.add("trace_type",TracingOrderKey.WAYBILL_TRACKING);
					paraOrder.add("trace_date",DateUtil.NowStr());
					paraOrder.add("trace_operator_role_id",10000);
					paraOrder.add("trace_pre_operator_role_id",porder.get("trace_operator_role_id",0l));
					floorOrderMgr.modPOrder(orders[i].get("oid",0l),paraOrder);
				}
			}
		}
		
	}
	

	public void setFloorWaybillLogMgrZJ(FloorWaybillLogMgrZJ floorWaybillLogMgrZJ) {
		this.floorWaybillLogMgrZJ = floorWaybillLogMgrZJ;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setFloorExpressMgr(FloorExpressMgr floorExpressMgr) {
		this.floorExpressMgr = floorExpressMgr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}
	
	public static void main(String[] args) 
		throws Exception 
	{
		TrackWebServiceClient trackWebServiceClient = new TrackWebServiceClient();
		Map result = trackWebServiceClient.trackCommit("796561383808");
		
		DBRow[] shippments = (DBRow[]) result.get("shippments");
		
		
		int finalStatus = WaybillInternalTrackingKey.NormalTransport;//整体状态标志判断预先设置为运输中
			
		//判断传回来的是否有妥投，如果有妥投，最终状态设置为妥投，并且整体状态标志为妥投
		for (int i = 0; i < shippments.length; i++) 
		{
			////system.out.println(shippments[i].getString("status_code"));
		}
		
	}
}
