package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorTradeMgrZJ;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.TradeMgrIFaceZJ;
import com.cwc.app.lucene.zj.TradeIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class TradeMgrZJ implements TradeMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorOrderMgrZr floorOrderMgrZr;
	private FloorTradeMgrZJ floorTradeMgrZJ;
	private SystemConfigIFace systemConfig;
	
	public DBRow[] searchTrades(String search_key,int search_mode,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"",""); 
				search_key = search_key.replaceAll("\\*","");
				search_key +="*";
			}
			
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return TradeIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(),page_count,search_mode,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"searchTrades",log);
		}
	}
	
	/**
	 * 通过订单ID查询创建订单的交易
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getCreateOrderTradeByOid(long oid)
		throws Exception
	{
		try 
		{
			return floorOrderMgrZr.getTradeByOrderIdAndIsCreateOrder(oid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getCreateOrderTradeByOid",log);
		}
	}
	
	/**
	 * 根据trade_id获得tradeItems
	 * @param trade_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTradeItemsByTradeId(long trade_id)
		throws Exception
	{
		try 
		{
			return floorTradeMgrZJ.getTradeItemsByTradeId(trade_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTradeItemsByTradeId",log);
		}
	}

	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}

	public void setFloorTradeMgrZJ(FloorTradeMgrZJ floorTradeMgrZJ) {
		this.floorTradeMgrZJ = floorTradeMgrZJ;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
}
