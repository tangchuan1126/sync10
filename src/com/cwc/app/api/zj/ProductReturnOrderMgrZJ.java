package com.cwc.app.api.zj;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.Cart;
import com.cwc.app.api.CartReturn;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnVoiceEmailPageString;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorFileMgrZJ;
import com.cwc.app.floor.api.zj.FloorReturnProductMgrZJ;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.ProductReturnOrderMgrIFaceZJ;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductStatusKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.TransactionType;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class ProductReturnOrderMgrZJ implements ProductReturnOrderMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorReturnProductMgrZJ floorReturnProductMgrZJ;
	private CartReturnProductReason cartReturnProductReason;
	private OrderMgrIFace orderMgr;
	private CartReturn cartReturn;
	private FloorProductMgr floorProductMgr;
	private FloorOrderMgr floorOrderMgr;
	private FloorOrderMgrZr floorOrderMgrZr;
	private FloorFileMgrZJ floorFileMgrZJ;
	
	/**
	 * 申请质保
	 */
	public long applicationWarranty(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long oid = StringUtil.getLong(request,"oid");
			
			cartReturnProductReason.flush(StringUtil.getSession(request));
			DBRow[] productReasons = cartReturnProductReason.getProductReason();
			
			if(productReasons==null||productReasons.length==0)
			{
				throw new Exception("cartReturnProductReason is null");
			}
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			DBRow productReturn = new DBRow();
			productReturn.add("oid",oid);
			productReturn.add("create_user",adminLoggerBean.getAdid());
			productReturn.add("create_date",DateUtil.NowStr());
			
			long rp_id = floorReturnProductMgrZJ.addReturnProduct(productReturn);
			
			for (int i = 0; i < productReasons.length; i++) 
			{
				DBRow returnProductReason = new DBRow();
				
				returnProductReason.add("pc_id",productReasons[i].get("cart_pid",0l));
				returnProductReason.add("quantity",productReasons[i].get("cart_quantity",0f));
				returnProductReason.add("return_reason",productReasons[i].getString("return_reason"));
				returnProductReason.add("warranty_type",productReasons[i].get("warranty_type",0));
				returnProductReason.add("rp_id",rp_id);
				returnProductReason.add("create_time",DateUtil.NowStr());
				
				floorReturnProductMgrZJ.addReturnProductReason(returnProductReason);
			}
			
			//保存成功清理购物车
			cartReturnProductReason.clearReturnProductReasonCart(StringUtil.getSession(request));
			
			//创建交易
			DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
			
			DBRow trade = new DBRow();
			trade.add("client_id",order.getString("client_id"));
			trade.add("relation_type",0);
			trade.add("relation_id",oid);
			trade.add("parent_txn_id ",order.getString("txn_id"));
			trade.add("is_create_order",0);
			trade.add("transaction_type",TransactionType.Warranty);
			trade.add("delivery_address_name",order.getString("address_name"));
			trade.add("delivery_address_street",order.getString("address_street"));
			trade.add("delivery_address_city",order.getString("address_city"));
			trade.add("delivery_address_state",order.getString("address_state"));
			trade.add("delivery_address_zip",order.getString("address_zip"));
			trade.add("delivery_address_country_code",order.getString("address_country_code"));
			trade.add("delivery_address_country",order.getString("address_country"));
			trade.add("delivery_pro_id",order.get("pro_id",0l));
			trade.add("delivery_ccid_id",order.get("ccid",0l));
			
			floorOrderMgrZr.insertTrade(trade);
			
			orderMgr.addPOrderNotePrivate(oid,"", TracingOrderKey.CREATE_WARRANTY, StringUtil.getSession(request),rp_id);
			
			return (rp_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"applicationWarranty",log);
		}
	}
	
	/**
	 * 标记质保是否需要退货
	 * @param request
	 * @return
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public long markNormalWarrantying(HttpServletRequest request)
		throws OperationNotPermitException,Exception
	{
		try
		{
			String note = StringUtil.getString(request, "note");
			int return_product_flag = StringUtil.getInt(request, "return_product_flag");
			long rp_id = StringUtil.getLong(request,"rp_id");
			
			DBRow productReturn  = floorReturnProductMgrZJ.getDetailReturnProduct(rp_id);
			
			long oid = productReturn.get("oid",0l);
			
			
//			DBRow order = new DBRow();
//			
//			order.add("after_service_status", AfterServiceKey.NORMAL_WARRANTYING);
//			fom.modPOrder(oid, order);
//			
			if (return_product_flag==1)
			{
				this.addReturnProducts(request,false);//第二个参数为false表示不直接发送邮件
			}
			else
			{
				DBRow para = new DBRow();
				para.add("product_status",ReturnProductStatusKey.NoReturn);
				floorReturnProductMgrZJ.modReturnProduct(rp_id, para);
			}
			
			orderMgr.addPOrderNotePrivate(oid, note, TracingOrderKey.NORMAL_WARRANTYING, StringUtil.getSession(request),rp_id);
			cartReturn.clearCart(StringUtil.getSession(request));
			return (rp_id);
		}
		catch (OperationNotPermitException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markNormalWarrantying",log);
		}
	}
	
	public long addReturnProducts(HttpServletRequest request,boolean flag)
		throws Exception
	{
		try
		{
			long rp_id = StringUtil.getLong(request, "rp_id");
			long oid = StringUtil.getLong(request, "oid");
			long ps_id = StringUtil.getLong(request, "ps_id");
			
			DBRow returnProductInfo = new DBRow();
			returnProductInfo.add("ps_id", ps_id); 
			returnProductInfo.add("product_status",ReturnProductStatusKey.Waiting);
			floorReturnProductMgrZJ.modReturnProduct(rp_id, returnProductInfo);
	
			/**
			 * 插入散件的时候，先插入到数据池，如果有重复商品，则累加数量
			 * pid做主键
			 */
			HashMap<String, DBRow> tmpDataPool = new HashMap<String, DBRow>();
			
			//先清空原来的退货商品记录
			floorReturnProductMgrZJ.delReturnProductItemsByRpId(rp_id);
			floorReturnProductMgrZJ.delReturnProductSubItemsByRpId(rp_id);
			
			//插入退货主商品
			cartReturn.flush(StringUtil.getSession(request));
			DBRow returnProducts[] = cartReturn.getDetailProducts();
			for (int i=0; i<returnProducts.length; i++)
			{
				DBRow returnProduct = new DBRow();
				returnProduct.add("p_name", returnProducts[i].getString("p_name"));
				returnProduct.add("pid", returnProducts[i].getString("cart_pid"));
				returnProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
				returnProduct.add("unit_name", returnProducts[i].getString("unit_name"));
				returnProduct.add("product_type", returnProducts[i].getString("cart_product_type"));
				returnProduct.add("rp_id", rp_id);
				long rpi_id = floorReturnProductMgrZJ.addReturnProductItems(returnProduct);
				
				//分解套装成散件
				if (returnProducts[i].getString("cart_product_type").equals(String.valueOf(ProductTypeKey.UNION_STANDARD)))
				{
					DBRow returnSubProducts[] = floorProductMgr.getProductsInSetBySetPid(returnProducts[i].get("pc_id", 0l));
					for (int j=0; j<returnSubProducts.length; j++)
					{
						if (tmpDataPool.containsKey(returnSubProducts[j].getString("pc_id")))
						{
							DBRow returnSubProduct =  tmpDataPool.get(returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
						else
						{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnSubProducts[j].getString("p_name"));
							returnSubProduct.add("pid", returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							returnSubProduct.add("unit_name", returnSubProducts[j].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
					}
				}
				else
				{
					//普通商品直接插入
					if (tmpDataPool.containsKey(returnProducts[i].getString("cart_pid")))
					{
						DBRow returnSubProduct =  tmpDataPool.get(returnProducts[i].getString("cart_pid"));
						returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
						tmpDataPool.put(returnProducts[i].getString("cart_pid"), returnSubProduct);
					}
					else
					{
						DBRow returnSubProduct = new DBRow();
						returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
						returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
						returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
						returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
						returnSubProduct.add("rpi_id", rpi_id);
						
						tmpDataPool.put(returnProducts[i].getString("cart_pid"), returnSubProduct);
					}
				}
				
			}
	
			//最后插入散件
			Set<String> keys = tmpDataPool.keySet();   
			for(String key:keys)
			{
				floorReturnProductMgrZJ.addReturnProductSubItems(tmpDataPool.get(key));
			}
	
			//清空退货购物车
			cartReturn.clearCart(StringUtil.getSession(request));
			
			//发送邮件通知顾客,当正常质保的时候则不直接发送邮件给客户
			if(flag)
			{
				DBRow detailOrder = floorOrderMgr.getDetailPOrderByOid(oid);
				
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
	            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
	
	            Mail mail = new Mail(user);
	            mail.setAddress(mailAddress);
	            mail.setMailBody(mailBody);
	            mail.send();
			}
			return(rp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnProducts",log);
		}
	}
	
	/**
	 * 文件方式登记退货 
	 * @param request
	 * @throws Exception
	 */
	public void returnProductForFile(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String permitFile = "jpg,gif,bmp,rar,jpeg,png,zip";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_imags_tmp/");
								   
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long rp_id = upload.getRequestRow().get("rp_id",0l);
				
			if (flag==2)
			{
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{	
				String temp_url = Environment.getHome()+"upl_imags_tmp/"+upload.getFileName();
				String[] temp = upload.getFileName().split("\\."); 
				String fileType = temp[temp.length-1];
				
				String fileName = "R"+rp_id+"_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+"."+fileType;
				String url = Environment.getHome()+"returnImg/"+fileName;
				
				FileUtil.moveFile(temp_url,url);
				
				AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
				
				DBRow file = new DBRow();
				file.add("file_name",fileName);
				file.add("file_with_id",rp_id);
				file.add("file_with_type",FileWithTypeKey.ProductReturn);
				file.add("upload_adid",adminLoggerBean.getAdid());
				file.add("upload_time",DateUtil.NowStr());
				floorFileMgrZJ.addFile(file);
				
				
				
				DBRow para = new DBRow();
				para.add("handle_date",DateUtil.NowStr());
				para.add("handle_user",adminLoggerBean.getAdid());
				para.add("product_status",ReturnProductStatusKey.GetImg);
				floorReturnProductMgrZJ.modReturnProduct(rp_id, para);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"returnProductForFile",log);
		}
	}
	
	/**
	 * 获得退货单商品的质保原因
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllReturnReason(long rp_id)
		throws Exception
	{
		try 
		{
			return floorReturnProductMgrZJ.getAllReturnReason(rp_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllReturnReason",log);
		}
	}
	
	/**
	 * 质保请求创建了账单
	 */
	public void hadCreateBill(long rp_id, long bill_id) 
		throws Exception 
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("bill_id",bill_id);
			para.add("status",ReturnProductKey.WAITINGPAY);
			
			floorReturnProductMgrZJ.modReturnProduct(rp_id, para);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"hadCreateBill",log);
		}
	}
	
	/**
	 * 过滤售后服务
	 */
	public DBRow[] filterReturnProduct(int status,int product_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorReturnProductMgrZJ.filterReturnProduct(status, product_status, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterReturnProduct",log);
		}
	}
	
	public DBRow[] filterReturnProductReason(String st,String en,int warranty_type,long catalog_id,long product_line_id,String p_name,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			long pc_id =0;
			if(product!=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return floorReturnProductMgrZJ.filterReturnProductReason(st, en, warranty_type, catalog_id, product_line_id, pc_id, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"filterReturnProductReason",log);
		}
	}
	
	/**
	 * 质保账单付款创建订单后关联服务
	 * @param bill_id
	 * @param oid
	 * @throws Exception
	 */
	public void warrantyInvoiceHasPay(long bill_id,long oid)
		throws Exception
	{
		try 
		{
			floorReturnProductMgrZJ.warrantyInvoiceHasPay(bill_id, oid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public DBRow getDetailReturnProduct(long rp_id)
		throws Exception
	{
		return floorReturnProductMgrZJ.getDetailReturnProduct(rp_id);
	}
	
	/**
	 * 根据服务单号查询
	 */
	public DBRow[] searchReturnProductReasonByRpid(String search_key, PageCtrl pc)
		throws Exception 
	{
		search_key = search_key.toUpperCase();
		
		long rp_id = 0;
		
		try 
		{
			if(search_key.substring(0,1).equals("R"))
			{
				rp_id = Long.parseLong(search_key.substring(1));
			}
			else
			{
				rp_id = Long.parseLong(search_key);
			}
			
		}
		catch (NumberFormatException e) 
		{
			rp_id = 0;
		}
		
		return floorReturnProductMgrZJ.searchReturnProductReasonByRpid(rp_id, pc);
	}

	public void setFloorReturnProductMgrZJ(
			FloorReturnProductMgrZJ floorReturnProductMgrZJ) {
		this.floorReturnProductMgrZJ = floorReturnProductMgrZJ;
	}

	public void setCartReturnProductReason(
			CartReturnProductReason cartReturnProductReason) {
		this.cartReturnProductReason = cartReturnProductReason;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setCartReturn(CartReturn cartReturn) {
		this.cartReturn = cartReturn;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}

	public void setFloorFileMgrZJ(FloorFileMgrZJ floorFileMgrZJ) {
		this.floorFileMgrZJ = floorFileMgrZJ;
	}
}
