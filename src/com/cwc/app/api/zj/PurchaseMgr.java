package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.AffirmPriceException;
import com.cwc.app.exception.purchase.CanNotInbondScanException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.purchase.NoProductInPurchaseException;
import com.cwc.app.exception.purchase.NoPurchaseException;
import com.cwc.app.exception.purchase.NullPurchaseException;
import com.cwc.app.exception.purchase.PurchaseDetailRepartException;
import com.cwc.app.exception.purchase.TransferException;
import com.cwc.app.exception.transport.NoExistTransportDetailException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportOrderDetailRepeatException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorPurchaseMgrLL;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorPurchaseMgrZyj;
import com.cwc.app.floor.api.zyj.FloorStorageCatalogMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.app.key.CurrencyKey;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.PurchaseArriveKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.PurchaseLogTypeKey;
import com.cwc.app.key.PurchaseMoneyKey;
import com.cwc.app.key.PurchaseProductModelKey;
import com.cwc.app.key.QualityInspectionKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.lucene.zr.ApplyMoneyIndexMgr;
import com.cwc.app.lucene.zr.PurchaseIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.verifycode.LoginLicence;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;

public class PurchaseMgr implements PurchaseIFace {
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger paypallog = Logger.getLogger("PAYPAL");
	
	private FloorPurchaseMgr fpm;
	private FloorProductMgr pm;
	private ProductMgr productMgr;
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	private SupplierMgrIFaceTJH supplierMgrTJH;
	private FloorPurchaseMgrLL floorPurchaseMgrLL;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private FloorTransportMgrZr floorTransportMgrZr;
	private SystemConfigIFace systemConfig;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorPurchaseMgrZyj floorPurchaseMgrZyj;
	private FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj;
	private FloorScheduleMgrZr floorScheduleMgrZr;
	private PurchaseMgrZyjIFace purchaseMgrZyj;
	private ApplyTransferMgrIFace applyTransferMgrZZZ;
	
	@Override
	public long addPurchaseAll(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			//提货地址
			String supplier					= StringUtil.getString(request,"supplier");
			long product_line_id			= StringUtil.getLong(request, "product_line_id");
			String supplier_house_number	= StringUtil.getString(request, "supplier_house_number");
			String supplier_street			= StringUtil.getString(request, "supplier_street");
			String supplier_city			= StringUtil.getString(request, "supplier_city");
			String supplier_zip_code		= StringUtil.getString(request, "supplier_zip_code");
			long supplier_province			= StringUtil.getLong(request, "supplier_province");
			String  supplier_pro_input		= "";
			if(-1 == supplier_province){
				supplier_pro_input			= StringUtil.getString(request, "supplier_pro_input");
			}else{
				supplier_pro_input			= "";
			}
			long supplier_native			= StringUtil.getLong(request, "supplier_native");
			String supplier_link_man		= StringUtil.getString(request, "supplier_link_man");
			String supplier_link_phone		= StringUtil.getString(request, "supplier_link_phone");
			
			//收货地址
			long ps_id					= StringUtil.getLong(request,"ps_id");
			long deliver_native			= StringUtil.getLong(request, "deliver_native");
			long deliver_provice		= StringUtil.getLong(request, "deliver_provice");
			String deliver_city			= StringUtil.getString(request, "deliver_city");
			String deliver_zip_code		= StringUtil.getString(request, "deliver_zip_code");
			String deliver_street		= StringUtil.getString(request, "deliver_street");
			String deliver_house_number	= StringUtil.getString(request, "deliver_house_number");
			String delivery_linkman		= StringUtil.getString(request,"delivery_linkman");
			String linkman_phone		= StringUtil.getString(request,"linkman_phone");
			String deliver_pro_input	= "";
			if(-1 == deliver_provice){
				deliver_pro_input=StringUtil.getString(request, "deliver_pro_input");
			}else{
				deliver_pro_input	= "";
			}
			
			//各项条款
			String repair_terms				= StringUtil.getString(request, "repair_terms_purchase");
			String labeling_terms			= StringUtil.getString(request, "labeling_terms_purchase");
			String delivery_product_terms	= StringUtil.getString(request, "delivery_product_terms_purchase");
			String purchase_terms			= StringUtil.getString(request, "purchase_terms_purchase");
			
			//各流程
			int invoice					= StringUtil.getInt(request, "invoice");
			double invoince_amount		= 0.0;
			int invoince_currency		= -1;
			double invoince_dot			= 0.0;
			if(InvoiceKey.INVOICE == invoice)
			{
				invoince_amount			= StringUtil.getDouble(request, "invoince_amount");
				invoince_currency		= StringUtil.getInt(request, "invoince_currency");
				invoince_dot			= StringUtil.getDouble(request, "invoince_dot");
			}
			int drawback				= StringUtil.getInt(request, "drawback");
			double rebate_rate			= 0.0;
			if(2 == drawback){
				rebate_rate				= StringUtil.getDouble(request, "rebate_rate");
			}
			int need_tag_third			= StringUtil.getInt(request, "need_tag_third");
			String purchase_date			= DateUtil.NowStr();
			//获取当前登录者的信息
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long adid						= adminLoggerBean.getAdid();
			String employeeName				=adminLoggerBean.getEmploye_name();
			
			TDate tDate = new TDate();
			tDate.addDay(+1);
			String updatetime = purchase_date;
			
			DBRow dbrow = new DBRow();
			//提货地址
			dbrow.add("supplier",supplier);
			dbrow.add("product_line_id",product_line_id);
			dbrow.add("supplier_house_number", supplier_house_number);
			dbrow.add("supplier_street", supplier_street);
			dbrow.add("supplier_city", supplier_city);
			dbrow.add("supplier_zip_code", supplier_zip_code);
			dbrow.add("supplier_province", supplier_province);
			dbrow.add("supplier_pro_input", supplier_pro_input);
			dbrow.add("supplier_native", supplier_native);
			dbrow.add("supplier_link_man", supplier_link_man);
			dbrow.add("supplier_link_phone", supplier_link_phone);
			
			//收货地址
			dbrow.add("ps_id",ps_id);
			dbrow.add("deliver_native", deliver_native);
			dbrow.add("deliver_provice", deliver_provice);
			dbrow.add("deliver_city", deliver_city);
			dbrow.add("deliver_zip_code", deliver_zip_code);
			dbrow.add("deliver_street", deliver_street);
			dbrow.add("deliver_house_number", deliver_house_number);
			dbrow.add("deliver_pro_input", deliver_pro_input);
			dbrow.add("delivery_linkman",delivery_linkman);
			dbrow.add("linkman_phone",linkman_phone);
			
			//各项条款
			dbrow.add("repair_terms",repair_terms);
			dbrow.add("labeling_terms",labeling_terms);
			dbrow.add("delivery_product_terms",delivery_product_terms);
			dbrow.add("purchase_terms",purchase_terms);
			
			//其他信息
			dbrow.add("purchase_date",purchase_date);
			dbrow.add("proposer_id",adid);
			dbrow.add("proposer",employeeName);
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			
			//各流程
			dbrow.add("invoice",invoice);
			dbrow.add("drawback",drawback);
			dbrow.add("invoince_amount",invoince_amount);
			dbrow.add("invoince_currency",invoince_currency);
			dbrow.add("invoince_dot",invoince_dot);
			dbrow.add("rebate_rate",rebate_rate);
			dbrow.add("need_tag", TransportTagKey.TAG);
			dbrow.add("need_third_tag", need_tag_third);
			dbrow.add("quality_inspection", QualityInspectionKey.NEED_QUALITY);
			dbrow.add("product_model", PurchaseProductModelKey.NEED_PRODUCT_MODEL);
			
			long purchase_id = fpm.addPurchase(dbrow);
			addPurchasefollowuplogs(PurchaseLogTypeKey.PRICE,PurchaseKey.OPEN, purchase_id, employeeName+"于"+DateUtil.NowStr()+"创建了采购单", adid,employeeName);
			
			//获取发票流程信息
			int needMailInvoice			= StringUtil.getInt(request, "needMailInvoice");
			int needMessageInvoice		= StringUtil.getInt(request, "needMessageInvoice");
			int needPageInvoice			= StringUtil.getInt(request, "needPageInvoice");
			String adminUserIdsInvoice	= StringUtil.getString(request, "adminUserIdsInvoice");
			
			//获取退税流程信息
			int needMailDrawback		= StringUtil.getInt(request, "needMailDrawback");
			int needMessageDrawback		= StringUtil.getInt(request, "needMessageDrawback");
			int needPageDrawback		= StringUtil.getInt(request, "needPageDrawback");
			String adminUserIdsDrawback	= StringUtil.getString(request, "adminUserIdsDrawback");
			
			//内部标签流程信息
			int needMailTag				= StringUtil.getInt(request, "needMailTag");
			int needMessageTag			= StringUtil.getInt(request, "needMessageTag");
			int needPageTag				= StringUtil.getInt(request, "needPageTag");
			String adminUserIdsTag		= StringUtil.getString(request, "adminUserIdsTag");
			
			//第三方标签流程信息
			int needMailTagThird		= StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird		= StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird		= StringUtil.getInt(request, "needPageTagThird");
			String adminUserIdsTagThird	= StringUtil.getString(request, "adminUserIdsTagThird");
			
			//获取质检流程信息
			int needMailQuality			= StringUtil.getInt(request, "needMailQuality");
			int needMessageQuality		= StringUtil.getInt(request, "needMessageQuality");
			int needPageQuality			= StringUtil.getInt(request, "needPageQuality");
			String adminUserIdsQuality	= StringUtil.getString(request, "adminUserIdsQuality");
			
			//获取商品范例流程信息
			int needMailProductModel	= StringUtil.getInt(request, "needMailProductModel");
			int needMessageProductModel	= StringUtil.getInt(request, "needMessageProductModel");
			int needPageProductModel	= StringUtil.getInt(request, "needPageProductModel");
			String adminUserIdsProductModel= StringUtil.getString(request, "adminUserIdsProductModel");
			
			
			//获取各流程相关人的名字
			String adminUserNamesPurchaser	= StringUtil.getString(request, "adminUserNamesPurchaser");
			String adminUserNamesDispatcher	= "";
				//StrUtil.getString(request, "adminUserNamesDispatcher");
			String adminUserNamesInvoice	= StringUtil.getString(request, "adminUserNamesInvoice");
			String adminUserNamesDrawback	= StringUtil.getString(request, "adminUserNamesDrawback");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			String adminUserNamesQuality	= StringUtil.getString(request, "adminUserNamesQuality");
			String adminUserNamesProductModel= StringUtil.getString(request, "adminUserNamesProductModel");
			
			//预计交货时间
			String expectArrTime			= StringUtil.getString(request, "expectArrTime");
			//上传质检要求
			purchaseMgrZyj.uploadPurchaseQualityInspection(request, purchase_id, QualityInspectionKey.NEED_QUALITY);
			//上传采购单详细
			String tempfilename = StringUtil.getString(request, "tempfilename");
			if(tempfilename.trim().length() > 0)
			{
				this.addPurchaseDetail(purchase_id, request);
			}
			//加创建的日志
			addPurchasefollowuplogs(1, purchase_id, "[创建采购单]", adid, employeeName);
			
			DBRow supplierRow = supplierMgrTJH.getDetailSupplier(Long.parseLong(supplier));
			PurchaseIndexMgr.getInstance().addIndex(purchase_id,supplierRow.getString("sup_name"),0,new long[0]);
			
			//组织发送邮件或者短信的内容
			String sendContent = handleMailMessageSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,
							drawback,rebate_rate,TransportTagKey.TAG,QualityInspectionKey.NEED_QUALITY,PurchaseProductModelKey.NEED_PRODUCT_MODEL,false,request
					, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice,
					adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel,
					expectArrTime);
			//发票任务和日志
			this.addPurchaseProcedureInvoice(adid, adminUserIdsInvoice, "", purchase_id, 
					ModuleKey.PURCHASE_ORDER,needPageInvoice, needMailInvoice, needMessageInvoice, request, false,
					ProcessKey.INVOICE, invoice, employeeName, sendContent, 
					invoince_amount, invoince_currency, invoince_dot, 1);
			//退税任务和日志
			this.addPurchaseProcedureDrawback(adid, adminUserIdsDrawback, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageDrawback, needMailDrawback, needMessageDrawback, request, false,
					ProcessKey.DRAWBACK, drawback, employeeName, sendContent, rebate_rate, 1);
			//内部标签任务和日志
			this.addPurchaseProcedureTag(adid, adminUserIdsTag, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageTag, needMailTag, needMessageTag, request, true,
					ProcessKey.PURCHASETAG, TransportTagKey.TAG, employeeName, sendContent, 1);
			//第三方标签任务和日志
			this.addPurchaseProcedureTagThird(adid, adminUserIdsTagThird, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageTagThird, needMailTagThird, needMessageTagThird, request, true,
					ProcessKey.PURCHASE_THIRD_TAG, need_tag_third, employeeName, sendContent, 1);
			//质检要求任务和日志
			this.addPurchaseProcedureQuality(adid, adminUserIdsQuality, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageQuality, needMailQuality, needMessageQuality, request, true, 
					ProcessKey.QUALITY_INSPECTION, QualityInspectionKey.NEED_QUALITY, employeeName, sendContent, 1);
			//商品范例任务和日志
			this.addPurchaseProcedureProductModel(adid, adminUserIdsProductModel, "", purchase_id, 
					ModuleKey.PURCHASE_ORDER, needPageProductModel, needMailProductModel, needMessageProductModel, request, true,
					ProcessKey.PURCHASE_RODUCTMODEL, PurchaseProductModelKey.NEED_PRODUCT_MODEL, employeeName, sendContent, 1);
			//采购和调度任务和日志
			String adminUserIdsPurchaser= StringUtil.getString(request, "adminUserIdsPurchaser");//采购员
//			String adminUserIdsDispatcher=StrUtil.getString(request, "adminUserIdsDispatcher");//调度人员
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(),adminUserIdsPurchaser, "",
					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					sendContent,
					true, true, true, request, true, ProcessKey.PURCHASE_PURCHASERS);
//			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
//					adminLoggerBean.getAdid(), adminUserIdsDispatcher, "",
//					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
//					sendContent,
//					true, true, true, request, true, ProcessKey.PURCHASE_DISPATCHER);
			
			return purchase_id;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addPurchaseAll",log);
		}
	}
	
	/**
	 * 根据供应商过滤采购单
	 * @param supplier_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasesBySupplier(long supplier_id, PageCtrl pc)
		throws Exception 
	{
		
		try
		{
			
			return (fpm.getPurchasesBySupplier(supplier_id, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 生成空采购单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addPurchase(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String supplier = StringUtil.getString(request,"supplier");
			long ps_id=StringUtil.getLong(request,"ps_id");
			long product_line_id		= StringUtil.getLong(request, "product_line_id");
			long deliver_native			= StringUtil.getLong(request, "deliver_native");
			long deliver_provice		= StringUtil.getLong(request, "deliver_provice");
			String deliver_city			= StringUtil.getString(request, "deliver_city");
			String deliver_zip_code		= StringUtil.getString(request, "deliver_zip_code");
			String deliver_street		= StringUtil.getString(request, "deliver_street");
			String deliver_house_number	= StringUtil.getString(request, "deliver_house_number");
			String delivery_linkman = StringUtil.getString(request,"delivery_linkman");
			String linkman_phone = StringUtil.getString(request,"linkman_phone");
			String deliver_pro_input	= "";
			if(-1 == deliver_provice){
				deliver_pro_input=StringUtil.getString(request, "deliver_pro_input");
			}else{
				deliver_pro_input	= "";
			}
			
			String supplier_house_number	= StringUtil.getString(request, "supplier_house_number");
			String supplier_street			= StringUtil.getString(request, "supplier_street");
			String supplier_city			= StringUtil.getString(request, "supplier_city");
			String supplier_zip_code		= StringUtil.getString(request, "supplier_zip_code");
			long supplier_province			= StringUtil.getLong(request, "supplier_province");
			String  supplier_pro_input		= "";
			if(-1 == supplier_province){
				supplier_pro_input			= StringUtil.getString(request, "supplier_pro_input");
			}else{
				supplier_pro_input			= "";
			}
			long supplier_native			= StringUtil.getLong(request, "supplier_native");
			String supplier_link_man		= StringUtil.getString(request, "supplier_link_man");
			String supplier_link_phone		= StringUtil.getString(request, "supplier_link_phone");
			
			String purchase_date=DateUtil.NowStr();
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			long proposer_id=adminLoggerBean.getAdid();
			String proposer=adminLoggerBean.getEmploye_name();
			
			TDate tDate = new TDate();
			tDate.addDay(+1);

			String updatetime=purchase_date;
			
			DBRow dbrow = new DBRow();
			dbrow.add("supplier",supplier);
			dbrow.add("product_line_id",product_line_id);
			dbrow.add("ps_id",ps_id);
			dbrow.add("purchase_date",purchase_date);
			dbrow.add("proposer",proposer_id);
			dbrow.add("proposer",proposer);
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			
			dbrow.add("deliver_native", deliver_native);
			dbrow.add("deliver_provice", deliver_provice);
			dbrow.add("deliver_city", deliver_city);
			dbrow.add("deliver_zip_code", deliver_zip_code);
			dbrow.add("deliver_street", deliver_street);
			dbrow.add("deliver_house_number", deliver_house_number);
			dbrow.add("deliver_pro_input", deliver_pro_input);
			dbrow.add("delivery_linkman",delivery_linkman);
			dbrow.add("linkman_phone",linkman_phone);
			
			dbrow.add("supplier_house_number", supplier_house_number);
			dbrow.add("supplier_street", supplier_street);
			dbrow.add("supplier_city", supplier_city);
			dbrow.add("supplier_zip_code", supplier_zip_code);
			dbrow.add("supplier_province", supplier_province);
			dbrow.add("supplier_pro_input", supplier_pro_input);
			dbrow.add("supplier_native", supplier_native);
			dbrow.add("supplier_link_man", supplier_link_man);
			dbrow.add("supplier_link_phone", supplier_link_phone);
			
			long purchaseId = fpm.addPurchase(dbrow);
			
			addPurchasefollowuplogs(1, purchaseId, "[创建采购单]", proposer_id, proposer);
			
			DBRow supplierRow = supplierMgrTJH.getDetailSupplier(Long.parseLong(supplier));
			
			PurchaseIndexMgr.getInstance().addIndex(purchaseId,supplierRow.getString("sup_name"),0,new long[0]);
			return purchaseId;
		} 
		catch(RuntimeException e) 
		{
			throw new SystemException(e,"addPurchase",log);
		}
	}
	@Override
	public void updatePurchaseBasic(HttpServletRequest request) throws Exception{
		try {
		long purchase_id			= StringUtil.getLong(request, "purchase_id");
		String supplier				= StringUtil.getString(request,"supplier");
		long ps_id					= StringUtil.getLong(request,"ps_id");
		long product_line_id		= StringUtil.getLong(request, "product_line_id");
		long deliver_native			= StringUtil.getLong(request, "deliver_native");
		long deliver_provice		= StringUtil.getLong(request, "deliver_provice");
		String deliver_city			= StringUtil.getString(request, "deliver_city");
		String deliver_zip_code		= StringUtil.getString(request, "deliver_zip_code");
		String deliver_street		= StringUtil.getString(request, "deliver_street");
		String deliver_house_number	= StringUtil.getString(request, "deliver_house_number");
		String delivery_linkman		= StringUtil.getString(request,"delivery_linkman");
		String linkman_phone		= StringUtil.getString(request,"linkman_phone");
		String deliver_pro_input	= "";
		if(-1 == deliver_provice){
			deliver_pro_input=StringUtil.getString(request, "deliver_pro_input");
		}else{
			deliver_pro_input		= "";
		}
		
		String supplier_house_number	= StringUtil.getString(request, "supplier_house_number");
		String supplier_street			= StringUtil.getString(request, "supplier_street");
		String supplier_city			= StringUtil.getString(request, "supplier_city");
		String supplier_zip_code		= StringUtil.getString(request, "supplier_zip_code");
		long supplier_province			= StringUtil.getLong(request, "supplier_province");
		String  supplier_pro_input		= "";
		if(-1 == supplier_province){
			supplier_pro_input			= StringUtil.getString(request, "supplier_pro_input");
		}else{
			supplier_pro_input			= "";
		}
		long supplier_native			= StringUtil.getLong(request, "supplier_native");
		String supplier_link_man		= StringUtil.getString(request, "supplier_link_man");
		String supplier_link_phone		= StringUtil.getString(request, "supplier_link_phone");
		String purchase_date=DateUtil.NowStr();
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long proposer_id=adminLoggerBean.getAdid();
		String proposer=adminLoggerBean.getEmploye_name();
		TDate tDate = new TDate();
		tDate.addDay(+1);
		String updatetime=purchase_date;
		DBRow dbrow = new DBRow();
		dbrow.add("supplier",supplier);
		dbrow.add("product_line_id",product_line_id);
		dbrow.add("ps_id",ps_id);
		dbrow.add("purchase_date",purchase_date);
		dbrow.add("proposer",proposer_id);
		dbrow.add("proposer",proposer);
		dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		dbrow.add("updatetime",updatetime);
		dbrow.add("deliver_native", deliver_native);
		dbrow.add("deliver_provice", deliver_provice);
		dbrow.add("deliver_city", deliver_city);
		dbrow.add("deliver_zip_code", deliver_zip_code);
		dbrow.add("deliver_street", deliver_street);
		dbrow.add("deliver_house_number", deliver_house_number);
		dbrow.add("deliver_pro_input", deliver_pro_input);
		dbrow.add("delivery_linkman",delivery_linkman);
		dbrow.add("linkman_phone",linkman_phone);
		dbrow.add("supplier_house_number", supplier_house_number);
		dbrow.add("supplier_street", supplier_street);
		dbrow.add("supplier_city", supplier_city);
		dbrow.add("supplier_zip_code", supplier_zip_code);
		dbrow.add("supplier_province", supplier_province);
		dbrow.add("supplier_pro_input", supplier_pro_input);
		dbrow.add("supplier_native", supplier_native);
		dbrow.add("supplier_link_man", supplier_link_man);
		dbrow.add("supplier_link_phone", supplier_link_phone);
		
		fpm.updatePurchaseBasic(dbrow, purchase_id);
		
		String eta	= StringUtil.getString(request, "eta");
		if(eta.length() > 0){
			DBRow purchaseDetail = new DBRow();
			purchaseDetail.add("eta", eta);
			floorPurchaseMgrLL.updatePurchaseDetailByPurchaseId(String.valueOf(purchase_id), purchaseDetail);
		}
		
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseBasic",log);
		}
	}
	public void addPurchaseAllProcedures(HttpServletRequest request, long purchase_id) throws Exception{
		try {
			AdminMgr adminMgr				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long proposer_id				= adminLoggerBean.getAdid();
			String proposer					=adminLoggerBean.getEmploye_name();
//			long purchase_id				= StrUtil.getLong(request, "purchase_id");
			DBRow purchaseRow			= fpm.getPurchaseByPurchaseid(purchase_id);
			
			
			int invoice					= StringUtil.getInt(request, "invoice");
			int drawback				= StringUtil.getInt(request, "drawback");
			int need_tag				= StringUtil.getInt(request, "need_tag");
			int quality_inspection		= StringUtil.getInt(request, "quality_inspection");
			int product_model			= StringUtil.getInt(request, "product_model");
			int needMailInvoice			= StringUtil.getInt(request, "needMailInvoice");
			int needMessageInvoice		= StringUtil.getInt(request, "needMessageInvoice");
			int needPageInvoice			= StringUtil.getInt(request, "needPageInvoice");
			String adminUserIdsInvoice	= StringUtil.getString(request, "adminUserIdsInvoice");
			double invoince_amount		= 0.0;
			int invoince_currency		= -1;
			double invoince_dot			= 0.0;
			double rebate_rate			= 0.0;
			if(2 == drawback){
				rebate_rate				= StringUtil.getDouble(request, "rebate_rate");
			}else{
				rebate_rate				= 0.0;
			}
			String purchase_date=DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			String updatetime=purchase_date;
			DBRow dbrow = new DBRow();
			dbrow.add("invoice",invoice);
			dbrow.add("drawback",drawback);
			dbrow.add("invoince_amount",invoince_amount);
			dbrow.add("invoince_currency",invoince_currency);
			dbrow.add("invoince_dot",invoince_dot);
			dbrow.add("rebate_rate",rebate_rate);
			dbrow.add("need_tag", need_tag);
			dbrow.add("quality_inspection", quality_inspection);
			dbrow.add("product_model", product_model);
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			fpm.updatePurchaseBasic(dbrow, purchase_id);
			String adminUserNamesPurchaser	= StringUtil.getString(request, "adminUserNamesPurchaser");
			String adminUserNamesDispatcher	= StringUtil.getString(request, "adminUserNamesDispatcher");
			String adminUserNamesInvoice	= StringUtil.getString(request, "adminUserNamesInvoice");
			String adminUserNamesDrawback	= StringUtil.getString(request, "adminUserNamesDrawback");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			String adminUserNamesQuality	= StringUtil.getString(request, "adminUserNamesQuality");
			String adminUserNamesProductModel= StringUtil.getString(request, "adminUserNamesProductModel");
			CurrencyKey currencyKey	= new CurrencyKey();
			DrawbackKey drawbackKey = new DrawbackKey();
			InvoiceKey invoiceKey	= new InvoiceKey();
			String invoiceContent	= "创建采购单["+purchase_id+"],[发票流程]["+invoiceKey.getInvoiceById(String.valueOf(invoice))+"]";
			if(2 == invoice){
				invoince_amount			= StringUtil.getDouble(request, "invoince_amount");
				invoince_currency		= StringUtil.getInt(request, "invoince_currency");
				invoince_dot			= StringUtil.getDouble(request, "invoince_dot");
				invoiceContent			+= "，开票金额："+invoince_amount
				+  " "+currencyKey.getCurrencyById(invoince_currency+"")
				+  "，票点："+invoince_dot+"%";
				boolean need_mail_invoice = false;
				if(2 == needMailInvoice){
					need_mail_invoice	= true;
				}
				boolean need_message_invoice = false;
				if(2 == needMessageInvoice){
					need_message_invoice = true;
				}
				boolean need_page_invoice = false;
				if(2 == needPageInvoice){
					need_page_invoice = true;
				}
				scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
						adminLoggerBean.getAdid(), adminUserIdsInvoice, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[发票流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel)
						, need_page_invoice, need_mail_invoice, need_message_invoice, request, false, ProcessKey.INVOICE);
			}else{
				invoince_amount			= 0.0;
				invoince_currency		= -1;
				invoince_dot			= 0.0;
			}
			int needMailDrawback		= StringUtil.getInt(request, "needMailDrawback");
			int needMessageDrawback		= StringUtil.getInt(request, "needMessageDrawback");
			int needPageDrawback		= StringUtil.getInt(request, "needPageDrawback");
			String adminUserIdsDrawback	= StringUtil.getString(request, "adminUserIdsDrawback");
			String drawbackContent 		= "创建采购单["+purchase_id+"],[退税流程]["+drawbackKey.getDrawbackById(String.valueOf(drawback))+"]";
			if(2 == drawback){
				drawbackContent			+= "，退税率："+rebate_rate+"%";
				boolean need_mail_drawback = false;
				if(2 == needMailDrawback){
					need_mail_drawback = true;
				}
				boolean need_message_drawback = false;
				if(2 == needMessageDrawback){
					need_message_drawback = true;
				}
				boolean need_page_drawback = false;
				if(2 == needPageDrawback){
					need_page_drawback = true;
				}
				scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
						adminLoggerBean.getAdid(), adminUserIdsDrawback, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[退税流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel), need_page_drawback, 
						need_mail_drawback, need_message_drawback, request, false, ProcessKey.DRAWBACK);
			}
			int needMailTag				= StringUtil.getInt(request, "needMailTag");
			int needMessageTag			= StringUtil.getInt(request, "needMessageTag");
			int needPageTag				= StringUtil.getInt(request, "needPageTag");
			String adminUserIdsTag		= StringUtil.getString(request, "adminUserIdsTag");
			String tagContent			= "";
			if(2 == need_tag){
				boolean need_mail_tag	= false;
				if(2 == needMailTag){
					need_mail_tag		= true;
				}
				boolean need_message_tag= false;
				if(2 == needMessageTag){
					need_message_tag	= true;
				}
				boolean need_page_tag	= false;
				if(2 == needPageTag){
					need_page_tag		= true;
				}
				tagContent = "创建采购单["+purchase_id+"],[制签流程][需要制签]";
				TransportTagKey transportTagKey = new TransportTagKey();
				TDate tagEndTime = new TDate();
				tagEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period")));
				scheduleMgrZr.addScheduleByExternalArrangeNow(purchaseRow.getString("purchase_date"), tagEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), 
						adminLoggerBean.getAdid(), adminUserIdsTag, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[制签流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel), need_page_tag, 
						need_mail_tag, need_message_tag, request, true, ProcessKey.PURCHASETAG);
			}else{
				tagContent = "创建采购单["+purchase_id+"],[制签流程][不需要制签]";
			}
			int needMailQuality			= StringUtil.getInt(request, "needMailQuality");
			int needMessageQuality		= StringUtil.getInt(request, "needMessageQuality");
			int needPageQuality			= StringUtil.getInt(request, "needPageQuality");
			String adminUserIdsQuality	= StringUtil.getString(request, "adminUserIdsQuality");
			if(2 == quality_inspection){
				boolean need_mail_quality= false;
				if(2 == needMailQuality){
					need_mail_quality	= true;
				}
				boolean need_message_quality= false;
				if(2 == needMessageQuality){
					need_message_quality = true;
				}
				boolean need_page_quality= false;
				if(2 == needPageQuality){
					need_page_quality = true;
				}
				scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
						adminLoggerBean.getAdid(), adminUserIdsQuality, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[质检流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel), need_page_quality, 
						need_mail_quality, need_message_quality, request, false, ProcessKey.QUALITY_INSPECTION);
			}
			int needMailProductModel	= StringUtil.getInt(request, "needMailProductModel");
			int needMessageProductModel	= StringUtil.getInt(request, "needMessageProductModel");
			int needPageProductModel	= StringUtil.getInt(request, "needPageProductModel");
			String adminUserIdsProductModel= StringUtil.getString(request, "adminUserIdsProductModel");
			if(2 == product_model){
				boolean need_mail_product_model = false;
				if(2 == needMailProductModel){
					need_mail_product_model = true;
				}
				boolean need_message_product_model = false;
				if(2 == needMessageProductModel){
					need_message_product_model = true;
				}
				boolean need_page_product_model = false;
				if(2 == needPageProductModel){
					need_page_product_model = true;
				}
				scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
						adminLoggerBean.getAdid(), adminUserIdsProductModel, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[商品范例]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel), need_page_product_model, 
								need_mail_product_model, need_message_product_model, request, false, ProcessKey.PURCHASE_RODUCTMODEL);
			}
			String adminUserIdsPurchaser= StringUtil.getString(request, "adminUserIdsPurchaser");//采购员
			String adminUserIdsDispatcher=StringUtil.getString(request, "adminUserIdsDispatcher");//调度人员
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(),adminUserIdsPurchaser, "",
					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
							, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel),
					true, true, true, request, true, ProcessKey.PURCHASE_PURCHASERS);
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(), adminUserIdsDispatcher, "",
					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
							, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel),
					true, true, true, request, true, ProcessKey.PURCHASE_DISPATCHER);
			
			addPurchasefollowuplogs(ProcessKey.INVOICE, invoice, purchase_id, invoiceContent, proposer_id, proposer);
			addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, drawbackContent, proposer_id, proposer);
			addPurchasefollowuplogs(ProcessKey.PURCHASETAG, invoice, purchase_id, "[制签流程]["+new TransportTagKey().getTransportTagById(need_tag)+"]", proposer_id, proposer);
			addPurchasefollowuplogs(ProcessKey.QUALITY_INSPECTION, drawback, purchase_id, "[质检流程]["+new QualityInspectionKey().getQualityInspectionById(quality_inspection)+"]", proposer_id, proposer);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addPurchaseAllProcedures",log);
		}
	}
	/**
	 * 处理发送内容 采购的基础信息
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	private String handleSendContent(long purchase_id, int invoice,double amount, int currency,
			double dot, int drawback, double rebate_rate,int need_tag, int quality, int productModel, boolean isUpdate, HttpServletRequest request
			, String adminUserNamesPurchaser, String adminUserNamesDispatcher, String adminUserNamesInvoice,
			String adminUserNamesDrawback, String adminUserNamesTag, String adminUserNamesQuality, String adminUserNamesProduct) throws Exception{
		try {
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			DBRow purchaseRow	= fpm.getPurchaseByPurchaseid(purchase_id);
			String supplierName	= "";	
			long sid = 0;
			try{
				sid = Long.parseLong(purchaseRow.getString("supplier"));
			}catch(NumberFormatException e){
				sid = 0;
			}
			if(sid ==0){
				supplierName = purchaseRow.getString("supplier");
			}else{
				DBRow supplier = supplierMgrTJH.getDetailSupplier(sid);
				supplierName	= supplier.getString("sup_name");
			}
			long pro_cata_id = purchaseRow.get("ps_id",0L);
			DBRow storageCatalog	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id);
			String cata_name = "";
			if(null != storageCatalog){
				cata_name = storageCatalog.getString("title");
			}
			double sumPrice = 0.0;
			if(null != purchaseRow){
				sumPrice = fpm.sumPurchaseDetailByPurchaseid(purchase_id);
			}
			DBRow purchaseDetailOne = fpm.getPurchaseDetailOneByPurchaseId(purchase_id);
			String eta = "";
			if(null != purchaseDetailOne){
				eta = purchaseDetailOne.getString("eta");
			}
			String rnStr = "";
			if(isUpdate){
				rnStr = "供应商:"+supplierName+",仓库:"+cata_name
				+",总金额:"+sumPrice+",预计交货时间:"+eta+";";
			}else{
				rnStr = "供应商:"+supplierName+",仓库:"+cata_name
				+",总金额:"+sumPrice+",预计交货时间:"+eta+";";
			}
			String purchasePersonDispacher = "\n采购:" + adminUserNamesPurchaser;
			//+ "\n调度:" + adminUserNamesDispatcher;
			String invoiceContent		= "";
			InvoiceKey invoiceKey	= new InvoiceKey();
			DrawbackKey drawbackKey		= new DrawbackKey();
			CurrencyKey currencyKey	= new CurrencyKey();
			String invoicePerson		= "";
			if(2 == invoice){
				invoicePerson			+= "\n[发票流程]"+adminUserNamesInvoice;
				invoiceContent			+= "\n开票金额:"+amount
				+ currencyKey.getCurrencyById(currency+"")
				+  ",票点:"+dot+"%";
			}else if(1 == invoice){
				invoiceContent			+= "";
			}else{
				invoicePerson			+= "\n[发票流程]"+adminUserNamesInvoice;
				invoiceContent			+= "\n开票金额:"+amount
				+  currencyKey.getCurrencyById(currency+"")
				+  ",票点:"+dot+"%";
			}
			String drawbackContent		= "";
			if(!"".equals(invoiceContent)){
				drawbackContent			= ";";
			}
			String drawbackPerson		= "";
			if(2 == drawback){
				drawbackContent			+= "退税率:"+rebate_rate+"%;";
				drawbackPerson			= "\n[退税流程]"+adminUserNamesDrawback;
			}else if(1 == drawback){
				drawbackContent			+= "";
			}else{
				drawbackContent			+= "退税率:"+rebate_rate+"%;";
				drawbackPerson			= "\n[退税流程]"+adminUserNamesDrawback;
			}
			String needTagContent		= "";
			TransportTagKey transportTagKey = new TransportTagKey();
			if(2 == need_tag){
				needTagContent			+= "\n[制签流程]"+adminUserNamesTag;
			}else if(1 == need_tag){
				needTagContent			+= "";
			}else{
				needTagContent			+= "\n[制签流程]"+adminUserNamesTag;
			}
			String qualityContent		= "";
			QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();
			if(2 == quality){
				qualityContent			+= "\n[质检流程]"+adminUserNamesQuality;
			}else if(1 == quality){
				qualityContent			+= "";
			}else{
				qualityContent			+= "\n[质检流程]"+adminUserNamesQuality;
			}
			String productModelContent = "";
			if(2 == productModel){
				productModelContent		+= "\n[商品范例]"+adminUserNamesProduct;
			}else if(1 == productModel){
				productModelContent		+= "";
			}else{
				productModelContent		+= "\n[商品范例]"+adminUserNamesProduct;
			}
			rnStr += invoiceContent + drawbackContent;
			//+ purchasePersonDispacher + invoicePerson + drawbackPerson + needTagContent + qualityContent+productModelContent;
			return rnStr;
		} catch (Exception e) {
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	
	/**
	 * 处理发送内容 采购的基础信息
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	private String handleMailMessageSendContent(long purchase_id, int invoice,double amount, int currency,
			double dot, int drawback, double rebate_rate,int need_tag, int quality, int productModel, boolean isUpdate, HttpServletRequest request,
			String adminUserNamesPurchaser, String adminUserNamesDispatcher, String adminUserNamesInvoice,
			String adminUserNamesDrawback, String adminUserNamesTag, String adminUserNamesQuality, String adminUserNamesProduct,
			String expectArrTime) throws Exception{
		try {
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			DBRow purchaseRow	= fpm.getPurchaseByPurchaseid(purchase_id);
			String supplierName	= "";	
			long sid = 0;
			try{
				sid = Long.parseLong(purchaseRow.getString("supplier"));
			}catch(NumberFormatException e){
				sid = 0;
			}
			if(sid ==0){
				supplierName = purchaseRow.getString("supplier");
			}else{
				DBRow supplier = supplierMgrTJH.getDetailSupplier(sid);
				supplierName	= supplier.getString("sup_name");
			}
			long pro_cata_id = purchaseRow.get("ps_id",0L);
			DBRow storageCatalog	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id);
			String cata_name = "";
			if(null != storageCatalog){
				cata_name = storageCatalog.getString("title");
			}
			double sumPrice = 0.0;
			if(null != purchaseRow){
				sumPrice = fpm.sumPurchaseDetailByPurchaseid(purchase_id);
			}
			String rnStr = "";
			if(isUpdate){
				rnStr = "供应商:"+supplierName+",仓库:"+cata_name
				+",总金额:"+sumPrice+",预计交货时间:"+expectArrTime+";";
			}else{
				rnStr = "供应商:"+supplierName+",仓库:"+cata_name
				+",总金额:"+sumPrice+",预计交货时间:"+expectArrTime+";";
			}
			String purchasePersonDispacher = "\n采购:" + adminUserNamesPurchaser;
//			+ "\n调度:" + adminUserNamesDispatcher;
			String invoiceContent		= "";
			CurrencyKey currencyKey	= new CurrencyKey();
			String invoicePerson		= "";
			if(2 == invoice){
				invoicePerson			+= "\n[发票流程]"+adminUserNamesInvoice;
				invoiceContent			+= "\n开票金额:"+amount
				+ currencyKey.getCurrencyById(currency+"")
				+  ",票点:"+dot+"%";
			}else if(1 == invoice){
				invoiceContent			+= "";
			}else{
				invoicePerson			+= "\n[发票流程]"+adminUserNamesInvoice;
				invoiceContent			+= "\n开票金额:"+amount
				+  currencyKey.getCurrencyById(currency+"")
				+  ",票点:"+dot+"%";
			}
			String drawbackContent		= "";
			if(!"".equals(invoiceContent)){
				drawbackContent			= ";";
			}
			String drawbackPerson		= "";
			if(2 == drawback){
				drawbackContent			+= "退税率:"+rebate_rate+"%;";
				drawbackPerson			= "\n[退税流程]"+adminUserNamesDrawback;
			}else if(1 == drawback){
				drawbackContent			+= "";
			}else{
				drawbackContent			+= "退税率:"+rebate_rate+"%;";
				drawbackPerson			= "\n[退税流程]"+adminUserNamesDrawback;
			}
			String needTagContent		= "";
			TransportTagKey transportTagKey = new TransportTagKey();
			if(2 == need_tag){
				needTagContent			+= "\n[制签流程]"+adminUserNamesTag;
			}else if(1 == need_tag){
				needTagContent			+= "";
			}else{
				needTagContent			+= "\n[制签流程]"+adminUserNamesTag;
			}
			String qualityContent		= "";
			QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();
			if(2 == quality){
				qualityContent			+= "\n[质检流程]"+adminUserNamesQuality;
			}else if(1 == quality){
				qualityContent			+= "";
			}else{
				qualityContent			+= "\n[质检流程]"+adminUserNamesQuality;
			}
			String productModelContent = "";
			if(2 == productModel){
				productModelContent		+= "\n[商品范例]"+adminUserNamesProduct;
			}else if(1 == productModel){
				productModelContent		+= "";
			}else{
				productModelContent		+= "\n[商品范例]"+adminUserNamesProduct;
			}
			rnStr += invoiceContent + drawbackContent;
			//+ purchasePersonDispacher+ invoicePerson + drawbackPerson + needTagContent + qualityContent+productModelContent
			return rnStr;
		} catch (Exception e) {
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	/**
	 * 更新流程
	 */
	@Override
	public void updatePurchaseAllProcedures(HttpServletRequest request) throws Exception{
		try {
			long purchase_id			= StringUtil.getLong(request, "purchase_id");
			DBRow purchaseRow			= fpm.getPurchaseByPurchaseid(purchase_id);
			int pur_invoice				= purchaseRow.get("invoice", 0);
			int pur_drawback			= purchaseRow.get("drawback", 0);
			int pur_need_tag			= purchaseRow.get("need_tag", 0);
			int pur_quality				= purchaseRow.get("quality_inspection", 0);
			int pur_product_model		= purchaseRow.get("product_model", 0);
			double pur_amount			= purchaseRow.get("invoince_amount", 0.00);
			int pur_currency			= purchaseRow.get("invoince_currency", 0);
			double pur_dot				= purchaseRow.get("invoince_dot", 0.00);
			double pur_rebate_rate		= purchaseRow.get("rebate_rate", 0.00);
			double invoince_amount		= StringUtil.getDouble(request, "invoince_amount");
			int invoince_currency		= StringUtil.getInt(request, "invoince_currency");
			double invoince_dot			= StringUtil.getDouble(request, "invoince_dot");
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String adminUserNamesPurchaser	= StringUtil.getString(request, "adminUserNamesPurchaser");
			String adminUserNamesDispatcher	= StringUtil.getString(request, "adminUserNamesDispatcher");
			String adminUserNamesInvoice	= StringUtil.getString(request, "adminUserNamesInvoice");
			String adminUserNamesDrawback	= StringUtil.getString(request, "adminUserNamesDrawback");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			String adminUserNamesQuality	= StringUtil.getString(request, "adminUserNamesQuality");
			String adminUserNamesProductModel= StringUtil.getString(request, "adminUserNamesProductModel");
			if(InvoiceKey.FINISH == pur_invoice){
				DBRow[] invoicePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
				String invoicePersonsNames = "";
				if(invoicePersons.length > 0){
					invoicePersonsNames += invoicePersons[0].getString("employe_name");
					for (int i = 1; i < invoicePersons.length; i++) {
						invoicePersonsNames += ","+ invoicePersons[i].getString("employe_name");
					}
				}
				adminUserNamesInvoice = invoicePersonsNames;
			}
			if(DrawbackKey.FINISH == pur_drawback){
				DBRow[] drawbackPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
				String drawbackPersonsNames= "";
				if(drawbackPersons.length > 0){
					drawbackPersonsNames += drawbackPersons[0].getString("employe_name");
					for (int i = 1; i < drawbackPersons.length; i++) {
						drawbackPersonsNames += ","+ drawbackPersons[i].getString("employe_name");
					}
				}
				adminUserNamesDrawback = drawbackPersonsNames;
			}
			if(TransportTagKey.FINISH == pur_need_tag){
				DBRow[] tagPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				String tagPersonsNames= "";
				if(tagPersons.length > 0){
					tagPersonsNames += tagPersons[0].getString("employe_name");
					for (int i = 1; i < tagPersons.length; i++) {
						tagPersonsNames += ","+ tagPersons[i].getString("employe_name");
					}
				}
				adminUserNamesTag = tagPersonsNames;
			}
			if(QualityInspectionKey.FINISH == pur_quality){
				DBRow[] qualityPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
				String qualityPersonsNames= "";
				if(qualityPersons.length > 0){
					qualityPersonsNames += qualityPersons[0].getString("employe_name");
					for (int i = 1; i < qualityPersons.length; i++) {
						qualityPersonsNames += ","+ qualityPersons[i].getString("employe_name");
					}
				}
				adminUserNamesQuality = qualityPersonsNames;
			}
			if(PurchaseProductModelKey.FINISH == pur_product_model){
				DBRow[] productModelPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
				String productModelersonsNames= "";
				if(productModelPersons.length > 0){
					productModelersonsNames += productModelPersons[0].getString("employe_name");
					for (int i = 1; i < productModelPersons.length; i++) {
						productModelersonsNames += ","+ productModelPersons[i].getString("employe_name");
					}
				}
				adminUserNamesProductModel = productModelersonsNames;
			}
			int invoice					= StringUtil.getInt(request, "invoice");
			if(InvoiceKey.INVOICING == pur_invoice || InvoiceKey.FINISH == pur_invoice){
				invoice					= pur_invoice;
				invoince_amount			= pur_amount;
				invoince_currency		= pur_currency;
				invoince_dot			= pur_dot;
			}
			int drawback				= StringUtil.getInt(request, "drawback");
			double rebate_rate			= StringUtil.getDouble(request, "rebate_rate");
			if(1 == drawback){
				rebate_rate				= 0.00;
			}
			if(DrawbackKey.DRAWBACKING == pur_drawback || DrawbackKey.FINISH == pur_drawback){
				drawback				= pur_drawback;
				rebate_rate				= pur_rebate_rate;//????
			}
			int need_tag				= StringUtil.getInt(request, "need_tag");
			if(TransportTagKey.FINISH == pur_need_tag){
				need_tag				= pur_need_tag;
			}
			int quality_inspection		= StringUtil.getInt(request, "quality_inspection");
			if(QualityInspectionKey.FINISH == pur_quality){
				quality_inspection		= pur_quality;
			}
			if(1 == invoice){
				invoince_amount			= 0.00;
				invoince_currency		= -1;
				invoince_dot			= 0.00;
			}
			int product_model			= StringUtil.getInt(request, "product_model");
			if(PurchaseProductModelKey.FINISH == pur_product_model){
				product_model			= pur_product_model;
			}
			InvoiceKey invoiceKey		= new InvoiceKey();
			CurrencyKey currencyKey		= new CurrencyKey();
			DrawbackKey drawbackKey		= new DrawbackKey();
			String invoiceContent	= "";
			if(2 == invoice){
				invoiceContent			+= "[修改采购单][发票流程]["+invoiceKey.getInvoiceById(String.valueOf(invoice))+"]";
				invoiceContent			+= "，开票金额："+invoince_amount
				+  " "+currencyKey.getCurrencyById(invoince_currency+"")
				+  "，票点："+invoince_dot+"%";
			}else if(1 != invoice){
				invoiceContent			+= "[修改采购单][发票流程]["+invoiceKey.getInvoiceById(String.valueOf(pur_invoice))+"]";
				invoiceContent			+= "，开票金额："+invoince_amount
				+  " "+currencyKey.getCurrencyById(invoince_currency+"")
				+  "，票点："+invoince_dot+"%";
			}else if(1 == invoice){
				invoiceContent			+= "[修改采购单][发票流程]["+invoiceKey.getInvoiceById(String.valueOf((invoice)))+"]";
			}
			String drawbackContent 		= "";
			if(2 == drawback){
				drawbackContent			= "[修改采购单][退税流程]["+drawbackKey.getDrawbackById(String.valueOf(drawback))+"]";
				drawbackContent			+= "，退税率："+rebate_rate+"%";
			}else if(1 != drawback){
				drawbackContent			= "[修改采购单][退税流程]["+drawbackKey.getDrawbackById(String.valueOf(pur_drawback))+"]";
				drawbackContent			+= "，退税率："+rebate_rate+"%";
			}else if(1 == drawback){
				drawbackContent			= "[修改采购单][退税流程]["+drawbackKey.getDrawbackById(String.valueOf(pur_drawback))+"]";
			}
			long proposer_id=adminLoggerBean.getAdid();
			String proposer=adminLoggerBean.getEmploye_name();
			String adminUserIdsInvoice	= StringUtil.getString(request, "adminUserIdsInvoice");
			int needMailInvoice			= StringUtil.getInt(request, "needMailInvoice");
			int needMessageInvoice		= StringUtil.getInt(request, "needMessageInvoice");
			int needPageInvoice			= StringUtil.getInt(request, "needPageInvoice");
			
			String purchase_date=DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			String updatetime=purchase_date;
			DBRow dbrow = new DBRow();
			dbrow.add("invoice",invoice);
			dbrow.add("drawback",drawback);
			dbrow.add("invoince_amount",invoince_amount);
			dbrow.add("invoince_currency",invoince_currency);
			dbrow.add("invoince_dot",invoince_dot);
			dbrow.add("rebate_rate",rebate_rate);
			dbrow.add("need_tag", need_tag);
			dbrow.add("quality_inspection", quality_inspection);
			dbrow.add("product_model", product_model);
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			fpm.updatePurchaseBasic(dbrow, purchase_id);
			
			//修改时，任务的内容信息
			String strSendContent = handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
					, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel);
					boolean need_mail_invoice = false;
					if(2 == needMailInvoice){
						need_mail_invoice	= true;
					}
					boolean need_message_invoice = false;
					if(2 == needMessageInvoice){
						need_message_invoice = true;
					}
					boolean need_page_invoice = false;
					if(2 == needPageInvoice){
						need_page_invoice = true;
					}
			if((0 == pur_invoice  && 2 == invoice) || (1 == pur_invoice && 2 == invoice)){
				if(2 == invoice){
					scheduleMgrZr.addScheduleByExternalArrangeNow(purchaseRow.getString("purchase_date"), "2012-03-15 13:23:34", 
							adminLoggerBean.getAdid(), adminUserIdsInvoice, "",
							purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[发票流程]", 
							handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
									, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel)
							, need_page_invoice, need_mail_invoice, need_message_invoice, request, false, ProcessKey.INVOICE);
					addPurchasefollowuplogs(ProcessKey.INVOICE, invoice, purchase_id, invoiceContent, proposer_id, proposer);
				}
			}else if(2 == pur_invoice && 1 == invoice){
				DBRow scheduleInvoice	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
				long scheduleInvoiceId	= scheduleInvoice.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleInvoiceId, request);
				//floorScheduleMgrZr.deleteScheduleById(scheduleInvoiceId);
				addPurchasefollowuplogs(ProcessKey.INVOICE, invoice, purchase_id, "[发票流程][不需要发票]", proposer_id, proposer);
			}else if((2 == pur_invoice && 2 == invoice) || 3 == pur_invoice){
				boolean invoiceContentIsChange	= false;
				boolean invoicePersonIsChange	= false;
				if(pur_amount != invoince_amount || pur_currency != invoince_currency || pur_dot != invoince_dot){
					invoiceContentIsChange = true;
				}
				DBRow[] invoicePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
				if(!"".equals(adminUserIdsInvoice)){
					String[] invoicePersonArr = adminUserIdsInvoice.split(",");
					if(invoicePersonArr.length != invoicePersons.length){
						invoicePersonIsChange = true;
					}else{
						HashSet<String> invoicePersonIdSet = new HashSet<String>();
						for (int i = 0; i < invoicePersons.length; i++) {
							invoicePersonIdSet.add(invoicePersons[i].getString("schedule_execute_id"));
						}
						int invoicePersonIdsBeforeLen = invoicePersonIdSet.size();
						for(int j = 0; j < invoicePersonArr.length; j ++){
							invoicePersonIdSet.add(invoicePersonArr[j]);
						}
						int invoicePersonIdsNowLen = invoicePersonIdSet.size();
						if(invoicePersonIdsBeforeLen != invoicePersonIdsNowLen){
							invoicePersonIsChange = true;
						}
					}
				}
				if(invoiceContentIsChange || invoicePersonIsChange){
					scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE, 
							strSendContent, adminUserIdsInvoice, request, need_mail_invoice, need_message_invoice, need_page_invoice);
					if(3 == pur_invoice){
						addPurchasefollowuplogs(ProcessKey.INVOICE, pur_invoice, purchase_id, "[发票流程]["+invoiceKey.getInvoiceById(pur_invoice)+"]", proposer_id, proposer);
					}else{
						addPurchasefollowuplogs(ProcessKey.INVOICE, invoice, purchase_id, invoiceContent, proposer_id, proposer);
					}
				}
			}
			
			int needMailDrawback		= StringUtil.getInt(request, "needMailDrawback");
			int needMessageDrawback		= StringUtil.getInt(request, "needMessageDrawback");
			int needPageDrawback		= StringUtil.getInt(request, "needPageDrawback");
			String adminUserIdsDrawback	= StringUtil.getString(request, "adminUserIdsDrawback");
			
				boolean need_mail_drawback = false;
				if(2 == needMailDrawback){
					need_mail_drawback = true;
				}
				boolean need_message_drawback = false;
				if(2 == needMessageDrawback){
					need_message_drawback = true;
				}
				boolean need_page_drawback = false;
				if(2 == needPageDrawback){
					need_page_drawback = true;
				}
			//退税未添加过任务或者创建时不需要任务，而修改时需要任务，添加任务
			if((0 == pur_drawback && 2 == drawback) || (1 == pur_drawback && 2 == drawback)){
				scheduleMgrZr.addScheduleByExternalArrangeNow(purchaseRow.getString("purchase_date"), "2012-03-16 13:23:34", 
						adminLoggerBean.getAdid(), adminUserIdsDrawback, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[退税流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
						need_page_drawback, need_mail_drawback, need_message_drawback, request, false, ProcessKey.DRAWBACK);
			
				addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, drawbackContent, proposer_id, proposer);
			//创建时需要，更新时不需要，删除任务
			}else if(2 == pur_drawback && 1 == drawback){
				DBRow scheduleDrawback	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
				long scheduleDrawbackId	= scheduleDrawback.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleDrawbackId, request);
				//floorScheduleMgrZr.deleteScheduleById(scheduleDrawbackId);
				addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, "不需要退税", proposer_id, proposer);
			//创建和更新都需要,比较内容或者负责人
			}else if((2 == pur_drawback && 2 == drawback) || 3 == pur_drawback){
				boolean drawbackContentIsChange = false;
				boolean drawbackPersonIsChange	= false;
				//内容不同，更新内容
				String strDrawback = handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
						, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel);
				if(rebate_rate != pur_rebate_rate){
					drawbackContentIsChange = true;
					
					
				}
				//比较退税的负责人Ids
				DBRow[] drawbackPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
				if(!"".equals(adminUserIdsDrawback)){
					String[] drawbackPersonsArr = adminUserIdsDrawback.split(",");
					//如果已安排的负责人与更新时的负责人长度不同，更改任务负责人员
					if(drawbackPersons.length != drawbackPersonsArr.length){
						drawbackPersonIsChange = true;
						
					//如果相同，看看人员是否全部相同
					}else{
						HashSet<String> drawbackPersonSet = new HashSet<String>();
						for (int i = 0; i < drawbackPersons.length; i++) {
							drawbackPersonSet.add(drawbackPersons[i].getString("schedule_execute_id"));
						}
						int drawbackPersonsLen = drawbackPersonSet.size();
						for (int j = 0; j < drawbackPersonsArr.length; j++) {
							drawbackPersonSet.add(drawbackPersonsArr[j]);
						}
						int drawbackPersonsArrLen = drawbackPersonSet.size();
						//如果负责人不同，更改任务的负责人接口
						if(drawbackPersonsLen != drawbackPersonsArrLen){
							drawbackPersonIsChange = true;
							
							
							
						}
						
					}
				}
				if(drawbackContentIsChange || drawbackPersonIsChange){
					DBRow scheduleDrawback	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
					if(null == scheduleDrawback){
						scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
								adminLoggerBean.getAdid(), adminUserIdsDrawback, "",
								purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[退税流程]", 
								handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
										, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel), need_page_drawback, 
								need_mail_drawback, need_message_drawback, request, false, ProcessKey.DRAWBACK);
						if(2 == drawback){
							addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, drawbackContent, proposer_id, proposer);
						}else{
							addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, "[退税流程]["+drawbackKey.getDrawbackById(pur_drawback)+"]", proposer_id, proposer);
						}
					}else{
						scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK,
								strSendContent, adminUserIdsDrawback, request, need_mail_drawback, need_message_drawback, need_page_drawback);
						if(2 == drawback){
							addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, drawbackContent, proposer_id, proposer);
						}else{
							addPurchasefollowuplogs(ProcessKey.DRAWBACK, drawback, purchase_id, "[退税流程]["+drawbackKey.getDrawbackById(pur_drawback)+"]", proposer_id, proposer);
						}
					}
				}
			}
			int needMailTag				= StringUtil.getInt(request, "needMailTag");
			int needMessageTag			= StringUtil.getInt(request, "needMessageTag");
			int needPageTag				= StringUtil.getInt(request, "needPageTag");
			String adminUserIdsTag		= StringUtil.getString(request, "adminUserIdsTag");
			//没有创建过任务，但现在需要任务，创建任务
				boolean need_mail_tag	= false;
				if(2 == needMailTag){
					need_mail_tag		= true;
				}
				boolean need_message_tag= false;
				if(2 == needMessageTag){
					need_message_tag	= true;
				}
				boolean need_page_tag	= false;
				if(2 == needPageTag){
					need_page_tag		= true;
				}
			if((0 == pur_need_tag && 2 == need_tag) || (1 == pur_need_tag && 2 == need_tag)){
				scheduleMgrZr.addScheduleByExternalArrangeNow(purchaseRow.getString("purchase_date"), "2012-03-17 13:23:34", 
						adminLoggerBean.getAdid(), adminUserIdsTag, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[制签流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
						need_page_tag, need_mail_tag, need_message_tag, request, true, ProcessKey.PURCHASETAG);
				addPurchasefollowuplogs(ProcessKey.PURCHASETAG, need_tag, purchase_id, "[制签流程][需要制签]", proposer_id, proposer);
			}else if(2 == pur_need_tag && 1 == need_tag){
				DBRow scheduleTag	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				long scheduleTagId	= scheduleTag.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleTagId, request);
				addPurchasefollowuplogs(ProcessKey.PURCHASETAG, need_tag, purchase_id, "[制签流程][不需要制签]", proposer_id, proposer);
			}else if(2 == pur_need_tag && 2 == need_tag){
				boolean needTagPersonIsChange = false;
				DBRow[] tagPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				if(!"".equals(adminUserIdsTag)){
					String[] tagPersonArr = adminUserIdsTag.split(",");
					if(tagPersons.length != tagPersonArr.length){
						needTagPersonIsChange = true;
					}else{
						HashSet<String> tagPersonSet = new HashSet<String>();
						for (int i = 0; i < tagPersons.length; i++) {
							tagPersonSet.add(tagPersons[i].getString("schedule_execute_id"));
						}
						int tagPersonsLen = tagPersonSet.size();
						for (int j = 0; j < tagPersonArr.length; j++) {
							tagPersonSet.add(tagPersonArr[j]);
						}
						int tagPersonsArrLen = tagPersonSet.size();
						if(tagPersonsLen != tagPersonsArrLen){
							needTagPersonIsChange = true;
						}
					}
				}
				if(needTagPersonIsChange){
					scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG, 
							strSendContent, adminUserIdsTag, request, need_mail_tag, need_message_tag, need_page_tag);
					addPurchasefollowuplogs(ProcessKey.PURCHASETAG, need_tag, purchase_id, "[制签流程][需要制签]", proposer_id, proposer);
				}
			}
			int needMailQuality			= StringUtil.getInt(request, "needMailQuality");
			int needMessageQuality		= StringUtil.getInt(request, "needMessageQuality");
			int needPageQuality			= StringUtil.getInt(request, "needPageQuality");
			String adminUserIdsQuality	= StringUtil.getString(request, "adminUserIdsQuality");
			//原来不需要，现在需要，添加任务
				boolean need_mail_quality= false;
				if(2 == needMailQuality){
					need_mail_quality	= true;
				}
				boolean need_message_quality= false;
				if(2 == needMessageQuality){
					need_message_quality = true;
				}
				boolean need_page_quality= false;
				if(2 == needPageQuality){
					need_page_quality = true;
				}
			if((0 == pur_quality && 2 == quality_inspection) || (1 == pur_quality && 2 == quality_inspection)){
				scheduleMgrZr.addScheduleByExternalArrangeNow("2012-03-19 13:23:34", "2012-03-18 13:23:34", 
						adminLoggerBean.getAdid(), adminUserIdsQuality, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[质检流程]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
						need_page_quality, need_mail_quality, need_message_quality, request, false, ProcessKey.QUALITY_INSPECTION);
				addPurchasefollowuplogs(ProcessKey.QUALITY_INSPECTION, drawback, purchase_id, "[质检流程]["+new QualityInspectionKey().getQualityInspectionById(quality_inspection)+"]", proposer_id, proposer);
			//原来需要，现在不需要，删除任务
			}else if(2 == pur_quality && 1 == quality_inspection){
				DBRow scheduleQuality	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
				long scheduleQualityId	= scheduleQuality.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleQualityId, request);
				addPurchasefollowuplogs(ProcessKey.QUALITY_INSPECTION, drawback, purchase_id, "[质检流程]["+new QualityInspectionKey().getQualityInspectionById(pur_quality)+"]", proposer_id, proposer);
			}else if((2 == pur_quality && 2 == quality_inspection) || 3 == pur_quality){
				boolean qualityPersonIsChange = false;
				//比较质检的负责人Ids
				DBRow[] qualityPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
				if(!"".equals(adminUserIdsQuality)){
					String[] qualityPersonArr = adminUserIdsQuality.split(",");
					if(qualityPersons.length != qualityPersonArr.length){
						qualityPersonIsChange = true;
					}else{
						HashSet<String> qualityPersonSet = new HashSet<String>();
						for (int i = 0; i < qualityPersons.length; i++) {
							qualityPersonSet.add(qualityPersons[i].getString("schedule_execute_id"));
						}
						int qualityPersonsLen = qualityPersonSet.size();
						for (int j = 0; j < qualityPersonArr.length; j++) {
							qualityPersonSet.add(qualityPersonArr[j]);
						}
						int qualityPersonsArrLen = qualityPersonSet.size();
						//人不同，调接口，改任务负责人
						if(qualityPersonsLen != qualityPersonsArrLen){
							qualityPersonIsChange = true;
						}
					}
				}
				if(qualityPersonIsChange){
					scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION, 
							strSendContent, adminUserIdsQuality, request, need_mail_quality, need_message_quality, need_page_quality);
					addPurchasefollowuplogs(ProcessKey.QUALITY_INSPECTION, drawback, purchase_id, "[质检流程]["+new QualityInspectionKey().getQualityInspectionById(pur_quality)+"]", proposer_id, proposer);
				}
			}
			int needMailProductModel	= StringUtil.getInt(request, "needMailProductModel");
			int needMessageProductModel	= StringUtil.getInt(request, "needMessageProductModel");
			int needPageProductModel	= StringUtil.getInt(request, "needPageProductModel");
			String adminUserIdsProductModel= StringUtil.getString(request, "adminUserIdsProductModel");
			boolean need_mail_product_model = false;
			if(2 == needMailProductModel){
				need_mail_product_model = true;
			}
			boolean need_message_product_model = false;
			if(2 == needMessageProductModel){
				need_message_product_model = true;
			}
			boolean need_page_product_model = false;
			if(2 == needPageProductModel){
				need_page_product_model = true;
			}
			if((0 == pur_product_model && 2 == product_model) || (1 == pur_product_model && 2 == product_model)){
				scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
						adminLoggerBean.getAdid(), adminUserIdsProductModel, "",
						purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+",[商品范例]", 
						handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,true,request
								, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
								need_page_product_model, need_mail_product_model, need_message_product_model, request, false, ProcessKey.PURCHASE_RODUCTMODEL);
			}else if(2 == pur_product_model && 1 == product_model){
				DBRow scheduleProductModel	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
				if(null != scheduleProductModel){
					long scheduleProductModelId	= scheduleProductModel.get("schedule_id", 0L);
					scheduleMgrZr.deleteScheduleByScheduleId(scheduleProductModelId, request);
				}
			}else if((2 == pur_product_model && 2 == product_model) || 3 == pur_product_model){
				boolean productModelPersonIsChange = false;
				DBRow[] productModelPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
				if(!"".equals(adminUserIdsProductModel)){
					String[] productModelPersonArr = adminUserIdsProductModel.split(",");
					if(productModelPersons.length != productModelPersonArr.length){
						productModelPersonIsChange = true;
					}else{
						HashSet<String> productModelPersonSet = new HashSet<String>();
						for (int i = 0; i < productModelPersons.length; i++) {
							productModelPersonSet.add(productModelPersons[i].getString("schedule_execute_id"));
						}
						int productModelPersonsLen = productModelPersonSet.size();
						for (int j = 0; j < productModelPersonArr.length; j++) {
							productModelPersonSet.add(productModelPersonArr[j]);
						}
						int productModelPersonsArrLen = productModelPersonSet.size();
						if(productModelPersonsLen != productModelPersonsArrLen){
							productModelPersonIsChange = true;
						}
					}
				}
				if(productModelPersonIsChange){
					scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL, 
							strSendContent, adminUserIdsProductModel, request, need_mail_product_model, need_message_product_model,need_page_product_model);
				}
			}
			String adminUserIdsPurchaser= StringUtil.getString(request, "adminUserIdsPurchaser");//采购员
			//比较采购员Ids
			DBRow[] purchaserPersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
			if(!"".equals(adminUserIdsPurchaser)){
				boolean purchaserPersonIsChange = false;
				String[] purchaserArr   = adminUserIdsPurchaser.split(",");
				//如果人员个数不同，调接口，改任务负责人
				if(purchaserPersons.length != purchaserArr.length){
					purchaserPersonIsChange = true;
				//人数同，比人员	
				}else{
					HashSet<String> purchaserPersonSet = new HashSet<String>();
					for (int i = 0; i < purchaserPersons.length; i++) {
						purchaserPersonSet.add(purchaserPersons[i].getString("schedule_execute_id"));
					}
					int purchaserPersonsLen = purchaserPersonSet.size();
					for (int i = 0; i < purchaserArr.length; i++) {
						purchaserPersonSet.add(purchaserArr[i]);
					}
					int purchaserPersonsArrLen = purchaserPersonSet.size();
					if(purchaserPersonsLen != purchaserPersonsArrLen){
						purchaserPersonIsChange = true;
					}
				}
				if(purchaserPersonIsChange){
					DBRow schedulePurchaser	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS);
					if(null == schedulePurchaser){
						scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
								adminLoggerBean.getAdid(),adminUserIdsPurchaser, "",
								purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
								handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
										, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
								true, true, true, request, true, ProcessKey.PURCHASE_PURCHASERS);
					}else{
						scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_PURCHASERS, 
								strSendContent, adminUserIdsPurchaser, request, true, true, true);
					}
				}
			}
			String adminUserIdsDispatcher=StringUtil.getString(request, "adminUserIdsDispatcher");//调度人员
			DBRow[] dispatcherPersons = scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER);
			if(!"".equals(adminUserIdsDispatcher)){
				boolean dispatcherPersonIdChange = false;
				String[] dispacherArr = adminUserIdsDispatcher.split(",");
				if(dispatcherPersons.length != dispacherArr.length){
					dispatcherPersonIdChange = true;
				}else{
					HashSet<String> dispacherPersonSet = new HashSet<String>();
					for (int i = 0; i < dispatcherPersons.length; i++) {
						dispacherPersonSet.add(dispatcherPersons[i].getString("schedule_execute_id"));
		}
					int dispacherPersonsLen = dispacherPersonSet.size();
					for (int i = 0; i < dispacherArr.length; i++) {
						dispacherPersonSet.add(dispacherArr[i]);
		}
					int dispacherPersonArrLen = dispacherPersonSet.size();
					if(dispacherPersonsLen != dispacherPersonArrLen){
						dispatcherPersonIdChange = true;
					}
				}
				if(dispatcherPersonIdChange){
					DBRow scheduleDispatcher	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER);
					if(null == scheduleDispatcher){
						scheduleMgrZr.addScheduleByExternalArrangeNow("", "", 
								adminLoggerBean.getAdid(), adminUserIdsDispatcher, "",
								purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
								handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
										, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel),
										true, true, true, request, true, ProcessKey.PURCHASE_DISPATCHER);
					}else{
						scheduleMgrZr.updateScheduleExternal(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_DISPATCHER, 
								strSendContent, adminUserIdsDispatcher, request, true, true, true);
					}
				}
			}
			//更改主单据数据
			//添加日志
//			addPurchasefollowuplogs(ProcessKey.PURCHASETAG, need_tag, purchase_id, "", proposer_id, proposer);
//			addPurchasefollowuplogs(5, drawback, purchase_id, drawbackContent, proposer_id, proposer);
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseAllProcedures",log);
		}
	}
	
	@Override
	public long addPurchasefollowuplogs(int followup_type,int followTypeSub, long purchase_id,String followup_content,long follower_id,String follower)
	throws Exception
	{
		DBRow dbrow=new DBRow();
		dbrow.add("followup_type", followup_type);
		dbrow.add("followup_type_sub", followTypeSub);
		dbrow.add("purchase_id", purchase_id);
		dbrow.add("followup_content",followup_content);
		dbrow.add("follower_id",follower_id);
		dbrow.add("follower",follower);
		dbrow.add("followup_date",DateUtil.NowStr());
		return (fpm.addPurchasefollowuplogs(dbrow));
	}
	@Override
	public String[] updatePurchaseTermsAndQuality(HttpServletRequest request) throws Exception{
		String[] reArray = new String[5];
		try {
			String permitFile = "jpg,gif,bmp,jpeg,png,xls,doc,xlsx,docx,rar";
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_imags_tmp/");
			upload.setPermitFile(permitFile);
			int flag = upload.upload(request);
			long file_with_id				= upload.getRequestRow().get("purchase_id", 0L);
			int file_with_type				= upload.getRequestRow().get("file_with_type",0);
			String sn						= upload.getRequestRow().getString("sn");
			String path						= upload.getRequestRow().getString("path");
			String tempfilename				= upload.getRequestRow().getString("tempfilename");
			int isOutterUpdate				= upload.getRequestRow().get("isOutterUpdate",0);
			String expectArrTime			= upload.getRequestRow().getString("expectArrTime");
			String temp_url = Environment.getHome()+"upl_imags_tmp/"+upload.getFileName();
			String[] temp = upload.getFileName().split("\\."); 
			String fileType = temp[temp.length-1];
			String fileName = sn+"_"+ file_with_id+"_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+"."+fileType;
			String url = Environment.getHome()+"upload/"+path+"/"+fileName;
			FileUtil.moveFile(temp_url,url);
			DBRow file = new DBRow();
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("upload_adid",adminLoggerBean.getAdid());
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addTransportCertificateFile(file);
			if (flag==2)
			{
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				reArray[0] = fileName;
			}
			reArray[1] = file_with_id+"";
			reArray[2] = tempfilename;
			reArray[3] = isOutterUpdate +"";
			reArray[4] = expectArrTime;
			return reArray;
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseTermsAndQuality",log);
		}finally{
			return reArray;
		}
	}
	@Override
	public void addPurchaseTerms(HttpServletRequest request, long purchase_id) throws Exception{
		try {
			String repair_terms				= StringUtil.getString(request, "repair_terms_purchase");
			String labeling_terms			= StringUtil.getString(request, "labeling_terms_purchase");
			String delivery_product_terms	= StringUtil.getString(request, "delivery_product_terms_purchase");
			String purchase_terms			= StringUtil.getString(request, "purchase_terms_purchase");
			DBRow dbrow						= new DBRow();
			dbrow.add("repair_terms",repair_terms);
			dbrow.add("labeling_terms",labeling_terms);
			dbrow.add("delivery_product_terms",delivery_product_terms);
			dbrow.add("purchase_terms",purchase_terms);
			floorPurchaseMgrLL.updatePurchaseTerms(purchase_id, dbrow);
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseTerms",log);
		}
	}
	@Override
	public void updatePurchaseTerms(HttpServletRequest request) throws Exception{
		try {
			long purchaseId					= StringUtil.getLong(request, "purchase_id");
			String repair_terms				= StringUtil.getString(request, "repair_terms_purchase");
			String labeling_terms			= StringUtil.getString(request, "labeling_terms_purchase");
			String delivery_product_terms	= StringUtil.getString(request, "delivery_product_terms_purchase");
			String purchase_terms			= StringUtil.getString(request, "purchase_terms_purchase");
//			
			DBRow dbrow						= new DBRow();
			dbrow.add("repair_terms",repair_terms);
			dbrow.add("labeling_terms",labeling_terms);
			dbrow.add("delivery_product_terms",delivery_product_terms);
			dbrow.add("purchase_terms",purchase_terms);
			floorPurchaseMgrLL.updatePurchaseTerms(purchaseId, dbrow);
			purchaseMgrZyj.uploadPurchaseQualityInspection(request, purchaseId, StringUtil.getInt(request, "quality_inspection"));
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseTerms",log);
		}
	}
	
	/**
	 * 采购单过滤
	 * @param supplier_id
	 * @param purchase_status
	 * @param money_status
	 * @param beginTime
	 * @param endTime
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime,String endTime,PageCtrl pc, int day, long productline_id,int invoice,int drawback,int need_tag,int quality_inspection)
		throws Exception 
	{
		try 
		{
//			paypallog.info("ACCP");
			return fpm.getPurchaseFillter(supplier_id,ps_id,purchase_status, money_status,arrival_time,beginTime, endTime, pc, day, productline_id,invoice,drawback,need_tag,quality_inspection);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}	
	
	/**
	 * 供应商过滤采购单
	 * @param supplier_id
	 * @param ps_id
	 * @param purchase_status
	 * @param money_status
	 * @param arrival_time
	 * @param beginTime
	 * @param endTime
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseSupplierFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime, String endTime, PageCtrl pc)
		throws Exception 
	{
		try 
		{
			return fpm.getPurchaseSupplierFillter(supplier_id,ps_id,purchase_status, money_status,arrival_time,beginTime, endTime, pc);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getPurchaseSupplierFillter",log);
		}
	}	
	
	/**
	 * 采购单取消
	 * @param request
	 * @throws Exception
	 */
	public void cancelPurchase(HttpServletRequest request) 
		throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			DBRow param = new DBRow();
			param.add("purchase_status",PurchaseKey.CANCEL);
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			
			//取消资金申请
			applyMoneyMgrZZZ.cancelApplyMoneyByTypeAndAssociationId(100001,purchase_id);//资金申请类型100001为采购单
			//删除任务
			//this.deletePurchaseAllSchedule(purchase_id, request);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelPurchase",log);
		}
	}
	
	/**
	 * 删除与采购单相关的所有任务
	 * 现在与采购单关联的任务包括:采购单各个流程，采购单定金，采购单定金相关的转账
	 */
	private void deletePurchaseAllSchedule(long purchase_id, HttpServletRequest request) throws Exception
	{
		try 
		{
			//各流程任务
			int[] associateTypeArr = {ModuleKey.PURCHASE_ORDER};
			DBRow[] purchaseSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(purchase_id, associateTypeArr);
			for (int i = 0; i < purchaseSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(purchaseSchedules[i].get("schedule_id", 0L), request);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deletePurchaseAllSchedule",log);
		}
	}
	
	/**
	 * 根据条件查询采购单（页面使用）
	 * @param pc
	 * @param type
	 * @param cmd
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasePara(PageCtrl pc, int type,String cmd,long supplier_id)
		throws Exception 
	{
		try 
		{
			if (cmd!=null) 
			{
				
				return (fpm.getPurchaseByPara(pc, cmd,type,supplier_id));
			}
			
			return (fpm.getPurchaseByPara(pc,null,0,supplier_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchasePara",log);
		}
		
	}
	
	/**
	 * 根据条件查询全部采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseALL(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			return (fpm.getPurchaseByPara(pc,null,0,supplier_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseALL",log);
		}
	}
	
	/**
	 * 根据条件查询采购单需跟进
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseFOLLOWUP(HttpServletRequest request,PageCtrl pc,long ps_id,int day)
		throws Exception
	{
		try 
		{	
			return (fpm.getPurchaseByfollowup(pc,ps_id,day));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseFOLLOWUP",log);
		}
	}
	/**
	 * 根据条件查询采购单需确认价格
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseAFFIRMPRICE(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "purchase_status";
			
			return fpm.getPurchaseByPara(pc, type,1,supplier_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseAFFIRMPRICE",log);
		}
	}
	
	/**
	 * 根据条件查询采购单价格审核
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseAFFIRMTRANSFER(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "purchase_status";

			return fpm.getPurchaseByPara(pc, type,5,supplier_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseAFFIRMTRANSFER",log);
		}
	}
	
	/**
	 * 根据条件查询采购单未转账
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseNOTTRANSFER(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "purchase_status";
			
			return fpm.getPurchaseByPara(pc, type,6,supplier_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseNOTTRANSFER",log);
		}
	}
	
	/**
	 * 根据条件查询采购单部分转账
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasePARTTRANSFER(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "money_status";
			
			return fpm.getPurchaseByPara(pc, type,2,supplier_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchasePARTTRANSFER",log);
		}
	}
	
	/**
	 * 根据条件查询采购单全部转账
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseALLTRANSFER(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "money_status";
			
			return fpm.getPurchaseByPara(pc,type,3,supplier_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseALLTRANSFER",log);
		}
	}
	
	/**
	 * 根据条件查询采购单已完成
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseFINISH(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			
			return fpm.getPurchaseByfinish(pc,supplier_id);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getPurchaseFINISH",log);
		}
	}
	
	/**
	 * 根据条件查询采购单已取消
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseCANCEL(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{

			return fpm.getPurchaseBycancel(pc,supplier_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseCANCEL",log);
		}
	}
	
	/**
	 * 根据条件查询采购单需结清
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasePAYMENT(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			String type = "purchase_status";

			return fpm.getPurchaseByPara(pc,type,7,supplier_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseCANCEL",log);
		}
	}
	
	/**
	 * 全部到货采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseAllArrive(PageCtrl pc,long supplier_id) 
		throws Exception 
	{
		return (fpm.getPurchaseAllArrive(pc,supplier_id));
	}
	
	/**
	 * 没到货采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseNoArrive(PageCtrl pc,long supplier_id) 
		throws Exception 
	{
		return (fpm.getPurchaseNoArrive(pc,supplier_id));
	}
	
	
	/**
	 * 部分到货采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasePartArrive(PageCtrl pc,long supplier_id) 
		throws Exception 
	{
		return (fpm.getPurchasePartArrive(pc,supplier_id));
	}
	
	/**
	 * 多到货订单
	 * @param pc
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseMore(PageCtrl pc,long supplier_id)
		throws Exception
	{
		return (fpm.getPurchaseMore(pc, supplier_id));
	}
	
	/**
	 * 通知采购员
	 * @throws Exception
	 */
	public void notice(HttpServletRequest request)
		throws Exception
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		((PurchaseIFace)AopContext.currentProxy()).noticePurchaserForJbpm(purchase_id);
	}
	
	/**
	 * 工作流拦截通知采购员
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long noticePurchaserForJbpm(long purchase_id)
		throws Exception
	{
		return (purchase_id);
	}
	
	/**
	 * 根据ID获得采购单（页面使用）
	 */
	public DBRow getDetailPurchaseByPurchaseid(String id) 
		throws Exception 
	{
		try 
		{
			String regEx="[^0-9]";  
			Pattern p = Pattern.compile(regEx);  
			Matcher m = p.matcher(id);   
			long purchase_id = 0;
			if(!m.replaceAll("").equals(""))
			{
				purchase_id = Long.parseLong(m.replaceAll(""));
			}
			return (fpm.getPurchaseByPurchaseid(purchase_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseByPurchaseid",log);
		}
	}
	
	
	/**
	 * 转账
	 */
	public long purchaseTransfer(HttpServletRequest request) 
		throws Exception,TransferException
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String followup_content = StringUtil.getString(request,"followup_content");
			String transfertype = StringUtil.getString(request,"transfer_type");
			int money_status = StringUtil.getInt(request,"money_status");

			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
			if(fpm.checkPurchaseTransfer(purchase_id))
			{
				throw new TransferException();
			}
			
			DBRow para = new DBRow();
			para.add("updatetime",DateUtil.NowStr());
			para.add("transfer_date",DateUtil.NowStr());
			DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
//			if(purchase.get("purchase_status",0)==PurchaseKey.AFFIRMTRANSFER)//只在可转账时转款后被锁定
//			{
//				para.add("purchase_status",PurchaseKey.LOCK);
//			}
			
			if(transfertype.equals("part"))
			{
				//非第一次部分转账就不在变换为转账状态（少修改个字段）
				if (money_status==1) 
				{
					para.add("money_status",PurchaseKey.LOCK);
				}
				
				fpm.updatePurchase(purchase_id, para);
			}
			
			if(transfertype.equals("all"))
			{
				//para.add("money_status", PurchaseMoneyKey.ALLTRANSFER);
//				//改变商品原有价格与该采购单价格一致，已经移动到申请资金后变动
//				DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id,null,null);
//				for(int i=0;i<purchaseDetails.length;i++)
//				{
//					DBRow product = pm.getDetailProductByPname(purchaseDetails[i].getString("purchase_name"));
//					
//					if (product!=null) //预防找不到商品报错
//					{
//						DBRow updateparam = new DBRow();
//						updateparam.add("unit_price", purchaseDetails[i].get("price", 0.00));
//						pm.modProduct(product.get("pc_id", 0l), updateparam);
//					}
//				}
				
				fpm.updatePurchase(purchase_id, para);
				
				DBRow finish = fpm.checkPurchaseFinish(purchase_id);

				if(finish!=null)
				{
					DBRow updatepara = new DBRow();
					updatepara.add("purchase_status",PurchaseKey.FINISH);
						
					fpm.updatePurchase(purchase_id,updatepara);
				}
				
			}
			
			((PurchaseIFace)AopContext.currentProxy()).purchaseTransferForJbpm(purchase_id);
			
			return (addPurchasefollowuplogs(2, purchase_id, followup_content, follower_id,follower));
		} 
		
		catch (TransferException e) 
		{
			throw e;
		}
		
		catch (Exception e) 
		{
			throw new SystemException(e,"purchaseTransfer",log);
		}
	}
	
	/**
	 * 为工作流提供拦截，采购单转账
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long purchaseTransferForJbpm(long purchase_id)
		throws Exception
	{
		return purchase_id;
	}
	
	/**
	 * 采购单跟进
	 * @param request
	 * @throws Exception
	 */
	public void followupPurchase(HttpServletRequest request) 
		throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String followup_content = StringUtil.getString(request,"followup_content");
			
			DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
			int followuptype=purchase.get("money_status",0);
			TDate tDate = new TDate();
			
			if(followuptype==3)//已全部转款，3天后跟进
			{
				tDate.addDay(+3);
			}
			else//初始化+1天跟进在采购单生成时已做
			{
				tDate.addDay(+2);
			}
			
			int followupptype = StringUtil.getInt(request,"followup_ptype");
			if(followupptype==1)
				followup_content = "价格跟进:"+followup_content;
			else if(followupptype==2)
				followup_content = "交货跟进:"+followup_content;
			
			DBRow param = new DBRow();
			param.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
			addPurchasefollowuplogs(1, purchase_id,followup_content,follower_id, follower);
			String eta = StringUtil.getString(request, "eta") ;
			if(eta.length() > 0 ){
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"followupPurchase",log);
		}
	}
	
	/**
	 * 写备注（采购员使用）
	 * @param request
	 * @throws Exception
	 */
	public void logsPurchase(HttpServletRequest request) 
		throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String followup_content = StringUtil.getString(request,"followup_content");
			
			
			
			DBRow param = new DBRow();
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
			addPurchasefollowuplogs(1, purchase_id,followup_content, follower_id, follower);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"logsPurchase",log);
		}
	}
	
	/**
	 * 采购员确定价格
	 * @param request
	 * @throws Exception
	 */
	public void affirmPricePurchase(HttpServletRequest request) 
	throws Exception,NullPurchaseException,AffirmPriceException
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			if(fpm.notNullPurchase(purchase_id))//
			{
				throw new NullPurchaseException();
			}
			if (fpm.checkPurchasePrice(purchase_id)) //判断是否有价格为空或者采购数量为0
			{
				throw new AffirmPriceException();
			}
				String followup_content = StringUtil.getString(request,
						"followup_content");
				DBRow param = new DBRow();
				DBRow purchaseRow = floorPurchaseMgrLL.getPurchaseById(Long.toString(purchase_id));
				TDate tDate = new TDate();
				param.add("price_affirm_over", tDate.getDiffDate(purchaseRow.getString("purchase_date"), "dd"));
				param.add("purchase_status",PurchaseKey.AFFIRMTRANSFER);
				param.add("updatetime", DateUtil.NowStr());
				fpm.updatePurchase(purchase_id, param);
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr
						.getAdminLoginBean(StringUtil.getSession(request));
				String follower = adminLoggerBean.getEmploye_name();
				long follower_id = adminLoggerBean.getAdid();
				
//				SendMail sendMail = new SendMail();
//				sendMail.sendCustomAsyn("vvmecom@126.com","vvmecom@126.com","vvmecom+","sales@vvme.com","VVME LLC","smtp.126.com","25","joe@vvme.com","采购单"+purchase_id+"价格已确定",followup_content);
				
				//addPurchasefollowuplogs(1, purchase_id, followup_content, follower_id,follower);
				//需要加资金的日志
				addPurchasefollowuplogs(2, purchase_id, follower+"于"+DateUtil.NowStr()+"确认了价格,"+followup_content, follower_id,follower);
				addPurchasefollowuplogs(PurchaseLogTypeKey.PRICE,PurchaseKey.AFFIRMTRANSFER, purchase_id, follower+"于"+DateUtil.NowStr()+"确认了价格,"+followup_content, follower_id,follower);
				
				((PurchaseIFace)AopContext.currentProxy()).affirmPricePurchaseForJbpm(purchase_id);
		} 
		catch(NullPurchaseException e)
		{
			throw e;
		}
		catch(AffirmPriceException e)
		{
			throw e;
		}
		
		catch (Exception e)
		{
			throw new SystemException(e,"affirmPricePurchase",log);
		}
	}
	
	/**
	 * 采购单价格确定，提供工作流拦截
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long affirmPricePurchaseForJbpm(long purchase_id)
		throws Exception
	{
		return purchase_id;
	}
	
	/**
	 * 调度确定可以转账
	 * @param request
	 * @throws Exception
	 */
	public void affirmTransferPurchase(HttpServletRequest request) 
	throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			long supplier_id = StringUtil.getLong(request,"supplier_id");
			String remark = StringUtil.getString(request,"remark");
			
			double amount = StringUtil.getDouble(request,"amount");
			DBRow param = new DBRow();
			
			DBRow purchaseRow = floorPurchaseMgrLL.getPurchaseById(Long.toString(purchase_id));
			TDate tDate = new TDate();
			param.add("apply_money_over", tDate.getDiffDate(purchaseRow.getString("purchase_date"), "dd")-purchaseRow.get("price_affirm_over", 0));
			//param.add("purchase_status",PurchaseKey.AFFIRMTRANSFER);
			param.add("money_status",PurchaseMoneyKey.NOTTRANSFER);
			
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
//			SendMail sendMail = new SendMail();
//			sendMail.sendCustomAsyn("vvmecom@126.com","vvmecom@126.com","vvmecom+","sales@vvme.com","VVME LLC","smtp.126.com","25","xiaowu@vvme.com","采购单"+purchase_id+"已可以转账",followup_content);//xiaowu
			
			addPurchasefollowuplogs(1, purchase_id,remark, follower_id, follower);
			
			//资金申请
			DBRow supplier = supplierMgrTJH.getDetailSupplier(supplier_id);
			
			String payee  = StringUtil.getString(request, "payee");
			String bank_information = StringUtil.getString(request, "payment_information");
			DBRow applyTemp = applyMoneyMgrZZZ.addApplyMoney(DateUtil.NowStr(),String.valueOf(adminLoggerBean.getAdid()),100001l,purchase_id,amount,payee,bank_information,remark,100019,0,4,1,0,0,0,0,"RMB",adminLoggerBean,request);//采购单申请资金，成本中心，产品线暂时为0
			//ApplyMoneyIndexMgr.getInstance().addIndex(applyTemp.get("applyId", 0l), applyTemp.get("association_id", 0l),applyTemp.get("adid", 0l) ,applyTemp.getString("payment_information") ,applyTemp.getString("payee") ,applyTemp.get("association_type_id",0) );
			
			//改变商品原有价格与该采购单价格一致
			DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id,null,null);
			for(int i=0;i<purchaseDetails.length;i++)
			{
				DBRow product = pm.getDetailProductByPname(purchaseDetails[i].getString("purchase_name"));
				
				if (product!=null) //预防找不到商品报错
				{
					DBRow updateparam = new DBRow();
					updateparam.add("unit_price", purchaseDetails[i].get("price", 0.00));
					pm.modProduct(product.get("pc_id", 0l), updateparam);
					
					//根据商品ID获得商品信息,记录商品修改日志
					DBRow product_log = pm.getDetailProductByPcid(product.get("pc_id", 0l));
					
					product_log.add("account",adminLoggerBean.getAdid());
					product_log.add("edit_type",ProductEditTypeKey.UPDATE);
					product_log.add("edit_reason",ProductEditReasonKey.PURCHASE);
					product_log.add("post_date",DateUtil.NowStr());
					floorProductLogsMgrZJ.addProductLogs(product_log);
				}
			}
			
			DBRow[] changes = fpm.getChangePriceProduct(purchase_id);
			
			if(changes.length>0)//价格变动，提供工作流拦截
			{
				((PurchaseIFace)AopContext.currentProxy()).productPriceChange(purchase_id);
			}
			
			((PurchaseIFace)AopContext.currentProxy()).affirmTransferPurchaseForJbpm(purchase_id);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"affirmTransferPurchase",log);
		}
	}
	
	/**
	 * 采购单可转账，工作流拦截
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long affirmTransferPurchaseForJbpm(long purchase_id)
		throws Exception
	{
		return (purchase_id);
	}
	
	/**
	 * 调度强制采购单完成
	 * @param request
	 * @throws Exception
	 */
	public void finishPurchase(HttpServletRequest request) 
	throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String followup_content = StringUtil.getString(request,"followup_content");
			DBRow param = new DBRow();
			
			param.add("purchase_status",PurchaseKey.FINISH);
			
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
			addPurchasefollowuplogs(1, purchase_id,followup_content, follower_id, follower);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"finshPurchase",log);
		}
	}
	
	/**
	 * 根据采购单ID，供应商ID获得采购单
	 * @param purchase_id
	 * @param supplier
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseByIdAndSupplierId(HttpServletRequest request) 
		throws Exception
	{
		try 
		{
			String regEx="[^0-9]";  
			Pattern p = Pattern.compile(regEx);  
			Matcher m = p.matcher(StringUtil.getString(request,"purchase_id"));   
			long purchase_id = 0;
			if(!m.replaceAll("").equals(""))
			{
				purchase_id = Long.parseLong(m.replaceAll(""));
			}
			
			long supplier = StringUtil.getLong(request,"account");
			String licence = StringUtil.getString(request,"licence");
			if(!licence.equals(LoginLicence.getTheLicence(request)))
			{
				throw new VerifyCodeIncorrectException();
			}
			
			return fpm.getDetailPurchaseByIdAndSupplierId(purchase_id, supplier);
			
		}
		catch(VerifyCodeIncorrectException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	
	
	// 采购单详细
	/**
	 * 根据采购单ID获得采购单详细(页面使用)
	 * @param purchase_id,pc,type
	 * @throws Exception
	 */
	public DBRow[] getPurchaseDetailByPurchaseid(long purchase_id, PageCtrl pc,String type)
		throws Exception 
	{
		try 
		{
			DBRow[] purchaseDetail=fpm.getPurchaseDetail(purchase_id, pc,type);
			
			for(int i=0;i<purchaseDetail.length;i++)
			{
				String pname = purchaseDetail[i].getString("purchase_name");
				DBRow product = pm.getDetailProductByPname(pname);
				
				//预防商品已被删除
				if (product!=null) 
				{
					purchaseDetail[i].add("historyprice", product.getString("unit_price"));
					purchaseDetail[i].add("unit_name", product.getString("unit_name"));
				}
			}
			
			return (purchaseDetail);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseDetailByPurchaseid",log);
		}
	}
	
	/**
	 * 采购单内根据商品名模糊查询
	 * @param purchase_id
	 * @param purchase_name
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseDetailByPurchasename(long purchase_id,String purchase_name)
		throws Exception
	{
		try 
		{
			DBRow[] purchaseDetails = fpm.getPurchaseDetailsByPurchasename(purchase_id, purchase_name);
			
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				String pname = purchaseDetails[i].getString("purchase_name");
				DBRow product = pm.getDetailProductByPname(pname);
				
				//预防商品已被删除
				if (product!=null) 
				{
					purchaseDetails[i].add("historyprice",product.getString("unit_price"));
					purchaseDetails[i].add("unit_name", product.getString("unit_name"));
				}
			}
			
			
			return (purchaseDetails);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseDetailByPurchasename",log);
		}
	}
	
	/**
	 * 根据采购单Id，获取一条明细
	 */
	public DBRow getPurchaseDetailOneByPurchaseId(String purchase_id) throws Exception{
		try {
			DBRow dbRow = null;
			if(null != purchase_id && !"".equals(purchase_id)){
				dbRow = fpm.getPurchaseDetailOneByPurchaseId(Long.parseLong(purchase_id));
			}
			return dbRow;
		} catch (Exception e) {
			throw new SystemException(e,"getPurchaseDetailOneByPurchaseId",log);
		}
	}

	/**
	 * 采购单详细上传
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String[] uploadPurchaseDetail(HttpServletRequest request, String purchase_id_str, String tempfilename, String expectArrTime,int isOutterUpdate)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String[] msg=new String[4];
			if(!"".equals(tempfilename)){
				msg[0] = tempfilename;
				msg[1] = purchase_id_str;
				msg[2] = expectArrTime;
				msg[3] = isOutterUpdate+"";
				return msg;
			}else{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String url = "";
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long purchase_id = Long.parseLong(upload.getRequestRow().getString("purchase_id"));
//				String repair_terms				= upload.getRequestRow().getString("repair_terms");
//				String labeling_terms			= upload.getRequestRow().getString("labeling_terms");
//				String delivery_product_terms	= upload.getRequestRow().getString("delivery_product_terms");
//				String purchase_terms			= upload.getRequestRow().getString("purchase_terms");
//				DBRow dbrow						= new DBRow();
//				dbrow.add("repair_terms",repair_terms);
//				dbrow.add("labeling_terms",labeling_terms);
//				dbrow.add("delivery_product_terms",delivery_product_terms);
//				dbrow.add("purchase_terms",purchase_terms);
//				floorPurchaseMgrLL.updatePurchaseTerms(purchase_id, dbrow);
			
			msg[1]=String.valueOf(purchase_id);
				
			if (flag==2)
			{
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg[0] = upload.getFileName();
					msg[2] = upload.getRequestRow().getString("expectArrTime");
					msg[3] = upload.getRequestRow().getString("isOutterUpdate");
			}
			return msg;
			}
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadPurchaseDetail",log);
		}
	}
	
	/**
	 * 将excel文件转换成DBRow[]
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelshow(String filename,String type)
		throws Exception
	{
		try 
		{
			HashMap<String,String> dbrowname=new HashMap<String, String>();//excel导出DBRow 字段重设置
			dbrowname.put("0", "p_name");
			dbrowname.put("1", "price");
			dbrowname.put("2", "prefill_order_count");
			dbrowname.put("3", "prefill_backup_count");
			dbrowname.put("4", "factory_type");
			
			HashMap<String,String>pcidMap = new HashMap<String, String>();
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_imags_tmp/" + filename;	
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);

			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
  
			for(int i=1;i<rsRows;i++)
			{
			   DBRow pro = new DBRow();
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
				   if(j==0)
				   {
					   DBRow product = pm.getDetailProductByPname(rs.getCell(j,i).getContents().trim());
					   if(product==null)//商品查不到，跳过循环，不添加入数组
					   {
						   continue;
					   }
					   
					   if(pcidMap.containsKey(String.valueOf(product.get("pc_id",0l))))
					   {
						   continue;
					   }
					   pcidMap.put(String.valueOf(product.get("pc_id",0l)),String.valueOf(product.get("pc_id",0l)));
					   pro.add("pc_id",product.get("pc_id",0l));
				   }
				   pro.add(dbrowname.get(String.valueOf(j)),rs.getCell(j,i).getContents().trim());  //转换成DBRow数组输出,去掉空格
			    
				   ArrayList proName=pro.getFieldNames();//DBRow 字段名
				   for(int p=0;p<proName.size();p++)
				   {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				   }
			   }
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			}
			return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelshow",log);
		}
	}
	
	
	/**
	 * 上传的采购单详细录入数据库
	 * @param request
	 * @throws Exception
	 * @throws FileException
	 */
	public void savePurchaseDetail(HttpServletRequest request) 
		throws Exception,FileException
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			this.addPurchaseDetail(purchase_id, request);
		
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			long follower_id = adminLoggerBean.getAdid();
			addPurchasefollowuplogs(3, purchase_id,"采购单导入", follower_id, follower);
			
			DBRow paraPurchase = new DBRow();
			paraPurchase.add("updatetime",DateUtil.NowStr());
			
			fpm.updatePurchase(purchase_id, paraPurchase);
		} 
		catch (FileException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"savePurchaseDetail",log);
		}
	}
	
	/**
	 * 添加采购单详细
	 * @param purchase_id
	 * @param request
	 * @throws Exception
	 */
	public void addPurchaseDetail(long purchase_id,HttpServletRequest request)
		throws Exception
	{
		fpm.delPurchaseDetailByPurchaseid(purchase_id);//批量删除采购单明细
		String expectArrTime = StringUtil.getString(request, "expectArrTime");//预计到达时间
		
		String[] pcids = request.getParameterValues("pc_id");
		
		if (pcids==null)
		{
			pcids = new String[0];
		}
		for (int i = 0; i < pcids.length; i++) 
		{
			long pc_id = Long.parseLong(pcids[i]);
			
			int order_count = (int)StringUtil.getFloat(request,pcids[i]+"_order_count");
			int backup_count = (int)StringUtil.getFloat(request,pcids[i]+"_backup_count");
			int purchase_count = order_count+backup_count;
			
			String factory_type = StringUtil.getString(request,pcids[i]+"_factory");
			double price = StringUtil.getDouble(request,pcids[i]+"_price");
			
			DBRow product = pm.getDetailProductByPcid(pc_id);
			double history_price = product.get("unit_price",0d);
			String product_barcod = product.getString("p_code");
			
			
			DBRow purchaseDetail = new DBRow();
			purchaseDetail.add("purchase_id",purchase_id);
			purchaseDetail.add("purchase_name",product.getString("p_name"));
			purchaseDetail.add("purchase_count",purchase_count);
			purchaseDetail.add("price",price);
			purchaseDetail.add("proudct_barcod",product.getString("p_code"));
			purchaseDetail.add("product_history_price",history_price);
			purchaseDetail.add("product_id",pc_id);
			purchaseDetail.add("backup_count",backup_count);
			purchaseDetail.add("order_count",order_count);
			purchaseDetail.add("proudct_barcod",product_barcod);
			purchaseDetail.add("purchase_volume",product.get("volume",0f));
			purchaseDetail.add("factory_type",factory_type);
			purchaseDetail.add("purchase_weight",product.get("weight",0f));
			if(expectArrTime.length()>0 && expectArrTime.trim().length()>0)
			{
				purchaseDetail.add("eta", expectArrTime);
			}
			
			fpm.addPurchaseDetail(purchaseDetail);
		}
	}
	
	/**
	 * 价格改变
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public String productPriceChange(long purchase_id)
	throws Exception
	{
		StringBuffer re = new StringBuffer(""); 
		re.append(purchase_id);
		re.append("-");
		re.append(System.currentTimeMillis());
		return re.toString();
	}
	
	/**
	 * 打印详细
	 */
	public DBRow[] getPurchaseDetails(HttpServletRequest request)
		throws Exception
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		
		return (fpm.getPurchaseDetail(purchase_id,null,null));
	}

	/**
	 * 清空采购单详细
	 * @param request
	 * @throws Exception
	 */
	public void clearPurchaseDetailByPurchaseid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long purchase_id=StringUtil.getLong(request,"purchase_id");
			fpm.delPurchaseDetailByPurchaseid(purchase_id);
			
			DBRow paramupdate = new DBRow(); 
			paramupdate.add("updatetime",DateUtil.NowStr());
			paramupdate.add("purchasedetail",null);
			paramupdate.add("purchase_sumprice",null);
			
			fpm.updatePurchase(purchase_id, paramupdate);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"clearPurchaseDetailByPurchaseid",log);
		}
	}
	
	/**
	 * 入库文件上传
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String[] uploadIncoming(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String permitFile = "csv";
			
			TUpload upload = new TUpload();
			
			String url = "";
			String[] msg=new String[3];
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_incoming_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long purchase_id = Long.parseLong(upload.getRequestRow().getString("purchase_id"));
			
			msg[1]=String.valueOf(purchase_id);
			msg[2]=upload.getRequestRow().getString("machine_id");
				
			if (flag==2)
			{
				
				throw new FileTypeException();//上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg[0] = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadPurchaseDetail",log);
		}
	}
	
	/**
	 * 上传入库日志转换DBRow
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] csvDBRow(String filename,String type)
		throws Exception
	{
		String url = "";
		if (type.equals("show")) 
		{
			url = Environment.getHome() + "upl_incoming_tmp/" + filename;
		}
		else if(type.equals("data"))
		{
			url = Environment.getHome() + "administrator/purchase/upl_incoming_csv/" + filename;
		}
		String[] content = FileUtil.readFile2Array(url);
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		HashMap fieldNames = new HashMap();
		fieldNames.put(0,"product_barcod");
		fieldNames.put(1,"reap_count");
		fieldNames.put(2,"storage");
		for(int i=0;i<content.length;i++)
		{
			Pattern pattern = Pattern.compile("[\u4e00-\u9fa5]");
			Matcher matcher = pattern.matcher(content[i]);
			if(matcher.find())
			{
				continue;
			}
			DBRow db = new DBRow();
			String[] detail = content[i].split(",");
			for(int j=0;j<detail.length;j++)
			{
				if(fieldNames.containsKey(j))//预防多出列
				{
					db.add(fieldNames.get(j).toString(),detail[j]);
				}
			}
			al.add(db);
		}
		return al.toArray(new DBRow[0]);
	}
	
	/**
	 * 根据商品条码在采购单内查询商品
	 * @param purchase_id
	 * @param proudct_barcod
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseDetailByproductbarcod(long purchase_id,String proudct_barcod)
		throws Exception
	{
		return (fpm.getPurchaseDetailByProductbarcod(purchase_id, proudct_barcod));
	}
	
	/**
	 * 采购单商品入库
	 * @param request
	 * @throws Exception
	 */
	public void incoming(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String tempfilename = StringUtil.getString(request,"tempfilename");
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			String filename = "In"+purchase_id+"-"+tempfilename;
			
			String temp_url = Environment.getHome()+"upl_incoming_tmp/"+tempfilename;
			String url = Environment.getHome()+"administrator/purchase/upl_incoming_csv/"+filename;
			
			FileUtil.moveFile(temp_url,url);
			
			DBRow[] purchasedetails = csvDBRow(filename,"data");
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			String machine_id = "";
			if (request!=null) 
			{
				machine_id = StringUtil.getString(request, "machine_id");
				machine_id = machine_id.split("_")[1].split("\\.")[0];
			}
			incomingSub(purchasedetails, purchase_id, machine_id, adminLoggerBean);
			
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"incoming",log);
		}
	}
	
	
	/**
	 * 采购单关键字检索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchPurchaseBySupplierOrId(String key, PageCtrl pc)
		throws Exception 
	{
		return (fpm.searchPurchaseBySupplierOrId(key, pc));
	}
	
	
	/**
	 * 供应商根据关键字搜索采购单
	 * @param key
	 * @param pc
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] supplierSearchPurchaseBySupplierOrId(String key,PageCtrl pc, long supplier_id)
		throws Exception 
	{
		return fpm.supplierSearchPurchaseBySupplierOrId(key, pc, supplier_id);
	}
	
	
	/**
	 * 采购单入库提交
	 * @param purchasedetails
	 * @param purchase_id
	 * @param machine_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void incomingSub(DBRow[] purchasedetails,long purchase_id,String machine_id,AdminLoginBean adminLoggerBean)
		throws NoPurchaseException,ProductNotExistException,CanNotInbondScanException,Exception
	{
		try 
		{
			DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
			if(purchase == null)
			{
				throw new NoPurchaseException();
			}
			
			if(purchase.get("purchase_status",0)!=PurchaseKey.LOCK&&purchase.get("purchase_status",0)!=PurchaseKey.AFFIRMTRANSFER&&purchase.get("purchase_status",0)!=PurchaseKey.DELIVERYING)
			{
				throw new CanNotInbondScanException();
			}
			for (int i = 0; i < purchasedetails.length; i++)
			{
				DBRow purchasedetail = fpm.getPurchaseDetailByPcid(purchase_id,purchasedetails[i].get("tw_product_id",0l));
				float count = Float.valueOf(purchasedetails[i].getString("tw_count"));
				if(purchasedetail==null)
				{
					DBRow noInPurchaseProduct = productMgr.getDetailProductByPcid(purchasedetails[i].get("tw_product_id",0l));
					if(noInPurchaseProduct == null)
					{
						throw new ProductNotExistException();
					}
					DBRow noInPurchaseDeatil = new DBRow();
					noInPurchaseDeatil.add("purchase_id",purchase_id);
					noInPurchaseDeatil.add("purchase_name",noInPurchaseProduct.getString("p_name"));
					noInPurchaseDeatil.add("purchase_count",0f);
					noInPurchaseDeatil.add("reap_count",Float.parseFloat(purchasedetails[i].getString("tw_count")));
					noInPurchaseDeatil.add("price",noInPurchaseProduct.get("unit_price",0d));
					noInPurchaseDeatil.add("proudct_barcod",purchasedetails[i].getString("tw_product_barcode"));
					noInPurchaseDeatil.add("product_id",noInPurchaseProduct.get("pc_id",0l));
					noInPurchaseDeatil.add("product_history_price",noInPurchaseProduct.get("unit_price",0d));
					
					fpm.addPurchaseDetail(noInPurchaseDeatil);
					
					//throw new NoProductInPurchaseException();
				}
				else
				{
					float reap_count = Float.valueOf(purchasedetail.getString("reap_count"));
					
					reap_count = reap_count+count;
					
					DBRow param = new DBRow();
					param.add("reap_count",reap_count);
					fpm.updatePurchaseDetail(purchasedetail.get("purchase_detail_id",0), param);
					
					DBRow product = pm.getDetailProductByPcid(purchasedetail.get("product_id",0l));
					
					DBRow updateparam = new DBRow();
					updateparam.add("unit_price", purchasedetail.get("price",0.00));
					pm.modProduct(product.get("pc_id", 0l), updateparam);
					
					//根据商品ID获得商品信息,记录商品修改日志
					DBRow product_log = pm.getDetailProductByPcid(product.get("pc_id", 0l));
					
					product_log.add("account",adminLoggerBean.getAdid());
					product_log.add("edit_type",ProductEditTypeKey.UPDATE);
					product_log.add("edit_reason",ProductEditReasonKey.PURCHASE);
					product_log.add("post_date",DateUtil.NowStr());
					floorProductLogsMgrZJ.addProductLogs(product_log);
					
				}
				
			}
			
			//入库完后检查下采购单到货完成没
			DBRow[] results = fpm.checkPurchaseStats(purchase_id);
			
			DBRow updateparam = new DBRow();
			updateparam.add("updatetime",DateUtil.NowStr());
			if(results.length == 0)
			{
				if(fpm.checkPurchaseApprove(purchase_id) != 0)//采购单详细差异数量不为0
				{
					updateparam.add("arrival_time",PurchaseArriveKey.ARRIVEAPPROVE);//到货有差异
				}
				else
				{
					updateparam.add("arrival_time",PurchaseArriveKey.ALLARRIVE);//到货完成
					
					/**
					 * 到货完成认为是采购单完成
					 */
					DBRow para = new DBRow();
					TDate tDate = new TDate();
					para.add("purchase_over", tDate.getDiffDate(purchase.getString("purchase_date"), "dd"));
					para.add("purchase_status",PurchaseKey.FINISH);
					fpm.updatePurchase(purchase_id, para);
					
					deliveryMgrZJ.delDeliveryOrderNoFinishByPurchaseFinish(purchase_id);
				}
				
			}
			else
			{
				updateparam.add("arrival_time",PurchaseArriveKey.PARTARRIVE);//部分到货
			}
			
			fpm.updatePurchase(purchase_id,updateparam);

			//productMgr.reCheckLackingOrdersSub(adminLoggerBean);
			
			((PurchaseIFace)AopContext.currentProxy()).warehouseForJbpm(purchase_id);
		} 
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (NoProductInPurchaseException e) 
		{
			throw e;
		}
		catch (NoPurchaseException e) 
		{
			throw e;
		}
		catch(CanNotInbondScanException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"incomingSub",log);
		}
	}
	
	/**
	 * 工作流拦截采购单有商品入库
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long warehouseForJbpm(long purchase_id)
		throws Exception
	{
		return (purchase_id);
	}
	
	/**
	 * 采购单需结清，工作流拦截
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long paymentForJbpm(long purchase_id)
		throws Exception
	{
		return (purchase_id);
	}
	
	/**
	 * 修改采购单（合同条款）
	 * @param request
	 * @throws Exception
	 */
	public void modPurchase(HttpServletRequest request) 
		throws Exception 
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		String purchase_terms = StringUtil.getString(request,"purchase_terms");
		
		DBRow para = new DBRow();
		para.add("purchase_terms",purchase_terms);
		fpm.updatePurchase(purchase_id, para);
	}
	
	/**
	 * 修改交货地址
	 * @param request
	 * @throws Exception
	 */
	public void modPurchaseDelivery(HttpServletRequest request)
		throws Exception
	{
		long purchase_id = StringUtil.getLong(request,"purchase_id");
		String delivery_address = StringUtil.getString(request,"delivery_address");
		String delivery_linkman = StringUtil.getString(request,"delivery_linkman");
		String linkman_phone = StringUtil.getString(request,"linkman_phone");
		
		DBRow para = new DBRow();
		
		para.add("delivery_address",delivery_address);
		para.add("delivery_linkman",delivery_linkman);
		para.add("linkman_phone",linkman_phone);
		
		fpm.updatePurchase(purchase_id, para);
	}
	
	//日志
	/**
	 * @param followup_type 日志记录类型1跟进日志2转账日志3上传excel
	 */
	private long addPurchasefollowuplogs(int followup_type,long purchase_id,String followup_content,long follower_id,String follower)
		throws Exception
	{
		DBRow dbrow=new DBRow();
		dbrow.add("followup_type", followup_type);
		dbrow.add("purchase_id", purchase_id);
		dbrow.add("followup_content",followup_content);
		dbrow.add("follower_id",follower_id);
		dbrow.add("follower",follower);
		dbrow.add("followup_date",DateUtil.NowStr());
		return (fpm.addPurchasefollowuplogs(dbrow));
	}
	
	/**
	 * 根据类型显示跟进日志
	 * @param purchase_id
	 * @param followup_type
	 * @return 
	 * @throws Exception
	 */
	public DBRow[] getfollowuplogs(long purchase_id,int followup_type) 
		throws Exception
	{
		return (fpm.getfollowuplogs(purchase_id, followup_type));
	}
	
	
	
	//提供接口
	
	/**
	 * 根据商品查询最后一次采购信息
	 * @param pname
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByPnameNew(String pname,long ps_id) 
		throws Exception
	{
		try 
		{
			return (fpm.getDetailPurchaseDetailByPnameNew(pname,ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPurchaseDetailByPnameNew",log);
		}
	}
	
	/**
	 * 根据商品条码查询最后一次采购信息
	 * @param proudct_barcod
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByProductbarcodNew(String proudct_barcod,long ps_id)
		throws Exception
	{
		try 
		{
			return (fpm.getDetailPurchaseDetailByProductbarcodNew(proudct_barcod,ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPurchaseDetailByProductbarcodNew",log);
		}
	}
	
	/**
	 * 根据商品ID查询最后一次采购信息
	 * @param proudct_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByProductIdNew(long product_id,long ps_id)
		throws Exception
	{
		try 
		{
			return (fpm.getDetailPurchaseDetailByProductIdNew(product_id, ps_id));//返回查询到的采购单详细
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPurchaseDetailByProductIdNew",log);
		}
	}
	/**
	 * 查询所有预计到货时间小于今天且未到货采购单详细
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseDetailBydelay()
	throws Exception
	{
		try
		{
			return (fpm.getPurchaseDetailBydelay());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseDetailBydelay",log);
		}
	}
	
	/**
	 * 采购单货物预计到达日期已过还未到工作流拦截
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public long delayPurchaseForJbpm(long purchase_id) 
		throws Exception 
	{
		return (purchase_id);
	}
	
	/**
	 * 获得未完成的采购单
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNoFinishPurchase(long supplier_id) 
		throws Exception 
	{
		try 
		{
			return fpm.getNoFinishPurchase(supplier_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	@Override
	public DBRow[] getfollowuplogsLastNumber(long purchase_id, int followup_type, int number) throws Exception {
		try 
		{
			 return fpm.getLastNumberLogs(purchase_id, followup_type, number) ;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 根据采购单获得供应商地址和交货地址
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getSendAddressAndDeliveryAddressForPurchase(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
			
			DBRow supplier = supplierMgrTJH.getDetailSupplier(purchase.get("supplier",0l));
			
			DBRow allAddress = new DBRow();
			allAddress.add("deliver_house_number",purchase.getString("deliver_house_number"));
			allAddress.add("deliver_street",purchase.getString("deliver_street"));
			allAddress.add("deliver_name",purchase.getString("delivery_linkman"));
			allAddress.add("deliver_ccid",purchase.get("deliver_native",0l));
			allAddress.add("deliver_pro_id",purchase.get("deliver_provice",0l));
			allAddress.add("deliver_city",purchase.getString("deliver_city"));
			allAddress.add("deliver_zip_code",purchase.getString("deliver_zip_code"));
			allAddress.add("deliver_phone",purchase.getString("linkman_phone"));
			
			allAddress.add("send_house_number",purchase.getString("supplier_house_number"));
			allAddress.add("send_street",purchase.getString("supplier_street"));
			allAddress.add("send_name",purchase.getString("supplier_link_man"));
			allAddress.add("send_ccid",purchase.get("supplier_native",0l));
			allAddress.add("send_pro_id",purchase.get("supplier_province",0l));
			allAddress.add("send_city",purchase.getString("supplier_city"));
			allAddress.add("send_zip_code",purchase.getString("supplier_zip_code"));
			allAddress.add("send_phone",purchase.getString("supplier_link_phone"));
			
			allAddress.add("receive_psid",purchase.get("ps_id",0l));
			DBRow receiveProductStorage = floorStorageCatalogMgrZyj.getStorageCatalogById(purchase.get("ps_id",0l));
			if(null != receiveProductStorage)
			{
				allAddress.add("receive_psid_type", receiveProductStorage.get("storage_type", 0));
			}
			else
			{
				allAddress.add("receive_psid_type", -1);
			}
			allAddress.add("send_psid",supplier.get("id",0l));
			
			allAddress.add("sup_name",supplier.getString("sup_name"));
			
			
			return allAddress;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSendAddressAndDeliveryAddressForPurchase",log);
		}
		
	}
	
	/**
	 * 采购单详细Jqgrid展示
	 */
	public DBRow[] getPurchaseDetailByPurchaseId(long purchase_id, PageCtrl pc,String sidx, String sord, FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			return fpm.getPurchaseDetailByPurchaseIdForJqgrid(purchase_id, pc, sidx, sord, fillterBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseDetailByPurchaseId",log);
		}
	}
	
	/**
	 * 采购单详细jqgrid修改
	 */
	public void modPurchaseDetailGrid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long purchase_detail_id = StringUtil.getLong(request,"id");//grid默认参数是ID
			Map parameter = request.getParameterMap();
			String p_name;
			String eta;
			float backup_count;
			float order_count;
			double price;
			String factory_type;
			
			DBRow para = new DBRow();
			
			DBRow purchaseDetail = fpm.getDetailPurchaseDetailById(purchase_detail_id);
			DBRow product = productMgr.getDetailProductByPcid(purchaseDetail.get("product_id",0l));
			backup_count = purchaseDetail.get("backup_count",0f);
			order_count = purchaseDetail.get("order_count",0f);
			
			
			if (parameter.containsKey("p_name"))
			{
				p_name = StringUtil.getString(request,"p_name");
				product = productMgr.getDetailProductByPname(p_name);
				
				if(product==null)
				{
					throw new ProductNotExistException();
				}
				
				DBRow isExit = fpm.getPurchaseDetailByPcid(purchaseDetail.get("purchase_id",0l), purchaseDetail.get("product_id",0l));
				if(isExit!=null)
				{
					throw new PurchaseDetailRepartException();
				}
				
				para.add("purchase_name",p_name);
				para.add("product_id",product.get("pc_id",0l));
				para.add("product_history_price",product.get("unit_price",0d));
				para.add("proudct_barcod",product.getString("p_code"));
				
				para.add("purchase_volume",product.get("volume",0f));
				para.add("purchase_weight",product.get("weight",0f));
			}
			
			if(parameter.containsKey("eta"))
			{
				eta = StringUtil.getString(request,"eta");
				para.add("eta",eta);
			}
			
			if(parameter.containsKey("backup_count"))
			{
				backup_count = StringUtil.getFloat(request,"backup_count");
				para.add("backup_count",backup_count);
			}
			
			if(parameter.containsKey("order_count"))
			{
				order_count = StringUtil.getFloat(request,"order_count");
				para.add("order_count",order_count);
			}
			
			if(parameter.containsKey("price"))
			{
				price = StringUtil.getDouble(request,"price");
				para.add("price",price);
			}
			
			if(parameter.containsKey("factory_type"))
			{
				factory_type = StringUtil.getString(request,"factory_type");
				para.add("factory_type",factory_type);
			}
			//订购数量、备件数量、商品被修改后要重新计算商品总体积与采购单单明细的总数量
			if(parameter.containsKey("backup_count")||parameter.containsKey("order_count"))
			{
				float purchase_count = backup_count+order_count;
				
				//订购数量或备件数量修改，修改总数量
				if(parameter.containsKey("backup_count")||parameter.containsKey("order_count"))
				{
					para.add("purchase_count",purchase_count);
				}
				
			}
			fpm.updatePurchaseDetail(purchase_detail_id, para);
			

		} 
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modPurchaseDetailGrid",log);
		}
	}
	
	/**
	 * jqgrid采购单明细添加
	 */
	public void addPurchaseDetailGrid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String p_name = StringUtil.getString(request,"p_name");
			String eta = StringUtil.getString(request,"eta");
			float backup_count = StringUtil.getFloat(request,"backup_count");
			float order_count = StringUtil.getFloat(request,"order_count");
			double price = StringUtil.getDouble(request,"price");
			String factory_type = StringUtil.getString(request,"factory_type");
			
			float purchase_count = order_count+backup_count;
			
			DBRow product = productMgr.getDetailProductByPname(p_name);
			
			if(product==null)
			{
				throw new ProductNotExistException();
			}
			
			DBRow purchaseDetailIsExit = fpm.getPurchaseDetailByPcid(purchase_id,product.get("pc_id",0l));
			
			if(purchaseDetailIsExit!=null)
			{
				throw new PurchaseDetailRepartException();
			}
			
			float volume = product.get("volume",0f);
			float weight = product.get("weight",0f);
			
			DBRow purchaseDetail = new DBRow();
			purchaseDetail.add("purchase_name",p_name);
			purchaseDetail.add("product_id",product.get("pc_id",0l));
			purchaseDetail.add("product_history_price",product.get("unit_price",0d));
			purchaseDetail.add("proudct_barcod",product.getString("p_code"));
			purchaseDetail.add("purchase_id",purchase_id);
			purchaseDetail.add("eta",eta);
			purchaseDetail.add("backup_count",backup_count);
			purchaseDetail.add("order_count",order_count);
			purchaseDetail.add("price",price);
			purchaseDetail.add("factory_type",factory_type);
			purchaseDetail.add("purchase_count",purchase_count);
			
			purchaseDetail.add("purchase_volume",volume);
			purchaseDetail.add("purchase_weight",weight);
			
			fpm.addPurchaseDetail(purchaseDetail);
		}
		catch (PurchaseDetailRepartException e) 
		{
			throw e;
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addPurchaseDetailGrid",log);
		}
	}
	
	/**
	 * 删除采购单明细
	 * @param request
	 * @throws Exception
	 */
	public void delPurchaseDetailGrid(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long purchase_detail_id = StringUtil.getLong(request,"id");
			
			fpm.delPurchaseDetail(purchase_detail_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delPurchaseDetailGrid",log);
		}
	}
	
	public DBRow getPurchaseDetailById(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long purchase_detial_id = StringUtil.getLong(request,"purchase_detail_id");
			return fpm.getDetailPurchaseDetailById(purchase_detial_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseDetailById",log);
		}
	}
	
	public String downloadPurchase(HttpServletRequest request)
		throws Exception
	{
		try {
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
			
			DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id, null, null);
			if(purchase == null)
			{
				throw new NoPurchaseException();
			}
			if(purchaseDetails==null)
			{
				throw new NoProductInPurchaseException();
			}
			
			
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/purchase/purchase_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				DBRow purchaseDetail = purchaseDetails[i];
				long pc_id = purchaseDetail.get("product_id",0l);
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				
				row.createCell(1).setCellValue(purchaseDetail.get("price",0d));
				row.getCell(1).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				
				row.createCell(2).setCellValue(purchaseDetail.get("order_count",0f));
				row.getCell(2).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				
				row.createCell(3).setCellValue(purchaseDetail.get("backup_count",0f));
				row.getCell(3).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				
				
				row.createCell(4).setCellValue(purchaseDetail.getString("factory_type"));
			}
			
			String path = "upl_excel_tmp/P"+purchase_id+".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
			wb.write(fout);
			fout.close();
			return (path);
		}
		catch (NoPurchaseException e) 
		{
			throw e;
		}
		catch (NoProductInPurchaseException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"downloadPurchase",log);
		}
	}
	
	public DBRow[] searchPurchase(String search_key, int search_mode,PageCtrl pc) 
		throws Exception 
	{
		if(!search_key.equals("")&&!search_key.equals("\""))
		{
			search_key = search_key.replaceAll("\"","");
			search_key = search_key.replaceAll("'","");
			search_key = search_key.replaceAll("\\*","");
//			search_key +="*";这个索引是IK分词器，不要最后+*
		}
		
		
		int page_count = systemConfig.getIntConfigValue("page_count");
		
		return PurchaseIndexMgr.getInstance().getSearchResults(search_key,search_mode,page_count,pc); 
	}
	
	/**
	 * 获得采购单的体积
	 * @param purchase_id
	 * @return 四舍五入保留1位小数
	 * @throws Exception
	 */
	public float getPurchaseVolume(long purchase_id)
		throws Exception
	{
		try 
		{
			DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id, null, null);
			
			float volume = 0;
			
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				volume += purchaseDetails[i].get("purchase_volume",0f)*purchaseDetails[i].get("purchase_count",0f);
			}
			
			return (MoneyUtil.round(volume,1));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseVolume",log);
		}
	}
	
	/**
	 * 获得采购单重量
	 * @param purchase_id
	 * @return 四舍五入保留2位小数
	 * @throws Exception
	 */
	public float getPurchaseWeight(long purchase_id)
		throws Exception
	{
		try 
		{
			DBRow[] purchaseDetails = fpm.getPurchaseDetail(purchase_id, null, null);
			
			float weight = 0;
			
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				weight += purchaseDetails[i].get("purchase_weight",0f)*purchaseDetails[i].get("purchase_count",0f);
			}
			
			return (MoneyUtil.round(weight,2));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseVolume",log);
		}
	}
	
	
	
	/**
	 * 跟新采购单详细的重量与体积（取商品基础信息上数据）
	 * @param purchase_id
	 * @throws Exception
	 */
	public void updatePurchaseVW(long purchase_id)
		throws Exception
	{
		try
		{
			DBRow[] purchaseDetails; 
			
			if(purchase_id==0)
			{
				purchaseDetails = fpm.getAllPurchaseDetail();
			}
			else
			{
				purchaseDetails = fpm.getPurchaseDetailByPurchaseIdForJqgrid(purchase_id, null, null, null, null);
			}
				
			DBRow product = null;
			float product_volume = 0;
			float product_weight = 0;
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				product = pm.getDetailProductByPcid(purchaseDetails[i].get("product_id",0l));
				if(product!=null)
				{
					product_volume = product.get("volume",0f);
					product_weight = product.get("weight",0f);
				}
				DBRow para = new DBRow();
				para.add("purchase_volume",product_volume);
				para.add("purchase_weight",product_weight);
				fpm.updatePurchaseDetail(purchaseDetails[i].get("purchase_detail_id",0l), para);
			}
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updatePurchaseDetailVolume()
		throws Exception
	{
		try 
		{
			DBRow[] rows = new DBRow[0];
				//fpm.getAllPurchaseDetail();
			
			DBRow product = null;
			float product_volume = 0;
			float product_weight = 0;
			for (int i = 0; i < rows.length; i++) 
			{
				
				
				if(rows[i].get("volume",0f)==0f)
				{
					product = pm.getDetailProductByPcid(rows[i].get("product_id",0l));
					if(product!=null)
					{
						product_volume = product.get("volume",0f);
					}
					
					DBRow para = new DBRow();
					para.add("purchase_volume",product_volume);
					
					
					fpm.updatePurchaseDetail(rows[i].get("purchase_detail_id",0l), para);
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updatePurchaseDetailVolume",log);
		}
	}
	
	/**
	 * 添加各流程信息
	 */
	@Override
	public void addPurchaseProcedures(HttpServletRequest request, long purchase_id) throws Exception
	{
		try 
		{
			//获取当前登录者的信息
			AdminMgr adminMgr				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long adid						= adminLoggerBean.getAdid();
			String employeeName				=adminLoggerBean.getEmploye_name();
			//获取发票流程信息
			int invoice					= StringUtil.getInt(request, "invoice");
			double invoince_amount		= 0.0;
			int invoince_currency		= -1;
			double invoince_dot			= 0.0;
			if(InvoiceKey.INVOICE == invoice)
			{
				invoince_amount			= StringUtil.getDouble(request, "invoince_amount");
				invoince_currency		= StringUtil.getInt(request, "invoince_currency");
				invoince_dot			= StringUtil.getDouble(request, "invoince_dot");
			}
			int needMailInvoice			= StringUtil.getInt(request, "needMailInvoice");
			int needMessageInvoice		= StringUtil.getInt(request, "needMessageInvoice");
			int needPageInvoice			= StringUtil.getInt(request, "needPageInvoice");
			String adminUserIdsInvoice	= StringUtil.getString(request, "adminUserIdsInvoice");
			
			//获取退税流程信息
			int drawback				= StringUtil.getInt(request, "drawback");
			double rebate_rate			= 0.0;
			if(2 == drawback){
				rebate_rate				= StringUtil.getDouble(request, "rebate_rate");
			}
			int needMailDrawback		= StringUtil.getInt(request, "needMailDrawback");
			int needMessageDrawback		= StringUtil.getInt(request, "needMessageDrawback");
			int needPageDrawback		= StringUtil.getInt(request, "needPageDrawback");
			String adminUserIdsDrawback	= StringUtil.getString(request, "adminUserIdsDrawback");
			
			//获取制签流程信息
			int need_tag				= StringUtil.getInt(request, "need_tag");
			int needMailTag				= StringUtil.getInt(request, "needMailTag");
			int needMessageTag			= StringUtil.getInt(request, "needMessageTag");
			int needPageTag				= StringUtil.getInt(request, "needPageTag");
			String adminUserIdsTag		= StringUtil.getString(request, "adminUserIdsTag");
			
			//获取质检流程信息
			int quality_inspection		= StringUtil.getInt(request, "quality_inspection");
			int needMailQuality			= StringUtil.getInt(request, "needMailQuality");
			int needMessageQuality		= StringUtil.getInt(request, "needMessageQuality");
			int needPageQuality			= StringUtil.getInt(request, "needPageQuality");
			String adminUserIdsQuality	= StringUtil.getString(request, "adminUserIdsQuality");
			
			//获取商品范例流程信息
			int product_model			= StringUtil.getInt(request, "product_model");
			int needMailProductModel	= StringUtil.getInt(request, "needMailProductModel");
			int needMessageProductModel	= StringUtil.getInt(request, "needMessageProductModel");
			int needPageProductModel	= StringUtil.getInt(request, "needPageProductModel");
			String adminUserIdsProductModel= StringUtil.getString(request, "adminUserIdsProductModel");
			
			
			//获取各流程相关人的名字
			String adminUserNamesPurchaser	= StringUtil.getString(request, "adminUserNamesPurchaser");
			String adminUserNamesDispatcher	= StringUtil.getString(request, "adminUserNamesDispatcher");
			String adminUserNamesInvoice	= StringUtil.getString(request, "adminUserNamesInvoice");
			String adminUserNamesDrawback	= StringUtil.getString(request, "adminUserNamesDrawback");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			String adminUserNamesQuality	= StringUtil.getString(request, "adminUserNamesQuality");
			String adminUserNamesProductModel= StringUtil.getString(request, "adminUserNamesProductModel");
			
			//组织发送邮件或者短信的内容
			String sendContent = handleSendContent(purchase_id,invoice,invoince_amount,invoince_currency,invoince_dot,
							drawback,rebate_rate,need_tag,quality_inspection,product_model,false,request
					, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice,
					adminUserNamesDrawback, adminUserNamesTag, adminUserNamesQuality, adminUserNamesProductModel);
			//发票任务和日志
			this.addPurchaseProcedureInvoice(adid, adminUserIdsInvoice, "", purchase_id, 
					ModuleKey.PURCHASE_ORDER,needPageInvoice, needMailInvoice, needMessageInvoice, request, false,
					ProcessKey.INVOICE, invoice, employeeName, sendContent, 
					invoince_amount, invoince_currency, invoince_dot, 1);
			//退税任务和日志
			this.addPurchaseProcedureDrawback(adid, adminUserIdsDrawback, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageDrawback, needMailDrawback, needMessageDrawback, request, false,
					ProcessKey.DRAWBACK, drawback, employeeName, sendContent, rebate_rate, 1);
			//制签任务和日志
			this.addPurchaseProcedureTag(adid, adminUserIdsTag, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageTag, needMailTag, needMessageTag, request, true,
					ProcessKey.PURCHASETAG, need_tag, employeeName, sendContent, 1);
			//质检要求任务和日志
			this.addPurchaseProcedureQuality(adid, adminUserIdsQuality, "", purchase_id,
					ModuleKey.PURCHASE_ORDER, needPageQuality, needMailQuality, needMessageQuality, request, true, 
					ProcessKey.QUALITY_INSPECTION, quality_inspection, employeeName, sendContent, 1);
			//商品范例任务和日志
			this.addPurchaseProcedureProductModel(adid, adminUserIdsProductModel, "", purchase_id, 
					ModuleKey.PURCHASE_ORDER, needPageProductModel, needMailProductModel, needMessageProductModel, request, true,
					ProcessKey.PURCHASE_RODUCTMODEL, product_model, employeeName, sendContent, 1);
			//采购和调度任务和日志
			String adminUserIdsPurchaser= StringUtil.getString(request, "adminUserIdsPurchaser");//采购员
			String adminUserIdsDispatcher=StringUtil.getString(request, "adminUserIdsDispatcher");//调度人员
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(),adminUserIdsPurchaser, "",
					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					sendContent,
					true, true, true, request, true, ProcessKey.PURCHASE_PURCHASERS);
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(), adminUserIdsDispatcher, "",
					purchase_id, ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					sendContent,
					true, true, true, request, true, ProcessKey.PURCHASE_DISPATCHER);
			
		}
		catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedures",log);
		}
	}
	/**
	 * 保存发票流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @param invoince_amount 开票金额
	 * @param invoince_currency 货币
	 * @param invoince_dot 票点
	 * @param isNewOrUpdate 是更新还是创建
	 * @throws Exception
	 */
	private void addPurchaseProcedureInvoice(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName, String content,
			double invoince_amount, int invoince_currency, double invoince_dot, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(InvoiceKey.INVOICE == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				CurrencyKey currencyKey	= new CurrencyKey();
				InvoiceKey invoiceKey	= new InvoiceKey();
				String schedultTitle	= "采购单:"+purchase_id+",[发票流程]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"],["
				+ invoiceKey.getInvoiceById(String.valueOf(activity))+"]"
				+ ",开票金额："+invoince_amount
				+  " "+currencyKey.getCurrencyById(invoince_currency+"")
				+  ",票点："+invoince_dot+"%";
				
				this.addPurchaseScheduleOneProdure("", "", 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureInvoice",log);
		}
	}
	
	/**
	 * 保存退税流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @param rebate_rate 退税率
	 */
	private void addPurchaseProcedureDrawback(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName,
			String content, double rebate_rate, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(DrawbackKey.DRAWBACK == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				DrawbackKey drawbackKey	= new DrawbackKey();
				String schedultTitle	= "采购单:"+purchase_id+",[退税流程]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"],["
										+ drawbackKey.getDrawbackById(String.valueOf(activity))+"]"
										+ ",退税率："+rebate_rate+"%";
				
				this.addPurchaseScheduleOneProdure("", "", 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureDrawback",log);
		}
	}
	
	/**
	 * 保存制签流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @throws Exception
	 */
	private void addPurchaseProcedureTag(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName,String content, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(TransportTagKey.TAG == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				TransportTagKey transportTagKey = new TransportTagKey();
				String schedultTitle	= "采购单:"+purchase_id+",[内部标签]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"]";
				
				TDate tagEndTime = new TDate();
				tagEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period")));
				
				this.addPurchaseScheduleOneProdure(DateUtil.NowStr(), tagEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureTag",log);
		}
	}
	
	/**
	 * 保存制签流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @throws Exception
	 */
	private void addPurchaseProcedureTagThird(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName,String content, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(TransportTagKey.TAG == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				TransportTagKey transportTagKey = new TransportTagKey();
				String schedultTitle	= "采购单:"+purchase_id+",[第三方流程]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"]";
				if(TransportTagKey.TAG == activity)
				{
					selfLogContent += "[制签中]";
				}
				else
				{
					selfLogContent += "[无需制签]";
				}
				
				TDate tagEndTime = new TDate();
				tagEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period")));
				
				this.addPurchaseScheduleOneProdure(DateUtil.NowStr(), tagEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureTag",log);
		}
	}
	
	/**
	  * 保存质检要求流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @throws Exception
	 */
	private void addPurchaseProcedureQuality(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName,String content, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(QualityInspectionKey.NEED_QUALITY == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();
				String schedultTitle	= "采购单:"+purchase_id+",[质检要求流程]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"],["
										+ qualityInspectionKey.getQualityInspectionById(String.valueOf(activity))+"]";
				
				TDate qualityEndTime = new TDate();
				qualityEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_quality_period")));
				
				this.addPurchaseScheduleOneProdure(DateUtil.NowStr(), qualityEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureQuality",log);
		}
	}
	
	/**
	 * 保存商品范例流程
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param content 发送的内容
	 * @throws Exception
	 */
	private void addPurchaseProcedureProductModel(long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, String employeeName,String content, int isNewOrUpdate
		) throws Exception
	{
		try {
			if(PurchaseProductModelKey.NEED_PRODUCT_MODEL == activity)
			{
				String isNewOrUpdateStr = "";
				if(1 == isNewOrUpdate)
				{
					isNewOrUpdateStr = "创建";
				}
				else
				{
					isNewOrUpdateStr = "修改";
				}
				PurchaseProductModelKey purchaseProductModelKey = new PurchaseProductModelKey();
				String schedultTitle	= "采购单:"+purchase_id+",[商品范例流程]";
				//添加自身日志的内容
				String selfLogContent	= isNewOrUpdateStr+"采购单["+purchase_id+"],["
										+ purchaseProductModelKey.getProductModelKeyName(String.valueOf(activity))+"]";
				
				TDate pmEndTime = new TDate();
				pmEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_product_model")));
				
				this.addPurchaseScheduleOneProdure(DateUtil.NowStr(), pmEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), 
						adid, executePersonIds, joinPersonIds, 
						purchase_id, moduleId,
						schedultTitle, content, 
						page, mail, message, request, isNow,
						processType, activity, employeeName, selfLogContent);
			}
		} catch (Exception e) {
			throw new SystemException(e,"addPurchaseProcedureProductModel",log);
		}
	}
	
	/**
	 * 保存某个流程的任务
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param adid 当前登录者ID
	 * @param executePersonIds 任务执行人
	 * @param joinPersonIds 任务参与人
	 * @param purchase_id 采购单ID
	 * @param moduleId 模块ID
	 * @param title 标题
	 * @param content 发送的内容
	 * @param page 是否需要发页面通知
	 * @param mail 是否发邮件通知
	 * @param message 是否发短信
	 * @param request
	 * @param isNow 现在是否立即安排任务
	 * @param processType 流程ID
	 * @param activity 流程当前阶段
	 * @param employeeName 当前登录者名字
	 * @param logContent 日志内容
	 * @throws Exception
	 */
	private void addPurchaseScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception {
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			//添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, moduleId, processType);
			if(null == schedule){
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
						adid, executePersonIds, joinPersonIds,
						purchase_id, moduleId, title, content,
						needPage, needMail, needMessage, request, isNow, processType);
				//添加日志
				this.addPurchasefollowuplogs(processType, activity, purchase_id, logContent, adid, employeeName);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addPurchaseScheduleOneProdure",log);
		}
	}
	
	/**
	 * 更新任务
	 */
	private void updatePurchaseScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long purchase_id, int moduleId, String title, String content,
			int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent, boolean isChange
				) throws Exception{
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, moduleId, processType);
			DBRow[] executePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(purchase_id, moduleId, processType);
			boolean isUpdate = false;
			if(!"".equals(executePersonIds)){
				String[] adminUserIds	= executePersonIds.split(",");
				if(executePersons.length != adminUserIds.length){
					if(null != schedule){
						isUpdate = true;
					}else{
						this.addPurchaseScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
								joinPersonIds, purchase_id, moduleId, title, 
								content, page, mail, message, request, isNow, processType,
								activity, employeeName, logContent);
					}
				}else{
					HashSet<String> executePersonSet = new HashSet<String>();
					for (int i = 0; i < executePersons.length; i++) {
						executePersonSet.add(executePersons[i].getString("schedule_execute_id"));
					}
					int executePersonLen = executePersonSet.size();
					for (int i = 0; i < adminUserIds.length; i++) {
						executePersonSet.add(adminUserIds[i]);
					}
					int executePersonArrLen = executePersonSet.size();
					if(executePersonLen != executePersonArrLen){
						if(null != schedule){
							isUpdate = true;
						}else{
							this.addPurchaseScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
									joinPersonIds, purchase_id, moduleId, title, 
									content, page, mail, message, request, isNow, processType,
									activity, employeeName, logContent);
						}
					}
				}
			}
			if(isUpdate || isChange)
			{
				scheduleMgrZr.updateScheduleExternal(purchase_id, moduleId, processType, 
						content, executePersonIds, request, needMail, needMessage, needPage);
				//添加日志
				this.addPurchasefollowuplogs(processType, activity, purchase_id, logContent, adid, employeeName);
			}
		}catch (Exception e)
		{
			throw new SystemException(e,"updatePurchaseScheduleOneProdure",log);
		}
	}
	
	@Override
	public void updatePurchaseProdures(HttpServletRequest request) throws Exception
	{
		try 
		{
			//获取当前登录者的信息
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long adid						= adminLoggerBean.getAdid();
			String employeeName				=adminLoggerBean.getEmploye_name();
			//从数据库中取出已存的各个流程的信息
			String purchase_id			= StringUtil.getString(request, "purchase_id");
			DBRow purchaseRow			= fpm.getPurchaseByPurchaseid(Long.parseLong(purchase_id));
			int pur_invoice				= purchaseRow.get("invoice", 0);
			double pur_amount			= purchaseRow.get("invoince_amount", 0.00);
			int pur_currency			= purchaseRow.get("invoince_currency", 0);
			double pur_dot				= purchaseRow.get("invoince_dot", 0.00);
			int pur_drawback			= purchaseRow.get("drawback", 0);
			double pur_rebate_rate		= purchaseRow.get("rebate_rate", 0.00);
			int pur_need_tag			= purchaseRow.get("need_tag", 0);
			int pur_need_tag_third		= purchaseRow.get("need_third_tag", 0);
			int pur_quality				= purchaseRow.get("quality_inspection", 0);
			int pur_product_model		= purchaseRow.get("product_model", 0);
			
			int invoice					= StringUtil.getInt(request, "invoice");
			double invoince_amount		= StringUtil.getDouble(request, "invoince_amount");
			int invoince_currency		= StringUtil.getInt(request, "invoince_currency");
			double invoince_dot			= StringUtil.getDouble(request, "invoince_dot");
			int drawback				= StringUtil.getInt(request, "drawback");
			double rebate_rate			= StringUtil.getDouble(request, "rebate_rate");
			int need_tag_third			= StringUtil.getInt(request, "need_tag_third");
			String purchase_date		= DateUtil.NowStr();
			TDate tDate 				= new TDate();
			tDate.addDay(+1);
			String updatetime			= purchase_date;
			DBRow dbrow					= new DBRow();
			if(InvoiceKey.NOINVOICE == invoice || InvoiceKey.INVOICE == invoice)
			{
				dbrow.add("invoice",invoice);
				dbrow.add("invoince_amount",invoince_amount);
				dbrow.add("invoince_currency",invoince_currency);
				dbrow.add("invoince_dot",invoince_dot);
			}
			if(DrawbackKey.DRAWBACK == drawback || DrawbackKey.NODRAWBACK == drawback)
			{
				dbrow.add("drawback",drawback);
				dbrow.add("rebate_rate",rebate_rate);
			}
			if(TransportTagKey.TAG == need_tag_third || TransportTagKey.NOTAG == need_tag_third)
			{
				dbrow.add("need_third_tag", need_tag_third);
			}
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			fpm.updatePurchaseBasic(dbrow, Long.parseLong(purchase_id));
			
			//发票
			String adminUserIdsInvoice	= StringUtil.getString(request, "adminUserIdsInvoice");
			int needMailInvoice			= StringUtil.getInt(request, "needMailInvoice");
			int needMessageInvoice		= StringUtil.getInt(request, "needMessageInvoice");
			int needPageInvoice			= StringUtil.getInt(request, "needPageInvoice");
			//退税
			int needMailDrawback		= StringUtil.getInt(request, "needMailDrawback");
			int needMessageDrawback		= StringUtil.getInt(request, "needMessageDrawback");
			int needPageDrawback		= StringUtil.getInt(request, "needPageDrawback");
			String adminUserIdsDrawback	= StringUtil.getString(request, "adminUserIdsDrawback");
			//内部标签
			int needMailTag				= StringUtil.getInt(request, "needMailTag");
			int needMessageTag			= StringUtil.getInt(request, "needMessageTag");
			int needPageTag				= StringUtil.getInt(request, "needPageTag");
			String adminUserIdsTag		= StringUtil.getString(request, "adminUserIdsTag");
			//第三方标签
			int needMailTagThird		= StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird		= StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird		= StringUtil.getInt(request, "needPageTagThird");
			String adminUserIdsTagThird	= StringUtil.getString(request, "adminUserIdsTagThird");
			//质检
			int needMailQuality			= StringUtil.getInt(request, "needMailQuality");
			int needMessageQuality		= StringUtil.getInt(request, "needMessageQuality");
			int needPageQuality			= StringUtil.getInt(request, "needPageQuality");
			String adminUserIdsQuality	= StringUtil.getString(request, "adminUserIdsQuality");
			//商品范例
			int needMailProductModel	= StringUtil.getInt(request, "needMailProductModel");
			int needMessageProductModel	= StringUtil.getInt(request, "needMessageProductModel");
			int needPageProductModel	= StringUtil.getInt(request, "needPageProductModel");
			String adminUserIdsProductModel= StringUtil.getString(request, "adminUserIdsProductModel");
			
			//各流程的人名
			String adminUserNamesPurchaser	= StringUtil.getString(request, "adminUserNamesPurchaser");
			String adminUserNamesDispatcher	= "";
//				StrUtil.getString(request, "adminUserNamesDispatcher");
			String adminUserNamesInvoice	= StringUtil.getString(request, "adminUserNamesInvoice");
			String adminUserNamesDrawback	= StringUtil.getString(request, "adminUserNamesDrawback");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			String adminUserNamesQuality	= StringUtil.getString(request, "adminUserNamesQuality");
			String adminUserNamesProductModel= StringUtil.getString(request, "adminUserNamesProductModel");
			
			String content = handleSendContent(Long.valueOf(purchase_id),invoice,invoince_amount,invoince_currency,invoince_dot,
					drawback,rebate_rate,pur_need_tag,pur_quality,pur_product_model,true,request
					, adminUserNamesPurchaser, adminUserNamesDispatcher, adminUserNamesInvoice, adminUserNamesDrawback, 
					adminUserNamesTag, adminUserNamesQuality,adminUserNamesProductModel);
			//发票
			this.updatePurchaseProcedureInvoice(adid, adminUserIdsInvoice, "", Long.valueOf(purchase_id),
					ModuleKey.PURCHASE_ORDER, needPageInvoice, needMailInvoice, needMessageInvoice, request, false,
					ProcessKey.INVOICE, invoice, pur_invoice, employeeName, 
					content, invoince_amount, invoince_currency, invoince_dot, pur_amount, pur_currency, pur_dot);
			//退税
			this.updatePurchaseProcedureDrawback(adid, adminUserIdsDrawback, "", Long.valueOf(purchase_id),
					ModuleKey.PURCHASE_ORDER, needPageDrawback, needMailDrawback, needMessageDrawback, request, false, 
					ProcessKey.DRAWBACK, drawback, pur_drawback, employeeName, content, rebate_rate, pur_rebate_rate);
			//内部标签
			this.updatePurchaseProcedureTag(adid, adminUserIdsTag, "", Long.valueOf(purchase_id), 
					ModuleKey.PURCHASE_ORDER, needPageTag, needMailTag, needMessageTag, request, true,
					ProcessKey.PURCHASETAG, pur_need_tag, pur_need_tag, employeeName, content);
			//第三方标签
			this.updatePurchaseProcedureTagThird(adid, adminUserIdsTagThird, "", Long.valueOf(purchase_id), 
					ModuleKey.PURCHASE_ORDER, needPageTagThird, needMailTagThird, needMessageTagThird, request, true,
					ProcessKey.PURCHASE_THIRD_TAG, need_tag_third, pur_need_tag_third, employeeName, content);
			//质检要求
			this.updatePurchaseProcedureQuality(adid, adminUserIdsQuality, "", Long.valueOf(purchase_id),
					ModuleKey.PURCHASE_ORDER, needPageQuality, needMailQuality, needMessageQuality, request, true, 
					ProcessKey.QUALITY_INSPECTION, pur_quality, pur_quality, employeeName, content);
			//商品范例
			this.updatePurchaseProcedureProductModel(adid, adminUserIdsProductModel, "", Long.valueOf(purchase_id),
					ModuleKey.PURCHASE_ORDER, needPageProductModel, needMailProductModel, needMessageProductModel, request, true, 
					ProcessKey.PURCHASE_RODUCTMODEL, pur_product_model, pur_product_model, employeeName, content);
			//采购和调度任务和日志
			String adminUserIdsPurchaser= StringUtil.getString(request, "adminUserIdsPurchaser");//采购员
//			String adminUserIdsDispatcher=StrUtil.getString(request, "adminUserIdsDispatcher");//调度人员
			this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), "", 
					adminLoggerBean.getAdid(),adminUserIdsPurchaser, "",
					Long.valueOf(purchase_id), ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
					content,
					2, 2, 2, request, true, ProcessKey.PURCHASE_PURCHASERS, 1, employeeName, "", false);
//			this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), "", 
//					adminLoggerBean.getAdid(), adminUserIdsDispatcher, "",
//					Long.valueOf(purchase_id), ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id, 
//					content,
//					2, 2, 2, request, true, ProcessKey.PURCHASE_DISPATCHER, 1, employeeName, "", false);
			
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProdures",log);
		}
	}
	
	/**
	 * 删除日志
	 */
	private void deletePurchaseScheduleById(long purchase_id, int moduleId, int processType, int activity, String employeeName, String logContent, long adid, HttpServletRequest request) throws Exception{
		try 
		{
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, moduleId, processType);
			if(null != schedule){
				long scheduleId	= schedule.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleId, request);
				this.addPurchasefollowuplogs(processType, activity, purchase_id, logContent, adid, employeeName);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteTransportScheduleById",log);
		}
	}
	
	private void updatePurchaseProcedureInvoice(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content,
			double invoince_amount, int invoince_currency, double invoince_dot,
			double pur_amount, int pur_currency, double pur_dot
		)throws Exception
	{
		try 
		{
			InvoiceKey invoiceKey = new InvoiceKey();
			CurrencyKey currencyKey = new CurrencyKey();
			String selfLogContent = "[";
			//如果原来为不需要或者原来未填，现在需要，加任务
			if((0 == pur_activity && InvoiceKey.INVOICE == activity) || (InvoiceKey.NOINVOICE == pur_activity && InvoiceKey.INVOICE == activity))
			{
				selfLogContent += invoiceKey.getInvoiceById(activity)+"],开票金额："+invoince_amount
								+  " "+currencyKey.getCurrencyById(invoince_currency+"")
								+  ",票点："+invoince_dot+"%";
				this.addPurchaseProcedureInvoice(adid, executePersonIds, joinPersonIds, purchase_id,
						moduleId, page, mail, message, request, isNow, processType, activity, employeeName, content, 
						invoince_amount, invoince_currency, invoince_dot, 2);
			}
			else if(InvoiceKey.INVOICE == pur_activity && InvoiceKey.NOINVOICE == activity)
			{
				selfLogContent += invoiceKey.getInvoiceById(activity)+"]";
				this.deletePurchaseScheduleById(purchase_id, moduleId, processType, activity, employeeName, selfLogContent, adid, request);
			}
			else if(InvoiceKey.INVOICE == pur_activity && InvoiceKey.INVOICE == activity)
			{
				selfLogContent += invoiceKey.getInvoiceById(activity)+"],开票金额："+invoince_amount
				+  " "+currencyKey.getCurrencyById(invoince_currency+"")
				+  ",票点："+invoince_dot+"%";
				boolean isChange = false;
				if(invoince_amount != pur_amount || invoince_currency != pur_currency || invoince_dot != pur_dot)
				{
					isChange = true;
				}
				this.updatePurchaseScheduleOneProdure("", "", adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[发票流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, isChange
						);
			}
			else if(InvoiceKey.INVOICING == pur_activity || InvoiceKey.FINISH == pur_activity)
			{
				selfLogContent += invoiceKey.getInvoiceById(pur_activity)+"]";
				this.updatePurchaseScheduleOneProdure("", "", adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[发票流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureInvoice",log);
		}
	}
	
	private void updatePurchaseProcedureDrawback(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content,
			double rebate_rate, double pur_rebate_rate
		)throws Exception
	{
		try 
		{
			DrawbackKey drawbackKey = new DrawbackKey();
			String selfLogContent = "[";
			//如果原来为不需要或者原来未填，现在需要，加任务
			if((0 == pur_activity && DrawbackKey.DRAWBACK == activity) || (DrawbackKey.NODRAWBACK == pur_activity && DrawbackKey.DRAWBACK == activity))
			{
				selfLogContent += drawbackKey.getDrawbackById(activity)+"],退税率:"+rebate_rate+"%";
				this.addPurchaseProcedureDrawback(adid, executePersonIds, joinPersonIds, purchase_id, 
						moduleId, page, mail, message, request, isNow, 
						processType, activity, employeeName, selfLogContent, rebate_rate, 2);
			}
			else if(DrawbackKey.DRAWBACK == pur_activity && DrawbackKey.NODRAWBACK == activity)
			{
				selfLogContent += drawbackKey.getDrawbackById(activity)+"]";
				this.deletePurchaseScheduleById(purchase_id, moduleId, processType, activity, employeeName, selfLogContent, adid, request);
			}
			else if(DrawbackKey.DRAWBACK == pur_activity && DrawbackKey.DRAWBACK == activity)
			{
				selfLogContent += drawbackKey.getDrawbackById(activity)+"],退税率:"+rebate_rate+"%";
				boolean isChange = false;
				if(rebate_rate != pur_rebate_rate)
				{
					isChange = true;
				}
				this.updatePurchaseScheduleOneProdure("", "", adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[退税流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent,isChange
						);
			}
			else if(DrawbackKey.DRAWBACKING == pur_activity || DrawbackKey.FINISH == pur_activity)
			{
				selfLogContent += drawbackKey.getDrawbackById(pur_activity)+"]";
				this.updatePurchaseScheduleOneProdure("", "", adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[退税流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureDrawback",log);
		}
	}
	
	private void updatePurchaseProcedureTag(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content
		)throws Exception
	{
		try 
		{
			String selfLogContent = "";
			TDate endTime = new TDate();
			endTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period")));
			//如果原来为不需要或者原来未填，现在需要，加任务
			if(TransportTagKey.TAG == pur_activity)
			{
				selfLogContent += "[制签中]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), endTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[内部标签流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
			else if(TransportTagKey.FINISH == pur_activity)
			{
				selfLogContent += "[制签完成]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), endTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[内部标签流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureTag",log);
		}
	}
	
	private void updatePurchaseProcedureTagThird(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content
		)throws Exception
	{
		try 
		{
			String selfLogContent = "";
			TDate endTime = new TDate();
			endTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period")));
			//如果原来为不需要或者原来未填，现在需要，加任务
			if((0 == pur_activity && TransportTagKey.TAG == activity) || (TransportTagKey.NOTAG == pur_activity && TransportTagKey.TAG == activity))
			{
				selfLogContent += "[制签中]";
				this.addPurchaseProcedureTag(adid, executePersonIds, joinPersonIds, purchase_id, 
						moduleId, page, mail, message, request, isNow,
						processType, activity, employeeName, content, 2);
			}
			else if(TransportTagKey.TAG == pur_activity && TransportTagKey.NOTAG == activity)
			{
				selfLogContent += "[无需制签]";
				this.deletePurchaseScheduleById(purchase_id, moduleId, processType, activity, employeeName, selfLogContent, adid, request);
			}
			else if(TransportTagKey.TAG == pur_activity && TransportTagKey.TAG == activity)
			{
				selfLogContent += "[制签中]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), endTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[第三方标签流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
			else if(TransportTagKey.FINISH == pur_activity)
			{
				selfLogContent += "[制签完成]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), endTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[第三方标签流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureTag",log);
		}
	}
	
	
	private void updatePurchaseProcedureQuality(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content
		)throws Exception
	{
		try 
		{
			QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();
			String selfLogContent = "[";
			TDate qualityEndTime = new TDate();
			qualityEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_quality_period")));
			
			//如果原来为不需要或者原来未填，现在需要，加任务
			if(QualityInspectionKey.NEED_QUALITY == pur_activity)
			{
				selfLogContent += qualityInspectionKey.getQualityInspectionById(activity)+"]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), qualityEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[质检要求流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
			else if(QualityInspectionKey.FINISH == pur_activity)
			{
				selfLogContent += qualityInspectionKey.getQualityInspectionById(pur_activity)+"]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), qualityEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[质检要求流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureQuality",log);
		}
	}
	
	private void updatePurchaseProcedureProductModel(
			long adid, String executePersonIds,
			String joinPersonIds, long purchase_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String employeeName, String content
		)throws Exception
	{
		try 
		{
			String selfLogContent = "";
			TDate pmEndTime = new TDate();
			pmEndTime.addDay(Integer.parseInt(systemConfig.getStringConfigValue("purchase_product_model")));
			
			//如果原来为不需要或者原来未填，现在需要，加任务
			if(PurchaseProductModelKey.NEED_PRODUCT_MODEL == pur_activity)
			{
				selfLogContent += "[商品范例上传中]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), pmEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[商品范例流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
			else if(PurchaseProductModelKey.FINISH == pur_activity)
			{
				selfLogContent += "[商品范例上传完成]";
				this.updatePurchaseScheduleOneProdure(DateUtil.NowStr(), pmEndTime.formatDate("yyyy-MM-dd HH:mm:ss"), adid,
						executePersonIds, joinPersonIds, purchase_id,
						ModuleKey.PURCHASE_ORDER, "采购单:"+purchase_id+"[商品范例流程]", content,
						page, mail, message, request, isNow, processType,
						activity, employeeName, selfLogContent, false
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchaseProcedureProductModel",log);
		}
	}
	/**
	 * 获取某个流程的相关人的IDs
	 * @param transport_id
	 * @param moduleId
	 * @param processType
	 * @return
	 * @throws Exception
	 */
	private String getSchedulePersonIdsByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception{
		try 
		{
			DBRow[] persons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(transport_id), moduleId, processType);
			String personsIds = "";
			if(persons.length > 0){
				personsIds += persons[0].getString("schedule_execute_id");
				for (int i = 1; i < persons.length; i++) {
					personsIds += ","+persons[i].getString("schedule_execute_id");
				}
			}
			return personsIds;
		} 
		catch (Exception e) {
			throw new SystemException(e,"getSchedulePersonIdsByTransportIdAndType",log);
		}
		
	}
	/**
	 * 获取某个流程的相关人Names
	 * @param transport_id
	 * @param moduleId
	 * @param processType
	 * @return
	 * @throws Exception
	 */
	private String getSchedulePersonNamesByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception{
		try 
		{
			DBRow[] persons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(transport_id), moduleId, processType);
			String personNames = "";
			if(persons.length > 0){
				personNames += persons[0].getString("employe_name");
				for (int i = 1; i < persons.length; i++) {
					personNames += ","+persons[i].getString("employe_name");
				}
			}
			return personNames;
		} 
		catch (Exception e) {
			throw new SystemException(e,"getSchedulePersonNamesByTransportIdAndType",log);
		}
	}
	
	
	/**
	 * 获得采购单全部货款（单体价格X采购数量）
	 * @param purchase_id
	 * @return 已四舍五入
	 * @throws Exception
	 */
	public double getPurchasePrice(long purchase_id)
		throws Exception
	{
		try 
		{
			return fpm.getPurchasePrice(purchase_id);
		} catch (Exception e) {
			throw new SystemException(e,"getPurchasePrice",log);
		}
	}
	public void setFpm(FloorPurchaseMgr fpm) 
	{
		this.fpm = fpm;
	}

	public void setPm(FloorProductMgr pm) {
		this.pm = pm;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) {
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}

	public void setSupplierMgrTJH(SupplierMgrIFaceTJH supplierMgrTJH) {
		this.supplierMgrTJH = supplierMgrTJH;
	}
	
	public void setFloorPurchaseMgrLL(FloorPurchaseMgrLL floorPurchaseMgrLL) {
		this.floorPurchaseMgrLL = floorPurchaseMgrLL;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setFloorPurchaseMgrZyj(FloorPurchaseMgrZyj floorPurchaseMgrZyj) {
		this.floorPurchaseMgrZyj = floorPurchaseMgrZyj;
	}

	public void setFloorStorageCatalogMgrZyj(
			FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj) {
		this.floorStorageCatalogMgrZyj = floorStorageCatalogMgrZyj;
	}

	public void setFloorScheduleMgrZr(FloorScheduleMgrZr floorScheduleMgrZr) {
		this.floorScheduleMgrZr = floorScheduleMgrZr;
	}
	
		public void setPurchaseMgrZyj(PurchaseMgrZyjIFace purchaseMgrZyj) {
		this.purchaseMgrZyj = purchaseMgrZyj;
	}

		public void setApplyTransferMgrZZZ(ApplyTransferMgrIFace applyTransferMgrZZZ) {
			this.applyTransferMgrZZZ = applyTransferMgrZZZ;
		}
}
