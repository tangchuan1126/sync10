package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorProductStoreEditLogMgrZJ;
import com.cwc.app.iface.zj.ProductStoreEditLogMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ProductStoreEditLogMgrZJ implements ProductStoreEditLogMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStoreEditLogMgrZJ floorProductStoreEditLogMgrZJ;
	private FloorProductMgr floorProductMgr;
	
	/**
	 * 根据商品ID，仓库ID获得库存信息
	 */
	public void addProductStoreLogForPcidAndPsid(long pc_id, long ps_id,AdminLoginBean adminLoggerBean, int edit_reason) 
		throws Exception 
	{
		try 
		{
			DBRow product_store = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pc_id);
			
			
			DBRow product_store_edit = new DBRow();
			
			product_store_edit.add("pid",product_store.get("pid",0l));
			product_store_edit.add("pc_id",product_store.get("pc_id",0l));
			product_store_edit.add("cid",product_store.get("cid",0l));
			product_store_edit.add("store_count",product_store.get("store_count",0f));
			product_store_edit.add("storage_unit_price",product_store.get("storage_unit_price",0d));
			product_store_edit.add("storage_unit_freight",product_store.get("storage_unit_freight",0d));
			
			product_store_edit.add("edit_adid",adminLoggerBean.getAdid());
			product_store_edit.add("edit_reason",edit_reason);
			product_store_edit.add("post_date",DateUtil.NowStr());
			
			floorProductStoreEditLogMgrZJ.addProductStoreEditLog(product_store_edit);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductStoreLogForPcidAndPsid",log);
		}
		
	}

	/**
	 * 根据PID获得仓库信息
	 */
	public void addProductStoreLogForPid(long pid,AdminLoginBean adminLoggerBean, int edit_reason) 
		throws Exception 
	{
		try 
		{
			DBRow product_store = floorProductMgr.getDetailProductStorageByPid(pid);
			
			DBRow product_store_edit = new DBRow();
			
			product_store_edit.add("pid",product_store.get("pid",0l));
			product_store_edit.add("pc_id",product_store.get("pc_id",0l));
			product_store_edit.add("cid",product_store.get("cid",0l));
			product_store_edit.add("store_count",product_store.get("store_count",0f));
			product_store_edit.add("storage_unit_price",product_store.get("storage_unit_price",0d));
			product_store_edit.add("storage_unit_freight",product_store.get("storage_unit_freight",0d));
			
			product_store_edit.add("edit_adid",adminLoggerBean.getAdid());
			product_store_edit.add("edit_reason",edit_reason);
			product_store_edit.add("post_date",DateUtil.NowStr());
			
			floorProductStoreEditLogMgrZJ.addProductStoreEditLog(product_store_edit);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductStoreLogForPid",log);
		}
	}

	public void setFloorProductStoreEditLogMgrZJ(
			FloorProductStoreEditLogMgrZJ floorProductStoreEditLogMgrZJ) {
		this.floorProductStoreEditLogMgrZJ = floorProductStoreEditLogMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

}
