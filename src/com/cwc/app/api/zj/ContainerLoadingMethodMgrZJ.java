package com.cwc.app.api.zj;

import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

import sysu.yujian.containerloading.ContainerLoading;
import sysu.yujian.containerloading.ExecutionResult;
import sysu.yujian.containerloading.Instance;
import sysu.yujian.containerloading.ProblemLoader;
import sysu.yujian.containerloading.Solution;

public class ContainerLoadingMethodMgrZJ {
	
	private FloorProductMgr floorProductMgr;
	static Logger log = Logger.getLogger("ACTION");
	/**
	 * 装箱算法
	 * @param boxLength 外箱长
	 * @param boxWidth	外箱宽
	 * @param boxHeigth	外箱高
	 * @param version	使用算法 0-3
	 * @param products	传入的计算数据
	 * @param makefile	是否生成装箱方式文件
	 * @return
	 * @throws Exception
	 */
	public ContainerLoadingMethodMgrZJ(FloorProductMgr floorProductMgr) 
	{
		this.floorProductMgr = floorProductMgr;
	}
	
	public int packedCount(int boxLength,int boxWidth,int boxHeigth,int version,String products,boolean makefile)
		throws Exception
	{
		int processedNumber = 1;
	    int searchTime = 500;
	    int blockType = 2;
	    //	     4 1 60 1 30 1 20 1 5 2 80 1 55 1 38 1 4 3 90 1 55 1 40 1 5 4 20 1 30 1 40 1 6
	    int packeds = 0;
	    
		ProblemLoader problemLoader = new ProblemLoader(" 1 1 999 "+boxLength+" "+boxWidth+" "+boxHeigth+" "+products);

		File resultDir = new File("result");
		resultDir.mkdirs();
	     
		double totalUtilization = 0D;
		double totalTime = 0D;

	    int processed = 0;
	      
	    Instance instance = problemLoader.getNextInstance();

	    ContainerLoading solver = new ContainerLoading(instance);
	    ExecutionResult exeResult = solver.solve(searchTime, blockType, version);
	    Solution solution = exeResult.solution;
	    
	    //if (solution == null) continue;
	    totalTime += solver.excutionTime * 0.001D;
	    totalUtilization += solution.getUtilization();

	    if(makefile) 
	    {
			File dir = new File(resultDir, "BR" + System.currentTimeMillis() + "/");
			dir.mkdirs();
			File nbFile = new File(dir, "instance" + System.currentTimeMillis() + ".nb");
			solution.draw(nbFile.getCanonicalPath());
			FileWriter fw = new FileWriter(new File(dir, "instance"
					+ instance.getID() + ".txt"));
			fw.write(solution.toString());
			fw.close();
		}
	    
		packeds = solution.getPlacedCuboid().size()-1;
//		//system.out.println("=============================================================");
//	    //system.out.println("boxLength:"+boxLength+"boxWidth:"+boxWidth+"boxHeigth"+boxHeigth+"version:"+version);
//	    //system.out.println("Packed Boxes: "+packeds);
	    
	    return packeds;
	}
	
	//边长检查是否可以装箱
	public boolean sidePacked(int boxLength,int boxWidth,int boxHeigth,DBRow[] productCart)
		throws Exception
	{
		try 
		{
			if(productCart==null||productCart.length==0)
			{
				throw new Exception("ProductCart NULL");
			}
			
			for (int i = 0; i < productCart.length; i++) 
			{
				DBRow product = floorProductMgr.getDetailProductByPcid(productCart[i].get("cart_pid",0l));
				
				int lenght = (int)product.get("length",0f);
				int width = (int)product.get("width",0f);
				int height = (int)product.get("heigth",0f);
					
				//不确定商品数据上的长宽高按照最长，中长，最短排序,冒泡排序处理下
				int[] array = {lenght,width,height};
				
				for (int j = 0; j < array.length; j++) 
				{
					for (int j2 = j+1; j2 <array.length; j2++)
					{
						if(array[j]<array[j2])
						{
							int temp = array[j];
							
							array[j] = array[j2];
							
							array[j2] = temp;
						}
					}
				}
				
				if(productCart.length==1&&productCart[0].get("cart_quantity",0f)>0&&productCart[0].get("cart_quantity",0f)<=1)//只计算一种货物，只有一件，不走glas算法，自己判断(抄单有抄0.5的可能)
				{
					//盒子的长宽高分有一项小于任意商品的长宽高
					if(boxLength<array[0]||boxWidth<array[1]||boxHeigth<array[2])
					{
						return false;//只有一种货物，数量只有一件，装不下就真是装不下，不用往下执行，返回false;
					}
					else
					{
						return true;//只有一种货物，数量只有一件，装下了
					}
				}
				else//货物超过1种或同种货物件数大于1件
			    {	
					//盒子的长宽高分有一项小于任意商品的长宽高
					if(boxLength<array[0]||boxWidth<array[1]||boxHeigth<array[2])
					{
						return false;//因为是多种货物，有一件货物超过箱子长宽高都认为装不下，不用继续执行算法
					}
			    }
			}
				
			return true;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"sidePacked",log);
		}
	}
	
	public boolean volumPacked(int boxLength,int boxWidth,int boxHeigth,DBRow[] productCart)
		throws Exception
	{
		float boxVolum = boxLength*boxWidth*boxHeigth;
		
		float sumVolum = 0;
		for (int i = 0; i < productCart.length; i++) 
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(productCart[i].get("cart_pid",0l));
			
			int lenght = (int)product.get("length",0f);
			int width = (int)product.get("width",0f);
			int height = (int)product.get("heigth",0f);
			
			sumVolum += lenght*width*height*productCart[i].get("cart_quantity",0f);
		}
		
		if(boxVolum>sumVolum)
		{
			return true;//盒子体积大于总体积
		}
		else
		{
			return false;//盒子体积小于总体积
		}
	}
	
	/**
	 * 香港装箱算法
	 * @param boxLength
	 * @param boxWidth
	 * @param boxHeigth
	 * @param version
	 * @param productCart
	 * @param makefile
	 * @return
	 * @throws Exception
	 */
	public boolean packingAlgorithm(int boxLength,int boxWidth,int boxHeigth,int version,DBRow[] productCart,boolean makefile)
		throws Exception
	{
		//int processedNumber = 1;
		int searchTime = 500;
		int blockType = 2;
		//	     4 1 60 1 30 1 20 1 5 2 80 1 55 1 38 1 4 3 90 1 55 1 40 1 5 4 20 1 30 1 40 1 6
		int packeds = 0;
		
		int sum = 0;
		String productString = "";
		productString+="  "+(productCart.length+1)+"  1 1 1 1 1 1 1 1	";//商品种类数量与防止只有一种商品
		
		for (int i = 0; i < productCart.length; i++) 
		{
			DBRow product = floorProductMgr.getDetailProductByPcid(productCart[i].get("cart_pid",0l));
			
			int lenght = (int)product.get("length",0f);
			int width = (int)product.get("width",0f);
			int height = (int)product.get("heigth",0f);
			
			//不确定商品数据上的长宽高按照最长，中长，最短排序,冒泡排序处理下
			int[] array = {lenght,width,height};
			
			for (int j = 0; j < array.length; j++) 
			{
				for (int j2 = j+1; j2 <array.length; j2++)
				{
					if(array[j]<array[j2])
					{
						int temp = array[j];
						
						array[j] = array[j2];
						
						array[j2] = temp;
					}
				}
			}
			
			int one_item_count = (int)(productCart[i].get("cart_quantity",0f)*2);
			 
			if(one_item_count%2==0)//抄单商品中没有0.5结尾
			{
				 sum+=(int)(productCart[i].get("cart_quantity",0f));
				 //productString+= "  	"+(i+2)+" "+(int)(product.get("length",0f))+" 1 "+(int)(product.get("width",0f))+" 1 "+(int)(product.get("heigth",0f))+" 1 "+(int)(productCart[i].get("cart_quantity",0f))+" ";
				 productString+= "  	"+(i+2)+" "+(int)(array[0])+" 1 "+(int)(array[1])+" 1 "+(int)(array[2])+" 1 "+(int)(productCart[i].get("cart_quantity",0f))+" ";
			}
			else//商品数量包含0.5，最长边除以2，商品打算装箱
			{
				 sum+=one_item_count;
				 productString+= "  	"+(i+2)+" "+(int)(array[0]/2)+" 1 "+(int)(array[1])+" 1 "+(int)(array[2])+" 1 "+one_item_count+" ";
			}
		}
		
		ProblemLoader problemLoader = new ProblemLoader(" 1 1 999 "+ boxLength + " " + boxWidth + " " + boxHeigth + " "+ productString);
		File resultDir = new File("result");
		resultDir.mkdirs();
		double totalUtilization = 0D;
		double totalTime = 0D;
//		int processed = 0;
		Instance instance = problemLoader.getNextInstance();
		ContainerLoading solver = new ContainerLoading(instance);
		ExecutionResult exeResult = solver.solve(searchTime, blockType,version);
		Solution solution = exeResult.solution;
		//if (solution == null) continue;
		totalTime += solver.excutionTime * 0.001D;
		totalUtilization += solution.getUtilization();
		if (makefile) {
			File dir = new File(resultDir, "BR"
					+ System.currentTimeMillis() + "/");
			dir.mkdirs();
			File nbFile = new File(dir, "instance"
					+ System.currentTimeMillis() + ".nb");
			solution.draw(nbFile.getCanonicalPath());
			FileWriter fw = new FileWriter(new File(dir, "instance"
					+ instance.getID() + ".txt"));
			fw.write(solution.toString());
			fw.close();
		}
		packeds = solution.getPlacedCuboid().size() - 1;
		
		if(packeds==sum)
		{
			return true;//箱子可以装下
		}
		else
		{
			return false;//箱子装不下
		}
	}
	
	public boolean packed(int boxLength,int boxWidth,int boxHeigth,int version,DBRow[] productCart,boolean makefile)
		throws Exception
	{
		try 
		{
			String productString = "";
			productString+="  "+(productCart.length+1)+"  1 1 1 1 1 1 1 1	";//商品种类数量与防止只有一种商品
			
			if(sidePacked(boxLength, boxWidth, boxHeigth, productCart))//检查每种商品边长是否有超过箱子的
			{
				if(volumPacked(boxLength, boxWidth, boxHeigth, productCart))//总商品体积是否超过箱子体积
				{
					if(productCart.length==1&&productCart[0].get("cart_quantity",0f)==1)
					{
						return true;
					}
					else
					{
						return packingAlgorithm(boxLength, boxWidth, boxHeigth, version, productCart, makefile);//返回装箱算法计算的装下与否结果
					}
					
					
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			return false;
		}
		
	}
	
	public static void main(String[] args)
	{
		
		////system.out.println((1.5/0.5)%2);
		////system.out.println((0.5%0.5)%2);
//		try 
//		{
//			ContainerLoadingMgrZJ c = new ContainerLoadingMgrZJ();
//			c.packedCount(100,100,100,1,"  4 " +
//					"	1 60 1 30 1 20 1 5 " +
//					"	2 80 1 55 1 38 1 4 " +
//					"	3 90 1 55 1 40 1 5 " +
//					"	4 20 1 30 1 40 1 6",false);
//		}
//		catch (Exception e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}


	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
}
