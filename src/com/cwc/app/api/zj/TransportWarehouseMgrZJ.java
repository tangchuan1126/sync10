package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorTransportWarehouseMgrZJ;
import com.cwc.app.iface.zj.TransportWarehouseMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class TransportWarehouseMgrZJ implements TransportWarehouseMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportWarehouseMgrZJ floorTransportWarehouseMgrZJ;

	/**
	 * 伪转运入库提交
	 * @param dbs
	 * @param transport_id
	 * @throws Exception
	 */
	public void addTransportWareHouseSub(DBRow[] dbs, long transport_id,String machine_id)
		throws Exception 
	{
		try 
		{
			delTransportWareHouseByTransportId(transport_id,machine_id);
			for (int i = 0; i < dbs.length; i++) 
			{
				floorTransportWarehouseMgrZJ.addTransportWareHouse(dbs[i]);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	
	/**
	 * 获得转运交货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportByTransportId(long transport_id)
		throws Exception 
	{
		try 
		{
			return floorTransportWarehouseMgrZJ.compareTransportByTransportId(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 转运单发货与收货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportOutWareByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportWarehouseMgrZJ.compareTransportOutWareByTransportId(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compareTransportOutWareByTransportId",log);
		}
	}
	
	/**
	 * 转运单收货数量比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] transportReceiveCompareCount(long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportWarehouseMgrZJ.transportReceiveCompareCount(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compareTransportOutWareByTransportId",log);
		}
	}
	
	
	

	/**
	 * 删除转运伪入库
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransportWareHouseByTransportId(long transport_id,String machine_id)
		throws Exception 
	{
		try 
		{
			floorTransportWarehouseMgrZJ.delTransportWareHouseByTransportId(transport_id,machine_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 获得转运交货
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportWareHouseByTransportId(long transport_id)
		throws Exception 
	{
		try
		{
			return floorTransportWarehouseMgrZJ.getTransportWarehouse(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportByTransportId",log);
		}
	}
	
	/**
	 * 验证同张单据 不同机器序列号相同
	 */
	public DBRow[] getTransportWareHouseBySNNotMachine(String sn,String machine_id,long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportWarehouseMgrZJ.getTransportWareHouseBySNNotMachine(sn, machine_id, transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportWareHouseBySNNotMachine",log);
		}
	}
	
	/**
	 * 转运单收货放货差异
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportWareHousePutLocationCompare(long transport_id)
		throws Exception
	{
		try 
		{
			return floorTransportWarehouseMgrZJ.transportReceivePutLocationCompare(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTransportWareHousePutLocationCompare",log);
		}
	}

	public void setFloorTransportWarehouseMgrZJ(FloorTransportWarehouseMgrZJ floorTransportWarehouseMgrZJ) {
		this.floorTransportWarehouseMgrZJ = floorTransportWarehouseMgrZJ;
	}
}
