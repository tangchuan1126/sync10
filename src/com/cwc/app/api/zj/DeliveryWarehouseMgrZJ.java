package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;

import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zj.FloorDeliveryWarehouseMgrZJ;
import com.cwc.app.iface.zj.DeliveryWarehouseMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.TUpload;

public class DeliveryWarehouseMgrZJ implements DeliveryWarehouseMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorDeliveryWarehouseMgrZJ floorDeliveryWarehouseMgrZJ;
	private FloorDeliveryMgrZJ floorDeliveryMgrZJ;
	/**
	 * 伪交货，文件形式
	 * @param request
	 * @throws Exception
	 */
	public DBRow[] addCompareDeliveryWareHouse(String filename,long delivery_order_id)
		throws Exception 
	{
		try 
		{
			
			DBRow[] deliveryWarehouses = excelDBRow(filename);
			
			DBRow delivery_order = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);
			
			String delivery_order_number = delivery_order.getString("delivery_order_number");
			
			addDeliveryWareHouseSub(deliveryWarehouses,delivery_order_number);
			
			DBRow[] compare = compareDeliveryByDeliveryOrderNumber(delivery_order_number);
			
			return (compare);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 添加伪交货
	 * @param dbs
	 * @throws Exception
	 */
	public void addDeliveryWareHouseSub(DBRow[] dbs,String delivery_order_number)
		throws Exception
	{
		try 
		{
			delDeliveryWareHouseByDeliveryOrderNumber(delivery_order_number);
			for (int i = 0; i < dbs.length; i++) 
			{
				floorDeliveryWarehouseMgrZJ.addDeliveryWareHouse(dbs[i]);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 根据交货单号通过伪交货比较交货
	 * @param delivery_order_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareDeliveryByDeliveryOrderNumber(String delivery_order_number) 
		throws Exception 
	{
		try 
		{
			DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDeliveryOrderByDeliveryNumber(delivery_order_number);
			return floorDeliveryWarehouseMgrZJ.compareDeliveryByDeliveryOrderNumber(deliveryOrder.get("delivery_order_id",0l));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	
	/**
	 * 根据交货单号清除伪交货
	 * @param delivery_order_number
	 * @throws Exception
	 */
	public void delDeliveryWareHouseByDeliveryOrderNumber(String delivery_order_number) throws Exception
	{
		try 
		{
			floorDeliveryWarehouseMgrZJ.delDeliveryWareHouseByDeliveryOrderNumber(delivery_order_number);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	
	/**
	 * 根据交货单ID获得虚拟交货数据
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryByDeliveryOrderId(long delivery_order_id)
		throws Exception 
	{
		try 
		{
			return (floorDeliveryWarehouseMgrZJ.getDeliveryByDeliveryOrderId(delivery_order_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	

	/**
	 * 文件上传入库
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public String[] uploadIncoming(HttpServletRequest request)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String[] msg=new String[2];
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long delivery_order_id = Long.parseLong(upload.getRequestRow().getString("delivery_order_id"));
			
			msg[1] = String.valueOf(delivery_order_id);
				
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg[0] = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public DBRow[] excelDBRow(String filename)
	throws Exception
	{
		String url = Environment.getHome() + "administrator/upl_excel_tmp/" + filename;
		
		String[] content = FileUtil.readFile2Array(url);
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		HashMap<Integer,String> fieldNames = new HashMap<Integer,String>();
		fieldNames.put(0,"product_barcod");
		fieldNames.put(1,"reap_count");
		fieldNames.put(2,"storage");
		
		
		
		
		InputStream is = new FileInputStream(url);
		Workbook rwb = Workbook.getWorkbook(is);

		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数

		  for(int i=1;i<rsRows;i++)
		  {
			  Pattern pattern = Pattern.compile("[\u4e00-\u9fa5]");
				Matcher matcher = pattern.matcher(content[i]);
				if(matcher.find())
				{
					continue;
				}
				DBRow pro = new DBRow();
				boolean result = false;//判断是否纯空行数据
				for (int j=0;j<rsColumns;j++)
				{
					if(fieldNames.containsKey(j))//预防多出列
					{
						pro.add(fieldNames.get(j),rs.getCell(j,i).getContents().trim());  //转换成DBRow数组输出,去掉空格
					}
			    
					ArrayList proName=pro.getFieldNames();//DBRow 字段名
					for(int p=0;p<proName.size();p++)
					{
						if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
						{
							result = true;
						}
					}
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
		  }
		  
		  return (al.toArray(new DBRow[0]));
	}

	public static void setLog(Logger log) {
		DeliveryWarehouseMgrZJ.log = log;
	}

	public void setFloorDeliveryWarehouseMgrZJ(
			FloorDeliveryWarehouseMgrZJ floorDeliveryWarehouseMgrZJ) {
		this.floorDeliveryWarehouseMgrZJ = floorDeliveryWarehouseMgrZJ;
	}

	public void setFloorDeliveryMgrZJ(FloorDeliveryMgrZJ floorDeliveryMgrZJ) {
		this.floorDeliveryMgrZJ = floorDeliveryMgrZJ;
	}

}
