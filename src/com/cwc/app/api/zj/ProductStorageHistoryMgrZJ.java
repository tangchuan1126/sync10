package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorProductStorageHistoryMgrZJ;
import com.cwc.app.iface.zj.ProductStorageHistoryMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProductStorageHistoryMgrZJ implements ProductStorageHistoryMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStorageHistoryMgrZJ floorProductStorageHistoryMgrZJ;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorProductMgr floorProductMgr;
	@Override
	public void saveProductStorageHistroy() 
		throws Exception
	{
		DBRow[] productStoreCatalog = floorCatalogMgr.getAllSendProductStoreCatalog();
		
		for (int i = 0; i < productStoreCatalog.length; i++) 
		{
			DBRow[] productStores = floorProductMgr.getProductStorages(productStoreCatalog[i].get("id",0l),0, null);
			
			for (int j = 0; j < productStores.length; j++) 
			{
				DBRow productStoreHistory = new DBRow();
				productStoreHistory.add("store_count",productStores[j].get("store_count",0f));
				productStoreHistory.add("ps_id",productStores[j].get("cid",0l));
				productStoreHistory.add("pc_id",productStores[j].get("pc_id",0l));
				productStoreHistory.add("damaged_count",productStores[j].get("damaged_count",0f));
				productStoreHistory.add("damaged_package_count",productStores[j].get("damaged_package_count",0f));
				productStoreHistory.add("post_date",DateUtil.NowStr());
				
				DBRow product = floorProductMgr.getDetailProductByPcid(productStores[j].get("pc_id",0l));
				productStoreHistory.add("unit_price",product.get("unit_price",0d));
				
				floorProductStorageHistoryMgrZJ.addProductStorageHistory(productStoreHistory);
			}
			
		}
	}
	
	/**
	 * 查询库存历史
	 */
	
	public DBRow[] getAllProductStorageHistory(PageCtrl pc) throws Exception 
	{
			try {
				return (floorProductStorageHistoryMgrZJ.getAllProductStorageHistory(pc));
			} catch (Exception e) {
				throw new SystemException(e,"getAllProductStorageHistory",log);
			}
	}
	/**
	 * 高级查询
	 */
	public DBRow[] getProductStorageHistoryByPsid(String st,String en,String p_name,long psId , PageCtrl pc)throws Exception
	{
		
		try {
			long pc_id = 0;
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product != null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return (floorProductStorageHistoryMgrZJ.getProductStorageHistoryByPsid(st,en,pc_id,psId,pc));
		} catch (Exception e) {
			throw new SystemException(e,"getProductStorageHistoryByPsid",log);
		}
    }
	/**
	 * 导出库存历史记录
	 */
	public String exportProductStorageHistory(HttpServletRequest request)
	throws WareHouseErrorException,Exception  
	{
		
		try{
				long productLine = StringUtil.getLong(request,"productLine");
				long psId=StringUtil.getLong(request,"psId");
				String store_date=StringUtil.getString(request,"store_date");
				
				DBRow [] exportProductStorageHistory=floorProductStorageHistoryMgrZJ.exportProductStorageHistory(productLine, psId, store_date);
				long [] productStorageHistorys = new long[exportProductStorageHistory.length];
				if(exportProductStorageHistory.length>0){
					XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product/ExportStorageHistoryModel.xlsx"));  
					XSSFSheet sheet= wb.getSheetAt(0);  
					XSSFCellStyle style = wb.createCellStyle();
					style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					style.setLocked(false); //创建样式
					style.setWrapText(true);
					
					XSSFFont  font =wb.createFont();
					font.setFontName("Arial");   
					font.setFontHeightInPoints((short)10);   
					font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
					
					XSSFCellStyle stylelock = wb.createCellStyle();
					stylelock.setLocked(true); 
					stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
					
					XSSFRow row = sheet.getRow(0); 
					for (int i = 0; i < exportProductStorageHistory.length; i++) 
					{
						row = sheet.createRow((int)i+1);
						sheet.setDefaultColumnWidth(26);
						DBRow productStorageHistory = exportProductStorageHistory[i];
						//设置excel列的值
						productStorageHistorys[i] = productStorageHistory.get("psh_id", 0l);
						row.createCell(0).setCellValue(productStorageHistory.getString("p_name"));
						row.getCell(0).setCellStyle(style);
						
						row.createCell(1).setCellValue(productStorageHistory.getString("name"));
						row.getCell(1).setCellStyle(style);
						
						row.createCell(2).setCellValue(productStorageHistory.get("store_count",0f));
						row.getCell(2).setCellStyle(style);
						
						row.createCell(3).setCellValue(productStorageHistory.get("unit_price",0.00));
						row.getCell(3).setCellStyle(style);
						
						row.createCell(4).setCellValue(productStorageHistory.get("damaged_count",0.0));
						row.getCell(4).setCellStyle(style);
						
						row.createCell(5).setCellValue(productStorageHistory.get("damaged_package_count",0.0));
						row.getCell(5).setCellStyle(style);
				
						row.createCell(6).setCellValue(productStorageHistory.getString("post_date").substring(0, 11));
						row.getCell(6).setCellStyle(style);
					}
					

					//写文件  
					String path = "upl_excel_tmp/export_ProductStorageHistory_"+store_date+".xlsx";
					OutputStream os= new FileOutputStream(Environment.getHome()+path);
					wb.write(os);  
					os.close();  
					return (path);
				}
				else
				{
					return "WareHouseErrorException";
				}
		}catch (WareHouseErrorException e) 
				{
					throw e;
			    }
				catch (Exception e) 
				{
					throw new SystemException(e,"exportProductStorageHistory",log);
			    }
    }
	

	
	public void setFloorProductStorageHistoryMgrZJ(
			FloorProductStorageHistoryMgrZJ floorProductStorageHistoryMgrZJ) {
		this.floorProductStorageHistoryMgrZJ = floorProductStorageHistoryMgrZJ;
	}
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


}
