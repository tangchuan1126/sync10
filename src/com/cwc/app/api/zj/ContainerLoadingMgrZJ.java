package com.cwc.app.api.zj;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorContainerLoadingMgrZJ;
import com.cwc.app.floor.api.zj.FloorContainerProductMgrZJ;
import com.cwc.app.iface.zj.ContainerLoadingMgrIFaceZJ;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ContainerLoadingMgrZJ implements ContainerLoadingMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorContainerLoadingMgrZJ floorContainerLoadingMgrZJ;
	private FloorContainerProductMgrZJ floorContainerProductMgrZJ;
	
	public HashMap<String, DBRow> getContainerLoadingParentList(long con_id)
		throws Exception 
	{
		try 
		{
			HashMap<String,DBRow> containerLoadingParent = new HashMap<String, DBRow>();
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
			
			DBRow containerLoading = floorContainerLoadingMgrZJ.getDetailContainerLoadingByCon(con_id);
			int container_type = containerLoading.get("container_type",0);
			long parent_con_id = containerLoading.get("parent_cont_id",0l);
			//containerLoadingParent.put(containerTypeKey.getContainerTypeKeyValue(container_type),containerLoading);
			
			while (parent_con_id!=0); 
			{
				containerLoading = floorContainerLoadingMgrZJ.getDetailContainerLoadingByCon(parent_con_id);
				container_type = containerLoading.get("container_type",0);
				parent_con_id = containerLoading.get("parent_cont_id",0l);
				containerLoadingParent.put(containerTypeKey.getContainerTypeKeyValue(container_type),containerLoading);
			}
			
			return containerLoadingParent;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getContainerLoadingParentList",log);
		}
		
	}
	
	public HashMap<String, DBRow[]> getContainerLoadingSonList(long parent_con_id) 
		throws Exception 
	{
		try 
		{
			HashMap<String,DBRow[]> containerLoadingSonList = new HashMap<String, DBRow[]>();
			ArrayList<DBRow> bLPList = new ArrayList<DBRow>();
			ArrayList<DBRow> iLPList = new ArrayList<DBRow>();
			
			DBRow containerLoading = floorContainerLoadingMgrZJ.getDetailContainerLoadingByCon(parent_con_id);
			
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
			
			DBRow[] containerLoadings = floorContainerLoadingMgrZJ.getDetailContainerLoadingByParent(parent_con_id);
			
			/*
			for (int i = 0; i < containerLoadings.length; i++) 
			{
				DBRow sonContainerLoading = containerLoadings[i];
				if(sonContainerLoading.get("container_type",0)==ContainerTypeKey.BLP) //如果是BLP加入BLP容器list
				{
					bLPList.add(sonContainerLoading);
					

					DBRow[] nextSonContainerLoads = floorContainerLoadingMgrZJ.getDetailContainerLoadingByParent(sonContainerLoading.get("con_id",0l));
						
					for (int j = 0; j < nextSonContainerLoads.length; j++) 
					{
						if (nextSonContainerLoads[j].get("container_type",0)==ContainerTypeKey.ILP)
						{
							iLPList.add(nextSonContainerLoads[j]);
						}
					}
				}
				else if(sonContainerLoading.get("container_type",0)==ContainerTypeKey.ILP) //如果是BLP加入ILP容器list 
				{
					iLPList.add(sonContainerLoading);
				}
			}
			if (bLPList.size()>0) 
			{
				containerLoadingSonList.put(containerTypeKey.getContainerTypeKeyValue(ContainerTypeKey.BLP),bLPList.toArray(new DBRow[0]));
			}
			if(iLPList.size()>0)
			{
				containerLoadingSonList.put(containerTypeKey.getContainerTypeKeyValue(ContainerTypeKey.BLP),bLPList.toArray(new DBRow[0]));
			}
			*///去掉ILP和BLP
			return containerLoadingSonList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getContainerLoadingSonList",log);
		}
	}

	public void setFloorContainerLoadingMgrZJ(
			FloorContainerLoadingMgrZJ floorContainerLoadingMgrZJ) {
		this.floorContainerLoadingMgrZJ = floorContainerLoadingMgrZJ;
	}

	public void setFloorContainerProductMgrZJ(
			FloorContainerProductMgrZJ floorContainerProductMgrZJ) {
		this.floorContainerProductMgrZJ = floorContainerProductMgrZJ;
	}

}
