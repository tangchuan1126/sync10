package com.cwc.app.api.zj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zr.BoxTypeMgrZr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.LPType.BLPHasExistException;
import com.cwc.app.exception.LPType.CLPHasExistException;
import com.cwc.app.exception.LPType.InnerContainerTypeNotFoundException;
import com.cwc.app.exception.LPType.LPTypeHasExistException;
import com.cwc.app.exception.android.ContainerTypeNotFoundException;
import com.cwc.app.exception.android.TitleNotFoundException;
import com.cwc.app.exception.product.CatalogNotExistException;
import com.cwc.app.exception.product.CatalogNotExistProductException;
import com.cwc.app.exception.product.ProductAttributeDifferentException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.proprietary.ProductCatalogTitleIsExistException;
import com.cwc.app.exception.proprietary.SKUTitleIsExistException;
import com.cwc.app.exception.proprietary.TitleHasExistException;
import com.cwc.app.exception.shipTo.CountryNotExistException;
import com.cwc.app.exception.shipTo.CountryProvinceNotExistException;
import com.cwc.app.exception.shipTo.ShipToHasExistException;
import com.cwc.app.exception.shipTo.ShipToNotExistException;
import com.cwc.app.exception.shipTo.StorageCatalogIsExistException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zj.FloorCountryMgrZJ;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.InitDataMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.GlobalKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.app.lucene.zyj.TitleIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.XmlUtil;

public class InitDataMgrZJ implements InitDataMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorCatalogMgr floorCatalogMgr;
	private FloorProductCodeMgrZJ floorProductCodeMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	private BoxTypeMgrZr boxTypeMgrZr;
	private FloorShipToMgrZJ floorShipToMgrZJ;
	private FloorCountryMgrZJ floorCountryMgrZJ;
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	private AdminMgrIFaceZJ adminMgrZJ;
	private FloorProductLineMgrTJH floorProductLineMgrTJH;
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	private	FloorProductStoreMgr productStoreMgr;
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	@Override
	public Map<String, DBRow[]> initData(HttpServletRequest request) throws Exception {
		
		try {
			
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			String file_name = StringUtil.getString(request,"file_name");
			
			String path = Environment.getHome() + "upl_imags_tmp/" + file_name;
			String file_type = file_name.substring(file_name.lastIndexOf("."));
			
			InputStream is = new FileInputStream(path);
			Workbook rwb;
			
			if (file_type.equals(".xls")) {
				
				rwb = new HSSFWorkbook(is);
			} else {
				
				rwb = new XSSFWorkbook(is);
			}
			
			//初始化商品分类
			ArrayList<DBRow> errorCatalogList = initCatalog(rwb);
			//初始化商品信息
			ArrayList<DBRow> errorProductList = initProduct(rwb, adminLoggerBean);
			//初始化Title信息
			ArrayList<DBRow> errorTitleList = initTitle(rwb);
			//初始化ShipTo
			ArrayList<DBRow> errorShipTo = initShipTo(rwb);
			//初始化ShipToAddress
			ArrayList<DBRow> errorShipToAddress = initShipToAddress(rwb);
			//初始化SKUTitle关系
			ArrayList<DBRow> errorSKUTitle = initSKUTitle(rwb);
			//初始化CategoryTitle关系
			ArrayList<DBRow> errorCategoryTitle = initCategoryTitle(rwb);
			//初始化ContainerType
			ArrayList<DBRow> errorContainerType = initContainerType(rwb);
			//初始化BLP
			ArrayList<DBRow> errorBLP = initBLP(rwb);
			//初始化CLP
			ArrayList<DBRow> errorCLP = initCLP(rwb);
			
			Map<String,DBRow[]> errorResult = new HashMap<String, DBRow[]>();
			errorResult.put("errorCatalog",errorCatalogList.toArray(new DBRow[0]));
			errorResult.put("errorProduct",errorProductList.toArray(new DBRow[0]));
			errorResult.put("errorTitle",errorTitleList.toArray(new DBRow[0]));
			errorResult.put("errorSKUTitle",errorSKUTitle.toArray(new DBRow[0]));
			errorResult.put("errorCategoryTitle",errorCategoryTitle.toArray(new DBRow[0]));
			errorResult.put("errorContainerType",errorContainerType.toArray(new DBRow[0]));
			errorResult.put("errorBLP",errorBLP.toArray(new DBRow[0]));
			errorResult.put("errorCLP",errorCLP.toArray(new DBRow[0]));
			errorResult.put("errorShipTo",errorShipTo.toArray(new DBRow[0]));
			errorResult.put("errorShipToAddress",errorShipToAddress.toArray(new DBRow[0]));
			
			String filePath = this.importErrorExcel(errorResult,rwb);
			DBRow errorFileName = new DBRow();
			errorFileName.add("file_path",filePath);
			errorResult.put("errorFile",new DBRow[]{errorFileName});
			
			return errorResult;
			
		} catch (Exception e) {
			throw new SystemException(e,"initData",log);
		}
	}
	
	/**
	 * 基础数据：Excel数据初始化
	 * @param Excel请求
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public String initDataExcelRequest(HttpServletRequest request) throws Exception {
		
		//用二进制流接收数据
		InputStream inputStream = request.getInputStream();
		
		String xml = getPost(inputStream);
		//xml = xml.replaceAll("\"","\\\"");
		DBRow row = XmlUtil.initDataXmlHasSpaceStringToDBRow(xml,new String[]{"Sheet","Account","Password"});
		//initDataXmlHasSpaceStringToDBRow   initDataXmlStringToDBRow
		
		String sheet  = row.getString("sheet");
		String account = row.getString("account");
		String password = row.getString("password");
		
		//验证登录
		DBRow login = adminMgrZJ.MachineLogin(account, password);
		
		String result = "";
		
		if(login != null){
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAccount(account);
			adminLoggerBean.setAdgid(login.get("adgid",0l));
			adminLoggerBean.setAdid(login.get("adid",0l));
			adminLoggerBean.setPs_id(login.get("ps_id",0l));
			adminLoggerBean.setEmploye_name(login.getString("employe_name"));
			adminLoggerBean.setDepartment((DBRow[]) login.getValue("departments"));
			
			adminLoggerBean.setCorporationId(login.get("corporation_id", 0));
			adminLoggerBean.setCorporationType(login.get("corporation_type", 0));
			
			switch (sheet) {
			
				case "Category":
					result = initCatalog(xml,adminLoggerBean);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
				case "Title":
					result = initTitle(xml,adminLoggerBean);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
				case "SKU-TITLE":
					result = initSKUCatalogTitle(xml, adminLoggerBean);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
				case "ShiptoAddress":
					result = initShipToAddress(xml);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
				case "Packaging Type":
					result = initContainerType(xml);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
				case "BoxCONFIGURATION(BLP)":
					result = initBLP(xml, login.get("ps_id",0l));
					result = " {\"info\":\""+result+"\"} ";
					break;
					
			    case "CLP Type":
					result = initCLP(xml, login.get("ps_id",0l), adminLoggerBean);
					result = " {\"info\":\""+result+"\"} ";
					break;
					
			    case "Login":
			    	result = initReturnData(adminLoggerBean);
			    	break;
			}
		} else {
			result = "LoginError";
			result = " {\"info\":\""+result+"\"} ";
		}
		
		return result;
	}
	
	public String initReturnData(AdminLoginBean adminLoggerBean) throws Exception {
		
		DBRow result = new DBRow();
		//返回packaging
		DBRow[] packaging = floorContainerMgrZYZ.getContainerTypeForExcel();
		result.put("packaging", packaging);
		
		//返回customer
		if(adminLoggerBean.getCorporationType()==1){
			
			DBRow customer = floorContainerMgrZYZ.getCustomerById(adminLoggerBean.getCorporationId());
			result.put("customer", customer.get("customer_id",""));
		}
		
		return StringUtil.convertDBRowsToJsonString(result);
	}
	/**
	 * 基础数据：初始化导入商品分类Excel
	 * @param rwb
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public String initCatalog(String xml,AdminLoginBean adminLoggerBean) throws Exception {
		
		//最多三级分类
		long MAXLEVEL = 3L;
		//String[] nodeNames = {"Level1","Level2","Level3","Level4"};
		String[] nodeNames = {"Level1","Level2","Level3"};
		
		DBRow catalog = XmlUtil.initDataXmlStringToDBRowCategory(xml, nodeNames);
		
		String level1 = catalog.getString("level1").trim();
		String level2 = catalog.getString("level2").trim();
		String level3 = catalog.getString("level3").trim();
		//String level4 = catalog.getString("level4").trim();
		
		//String param[] = {level1,level2,level3,level4};
		String param[] = {level1,level2,level3};
		
		//验证是否可以添加
		String result = "Success";
		boolean validate = false;
		
		//验证数据是否全部为空
		for(String oneResult:param){
			
			if(!oneResult.equals("")){
				
				validate = true;
				break;
			}
		}
		
		if(!validate){
			
			result = "Import failed, data is null.";
		}
		
		if(validate){
			//*验证是否数据有断行
			for(int i = 0;i<param.length;i++){
				
				if(param[i].equals("") && validate){
					
					for(int j = i+1;j<param.length;j++){
						
						if(!param[j].equals("")){
							
							result = "Import failed, parent product category is null.";
							validate = false;
							break;
						}
					}
				}
			}
		}
		
		try {
			
			String catalogArray[] = null;
					
			//**验证是否可以添加
			if(validate){
				
				//重新组装数据,去掉空的数据
				int index = 0;
				
				for(int i=0;i<param.length;i++){
					
					if(param[i].equals("")){
						
						index = i;
						break;
					}
				}
				
				catalogArray = new String[index == 0 ? param.length : index];
				
				System.arraycopy(param, 0, catalogArray, 0, index == 0 ? param.length : index);
				
				//
				int length = catalogArray.length;
				
				for(int i = 0;i<length;i++){
					
					if(null != floorProductLineMgrTJH.getProductLineByName(catalogArray[i])){
						
						result = "Import failed, Product Line '"+catalogArray[i]+"' already exists";
						validate = false;
						break;
					}
					
					DBRow productCatalog = floorCatalogMgr.getProductCatalogByName(catalogArray[i]);
					
					if(i==0){
						
						if(productCatalog!=null){
							
							//1.判断如果后续加上以后会不会超过3级
							long level = this.getProductCatalogUpLevel(productCatalog.get("id", 0L));
							
							if( level+length-1 > MAXLEVEL){
								
								result = "Import failed, maximum "+MAXLEVEL+" layers of product categories.";
								validate = false;
								break;
							}
						}
						
					}else{
						
						if(productCatalog != null){
							
							//2.是否有上一级,上一级是不是上一个数据
							boolean existOtherParent = this.existParentProductCatalog(productCatalog.get("id", 0L),catalogArray[i-1]);
							
							if(!existOtherParent){
								
								result = "Import failed, one sub-category can only belong to one parent category.";
								validate = false;
								break;
							}

							//3.(下级的级别+挂上的级别)会不会超出3级 ,
							long level = this.getProductCatalogDownLevel(productCatalog.get("id", 0L));
							
							if((i+level > MAXLEVEL)){
								
								result = "Import failed, maximum "+MAXLEVEL+" layers of product categories.";
								validate = false;
								break;
							}
						}
					}
				}
			}
			
			//***验证通过,开始导入数据
			if(validate){
				
				long parentid = 0;
				
				long[] categorys = new long[catalogArray.length];
				
				int i = 0;
				
				for(String oneResult:catalogArray){
					
					DBRow productCatalog = floorCatalogMgr.getProductCatalogByName(oneResult);
					
					if(productCatalog == null){
						
						//添加分类adminLoggerBean
						DBRow catalogRow = new DBRow();
						catalogRow.add("title", oneResult);
						catalogRow.add("parentid", parentid);
						catalogRow.add("creator", adminLoggerBean.getAdid());
						catalogRow.add("create_time", DateUtil.NowStr());
						
						parentid = floorCatalogMgr.addProductCatalog(catalogRow);
						
					}else{
						
						//关联分类
						DBRow catalogRow = new DBRow();
						catalogRow.add("parentid", parentid);
						
						floorCatalogMgr.modProductCatalog(productCatalog.get("id", 0L), catalogRow);
						
						parentid = productCatalog.get("id", 0L);
					}
					
					categorys[i++] = parentid;
				}
				
				floorProductCatalogMgrZJ.callProductCatalogChildList(0);
				
				//导入分类后,分类与title的关系
				this.updateCategoryAndTitlesRelation(categorys);
				
			}
		} catch (Exception e){
			
			throw new Exception("InitDataMgrZJ.initCatalog(String xml) error:" + e);
		}
		
		return result;
	}
	
	/**
	 * 从最低的分类开始循环,如果与title有关系则向上级分类添加.
	 * 
	 * @param Long[分类ID]
	 * @author subin
	*/
	private void updateCategoryAndTitlesRelation(long[] categorys) throws Exception {
		
		for(int i = categorys.length;i-1 > 0;i-- ){
			
			//查询分类ID与title是否有关系
			DBRow[] relation = floorCatalogMgr.getTitleProductCatalog(categorys[i-1]);
			
			if( relation.length > 0 ){
				
				for(DBRow oneResult : relation){
					
					DBRow relation2 = floorCatalogMgr.getTitleProductCatalog(categorys[i-2], oneResult.get("tpc_title_id",0L) ,oneResult.get("tpc_customer_id",0));
					
					if(relation2 == null){
						
						DBRow data = new DBRow();
						data.put("tpc_title_id", oneResult.get("tpc_title_id",0L));
						data.put("tpc_product_catalog_id", categorys[i-2]);
						data.put("tpc_customer_id", oneResult.get("tpc_customer_id",0));
						
						floorCatalogMgr.insertTitleProductCatalog(data);
					}
				}
			}
		}
	}
	
	private long getProductCatalogUpLevel(long id){
		
		long level = 0;
		
		try {
			DBRow result = floorCatalogMgr.getProductCatalogById(id);
		
			if(result != null){
				
				long parentid = result.get("parentid",0L);
				
				if(parentid != 0L){
					
					level = this.getProductCatalogUpLevel(parentid);
					
					level++;
					
				}else{
					
					level = 1;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return level;
	}
	
	private boolean existParentProductCatalog(long id , String name){
		
		boolean returnVal = true;
		
		try {
			
			DBRow result = floorCatalogMgr.getProductCatalogById(id);
			
			if(result != null ){
				
				if(result.get("parentid",0L) != 0L){
					
					DBRow result1 = floorCatalogMgr.getProductCatalogById(result.get("parentid",0L));
					
					if(!result1.get("title", "").equals(name)){
						
						returnVal = false;
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return returnVal;
	}
	
	private long getProductCatalogDownLevel(long id){
		
		long level = 1L;
		
		try {
			
			DBRow[] levelRow = floorCatalogMgr.getProductCatalogByParentId(id, null);
			
			if(levelRow!=null && levelRow.length > 0){
				
				long num1 = 0L;
				
				for(DBRow oneResult : levelRow){
					
					long num2 = this.getProductCatalogDownLevel(oneResult.get("id", 0L));
					
					if(num2 > num1){
						
						num1 = num2;
					}
				}
				
				level = level + num1;
			}
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return level;
	}
	
	/**
	 * 基础数据：初始化创建Title Excel
	 * @param rwb
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public String initTitle(String xml,AdminLoginBean adminLoggerBean) throws Exception {
		
		String result;
		
		try {
			
			String title_name = XmlUtil.initDataXmlStringToDBRowCategory(xml,new String[]{"Title"}).getString("title");
			DBRow titleIsExist = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name.toUpperCase());

			if (titleIsExist != null) {
				
				throw new TitleHasExistException();
			}

			DBRow title = new DBRow();//adminLoggerBean
			title.add("title_name", title_name);
			title.add("creator", adminLoggerBean.getAdid());
			title.add("create_time", DateUtil.NowStr());

			long title_id = floorProprietaryMgrZyj.addProprietary(title);
			TitleIndexMgr.getInstance().addIndex(title_id);
			
			result = "Success";
		} catch (TitleHasExistException e) {
			result = "Title Has Exist";
		} catch (Exception e) {
			result = "System Error";
		}
		
		return result;
	}
	
	/**
	 * 基础数据：初始化创建SKU-CATEGORY-TITLE Excel
	 * @param rwb,AdminLoginBean
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public String initSKUCatalogTitle(String xml,AdminLoginBean adminLoggerBean) throws Exception {
		
		String result;
		String msg = "";	
		try {
			
			String[] nodeNames = new String[]{
									"Pname"
									,"LOT"
									,"SKU"
									,"KeyCode"
									,"Category"
									,"Title"
									
									,"Customer"
									
									,"UPC"
									,"PieceDepth"
									,"PieceHeight"
									,"PieceWidth"
									,"WeightPerUnit"
									,"UnitCost"
									,"Class"
									,"NMFCCode"
									,"snLength"
									,"LengthUOM"
									,"WeightUOM"
									,"PriceUOM"
								};
			
			DBRow requestRow = XmlUtil.initDataXmlStringToDBRowCategory(xml,nodeNames);
			
			String lot = requestRow.getString("lot");
			String sku = requestRow.getString("sku");
			
			//String p_name = requestRow.getString("pname");
			String p_name = sku;
			if(!StringUtil.isBlank(lot))
			{
				p_name += "/" + lot;
			}
			String p_code = requestRow.getString("keycode").trim();
			if(p_code.equals("")){
				
				p_code = p_name;
			}
			
			String category = requestRow.getString("category");
			String title_name = requestRow.getString("title");
			
			String Customer_name = requestRow.getString("customer");
			
			String upc = requestRow.getString("upc");
			String lengthString = requestRow.getString("piecedepth");
			String widthString = requestRow.getString("piecewidth");
			String heightString = requestRow.getString("pieceheight");
			String weightString = requestRow.getString("weightperunit");
			String unitCost = requestRow.getString("unitcost");
			String freightClass = requestRow.getString("class");
			String nmfc_code = requestRow.getString("nmfccode");
			String LengthUOM = requestRow.getString("LengthUOM");
			String WeigthUOM = requestRow.getString("WeightUOM");
			String PriceUOM = requestRow.getString("PriceUOM");
			String snLengthStr = requestRow.getString("snLength");
			
			/**是否存在此分类*/
			DBRow catalog = floorCatalogMgr.getProductCatalogByName(category);
			if (catalog==null) throw new CatalogNotExistException();
			//分类ID
			long catalog_id = catalog.get("id",0l);
			
			//验证此商品分类是否是最后一级
			DBRow[] subRow = floorCatalogMgr.getProductCatalogByParentId(catalog_id, null);
			
			if(subRow != null && subRow.length > 0){
				
				result = "There are sub-categories in this category";
				return result;
			}
			
			/**是否存在此title*/
			DBRow title = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);
			
			/**是否存在此customer*/
			DBRow customer = floorProprietaryMgrZyj.getCustomerByName(Customer_name);
			
			//判断title和customer, 要么都有值 , 要么都没值.
			if (title==null && customer != null) 
				
				throw new TitleNotFoundException();
			
			if (customer==null && title != null ) {
				
				result = "Customer Not Found.";
				return result;
			}
			
			long pc_id;
			//是否存在此商品
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			//商品不存在,添加商品
			if (product == null) {
				
				pc_id = this.initAddProduct(
							p_name
							,sku
							,lot
							,p_code
							,catalog_id
							,upc
							,lengthString
							,widthString
							,heightString
							,weightString
							,unitCost
							,freightClass
							,nmfc_code
							,adminLoggerBean
							,LengthUOM
							,WeigthUOM
							,PriceUOM
							,snLengthStr
						);
				
				//记录商品添加日志
				DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.ADD);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
			
			//商品存在,验证一些基础属性是否一致
			} else {
				pc_id = product.get("pc_id",0l);
				DBRow ret = this.checkProductAttribute(product, p_name, p_code, catalog_id, upc, lengthString, widthString, heightString, weightString, unitCost, freightClass, nmfc_code,LengthUOM
						,WeigthUOM
						,PriceUOM);
				if ("false".equals(ret.getString("result"))){
					msg = ret.getString("msg");
					throw new ProductAttributeDifferentException();
				}else{
					boolean isInsertSn = true; 
					DBRow[] snRows = floorProductMgr.getProductSnByPcId(pc_id);
					for(DBRow sn : snRows)
					{
						if(snLengthStr.equals(sn.getString("sn_size")))
						{
							// 如果sn有相同，则返回 不需要做插入操作
							isInsertSn = false;
							break;
						}
					}
					
					if(isInsertSn)
					{
						// 如果sn不同，则插入一条新的数据
						DBRow sn = new DBRow();
						sn.add("pc_id", pc_id);
						sn.add("sn_size", snLengthStr);
						sn.add("default_value", 0);
						sn.add("creator", adminLoggerBean.getAdid());
						sn.add("create_time", DateUtil.NowStr());
						floorProductMgr.addProductSn(sn);
					}
				}
			}
			
			if(title != null && customer != null){
				
				//titleID
				long title_id = title.get("title_id",0l);
				
				long customer_id = customer.get("customer_key", 0l);
				
				//添加SKU与Title的关系
				this.initSKUTitle(pc_id, title_id, customer_id);
			}
			
			//添加Category与Title的关系
			//this.initCategoryTitle(catalog_id,title_id);
			/*
			DBRow[] pcodes = floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			ProductIndexMgr.getInstance().addIndexAsyn(pc_id, p_name,pcodeString, catalog_id,"",1);*/
			
			//this.updateCategoryAndLineForTitleRelation(pc_id);
			proprietaryMgrZyj.updateCustomerTitleAndCategoryLineRelation(pc_id);
			
			result = "Success";
			
		} catch (NumberFormatException e) {
			
			result = "Length,Height,Width,Weight,Unit cost,Freight Class must Number Format";

		} catch (CatalogNotExistException e) {
			
			result = "CATEGORY Unable to Identify";
		} catch (ProductAttributeDifferentException e) {
			
			result = msg + " Different";
		} catch (ProductCodeIsExistException e) {
			
			result = "MainCode Repeat";
			
		} catch ( TitleNotFoundException e) {
			
			result = "Title Not Found.";
			
		}   catch (Exception e) {
			
			result = "System Error "+e;
		}
		return result;
	}
	
	/**
	 * 导入商品后,更新产品分类与title的关系,更新产品线与title的关系
	 * 
	 * @param 商品ID
	 * @author subin
	*/
	@SuppressWarnings("unused")
	private void updateCategoryAndLineForTitleRelation(long pcId) throws Exception{
		
		//查询商品的分类
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		//查询商品对应的title
		DBRow[] titles = floorProprietaryMgrZyj.getProudctHasTitleList(pcId);
		
		long categoryId = product.get("catalog_id", 0L);
		
		//如果两个都存在的话
		if(categoryId != 0 && titles.length > 0){
			
			//循环分类与title
			this.updateCategoryForTitleRelation(categoryId,titles);
			
			DBRow category = floorCatalogMgr.getProductCatalogById(categoryId);
			
			//循环产品线与title
			this.updateLineForTitleRelation(category.get("product_line_id", 0L),titles);
		}
	}
	
	//更新产品分类与title的关系
	private void updateCategoryForTitleRelation(long categoryId,DBRow[] titles) throws Exception {
		
		//如果存在分类
		if(categoryId != 0 && titles.length > 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductCatalog(categoryId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpc_title_id", one.get("title_id",0L));
					row.put("tpc_product_catalog_id",categoryId);
					//添加
					floorCatalogMgr.insertTitleProductCatalog(row);
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.updateCategoryForTitleRelation(parentCategory.get("id",0L),titles);
			}
		}
	}
	
	//更新产品线与title的关系
	private void updateLineForTitleRelation(long lineId,DBRow[] titles) throws Exception{
		
		if(lineId != 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductLine(lineId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title_id",0L));
					row.put("tpl_product_line_id", lineId);
					//添加
					floorCatalogMgr.insertTitleProductLine(row);
				}
			}
		}
	}
	
	private String importErrorExcel(Map<String,DBRow[]> errorResult,Workbook rwb)
		throws Exception
	{
		CellStyle errorStyle = rwb.createCellStyle();
		errorStyle.setFillForegroundColor(IndexedColors.RED.index);
		errorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		DBRow[] errorProduct = errorResult.get("errorProduct");
		this.addErrorNotation(rwb.getSheet("SKU"),errorProduct,errorStyle);
		
		DBRow[]	errorTitle = errorResult.get("errorTitle");
		this.addErrorNotation(rwb.getSheet("TITLE"),errorTitle,errorStyle);
		
		DBRow[] errorSKUTitle = errorResult.get("errorSKUTitle");
		this.addErrorNotation(rwb.getSheet("SKU&TITLE"),errorSKUTitle, errorStyle);
		
		DBRow[] errorCategoryTitle = errorResult.get("errorCategoryTitle");
		this.addErrorNotation(rwb.getSheet("TITLE&CATEGORY"),errorCategoryTitle, errorStyle);
		
		DBRow[] errorContainerType = errorResult.get("errorContainerType");
		this.addErrorNotation(rwb.getSheet("ContainerType"),errorContainerType, errorStyle);
		
		DBRow[] errorBLP = errorResult.get("errorBLP");
		this.addErrorNotation(rwb.getSheet("BLP"),errorBLP, errorStyle);
		
		DBRow[] errorCLP = errorResult.get("errorCLP");
		this.addErrorNotation(rwb.getSheet("CLP"),errorCLP, errorStyle);
		
		DBRow[] errorShipTo = errorResult.get("errorShipTo");
		this.addErrorNotation(rwb.getSheet("ShipTo"),errorShipTo, errorStyle);
		
		DBRow[] errorShipToAddress = errorResult.get("errorShipToAddress");
		this.addErrorNotation(rwb.getSheet("ShipToAddress"),errorShipToAddress, errorStyle);
		
		String fileName = "initError_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsm";
		String path = "upl_excel_tmp/"+fileName;
		OutputStream os= new FileOutputStream(Environment.getHome()+path);
		rwb.write(os);  
		os.close();  
		return (path);
	}
	
	private void addErrorNotation(Sheet sheet,DBRow[] error,CellStyle errorStyle)
		throws Exception
	{
		try 
		{
			
			Drawing patr = sheet.createDrawingPatriarch();
			for (int i = 0; i < error.length; i++) 
			{
				int line = error[i].get("line",0);
				
				Row errorRow = sheet.getRow(line);
				if (errorRow!=null)
				{
					int lastCellNum = errorRow.getLastCellNum();
					
					//前四个参数是坐标点,后四个参数是编辑和显示批注时的大小. 
					Comment comment= patr.createCellComment(new XSSFClientAnchor(0,0,0,0,(short)lastCellNum,line,(short)lastCellNum+2,line+1)); 
					//输入批注信息 
					comment.setString(new XSSFRichTextString(error[i].getString("reason"))); 
					//添加作者,选中B5单元格,看状态栏 
					comment.setAuthor("System");
					comment.setVisible(false);
					
					//将批注添加到单元格对象中 
					
					Cell errorReasonCell = errorRow.createCell(lastCellNum);
					errorReasonCell.setCellValue(error[i].getString("reason"));
					errorReasonCell.setCellStyle(errorStyle);
					errorReasonCell.setCellComment(comment);
					
					for (int j = 0; j < lastCellNum; j++) 
					{
						Cell cell = errorRow.getCell(j); 
						if (cell!=null)
						{
							cell.setCellStyle(errorStyle);
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addErrorNotation",log);
		}
	}
	
	
	
	public ArrayList<DBRow> initCatalog(Workbook rwb)
		throws Exception
	{
		try 
		{
			HashMap<String,String> catalogName = new HashMap<>();
			
			
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			
			Sheet sheet = (Sheet) rwb.getSheet("Category");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++)
			{
				Row row = sheet.getRow(i);
				if(row!=null)
				{
					String catalog_name = this.getCellValue(row.getCell(0));
					String parent_catalog_name = this.getCellValue(row.getCell(1));
					
					if (rowIsNull(new String[]{catalog_name,parent_catalog_name})==false) 
					{
						//先检查父类分类是否创建，若创建了，就不在创建
						if (0 == floorCatalogMgr.getProductCatalogsByName(parent_catalog_name).length) 
						{
							DBRow catalog = new DBRow();
							catalog.add("title", parent_catalog_name);
							catalog.add("parentid",0);
							floorCatalogMgr.addProductCatalog(catalog);
						}
						catalogName.put(catalog_name, parent_catalog_name);
					}
				}
			}
			
			Set<String> mapKey = catalogName.keySet();
			
			for (String catalog_title : mapKey) 
			{
				if (0==floorCatalogMgr.getProductCatalogsByName(catalog_title).length) 
				{
					DBRow parent_catalog = floorCatalogMgr.getProductCatalogsByName(catalogName.get(catalog_title))[0];
					
					DBRow catalog = new DBRow();
					catalog.add("title",catalog_title);
					catalog.add("parentid",parent_catalog.get("id",0l));
					
					floorCatalogMgr.addProductCatalog(catalog);
				} 
				else
				{
					DBRow errorCatalog = new DBRow();
					errorCatalog.add("title",catalog_title);
					errorCatalog.add("parent",catalogName.get(catalog_title));
					
					errorList.add(errorCatalog);
				}
			}
			
			floorProductCatalogMgrZJ.callProductCatalogChildList(0);
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initCatalog",log);
		}
	}
	
	private long initAddProduct(String p_name,String sku,String lot,String p_code,long catalog_id,String upc
			,String lengthString,String widthString,String heightString,String weightString,String unitCost
			,String freightClass,String nmfc_code,AdminLoginBean adminLoggerBean
			,String LengthUOM, String WeigthUOM, String PriceUOM, String snLengthStr) throws Exception {
		
//		DBRow pcode = floorProductCodeMgrZJ.getDetailProductCode(p_code);
//		
//		if (pcode != null) {
//			
//			throw new ProductCodeIsExistException();
//		}
		
		//excel 记录商品的长度单位为英寸
		double lengthDouble = Double.parseDouble(lengthString);
		double widthDouble = Double.parseDouble(widthString);
		double heightDouble = Double.parseDouble(heightString);
		//double inchToMM = 25.4;

		//int length = (int) (lengthInch);// * inchToMM
		//int width = (int) (widthInch);
		//int height = (int) (heightInch);
		double volume = lengthDouble * widthDouble * heightDouble;// / 1000;

		//excel 记录商品重量单位为磅
		double weightLBS = Double.parseDouble(weightString);
		//double LBSToKg = 0.45359237;
		double weight = MoneyUtil.round((weightLBS),2);// * LBSToKg
		int snLength = Integer.parseInt(snLengthStr);

		double unit_price = Double.parseDouble(unitCost.length() > 0 ? unitCost : "0");
		int freight_class = Integer.parseInt(freightClass.length() > 0 ? freightClass : "0");
		
		//将uom字符串转换成key
		LengthUOMKey lengthUOMKey = new LengthUOMKey();
		WeightUOMKey weightUOMKey = new WeightUOMKey();
		PriceUOMKey priceUOMKey = new PriceUOMKey();
		int lengthUom = lengthUOMKey.getLengthUOMKeyByValue(LengthUOM);
		int weightUom = weightUOMKey.getWeightUOMKeyByValue(WeigthUOM);
		int priceUom = priceUOMKey.getMoneyUOMKeyByValue(PriceUOM);

		DBRow product = new DBRow();
		
		product.add("p_name", p_name);
		product.add("sku", sku);
		product.add("lot_no", lot);
		product.add("catalog_id", catalog_id);
		product.add("length", lengthDouble);
		product.add("width", widthDouble);
		product.add("heigth", heightDouble);
		product.add("weight", weight);
		product.add("volume", volume);
		product.add("unit_name", "ITEM");
		product.add("unit_price", unit_price);
		product.add("freight_class", freight_class);
		product.add("nmfc_code", nmfc_code);
		product.add("length_uom", lengthUom);
		product.add("weight_uom", weightUom);
		product.add("price_uom", priceUom);
		product.add("sn_size", snLength);
		product.add("creator", adminLoggerBean.getAdid());
		product.add("create_time", DateUtil.NowStr());
		//添加商品
		long pc_id = floorProductMgr.addProduct(product);
		
		// 添加product sn
		DBRow snInfo = new DBRow();
		snInfo.add("pc_id", pc_id);
		snInfo.add("sn_size", snLengthStr);
		snInfo.add("default_value", 1);
		snInfo.add("creator", adminLoggerBean.getAdid());
		snInfo.add("create_time", DateUtil.NowStr());
		floorProductMgr.delProductSn(pc_id);
		floorProductMgr.addProductSn(snInfo);
		
		//添加商品唯一识别码
		DBRow product_code = new DBRow();
		product_code.add("pc_id", pc_id);
		product_code.add("p_code", p_code);
		product_code.add("code_type", CodeTypeKey.Main);
		product_code.add("create_adid",adminLoggerBean.getAdid());
		product_code.add("create_time", DateUtil.NowStr());
		long pcode_id = floorProductCodeMgrZJ.addProductCode(product_code);

		//添加UPC条码
		DBRow productCodeUPC = new DBRow();
		productCodeUPC.add("pc_id", pc_id);
		productCodeUPC.add("p_code", upc);
		productCodeUPC.add("code_type", CodeTypeKey.UPC);
		productCodeUPC.add("create_adid",adminLoggerBean.getAdid());
		productCodeUPC.add("create_time", DateUtil.NowStr());
		long pcodeUPC_id = floorProductCodeMgrZJ.addProductCode(productCodeUPC);

		DBRow[] pcodes = floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
		String pcodeString = "";
		
		for (int q = 0; q < pcodes.length; q++) {
			
			pcodeString += pcodes[q].getString("p_code") + " ";
		}

		ProductIndexMgr.getInstance().addIndex(pc_id,p_name,pcodeString, catalog_id, "", 1);

		DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
		product_log.add("account", adminLoggerBean.getAdid());
		product_log.add("edit_type", ProductEditTypeKey.ADD);
		product_log.add("edit_reason",ProductEditReasonKey.SELF);
		product_log.add("post_date", DateUtil.NowStr());

		floorProductLogsMgrZJ.addProductLogs(product_log);
		
		return pc_id;
	}
	
	//检查商品属性
		
	public DBRow checkProductAttribute(DBRow product,String p_name,String p_code,long catalog_id,String upc,String lengthString,String widthString,String heightString,String weightString,String unitCost,String freightClass,String nmfc_code,String LengthUOM, String WeigthUOM,String PriceUOM) throws Exception {
		//excel 记录商品的长度单位为英寸
		double lengthDouble = Double.parseDouble(lengthString);
		double widthDouble = Double.parseDouble(widthString);
		double heightDouble = Double.parseDouble(heightString);
		
		//double inchToMM = 25.4;
		//int length = (int) (lengthInch);// * inchToMM
		//int width = (int) (widthInch);
		//int height = (int) (heightInch);
		//double LBSToKg = 0.45359237;
		//double volume = lengthDouble * widthDouble * heightDouble;// / 1000;
		//excel 记录商品重量单位为磅
		
		double weightLBS = Double.parseDouble(weightString);
		double weight = MoneyUtil.round((weightLBS),2);// * LBSToKg
		double unit_price = Double.parseDouble(unitCost.length() > 0 ? unitCost : "0");
		int freight_class = Integer.parseInt(freightClass.length() > 0 ? freightClass : "0");
		
		/*if (!p_name.equals(product.getString("p_name"))){
			return false;
		}*/
		DBRow ret = new DBRow();
		
		if (!upc.equals(product.getString("p_code_upc"))){
			ret.add("result", "false");
			ret.add("msg", "UPC");
			return ret;
		}
		if (!p_code.equals(product.getString("p_code"))){
			ret.add("result", "false");
			ret.add("msg", "Key Code");
			return ret;
		}
		
		if (catalog_id !=product.get("catalog_id",0l)){
			
			ret.add("result", "false");
			ret.add("msg", "Category");
			return ret;
		}
		
		/*if ((Math.round(volume*1000)/1000.0)!=product.get("volume", 0D)){
			
			return false;
		}*/
		
		if (lengthDouble!=product.get("length",0d)){
			
			ret.add("result", "false");
			ret.add("msg", "Length");
			return ret;
		}
		if (widthDouble!=product.get("width",0d)){
					
			ret.add("result", "false");
			ret.add("msg", "Width");
			return ret;
		}
		if (heightDouble!=product.get("heigth",0d)){
			
			ret.add("result", "false");
			ret.add("msg", "Height");
			return ret;
		}
		if (weight!=product.get("weight",0d)){
			
			ret.add("result", "false");
			ret.add("msg", "Weight");
			return ret;
		}
		
		if (unit_price!=product.get("unit_price",0d)){
			
			ret.add("result", "false");
			ret.add("msg", "Price");
			return ret;
		}
		
		if (freight_class!=product.get("freight_class",0)){
			
			ret.add("result", "false");
			ret.add("msg", "Freight Class");
			return ret;
		}
		
		if (!nmfc_code.equals(product.getString("nmfc_code"))){
			
			ret.add("result", "false");
			ret.add("msg", "NMFC Code");
			return ret;
		}
		
		// string equals int
		if(!LengthUOM.equals(new LengthUOMKey().getLengthUOMKey(product.get("length_uom",0)))){
			
			ret.add("result", "false");
			ret.add("msg", "Length UOM");
			return ret;
		}
		if(!WeigthUOM.equals(new WeightUOMKey().getWeightUOMKey(product.get("weight_uom",0)))){
			
			ret.add("result", "false");
			ret.add("msg", "Weight UOM");
			return ret;
		}
		if(!PriceUOM.equals(new PriceUOMKey().getMoneyUOMKey(product.get("price_uom",0)))){
			
			ret.add("result", "false");
			ret.add("msg", "Price UOM");
			return ret;
		}
		
		
		return ret;
	}
	
	/**
	 * 初始化创建商品
	 * @param rwb
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DBRow> initProduct(Workbook rwb,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("SKU");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				
				if (row!=null)
				{
					String lotNumber = this.getCellValue(row.getCell(0));
					String sku = this.getCellValue(row.getCell(1));
					String p_name = this.getCellValue(row.getCell(2));
					String p_code = this.getCellValue(row.getCell(3));
					String category = this.getCellValue(row.getCell(4));
					String upc = this.getCellValue(row.getCell(5));
					String unit_name = this.getCellValue(row.getCell(6));
					String lengthString = this.getCellValue(row.getCell(7));
					String widthString = this.getCellValue(row.getCell(8));
					String heightString = this.getCellValue(row.getCell(9));
					String weightString = this.getCellValue(row.getCell(10));
					String unitCost = this.getCellValue(row.getCell(11));
					String freightClass = this.getCellValue(row.getCell(12));
					String nmfc_code = this.getCellValue(row.getCell(13));
					String[] rowValues = new String[]{lotNumber,sku,p_name,p_code,category,upc,unit_name,lengthString,widthString,heightString,weightString,unitCost,freightClass,nmfc_code};
					
					if (rowIsNull(rowValues)==false) 
					{
						
						if (p_code.equals("") && !lotNumber.equals("")) 
						{
							p_code = lotNumber;
						}
						try
						{

							DBRow isExistProduct = floorProductMgr.getDetailProductByPname(p_name);

							if (isExistProduct != null) 
							{
								throw new ProductNameIsExistException();
							}

							DBRow pcode = floorProductCodeMgrZJ.getDetailProductCode(p_code);
							if (pcode != null) 
							{
								throw new ProductCodeIsExistException();
							}

							DBRow[] catalogs = floorCatalogMgr.getProductCatalogsByName(category);

							if (catalogs.length != 1) 
							{
								throw new CatalogNotExistProductException();
							}

							DBRow catalog = catalogs[0];
							long catalog_id = catalog.get("id", 0l);

							//excel 记录商品的长度单位为英寸
							double lengthInch = Double.parseDouble(lengthString);               
							double widthInch = Double.parseDouble(widthString);
							double heightInch = Double.parseDouble(heightString);

							double inchToMM = 25.4;

							int length = (int) (lengthInch * inchToMM);
							int width = (int) (widthInch * inchToMM);
							int height = (int) (heightInch * inchToMM);
							int volume = length * width * height / 1000;

							//excel 记录商品重量单位为磅
							double weightLBS = Double.parseDouble(weightString);
							double LBSToKg = 0.45359237;
							double weight = MoneyUtil.round((weightLBS * LBSToKg),2);

							double unit_price = Double.parseDouble(unitCost.length() > 0 ? unitCost : "0");
							int freight_class = Integer.parseInt(freightClass.length() > 0 ? freightClass : "0");

							DBRow product = new DBRow();
							product.add("p_name", p_name);
							product.add("catalog_id", catalog_id);
							product.add("length", length);
							product.add("width", width);
							product.add("heigth", height);
							product.add("weight", weight);
							product.add("volume", volume);
							product.add("unit_name", unit_name);
							product.add("unit_price", unit_price);
							product.add("freight_class", freight_class);
							product.add("nmfc_code", nmfc_code);

							long pc_id = floorProductMgr.addProduct(product);
							//添加商品唯一识别码
							DBRow product_code = new DBRow();
							product_code.add("pc_id", pc_id);
							product_code.add("p_code", pc_id);
							product_code.add("code_type", CodeTypeKey.Main);
							product_code.add("create_adid",adminLoggerBean.getAdid());
							product_code.add("create_time", DateUtil.NowStr());
							long pcode_id = floorProductCodeMgrZJ.addProductCode(product_code);

							//添加UPC条码
							DBRow productCodeUPC = new DBRow();
							productCodeUPC.add("pc_id", pc_id);
							productCodeUPC.add("p_code", upc);
							productCodeUPC.add("code_type", CodeTypeKey.UPC);
							productCodeUPC.add("create_adid",adminLoggerBean.getAdid());
							productCodeUPC.add("create_time", DateUtil.NowStr());
							long pcodeUPC_id = floorProductCodeMgrZJ.addProductCode(productCodeUPC);

							DBRow[] pcodes = floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
							String pcodeString = "";
							for (int q = 0; q < pcodes.length; q++) 
							{
								pcodeString += pcodes[q].getString("p_code") + " ";
							}

							ProductIndexMgr.getInstance().addIndex(pc_id, p_name,pcodeString, catalog_id, unit_name, 1);

							DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
							product_log.add("account", adminLoggerBean.getAdid());
							product_log.add("edit_type", ProductEditTypeKey.ADD);
							product_log.add("edit_reason",ProductEditReasonKey.SELF);
							product_log.add("post_date", DateUtil.NowStr());

							floorProductLogsMgrZJ.addProductLogs(product_log);
						} 
						catch (NumberFormatException e) 
						{
							DBRow error = this.errorProductRow(lotNumber, sku,p_code, category, upc, unit_name, lengthString,heightString, widthString, weightString,unitCost, freightClass, nmfc_code);
							error.add("line", i);
							error.add("reason","Depth,Height,Width,Weight,Unit cost,Class must Number Format");

							errorList.add(error);
						}
						catch (CatalogNotExistProductException e) 
						{
							DBRow error = this.errorProductRow(lotNumber, sku,p_code, category, upc, unit_name, lengthString,heightString, widthString, weightString,unitCost, freightClass, nmfc_code);
							error.add("line", i);
							error.add("reason", "CATEGORY Unable to Identify");
							errorList.add(error);
						} 
						catch (ProductNameIsExistException e) 
						{
							DBRow error = this.errorProductRow(lotNumber, sku,p_code, category, upc, unit_name, lengthString,heightString, widthString, weightString,unitCost, freightClass, nmfc_code);
							error.add("line", i);
							error.add("reason", "Product Repeat");
							errorList.add(error);
						}
						catch (ProductCodeIsExistException e) 
						{
							DBRow error = this.errorProductRow(lotNumber, sku,p_code, category, upc, unit_name, lengthString,heightString, widthString, weightString,unitCost, freightClass, nmfc_code);
							error.add("line", i);
							error.add("reason", "Product Code Repeat");
							errorList.add(error);
						} 
						catch (Exception e) 
						{
							DBRow error = this.errorProductRow(lotNumber, sku,p_code, category, upc, unit_name, lengthString,heightString, widthString, weightString,unitCost, freightClass, nmfc_code);

							error.add("line", i);
							error.add("reason", "System Error:" + e);
							errorList.add(error);
						}
					}
				}
				
			}
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initProduct",log);
		}
	}
	
	
	
	public ArrayList<DBRow> initTitle(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("TITLE");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i <rsRows; i++)
			{
				Row row = sheet.getRow(i);
				if (row!=null)
				{
					String title_name = this.getCellValue(row.getCell(0)).toUpperCase().trim().replaceAll(" ","");
					String[] rowValues = new String[]{title_name};	
					
					if (rowIsNull(rowValues)==false) 
					{
						try 
						{
							DBRow titleIsExist = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);

							if (titleIsExist != null) 
							{
								throw new TitleHasExistException();
							}

							DBRow title = new DBRow();
							title.add("title_name", title_name);

							floorProprietaryMgrZyj.addProprietary(title);
						}
						catch (TitleHasExistException e) 
						{
							DBRow errorTitle = new DBRow();
							errorTitle.add("title_name", title_name);
							errorTitle.add("line", i);
							errorTitle.add("reason", "Title Repeat");

							errorList.add(errorTitle);
						}
						catch (Exception e) 
						{
							DBRow errorTitle = new DBRow();
							errorTitle.add("title_name", title_name);
							errorTitle.add("line", i);
							errorTitle.add("reason", "System Error:" + e);
							errorList.add(errorTitle);
						}
					}
				}
			}
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initTitle",log);
		}
	}
	
	/**
	 * 初始化Title与商品的分配
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public void initSKUTitle(long pc_id,long title_id) throws Exception {
		
		DBRow titleProduct = floorProprietaryMgrZyj.getDetailTilteProductByPCTitle(pc_id, title_id);

		if (titleProduct == null) {
			
			titleProduct = new DBRow();
			titleProduct.add("tp_pc_id", pc_id);
			titleProduct.add("tp_title_id", title_id);
			
			floorProprietaryMgrZyj.addProprietaryProduct(titleProduct);
			
			DBRow[] containerProduct=this.floorClpTypeMgrZr.findProductNode(pc_id);
			DBRow containerProductLine=this.floorClpTypeMgrZr.findProductLineNode(pc_id);
			DBRow[] hasTitle = proprietaryMgrZyj.getProductHasTitleList(pc_id);
			String titleid = "";
			for(int j = 0; j<hasTitle.length; j++){
				
				titleid += ","+hasTitle[j].get("title_id",0l);
			}
			
			for(int k = 0; k < containerProduct.length; k++){
				Map<String,Object> productNode = new HashMap<String, Object>();

				productNode.put("pc_id",pc_id);
				productNode.put("title_id",titleid.equals("")?titleid:titleid.substring(1));				
				productNode.put("product_line",containerProductLine.get("product_line_id",0l));
				productNode.put("catalog_id",containerProduct[k].get("catalog_id",0l));
				productNode.put("p_name",containerProduct[k].getString("p_name"));
				productNode.put("union_flag",containerProduct[k].get("union_flag",0));
				
				
				Map<String,Object> searchFor = new HashMap<String, Object>();
				searchFor.put("pc_id", pc_id);
				
			//	productStoreMgr.addNode(1,NodeType.Product,productNode);//添加商品节点
				productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//添加商品节点
				//productStoreMgr.clearGraphDB(1, false);
			}
			
			
			
		}
		//floorProprietaryMgrZyj.calculateCatelogAndLineTitle(pc_id);
	}
	
	public void initSKUTitle(long pc_id,long title_id, long customer_id) throws Exception {
		
		DBRow titleProduct = floorProprietaryMgrZyj.getDetailTilteProductByPCTitle(pc_id, title_id, customer_id);

		if (titleProduct == null) {
			
			titleProduct = new DBRow();
			titleProduct.add("tp_pc_id", pc_id);
			titleProduct.add("tp_title_id", title_id);
			titleProduct.add("tp_customer_id", customer_id);
			
			floorProprietaryMgrZyj.addProprietaryProduct(titleProduct);
		}
	}
	
	public ArrayList<DBRow> initSKUTitle(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("SKU&TITLE");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				if (row!=null)
				{
					String lot = this.getCellValue(row.getCell(0));
					String sku = this.getCellValue(row.getCell(1));
					String p_name = this.getCellValue(row.getCell(2));
					String title_name = this.getCellValue(row.getCell(3)).toUpperCase().trim().replaceAll(" ","");
					
					
					String[] rowValues = new String[]{lot,sku,p_name,title_name};
					
					if (rowIsNull(rowValues)==false)
					{
						DBRow product = floorProductMgr.getDetailProductByPname(p_name);
						DBRow title = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);
						try 
						{
							if (product == null)
							{
								throw new ProductNotExistException();
							}

							if (title == null)
							{
								throw new TitleNotFoundException();
							}

							long pc_id = product.get("pc_id", 0l);
							long title_id = title.get("title_id", 0l);

							DBRow titleProduct = floorProprietaryMgrZyj.getDetailTilteProductByPCTitle(pc_id, title_id);

							if (titleProduct != null) 
							{
								throw new SKUTitleIsExistException();
							}

							titleProduct = new DBRow();
							titleProduct.add("tp_pc_id", pc_id);
							titleProduct.add("tp_title_id", title_id);
							floorProprietaryMgrZyj.addProprietaryProduct(titleProduct);
							
							
							
						} 
						catch (SKUTitleIsExistException e) 
						{
							DBRow errorTitleProduct = new DBRow();
							errorTitleProduct.add("lot", lot);
							errorTitleProduct.add("sku", sku);
							errorTitleProduct.add("title", title_name);
							errorTitleProduct.add("line",i);
							errorTitleProduct.add("reason", "SKU&TITLE The Defined");

							errorList.add(errorTitleProduct);

						} 
						catch (ProductNotExistException e) 
						{
							DBRow errorTitleProduct = new DBRow();
							errorTitleProduct.add("lot", lot);
							errorTitleProduct.add("sku", sku);
							errorTitleProduct.add("title", title_name);
							errorTitleProduct.add("line", i);
							errorTitleProduct.add("reason","Product Unable to Identify");
							errorList.add(errorTitleProduct);

						} 
						catch (TitleNotFoundException e) 
						{
							DBRow errorTitleProduct = new DBRow();
							errorTitleProduct.add("lot", lot);
							errorTitleProduct.add("sku", sku);
							errorTitleProduct.add("title", title_name);
							errorTitleProduct.add("line", i);
							errorTitleProduct.add("reason","Title Unable to Identify");
							errorList.add(errorTitleProduct);

						} 
						catch (Exception e) 
						{
							DBRow errorTitleProduct = new DBRow();
							errorTitleProduct.add("lot", lot);
							errorTitleProduct.add("sku", sku);	
							errorTitleProduct.add("title", title_name);
							errorTitleProduct.add("line", i);
							errorTitleProduct.add("reason", "System Error:" + e);
							errorList.add(errorTitleProduct);
						}
					}
				}
			}
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initSKUTitle",log);
		}
	}
	
	/**
	 * 初始化分类与Title的权限
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public void initCategoryTitle(long catalog_id,long title_id)
		throws Exception
	{
		DBRow[] parentCatalogs = floorProductCatalogMgrZJ.getFatherTree(catalog_id);
		
		for (int i = 0; i < parentCatalogs.length; i++) 
		{
			DBRow isExistTitleCatalog = floorProprietaryMgrZyj.getDetailTitleCatalogByTitleCatalog(catalog_id, title_id);
			if (isExistTitleCatalog == null) 
			{
				DBRow titleCatalog = new DBRow();
				titleCatalog.add("tpc_title_id", title_id);
				titleCatalog.add("tpc_product_catalog_id", catalog_id);

				floorProprietaryMgrZyj.addProprietaryProductCatagory(titleCatalog);
			}
		}
	}
	
	public ArrayList<DBRow> initCategoryTitle(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("TITLE&CATEGORY");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				if (row!=null)
				{
					String title_name = this.getCellValue(row.getCell(0));
					String catalog_title  = this.getCellValue(row.getCell(1));
					
					String[] rowValues = new String[]{title_name,catalog_title};
					
					if (rowIsNull(rowValues)==false) 
					{
						DBRow title = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);
						DBRow[] catalog = floorCatalogMgr.getProductCatalogsByName(catalog_title);
						try 
						{
							if (title == null) 
							{
								throw new TitleNotFoundException();
							}
							if (catalog.length != 1) 
							{
								throw new CatalogNotExistProductException();
							}

							long title_id = title.get("title_id", 0l);
							long catalog_id = catalog[0].get("id", 0l);

							DBRow isExistTitleCatalog = floorProprietaryMgrZyj.getDetailTitleCatalogByTitleCatalog(catalog_id, title_id);
							if (isExistTitleCatalog != null) 
							{
								throw new ProductCatalogTitleIsExistException();
							}

							DBRow titleCatalog = new DBRow();
							titleCatalog.add("tpc_title_id", title_id);
							titleCatalog.add("tpc_product_catalog_id", catalog_id);

							floorProprietaryMgrZyj.addProprietaryProductCatagory(titleCatalog);
						} 
						catch (ProductCatalogTitleIsExistException e) 
						{
							DBRow errorTitleCatalog = new DBRow();
							errorTitleCatalog.add("title_name", title_name);
							errorTitleCatalog.add("catalog_title", catalog_title);
							errorTitleCatalog.add("line", i);
							errorTitleCatalog.add("reason","Title&Catagory The defind");
							
							errorList.add(errorTitleCatalog);
						}
						catch (TitleNotFoundException e) 
						{
							DBRow errorTitleCatalog = new DBRow();
							errorTitleCatalog.add("title_name", title_name);
							errorTitleCatalog.add("catalog_title", catalog_title);
							errorTitleCatalog.add("line", i);
							errorTitleCatalog.add("reason","Title Unable to Identify");
							
							errorList.add(errorTitleCatalog);
						}
						catch (CatalogNotExistProductException e)
						{
							DBRow errorTitleCatalog = new DBRow();
							errorTitleCatalog.add("title_name", title_name);
							errorTitleCatalog.add("catalog_title", catalog_title);
							errorTitleCatalog.add("line", i);
							errorTitleCatalog.add("reason","Category Unable to Identify");
							
							errorList.add(errorTitleCatalog);
						} 
						catch (Exception e)
						{
							DBRow errorTitleCatalog = new DBRow();
							errorTitleCatalog.add("title_name", title_name);
							errorTitleCatalog.add("catalog_title", catalog_title);
							errorTitleCatalog.add("line", i);
							errorTitleCatalog.add("reason", "System Error:" + e);
							
							errorList.add(errorTitleCatalog);
						}
					}
				}
			}
			
			return errorList;
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"initCategoryTitle",log);
		}
	}
	
	/**
	 * 初始化基础容器
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DBRow> initContainerType(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("ContainerType");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				
				if (row!=null)
				{
					String typeName = this.getCellValue(row.getCell(0));
					String lengthInchString = this.getCellValue(row.getCell(1));
					String widthInchString = this.getCellValue(row.getCell(2));
					String heightInchString = this.getCellValue(row.getCell(3));
					String weightLBSString = this.getCellValue(row.getCell(4));
					String maxLoadLBSString = this.getCellValue(row.getCell(5));
					
					String[] rowValues = new String[]{typeName,lengthInchString,widthInchString,heightInchString,weightLBSString,maxLoadLBSString};
					
					if (rowIsNull(rowValues)==false) 
					{
						int container_type = ContainerTypeKey.TLP;
						if (typeName.indexOf("PALLET") != -1) {
							container_type = ContainerTypeKey.CLP;
						} else if (typeName.indexOf("CTN") != -1) {
//							container_type = ContainerTypeKey.BLP;//去掉ILP和BLP
						}
						double inchTocm = 2.54;
						double LBSToKg = 0.45359237;
						try
						{
							double lengthInch = Double.parseDouble(lengthInchString);
							double widthInch = Double.parseDouble(widthInchString);
							double heightInch = Double.parseDouble(heightInchString);
							double weightLBS = Double.parseDouble(weightLBSString);
							double maxLoadWeightLBS = Double.parseDouble(maxLoadLBSString);

							int length = (int) (lengthInch * inchTocm);
							int width = (int) (widthInch * inchTocm);
							int height = (int) (heightInch * inchTocm);
							double weight = MoneyUtil.round((weightLBS * LBSToKg),2);
							double max_load = MoneyUtil.round((maxLoadWeightLBS * LBSToKg), 2);

							DBRow isExistContainerType = floorContainerMgrZYZ.getContainerTypeIdByName(typeName);

							if (isExistContainerType != null) 
							{
								throw new LPTypeHasExistException();
							}

							DBRow addRow = new DBRow();
							addRow.add("type_name", typeName);
							addRow.add("length", length);
							addRow.add("width", width);
							addRow.add("height", height);
							addRow.add("weight", weight);
							addRow.add("max_load", max_load);
							addRow.add("max_height", 0);
							addRow.add("container_type", container_type);
							floorContainerMgrZYZ.addContainerType(addRow);
							
						} 
						catch (LPTypeHasExistException e) 
						{
							DBRow errorContainerType = new DBRow();
							errorContainerType.add("type_name", typeName);
							errorContainerType.add("length", lengthInchString);
							errorContainerType.add("width", widthInchString);
							errorContainerType.add("height", heightInchString);
							errorContainerType.add("weight", weightLBSString);
							errorContainerType.add("max_load", maxLoadLBSString);
							errorContainerType.add("line", i);
							errorContainerType.add("reason","Container Type Defined");

							errorList.add(errorContainerType);
						}
						catch (Exception e) 
						{
							DBRow errorContainerType = new DBRow();
							errorContainerType.add("type_name", typeName);
							errorContainerType.add("length", lengthInchString);
							errorContainerType.add("width", widthInchString);
							errorContainerType.add("height", heightInchString);
							errorContainerType.add("weight", weightLBSString);
							errorContainerType.add("max_load", maxLoadLBSString);
							errorContainerType.add("line", i);
							errorContainerType.add("reason", "System Error:" + e);
							errorList.add(errorContainerType);
						}
					}
				}
			}
			
			return errorList;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"initContainerType",log);
		}
	}
	
	public String initContainerType(String xml)
		throws Exception
	{
		String result;
		try 
		{
			String[] nodeNames = new String[]{"PackagingType","Length","Width","Height","Weight","TotalWeight","MaxHeight","LengthUOM", "WeightUOM"};
			DBRow requestRow = XmlUtil.initDataXmlStringToDBRowCategory(xml,nodeNames);
			
			String typeName = requestRow.getString("packagingtype");
			String lengthInchString = requestRow.getString("length");
			String widthInchString = requestRow.getString("width");
			String heightInchString = requestRow.getString("height");
			String weightLBSString = requestRow.getString("weight");
			String maxLoadLBSString = requestRow.getString("TotalWeight");
			String MaxHeightString = requestRow.getString("MaxHeight");
			String LengthUOMString = requestRow.getString("LengthUOM");
			String WeigthUOMString = requestRow.getString("WeightUOM");
			
			double lengthDouble = Double.parseDouble(lengthInchString);
			double widthDouble = Double.parseDouble(widthInchString);
			double heightDouble = Double.parseDouble(heightInchString);
			double weightLBS = Double.parseDouble(weightLBSString);
			double maxLoadWeightLBS = Double.parseDouble(maxLoadLBSString);
			double maxHeight = Double.parseDouble(MaxHeightString);
			double weight = MoneyUtil.round((weightLBS),2);
			double max_load = MoneyUtil.round((maxLoadWeightLBS), 2);

			DBRow isExistContainerType = floorContainerMgrZYZ.getContainerTypeIdByName(typeName);

			if (isExistContainerType != null) 
			{
				throw new LPTypeHasExistException();
			}

			//将uom字符串转换成key
			LengthUOMKey lengthUOMKey = new LengthUOMKey();
			WeightUOMKey weightUOMKey = new WeightUOMKey();
			int lengthUom = lengthUOMKey.getLengthUOMKeyByValue(LengthUOMString);
			int weightUom = weightUOMKey.getWeightUOMKeyByValue(WeigthUOMString);
			
			DBRow addRow = new DBRow();
			addRow.add("type_name", typeName);
			addRow.add("length", lengthDouble);
			addRow.add("width", widthDouble);
			addRow.add("height", heightDouble);
			addRow.add("weight", weight);
			addRow.add("max_load", max_load);
			addRow.add("max_height", maxHeight);
			addRow.add("length_uom", lengthUom);
			addRow.add("weight_uom", weightUom);
			floorContainerMgrZYZ.addContainerType(addRow);
			
			result = "Success";
		}
		catch (LPTypeHasExistException e) 
		{
			result = "Packaging Type Defined";
		}
		catch (Exception e) 
		{
			result = "System Error:" + e;
		}		
		
		return result;
	}
	
	public int findContainerTypeIdByName(String type) throws Exception
	{
		int typeId = 0;
		switch (type) {
		/*
			case "ILP":
				typeId = ContainerTypeKey.ILP;
				break;
			case "BLP":
				typeId = ContainerTypeKey.BLP;
				break;*///去掉ILP和BLP
			case "CLP":
				typeId = ContainerTypeKey.CLP;
				break;
			case "TLP":
				typeId = ContainerTypeKey.TLP;
				break;
			default:
				typeId = 0;
				break;
		}
		return typeId;
	}
	
	/**
	 * 初始化BLP
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public String initBLP(String xml, long ps_id)
		throws Exception
	{
		String result;
		try
		{
//			String[] nodeNames = new String[]{"Pname","InnerType","InnerName","LoadingLength","LoadingWidth","LoadingHeight","ContainerType","BLPName","InnerHasSN"}; 
			String[] nodeNames = new String[]{"Pname","InnerType","InnerName","LoadingLength","LoadingWidth","LoadingHeight","ContainerType","BLPName"};
			DBRow requestRow = XmlUtil.initDataXmlStringToDBRowCategory(xml,nodeNames);
			
			String p_name = requestRow.getString("pname");
			String innerType = requestRow.getString("innertype");
//			String innerTypeId = requestRow.getString("innername");
			String innerLengthString = requestRow.getString("loadinglength");
			String innerWidthString = requestRow.getString("loadingwidth");
			String innerHeightString = requestRow.getString("loadingheight");
			String containerTypeName = requestRow.getString("containertype");
			String boxTypeName = requestRow.getString("blpname");
			String innerHasSN = requestRow.getString("innerhassn");
			
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			DBRow containerType = floorContainerMgrZYZ.getContainerTypeIdByName(containerTypeName);
			
			if (product==null)
			{
				throw new ProductNotExistException();
			}
			if (containerType==null)
			{
				throw new ContainerTypeNotFoundException();
			}
			long pc_id = product.get("pc_id",0l);
			
			int innerCount = 1;
			long ibt_id = 0;
			float inner_weight = 0F;
			String inner_weight_uom = "";
			String ilpTypeName = "";
			if (!innerType.toUpperCase().equals("PRODUCT"))
			{
				DBRow innerBoxType = boxTypeMgrZr.findLpTypeByPcidLpTypeName(pc_id, innerType);
						//boxTypeMgrZr.findIlpByIlpId(ibt_id);
						//floorLPTypeMgrZJ.getDetailILPType(ibt_id);
				if (innerBoxType==null)
				{
					throw new InnerContainerTypeNotFoundException();
				}
				ibt_id = innerBoxType.get("lpt_id", 0L);
				innerCount = innerBoxType.get("inner_total_pc",0);
				inner_weight = innerBoxType.get("total_weight", 0F);
				ilpTypeName = innerBoxType.getString("lp_name");
				inner_weight_uom = innerBoxType.getString("weight_uom");
			}
			
			int is_has_sn;
			if (innerHasSN.toUpperCase().equals("Y"))
			{
				is_has_sn = 1;
			}
			else
			{
				is_has_sn = 2;
			}
			
			float pc_weight = product.get("weight", 0F);
			String pc_weight_uom = product.getString("weight_uom");
			int innerLength = Integer.parseInt(innerLengthString);
			int innerWidth = Integer.parseInt(innerWidthString);
			int innerHeight = Integer.parseInt(innerHeightString);
			long container_type_id = containerType.get("type_id",0l);
			float box_length = containerType.get("length",0f); 
			float box_width = containerType.get("width",0f);
			float box_height = containerType.get("height",0f);
			float box_weight = containerType.get("weight",0f);
			
//			int box_type_sort = boxTypeMgrZr.getBoxTypeSort(pc_id); 
			
			int box_total = innerLength*innerWidth*innerHeight;
			int box_total_piece = box_total*innerCount; 
			
			float total_weight = 0F;
			//应该换算成一种单位去计算重量
			if(!innerType.toUpperCase().equals("PRODUCT"))
			{
				total_weight = inner_weight * box_total + box_weight;
			}
			else
			{
				total_weight = box_total_piece * pc_weight + box_weight;
			}
			if(StrUtil.isBlank(boxTypeName))
			{
				boxTypeName = "BLP"+innerLength+"*"+innerWidth+"*"+innerHeight;
				if(!innerType.toUpperCase().equals("PRODUCT"))
				{
					boxTypeName += "["+ilpTypeName+"]";
				}
			}
			
			DBRow isExitBoxType = floorLPTypeMgrZJ.getDetailBLPType(pc_id,innerLength,innerWidth,innerHeight,ibt_id,container_type_id);
			
			if (isExitBoxType != null)
			{
				throw new BLPHasExistException();
			}
			
			DBRow boxType = new DBRow();
			boxType.add("pc_id",pc_id);
			boxType.add("stack_length_qty",innerLength);
			boxType.add("stack_width_qty",innerWidth);
			boxType.add("stack_height_qty",innerHeight);
			boxType.add("inner_pc_or_lp",ibt_id);
			boxType.add("inner_total_lp",box_total);
			boxType.add("inner_total_pc",box_total_piece);
			boxType.add("ibt_length",box_length);
			boxType.add("ibt_width",box_width);
			boxType.add("ibt_height",box_height);
			boxType.add("ibt_weight",box_weight);
			boxType.add("basic_type_id",containerType.get("type_id",0l));
			boxType.add("lp_name",boxTypeName);
			boxType.add("is_has_sn",is_has_sn);
			boxType.add("length_uom",containerType.getString("length_uom"));
			boxType.add("weight_uom",containerType.getString("weight_uom"));
			boxType.add("total_weight", total_weight);
			boxType.add("active", YesOrNotKey.YES);
			
			long box_type_id = floorLPTypeMgrZJ.addBoxType(boxType);
			
			
		
			
			result = "Success";
		}
		catch(NumberFormatException e)
		{
			result = "StackLength,StackWidth,StackHeight Must Be Number Format";
		}
		catch(BLPHasExistException e)
		{
			result = "BLP Type Defined";
		}
		catch (InnerContainerTypeNotFoundException e)
		{
			result = "InnerContainerType Unable to Identify";
		}
		catch (ContainerTypeNotFoundException e) 
		{
			result = "ContainerType Unable to Identify";
		}
		catch (ProductNotExistException e)
		{
			result = "Product Unable to Identify";
		}
		catch (Exception e) 
		{
			result = "System Error:"+e;
		}
		
	
		
		
		
		return result;
	}
	
	public ArrayList<DBRow> initBLP(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("BLP");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				if (row!=null) 
				{
					String sku = this.getCellValue(row.getCell(0));
					String lot = this.getCellValue(row.getCell(1));
					String p_name = this.getCellValue(row.getCell(2));
					String innerType = this.getCellValue(row.getCell(3));
					String innerTypeId = this.getCellValue(row.getCell(4));
					String innerLengthString = this.getCellValue(row.getCell(5));
					String innerWidthString = this.getCellValue(row.getCell(6));
					String innerHeightString = this.getCellValue(row.getCell(7));
					String containerTypeName = this.getCellValue(row.getCell(9));
					String boxTypeName = this.getCellValue(row.getCell(10));
					
					String[] rowValues = new String[]{p_name,innerType,innerTypeId,innerLengthString,innerWidthString,innerHeightString,containerTypeName};
					
					if (rowIsNull(rowValues))
					{
						continue;
					}
					
					DBRow product = floorProductMgr.getDetailProductByPname(p_name);
					DBRow containerType = floorContainerMgrZYZ.getContainerTypeIdByName(containerTypeName);
					
					int innerCount = 1;
					long ibt_id = 0;
					try
					{
						if (innerType.toUpperCase().equals("ILP"))
						{
							ibt_id = Integer.parseInt(innerTypeId);
								
							DBRow innerBoxType = floorLPTypeMgrZJ.getDetailILPType(ibt_id);
							
							if (innerBoxType==null)
							{
								throw new InnerContainerTypeNotFoundException();
							}
								
							innerCount = innerBoxType.get("ibt_total",0);
						}
						
						if (product==null)
						{
							throw new ProductNotExistException();
						}
						if (containerType==null)
						{
							throw new ContainerTypeNotFoundException();
						}
						long pc_id = product.get("pc_id",0l);
						int innerLength = Integer.parseInt(innerLengthString);
						int innerWidth = Integer.parseInt(innerWidthString);
						int innerHeight = Integer.parseInt(innerHeightString);
						long container_type_id = containerType.get("type_id",0l);
						float box_length = containerType.get("length",0f); 
						float box_width = containerType.get("width",0f);
						float box_height = containerType.get("height",0f);
						float box_weight = containerType.get("weight",0f);
						
						int box_type_sort = boxTypeMgrZr.getBoxTypeSort(pc_id); 
						
						int box_total = innerLength*innerWidth*innerHeight;
						int box_total_piece = box_total*innerCount; 
						
						DBRow isExitBoxType = floorLPTypeMgrZJ.getDetailBLPType(pc_id,innerLength,innerWidth,innerHeight,ibt_id,container_type_id);
						
						if (isExitBoxType != null)
						{
							throw new BLPHasExistException();
						}
						
						DBRow boxType = new DBRow();
						boxType.add("box_pc_id",pc_id);
						boxType.add("box_total_length",innerLength);
						boxType.add("box_total_width",innerWidth);
						boxType.add("box_total_height",innerHeight);
						boxType.add("box_inner_type",ibt_id);
						boxType.add("box_total",box_total);
						boxType.add("box_total_piece",box_total_piece);
						boxType.add("box_length",box_length);
						boxType.add("box_width",box_width);
						boxType.add("box_height",box_height);
						boxType.add("box_weight",box_weight);
						boxType.add("box_type_sort",box_type_sort);
						boxType.add("container_type_id",containerType.get("type_id",0l));
						boxType.add("box_name",boxTypeName);
						
						long box_type_id = floorLPTypeMgrZJ.addBoxType(boxType);
					}
					catch(NumberFormatException e)
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","InnerLength,InnerWidth,InnerHeight must NumberFormat");
						
						errorList.add(errorBoxType);
					}
					catch(BLPHasExistException e)
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","BLP Type Defined");
						
						errorList.add(errorBoxType);
					}
					catch (InnerContainerTypeNotFoundException e)
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","InnerContainerType Unable to Identify");
						
						errorList.add(errorBoxType);
					}
					catch (ContainerTypeNotFoundException e) 
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","ContainerType Unable to Identify");
						
						errorList.add(errorBoxType);
					}
					catch (ProductNotExistException e)
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","Product Unable to Identify");
						
						errorList.add(errorBoxType);
					}
					catch (Exception e) 
					{
						DBRow errorBoxType = new DBRow();
						errorBoxType.add("p_name",p_name);
						errorBoxType.add("inner_type",innerType);
						errorBoxType.add("inner_type_id",innerTypeId);
						errorBoxType.add("inner_length",innerLengthString);
						errorBoxType.add("inner_width",innerWidthString);
						errorBoxType.add("inner_height",innerHeightString);
						errorBoxType.add("container_type_name",containerTypeName);
						errorBoxType.add("line",i);
						errorBoxType.add("reason","System Error:"+e);
						
						errorList.add(errorBoxType);
					}
				}
			}
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initBLP",log);
		}
	}
	
	/**
	 * 初始化CLP
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public String initCLP(String xml, long ps_id, AdminLoginBean adminLoggerBean)
		throws Exception
	{		
		String result;
		try 
		{
			String[] nodeNames = new String[]{"Pname","Title","Customer","Shipto","InnerType","LoadingLength","LoadingWidth","LoadingHeight","ContainerType","InnerHasSN"};
			
			DBRow requestRow = XmlUtil.initDataXmlStringToDBRowCategory(xml,nodeNames);
			
			String p_name = requestRow.getString("pname");
			String title_name = requestRow.getString("title");
			String customerName = requestRow.getString("customer");
			String ship_to = requestRow.getString("shipto");
			String innerType = requestRow.getString("innertype");
			String innerLengthString = requestRow.getString("loadinglength");
			String innerWidthString = requestRow.getString("loadingwidth");
			String innerHeightString = requestRow.getString("loadingheight");
			String containerTypeName = requestRow.getString("containertype").trim();
			String innerHasSN = requestRow.getString("innerhassn");
			String clp_name = "";
			int level = 0;
			
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			DBRow containerType = floorContainerMgrZYZ.getContainerTypeIdByName(containerTypeName);
			
			int innerCount = 1;
			long box_type_id = 0; // 代表商品本身
			
			if (product==null) {
				throw new ProductNotExistException();
			}
			
			if (containerType==null) {
				throw new ContainerTypeNotFoundException();
			}
			
			long pc_id = product.get("pc_id",0l);
			
			DBRow customer = null;
			if(StringUtils.isNotEmpty(customerName)){
				
				DBRow customer1 = floorProprietaryMgrZyj.getCustomerByName(customerName);
				
				if(customer1 == null ){
					
					result = "Customer Not Found.";
					return result;
					
				} else {
					
					/**是否存在此customer*/
					customer = floorProprietaryMgrZyj.getCustomerByName(customerName,pc_id);
					
					if (customer==null) {
						
						result = "Customer don't have permission to access the products!";
						return result;
					}
				}
			}
			
			//是否存在此title
			DBRow title = null;
			
			if(StringUtils.isNotEmpty(title_name)){
				
				DBRow titleIsExist = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name.toUpperCase());
				
				if( titleIsExist != null){
					
					long adid = 0;
					
					if(DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean) && adid <= 0){
						
						adid = adminLoggerBean.getAdid();
					}
					
					DBRow[] titles = floorProprietaryMgrZyj.findTitlesByAdidAndProduct(adid, pc_id);
					
					for(int i=0; i<titles.length; i++){
						
						if(StringUtils.equals(titles[i].getString("title_name"), title_name)){
							
							title = titles[i];
							break;
						}
					}
					
					if ( title == null ) {
						
						throw new TitleNotFoundException();
					}
					
				} else {
					
					result = "Title Not Found.";
					return result;
				}
			}
			
			//如果title/customer都存在验证是否存在关系
			if(title != null && customer != null){
				
				DBRow titleProduct = floorProprietaryMgrZyj.getDetailTilteProductByPCTitle(pc_id, title.get("title_id",0l), customer.get("customer_key",0l));
				
				if(titleProduct == null){
					
					result = "Title And Customer don't have permission to access the products!";
					return result;
				}
			}
			
			//是否存在此ship to
			DBRow shipTo = null;
			
			if(StringUtils.isNotEmpty(ship_to)){
				
				shipTo = floorShipToMgrZJ.getDetailShipTo(ship_to);
				
				if (shipTo==null) {
					
					throw new ShipToNotExistException();
				}
			}
			
			long title_id = null==title?0:title.get("title_id",0l);
			long ship_to_id = null==shipTo?0:shipTo.get("ship_to_id",0l);
			long customer_id = null==customer?0:customer.get("customer_key",0l);
			
			float pc_weight = product.get("weight", 0F);
			int pc_weight_uom = product.get("weight_uom", 0);
			int innerLength = Integer.parseInt(innerLengthString);
			
			int innerWidth = Integer.parseInt(innerWidthString);
			int innerHeight = Integer.parseInt(innerHeightString);
			long container_type_id = containerType.get("type_id",0l);
			int sku_lp_total_box = innerLength*innerWidth*innerHeight;
			int sku_lp_total_piece = sku_lp_total_box*innerCount;
			int containerTypeUom = containerType.get("weight_uom", 0);
			
			if(StrUtil.isBlank(clp_name)) {
				
				clp_name = "CLP"+innerLength+"*"+innerWidth+"*"+innerHeight+"["+containerTypeName+"]";
			}
			
			int is_has_sn;
			if (innerHasSN.toUpperCase().equals("Y")) {
				
				is_has_sn = YesOrNotKey.YES;
				
			} else {
				
				is_has_sn = YesOrNotKey.NO;
			}
			
			float total_weight = 0F;
			
			if(!innerType.toUpperCase().equals("PRODUCT")) {
				
				DBRow boxType = boxTypeMgrZr.findLpTypeByPcidLpTypeName(pc_id, innerType);
				
				if (boxType==null) {
					throw new InnerContainerTypeNotFoundException();
				}
				
				innerCount = boxType.get("inner_total_pc",0);
				sku_lp_total_piece = sku_lp_total_box*innerCount;
				clp_name += "["+boxType.getString("lp_name")+"]";
				box_type_id = boxType.get("lpt_id", 0L);
				float box_total_weight = boxType.get("total_weight", 0F);
				//换算成一种单位去计算重量
				int innerWeightUom = boxType.get("weight_uom", 0);
				box_total_weight = MoneyUtil.weightUnitConverter(box_total_weight, innerWeightUom, containerTypeUom);
				total_weight = box_total_weight*sku_lp_total_box+containerType.get("weight", 0F);
				level = boxType.get("level", 0)+1;
				
			} else {
				
				pc_weight = MoneyUtil.weightUnitConverter(pc_weight, pc_weight_uom, containerTypeUom);
				//换算成一种单位去计算重量
				total_weight = pc_weight*sku_lp_total_box+containerType.get("weight", 0F);
				level = 1;
			}
			
			DBRow isExistCLP = floorLPTypeMgrZJ.getDetailBLPType(pc_id,innerLength,innerWidth,innerHeight,box_type_id,container_type_id);
		
			long clp_type_id;
			
			//存在clp type
			if (isExistCLP != null) {
				
				clp_type_id = isExistCLP.get("lpt_id",0l);
				
				if (title_id!=0 || ship_to_id!=0 || customer_id!=0) {
					
					/*DBRow clpShipTo = floorLPTypeMgrZJ.getCLPTypeForShipTo(clp_type_id, ship_to_id, title_id, customer_id);
					
					if (clpShipTo!=null) {
						
						throw new CLPHasExistException();
					}*/
					
					boolean flg = floorLPTypeMgrZJ.getCLPTitleShipToInfo(clp_type_id, ship_to_id, title_id, customer_id);
					
					if(flg) {
						
						throw new CLPHasExistException();
					}
					
				} else {
					
					throw new CLPHasExistException();
				}
				
			//不存在clp type
			} else {
				
				DBRow boxType = new DBRow();
				boxType.add("pc_id",pc_id);
				boxType.add("basic_type_id",containerType.get("type_id",0l));
				boxType.add("inner_pc_or_lp",box_type_id);
				boxType.add("stack_length_qty",innerLength);
				boxType.add("stack_width_qty",innerWidth);
				boxType.add("stack_height_qty",innerHeight);
				boxType.add("inner_total_lp",sku_lp_total_box);
				boxType.add("inner_total_pc",sku_lp_total_piece);
				boxType.add("is_has_sn",is_has_sn);
				boxType.add("lp_name",clp_name);
				boxType.add("length_uom",containerType.getString("length_uom"));
				boxType.add("weight_uom",containerType.getString("weight_uom"));
				boxType.add("ibt_length",containerType.get("length", 0F));
				boxType.add("ibt_width",containerType.get("width", 0F));
				boxType.add("ibt_height",containerType.get("height", 0F));
				boxType.add("ibt_weight",containerType.get("weight", 0F));
				boxType.add("active", YesOrNotKey.YES);
				boxType.add("total_weight", total_weight);
				boxType.add("level", level);
				boxType.add("container_type", ContainerTypeKey.CLP);
				
				clp_type_id = floorClpTypeMgrZr.addClpType(boxType);
			}
			
			if (title_id!=0 || ship_to_id!=0 || customer_id!=0) {
				
				DBRow row = new DBRow();
				row.add("lp_type_id",clp_type_id);
	 			row.add("ps_id",ps_id);
	 			row.add("ship_to_id", ship_to_id);
	 			row.add("title_id", title_id);
	 			row.add("customer_id", customer_id);
	 			
	 			// 获取排序
				DBRow sortRow = floorClpTypeMgrZr.findMaxSort(ship_to_id, pc_id,title_id, customer_id);
				int sort=sortRow.get("sort", 0);
				row.add("ship_to_sort",sort+1);
	 			row.add("lp_pc_id",pc_id);
	 			
	 		    floorClpTypeMgrZr.addClpPs(row);
			}
			
			
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",clp_type_id);
			containerNode.put("type_id",clp_type_id);			
			containerNode.put("pc_id",pc_id);
			containerNode.put("basic_type_id",containerType.get("type_id",0l));
			containerNode.put("inner_pc_or_lp",box_type_id);
			containerNode.put("stack_length_qty", innerLength);
			containerNode.put("stack_width_qty", innerWidth);
			containerNode.put("stack_height_qty",innerHeight);			
			containerNode.put("is_has_sn",is_has_sn);
			containerNode.put("inner_total_lp",sku_lp_total_box);
			containerNode.put("inner_total_pc",sku_lp_total_piece);
			containerNode.put("length_uom",containerType.getString("length_uom"));
			containerNode.put("weight_uom",containerType.getString("weight_uom"));		
			containerNode.put("container_type",1);
			containerNode.put("lp_name",clp_name);
			containerNode.put("active",1);
			containerNode.put("ibt_length",containerType.get("length", 0F));
			containerNode.put("ibt_width",containerType.get("width", 0F));
			containerNode.put("ibt_height",containerType.get("height", 0F));
			containerNode.put("ibt_weight",containerType.get("weight", 0F));
			containerNode.put("total_weight",total_weight); 
			containerNode.put("level", level);
			
			if(box_type_id==0l){
				DBRow[] containerProduct=this.floorClpTypeMgrZr.findProductNode(pc_id);
				 DBRow[] hasTitle = proprietaryMgrZyj.getProductHasTitleList(pc_id);
				 
				 for(int i = 0; i < containerProduct.length; i++){
						
						Map<String,Object> productNode = new HashMap<String, Object>();
						productNode.put("pc_id",pc_id);
						
						String titleid = "";
						
						for(int j = 0; j<hasTitle.length; j++){
							
							titleid += ","+hasTitle[j].get("title_id",0l);
						}
						
						productNode.put("title_id",titleid.equals("")?titleid:titleid.substring(1));
						productNode.put("p_name",containerProduct[i].getString("p_name"));
						productNode.put("unit_name",containerProduct[i].getString("unit_name"));
						productNode.put("unit_price",containerProduct[i].getString("unit_price"));
						productNode.put("catalog_id",containerProduct[i].getString("catalog_id"));
						productNode.put("orignal_pc_id", containerProduct[i].get("orignal_pc_id",0l));
						productNode.put("width",containerProduct[i].getString("width"));
						productNode.put("weight",containerProduct[i].getString("weight"));
						productNode.put("heigth", containerProduct[i].getString("heigth"));
						productNode.put("length", containerProduct[i].getString("length"));
						productNode.put("length_nom",containerProduct[i].get("length_uom",0l));
						productNode.put("weight_nom",containerProduct[i].get("weight_uom",0l));
						productNode.put("price_uom",containerProduct[i].getString("price_uom"));
						//productNode.put("p_code", containerProduct[i].getString("p_code"));
						productNode.put("union_flag",containerProduct[i].get("union_flag",0));
						productNode.put("alive",containerProduct[i].get("alive",0));
						
						Map<String,Object> relProps = new HashMap<String, Object>();
						//	relProps.put("locked_quantity",sku_lp_total_piece);
						relProps.put("stack_length_qty",innerLength);
						relProps.put("stack_width_qty",innerWidth);
						relProps.put("stack_height_qty",innerHeight);
						relProps.put("total_weight",total_weight);
						relProps.put("weight_nom",containerType.getString("weight_uom"));
						
						
						Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
						searchContainerNodes.put("con_id", clp_type_id);
						Map<String,Object> searchProductNodes = new HashMap<String, Object>();
						searchProductNodes.put("pc_id", pc_id);
						DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
						DBRow[]  existProductNode =	productStoreMgr.searchNodes(1, NodeType.Product,searchProductNodes);
						
						if(existContainerNode.length>0){
							productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加商品节点
						}else{
							productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
						}
						if(existProductNode.length>0){
							productStoreMgr.updateNode(1,NodeType.Product,searchProductNodes,productNode);//添加商品节点
						}else{
							productStoreMgr.addNode(1,NodeType.Product,productNode);//添加商品节点
						}
						
						productStoreMgr.addRelation(1,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);//添加容易与商品的关系
						
					}
						
					}else{

						DBRow tocon=this.floorClpTypeMgrZr.findContainerProperty(box_type_id);

						
						Map<String,Object> toContainerNode = new HashMap<String, Object>();
						toContainerNode.put("con_id",box_type_id);
						toContainerNode.put("type_id",box_type_id);	
						
					    toContainerNode.put("pc_id",tocon.get("pc_id",01));			  
				        //toContainerNode.put("sort",tocon.get("sort",01));
					    toContainerNode.put("basic_type_id",tocon.get("basic_type_id",01));
						toContainerNode.put("inner_pc_or_lp",tocon.get("inner_pc_or_lp",01));
						toContainerNode.put("is_has_sn",tocon.get("is_has_sn",01));
						toContainerNode.put("inner_total_lp",tocon.get("inner_total_lp",01));
						toContainerNode.put("inner_total_pc",tocon.get("inner_total_pc",01));
						//toContainerNode.put("length_uom",tocon.get("length_uom",01));
						//toContainerNode.put("weight_uom",tocon.get("weight_uom",01));		
						toContainerNode.put("container_type",tocon.get("container_type",01));
					    toContainerNode.put("lp_name",tocon.getString("lp_name"));
						toContainerNode.put("active",tocon.get("active",01));
						toContainerNode.put("ibt_length",tocon.get("ibt_length",01));
						toContainerNode.put("ibt_width",tocon.get("ibt_width",01));
						toContainerNode.put("ibt_height",tocon.get("ibt_height",01));
						//	toContainerNode.put("ibt_weight",tocon.get("ibt_weight",01));
						//toContainerNode.put("total_weight",tocon.get("total_weight",01));
						toContainerNode.put("stack_length_qty", tocon.get("stack_length_qty",01));
						toContainerNode.put("stack_width_qty", tocon.get("stack_width_qty",01));
						toContainerNode.put("stack_height_qty", tocon.get("stack_height_qty",01)); 
						

						
						Map<String,Object> ContainerNodeConcern = new HashMap<String, Object>();
						ContainerNodeConcern.put("quantity", sku_lp_total_piece);
						ContainerNodeConcern.put("stack_length_qty", innerLength);
						ContainerNodeConcern.put("stack_width_qty", innerWidth);
						ContainerNodeConcern.put("stack_height_qty", innerHeight);
						ContainerNodeConcern.put("total_weight", total_weight);
						
						Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
						searchContainerNodes.put("con_id", clp_type_id);
						
						Map<String,Object> searchToContainerNodes = new HashMap<String, Object>();
						searchToContainerNodes.put("con_id", box_type_id);
						DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
						DBRow[]  existToContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchToContainerNodes);

						if(existContainerNode.length>0){
							productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加商品节点
						}else{
							productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
						}
						
						if(existToContainerNode.length>0){
							productStoreMgr.updateNode(1,NodeType.Container,searchToContainerNodes,toContainerNode);//添加商品节点
						}else{
							productStoreMgr.addNode(1,NodeType.Container,toContainerNode);//添加容器节点
						}
						
						
						productStoreMgr.addRelation(1,NodeType.Container,NodeType.Container,containerNode,toContainerNode,ContainerNodeConcern);
						
			}
			
			result = "Success";
			
		}
		catch (TitleNotFoundException e) 
		{
			result = "Title don't have permission to access the products!";
		}
		catch (CLPHasExistException e) 
		{
			result = "CLP Type Defined";
		}
		catch (ProductNotExistException e) 
		{
			result = "Product Unable to Identify";
		}
		catch (ContainerTypeNotFoundException e) 
		{
			result = "Packaging Type Unable to Identify";
		}
		catch (NumberFormatException e) 
		{
			result = "StackLength,StackWidth,StackHeight Must Be Number Format";
		}
		catch (InnerContainerTypeNotFoundException e) 
		{
			result = "Inner CLP Type Unable to Identify";
		}
		catch (ShipToNotExistException e)
		{
			result = "ShipTo Unable to Identify";
		}
		catch (Exception e) 
		{
			result = "System Error:"+e;
		}
		
		return result;
	}
	
	public ArrayList<DBRow> initCLP(Workbook rwb)
		throws Exception
	{
		try 
		{
			long ps_id = 0L;
			
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("CLP");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				
				if (row!=null)
				{
					String sku = this.getCellValue(row.getCell(0));
					String lot = this.getCellValue(row.getCell(1));
					String p_name = this.getCellValue(row.getCell(2));
					String ship_to = this.getCellValue(row.getCell(3));
					String innerType = this.getCellValue(row.getCell(4));
					String boxName = this.getCellValue(row.getCell(5));
						String innerLengthString = this.getCellValue(row.getCell(6));
						String innerWidthString = this.getCellValue(row.getCell(7));
					String innerHeightString = this.getCellValue(row.getCell(8));
					String containerTypeName = this.getCellValue(row.getCell(11));
					String title_name = this.getCellValue(row.getCell(10)).toUpperCase();
					
					String[] rowValues = new String[]{lot,sku,p_name,ship_to,innerType,boxName,innerLengthString,innerWidthString,innerHeightString,containerTypeName,title_name};
					
					if (rowIsNull(rowValues))//纯空行不操作
					{
						continue;
					}
					
					DBRow product = floorProductMgr.getDetailProductByPname(p_name);
					DBRow title = floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);
					DBRow containerType = floorContainerMgrZYZ.getContainerTypeIdByName(containerTypeName);
					DBRow shipTo = floorShipToMgrZJ.getDetailShipTo(ship_to);
					boolean createCLPPS = true;

					int innerCount = 1;
					long box_type_id = 0;
					
					try 
					{
						if (product==null)
						{
							throw new ProductNotExistException();
						}
						if (containerType==null)
						{
							throw new ContainerTypeNotFoundException();
						}
						if (title==null)
						{
							throw new TitleNotFoundException();
						}
						if (shipTo==null&&createCLPPS)
						{
							throw new ShipToNotExistException();
						}
						
						long pc_id = product.get("pc_id",0l);
						long title_id = title.get("title_id",0l);
						int innerLength = Integer.parseInt(innerLengthString);
						int innerWidth = Integer.parseInt(innerWidthString);
						int innerHeight = Integer.parseInt(innerHeightString);
						long container_type_id = containerType.get("type_id",0l);
						int sku_lp_total_box = innerLength*innerWidth*innerHeight;
						int sku_lp_total_piece = sku_lp_total_box*innerCount;
						long ship_to_id = shipTo.get("ship_to_id",0l);
						
						if (innerType.toUpperCase().equals("BLP"))
						{
							DBRow boxType = floorLPTypeMgrZJ.getDetailBLPTypeByBLPName(boxName);
							
							if (boxType==null)
							{
								throw new InnerContainerTypeNotFoundException();
							}
								
							innerCount = boxType.get("box_total_piece",0);
						}
						
						DBRow isExistCLP = floorLPTypeMgrZJ.getDetaiCLP(pc_id,container_type_id,innerLength,innerWidth,innerHeight,box_type_id,title_id);
						long clp_type_id;
						
						if (isExistCLP!=null)
						{
							clp_type_id = isExistCLP.get("sku_lp_type_id",0l);
							
							if (createCLPPS) 
							{
								DBRow clpShipTo = floorLPTypeMgrZJ.getCLPTypeForShipTo(clp_type_id,ship_to_id, title_id);
								if (clpShipTo!=null)
								{
									throw new CLPHasExistException();
								}
							}
							else
							{
								throw new CLPHasExistException();
							}
						}
						else
						{
							DBRow clp = new DBRow();
							clp.add("sku_lp_pc_id",pc_id);
							clp.add("sku_lp_title_id",title_id);
							clp.add("lp_type_id",container_type_id);
							clp.add("sku_lp_box_type",box_type_id);
							clp.add("sku_lp_box_length",innerLength);
							clp.add("sku_lp_box_width",innerWidth);
							clp.add("sku_lp_box_height",innerHeight);
							clp.add("sku_lp_total_box",sku_lp_total_box);
							clp.add("sku_lp_total_piece",sku_lp_total_piece);
							
							clp_type_id = floorLPTypeMgrZJ.addClpType(clp);
						}
						
						if (createCLPPS) 
						{
							this.addCLPPs(clp_type_id,pc_id,ship_to_id, title_id, ps_id);
						}
						
					} 
					catch (TitleNotFoundException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","Title Unable to Identify");

						errorList.add(errorCLP);
					}
					catch (CLPHasExistException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","CLP Type Defined");

						errorList.add(errorCLP);
					}
					catch (ProductNotExistException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","Product Unable to Identify");
						
						errorList.add(errorCLP);
					}
					catch (ContainerTypeNotFoundException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","ContainerType Unable to Identify");
						
						errorList.add(errorCLP);
					}
					catch (NumberFormatException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","InnerLength,InnerWidth,InnerHeight must NumberFormat");
						
						errorList.add(errorCLP);
					}
					catch (InnerContainerTypeNotFoundException e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","InnerContainerType Unable to Identify");
						
						errorList.add(errorCLP);
					}
					catch (ShipToNotExistException e)
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","ShipTo Unable to Identify");
						
						errorList.add(errorCLP);
					}
					catch (Exception e) 
					{
						DBRow errorCLP = new DBRow();
						errorCLP.add("p_name",p_name);
						errorCLP.add("ship_to",ship_to);
						errorCLP.add("inner_type",innerType);
						errorCLP.add("box_type",boxName);
						errorCLP.add("inner_length",innerLengthString);
						errorCLP.add("inner_width",innerWidthString);
						errorCLP.add("inner_height",innerHeightString);
						errorCLP.add("title",title_name);
						errorCLP.add("container_type",containerTypeName);
						errorCLP.add("line",i);
						errorCLP.add("reason","System Error:"+e);
						
						errorList.add(errorCLP);
						e.printStackTrace();
					}
				}
			}
			
			return errorList;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initCLP",log);
		}
	}
	
	/**
	 * 初始化ShipTo
	 * @param rwb
	 * @throws Exception
	 */
	public long initShipTo(String ship_to_name)
		throws Exception
	{
		DBRow shipTo = new DBRow();
		shipTo.add("ship_to_name",ship_to_name);
			
		return floorShipToMgrZJ.addShipTo(shipTo);
	}
	
	public ArrayList<DBRow> initShipTo(Workbook rwb)
		throws Exception
	{
		ArrayList<DBRow> errorList = new ArrayList<DBRow>();
		Sheet sheet = (Sheet) rwb.getSheet("ShipTo");
		int rsRows = sheet.getPhysicalNumberOfRows();
		
		for (int i = 1; i < rsRows; i++) 
		{
			Row row = sheet.getRow(i);
			
			if (row!=null)
			{
				String ship_to_name = this.getCellValue(row.getCell(0));
				String[] rowValues = new String[]{ship_to_name};
				
				if (rowIsNull(rowValues))
				{
					continue;
				}
				
				DBRow isExistShipTo = floorShipToMgrZJ.getDetailShipTo(ship_to_name);
				try 
				{
					if (isExistShipTo!=null)
					{
						throw new ShipToHasExistException();
					}
					DBRow shipTo = new DBRow();
					shipTo.add("ship_to_name",ship_to_name);
					
					floorShipToMgrZJ.addShipTo(shipTo);
				}
				catch (ShipToHasExistException e) 
				{
					DBRow errorShipTo = new DBRow();
					errorShipTo.add("ship_to_name",ship_to_name);
					errorShipTo.add("line",i);
					errorShipTo.add("reason","ShipTo Defined");
					
					errorList.add(errorShipTo);
				}
				catch (Exception e) 
				{
					DBRow errorShipTo = new DBRow();
					errorShipTo.add("ship_to_name",ship_to_name);
					errorShipTo.add("line",i);
					errorShipTo.add("reason","System Error"+e);
					
					errorList.add(errorShipTo);
				}
			}
		}
		
		return errorList;
	}
	
	public String initShipToAddress(String xml)
			throws Exception
		{
			String result;
			try
			{
				String[] nodeNames = new String[]{"AccountID","Name","Address1","Address2","City","State","ZipCode","Country"}; 
				
				DBRow requestRow = XmlUtil.initDataXmlStringToDBRowCategory(xml,nodeNames);

				String ship_to_name = requestRow.getString("accountid");
				String storage_title = requestRow.getString("name");
				String home_number = requestRow.getString("address1");
				String street = requestRow.getString("address2");
				
				String city = requestRow.getString("city");
				String state = requestRow.getString("state");
				String zip_code = requestRow.getString("zipcode");
				String country_code = requestRow.getString("country");
				
				
				DBRow shipTo = floorShipToMgrZJ.getDetailShipTo(ship_to_name);
				long ship_to_id;
				if (shipTo==null)
				{
					ship_to_id = initShipTo(ship_to_name);
				}
				else
				{
					ship_to_id = shipTo.get("ship_to_id",0l);
				}
				int storage_type = StorageTypeKey.RETAILER;
				
				DBRow[] checkAddressTitle = floorShipToMgrZJ.getStorageCatalogOfShipToByTitle(storage_title, ship_to_id, storage_type);
						//floorCatalogMgr.getProductStorageDetailCatalogByTitle(storage_title);
				
				if (checkAddressTitle.length > 0)
				{
					throw new StorageCatalogIsExistException();
				}
				
				DBRow country = floorCountryMgrZJ.getDetailCountryByCountryCode(country_code);
				
				if (country == null) 
				{
					country = floorCountryMgrZJ.getDetailCountryByCountryName(country_code);
					
					if(country == null){
						throw new CountryNotExistException();
					}
					
				}
				
				long nation_id = country.get("ccid",0l);
				String nation_name = country.getString("c_country");
				
				DBRow province = floorCountryMgrZJ.getDetailCountryProvinceByProvinceCode(state,nation_id);
				String pro_name = "";
				long pro_id = -1;
				if (province==null)
				{
					pro_name = state;
				}
				else
				{
					pro_id = province.get("pro_id",0l);
					pro_name = province.getString("pro_name");
				}
			
				
				long storageId = addProductStorageCatalogShiTo(storage_title, zip_code,null,null, nation_id,pro_id, city, home_number, street, pro_name,ship_to_id, storage_type);
				
				ShipToIndexMgr.getInstance().addIndex(ship_to_id);
				ShipToStorageIndexMgr.getInstance().addIndex(storageId, storage_title, city, street, home_number, zip_code, "", nation_name, pro_name, pro_name, ship_to_id+"", ship_to_name, StorageTypeKey.RETAILER+"");
				result = "Success";
			}
			catch (StorageCatalogIsExistException e) 
			{
				result = "Ship To Address Name Repeat";
			}
			catch (CountryNotExistException e) 
			{
				result = "Nation not find";
			}
			catch (CountryProvinceNotExistException e) 
			{
				result = "Province not find";
				
			}
			catch (Exception e) 
			{
				result = "System Error"+e;
			}
			
			return result;
		}
		
	
	
	public ArrayList<DBRow> initShipToAddress(Workbook rwb)
		throws Exception
	{
		try 
		{
			ArrayList<DBRow> errorList = new ArrayList<DBRow>();
			Sheet sheet = (Sheet) rwb.getSheet("ShipToAddress");
			int rsRows = sheet.getPhysicalNumberOfRows();
			
			for (int i = 1; i < rsRows; i++) 
			{
				Row row = sheet.getRow(i);
				
				if (row!=null)
				{
					String ship_to_name = this.getCellValue(row.getCell(0));
					String storage_title = this.getCellValue(row.getCell(1));
					String street = this.getCellValue(row.getCell(2));
					String home_number = this.getCellValue(row.getCell(3));
					String city = this.getCellValue(row.getCell(4));
					String state = this.getCellValue(row.getCell(5));
					String zip_code = this.getCellValue(row.getCell(6));
					String country_code = this.getCellValue(row.getCell(7));
					
					String[] rowValues = new String[]{ship_to_name,storage_title,street,home_number,city,state,zip_code,country_code};
					
					if (rowIsNull(rowValues))
					{
						continue;
					}
					
					try
					{
						DBRow isExistProductStorageCatalog = floorCatalogMgr.getProductStorageDetailCatalogByTitle(storage_title);
						
						if (isExistProductStorageCatalog!=null)
						{
							throw new StorageCatalogIsExistException();
						}
						
						DBRow country = floorCountryMgrZJ.getDetailCountryByCountryCode(country_code);
						if (country==null) 
						{
							throw new CountryNotExistException();
						}
						
						long nation_id = country.get("ccid",0l);
						DBRow province = floorCountryMgrZJ.getDetailCountryProvinceByProvinceCode(state,nation_id);
						if (province==null)
						{
							throw new CountryProvinceNotExistException();
						}
						
						long pro_id = province.get("pro_id",0l);
						String pro_name = province.getString("pro_name");
						
						DBRow shipTo = floorShipToMgrZJ.getDetailShipTo(ship_to_name);
						
						if (shipTo==null)
						{
							throw new ShipToNotExistException();
						}
						
						long ship_to_id = shipTo.get("ship_to_id",0l);
						int storage_type = StorageTypeKey.RETAILER;
						this.addProductStorageCatalogShiTo(storage_title, zip_code,null,null, nation_id, pro_id, city, home_number, street, pro_name,ship_to_id, storage_type);
					}
					catch (StorageCatalogIsExistException e) 
					{
						DBRow errorShipToAddress = new DBRow();
						errorShipToAddress.add("ship_to_name",ship_to_name);
						errorShipToAddress.add("storage_title",storage_title);
						errorShipToAddress.add("street",street);
						errorShipToAddress.add("home_number",home_number);
						errorShipToAddress.add("city",city);
						errorShipToAddress.add("state",state);
						errorShipToAddress.add("zip_code",zip_code);
						errorShipToAddress.add("country_code",country_code);
						errorShipToAddress.add("reason","ShipToAddress Name Repeat");
						errorShipToAddress.add("line",i);
						
						errorList.add(errorShipToAddress);
					}
					catch (CountryNotExistException e) 
					{
						DBRow errorShipToAddress = new DBRow();
						errorShipToAddress.add("ship_to_name",ship_to_name);
						errorShipToAddress.add("storage_title",storage_title);
						errorShipToAddress.add("street",street);
						errorShipToAddress.add("home_number",home_number);
						errorShipToAddress.add("city",city);
						errorShipToAddress.add("state",state);
						errorShipToAddress.add("zip_code",zip_code);
						errorShipToAddress.add("country_code",country_code);
						errorShipToAddress.add("reason","Country not find");
						errorShipToAddress.add("line",i);
						
						errorList.add(errorShipToAddress);
					}
					catch (CountryProvinceNotExistException e) 
					{
						DBRow errorShipToAddress = new DBRow();
						errorShipToAddress.add("ship_to_name",ship_to_name);
						errorShipToAddress.add("storage_title",storage_title);
						errorShipToAddress.add("street",street);
						errorShipToAddress.add("home_number",home_number);
						errorShipToAddress.add("city",city);
						errorShipToAddress.add("state",state);
						errorShipToAddress.add("zip_code",zip_code);
						errorShipToAddress.add("country_code",country_code);
						errorShipToAddress.add("reason","Province not find");
						errorShipToAddress.add("line",i);
						
						errorList.add(errorShipToAddress);
					}
					catch (ShipToNotExistException e) 
					{
						DBRow errorShipToAddress = new DBRow();
						errorShipToAddress.add("ship_to_name",ship_to_name);
						errorShipToAddress.add("storage_title",storage_title);
						errorShipToAddress.add("street",street);
						errorShipToAddress.add("home_number",home_number);
						errorShipToAddress.add("city",city);
						errorShipToAddress.add("state",state);
						errorShipToAddress.add("zip_code",zip_code);
						errorShipToAddress.add("country_code",country_code);
						errorShipToAddress.add("reason","ShipTo Not Exist");
						errorShipToAddress.add("line",i);
						
						errorList.add(errorShipToAddress);
					}
					catch (Exception e) 
					{
						DBRow errorShipToAddress = new DBRow();
						errorShipToAddress.add("ship_to_name",ship_to_name);
						errorShipToAddress.add("storage_title",storage_title);
						errorShipToAddress.add("street",street);
						errorShipToAddress.add("home_number",home_number);
						errorShipToAddress.add("city",city);
						errorShipToAddress.add("state",state);
						errorShipToAddress.add("zip_code",zip_code);
						errorShipToAddress.add("country_code",country_code);
						errorShipToAddress.add("reason","System Error"+e);
						errorShipToAddress.add("line",i);
						
						errorList.add(errorShipToAddress);
					}
				}
			}
			
			return errorList;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"initShipToAddress",log);
		}
	}
	
	private long addProductStorageCatalogShiTo(String storage_title,String zip_code,String contact,String phone,long nation_id,long pro_id,String city,String home_number,String street,String pro_name,long storage_type_id, int storage_type)
		throws Exception
	{
		DBRow storageCatalog = new DBRow();
		storageCatalog.add("title",storage_title);
		storageCatalog.add("send_zip_code",zip_code);
		storageCatalog.add("contact",null);
		storageCatalog.add("phone",null);
		storageCatalog.add("native",nation_id);
		storageCatalog.add("deliver_zip_code",zip_code);
		storageCatalog.add("pro_id",pro_id);
		storageCatalog.add("city",city);
		storageCatalog.add("send_nation",nation_id);
		storageCatalog.add("send_pro_id",pro_id);
		storageCatalog.add("send_city",city);
		storageCatalog.add("send_house_number",home_number);
		storageCatalog.add("send_street",street);
		storageCatalog.add("send_contact",null);
		storageCatalog.add("send_phone",null);
		storageCatalog.add("send_pro_input",pro_name);
		storageCatalog.add("deliver_nation",nation_id);
		storageCatalog.add("deliver_pro_id",pro_id);
		storageCatalog.add("deliver_city",city);
		storageCatalog.add("deliver_house_number",home_number);
		storageCatalog.add("deliver_street",street);
		storageCatalog.add("deliver_contact",null);
		storageCatalog.add("deliver_phone",null);
		storageCatalog.add("deliver_pro_input",pro_name);
		storageCatalog.add("storage_type",storage_type);
	      storageCatalog.add("storage_type_id",storage_type_id);
		
		return floorCatalogMgr.addProductStorageCatalog(storageCatalog);
	}
	
	private String getCellValue(Cell cell)
		throws Exception
	{
		String cellValue = ""; 
		try 
		{
			if (cell!=null)
			{
				if (cell.getCellType()==Cell.CELL_TYPE_NUMERIC)
				{
					 DecimalFormat decimalFormat = new DecimalFormat("###");
					 double value = cell.getNumericCellValue();
					 
					 cellValue = decimalFormat.format(value);
				}
				else if(cell.getCellType() == Cell.CELL_TYPE_STRING)
				{
					cellValue = cell.getStringCellValue();
				}
				else if(cell.getCellType() == Cell.CELL_TYPE_FORMULA)
				{
					cellValue = cell.getStringCellValue();
				}
				else if(cell.getCellType() == Cell.CELL_TYPE_BLANK)
				{
					cellValue = "";
				}
				else
				{
					cellValue = String.valueOf(cell.getCellType());
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return cellValue.toUpperCase().replaceAll(" ","");
	}
	
	public DBRow errorProductRow(String lotNumber,String sku,String key_code,String category,String upc,String unit_name,String lengthString,String heightString,String widthString,String weightString,String unitCost,String freightClass,String nmfc_code)
		throws Exception
	{
		DBRow error = new DBRow();
		error.add("lot",lotNumber);
		error.add("sku",sku);
		error.add("key_code",key_code);
		error.add("category",category);
		error.add("upc",upc);
		error.add("uom",unit_name);
		error.add("length",lengthString);
		error.add("height",heightString);
		error.add("width",widthString);
		error.add("weight",weightString);
		error.add("unit_price",unitCost);
		error.add("freight_class",freightClass);
		error.add("nmfc_code",nmfc_code);
		
		return error;
	}
	
	private long addCLPPs(long clp_type_id,long pc_id,long ship_to_id, long title_id, long ps_id)
		throws Exception
	{
		try 
		{
			DBRow clpShipTo = new DBRow();
			clpShipTo.add("clp_type_id",clp_type_id);
			clpShipTo.add("lp_pc_id",pc_id);
			clpShipTo.add("ship_to_sort",floorLPTypeMgrZJ.findMaxSort(pc_id,ship_to_id)+1);
			clpShipTo.add("ship_to_id",ship_to_id);
			clpShipTo.add("title_id", title_id);
			clpShipTo.add("ps_id", ps_id);
			return floorLPTypeMgrZJ.addCLPPs(clpShipTo);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addCLPPs",log);
		}
	}
	
	private boolean rowIsNull(String[] rowValues)
		throws Exception
	{
		boolean isNull = true;
		
		for (int i = 0; i < rowValues.length; i++) 
		{
			if (rowValues[i].length()>0) 
			{
				isNull=false;
				break;
			}
		}
		
		return isNull;
	}
	
	private String getPost(InputStream inputStream)
			throws Exception
		{
			BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
			byte[] buffer = new byte[1024];                                             //数据缓冲区
			int count = 0;                                                            //每个缓冲区的实际数据长度
			ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
			byte[] iXMLData = null;   
			try 
			{
				input = new BufferedInputStream(inputStream);
				while ((count = input.read(buffer)) != -1)
				{
				    streamXML.write(buffer, 0, count);
				} 
			 }
			 catch (Exception e)
			 {
				e.printStackTrace();
			 }
			 finally
			 {
				if(input != null)
				{
				    try 
				    {
				     input.close();
				    }
				    catch (Exception f)
				    {
				    	f.printStackTrace();
				    }
				 }
			}
			iXMLData = streamXML.toByteArray();  
			
			return (new String(iXMLData));
		}
	
	
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setFloorProductCodeMgrZJ(FloorProductCodeMgrZJ floorProductCodeMgrZJ) {
		this.floorProductCodeMgrZJ = floorProductCodeMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}

	public void setFloorProprietaryMgrZyj(
			FloorProprietaryMgrZyj floorProprietaryMgrZyj) {
		this.floorProprietaryMgrZyj = floorProprietaryMgrZyj;
	}

	public void setFloorContainerMgrZYZ(FloorContainerMgrZYZ floorContainerMgrZYZ) {
		this.floorContainerMgrZYZ = floorContainerMgrZYZ;
	}

	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}

	public void setBoxTypeMgrZr(BoxTypeMgrZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}

	public void setFloorShipToMgrZJ(FloorShipToMgrZJ floorShipToMgrZJ) {
		this.floorShipToMgrZJ = floorShipToMgrZJ;
	}

	public void setFloorCountryMgrZJ(FloorCountryMgrZJ floorCountryMgrZJ) {
		this.floorCountryMgrZJ = floorCountryMgrZJ;
	}

	public void setFloorProductCatalogMgrZJ(
			FloorProductCatalogMgrZJ floorProductCatalogMgrZJ) {
		this.floorProductCatalogMgrZJ = floorProductCatalogMgrZJ;
	}

	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}
	
	public void setFloorProductLineMgrTJH(
			FloorProductLineMgrTJH floorProductLineMgrTJH) {
		this.floorProductLineMgrTJH = floorProductLineMgrTJH;
	}
	
	public void setFloorClpTypeMgrZr(FloorClpTypeMgrZr floorClpTypeMgrZr) {
		this.floorClpTypeMgrZr = floorClpTypeMgrZr;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
