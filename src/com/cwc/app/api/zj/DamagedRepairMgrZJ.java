package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderDetailsException;
import com.cwc.app.exception.damagedRepair.NoExistDamagedRepairOrderException;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.transport.NoExistTransportDetailException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorDamagedRepairMgrZJ;
import com.cwc.app.floor.api.zj.FloorDamagedRepairOutboundMgrZJ;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.DamagedRepairMgrIFaceZJ;
import com.cwc.app.key.DamagedRepairKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class DamagedRepairMgrZJ implements DamagedRepairMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorDamagedRepairMgrZJ floorDamagedRepairMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ;
	private ProductMgrIFace productMgr;
	
	
	/**
	 * 增加返修转运单
	 * @param request
	 * @throws Exception
	 */
	public void addDamagedRepair(HttpServletRequest request)
		throws Exception 
	{
		try 
		{	
			long send_psid = StringUtil.getLong(request,"send_psid");
			long receive_psid = StringUtil.getLong(request,"receive_psid");
			
			String repair_address = StringUtil.getString(request,"repair_address");
			String repair_linkman = StringUtil.getString(request,"repair_linkman");
			String repair_linkman_phone = StringUtil.getString(request,"repair_linkman_phone");
			
			DBRow damaged_repair = new DBRow();
			
			damaged_repair.add("send_psid",send_psid);
			damaged_repair.add("receive_psid",receive_psid);
			damaged_repair.add("repair_address",repair_address);
			damaged_repair.add("repair_linkman",repair_linkman);
			damaged_repair.add("repair_linkman_phone",repair_linkman_phone);
			damaged_repair.add("repair_status",DamagedRepairKey.READY);
			damaged_repair.add("repair_date",DateUtil.NowStr());
			damaged_repair.add("repair_create_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAccount());//转运单的创建者
			
			floorDamagedRepairMgrZJ.addDamagedRepairOrder(damaged_repair);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addDamagedRepair",log);
		}
	}

	
	/**
	 *	删除返修转运单 
	 * @param request
	 * @throws Exception
	 */
	public void delDamagedRepair(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long repair_id = StringUtil.getLong(request,"repair_id");
			
			floorDamagedRepairMgrZJ.delDamagedRepairOrder(repair_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delDamagedRepair",log);
		}
	}

	/**
	 *	修改返修转运单 
	 * @param request
	 * @throws Exception
	 */
	public void modDamagedRepair(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long repair_id = StringUtil.getLong(request,"repair_id");
			
			long send_psid = StringUtil.getLong(request,"send_psid");
			long receive_psid = StringUtil.getLong(request,"receive_psid");
			String repair_address = StringUtil.getString(request,"repair_address");
			String repair_linkman = StringUtil.getString(request,"repair_linkman");
			String repair_linkman_phone = StringUtil.getString(request,"linkman_phone");
			
			String repair_waybill_name = StringUtil.getString(request,"repair_waybill_name");
			String repair_waybill_number = StringUtil.getString(request,"repair_waybill_number");
			
			DBRow damaged_repair = new DBRow();
			
			damaged_repair.add("send_psid",send_psid);
			damaged_repair.add("receive_psid",receive_psid);
			damaged_repair.add("repair_address",repair_address);
			damaged_repair.add("repair_linkman",repair_linkman);
			damaged_repair.add("repair_linkman_phone",repair_linkman_phone);
			damaged_repair.add("repair_waybill_name",repair_waybill_name);
			damaged_repair.add("repair_waybill_number",repair_waybill_number);
			
			floorDamagedRepairMgrZJ.modDamagedRepairOrder(repair_id,damaged_repair);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modDamagedRepair",log);
		}
	}

	/**
	 * 过滤返修单
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterDamagedRepair(long send_psid, long receive_psid,int status, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorDamagedRepairMgrZJ.filterDamagedRepairOrder(send_psid,receive_psid,status,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterDamagedRepair",log);
		}
	}
	
	
	/**
	 * jqgrid获得返修单明细
	 * @param repair_id
	 * @param pc
	 * @param sidx
	 * @param sord
	 * @param fillterBean
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairDetailsByRepairId(long repair_id,PageCtrl pc, String sidx, String sord, FilterBean fillterBean)
		throws Exception 
	{
		
		try 
		{
			return (floorDamagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id, pc, sidx, sord, fillterBean));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDamagedRepairDetailsByRepairId",log);
		}
	}
	
	/**
	 * 根据返修单ID获得返修详细
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDamagedRepair(long repair_id) 
		throws Exception
	{
		
		try 
		{
			return floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailDamagedRepair",log);
		}
	}
	
	/**
	 * 增加转返修单详细（单个）jqgrid的form增加
	 * @param request
	 * @throws Exception
	 */
	public void addDamagedRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String repair_p_name = StringUtil.getString(request,"p_name");
			float repair_count = StringUtil.getFloat(request,"repair_count");
			long repair_id = StringUtil.getLong(request,"repair_id");
			String repair_box = StringUtil.getString(request,"repair_box");
			
			
			DBRow product = floorProductMgr.getDetailProductByPname(repair_p_name);
			if(product==null)
			{
				throw new ProductNotExistException();
			}
			
			long repair_pc_id = product.get("pc_id",0l);
			String repair_p_code = product.getString("p_code");
			
			DBRow damagedRepairDetail = new DBRow();
			damagedRepairDetail.add("repair_p_name",repair_p_name);
			damagedRepairDetail.add("repair_count",repair_count);
			damagedRepairDetail.add("repair_id",repair_id);
			damagedRepairDetail.add("repair_box",repair_box);
			damagedRepairDetail.add("repair_pc_id",repair_pc_id);
			damagedRepairDetail.add("repair_p_code",repair_p_code);
			
			floorDamagedRepairMgrZJ.addDamagedRepairDetail(damagedRepairDetail);
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addDamagedRepairDetail",log);
		}
	}
	
	
	/**
	 * 修改返修单明细 
	 * @param request
	 * @throws Exception
	 */
	public void modDamagedRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try {
			long repair_detail_id = StringUtil.getLong(request,"id");//grid默认参数是ID
			Map parameter = request.getParameterMap();
			float repair_count;
			String transport_box;
			String repair_p_name;
			DBRow para = new DBRow();
			if(parameter.containsKey("repair_count"))
			{
				repair_count = StringUtil.getFloat(request,"repair_count");
				para.add("repair_count",repair_count);
			}
			
			if(parameter.containsKey("repair_box"))
			{
				transport_box = StringUtil.getString(request,"repair_box");
				para.add("repair_box",transport_box);
			}
			if(parameter.containsKey("p_name"))
			{
				repair_p_name = StringUtil.getString(request,"p_name");
				
				DBRow product = floorProductMgr.getDetailProductByPname(repair_p_name);
				if(product == null)
				{
					throw new ProductNotExistException();
				}
				
				long product_id = product.get("pc_id",0l);
				
				DBRow damaged_repair_detail = floorDamagedRepairMgrZJ.getDetailDamagedRepairDetail(repair_detail_id);
				long repair_id = damaged_repair_detail.get("repair_id",0l);
				
				DBRow productRepair = floorDamagedRepairMgrZJ.getDetailDamagedRepairDetailByPcid(repair_id, product_id); 
				if(productRepair !=null)
				{
					throw new RepeatProductException();
				}
				para.add("repair_pc_id",product.get("pc_id",0l));
				para.add("repair_p_code",product.getString("p_code"));
				para.add("repair_p_name",product.getString("p_name"));
			}
			floorDamagedRepairMgrZJ.modDamagedRepairDetail(repair_detail_id, para);
		} 
		catch (ProductNotExistException e) 
		{
			throw e;
		}
		catch(RepeatProductException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 删除返修明细
	 * @param request
	 * @throws Exception
	 */
	public void delDamagedRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long repair_detail_id = StringUtil.getLong(request,"id");//id因为jqgrid因素
			
			floorDamagedRepairMgrZJ.delDamagedRepairDetail(repair_detail_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delDamagedRepairDetail",log);
		}
	}
	
	
	/**
	 * 获得返修单明细根据ID
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getDamagedRepairDetailById(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long repair_detail_id = StringUtil.getLong(request,"repair_detail_id");
			
			return (floorDamagedRepairMgrZJ.getDetailDamagedRepairDetail(repair_detail_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getdamagedRepairDetailById",log);
		}
	}
	
	/**
	 * 上传返修明细
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public String[] uploadDamagedRepairDetail(HttpServletRequest request)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String[] msg=new String[2];
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long repair_id = Long.parseLong(upload.getRequestRow().getString("repair_id"));
			
			msg[1]=String.valueOf(repair_id);
				
			if (flag==2)
			{
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg[0] = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadDamagedRepairDetail",log);
		}
	}
	
	/**
	 * 将excel文件转换成DBRow[]
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelshow(String filename,String type)
		throws Exception
	{
		try 
		{
			HashMap<String,String> filedName = new HashMap<String, String>();
			filedName.put("0","product_name");
			filedName.put("1","repair_count");
			filedName.put("2","repair_box");
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_excel_tmp/" + filename;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);
	
			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
	
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();
			   
			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
			    
				   pro.add(filedName.get(String.valueOf(j)),rs.getCell(j,i).getContents().trim());
				   
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  
			  return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelshow",log);
		}
	}
	
	/**
	 * 上传文件保存转运单明细
	 * @param request
	 * @throws Exception
	 */
	public void saveDamagedRepairDetail(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long repair_id= Long.parseLong(StringUtil.getString(request,"repair_id"));
			floorDamagedRepairMgrZJ.delAllDamagedRepairDetailByRepairId(repair_id);//批量删除返修明细
			
			String filenanme = StringUtil.getString(request,"tempfilename");
			
			
			DBRow[] details = excelshow(filenanme,null);
			
			for (int i = 0; i < details.length; i++) 
			{
				DBRow product = floorProductMgr.getDetailProductByPname(details[i].getString("product_name"));
				
				if(product==null)
				{
					throw new ProductNotExistException();
				}
				
				long repair_pc_id = product.get("pc_id",0l);
				String p_code = product.getString("p_code");
				float repair_count = Float.parseFloat(details[i].getString("repair_count"));
				String repair_box = details[i].getString("transport_box");
				
				DBRow damaged_repair_detail = new DBRow();
				damaged_repair_detail.add("repair_pc_id",repair_pc_id);
				damaged_repair_detail.add("repair_count",repair_count);
				damaged_repair_detail.add("repair_id",repair_id);
				damaged_repair_detail.add("repair_box",repair_box);
				damaged_repair_detail.add("repair_p_code",p_code);
				damaged_repair_detail.add("repair_p_name",details[i].getString("product_name"));
				damaged_repair_detail.add("repair_reap_count",0);//到达数量
				
				floorDamagedRepairMgrZJ.addDamagedRepairDetail(damaged_repair_detail);
			}
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 下载返修单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downloadDamagedRepairOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long repair_id = StringUtil.getLong(request,"repair_id");
			DBRow damagedRepair = floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
			
			DBRow[] damagedRepairDetails = floorDamagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id, null, null, null, null);
			if(damagedRepair == null)
			{
				throw new NoExistDamagedRepairOrderException();
			}
			if(damagedRepairDetails==null)
			{
				throw new NoExistDamagedRepairOrderDetailsException();
			}
			
			
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/return_step/repair_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); //创建一个居中格式
			style.setLocked(false);//设置不锁定
			style.setWrapText(true); 
			
			for (int i = 0; i < damagedRepairDetails.length; i++) 
			{
				DBRow damagedRepairDetail = damagedRepairDetails[i];
				long pc_id = damagedRepairDetail.get("repair_pc_id",0l);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(damagedRepairDetail.getString("repair_count"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(damagedRepairDetail.getString("repair_box"));
				row.getCell(2).setCellStyle(style);
			}
			String path = "upl_excel_tmp/R"+repair_id+".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
			wb.write(fout);
			fout.close();
			return ("../../"+path);
		} 
		catch(NoExistTransportOrderException e)
		{
			throw e;
		}
		catch(NoExistTransportDetailException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 检查返修库存
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkDamagedRepairStorage(long repair_id) 
		throws Exception 
	{
		try 
		{
			DBRow damagedRepair = floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
			
			if(damagedRepair == null)//验证返修单存在否
			{
				throw new NoExistDamagedRepairOrderException();
			}
			
			long ps_id = damagedRepair.get("send_psid",0l);
			
			DBRow[] damagedRepairDetails = floorDamagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id,null,null,null,null);
			
			if(damagedRepairDetails == null||damagedRepairDetails.length==0)
			{
				throw new NoExistDamagedRepairOrderDetailsException();
			}
			
			
			return (floorDamagedRepairMgrZJ.checkDamagedRepairStorage(repair_id, ps_id));
		}
		catch (NoExistDamagedRepairOrderException e)
		{
			throw e;
		}
		catch(NoExistDamagedRepairOrderDetailsException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"checkDamagedRepairStorage",log);
		}
	}
	
	/**
	 * 返修单装箱
	 * @param repair_id
	 * @throws Exception
	 */
	public void packingDamagedRepair(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long repair_id = StringUtil.getLong(request,"repair_id");
			
			DBRow damagedRepair = floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
			
			long ps_id = damagedRepair.get("send_psid",0l);
			
			DBRow[] damagedRepair_details = floorDamagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id, null, null, null, null);
			if(damagedRepair_details == null||damagedRepair_details.length==0)
			{
				throw new NoExistDamagedRepairOrderDetailsException();
			}
			for (int i = 0; i < damagedRepair_details.length; i++) 
			{
				
				float repair_count = damagedRepair_details[i].get("repair_count",0f);
				long pcid = damagedRepair_details[i].get("repair_pc_id",0l);
				DBRow storage = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pcid);
				
				long pid = storage.get("pid",0l);
				float damaged_count = storage.get("damaged_count",0f);
				float damaged_package_count = storage.get("damaged_package_count",0f);
				
				damaged_count = damaged_count-repair_count;//先用残损值减去返修量
				
				DBRow para = new DBRow();
				
				if(damaged_count>0)
				{
					para.add("damaged_count",damaged_count);
				}
				else
				{
					para.add("damaged_count",0f);
					para.add("damaged_package_count",(damaged_package_count+damaged_count));
				}
				//修改残损库存
				floorProductMgr.modProductStorage(pid,para);
				
				
				//记录残损件出库日志
				AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
				
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(repair_id);
				deProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
				deProductDamagedCountLogBean.setPs_id(ps_id);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR);
				deProductDamagedCountLogBean.setQuantity(-repair_count);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(pcid);
				productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			
			DBRow repair_para = new DBRow();
			repair_para.add("repair_status",DamagedRepairKey.PACKING);
			repair_para.add("repair_packing_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAccount());
			floorDamagedRepairMgrZJ.modDamagedRepairOrder(repair_id, repair_para);
		} 
		catch (Exception e) 
		{
			throw new SystemException();
		}
	}
	
	
	/**
	 * 过滤返修单明细（条码机下载用） 
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairDetailByPsWithStatus(long send_psid,long receive_psid, int status) 
		throws Exception 
	{	
		
		try 
		{
			return (floorDamagedRepairMgrZJ.getDamagedRepairDetailByPsWithStatus(send_psid, receive_psid, status));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDamagedRepairDetailByPsWithStatus",log);
		}
	}
	
	/**
	 * 返修单起运
	 * @param repair_id
	 * @throws Exception
	 */
	public void deliveryDamagedRepair(long repair_id)
		throws Exception
	{
		try 
		{
			long receive_id = floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id).get("receive_psid",0l);
			
			DBRow[] damagedRepairOutbounds = floorDamagedRepairOutboundMgrZJ.getDamagedRepairOutboundByRepairId(repair_id);
			
			//记录转运单实际发货量
			for(int i = 0;i<damagedRepairOutbounds.length;i++)
			{
				long pc_id = damagedRepairOutbounds[i].get("pc_id",0l);
				
				DBRow para = new DBRow();
				para.add("repair_send_count",damagedRepairOutbounds[i].get("real_send_count",0f));//发货数量
				
				floorDamagedRepairMgrZJ.deliveryDamagedRepairDetail(repair_id, pc_id, para);
			}
			
//			((TransportMgrIFaceZJ)AopContext.currentProxy()).deliveryTransportForJbpm(transport_id, receive_id);
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"deliveryDamagedRepair",log);
		}
	}
	
	/**
	 * 搜索返修单
	 */
	public DBRow[] searchDamagedRepairByNumber(String key, PageCtrl pc)
		throws Exception 
	{
		try
		{
			return floorDamagedRepairMgrZJ.searchDamagedRepairById(key, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deliveryDamagedRepair",log);
		}
	}
	
	public void setFloorDamagedRepairMgrZJ(
			FloorDamagedRepairMgrZJ floorDamagedRepairMgrZJ) {
		this.floorDamagedRepairMgrZJ = floorDamagedRepairMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


	public void setFloorDamagedRepairOutboundMgrZJ(
			FloorDamagedRepairOutboundMgrZJ floorDamagedRepairOutboundMgrZJ) {
		this.floorDamagedRepairOutboundMgrZJ = floorDamagedRepairOutboundMgrZJ;
	}


	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
}
