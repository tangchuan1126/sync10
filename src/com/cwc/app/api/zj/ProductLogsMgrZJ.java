package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.iface.zj.ProductLogsMgrIFaceZJ;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class ProductLogsMgrZJ implements ProductLogsMgrIFaceZJ {

	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private FloorProductMgr floorProductMgr;
	
	public DBRow[] searchProductLogs(String p_name, String start, String end,long adid, int edit_type, int edit_reason, PageCtrl pc)
		throws Exception 
	{
		long pc_id = 0;
		
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		if(product!=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		return (floorProductLogsMgrZJ.searchProductLogs(pc_id, start, end, adid, edit_type, edit_reason, pc));
	}
	
	public void AllProductAddLogs()
		throws Exception
	{
		DBRow[] products = floorProductMgr.getAllProducts(null);
		
		for (int i = 0; i < products.length; i++) 
		{
			
			products[i].add("account",100228);
			products[i].add("edit_type",ProductEditTypeKey.ADD);
			products[i].add("edit_reason",ProductEditReasonKey.SELF);
			products[i].add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(products[i]);
		}
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

}
