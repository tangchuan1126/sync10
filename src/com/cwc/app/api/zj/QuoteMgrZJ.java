package com.cwc.app.api.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.ExpressMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorQuoteMgr;
import com.cwc.app.floor.api.zj.FloorQuoteMgrZJ;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zj.QuoteMgrIFaceZJ;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.util.StringUtil;

public class QuoteMgrZJ implements QuoteMgrIFaceZJ {
	static Logger log = Logger.getLogger("ACTION");
	private FloorQuoteMgr floorQuoteMgr;
	private FloorQuoteMgrZJ floorQuoteMgrZJ;
	private ProductMgrIFaceZJ productMgrZJ;
	private FloorProductMgr floorProductMgr;
	private ExpressMgr expressMgr;
	private SystemConfig systemConfig;
	
	public void qutoPriceChangeGrossProfit(HttpServletRequest request)
		throws Exception 
	{
		long catalog_id = StringUtil.getLong(request,"pc_id");
		long pro_line_id = StringUtil.getLong(request,"pro_line_id");
		
		DBRow[] products = productMgrZJ.filterProduct(catalog_id, pro_line_id, 0,0,0,null);//页面过滤出的商品
		
//		for (int i = 0; i < products.length; i++)//循环商品
//		{
//			double product_cost_coefficient = systemConfig.getDoubleConfigValue( "product_cost_coefficient" );//商品成本修正率
			
//			double unit_price = products[i].get("unit_price",0d);
//			unit_price = MoneyUtil.round(unit_price,2);
			
			DBRow[] allGrossProfits = floorQuoteMgrZJ.getAllRpiFromProductProfit();//getAllAreaGrossProfitByPcid(products[i].get("pc_id",0l));
			
			for (int j = 0; j < allGrossProfits.length; j++)//循环毛利率
			{
				DBRow retailPriceAndItem = floorQuoteMgrZJ.getRetailPriceAndItemByRpiId(allGrossProfits[j].get("rpi_id",0l));
				
				if(retailPriceAndItem==null)
				{
					continue;
				}
				
				double new_retail_price = retailPriceAndItem.get("new_retail_price",0d);
				double usd = systemConfig.getDoubleConfigValue("USD");
				new_retail_price = new_retail_price*usd;
				
				long ca_id = retailPriceAndItem.get("ca_id",0l);
				
				
				
				DBRow productProfit = new DBRow();
				productProfit.add("pid",retailPriceAndItem.get("pid",0l));
				productProfit.add("ccid",0l);
				productProfit.add("profit",retailPriceAndItem.get("gross_profit",0d));
				productProfit.add("rpi_id",retailPriceAndItem.get("rpi_id",0l));
				
				productProfit.add("ca_id",retailPriceAndItem.get("ca_id",0l));
				productProfit.add("retail_price",retailPriceAndItem.get("new_retail_price",0d));
				productProfit.add("shipping_fee",retailPriceAndItem.get("shipping_fee",0d));
				productProfit.add("sc_id",retailPriceAndItem.get("sc_id",0l));
				
				
				
				

				
				DBRow detailCountryArea = floorProductMgr.getDetailCountryAreaByCaId(ca_id);
//				long ccid = detailCountryArea.get("sig_country",0l);
//				long pro_id = detailCountryArea.get("sig_state",0l);
				
//				float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
//				float totalWeight = products[i].get("weight",0f); //* weight_cost_coefficient;
//				
//				long sc_id = retailPriceAndItem.get("sc_id",0l);
//				
//				if(sc_id==0)//兼容老数据
//				{
//					continue;
//				}
//				
//				ShippingInfoBean shippingInfoBean = null;
//				double shipping_fee = 0;
//				try 
//				{
//					shippingInfoBean = expressMgr.getShippingFee(sc_id,totalWeight,ccid,pro_id);
//					shipping_fee = shippingInfoBean.getShippingFee();
//				} 
//				catch (ProvinceOutSizeException e)
//				{
//
//				}
//				catch (CountryOutSizeException e)
//				{
//
//				}
//				catch (WeightCrossException e)
//				{
//
//				}
//				catch (WeightOutSizeException e)
//				{
//
//				}
				
				 
				
//				double gross_profit = new_retail_price-shipping_fee-unit_price;
//				
//				DBRow dbrow = new DBRow();
//				dbrow.add("profit",gross_profit);
				
//				floorQuoteMgr.modProductProfitByPidCcid(products[i].get("pc_id",0l),allGrossProfits[j].get("ccid",0l),dbrow);
				
				floorQuoteMgr.addProductProfit(productProfit);
				
				floorQuoteMgrZJ.delAllProductProfitByRpiId(productProfit.get("rpi_id",0l));
				
				////system.out.println("product:"+0+"/"+products.length+"------productProfit:"+j+"/"+allGrossProfits.length);
//			}
		}
	}
	
	/**
	 * 根据商品ID获得该商品的所有区域销售的毛利
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	private DBRow[] getAllAreaGrossProfitByPcid(long pc_id)
		throws Exception
	{
		try 
		{
			return floorQuoteMgrZJ.getProductProfitByPcid(pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllAreaGrossProfitByPcid",log);
		}
	}
	
	/**
	 * 获得商品的毛利（有定价依据定价的毛利走，未定价依据商品+运费的盒）
	 * @param ccid
	 * @param pro_id
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public double getProductProfitValue(long ccid,long pro_id,long pc_id,long ps_id,float quantity)
		throws Exception
	{
		ShippingInfoBean[] shippingInfoBeans = null;
		
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		product.add("cart_pid",pc_id);
		product.add("cart_quantity",quantity);
		
		double unit_price = product.get("unit_price",0d);
		unit_price = MoneyUtil.round(unit_price,2);
		
		//计算处理该地区适合发哪个快递和运费
		//先判断是国内发货还是国际发货
		boolean isDomesticFlag = expressMgr.isDomesticShipping(ccid, ps_id);
		
		DBRow express[];
		if (isDomesticFlag)//国内发货
		{
			express = expressMgr.getDomesticExpressCompanyByPsIdProId( ps_id, pro_id,product.get("weight",0f) , null);
		}
		else
		{
			express = expressMgr.getInternationalExpressCompanyByPsIdProId(ps_id,ccid,product.get("weight",0f),null);
		}
		
		shippingInfoBeans = new ShippingInfoBean[express.length];
		for (int k=0; k<express.length; k++)
		{
			ShippingInfoBean shippingInfoBean;
			try 
			{
				
//				shippingInfoBean = expressMgr.getShippingFee(express[k].get("sc_id",0l),totalWeight, ccid,pro_id);
				
				shippingInfoBean = expressMgr.getShippingFeeForProduct(express[k].get("sc_id",0l), ccid, pro_id, new DBRow[]{product});
				shippingInfoBean.setSc_id(express[k].get("sc_id",0l));//塞入快递公司ID
				//对于运费进行修正
//				shippingInfoBean.setShippingFee( getLowestShippingPrice(shippingInfoBean.getShippingFee()) );
				shippingInfoBeans[k] = shippingInfoBean;
			} 
			catch (ProvinceOutSizeException e)
			{
				continue;
			}
			catch (CountryOutSizeException e)
			{
				continue;
			}
			catch (WeightCrossException e)
			{
				continue;
			}
			catch (WeightOutSizeException e)
			{
				continue;
			}
		}
		
		//运费为0的快递从快递列表剔除
		ArrayList<ShippingInfoBean> shippingInfoBeanlist = new ArrayList<ShippingInfoBean>();
		for (int i = 0; i <shippingInfoBeans.length; i++) 
		{
			if(shippingInfoBeans[i].getShippingFee()>0)
			{
				shippingInfoBeanlist.add(shippingInfoBeans[i]);
			}
		}
		shippingInfoBeans = shippingInfoBeanlist.toArray(new ShippingInfoBean[0]);
		
		
		//按照运费升密排序(最小的排在前面)
		ShippingInfoBean tmpShippingInfoBean;
		for (int sorti=0; sorti<shippingInfoBeans.length; sorti++)
		{
			for (int sortj=sorti+1; sortj<shippingInfoBeans.length; sortj++)
			{
				if (shippingInfoBeans[sorti].getShippingFee()>shippingInfoBeans[sortj].getShippingFee()&&shippingInfoBeans[sortj].getShippingFee()>0)
				{
					tmpShippingInfoBean = shippingInfoBeans[sorti];
					shippingInfoBeans[sorti] = shippingInfoBeans[sortj];
					shippingInfoBeans[sortj] = tmpShippingInfoBean;
				}
			}
		}

		/**
		 * 国内发货，使用默认毛利，计算所有商品零售价
		 * 国家、分区报价，都是直接读取国家毛利
		 **/
		double gross_profit = 0;
		
		
		DBRow detailProductCountryProfit = floorQuoteMgr.getDetailProductCountryProfitByPidCcId(pc_id,ccid);
		if (detailProductCountryProfit!=null||(detailProductCountryProfit!=null&&detailProductCountryProfit.get("profit", 0d)>0))
		{
			gross_profit = detailProductCountryProfit.get("profit", 0d);
		}
		else
		{
  			//如果原来商品没有定价，则设定毛利=35%
			/**
     		 * 如果没有合适快递，设置毛利为0
			 */
			if (shippingInfoBeans.length>0)
			{				
				gross_profit = (unit_price*0.6)+(shippingInfoBeans[0].getShippingFee()*0.2);
				gross_profit = MoneyUtil.round(gross_profit, 2); 
			}
			else
			{
				gross_profit = (unit_price)*0.6;
				gross_profit = MoneyUtil.round(gross_profit,2);
			}							
		}
		
		return gross_profit;
	}
	
	public void setFloorQuoteMgr(FloorQuoteMgr floorQuoteMgr) {
		this.floorQuoteMgr = floorQuoteMgr;
	}

	public void setFloorQuoteMgrZJ(FloorQuoteMgrZJ floorQuoteMgrZJ) {
		this.floorQuoteMgrZJ = floorQuoteMgrZJ;
	}

	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setExpressMgr(ExpressMgr expressMgr) {
		this.expressMgr = expressMgr;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
	

}
