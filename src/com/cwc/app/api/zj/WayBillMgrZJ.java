package com.cwc.app.api.zj;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;




















import org.apache.log4j.Logger;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.Cart;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.emailtemplate.velocity.printorder.PrintDHLEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintEMSEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintEPacketEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintEQUICKEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintFEDEXEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintFEDEXIEEmailPageString;
import com.cwc.app.emailtemplate.velocity.printorder.PrintUSPSEmailPageString;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.exception.waybill.OrderItemWaitErrorException;
import com.cwc.app.exception.waybill.WayBillOrderPrintedException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorWaybillLogMgrZJ;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.OrderMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.WayBillFromKey;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.key.WaybillInternalOperKey;
import com.cwc.app.lucene.zr.WayBillIndexMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;

public class WayBillMgrZJ implements WayBillMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorOrderMgr floorOrderMgr;
	private SystemConfig systemConfig;
	private ProductMgrIFace productMgr;
	private OrderMgrIFace orderMgr;
	private FloorExpressMgr floorExpressMgr;
	private OrderMgrIFaceZJ orderMgrZJ;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorWaybillLogMgrZJ floorWaybillLogMgrZJ;
	private WayBillMgrIfaceZR wayBillMgrZR;
	private CatalogMgrIFace catalogMgr;
	private ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ;
	
	
	public void addWayBillOrder(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long oid = StringUtil.getLong(request,"oid");
			long sc_id = StringUtil.getLong(request,"sc_id");
			String tracking_number = StringUtil.getString(request,"tracking_number");
			double shipping_cost = StringUtil.getDouble(request,"shipping_cost");
			long ps_id = StringUtil.getLong(request,"ps_id");
			String destination = StringUtil.getString(request,"destination");
			
			long inv_di_id = StringUtil.getLong(request,"inv_di_id");
			String inv_uv = StringUtil.getString(request,"inv_uv");
			String inv_rfe = StringUtil.getString(request,"inv_rfe");
			String inv_dog = StringUtil.getString(request,"inv_dog");
			String inv_tv = StringUtil.getString(request,"inv_tv");
			
			//this.addWayBillOrderSub(oid, sc_id, tracking_number, shipping_cost, ps_id, destination,StrUtil.getSession(request),inv_di_id,inv_uv,inv_rfe,inv_dog,inv_tv,"");
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addWayBillOrder",log);
		}
	}
	
	public long addWayBillOrderSub(String address_name,String address_country,String address_city,String address_zip,String address_street,String address_state,long ccid,long pro_id,String client_id,String tel,long sc_id,String tracking_number,double shipping_cost,long ps_id,String destination,HttpSession session,long inv_di_id,String inv_uv,String inv_rfe,String inv_dog,String inv_tv,String mail_piece_shape,String delivery_note,AdminLoginBean adminLoggerBean,int waybill_from_type,String hs_code,DBRow[] cartProducts,int pkcount,String material,String dtp,String inv_rfe_chinese,String inv_dog_chinese,String material_chinese,double shippment_rate) 
		throws Exception 
	{
		
		try 
		{
			CartWaybill cartWaybill = (CartWaybill)MvcUtil.getBeanFromContainer("cartWaybill");
			float all_weight = 0.0f;
//			DBRow[] cartProducts = null;
			double waybill_prodcut_cost = 0.0d;
			DBRow dbrow = new DBRow();
			if(waybill_from_type==WayBillFromKey.Record)
			{
		
//				cartWaybill.flush(session);
			
				all_weight = cartWaybill.getCartWeight(cartProducts);
//				cartProducts = cartWaybill.getDetailProduct();
				waybill_prodcut_cost = this.productCost(cartProducts);
			}
			else if(waybill_from_type==WayBillFromKey.Split)
			{
				String parentWayBIllId =(String) session.getAttribute("waybillid");
				all_weight = (Float)session.getAttribute("weight");
//				waybill_prodcut_cost  = this.productCost((DBRow[])session.getAttribute("detailproduct"));
				waybill_prodcut_cost  = this.productCost(cartProducts);
				dbrow.add("parent_waybill_id", parentWayBIllId);
			}
			else if(waybill_from_type==WayBillFromKey.Replace)
			{
				String parentWayBIllId =(String) session.getAttribute("waybillid");
				all_weight = (Float)session.getAttribute("weight");
				waybill_prodcut_cost  = this.productCost(cartProducts);
				dbrow.add("replace_waybill_id", parentWayBIllId);
			}
			
			dbrow.add("sc_id",sc_id);
			if(!tracking_number.trim().equals(""))
			{
				dbrow.add("tracking_number",tracking_number);
			}
			
			dbrow.add("shipping_cost",shipping_cost);
			dbrow.add("ps_id",ps_id);
			dbrow.add("waybill_prodcut_cost",waybill_prodcut_cost);
			dbrow.add("address_name",address_name);
			dbrow.add("address_country",address_country);
			dbrow.add("address_city",address_city);
			dbrow.add("address_zip",address_zip);
			dbrow.add("address_street",address_street);
			dbrow.add("address_state",address_state);
			dbrow.add("ccid",ccid);
			dbrow.add("pro_id",pro_id);
			dbrow.add("delivery_note",delivery_note);
			dbrow.add("client_id",client_id);
			dbrow.add("tel",tel);
			dbrow.add("pkcount",pkcount);//一票几件
			
			if(!destination.trim().equals(""))
			{
				dbrow.add("destination",destination);
			}
			dbrow.add("all_weight",all_weight);
			dbrow.add("inv_di_id",inv_di_id);
			dbrow.add("inv_dog",inv_dog);
			dbrow.add("inv_rfe",inv_rfe);
			dbrow.add("material",material);
			dbrow.add("inv_uv",inv_uv);
			dbrow.add("inv_tv",inv_tv);
			
			dbrow.add("inv_rfe_chinese",inv_rfe_chinese);
			dbrow.add("inv_dog_chinese",inv_dog_chinese);
			dbrow.add("material_chinese",material_chinese);
			
			dbrow.add("create_date",DateUtil.NowStr());
			dbrow.add("create_account",adminLoggerBean.getAdid());
			dbrow.add("mail_piece_shape",mail_piece_shape);
			dbrow.add("hs_code",hs_code);
			
			dbrow.add("dtp",dtp);
			
			dbrow.add("trace_date",DateUtil.NowStr());//创建日期为首次跟进日期
			dbrow.add("trace_flag",1);				  //创建运单，开启运单跟踪
			
			float print_weight = orderMgr.calculateWayBillWeight(sc_id, all_weight);//运动总重量X使用的快递公司的打印重折扣
			dbrow.add("print_weight",print_weight);
			dbrow.add("shippment_rate",shippment_rate);
			
			long waybill_id = floorWayBillOrderMgrZJ.addWayBillOrder(dbrow);
			
			
			
			long from_waybill_id = 0;
			if(waybill_from_type==WayBillFromKey.Record)//true代表扣库存生成运单明细
			{
				addWayBillOrderItemsReduceStore(cartProducts, adminLoggerBean, waybill_id, ps_id, tracking_number, all_weight, shipping_cost);
			}
			else//不扣库存创建运单明细（运单拆分运单创建明细）
			{
				String ids =(String) session.getAttribute("ids");
				from_waybill_id =  Long.parseLong((String)session.getAttribute("waybillid"));
				String numbers =(String)session.getAttribute("numbers");
				this.caluteWaybillItem(waybill_id,from_waybill_id,ids, numbers, tracking_number , all_weight , shipping_cost,adminLoggerBean.getAdid());
			}
			
			/*
			 * 记录运单日志(创建)
			 */
			DBRow waybillLog = new DBRow();
			waybillLog.add("waybill_id",waybill_id);
			waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Create);
			waybillLog.add("trackingNumber",tracking_number);
			waybillLog.add("sc_id",sc_id);
			waybillLog.add("operator_adid",adminLoggerBean.getAdid());
			waybillLog.add("operator_time",DateUtil.NowStr());
			waybillLog.add("from_waybill_id",from_waybill_id);
			floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
			
			if(from_waybill_id!=0)//拆分生成的运单，需要回填订单日志
			{
//				this.addOrderNoteWithWaybill(waybill_id,WaybillInternalOperKey.Create, adminLoggerBean);
				this.createWayBillAddNoteOrder(waybill_id, adminLoggerBean);
			}
			
			//确定运单所属区域
			long slc_area_id = productStoreLocationMgrZJ.getAreaForBill(waybill_id,ProductStoreBillKey.WAYBILL_ORDER);
			DBRow para = new DBRow();
			para.add("waybill_area_id",slc_area_id);
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
			
			
			//清空运单购物车
//			cartWaybill.clearCart(session);
			
			session.removeAttribute("waybillid");
			session.removeAttribute("weight");
			session.removeAttribute("detailproduct");
			session.removeAttribute("ids");
			session.removeAttribute("numbers");
			
			//更新订单TrackingNumber索引
			updateTrackingNumberToOrder(waybill_id);
			
			this.calculationOrderToWayBill(waybill_id);
			
			this.createWayBillAddNoteOrder(waybill_id,adminLoggerBean);
			
			
			WayBillIndexMgr.getInstance().addIndex(waybill_id, tracking_number, client_id, address_name, address_street, address_zip, address_city, address_state, address_country);
			return (waybill_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addWayBillOrderSub",log);
		}
	}
	@Override
	public void caluteWaybillItem(long newWayBillId,long wayBillId ,String ids, String numbers,String tracking_number,double total_weight , double total_shipping_cost,long adid) throws Exception 
	{
		try
		{
			String[] idArray = ids.split(",");
			String[] numbersArray = numbers.split(",");
			for(int index = 0 , count = idArray.length ; index < count ; index++ )
			{
				this.addWayBillItemAndUpdateOldWayBillItem(newWayBillId,idArray[index], numbersArray[index], tracking_number,total_weight, total_shipping_cost);
			} 
			
			DBRow wayBillRow = new DBRow() ;
			wayBillRow = floorWayBillOrderMgrZJ.getWayBillOrderById(wayBillId);
			int state = floorWayBillOrderMgrZJ.updateWayBillStatus(wayBillId,adid);
			if(state == WayBillOrderStatusKey.CANCEL)
			{
				DBRow waybillLog = new DBRow();
				waybillLog.add("waybill_id",wayBillId);
				waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Cancel);
				waybillLog.add("trackingNumber",wayBillRow.getString("tracking_number"));
				waybillLog.add("sc_id",wayBillRow.get("sc_id", 0l));
				waybillLog.add("operator_adid",adid);
				waybillLog.add("operator_time",DateUtil.NowStr());
				floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
				
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAdid(adid);
				
//				this.addOrderNoteWithWaybill(wayBillId,WaybillInternalOperKey.Cancel, adminLoggerBean);
				
				this.cancelWayBillAddNoteOrder(wayBillId, adminLoggerBean);
			}
		}catch (Exception e) {
			throw new SystemException(e,"caluteWaybillItem",log);
		}
	
	}
	// 拆分的时候 添加WOI 同时更新WayBill中的某些数据
	@Override
	public void addWayBillItemAndUpdateOldWayBillItem(long newWayBillId,
			String id, String number, String tracking_number,
			double total_weight, double total_shipping_cost) throws Exception {
		try{
			
 			DBRow WayBillItemRow =floorWayBillOrderMgrZJ.getWayBillItemById(Long.parseLong(id));
			DBRow insertNewRow = new DBRow(); // 这里应该是Copy一份数据WayBillItemRow.
			List<String> names = WayBillItemRow.getFieldNames();
			for(String name  : names ) {
				insertNewRow.add(name, WayBillItemRow.getString(name));
			}
			insertNewRow.remove("waybill_order_item_id");
			double newQuantity = Double.parseDouble(number);
			double value =  newQuantity - WayBillItemRow.get("quantity", 0.0d);
			if(value > 0){
				throw new RuntimeException("拆分数量大于了待发数量");
			}else{
				double basic = newQuantity / WayBillItemRow.get("quantity", 0.0d);
				double prodcut_cost = insertNewRow.get("prodcut_cost", 0.0d) * basic;
				double saleroom = insertNewRow.get("saleroom", 0.0d) * basic;
				double weight = insertNewRow.get("weight", 0.0d) * basic;
				insertNewRow.add("prodcut_cost", prodcut_cost);
				insertNewRow.add("saleroom", saleroom);
				insertNewRow.add("weight", weight);
				insertNewRow.add("tracking_number", tracking_number);
				insertNewRow.add("waybill_order_id", newWayBillId);
				insertNewRow.add("quantity", newQuantity);
				insertNewRow.add("shipping_cost",( weight /  total_weight) * total_shipping_cost);
				floorWayBillOrderMgrZJ.addWayBillOrderItem(insertNewRow);
				//dbUtilAutoTran.insert(tableName, insertNewRow);
				double updateQuantity =	WayBillItemRow.get("quantity", 0.0f) - newQuantity;
				long updateId = WayBillItemRow.get("waybill_order_item_id",0l);
				WayBillItemRow.remove("waybill_order_item_id");
				double updateProdcutCost = WayBillItemRow.get("prodcut_cost", 0.0d) - prodcut_cost;
				double updateSaleroom = WayBillItemRow.get("saleroom", 0.0d) - saleroom;
				double updateWeight = WayBillItemRow.get("weight", 0.0d) - weight;
				WayBillItemRow.add("quantity",updateQuantity);
				WayBillItemRow.add("prodcut_cost",updateProdcutCost);
				WayBillItemRow.add("saleroom",updateSaleroom);
				WayBillItemRow.add("weight",updateWeight);
				if(updateQuantity <= 0 ){
					//如果是删除完的时候就是要删除主运单的WOI
					floorWayBillOrderMgrZJ.delWayBillOrderItemById(updateId);
				} else{
				floorWayBillOrderMgrZJ.updateWayBillItemByIdRow(updateId, WayBillItemRow);
				}
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorWayBillOrderMgrZJ addWayBillItemAndUpdateOldWayBillItem :"+e);
		}
		
	}
	 
	@Override
	public DBRow[] getWayBillByOId(long oid , PageCtrl pc) throws Exception {
		try
		{
			return floorWayBillOrderMgrZJ.getWayBillByOid(oid , pc); 
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillByOId",log);
		}
	}
	@Override
	public DBRow[] getWayBillByTrackingNumber(String number , PageCtrl pc) throws Exception {
		try{
			return floorWayBillOrderMgrZJ.getWayBillByTrackingNumber(number,pc);
		}catch (Exception e) {
			throw new SystemException(e,"getWayBillByTrackingNumber",log);
		}
	}
	
	/**
	 * 生成运单明细，扣库存
	 * @param cartProducts
	 * @param adminLoggerBean
	 * @param waybill_id
	 * @param ps_id
	 * @param tracking_number
	 * @param all_weight
	 * @param shipping_cost
	 * @throws Exception
	 */
	private void addWayBillOrderItemsReduceStore(DBRow[] cartProducts,AdminLoginBean adminLoggerBean,long waybill_id,long ps_id,String tracking_number,float all_weight,double shipping_cost)
		throws Exception
	{
		try 
		{
			if(cartProducts==null||!(cartProducts.length>0))
			{
				throw new Exception(waybill_id+" cartProducts is null");
			}
			
			long oid = 1;
			
			int wayBillOrderIsLacking = ProductStatusKey.IN_STORE; 
			for (int i=0; i<cartProducts.length; i++)
			{
				
				//减少order_item的待发数量
				floorOrderMgr.subWaitQuantity(StringUtil.getLong(cartProducts[i].getString("order_item_id")),StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
				DBRow orderItem = floorOrderMgr.getDetailOrderItem(StringUtil.getLong(cartProducts[i].getString("order_item_id")));
				
				if(orderItem.get("wait_quantity",0f)<0)
				{
					throw new OrderItemWaitErrorException();
				}
					/**
					 * 减少库存区分普通商品、标准套装、标准套装拼装、定制套装
					 * 普通商品：直接减少库存
					 * 标准套装：
					 *	1、标准套装够库存，直接减少库存
					 *  2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
					 *  3、标准套装没库存，散件拼装有库存，减少散件库存
					 *  4、标准套装没库存，散件拼装没库存，减少标准套装库存
					 * 注意：当标准套装库存不够，散件够，需要拆分成虚拟标准套装（即拼装成的） 
					 * 
					 * 定制套装：套装中的散件减少库存
					 */

						//先把手工套装转换为标准套装
						//为了让系统重新判断套装是否需要拆分
						if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL)
						{
							cartProducts[i].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
						}

						//进出库日志
						ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
						deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
						deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
						deInProductStoreLogBean.setOid(waybill_id);
						deInProductStoreLogBean.setBill_type(ProductStoreBillKey.WAYBILL_ORDER);
						deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_WAYBILL);
						
						long cart_pc_id = StringUtil.getLong(cartProducts[i].getString("cart_pid"));
						float cart_quantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity"));
						
						if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
						{
							int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货

							//确保两个线程同时检查库存的时候有先后顺序
							synchronized(String.valueOf(ps_id)+"_"+cart_pc_id)
							{
								//检查库存
								//orderIsLacking
								DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,cart_pc_id);
								
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
								}
								else
								{
									if (detailPro.get("store_count", 0f)<cart_quantity)
									{
										lacking = ProductStatusKey.STORE_OUT;
										wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
									}
								}
								
								if(lacking == ProductStatusKey.IN_STORE)
								{
									floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,cart_pc_id,cart_quantity);
									
									/**
									 * 位置库存预保留
									 */
									productStoreLocationMgrZJ.reserveTheoretical(ps_id,cart_pc_id,cart_quantity,waybill_id,ProductStoreBillKey.WAYBILL_ORDER,adminLoggerBean.getAdid(),null,"FIFO");
								}
							}

							DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
							oid = order_item.get("oid",0l);
							DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
							double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
							double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
							double orderProductCost = getOrderProductCost(oid);
							String order_source = orderForIid.getString("order_source");
							
							DBRow waybillItem = new DBRow();
							waybillItem.add("oid",oid);
							waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
							waybillItem.add("pc_id",cart_pc_id);
							waybillItem.add("quantity",cart_quantity);
							waybillItem.add("tracking_number",tracking_number);
							waybillItem.add("waybill_order_id",waybill_id);
							waybillItem.add("ps_id",ps_id);
							waybillItem.add("product_type",cartProducts[i].getString("cart_product_type"));
							waybillItem.add("order_source",order_source);
							
							waybillItem.add("product_status",lacking);
							
							DBRow product = floorProductMgr.getDetailProductByPcid(cart_pc_id);
							double prodcut_cost = product.get("unit_price",0d)*cart_quantity;
							float one_weight = (float)(product.get("weight",0f)*cart_quantity);
							float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
							String unit_name = product.getString("unit_name");
							double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
							
							waybillItem.add("p_name",product.getString("p_name"));
							waybillItem.add("saleroom",saleroom);
							waybillItem.add("unit_name",unit_name);
							waybillItem.add("weight",one_weight);
							waybillItem.add("prodcut_cost",prodcut_cost);
							waybillItem.add("shipping_cost",one_shipping_cost);
							
							floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);

						}
						else if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD)
						{
							synchronized(String.valueOf(ps_id)+"_"+cartProducts[i].getString("cart_pid"))
							{
								boolean notBeCreateStorage = false;
								DBRow detailProductStorage = floorProductMgr.getDetailProductProductStorageByPcid( ps_id,cart_pc_id );
								if (detailProductStorage==null)
								{
									notBeCreateStorage = true;//没建库
									detailProductStorage = new DBRow();
									detailProductStorage.add("store_count", 0);
								}

								//1、标准套装够库存，直接减少库存
								if (detailProductStorage.get("store_count",0f)>=cart_quantity)
								{
									DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
									oid = order_item.get("oid",0l);
									DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
									double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
									double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
									double orderProductCost = getOrderProductCost(oid);
									String order_source = orderForIid.getString("order_source");
									
									DBRow waybillItem = new DBRow();
									waybillItem.add("oid",oid);
									waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
									waybillItem.add("pc_id",cart_pc_id);
									waybillItem.add("quantity",cart_quantity);
									waybillItem.add("tracking_number",tracking_number);
									waybillItem.add("waybill_order_id",waybill_id);
									waybillItem.add("ps_id",ps_id);
									waybillItem.add("product_type",cartProducts[i].getString("cart_product_type"));
									waybillItem.add("order_source",order_source);
									
									waybillItem.add("product_status",ProductStatusKey.IN_STORE);//库存够套装，直接有货
									
									
									DBRow product = floorProductMgr.getDetailProductByPcid(cart_pc_id);
									double prodcut_cost = product.get("unit_price",0d)*cart_quantity;
									float one_weight = (float)(product.get("weight",0f)*cart_quantity);
									float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
									String unit_name = product.getString("unit_name");
									double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
									
									waybillItem.add("p_name",product.getString("p_name"));
									waybillItem.add("saleroom",saleroom);
									waybillItem.add("unit_name",unit_name);
									waybillItem.add("weight",one_weight);
									waybillItem.add("prodcut_cost",prodcut_cost);
									waybillItem.add("shipping_cost",one_shipping_cost);
									floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);

									floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,cart_pc_id,cart_quantity);
									
									/**
									 * 位置库存预保留
									 */
									productStoreLocationMgrZJ.reserveTheoretical(ps_id, cart_pc_id, cart_quantity, waybill_id, ProductStoreBillKey.WAYBILL_ORDER,adminLoggerBean.getAdid(),null,"FIFO");
									
								}
								//2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
								else
								{
									float needManualQuantity;
									//计算需要散件的套数
									if (detailProductStorage.get("store_count", 0f)>0)
									{
										//除了标准套装提供的数量外，还需要瓶装多少套散件
										needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - detailProductStorage.get("store_count", 0f);
									}
									else
									{
										needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity"));
									}

									//标准套装不够，检查散件是否够拼装
									boolean isLacking = false;
									DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid( StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
									for (int j=0; j<productsInSet.length; j++)
									{
										float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
										DBRow productStorage = floorProductMgr.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
										if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
										{
											isLacking = true;
											break;
										}
									}
									//散件也不够库存，并且商品已经在这个区域仓库建库，直接减掉标准套装库存
									if (isLacking&&notBeCreateStorage==false)
									{
										DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
										oid = order_item.get("oid",0l);
										DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
										double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
										double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
										double orderProductCost = getOrderProductCost(oid);
										String order_source = orderForIid.getString("order_source");
										
										DBRow waybillItem = new DBRow();
										waybillItem.add("oid",oid);
										waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
										waybillItem.add("pc_id",cartProducts[i].getString("cart_pid"));
										waybillItem.add("quantity",cartProducts[i].getString("cart_quantity"));
										waybillItem.add("tracking_number",tracking_number);
										waybillItem.add("waybill_order_id",waybill_id);
										waybillItem.add("ps_id",ps_id);
										waybillItem.add("product_type",cartProducts[i].getString("cart_product_type"));
										waybillItem.add("order_source",order_source);
										
										waybillItem.add("product_status",ProductStatusKey.STORE_OUT);//套装商品拼装也缺货，整个套装缺货
										wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
										
										DBRow product = floorProductMgr.getDetailProductByPcid(cartProducts[i].get("cart_pid",0l));
										double prodcut_cost = product.get("unit_price",0d)*cartProducts[i].get("cart_quantity",0d);
										
										float one_weight = (float)(product.get("weight",0f)*cartProducts[i].get("cart_quantity",0d));
										float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
										String unit_name = product.getString("unit_name");
										double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
										
										waybillItem.add("p_name",product.getString("p_name"));
										waybillItem.add("saleroom",saleroom);
										waybillItem.add("unit_name",unit_name);
										waybillItem.add("weight",one_weight);
										waybillItem.add("prodcut_cost",prodcut_cost);
										waybillItem.add("shipping_cost",one_shipping_cost);
										floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);
										
										//缺货抄单，缺货商品不扣库存
//										floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
										
									}
									//散件够库存
									else
									{
										//实际需要标准套装数-瓶装提供的数目
										float needUnionStandardCount = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - needManualQuantity;
										
										productMgr.combinationProductSystem(ps_id,cart_pc_id,(int)needManualQuantity,ProductStoreBillKey.WAYBILL_ORDER,waybill_id,adminLoggerBean,ProductStoreOperationKey.OUT_STORE_WAYBILL);										
										
										//标准套装需要的数目
										if (needUnionStandardCount>0)
										{
											DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
											oid = order_item.get("oid",0l);
											DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
											double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
											double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
											double orderProductCost = getOrderProductCost(oid);
											String order_source = orderForIid.getString("order_source");
											
											DBRow waybillItem = new DBRow();
											waybillItem.add("oid",oid);
											waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
											waybillItem.add("pc_id",cartProducts[i].getString("cart_pid"));
											waybillItem.add("quantity",needUnionStandardCount);
											waybillItem.add("tracking_number",tracking_number);
											waybillItem.add("waybill_order_id",waybill_id);
											waybillItem.add("ps_id",ps_id);
											waybillItem.add("product_type",cartProducts[i].getString("cart_product_type"));
											waybillItem.add("order_source",order_source);
											
											waybillItem.add("product_status",ProductStatusKey.IN_STORE);
											
											DBRow product = floorProductMgr.getDetailProductByPcid(cartProducts[i].get("cart_pid",0l));
											double prodcut_cost = product.get("unit_price",0d)*needUnionStandardCount;
											float one_weight = (float)(product.get("weight",0f)*needUnionStandardCount);
											float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
											String unit_name = product.getString("unit_name");
											double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
											
											waybillItem.add("p_name",product.getString("p_name"));
											waybillItem.add("saleroom",saleroom);
											waybillItem.add("unit_name",unit_name);
											waybillItem.add("weight",one_weight);
											waybillItem.add("prodcut_cost",prodcut_cost);
											waybillItem.add("shipping_cost",one_shipping_cost);
											floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);
											
											/**
											 * 实际需要套装多少独立扣掉
											 */
											floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,cart_pc_id,needUnionStandardCount);
											productStoreLocationMgrZJ.reserveTheoretical(ps_id, cart_pc_id, needUnionStandardCount,waybill_id,ProductStoreBillKey.WAYBILL_ORDER,adminLoggerBean.getAdid(),null,"FIFO");
//											floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), needUnionStandardCount);
										}
										
										//插入散件套装
										DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
										oid = order_item.get("oid",0l);
										DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
										double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
										double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
										double orderProductCost = getOrderProductCost(oid);
										String order_source = orderForIid.getString("order_source");
										
										DBRow waybillItem = new DBRow();
										waybillItem.add("oid",oid);
										waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
										waybillItem.add("pc_id",cartProducts[i].getString("cart_pid"));
										waybillItem.add("quantity",needManualQuantity);
										waybillItem.add("tracking_number",tracking_number);
										waybillItem.add("waybill_order_id",waybill_id);
										waybillItem.add("ps_id",ps_id);
										waybillItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);
										waybillItem.add("order_source",order_source);
										
										waybillItem.add("product_status",ProductStatusKey.IN_STORE);
										
										DBRow product = floorProductMgr.getDetailProductByPcid(cartProducts[i].get("cart_pid",0l));
										double prodcut_cost = product.get("unit_price",0d)*needManualQuantity;
										float one_weight = (float)(product.get("weight",0f)*needManualQuantity);
										float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
										String unit_name = product.getString("unit_name");
										double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
										
										waybillItem.add("p_name",product.getString("p_name"));
										waybillItem.add("saleroom",saleroom);
										waybillItem.add("unit_name",unit_name);
										waybillItem.add("weight",one_weight);
										waybillItem.add("prodcut_cost",prodcut_cost);
										waybillItem.add("shipping_cost",one_shipping_cost);
										floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);
										
//										for (int j=0; j<productsInSet.length; j++)
//										{
//											float productsInSetCount = productsInSet[j].get("quantity",0f)*needManualQuantity;
//											floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[j].get("pc_id",0l), productsInSetCount);
//										}
										
										//拼装好后最后一次性减库存
										floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,cart_pc_id,cart_quantity);
									}
								}
							}
						}
						else  //定制 
						{
							int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货
							
							
							DBRow productsCustomInSet[] = floorProductMgr.getProductUnionsBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
							
							DBRow product = floorProductMgr.getDetailProductByPcid(cartProducts[i].get("cart_pid",0l));
							
							String unit_name = product.getString("unit_name"); 
							double custom_product_price = product.get("unit_price",0d);//定制商品单价
							float custom_product_weight = product.get("weight",0f);//定制商品重量
							for (int j=0; j<productsCustomInSet.length; j++)
							{
//							DBRow union_product = floorProductMgr.getDetailProductByPcid(productsCustomInSet[j].get("pid", 0l));
//							custom_product_price += union_product.get("unit_price",0d)*productsCustomInSet[j].get("quantity",0f);
//							custom_product_weight += union_product.get("weight",0d)*productsCustomInSet[j].get("quantity",0f);
								
								synchronized(String.valueOf(ps_id)+"_"+productsCustomInSet[j].getString("pc_id"))
								{
									//检查库存
									DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[j].get("pid", 0l));
									if (detailPro==null)	//	商品还没进库，当然缺货
									{
										lacking = ProductStatusKey.STORE_OUT;
										wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
									}
									else
									{
										if (detailPro.get("store_count", 0f)<StringUtil.getFloat(productsCustomInSet[j].getString("quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")))
										{
											lacking = ProductStatusKey.STORE_OUT;
											wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
										}
									}
								}
							}
							
							//定制商品检查完全部散件后再扣库存
							for (int j = 0; j < productsCustomInSet.length; j++) 
							{
								if(lacking==ProductStatusKey.IN_STORE)
								{
									floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsCustomInSet[j].get("pid", 0l), StringUtil.getFloat(productsCustomInSet[j].getString("quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
								}
							}
							
							DBRow order_item = floorOrderMgr.getDetailOrderItem(cartProducts[i].get("order_item_id",0l));
							oid = order_item.get("oid",0l);
							DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
							double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
							double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
							double orderProductCost = getOrderProductCost(oid);
							String order_source = orderForIid.getString("order_source");
							
							DBRow waybillItem = new DBRow();
							waybillItem.add("oid",oid);
							waybillItem.add("order_item_id",cartProducts[i].getString("order_item_id"));
							waybillItem.add("pc_id",cartProducts[i].getString("cart_pid"));
							waybillItem.add("quantity",cartProducts[i].getString("cart_quantity"));
							waybillItem.add("tracking_number",tracking_number);
							waybillItem.add("waybill_order_id",waybill_id);
							waybillItem.add("ps_id",ps_id);
							waybillItem.add("product_type",cartProducts[i].getString("cart_product_type"));
							waybillItem.add("order_source",order_source);
							
							waybillItem.add("product_status",lacking);
							
							double prodcut_cost = custom_product_price*cartProducts[i].get("cart_quantity",0d);
							float one_weight = (float)(custom_product_weight*cartProducts[i].get("cart_quantity",0d));
							float one_shipping_cost = (float)(one_weight/all_weight*shipping_cost);
							double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
							
							waybillItem.add("p_name",product.getString("p_name"));
							waybillItem.add("saleroom",saleroom);
							waybillItem.add("unit_name",unit_name);
							waybillItem.add("weight",one_weight);
							waybillItem.add("prodcut_cost",prodcut_cost);
							waybillItem.add("shipping_cost",one_shipping_cost);
							floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);
						}
					}
			
			DBRow para = new DBRow();
			para.add("product_status",wayBillOrderIsLacking);
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
		} 
		catch (OrderItemWaitErrorException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addWayBillOrderItemsReduceStore",log);
		}
	}
	
	public void upLoadTrackingNumberToEbay(long waybill_id)
		throws Exception
	{
		DBRow waybillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		if(orderIds!=null&&orderIds.length>0)
		{
			String trackingNumber = waybillOrder.getString("tracking_number");
			for (int j = 0; j < orderIds.length; j++)
			{
				if(trackingNumber.trim().equals(""))
				{
					trackingNumber = "update soon";
				}
				productMgr.upLoadEbayTrackingNumber(orderIds[j].get("oid",0l),trackingNumber,waybillOrder.get("sc_id",0l));
			}
		}
		
	}
	
	/**
	 * 计算从订单转变为运单所用时间
	 * @param waybill_id
	 * @throws Exception
	 */
	public void calculationOrderToWayBill(long waybill_id)
		throws Exception
	{
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		for (int j = 0; j < orderIds.length; j++) 
		{
			DBRow porder = floorOrderMgr.getDetailPOrderByOid(orderIds[j].get("oid",0l));
			long post_date = new TDate(porder.getString("post_date")).getDateTime();
			long cost_time = System.currentTimeMillis()- post_date;
			DBRow para = new DBRow();
			para.add("outbound_cost",cost_time);
			
			floorOrderMgr.modPOrder(orderIds[j].get("oid",0l),para);
		}
	}
	
	/**
	 * 更新运单对应的订单的tracingNumber
	 * @param waybill_id
	 * @throws Exception
	 */
	public void updateTrackingNumberToOrder(long waybill_id)
		throws Exception
	{
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		
		for (int j = 0; j < orderIds.length; j++) 
		{
			
			DBRow[] waybillOrders = floorWayBillOrderMgrZJ.getTracingNumberByOrderId(orderIds[j].get("oid",0l));
			
			StringBuffer trackingNumber = new StringBuffer("");
			for (int k = 0; k < waybillOrders.length; k++) 
			{
				DBRow shipCompany = floorExpressMgr.getDetailCompany(waybillOrders[k].get("sc_id",0l));
				
				trackingNumber.append(shipCompany.getString("name")+" "+waybillOrders[k].getString("tracking_number"));
				
				if(k<waybillOrders.length-1)
				{
					trackingNumber.append(" ");
				}
			}
			
			orderMgr.printUpdateWayBillNumber(orderIds[j].get("oid",0l),trackingNumber.toString());
		}
	}
	
	/**
	 * 对session中的运单购物车进行商品价值估算
	 * @param cartWaybill
	 * @return
	 * @throws Exception
	 */
	private double productCost(DBRow[] cartWaybill)
		throws Exception
	{
		try 
		{
			double product_cost = 0;
			for (int i=0; i<cartWaybill.length; i++)
			{
				DBRow product = floorProductMgr.getDetailProductByPcid(cartWaybill[i].get("cart_pid",0l));
				product_cost += MoneyUtil.mul(product.get("unit_price", 0d),cartWaybill[i].get("cart_quantity", 0d));
			}
			
			return (product_cost);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"productCost",log);
		}
	}
	
	/**
	 * 返回与此订单所有有关联的运单
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderByOid(long oid)
		throws Exception
	{
		try 
		{
			return floorWayBillOrderMgrZJ.getWayBillOrderByOid(oid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillOrderByOid",log);
		}
	}
	
	/**
	 * 根据订单ID集合获得运单集合
	 * @param oids
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrdersByOids(long[] oids)
		throws Exception
	{
		try 
		{
			return floorWayBillOrderMgrZJ.getWayBillOrdersByOids(oids);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillOrdersByOids",log);
		}
	}
	
	/**
	 * 仓库取消运单（不做以待打印限定）
	 * @param request
	 * @throws Exception
	 */
	public void cancelWayBillOrderFromStore(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			
			if(waybill.get("parent_waybill_id",0l)>0)
			{
				wayBillMgrZR.deleteSplitWayBillBy(waybill_id,waybill.get("parent_waybill_id",0l),adminLoggerBean);
			}
			else
			{
				this.returnWayBillToOrder(waybill_id, adminLoggerBean);
			}
		} 
		catch(WayBillOrderPrintedException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelWayBillOrder",log);
		}
	}
	
	/**
	 * 运单退回订单
	 * @param waybill_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	private void returnWayBillToOrder(long waybill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		//业务单据取消，库位库存操作取消
		productStoreLocationMgrZJ.cancelOutListDetail(waybill_id,ProductStoreBillKey.WAYBILL_ORDER);
		
		DBRow[] wayBillOrderItems = floorWayBillOrderMgrZJ.getWayBillOrderItemsByWayBillId(waybill_id);
		
		//回退库存
		ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
		inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
		inProductStoreLogBean.setOid(waybill_id);
		inProductStoreLogBean.setBill_type(ProductStoreBillKey.WAYBILL_ORDER);
		InOutStoreKey is = new InOutStoreKey();
		
		inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_WAYBILL_CANCEL);
		
		for (int i = 0; i < wayBillOrderItems.length; i++)
		{
			//运单取消，订单明细待发数量回退
			floorOrderMgr.returnWaitQuantity(wayBillOrderItems[i].get("order_item_id",0l),wayBillOrderItems[i].get("quantity",0f));
			
			//订单的运单被取消了,检查修改订单状态
			orderMgr.changeOrderHandle(wayBillOrderItems[i].get("oid",0l));
			
			//开始退库存
			if(wayBillOrderItems[i].get("product_type",0)==ProductTypeKey.NORMAL||wayBillOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_STANDARD)//普通商品与标准套装
			{
				if(wayBillOrderItems[i].get("product_status",0)==ProductStatusKey.IN_STORE)//运单缺货商品未扣过库存
				{
//					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,wayBillOrderItems[i].get("ps_id",0l),wayBillOrderItems[i].get("pc_id", 0l),wayBillOrderItems[i].get("quantity",0f),0l);//库存添加
				}
				
			}
			else if(wayBillOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)//标准拼装
			{
				if(wayBillOrderItems[i].get("product_status",0)==ProductStatusKey.IN_STORE)//运单缺货商品未扣过库存
				{
//					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,wayBillOrderItems[i].get("ps_id",0l),wayBillOrderItems[i].get("pc_id", 0l),wayBillOrderItems[i].get("quantity",0f),0l);//库存添加
//					
//					productMgr.splitProductSystem(wayBillOrderItems[i].get("ps_id",0l),wayBillOrderItems[i].get("pc_id", 0l),(int)wayBillOrderItems[i].get("quantity",0f),ProductStoreBillKey.WAYBILL_ORDER,waybill_id, adminLoggerBean);
				}
			}
				
			else//标准拼装与定制
			{
				if(wayBillOrderItems[i].get("product_status",0)==ProductStatusKey.IN_STORE)//运单缺货商品未扣过库存
				{
					DBRow[] productUnions = floorProductMgr.getProductUnionsBySetPid(wayBillOrderItems[i].get("pc_id",0l));
					
					for (int j = 0; j < productUnions.length; j++) 
					{
						float count = wayBillOrderItems[i].get("quantity",0f)*productUnions[j].get("quantity",0f); 
//						floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,wayBillOrderItems[i].get("ps_id",0l),productUnions[j].get("pid", 0l),count,0l);//库存添加
					}
				}
			}
		}
		floorWayBillOrderMgrZJ.cancelWayBillOrder(waybill_id,adminLoggerBean.getAdid());
		
		//单据回退，库存批次回退
		productMgr.reBackAllBill(waybill_id,ProductStoreBillKey.WAYBILL_ORDER,adminLoggerBean);
		
		updateTrackingNumberToOrder(waybill_id);
		
		
		DBRow waybillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		
		DBRow waybillLog = new DBRow();
		waybillLog.add("waybill_id",waybill_id);
		waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Cancel);
		waybillLog.add("trackingNumber",waybillOrder.getString("tracking_number"));
		waybillLog.add("sc_id",waybillOrder.get("sc_id",0l));
		waybillLog.add("operator_adid",adminLoggerBean.getAdid());
		waybillLog.add("operator_time",DateUtil.NowStr());
		floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
		
		
		//运单取消记录订单日志
		this.cancelWayBillAddNoteOrder(waybill_id, adminLoggerBean);
		
		//所有通过运单操作订单的操作都完成了，再删除运单明细
		floorWayBillOrderMgrZJ.delWayBillOrderItems(waybill_id);
		//删除运单本身
		floorWayBillOrderMgrZJ.deleteWayBillById(waybill_id);
		
		WayBillIndexMgr.getInstance().deleteIndex(waybill_id);
	}
	
	/**
	 * 客服取消运单
	 */
	public void cancelWayBillOrder(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			
			if(waybill.get("status",0)!=WayBillOrderStatusKey.WAITPRINT||waybill.get("parent_waybill_id",0l)!=0)
			{
				throw new WayBillOrderPrintedException();
			}
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			this.returnWayBillToOrder(waybill_id, adminLoggerBean);
			
		} 
		catch(WayBillOrderPrintedException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelWayBillOrder",log);
		}
	}
	
	public double getOrderProductCost(long oid)
		throws Exception
	{
		try 
		{
			double product_cost = 0;//商品成本
			float weight = 0;		//商品重量
			double total_mc_gross = 0;//订单总成本

			//注意子订单、注意乘以商品数量
			//普通商品、标准套装、手工套装、定制商品

			DBRow orderItems[] = floorOrderMgr.getPOrderItemsByOid(oid);
			for (int i=0; i<orderItems.length; i++)
			{				
				product_cost += MoneyUtil.mul(orderItems[i].get("unit_price", 0d),orderItems[i].get("quantity", 0d));
				weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
			}
			return (product_cost);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderProductCost",log);
		}
	}
	
	
	
	/**
	 * 根据运单ID获得运单明细
	 * @param wayBill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderItems(long wayBill_id)
		throws Exception
	{
		try 
		{
			return (floorWayBillOrderMgrZJ.getWayBillOrderItemsByWayBillId(wayBill_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillOrderItems",log);
		}
	}
	
	/**
	 * 过滤运单
	 * @param pc_id
	 * @param product_line_id
	 * @param catalog_id
	 * @param order_source
	 * @param ps_id
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param out_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterWayBillOrder(String st_date,String en_date,String p_name,long product_line_id,long catalog_id,String order_source,long ps_id,long ca_id,long ccid,long pro_id,long out_id,PageCtrl pc,int status,long sc_id,int product_status,int internal_tracking_status)
		throws Exception
	{
		try 
		{
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			long pc_id = 0;
			if (product!=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return (floorWayBillOrderMgrZJ.searchWayBillOrder(st_date,en_date,pc_id, product_line_id, catalog_id, order_source, ps_id, ca_id, ccid, pro_id, out_id, pc,status,sc_id,product_status,internal_tracking_status));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterWayBillOrder",log);
		}
	}
	
	//add by zhangRui 返回具体的一个运单的详细信息
	@Override
	public DBRow getDetailInfoWayBillById(long wayBillId) throws Exception {
	 
		try 
		{
			return floorWayBillOrderMgrZJ.getWayBillOrderById(wayBillId);
			//return (floorWayBillOrderMgrZJ.searchWayBillOrder(pc_id, product_line_id, catalog_id, order_source, ps_id, ca_id, ccid, pro_id, out_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailInfoWayBillById",log);
		}
	}
	
	/**
	 * 修改运单的trackingNumber（EMS等打印后才能上传TrackingNumber）
	 * @param wayBillId
	 * @param tracking_number
	 * @throws Exception
	 */
	public void upLoadTrackingNumber(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			String tracking_number = StringUtil.getString(request,"tracking_number");
			
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow para = new DBRow();
			para.add("tracking_number",tracking_number);
			
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id,para);
			
			floorWayBillOrderMgrZJ.modWayBIllOrderItemsByWayBillId(waybill_id, para);
			
			upLoadTrackingNumberToEbay(waybill_id);
			
			updateTrackingNumberToOrder(waybill_id);
			
			printWayBill(waybill_id,adminLoggerBean.getAdid(),request);
			
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderByWayBillId(waybill_id);
			String client_id = waybill.getString("client_id");
			String address_name = waybill.getString("address_name");
			String address_street = waybill.getString("address_street");
			String address_zip = waybill.getString("address_zip");
			String address_city = waybill.getString("address_city");
			String address_state = waybill.getString("address_state");
			String address_country = waybill.getString("address_country");
			
			WayBillIndexMgr.getInstance().updateIndex(waybill_id, tracking_number, client_id, address_name, address_street, address_zip, address_city, address_state, address_country);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"upLoadTrackingNumber",log);
		}
	}
	
	/**
	 * 打印运单
	 */
	public void printWayBill(long waybill_id,long adid,HttpServletRequest request)
		throws Exception
	{
		try 
		{
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			DBRow wayBillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			DBRow shipCompany = floorExpressMgr.getDetailCompany(wayBillOrder.get("sc_id",0l));
			
			int key = 0;//1.DHL 2.USPS 3.FEDEX 4.EMS 5.EQUICK 6.EPacket 7.FedexIE
			if(shipCompany.getString("name").trim().toLowerCase().contains("dhl"))
			{
				key = 1;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("usps"))
			{
				key = 2;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("fedex"))
			{
				key = 3;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("ems"))
			{
				key = 4;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("equick"))
			{
				key = 5;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("epacket"))
			{
				key = 6;
			}
			if(shipCompany.getString("name").trim().toLowerCase().contains("fedexie")||shipCompany.getString("name").trim().toLowerCase().contains("fedexip"))//FedexIE与FedexIP使用同一个模板
			{
				key = 7;
			}
			
			long print_cost = System.currentTimeMillis()- new TDate(wayBillOrder.getString("create_date")).getDateTime();
			
			if(wayBillOrder.get("status",0)==WayBillOrderStatusKey.WAITPRINT)
			{
				DBRow para = new DBRow();
				para.add("status",WayBillOrderStatusKey.PERINTED);
				para.add("print_date",DateUtil.NowStr());
				para.add("print_account",adid);
				para.add("print_cost",print_cost);
				floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id,para);
				
				DBRow waybillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
				DBRow waybillLog = new DBRow();
				waybillLog.add("waybill_id",waybill_id);
				waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Print);
				waybillLog.add("trackingNumber",waybillOrder.getString("tracking_number"));
				waybillLog.add("sc_id",waybillOrder.get("sc_id",0l));
				waybillLog.add("operator_adid",adid);
				waybillLog.add("operator_time",DateUtil.NowStr());
				floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
				
				this.addOrderNoteWithWaybill(waybill_id,WaybillInternalOperKey.Print, adminLoggerBean);
			}
			
			
			DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
			try 
			{
				for (int i = 0; i < orderIds.length; i++) 
				{
					//1.DHL 2.USPS 3.FEDEX 4.EMS 5.EQUICK 6.EPacket
					switch (key)
					{
						case 1:
							sendMailForDHLPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
						case 2:
							sendMailForUSPSPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
						case 3:
							sendMailForFedexPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
						case 4:
							sendMailForEMSPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
						case 5	:
							sendMailForEQUICKPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
						case 6	:
							sendMailForEPacketPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;	
						case 7	:
							sendMailForFedexIEPrinted(orderIds[i].get("oid",0l),wayBillOrder.getString("tracking_number"));
							break;
					}
				}
			} 
			catch (Exception e) 
			{
				StackTraceElement[] ste = e.getStackTrace();
				StringBuffer sb = new StringBuffer();
				sb.append(e.getMessage() + "<br>");
				for (int i = 0;i < ste.length;i++)
				{
					sb.append(ste[i].toString() + "<br>");
				}
				
				String to = "zhanjie19861128@126.com";
				String subject = "SenMail error:"+e.getMessage();
		    	
				User user = new User("echo@vvme.com");
		        MailAddress mailAddress = new MailAddress(to);
		        MailBody mailBody = new MailBody(subject,sb.toString(),true,null);

		        Mail mail = new Mail(user);
		        mail.setAddress(mailAddress);
		        mail.setMailBody(mailBody);
		        mail.send();
			}
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"printWayBill",log);
		}
	}
	
	private void sendMailForEMSPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintEMSEmailPageString printUSPSEmailPageString = new PrintEMSEmailPageString(oid,trackingNumber);
		User user = new User(printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getPassWord(),printUSPSEmailPageString.getHost(),printUSPSEmailPageString.getPort(),printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getAuthor(),printUSPSEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printUSPSEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	    
	}
	
	private void sendMailForEPacketPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintEPacketEmailPageString printEPacketEmailPageString = new PrintEPacketEmailPageString(oid,trackingNumber);
		User user = new User(printEPacketEmailPageString.getUserName(),printEPacketEmailPageString.getPassWord(),printEPacketEmailPageString.getHost(),printEPacketEmailPageString.getPort(),printEPacketEmailPageString.getUserName(),printEPacketEmailPageString.getAuthor(),printEPacketEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printEPacketEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
	
	private void sendMailForUSPSPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		
		PrintUSPSEmailPageString printUSPSEmailPageString = new PrintUSPSEmailPageString(oid,trackingNumber);
		User user = new User(printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getPassWord(),printUSPSEmailPageString.getHost(),printUSPSEmailPageString.getPort(),printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getAuthor(),printUSPSEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));

	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printUSPSEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
	
	private void sendMailForDHLPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintDHLEmailPageString printUSPSEmailPageString = new PrintDHLEmailPageString(oid,trackingNumber);
		User user = new User(printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getPassWord(),printUSPSEmailPageString.getHost(),printUSPSEmailPageString.getPort(),printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getAuthor(),printUSPSEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printUSPSEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
	
	private void sendMailForFedexPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintFEDEXEmailPageString printUSPSEmailPageString = new PrintFEDEXEmailPageString(oid,trackingNumber);
		User user = new User(printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getPassWord(),printUSPSEmailPageString.getHost(),printUSPSEmailPageString.getPort(),printUSPSEmailPageString.getUserName(),printUSPSEmailPageString.getAuthor(),printUSPSEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printUSPSEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
	
	private void sendMailForFedexIEPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintFEDEXIEEmailPageString printFedexIEEmailPageString = new PrintFEDEXIEEmailPageString(oid,trackingNumber);
		User user = new User(printFedexIEEmailPageString.getUserName(),printFedexIEEmailPageString.getPassWord(),printFedexIEEmailPageString.getHost(),printFedexIEEmailPageString.getPort(),printFedexIEEmailPageString.getUserName(),printFedexIEEmailPageString.getAuthor(),printFedexIEEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("client_id"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printFedexIEEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
		
	private void sendMailForEQUICKPrinted(long oid,String trackingNumber)
		throws Exception
	{
		DBRow order = floorOrderMgr.getDetailPOrderByOid(oid);
		PrintEQUICKEmailPageString printEQUICKEmailPageString = new PrintEQUICKEmailPageString(oid,trackingNumber);
		
		User user = new User(printEQUICKEmailPageString.getUserName(),printEQUICKEmailPageString.getPassWord(),printEQUICKEmailPageString.getHost(),printEQUICKEmailPageString.getPort(),printEQUICKEmailPageString.getUserName(),printEQUICKEmailPageString.getAuthor(),printEQUICKEmailPageString.getRePly());
	    MailAddress mailAddress = new MailAddress(order.getString("payper_email"));
	    
	    if(order.getString("client_id").contains("@"))
	    {
	    	MailBody mailBody = new MailBody(printEQUICKEmailPageString,true,null);
		    
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
	    }	
	}
	
	/**
	 * 运单发货
	 * @param waybill_id
	 * @param ship_weight
	 * @throws Exception
	 */
	public void shipWayBill(long waybill_id,float ship_weight,long adid)
		throws Exception
	{
		DBRow wayBillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		
		int status = wayBillOrder.get("status",0);
		
		if(status==WayBillOrderStatusKey.PERINTED)
		{
			long delivery_cost = System.currentTimeMillis()- new TDate(wayBillOrder.getString("print_date")).getDateTime();
			
			DBRow para = new DBRow();
			para.add("status",WayBillOrderStatusKey.SHIPPED);
			para.add("delivery_weight",ship_weight);
			para.add("delivery_account",adid);
			para.add("delivery_date",DateUtil.NowStr());
			para.add("delivery_cost",delivery_cost);
			
			para.add("trace_date",DateUtil.NowStr());
			para.add("trace_flag",0);
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
			
			DBRow waybillLog = new DBRow();
			waybillLog.add("waybill_id",waybill_id);
			waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Send);
			waybillLog.add("trackingNumber",wayBillOrder.getString("tracking_number"));
			waybillLog.add("sc_id",wayBillOrder.get("sc_id",0l));
			waybillLog.add("operator_adid",adid);
			waybillLog.add("operator_time",DateUtil.NowStr());
			floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
			
			AdminLoginBean adminLoggerBean = new AdminLoginBean();
			adminLoggerBean.setAdid(adid);
			this.addOrderNoteWithWaybill(waybill_id,WaybillInternalOperKey.Send,adminLoggerBean);
			
			DBRow[] orders = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
			
			for (int i = 0; i < orders.length; i++) 
			{
				orderMgrZJ.checkOrderShip(orders[i].get("oid",0l));
			}
		}
//		batchMgrLL.batchWaybill(waybill_id, adid);
	}
	
	/**
	 * 重算登录人所属仓库的缺货运单
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void reCheckLackingWayBillOrdersSub(AdminLoginBean adminLoggerBean,long ps_id)
		throws Exception
	{
		DBRow[] lackingWayBillOrders = floorWayBillOrderMgrZJ.getLackingWayBillOrdersByPsid(ps_id);
		for(int q = 0;q<lackingWayBillOrders.length;q++)
		{
			DBRow[] wayBillOrderLackingItems = floorWayBillOrderMgrZJ.getLackingWayBillItems(lackingWayBillOrders[q].get("waybill_id",0l));
			
			//缺货运单明细扣库存
			int wayBillProductStatus = this.reCheckLackingWaybillOrderItems(wayBillOrderLackingItems, adminLoggerBean,ps_id);//返回运单的货物状态
			
			DBRow para = new DBRow();
			para.add("product_status",wayBillProductStatus);
			if(wayBillProductStatus==ProductStatusKey.IN_STORE)//从缺货变为不缺货，记录缺货时间
			{
				DBRow wayBillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(lackingWayBillOrders[q].get("waybill_id",0l));
				
				long lacking_cost = System.currentTimeMillis()- new TDate(wayBillOrder.getString("create_date")).getDateTime();
				
				para.add("lacking_cost",lacking_cost);
			}
			floorWayBillOrderMgrZJ.modWayBillOrderById(lackingWayBillOrders[q].get("waybill_id",0l), para);
			
			if(wayBillProductStatus==ProductStatusKey.IN_STORE)//从缺货变为不缺货，记录订单日志
			{
				this.reCheckWayBillAddNoteOrder(lackingWayBillOrders[q].get("waybill_id",0l), adminLoggerBean);
			}
		}
	}
	
	public void reCheckLackingWaybillOrder(HttpServletRequest request)
		throws Exception
	{
		long waybill_id = StringUtil.getLong(request,"waybill_id");
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		
		this.reCheckLackingWaybillOrderSub(waybill_id, adminLoggerBean);
	}
	
	/**
	 * 缺货运单重算(只重算一张运单)
	 * @param waybill_id
	 * @param adminLoggerBean
	 * @param ps_id
	 * @throws Exception
	 */
	public void reCheckLackingWaybillOrderSub(long waybill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			long ps_id = waybill.get("ps_id",0l);
			
			DBRow[] wayBillOrderLackingItems = floorWayBillOrderMgrZJ.getLackingWayBillItems(waybill_id);
			
			//缺货运单明细扣库存
			int wayBillProductStatus = this.reCheckLackingWaybillOrderItems(wayBillOrderLackingItems, adminLoggerBean,ps_id);//返回运单的货物状态
			
			DBRow para = new DBRow();
			para.add("product_status",wayBillProductStatus);
			if(wayBillProductStatus==ProductStatusKey.IN_STORE)//从缺货变为不缺货，记录缺货时间
			{
				DBRow wayBillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
				
				long lacking_cost = System.currentTimeMillis()- new TDate(wayBillOrder.getString("create_date")).getDateTime();
				
				para.add("lacking_cost",lacking_cost);
			}
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
			
			if(wayBillProductStatus==ProductStatusKey.IN_STORE)//从缺货变为不缺货，记录订单日志
			{
				this.reCheckWayBillAddNoteOrder(waybill_id, adminLoggerBean);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"reCheckLackingWaybillOrder",log);
		}
	}
	
	/**
	 * 此方法用于重算运单缺货时修改运单明细，运单明细拆分时要注意是否是修改
	 * @param lackingWayBillItems
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	private int reCheckLackingWaybillOrderItems(DBRow[] lackingWayBillItems,AdminLoginBean adminLoggerBean,long ps_id)
		throws Exception
	{
		int wayBillOrderIsLacking = ProductStatusKey.IN_STORE;
		long oid = 0;
		for (int i = 0; i < lackingWayBillItems.length; i++) 
		{
			//进出库日志
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(lackingWayBillItems[i].get("waybill_order_id",0l));
			deInProductStoreLogBean.setBill_type(ProductStoreBillKey.WAYBILL_ORDER);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_STOREOUT_WAYBILL);
			
			if (lackingWayBillItems[i].get("product_type",0)==ProductTypeKey.NORMAL)
			{
				int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货

				//确保两个线程同时检查库存的时候有先后顺序
				synchronized(String.valueOf(ps_id)+"_"+lackingWayBillItems[i].getString("pc_id"))
				{
					//检查库存
					//orderIsLacking
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,lackingWayBillItems[i].get("pc_id",0l));
					
					if (detailPro==null)	//	商品还没进库，当然缺货
					{
						lacking = ProductStatusKey.STORE_OUT;
						wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
					}
					else
					{
						if (detailPro.get("store_count", 0f)<lackingWayBillItems[i].get("quantity",0f))
						{
							lacking = ProductStatusKey.STORE_OUT;
							wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
						}
						else//缺货时不再扣库存，不缺货时才扣
						{
							floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,lackingWayBillItems[i].get("pc_id",0l),lackingWayBillItems[i].get("quantity",0f));
						}
					}
				}
				DBRow wayBillItem = new DBRow();
				wayBillItem.add("product_status",lacking);
				floorWayBillOrderMgrZJ.modWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l),wayBillItem);

			}
			else if (lackingWayBillItems[i].get("product_type",0)==ProductTypeKey.UNION_STANDARD)
			{
				synchronized(String.valueOf(ps_id)+"_"+lackingWayBillItems[i].getString("pc_id"))
				{
					boolean notBeCreateStorage = false;
					DBRow detailProductStorage = floorProductMgr.getDetailProductProductStorageByPcid( ps_id,lackingWayBillItems[i].get("pc_id",0l));
					if (detailProductStorage==null)
					{
						notBeCreateStorage = true;//没建库
						detailProductStorage = new DBRow();
						detailProductStorage.add("store_count",0f);
					}

					//1、标准套装够库存，直接减少库存
					if (detailProductStorage.get("store_count", 0f)>=lackingWayBillItems[i].get("quantity",0f))
					{
						DBRow waybillItem = new DBRow();
						
						waybillItem.add("product_status",ProductStatusKey.IN_STORE);//库存够套装，直接有货
						floorWayBillOrderMgrZJ.modWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l),waybillItem);

						floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,lackingWayBillItems[i].get("pc_id",0l),lackingWayBillItems[i].get("quantity",0f));
					}
					//2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
					else
					{
						float needManualQuantity ;
						//计算需要散件的套数
						if (detailProductStorage.get("store_count", 0f)>0)
						{
							//除了标准套装提供的数量外，还需要瓶装多少套散件
							needManualQuantity = lackingWayBillItems[i].get("quantity",0f) - detailProductStorage.get("store_count", 0f);
						}
						else
						{
							needManualQuantity = lackingWayBillItems[i].get("quantity",0f);
						}

						//标准套装不够，检查散件是否够拼装
						boolean isLacking = false;
						DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(lackingWayBillItems[i].get("pc_id",0l));
						for (int j=0; j<productsInSet.length; j++)
						{
							float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
							DBRow productStorage = floorProductMgr.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
							if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
							{
								isLacking = true;
								break;
							}
						}
						//散件也不够库存，并且商品已经在这个区域仓库建库，标记缺货不扣库存
						if (isLacking&&notBeCreateStorage==false)
						{
							DBRow waybillItem = new DBRow();
							waybillItem.add("product_status",ProductStatusKey.STORE_OUT);//套装商品拼装也缺货，整个套装缺货
							
							wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
							
							floorWayBillOrderMgrZJ.modWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l),waybillItem);
							
//							floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,lackingWayBillItems[i].get("pc_id",0l),lackingWayBillItems[i].get("quantity",0f));
							
						}
						//散件够库存
						else//标准套装缺货重算库存，符合部分套装，部分拼装形式，原套装item修改，同时新增散件item
						{
							//实际需要标准套装数-瓶装提供的数目
							float needUnionStandardCount = lackingWayBillItems[i].get("quantity",0f) - needManualQuantity;
							productMgr.combinationProductSystem(ps_id,StringUtil.getLong(lackingWayBillItems[i].getString("pc_id")),(int)needManualQuantity,ProductStoreBillKey.WAYBILL_ORDER,lackingWayBillItems[i].get("waybill_order_id",0l),adminLoggerBean,ProductStoreOperationKey.OUT_STORE_WAYBILL);

							//标准套装需要的数目
							if (needUnionStandardCount>0)
							{
								DBRow order_item = floorOrderMgr.getDetailOrderItem(lackingWayBillItems[i].get("order_item_id",0l));
								oid = order_item.get("oid",0l);
								DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
								double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
								double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
								double orderProductCost = getOrderProductCost(oid);
//								String order_source = orderForIid.getString("order_source");
								
								DBRow waybillItem = new DBRow();
								waybillItem.add("quantity",needUnionStandardCount);
//								waybillItem.add("ps_id",ps_id);
//								waybillItem.add("product_type",lackingWayBillItems[i].getString("product_type"));
								
								waybillItem.add("product_status",ProductStatusKey.IN_STORE);
								
								DBRow product = floorProductMgr.getDetailProductByPcid(lackingWayBillItems[i].get("pc_id",0l));
								double prodcut_cost = product.get("unit_price",0d)*needUnionStandardCount;
								float one_weight = (float)(product.get("weight",0f)*needUnionStandardCount);
								float one_shipping_cost = lackingWayBillItems[i].get("shipping_cost",0f)/lackingWayBillItems[i].get("quantity",0f)*needUnionStandardCount;
								String unit_name = product.getString("unit_name");
								double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
								
								waybillItem.add("p_name",product.getString("p_name"));
								waybillItem.add("saleroom",saleroom);
								waybillItem.add("unit_name",unit_name);
								waybillItem.add("weight",one_weight);
								waybillItem.add("prodcut_cost",prodcut_cost);
								waybillItem.add("shipping_cost",one_shipping_cost);
								
								floorWayBillOrderMgrZJ.modWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l),waybillItem);
								
//								floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,lackingWayBillItems[i].get("pc_id",0l),needUnionStandardCount);
							}
							else//不需要一个套装，则删除原先的套装型明细
							{
								floorWayBillOrderMgrZJ.delWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l));
							}
							
							//插入散件套装
							DBRow order_item = floorOrderMgr.getDetailOrderItem(lackingWayBillItems[i].get("order_item_id",0l));
							oid = order_item.get("oid",0l);
							DBRow orderForIid = floorOrderMgr.getDetailPOrderByOid(oid);
							double currency = systemConfig.getDoubleConfigValue(orderForIid.getString("mc_currency") );
							double orderSaleroom = orderForIid.get("mc_gross", 0d)*currency;
							double orderProductCost = getOrderProductCost(oid);
							String order_source = orderForIid.getString("order_source");
							
							DBRow waybillItem = new DBRow();
							waybillItem.add("oid",oid);
							waybillItem.add("order_item_id",lackingWayBillItems[i].getString("order_item_id"));
							waybillItem.add("pc_id",lackingWayBillItems[i].getString("pc_id"));
							waybillItem.add("quantity",needManualQuantity);
							waybillItem.add("tracking_number",lackingWayBillItems[i].getString("tracking_number"));
							waybillItem.add("waybill_order_id",lackingWayBillItems[i].get("waybill_order_id",0l));
							waybillItem.add("ps_id",ps_id);
							waybillItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);
							waybillItem.add("order_source",order_source);
							
							waybillItem.add("product_status",ProductStatusKey.IN_STORE);
							
							DBRow product = floorProductMgr.getDetailProductByPcid(lackingWayBillItems[i].get("pc_id",0l));
							double prodcut_cost = product.get("unit_price",0d)*needManualQuantity;
							float one_weight = (float)(product.get("weight",0f)*needManualQuantity);
							float one_shipping_cost = lackingWayBillItems[i].get("shipping_cost",0f)/lackingWayBillItems[i].get("quantity",0f)*needManualQuantity;
							String unit_name = product.getString("unit_name");
							double saleroom = prodcut_cost/orderProductCost*orderSaleroom;
							
							waybillItem.add("p_name",product.getString("p_name"));
							waybillItem.add("saleroom",saleroom);
							waybillItem.add("unit_name",unit_name);
							waybillItem.add("weight",one_weight);
							waybillItem.add("prodcut_cost",prodcut_cost);
							waybillItem.add("shipping_cost",one_shipping_cost);
							floorWayBillOrderMgrZJ.addWayBillOrderItem(waybillItem);
							
//							for (int j=0; j<productsInSet.length; j++)
//							{
//								float productsInSetCount = productsInSet[j].get("quantity",0f)*needManualQuantity;
//								floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[j].get("pc_id",0l), productsInSetCount);
//							}
							floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StringUtil.getLong(lackingWayBillItems[i].getString("pc_id")),lackingWayBillItems[i].get("quantity",0f));
						}
					}
				}
			}
			else  //定制 
			{
				int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货
				
				
				DBRow productsCustomInSet[] = floorProductMgr.getProductUnionsBySetPid(lackingWayBillItems[i].get("pc_id",0l));
				
				DBRow product = floorProductMgr.getDetailProductByPcid(lackingWayBillItems[i].get("pc_id",0l));
				
				String unit_name = product.getString("unit_name"); 
				double custom_product_price = product.get("unit_price",0d);//定制商品单价
				float custom_product_weight = product.get("weight",0f);//定制商品重量
				for (int j=0; j<productsCustomInSet.length; j++)
				{
					
					synchronized(String.valueOf(ps_id)+"_"+productsCustomInSet[j].getString("pc_id"))
					{
						//检查库存
						DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[j].get("pid", 0l));
						if (detailPro==null)	//	商品还没进库，当然缺货
						{
							lacking = ProductStatusKey.STORE_OUT;
							wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<productsCustomInSet[j].get("quantity",0f)*lackingWayBillItems[i].get("quantity",0f))
							{
								lacking = ProductStatusKey.STORE_OUT;
								wayBillOrderIsLacking = ProductStatusKey.STORE_OUT;
							}
						}
						
					}
				}
				
				//定制商品的散件全部不缺货才扣库存
				for (int j = 0; j < productsCustomInSet.length; j++) 
				{
					if(lacking==ProductStatusKey.IN_STORE)
					{
						floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsCustomInSet[j].get("pid", 0l),productsCustomInSet[j].get("quantity",0f)*lackingWayBillItems[i].get("quantity",0f));
					}
				}
			
				DBRow waybillItem = new DBRow();
				
				waybillItem.add("product_status",lacking);
				floorWayBillOrderMgrZJ.modWayBillOrderItemById(lackingWayBillItems[i].get("waybill_order_item_id",0l),waybillItem);
			}
		}
		
		return wayBillOrderIsLacking;
	}
	
	/**
	 * 获得订单最小关税的关税代码
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public String getHsCode(long waybill_id)
		throws Exception
	{
		try
		{
			HashMap<String, DBRow> catalog = new HashMap<String, DBRow>();
			
			DBRow orderItems[] = floorWayBillOrderMgrZJ.getWayBillOrderItemsByWayBillId(waybill_id);
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			//先找到订单所有商品所属的所有父类
			//把父类的tax起来
			for (int i=0; i<orderItems.length; i++)
			{
				DBRow fathers[] = tree.getAllFather(floorProductMgr.getDetailProductByPcid(orderItems[i].get("pc_id",0l)).get("catalog_id",0l));
				
				if (catalog.containsKey(fathers[0].get("id", 0l))==false)
				{
					catalog.put(fathers[0].getString("id"), fathers[0]);
				}
			}
			//去除该订单包含的最低tax
			Set<String> keys = catalog.keySet();
			String maxTaxId=null;
			float tmpTax = 0;
			int i = 0;
			for(String key:keys)
			{   
				DBRow c = (DBRow)catalog.get(key);
				if (c.getString("hs_code").equals(""))
				{
					continue;
				}
				if (i++==0)
				{
					maxTaxId = c.getString("hs_code");
					tmpTax = c.get("tax",0f);
				}
				if (tmpTax>c.get("tax",0f))
				{
					maxTaxId = c.getString("hs_code");
					tmpTax = c.get("tax",0f);
				}
			}
			
			return(maxTaxId);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"Exception",log);
		}
	}
	
	/**
	 * 根据运单返回订单列表
	 */
	public DBRow[] returnOrdersByWayBillId(long waybill_id)
		throws Exception
	{
		return floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
	}
	
	/**
	 * 根据商品ID，仓库ID，运单明细状态，查询运单
	 * @param pc_id
	 * @param ps_id
	 * @param product_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderByPcid(long pc_id,long ps_id,int[] product_status)
		throws Exception
	{
		try 
		{
			return floorWayBillOrderMgrZJ.getWayBillOrderByPcid(pc_id, ps_id, product_status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillOrderByPcid",log);
		}
	}
	
	/**
	 * 根据商品ID，订单ID获得发货时间
	 * @param oid
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryDateByOidandPcid(long oid,long pc_id)
		throws Exception
	{
		try 
		{
			return floorWayBillOrderMgrZJ.getDeliveryDateByOidandPcid(oid, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDeliveryDateByOidandPcid",log);
		}
	}
	
	public void errorUpdate(String trackingNumber)
		throws Exception
	{
		DBRow[] waybill_ids = floorWayBillOrderMgrZJ.error(trackingNumber);
		
		for (int i = 0; i < waybill_ids.length; i++) 
		{
			updateTrackingNumberToOrder(waybill_ids[i].get("waybill_id",0l));
		}
	}
	
	/**
	 * 运单统计
	 * @param start_date
	 * @param end_date
	 * @param date_type
	 * @param sc_id
	 * @param differences
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statisticsWayBillOrder(String start_date,String end_date,int date_type,long sc_id,float differences,int difference_type,int cost,int cost_type,long ps_id,int unfinished,int store_out_unfinished,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return floorWayBillOrderMgrZJ.statisticsWayBillOrder(start_date, end_date, date_type, sc_id, differences,difference_type,cost,cost_type,ps_id,unfinished,store_out_unfinished,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 根据内部运单ID查询
	 */
	public DBRow getWayBillOrderByWayBillId(long waybill_id) 
		throws Exception 
	{
		try 
		{
			return floorWayBillOrderMgrZJ.getWayBillOrderByWayBillId(waybill_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillOrderByWayBillId",log);
		}
	}
	
	/**
	 * 运单被退货
	 * @param request
	 * @throws Exception
	 */
	public void wayBillReturn(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			int return_type = StringUtil.getInt(request,"return_type");
			String return_note = StringUtil.getString(request,"return_note");
			
			DBRow para = new DBRow(); 
			para.add("waybill_id",waybill_id);
			para.add("return_type",return_type);
			para.add("return_note",return_note);
			para.add("result",null);
			para.add("result_note",null);
			
			
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"wayBillReturn",log);
		}
	}
	
	/**
	 * 运单退货处理结果
	 * @param request
	 * @throws Exception
	 */
	public void wayBillResult(HttpServletRequest request)
		throws Exception
	{
		long waybill_id = StringUtil.getLong(request,"waybill_id");
		int result_type = StringUtil.getInt(request,"result");
		String result_note = StringUtil.getString(request,"result_note");
		
		DBRow para = new DBRow(); 
		para.add("waybill_id",waybill_id);
		para.add("result",result_type);
		para.add("result_note",result_note);
		
		floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
	}
	
	/**
	 * 导出运单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportWayBill(HttpServletRequest request)
		throws Exception
	{
		String p_name = StringUtil.getString(request,"p_name","");
		      
		String order_source =  StringUtil.getString(request,"order_source","");
		long ps_id = StringUtil.getLong(request,"ps_id",0l);
		long ca_id = StringUtil.getLong(request,"ca_id",0l);
		long ccid = StringUtil.getLong(request,"ccid",0l);
		long pro_id = StringUtil.getLong(request,"pro_id",0l);
		long out_id = StringUtil.getLong(request,"out_id",0l);
		long pcid = StringUtil.getLong(request,"pcid",0l);;
		long pro_line_id = StringUtil.getLong(request,"pro_line_id",0l); ;
		int status = StringUtil.getInt(request,"status");
		int product_status = StringUtil.getInt(request,"product_status");
		int internal_tracking_status = StringUtil.getInt(request,"internal_tracking_status",-1);
		 
		 
		String st = StringUtil.getString(request,"st");
		String en = StringUtil.getString(request,"en");
		long sc_id = StringUtil.getLong(request,"sc_id",0);
		 		
		int nextRowNumber = 1;//记录应当创建第几行
		
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		
		long pc_id = 0;
		if (product!=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		DBRow[] wayBillOrders = floorWayBillOrderMgrZJ.searchWayBillOrder(st, en, pc_id, pro_line_id, pcid, order_source, ps_id, ca_id, ccid, pro_id, out_id,null, status, sc_id, product_status,internal_tracking_status);
		
		//读取模板
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/waybill/wayBillTemplate.xlsx"));  
		XSSFSheet sheet= wb.getSheet("Sheet1");
		
		for (int i = 0; i < wayBillOrders.length; i++)
		{
			DBRow[] wayBillDetails = floorWayBillOrderMgrZJ.getAllItemInWayBillById(wayBillOrders[i].get("waybill_id",0l));
			
			
			
			int form = nextRowNumber;//从第几行开始合并
			
			DBRow allSaleroom = floorWayBillOrderMgrZJ.getWayBillSaleroomByWayBillId(wayBillOrders[i].get("waybill_id",0l));
			double waybill_saleroom = allSaleroom.get("all_saleroom",0d);
			double waybill_profit = waybill_saleroom-wayBillOrders[i].get("shipping_cost",0d)-wayBillOrders[i].get("waybill_prodcut_cost",0d);
			
			for (int j = 0; j < wayBillDetails.length; j++) 
			{
				XSSFRow row = sheet.createRow(nextRowNumber);
				
				nextRowNumber++;
				
				DBRow productAllDetail = floorProductMgr.getAllDetailProductByPcid(wayBillDetails[j].get("pc_id",0l));
				
				row.createCell(0).setCellValue(productAllDetail.getString("p_name"));
				row.createCell(1).setCellValue(wayBillDetails[j].get("quantity",0f));
				row.createCell(2).setCellValue(productAllDetail.getString("product_line_name"));
				row.createCell(3).setCellValue(productAllDetail.getString("catalog_name"));
				row.createCell(4).setCellValue(wayBillDetails[j].get("shipping_cost",0d));
				row.createCell(5).setCellValue(wayBillDetails[j].get("prodcut_cost",0d));
				
				double saleroom = wayBillDetails[j].get("saleroom",0d);
				double profit = wayBillDetails[j].get("saleroom",0d)-wayBillDetails[j].get("prodcut_cost",0d)-wayBillDetails[j].get("shipping_cost",0d);
				
				row.createCell(6).setCellValue(saleroom);
				row.createCell(7).setCellValue(profit);
				
//				if(j==0)
//				{
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,8,8));
					
					DBRow productStoreCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(wayBillOrders[i].get("ps_id",0l));
					row.createCell(8).setCellValue(productStoreCatalog.getString("title"));
					
					
					DBRow shipingCompany = floorExpressMgr.getDetailCompany(wayBillOrders[i].get("sc_id",0l));
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,9,9));
					row.createCell(9).setCellValue(shipingCompany.getString("name"));
					if(j==0)
					{
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,10,10));
						row.createCell(10).setCellValue(wayBillOrders[i].get("waybill_id",0l));
						
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,11,11));
						row.createCell(11).setCellValue(wayBillOrders[i].getString("address_country"));
					}
					
					
					WayBillOrderStatusKey wayBillOrderStatusKey = new WayBillOrderStatusKey();
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,11,11));
					row.createCell(12).setCellValue(wayBillOrderStatusKey.getStatusById(wayBillOrders[i].get("status",0)));
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,12,12));
					row.createCell(13).setCellValue(wayBillOrders[i].getString("tracking_number"));
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,13,13));
					row.createCell(14).setCellValue(wayBillOrders[i].get("shipping_cost",0d));
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,14,14));
					row.createCell(15).setCellValue(wayBillOrders[i].get("waybill_prodcut_cost",0d));
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,15,15));
					row.createCell(16).setCellValue(waybill_saleroom);
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,16,16));
					row.createCell(17).setCellValue(waybill_profit);
					
					row.createCell(18).setCellValue(wayBillOrders[i].get("delivery_weight",0d));//数据库为float，如果不使用double类型，excel模板的样式认为自己被改变，无法形成保留2位效果
					
					
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,17,17));
					row.createCell(19).setCellValue(wayBillOrders[i].getString("create_date"));
					
					int print_hour = (int) MoneyUtil.round((double)(wayBillOrders[i].get("print_cost",0l)/3600/1000),0) ;
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,18,18));
					row.createCell(20).setCellValue(print_hour);
					
					int delivery_hour = (int) MoneyUtil.round((double)(wayBillOrders[i].get("delivery_cost",0l)/3600/1000),0);
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,19,19));
					row.createCell(21).setCellValue(delivery_hour);
					
					int lacking_hour = (int) MoneyUtil.round((double)(wayBillOrders[i].get("lacking_cost",0l)/3600/1000),0) ;
					//sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,20,20));
					row.createCell(22).setCellValue(lacking_hour);
					
					DBRow order = floorOrderMgr.getDetailPOrderByOid(wayBillDetails[j].get("oid",0l));
					
					row.createCell(23).setCellValue(order.getString("order_source"));
//				}
			}
		}
		
		//写文件  
		String path = "upl_excel_tmp/exportWayBill_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
		OutputStream os= new FileOutputStream(Environment.getHome()+path);
		wb.write(os);  
		os.close();  
		return (path);
	}
	
	/**
	 * 生产运单记录订单日志
	 * @param waybill_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void createWayBillAddNoteOrder(long waybill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		
		DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		
		DBRow shippingCompany = floorExpressMgr.getDetailCompany(waybill.get("sc_id",0l));
		
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		int trace_type;
		
		TracingOrderKey trackingOrderKey = new TracingOrderKey();
		
		DBRow waybillOrder = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		
		if(waybillOrder.get("product_status",0)==ProductStatusKey.STORE_OUT)//运单是缺货状态
		{
			trace_type = TracingOrderKey.RECORD_STOREOUT_WAYBILL;
		}
		else
		{
			trace_type = TracingOrderKey.RECORD_NORMAL_WAYBILL;
		}
		
		for (int j = 0; j < orderIds.length; j++) 
		{
			orderMgr.addPOrderNotePrivate(orderIds[j].get("oid",0l),trackingOrderKey.getTracingOrderKeyById(String.valueOf(trace_type))+shippingCompany.getString("name")+":"+waybill.getString("tracking_number"),trace_type,adminLoggerBean,0);
		}
	}
	
	/**
	 * 取消运单回填订单日志（必须在运单删除前执行）
	 * @param waybill_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void cancelWayBillAddNoteOrder(long waybill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		
		DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
		
		DBRow shippingCompany = floorExpressMgr.getDetailCompany(waybill.get("sc_id",0l));
		
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		int trace_type = TracingOrderKey.CANCEL_WAYBILL;
		
		TracingOrderKey trackingOrderKey = new TracingOrderKey();
		
		for (int j = 0; j < orderIds.length; j++) 
		{
			orderMgr.addPOrderNotePrivate(orderIds[j].get("oid",0l),trackingOrderKey.getTracingOrderKeyById(String.valueOf(trace_type))+shippingCompany.getString("name")+":"+waybill.getString("tracking_number"),trace_type,adminLoggerBean,0);
		}
	}
	
	public void reCheckWayBillAddNoteOrder(long waybill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		DBRow[] orderIds = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
		
		for (int j = 0; j < orderIds.length; j++) 
		{
			orderMgr.addPOrderNotePrivate(orderIds[j].get("oid",0l),"",TracingOrderKey.WAYBILL_REPLENISH,adminLoggerBean,0);
		}
		
		//运单从缺货变为有货，跟进日期更新
		DBRow para = new DBRow();
		para.add("trace_date",DateUtil.NowStr());
		floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
	}
	
	/**
	 * 更新residentialStatus
	 */
	public void updateResidentialStatus(long waybill_id,String residentialStatus) 
		throws Exception 
	{
		try
		{
			DBRow para = new DBRow();
			para.add("residential_status",residentialStatus);
			
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateResidentialStatus",log);
		}
	}
	
	/**
	 * 运单日志回填订单日志
	 * @param waybill_id
	 * @throws Exception
	 */
	public void addOrderNoteWithWaybill(long waybill_id,int operation_type,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			
			DBRow shippingCompany = floorExpressMgr.getDetailCompany(waybill.get("sc_id",0l));
			
			DBRow[] orders = floorWayBillOrderMgrZJ.getOrderIdByWayBill(waybill_id);
			
			WaybillInternalOperKey waybillInternalOperKey = new WaybillInternalOperKey();
			
			
			for (int i = 0; i < orders.length; i++) 
			{
				orderMgr.addPOrderNotePrivate(orders[i].get("oid",0l),waybillInternalOperKey.getWaybillInternalOper(String.valueOf(operation_type))+shippingCompany.getString("name")+":"+waybill.getString("tracking_number"),TracingOrderKey.WAYBILL_TRACKING, adminLoggerBean,0);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOrderNoteWithWaybill",log);
		}
	}
	
	/**
	 * 添加运单跟进
	 * @param request
	 * @throws Exception
	 */
	public void addWaybillNote(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String note = StringUtil.getString(request,"note");
			int trace_type = StringUtil.getInt(request,"trace_type");
			long waybill_id = StringUtil.getLong(request,"waybill_id");
			
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow waybillNote = new DBRow();
			waybillNote.add("note",note);
			waybillNote.add("trace_type",trace_type);
			waybillNote.add("waybill_id",waybill_id);
			waybillNote.add("post_date",DateUtil.NowStr());
			waybillNote.add("adid",adminLoggerBean.getAdid());			
			floorWayBillOrderMgrZJ.addWaybillNote(waybillNote);
			
			DBRow para = new DBRow();
			para.add("trace_date",DateUtil.NowStr());
			floorWayBillOrderMgrZJ.modWayBillOrderById(waybill_id, para);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addWaybillNote",log);
		}
	}
	
	/**
	 * 获得发货仓库的未跟进缺货运单
	 * @param stockout_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockoutWaybillConutGroupByPs()
		throws Exception
	{
		try 
		{
			int stockout_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_stockout_period"));
			
			DBRow[] storageCatalogs = catalogMgr.getProductDevStorageCatalogTree();
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i = 0; i < storageCatalogs.length; i++) 
			{
				DBRow stockCount = floorWayBillOrderMgrZJ.getStockoutWaybillOrderCountByPs(stockout_day,storageCatalogs[i].get("id",0l));
				
				if(stockCount == null)
				{
					stockCount = new DBRow();
				}
				
				DBRow row = new DBRow();
				row.add("stockcount",stockCount.get("stockout_count",0));
				row.add("psid",storageCatalogs[i].get("id",0l));
				row.add("title",storageCatalogs[i].getString("title"));
				
				list.add(row);
			}
			
			return list.toArray(new DBRow[0]);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getStockoutWaybillConutGroupByPs",log);
		}
	}
	
	/**
	 * 获得超期正常未发货且未跟进运单
	 * @param overdue_hour
	 * @param track_day
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOverdueSendWaybillOrderCountByPs()
		throws Exception
	{
		int overdue_hour = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_send_period"));
		int overdue_track_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_track_day"));
		
		DBRow[] storageCatalogs = catalogMgr.getProductDevStorageCatalogTree();
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++)
		{
			DBRow overdueCount = floorWayBillOrderMgrZJ.getOverdueSendWaybillOrderCountByPs(overdue_hour,overdue_track_day,storageCatalogs[i].get("id",0l));
			
			if(overdueCount == null)
			{
				overdueCount = new DBRow();
			}
			
			DBRow row = new DBRow();
			row.add("overduecount",overdueCount.get("overdue_send_count",0));
			row.add("title",storageCatalogs[i].getString("title"));
			row.add("psid",storageCatalogs[i].get("id",0l));
			
			list.add(row);
		}
		
		return list.toArray(new DBRow[0]);
	}
	
	/**
	 * 根据仓库获得超期正常未发货需跟进运单
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOverdueSendWaybillOrderByPs(long ps_id,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			int overdue_hour = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_send_period"));
			int overdue_track_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_track_day"));
			
			return (floorWayBillOrderMgrZJ.getOverdueSendWaybillOrders(overdue_hour, overdue_track_day,ps_id,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOverdueSendWaybillOrderByPs",log);
		}
	}
	
	public String waybillSendDownload(HttpServletRequest request)
		throws Exception
	{
		long cid = StringUtil.getLong(request,"cid");
		long scid = StringUtil.getLong(request,"sc_id");
		String st_datemark = StringUtil.getString(request,"st");
		String en_datemark = StringUtil.getString(request,"en");
		
		TDate tDate = new TDate();

		if (st_datemark.equals("")&&en_datemark.equals(""))
		{
			st_datemark = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			en_datemark = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
		}

		long stLong = tDate.getDateTime(st_datemark);
		long enLong = tDate.getDateTime(en_datemark);
		
		DBRow[] waybillSends = productMgr.getShipProductsByStEnPcCidScid(cid, scid, stLong,enLong);
		
		//读取模板
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/waybill/send_waybill.xlsx"));  
		XSSFSheet sheet= wb.getSheet("Sheet1");
		
		int nextRowNumber = 1;//记录应当创建第几行
		
		for (int i = 0; i < waybillSends.length; i++)
		{
			DBRow waybill = this.getDetailInfoWayBillById(waybillSends[i].get("name",0l));
			
			float weight = MoneyUtil.round(waybill.get("print_weight",0f),2);
			
			DBRow[] wayBillDetails = floorWayBillOrderMgrZJ.getAllItemInWayBillById(waybill.get("waybill_id",0l));
			
			int form = nextRowNumber;//从第几行开始合并
			
			float sum_count = 0;
			
			for (int j = 0; j < wayBillDetails.length; j++) 
			{
				sum_count += wayBillDetails[j].get("quantity",0f);
			}
			
			String inv_tv = waybill.getString("inv_tv");
			double dutiableDeclared;
			double dutiableUnitPrice;
			if(inv_tv.trim().equals(""))
			{
				dutiableUnitPrice = MoneyUtil.round(Double.parseDouble(waybill.getString("inv_uv")),2);
				dutiableDeclared = dutiableUnitPrice*sum_count;
			}
			else
			{
				dutiableDeclared = Double.parseDouble(inv_tv);
				dutiableUnitPrice = MoneyUtil.div(dutiableDeclared,sum_count,2);
			}
			
			if(wayBillDetails.length>3)
			{
				XSSFRow row = sheet.createRow(nextRowNumber);
				
				nextRowNumber++;
				
				float count = MoneyUtil.round(sum_count,0);
				DBRow productAllDetail = floorProductMgr.getAllDetailProductByPcid(wayBillDetails[0].get("pc_id",0l));
				
				row.createCell(0).setCellValue(1);
				row.createCell(1).setCellValue(waybill.getString("tracking_number"));
				row.createCell(2).setCellValue(waybill.getString("inv_dog_chinese")+"("+waybill.getString("material_chinese")+")");
				row.createCell(3).setCellValue(productAllDetail.getString("p_name"));
				row.createCell(4).setCellValue(count);
				
				row.createCell(5).setCellValue(Double.parseDouble(String.valueOf(weight)));
				row.createCell(6).setCellValue(dutiableUnitPrice);
				row.createCell(7).setCellValue(MoneyUtil.mul(dutiableUnitPrice,count));
				
				row.createCell(8).setCellValue(waybill.getString("address_country"));
				row.createCell(9).setCellValue(waybill.getString("address_name"));
			}
			else
			{
				for (int j = 0; j < wayBillDetails.length; j++) 
				{
					XSSFRow row = sheet.createRow(nextRowNumber);
					
					nextRowNumber++;
					
					float count = MoneyUtil.round(wayBillDetails[j].get("quantity",0f),0);
					DBRow productAllDetail = floorProductMgr.getAllDetailProductByPcid(wayBillDetails[j].get("pc_id",0l));
					
					if(j==0)
					{
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,0,0));
						row.createCell(0).setCellValue(1);
						
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,1,1));
						row.createCell(1).setCellValue(waybill.getString("tracking_number"));
						
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,2,2));
						row.createCell(2).setCellValue(waybill.getString("inv_dog_chinese")+"("+waybill.getString("material_chinese")+")");
					}
					
					row.createCell(3).setCellValue(productAllDetail.getString("p_name"));
					row.createCell(4).setCellValue(count);
					
					if(j==0)
					{
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,5,5));
						row.createCell(5).setCellValue(Double.parseDouble(String.valueOf(weight)));
					}
					
					row.createCell(6).setCellValue(dutiableUnitPrice);
					row.createCell(7).setCellValue(MoneyUtil.mul(dutiableUnitPrice,count));
					
					if(j==0)
					{
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,8,8));
						row.createCell(8).setCellValue(waybill.getString("address_country"));
						
						sheet.addMergedRegion(new CellRangeAddress(form,form+wayBillDetails.length-1,9,9));
						row.createCell(9).setCellValue(waybill.getString("address_name"));
					}
				}
			}
		}
		
		//写文件  
		String path = "upl_excel_tmp/exportSendWayBill_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
		OutputStream os= new FileOutputStream(Environment.getHome()+path);
		wb.write(os);  
		os.close();  
		return (path);
		
	}

	/**
	 * 根据仓库获得缺货未跟进运单
	 */
	public DBRow[] getStockoutWaybillByPs(long ps_id,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			int stockout_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_stockout_period"));
			
			return floorWayBillOrderMgrZJ.getStockoutWaybillOrders(stockout_day,ps_id,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStockoutWaybillByPs",log);
		}
	}
	
	/**
	 * 导出运单数据统计
	 */
	
	public String exportWayBillCount(HttpServletRequest request)
		throws WareHouseErrorException,Exception 
	{
		try
		{
			String st = StringUtil.getString(request,"st");
			String en = StringUtil.getString(request,"en");
			long ps_id = StringUtil.getLong(request,"ps_id",0l);
			
			DBRow[] wayBillCount=floorWayBillOrderMgrZJ.getWayBillCount(st, en, ps_id);
			DBRow DateRow[]= floorWayBillOrderMgrZJ.getDate(st, en);
			
			if(wayBillCount.length>0){
				XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/waybill/ExportWaybillCountModel.xlsx"));  
				XSSFSheet sheet= wb.getSheet("Sheet1");
				
				XSSFCellStyle style = wb.createCellStyle();
				style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				style.setLocked(false); //创建样式
				style.setWrapText(true);
				
				XSSFFont  font =wb.createFont();
				font.setFontName("Arial");   
				font.setFontHeightInPoints((short)10);   
				font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); 
				
				XSSFCellStyle styleTitle = wb.createCellStyle();
				styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				styleTitle.setLocked(false);  
				styleTitle.setWrapText(true);
				styleTitle.setFont(font);
				
				XSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); 
				stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				//title赋值
				XSSFRow row = sheet.createRow(0);  
				
				row.createCell(0).setCellValue("发货仓库");
				row.getCell(0).setCellStyle(styleTitle);
				
				row.createCell(1).setCellValue("阶段总发货数");
				row.getCell(1).setCellStyle(styleTitle);
				
				int blankCell = 1;//时间显示从第几列开始
				blankCell++;
				for (int i = 0; i < DateRow.length; i++)
				{
					row.createCell(i+blankCell).setCellValue(DateRow[i].getString("date"));
					row.getCell(i+blankCell).setCellStyle(styleTitle);	
				}
				//赋值
				for(int i=0;i<wayBillCount.length;i++){
					blankCell = 1;
					row = sheet.createRow(i+1); 
					DBRow rowTemp = wayBillCount[i];
					

					row.createCell(0).setCellValue(rowTemp.getString("title"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(rowTemp.getString("sum_quantity"));
					row.getCell(1).setCellStyle(style);
					
					blankCell++;
					for (int j = 0; j < DateRow.length; j++)
					{
						row.createCell(j+blankCell).setCellValue(rowTemp.get("date_"+j,0d));
						row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
						row.getCell(j+blankCell).setCellStyle(style);	
					}
					
				}
				
				//写文件
				String path = "upl_excel_tmp/exportWaybillCount"+st+"至"+en+".xlsx";
				OutputStream os= new FileOutputStream(Environment.getHome()+path);
				wb.write(os);  
				os.close();  
				return (path);
			}
			else
			{
				return "WareHouseErrorException";
			}
			
		}catch (WareHouseErrorException e) 
		{
			throw e;
	    }
		catch (Exception e) 
		{
			throw new SystemException(e,"exportWayBillCount",log);
	    }
			
    }
	
	public void setFloorWayBillOrderMgrZJ(FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) 
	{
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setFloorExpressMgr(FloorExpressMgr floorExpressMgr) {
		this.floorExpressMgr = floorExpressMgr;
	}

	public void setOrderMgrZJ(OrderMgrIFaceZJ orderMgrZJ) {
		this.orderMgrZJ = orderMgrZJ;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setFloorWaybillLogMgrZJ(FloorWaybillLogMgrZJ floorWaybillLogMgrZJ) {
		this.floorWaybillLogMgrZJ = floorWaybillLogMgrZJ;
	}

	public void setWayBillMgrZR(WayBillMgrIfaceZR wayBillMgrZR) {
		this.wayBillMgrZR = wayBillMgrZR;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	public void setProductStoreLocationMgrZJ(
			ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ) {
		this.productStoreLocationMgrZJ = productStoreLocationMgrZJ;
	}

}
