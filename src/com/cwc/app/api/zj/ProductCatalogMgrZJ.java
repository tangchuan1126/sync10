package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.product.LEVEL2ToLEVEL1ParentCanNotDefineException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.iface.zj.ProductCatalogMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProductCatalogMgrZJ implements ProductCatalogMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	
	private FloorCatalogMgr floorCatalogMgr;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	private ProductMgrIFaceZJ productMgrZJ;
	
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

	public void setFloorProductCatalogMgrZJ(FloorProductCatalogMgrZJ floorProductCatalogMgrZJ) {
		this.floorProductCatalogMgrZJ = floorProductCatalogMgrZJ;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	public DBRow[] getAllProductCatalogChild() throws Exception {
		try 
		{
			return (floorProductCatalogMgrZJ.getAllProductCatalogChild());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductCatalogChild",log);
		}
	}
	
	/**
	 * 移动商品分类
	 */
	public void moveCatalog(HttpServletRequest request) throws Exception {
		
		try {
			
			long catalog_id = StringUtil.getLong(request,"catalog_id");
			long move_to = StringUtil.getLong(request,"move_to");
			
			if( floorProductCatalogMgrZJ.checkCatalogParent(move_to, catalog_id) != null) {
				
				throw new LEVEL2ToLEVEL1ParentCanNotDefineException();
			}
			
			//判断要移动到的分类下是否有商品,如果有商品,则找到最底层的分类,如果有多个,则不移动
			DBRow[] product = floorCatalogMgr.getProductByCategory(move_to);
			
			if(product.length > 0){
				
				long categoryId = catalog_id;
				//查看是否有子分类
				DBRow[] category = floorCatalogMgr.getSubProductCatalogByParentId(catalog_id);
				
				if(category.length > 1){
					
					//分类太多
					throw new SuperAdminException();
					
				} else if (category.length > 0){
					
					categoryId = category[0].get("id", 0);
				}
				
				for(DBRow one : product){
					
					DBRow param = new DBRow();
					param.put("catalog_id", categoryId);
					
					floorCatalogMgr.updateProduct(one.get("pc_id",0),param);
					
					productMgrZJ.updateCustomerTitleAndCategoryLineRelation(one.get("pc_id",0), move_to);
				}
			}
			
			//
			DBRow productCatalog = floorCatalogMgr.getDetailProductCatalogById(catalog_id);
			DBRow moveToProductCatalog = floorCatalogMgr.getDetailProductCatalogById(move_to);
			
			DBRow row = new DBRow();
			row.add("parentid",move_to);
			
			long product_line = 0L;
			
			//如果父分类有产品线
			if(null != moveToProductCatalog && moveToProductCatalog.get("product_line_id",0l)>0){
				
				product_line = moveToProductCatalog.get("product_line_id",0l);
			}
			
			row.add("product_line_id",product_line);
			
			//1.修改此分类所属的父分类,并且将此分类的产品线修改为父分类的产品线
			floorCatalogMgr.modProductCatalog(catalog_id, row);
			
			//2.修改子分类的产品线
			if(product_line > 0) {
				
				DBRow[] childlist = floorProductCatalogMgrZJ.getAllChildCatalog(catalog_id);
				
				for (int i = 0; i < childlist.length; i++) {
					
					DBRow productLinePara = new DBRow();
					productLinePara.add("product_line_id",moveToProductCatalog.get("product_line_id",0l));
					floorCatalogMgr.modProductCatalog(childlist[i].get("pc_id",0l),productLinePara);
				}
			}
			
			//3.修改产品分类平铺表[pc_child_list]
			callProductCatalogChildList(0);
			
			//4.修改移动分类后影响的title关系
			this.updateCustomerTitleAndCategoryLine(productCatalog.get("product_line_id",0), productCatalog.get("parentid",0), catalog_id);
			
			
		} catch(LEVEL2ToLEVEL1ParentCanNotDefineException e) {
			throw e;
		} catch(SuperAdminException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e,"moveCatalog",log);
		}
	}
	
	/********************************************************************************************/
	
	private void updateCustomerTitleAndCategoryLine(long beforeLineId,long beforeParentId,long categoryId) throws Exception{
		
		if(beforeParentId != 0 ){
			
			proprietaryMgrZyj.customerTitleAndCategory(beforeParentId);
		}
		
		DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);

		if(parentCategory!=null){
			
			proprietaryMgrZyj.customerTitleAndCategory(parentCategory.get("id",0));
			
		}
		
		/**
		* 1.只有一级分类可以关联产品线
		* 2.一个分类只能属于一个产品线
		* 3.父级分类关联一个产品线,其子集分类都关联这个产品线.
		*/
		
		DBRow category = floorCatalogMgr.getProductCategory(categoryId);
		
		if(beforeLineId != category.get("product_line_id", 0)){
			
			//原产品线与title的关系
			proprietaryMgrZyj.customerTitleAndLine(beforeLineId);
			
			//现在的产品线与title的关系
			proprietaryMgrZyj.customerTitleAndLine(category.get("product_line_id", 0));
		}
	}
	
	/********************************************************************************************/
	
	/**
	 * 根据分类ID获得父类树形结构
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFatherTree(long catalog_id) throws Exception {
		
		try  {
			return (floorProductCatalogMgrZJ.getFatherTree(catalog_id));
		} catch (Exception e) {
			throw new SystemException(e,"getFatherTree",log);
		}
	}
	
	private void callProductCatalogChildList(long catalog_id) throws Exception {
		
		floorProductCatalogMgrZJ.callProductCatalogChildList(catalog_id);
	}

	/*****************************************************************************************************/
	
	private void beforeParentCategoryAndTitleRelation(long beforeParentId,long categoryId)throws Exception{
		
		//查询此分类关联的title
		DBRow[] title1 = floorCatalogMgr.getTitleForCategory(categoryId);
		
		//查询原父分类下的分类关联的title//查询原父分类下商品关联的title
		DBRow[] title4 = floorCatalogMgr.getTitleForCategoryProductAndSubCatagory(beforeParentId);
		
		//对比title
		for(DBRow one:title4){
			
			for(DBRow two:title1){
				
				if(one.get("title",-1L) == two.get("title",-2L)){
					
					two.put("exist",1);
					break;
				}
			}
		}
		
		//删除不存在的title
		for(DBRow one:title1){
			
			if(one.get("exist",0)==0){
				
				floorCatalogMgr.deleteTitleProductCatalog(beforeParentId,one.get("title",-1L));
			}
		}
		
		//判断是否原父分类依然有父分类
		DBRow parentCategory = floorCatalogMgr.getParentCategory(beforeParentId);
		
		if(parentCategory != null){
			
			this.beforeParentCategoryAndTitleRelation(parentCategory.get("id",-1L),parentCategory.get("id",-1L));
		}
	}
	
	private void nowParentCategoryAndTitleRelation(long categoryId) throws Exception{
		
		//查询现在的父级
		DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
		
		if(parentCategory!=null){
			
			DBRow[] title1 = floorCatalogMgr.getTitleForCategory(parentCategory.get("id",-1L));
			
			DBRow[] title2 = floorCatalogMgr.getTitleForCategory(categoryId);
			
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			
			//添加不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					DBRow row = new DBRow();
					row.put("tpc_title_id", one.get("title",-1L));
					row.put("tpc_product_catalog_id", parentCategory.get("id",-1L));
					
					floorCatalogMgr.insertTitleProductCatalog(row);
				}
			}
			
			this.nowParentCategoryAndTitleRelation(parentCategory.get("id",-1L));
		}
	}
	
	//原产品线与title的关系
	private void beforeLineAndTitleRelation(long beforeLineId) throws Exception{
		
		//查询产品线下产品分类与title的关系
		DBRow[] title1 = floorCatalogMgr.getTitleForLineCategoryRelation(beforeLineId);
		//查询产品线与title的关系
		DBRow[] title2 = floorCatalogMgr.getTitleForLineByLineId(beforeLineId);
		//对比title
		for(DBRow one:title1){
			
			for(DBRow two:title2){
				
				if(one.get("title",-1L) == two.get("title",-2L)){
					
					two.put("exist",1);
					break;
				}
			}
		}
		//删除不存在的title
		for(DBRow one:title2){
			
			if(one.get("exist",0)==0){
				
				floorCatalogMgr.deleteTitleProductLine(beforeLineId,one.get("title",-1L));
			}
		}
	}
	
	//现产品线与title的关系
	private void nowLineAndTitleRelation(long categoryId) throws Exception{
		
		//查询产品线
		DBRow category = floorCatalogMgr.getProductCategory(categoryId);
		//查询此产品线的title
		DBRow[] title1 = floorCatalogMgr.getTitleForLine(categoryId);
		//查询此分类的title
		DBRow[] title2 = floorCatalogMgr.getTitleForCategory(categoryId);
		//对比title
		for(DBRow one:title1){
			
			for(DBRow two:title2){
				
				if(one.get("title",-1L) == two.get("title",-2L)){
					
					two.put("exist",1);
					break;
				}
			}
		}
		//添加不存在的title
		for(DBRow one:title2){
			
			if(one.get("exist",0)==0){
				
				DBRow row = new DBRow();
				row.put("tpl_title_id", one.get("title",-1L));
				row.put("tpl_product_line_id", category.get("product_line_id",-1L));
				
				floorCatalogMgr.insertTitleProductLine(row);
			}
		}
	}

	/**
	* 分类移动:只要级别不要超过3级
	*
	* A:影响此分类父级与title的关系 
	* 	1.原来的父级与title的关系 
	* 	2.现在的父级与title的关系
	*
	* B:影响此分类的产品线
	* 	1.产品线不变
	*	2.产品线变为现在父级的产品线
	*
	*@param [改变前分类所属的产品线如果没有默认0,改变前父级分类ID如果没有默认0,改变的分类ID]
	*@author subin
	*/
	private void categoryToCategoryAffect(long beforeLineId,long beforeParentId,long categoryId) throws Exception{
		
		/**
		* 1.一级分类不可移动
		* 2.二级分类只能移动到一级分类上
		* 3.三级分类可以移动到一级分类和二级分类上
		*/
		
		if(beforeParentId != 0 ){
			//原来的父级与title的关系 
			this.beforeParentCategoryAndTitleRelation(beforeParentId,categoryId);
		}
		
		//查询现在父级与title的关系
		this.nowParentCategoryAndTitleRelation(categoryId);
		
		/**
		* 1.只有一级分类可以关联产品线
		* 2.一个分类只能属于一个产品线
		* 3.父级分类关联一个产品线,其子集分类都关联这个产品线.
		*/
		
		DBRow category = floorCatalogMgr.getProductCategory(categoryId);
		
		if(beforeLineId != category.get("product_line_id", 0)){
			
			//原产品线与title的关系
			this.beforeLineAndTitleRelation(beforeLineId);
			
			//现在的产品线与title的关系
			this.nowLineAndTitleRelation(categoryId);
		}
	}
}
