package com.cwc.app.api.zj;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorProductStorageMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStoreLogsDetailMgrZJ;
import com.cwc.app.iface.zj.ProductStoreLogsDetailMgrIFaceZJ;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreLogsDetailOperationTypeKey;
import com.cwc.app.key.ProductStoreLogsDetailUseTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductStoreUnionLogKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.Tree;

public class ProductStoreLogsDetailMgrZJ implements ProductStoreLogsDetailMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStoreLogsDetailMgrZJ floorProductStoreLogsDetailMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorProductStorageMgrZJ floorProductStorageMgrZJ;

	/**
	 * 单纯的生成入库新批次，之前无任何关联批次（用于交货单入库）
	 */
	public void inStoreProdcutLogsDetailNoFrom(long psl_id,ProductStoreLogBean inProductStoreLog) 
		throws Exception 
	{
		try 
		{
			long pc_id = inProductStoreLog.getPc_id();
			long ps_id = inProductStoreLog.getPs_id();
			double quantity = inProductStoreLog.getQuantity();
			
			DBRow productLogsDetail = new DBRow();
			
			productLogsDetail.add("psl_id",psl_id);
			productLogsDetail.add("quantity",quantity);
			productLogsDetail.add("used_quantity",0);
			productLogsDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
			productLogsDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.InStore);
			productLogsDetail.add("pc_id",pc_id);
			productLogsDetail.add("ps_id",ps_id);
//			productLogsDetail.add("purchase_unit_price",inProductStoreLog.getPurchase_unit_price());
//			productLogsDetail.add("shipping_fee",inProductStoreLog.getShipping_fee());
			productLogsDetail.add("create_time",DateUtil.NowStr());
			
			floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(productLogsDetail);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"inStoreProdcutLogsDetailNoFrom",log);
		}
	}
	
	/**
	 * 关联出库批次入库（转运单入库）
	 * @param psl_id
	 * @param inProductStoreLog
	 * @throws Exception
	 */
	public void inStoreProdcutLogsDetailHasOut(long psl_id,ProductStoreLogBean inProductStoreLog) 
		throws Exception
	{
		try 
		{
			long pc_id = inProductStoreLog.getPc_id();
			long ps_id = inProductStoreLog.getPs_id();
			int bill_type = inProductStoreLog.getBill_type();
			long bill_id = inProductStoreLog.getOid();
			int quantity_type = inProductStoreLog.getQuantity_type();
			float quantity = (float)inProductStoreLog.getQuantity();
			
			DBRow[] outStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreLogsDetailsOutStore(pc_id, ps_id, bill_type, bill_id, quantity_type);
			
			for (int i = 0; i < outStoreLogDetails.length; i++) 
			{
				double purchase_unit_price = outStoreLogDetails[i].get("purchase_unit_price",0d);
				quantity = quantity-outStoreLogDetails[i].get("used_quantity",0f);
				
				DBRow inStoreLogDetail = new DBRow();
				inStoreLogDetail.add("psl_id",psl_id);
				inStoreLogDetail.add("used_quantity",0);
				inStoreLogDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
				inStoreLogDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.InStore);
				inStoreLogDetail.add("pc_id",pc_id);
				inStoreLogDetail.add("ps_id",ps_id);
				inStoreLogDetail.add("purchase_unit_price",purchase_unit_price);
//				inStoreLogDetail.add("shipping_fee",inProductStoreLog.getShipping_fee());
				inStoreLogDetail.add("create_time",DateUtil.NowStr());
				inStoreLogDetail.add("from_psld_id",outStoreLogDetails[i].get("psld_id",0l));
				
				if (quantity>0)//到货数量与出库批次不符（如少到）
				{
					inStoreLogDetail.add("quantity",outStoreLogDetails[i].get("used_quantity",0f));
				}
				else if(quantity<=0)//实际到货数量比批次记录数量少时,记录实际到达数量
				{
					inStoreLogDetail.add("quantity",Math.abs(quantity));
				}
				
				floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(inStoreLogDetail);
			}
			
			//到达数量比出库批次多,记录批次，无法关联
			if(quantity>0)
			{
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				DBRow inStoreLogDetail = new DBRow();
				inStoreLogDetail.add("psl_id",psl_id);
				inStoreLogDetail.add("quantity",Math.abs(quantity));
				inStoreLogDetail.add("used_quantity",0);
				inStoreLogDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
				inStoreLogDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.InStore);
				inStoreLogDetail.add("pc_id",pc_id);
				inStoreLogDetail.add("ps_id",ps_id);
				inStoreLogDetail.add("purchase_unit_price",product.get("unit_price",0d));//无法关联，按照最新采购价录入
//				inStoreLogDetail.add("shipping_fee",inProductStoreLog.getShipping_fee());
				inStoreLogDetail.add("create_time",DateUtil.NowStr());
				
				floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(inStoreLogDetail);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"inStoreProdcutLogsDetailHasOut",log);
		}
		
	}

	/**
	 * 出库操作记录库存日志详细（出库批次）
	 */
	public synchronized void outStoreProductLogsDetail(long psl_id,ProductStoreLogBean outProductStoreLog) 
		throws Exception 
	{
		try 
		{
			long pc_id = outProductStoreLog.getPc_id();
			long ps_id = outProductStoreLog.getPs_id();
			int bill_type = outProductStoreLog.getBill_type();
			long bill_id = outProductStoreLog.getOid();
			int quantity_type = outProductStoreLog.getQuantity_type();
			float quantity = (float)outProductStoreLog.getQuantity()*-1;//日志bean里出库数量记录为负数
			
			DBRow[] getOutStoreDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreLogsDetailsCanOut(ps_id, pc_id);
			int i = 0;
			do 
			{
				DBRow storeDetail = getOutStoreDetails[i];
				float store_detail_quantity = storeDetail.get("quantity",0f); 
				float store_detail_used_quantity = storeDetail.get("used_quantity",0f);
				double purchase_unit_price = storeDetail.get("purchase_unit_price",0d);
				
				long psld_id = storeDetail.get("psld_id",0l);
				
				float store_can_use_quantity = store_detail_quantity-store_detail_used_quantity;
				
				float out_quantity = 0f;//批次
				
				/**
				 * 修改用于出库的批次开始
				 **/
				DBRow modOutStoreDetail = new DBRow();
				//需求量大于可使用量
				if(quantity>store_can_use_quantity)
				{
					float used_quantity = store_detail_used_quantity+store_can_use_quantity;
					
					out_quantity = store_can_use_quantity;
					
					quantity -= store_can_use_quantity;
					
					//使用完，标记已使用数量与使用完成
					
					modOutStoreDetail.add("used_quantity",used_quantity);
					modOutStoreDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.AllUsed);
				}
				else//需求数量不大于可用数量
				{
					float used_quantity = store_detail_used_quantity+quantity;
					
					out_quantity = quantity;
					
					quantity = 0;
					
					modOutStoreDetail.add("used_quantity",used_quantity);
					if(used_quantity<store_detail_quantity)
					{
						modOutStoreDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.PartUsed);
					}
					else
					{
						modOutStoreDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.AllUsed);
					}
				}
				
				floorProductStoreLogsDetailMgrZJ.modProductStoreLogsDetail(psld_id,modOutStoreDetail);
				/**
				 * 修改用于出库的批次结束
				 **/
				
				//记录出库日志详细
				DBRow productLogsDetail = new DBRow();
				productLogsDetail.add("psl_id",psl_id);
				productLogsDetail.add("quantity",out_quantity);
				productLogsDetail.add("used_quantity",out_quantity);
				productLogsDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.AllUsed);
				productLogsDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.OutStore);
				productLogsDetail.add("pc_id",pc_id);
				productLogsDetail.add("ps_id",ps_id);
				productLogsDetail.add("purchase_unit_price",purchase_unit_price);
//				productLogsDetail.add("shipping_fee",outProductStoreLog.getShipping_fee());
				productLogsDetail.add("create_time",DateUtil.NowStr());
				productLogsDetail.add("from_psld_id",psld_id);
				
				floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(productLogsDetail);
				i++;
			} 
			while (quantity!=0);
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			////system.out.println("outStoreProductLogsDetail ArrayIndexOutOfBoundsException pc_id:"+outProductStoreLog.getPc_id()+"-----ps_id:"+outProductStoreLog.getPs_id());
			throw new SystemException(e,"outStoreProductLogsDetail ArrayIndexOutOfBoundsException pc_id:"+outProductStoreLog.getPc_id()+"-----ps_id:"+outProductStoreLog.getPs_id(),log);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"outStoreProductLogsDetail",log);
		}
		
	}
	
	/**
	 * 套装商品拆散记录库存日志详细
	 * @param psl_id	散件商品入库日志ID
	 * @param inProductStoreLog	
	 * @param from_psl_id	套装商品出库日志ID
	 * @throws Exception
	 */
	public void splitProductUnionProductStoreLogsDetail(long psl_id,ProductStoreLogBean inProductStoreLog,long from_psl_id)
		throws Exception
	{
		long pc_id = inProductStoreLog.getPc_id();
		long ps_id = inProductStoreLog.getPs_id();
//		double quantity = inProductStoreLog.getQuantity();
		
		DBRow[] outProductStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreDetailLogsByPslid(from_psl_id);
		
		long union_pc_id = outProductStoreLogDetails[0].get("pc_id",0l);
		
		DBRow union = floorProductMgr.getDetailProductInSet(union_pc_id, pc_id);
		float union_quantity = union.get("quantity",0f);//套装组合关系数量
		
		for (int i = 0; i < outProductStoreLogDetails.length; i++) 
		{
			float union_product_quantity = outProductStoreLogDetails[i].get("used_quantity",0f);//批次记录的套装数量
			
			DBRow productLogsDetail = new DBRow();
			
			float quantity = union_product_quantity*union_quantity;
			
			productLogsDetail.add("psl_id",psl_id);
			productLogsDetail.add("quantity",quantity);
			productLogsDetail.add("used_quantity",0);
			productLogsDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
			productLogsDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.InStore);
			productLogsDetail.add("pc_id",pc_id);
			productLogsDetail.add("ps_id",ps_id);
//			productLogsDetail.add("purchase_unit_price",inProductStoreLog.getPurchase_unit_price());
//			productLogsDetail.add("shipping_fee",inProductStoreLog.getShipping_fee());
			productLogsDetail.add("create_time",DateUtil.NowStr());
			productLogsDetail.add("from_psld_id",outProductStoreLogDetails[i].get("psld_id",0l));
			
			long to_psld_id = floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(productLogsDetail);
			
			//记录关系日志
			DBRow productStoreUnionLog = new DBRow();
			productStoreUnionLog.add("from_psld_id", outProductStoreLogDetails[i].get("psld_id",0l));
			productStoreUnionLog.add("from_quantity",union_product_quantity);
			productStoreUnionLog.add("to_psld_id",to_psld_id);
			productStoreUnionLog.add("to_quantity",quantity);
			productStoreUnionLog.add("split_combination",ProductStoreUnionLogKey.Split);
			floorProductStoreLogsDetailMgrZJ.addProductStoreUnionLog(productStoreUnionLog);
		}
	}
	
	/**
	 * 回退批次
	 */
	public void rebackStoreProductLogsDetail(long psl_id,ProductStoreLogBean rebackStoreLog)
		throws Exception 
	{
		long pc_id = rebackStoreLog.getPc_id();
		long ps_id = rebackStoreLog.getPs_id();
		int bill_type = rebackStoreLog.getBill_type();
		long bill_id = rebackStoreLog.getOid();
		int quantity_type = rebackStoreLog.getQuantity_type();
		int operation = rebackStoreLog.getOperation();
		
		DBRow[] storeLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreLogsDetails(pc_id, ps_id, bill_type, bill_id, quantity_type);
		
		//出库批次回退,全部批次取消
		for (int i = 0; i < storeLogDetails.length; i++) 
		{
			//出库的批次回退来源，入库的批次只要标记取消
			if(storeLogDetails[i].get("operation_type",0)==ProductStoreLogsDetailOperationTypeKey.OutStore) 
			{
				long from_psld_id = storeLogDetails[i].get("from_psld_id", 0l);
				DBRow fromProductLogDetail = floorProductStoreLogsDetailMgrZJ
						.getProductStoreDetailLogByPsld(from_psld_id);
				float use_quantity = fromProductLogDetail.get("used_quantity",0f);
				use_quantity = use_quantity - storeLogDetails[i].get("used_quantity", 0f);
				//回退来源
				DBRow paraFrom = new DBRow();
				paraFrom.add("used_quantity", use_quantity);
				if (use_quantity == 0) 
				{
					paraFrom.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
				} 
				else 
				{
					paraFrom.add("use_status",ProductStoreLogsDetailUseTypeKey.PartUsed);
				}
				floorProductStoreLogsDetailMgrZJ.modProductStoreLogsDetail(fromProductLogDetail.get("psld_id", 0l),paraFrom);
			}
			//标记本项已取消
			DBRow para = new DBRow();
			para.add("operation_type",ProductStoreLogsDetailOperationTypeKey.RebackStore);
			floorProductStoreLogsDetailMgrZJ.modProductStoreLogsDetail(storeLogDetails[i].get("psld_id",0l),para);
		}
	}
	

	/**
	 * 库存入库记录详细总入口
	 */
	public void inStoreProdcutLogsDetail(long psl_id,ProductStoreLogBean inProductStoreLog,long from_psl_id)
		throws Exception 
	{
		try 
		{
			long pc_id = inProductStoreLog.getPc_id();
			long ps_id = inProductStoreLog.getPs_id();
			int bill_type = inProductStoreLog.getBill_type();
			long bill_id = inProductStoreLog.getOid();
			int quantity_type = inProductStoreLog.getQuantity_type();
			int operation = inProductStoreLog.getOperation();
			
			DBRow[] outStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreLogsDetailsOutStore(pc_id, ps_id, bill_type, bill_id, quantity_type);
			
			if(operation==ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL)//手工拆散记录
			{
				this.splitProductUnionProductStoreLogsDetail(psl_id, inProductStoreLog, from_psl_id);
			}
			//系统拼装，手工拼装记录，不完整套装组合
			else if(operation==ProductStoreOperationKey.IN_STORE_BACK_UP_COMBINATION ||operation==ProductStoreOperationKey.IN_STORE_SYSTEM_COMBINATION||operation==ProductStoreOperationKey.IN_STORE_COMBINATION_UNION_STANDARD)
			{
				this.inStoreProdcutLogsDetailNoFrom(psl_id, inProductStoreLog);
			}
			else
			{
				if(outStoreLogDetails==null||outStoreLogDetails.length==0)//之前无出库批次直接加批次
				{
					this.inStoreProdcutLogsDetailNoFrom(psl_id, inProductStoreLog);
				}
				else//有出库批次的入库
				{
					this.inStoreProdcutLogsDetailHasOut(psl_id, inProductStoreLog);
				}
			}
			
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"inStoreProdcutLogsDetail",log);
		}
	}
	
	/**
	 * 记录商品批次拼装关系
	 * @param from_psl_id
	 * @param to_psl_id
	 * @throws Exception
	 */
	public void combinationToProdcutStoreUnionLog(long[] fromPslIds,long to_psl_id)
		throws Exception
	{
		//拼装商品，入库商品的日志详细（记录为一个批次，应只有一条）
		DBRow[] combinationInStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreDetailLogsByPslid(to_psl_id);
		
		long to_psld_id = combinationInStoreLogDetails[0].get("psld_id",0l);
		float to_quantity = combinationInStoreLogDetails[0].get("quantity",0f);
		
		double sumCombinationPrice = 0;
		//获得出库的批次
		for (int i = 0; i < fromPslIds.length; i++) 
		{
			DBRow[] combinationOutStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreDetailLogsByPslid(fromPslIds[i]);
			
			//循环记录每个批次与入库批次的组合关系
			for (int j = 0; j < combinationOutStoreLogDetails.length; j++) 
			{
				long from_psld_id = combinationOutStoreLogDetails[j].get("psld_id",0l);
				float from_quantity = combinationOutStoreLogDetails[j].get("quantity",0f);
				double from_purchase_price = combinationOutStoreLogDetails[j].get("purchase_unit_price",0d);
				sumCombinationPrice += from_purchase_price*from_quantity;
				
				DBRow productStoreUnionLog = new DBRow();
				productStoreUnionLog.add("from_psld_id",from_psld_id);
				productStoreUnionLog.add("from_quantity",from_quantity);
				productStoreUnionLog.add("to_psld_id",to_psld_id);
				productStoreUnionLog.add("to_quantity",to_quantity);
				productStoreUnionLog.add("split_combination",ProductStoreUnionLogKey.Combination);
				floorProductStoreLogsDetailMgrZJ.addProductStoreUnionLog(productStoreUnionLog);
			}
		}
		
		DBRow para = new DBRow();
		para.add("purchase_unit_price",sumCombinationPrice/to_quantity);
		floorProductStoreLogsDetailMgrZJ.modProductStoreLogsDetail(to_psld_id, para);
		
	}

	/**
	 * 根据日志获得批次
	 * @param psl_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreDetailLogsByPslid(long psl_id)
		throws Exception
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.getProductStoreDetailLogsByPslid(psl_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreDetailLogsByPslid",log);
		}
	}
	
	public void initProductStoreLogDetail(long ps_id)
		throws Exception
	{
		try 
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			ArrayList al = tree.getAllSonNode(ps_id);
			al.add(String.valueOf(ps_id));
			
			DBRow[] productStorages = floorProductStorageMgrZJ.getProductStorageInCids(0,0,(String[])al.toArray(new String[0]),ProductStatusKey.IN_STORE,-1,null);
			
			for (int i = 0; i < productStorages.length; i++) 
			{
				double shippingFeeKg = 1;
				if(productStorages[i].get("cid",0l)==100043)
				{
					shippingFeeKg = 4;
				}
				DBRow product = floorProductMgr.getDetailProductByPcid(productStorages[i].get("pc_id",0l));
				float quantity = productStorages[i].get("store_count",0f);
				long pc_id = productStorages[i].get("pc_id",0l);
				
				DBRow[] productStoreLogs = floorProductStoreLogsDetailMgrZJ.getInProductStoreLogByPcid(pc_id,ps_id);
				
				int q = 0;
				do 
				{
					float logDetailQuantity = 0f;
					String time = DateUtil.NowStr();
					long psl_id = 0;
					if(q<productStoreLogs.length)
					{
						if(productStoreLogs[q].get("quantity",0f)>=quantity)
						{
							logDetailQuantity = quantity;
							quantity = 0;
						}
						else
						{
							logDetailQuantity = productStoreLogs[q].get("quantity",0f);
							quantity = quantity-logDetailQuantity;
						}
						
						time = productStoreLogs[q].getString("post_date");
						psl_id = productStoreLogs[q].get("psl_id",0l);
					}
					else
					{
						logDetailQuantity = quantity;
						quantity = 0;
					}

					DBRow productLogsDetail = new DBRow();
					productLogsDetail.add("psl_id",psl_id);
					productLogsDetail.add("quantity",logDetailQuantity);
					productLogsDetail.add("used_quantity",0);
					productLogsDetail.add("use_status",ProductStoreLogsDetailUseTypeKey.NotUsed);
					productLogsDetail.add("operation_type",ProductStoreLogsDetailOperationTypeKey.InStore);
					productLogsDetail.add("pc_id",pc_id);
					productLogsDetail.add("ps_id",productStorages[i].get("cid",0l));
					productLogsDetail.add("purchase_unit_price",product.get("unit_price",0d));
					productLogsDetail.add("shipping_fee",product.get("weight",0f)*shippingFeeKg);
					productLogsDetail.add("create_time",time);
					productLogsDetail.add("from_psld_id",0l);
					
					floorProductStoreLogsDetailMgrZJ.addProductStoreLogsDetail(productLogsDetail);
					q++;
					////system.out.println(q+":"+"pc_id:"+pc_id+"quantity:"+quantity);
					
				} 
				while (quantity>0);
				
				
				
				
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"initProductStoreLogDetail",log);
		}
	}
	
	/**
	 * 根据批次ID获得批次
	 */
	public DBRow getDetailProductStoreDetailLogsByPsldid(long psld_id)
		throws Exception 
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.getProductStoreDetailLogByPsld(psld_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailProductStoreDetailLogsByPsldid",log);
		}
	}

	public DBRow[] getProductStoreLogDetailsByFromPsldId(long from_psld_id)
		throws Exception 
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.getProductStoreLogDetailsByFromPsld(from_psld_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreLogDetailsByFromPsldId",log);
		}
	}
	
	/**
	 * 根据目标批次ID获得来源批次
	 * @param to_psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorelogDetailsByToPsld(long to_psld_id)
		throws Exception
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.getProductStorelogDetailsFromUnionLogByToPsld(to_psld_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStorelogDetailsByToPsld",log);
		}
	}
	
	public DBRow[] getProductStorelogDetailsFromUnionLogByFromPsld(long from_psld_id)
		throws Exception
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.getProductStorelogDetailsFromUnionLogByFromPsld(from_psld_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStorelogDetailsFromUnionLogByFromPsld",log);
		}
	}
	
	/**
	 * 积压货物
	 * @param product_line_id
	 * @param catalog_id
	 * @param ps_id
	 * @param p_name
	 * @param backlog_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] backlogOfGoods(long product_line_id,long catalog_id,long ps_id,String p_name,int backlog_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			long pc_id = 0l;
			if(product!=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return floorProductStoreLogsDetailMgrZJ.backlogOfGoods(product_line_id, catalog_id, ps_id, pc_id, backlog_day, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"backlogOfGoods",log);
		}
	}
	
	/**
	 * 获得压货批次
	 * @param pc_id
	 * @param ps_id
	 * @param backlog_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] backlogGoodsStoreLogDetals(long pc_id,long ps_id,int backlog_day)
		throws Exception
	{
		try 
		{
			return floorProductStoreLogsDetailMgrZJ.backlogGoodsStoreLogDetals(pc_id, ps_id, backlog_day);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"backlogGoodsStoreLogDetals",log);
		}
	}
	
	public DBRow[] salesRate(String st,String en,long product_line_id,long catalog_id,String p_name,long ps_id,int sales_rate,int greater_less,PageCtrl pc)
		throws Exception
	{
		try 
		{
			long pc_id = 0;
			
			try 
			{
				pc_id = Long.parseLong(p_name);
			}
			catch(NumberFormatException e) 
			{
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				
				if(product !=null)
				{
					pc_id = product.get("pc_id",0l);
				}
			}
			
			return floorProductStoreLogsDetailMgrZJ.salesRate(st, en, product_line_id, catalog_id, pc_id, ps_id, sales_rate, greater_less, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"salesRate",log);
		}
	}
	
	public void setFloorProductStoreLogsDetailMgrZJ(
			FloorProductStoreLogsDetailMgrZJ floorProductStoreLogsDetailMgrZJ) {
		this.floorProductStoreLogsDetailMgrZJ = floorProductStoreLogsDetailMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorProductStorageMgrZJ(
			FloorProductStorageMgrZJ floorProductStorageMgrZJ) {
		this.floorProductStorageMgrZJ = floorProductStorageMgrZJ;
	}

}
