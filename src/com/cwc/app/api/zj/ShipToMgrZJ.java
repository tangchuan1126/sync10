package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.lucene.ys.ShipToIndexMgr;
import com.cwc.app.lucene.ys.ShipToStorageIndexMgr;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ShipToMgrZJ implements ShipToMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorShipToMgrZJ floorShipToMgrZJ;
	
	public DBRow[] getAllShipTo() throws Exception {
		try {
			return floorShipToMgrZJ.getAllShipTo();
		} catch (Exception e) {
			throw new SystemException(e, "getAllShipTo", log);
		}
	}
	
	public DBRow[] getShipTosWithCLP(long clp_type_id) throws Exception {
		return floorShipToMgrZJ.getShipTosWithCLP(clp_type_id);
	}
	
	public void setFloorShipToMgrZJ(FloorShipToMgrZJ floorShipToMgrZJ) {
		this.floorShipToMgrZJ = floorShipToMgrZJ;
	}
	
	@Override
	public DBRow[] GetAllShipToCustomer(long typeId, long shipToId) throws Exception {
		return this.floorShipToMgrZJ.getAllShipToCustomer(typeId, shipToId);
		
	}
	
	@Override
	public DBRow[] GetAllShipToCustomer0(long typeId, long shipToId) throws Exception {
		return this.floorShipToMgrZJ.getAllShipToCustomer(typeId, shipToId);
		
	}
	
	public DBRow[] getAllShipToOrderByName(PageCtrl pc) throws Exception {
		try {
			return floorShipToMgrZJ.getAllShipToOrderByName(pc);
		} catch (Exception e) {
			throw new SystemException(e, "getAllShipTo", log);
		}
	}
	
	public DBRow addShipTo(HttpServletRequest request) throws Exception{
		String shipToName = StringUtil.getString(request, "ship_to_name");
		if(shipToName==null || shipToName.equals("")){return null;}
		if(floorShipToMgrZJ.getDetailShipTo(shipToName)==null){
			DBRow shipTo = new DBRow();
			shipTo.add("ship_to_name", shipToName.toUpperCase());
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			int adid = (int)adminLoggerBean.getAdid();
			String nowStr = DateUtil.NowStr();
			shipTo.put("creator", adid);
			shipTo.put("create_time", nowStr);
			long AddedId = floorShipToMgrZJ.addShipTo(shipTo);
			shipTo = floorShipToMgrZJ.getDetailShipToById(AddedId);
			ShipToIndexMgr.getInstance().addIndex(AddedId);
			return shipTo;
		}
		return null;
	}
	
	public boolean deleteShipTo(HttpServletRequest request) throws Exception{
		long storage_type_id = StringUtil.getLong(request, "ship_to_id");
		//TODO 加storageType
		int storage_type = StringUtil.getInt(request, "storage_type");
		DBRow[] storageCatalog = floorShipToMgrZJ.getStorageCatalogsOfShipTo(storage_type_id, storage_type);
		if(storageCatalog.length>0){
			return false;
		}else{
			boolean flag = floorShipToMgrZJ.deleteShipTo(storage_type_id);
			if(flag){
				ShipToIndexMgr.getInstance().deleteIndex(storage_type_id);
			}
			return flag;
		}
		
			
	}
	public DBRow updateShipTo(HttpServletRequest request) throws Exception{
		String shipToName = StringUtil.getString(request, "ship_to_name");
		long shipToId = StringUtil.getLong(request, "ship_to_id");
		
		DBRow ShipToName = floorShipToMgrZJ.getDetailShipTo(shipToName);
		if(floorShipToMgrZJ.getDetailShipToById(shipToId)!=null && ShipToName == null){
			DBRow shipTo = new DBRow();
			shipTo.add("ship_to_name", shipToName.toUpperCase());
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			int adid = (int)adminLoggerBean.getAdid();
			String nowStr = DateUtil.NowStr();
			shipTo.put("creator", adid);
			shipTo.put("create_time", nowStr);
			int result = floorShipToMgrZJ.updateShipTo(shipTo,shipToId);
			if(result>0){
				shipTo = floorShipToMgrZJ.getDetailShipToById(shipToId);
				ShipToIndexMgr.getInstance().updateIndex(shipToId);
				return shipTo;
			}
		}
		return null;
	}
	
	public DBRow getShipToById(long shipToId) throws Exception{
		
		return floorShipToMgrZJ.getDetailShipToById(shipToId);
	}
	
	public DBRow[] getStorageCatalogOfShipToByTitle(HttpServletRequest request) throws Exception{
		String title = StringUtil.getString(request, "title");
		long storageTypeId = StringUtil.getLong(request, "storage_type_id");
		//TODO 加storageType
		int storage_type = StorageTypeKey.RETAILER;
		return floorShipToMgrZJ.getStorageCatalogOfShipToByTitle(title, storageTypeId, storage_type);
		
	}
	
	public DBRow addStorageCatalog(HttpServletRequest request) throws Exception{
		String name = StringUtil.getString(request, "name").toUpperCase();
		String house_address = StringUtil.getString(request, "house_address");
		String street_address = StringUtil.getString(request, "street_address");
		String city = StringUtil.getString(request, "city");
		String state = StringUtil.getString(request, "state");
		String contact = StringUtil.getString(request, "contact");
		String phone = StringUtil.getString(request, "phone");
		String zipcode = StringUtil.getString(request, "zipcode");
		String text_state = StringUtil.getString(request, "is_text_state");
		String email = StringUtil.getString(request, "email");
		String fax = StringUtil.getString(request, "fax");
		int is_bill_dc = StringUtil.getInt(request, "is_bill_dc");
		int storage_type = StorageTypeKey.RETAILER;

		long storage_type_id = StringUtil.getLong(request, "storage_type_id");//ship_to_id
		long state_dd = StringUtil.getLong(request, "state_dd");
		long country = StringUtil.getLong(request, "country");
		AdminMgr am = new AdminMgr();
		AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
		int adid = (int)adminLoggerBean.getAdid();
		String nowStr = DateUtil.NowStr();
		
		DBRow item = new DBRow();
		item.put("title", name);
		item.put("send_zip_code", zipcode);
		item.put("deliver_zip_code", zipcode);
		item.put("contact", contact);
		item.put("send_contact", contact);
		item.put("deliver_contact", contact);
		item.put("phone", phone);
		item.put("send_phone", phone);
		item.put("deliver_phone", phone);
		item.put("native", country);
		item.put("send_nation", country);
		item.put("deliver_nation", country);
		if(text_state.equalsIgnoreCase("true")){
			item.put("send_pro_input", state);
			item.put("deliver_pro_input", state);
		}else{
			item.put("pro_id", state_dd);
			item.put("send_pro_id", state_dd);
			item.put("deliver_pro_id", state_dd);
			DBRow prov = floorShipToMgrZJ.getDetailProvinceByProId(state_dd);
			String provName = prov.getString("pro_name");
			item.put("send_pro_input", provName);
			item.put("deliver_pro_input", provName);
		}
		item.put("city", city);
		item.put("send_city", city);
		item.put("deliver_city", city);
		
		item.put("send_house_number", house_address);
		item.put("deliver_house_number", house_address);
		item.put("send_street", street_address);
		item.put("deliver_street", street_address);
		item.put("send_email", email);
		item.put("send_fax", fax);
		item.put("delivery_email", email);
		item.put("delivery_fax", fax);
		
		item.put("storage_type_id", storage_type_id);
		item.put("storage_type", storage_type);
		item.put("is_bill_dc", is_bill_dc);
		
		item.put("creator", adid);
		item.put("create_time", nowStr);
		
		long id = floorShipToMgrZJ.addStorageCatalog(item);
		item = floorShipToMgrZJ.getStorageCatalogInfoById(id);
		DBRow shipTo = floorShipToMgrZJ.getDetailShipToById(storage_type_id);
		ShipToStorageIndexMgr.getInstance().addIndex(id, item.getString("title"), item.getString("city"), item.getString("send_street"), item.getString("send_house_number"), item.getString("send_zip_code"), item.getString("contact"), item.getString("c_country"), item.getString("pro_name"), item.getString("send_pro_input"), item.getString("storage_type_id"), shipTo.getString("ship_to_name"), item.getString("storage_type"));
		
		return item;
		
	}
	
	public DBRow[] getStorageCatalogsOfShipTo(HttpServletRequest request) throws Exception{
		
		long storage_type_id = StringUtil.getLong(request, "storage_type_id");//ship_to_id
		int storage_type = StorageTypeKey.RETAILER;
		String shipToName = floorShipToMgrZJ.getDetailShipToById(storage_type_id).getString("ship_to_name");
		DBRow[] storageCatalogsOfShipTo = floorShipToMgrZJ.getAddressBook(storage_type_id, storage_type, null);
				//.getStorageCatalogsOfShipTo(storage_type_id, storage_type);
		for(int i=0; i< storageCatalogsOfShipTo.length; i++){
			storageCatalogsOfShipTo[i].add("ship_to_name", shipToName);
			storageCatalogsOfShipTo[i].add("ship_to_id", storage_type_id);
		}
		
		return storageCatalogsOfShipTo;
		
	}
	
	public DBRow[] getStorageCatalogsOfShipTo(long storage_type_id, int storage_type) throws Exception{
			
		return floorShipToMgrZJ.getStorageCatalogsOfShipTo(storage_type_id, storage_type);
			
	}
	
	public boolean deleteStorageCatalog(HttpServletRequest request) throws Exception{
		long storageId = StringUtil.getLong(request, "storage_id");
		boolean flag = floorShipToMgrZJ.deleteStorageCatalog(storageId);
		ShipToStorageIndexMgr.getInstance().deleteIndex(storageId);
		return flag;
			
	}

	@Override
	public DBRow updateStorageCatalog(HttpServletRequest request)
			throws Exception {
		
		String name = StringUtil.getString(request, "name").toUpperCase();
		String house_address = StringUtil.getString(request, "house_address");
		String street_address = StringUtil.getString(request, "street_address");
		String city = StringUtil.getString(request, "city");
		String state = StringUtil.getString(request, "state");
		String contact = StringUtil.getString(request, "contact");
		String phone = StringUtil.getString(request, "phone");
		String zipcode = StringUtil.getString(request, "zipcode");
		String text_state = StringUtil.getString(request, "is_text_state");

		long storageId = StringUtil.getLong(request, "storage_id");
		long state_dd = StringUtil.getLong(request, "state_dd");
		long country = StringUtil.getLong(request, "country");
		String email = StringUtil.getString(request, "email");
		String fax = StringUtil.getString(request, "fax");
		int is_bill_dc = StringUtil.getInt(request, "is_bill_dc");
		AdminMgr am = new AdminMgr();
		AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
		int adid = (int)adminLoggerBean.getAdid();
		String nowStr = DateUtil.NowStr();
		
		if(floorShipToMgrZJ.getDetailStorageCatalogById(storageId)!=null){
			DBRow item = new DBRow();
			item.put("title", name);
			item.put("send_zip_code", zipcode);
			item.put("deliver_zip_code", zipcode);
			item.put("contact", contact);
			item.put("send_contact", contact);
			item.put("deliver_contact", contact);
			item.put("phone", phone);
			item.put("send_phone", phone);
			item.put("deliver_phone", phone);
			item.put("native", country);
			item.put("send_nation", country);
			item.put("deliver_nation", country);
			if(text_state.equalsIgnoreCase("true")){
				item.put("send_pro_input", state);
				item.put("deliver_pro_input", state);
				
				item.put("pro_id", null);
				item.put("send_pro_id", null);
				item.put("deliver_pro_id", null);
			}else{
				item.put("pro_id", state_dd);
				item.put("send_pro_id", state_dd);
				item.put("deliver_pro_id", state_dd);
				DBRow prov = floorShipToMgrZJ.getDetailProvinceByProId(state_dd);
				String provName = prov.getString("pro_name");
				item.put("send_pro_input", provName);
				item.put("deliver_pro_input", provName);

			}
			item.put("city", city);
			item.put("send_city", city);
			item.put("deliver_city", city);
			
			item.put("send_house_number", house_address);
			item.put("deliver_house_number", house_address);
			item.put("send_street", street_address);
			item.put("deliver_street", street_address);
			item.put("send_email", email);
			item.put("send_fax", fax);
			item.put("delivery_email", email);
			item.put("delivery_fax", fax);
			item.put("creator", adid);
			item.put("create_time", nowStr);
			item.put("is_bill_dc", is_bill_dc);
			
			int result = floorShipToMgrZJ.updateStorageCatalog(item,storageId);
			if(result>0){
				item = floorShipToMgrZJ.getStorageCatalogInfoById(storageId);
				DBRow shipTo = floorShipToMgrZJ.getDetailShipToById(Long.parseLong(item.getString("storage_type_id")));
				long storage_type_id = Long.parseLong(item.getString("storage_type_id"));
				String ship_to_name = shipTo.getString("ship_to_name");
				ShipToStorageIndexMgr.getInstance().updateIndex(storageId, item.getString("title"), item.getString("city"), item.getString("send_street"), item.getString("send_house_number"), item.getString("send_zip_code"), item.getString("contact"), item.getString("c_country"), item.getString("pro_name"), item.getString("send_pro_input"), item.getString("storage_type_id"), shipTo.getString("ship_to_name"),item.getString("storage_type"));
				item = floorShipToMgrZJ.getDetailStorageCatalogById(storageId);
				item.put("ship_to_id", storage_type_id);
				item.put("ship_to_name", ship_to_name);
				return item;
			}
		}
		return null;
		
	}
	
	
	public DBRow updateActivePause(HttpServletRequest request,int active)
			throws Exception {
		
		long storageId = StringUtil.getLong(request, "storage_id");
		
		AdminMgr am = new AdminMgr();
		AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
		int adid = (int)adminLoggerBean.getAdid();
		String nowStr = DateUtil.NowStr();
		
		if(floorShipToMgrZJ.getDetailStorageCatalogById(storageId)!=null){
			DBRow item = new DBRow();
			item.put("active", active);
			item.put("creator", adid);
			item.put("create_time", nowStr);
			
			int result = floorShipToMgrZJ.updateStorageCatalog(item,storageId);
			if(result>0){
				item = floorShipToMgrZJ.getStorageCatalogInfoById(storageId);
				DBRow shipTo = floorShipToMgrZJ.getDetailShipToById(Long.parseLong(item.getString("storage_type_id")));
				long storage_type_id = Long.parseLong(item.getString("storage_type_id"));
				String ship_to_name = shipTo.getString("ship_to_name");
				ShipToStorageIndexMgr.getInstance().updateIndex(storageId, item.getString("title"), item.getString("city"), item.getString("send_street"), item.getString("send_house_number"), item.getString("send_zip_code"), item.getString("contact"), item.getString("c_country"), item.getString("pro_name"), item.getString("send_pro_input"), item.getString("storage_type_id"), shipTo.getString("ship_to_name"),item.getString("storage_type"));
				item = floorShipToMgrZJ.getDetailStorageCatalogById(storageId);
				item.put("ship_to_id", storage_type_id);
				item.put("ship_to_name", ship_to_name);
				return item;
			}
			
		}
		return null;
		
	}
	
	
	
	public DBRow getStorageCatalogInfoById(long storageId) throws Exception{
		
		return floorShipToMgrZJ.getStorageCatalogInfoById(storageId);
		
	}
	
	public DBRow[] getShipToListByIds(String ids, PageCtrl pc) throws Exception {
		
		return floorShipToMgrZJ.getShipToListByIds(ids, pc);
		
	}
	
	public DBRow[] getShipToStorageListByIds(String ids, PageCtrl pc) throws Exception {
		
		return floorShipToMgrZJ.getShipToStorageListByIds(ids, pc);
		
	}
	
	/**
	 * ship to advanced search
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public DBRow[] ShipToAdvanceSearch(DBRow filter,PageCtrl pc) throws Exception{
		return floorShipToMgrZJ.ShipToAdvanceSearch(filter,pc);
	}
	
	/**
	 * ship to 导出excel
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportProduct(HttpServletRequest request) throws Exception 
		{
			try 
			{
				String search_type = StringUtil.getString(request, "search_type");
				DBRow[] addressBooks = new DBRow[0];
				
				// Ship to search
				if("easySearch".equals(search_type)) {
					
					String ship_to_name = StringUtil.getString(request, "ship_to_name");
					
					PageCtrl pc = new PageCtrl();
					
					pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
					pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));
					
					if(!ship_to_name.equals("")&&!ship_to_name.equals("\"")) {
						ship_to_name = ship_to_name.replaceAll("\"",""); 
						ship_to_name = ship_to_name.replaceAll("\\*","");
						ship_to_name +="*";
					}
					
					String titleId = "0";
					
					DBRow[] storages = ShipToIndexMgr.getInstance().mergeSearch("ship_to_name", ship_to_name, pc);
					
					for(int i= 0 ;i< storages.length ;i++){
						
						titleId += ","+storages[i].get("ship_to_id", 0);
					}
					
					addressBooks = floorShipToMgrZJ.getShipToDetailedById(titleId);
					
				// address book search	
				} else if("easyStorageSearch".equals(search_type)) {
					
					String address_name = StringUtil.getString(request, "address_name");
					long shipToId = 0;
					PageCtrl pc = new PageCtrl();
					
					pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
					pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));

					PageCtrl internalPc = new PageCtrl();
					internalPc.setPageNo(1);
					internalPc.setPageSize(10000);
					
					
					if(!address_name.equals("")&&!address_name.equals("\""))
					{
						address_name = address_name.replaceAll("\"",""); 
						address_name = address_name.replaceAll("\\*","");
						address_name +="*";
						
					}
					DBRow storages[] = ShipToStorageIndexMgr.getInstance().mergeSearch("title", address_name, internalPc);
					
					if (storages.length > 0){
						
						List<String> ids = new ArrayList<String>();
						StringBuilder sb = new StringBuilder();
						
						for(DBRow row : storages){
							
							if(!ids.contains(row.getString("storage_type_id"))){
								
								ids.add(row.getString("storage_type_id"));
								sb.append(row.getString("storage_type_id"));
								sb.append(",");
							}
						}
							
						String idsList = 	storages.length>0 ? sb.substring(0, sb.toString().length()-1) : sb.toString();
							
							DBRow[] shipToIds = getShipToListByIds(idsList,pc);
							
							sb = new StringBuilder();
							boolean removeEnding=false;
							if(shipToIds != null){
								for(DBRow ship : shipToIds){
									shipToId = ship.get("ship_to_id", 0l);
									for(DBRow row : storages){
										if(Long.parseLong(row.getString("storage_type_id"))==shipToId){
											sb.append(row.getString("id"));
											sb.append(",");
											removeEnding=true;
										}
									}
								}
							}
							
							String idsList2 = 	removeEnding ? sb.substring(0, sb.toString().length()-1) : sb.toString();
							
							if(!idsList2.equals("")){
								
								addressBooks = getShipToStorageListByIds(idsList2,internalPc);
							}
						}
				// advanced search	
				} else if("advancedSearch".equals(search_type)) {
					
					String country = StringUtil.getString(request, "country_value");
					String state = StringUtil.getString(request, "state_value");
					String shipTo = StringUtil.getString(request, "ship_to");
					String storage = StringUtil.getString(request, "storage");
					String storageType = StringUtil.getString(request, "storage_type")=="" ? String.valueOf(StorageTypeKey.RETAILER) :StringUtil.getString(request, "storage_type");
					long shipToId = 0;
					
					PageCtrl pc = new PageCtrl();
					
					pc.setPageNo(Integer.parseInt((StringUtil.getString(request, "pageNo")=="")? "1" :StringUtil.getString(request, "pageNo")));
					pc.setPageSize(Integer.parseInt((StringUtil.getString(request, "pageSize")=="")? "10" :StringUtil.getString(request, "pageSize")));
					
					DBRow filter = new DBRow();
					if(!country.equals("")&&!country.equals("\""))
					{
						country = country.replaceAll("\"",""); 
						country = country.replaceAll("\\*","");
						country +="*";
						filter.add("country", country);
						
					}
					if(!state.equals("")&&!state.equals("\""))
					{
						state = state.replaceAll("\"",""); 
						state = state.replaceAll("\\*","");
						state +="*";
						filter.add("state", state);
						
					}
					if(!shipTo.equals("")&&!shipTo.equals("\""))
					{
						shipTo = shipTo.replaceAll("\"",""); 
						shipTo = shipTo.replaceAll("\\*","");
						shipTo +="*";
						filter.add("ship_to_name", shipTo);
						
					}
					if(!storage.equals("")&&!storage.equals("\""))
					{
						storage = storage.replaceAll("\"",""); 
						storage = storage.replaceAll("\\*","");
						storage +="*";
						filter.add("title", storage);
					}
					
					DBRow[] shipTos  = ShipToStorageIndexMgr.getInstance().getSearchResults(filter, shipToId, pc,storageType);
					ArrayList<DBRow> result = new ArrayList<DBRow>();
					for(int i=0;i<shipTos.length;i++){
						long id = Long.parseLong(shipTos[i].getString("storage_type_id"));
						DBRow shipToRow = getShipToById(id);
						if(shipToRow!=null)result.add(shipToRow);
					}
					
					DBRow[] shipToIds = result.toArray(new DBRow[0]);
					
					if(shipToIds!=null){
						for(DBRow ship : shipToIds){
							shipToId = ship.get("ship_to_id", 0l);
							filter.add("storage_type_id", shipToId);
							addressBooks  = ShipToStorageIndexMgr.getInstance().getSearchResults(filter, shipToId, pc,storageType);
						}
					}
					
				//无搜索条件下	
				} else {
					
					addressBooks = floorShipToMgrZJ.getAllShipToDetail();
				}
				
				
				long[] products = new long[addressBooks.length];
				
				if (addressBooks.length>0) 
				{
					XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/ship_to/ShiptoAddress.xlsm"));//根据模板创建工作文件
					XSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
					
					XSSFRow row = sheet.getRow(0);
					
					XSSFCellStyle stylelock = wb.createCellStyle();
					stylelock.setLocked(true); //创建一个锁定样式
					
					for (int i = 0; i < addressBooks.length; i++) 
					{
						row = sheet.createRow((int) i + 1);

						sheet.setDefaultColumnWidth(26);
						DBRow addressBook = addressBooks[i];
						
						products[i] = addressBook.get("ship_to_id",0l);
						
						
						row.createCell(1).setCellValue(addressBook.getString("ship_to_name"));//ship to
						row.createCell(2).setCellValue(addressBook.getString("title"));//name
						row.createCell(3).setCellValue(addressBook.getString("send_house_number"));//address1
						row.createCell(4).setCellValue(addressBook.getString("send_street"));//address2
						row.createCell(5).setCellValue(addressBook.getString("city"));//city
						
						if("".equals(addressBook.getString("pro_id")) || "-1".equals(addressBook.getString("pro_id"))){
							row.createCell(6).setCellValue(addressBook.getString("send_pro_input"));//Province
						}else{
							row.createCell(6).setCellValue(addressBook.getString("pro_name"));//Province
						
						}
						row.createCell(7).setCellValue(addressBook.getString("send_zip_code"));//zip code
						row.createCell(8).setCellValue(addressBook.getString("c_country"));//nation
					}
					
					String path = "upl_excel_tmp/ship_to_address"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xlsm";
					FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
					wb.write(fout);
					fout.close();
					return (path);
				}
				else
				{
					return "0";
				}
	        } 
			catch (Exception e) 
			{
				throw new SystemException(e,"exportProduct",log);
		    }
		}

	
	
	/**
	 * 查询地址本
	 * @param storage_type_id
	 * @param storage_type
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年7月1日 下午6:51:19
	 */
	public DBRow[] getAddressBook(long storage_type_id, int storage_type, PageCtrl pc) throws Exception
	{
		try
		{
			return floorShipToMgrZJ.getAddressBook(storage_type_id, storage_type, pc);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAddressBook",log);
		}
	}
}
