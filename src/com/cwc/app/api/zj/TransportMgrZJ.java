package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zyj.TransportMgrZyj;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuB2BOrderBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.b2b.B2BOrderDetailWaitCountNotEnoughException;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.productStorage.CanNotSerialNumberReserveException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.transport.CanNotFillSerialNumberException;
import com.cwc.app.exception.transport.HadSerialNumberException;
import com.cwc.app.exception.transport.NoExistTransportDetailException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportLackException;
import com.cwc.app.exception.transport.TransportOrderDetailRepeatException;
import com.cwc.app.exception.transport.TransportSerialNumberRepeatException;
import com.cwc.app.exception.transport.TransportStockInHandleErrorException;
import com.cwc.app.exception.transport.TransportStockOutHandleErrorException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.ll.FreightMgrIFaceLL;
import com.cwc.app.iface.zj.ConfigChangeMgrIFaceZJ;
import com.cwc.app.iface.zj.ContainerProductMgrIFaceZJ;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.key.B2BOrderConfigChangeKey;
import com.cwc.app.key.B2BOrderKey;
import com.cwc.app.key.B2BOrderStatusKey;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.FirstOutTypeKey;
import com.cwc.app.key.GlobalKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.SimulationTransportKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportPutKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.lucene.zr.TransportIndexMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class TransportMgrZJ implements TransportMgrIFaceZJ {
	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorProductMgr floorProductMgr;
	private ProductMgrIFace productMgr;
	private TransportApproveMgrIFaceZJ transportApproveMgrZJ;
	private FloorTransportMgrLL floorTransportMgrLL;
	private FloorSupplierMgrTJH floorSupplierMgrTJH;
	private FloorPurchaseMgr floorPurchaseMgr;
	private SystemConfigIFace systemConfig;
	private TransportMgrZyj transportMgrZyj;
	private FreightMgrIFaceLL freightMgrLL;
	private FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorAdminMgr floorAdminMgr;
	private FloorDeliveryMgrZJ floorDeliveryMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ;
	private DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj;
	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	private ContainerProductMgrIFaceZJ containerProductMgrZJ;
	private TransportOutboundMgrIFaceZJ transportOutboundMgrZJ;
	private TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ;
	private TransportWarehouseMgrZJ transportWarehouseMgrZJ;
	private LPTypeMgrIFaceZJ lpTypeMgrZJ;
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	private ConfigChangeMgrIFaceZJ configChangeMgrZJ;
	
	/**
	 * 增加转运单
	 * 
	 * @param request
	 * @throws Exception
	 */
	public long addTransport(HttpServletRequest request) throws Exception {
		try {
			// 收货信息
			long receive_psid = StringUtil.getLong(request, "receive_psid");
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
			String deliver_city = StringUtil.getString(request, "deliver_city");
			String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
			String deliver_street = StringUtil.getString(request, "deliver_street");
			String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
			String deliver_name = StringUtil.getString(request, "deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
			String transport_receive_date = StringUtil.getString(request, "transport_receive_date");
			String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
			
			// 提货地址
			long send_psid = StringUtil.getLong(request, "send_psid");
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			long send_ccid = StringUtil.getLong(request, "send_ccid");
			long send_pro_id = StringUtil.getLong(request, "send_pro_id");
			String send_city = StringUtil.getString(request, "send_city");
			String send_house_number = StringUtil.getString(request, "send_house_number");
			String send_street = StringUtil.getString(request, "send_street");
			String send_zip_code = StringUtil.getString(request, "send_zip_code");
			String send_name = StringUtil.getString(request, "send_name");
			String send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
			String transport_out_date = StringUtil.getString(request, "transport_out_date");
			String address_state_send = StringUtil.getString(request, "address_state_send");
			
			String remark = StringUtil.getString(request, "remark");
			long title_id = StringUtil.getLong(request, "title_id");
			// 运输设置
			long fr_id = StringUtil.getLong(request, "fr_id");
			String transport_waybill_name = StringUtil.getString(request, "transport_waybill_name");
			String transport_waybill_number = StringUtil.getString(request, "transport_waybill_number");
			int transportby = StringUtil.getInt(request, "transportby");
			String carriers = StringUtil.getString(request, "carriers");
			String transport_send_place = StringUtil.getString(request, "transport_send_place");
			String transport_receive_place = StringUtil.getString(request, "transport_receive_place");
			
			// 主流程
			String adminUserIdsTransport = StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport = StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport = StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport = StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport = StringUtil.getInt(request, "needPageTransport");
			
			// 出口报关
			int declaration = StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration = StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration = StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration = StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration = StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration = StringUtil.getInt(request, "needPageDeclaration");
			
			// 进口清关
			int clearance = StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance = StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance = StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance = StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance = StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance = StringUtil.getInt(request, "needPageClearance");
			
			// 商品图片
			String adminUserIdsProductFile = StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile = StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile = StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile = StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile = StringUtil.getInt(request, "needPageProductFile");
			
			// 内部标签
			int tag = StringUtil.getInt(request, "tag");
			String adminUserIdsTag = StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag = StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag = StringUtil.getInt(request, "needMailTag");
			int needMessageTag = StringUtil.getInt(request, "needMessageTag");
			int needPageTag = StringUtil.getInt(request, "needPageTag");
			
			// 第三方标签
			int tag_third = StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird = StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird = StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird = StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird = StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird = StringUtil.getInt(request, "needPageTagThird");
			
			// 运费
			
			int stock_in_set = StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet = StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet = StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet = StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet = StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet = StringUtil.getInt(request, "needPageStockInSet");
			
			// 单证
			int certificate = StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate = StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate = StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate = StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate = StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate = StringUtil.getInt(request, "needPageCertificate");
			
			// 质检
			int quality_inspection = StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection = StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection = StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection = StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection = StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection = StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow transport = new DBRow();
			// 收货信息
			transport.add("receive_psid", receive_psid);
			transport.add("deliver_ccid", deliver_ccid);
			transport.add("deliver_pro_id", deliver_pro_id);
			transport.add("deliver_city", deliver_city);
			transport.add("deliver_house_number", deliver_house_number);
			transport.add("deliver_street", deliver_street);
			transport.add("deliver_zip_code", deliver_zip_code);
			transport.add("transport_address", "");
			transport.add("transport_linkman", deliver_name);
			transport.add("transport_linkman_phone", deliver_linkman_phone);
			if (1 == receiveProductStorage.get("storage_type", 0) && !"".equals(transport_receive_date)) {
				transport.add("transport_receive_date", transport_receive_date);
			} else {
				transport.add("transport_receive_date", null);
			}
			transport.add("address_state_deliver", address_state_deliver);
			
			// 提货信息
			transport.add("send_psid", send_psid);
			transport.add("send_city", send_city);
			transport.add("send_house_number", send_house_number);
			transport.add("send_street", send_street);
			transport.add("send_ccid", send_ccid);
			transport.add("send_pro_id", send_pro_id);
			transport.add("send_zip_code", send_zip_code);
			transport.add("send_name", send_name);
			transport.add("send_linkman_phone", send_linkman_phone);
			if (1 == sendProductStorage.get("storage_type", 0) && !"".equals(transport_out_date)) {
				transport.add("transport_out_date", transport_out_date);
			} else {
				transport.add("transport_out_date", null);
			}
			transport.add("address_state_send", address_state_send);
			// 其他信息(ETA,备注)
			transport.add("remark", remark);
			transport.add("title_id", title_id);
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			// 主流程信息
			transport.add("transport_status", TransportOrderKey.READY);// 创建的转运单都是准备中
			transport.add("transport_date", DateUtil.NowStr());
			transport.add("create_account_id", logingUserId);// 转运单的创建者
			transport.add("create_account", loginUserName);// 转运单的创建者
			transport.add("updatedate", DateUtil.NowStr());
			transport.add("updateby", logingUserId);// 转运单的创建者
			transport.add("updatename", loginUserName);// 转运单的创建者
			
			// 提货与收货仓库类型
			transport.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
			transport.add("from_ps_type", sendProductStorage.get("storage_type", 0));
			
			// 运输设置
			transport.add("fr_id", fr_id);
			transport.add("transport_waybill_name", transport_waybill_name);
			transport.add("transport_waybill_number", transport_waybill_number);
			transport.add("transportby", transportby);
			transport.add("carriers", carriers);
			transport.add("transport_send_place", transport_send_place);
			transport.add("transport_receive_place", transport_receive_place);
			
			// 流程信息
			transport.add("declaration", declaration);
			transport.add("clearance", clearance);
			transport.add("product_file", TransportProductFileKey.PRODUCTFILE);
			transport.add("tag", tag);
			transport.add("tag_third", tag_third);
			transport.add("stock_in_set", stock_in_set);
			transport.add("certificate", certificate);
			transport.add("quality_inspection", quality_inspection);
			
			long transport_id = floorTransportMgrZJ.addTransport(transport);
			
			// 添加转运单详细
			this.addPurchaseTransportDetail(0, transport_id, request);
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "创建转运单:单号" + transport_id, (new AdminMgr())
					.getAdminLoginBean(StringUtil.getSession(request)).getAdid(),
					(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),
					TransportLogTypeKey.Goods, TransportOrderKey.READY);// 类型1跟进记录
			
			freightMgrLL.transportSetFreight(transport_id, request);
			
			// 添加转运单索引
			this.editTransportIndex(transport_id, "add");
			
			// 转运单与交货单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id，转运单传0
			addSculde(transport_id, 0L, sendProductStorage.getString("title"),
					receiveProductStorage.getString("title"), transport_receive_date, logingUserId, loginUserName,
					request, adminUserIdsTransport, adminUserNamesTransport, needMailTransport, needMessageTransport,
					needPageTransport, declaration, adminUserNamesDeclaration, adminUserIdsDeclaration,
					needPageDeclaration, needMailDeclaration, needMessageDeclaration, clearance,
					adminUserNamesClearance, adminUserIdsClearance, needPageClearance, needMailClearance,
					needMessageClearance, TransportProductFileKey.PRODUCTFILE, adminUserNamesProductFile,
					adminUserIdsProductFile, needPageProductFile, needMailProductFile, needMessageProductFile, tag,
					adminUserNamesTag, adminUserIdsTag, needPageTag, needMailTag, needMessageTag, stock_in_set,
					adminUserNamesStockInSet, adminUserIdsStockInSet, needPageStockInSet, needMailStockInSet,
					needMessageStockInSet, certificate, adminUserNamesCertificate, adminUserIdsCertificate,
					needPageCertificate, needMailCertificate, needMessageCertificate, quality_inspection,
					adminUserNamesQualityInspection, adminUserIdsQualityInspection, needPageQualityInspection,
					needMailQualityInspection, needMessageQualityInspection, tag_third, adminUserIdsTagThird,
					adminUserNamesTagThird, needMailTagThird, needMessageTagThird, needPageTagThird);
			
			return transport_id;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	public void addSculde(long transport_id, long purchase_id, String cata_name_send, String cata_name_deliver,
			String eta, long logingUserId, String loginUserName, HttpServletRequest request,
			String adminUserIdsTransport, String adminUserNamesTransport, int needMailTransport,
			int needMessageTransport, int needPageTransport, int declaration, String adminUserNamesDeclaration,
			String adminUserIdsDeclaration, int needPageDeclaration, int needMailDeclaration,
			int needMessageDeclaration, int clearance, String adminUserNamesClearance, String adminUserIdsClearance,
			int needPageClearance, int needMailClearance, int needMessageClearance, int product_file,
			String adminUserNamesProductFile, String adminUserIdsProductFile, int needPageProductFile,
			int needMailProductFile, int needMessageProductFile, int tag, String adminUserNamesTag,
			String adminUserIdsTag, int needPageTag, int needMailTag, int needMessageTag, int stock_in_set,
			String adminUserNamesStockInSet, String adminUserIdsStockInSet, int needPageStockInSet,
			int needMailStockInSet, int needMessageStockInSet, int certificate, String adminUserNamesCertificate,
			String adminUserIdsCertificate, int needPageCertificate, int needMailCertificate,
			int needMessageCertificate, int quality_inspection, String adminUserNamesQualityInspection,
			String adminUserIdsQualityInspection, int needPageQualityInspection, int needMailQualityInspection,
			int needMessageQualityInspection, int tag_third, String adminUserIdsTagThird,
			String adminUserNamesTagThird, int needMailTagThird, int needMessageTagThird, int needPageTagThird)
			throws Exception {
		String sendContent = this.handleSendContent(transport_id, purchase_id, declaration, adminUserNamesDeclaration,
				clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile, tag, adminUserNamesTag,
				stock_in_set, adminUserNamesStockInSet, certificate, adminUserNamesCertificate, quality_inspection,
				adminUserNamesQualityInspection, cata_name_send, cata_name_deliver, eta);
		
		// 转运单主流程任务
		transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), "", logingUserId, adminUserIdsTransport, "",
				transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id + "[主流程]", sendContent,
				needPageTransport, needMailTransport, needMessageTransport, request, true, 4, TransportOrderKey.READY,
				loginUserName, "创建转运单" + transport_id + "[备货中]");
		// 报关
		if (DeclarationKey.DELARATION == declaration) {
			transportMgrZyj.addTransportScheduleOneProdure("", "", logingUserId, adminUserIdsDeclaration, "",
					transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id + "[报关流程]", sendContent,
					needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
					DeclarationKey.DELARATION, loginUserName, "创建转运单" + transport_id + "[需要报关]");
		}
		
		// 清关
		if (ClearanceKey.CLEARANCE == clearance) {
			transportMgrZyj.addTransportScheduleOneProdure("", "", logingUserId, adminUserIdsClearance, "",
					transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id + "[清关流程]", sendContent,
					needPageClearance, needMailClearance, needMessageClearance, request, false, 6,
					ClearanceKey.CLEARANCE, loginUserName, "创建转运单" + transport_id + "[需要清关]");
		}
		// 商品图片
		if (TransportProductFileKey.PRODUCTFILE == product_file) {
			TDate tdateProductFile = new TDate();
			tdateProductFile
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			
			transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeProductFile, logingUserId,
					adminUserIdsProductFile, "", transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id
							+ "[商品图片流程]", sendContent, needPageProductFile, needMailProductFile,
					needMessageProductFile, request, true, 10, TransportProductFileKey.PRODUCTFILE, loginUserName,
					"创建转运单" + transport_id + "[商品文件上传中]");
		}
		// 内部标签
		if (TransportTagKey.TAG == tag) {
			TDate tdateTag = new TDate();
			tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
			String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
			// 添加
			transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeTag, logingUserId,
					adminUserIdsTag, "", transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id + "[内部标签流程]",
					sendContent, needPageTag, needMailTag, needMessageTag, request, true, 8, TransportTagKey.TAG,
					loginUserName, "创建转运单" + transport_id + "[制签中]");
		}
		
		// 第三方标签
		if (TransportTagKey.TAG == tag_third) {
			TDate tdateTagThird = new TDate();
			tdateTagThird.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
			String endTimeTagThird = tdateTagThird.formatDate("yyyy-MM-dd HH:mm:ss");
			// 添加
			transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeTagThird, logingUserId,
					adminUserIdsTagThird, "", transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id
							+ "[第三方标签流程]", sendContent, needPageTagThird, needMailTagThird, needMessageTagThird,
					request, true, TransportLogTypeKey.THIRD_TAG, TransportTagKey.TAG, loginUserName, "创建转运单"
							+ transport_id + "[制签中]");
		}
		
		// 运费
		if (TransportStockInSetKey.SHIPPINGFEE_SET == stock_in_set) {
			// 添加
			transportMgrZyj.addTransportScheduleOneProdure("", "", logingUserId, adminUserIdsStockInSet, "",
					transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id + "[运费流程]", sendContent,
					needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5,
					TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "创建转运单" + transport_id + "[需要运费]");
		}
		
		// 单证
		if (TransportCertificateKey.CERTIFICATE == certificate) {
			TDate tdateCertificate = new TDate();
			tdateCertificate
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeCertificate, logingUserId,
					adminUserIdsCertificate, "", transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id
							+ "[单证流程]", sendContent, needPageCertificate, needMailCertificate, needMessageCertificate,
					request, true, 9, TransportCertificateKey.CERTIFICATE, loginUserName, "创建转运单" + transport_id
							+ "[单证采集中]");
		}
		
		// 质检
		if (TransportQualityInspectionKey.NEED_QUALITY == quality_inspection) {
			TDate tdateQuality = new TDate();
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeQuality, logingUserId,
					adminUserIdsQualityInspection, "", transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:" + transport_id
							+ "[质检流程]", sendContent, needPageQualityInspection, needMailQualityInspection,
					needMessageQualityInspection, request, true, 11, TransportQualityInspectionKey.NEED_QUALITY,
					loginUserName, "创建转运单" + transport_id + "[需要质检]");
		}
	}
	
	public long updateTransportBasic(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			
			DBRow oldTransport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			int isOutter = StringUtil.getInt(request, "isOutter");
			
			long send_psid = StringUtil.getLong(request, "send_psid", oldTransport.get("send_psid", 0l));
			long receive_psid = StringUtil.getLong(request, "receive_psid");
			
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			
			long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
			String deliver_city = StringUtil.getString(request, "deliver_city");
			String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
			String deliver_street = StringUtil.getString(request, "deliver_street");
			String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
			String deliver_name = StringUtil.getString(request, "deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
			String transport_receive_date = StringUtil.getString(request, "transport_receive_date");
			String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
			long send_ccid = StringUtil.getLong(request, "send_ccid");
			long send_pro_id = StringUtil.getLong(request, "send_pro_id");
			String send_city = StringUtil.getString(request, "send_city");
			String send_house_number = StringUtil.getString(request, "send_house_number");
			String send_street = StringUtil.getString(request, "send_street");
			String send_zip_code = StringUtil.getString(request, "send_zip_code");
			String send_name = StringUtil.getString(request, "send_name");
			String send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
			String transport_out_date = StringUtil.getString(request, "transport_out_date");
			String address_state_send = StringUtil.getString(request, "address_state_send");
			String remark = StringUtil.getString(request, "remark");
			long title_id = StringUtil.getLong(request, "title_id");
			
			DBRow transport = new DBRow();
			transport.add("receive_psid", receive_psid);
			transport.add("deliver_ccid", deliver_ccid);
			transport.add("deliver_pro_id", deliver_pro_id);
			transport.add("deliver_city", deliver_city);
			transport.add("deliver_house_number", deliver_house_number);
			transport.add("deliver_street", deliver_street);
			transport.add("deliver_zip_code", deliver_zip_code);
			transport.add("address_state_deliver", address_state_deliver);
			if (1 == receiveProductStorage.get("storage_type", 0) && !"".equals(transport_receive_date)) {
				transport.add("transport_receive_date", transport_receive_date);
			} else {
				transport.add("transport_receive_date", null);
			}
			
			if (oldTransport.get("transport_status", 0) == TransportOrderKey.READY) {
				
				transport.add("send_psid", send_psid);
				transport.add("send_city", send_city);
				transport.add("send_house_number", send_house_number);
				transport.add("send_street", send_street);
				transport.add("send_ccid", send_ccid);
				transport.add("send_pro_id", send_pro_id);
				transport.add("send_zip_code", send_zip_code);
				transport.add("send_name", send_name);
				transport.add("send_linkman_phone", send_linkman_phone);
				transport.add("address_state_send", address_state_send);
				transport.add("from_ps_type", sendProductStorage.get("storage_type", 0));
				if (1 == sendProductStorage.get("storage_type", 0) && !"".equals(transport_out_date)) {
					transport.add("transport_out_date", transport_out_date);
				} else {
					transport.add("transport_out_date", null);
				}
			}
			
			if (isOutter != 2) {
				transport.add("create_account_id", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
						.getAdid());// 转运单的创建者
				transport.add("create_account", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
						.getEmploye_name());// 转运单的创建者
			}
			
			transport.add("transport_address", "");
			transport.add("transport_linkman", deliver_name);
			transport.add("transport_linkman_phone", deliver_linkman_phone);
			transport.add("remark", remark);
			transport.add("title_id", title_id);
			// transport.add("transport_status",TransportOrderKey.READY);//创建的转运单都是准备中
			
			transport.add("transport_date", DateUtil.NowStr());
			
			transport.add("updatedate", DateUtil.NowStr());
			transport.add("updateby", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());// 转运单的创建者
			transport.add("updatename", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
					.getEmploye_name());// 转运单的创建者
			transport.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
			
			floorTransportMgrZJ.modTransport(transport_id, transport);
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "修改转运单:单号" + transport_id, (new AdminMgr())
					.getAdminLoginBean(StringUtil.getSession(request)).getAdid(),
					(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(), 1);// 类型1跟进记录
			
			// 修改转运单索引
			this.editTransportIndex(transport_id, "update");
			
			return transport_id;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	public long updateTransportForPurchaseBasic(HttpServletRequest request) throws Exception {
		long transport_id = StringUtil.getLong(request, "transport_id");
		
		long purchase_id = StringUtil.getLong(request, "purchase_id");
		
		long send_psid = StringUtil.getLong(request, "send_psid");
		long receive_psid = StringUtil.getLong(request, "receive_psid");
		
		DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
		
		long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
		long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
		String deliver_city = StringUtil.getString(request, "deliver_city");
		String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
		String deliver_street = StringUtil.getString(request, "deliver_street");
		String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
		String deliver_name = StringUtil.getString(request, "deliver_name");
		String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
		String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
		String transport_receive_date = StringUtil.getString(request, "transport_receive_date");
		
		long send_ccid = StringUtil.getLong(request, "send_ccid");
		long send_pro_id = StringUtil.getLong(request, "send_pro_id");
		String send_city = StringUtil.getString(request, "send_city");
		String send_house_number = StringUtil.getString(request, "send_house_number");
		String send_street = StringUtil.getString(request, "send_street");
		String send_zip_code = StringUtil.getString(request, "send_zip_code");
		String send_name = StringUtil.getString(request, "send_name");
		String send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
		String address_state_send = StringUtil.getString(request, "address_state_send");
		
		String remark = StringUtil.getString(request, "remark");
		long title_id = StringUtil.getLong(request, "title_id");
		
		DBRow transport = new DBRow();
		
		transport.add("receive_psid", receive_psid);
		
		transport.add("deliver_ccid", deliver_ccid);
		transport.add("deliver_pro_id", deliver_pro_id);
		transport.add("deliver_city", deliver_city);
		transport.add("deliver_house_number", deliver_house_number);
		transport.add("deliver_street", deliver_street);
		transport.add("deliver_zip_code", deliver_zip_code);
		transport.add("address_state_deliver", address_state_deliver);
		
		transport.add("send_psid", send_psid);
		transport.add("send_city", send_city);
		transport.add("send_house_number", send_house_number);
		transport.add("send_street", send_street);
		transport.add("send_ccid", send_ccid);
		transport.add("send_pro_id", send_pro_id);
		
		transport.add("send_zip_code", send_zip_code);
		transport.add("send_name", send_name);
		transport.add("send_linkman_phone", send_linkman_phone);
		
		transport.add("address_state_send", address_state_send);
		transport.add("transport_address", "");
		transport.add("transport_linkman", deliver_name);
		transport.add("transport_linkman_phone", deliver_linkman_phone);
		
		if (!"".equals(transport_receive_date)) {
			transport.add("transport_receive_date", transport_receive_date);
		}
		transport.add("remark", remark);
		transport.add("title_id", title_id);
		
		transport.add("transport_date", DateUtil.NowStr());
		transport
				.add("create_account_id", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());// 转运单的创建者
		transport.add("create_account", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
				.getEmploye_name());// 转运单的创建者
		transport.add("updatedate", DateUtil.NowStr());
		transport.add("updateby", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());// 转运单的创建者
		transport.add("updatename", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
				.getEmploye_name());// 转运单的创建者
		transport.add("purchase_id", purchase_id);
		
		transport.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
		transport.add("from_ps_type", ProductStorageTypeKey.SupplierWarehouse);
		
		DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(purchase_id);
		
		int number = 1;
		try {
			if (purchaseTransportOrder.length > 0) {
				String numbers = purchaseTransportOrder[purchaseTransportOrder.length - 1]
						.getString("transport_number");
				number = Integer.parseInt(numbers) + 1;
			}
		} catch (NumberFormatException e) {
			number = 1;
		}
		
		transport.add("transport_number", number);
		
		floorTransportMgrZJ.modTransport(transport_id, transport);
		
		// this.addPurchaseTransportDetail(purchase_id, transport_id);
		
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "修改转运单:单号" + transport_id, (new AdminMgr())
				.getAdminLoginBean(StringUtil.getSession(request)).getAdid(),
				(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(), 1);// 类型1跟进记录
		
		// 更新索引
		this.editTransportIndex(transport_id, "update");
		// TransportIndexMgr.getInstance().updateIndex(transport_id,0,supplier.getString("sup_name"),receiveProductStorage.getString("title"),oldTransport.getString("transport_waybill_number"),oldTransport.getString("transport_waybill_name"),oldTransport.getString("carriers"));
		
		return transport_id;
	}
	
	/**
	 * 添加采购交货转运单
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addTransportForPurchase(HttpServletRequest request) throws Exception {
		try {
			long purchase_id = StringUtil.getLong(request, "purchase_id");
			int is_relate_purchase = 0;
			if (-1 == purchase_id) {
				purchase_id = 0;
				is_relate_purchase = 2;
			} else {
				is_relate_purchase = 1;
			}
			
			// 收货信息
			long receive_psid = StringUtil.getLong(request, "receive_psid");
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
			String deliver_city = StringUtil.getString(request, "deliver_city");
			String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
			String deliver_street = StringUtil.getString(request, "deliver_street");
			String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
			String deliver_name = StringUtil.getString(request, "deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
			String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
			
			// 提货地址
			long send_psid = StringUtil.getLong(request, "send_psid");
			int isSelectStorage = StringUtil.getInt(request, "isSelectStorage");
			String storageOrSupplierName = "";
			// 仓库
			int send_ps_type = 0;
			if (1 == isSelectStorage) {
				DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
				if (null != sendProductStorage) {
					storageOrSupplierName = sendProductStorage.getString("title");
					send_ps_type = sendProductStorage.get("storage_type", 0);
				}
				
			}
			// 供应商
			else {
				DBRow supplier = floorSupplierMgrTJH.getDetailSupplier(send_psid);// 采购单交货转运，提货仓库为供应商
				if (null != supplier) {
					storageOrSupplierName = supplier.getString("sup_name");
					send_ps_type = ProductStorageTypeKey.SupplierWarehouse;
				}
			}
			long send_ccid = StringUtil.getLong(request, "send_ccid");
			long send_pro_id = StringUtil.getLong(request, "send_pro_id");
			String send_city = StringUtil.getString(request, "send_city");
			String send_house_number = StringUtil.getString(request, "send_house_number");
			String send_street = StringUtil.getString(request, "send_street");
			String send_zip_code = StringUtil.getString(request, "send_zip_code");
			String send_name = StringUtil.getString(request, "send_name");
			String send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
			String transport_receive_date = StringUtil.getString(request, "transport_receive_date");
			String address_state_send = StringUtil.getString(request, "address_state_send");
			String remark = StringUtil.getString(request, "remark");
			long title_id = StringUtil.getLong(request, "title_id");
			// 运输设置
			long fr_id = StringUtil.getLong(request, "fr_id");
			String transport_waybill_name = StringUtil.getString(request, "transport_waybill_name");
			String transport_waybill_number = StringUtil.getString(request, "transport_waybill_number");
			int transportby = StringUtil.getInt(request, "transportby");
			String carriers = StringUtil.getString(request, "carriers");
			String transport_send_place = StringUtil.getString(request, "transport_send_place");
			String transport_receive_place = StringUtil.getString(request, "transport_receive_place");
			
			// 主流程
			String adminUserIdsTransport = StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport = StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport = StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport = StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport = StringUtil.getInt(request, "needPageTransport");
			
			// 出口报关
			int declaration = StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration = StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration = StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration = StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration = StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration = StringUtil.getInt(request, "needPageDeclaration");
			
			// 进口清关
			int clearance = StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance = StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance = StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance = StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance = StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance = StringUtil.getInt(request, "needPageClearance");
			
			// 商品图片
			String adminUserIdsProductFile = StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile = StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile = StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile = StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile = StringUtil.getInt(request, "needPageProductFile");
			
			// 内部标签
			String adminUserIdsTag = StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag = StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag = StringUtil.getInt(request, "needMailTag");
			int needMessageTag = StringUtil.getInt(request, "needMessageTag");
			int needPageTag = StringUtil.getInt(request, "needPageTag");
			
			// 第三方标签
			int tag_third = StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird = StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird = StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird = StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird = StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird = StringUtil.getInt(request, "needPageTagThird");
			
			int stock_in_set = StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet = StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet = StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet = StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet = StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet = StringUtil.getInt(request, "needPageStockInSet");
			
			// 单证
			int certificate = StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate = StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate = StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate = StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate = StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate = StringUtil.getInt(request, "needPageCertificate");
			
			// 质检
			String adminUserIdsQualityInspection = StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection = StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection = StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection = StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection = StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow transport = new DBRow();
			// 采购单号
			transport.add("purchase_id", purchase_id);
			transport.add("is_relate_purchase", is_relate_purchase);
			
			// 收货信息
			transport.add("receive_psid", receive_psid);
			transport.add("deliver_ccid", deliver_ccid);
			transport.add("deliver_pro_id", deliver_pro_id);
			transport.add("deliver_city", deliver_city);
			transport.add("deliver_house_number", deliver_house_number);
			transport.add("deliver_street", deliver_street);
			transport.add("deliver_zip_code", deliver_zip_code);
			transport.add("transport_address", "");
			transport.add("transport_linkman", deliver_name);
			transport.add("transport_linkman_phone", deliver_linkman_phone);
			transport.add("address_state_deliver", address_state_deliver);
			
			// 提货信息
			transport.add("send_psid", send_psid);
			transport.add("send_city", send_city);
			transport.add("send_house_number", send_house_number);
			transport.add("send_street", send_street);
			transport.add("send_ccid", send_ccid);
			transport.add("send_pro_id", send_pro_id);
			transport.add("send_zip_code", send_zip_code);
			transport.add("send_name", send_name);
			transport.add("send_linkman_phone", send_linkman_phone);
			transport.add("address_state_send", address_state_send);
			
			// 其他信息(ETA,备注)
			if (!"".equals(transport_receive_date)) {
				transport.add("transport_receive_date", transport_receive_date);
			}
			transport.add("remark", remark);
			transport.add("title_id", title_id);
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			if (0 != purchase_id) {
				// 设置交货型转运的批次
				DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(purchase_id);
				int number = 1;
				try {
					if (purchaseTransportOrder.length > 0) {
						String numbers = purchaseTransportOrder[purchaseTransportOrder.length - 1]
								.getString("transport_number");
						number = Integer.parseInt(numbers) + 1;
					}
				} catch (NumberFormatException e) {
					number = 1;
				}
				transport.add("transport_number", number);
			}
			
			// 主流程信息
			transport.add("transport_status", TransportOrderKey.READY);// 创建的转运单都是准备中
			transport.add("transport_date", DateUtil.NowStr());
			transport.add("create_account_id", logingUserId);// 转运单的创建者
			transport.add("create_account", loginUserName);// 转运单的创建者
			transport.add("updatedate", DateUtil.NowStr());
			transport.add("updateby", logingUserId);// 转运单的创建者
			transport.add("updatename", loginUserName);// 转运单的创建者
			
			// 提货与收货仓库类型
			transport.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
			transport.add("from_ps_type", send_ps_type);
			
			// 运输设置
			transport.add("fr_id", fr_id);
			transport.add("transport_waybill_name", transport_waybill_name);
			transport.add("transport_waybill_number", transport_waybill_number);
			transport.add("transportby", transportby);
			transport.add("carriers", carriers);
			transport.add("transport_send_place", transport_send_place);
			transport.add("transport_receive_place", transport_receive_place);
			
			// 流程信息
			transport.add("declaration", declaration);
			transport.add("clearance", clearance);
			transport.add("product_file", TransportProductFileKey.PRODUCTFILE);
			transport.add("tag", TransportTagKey.TAG);
			transport.add("tag_third", tag_third);
			transport.add("stock_in_set", stock_in_set);
			transport.add("certificate", certificate);
			transport.add("quality_inspection", TransportQualityInspectionKey.NEED_QUALITY);
			
			long transport_id = floorTransportMgrZJ.addTransport(transport);
			
			this.addPurchaseTransportDetail(purchase_id, transport_id, request);
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "创建转运单:单号" + transport_id, (new AdminMgr())
					.getAdminLoginBean(StringUtil.getSession(request)).getAdid(),
					(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),
					TransportLogTypeKey.Goods, TransportOrderKey.READY);// 类型1跟进记录
			
			freightMgrLL.transportSetFreight(transport_id, request);
			
			// 创建交货时，如果采购单的状态为价格已确认或需跟进交货才需要更新采购单的状态为交货中
			if (0 != purchase_id) {
				DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
				int purchase_status = purchase.get("purchase_status", 0);
				if (PurchaseKey.AFFIRMPRICE == purchase_status || PurchaseKey.AFFIRMTRANSFER == purchase_status) {
					DBRow purchaseRow = new DBRow();
					purchaseRow.add("purchase_status", PurchaseKey.DELIVERYING);
					floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
				}
			}
			
			// 添加转运单索引
			this.editTransportIndex(transport_id, "add");
			
			// 转运单与交货单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id
			addSculde(transport_id, purchase_id, storageOrSupplierName, receiveProductStorage.getString("title"),
					transport_receive_date, logingUserId, loginUserName, request, adminUserIdsTransport,
					adminUserNamesTransport, needMailTransport, needMessageTransport, needPageTransport, declaration,
					adminUserNamesDeclaration, adminUserIdsDeclaration, needPageDeclaration, needMailDeclaration,
					needMessageDeclaration, clearance, adminUserNamesClearance, adminUserIdsClearance,
					needPageClearance, needMailClearance, needMessageClearance, TransportProductFileKey.PRODUCTFILE,
					adminUserNamesProductFile, adminUserIdsProductFile, needPageProductFile, needMailProductFile,
					needMessageProductFile, TransportTagKey.TAG, adminUserNamesTag, adminUserIdsTag, needPageTag,
					needMailTag, needMessageTag, stock_in_set, adminUserNamesStockInSet, adminUserIdsStockInSet,
					needPageStockInSet, needMailStockInSet, needMessageStockInSet, certificate,
					adminUserNamesCertificate, adminUserIdsCertificate, needPageCertificate, needMailCertificate,
					needMessageCertificate, TransportQualityInspectionKey.NEED_QUALITY,
					adminUserNamesQualityInspection, adminUserIdsQualityInspection, needPageQualityInspection,
					needMailQualityInspection, needMessageQualityInspection, tag_third, adminUserIdsTagThird,
					adminUserNamesTagThird, needMailTagThird, needMessageTagThird, needPageTagThird);
			
			return transport_id;
		} catch (Exception e) {
			throw new SystemException(e, "addTransportForPurchase", log);
		}
	}
	
	/**
	 * 采购单交货转运单生成转运单明细
	 * 
	 * @param purchase_id
	 * @param transport_id
	 * @throws Exception
	 */
	private void addPurchaseTransportDetail2(long purchase_id, long transport_id, HttpServletRequest request)
			throws Exception {
		try {
			floorTransportMgrZJ.delTransportDetailByTransportId(transport_id);// 批量删除转运明细
			
			String[] pcids = request.getParameterValues("pc_id");
			
			if (pcids == null) {
				pcids = new String[0];
			}
			for (int i = 0; i < pcids.length; i++) {
				long pc_id = Long.parseLong(pcids[i]);
				
				int deliver_count = (int) StringUtil.getFloat(request, pcids[i] + "_deliver_count");
				int backup_count = (int) StringUtil.getFloat(request, pcids[i] + "_backup_count");
				String box = StringUtil.getString(request, pcids[i] + "_box");
				int count = deliver_count + backup_count;
				
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				double send_price = product.get("unit_price", 0d);
				if (purchaseDetail != null) {
					send_price = purchaseDetail.get("price", 0d);
				}
				
				float union_count = 0;
				float normal_count = 0;
				
				if (product.get("union_flag", 0) == 1) {
					union_count = count;
				} else {
					normal_count = count;
				}
				
				DBRow transportDetail = new DBRow();
				transportDetail.add("transport_pc_id", pc_id);
				transportDetail.add("transport_delivery_count", deliver_count);
				transportDetail.add("transport_backup_count", backup_count);
				transportDetail.add("transport_count", count);
				transportDetail.add("transport_id", transport_id);
				transportDetail.add("transport_p_name", product.getString("p_name"));
				transportDetail.add("transport_p_code", product.getString("p_code"));
				transportDetail.add("transport_volume", product.getString("volume"));
				transportDetail.add("transport_weight", product.getString("weight"));
				transportDetail.add("transport_box", box);
				
				transportDetail.add("transport_send_price", send_price);
				transportDetail.add("transport_union_count", union_count);
				transportDetail.add("transport_normal_count", normal_count);
				
				floorTransportMgrZJ.addTransportDetail(transportDetail);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addPurchaseTransportDetail", log);
		}
	}
	
	/**
	 * 保存导入转运单商品
	 * 
	 * @param purchase_id
	 * @param transport_id
	 * @param request
	 * @throws Exception
	 */
	public void addPurchaseTransportDetail(long purchase_id, long transport_id, HttpServletRequest request)
			throws Exception {
		floorTransportMgrZJ.delTransportDetailByTransportId(transport_id);// 批量删除转运明细
		
		String import_name = StringUtil.getString(request, "import_name");
		DBRow[] importTransportDetails = floorTransportMgrZJ.getImportTransportDetail(import_name);
		
		for (int i = 0; i < importTransportDetails.length; i++) {
			long pc_id = importTransportDetails[i].get("transport_pc_id", 0l);
			
			int deliver_count = (int) importTransportDetails[i].get("transport_delivery_count", 0f);
			int backup_count = (int) importTransportDetails[i].get("transport_backup_count", 0f);
			String box = importTransportDetails[i].getString("transport_detail_box");
			String lot_number = importTransportDetails[i].getString("lot_number");
			if ("".equals(lot_number)) {
				lot_number = String.valueOf(transport_id);
			}
			int count = deliver_count + backup_count;
			String serial_number = importTransportDetails[i].getString("transport_detail_serial_number");
			
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			double send_price = product.get("unit_price", 0d);
			if (0 != purchase_id) {
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				if (purchaseDetail != null) {
					send_price = purchaseDetail.get("price", 0d);
				}
			}
			
			float union_count = 0;
			float normal_count = 0;
			
			if (product.get("union_flag", 0) == 1) {
				union_count = count;
			} else {
				normal_count = count;
			}
			
			DBRow transportDetail = new DBRow();
			transportDetail.add("transport_pc_id", pc_id);
			transportDetail.add("transport_delivery_count", deliver_count);
			transportDetail.add("transport_backup_count", backup_count);
			transportDetail.add("transport_count", count);
			transportDetail.add("transport_id", transport_id);
			transportDetail.add("transport_p_name", product.getString("p_name"));
			transportDetail.add("transport_p_code", product.getString("p_code"));
			transportDetail.add("transport_volume", product.getString("volume"));
			transportDetail.add("transport_weight", product.getString("weight"));
			transportDetail.add("transport_box", box);
			transportDetail.add("lot_number", lot_number);
			
			transportDetail.add("transport_send_price", send_price);
			transportDetail.add("transport_union_count", union_count);
			transportDetail.add("transport_normal_count", normal_count);
			if (!serial_number.equals("")) {
				transportDetail.add("transport_product_serial_number", serial_number);
			}
			
			floorTransportMgrZJ.addTransportDetail(transportDetail);
		}
	}
	
	/**
	 * 获得采购单需交货预填文件
	 * 
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public String getPurchaseTransportDetailPrefill(HttpServletRequest request) throws Exception {
		try {
			long purchase_id = StringUtil.getLong(request, "purchase_id");
			
			DBRow[] purchaseDetails = floorPurchaseMgr.getPurchaseDetail(purchase_id, null, null);
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; purchaseDetails != null && i < purchaseDetails.length; i++) {
				long transport_pc_id = purchaseDetails[i].get("product_id", 0l);
				
				DBRow product = floorProductMgr.getDetailProductByPcid(transport_pc_id);
				if (product == null) {
					continue;
				}
				float order_count = purchaseDetails[i].get("order_count", 0f);
				float backup_count = purchaseDetails[i].get("backup_count", 0f);
				float reap_count = purchaseDetails[i].get("reap_count", 0f);
				float purchase_count = purchaseDetails[i].get("purchase_count", 0f);
				
				float transit_deliver_count = floorTransportMgrZJ.getTransitDeliverCountForPurchase(purchase_id,
						transport_pc_id);
				float transit_backup_count = floorTransportMgrZJ.getTransitBackupCountForPurchase(purchase_id,
						transport_pc_id);
				
				float prefill_backup_count = backup_count - transit_backup_count;
				float prefill_deliver_count = order_count - transit_deliver_count;
				
				float prefill_count = prefill_backup_count + prefill_deliver_count;
				
				if (prefill_count > 0 && purchase_count > reap_count) {
					DBRow prefill = new DBRow();
					prefill.add("prefill_backup_count", prefill_backup_count);
					prefill.add("prefill_deliver_count", prefill_deliver_count);
					prefill.add("p_name", product.getString("p_name"));
					prefill.add("pc_id", transport_pc_id);
					
					list.add(prefill);
				}
			}
			
			POIFSFileSystem fs = null;// 获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
					+ "/administrator/transport/transport_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 创建一个居中格式
			style.setLocked(false);// 设置不锁定
			style.setWrapText(true);
			
			for (int i = 0; i < list.size(); i++) {
				DBRow transportDetail = list.get(i);
				long pc_id = transportDetail.get("pc_id", 0l);
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(transportDetail.getString("prefill_deliver_count"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(transportDetail.getString("prefill_backup_count"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(transportDetail.getString("transport_product_serial_number"));
				row.getCell(3).setCellStyle(style);
				
				row.createCell(4).setCellValue(transportDetail.getString("transport_box"));
				row.getCell(4).setCellStyle(style);
			}
			String filename = +System.currentTimeMillis() + ".xls";
			String path = "upl_imags_tmp/" + filename;
			FileOutputStream fout = new FileOutputStream(Environment.getHome() + path);
			wb.write(fout);
			fout.close();
			
			return path;
		} catch (Exception e) {
			throw new SystemException(e, "getPurchaseTransportDetailPrefill", log);
		}
		
	}
	
	/**
	 * 增加转运单详细（单个）jqgrid的form增加
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void addTransportDetail(HttpServletRequest request) throws Exception {
		try {
			String transport_p_name = StringUtil.getString(request, "p_name");
			
			long transport_id = StringUtil.getLong(request, "transport_id");
			String transport_box = StringUtil.getString(request, "transport_box");
			String lot_number = StringUtil.getString(request, "lot_number");
			float transport_delivery_count = StringUtil.getFloat(request, "transport_delivery_count");
			float transport_backup_count = StringUtil.getFloat(request, "transport_backup_count");
			float transport_count = transport_delivery_count + transport_backup_count;
			String transport_product_serial_number = StringUtil.getString(request, "transport_product_serial_number",
					null);
			
			if (transport_count > 1
					&& (transport_product_serial_number != null && !transport_product_serial_number.equals(""))) {
				throw new CanNotFillSerialNumberException();
			}
			
			DBRow product = floorProductMgr.getDetailProductByPname(transport_p_name);
			if (product == null) {
				throw new ProductNotExistException();
			}
			
			long transport_pc_id = product.get("pc_id", 0l);
			
			DBRow transportDetailIsExit = floorTransportMgrZJ.getTransportDetailByTPId(transport_id, transport_pc_id,
					transport_product_serial_number);
			
			if (transportDetailIsExit != null) {
				throw new TransportOrderDetailRepeatException();
			}
			
			float transport_volume = product.get("volume", 0f);
			float transport_weight = product.get("weight", 0f);
			String transport_p_code = product.getString("p_code");
			
			DBRow transportDetail = new DBRow();
			transportDetail.add("transport_p_name", transport_p_name);
			transportDetail.add("transport_count", transport_count);
			transportDetail.add("transport_id", transport_id);
			transportDetail.add("transport_box", transport_box);
			transportDetail.add("lot_number", lot_number);
			transportDetail.add("transport_pc_id", transport_pc_id);
			transportDetail.add("transport_p_code", transport_p_code);
			transportDetail.add("transport_volume", transport_volume);
			transportDetail.add("transport_weight", transport_weight);
			transportDetail.add("transport_product_serial_number", transport_product_serial_number);
			
			transportDetail.add("transport_backup_count", transport_backup_count);
			transportDetail.add("transport_delivery_count", transport_delivery_count);
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			long purchase_id = transport.get("purchase_id", 0l);
			double send_price = product.get("unit_price", 0d);
			if (purchase_id != 0) {
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, transport_pc_id);
				if (purchaseDetail != null) {
					send_price = purchaseDetail.get("price", 0d);
				}
			}
			transportDetail.add("transport_send_price", send_price);
			
			floorTransportMgrZJ.addTransportDetail(transportDetail);
		} catch (CanNotFillSerialNumberException e) {
			throw e;
		} catch (HadSerialNumberException e) {
			throw e;
		} catch (ProductNotExistException e) {
			throw e;
		} catch (TransportOrderDetailRepeatException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 删除转运单
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delTransport(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			long purchase_id = this.getDetailTransportById(transport_id).get("purchase_id", 0L);
			
			// 删除转运单索引
			this.editTransportIndex(transport_id, "del");
			
			floorTransportMgrZJ.delTransport(transport_id);
			floorTransportMgrZJ.delTransportDetailByTransportId(transport_id);
			// 删除日志
			transportMgrZyj.deleteTransportLogsByTransportId(transport_id);
			// 删除运费项目
			transportMgrZyj.deleteTransportFreightByTransportId(transport_id);
			// 删除文件，(单证，清关，报关，运费，制签)需要根据关联ID，关联类型的数组，因为采购和转运单可能冲突，导致误删
			int[] types = { FileWithTypeKey.transport_certificate, FileWithTypeKey.transport_clearance,
					FileWithTypeKey.transport_declaration, FileWithTypeKey.TRANSPORT_STOCKINSET,
					FileWithTypeKey.TRANSPORT_TAG };
			transportMgrZyj.deleteTransportFileByTransportIdAndTypes(transport_id, types);
			// 删除转运单下商品的文件(7,16)(商品文件，商品标签文件)
			int[] productTypes = { FileWithTypeKey.product_file, FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE };
			transportMgrZyj.deleteTransportProductFileByTransportIdAndTypes(transport_id, productTypes);
			// 删除任务
			this.deleteTransportSchedule(transport_id, purchase_id, request);
			
		} catch (Exception e) {
			throw new SystemException(e, "delTransport", log);
		}
	}
	
	/**
	 * 删除任务
	 * 
	 * @throws Exception
	 */
	private void deleteTransportSchedule(long transport_id, long purchase_id, HttpServletRequest request)
			throws Exception {
		try {
			// 各流程任务
			int[] associateTypeArr = { ModuleKey.TRANSPORT_ORDER };
			DBRow[] transportSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(transport_id, associateTypeArr);
			for (int i = 0; i < transportSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(transportSchedules[i].get("schedule_id", 0L), request);
			}
		} catch (Exception e) {
			throw new SystemException(e, "deleteTransportSchedule", log);
		}
	}
	
	/**
	 * 删除转运单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delTransportDetail(HttpServletRequest request) throws Exception {
		try {
			long transport_detail_id = StringUtil.getLong(request, "id");// id因为jqgrid因素
			
			floorTransportMgrZJ.delTransportDetail(transport_detail_id);
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 修改转运单
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modTransport(HttpServletRequest request) throws Exception {
		long transport_id = StringUtil.getLong(request, "transport_id");
		long send_psid = StringUtil.getLong(request, "send_psid");
		long receive_psid = StringUtil.getLong(request, "receive_psid");
		
		String transport_address = StringUtil.getString(request, "transport_address");
		String transport_linkman = StringUtil.getString(request, "transport_linkman");
		String transport_linkman_phone = StringUtil.getString(request, "linkman_phone");
		String transport_waybill_name = StringUtil.getString(request, "transport_waybill_name");
		String transport_waybill_number = StringUtil.getString(request, "transport_waybill_number");
		String transport_receive_date = StringUtil.getString(request, "transport_receive_date");
		
		String transport_send_place = StringUtil.getString(request, "transport_send_place");
		String transport_send_freight = StringUtil.getString(request, "transport_send_freight");
		String transport_receive_place = StringUtil.getString(request, "transport_receive_place");
		String transport_receive_freight = StringUtil.getString(request, "transport_receive_freight");
		
		DBRow transport = new DBRow();
		
		transport.add("send_psid", send_psid);
		transport.add("receive_psid", receive_psid);
		transport.add("transport_address", transport_address);
		transport.add("transport_linkman", transport_linkman);
		transport.add("transport_linkman_phone", transport_linkman_phone);
		transport.add("transport_waybill_name", transport_waybill_name);
		transport.add("transport_waybill_number", transport_waybill_number);
		transport.add("transport_receive_date", transport_receive_date);
		
		transport.add("transport_send_place", transport_send_place);
		transport.add("transport_send_freight", transport_send_freight);
		transport.add("transport_receive_place", transport_receive_place);
		transport.add("transport_receive_freight", transport_receive_freight);
		
		floorTransportMgrZJ.modTransport(transport_id, transport);
	}
	
	/**
	 * 修改转运单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modTransportDetail(HttpServletRequest request) throws Exception {
		try {
			long transport_detail_id = StringUtil.getLong(request, "id");// grid默认参数是ID
			
			DBRow transportDetail = floorTransportMgrZJ.getTransportDetailById(transport_detail_id);
			long transport_id = transportDetail.get("transport_id", 0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(transportDetail.get("transport_pc_id", 0l));
			
			Map parameter = request.getParameterMap();
			float transport_delivery_count = transportDetail.get("transport_delivery_count", 0f);
			float transport_backup_count = transportDetail.get("transport_backup_count", 0f);
			
			String transport_box;
			String lot_number;
			String transport_p_name;
			DBRow para = new DBRow();
			
			if (parameter.containsKey("transport_delivery_count")) {
				transport_delivery_count = StringUtil.getFloat(request, "transport_delivery_count");
				para.add("transport_delivery_count", transport_delivery_count);
			}
			
			if (parameter.containsKey("transport_backup_count")) {
				transport_backup_count = StringUtil.getFloat(request, "transport_backup_count");
				para.add("transport_backup_count", transport_backup_count);
			}
			
			if (parameter.containsKey("transport_box")) {
				transport_box = StringUtil.getString(request, "transport_box");
				para.add("transport_box", transport_box);
			}
			if (parameter.containsKey("lot_number")) {
				lot_number = StringUtil.getString(request, "lot_number");
				para.add("lot_number", lot_number);
			}
			if (parameter.containsKey("p_name")) {
				transport_p_name = StringUtil.getString(request, "p_name");
				
				product = floorProductMgr.getDetailProductByPname(transport_p_name);
				if (product == null) {
					throw new ProductNotExistException();
				}
				
				long product_id = product.get("pc_id", 0l);
				
				// 检查转运单内是否有重复商品
				// DBRow productTransport = floorTransportMgrZJ.getTransportDetailByTPId(transport_id, product_id);
				// if(productTransport !=null)
				// {
				// throw new RepeatProductException();
				// }
				para.add("transport_pc_id", product.get("pc_id", 0l));
				para.add("transport_p_code", product.getString("p_code"));
				para.add("transport_p_name", transport_p_name);
				
				para.add("transport_volume", product.get("volume", 0f));
				para.add("transport_weight", product.get("weight", 0f));
				
				DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
				long purchase_id = transport.get("purchase_id", 0l);
				
				double send_price = product.get("unit_price", 0d);
				if (purchase_id != 0) {
					DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, product_id);
					if (purchaseDetail != null) {
						send_price = purchaseDetail.get("price", 0d);
					}
				}
				para.add("transport_send_price", send_price);
			}
			
			if (parameter.containsKey("transport_product_serial_number")) {
				if (transportDetail.get("transport_count", 0f) != 1) {
					throw new CanNotFillSerialNumberException();
				}
				String transport_product_serial_number = StringUtil.getString(request,
						"transport_product_serial_number");
				if (transport_product_serial_number.equals("")) {
					transport_product_serial_number = null;
				}
				
				DBRow transportDetailSN = floorTransportMgrZJ.getTransportDetailBySN(transport_id,
						transport_product_serial_number);
				if (transportDetailSN != null) {
					throw new TransportSerialNumberRepeatException();
				}
				
				para.add("transport_product_serial_number", transport_product_serial_number);
			}
			
			// 交货数量、备件数量修改后从重新计算总数量
			if (parameter.containsKey("transport_delivery_count") || parameter.containsKey("transport_backup_count")) {
				float transport_count = transport_backup_count + transport_delivery_count;
				para.add("transport_count", transport_count);
				
				if (transport_count != 1 && !transportDetail.getString("transport_product_serial_number").equals("")) {
					throw new HadSerialNumberException();
				}
			}
			
			if (parameter.containsKey("clp_type")) {
				long clp_type_id = StringUtil.getLong(request, "clp_type");
				
				DBRow clpType = lpTypeMgrZJ.getDetailCLPType(clp_type_id);
				
				if (clpType == null) {
					clpType = new DBRow();
				}
				
				para.add("clp_type_id", clp_type_id);
				para.add("blp_type_id", clpType.get("sku_lp_box_type", 0));
			}
			
			if (parameter.containsKey("blp_type")) {
				long clp_type_id = transportDetail.get("clp_type_id", 0l);
				
				if (clp_type_id == 0l) {
					long blp_type_id = StringUtil.getLong(request, "blp_type");
					para.add("blp_type_id", blp_type_id);
				}
				
			}
			
			floorTransportMgrZJ.modTransportDetail(transport_detail_id, para);
		} catch (CanNotFillSerialNumberException e) {
			throw e;
		} catch (TransportSerialNumberRepeatException e) {
			throw e;
		} catch (HadSerialNumberException e) {
			throw e;
		} catch (ProductNotExistException e) {
			throw e;
		} catch (RepeatProductException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 过滤转运单
	 * 
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration,
			int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id)
			throws Exception {
		try {
			return (floorTransportMgrZJ.fillterTransport(send_psid, receive_psid, pc, status, declaration, clearance,
					invoice, drawback, day, stock_in_set, create_account_id));
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据转运单ID获得转运单详细
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTransportById(long transport_id) throws Exception {
		try {
			return (floorTransportMgrZJ.getDetailTransportById(transport_id));
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据转运单ID获得转运单明细
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetailByTransportId(long transport_id, PageCtrl pc, String sidx, String sord,
			FilterBean fillterBean) throws Exception {
		try {
			return (floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, pc, sidx, sord, fillterBean));
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据转运单号搜索转运单
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchTransportByNumber(String search_key, int search_mode, PageCtrl pc) throws Exception {
		try {
			if (!search_key.equals("") && !search_key.equals("\"")) {
				search_key = search_key.replaceAll("\"", "");
				search_key = search_key.replaceAll("'", "");
				search_key = search_key.replaceAll("\\*", "");
				// search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return TransportIndexMgr.getInstance().getSearchResults(search_key, search_mode, page_count, pc);
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 供应商对转运单搜索
	 * 
	 * @param searchKey
	 * @param supplier_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] supplierSearchTransport(String searchKey, long supplier_id, PageCtrl pc) throws Exception {
		try {
			String type = searchKey.substring(0, 1);
			type = type.toUpperCase();
			
			DBRow[] transports = new DBRow[0];
			long id;
			
			try {
				id = Long.parseLong(searchKey.substring(1));
				
				if (type.equals("P") || type.equals("T")) {
					transports = floorTransportMgrZJ.supplierSearch(supplier_id, type, id, pc);
				}
			} catch (NumberFormatException e) {
				
			}
			
			return (transports);
		} catch (Exception e) {
			throw new SystemException(e, "supplierSearchTransport", log);
		}
	}
	
	/**
	 * 转运单详细上传
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String[] uploadTransportDetail(HttpServletRequest request) throws Exception, FileTypeException,
			FileException {
		try {
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String url = "";
			String[] msg = new String[2];
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()));
			upload.setRootFolder("/upl_imags_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long transport_id = Long.parseLong(upload.getRequestRow().getString("transport_id"));
			
			msg[1] = String.valueOf(transport_id);
			
			if (flag == 2) {
				throw new FileTypeException();// 采购单文件上传格式不对
			} else if (flag == 1) {
				throw new FileException();
			} else {
				msg[0] = upload.getFileName();
			}
			return msg;
		} catch (FileTypeException e) {
			throw e;
		} catch (FileException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "uploadPurchaseDetail", log);
		}
	}
	
	/**
	 * 将excel文件转换成DBRow[]
	 * 
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelshow(String filename) throws Exception {
		try {
			HashMap<String, String> filedName = new HashMap<String, String>();
			filedName.put("0", "p_name");
			filedName.put("1", "prefill_deliver_count");
			filedName.put("2", "prefill_backup_count");
			filedName.put("3", "serial_number");
			filedName.put("4", "prefill_box");
			filedName.put("5", "lot_number");
			
			HashMap<String, String> pcidMap = new HashMap<String, String>();
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_imags_tmp/" + filename;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);
			
			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns(); // excel表字段数
			int rsRows = rs.getRows(); // excel表记录行数
			
			for (int i = 1; i < rsRows; i++) {
				DBRow pro = new DBRow();
				
				boolean result = false;// 判断是否纯空行数据
				for (int j = 0; j < rsColumns; j++) {
					if (j == 0) {
						DBRow product = floorProductMgr.getDetailProductByPname(rs.getCell(j, i).getContents().trim());
						if (product != null)// 商品查不到，跳过循环，不添加入数组
						{
							pcidMap.put(String.valueOf(product.get("pc_id", 0l)),
									String.valueOf(product.get("pc_id", 0l)));
							pro.add("pc_id", product.get("pc_id", 0l));
							// continue;
						}
						
						// 去重商品
						// if(pcidMap.containsKey(String.valueOf(product.get("pc_id",0l))))
						// {
						// continue;
						// }
					}
					
					pro.add(filedName.get(String.valueOf(j)), rs.getCell(j, i).getContents().trim());
					
					ArrayList proName = pro.getFieldNames();// DBRow 字段名
					for (int p = 0; p < proName.size(); p++) {
						if (!pro.getString(proName.get(p).toString()).trim().equals(""))// 去掉空格
						{
							result = true;
						}
					}
				}
				
				if (result)// 不是纯空行就加入到DBRow
				{
					al.add(pro);
				}
			}
			return (al.toArray(new DBRow[0]));
		} catch (Exception e) {
			throw new SystemException(e, "excelshow", log);
		}
	}
	
	/**
	 * 上传文件保存转运单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void saveTransportDetail(HttpServletRequest request) throws Exception {
		try {
			long transport_id = Long.parseLong(StringUtil.getString(request, "transport_id"));
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			long purchase_id = transport.get("purchase_id", 0l);
			
			this.addPurchaseTransportDetail(purchase_id, transport_id, request);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "转运单导入:", adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name(), 3);
		} catch (ProductNotExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据转运明细ID获得转运明细
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportDetailById(HttpServletRequest request) throws Exception {
		try {
			long transport_detail_id = StringUtil.getLong(request, "transport_detail_id");
			
			return (floorTransportMgrZJ.getTransportDetailById(transport_detail_id));
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 转运单装箱
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void packingTransport(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			String reserve_type = StringUtil.getString(request, "reserve_type");
			
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			// 复杂的验证是否缺货
			DBRow[] lacking = this.checkStorage(transport_id);
			if (lacking.length > 0) {
				throw new TransportLackException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; i < transportDetails.length; i++) {
				float store_count = floorProductMgr.getDetailProductProductStorageByPcid(
						transport.get("send_psid", 0l), transportDetails[i].get("transport_pc_id", 0l)).get(
						"store_count", 0f);
				float difference = store_count - transportDetails[i].get("transport_count", 0f);// 转运量与实际库存的差额，大于0说明套装足够，小于0需要单独扣除散件库存，散件商品绝对差额>=0
				double sumPrice = 0;
				double sumPrice1 = 0;
				double sumPrice2 = 0;
				if (difference < 0) {
					if (store_count != 0)// 套装没库存
					{
						sumPrice1 = getStockOutPrice(transport.get("send_psid", 0l),
								transportDetails[i].get("transport_pc_id", 0l), 0);
						
						// list.add(transport_detail_storage);//实际扣的套装
						
						// 转运扣掉的套装数
						DBRow para = new DBRow();
						para.add("transport_union_count", store_count);
						floorTransportMgrZJ
								.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
					}
					
					// 转运的套装拼装数
					DBRow para = new DBRow();
					sumPrice = (sumPrice1 + sumPrice2) / transportDetails[i].get("transport_count", 0f);
					para.add("transport_normal_count", difference * -1);
					para.add("transport_send_price", sumPrice);// 记录出库金额
					floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
					
					// 系统预拼装
					productMgr.combinationProductSystem(transport.get("send_psid", 0l),
							transportDetails[i].get("transport_pc_id", 0l), (int) Math.abs(difference),
							ProductStoreBillKey.TRANSPORT_ORDER, transport_id, adminLoggerBean,
							ProductStoreOperationKey.OUT_STORE_TRANSPORT);
					
					float transport_union_count = transportDetails[i].get("transport_count", 0f) - Math.abs(difference);
					if (transport_union_count > 0) {
						DBRow transport_detail_storage = new DBRow();// 实际扣库存的DBRow
						transport_detail_storage.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
						transport_detail_storage.add("transport_count", transport_union_count);
						
						list.add(transport_detail_storage);
					}
				} else {
					DBRow transport_detail = transportDetails[i];
					transport_detail.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
					list.add(transport_detail);
					
					sumPrice = getStockOutPrice(transport.get("send_psid", 0l),
							transportDetails[i].get("transport_pc_id", 0l), 0);
					DBRow product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get("transport_pc_id",
							0l));
					
					DBRow para = new DBRow();
					if (product.get("union_flag", 0) == 0)// 转运散件商品
					{
						
						para.add("transport_normal_count", transport_detail.get("transport_count", 0f));
						
					} else// 套装商品
					{
						para.add("transport_union_count", transport_detail.get("transport_count", 0f));
					}
					para.add("transport_send_price", sumPrice);// 记录出库金额
					// 记录转运的类型数量
					floorTransportMgrZJ.modTransportDetail(transport_detail.get("transport_detail_id", 0l), para);
				}
				
			}
			
			// 扣除库存
			
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(transport_id);
			deInProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_TRANSPORT);
			
			for (int q = 0; q < list.size(); q++) {
				floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean, transport.get("send_psid", 0l),
						list.get(q).get("pc_id", 0l), list.get(q).get("transport_count", 0f));
				
				productStoreLocationMgrZJ.reserveTheoretical(transport.get("send_psid", 0l),
						list.get(q).get("pc_id", 0l), list.get(q).get("transport_count", 0f), transport_id,
						ProductStoreBillKey.TRANSPORT_ORDER, adminLoggerBean.getAdid(),
						list.get(q).getString("transport_product_serial_number"), reserve_type);
				
			}
			
			if (transport.get("transport_status", 0) == TransportOrderKey.READY)// 准备状态的才可转换成已起运
			{
				DBRow para = new DBRow();
				para.add("transport_status", TransportOrderKey.PACKING);
				para.add("packing_account", adminLoggerBean.getAdid());// 决定装箱人（谁操作的确定装箱）
				
				long slc_area_id = productStoreLocationMgrZJ.getAreaForBill(transport_id,
						ProductStoreBillKey.TRANSPORT_ORDER);
				para.add("transport_area_id", slc_area_id);
				
				int reserveType = FirstOutTypeKey.FIFO;
				
				if (reserve_type.equals("SNFO")) {
					reserveType = FirstOutTypeKey.SNFO;
				}
				para.add("reserve_type", reserveType);
				floorTransportMgrZJ.modTransport(transport_id, para);
			}
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "装箱:装箱完成", adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods, TransportOrderKey.PACKING);
			// 跟进
			scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.Goods,
					"转运单:T" + transport_id + "装箱完成", false, request, "transport_intransit_period");
		} catch (CanNotSerialNumberReserveException e) {
			throw e;
		} catch (TransportLackException e) {
			throw e;
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 判断实际装箱与计划装箱，如有差异，整张单据回退，重新扣库存
	 */
	public void packingDiffTransport(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			DBRow[] transportDiffDetails = floorTransportMgrZJ.getTransportDetailDiffByTransportId(transport_id);
			
			// 实际装箱与预装箱有差异,业务单据库存批次操作回退
			if (transportDiffDetails.length > 0) {
				productMgr.reBackAllBill(transport_id, ProductStoreBillKey.TRANSPORT_ORDER, adminLoggerBean);
				
				// 复杂的验证是否缺货
				DBRow[] lacking = this.checkStorage(transport_id);
				if (lacking.length > 0) {
					throw new TransportLackException();
				}
				
				// 从新扣库存
				DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null,
						null, null, null);
				ArrayList<DBRow> list = new ArrayList<DBRow>();
				
				for (int i = 0; i < transportDetails.length; i++) {
					float store_count = floorProductMgr.getDetailProductProductStorageByPcid(
							transport.get("send_psid", 0l), transportDetails[i].get("transport_pc_id", 0l)).get(
							"store_count", 0f);
					float difference = store_count - transportDetails[i].get("transport_count", 0f);// 转运量与实际库存的差额，大于0说明套装足够，小于0需要单独扣除散件库存，散件商品绝对差额>=0
					double sumPrice = 0;
					double sumPrice1 = 0;
					double sumPrice2 = 0;
					if (difference < 0) {
						if (store_count != 0)// 套装没库存
						{
							sumPrice1 = getStockOutPrice(transport.get("send_psid", 0l),
									transportDetails[i].get("transport_pc_id", 0l), 0);
							
							// list.add(transport_detail_storage);//实际扣的套装
							
							// 转运扣掉的套装数
							DBRow para = new DBRow();
							para.add("transport_union_count", store_count);
							floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l),
									para);
						}
						
						// 转运的套装拼装数
						DBRow para = new DBRow();
						sumPrice = (sumPrice1 + sumPrice2) / transportDetails[i].get("transport_count", 0f);
						para.add("transport_normal_count", difference * -1);
						para.add("transport_send_price", sumPrice);// 记录出库金额
						floorTransportMgrZJ
								.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
						
						// 系统预拼装
						productMgr.combinationProductSystem(transport.get("send_psid", 0l),
								transportDetails[i].get("transport_pc_id", 0l), (int) Math.abs(difference),
								ProductStoreBillKey.TRANSPORT_ORDER, transport_id, adminLoggerBean, 0);
						
						DBRow transport_detail_storage = new DBRow();// 实际扣库存的DBRow
						transport_detail_storage.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
						transport_detail_storage.add("transport_count", transportDetails[i].get("transport_count", 0f));
						
						list.add(transport_detail_storage);
						
					} else {
						DBRow transport_detail = transportDetails[i];
						transport_detail.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
						list.add(transport_detail);
						
						sumPrice = getStockOutPrice(transport.get("send_psid", 0l),
								transportDetails[i].get("transport_pc_id", 0l), 0);
						DBRow product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get(
								"transport_pc_id", 0l));
						
						DBRow para = new DBRow();
						if (product.get("union_flag", 0) == 0)// 转运散件商品
						{
							
							para.add("transport_normal_count", transport_detail.get("transport_count", 0f));
							
						} else// 套装商品
						{
							para.add("transport_union_count", transport_detail.get("transport_count", 0f));
						}
						para.add("transport_send_price", sumPrice);// 记录出库金额
						// 记录转运的类型数量
						floorTransportMgrZJ.modTransportDetail(transport_detail.get("transport_detail_id", 0l), para);
					}
					
				}
				
				// 扣除库存
				
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(transport_id);
				deInProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_TRANSPORT);
				
				for (int q = 0; q < list.size(); q++) {
					floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,
							transport.get("send_psid", 0l), list.get(q).get("pc_id", 0l),
							list.get(q).get("transport_count", 0f));
				}
			}
		} catch (TransportLackException e) {
			throw e;
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	public void packingDiffTransport2(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailDiffByTransportId(transport_id);
			
			// 复杂的验证是否缺货
			DBRow[] lacking = this.checkStorageDiff(transport_id);
			if (lacking.length > 0) {
				throw new TransportLackException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; i < transportDetails.length; i++) {
				float store_count = floorProductMgr.getDetailProductProductStorageByPcid(
						transport.get("send_psid", 0l), transportDetails[i].get("transport_pc_id", 0l)).get(
						"store_count", 0f);
				float difference = store_count - transportDetails[i].get("transport_send_count", 0f);// 转运量与实际库存的差额，大于0说明套装足够，小于0需要单独扣除散件库存，散件商品绝对差额>=0
				double sumPrice = 0;
				double sumPrice1 = 0;
				double sumPrice2 = 0;
				if (difference < 0) {
					if (store_count != 0)// 套装没库存
					{
						sumPrice1 = getStockOutPrice(transport.get("send_psid", 0l),
								transportDetails[i].get("transport_pc_id", 0l), 0);
						
						// list.add(transport_detail_storage);//实际扣的套装
						
						// 转运扣掉的套装数
						DBRow para = new DBRow();
						para.add("transport_union_count", store_count);
						floorTransportMgrZJ
								.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
					}
					
					// 转运的套装拼装数
					DBRow para = new DBRow();
					sumPrice = (sumPrice1 + sumPrice2) / transportDetails[i].get("transport_count", 0f);
					para.add("transport_normal_count", difference * -1);
					para.add("transport_send_price", sumPrice);// 记录出库金额
					floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
					
					// 系统预拼装
					productMgr.combinationProductSystem(transport.get("send_psid", 0l),
							transportDetails[i].get("transport_pc_id", 0l),
							(int) transportDetails[i].get("transport_count", 0f), ProductStoreBillKey.TRANSPORT_ORDER,
							transport_id, adminLoggerBean, 0);
					
					DBRow transport_detail_storage = new DBRow();// 实际扣库存的DBRow
					transport_detail_storage.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
					transport_detail_storage.add("transport_count", transportDetails[i].get("transport_count", 0f));
					
					list.add(transport_detail_storage);
				} else {
					DBRow transport_detail = transportDetails[i];
					transport_detail.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
					// list.add(transport_detail);
					
					sumPrice = getStockOutPrice(transport.get("send_psid", 0l),
							transportDetails[i].get("transport_pc_id", 0l), 0);
					DBRow product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get("transport_pc_id",
							0l));
					
					DBRow para = new DBRow();
					if (product.get("union_flag", 0) == 0)// 转运散件商品
					{
						
						para.add("transport_normal_count", transport_detail.get("transport_send_count", 0f));
						
					} else// 套装商品
					{
						para.add("transport_union_count", transport_detail.get("transport_send_count", 0f));
					}
					para.add("transport_send_price", sumPrice);// 记录出库金额
					// 记录转运的类型数量
					floorTransportMgrZJ.modTransportDetail(transport_detail.get("transport_detail_id", 0l), para);
					
					DBRow transport_detail_storage = new DBRow();// 实际扣库存的DBRow
					transport_detail_storage.add("pc_id", transportDetails[i].get("transport_pc_id", 0l));
					transport_detail_storage.add("transport_count", transport_detail.get("transport_send_count", 0f));
					list.add(transport_detail_storage);// 实际扣的套装
				}
				
			}
			
			// 扣除库存
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(transport_id);
			deInProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_TRANSPORT);
			
			for (int q = 0; q < list.size(); q++) {
				floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean, transport.get("send_psid", 0l),
						list.get(q).get("pc_id", 0l), list.get(q).get("transport_count", 0f));
			}
		} catch (TransportLackException e) {
			throw e;
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	private double getStockOutPrice(long cid, long pc_id, int union_flag) throws Exception {
		double sumPrice = 0;
		if (union_flag == 1) {
			DBRow[] set_product = floorProductMgr.getProductUnionsBySetPid(pc_id);
			for (int i = 0; i < set_product.length; i++) {
				DBRow product = floorProductMgr.getDetailProductByPcid(set_product[i].get("pid", 0l));
				sumPrice += product.get("unit_price", 0d);
			}
		} else {
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			sumPrice = product.get("unit_price", 0d);
		}
		return sumPrice;
	}
	
	/**
	 * 检查库存，返回缺货信息
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkStorage(long transport_id) throws Exception {
		try {
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			
			if (!(transportDetails.length > 0)) {
				throw new NoExistTransportDetailException();
			}
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			long ps_id = transport.get("send_psid", 0l);
			DBRow productCount = new DBRow();
			
			ArrayList<DBRow> result = new ArrayList<DBRow>();
			
			boolean orderIsLacking = false;
			for (int j = 0; j < transportDetails.length; j++) {
				long pid = StringUtil.getLong(transportDetails[j].getString("transport_pc_id"));
				DBRow product = floorProductMgr.getDetailProductByPcid(pid);// 每个商品信息，为了获得商品
				
				float quantity = StringUtil.getFloat(transportDetails[j].getString("transport_count"));
				
				if (product.get("union_flag", 0) == 0)// 0散件
				{
					int lacking = 0;
					
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pid);
					
					if (detailPro == null) {
						lacking = 1;
						orderIsLacking = true;
					} else {
						if (detailPro.get("store_count", 0f) < quantity + this.getCumProductQuantity(productCount, pid)) {
							lacking = 1;
							orderIsLacking = true;
						}
					}
					this.addCumProductQuantity(productCount, pid, quantity);
					
					// 对原来的购物车中的item增加一个缺货标记字段
					transportDetails[j].add("lacking", lacking);
				} else if (product.get("union_flag", 0) == 1)// 1套装
				{
					int lacking = 0;
					boolean standardUnionLacking = false;
					boolean manualUnionLacking = false;
					float store_count = 0;
					
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pid);
					
					if (detailPro == null) {
						lacking = 1;
						standardUnionLacking = true;
					} else {
						
						store_count = detailPro.get("store_count", 0f);
						
						if (store_count < quantity) {
							lacking = 1;
							standardUnionLacking = true;
						}
					}
					// 标准套装有货
					if (lacking == 0) {
						transportDetails[j].add("lacking", lacking);
					} else// 否则，再检查散件
					{
						lacking = 0;
						
						float different = quantity - store_count;
						
						DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(pid);
						for (int jj = 0; jj < productsInSet.length; jj++) {
							// 检查库存
							DBRow detailPro2 = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,
									productsInSet[jj].get("pid", 0l));
							
							if (detailPro2 == null) // 商品还没进库，当然缺货
							{
								lacking = 1;
								manualUnionLacking = true;
							} else {
								
								if (detailPro2.get("store_count", 0f) < productsInSet[jj].get("quantity", 0f)
										* (different)
										+ this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l))) {
									lacking = 1;
									manualUnionLacking = true;
								}
							}
							
							this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l),
									productsInSet[jj].get("quantity", 0f) * different);
						}
						
						if (standardUnionLacking && manualUnionLacking) {
							orderIsLacking = true;
						}
						transportDetails[j].add("lacking", lacking);
					}
					
				}
				
				if (transportDetails[j].get("lacking", 0) == 1)// 缺货加入缺货列表
				{
					result.add(transportDetails[j]);
				}
				
			}
			
			return result.toArray(new DBRow[0]);
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	public DBRow[] checkStorageDiff(long transport_id) throws Exception {
		try {
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailDiffByTransportId(transport_id);
			
			if (!(transportDetails.length > 0)) {
				return new DBRow[0];
			}
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			long ps_id = transport.get("send_psid", 0l);
			DBRow productCount = new DBRow();
			
			ArrayList<DBRow> result = new ArrayList<DBRow>();
			
			boolean orderIsLacking = false;
			for (int j = 0; j < transportDetails.length; j++) {
				long pid = StringUtil.getLong(transportDetails[j].getString("transport_pc_id"));
				DBRow product = floorProductMgr.getDetailProductByPcid(pid);// 每个商品信息，为了获得商品
				
				float quantity = StringUtil.getFloat(transportDetails[j].getString("transport_count"));
				
				if (product.get("union_flag", 0) == 0)// 0散件
				{
					int lacking = 0;
					
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pid);
					
					if (detailPro == null) {
						lacking = 1;
						orderIsLacking = true;
					} else {
						if (detailPro.get("store_count", 0f) < quantity + this.getCumProductQuantity(productCount, pid)) {
							lacking = 1;
							orderIsLacking = true;
						}
					}
					this.addCumProductQuantity(productCount, pid, quantity);
					
					// 对原来的购物车中的item增加一个缺货标记字段
					transportDetails[j].add("lacking", lacking);
				} else if (product.get("union_flag", 0) == 1)// 1套装
				{
					int lacking = 0;
					boolean standardUnionLacking = false;
					boolean manualUnionLacking = false;
					float store_count = 0;
					
					DBRow detailPro = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pid);
					
					if (detailPro == null) {
						lacking = 1;
						standardUnionLacking = true;
					} else {
						
						store_count = detailPro.get("store_count", 0f);
						
						if (store_count < quantity) {
							lacking = 1;
							standardUnionLacking = true;
						}
					}
					// 标准套装有货
					if (lacking == 0) {
						transportDetails[j].add("lacking", lacking);
					} else// 否则，再检查散件
					{
						lacking = 0;
						
						float different = quantity - store_count;
						
						DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(pid);
						for (int jj = 0; jj < productsInSet.length; jj++) {
							// 检查库存
							DBRow detailPro2 = floorProductMgr.getDetailProductProductStorageByPcid(ps_id,
									productsInSet[jj].get("pid", 0l));
							
							if (detailPro2 == null) // 商品还没进库，当然缺货
							{
								lacking = 1;
								manualUnionLacking = true;
							} else {
								
								if (detailPro2.get("store_count", 0f) < productsInSet[jj].get("quantity", 0f)
										* (different)
										+ this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l))) {
									lacking = 1;
									manualUnionLacking = true;
								}
							}
							
							this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l),
									productsInSet[jj].get("quantity", 0f) * different);
						}
						
						if (standardUnionLacking && manualUnionLacking) {
							orderIsLacking = true;
						}
						transportDetails[j].add("lacking", lacking);
					}
					
				}
				
				if (transportDetails[j].get("lacking", 0) == 1)// 缺货加入缺货列表
				{
					result.add(transportDetails[j]);
				}
				
			}
			return result.toArray(new DBRow[0]);
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 转运单起运
	 * 
	 * @param transport_id
	 * @throws Exception
	 */
	public void deliveryTransport(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		try {
			long receive_id = floorTransportMgrZJ.getDetailTransportById(transport_id).get("receive_psid", 0l);
			
			// DBRow[] transportbounds = floorTransportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
			//
			// //记录转运单实际发货量
			// for(int i = 0;i<transportbounds.length;i++)
			// {
			// long pc_id = transportbounds[i].get("to_pc_id",0l);
			//
			// DBRow para = new DBRow();
			// para.add("transport_send_count",transportbounds[i].get("to_count",0f));//发货数量
			//
			// if(floorTransportMgrZJ.transportDetailDelivery(transport_id, pc_id, para)==0)//没有记录就插入新记录
			// {
			// DBRow transportDetail = new DBRow();
			// transportDetail.add("transport_p_name",transportbounds[i].getString("to_p_name"));
			// transportDetail.add("transport_delivery_count",0);
			// transportDetail.add("transport_count",0);
			// transportDetail.add("transport_send_count",transportbounds[i].get("to_count",0f));
			// transportDetail.add("transport_id",transport_id);
			// transportDetail.add("transport_box","");
			// transportDetail.add("transport_pc_id",transportbounds[i].getString("to_pc_id"));
			// transportDetail.add("transport_p_code",transportbounds[i].getString("to_p_code"));
			//
			// floorTransportMgrZJ.addTransportDetail(transportDetail);
			// }
			// }
			
			// batchMgrLL.batchTransportStockOut(transport_id, adminLoggerBean);
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			if (transport.get("purchase_id", 0l) != 0) {
				DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null,
						null, null, null);
				for (int i = 0; i < transportDetails.length; i++) {
					serialNumberMgrZJ.addSerialProduct(transportDetails[i].get("transport_pc_id", 0l),
							transportDetails[i].getString("transport_product_serial_number"), 0l);
				}
			}
			
			// 修改转运单状态
			DBRow para = new DBRow();
			para.add("transport_status", TransportOrderKey.INTRANSIT);
			floorTransportMgrZJ.modTransport(transport_id, para);
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "启运:启运完成", adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods, TransportOrderKey.INTRANSIT);
			((TransportMgrIFaceZJ) AopContext.currentProxy()).deliveryTransportForJbpm(transport_id, receive_id);
			// 移到转运发货审核创建改
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据转运仓库、接收仓库、转运单状态过滤转运单明细
	 * 
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetailByPsWithStatus(long send_psid, long receive_psid, int status) throws Exception {
		try {
			return floorTransportMgrZJ.getTransportDetailByPsWithStatus(send_psid, receive_psid, status);
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 转运入库提交
	 * 
	 * @param transport_id
	 * @param transportDetailsWarehouse
	 * @param adminLoggerBean
	 * @param machine_id
	 * @throws Exception
	 */
	public void transportWarehousingSub(long transport_id, DBRow[] transportDetailsWarehouse,
			AdminLoginBean adminLoggerBean, String machine_id) throws Exception {
		DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);// 获得转运单
		DBRow transportPara = new DBRow();
		
		for (int i = 0; i < transportDetailsWarehouse.length; i++) {
			float count = Float.valueOf(transportDetailsWarehouse[i].getString("tw_count"));// 获得交货数量
			if (count > 0f) {
				DBRow transportDetail = floorTransportMgrZJ.getTransportDetailByTPId(transport_id,
						transportDetailsWarehouse[i].get("tw_product_id", 0l));// 获得转运详细
				
				if (transportDetail != null) {
					float reap_count = transportDetail.get("transport_reap_count", 0f);
					reap_count = reap_count + count;// 到货数量累加
					
					transportDetail.add("transport_reap_count", reap_count);
					floorTransportMgrZJ.modTransportDetail(transportDetail.get("transport_detail_id", 0l),
							transportDetail);// 修改到货量
				} else {
					DBRow noInTransportOrderproduct = floorProductMgr
							.getDetailProductByPcid(transportDetailsWarehouse[i].get("tw_product_id", 0l));
					if (noInTransportOrderproduct == null) {
						throw new ProductNotExistException();
					}
					
					transportDetail = new DBRow();
					
					transportDetail.add("transport_pc_id", noInTransportOrderproduct.get("pc_id", 0l));
					transportDetail.add("transport_p_name", noInTransportOrderproduct.getString("p_name"));
					transportDetail.add("transport_p_code", noInTransportOrderproduct.getString("p_code"));
					transportDetail.add("transport_count", 0);// 原本预计转运量
					transportDetail.add("transport_send_count", 0);// 实际转运量
					transportDetail.add("transport_delivery_count", 0);
					transportDetail.add("transport_reap_count", count);
					transportDetail.add("transport_id", transport_id);
					
					floorTransportMgrZJ.addTransportDetail(transportDetail);
				}
				
				// 商品入库
			}
			
		}
		// 检查转运单是否已完成
		DBRow[] checkApprove = floorTransportMgrZJ.checkTransportDetailApprove(transport_id);
		if (checkApprove != null && checkApprove.length > 0) {
			transportApproveMgrZJ.addTransportApproveSub(transport_id, adminLoggerBean);
			
			transportPara.add("transport_status", TransportOrderKey.APPROVEING);
		} else {
			TDate tDate = new TDate();
			TDate endDate = new TDate(transport.getString("transport_date") + " 00:00:00");
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			transportPara.add("all_over", diffDay);
			transportPara.add("transport_status", TransportOrderKey.FINISH);
			
			// 默认为转运单入库
			int product_store_operation = ProductStoreOperationKey.IN_STORE_TRANSPORT;
			int bill_type = ProductStoreBillKey.TRANSPORT_ORDER;
			
			long purchase_id = transport.get("purchase_id", 0l);
			if (purchase_id != 0)// 如果是采购交货转运，库存操作日志记录为交货单入库
			{
				product_store_operation = ProductStoreOperationKey.IN_STORE_DELIVERY;
				bill_type = ProductStoreBillKey.DELIVERY_ORDER;
			}
			
			// 转运单确认入库(加库存)
			// transportApproveMgrZJ.stockIn(transport_id, adminLoggerBean,bill_type,product_store_operation);
		}
		
		floorTransportMgrZJ.modTransport(transport_id, transportPara);// 修改转运状态
		
		// 必须在单据入库更改过单据状态后进行运费计算(屏蔽)
		this.updateTransportShippingFeeToProductStoreLogDetail(transport_id);// 更新批次运费
		
		TransportOrderKey transportOrderKey = new TransportOrderKey();
		floorTransportMgrLL.insertLogs(
				Long.toString(transport_id),
				"转运单" + transport_id + "入库:"
						+ transportOrderKey.getTransportOrderStatusById(transportPara.get("transport_status", 0)),
				adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods,
				transportPara.get("transport_status", 0));
		// 处理签到信息
		doorOrLocationOccupancyMgrZyj.updateDoorOrLocationStatusAndTime(transport_id,
				transportPara.get("transport_status", 0));
		scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, 4, "转运单:" + transport_id
				+ "入库", true, adminLoggerBean, "transport_intransit_period", "true");
	}
	
	/**
	 * 为方便入库时调用
	 * 
	 * @param transport_id
	 * @throws Exception
	 */
	public void updateTransportShippingFeeToProductStoreLogDetail(long transport_id) throws Exception {
		freightMgrLL.updateTransportShippingFeeToProductStoreLogDetail(transport_id);// 更新批次运费
	}
	
	/**
	 * 累计商品需要的数量 累加一个订单某个商品需要的累计数量
	 * 
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @return
	 */
	private float getCumProductQuantity(DBRow productCount, long pid) throws Exception {
		if (productCount.getString(String.valueOf(pid)).equals("")) {
			return (0);
		} else {
			return (StringUtil.getFloat(productCount.getString(String.valueOf(pid))));
		}
	}
	
	/**
	 * 累计订单某个商品需要的数量
	 * 
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addCumProductQuantity(DBRow productCount, long pid, float quantity) throws Exception {
		if (productCount.getString(String.valueOf(pid)).equals("")) {
			productCount.add(String.valueOf(pid), quantity);
		} else {
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}
	
	/**
	 * 下载转运单
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downloadTransportOrder(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			if (transport == null) {
				throw new NoExistTransportOrderException();
			}
			if (transportDetails == null) {
				throw new NoExistTransportDetailException();
			}
			
			POIFSFileSystem fs = null;// 获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
					+ "/administrator/transport/transport_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 创建一个居中格式
			style.setLocked(false);// 设置不锁定
			style.setWrapText(true);
			
			for (int i = 0; i < transportDetails.length; i++) {
				DBRow transportDetail = transportDetails[i];
				long pc_id = transportDetail.get("transport_pc_id", 0l);
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(transportDetail.getString("transport_delivery_count"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(transportDetail.getString("transport_backup_count"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(transportDetail.getString("transport_product_serial_number", "NULL"));
				row.getCell(3).setCellStyle(style);
				
				row.createCell(4).setCellValue(transportDetail.getString("transport_box"));
				row.getCell(4).setCellStyle(style);
				
				row.createCell(5).setCellValue(transportDetail.getString("lot_number"));
				row.getCell(5).setCellStyle(style);
			}
			String path = "upl_excel_tmp/T" + transport_id + ".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome() + path);
			wb.write(fout);
			fout.close();
			return (path);
		} catch (NoExistTransportOrderException e) {
			throw e;
		} catch (NoExistTransportDetailException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 工作流拦截转运已装箱发货
	 * 
	 * @param transport_id
	 * @throws Exception
	 */
	public String deliveryTransportForJbpm(long transport_id, long receive_id) throws Exception {
		return (transport_id + "," + receive_id);
	}
	
	/**
	 * 终止装箱
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void rebackTransport(HttpServletRequest request) throws Exception {
		long transport_id = StringUtil.getLong(request, "transport_id");
		DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
		
		DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null, null,
				null);
		
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		// 循环退回库存
		for (int i = 0; i < transportDetails.length; i++) {
			// //商品入库
			//
			// ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			// inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			// inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			// inProductStoreLogBean.setOid(transport.get("transport_id", 0l));
			// inProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			//
			// inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_TRANSPORT_RE);//转运中止
			//
			// floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,transport.get("send_psid",0l),transportDetails[i].get("transport_pc_id",0l),transportDetails[i].get("transport_count",0f),0l);//库存添加
			//
			// DBRow[] unionId =
			// floorProductMgr.getProductUnionsBySetPid(transportDetails[i].get("transport_pc_id",0l));
			//
			// //套装转换为散件,将预拼装商品再预拆散
			// if(transportDetails[i].get("transport_normal_count",0f)>0&&(unionId!=null&&unionId.length>0))
			// {
			// productMgr.splitProductSystem(transport.get("send_psid",0l),transportDetails[i].get("transport_pc_id",0l),(int)transportDetails[i].get("transport_normal_count",0f),ProductStoreBillKey.TRANSPORT_ORDER,transport_id,
			// adminLoggerBean);
			// }
			
			DBRow detailpara = new DBRow();
			detailpara.add("transport_send_count", 0f);
			detailpara.add("transport_union_count", 0f);
			detailpara.add("transport_normal_count", 0f);
			
			floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), detailpara);
		}
		// 单据回退
		productMgr.reBackAllBill(transport_id, ProductStoreBillKey.TRANSPORT_ORDER, adminLoggerBean);
		// 转运单状态变为准备
		DBRow updatepara = new DBRow();
		updatepara.add("transport_status", TransportOrderKey.READY);
		floorTransportMgrZJ.modTransport(transport_id, updatepara);
		
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "停止装箱:单号" + transport_id,
				adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods,
				TransportOrderKey.READY);
		// 跟进
		scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.Goods,
				"转运单:T" + transport_id + "停止装箱", false, request, "transport_intransit_period");
	}
	
	// 差异退货
	public void rebackDiffTransport(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		DBRow transport = getDetailTransportById(transport_id);
		
		DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailDiffByTransportId(transport_id);
		
		// 循环退回库存
		for (int i = 0; i < transportDetails.length; i++) {
			// 商品入库
			
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(transport.get("transport_id", 0l));
			inProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_TRANSPORT_RE);// 转运中止
			
			floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean, transport.get("send_psid", 0l),
					transportDetails[i].get("transport_pc_id", 0l), transportDetails[i].get("transport_count", 0f), 0l);// 库存添加
			
			DBRow[] unionId = floorProductMgr.getProductUnionsBySetPid(transportDetails[i].get("transport_pc_id", 0l));
			
			// 套装转换为散件,将预拼装商品再预拆散
			if (transportDetails[i].get("transport_normal_count", 0f) > 0 && (unionId != null && unionId.length > 0)) {
				productMgr.splitProductSystem(transport.get("send_psid", 0l),
						transportDetails[i].get("transport_pc_id", 0l),
						(int) transportDetails[i].get("transport_normal_count", 0f),
						ProductStoreBillKey.TRANSPORT_ORDER, transport_id, adminLoggerBean);
			}
			
			DBRow detailpara = new DBRow();
			detailpara.add("transport_union_count", 0f);
			detailpara.add("transport_normal_count", 0f);
			
			floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), detailpara);
		}
	}
	
	/**
	 * 运输中的转运单中止运输
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void reStorageTransport(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			// 运输中的停止运输，将实际装箱量修改为转运预计量，为了准备中取消回退库存
			for (int i = 0; i < transportDetails.length; i++) {
				DBRow para = new DBRow();
				para.add("transport_send_count", 0);
				
				floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
			}
			
			DBRow transportpara = new DBRow();
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			if (transport.get("purchase_id", 0) == 0)// 转运单中止运输后变为装箱中
			{
				transportpara.add("transport_status", TransportOrderKey.PACKING);
			} else// 交货单中止运输后变为准备中
			{
				transportpara.add("transport_status", TransportOrderKey.READY);
			}
			floorTransportMgrZJ.modTransport(transport_id, transportpara);
			// batchMgrLL.batchStockOutDelete(transport_id, 3);
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "终止运输", adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods,
					transportpara.get("transport_status", 0));
			
			scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, 4, "转运单:" + transport_id
					+ "终止运输", false, request, "transport_intransit_period");
		} catch (Exception e) {
			throw new SystemException(e, "reStorageTransport", log);
		}
	}
	
	/**
	 * 获得转运单发货商品重量（商品数量以发货数计算）
	 */
	public float getTransportDetailsSendWeight(long transport_id) throws Exception {
		try {
			DBRow[] transportProducts = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			
			float weight = 0;
			for (int i = 0; i < transportProducts.length; i++) {
				DBRow product = floorProductMgr.getDetailProductByPcid(transportProducts[i].get("transport_pc_id", 0l));
				
				weight += product.get("weight", 0f) * transportProducts[i].get("transport_send_count", 0f);
			}
			
			return weight;
		} catch (Exception e) {
			throw new SystemException(e, "getTransportDetailsSendWeight", log);
		}
	}
	
	/**
	 * 转运单生成运单
	 */
	public void printTransportWayBill(String airWayBillNumber, long ccid, long pro_id, long sc_id, long id,
			String send_name, String send_zip_code, String send_address1, String send_address2, String send_address3,
			String send_linkman_phone, String send_city, String deliver_name, String deliver_zip_code,
			String deliver_address1, String deliver_address2, String deliver_address3, String deliver_linkman_phone,
			String deliver_city, float weight, String shipingName, String hs_code, int pkcount,
			AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow transportPara = new DBRow();
			
			transportPara.add("transport_waybill_number", airWayBillNumber);
			transportPara.add("deliver_ccid", ccid);
			transportPara.add("deliver_pro_id", pro_id);
			transportPara.add("sc_id", sc_id);
			transportPara.add("send_name", send_name);
			transportPara.add("send_zip_code", send_zip_code);
			transportPara.add("send_house_number", send_address1);
			transportPara.add("send_street", send_address2);
			transportPara.add("send_address3", send_address3);
			transportPara.add("send_linkman_phone", send_linkman_phone);
			transportPara.add("send_city", send_city);
			transportPara.add("transport_linkman", deliver_name);
			transportPara.add("deliver_zip_code", deliver_zip_code);
			transportPara.add("deliver_house_number", deliver_address1);
			transportPara.add("deliver_street", deliver_address2);
			transportPara.add("deliver_address3", deliver_address3);
			transportPara.add("transport_linkman_phone", deliver_linkman_phone);
			transportPara.add("deliver_city", deliver_city);
			transportPara.add("weight", weight);
			transportPara.add("transport_waybill_name", shipingName);
			transportPara.add("hs_code", hs_code);
			transportPara.add("pkcount", pkcount);
			
			floorTransportMgrZJ.modTransport(id, transportPara);
			
			floorTransportMgrLL.insertLogs(Long.toString(id), "转运单" + id + "生成快递单" + airWayBillNumber,
					adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Update);
		} catch (Exception e) {
			throw new SystemException(e, "printTransportWayBill", log);
		}
	}
	
	/**
	 * 上传发票文件
	 * 
	 * @param request
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public void uploadTransportInvoice(HttpServletRequest request) throws Exception, FileTypeException, FileException {
		try {
			String permitFile = "xls,doc,xlsx,docx";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long transport_id = Long.parseLong(upload.getRequestRow().getString("transport_id"));
			
			if (flag == 2) {
				throw new FileTypeException();// 文件上传格式不对
			} else if (flag == 1) {
				throw new FileException();
			} else {
				String[] temp = upload.getFileName().split("\\.");
				String fileType = temp[temp.length - 1];
				
				String fileName = "T" + transport_id + "." + fileType;
				String temp_url = Environment.getHome() + "upl_excel_tmp/" + upload.getFileName();
				String invoice_path = "invoice_file/" + fileName;
				String url = Environment.getHome() + invoice_path;
				FileUtil.moveFile(temp_url, url);
				
				DBRow para = new DBRow();
				para.add("invoice_path", invoice_path);
				
				floorTransportMgrZJ.modTransport(transport_id, para);
				
			}
		} catch (FileTypeException e) {
			throw e;
		} catch (FileException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "uploadTransportInvoice", log);
		}
	}
	
	public void updateCreateIdAndPackingId() throws Exception {
		try {
			DBRow[] rows = floorTransportMgrZJ.fillterTransport(0, 0, null, 0, 0, 0, 0, 0, 0, 0, 0);
			
			for (int i = 0; i < rows.length; i++) {
				long create_account_id = 0;
				DBRow[] createAdmin = floorAdminMgr.getDetailAdminByAccountOrEmployeName(rows[i]
						.getString("create_account"));
				if (createAdmin.length > 0) {
					create_account_id = createAdmin[0].get("adid", 0l);
				}
				
				long packing_account = 0;
				DBRow[] packingAdmin = floorAdminMgr.getDetailAdminByAccountOrEmployeName(rows[i]
						.getString("packing_account"));
				if (packingAdmin.length > 0) {
					packing_account = packingAdmin[0].get("adid", 0l);
				}
				
				DBRow para = new DBRow();
				para.add("create_account_id", create_account_id);
				para.add("packing_account", packing_account);
				
				floorTransportMgrZJ.modTransport(rows[i].get("transport_id", 0l), para);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(e, "updateCreateIdAndPackingId", log);
		}
	}
	
	public void transportPsType() throws Exception {
		try {
			DBRow[] rows = floorTransportMgrZJ.fillterTransport(0, 0, null, 0, 0, 0, 0, 0, 0, 0, 0);
			
			for (int i = 0; i < rows.length; i++) {
				if (rows[i].get("purchase_id", 0l) == 0)// 转运单的
				{
					long send_psid = rows[i].get("send_psid", 0l);
					long receive_psid = rows[i].get("receive_psid", 0l);
					
					DBRow sendProductStorageCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
					DBRow receiveProductStorageCatalog = floorCatalogMgr
							.getDetailProductStorageCatalogById(receive_psid);
					
					DBRow para = new DBRow();
					para.add("from_ps_type", sendProductStorageCatalog.get("storage_type", 0));
					para.add("target_ps_type", receiveProductStorageCatalog.get("storage_type", 0));
					
					floorTransportMgrZJ.modTransport(rows[i].get("transport_id", 0l), para);
				} else// 交货单的
				{
					long receive_psid = rows[i].get("receive_psid", 0l);
					
					DBRow receiveProductStorageCatalog = floorCatalogMgr
							.getDetailProductStorageCatalogById(receive_psid);
					DBRow para = new DBRow();
					para.add("from_ps_type", ProductStorageTypeKey.SupplierWarehouse);
					para.add("target_ps_type", receiveProductStorageCatalog.get("storage_type", 0));
					
					floorTransportMgrZJ.modTransport(rows[i].get("transport_id", 0l), para);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(e, "transportPsType", log);
		}
	}
	
	/**
	 * 交货单转换为转运单
	 * 
	 * @throws Exception
	 */
	public void deliveryChangeToTransport() throws Exception {
		try {
			DBRow[] allDeliveryOrders = floorDeliveryMgrZJ.getAllDeliveryOrder(null);
			
			for (int i = 0; i < allDeliveryOrders.length; i++) {
				// 将交货单转为转运单
				DBRow deliveryOrder = allDeliveryOrders[i];
				long delivery_order_id = deliveryOrder.get("delivery_order_id", 0l);
				
				String transport_number = deliveryOrder.getString("delivery_order_number", null);
				long purchase_id = deliveryOrder.get("delivery_purchase_id", 0l);
				String transport_waybill_number = deliveryOrder.getString("waybill_number");
				long send_psid = deliveryOrder.get("delivery_supplier_id", 0l);
				String transport_waybill_name = deliveryOrder.getString("waybill_name");
				int transportby = deliveryOrder.get("transportby", 0);
				String carriers = deliveryOrder.getString("carriers");
				long transport_send_country = deliveryOrder.get("begin_country", 0l);
				long transport_receive_country = deliveryOrder.get("end_country", 0l);
				String transport_send_place = deliveryOrder.getString("begin_port", null);
				String transport_receive_place = deliveryOrder.getString("end_port", null);
				String transport_receive_date = deliveryOrder.getString("arrival_eta", null);
				
				int delivery_order_status = deliveryOrder.get("delivery_order_status", 0);
				
				int transport_status = 0;
				
				switch (delivery_order_status) {
				case 1:// DeliveryOrderKey.READY:
					transport_status = TransportOrderKey.READY;
					break;
				
				case 2:// DeliveryOrderKey.INTRANSIT:
					transport_status = TransportOrderKey.INTRANSIT;
					break;
				
				case 3:// DeliveryOrderKey.FINISH:
					transport_status = TransportOrderKey.FINISH;
					break;
				
				case 4:// DeliveryOrderKey.APPROVEING:
					transport_status = TransportOrderKey.APPROVEING;
					break;
				
				case 5:// DeliveryOrderKey.NOFINISH:
					transport_status = TransportOrderKey.NOFINISH;
					break;
				}
				
				String transport_address = deliveryOrder.getString("delivery_address");
				String transport_linkman = deliveryOrder.getString("delivery_linkman");
				String transport_linkman_phone = deliveryOrder.getString("delivery_linkman_phone");
				String transport_date = deliveryOrder.getString("delivery_date");
				
				long create_account_id = deliveryOrder.get("delivery_create_account_id", 0l);
				String create_account = deliveryOrder.getString("delivery_create_account");
				int declaration = deliveryOrder.get("declaration", 0);
				int clearance = deliveryOrder.get("clearance", 0);
				int drawback = deliveryOrder.get("drawback", 0);
				int invoice = deliveryOrder.get("invoice", 0);
				String remark = deliveryOrder.getString("remark");
				String updatedate = deliveryOrder.getString("updatedate", null);
				int updateby = deliveryOrder.get("updateby", 0);
				String updatename = deliveryOrder.getString("updatename", null);
				double declaration_over = deliveryOrder.get("declaration_over", 0d);
				double clearance_over = deliveryOrder.get("clearance_over", 0d);
				double drawback_over = deliveryOrder.get("drawback_over", 0d);
				double invoice_over = deliveryOrder.get("invoice_over", 0d);
				double all_over = deliveryOrder.get("all_over", 0d);
				long fr_id = deliveryOrder.get("fr_id", 0l);
				int stock_in_set = deliveryOrder.get("stock_in_set", 0);
				String send_zip_code = deliveryOrder.getString("send_zip_code", null);
				long sc_id = deliveryOrder.get("sc_id", 0l);
				long deliver_ccid = deliveryOrder.get("ccid", 0l);
				long deliver_pro_id = deliveryOrder.get("pro_id", 0l);
				String invoice_path = deliveryOrder.getString("invoice_path", null);
				String send_house_number = deliveryOrder.getString("send_address1", null);
				
				String send_street = deliveryOrder.getString("send_address2", null);
				String send_address3 = deliveryOrder.getString("send_address3", null);
				String deliver_house_number = deliveryOrder.getString("deliver_address1", null);
				String deliver_street = deliveryOrder.getString("deliver_address2", null);
				String deliver_address3 = deliveryOrder.getString("deliver_address3", null);
				String send_name = deliveryOrder.getString("send_name", null);
				String send_linkman_phone = deliveryOrder.getString("send_linkman_phone", null);
				String deliver_zip_code = deliveryOrder.getString("deliver_zip_code", null);
				String send_city = deliveryOrder.getString("send_city", null);
				String deliver_city = deliveryOrder.getString("deliver_city", null);
				float weight = deliveryOrder.get("weight", 0f);
				String hs_code = deliveryOrder.getString("hs_code", null);
				int pkcount = deliveryOrder.get("pkcount", 0);
				long receive_psid = deliveryOrder.get("delivery_psid", 0l);
				
				DBRow transport = new DBRow();
				
				transport.add("transport_number", transport_number);
				transport.add("purchase_id", purchase_id);
				transport.add("transport_waybill_number", transport_waybill_number);
				transport.add("send_psid", send_psid);
				transport.add("from_ps_type", ProductStorageTypeKey.SupplierWarehouse);
				transport.add("transport_waybill_name", transport_waybill_name);
				transport.add("transportby", transportby);
				transport.add("carriers", carriers);
				transport.add("transport_send_country", transport_send_country);
				transport.add("transport_receive_country", transport_receive_country);
				transport.add("transport_send_place", transport_send_place);
				transport.add("transport_receive_place", transport_receive_place);
				transport.add("transport_receive_date", transport_receive_date);
				transport.add("transport_status", transport_status);
				transport.add("transport_address", transport_address);
				transport.add("transport_linkman", transport_linkman);
				transport.add("transport_linkman_phone", transport_linkman_phone);
				transport.add("transport_date", transport_date);
				transport.add("create_account_id", create_account_id);
				transport.add("create_account", create_account);
				transport.add("declaration", declaration);
				transport.add("clearance", clearance);
				transport.add("drawback", drawback);
				transport.add("invoice", invoice);
				transport.add("remark", remark);
				transport.add("updatedate", updatedate);
				transport.add("updateby", updateby);
				transport.add("updatename", updatename);
				transport.add("declaration_over", declaration_over);
				transport.add("clearance_over", clearance_over);
				transport.add("drawback_over", drawback_over);
				transport.add("invoice_over", invoice_over);
				transport.add("all_over", all_over);
				transport.add("fr_id", fr_id);
				transport.add("stock_in_set", stock_in_set);
				transport.add("send_zip_code", send_zip_code);
				transport.add("sc_id", sc_id);
				transport.add("deliver_ccid", deliver_ccid);
				transport.add("deliver_pro_id", deliver_pro_id);
				transport.add("invoice_path", invoice_path);
				transport.add("send_house_number", send_house_number);
				
				transport.add("send_street", send_street);
				transport.add("send_address3", send_address3);
				transport.add("deliver_house_number", deliver_house_number);
				transport.add("deliver_street", deliver_street);
				transport.add("deliver_address3", deliver_address3);
				transport.add("send_name", send_name);
				transport.add("send_linkman_phone", send_linkman_phone);
				transport.add("deliver_zip_code", deliver_zip_code);
				transport.add("send_city", send_city);
				transport.add("deliver_city", deliver_city);
				transport.add("weight", weight);
				transport.add("hs_code", hs_code);
				transport.add("pkcount", pkcount);
				transport.add("receive_psid", receive_psid);
				
				// 添加入转运单
				long transport_id = floorTransportMgrZJ.addTransport(transport);
				////system.out.println("Delivery:" + delivery_order_id + "change to Transport:" + transport_id);
				
				/**
				 * 将交货单明细导入转运单明细
				 */
				DBRow[] deliveryDetails = floorDeliveryMgrZJ.getDeliverOrderDetailsByDeliveryId(delivery_order_id);
				
				for (int j = 0; j < deliveryDetails.length; j++) {
					DBRow deliveryOrderDetail = deliveryDetails[j];
					
					long delivery_order_detial_id = deliveryOrderDetail.get("delivery_order_detail_id", 0l);
					long transport_pc_id = deliveryOrderDetail.get("product_id", 0l);
					String transport_p_name = deliveryOrderDetail.getString("product_name");
					String transport_p_code = deliveryOrderDetail.getString("product_barcode");
					float transport_count = deliveryOrderDetail.get("delivery_count", 0f);
					float transport_send_count = deliveryOrderDetail.get("delivery_count", 0f);
					float transport_reap_count = deliveryOrderDetail.get("delivery_reap_count", 0f);
					String transport_box = deliveryOrderDetail.getString("delivery_box");
					
					DBRow product = floorProductMgr.getDetailProductByPcid(transport_pc_id);
					
					float transport_union_count = 0;
					float transport_normal_count = 0;
					if (product.get("union_flag", 0) == 1) {
						transport_union_count = transport_send_count;
					} else {
						transport_normal_count = transport_send_count;
					}
					
					DBRow transportDetail = new DBRow();
					transportDetail.add("transport_id", transport_id);
					transportDetail.add("transport_pc_id", transport_pc_id);
					transportDetail.add("transport_p_name", transport_p_name);
					transportDetail.add("transport_p_code", transport_p_code);
					transportDetail.add("transport_count", transport_count);
					transportDetail.add("transport_send_count", transport_send_count);
					transportDetail.add("transport_reap_count", transport_reap_count);
					transportDetail.add("transport_box", transport_box);
					transportDetail.add("transport_union_count", transport_union_count);
					transportDetail.add("transport_normal_count", transport_normal_count);
					
					long transport_detail_id = floorTransportMgrZJ.addTransportDetail(transportDetail);
					
					////system.out.println("Delivery:" + delivery_order_id + " DeliveryOrderDetail:"//+ delivery_order_detial_id + " change to TransportDetail:" + transport_detail_id);
				}
				
				/**
				 * 修改交货单对应的资金申请的关联ID
				 */
				DBRow para = new DBRow();
				para.add("association_id", transport_id);
				
				floorApplyMoneyMgrZZZ.updateApplyMoneyByAssociationTypeAndAssociationId(5, transport_id, para);
				
				////system.out.println("Delivery:" + delivery_order_id + " applymoney change ");
			}
			
			////system.out.println("Finish");
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(e, "deliverChangeTransport", log);
		}
	}
	
	/**
	 * 获得采购单内某商品的在途数量
	 * 
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitCountForPurchase(long purchase_id, long pc_id) throws Exception {
		try {
			return (floorTransportMgrZJ.getTransitCountForPurchase(purchase_id, pc_id));
		} catch (Exception e) {
			throw new SystemException(e, "getTransitCountForPurchase", log);
		}
	}
	
	/**
	 * 获得转运的体积
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public float getTransportVolume(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.getTransportVolume(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportVolume", log);
		}
	}
	
	/**
	 * 获得转运单的总重量
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public float getTransportWeight(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.getTransportWeight(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportWeight", log);
		}
	}
	
	/**
	 * 获得所有转运单出库金额
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public double getTransportSendPrice(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.getTransportSendPrice(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportSendPrice", log);
		}
	}
	
	/**
	 * 根据供应商ID获得转运单
	 * 
	 * @param supplier_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportBySupplierId(long supplier_id, PageCtrl pc) throws Exception {
		try {
			return floorTransportMgrZJ.getTransportBySupplierId(supplier_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportBySupplierId", log);
		}
	}
	
	/**
	 * 操作转运单索引
	 * 
	 * @param transport_id 转运单号
	 * @param type 操作类型(add,update)
	 * @throws Exception
	 */
	public void editTransportIndex(long transport_id, String type) throws Exception {
		DBRow oldTransport = floorTransportMgrZJ.getDetailTransportById(transport_id);
		long purchase_id = oldTransport.get("purchase_id", 0);
		
		String sendStorage = "";
		if (purchase_id == 0) {
			DBRow sendProductStorageCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(oldTransport.get(
					"send_psid", 0l));
			sendStorage = sendProductStorageCatalog.getString("title");
		} else {
			DBRow supplier = floorSupplierMgrTJH.getDetailSupplier(oldTransport.get("send_psid", 0l));
			sendStorage = supplier.getString("sup_name");
		}
		
		DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(oldTransport.get(
				"receive_psid", 0l));
		
		if (type.equals("add")) {
			TransportIndexMgr.getInstance().addIndex(transport_id, purchase_id, sendStorage,
					receiveProductStorage.getString("title"), oldTransport.getString("transport_waybill_number"),
					oldTransport.getString("transport_waybill_name"), oldTransport.getString("carriers"));
		} else if (type.equals("update")) {
			TransportIndexMgr.getInstance().updateIndex(transport_id, purchase_id, sendStorage,
					receiveProductStorage.getString("title"), oldTransport.getString("transport_waybill_number"),
					oldTransport.getString("transport_waybill_name"), oldTransport.getString("carriers"));
		} else if (type.equals("del")) {
			TransportIndexMgr.getInstance().deleteIndex(transport_id);
		}
	}
	
	/**
	 * 构成内容
	 * 
	 * @param transport_id
	 * @param declaration
	 * @param declarationPerson
	 * @param clearance
	 * @param clearancePerson
	 * @param productFile
	 * @param productPerson
	 * @param tag
	 * @param tagPerson
	 * @param stockInSet
	 * @param stockPerson
	 * @param certificate
	 * @param certificatePerson
	 * @param quality
	 * @param qualityPerson
	 * @param cata_name_send 提货仓库名称（转运单为仓库名,交货型转运单为供应商名）
	 * @param cata_name_deliver 收货仓库名称
	 * @param eta
	 * @return
	 * @throws Exception
	 */
	public String handleSendContent(long transport_id, long purchase_id, int declaration, String declarationPerson,
			int clearance, String clearancePerson, int productFile, String productPerson, int tag, String tagPerson,
			int stockInSet, String stockPerson, int certificate, String certificatePerson, int quality,
			String qualityPerson, String cata_name_send, String cata_name_deliver, String eta) throws Exception {
		try {
			String rnStr = "";
			
			// 获取提货仓库的名字
			if (0 == purchase_id) {
				rnStr += "提货仓库:" + cata_name_send + ",";
			}
			// 如果是交货单
			else {
				rnStr += "供应商:" + cata_name_send + ",";
			}
			
			// 获取收货仓库的名字
			rnStr += "收货仓库:" + cata_name_deliver + ",";
			// 获取eta
			if (!"".equals(eta)) {
				
				rnStr += "预计到货时间:" + eta + ",";
			}
			
			// 转运货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" + this.getTransportVolume(transport_id);
			
			// if(DeclarationKey.NODELARATION != declaration){
			// rnStr += "\n[报关流程]"+declarationPerson;
			// }
			// if(ClearanceKey.NOCLEARANCE != clearance){
			// rnStr += "\n[清关流程]"+clearancePerson;
			// }
			// if(TransportProductFileKey.NOPRODUCTFILE != productFile){
			// rnStr += "\n[图片流程]"+productPerson;
			// }
			// if(TransportTagKey.NOTAG != tag){
			// rnStr += "\n[制签流程]"+tagPerson;
			// }
			// if(TransportStockInSetKey.SHIPPINGFEE_NOTSET != stockInSet){
			// rnStr += "\n[运费流程]"+stockPerson;
			// }
			// if(TransportCertificateKey.NOCERTIFICATE != certificate){
			// rnStr += "\n[单证流程]"+certificatePerson;
			// }
			// if(QualityInspectionKey.NO_NEED_QUALITY != quality){
			// rnStr += "\n[质检流程]"+qualityPerson;
			// }
			return rnStr;
		} catch (Exception e) {
			throw new SystemException(e, "handleSendContent", log);
		}
	}
	
	public void updateTransportDetailVW(long transport_id) throws Exception {
		DBRow[] transportDetails;
		
		if (transport_id == 0) {
			transportDetails = floorTransportMgrZJ.getAllTransportDetail();
		} else {
			transportDetails = floorTransportMgrZJ
					.getTransportDetailByTransportId(transport_id, null, null, null, null);
		}
		
		DBRow product = null;
		float product_volume = 0;
		float product_weight = 0;
		for (int i = 0; i < transportDetails.length; i++) {
			
			if (transportDetails[i].get("transport_volume", 0f) == 0f) {
				product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get("transport_pc_id", 0l));
				if (product != null) {
					product_volume = product.get("volume", 0f);
					product_weight = product.get("weight", 0f);
				}
				
				DBRow para = new DBRow();
				para.add("transport_volume", product_volume);
				para.add("transport_weight", product_weight);
				
				floorTransportMgrZJ.modTransportDetail(transportDetails[i].get("transport_detail_id", 0l), para);
			}
		}
	}
	
	/**
	 * 转运交货跟进一级跟进
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow trackDeliveryAndTransportCountFirstMenu() throws Exception {
		int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 转运实物图片周期
		int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 转运质检周期
		int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 转运内部标签周期
		int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 转运第三方标签周期
		
		int need_track_ready_transport_day = systemConfig.getIntConfigValue("transport_ready_period");// 转运单备货跟进周期（天）
		int need_track_packing_transport_day = systemConfig.getIntConfigValue("transport_packing_period");// 转运单装箱跟进周期（天）
		
		int need_track_intransit_day = systemConfig.getIntConfigValue("transport_intransit_period");// 交货转运单运输中跟进周期（天）
		int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("transport_alreadyreceive_period");// 交货转运单已收货跟进周期（时）
		
		int certificate_day = systemConfig.getIntConfigValue("transport_certificate_period"); // 转运单的单证流程周期
		int clearance_day = systemConfig.getIntConfigValue("transport_clearance_period"); // 转运单的进口清关流程周期
		int declaration_day = systemConfig.getIntConfigValue("transport_declaration_period"); // 转运单的出口报送流程周期
		
		int need_track_ready_transprot_count = 0;
		int need_track_packing_transport_count = 0;
		int need_track_tag_transport_count = 0;
		int need_track_third_tag_transport_count = 0;
		int need_track_product_file_transport_count = 0;
		int need_track_quality_inspection_transport_count = 0;
		
		int need_track_intransit_count = 0;
		int need_track_alreadyRecive_transport_count = 0;
		
		DBRow[] track_delivery = this.trackDeliveryCountGroupByProductLine();
		
		int track_delivery_count = 0;
		for (int i = 0; i < track_delivery.length; i++) {
			track_delivery_count += track_delivery[i].get("track_count", 0);
		}
		
		int track_send_count = 0;
		// DBRow[] track_send_transport = this.trackSendTransportCountGroupByPs();
		// for (int i = 0; i < track_send_transport.length; i++)
		// {
		// track_send_count += track_send_transport[i].get("need_track_send_count",0)
		// }
		
		long ps_id = 0;
		DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
		for (int i = 0; i < storageCatalogs.length; i++) {
			ps_id = storageCatalogs[i].get("id", 0l);
			
			need_track_ready_transprot_count += floorTransportMgrZJ.getNeedTrackReadyTransportCountByPsid(ps_id,
					need_track_ready_transport_day);
			need_track_packing_transport_count += floorTransportMgrZJ.getNeedTrackPackingTransportCountByPsid(ps_id,
					need_track_packing_transport_day);
			need_track_tag_transport_count += floorTransportMgrZJ.getNeedTrackTagCountByPS(ps_id, tag_day);
			need_track_third_tag_transport_count += floorTransportMgrZJ.getNeedTrackThirdTagCountByPS(ps_id,
					third_tag_day);
			need_track_product_file_transport_count += floorTransportMgrZJ.getNeedTrackProductFileCountByPS(ps_id,
					product_file_day);
			need_track_quality_inspection_transport_count += floorTransportMgrZJ
					.getNeedTrackQualityInspectionCountByPS(ps_id, quality_inspection_day);
			
			need_track_intransit_count += floorTransportMgrZJ.getNeedTrackIntransitTransportCountByPsid(ps_id,
					need_track_intransit_day);
			need_track_alreadyRecive_transport_count += floorTransportMgrZJ.getNeedTrackAlreadyReciveTransportCount(
					ps_id, need_track_alreadyRecive_transport_hour);
		}
		track_send_count = need_track_ready_transprot_count + need_track_packing_transport_count
				+ need_track_tag_transport_count + need_track_third_tag_transport_count
				+ need_track_product_file_transport_count + need_track_quality_inspection_transport_count;
		int track_recive_count = need_track_intransit_count + need_track_alreadyRecive_transport_count;
		
		// 海运
		int track_certificate = floorTransportMgrZJ.getNeedTrackCertificateCount(certificate_day);
		int track_clearance_count = floorTransportMgrZJ.getNeedTrackClearanceCount(clearance_day);
		int track_declaration_count = floorTransportMgrZJ.getNeedTrackDeclarationCount(declaration_day);
		
		int ocean_shipping_count = track_certificate + track_clearance_count + track_declaration_count;
		
		DBRow result = new DBRow();
		result.add("track_delivery_count", track_delivery_count);
		result.add("track_send_count", track_send_count);
		result.add("track_recive_count", track_recive_count);
		result.add("ocean_shipping_count", ocean_shipping_count);
		
		return result;
	}
	
	/**
	 * 海运需跟进
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackOceanShippingCount() throws Exception {
		int certificate_day = systemConfig.getIntConfigValue("transport_certificate_period"); // 转运单的单证流程周期
		int clearance_day = systemConfig.getIntConfigValue("transport_clearance_period"); // 转运单的进口清关流程周期
		int declaration_day = systemConfig.getIntConfigValue("transport_declaration_period"); // 转运单的出口报送流程周期
		
		int track_certificate_count = floorTransportMgrZJ.getNeedTrackCertificateCount(certificate_day);
		int track_clearance_count = floorTransportMgrZJ.getNeedTrackClearanceCount(clearance_day);
		int track_declaration_count = floorTransportMgrZJ.getNeedTrackDeclarationCount(declaration_day);
		
		DBRow[] result = new DBRow[3];
		
		result[0] = new DBRow();
		result[0].add("track_title", "单证");
		result[0].add("track_count", track_certificate_count);
		result[0].add("cmd", "track_certificate");
		
		result[1] = new DBRow();
		result[1].add("track_title", "清关");
		result[1].add("track_count", track_clearance_count);
		result[1].add("cmd", "track_clearance");
		
		result[2] = new DBRow();
		result[2].add("track_title", "报关");
		result[2].add("track_count", track_declaration_count);
		result[2].add("cmd", "track_declaration");
		
		return result;
	}
	
	/**
	 * 需跟进海运的交货和转运单
	 * 
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackOceanShippingTransport(String cmd, PageCtrl pc) throws Exception {
		
		DBRow[] result = new DBRow[0];
		
		if (cmd.equals("track_certificate")) {
			int certificate_day = systemConfig.getIntConfigValue("transport_certificate_period"); // 转运单的单证流程周期
			result = floorTransportMgrZJ.getNeedTrackCertificateTransport(certificate_day, pc);
		} else if (cmd.equals("track_clearance")) {
			int clearance_day = systemConfig.getIntConfigValue("transport_clearance_period"); // 转运单的进口清关流程周期
			result = floorTransportMgrZJ.getNeedTrackClearanceTransport(clearance_day, pc);
		} else if (cmd.equals("track_declaration")) {
			int declaration_day = systemConfig.getIntConfigValue("transport_declaration_period"); // 转运单的出口报送流程周期
			result = floorTransportMgrZJ.getNeedTrackDeclarationTransport(declaration_day, pc);
		}
		
		return result;
	}
	
	/**
	 * 备货中的交货单按产品线分组
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackDeliveryCountGroupByProductLine() throws Exception {
		try {
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("transport_ready_period"); // 交货单备货跟进周期（天）
			
			int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 交货实物图片周期
			int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 交货质检周期
			int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 交货内部标签周期
			int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 转运第三方标签周期
			
			DBRow[] ready_delivery = floorTransportMgrZJ
					.getNeedTrackReadDeliveryCountGroupByProductLine(need_track_ready_delivery_day);
			DBRow[] track_product_file = floorTransportMgrZJ
					.getNeedTrackProductFileCountGroupByProductLine(product_file_day);
			DBRow[] track_quality_inspection = floorTransportMgrZJ
					.getNeedTrackQualityInspectionCountGroupByProductLine(quality_inspection_day);
			DBRow[] track_tag = floorTransportMgrZJ.getNeedTrackTagCountGroupByProductLine(tag_day);
			DBRow[] third_track_tag = floorTransportMgrZJ.getNeedTrackThirdTagCountGroupByProductLine(third_tag_day);
			
			Map<String, DBRow> map = new HashMap<String, DBRow>();
			// 备货需跟进的交货单
			for (int i = 0; i < ready_delivery.length; i++) {
				String product_line_name = ready_delivery[i].getString("product_line_name");
				long product_line_id = ready_delivery[i].get("product_line_id", 0l);
				int readyneedtrackcountforline = ready_delivery[i].get("readyneedtrackcountforline", 0);
				
				int track_count = 0;
				if (map.containsKey(String.valueOf(product_line_id))) {
					track_count = map.get(String.valueOf(product_line_id)).get("track_count", 0);
				}
				track_count += readyneedtrackcountforline;
				
				DBRow result = new DBRow();
				result.add("product_line_name", product_line_name);
				result.add("product_line_id", product_line_id);
				result.add("track_count", track_count);
				
				map.put(String.valueOf(product_line_id), result);
			}
			// 实物图片需跟进的交货单
			for (int i = 0; i < track_product_file.length; i++) {
				String product_line_name = track_product_file[i].getString("product_line_name");
				long product_line_id = track_product_file[i].get("product_line_id", 0l);
				int need_track_product_file_count = track_product_file[i].get("need_track_product_file_count", 0);
				
				int track_count = 0;
				if (map.containsKey(String.valueOf(product_line_id))) {
					track_count = map.get(String.valueOf(product_line_id)).get("track_count", 0);
				}
				track_count += need_track_product_file_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name", product_line_name);
				result.add("product_line_id", product_line_id);
				result.add("track_count", track_count);
				
				map.put(String.valueOf(product_line_id), result);
			}
			// 质检需跟进的交货单
			for (int i = 0; i < track_quality_inspection.length; i++) {
				String product_line_name = track_quality_inspection[i].getString("product_line_name");
				long product_line_id = track_quality_inspection[i].get("product_line_id", 0l);
				int need_track_quality_inspection_count = track_quality_inspection[i].get(
						"need_track_quality_inspection_count", 0);
				
				int track_count = 0;
				if (map.containsKey(String.valueOf(product_line_id))) {
					track_count = map.get(String.valueOf(product_line_id)).get("track_count", 0);
				}
				track_count += need_track_quality_inspection_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name", product_line_name);
				result.add("product_line_id", product_line_id);
				result.add("track_count", track_count);
				
				map.put(String.valueOf(product_line_id), result);
			}
			// 内部标签需跟进的交货单
			for (int i = 0; i < track_tag.length; i++) {
				String product_line_name = track_tag[i].getString("product_line_name");
				long product_line_id = track_tag[i].get("product_line_id", 0l);
				int need_track_tag_count = track_tag[i].get("need_track_tag_count", 0);
				
				int track_count = 0;
				if (map.containsKey(String.valueOf(product_line_id))) {
					track_count = map.get(String.valueOf(product_line_id)).get("track_count", 0);
				}
				track_count += need_track_tag_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name", product_line_name);
				result.add("product_line_id", product_line_id);
				result.add("track_count", track_count);
				
				map.put(String.valueOf(product_line_id), result);
			}
			// 第三方标签需跟进的交货单
			for (int i = 0; i < third_track_tag.length; i++) {
				String product_line_name = third_track_tag[i].getString("product_line_name");
				long product_line_id = third_track_tag[i].get("product_line_id", 0l);
				int need_track_tag_count = third_track_tag[i].get("need_track_tag_count", 0);
				
				int track_count = 0;
				if (map.containsKey(String.valueOf(product_line_id))) {
					track_count = map.get(String.valueOf(product_line_id)).get("track_count", 0);
				}
				track_count += need_track_tag_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name", product_line_name);
				result.add("product_line_id", product_line_id);
				result.add("track_count", track_count);
				
				map.put(String.valueOf(product_line_id), result);
			}
			
			Set<String> keys = map.keySet();
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (String key : keys) {
				list.add(map.get(key));
			}
			
			return list.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "trackDeliverySecondMenu", log);
		}
	}
	
	/**
	 * 需跟进发货仓库分组
	 */
	public DBRow[] trackSendTransportCountGroupByPs() throws Exception {
		
		int need_track_ready_transport_day = systemConfig.getIntConfigValue("transport_ready_period");// 转运单备货跟进周期（天）
		int need_track_packing_transport_day = systemConfig.getIntConfigValue("transport_packing_period");// 转运单装箱跟进周期（天）
		
		int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 转运实物图片周期
		int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 转运质检周期
		int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 转运内部标签周期
		int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 转运第三方标签周期
		
		long ps_id = 0;
		DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
		
		int need_track_ready_transprot_count;
		int need_track_packing_transport_count;
		int need_track_product_file_transport_count;
		int need_track_quality_inspection_transport_count;
		int need_track_tag_transport_count;
		int need_track_third_tag_transport_count;
		
		int need_track_send_count;
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++) {
			ps_id = storageCatalogs[i].get("id", 0l);
			
			need_track_ready_transprot_count = floorTransportMgrZJ.getNeedTrackReadyTransportCountByPsid(ps_id,
					need_track_ready_transport_day);
			need_track_packing_transport_count = floorTransportMgrZJ.getNeedTrackPackingTransportCountByPsid(ps_id,
					need_track_packing_transport_day);
			need_track_tag_transport_count = floorTransportMgrZJ.getNeedTrackTagCountByPS(ps_id, tag_day);
			need_track_third_tag_transport_count = floorTransportMgrZJ.getNeedTrackTagCountByPS(ps_id, third_tag_day);
			need_track_product_file_transport_count = floorTransportMgrZJ.getNeedTrackProductFileCountByPS(ps_id,
					product_file_day);
			need_track_quality_inspection_transport_count = floorTransportMgrZJ.getNeedTrackQualityInspectionCountByPS(
					ps_id, quality_inspection_day);
			
			need_track_send_count = need_track_ready_transprot_count + need_track_packing_transport_count
					+ need_track_product_file_transport_count + need_track_quality_inspection_transport_count
					+ need_track_tag_transport_count + need_track_third_tag_transport_count;
			if (need_track_send_count > 0) {
				DBRow storeCount = new DBRow();
				storeCount.add("title", storageCatalogs[i].getString("title"));
				storeCount.add("ps_id", ps_id);
				storeCount.add("need_track_send_count", need_track_send_count);
				
				list.add(storeCount);
			}
		}
		
		return list.toArray(new DBRow[0]);
	}
	
	/**
	 * 根据仓库ID获得发货需跟进数
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow trackSendTransportPsid(long ps_id) throws Exception {
		int need_track_ready_transport_day = systemConfig.getIntConfigValue("transport_ready_period");// 转运单备货跟进周期（天）
		int need_track_packing_transport_day = systemConfig.getIntConfigValue("transport_packing_period");// 转运单装箱跟进周期（天）
		int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 转运实物图片周期
		int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 转运质检周期
		int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 转运内部标签周期
		int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 转运第三方标签周期
		
		int need_track_ready_transprot_count = floorTransportMgrZJ.getNeedTrackReadyTransportCountByPsid(ps_id,
				need_track_ready_transport_day);
		int need_track_packing_transport_count = floorTransportMgrZJ.getNeedTrackPackingTransportCountByPsid(ps_id,
				need_track_packing_transport_day);
		int need_track_tag_transport_count = floorTransportMgrZJ.getNeedTrackTagCountByPS(ps_id, tag_day);
		int need_track_third_tag_transport_count = floorTransportMgrZJ.getNeedTrackThirdTagCountByPS(ps_id,
				third_tag_day);
		int need_track_product_file_transport_count = floorTransportMgrZJ.getNeedTrackProductFileCountByPS(ps_id,
				product_file_day);
		int need_track_quality_inspection_transport_count = floorTransportMgrZJ.getNeedTrackQualityInspectionCountByPS(
				ps_id, quality_inspection_day);
		
		DBRow result = new DBRow();
		result.add("need_track_ready_transprot_count", need_track_ready_transprot_count);
		result.add("need_track_packing_transport_count", need_track_packing_transport_count);
		result.add("need_track_tag_transport_count", need_track_tag_transport_count);
		result.add("need_track_third_tag_transport_count", need_track_third_tag_transport_count);
		result.add("need_track_product_file_transport_count", need_track_product_file_transport_count);
		result.add("need_track_quality_inspection_transport_count", need_track_quality_inspection_transport_count);
		result.add("ps_id", ps_id);
		
		return result;
	}
	
	/**
	 * 需跟进收货仓库分组
	 */
	public DBRow[] trackReciveTransportCountGroupByPs() throws Exception {
		int need_track_intransit_day = systemConfig.getIntConfigValue("transport_intransit_period");// 交货转运单运输中跟进周期（天）
		int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("transport_alreadyreceive_period");// 交货转运单已收货跟进周期（时）
		
		int need_track_intransit_count;
		int need_track_alreadyRecive_transport_count;
		int need_track_recive_count;
		long ps_id = 0;
		DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++) {
			ps_id = storageCatalogs[i].get("id", 0l);
			
			need_track_intransit_count = floorTransportMgrZJ.getNeedTrackIntransitTransportCountByPsid(ps_id,
					need_track_intransit_day);
			need_track_alreadyRecive_transport_count = floorTransportMgrZJ.getNeedTrackAlreadyReciveTransportCount(
					ps_id, need_track_alreadyRecive_transport_hour);
			
			need_track_recive_count = need_track_intransit_count + need_track_alreadyRecive_transport_count;
			
			if (need_track_recive_count > 0) {
				DBRow storeCount = new DBRow();
				storeCount.add("title", storageCatalogs[i].getString("title"));
				storeCount.add("ps_id", ps_id);
				storeCount.add("need_track_recive_count", need_track_recive_count);
				
				list.add(storeCount);
			}
		}
		
		return list.toArray(new DBRow[0]);
	}
	
	public DBRow trackReciveTransportPsid(long ps_id) throws Exception {
		int need_track_intransit_day = systemConfig.getIntConfigValue("transport_intransit_period");// 交货转运单运输中跟进周期（天）
		int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("transport_alreadyreceive_period");// 交货转运单已收货跟进周期（时）
		
		int need_track_intransit_count = floorTransportMgrZJ.getNeedTrackIntransitTransportCountByPsid(ps_id,
				need_track_intransit_day);
		int need_track_alreadyRecive_transport_count = floorTransportMgrZJ.getNeedTrackAlreadyReciveTransportCount(
				ps_id, need_track_alreadyRecive_transport_hour);
		
		DBRow result = new DBRow();
		result.add("need_track_intransit_count", need_track_intransit_count);
		result.add("need_track_alreadyrecive_transport_count", need_track_alreadyRecive_transport_count);
		result.add("ps_id", ps_id);
		return result;
	}
	
	/**
	 * 需跟进的交货单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("transport_ready_period");// 转运单备货跟进周期（天）
			
			return floorTransportMgrZJ.getNeedTrackReadyDelivery(product_line_id, need_track_ready_delivery_day, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackReadyDelivery", log);
		}
	}
	
	/**
	 * 根据仓库还有类型过滤需跟进的转运单发货
	 * 
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackSendTransportByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception {
		DBRow[] results = new DBRow[0];
		if (cmd.equals("ready_send_ps")) {
			int need_track_ready_transport_day = systemConfig.getIntConfigValue("transport_ready_period");// 转运单备货跟进周期（天）
			results = floorTransportMgrZJ.getNeedTrackSendTransportByPsid(ps_id, need_track_ready_transport_day,
					TransportOrderKey.READY, pc);
		} else if (cmd.equals("packing_send_ps")) {
			int need_track_packing_transport_day = systemConfig.getIntConfigValue("transport_packing_period");// 转运单装箱跟进周期（天）
			results = floorTransportMgrZJ.getNeedTrackSendTransportByPsid(ps_id, need_track_packing_transport_day,
					TransportOrderKey.PACKING, pc);
		} else if (cmd.equals("tag_send_ps")) {
			int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 转运制签周期
			results = floorTransportMgrZJ.getNeedTrackTagTransportByPs(ps_id, tag_day, pc);
		} else if (cmd.equals("product_file_send_ps")) {
			int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 转运实物图片周期
			results = floorTransportMgrZJ.getNeedTrackProductFileTransport(ps_id, product_file_day, pc);
		} else if (cmd.equals("quality_inspection_send_ps")) {
			int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 转运质检周期
			results = floorTransportMgrZJ.getNeedTrackQualityInspectionTransport(ps_id, quality_inspection_day, pc);
		}
		
		return results;
	}
	
	/**
	 * 根据仓库还有类型过滤需跟进的仓库收货
	 * 
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReceiveTransportByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception {
		DBRow[] results = new DBRow[0];
		if (cmd.equals("intransit_receive_ps")) {
			int need_track_intransit_day = systemConfig.getIntConfigValue("transport_intransit_period");// 交货转运单运输中跟进周期（天）
			
			results = floorTransportMgrZJ.getNeedTrackReceiveTransportByPsid(ps_id, need_track_intransit_day,
					TransportOrderKey.INTRANSIT, pc);
		} else if (cmd.equals("alreadyRecive_receive_ps")) {
			int need_track_alreadyRecive_transport_hour = systemConfig
					.getIntConfigValue("transport_alreadyreceive_period");// 交货转运单已收货跟进周期（时）
			
			results = floorTransportMgrZJ.getNeedTrackReceiveTransportByPsid(ps_id,
					need_track_alreadyRecive_transport_hour, TransportOrderKey.AlREADYARRIAL, pc);
		}
		
		return results;
	}
	
	/**
	 * 根据产品线获得需跟进的数量
	 * 
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryTrackByProductLine(long product_line_id) throws Exception {
		try {
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("transport_ready_period"); // 交货单备货跟进周期（天）
			int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 交货实物图片周期
			int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 交货质检周期
			int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 交货内部标签周期
			int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 转运第三方标签周期
			
			DBRow[] track_ready_delivery = floorTransportMgrZJ.getNeedTrackReadyDelivery(product_line_id,
					need_track_ready_delivery_day, null);
			DBRow[] track_product_file_delivery = floorTransportMgrZJ.getNeedTrackProductFileDelivery(product_line_id,
					product_file_day, null);
			DBRow[] track_quality_inspection_delivery = floorTransportMgrZJ.getNeedTrackQualityInspectionDelivery(
					product_line_id, quality_inspection_day, null);
			DBRow[] track_tag_delivery = floorTransportMgrZJ.getNeedTrackTagDelivery(product_line_id, tag_day, null);
			DBRow[] track_third_tag_delivery = floorTransportMgrZJ.getNeedTrackThirdTagDelivery(product_line_id,
					third_tag_day, null);
			
			DBRow[] result = new DBRow[5];
			
			result[0] = new DBRow();
			result[0].add("track_tilte", "备货中");
			result[0].add("track_count", track_ready_delivery.length);
			result[0].add("cmd", "ready_delivery");
			
			result[1] = new DBRow();
			result[1].add("track_tilte", "内部标签");
			result[1].add("track_count", track_tag_delivery.length);
			result[1].add("cmd", "tag_delivery");
			
			result[2] = new DBRow();
			result[2].add("track_tilte", "质检报告");
			result[2].add("track_count", track_quality_inspection_delivery.length);
			result[2].add("cmd", "quality_inspection_delivery");
			
			result[3] = new DBRow();
			result[3].add("track_tilte", "实物图片");
			result[3].add("track_count", track_product_file_delivery.length);
			result[3].add("cmd", "product_file_delivery");
			
			result[4] = new DBRow();
			result[4].add("track_tilte", "第三方标签");
			result[4].add("track_count", track_third_tag_delivery.length);
			result[4].add("cmd", "third_tag_delivery");
			
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "getDeliveryTrackByProductLine", log);
		}
	}
	
	/**
	 * 获得需跟进实物图片的交货单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileDelivery(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int product_file_day = systemConfig.getIntConfigValue("transport_product_file_period"); // 交货实物图片周期
			
			return (floorTransportMgrZJ.getNeedTrackProductFileDelivery(product_line_id, product_file_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getProductFileDelivery", log);
		}
	}
	
	/**
	 * 获得需跟进质检的交货单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionDelivery(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int quality_inspection_day = systemConfig.getIntConfigValue("transport_quality_inspection"); // 交货质检周期
			
			return (floorTransportMgrZJ.getNeedTrackQualityInspectionDelivery(product_line_id, quality_inspection_day,
					pc));
		} catch (Exception e) {
			throw new SystemException(e, "getQualityInspectionDelivery", log);
		}
	}
	
	/**
	 * 需跟进内部标签的交货单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagDelivery(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int tag_day = systemConfig.getIntConfigValue("transport_tag_period"); // 交货内部标签周期
			
			return (floorTransportMgrZJ.getNeedTrackTagDelivery(product_line_id, tag_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackTagDelivery", log);
		}
	}
	
	/**
	 * 需跟进第三方标签的交货单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagDelivery(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int third_tag_day = systemConfig.getIntConfigValue("transport_third_tag_period"); // 交货第三方标签周期
			
			return (floorTransportMgrZJ.getNeedTrackThirdTagDelivery(product_line_id, third_tag_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackTagDelivery", log);
		}
	}
	
	/**
	 * 导入转运单明细(返回系统文件名)
	 */
	public String importTransportDetail(String fileName) throws Exception {
		DBRow[] fileDetails = this.excelshow(fileName);
		
		String[] type = fileName.split("\\.");
		String fileType = type[type.length - 1];
		
		String baseUrl = Environment.getHome() + "upl_imags_tmp/";
		String systemTimeFile = System.currentTimeMillis() + "." + fileType;
		FileUtil.moveFile(baseUrl + fileName, baseUrl + systemTimeFile);
		for (int i = 0; i < fileDetails.length; i++) {
			DBRow product = floorProductMgr.getDetailProductByPname(fileDetails[i].getString("p_name"));
			if (product == null) {
				product = new DBRow();
			}
			
			float deliver_count = Float.parseFloat(fileDetails[i].getString("prefill_deliver_count"));
			float backup_count = Float.parseFloat(fileDetails[i].getString("prefill_backup_count"));
			float count = deliver_count + backup_count;
			String box = fileDetails[i].getString("prefill_box");
			String serial_number = fileDetails[i].getString("serial_number");
			String lot_number = fileDetails[i].getString("lot_number");
			
			DBRow importTransportDetail = new DBRow();
			importTransportDetail.add("transport_pc_id", product.get("pc_id", 0l));
			importTransportDetail.add("transport_delivery_count", deliver_count);
			importTransportDetail.add("transport_backup_count", backup_count);
			importTransportDetail.add("transport_count", count);
			importTransportDetail.add("file_name", systemTimeFile);
			importTransportDetail.add("import_p_name", fileDetails[i].getString("p_name"));
			importTransportDetail.add("lot_number", lot_number);
			importTransportDetail.add("line_number", i + 2);
			
			if (!box.equals("")) {
				importTransportDetail.add("transport_detail_box", box);
			}
			
			if (!serial_number.equals("")) {
				importTransportDetail.add("transport_detail_serial_number", serial_number);
			}
			
			floorTransportMgrZJ.addTransportImportDetail(importTransportDetail);
		}
		
		return systemTimeFile;
	}
	
	/**
	 * 根据系统文件名返回错误
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] errorImportTransportDetail(String fileName) throws Exception {
		try {
			DBRow[] errors = floorTransportMgrZJ.errorImportTransportDetail(fileName);
			
			ArrayList<DBRow> errorlist = new ArrayList<DBRow>();// 整理好的错误信息集合
			HashMap<String, String> map = new HashMap<String, String>();
			DBRow error = new DBRow();
			for (int i = 0; i < errors.length; i++) {
				if (map.containsKey(errors[i].getString("number")))// 如果同一行有多个错误信息，错误信息用，分割组合
				{
					String errorMessage = error.getString("errorMessage");
					errorMessage += "," + errors[i].getString("error_type");
					error.add("errorMessage", errorMessage);
					
				} else {
					map.put(errors[i].getString("line_number"), errors[i].getString("transport_pc_id"));
					if (i > 0) {
						errorlist.add(error);
					}
					error = new DBRow();
					error.add("p_name", errors[i].getString("p_name"));
					error.add("transport_delivery_count", errors[i].getString("transport_delivery_count"));
					error.add("transport_backup_count", errors[i].getString("transport_backup_count"));
					error.add("transport_detail_serial_number", errors[i].getString("transport_detail_serial_number"));
					error.add("transport_detail_box", errors[i].getString("transport_detail_box"));
					error.add("lot_number", errors[i].getString("lot_number"));
					error.add("errorProductRow", errors[i].getString("number"));
					error.add("errorMessage", errors[i].getString("error_type"));
				}
				// 最后一条记录加入错误集合中
				if (i == errors.length - 1) {
					errorlist.add(error);
				}
			}
			return errorlist.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "errorImportTransportDetail", log);
		}
	}
	
	/**
	 * 转运单收货完成
	 * 
	 * @param transport_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void transportReceiveOver(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("transport_status", TransportOrderKey.AlREADYRECEIVE);
			
			floorTransportMgrZJ.modTransport(transport_id, para);
			
			transportApproveMgrZJ.addTransportApproveSub(transport_id, adminLoggerBean);
		} catch (Exception e) {
			throw new SystemException(e, "transportReceiveOver", log);
		}
	}
	
	public void transportPutOver(long transport_id, AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("put_status", TransportPutKey.Puted);
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			if (transport.get("transport_status", 0) != TransportOrderKey.APPROVEING) {
				para.add("transport_status", TransportOrderKey.FINISH);
			}
			
			floorTransportMgrZJ.modTransport(transport_id, para);
		} catch (Exception e) {
			throw new SystemException(e, "transportPutOver", log);
		}
	}
	
	/**
	 * 发货收货比较
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] sendReceiveDifferents(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.sendReceiveDifferents(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "sendReceiveDifferents", log);
		}
	}
	
	/**
	 * 计划发货比较
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] planSendDifferents(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.planSendDifferents(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "planSendDifferents", log);
		}
	}
	
	/**
	 * 发货收货序列号不同
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] sendReceiveDifferentsSN(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.sendReceiveDifferentsSN(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "sendReceiveDifferentsSN", log);
		}
	}
	
	/**
	 * 计划发货序列号不同
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] planSendDifferentsSN(long transport_id) throws Exception {
		try {
			return floorTransportMgrZJ.planSendDifferentsSN(transport_id);
		} catch (Exception e) {
			throw new SystemException(e, "planSendDifferentsSN", log);
		}
	}
	
	/**
	 * 模拟出入库
	 */
	public String simulationOutIn(long transport_id, String file_name, int simulation_type) throws Exception {
		try {
			String[] type = file_name.split("\\.");
			String fileType = type[type.length - 1];
			String baseUrl = Environment.getHome() + "upl_imags_tmp/";
			String systemTimeFile = System.currentTimeMillis() + "." + fileType;
			FileUtil.moveFile(baseUrl + file_name, baseUrl + systemTimeFile);
			DBRow[] simulations = excelShowSimulation(systemTimeFile);
			
			for (int i = 0; i < simulations.length; i++) {
				DBRow simulation = simulations[i];
				String lp = simulation.getString("simulation_lp");
				if (lp.equals("")) {
					simulation.add("simulation_lp", null);
				}
				simulation.add("simulation_line_number", i + 2);
				simulation.add("simulation_type", simulation_type);
				simulation.add("simulation_transport_id", transport_id);
				simulation.add("simulation_file_name", systemTimeFile);
				
				floorTransportMgrZJ.addSimulationOutIn(simulation);
			}
			
			return systemTimeFile;
		} catch (Exception e) {
			throw new SystemException(e, "simulationOutError", log);
		}
	}
	
	/**
	 * 盘点机模拟操作文件内错误
	 * 
	 * @param systemTimeFile
	 * @param simulation_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] simulationOutInError(String systemTimeFile, int simulation_type) throws Exception {
		try {
			DBRow[] errors = new DBRow[0];
			if (simulation_type == SimulationTransportKey.Out) {
				errors = floorTransportMgrZJ.simulationTransportOutError(systemTimeFile);
			}
			if (simulation_type == SimulationTransportKey.In) {
				errors = floorTransportMgrZJ.simulationTransportInError(systemTimeFile);
			}
			// 整理好的错误信息集合
			ArrayList<DBRow> errorlist = new ArrayList<DBRow>();
			HashMap<String, String> map = new HashMap<String, String>();
			DBRow error = new DBRow();
			for (int i = 0; i < errors.length; i++) {
				if (map.containsKey(errors[i].getString("line_number")))// 如果同一行有多个错误信息，错误信息用，分割组合
				{
					String errorMessage = error.getString("errorMessage");
					errorMessage += "," + errors[i].getString("error_type");
					error.add("errorMessage", errorMessage);
				} else {
					map.put(errors[i].getString("line_number"), errors[i].getString("pc_id"));
					if (i > 0) {
						errorlist.add(error);
					}
					error = new DBRow();
					error.add("p_name", errors[i].getString("p_name"));
					error.add("count", errors[i].getString("count"));
					error.add("serial_number", errors[i].getString("serial_number"));
					error.add("lp", errors[i].getString("lp"));
					error.add("errorProductRow", errors[i].getString("line_number"));
					error.add("errorMessage", errors[i].getString("error_type"));
				}
				// 最后一条记录加入错误集合中
				if (i == errors.length - 1) {
					errorlist.add(error);
				}
			}
			return errorlist.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "simulationOutInError", log);
		}
	}
	
	public DBRow[] excelShowSimulation(String file_name) throws Exception {
		HashMap<String, String> filedName = new HashMap<String, String>();
		filedName.put("0", "simulation_p_name");
		filedName.put("1", "simulation_count");
		filedName.put("2", "simulation_serial_number");
		filedName.put("3", "simulation_lp");
		
		HashMap<String, String> pcidMap = new HashMap<String, String>();
		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		String path = Environment.getHome() + "upl_imags_tmp/" + file_name;
		
		InputStream is = new FileInputStream(path);
		Workbook rwb = Workbook.getWorkbook(is);
		
		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns(); // excel表字段数
		int rsRows = rs.getRows(); // excel表记录行数
		
		for (int i = 1; i < rsRows; i++) {
			DBRow pro = new DBRow();
			
			boolean result = false;// 判断是否纯空行数据
			for (int j = 0; j < rsColumns; j++) {
				if (j == 0) {
					DBRow product = floorProductMgr.getDetailProductByPname(rs.getCell(j, i).getContents().trim());
					if (product != null)// 商品查不到，跳过循环，不添加入数组
					{
						pcidMap.put(String.valueOf(product.get("pc_id", 0l)), String.valueOf(product.get("pc_id", 0l)));
						pro.add("simulation_pc_id", product.get("pc_id", 0l));
					} else {
						pro.add("simulation_pc_id", 0);
					}
					
				}
				
				pro.add(filedName.get(String.valueOf(j)), rs.getCell(j, i).getContents().trim());
				
				ArrayList proName = pro.getFieldNames();// DBRow 字段名
				for (int p = 0; p < proName.size(); p++) {
					if (!pro.getString(proName.get(p).toString()).trim().equals(""))// 去掉空格
					{
						result = true;
					}
				}
			}
			
			if (result)// 不是纯空行就加入到DBRow
			{
				al.add(pro);
			}
		}
		
		return (al.toArray(new DBRow[0]));
	}
	
	/**
	 * 页面模拟盘点机出库
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void transportSimulationOut(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			String fileName = StringUtil.getString(request, "import_name");
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			String machine_id = "WEB";
			
			// 单据状态检查
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			int transport_status = transport.get("transport_status", 0);
			/**
			 * 普通转运单单，非装箱状态不许出库（扣过库存） 交货转运单,非准备状态不许出库（不扣库存）
			 */
			if (transport.get("purchase_id", 0l) == 0 && transport_status != TransportOrderKey.PACKING) {
				throw new TransportStockOutHandleErrorException("转运单出库状态不正确!");
			} else if (transport.get("purchase_id", 0l) != 0 && transport_status != TransportOrderKey.READY) {
				throw new TransportStockOutHandleErrorException("转运单出库状态不正确!");
			}
			
			// 存入发货明细
			DBRow[] outbounds = null;
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			if (!fileName.equals("")) {
				outbounds = floorTransportMgrZJ.getSimulationOutInByFileName(fileName);
			} else {
				outbounds = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null, null, null);
			}
			
			for (int i = 0; i < outbounds.length; i++) {
				String lp;
				long pc_id;
				String sn;
				float to_count;
				if (!fileName.equals("")) {
					lp = outbounds[i].getString("simulation_lp");
					pc_id = outbounds[i].get("simulation_pc_id", 0l);
					sn = outbounds[i].getString("simulation_serial_number");
					to_count = outbounds[i].get("simulation_count", 0f);
				} else {
					lp = outbounds[i].getString("transport_detail_lp");
					pc_id = outbounds[i].get("transport_pc_id", 0l);
					sn = outbounds[i].getString("transport_product_serial_number");
					to_count = outbounds[i].get("transport_count", 0f);
				}
				
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				
				long lp_id = 0;
				if (contianer != null) {
					lp_id = contianer.get("con_id", 0l);
				}
				
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				
				if (product == null) {
					throw new ProductNotExistException();
				}
				
				DBRow db = new DBRow();
				db.add("to_transport_id", transport_id);
				db.add("to_pc_id", product.get("pc_id", 0l));
				db.add("to_p_name", product.getString("p_name"));
				db.add("to_p_code", product.getString("p_code"));
				db.add("to_count", to_count);
				db.add("to_send_adid", adminLoggerBean.getAdid());
				db.add("to_send_time", DateUtil.NowStr());
				db.add("to_machine_id", machine_id);
				db.add("to_send_adid", adminLoggerBean.getAdid());
				db.add("to_send_time", DateUtil.NowStr());
				
				if (lp_id != 0) {
					db.add("to_lp_id", lp_id);
				}
				
				if (!sn.equals("")) {
					db.add("to_serial_number", sn);
				}
				
				list.add(db);
			}
			
			transportOutboundMgrZJ.addTransportOutboundSub(list.toArray(new DBRow[0]), transport_id, machine_id);
			
			this.deliveryTransport(transport_id, adminLoggerBean);// 转运单已起运
			
			if (transport.get("purchase_id", 0l) == 0) {
				transportOutboundApproveMgrZJ.addTransportOutboundApproveSub(transport_id, adminLoggerBean);// 创建转运出库审核
			}
			
			// 处理签到信息
			doorOrLocationOccupancyMgrZyj.updateDoorOrLocationStatusAndTime(transport_id, TransportOrderKey.INTRANSIT);
			
			floorTransportMgrLL.insertLogs(String.valueOf(transport_id), "转运单:" + transport_id + "起运",
					adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4,
					transport.get("transport_status", 0));
			scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, 4, "转运单:" + transport_id
					+ "起运", false, request, "transport_intransit_period");
		} catch (Exception e) {
			throw new SystemException(e, "transportSimulationOut", log);
		}
	}
	
	/**
	 * 页面模拟盘点机收货
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void transportSimulationIn(HttpServletRequest request) throws Exception {
		try {
			long transport_id = StringUtil.getLong(request, "transport_id");
			String fileName = StringUtil.getString(request, "import_name");
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			String machine_id = "WEB";
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			int transport_status = transport.get("transport_status", 0);
			if (!(transport_status == TransportOrderKey.INTRANSIT || transport_status == TransportOrderKey.AlREADYARRIAL)) {
				throw new TransportStockInHandleErrorException("转运单入库状态不正确!");
			}
			
			DBRow[] warehouses = null;
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			if (!fileName.equals("")) {
				warehouses = floorTransportMgrZJ.getSimulationOutInByFileName(fileName);
			} else {
				warehouses = transportOutboundMgrZJ.getTransportOutboundByTransportId(transport_id);
			}
			
			for (int i = 0; i < warehouses.length; i++) {
				String lp;
				long pc_id;
				String sn;
				float tw_count;
				if (!fileName.equals("")) {
					lp = warehouses[i].getString("simulation_lp");
					pc_id = warehouses[i].get("simulation_pc_id", 0l);
					sn = warehouses[i].getString("simulation_serial_number");
					tw_count = warehouses[i].get("simulation_count", 0f);
				} else {
					lp = warehouses[i].getString("to_lp_id");
					pc_id = warehouses[i].get("to_pc_id", 0l);
					sn = warehouses[i].getString("to_serial_number");
					tw_count = warehouses[i].get("to_count", 0f);
				}
				
				// 通过条码机上传的ID查询商品
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				if (product == null) {
					throw new ProductNotExistException();
				}
				
				DBRow contianer = containerProductMgrZJ.getDetailContainer(lp);
				long lp_id = 0;
				if (contianer != null) {
					lp_id = contianer.get("con_id", 0l);
				} else {
					lp_id = Long.parseLong(lp);
				}
				
				DBRow db = new DBRow();
				db.add("tw_transport_id", transport_id);
				db.add("tw_product_id", product.get("pc_id", 0l));
				db.add("tw_product_name", product.getString("p_name"));
				db.add("tw_product_barcode", product.getString("p_code"));
				db.add("tw_count", tw_count);
				db.add("machine_id", machine_id);
				if (lp_id != 0) {
					db.add("tw_lp_id", lp_id);
				}
				
				if (!sn.equals("NULL")) {
					db.add("tw_serial_number", sn);
				}
				
				list.add(db);
			}
			
			transportWarehouseMgrZJ.addTransportWareHouseSub(list.toArray(new DBRow[0]), transport_id, machine_id);
			
			DBRow[] transportDetailsWarehouse = transportWarehouseMgrZJ
					.getTransportWareHouseByTransportId(transport_id);
			this.transportWarehousingSub(transport_id, transportDetailsWarehouse, adminLoggerBean, "");// 入库检查
		} catch (Exception e) {
			throw new SystemException(e, "transportSimulationIn", log);
		}
	}
	
	/**
	 * 保留分配展示
	 * 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] showReceiveAllocate(long transport_id) throws Exception {
		try {
			DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,
					null, null);
			
			ArrayList<DBRow> receiveAllocateList = new ArrayList<DBRow>();
			for (int i = 0; i < transportDetails.length; i++) {
				long pc_id = transportDetails[i].get("transport_pc_id", 0l);
				int count = (int) transportDetails[i].get("transport_count", 0f);
				long clp_type_id = transportDetails[i].get("clp_type_id", 0l);
				long blp_type_id = transportDetails[i].get("blp_type_id", 0l);
				long transport_detail_id = transportDetails[i].get("transport_detail_id", 0l);
				
				DBRow receiveAllocate = lpTypeMgrZJ.calculateProductLPCount(pc_id, count, clp_type_id, blp_type_id);
				receiveAllocate.add("transport_detail_id", transport_detail_id);
				
				receiveAllocateList.add(receiveAllocate);
			}
			
			return receiveAllocateList.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "getReceiveAllocateShow", log);
		}
	}
	
	public DBRow[] getTransportOrderOutList(long transport_id, int system_bill_type) throws Exception {
		try {
			
			return floorTransportMgrZJ.getTransportOrderOutList(transport_id, system_bill_type);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportOrderOutList", log);
		}
	}
	
	public void initTranpsortDetailVolume() throws Exception {
		DBRow[] rows = floorTransportMgrZJ.getAllTransportDetail();
		
		DBRow product = null;
		float product_volume = 0;
		for (int i = 0; i < rows.length; i++) {
			
			if (rows[i].get("volume", 0f) == 0f) {
				product = floorProductMgr.getDetailProductByPcid(rows[i].get("transport_pc_id", 0l));
				if (product != null) {
					product_volume = product.get("volume", 0f);
				}
				
				DBRow para = new DBRow();
				para.add("transport_volume", product_volume * rows[i].get("transport_count", 0f));
				
				floorTransportMgrZJ.modTransportDetail(rows[i].get("transport_detail_id", 0l), para);
			}
		}
	}
	
	public void initSendPrice() throws Exception {
		try {
			DBRow[] transports = floorTransportMgrZJ.getAllTransport();
			
			for (int i = 0; i < transports.length; i++) {
				DBRow[] transportDetals = floorTransportMgrZJ.getTransportDetailByTransportId(
						transports[i].get("transport_id", 0l), null, null, null, null);
				
				for (int j = 0; j < transportDetals.length; j++) {
					long purchase_id = transports[i].get("purchase_id", 0l);
					
					long pc_id = transportDetals[j].get("transport_pc_id", 0l);
					DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
					double send_price = product.get("unit_price", 0d);
					
					if (purchase_id != 0) {
						DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
						if (purchaseDetail != null) {
							send_price = purchaseDetail.get("price", 0d);
						}
					}
					
					DBRow para = new DBRow();
					para.add("transport_send_price", send_price);
					
					floorTransportMgrZJ.modTransportDetail(transportDetals[j].get("transport_detail_id", 0l), para);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 如果当前登录者是客户，返回客户ID，否则返回0
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private long findLoginAdid(long create_account_id, HttpServletRequest request) throws Exception {
		try {
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));// 获取系统当前登录者
			if (GlobalKey.CLIENT_ADGID == adminLoggerBean.getAdgid() && 0 == create_account_id) {
				create_account_id = adminLoggerBean.getAdid();
			}
			return create_account_id;
		} catch (Exception e) {
			throw new SystemException(e, "findLoginAdid(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 添加转运单
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 * @author zyj
	 */
	public long addTransport(DBRow row) throws Exception {
		try {
			return floorTransportMgrZJ.addTransport(row);
		} catch (Exception e) {
			throw new SystemException(e, "addTransport(DBRow row)", log);
		}
	}
	
	/**
	 * 添加转运单明细
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 * @author zyj
	 */
	public long addTransportDetail(DBRow row) throws Exception {
		try {
			return floorTransportMgrZJ.addTransportDetail(row);
		} catch (Exception e) {
			throw new SystemException(e, "addTransportDetail(DBRow row)", log);
		}
	}
	
	/**
	 * 
	 * 根据B2BOrder创建转运单,json数据格式
	 * 
	 * @param b2b_oid
	 * @return <b>Date:</b>2015-1-9上午9:23:03<br>
	 * @author: cuicong
	 * @throws Exception
	 */
	public long addB2BOrderTransport(long b2b_oid, HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		// ----------------主单数据---------------------
		DBRow insertTransport = new DBRow();
		DBRow b2bOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);// b2bOrder 主单
		
		insertTransport.add("send_psid", b2bOrder.get("send_psid", 0));// '`send_psid` int(10) NOT NULL COMMENT '发送仓库'
		insertTransport.add("receive_psid", b2bOrder.get("receive_psid", 0));// `receive_psid` int(10) NOT NULL COMMENT
																				// '接收仓库'
		insertTransport.add("transport_number", b2bOrder.get("b2b_order_number", ""));// `transport_number`
																						// varchar(50)DEFAULT NULL
																						// COMMENT '原交货单序号'
		insertTransport.add("transport_waybill_number", b2bOrder.get("b2b_order_waybill_number", ""));//
		// `transport_waybill_number` varchar(100) DEFAULT NULL COMMENT '运输公司运单号'
		insertTransport.add("transport_waybill_name", b2bOrder.get("b2b_order_waybill_name", ""));
		// `transport_waybill_name` varchar(100) DEFAULT NULL COMMENT'运输公司名称'
		insertTransport.add("transport_address", b2bOrder.get("b2b_order_address", ""));
		// `transport_address` varchar(200) NOT NULL COMMENT '接收地址'
		insertTransport.add("transport_linkman", b2bOrder.get("b2b_order_linkman", ""));
		// `transport_linkman` varchar(100) NOT NULL COMMENT '接收联系人'
		insertTransport.add("transport_linkman_phone", b2bOrder.get("b2b_order_linkman_phone", ""));
		// `transport_linkman_phone` varchar(100) NOT NULL COMMENT // '接收联系人电话'
		insertTransport.add("transport_status", 1);
		// `transport_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT // '转运单状态1准备中2正常运输3审核中4完成5装箱中6差异运输',
		insertTransport.add("transport_date", new Timestamp(new Date().getTime()));
		// `transport_date` datetime NOT NULL/ COMMENT '创建时间'
		
		insertTransport.add("transport_receive_date", b2bOrder.get("b2b_order_receive_date", null));
		// `transport_receive_date` datetime DEFAULT NULL
		
		insertTransport.add("transport_send_country", b2bOrder.get("transport_send_country", 0l));
		// `transport_send_country` int(11) DEFAULT NULL COMMENT // '发货国家'
		
		insertTransport.add("transport_send_place", b2bOrder.get("b2b_order_send_place", ""));
		// `transport_send_place` varchar(200) DEFAULT NULL COMMENT '发货地（城市/港口）',
		
		insertTransport.add("transport_send_freight", b2bOrder.get("transport_send_freight", ""));
		// `transport_send_freight` varchar(200) DEFAULT NULL COMMENT// '发货货代',
		
		insertTransport.add("transport_receive_country", b2bOrder.get("b2b_order_receive_country", 0l));
		// `transport_receive_country` int(11) DEFAULT NULL COMMENT '收货国家',
		insertTransport.add("transport_receive_place", b2bOrder.get("b2b_order_receive_place", ""));
		// `transport_receive_place` varchar(200) DEFAULT NULL COMMENT '目的地（城市/港口）',
		insertTransport.add("transport_receive_freight", b2bOrder.get("b2b_order_receive_freight", ""));
		// `transport_receive_freight` varchar(200) DEFAULT NULL COMMENT '目的地货代',
		insertTransport.add("create_account_id", adminLoggerBean.getAdid());
		// `create_account_id` int(11) DEFAULT NULL,
		insertTransport.add("create_account", adminLoggerBean.getAccount());
		// `create_account` varchar(45) NOT NULL COMMENT '转运单创建人',
		insertTransport.add("packing_account", adminLoggerBean.getAccount());
		// `packing_account` varchar(45) DEFAULT NULL COMMENT '决定装箱人',
		insertTransport.add("remark", b2bOrder.get("remark", ""));
		// `remark` varchar(500) DEFAULT NULL COMMENT '备注',
		
		insertTransport.add("carriers", b2bOrder.get("carriers", ""));
		// `carriers` varchar(200) DEFAULT NULL COMMENT '承运公司',
		insertTransport.add("transportby", b2bOrder.get("b2b_orderby", 0));
		// `transportby` tinyint(4) DEFAULT NULL COMMENT// '运输方式',
		insertTransport.add("declaration", b2bOrder.get("declaration", 0));
		// `declaration` tinyint(4) DEFAULT '1' COMMENT '报关',
		insertTransport.add("clearance", b2bOrder.get("clearance", 0));
		// `clearance` tinyint(4) DEFAULT '1' COMMENT '清关',
		insertTransport.add("drawback", b2bOrder.get("drawback", 0));
		// `drawback` tinyint(4) DEFAULT '1' COMMENT '退税',
		insertTransport.add("invoice", b2bOrder.get("invoice", 0));
		// `invoice` tinyint(4) DEFAULT NULL COMMENT '开票',
		
		insertTransport.add("updatedate", new Timestamp(new Date().getTime()));
		// `updatedate` datetime DEFAULT NULL,
		insertTransport.add("updateby", adminLoggerBean.getAdid());
		// `updateby` int(11) DEFAULT NULL,
		insertTransport.add("updatename", new Timestamp(new Date().getTime()));
		// `updatename` varchar(50) DEFAULT NULL,
		insertTransport.add("declaration_over", b2bOrder.get("declaration_over", 0d));
		// `declaration_over` double(4,1) DEFAULT NULL COMMENT '报关完成小时',
		
		insertTransport.add("clearance_over", b2bOrder.get("clearance_over", 0d));
		// `clearance_over` double(4,1) DEFAULT NULL COMMENT// '清关完成小时'
		insertTransport.add("drawback_over", b2bOrder.get("drawback_over", 0d));
		// `drawback_over` double(4,1) DEFAULT NULL COMMENT// '退税完成小时',
		
		insertTransport.add("invoice_over", b2bOrder.get("invoice_over", 0d));
		// `invoice_over` double(4,1) DEFAULT NULL COMMENT // '发票完成小时',
		
		insertTransport.add("all_over", b2bOrder.get("all_over", 0d));
		// `all_over` double(4,1) DEFAULT NULL COMMENT '流程完成小时',
		insertTransport.add("cost", b2bOrder.get("cost", 0d));
		// `cost` double DEFAULT NULL, `fr_id` int(11) DEFAULT NULL,
		insertTransport.add("stock_in_set", b2bOrder.get("stock_in_set", 0));
		// `stock_in_set` tinyint(4) DEFAULT NULL,
		insertTransport.add("send_zip_code", b2bOrder.get("send_zip_code", ""));
		// `send_zip_code` varchar(20) DEFAULT NULL COMMENT// '目的地邮编',
		
		insertTransport.add("sc_id", b2bOrder.get("sc_id", 0));
		// `sc_id` int(10) DEFAULT NULL COMMENT '预算快递ID',
		insertTransport.add("deliver_ccid", b2bOrder.get("deliver_ccid", 0));
		// `deliver_ccid` int(10) DEFAULT NULL COMMENT // '计算运费目的国家ID',
		
		insertTransport.add("deliver_pro_id", b2bOrder.get("deliver_pro_id", 0));
		// `deliver_pro_id` int(10) DEFAULT // NULL COMMENT
		
		// '计算运费目的省份ID',
		insertTransport.add("invoice_path", b2bOrder.get("invoice_path", ""));
		// `invoice_path` varchar(50) DEFAULT NULL // COMMENT
		
		// '发票文件地址',
		insertTransport.add("send_house_number", b2bOrder.get("send_house_number", ""));
		// `send_house_number` varchar(300) DEFAULT NULL,
		insertTransport.add("send_street", b2bOrder.get("send_street", ""));// `send_street` varchar(300) DEFAULT NULL,
		insertTransport.add("send_address3", b2bOrder.get("send_address3", ""));// `send_address3` varchar(300) DEFAULT
																				// NULL,
		insertTransport.add("deliver_house_number", b2bOrder.get("deliver_house_number", ""));
		// `deliver_house_number` // varchar(300) DEFAULT/ NULL,
		insertTransport.add("deliver_street", b2bOrder.get("deliver_street", ""));
		// `deliver_street` varchar(300) // DEFAULT NULL, `
		insertTransport.add("deliver_address3", b2bOrder.get("deliver_address3", ""));
		// deliver_address3` varchar(300) DEFAULT NULL,
		insertTransport.add("send_name", b2bOrder.get("send_name", ""));
		// `send_name` varchar(50) DEFAULT NULL COMMENT '外部运单发件人',
		insertTransport.add("send_linkman_phone", b2bOrder.get("send_linkman_phone", ""));
		// `send_linkman_phone` varchar(50) DEFAULT NULL // COMMENT * // '外部运单收件人',
		
		insertTransport.add("deliver_zip_code", b2bOrder.get("deliver_zip_code", ""));
		// `deliver_zip_code` varchar(20) DEFAULT NULL // COMMENT '收货地邮编',
		
		insertTransport.add("send_city", b2bOrder.get("send_city", ""));//
		// COMMENT '收货地邮编',`send_city` varchar(50) DEFAULT NULL COMMENT '发货城市',
		insertTransport.add("deliver_city", b2bOrder.get("deliver_city", 0));
		// `deliver_city` varchar(50) DEFAULT NULL COMMENT // '目的城市',
		
		insertTransport.add("weight", b2bOrder.get("weight", 0f));// `weight` float(10,1) DEFAULT NULL COMMENT
		// '运单上记录的运费（DHL与此值不符）',
		insertTransport.add("hs_code", b2bOrder.get("hs_code", ""));// `hs_code` varchar(30) DEFAULT NULL,
		insertTransport.add("pkcount", b2bOrder.get("pkcount", 0f));// `pkcount` int(5) DEFAULT '1' COMMENT '一票几`weight`
		// float(10,1) DEFAULT // NULL COMMENT '运单上记录的运费（DHL与此值不符）', 件',
		
		insertTransport.add("send_ccid", b2bOrder.get("send_ccid", 0));
		// `send_ccid` int(10) DEFAULT NULL COMMENT // '发货国家',
		
		insertTransport.add("send_pro_id", b2bOrder.get("send_pro_id", 0));
		// `send_pro_id` int(10) DEFAULT NULL COMMENT // '发货省份',
		
		insertTransport.add("purchase_id", b2bOrder.get("purchase_id", 0));
		// `purchase_id` int(10) NOT NULL DEFAULT '0'// COMMENT // '采购单ID',
		insertTransport.add("from_ps_type", b2bOrder.get("from_ps_type", 0));
		// `from_ps_type` int(10) NOT NULL DEFAULT '0' COMMENT // '提取仓库类型',
		
		insertTransport.add("target_ps_type", b2bOrder.get("target_ps_type", 0));
		// `target_ps_type` int(10) NOT NULL DEFAULT '0'// COMMENT '目标仓库的仓库类型'
		
		insertTransport.add("packing_date", b2bOrder.get("packing_date", null));
		// `packing_date` datetime DEFAULT NULL COMMENT '装箱时间',
		insertTransport.add("deliveryer_id", b2bOrder.get("deliveryer_id", 0));
		// `deliveryer_id` int(10) DEFAULT NULL COMMENT // '确认收货人ID',
		
		insertTransport.add("deliveryed_date", b2bOrder.get("deliveryed_date", null));
		// `deliveryed_date` datetime DEFAULT NULL COMMENT // '确认收货',
		
		insertTransport.add("warehousinger_id", b2bOrder.get("warehousinger_id", 0));
		// `warehousinger_id` int(10) DEFAULT NULL COMMENT // '入库人ID',
		
		insertTransport.add("warehousing_date", b2bOrder.get("warehousing_date", null));
		// `warehousing_date` datetime DEFAULT NULL COMMENT // '入库时间'
		
		insertTransport.add("stock_in_set_over", b2bOrder.get("stock_in_set_over", 0d));
		// `stock_in_set_over` double(4,1) DEFAULT NULL // COMMENT '运费流程完成时间'
		insertTransport.add("tag", b2bOrder.get("tag", 1));
		// `tag` tinyint(4) DEFAULT NULL COMMENT '标签Key 1:无需打签 2:需打签 3 // 打签完成'
		
		insertTransport.add("certificate", b2bOrder.get("certificate", 1));
		// `certificate` tinyint(4) DEFAULT NULL COMMENT // '单证;1:无需单证;2需单证;3:单证完成'
		
		insertTransport.add("tag_over", b2bOrder.get("tag_over", 0d));
		// `tag_over` double(4,1) DEFAULT NULL COMMENT '标签所花费时间'
		insertTransport.add("certificate_over", b2bOrder.get("certificate_over", 0d));
		// `certificate_over` double(4,1) DEFAULT NULL // COMMENT '单证完成所花费时间',
		
		insertTransport.add("product_file", b2bOrder.get("product_file", 1));
		// `product_file` tinyint(4) DEFAULT NULL COMMENT // '商品图片流程',
		
		insertTransport.add("product_file_over", b2bOrder.get("product_file_over", 0d));
		// `product_file_over` double(4,1) DEFAULT NULL // COMMENT // '商品图片流程完成所需时间'
		
		insertTransport.add("quality_inspection", b2bOrder.get("quality_inspection", 1));
		// `quality_inspection` tinyint(4) DEFAULT NULL // COMMENT '质检流程'
		
		insertTransport.add("quality_inspection_over", b2bOrder.get("quality_inspection_over", 0d));
		// `quality_inspection_over` double(4,1) // DEFAULT NULL COMMENT // '质检完成所需时间
		
		insertTransport.add("tag_third", b2bOrder.get("tag_third", 1));
		// `tag_third` tinyint(4) DEFAULT NULL COMMENT '第三方标签',
		insertTransport.add("tag_third_over", b2bOrder.get("tag_third_over", 0d));
		// `tag_third_over` double(4,2) DEFAULT NULL COMMENT // '第三方标签完成时间'
		
		insertTransport.add("transport_area_id", b2bOrder.get("b2b_order_area_id", 0));
		// `transport_area_id` int(10) DEFAULT NULL // COMMENT '转运单所属区域'
		
		insertTransport.add("out_id", b2bOrder.get("out_id", 0));
		// `out_id` int(10) DEFAULT NULL COMMENT '转运单的拣货单ID
		
		insertTransport.add("deleiver_registration", b2bOrder.get("deleiver_registration", 0));
		// `deleiver_registration` int(10) DEFAULT // NULL COMMENT // '缷货签到司机'
		
		insertTransport.add("deleiver_registration_time", b2bOrder.get("deleiver_registration_time", null));
		// `deleiver_registration_time` datetime // DEFAULT NULL // COMMENT '缺货签到时间'
		
		insertTransport.add("send_registration", b2bOrder.get("send_registration", 0));
		// `send_registration` int(10) DEFAULT NULL // COMMENT '装货签到的司机'
		
		insertTransport.add("send_registration_time", b2bOrder.get("send_registration_time", null));
		// `send_registration_time` datetime DEFAULT // NULL COMMENT // '装货签到时间'
		
		insertTransport.add("transport_out_date", b2bOrder.get("b2b_order_out_date", null));
		// `transport_out_date` datetime DEFAULT NULL // COMMENT '预计出库时间'
		
		insertTransport.add("address_state_send", b2bOrder.get("address_state_send", ""));
		// `address_state_send` varchar(30) DEFAULT NULL // COMMENT '手填提货省'
		
		insertTransport.add("address_state_deliver", b2bOrder.get("address_state_deliver", ""));
		// `address_state_deliver` varchar(255) // DEFAULT NULL COMMENT // '手填收货省'
		
		insertTransport.add("put_status", b2bOrder.get("put_status", 0));
		// `put_status` tinyint(3) NOT NULL DEFAULT '1' COMMENT // '1不可放货2放货中3放货完成'
		
		insertTransport.add("is_not_page_sub", b2bOrder.get("is_not_page_sub", 0));
		// `is_not_page_sub` tinyint(2) DEFAULT NULL COMMENT // '2.不是页面提交过来的交货单'
		
		insertTransport.add("machine", b2bOrder.get("machine", ""));
		// `machine` varchar(20) DEFAULT NULL COMMENT// '如果是android创建用来标识是那台机子创建(用来查询用)'
		
		insertTransport.add("is_relate_purchase", b2bOrder.get("", 0));
		// `is_relate_purchase` tinyint(3) DEFAULT NULL // COMMENT // '是否与采购单关联1是2否'
		
		insertTransport.add("reserve_type", b2bOrder.get("is_relate_purchase", 0));
		// `reserve_type` tinyint(3) DEFAULT NULL COMMENT // '预保留形式1为FIFO,2为SNFO'
		
		insertTransport.add("title_id", b2bOrder.get("title_id", 0));
		// ------------------------明细数据----------------------------------------------------------
		long transport_id = this.addTransport(insertTransport);
		DBRow b2bitems[] = floorB2BOrderMgrZyj.getB2BOrderItems(b2b_oid);// b2bOrder 明细数据
		for (DBRow item : b2bitems) {
			/**
			 * CREATE TABLE `transport_detail` ( `transport_detail_id` int(10) NOT NULL COMMENT ' 主键ID',
			 * 
			 * 
			 * 
			 * `, , PRIMARY KEY (`transport_detail_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8; ,
			 */
			DBRow dbRow = new DBRow();
			dbRow.add("transport_pc_id", item.get("b2b_pc_id", 0));
			// `transport_pc_id` int(10) NOT NULL COMMENT '转运商品ID',
			dbRow.add("transport_delivery_count", item.get("b2b_delivery_count", 0f));
			// `transport_delivery_count` float(10,1) NOT NULL COMMENT '转运交货数量（交货型转运为交货数，转运为转换数）',
			dbRow.add("transport_backup_count", item.get("b2b_backup_count", 0f));
			// `transport_backup_count` float(10,1) DEFAULT '0.0' COMMENT '备件数量（交货型转运单的备件数量，内部转运单默认为0）',
			dbRow.add("transport_count", item.get("b2b_count", 0f));
			// `transport_count` float(10,1) NOT NULL COMMENT '转运总数量',
			dbRow.add("transport_send_count", item.get("b2b_send_count", 0f));
			// `transport_send_count` float(10,1) NOT NULL DEFAULT '0.0' COMMENT '实际发货数量',
			dbRow.add("transport_reap_count", item.get("b2b_reap_count", 0f));
			// `transport_reap_count` float(10,1) NOT NULL DEFAULT '0.0' COMMENT '实际到货数量',
			dbRow.add("transport_send_price", item.get("b2b_send_price", 0f));
			// transport_send_price` float(10,2) DEFAULT NULL COMMENT '出库成本（）'
			dbRow.add("transport_id", transport_id);
			// `transport_id` int(10) NOT NULL COMMENT '转运单ID',
			dbRow.add("transport_box", item.get("b2b_box", ""));
			// `transport_box` varchar(45) DEFAULT NULL COMMENT '外箱号',
			dbRow.add("transport_p_code", item.get("b2b_p_code", ""));
			// `transport_p_code` varchar(45) NOT NULL COMMENT '转运商品条码',
			dbRow.add("transport_p_name", item.get("b2b_p_name", ""));
			// `transport_p_name` varchar(45) NOT NULL COMMENT '转运商品名称',
			dbRow.add("transport_union_count", item.get("b2b_union_count", 0f));
			// `transport_union_count` float(10,1) NOT NULL DEFAULT '0.0' COMMENT '转运套装数',
			dbRow.add("transport_normal_count", item.get("b2b_normal_count", 0f));
			// `transport_normal_count` float(10,1) NOT NULL DEFAULT '0.0' COMMENT '转运散件数量',
			dbRow.add("transport_volume", item.get("b2b_volume", 0f));
			// `transport_volume` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品体积（基础数据内商品体积）',
			dbRow.add("transport_weight", item.get("b2b_weight", 0f));
			// `transport_weight` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品重量（基础数据内商品重量）',
			dbRow.add("transport_product_serial_number", item.get("b2b_product_serial_number", ""));
			// `transport_product_serial_number` varchar(80) DEFAULT NULL COMMENT '商品序列号',
			dbRow.add("clp_type_id", item.get("clp_type_id", 0));
			// `clp_type_id` int(10) DEFAULT NULL COMMENT '指定的CLP的ID',
			dbRow.add("blp_type_id", item.get("blp_type_id", 0));
			// `blp_type_id` int(10) DEFAULT NULL COMMENT '指定使用BLP的ID',
			dbRow.add("lot_number", item.get("lot_number", ""));
			// `lot_number` varchar(40) DEFAULT NULL,
			dbRow.add("b2b_detail_id", item.get("b2b_detail_id", 0f));
			// `b2b_detail_id` int(10) DEFAULT NULL COMMENT 'b2b订单明细ID'
			this.addTransportDetail(dbRow);
		}
		floorTransportMgrLL.insertLogs(Long.toString(transport_id), "创建转运单:单号" + transport_id,
				adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods,
				TransportOrderKey.READY);// 类型1跟进记录
		freightMgrLL.transportSetFreight(transport_id, request);
		return transport_id;
	}
	
	/**
	 * 根据B2BOrder创建转运单
	 */
	public long addB2BOrderTransport(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			long title_id = b2BOrderRow.get("title_id", 0l);
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			
			// 提货地址
			long send_psid = StringUtil.getLong(request, "ps_id");
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			
			// 运输设置
			long fr_id = StringUtil.getLong(request, "fr_id");
			String transport_waybill_name = StringUtil.getString(request, "transport_waybill_name");
			String transport_waybill_number = StringUtil.getString(request, "transport_waybill_number");
			int transportby = StringUtil.getInt(request, "transportby");
			String carriers = StringUtil.getString(request, "carriers");
			String transport_send_place = StringUtil.getString(request, "transport_send_place");
			String transport_receive_place = StringUtil.getString(request, "transport_receive_place");
			
			// 主流程
			String adminUserIdsB2BOrder = StringUtil.getString(request, "adminUserIdsB2BOrder");
			String adminUserNamesB2BOrder = StringUtil.getString(request, "adminUserNamesB2BOrder");
			int needMailB2BOrder = StringUtil.getInt(request, "needMailB2BOrder");
			int needMessageB2BOrder = StringUtil.getInt(request, "needMessageB2BOrder");
			int needPageB2BOrder = StringUtil.getInt(request, "needPageB2BOrder");
			
			// 出口报关
			int declaration = StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration = StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration = StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration = StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration = StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration = StringUtil.getInt(request, "needPageDeclaration");
			
			// 进口清关
			int clearance = StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance = StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance = StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance = StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance = StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance = StringUtil.getInt(request, "needPageClearance");
			
			// 商品图片
			String adminUserIdsProductFile = StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile = StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile = StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile = StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile = StringUtil.getInt(request, "needPageProductFile");
			
			// 内部标签
			int tag = StringUtil.getInt(request, "tag");
			String adminUserIdsTag = StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag = StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag = StringUtil.getInt(request, "needMailTag");
			int needMessageTag = StringUtil.getInt(request, "needMessageTag");
			int needPageTag = StringUtil.getInt(request, "needPageTag");
			
			// 第三方标签
			int tag_third = StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird = StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird = StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird = StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird = StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird = StringUtil.getInt(request, "needPageTagThird");
			
			// 运费
			
			int stock_in_set = StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet = StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet = StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet = StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet = StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet = StringUtil.getInt(request, "needPageStockInSet");
			
			// 单证
			int certificate = StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate = StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate = StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate = StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate = StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate = StringUtil.getInt(request, "needPageCertificate");
			
			// 质检
			int quality_inspection = StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection = StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection = StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection = StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection = StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection = StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(b2BOrderRow.get(
					"receive_psid", 0L));
			DBRow transportRow = new DBRow();
			// 收货信息
			transportRow.add("receive_psid", b2BOrderRow.get("receive_psid", 0L));
			transportRow.add("deliver_ccid", b2BOrderRow.get("deliver_ccid", 0L));
			transportRow.add("deliver_pro_id", b2BOrderRow.get("deliver_pro_id", 0L));
			transportRow.add("deliver_city", b2BOrderRow.getString("deliver_city"));
			transportRow.add("deliver_house_number", b2BOrderRow.getString("deliver_house_number"));
			transportRow.add("deliver_street", b2BOrderRow.getString("deliver_street"));
			transportRow.add("deliver_zip_code", b2BOrderRow.getString("deliver_zip_code"));
			transportRow.add("transport_linkman", b2BOrderRow.getString("b2b_order_linkman", "MM"));
			transportRow.add("transport_linkman_phone", b2BOrderRow.getString("b2b_order_linkman_phone"));
			if (!"".equals(b2BOrderRow.getString("b2b_order_receive_date"))) {
				transportRow.add("transport_receive_date", b2BOrderRow.getString("b2b_order_receive_date"));
			}
			transportRow.add("address_state_deliver", b2BOrderRow.getString("address_state_deliver"));
			
			// 提货信息
			transportRow.add("send_psid", send_psid);
			transportRow.add("send_city", sendProductStorage.getString("send_city"));
			transportRow.add("send_house_number", sendProductStorage.getString("send_house_number"));
			transportRow.add("send_street", sendProductStorage.getString("send_street"));
			transportRow.add("send_ccid", sendProductStorage.get("send_nation", 0L));
			transportRow.add("send_pro_id", sendProductStorage.get("send_pro_id", 0L));
			transportRow.add("send_zip_code", sendProductStorage.getString("send_zip_code"));
			transportRow.add("send_name", sendProductStorage.getString("send_contact"));
			transportRow.add("send_linkman_phone", sendProductStorage.getString("send_phone"));
			if (!"".equals(b2BOrderRow.getString("b2b_order_out_date"))) {
				transportRow.add("transport_out_date", b2BOrderRow.getString("b2b_order_out_date"));
			}
			transportRow.add("address_state_send", sendProductStorage.getString("send_pro_input"));
			transportRow.add("transport_address", "");
			// 其他信息(ETA,备注)
			transportRow.add("remark", b2BOrderRow.getString("remark"));
			transportRow.add("title_id", title_id);
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			// 主流程信息
			transportRow.add("transport_status", TransportOrderKey.PACKING);// B2B创建的转运单都是装箱中
			transportRow.add("transport_date", DateUtil.NowStr());
			transportRow.add("create_account_id", logingUserId);// 转运单的创建者
			transportRow.add("create_account", loginUserName);// 转运单的创建者
			transportRow.add("updatedate", DateUtil.NowStr());
			transportRow.add("updateby", logingUserId);// 转运单的创建者
			transportRow.add("updatename", loginUserName);// 转运单的创建者
			
			// 提货与收货仓库类型
			transportRow.add("target_ps_type", b2BOrderRow.get("storage_type", 0));
			transportRow.add("from_ps_type", sendProductStorage.get("storage_type", 0));
			
			// 运输设置
			transportRow.add("fr_id", fr_id);
			transportRow.add("transport_waybill_name", transport_waybill_name);
			transportRow.add("transport_waybill_number", transport_waybill_number);
			transportRow.add("transportby", transportby);
			transportRow.add("carriers", carriers);
			transportRow.add("transport_send_place", transport_send_place);
			transportRow.add("transport_receive_place", transport_receive_place);
			
			// 流程信息
			transportRow.add("declaration", declaration);
			transportRow.add("clearance", clearance);
			transportRow.add("product_file", TransportProductFileKey.PRODUCTFILE);
			transportRow.add("tag", tag);
			transportRow.add("tag_third", tag_third);
			transportRow.add("stock_in_set", stock_in_set);
			transportRow.add("certificate", certificate);
			transportRow.add("quality_inspection", quality_inspection);
			
			long transport_id = this.addTransport(transportRow);
			
			// 保留分配库存
			DBRow[] cartWaybillB2BProducts = new DBRow[0]; // cartWaybillB2BMgrZJ.getCartWaybillB2bProduct();
			
			PreCalcuB2BOrderBean preCalcuB2BOrderBean = productStoreMgrZJ.calcuOrderB2B(StringUtil.getSession(request),
					send_psid, title_id);
			
			if (preCalcuB2BOrderBean.getOrderStatus() == B2BOrderStatusKey.Enough
					|| (preCalcuB2BOrderBean.getOrderStatus() == B2BOrderStatusKey.ConfigNotEnough && preCalcuB2BOrderBean
							.getOrderConfigChange() == B2BOrderConfigChangeKey.Can)) {
				cartWaybillB2BProducts = preCalcuB2BOrderBean.getResult();
			}
			
			ArrayList<DBRow> containerTypeCountList = new ArrayList<DBRow>();
			ArrayList<DBRow> configChangeItems = new ArrayList<DBRow>();
			for (int i = 0; i < cartWaybillB2BProducts.length; i++) {
				int item_configChange = cartWaybillB2BProducts[i].get("order_item_configChange", 0);
				int cci_type = cartWaybillB2BProducts[i].get("cci_type", 0);
				long cci_type_id = cartWaybillB2BProducts[i].get("cci_type_id", 0l);
				int cci_count = cartWaybillB2BProducts[i].get("cci_count", 0);
				int cci_product_count = cartWaybillB2BProducts[i].get("cci_product_count", 0);
				
				long pc_id = cartWaybillB2BProducts[i].get("pc_id", 0l);
				int product_count = cartWaybillB2BProducts[i].get("product_count", 0);
				String lot_number = cartWaybillB2BProducts[i].getString("lot_number");
				
				long clp_type_id = cartWaybillB2BProducts[i].get("clp_type_id", 0l);
				int clp_count = cartWaybillB2BProducts[i].get("clp_count", 0);
				if (clp_count > 0) {
					
					DBRow clpRow = new DBRow();
					clpRow.add("container_type", ContainerTypeKey.CLP);
					clpRow.add("container_type_id", clp_type_id);
					
					int con_quantity = clp_count;
					
					if (cci_type == ContainerTypeKey.CLP && cci_type_id == clp_type_id
							&& item_configChange == B2BOrderConfigChangeKey.Can) {
						con_quantity = clp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type", cci_type);
						configChangeItem.add("cci_type_id", cci_type_id);
						configChangeItem.add("cci_count", cci_count);
						configChangeItem.add("cci_product_count", cci_product_count);
						configChangeItem.add("cci_pc_id", pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					clpRow.add("con_quantity", con_quantity);
					
					if (con_quantity > 0) {
						containerTypeCountList.add(clpRow);
					}
				}
				/*
				long blp_type_id = cartWaybillB2BProducts[i].get("blp_type_id", 0l);
				int blp_count = cartWaybillB2BProducts[i].get("blp_count", 0);
				if (blp_count > 0) {
					DBRow blpRow = new DBRow();
					blpRow.add("container_type", ContainerTypeKey.BLP);
					blpRow.add("container_type_id", blp_type_id);
					blpRow.add("con_quantity", blp_count);
					
					int con_quantity = blp_count;
					
					if (cci_type == ContainerTypeKey.BLP && cci_type_id == clp_type_id
							&& item_configChange == B2BOrderConfigChangeKey.Can) {
						con_quantity = blp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type", cci_type);
						configChangeItem.add("cci_type_id", cci_type_id);
						configChangeItem.add("cci_count", cci_count);
						configChangeItem.add("cci_product_count", cci_product_count);
						configChangeItem.add("cci_pc_id", pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					blpRow.add("con_quantity", con_quantity);
					
					if (con_quantity > 0) {
						containerTypeCountList.add(blpRow);
					}
				}
				
				long ilp_type_id = cartWaybillB2BProducts[i].get("ilp_type_id", 0l);
				int ilp_count = cartWaybillB2BProducts[i].get("ilp_count", 0);
				if (ilp_count > 0) {
					DBRow ilpRow = new DBRow();
					ilpRow.add("container_type", ContainerTypeKey.ILP);
					ilpRow.add("container_type_id", ilp_type_id);
					ilpRow.add("con_quantity", ilp_count);
					
					int con_quantity = ilp_count;
					
					if (cci_type == ContainerTypeKey.ILP && cci_type_id == clp_type_id
							&& item_configChange == B2BOrderConfigChangeKey.Can) {
						con_quantity = ilp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type", cci_type);
						configChangeItem.add("cci_type_id", cci_type_id);
						configChangeItem.add("cci_count", cci_count);
						configChangeItem.add("cci_product_count", cci_product_count);
						configChangeItem.add("cci_pc_id", pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					ilpRow.add("con_quantity", con_quantity);
					
					if (con_quantity > 0) {
						containerTypeCountList.add(ilpRow);
					}
					containerTypeCountList.add(ilpRow);
				}
				*///去掉ILP和BLP
				int orignail_count = cartWaybillB2BProducts[i].get("orignail_count", 0);
				if (orignail_count > 0) {
					DBRow orignailRow = new DBRow();
					orignailRow.add("container_type", ContainerTypeKey.Original);
					orignailRow.add("container_type_id", 0);
					orignailRow.add("con_quantity", orignail_count);
					containerTypeCountList.add(orignailRow);
				}
				
				DBRow[] containerTypes = containerTypeCountList.toArray(new DBRow[0]);
				containerTypeCountList.clear();
				
				productStoreMgrZJ.reserveProductStore(send_psid, pc_id, title_id, product_count, transport_id,
						ProductStoreBillKey.TRANSPORT_ORDER, lot_number, adminLoggerBean, containerTypes);
				
				if (preCalcuB2BOrderBean.getOrderConfigChange() == B2BOrderConfigChangeKey.Can) {
					configChangeMgrZJ.addConfigChangeForSystemBill(send_psid, title_id, adminLoggerBean,
							ProductStoreBillKey.TRANSPORT_ORDER, transport_id, configChangeItems.toArray(new DBRow[0]));
					
					DBRow para = new DBRow();
					para.add("transport_status", TransportOrderKey.CONFIGCHANGEING);
					
					floorTransportMgrZJ.modTransport(transport_id, para);
				}
			}
			
			// 添加转运单详细，从session中获取值
			this.addB2BTransportItemBySession(transport_id, request);
			
			// 设置运费
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "创建转运单:单号" + transport_id,
					adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), TransportLogTypeKey.Goods,
					TransportOrderKey.READY);// 类型1跟进记录
			freightMgrLL.transportSetFreight(transport_id, request);
			
			// 添加转运单索引
			this.editTransportIndex(transport_id, "add");
			
			// 修改b2b订单
			
			// 转运单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id，转运单传0
			this.addSculde(transport_id, 0L, sendProductStorage.getString("title"),
					receiveProductStorage.getString("title"), b2BOrderRow.getString("b2b_order_receive_date"),
					logingUserId, loginUserName, request, adminUserIdsB2BOrder, adminUserNamesB2BOrder,
					needMailB2BOrder, needMessageB2BOrder, needPageB2BOrder, declaration, adminUserNamesDeclaration,
					adminUserIdsDeclaration, needPageDeclaration, needMailDeclaration, needMessageDeclaration,
					clearance, adminUserNamesClearance, adminUserIdsClearance, needPageClearance, needMailClearance,
					needMessageClearance, TransportProductFileKey.PRODUCTFILE, adminUserNamesProductFile,
					adminUserIdsProductFile, needPageProductFile, needMailProductFile, needMessageProductFile, tag,
					adminUserNamesTag, adminUserIdsTag, needPageTag, needMailTag, needMessageTag, stock_in_set,
					adminUserNamesStockInSet, adminUserIdsStockInSet, needPageStockInSet, needMailStockInSet,
					needMessageStockInSet, certificate, adminUserNamesCertificate, adminUserIdsCertificate,
					needPageCertificate, needMailCertificate, needMessageCertificate, quality_inspection,
					adminUserNamesQualityInspection, adminUserIdsQualityInspection, needPageQualityInspection,
					needMailQualityInspection, needMessageQualityInspection, tag_third, adminUserIdsTagThird,
					adminUserNamesTagThird, needMailTagThird, needMessageTagThird, needPageTagThird);
			
			return transport_id;
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderTransport(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 将session中的选择的订单明细保存成转运单明细
	 * 
	 * @param transport_id
	 * @param request
	 * @throws Exception
	 */
	private void addB2BTransportItemBySession(long transport_id, HttpServletRequest request) throws Exception {
		try {
			
			HttpSession session = StringUtil.getSession(request);
			ArrayList<DBRow> sessionTransportItems = null;
			if (null != session.getAttribute(Config.cartWaybillB2B)) {
				sessionTransportItems = (ArrayList) session.getAttribute(Config.cartWaybillB2B);
			}
			if (null != sessionTransportItems) {
				for (int i = 0; i < sessionTransportItems.size(); i++) {
					DBRow row = sessionTransportItems.get(i);
					if (null != row) {
						long b2b_detail_id = row.get("b2b_detail_id", 0L);
						int product_count = row.get("product_count", 0);
						DBRow b2BOrderItemRow = floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_detail_id);
						DBRow transportDetail = new DBRow();
						transportDetail.add("transport_pc_id", b2BOrderItemRow.get("b2b_pc_id", 0L));
						transportDetail.add("transport_delivery_count", product_count);
						transportDetail.add("transport_backup_count", 0);
						transportDetail.add("transport_count", product_count);
						transportDetail.add("transport_id", transport_id);
						transportDetail.add("transport_p_name", b2BOrderItemRow.getString("b2b_p_name"));
						transportDetail.add("transport_p_code", b2BOrderItemRow.getString("b2b_p_code"));
						transportDetail.add("transport_volume", b2BOrderItemRow.get("b2b_volume", 0d));
						transportDetail.add("transport_weight", b2BOrderItemRow.get("b2b_weight", 0d));
						transportDetail.add("lot_number", b2BOrderItemRow.getString("lot_number"));
						if (!"".equals(b2BOrderItemRow.getString("clp_type_id"))) {
							transportDetail.add("clp_type_id", b2BOrderItemRow.getString("clp_type_id"));
						}
						if (!"".equals(b2BOrderItemRow.getString("blp_type_id"))) {
							transportDetail.add("blp_type_id", b2BOrderItemRow.getString("blp_type_id"));
						}
						transportDetail.add("transport_send_price", b2BOrderItemRow.get("b2b_send_price", 0d));
						transportDetail.add("transport_union_count", b2BOrderItemRow.get("b2b_union_count", 0F));
						transportDetail.add("transport_normal_count", b2BOrderItemRow.get("b2b_normal_count", 0F));
						transportDetail.add("b2b_detail_id", b2b_detail_id);
						
						// 更新B2B订单明细上的待上运单数量
						this.updateb2bOrderDetaiWaitCount(b2b_detail_id, product_count);
						
						this.addTransportDetail(transportDetail);
					}
				}
			}
		} catch (B2BOrderDetailWaitCountNotEnoughException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "addTransportItemBySession(HttpServletRequest request)", log);
		}
	}
	
	public void updateB2BOrder(long b2b_oid) throws Exception {
		// 订单已创建运单,变为装箱状态，修改发货仓库
		DBRow b2bPara = new DBRow();
		
		int b2b_order_status;
		DBRow[] waitAllocateB2BItems = floorB2BOrderMgrZyj.getWaitAllocateB2bOrderItem(b2b_oid);
		
		if (waitAllocateB2BItems.length > 0) {
			b2b_order_status = B2BOrderKey.PARTALLOCATE;
		} else {
			b2b_order_status = B2BOrderKey.PACKING;
		}
		
		b2bPara.add("b2b_order_status", b2b_order_status);
		// b2bPara.add("send_psid",send_psid);
		// b2bPara.add("send_city",sendProductStorage.getString("send_city"));
		// b2bPara.add("send_house_number",sendProductStorage.getString("send_house_number"));
		// b2bPara.add("send_street",sendProductStorage.getString("send_street"));
		// b2bPara.add("send_ccid",sendProductStorage.get("send_nation", 0L));
		// b2bPara.add("send_pro_id",sendProductStorage.get("send_pro_id", 0L));
		// b2bPara.add("send_zip_code",sendProductStorage.getString("send_zip_code"));
		// b2bPara.add("send_name",sendProductStorage.getString("send_contact"));
		// b2bPara.add("send_linkman_phone",sendProductStorage.getString("send_phone"));
		// b2bPara.add("address_state_send", sendProductStorage.getString("send_pro_input"));
		// b2bPara.add("from_ps_type",sendProductStorage.get("storage_type",0));
		floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, b2bPara);
	}
	
	/**
	 * 更新b2b订单明细的待发货数
	 * 
	 * @param b2b_order_detail_id
	 * @param allocate_count
	 * @throws Exception
	 */
	public void updateb2bOrderDetaiWaitCount(long b2b_order_detail_id, int allocate_count) throws Exception {
		DBRow b2bOrderDetail = floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_order_detail_id);
		
		float b2b_wait_count = b2bOrderDetail.get("b2b_wait_count", 0f);
		
		if (allocate_count > b2b_wait_count) {
			throw new B2BOrderDetailWaitCountNotEnoughException();
		}
		
		b2b_wait_count = b2b_wait_count - allocate_count;
		
		DBRow updatePara = new DBRow();
		updatePara.add("b2b_wait_count", b2b_wait_count);
		
		floorB2BOrderMgrZyj.updateB2bOrderDetail(b2b_order_detail_id, updatePara);
	}
	
	/**
	 * 根据B2B订单ID获得B2B运单
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailTransportsByB2BOid(long b2b_oid) throws Exception {
		try {
			return floorTransportMgrZJ.getDetailTransportsByB2BOid(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "getDetailTransportsByB2BOid", log);
		}
	}
	
	public long[] getTransportIdsByB2BOid(long b2b_oid) throws Exception {
		try {
			DBRow[] transports = this.getDetailTransportsByB2BOid(b2b_oid);
			
			long[] transportIds = new long[transports.length];
			for (int i = 0; i < transports.length; i++) {
				transportIds[i] = transports[i].get("transport_id", 0l);
			}
			
			return transportIds;
		} catch (Exception e) {
			throw new SystemException(e, "getTransportIdsByB2BOid", log);
		}
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	
	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}
	
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
	
	public void setTransportApproveMgrZJ(TransportApproveMgrIFaceZJ transportApproveMgrZJ) {
		this.transportApproveMgrZJ = transportApproveMgrZJ;
	}
	
	public void setFloorDeliveryMgrZJ(FloorDeliveryMgrZJ floorDeliveryMgrZJ) {
		this.floorDeliveryMgrZJ = floorDeliveryMgrZJ;
	}
	
	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
	
	public void setFloorApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ) {
		this.floorApplyMoneyMgrZZZ = floorApplyMoneyMgrZZZ;
	}
	
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}
	
	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}
	
	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	public void setFloorSupplierMgrTJH(FloorSupplierMgrTJH floorSupplierMgrTJH) {
		this.floorSupplierMgrTJH = floorSupplierMgrTJH;
	}
	
	public void setTransportMgrZyj(TransportMgrZyj transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}
	
	public void setFreightMgrLL(FreightMgrIFaceLL freightMgrLL) {
		this.freightMgrLL = freightMgrLL;
	}
	
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	
	public void setProductStoreLocationMgrZJ(ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ) {
		this.productStoreLocationMgrZJ = productStoreLocationMgrZJ;
	}
	
	public void setDoorOrLocationOccupancyMgrZyj(DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj) {
		this.doorOrLocationOccupancyMgrZyj = doorOrLocationOccupancyMgrZyj;
	}
	
	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ) {
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}
	
	public void setContainerProductMgrZJ(ContainerProductMgrIFaceZJ containerProductMgrZJ) {
		this.containerProductMgrZJ = containerProductMgrZJ;
	}
	
	public void setTransportOutboundMgrZJ(TransportOutboundMgrIFaceZJ transportOutboundMgrZJ) {
		this.transportOutboundMgrZJ = transportOutboundMgrZJ;
	}
	
	public void setTransportOutboundApproveMgrZJ(TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ) {
		this.transportOutboundApproveMgrZJ = transportOutboundApproveMgrZJ;
	}
	
	public void setTransportWarehouseMgrZJ(TransportWarehouseMgrZJ transportWarehouseMgrZJ) {
		this.transportWarehouseMgrZJ = transportWarehouseMgrZJ;
	}
	
	public void setLpTypeMgrZJ(LPTypeMgrIFaceZJ lpTypeMgrZJ) {
		this.lpTypeMgrZJ = lpTypeMgrZJ;
	}
	
	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}
	
	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}
	
	public void setConfigChangeMgrZJ(ConfigChangeMgrIFaceZJ configChangeMgrZJ) {
		this.configChangeMgrZJ = configChangeMgrZJ;
	}
	
}
