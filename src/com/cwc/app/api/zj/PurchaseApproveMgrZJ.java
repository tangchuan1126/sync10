package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.ll.FloorPurchaseMgrLL;
import com.cwc.app.floor.api.zj.FloorPurchaseApproveMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.iface.zj.DeliveryMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseApproveMgrIFaceZJ;
import com.cwc.app.key.ApprovePurchaseKey;
import com.cwc.app.key.PurchaseArriveKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.PurchaseLogTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class PurchaseApproveMgrZJ implements PurchaseApproveMgrIFaceZJ {

	private FloorPurchaseApproveMgrZJ floorPurchaseApproveMgrZJ;
	private FloorPurchaseMgr floorPurchaseMgr;
	static Logger log = Logger.getLogger("ACTION");
	private DeliveryMgrIFaceZJ deliveryMgrZJ;
	private FloorPurchaseMgrLL floorPurchaseMgrLL;
	
	public void setFloorPurchaseMgrLL(FloorPurchaseMgrLL floorPurchaseMgrLL) {
		this.floorPurchaseMgrLL = floorPurchaseMgrLL;
	}
	/**
	 * 采购单申请完成
	 */
	public void addPuchaseApprove(HttpServletRequest request) 
		throws Exception 
	{
		try
		{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			DBRow[] differences = floorPurchaseMgr.getPurchaseDetail(purchase_id,null,"differencesreap");
			
			String commit_account = adminLoggerBean.getEmploye_name();
			String commit_date = DateUtil.NowStr();
			int different_count = differences.length;
			
			DBRow dbrow = new DBRow();
			dbrow.add("purchase_id",purchase_id);
			dbrow.add("commit_account",commit_account);
			dbrow.add("commit_date",commit_date);
			dbrow.add("different_count",different_count);
			
			
			long pa_id = floorPurchaseApproveMgrZJ.addPuchaseApprove(dbrow);
			
			for (int i = 0; i < differences.length; i++) 
			{
				String product_name = differences[i].getString("purchase_name");
				String product_code = differences[i].getString("proudct_barcod");
				long product_id = differences[i].get("product_id",0l);
				float purchase_count = differences[i].get("purchase_count",0f);
				float reap_count = differences[i].get("reap_count",0f);
				
				DBRow difference = new DBRow();
				difference.add("product_name",product_name);
				difference.add("product_code",product_code);
				difference.add("product_id",product_id);
				difference.add("purchase_count",purchase_count);
				difference.add("reap_count",reap_count);
				difference.add("pa_id",pa_id);
				
				floorPurchaseApproveMgrZJ.addPurchaseApproveDifference(difference);
			}
			
			DBRow para = new DBRow();
			para.add("arrival_time",PurchaseArriveKey.WAITAPPROVE);
			para.add("purchase_status",PurchaseKey.APPROVEING);
			floorPurchaseMgr.updatePurchase(purchase_id, para);//修改采购单为待审核状态
			
			floorPurchaseMgrLL.insertLogs(Long.toString(purchase_id), "申请完成:采购单审核", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
			addPurchasefollowuplogs(PurchaseLogTypeKey.PRICE,PurchaseKey.APPROVEING, purchase_id, adminLoggerBean.getEmploye_name()+"于"+DateUtil.NowStr()+"申请完成:采购单审核", adminLoggerBean.getAdid(),adminLoggerBean.getEmploye_name());
			((PurchaseApproveMgrIFaceZJ)AopContext.currentProxy()).purcahseApproveForJbpm(pa_id);//工作流拦截采购单申请审核
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"addPuchaseApprove",log);
		}
	}
	private long addPurchasefollowuplogs(int followup_type,int followTypeSub, long purchase_id,String followup_content,long follower_id,String follower)
	throws Exception
	{
		DBRow dbrow=new DBRow();
		dbrow.add("followup_type", followup_type);
		dbrow.add("followup_type_sub", followTypeSub);
		dbrow.add("purchase_id", purchase_id);
		dbrow.add("followup_content",followup_content);
		dbrow.add("follower_id",follower_id);
		dbrow.add("follower",follower);
		dbrow.add("followup_date",DateUtil.NowStr());
		return (floorPurchaseMgr.addPurchasefollowuplogs(dbrow));
	}
	
	/**
	 * 提供工作流拦截采购单申请审核
	 * @param pa_id
	 * @return
	 * @throws Exception
	 */
	public long purcahseApproveForJbpm(long pa_id)
		throws Exception
	{
		return (pa_id);
	}
	
	/**
	 * 获得全部采购单审核，支持按仓库查找,审核状态，提交与审核日期排序
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allPurchaseApprove(long ps_id,PageCtrl pc,int approve_status,String sorttype)
		throws Exception
	{
		try 
		{
			return (floorPurchaseApproveMgrZJ.allPurchaseApprove(ps_id, pc,approve_status,sorttype));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"allPurchaseApprove",log);
		}
	}
	
	
	/**
	 * 根据采购单审核单ID获得采购单需审核差异详细
	 * @param pa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseApproveDifferenceByPaid(long pa_id,PageCtrl pc)
	throws Exception
	{
		try 
		{
			return (floorPurchaseApproveMgrZJ.getPurchaseApproveDifferenceByPaid(pa_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getPurchaseApproveDifferenceByPaid",log);
		}
	}
	
	
	
	/**
	 * 采购单差异进行审核
	 * @param pa_id
	 * @param pdd_ids
	 * @param notes
	 * @throws Exception
	 */
	public void approvePurchaseDifferent(long pa_id,long[] pdd_ids,String[] notes,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			for (int i = 0; i < pdd_ids.length; i++) 
			{
				DBRow modpara = new DBRow();
				modpara.add("note",notes[i]);
				modpara.add("approve_account",adminLoggerBean.getEmploye_name());
				modpara.add("approve_status",ApprovePurchaseKey.APPROVE);
				
				floorPurchaseApproveMgrZJ.modPurchaseApproveDifference(pdd_ids[i],modpara);
			}
			
			DBRow[] noApprove = floorPurchaseApproveMgrZJ.getPurchaseApproveDifferentsWithStatus(pa_id,ApprovePurchaseKey.WAITAPPROVE);//查找未审核的不同
			DBRow[] hasApprove = floorPurchaseApproveMgrZJ.getPurchaseApproveDifferentsWithStatus(pa_id,ApprovePurchaseKey.APPROVE);//查找已审核的不同
			
			
			DBRow modApprovePurchase = new DBRow();
			
			long purchase_id = floorPurchaseApproveMgrZJ.getDetailPurchaseApproveByPaid(pa_id).get("purchase_id",0l);
			if(noApprove.length == 0)//差异都已审核过，变更采购单审核状态
			{
				modApprovePurchase.add("approve_status",1);
				modApprovePurchase.add("approve_account",adminLoggerBean.getEmploye_name());
				modApprovePurchase.add("approve_date",DateUtil.NowStr());
				
				modApprovePurchase.add("difference_approve_count",hasApprove.length);
				floorPurchaseApproveMgrZJ.modPuchaseApprove(pa_id,modApprovePurchase);
				
				//采购单更改状态
				
				
				DBRow updatepara = new DBRow();
				TDate tDate = new TDate();
				DBRow purchaseRow = floorPurchaseMgrLL.getPurchaseById(Long.toString(purchase_id));
				updatepara.add("purchase_over", tDate.getDiffDate(purchaseRow.getString("purchase_date"), "dd"));
				updatepara.add("purchase_status",PurchaseKey.FINISH);
				updatepara.add("arrival_time",PurchaseArriveKey.ALLARRIVE);
				
				floorPurchaseMgr.updatePurchase(purchase_id,updatepara);
				
				deliveryMgrZJ.delDeliveryOrderNoFinishByPurchaseFinish(purchase_id);//采购单完成未完成交货单删除
				
			}
			else
			{
				modApprovePurchase.add("difference_approve_count",hasApprove.length);
				floorPurchaseApproveMgrZJ.modPuchaseApprove(pa_id,modApprovePurchase);
			}
			floorPurchaseMgrLL.insertLogs(Long.toString(purchase_id), "采购单审核:"+(noApprove.length==0?"完成审核":"部分审核"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"approvePurchaseDifferent",log);
		}
	}

	public void setFloorPurchaseApproveMgrZJ(
			FloorPurchaseApproveMgrZJ floorPurchaseApproveMgrZJ) {
		this.floorPurchaseApproveMgrZJ = floorPurchaseApproveMgrZJ;
	}

	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}

	public void setDeliveryMgrZJ(DeliveryMgrIFaceZJ deliveryMgrZJ) {
		this.deliveryMgrZJ = deliveryMgrZJ;
	}

}
