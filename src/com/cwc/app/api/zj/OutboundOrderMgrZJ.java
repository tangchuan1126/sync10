package com.cwc.app.api.zj;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorConfigChangeMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutListDetailMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutboundOrderMgrZJ;
import com.cwc.app.iface.zj.OutboundOrderMgrIFaceZJ;
import com.cwc.app.key.ChangeStatusKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class OutboundOrderMgrZJ implements OutboundOrderMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ;
	private FloorOutListDetailMgrZJ floorOutListDetailMgrZJ;
	private FloorConfigChangeMgrZJ floorConfigChangeMgrZJ;
	
	/**
	 * 计算补偿值
	 */
	public DBRow[] compensationValueForPs(long ps_id, int[] status, long pc_id)
		throws Exception 
	{
		try 
		{
			return floorOutboundOrderMgrZJ.compensationValueForPs(ps_id, status, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compensationValueForPs",log);
		}
	}

	/**
	 * 对某一仓库下的一种商品计算补偿值
	 */
	public DBRow compensationValueForPsAndP(long ps_id, int[] status, long pc_id)
		throws Exception 
	{
		try 
		{
			return floorOutboundOrderMgrZJ.compensationValueForPsAndP(ps_id, status, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"compensationValueForPsAndP",log);
		}
	}
	
	/**
	 * 对某一仓库下的一种商品的出库值（只计算运单）
	 * @param ps_id
	 * @param status
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow outStoreValueForPsAndP(long ps_id,int[] status,long pc_id)
		throws Exception
	{
		try 
		{
			return floorOutboundOrderMgrZJ.outStoreValueForPsAndP(ps_id, status, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"outStoreValueForPsAndP",log);
		}
	}
	
	/**
	 * 获得出货单内需出货商品（status标识哪些需要算入统计）
	 * @param out_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutOrderProductItems(long out_id, int[] status) 
		throws Exception 
	{
		
		try 
		{
			return floorOutboundOrderMgrZJ.getOutOrderItem(out_id, status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOutOrderProductItems",log);
		}
	}
	
	/**
	 * 获得出库单的位置
	 * @param out_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutOrderItemLocation(long out_id,int[] status)
		throws Exception
	{
		try 
		{
			return floorOutboundOrderMgrZJ.getOutOrderItemLocation(out_id, status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOutOrderItemLocation",log);
		}
	}
	
	/**
	 * 根据ID获得出库单
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailOutboundById(long out_id)
		throws Exception
	{
		try 
		{
			return floorOutboundOrderMgrZJ.getDetailOutboundById(out_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailOutboundById",log);
		}
	}
	
	/**
	 * 根据出库单与商品获得运单集合
	 */
	public DBRow[] getWayBillIdsByOutIdAndP(long out_id,long pc_id,int[] status)
		throws Exception
	{
		return floorOutboundOrderMgrZJ.getWayBillIdsByOutIdAndP(out_id, pc_id,status);
	}
	
	/**
	 * 修改运单出库单状态
	 * @param request
	 * @throws Exception
	 */
	public void modOutboundOrder(HttpServletRequest request)
		throws Exception
	{
		long out_id = StringUtil.getLong(request,"out_id");
		
		DBRow para = new DBRow();
		para.add("state",0);
		
		floorOutboundOrderMgrZJ.modOutboundOrder(out_id, para);
	}
	
	/**
	 * 整合运单的拣货单明细
	 * @param out_id
	 * @throws Exception
	 */
	public void summarizeWaybillOutListDetail(long out_id)
		throws Exception
	{
		DBRow[] outStoreBillDetails = floorOutboundOrderMgrZJ.summarizeWaybillOutListDetail(out_id);
		
		for (int i = 0; i < outStoreBillDetails.length; i++) 
		{
			DBRow outStoreBillDetail = new DBRow();
			outStoreBillDetail.add("slc_id",outStoreBillDetails[i].get("slc_id",0l));
			outStoreBillDetail.add("pc_id",outStoreBillDetails[i].get("pc_id",0l));
			outStoreBillDetail.add("quantity",outStoreBillDetails[i].get("quantity",0f));
			outStoreBillDetail.add("need_execut_quantity",outStoreBillDetails[i].get("quantity",0f));
			outStoreBillDetail.add("out_id",out_id);
			outStoreBillDetail.add("area_id",outStoreBillDetails[i].get("slc_area",0l));
			
			floorOutboundOrderMgrZJ.addOutStoreBillOrderDetail(outStoreBillDetail);
		}
		
		//确定拣货单的区域
		long area_id = floorOutListDetailMgrZJ.getOutArea(out_id);
		DBRow para = new DBRow();
		para.add("area_id",area_id);
		floorOutboundOrderMgrZJ.modOutboundOrder(out_id, para);
	}
	
	/**
	 * 整合转运单的拣货单明细
	 * @param out_id
	 * @throws Exception
	 */
	public void summarizeTransportOutListDetail(long out_id)
		throws Exception
	{
		DBRow[] outStoreBillDetails = floorOutboundOrderMgrZJ.summarizeTransportOutListDetail(out_id);
		
		for (int i = 0; i < outStoreBillDetails.length; i++) 
		{
			DBRow outStoreBillDetail = new DBRow();
			outStoreBillDetail.add("slc_id",outStoreBillDetails[i].get("slc_id",0l));
			outStoreBillDetail.add("pc_id",outStoreBillDetails[i].get("pc_id",0l));
			outStoreBillDetail.add("quantity",outStoreBillDetails[i].get("quantity",0f));
			outStoreBillDetail.add("need_execut_quantity",outStoreBillDetails[i].get("quantity",0f));
			outStoreBillDetail.add("out_id",out_id);
			outStoreBillDetail.add("area_id",outStoreBillDetails[i].get("slc_area",0l));
			String serialNumber = outStoreBillDetails[i].getString("out_list_serial_number");
			if (serialNumber.length()>0)
			{
				outStoreBillDetail.add("serial_number",serialNumber);
			}
			
			
			floorOutboundOrderMgrZJ.addOutStoreBillOrderDetail(outStoreBillDetail);
		}
		
		//确定拣货单的区域
		long area_id = floorOutListDetailMgrZJ.getOutArea(out_id);
		DBRow para = new DBRow();
		para.add("area_id",area_id);
		floorOutboundOrderMgrZJ.modOutboundOrder(out_id, para);
	}
	
	/**
	 * 为configChange创建拣货单
	 */
	public void addOutStorebillOrderForConfigChange(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long cc_id = StringUtil.getLong(request,"cc_id");
			DBRow configChange = floorConfigChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
			String ids = String.valueOf(cc_id);
			
			DBRow row=new DBRow();
			
			row.add("create_time",DateUtil.NowStr());  //创建时间
			row.add("ps_id",configChange.get("cc_psid",0l));   //出货仓库
			row.add("state",1);
			row.add("out_for_type",ProductStoreBillKey.CONFIG_CHANGE);
			
			long out_id = floorOutboundOrderMgrZJ.addOutStorebillOrder(row);
			
			DBRow[] outStorebillOrderDetails= floorOutboundOrderMgrZJ.summarizeSystemBillForType(ids,ProductStoreBillKey.CONFIG_CHANGE);
			for (int i = 0; i < outStorebillOrderDetails.length; i++) 
			{
				DBRow outStorebillOrderDetail = new DBRow();
				outStorebillOrderDetail.add("out_list_pc_id",outStorebillOrderDetails[i].get("out_list_pc_id",0l));
				outStorebillOrderDetail.add("out_list_slc_id",outStorebillOrderDetails[i].get("out_list_slc_id",0l));
				outStorebillOrderDetail.add("out_list_area_id",outStorebillOrderDetails[i].get("out_list_area_id",0l));
				outStorebillOrderDetail.add("out_list_serial_number",outStorebillOrderDetails[i].getString("out_list_serial_number"));
				outStorebillOrderDetail.add("pick_up_quantity",outStorebillOrderDetails[i].get("sum_quantity",0d));
				outStorebillOrderDetail.add("need_execut_quantity",outStorebillOrderDetails[i].get("sum_quantity",0d));
				outStorebillOrderDetail.add("system_bill_id",outStorebillOrderDetails[i].get("system_bill_id",0l));
				outStorebillOrderDetail.add("system_bill_type",outStorebillOrderDetails[i].get("system_bill_type",0l));
				outStorebillOrderDetail.add("create_adid",outStorebillOrderDetails[i].get("create_adid",0l));
				outStorebillOrderDetail.add("create_time",outStorebillOrderDetails[i].getString("create_time"));
				outStorebillOrderDetail.add("ps_location_id",outStorebillOrderDetails[i].get("ps_location_id",0l));
				outStorebillOrderDetail.add("from_container_type",outStorebillOrderDetails[i].get("from_container_type",0l));
				outStorebillOrderDetail.add("from_container_type_id",outStorebillOrderDetails[i].get("from_container_type_id",0l));
				outStorebillOrderDetail.add("from_con_id",outStorebillOrderDetails[i].get("from_con_id",0l));
				outStorebillOrderDetail.add("pick_container_type",outStorebillOrderDetails[i].get("pick_container_type",0l));
				outStorebillOrderDetail.add("pick_container_type_id",outStorebillOrderDetails[i].get("pick_container_type_id",0l));
				outStorebillOrderDetail.add("pick_con_id",outStorebillOrderDetails[i].get("pick_con_id",0l));
				outStorebillOrderDetail.add("pick_container_quantity",outStorebillOrderDetails[i].get("count",0l));
				outStorebillOrderDetail.add("out_storebill_order",out_id);
				
				floorOutboundOrderMgrZJ.addOutStoreBillOrderDetail(outStorebillOrderDetail);
			}
			
			DBRow configChangePara = new DBRow();
			configChangePara.add("cc_status",ChangeStatusKey.Picking);
			configChangePara.add("cc_out_id",out_id);
			
			floorConfigChangeMgrZJ.modConfigChange(cc_id,configChangePara);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOutStorebillOrderForConfigChange",log);
		}
		
	}

	/**
	 * 根据系统单据明细商品获得拣货的CLP
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param clp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetailByCLP(long pc_id,int system_bill_type,long system_bill_id,long clp_type_id)
		throws Exception
	{
		try 
		{
			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(pc_id, system_bill_type, system_bill_id,ContainerTypeKey.CLP,clp_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException("OutboundOrderMgrZJ getOutListDetailByCLP error:"+e);
		}
	}
	
	/**
	 * 根据系统单据明细商品获得拣货BLP
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param blp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetailByBLP(long pc_id,int system_bill_type,long system_bill_id,long blp_type_id)
		throws Exception
	{
		try 
		{
//			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(pc_id, system_bill_type, system_bill_id, ContainerTypeKey.BLP,blp_type_id);//去掉ILP和BLP
			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(pc_id, system_bill_type, system_bill_id, 0,blp_type_id);//去掉ILP和BLP
		} 
		catch (Exception e) 
		{
			throw new SystemException("OutboundOrderMgrZJ getOutListDetailByBLP error:"+e);
		}
	}
	
	/**
	 * 根据系统单据明细商品获得ILP
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param ilp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetailByILP(long pc_id,int system_bill_type,long system_bill_id,long ilp_type_id)
		throws Exception
	{
		try 
		{
//			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(pc_id, system_bill_type, system_bill_id, ContainerTypeKey.ILP,ilp_type_id);//去掉ILP和BLP
			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(pc_id, system_bill_type, system_bill_id, 0,ilp_type_id);//去掉ILP和BLP
		}
		catch (Exception e) 
		{
			throw new SystemException();
		}
	}
	
	public int getOutListContainerCountBySystemBill(int system_bill_type,long system_bill_id)
		throws Exception
	{
		try
		{
			DBRow[] containers = floorOutListDetailMgrZJ.getOutListDetailByContainerType(0, system_bill_type, system_bill_id, 0, 0);
			
			return containers.length;
		} 
		catch (Exception e) 
		{
			throw new SystemException("OutboundOrderMgrZJ getOutListContainerCountBySystemBill error:"+e);
		}
	}

	public void setFloorOutboundOrderMgrZJ(
			FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ) {
		this.floorOutboundOrderMgrZJ = floorOutboundOrderMgrZJ;
	}

	public void setFloorOutListDetailMgrZJ(
			FloorOutListDetailMgrZJ floorOutListDetailMgrZJ) {
		this.floorOutListDetailMgrZJ = floorOutListDetailMgrZJ;
	}

	public void setFloorConfigChangeMgrZJ(
			FloorConfigChangeMgrZJ floorConfigChangeMgrZJ) {
		this.floorConfigChangeMgrZJ = floorConfigChangeMgrZJ;
	}
}
