package com.cwc.app.api.zj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.key.GlobalKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.WmsItemUnitTicketKey;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.key.WmsOrderTypeKey;
import com.cwc.app.key.WmsSearchItemStatusKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.fr.base.StringUtils;

public class SQLServerMgrZJ implements SQLServerMgrIFaceZJ {

	 
	private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
	private TransactionTemplate sqlServerTransactionTemplate;
	private FloorAdminMgr floorAdminMgr;
	
	
	public DBRow[] getLoading(HttpServletRequest request) 
		throws Exception 
	{
		
		int number_type =  StringUtil.getInt(request, "number_type");
		String search_number =  StringUtil.getString(request, "search_number");
		String ps_name =  StringUtil.getString(request, "ps_name");
		
//		return floorSQLServerMgrZJ.getLoadingAll();
		
		return floorSQLServerMgrZJ.getLoadingSQLServer(search_number, ps_name,number_type);
	}
	
	public DBRow[] getLoading(int number_type,String search_number,String ps_name)throws Exception{
		return floorSQLServerMgrZJ.getLoadingSQLServer(search_number, ps_name,number_type);
	}
	
	public DBRow[] findMasterBolsByLoadNo(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		DBRow[] bolRows = new DBRow[0];
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			bolRows=floorSQLServerMgrZJ.findMasterBolByLoadNo(loadNo,CompanyID,CustomerID,status);
			for (int i = 0; i < bolRows.length; i++)
			{
				DBRow[] orderRows = floorSQLServerMgrZJ.findB2BOrderItemPlatesWmsByMasterBolNo(bolRows[i].get("MasterBOLNo", 0L), bolRows[i].getString("CompanyID"),status);
				int error_order_status = 0;
				String orderNo = "";
				String itemId = "";
				for (int j = 0; j < orderRows.length; j++) 
				{
					DBRow[] itemRows	= floorSQLServerMgrZJ.findItemInfoByItemId(orderRows[j].getString("ItemID"), orderRows[j].getString("CompanyID"), orderRows[j].getString("CustomerID"));
					if(0 == itemRows.length)
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST;
						orderNo = orderRows[j].getString("OrderNo");
						itemId = orderRows[j].getString("ItemID");
					}
					else if(itemRows.length > 1)
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_MORE;
						orderNo = orderRows[j].getString("OrderNo");
						itemId = orderRows[j].getString("ItemID");
					}
					else
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_CORRECT;
					}
					if(0 != error_order_status)
					{
						break;
					}
				}
				bolRows[i].add("error_order_no", orderNo);
				bolRows[i].add("error_order_item", itemId);
				bolRows[i].add("error_order_status", error_order_status);
			}
		}
		return bolRows;
	}
	
	public DBRow[] findB2BOrderItemPlatesWmsByMasterBolNo(long masterBOLNo, String companyId, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		DBRow[] orderRows = floorSQLServerMgrZJ.findB2BOrderItemPlatesWmsByMasterBolNo(masterBOLNo, companyId,status);
		for (int i = 0; i < orderRows.length; i++) 
		{
			DBRow[] itemRows	= floorSQLServerMgrZJ.findItemInfoByItemId(orderRows[i].getString("ItemID"), orderRows[i].getString("CompanyID"), orderRows[i].getString("CustomerID"));
			if(0 == itemRows.length)
			{
				orderRows[i].add("item_unit_info", WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST);
			}
			else if(itemRows.length > 1)
			{
				orderRows[i].add("item_unit_info", WmsSearchItemStatusKey.ITEM_UNIT_MORE);
			}
			else
			{
				orderRows[i].add("item_unit_info", WmsSearchItemStatusKey.ITEM_UNIT_CORRECT);
			}	
		}
		return orderRows;
	}
	
	public DBRow findLocationWmsByPlateNo(int plateNo, String companyId) throws Exception
	{
		return floorSQLServerMgrZJ.findLocationWmsByPlateNo(plateNo, companyId);
	}
	
	public DBRow findOrderCaseQtyByItemIdQty(String ItemID, String CompanyID, double qty,  String CustomerID) throws Exception
	{
		DBRow[] itemRows	= floorSQLServerMgrZJ.findItemInfoByItemId(ItemID, CompanyID, CustomerID);
		if(itemRows.length > 0)
		{
			DBRow itemRow		= itemRows[0];
			String Unit			= itemRow.getString("Unit");
			double QtyPerUnit		= itemRow.get("QtyPerUnit", 1);
			double UnitsPerPackage	= itemRow.get("UnitsPerPackage", 1);
			int caseQty				= 0;
			int pieceQty			= 0;
			int innerQty			= 0;
			int caseSumQty			= 0;
			
			//37  6 5
			if("Package".equals(Unit))
			{
				caseQty		= (int)qty;
				pieceQty	= 0;
				innerQty	= 0;
				caseSumQty	= (int)qty;
			}
			else if("Pallet".equals(Unit))
			{
				caseQty		= (int)qty;
				pieceQty	= 0;
				innerQty	= 0;
				caseSumQty	= (int)qty;
			}
			else if("Piece".equals(Unit))
			{
				caseQty		= (int)(qty/UnitsPerPackage); //6
				pieceQty	= (int)(qty%UnitsPerPackage); //1
				innerQty	= 0;
				caseSumQty	= caseQty;
				if(0d != pieceQty)
				{
					caseSumQty	= caseSumQty+1;
				}
			}
			else if("Inner Case".equals(Unit))
			{
				caseQty		= (int)(qty/(UnitsPerPackage*QtyPerUnit));   //1
				pieceQty	= (int)(qty%(UnitsPerPackage*QtyPerUnit)/QtyPerUnit);//1
				innerQty	=(int)(qty%(UnitsPerPackage*QtyPerUnit)%QtyPerUnit);//2
				caseSumQty	= caseQty;
				if(0d != qty%(UnitsPerPackage*QtyPerUnit))
				{
					caseSumQty	= caseSumQty+1;
				}
			}
			
			DBRow reRow = new DBRow();
			reRow.add("case_qty", caseQty);
			reRow.add("piece_qty", pieceQty);
			reRow.add("inner_qty", innerQty);
			reRow.add("case_sum_qty", caseSumQty);
			reRow.add("unit", Unit);
			return reRow;
		}
		else
		{
			return new DBRow();
		}
	}
	
	public DBRow findReceiptByContainerNo(String containerNo, long adid, String[] companyID,HttpServletRequest request) throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
		DBRow row = floorSQLServerMgrZJ.findReceiptByContainerNo(containerNo, status,companyID);
		if(null != row && row.get("countDate", 0) > 1)
		{
			row.add("AppointmentDate", "");
		}
		return row;
	}
	
	public DBRow findReceiptByBolNo(String BolNo, long adid, String[] companyID, HttpServletRequest request) throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
		DBRow row = floorSQLServerMgrZJ.findReceiptByBolNo(BolNo, status,companyID);
		if(null != row && row.get("countDate", 0) > 1)
		{
			row.add("AppointmentDate", "");
		}
		return row;
	}
	
	/**
	 * 通过no(可能是bol，也可能是ctn)查询receipts
	 * @param No
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByContainerBolNo1(String No, long adid,String[] companyID, HttpServletRequest request) throws Exception
	{
		DBRow row = null;
		long number_type=0;
		if(!StringUtil.isBlank(No))
		{
			String[] status = deliveryStatusExceptClosed();//getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			row = floorSQLServerMgrZJ.findReceiptByContainerNo(No,status,companyID);
			number_type=1;
			if(null == row)
			{
				row = floorSQLServerMgrZJ.findReceiptByBolNo(No,status,companyID);
				number_type=2;
			
			}
			if(null != row && row.get("countDate", 0) > 1)
			{
				row.add("AppointmentDate", "");
				
			}
			if(null != row){
				row.add("number_type", number_type);
			}
			
		}
		return row;
	}
	
	
	public DBRow findOrderInfoByLoadNo(String loadNo, long adid, HttpServletRequest request) throws Exception
	{
		DBRow row = null;
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			row = floorSQLServerMgrZJ.findMasterInfoByLoadNo(loadNo, status);
			if(null == row)
			{
				row = floorSQLServerMgrZJ.findOrderInfoByLoadNo(loadNo, status);
			}
		}
		return row;
	}
	
	
	public DBRow findPalletsByOrderNoCompanyId(String CompanyID, long OrderNo) throws Exception
	{
		return floorSQLServerMgrZJ.findPalletsByOrderNoCompanyId(CompanyID, OrderNo);
	}
	
	//通过loadNo，更新相关的window checkin date, seals,vehicle,dockId,carrierId
	public void updateLoadMasterOrderShipDateSealVehicleDockCarrier(String shipDate, String seals, String vehicle
			, String dockName, String carrierName, final String order_no,String OrderNo,int order_type, long adid, String companyID,String customerID, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(order_no))
		{
			DBRow row = new DBRow();
			
			if(!StringUtil.isBlank(seals))
			{
				row.add("Seals", seals);
			}
			if(!StringUtil.isBlank(vehicle))
			{
				row.add("VehicleNo", vehicle);
			}
			if(!StringUtil.isBlank(shipDate))
			{
				row.add("ShippedDate", shipDate);
			}
			if(!StringUtil.isBlank(dockName))
			{
				row.add("DockID", dockName);
			}
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow masterRow = floorSQLServerMgrZJ.findOneOfMasterBolsByLoadNo(order_no,status);
			if(ModuleKey.CHECK_IN_LOAD == order_type)
			{
				String appointmentDate = "";
				if(!StringUtil.isBlank(carrierName))
				{
					DBRow carrier = floorSQLServerMgrZJ.findCarrierInfoByCarrierName(carrierName, null!=masterRow?masterRow.getString("CompanyID"):"");
					if(null != carrier)
					{
						row.add("CarrierID", carrier.getString("CarrierID"));
					}
				}
				
				appointmentDate = null!=masterRow?masterRow.getString("AppointmentDate"):"";
				floorSQLServerMgrZJ.updateMasterBol(row, order_no,companyID,customerID,status);
				//更新order
				updateLoadOrdersShipDateSealVehicleDockCarrier(row, order_no, adid,companyID,customerID, appointmentDate, request);
			}
			else if(ModuleKey.CHECK_IN_ORDER == order_type || ModuleKey.CHECK_IN_PONO == order_type)
			{
				if(!StrUtil.isBlankAndCanParseLong(OrderNo))
				{
					floorSQLServerMgrZJ.updateOrder(row, Long.parseLong(OrderNo), companyID, customerID, status);
				}
			}
		}
	}
	
	//通过loadNo，更新相关orders的window checkin date, seals,vehicle,dockId,carrierId
	public void updateLoadOrdersShipDateSealVehicleDockCarrier(DBRow updateRow, String loadNo, long adid, String companyID,String customerID,String appointment, HttpServletRequest request) throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		DBRow[] orders = floorSQLServerMgrZJ.findMasterBolLinesByLoadNo(loadNo,companyID,customerID,status);
		for (int i = 0; i < orders.length; i++) 
		{
			String companyId	= orders[i].getString("CompanyID");
			long orderNo		= orders[i].get("OrderNo", 0L);
			String appointmentDate = orders[i].getString("AppointmentDate");
			if(StrUtil.isBlank(appointmentDate) && !StrUtil.isBlank(appointment))
			{
				updateRow.add("AppointmentDate", appointment);
			}
			floorSQLServerMgrZJ.updateOrder(updateRow, orderNo, companyId, customerID, status);
		}
	}
	
	
	public DBRow[] findMasterBolByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolByLoadNo(loadNo, companyId, customerId, status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	
	public DBRow findMasterBolSomeInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolSomeInfoByLoadNo(loadNo, companyId, customerId,status);
		}
		else
		{
			return new DBRow();
		}
	}
	
	
	public DBRow[] findLoadNoOrdersItemInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersItemInfoByLoadNo(loadNo, companyId, customerId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	public DBRow[] findLoadNoOrdersPONoInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByLoadNo(loadNo, companyId, customerId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	public DBRow findOrderSomeInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderSomeInfoByOrderNo(orderNo, companyId,status);
	}
	
	public DBRow[] findOrderItemsInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderItemsInfoByOrderNo(orderNo, companyId,status);
	}
	
	public DBRow findOrderPONoInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderPONoInfoByOrderNo(orderNo, companyId,status);
	}
	
	public DBRow findCarrierInfoByCarrierId(String carrierId, String companyId)throws Exception
	{
		return floorSQLServerMgrZJ.findCarrierInfoByCarrierId(carrierId, companyId);
	}
	
	public DBRow findCarrierInfoByCarrierName(String carrierName, String companyId) throws Exception
	{
		return floorSQLServerMgrZJ.findCarrierInfoByCarrierName(carrierName, companyId);
	}
	
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolLinesByLoadNo(loadNo,companyId,customerId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	public void addMasterBolAndOrderDatas(String startTime, String endTime, String companyId, long adid, HttpServletRequest request)throws Exception
	{
		//MasterBols	    "2014-06-01","2014-06-11","W12"
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		DBRow[] masterBolRows = floorSQLServerMgrZJ.findMasterBols(startTime, endTime, companyId,status);
		this.addMasterBols(masterBolRows);
		for (int i = 0; i < masterBolRows.length; i++) 
		{
			//MasterBolLines
			DBRow[] masterBolLineRows = floorSQLServerMgrZJ.findMasterBolLinesByMasterNo(masterBolRows[i].get("MasterBOLNo", 0L), masterBolRows[i].getString("CompanyID"));
			this.addMasterBolLines(masterBolLineRows);
			for (int j = 0; j < masterBolLineRows.length; j++)
			{
				//Orders
				DBRow[] orderRows = floorSQLServerMgrZJ.findOrdersByMasterNo(masterBolLineRows[j].get("MasterBOLNo", 0L), masterBolLineRows[j].getString("CompanyID"),status);
				this.addOrders(orderRows);
				for (int k = 0; k < orderRows.length; k++)
				{
					//OrderLines
					DBRow[] orderLineRows = floorSQLServerMgrZJ.findOrderLinesByOrderNoAndCompanyId(orderRows[k].get("OrderNo", 0L), orderRows[k].getString("CompanyID"));
					this.addOrderLines(orderLineRows);
					//OrderLinePlates
					DBRow[] orderLinePlateRows = floorSQLServerMgrZJ.findOrderLinePlatesByOrderNoAndCompanyId(orderRows[k].get("OrderNo", 0L), orderRows[k].getString("CompanyID"));
					this.addOrderLinePlates(orderLinePlateRows);
				}
			}
		}
	}
	
	public void addMasterBols(DBRow[] masterBols)throws Exception
	{
		for (int i = 0; i < masterBols.length; i++)
		{
			floorSQLServerMgrZJ.addMasterBols(masterBols[i]);
		}
	}
	public void addMasterBolLines(DBRow[] rows)throws Exception
	{
		for (int i = 0; i < rows.length; i++)
		{
			floorSQLServerMgrZJ.addMasterBOLLines(rows[i]);
		}
	}
	public void addOrders(DBRow[] rows)throws Exception
	{
		for (int i = 0; i < rows.length; i++)
		{
			floorSQLServerMgrZJ.addOrders(rows[i]);
		}
	}
	public void addOrderLines(DBRow[] rows)throws Exception
	{
		for (int i = 0; i < rows.length; i++)
		{
			floorSQLServerMgrZJ.addOrderLines(rows[i]);
		}
	}
	public void addOrderLinePlates(DBRow[] rows)throws Exception
	{
		for (int i = 0; i < rows.length; i++)
		{
			floorSQLServerMgrZJ.addOrderLinePlates(rows[i]);
		}
	}
	
	//通过bolNo,containerNo查询Receipts
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(bolNo) || !StringUtil.isBlank(containerNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			return floorSQLServerMgrZJ.findReceiptsByBolNoOrContainerNo(bolNo, containerNo, CompanyID,CustomerID,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	//通过containerNo，ReceiptsNo获得明细
	public DBRow[] findReceiptLinesPalteByBolNoOrCtnNO(String bolNo, String containerNo, long receiptsNo, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
		return floorSQLServerMgrZJ.findReceiptLinesPalteByBolNoOrCtnNO(bolNo, containerNo, receiptsNo,status);
	}
	
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrdersNoAndCompanyByLoadNo(loadNo,companyId,customerId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	public DBRow[] findMasterBolsSomeInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolsSomeInfoByLoadNo(loadNo,companyId,customerId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	public DBRow findItemSumCountWeightByMasterBolCompany(long mbol, String companyID, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findItemSumCountWeightByMasterBolCompany(mbol, companyID,status);
	}
	
	/**
	 * receipts:回写时间
	 * @param inYardDate
	 * @param devannedDate
	 * @param ctnNo
	 * @param bolNo
	 * @throws Exception
	 */
	public void updateReceiptInYardDateByBolOrCtnrNo(String inYardDate, String bolNo, String ctnNo, String SupplierID,String companyID, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(bolNo) || !StringUtil.isBlank(ctnNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			DBRow row = new DBRow();
			if(!StringUtil.isBlank(inYardDate))
			{
				row.add("InYardDate", inYardDate);
				if(!StringUtil.isBlank(bolNo))
				{
					floorSQLServerMgrZJ.updateReceiptsInYardDateByBolNo(bolNo,status,SupplierID,companyID, row);
				}
				else if(!StringUtil.isBlank(ctnNo))
				{
					floorSQLServerMgrZJ.updateReceiptsInYardDateByCtnNo(ctnNo,status,SupplierID,companyID, row);
				}
			}
		}
	}
	
	/**
	 *  查询所有卡车公司名称，去重，CarrierName
	 */
	public DBRow[] findAllCarriersNameNoRepeat() throws Exception
	{
		return floorSQLServerMgrZJ.findAllCarriersNameNoRepeat();
	}
	
	public long findMaxOrderNoByCompanyId(String companyID) throws Exception
	{
		return floorSQLServerMgrZJ.findMaxOrderNoByCompanyId(companyID);
	}
	
	
	public long addOrders(DBRow row) throws Exception
	{
		return floorSQLServerMgrZJ.addOrders(row);
	}
	
	public long addOrderLines(DBRow row)throws Exception
	{
		return floorSQLServerMgrZJ.addOrderLines(row);
	}
	
	public DBRow[] findItemInfoByItemId(String ItemID, String CompanyID, String CustomerID) throws Exception
	{
		return floorSQLServerMgrZJ.findItemInfoByItemId(ItemID, CompanyID, CustomerID);
	}
	
	public DBRow findShipToInfoByCustomerAndAccountAddress(String CustomerID, String AccountID, String address) throws Exception
	{
		return floorSQLServerMgrZJ.findShipToInfoByCustomerAndAccountAddress(CustomerID, AccountID, address);
	}
	
	public DBRow[] findCarrierWarehouseInfo() throws Exception
	{
		return floorSQLServerMgrZJ.findCarrierWarehouseInfo();
	}
	
	/**
	 * 查询customerAccount,a4打印模版名
	 */
	public DBRow[] findMasterOrBolLabelFormatByAccountCompanyCustomer(String AccountID, String CompanyID, String CustomerID) throws Exception
	{
		return floorSQLServerMgrZJ.findMasterOrBolLabelFormatByAccountCompanyCustomer(AccountID, CompanyID, CustomerID);
	}

	/**
	 * 通过loadNo获取MasterBolNo和OrderNo，格式   m1:o1,o2; m2:o3  24253:149200;24254:149201
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public String findMasterBolNoOrderNoStrByLoadNo(String loadNo, long adid) throws Exception
	{
		String MbolOrderNo = "";
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = pickStatusExceptClosed();
					//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] mbolRows = floorSQLServerMgrZJ.findMasterBolsByLoadNo(loadNo, "", "",status);
			for (int i = 0; i < mbolRows.length; i++) 
			{
				long mbolNo = mbolRows[i].get("MasterBOLNo", 0L);
				String companyId = mbolRows[i].getString("CompanyID");
				MbolOrderNo += ";"+mbolNo+",";
				DBRow[] orderLines = floorSQLServerMgrZJ.findMasterBolLinesByMasterNo(mbolNo, companyId);
				
				String orderNoStr = "";
				for (int j = 0; j < orderLines.length; j++) 
				{
					orderNoStr += ","+orderLines[j].get("OrderNo", 0L);
				}
				if(orderNoStr.length() > 0)
				{
					orderNoStr = orderNoStr.substring(1);
				}
				MbolOrderNo += orderNoStr;
			}
			if(MbolOrderNo.length() > 0)
			{
				MbolOrderNo = MbolOrderNo.substring(1);
			}
		}
		return MbolOrderNo;
	}
	
	
	/**
	 * 通过LoadNo得到Master数量
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public int findMasterBolNoCountByLoadNo(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolNoCountByLoadNo(loadNo, CompanyID, CustomerID,status);
		}
		else
		{
			return 0;
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByLoadNo(String loadNo
			,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrdersSomeInfoWmsByLoadNo(loadNo,CompanyID,CustomerID,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param OrderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersItemsWmsByOrderNo(long OrderNo, String companyId) throws Exception
	{
		return floorSQLServerMgrZJ.findOrdersItemsWmsByOrderNoForTicket(OrderNo, companyId);
	}
	
	
	/**
	 * 通过LoadNo查询companyID和CustomerID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadCompanyIdCustomerIdByLoadNo(String loadNo, long adid, HttpServletRequest request)throws Exception
	{
		if(!StrUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadCompanyIdCustomerIdByLoadNo(loadNo,status);
		}
		return new DBRow[0];
	}
	
	/**
	 * 通过LoadNo查询companyID和CustomerID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderCompanyIdCustomerIdByLoadNo(String loadNo, long adid, HttpServletRequest request)throws Exception
	{
		if(!StrUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrderCompanyIdCustomerIdByLoadNo(loadNo,status);
		}
		return new DBRow[0];
	}
	
	
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsrCompanyIdCustomerIdByBolNoOrContainerNo(String no, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtils.isBlank(no))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			DBRow[] rows = floorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByContainerNo(no,status);
			if(0 == rows.length)
			{
				rows = floorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByBolNo(no,status);
			}
			return rows;
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	/**
	 * 判断根据no是否能够查到receipts
	 * @param no
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptsrCompanyIdCustomerIdTypeByBolNoOrContainerNo(String no, long adid, HttpServletRequest request) throws Exception
	{
		DBRow row = new DBRow();
		DBRow[] rows = new DBRow[0];
		String type = "";
		if(!StringUtils.isBlank(no))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			rows = floorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByContainerNo(no,status);
			if(0 == rows.length)
			{
				rows = floorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByBolNo(no,status);
				if(rows.length != 0)
				{
					type = "BOL";
				}
			}
			else
			{
				type = "CTNR";
			}
			row.add("rows", rows);
			row.add("search_type", type);
			return row;
		}
		else
		{
			return new DBRow();
		}
	}
	
	
	/**
	 * 查预约到达时间为某天的Load
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoAppointTimeBetweenTime(String startTime, String endTime, String[] companyId, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = pickStatusExceptClosed();
		LinkedHashMap<String, DBRow> map = new LinkedHashMap<String, DBRow>();
		DBRow[] loadSuppliers = floorSQLServerMgrZJ.findLoadNoAppointTimeBetweenTime(startTime,endTime,status,companyId);
				//floorSQLServerMgrZJ.findLoadOrderNosAppointTimeBetweenTime(startTime, endTime, status, companyId);
				//floorSQLServerMgrZJ.findLoadNoAppointTimeBetweenTime(startTime,endTime,status,companyId);
//		System.out.println(loadSuppliers.length);
		
		for (int i = 0; i < loadSuppliers.length; i++) 
		{
			String OrderNo = loadSuppliers[i].getString("OrderNo");//LoadNo,OrderNo
			String supplier = loadSuppliers[i].getString("SupplierID");
			if(map.containsKey(OrderNo))
			{
				DBRow row = map.get(OrderNo);
				String suppliers = row.getString("SupplierIDs");
				if(!StrUtil.isBlank(supplier)&&suppliers.indexOf(supplier) == -1)
				{
					suppliers += ","+supplier;
					row.add("SupplierIDs", suppliers);
				}
			}
			else
			{
				DBRow row = loadSuppliers[i];
				if(!StrUtil.isBlank(supplier))
				{
					row.add("SupplierIDs", ","+supplier);
				}
				map.put(OrderNo, row);
			}
		}
		List<DBRow> list = new ArrayList<DBRow>();
		Iterator<Entry<String, DBRow>> it = map.entrySet().iterator();
		while(it.hasNext()){
			Entry<String, DBRow> entry = it.next();
			list.add(entry.getValue());
		}
		DBRow[] rows = (DBRow[])list.toArray(new DBRow[0]);
		return rows;
	}
	
	/**
	 * 查预约到达时间为某天的Receipts
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptBolCtnAppointTimeBetweenTime(String startTime, String endTime, String[] companyId, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = deliveryStatusExceptClosed();
		DBRow[] rows = floorSQLServerMgrZJ.findReceiptBolCtnAppointTimeBetweenTime(startTime,endTime,status, companyId);
		return rows;
	}
	
	/**
	 * 通过load查询bill of lading模版,有Master
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoadCompanyCustomer(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] rows = floorSQLServerMgrZJ.findBillOfLadingTemplateByLoadCompanyCustomerMaster(loadNo, companyId, customerId,status);
			if(rows.length == 0)
			{
				rows = floorSQLServerMgrZJ.findBillOfLadingTemplateByLoadCompanyCustomerNoMaster(loadNo, companyId, customerId,status);
			}
			return rows;
		}
		return new DBRow[0];
	}
	
	
	public DBRow[] findLoadNoOrdersItemInfoGroupByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByLoadNo(loadNo, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	public DBRow[] findOrderItemsInfoForCountingByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderItemsInfoForCountingByOrderNo(orderNo, companyId,status);
	}

	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNo(long MasterBOLNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(0 != MasterBOLNo)
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByMasterBolNo(MasterBOLNo, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNo(long MasterBOLNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(0 != MasterBOLNo)
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByMasterBolNo(MasterBOLNo, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	/**
	 * 通过LoadNo查询MasterBols 某些数据
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoNotOneByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolsSomeInfoNotOneByLoadNo(loadNo, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	
	/**
	 * 通过load查询bill of lading模版,有Master
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoad(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
//			int MasterBolOrBol = 0;//0无数据，1master,2order
			DBRow[] rows = floorSQLServerMgrZJ.findBillOfLadingTemplateByLoadMaster(loadNo, companyId, customerId,status);
//			if(rows.length == 0)
//			{
//				rows = floorSQLServerMgrZJ.findBillOfLadingTemplateByLoadNoMaster(loadNo, companyId, customerId,status);
//				if(rows.length > 0)
//				{
//					MasterBolOrBol = 2;
//				}
//			}
//			else
//			{
//				MasterBolOrBol = 1;
//			}
//			int count = 0;
//			if(rows.length > 0)
//			{
//				count = rows[0].get("cn", 0);
//			}
			for (int i = 0; i < rows.length; i++)
			{
				String LoadNo = rows[i].getString("LoadNo");
				String CompanyID = rows[i].getString("CompanyID");
				String CustomerID = rows[i].getString("CustomerID");
				String AccountID = rows[i].getString("AccountID");
				String ShipToID = rows[i].getString("ShipToID");
				String ShipToName = rows[i].getString("ShipToName");
				String masterBolStr = "";
//				String orderNoStr = "";
//				if(1 == MasterBolOrBol)
//				{
					ShipToID = StrUtil.replaceString(ShipToID, "'", "''");
					ShipToName = StrUtil.replaceString(ShipToName, "'", "''");
					DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolNosByAccountShiptoAddr(LoadNo, CompanyID, CustomerID, AccountID,ShipToID,ShipToName,status);
					for (int j = 0; j < masterBols.length; j++) 
					{
						masterBolStr += ","+masterBols[j].get("MasterBOLNo", 0);
					}
					if(masterBolStr.length() > 0)
					{
						masterBolStr = masterBolStr.substring(1);
					}
					rows[i].add("master_bol_nos", masterBolStr);
					rows[i].add("master_bol_nos_len", masterBols.length);
//				}
//				else
//				{
//					DBRow[] orders = floorSQLServerMgrZJ.findOrderNosByAccountShiptoAddr(LoadNo, CompanyID, CustomerID, AccountID,ShipToID,ShipToName,status);
//					for (int j = 0; j < orders.length; j++) 
//					{
//						orderNoStr += ","+orders[j].get("OrderNo", 0);
//					}
//					if(orderNoStr.length() > 0)
//					{
//						orderNoStr = orderNoStr.substring(1);
//					}
//					rows[i].add("order_nos", orderNoStr);
//					rows[i].add("order_no_len", orders.length);
//				}
			}
			
			return rows;
		}
		return new DBRow[0];
	}
	
	/**
	 * 通过MasterBolNo字符串查询一条MasterBol地址等
	 * @param masterBols
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoOneByMasterBolStr(String loadNo,String masterBols, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		Long[] masterBolArr = parseStrToLongArr(masterBols);
		if(!StringUtil.isBlank(loadNo) && masterBolArr.length > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolsSomeInfoOneByMasterBolArr(loadNo,masterBolArr, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	private Long[] parseStrToLongArr(String str) throws Exception
	{
		List<Long> longList = new ArrayList<Long>();
		if(!StringUtil.isBlank(str))
		{
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StringUtil.isBlankAndCanParseLong(arr[i]))
				{
					if(!"-1".equals(arr[i]))
					{
						longList.add(Long.valueOf(arr[i]));
					}
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	/**
	 * Generic 通过MasterBol字符串查询Items,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNoStr(String loadNo, String MasterBOLNoStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		Long[] masterBolArr = parseStrToLongArr(MasterBOLNoStr);
		if(!StringUtil.isBlank(loadNo) && masterBolArr.length > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByMasterBolNoArr(loadNo,masterBolArr, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNoStr(String loadNo,String MasterBOLNos, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		Long[] masterBolArr = parseStrToLongArr(MasterBOLNos);
		if(!StringUtil.isBlank(loadNo) && masterBolArr.length > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByMasterBolNoArr(loadNo,masterBolArr, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	public DBRow[] findMasterBolLinesByMasterBolStr(String loadNo, String masterBolStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		Long[] masterBolArr = parseStrToLongArr(masterBolStr);
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findMasterBolLinesByMasterBolArr(loadNo,masterBolArr, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
	
	public DBRow[] findOrdersNoAndCompanyByLoadNoOrderStr(String loadNo, String orderStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		Long[] orderArr = parseStrToLongArr(orderStr);
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrdersNoAndCompanyByLoadNoOrderArr(loadNo,orderArr, companyId, customerId,status);
		}
		return new DBRow[0];
	}
	
/**
	 * 通过Nos和类型类型，查询SupplierID
	 * @param no
	 * @param type
	 * @return
	 * @throws Exception   SupplierID
	 */
	public DBRow[] findSupplierIDByLoadNosBolNosCtnrs(String no, String type, long adid, HttpServletRequest request)throws Exception
	{
		String[] noArr = parseStrToStrArr(no);
		DBRow[] rows = new DBRow[0];
		if(!StringUtil.isBlank(no))
		{
			if("LOAD".equals(type.toUpperCase()))
			{
				String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
				rows = floorSQLServerMgrZJ.findSupplierIDWithMasterByLoadNoArr(noArr,status);
				if(rows.length == 0)
				{
					rows = floorSQLServerMgrZJ.findSupplierIDWithoutMasterByLoadNoArr(noArr,status);
				}
			}
			else if("DELIVERY".equals(type.toUpperCase()))
			{
				String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
				rows = floorSQLServerMgrZJ.findReceiptsSupplierIDByContainerNoArr(noArr,status);
				if(rows.length == 0)
				{
					rows = floorSQLServerMgrZJ.findReceiptsSupplierIDByBolNoArr(noArr,status);
				}
			}
		}
		return rows;
	}
	
	private String[] parseStrToStrArr(String str) throws Exception
	{
		List<String> list = new ArrayList<String>();
		if(!StringUtil.isBlank(str))
		{
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StringUtil.isBlank(arr[i]))
				{
						list.add(arr[i]);
				}
			}
		}
		return list.toArray(new String[0]);
	}
	
	
	/**
	 * shipping label
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByLoadNo(String loadNo,long masterBolNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception
	{
		//this.update
		
		
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] rows = floorSQLServerMgrZJ.findOrdersShipPalletByLoadNo(loadNo,masterBolNo, customerId, companyId,status);
			if(rows.length == 0)
			{
				rows = floorSQLServerMgrZJ.findOrderShipPalletByLoadNo(loadNo, customerId, companyId,status);
			}
			return rows;
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	
	/**
	 * shipping label
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByOrderNo(long orderNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception
	{
		if(orderNo > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrderShipPalletByOrderNo(orderNo, customerId, companyId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	/**通过loadno查询order信息
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return DBRow
	 * load_no, total_pallets, order_info(arr)
	 * [{"load_no":"130516733","total_pallets":"24","master_bol_no":1907, "pallet_types":[{CompanyID,PalletTypeID,PalletTypeName}],
	 * "order_info":[{"orderno":"98471","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098471001},{code_no:870098471002},{code_no:870098471003}]", "PalletTypeID":"PalletTypeID"},
	 * "orderno":"98508","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098508001},{code_no:870098508002},{code_no:870098508003}]"}]}]
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByLoadNoForTicket(loadNo, companyId, customerId,status);
			if(masterBols.length == 0)
			{
				//DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByLoadNo(loadNo, customerId, companyId);
				DBRow[]	rows = floorSQLServerMgrZJ.findOrderPalletByLoadNo(loadNo, customerId, companyId,status);
				if(rows.length > 0)
				{
					DBRow result = handleLoadOrders(rows, loadNo, 0,"");
					masterBols = new DBRow[1];
					masterBols[0] = result;
				}
				else
				{
					return null;
				}
			}
			else if(masterBols.length == 1)
			{
				DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByLoadNo(loadNo, customerId, companyId, masterBols[0].get("MasterBOLNo", 0L),status);
				DBRow result = handleLoadOrders(rows, loadNo, masterBols[0].get("MasterBOLNo", 0L),masterBols[0].getString("ProNo"));
				masterBols[0] = result;
			}
			else
			{
				for (int i = 0; i < masterBols.length; i++)
				{
					DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByLoadNo(loadNo, customerId, companyId, masterBols[i].get("MasterBOLNo", 0L),status);
					DBRow result = handleLoadOrders(rows, loadNo, masterBols[i].get("MasterBOLNo", 0L),masterBols[0].getString("ProNo"));
					masterBols[i] = result;
				}
			}
			return masterBols;
		}
		return null;
	}
	
	public DBRow addOrderInfoByMasterBol(String loadNo, String companyId, String customerId, long adid,long masterBol, HttpServletRequest request) throws Exception{
				
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByLoadNo(loadNo, customerId, companyId,masterBol,status);
		return handleLoadOrders(rows, loadNo, masterBol, "");
	}
	private DBRow handleLoadOrders(DBRow[] rows, String loadNo, long masterBolNo, String pro_no) throws Exception
	{
		DBRow result =new DBRow();
		String CompanyID = "";
		String CustomerID = "";
		int total_pallets = 0;
		String stagingAreaId = "";
		String master_note = "";
		for (int i = 0; i < rows.length; i++) 
		{
			int pallet = rows[i].get("Pallets", 0);
			if(StringUtil.isBlank(stagingAreaId))
			{
				stagingAreaId = rows[i].getString("StagingAreaID");
			}
			if(StringUtil.isBlank(CompanyID))
			{
				CompanyID = rows[i].getString("CompanyID");
			}
			if(StringUtil.isBlank(CustomerID))
			{
				CustomerID = rows[i].getString("CustomerID");
			}
			DBRow[] codeRows = new DBRow[pallet];
			for (int j = 0; j < pallet; j++) 
			{
				DBRow codeNoRow = new DBRow();
				String code = "87"+ MoneyUtil.fillZeroByRequire(rows[i].get("OrderNo", 0L), 7)+ MoneyUtil.fillZeroByRequire(j+1, 3);
				codeNoRow.add("code_no", code);
				codeRows[j] = codeNoRow;
			}
			total_pallets += pallet;
			rows[i].add("code_nos", codeRows);
			if(StrUtil.isBlank(master_note))
			{
				master_note = rows[i].getString("MNote");
			}
			DBRow[] orderRow = new DBRow[1];
			orderRow[0] = rows[i];
			DBRow caseInfo = handleOrdersAndItems(orderRow);
			if(null != caseInfo)
			{
				rows[i].add("case_total", caseInfo.get("case_total", 0));
			}
			rows[i].add("SupplierID", findSupplierIdsByOrderNoCompanyID(rows[i].get("OrderNo", 0L), rows[i].getString("CompanyID")));
		}
		result.add("load_no", loadNo);
		result.add("pro_no", pro_no);
		result.add("total_pallets", total_pallets);
		result.add("staging_area_id", stagingAreaId);
		result.add("order_info", rows);
		result.add("pallet_types", floorSQLServerMgrZJ.findPalletTypesByCompanyID(CompanyID));
		result.add("master_bol_no", masterBolNo);
		result.add("CustomerID", CustomerID);
		result.add("CompanyID", CompanyID);
		result.add("master_note", master_note);
		return result;
		
	}
	
	
	//	通过loadNo，更新相关的Status
		public void updateLoadMasterOrderStatus(final String loadNo, final String companyId, final String customerId
				, final long adid, final HttpServletRequest request)throws Exception
		{
			sqlServerTransactionTemplate
			.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try 
					{
						if(!StringUtil.isBlank(loadNo))
						{
							String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
							DBRow row = new DBRow();
							row.add("Status", "Closed");
							floorSQLServerMgrZJ.updateMasterBol(row, loadNo,companyId,customerId,status);
							//更新order
							updateLoadOrdersStatus(row, loadNo,companyId,customerId, adid, request);
						}
					} 
					catch (Exception e) 
					{
						throw new RuntimeException(e);
					}
				}
			});
		}
		
		//通过loadNo，更新相关orders的Status
		public void updateLoadOrdersStatus(DBRow updateRow, String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] orders = floorSQLServerMgrZJ.findMasterBolLinesByLoadNo(loadNo,companyId,customerId,status);
			for (int i = 0; i < orders.length; i++) 
			{
				long orderNo		= orders[i].get("OrderNo", 0L);
				floorSQLServerMgrZJ.updateOrder(updateRow, orderNo, companyId,customerId,status);
			}
		}
		
		/**
		 * 更新托盘数量
		 * @param orderNo
		 * @param companyId
		 * @param customerId
		 * @param pallets
		 * @throws Exception
		 */
		public void updateOrderPallets(final long orderNo, final String companyId, final String customerId, final int pallets) throws Exception
		{
			sqlServerTransactionTemplate
			.execute(new TransactionCallbackWithoutResult() {

				@Override
				protected void doInTransactionWithoutResult(
						TransactionStatus txStat) {
					try 
					{
						if(pallets > -1)
						{
							DBRow row = new DBRow();
							row.add("Pallets", pallets);
							floorSQLServerMgrZJ.updateOrderLines(row, orderNo, companyId);
						}
					} 
					catch (Exception e) 
					{
						throw new RuntimeException(e);
					}
				}
			});
		}
	
		
	/**
	 * shipping label
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByLoadNoForShippingLabel(String loadNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] rows = floorSQLServerMgrZJ.findOrdersShipPalletByLoadNo(loadNo, 0L, customerId, companyId,status);
			if(rows.length == 0)
			{
				rows = floorSQLServerMgrZJ.findOrderShipPalletByLoadNo(loadNo, customerId, companyId,status);
			}
			return rows;
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	
	/**
	 * shipping label
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByOrderNoForShippingLabel(long orderNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception
	{
		if(orderNo > 0)
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrderShipPalletByOrderNo(orderNo, customerId, companyId,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	/**
	 * 更新pallets数量
	 */
	public void updateOrderPalletsByOrderNo(final long orderNo, final String customerId, final String companyId,final int palletsOriginal
			, final int palletsCurrent, final long adid, final HttpServletRequest request) throws Exception
	{
		sqlServerTransactionTemplate
		.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try 
				{
//					System.out.println("orderNo:"+orderNo+","+customerId+","+companyId+","+palletsOriginal+","+palletsCurrent);
					if(orderNo > 0 && palletsOriginal != palletsCurrent)
					{
						DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNo(orderNo, customerId, companyId, 0);
						int diff = palletsOriginal - palletsCurrent;
						for (int i = 0; i < orderLines.length; i++) 
						{
							String itemId = orderLines[i].getString("ItemID");
							int lineNo = orderLines[i].get("LineNo", 0);
							int intPallets = orderLines[i].get("intPallets", 0);
							float pallets = orderLines[i].get("Pallets", 0f);
							if(diff == 0)
							{
								break;
							}
							else if(diff < 0)
							{
//								System.out.println("1:"+diff+","+pallets);
								updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -diff, adid, request);
								break;
							}
							else
							{
								if(intPallets < diff)
								{
//									System.out.println("1:"+diff+","+pallets);
									updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -pallets, adid, request);
									diff = diff - intPallets;
								}
								else
								{
									if(intPallets == diff && pallets < diff)
									{
										updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -pallets, adid, request);
										break;
									}
									else
									{
//										System.out.println("1:"+diff+","+pallets);
										updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -diff, adid, request);
										break;
									}
								}
							}
						}
					}
				} 
				catch (Exception e) 
				{
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	
	public void updateOrderLinePallets(long orderNo, String customerId, String companyId,String itemId, int lineNo, float pallets, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] rows = floorSQLServerMgrZJ.findOrderByOrderNo(orderNo, customerId, companyId,status);
//			System.out.println("rows:"+rows.length);
			if(rows.length > 0)
			{
				floorSQLServerMgrZJ.updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, pallets);
			}
		} 
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 暂时不需要
	 */
	public void updateOrderPalletsByLoadNo(final String loadNo,final String customerId, final String companyId, final DBRow[] orderPallets) throws Exception
	{
		sqlServerTransactionTemplate
		.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try 
				{
					if(!StringUtil.isBlank(loadNo))
					{
						DBRow row = new DBRow();
					
					}
				} 
				catch (Exception e) 
				{
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrders(String loadNo, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByLoadNoForTicket(loadNo, CompanyID, CustomerID,status);
					//findMasterBolsByLoadNo(loadNo, CompanyID, CustomerID);
			//有些load没有MasterBols
			if(masterBols.length > 0)
			{
				for (int i = 0; i < masterBols.length; i++) 
				{
					DBRow masterBolNoRow = masterBols[i];
					long masterBolNo = masterBolNoRow.get("MasterBOLNo", 0L);
					String companyId = masterBolNoRow.getString("CompanyID");
					String customerId = masterBolNoRow.getString("CustomerID");
					DBRow[] loadOrders = floorSQLServerMgrZJ.findOrderInfoByMasterBol(masterBolNo, companyId, customerId,status);
					DBRow result = handleOrdersAndItems(loadOrders);
					masterBolNoRow.append(result);
				}
				/*
				JSONArray mbs = DBRowUtils.dbRowArrayAsJSON(masterBols);
				for (int i = 0; i < masterBols.length; i++)
				{
					JSONObject Master = mbs.getJSONObject(i);
					DBRow[] load_orders = (DBRow[])masterBols[i].get("load_orders", new DBRow[0]);
					JSONArray ods = DBRowUtils.dbRowArrayAsJSON(load_orders);
					for (int j = 0; j < load_orders.length; j++)
					{
						JSONObject order = ods.getJSONObject(j);
						DBRow[] order_items = (DBRow[])load_orders[j].get("order_items", new DBRow[0]);
						JSONArray its = DBRowUtils.dbRowArrayAsJSON(order_items);
						order.put("order_items", its);
					}
					Master.put("load_orders", ods);
				}
				System.out.println("mbs:"+mbs);
				*/
				return masterBols;
			}
			else
			{
				DBRow[] loadOrders = floorSQLServerMgrZJ.findOrdersSomeInfoWmsByLoadNoForTicket(loadNo, CompanyID, CustomerID,status);
				DBRow result = handleOrdersAndItems(loadOrders);
				if(result != null)
				{
					loadOrders = (DBRow[])result.get("load_orders", new DBRow[0]);
				}
				/*
				JSONArray ods = DBRowUtils.dbRowArrayAsJSON(loadOrders);
				for (int j = 0; j < loadOrders.length; j++)
				{
					JSONObject order = ods.getJSONObject(j);
					DBRow[] order_items = (DBRow[])loadOrders[j].get("order_items", new DBRow[0]);
					JSONArray its = DBRowUtils.dbRowArrayAsJSON(order_items);
					order.put("order_items", its);
				}
				System.out.println("ods:"+ods);
				*/
				return loadOrders;
			}
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	private DBRow handleOrdersAndItems(DBRow[] loadOrders) throws Exception
	{
		DBRow result = new DBRow();
		WmsItemUnitTicketKey wmsItemUnitTicketKey = new WmsItemUnitTicketKey();
		int palletTotal = 0;
		int caseTotal = 0;
		int itemTotal = 0;
		for(int j = 0; j < loadOrders.length; j ++)
		{
			DBRow loadOrderRow = loadOrders[j];
			long OrderNo = loadOrderRow.get("OrderNo", 0L);
			DBRow[] loadOrderItemRows = floorSQLServerMgrZJ.findOrdersItemsWmsByOrderNoForConsolidate(OrderNo, loadOrderRow.getString("CompanyID"));
	  		DBRow loadPalletNumRow  = findPalletsByOrderNoCompanyId(loadOrderRow.getString("CompanyID"), OrderNo);//order的pallet总数
	  		double palletTotalOrder = loadPalletNumRow.get("palletNumInt", 0d);
	  		int caseTotalOrder = 0;
	  		int itemTotalOrder = 0;
	  		for(int m = 0; m < loadOrderItemRows.length; m ++)
	  		{
	  			DBRow loadOrderItemRow = loadOrderItemRows[m];
	  			DBRow qtyInfoRow 	= findOrderCaseQtyByItemIdQty(loadOrderItemRow.getString("ItemID"),loadOrderRow.getString("CompanyID"), loadOrderItemRow.get("OrderedQtyInt", 0d), loadOrderRow.getString("CustomerID"));
			 	int caseQty			= qtyInfoRow.get("case_qty", 0);
				int pieceQty		= qtyInfoRow.get("piece_qty", 0);
				int innerQty		= qtyInfoRow.get("inner_qty", 0);
				int caseSumQty		= qtyInfoRow.get("case_sum_qty", 0);
				String Unit			= qtyInfoRow.getString("unit");
				String caseQtyStr	= "";
				if(0 != caseQty)
				{
					caseQtyStr += "+" + (int)caseQty + "(Case)";
				}
				if(0 != pieceQty)
				{
					caseQtyStr += "+" + (int)pieceQty + "(Piece)";
				}
				if(0 != innerQty)
				{
					caseQtyStr += "+" + (int)innerQty + "(Inner)";
				}
				if(0 != pieceQty)
				{
					caseQtyStr += "=" + (int)caseSumQty + "(Case)";
				}
				if(caseQtyStr.length() > 0)
				{
					caseQtyStr = caseQtyStr.substring(1);
				}
				caseTotalOrder += caseSumQty;
				itemTotalOrder += loadOrderItemRow.get("OrderedQtyInt", 0d);
				loadOrderItemRow.add("item_plate_item_qty", loadOrderItemRow.get("OrderedQtyInt", 0d)+"("+wmsItemUnitTicketKey.getWmsItemUnitKeyByKey(Unit)+")");
				loadOrderItemRow.add("item_plate_case", caseSumQty+"(Case)");
				if(caseSumQty!=caseQty)
				{
					loadOrderItemRow.add("item_plate_case_str", caseQtyStr);
				}
				else
				{
					loadOrderItemRow.add("item_plate_case_str", "");
				}
				loadOrderItemRow.add("StagingAreaID", loadOrderRow.getString("StagingAreaID"));
				loadOrderItemRow.add("PickingType", loadOrderRow.getString("PickingType"));
//				System.out.println("i:"+loadOrderItemRow.getString("item_plate_item_qty")+","+loadOrderItemRow.getString("item_plate_case")+","+loadOrderItemRow.getString("item_plate_case_str"));
	  		}
	  		
	  		caseTotal += caseTotalOrder;
	  		itemTotal += itemTotalOrder;
	  		palletTotal += palletTotalOrder;
	  		loadOrderRow.add("pallet_total", palletTotalOrder);
	  		loadOrderRow.add("case_total", caseTotalOrder);
	  		loadOrderRow.add("item_total", itemTotalOrder);
	  		loadOrderRow.add("order_items", loadOrderItemRows);
		}
		result.add("load_orders", loadOrders);
		result.add("case_total", caseTotal);
		result.add("item_total", itemTotal);
		result.add("pallet_total", palletTotal);
		return result;
	}
	
	public DBRow[] findMasterBolsByLoadNoGroupAddress(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		DBRow[] bolRows = new DBRow[0];
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			bolRows=floorSQLServerMgrZJ.findMasterBolsByLoadNo(loadNo,CompanyID,CustomerID,status);
			for (int i = 0; i < bolRows.length; i++)
			{
				DBRow[] orderRows = floorSQLServerMgrZJ.findB2BOrderItemPlatesWmsByMasterBolNo(bolRows[i].get("MasterBOLNo", 0L), bolRows[i].getString("CompanyID"),status);
				int error_order_status = 0;
				String orderNo = "";
				String itemId = "";
				for (int j = 0; j < orderRows.length; j++) 
				{
					DBRow[] itemRows	= floorSQLServerMgrZJ.findItemInfoByItemId(orderRows[j].getString("ItemID"), orderRows[j].getString("CompanyID"), orderRows[j].getString("CustomerID"));
					if(0 == itemRows.length)
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST;
						orderNo = orderRows[j].getString("OrderNo");
						itemId = orderRows[j].getString("ItemID");
					}
					else if(itemRows.length > 1)
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_MORE;
						orderNo = orderRows[j].getString("OrderNo");
						itemId = orderRows[j].getString("ItemID");
					}
					else
					{
						error_order_status = WmsSearchItemStatusKey.ITEM_UNIT_CORRECT;
					}
					if(0 != error_order_status)
					{
						break;
					}
				}
				bolRows[i].add("error_order_no", orderNo);
				bolRows[i].add("error_order_item", itemId);
				bolRows[i].add("error_order_status", error_order_status);
			}
		}
		return bolRows;
	}
	
	/**
	 * 通过仓库获取托盘类型
	 * @param CompanyID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletTypesByCompanyID(String CompanyID) throws Exception
	{
		return floorSQLServerMgrZJ.findPalletTypesByCompanyID(CompanyID);
	}
		
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrdersByMasterBol(long masterBolNo, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByMasterBolNoForTicket(masterBolNo, CompanyID, CustomerID,status);
			//有些load没有MasterBols
			if(masterBols.length > 0)
			{
				for (int i = 0; i < masterBols.length; i++) 
				{
					DBRow masterBolNoRow = masterBols[i];
					//long masterBolNo = masterBolNoRow.get("MasterBOLNo", 0L);
					String companyId = masterBolNoRow.getString("CompanyID");
					String customerId = masterBolNoRow.getString("CustomerID");
					DBRow[] loadOrders = floorSQLServerMgrZJ.findOrderInfoByMasterBol(masterBolNo, companyId, customerId,status);
					DBRow result = handleOrdersAndItems(loadOrders);
					masterBolNoRow.append(result);
				}
				return masterBols;
			}
			return new DBRow[0];
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrdersByOrderNo(long orderNo, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] loadOrders = floorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNoForTicket(orderNo, CompanyID, CustomerID,status);
			DBRow result = handleOrdersAndItems(loadOrders);
			if(result != null)
			{
				loadOrders = (DBRow[])result.get("load_orders", new DBRow[0]);
			}
			return loadOrders;
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	
	
	
	/**通过loadno查询order信息
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return DBRow
	 * load_no, total_pallets, order_info(arr)
	 * [{"load_no":"130516733","total_pallets":"24","master_bol_no":1907, "pallet_types":[{CompanyID,PalletTypeID,PalletTypeName}],
	 * "order_info":[{"orderno":"98471","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098471001},{code_no:870098471002},{code_no:870098471003}]", "PalletTypeID":"PalletTypeID"},
	 * "orderno":"98508","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098508001},{code_no:870098508002},{code_no:870098508003}]"}]}]
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByMasterBolNo(long masterBolNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		if(masterBolNo > 0)
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByMasterBolNoForTicket(masterBolNo, companyId, customerId,status);
			if(masterBols.length == 1)
			{
				DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByMasterBolNo(customerId, companyId, masterBols[0].get("MasterBOLNo", 0L),status);
				DBRow result = handleLoadOrders(rows, masterBols[0].getString("LoadNo"), masterBols[0].get("MasterBOLNo", 0L), masterBols[0].getString("ProNo"));
				masterBols[0] = result;
			}
			else
			{
				for (int i = 0; i < masterBols.length; i++)
				{
					DBRow[] rows = floorSQLServerMgrZJ.findOrdersPalletByMasterBolNo(customerId, companyId, masterBols[i].get("MasterBOLNo", 0L),status);
					DBRow result = handleLoadOrders(rows, masterBols[i].getString("LoadNo"), masterBols[i].get("MasterBOLNo", 0L), masterBols[i].getString("ProNo"));
					masterBols[i] = result;
				}
			}
			return masterBols;
		}
		return null;
	}
	
	
	/**通过loadno查询order信息
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return DBRow
	 * load_no, total_pallets, order_info(arr)
	 * [{"load_no":"130516733","total_pallets":"24","master_bol_no":1907, "pallet_types":[{CompanyID,PalletTypeID,PalletTypeName}],
	 * "order_info":[{"orderno":"98471","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098471001},{code_no:870098471002},{code_no:870098471003}]", "PalletTypeID":"PalletTypeID"},
	 * "orderno":"98508","pallets":"3","od_status":"Closed","ml_status":"Closed","code_nos":"[{code_no:870098508001},{code_no:870098508002},{code_no:870098508003}]"}]}]
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByOrderNo(long orderNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		if(orderNo > 0)
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] masterBols = floorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNoForTicket(orderNo, companyId, customerId,status);
			if(masterBols.length == 1)
			{
				DBRow[]	rows = floorSQLServerMgrZJ.findOrderPalletByOrderNo(orderNo, customerId, companyId,status);
				DBRow result = handleLoadOrders(rows, "", masterBols[0].get("MasterBOLNo", 0L), masterBols[0].getString("ProNo"));
				masterBols[0] = result;
			}
			else
			{
				for (int i = 0; i < masterBols.length; i++)
				{
					DBRow[]	rows = floorSQLServerMgrZJ.findOrderPalletByOrderNo(orderNo, customerId, companyId,status);
					DBRow result = handleLoadOrders(rows, "", masterBols[i].get("MasterBOLNo", 0L), masterBols[i].getString("ProNo"));
					masterBols[i] = result;
				}
			}
			return masterBols;
		}
		return null;
	}
	
	
	/**
	 * 查询orderPallet ， 装货
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletForLoading(int order_type, String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(order_no) && 0 != order_type)
		{
			if(WmsOrderTypeKey.LOAD_NO == order_type)
			{
				return findOrderPalletByLoadNo(order_no, CompanyID, CustomerID, adid, request);
			}
			else if(WmsOrderTypeKey.MASTER_BOL_NO == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					return findOrderPalletByMasterBolNo(Long.parseLong(order_no), CompanyID, CustomerID, adid, request);
				}
			}
			else if(WmsOrderTypeKey.ORDER == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					return findOrderPalletByOrderNo(Long.parseLong(order_no), CompanyID, CustomerID, adid, request);
					//return findMasterBolOrLoadByOrderNo(Long.parseLong(order_no), CompanyID, CustomerID);
				}
			}
		}
		return new DBRow[0];
	}
	
	
	/**
	 * 列出order信息，为做consolidate
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersInfoForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(order_no) && 0 != order_type)
		{
			if(WmsOrderTypeKey.LOAD_NO == order_type)
			{
				return findLoadedMasterAndOrders(order_no, CompanyID, CustomerID, adid, request);
			}
			else if(WmsOrderTypeKey.MASTER_BOL_NO == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					return findLoadedMasterAndOrdersByMasterBol(Long.parseLong(order_no), CompanyID, CustomerID, adid, request);
				}
			}
			else if(WmsOrderTypeKey.ORDER == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					return findLoadedMasterAndOrdersByOrderNo(Long.parseLong(order_no), CompanyID, CustomerID, adid, request);
					//return findMasterBolOrLoadByOrderNo(Long.parseLong(order_no), CompanyID, CustomerID);
				}
			}
		}
		return new DBRow[0];
	}
	
	/**
	 * 通过OrderNo获取MasterBol和load的列表
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByOrderNo(long orderNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findMasterBolOrLoadByOrderNo(orderNo, companyId, customerId,status);
	}
	
	/**
	 * 通过 MasterBolNo获取LoadNo和MasterBOLNo
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByMasterBolNo(long MasterBolNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findMasterBolOrLoadByMasterBolNo(MasterBolNo, companyId, customerId,status);
	}
	
	/**
	 * 通过orderNo, itemNo, lineNo更改数量
	 * @param orderNo
	 * @param itemId
	 * @param changeQty
	 * @param lineNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
/*	public void updateOrderItemPalletQty(long orderNo, String itemId, int changeQty, int lineNo, String companyId, String customerId)throws Exception
	{
		if(orderNo > 0 && !StringUtil.isBlank(itemId) && !StringUtil.isBlank(companyId))
		{
			floorSQLServerMgrZJ.updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, changeQty);
		}
	}*/
	
	
	/**
	 * 更新pallets数量
	 */
	int consolidateChangedQty = 0;
//	final long orderNo, final String customerId, final String companyId,final int palletsOriginal, final int palletsCurrent
	public int updateOrderItemPalletQty(final long orderNo, final String itemId, final int changeQty, final int lineNo
			, final String companyId, final String customerId,final long adid,final HttpServletRequest request) throws Exception
	{
		consolidateChangedQty = 0;
		sqlServerTransactionTemplate
		.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try 
				{
//					System.out.println(orderNo+","+itemId+","+changeQty+","+lineNo+","+companyId+","+customerId);
					if(orderNo > 0 && !StringUtil.isBlank(itemId) && lineNo > 0)
					{
						DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoItem(orderNo, itemId, lineNo, companyId, customerId);
						if(orderLines.length > 0)
						{
							int palletsOriginal = orderLines[0].get("intPallets", 0);
							int palletsCurrent = palletsOriginal+changeQty;
							if(palletsOriginal != palletsCurrent)
							{
								int diff = palletsOriginal - palletsCurrent;
								String itemId = orderLines[0].getString("ItemID");
								int lineNo = orderLines[0].get("LineNo", 0);
								int intPallets = orderLines[0].get("intPallets", 0);
								float pallets = orderLines[0].get("Pallets", 0f);
								if(diff < 0)
								{
									updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -diff, adid, request);
									consolidateChangedQty = palletsCurrent;
								}
								else
								{
									if(intPallets < diff)
									{
										updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -pallets, adid, request);
										consolidateChangedQty = 0;
									}
									else
									{
										if(intPallets == diff && pallets < diff)
										{
											updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -pallets, adid, request);
											consolidateChangedQty = 0;
										}
										else
										{
											updateOrderLinePallets(orderNo, customerId, companyId, itemId, lineNo, -diff, adid, request);
											consolidateChangedQty = palletsCurrent;
										}
									}
								}
							}
						}
					}
				} 
				catch (Exception e) 
				{
					throw new RuntimeException(e);
				}
			}
		});
		return consolidateChangedQty;
	}
	
	/**
	 * 验证单据类型
	 * @param orderNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkOrderInfoByOrderNo1(String orderNo, String[] companyId, String[] status) throws Exception
	{
		DBRow[] result = null;
		if(!StringUtil.isBlank(orderNo))
		{
			DBRow[] orderInfos = new DBRow[0];
			if(!StringUtil.isBlankAndCanParseLong(orderNo))
			{
				orderInfos = floorSQLServerMgrZJ.checkOrderInfoByOrderNo(Long.parseLong(orderNo),status,companyId);
			}
			DBRow[] ponoInfos = floorSQLServerMgrZJ.checkOrderInfoByPoNo(orderNo,status,companyId);
			DBRow[] loadInfos = floorSQLServerMgrZJ.findLoadSomeInfoByLoadNo(orderNo,status,companyId);
			for (int i = 0; i < loadInfos.length; i++)
			{
				DBRow[] loadSome = floorSQLServerMgrZJ.checkMasterInfosByLoadNo(loadInfos[i].getString("LoadNo"), status, loadInfos[i].getString("CompanyID"), loadInfos[i].getString("CustomerID"));
				if(loadSome.length > 0)
				{
					loadInfos[i] = loadSome[0];
				}
			}
			if(loadInfos.length == 0)
			{
				loadInfos = floorSQLServerMgrZJ.checkOrdersByLoadNo(orderNo,status,companyId);
			}
			int orderInfoLen = orderInfos.length;
			int ponoInfoLen = ponoInfos.length;
			int loadInfoLen = loadInfos.length;
			result = new DBRow[orderInfoLen+ponoInfoLen+loadInfoLen];
			if(result.length > 0)
			{
				if(orderInfos.length > 0)
				{
					System.arraycopy(orderInfos, 0, result, 0, orderInfoLen);
				}
				if(ponoInfos.length > 0)
				{
					System.arraycopy(ponoInfos, 0, result, orderInfoLen, ponoInfoLen);
				}
				if(loadInfos.length > 0)
				{
					System.arraycopy(loadInfos, 0, result, orderInfoLen+ponoInfoLen, loadInfoLen);
				}
			}
		}
		return result;
	}
	
	/**
	 * 验证单据类型
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkOrderInfoByOrderNo(String number, String[] companyId, String[] status) throws Exception
	{
		DBRow[] result = null;
		if(!StringUtil.isBlank(number))
		{
			ModuleKey moduleKey = new ModuleKey();
			//当作load查
			List<DBRow> loadLoadInfos = new ArrayList<DBRow>();
			DBRow[] loadInfos = floorSQLServerMgrZJ.findLoadAllInfosByLoadNoCompanyIds(number, companyId, "", new String[]{"Imported", "Open"});
			for (int i = 0; i < loadInfos.length; i++)
			{
				loadLoadInfos.add(checkOrderLoad(loadInfos[i], new String[]{loadInfos[i].getString("Status")}, number, ModuleKey.CHECK_IN_LOAD));//.add("orders", orders);
			}
			List<DBRow> orderLoadInfos = new ArrayList<DBRow>();//有load的order
			List<DBRow> orderOrderInfos = new ArrayList<DBRow>();//没有load的order
			DBRow[] orderInfos = new DBRow[0];
			//当作Order查
			int isAllPicked = YesOrNotKey.YES;
			if(!StringUtil.isBlankAndCanParseLong(number))
			{
				orderInfos = floorSQLServerMgrZJ.findOrdersAllInfoByOrderNoCompanyIds(Long.parseLong(number), companyId, "", status);
				// order有load情况下返回load，有pono返回pono
				for (int i = 0; i < orderInfos.length; i++)
				{
					String mLoadNo = orderInfos[i].getString("m_load");
					String company = orderInfos[i].getString("CompanyID");
					String customer = orderInfos[i].getString("m_customer");
					String m_status = orderInfos[i].getString("m_status");
					if(!StrUtil.isBlank(mLoadNo))
					{
						DBRow orderLoad = new DBRow();
						orderLoad.add("LoadNo", mLoadNo);
						orderLoad.add("CompanyID", company);
						orderLoad.add("CustomerID", customer);
						orderLoad.add("Status", m_status);
						orderLoad = checkOrderLoad(orderLoad, new String[]{m_status}, number, ModuleKey.CHECK_IN_ORDER);
						orderLoadInfos.add(orderLoad);
					}
					else
					{
						long OrderNo = orderInfos[i].get("OrderNo", 0L);
						orderInfos[i].add("order_type", ModuleKey.CHECK_IN_ORDER);
						orderInfos[i].add("order_type_value", moduleKey.getModuleName(ModuleKey.CHECK_IN_ORDER));
						orderInfos[i].add("number", OrderNo);
						orderInfos[i].add("search_number", number);
						orderInfos[i].add("search_number_type", ModuleKey.CHECK_IN_ORDER);
//						if(isAllPicked == YesOrNotKey.YES && !WmsOrderStatusKey.PICKED.equals(orderInfos[i].getString("Status")))
//						{
//							isAllPicked = YesOrNotKey.NO;
//						}
						//更换验证order picked 
						if(!checkOrderFnOrderSerialnoFinalStatus(OrderNo, company).get("is_all_picked", "").equals("C")){
							isAllPicked = YesOrNotKey.NO;
						}
						orderInfos[i].add("is_all_picked", isAllPicked);
						orderOrderInfos.add(orderInfos[i]);
					}
				}
			}
			DBRow[] ponoInfos = floorSQLServerMgrZJ.findPoAllInfosByPONoCompanyIds(number, companyId, "", status);
			//TODO customerID合并，load合并……
			List<DBRow> ponoLoadInfos = new ArrayList<DBRow>();//有load的pono
			List<DBRow> ponoPonoInfos = new ArrayList<DBRow>();//没有load的
			// 有load返回load
			for (int i = 0; i < ponoInfos.length; i++) {
				String mLoadNo = ponoInfos[i].getString("m_load");
				String company = ponoInfos[i].getString("CompanyID");
				String customer = ponoInfos[i].getString("CustomerID");
				String m_status = ponoInfos[i].getString("m_status");
				if(!StrUtil.isBlank(mLoadNo))
				{
					DBRow orderLoad = new DBRow();
					orderLoad.add("LoadNo", mLoadNo);
					orderLoad.add("CompanyID", company);
					orderLoad.add("CustomerID", customer);
					orderLoad.add("Status", m_status);
					orderLoad = checkOrderLoad(orderLoad, status, number, ModuleKey.CHECK_IN_PONO);
					ponoLoadInfos.add(orderLoad);
				}
				else
				{
					DBRow[] orders = floorSQLServerMgrZJ.findOrdersSomeInfoByPONo(number, companyId, "", null);
					List<DBRow> orderList = new ArrayList<DBRow>();
					String customers = ",";
					String accounts = ",";
					int isAllPicked1 = YesOrNotKey.YES;
					for (int j = 0; j < orders.length; j++)
					{
//						DBRow order = new DBRow();
//						order.add("OrderNo", orders[j].getString("OrderNo"));
//						orderList.add(order);
						if(customers.indexOf(","+orders[j].getString("CustomerID")+",") == -1)
						{
							customers += orders[j].getString("CustomerID")+",";
						}
						if(accounts.indexOf(","+orders[j].getString("AccountID")+",") == -1)
						{
							accounts += orders[j].getString("AccountID")+",";
						}
//						if(isAllPicked1 == YesOrNotKey.YES && !WmsOrderStatusKey.PICKED.equals(orderInfos[i].getString("Status")))
//						{
//							isAllPicked1 = YesOrNotKey.NO;
//						}
					}
					if(customers.length() > 0)
					{
						customers = customers.substring(1);
						if(customers.length() > 0)
						{
							customers = customers.substring(0, customers.length()-1);
						}
					}
					if(accounts.length() > 0)
					{
						accounts = accounts.substring(1);
						if(accounts.length() > 0)
						{
							accounts = accounts.substring(0, accounts.length()-1);
						}
					}
					//替换掉orders
					DBRow pono = CheckPoFnOrderSerialnoFinalStatus(number, company, customer);
					DBRow[] orders1 = (DBRow[]) pono.get("orders", new DBRow[0]);
					isAllPicked1 = pono.get("is_all_picked", 0);
					
					ponoInfos[i].add("CustomerID", customers);
					ponoInfos[i].add("AccountID", accounts);					
					ponoInfos[i].add("orders", orders1);//替换掉orders
					ponoInfos[i].add("orders_len", orders1.length);//替换掉orders
					ponoInfos[i].add("order_type", ModuleKey.CHECK_IN_PONO);
					ponoInfos[i].add("order_type_value", moduleKey.getModuleName(ModuleKey.CHECK_IN_PONO));
					ponoInfos[i].add("number", ponoInfos[i].getString("PONo"));
					ponoInfos[i].add("search_number", number);
					ponoInfos[i].add("search_number_type", ModuleKey.CHECK_IN_PONO);
					ponoInfos[i].add("is_all_picked", isAllPicked1);
					//orderInfos[i].add("is_all_picked", isAllPicked1);
					ponoPonoInfos.add(ponoInfos[i]);
				}
			}
			
			List<DBRow> results = new ArrayList<DBRow>();
			//TODO 数据中验重的问题
			results.addAll(loadLoadInfos);
			results.addAll(orderLoadInfos);
			results.addAll(ponoLoadInfos);
			results.addAll(orderOrderInfos);
			results.addAll(ponoPonoInfos);
			result = results.toArray(new DBRow[0]);
		}
		return result;
	}
	
	
	public DBRow checkOrderLoad(DBRow row, String[] status, String search_number,int search_number_type) throws Exception
	{
		ModuleKey moduleKey = new ModuleKey();
		DBRow[] loadSome = floorSQLServerMgrZJ.checkMasterInfosByLoadNo(row.getString("LoadNo"), new String[]{row.getString("Status")}, row.getString("CompanyID"), "");
		DBRow[] masterInfos = floorSQLServerMgrZJ.findMasterBolByLoadNo(row.getString("LoadNo"), row.getString("CompanyID"), "", new String[]{row.getString("Status")});
		String customers = ",";
		String accounts = ",";
		for (int j = 0; j < masterInfos.length; j++) {
			if(customers.indexOf(","+masterInfos[j].getString("CustomerID")+",") == -1)
			{
				customers += masterInfos[j].getString("CustomerID")+",";
			}
			if(accounts.indexOf(","+masterInfos[j].getString("AccountID")+",") == -1)
			{
				accounts += masterInfos[j].getString("AccountID")+",";
			}
		}
		if(customers.length() > 0)
		{
			customers = customers.substring(1);
			if(customers.length() > 0)
			{
				customers = customers.substring(0, customers.length()-1);
			}
		}
		if(accounts.length() > 0)
		{
			accounts = accounts.substring(1);
			if(accounts.length() > 0)
			{
				accounts = accounts.substring(0, accounts.length()-1);
			}
		}
		row.add("CustomerID", customers);
		row.add("AccountID", accounts);
		if(loadSome.length > 0)
		{
			DBRow loadInfo = loadSome[0];
			row.add("StagingAreaID", loadInfo.getString("StagingAreaID"));
			row.add("FreightTerm", loadInfo.getString("FreightTerm"));
		}
		List<DBRow> orderList = new ArrayList<DBRow>();
		DBRow[] orders = floorSQLServerMgrZJ.findOrderSomeInfosByLoadNo(row.getString("LoadNo"), new String[]{row.getString("CompanyID")}, row.getString("CustomerID"), null);
		Map<String, DBRow> poMaps = new HashMap<String, DBRow>();
		int isAllPicked = YesOrNotKey.YES;
		for (int j = 0; j < orders.length; j++)
		{
			if(!poMaps.containsKey(orders[j].getString("PONo")))
			{
				poMaps.put(orders[j].getString("PONo"), orders[j]);
			}
			DBRow order = new DBRow();
			order.add("OrderNo", orders[j].getString("OrderNo"));
			orderList.add(order);
			if(isAllPicked == YesOrNotKey.YES && !WmsOrderStatusKey.PICKED.equals(orders[j].getString("Status")))
			{
				isAllPicked = YesOrNotKey.NO;
			}
		}
		//替换order的查询部分
		DBRow load = CheckLoadFnOrderSerialnoFinalStatus(row.getString("LoadNo"), null, row.getString("CompanyID"), row.getString("CustomerID"));
		DBRow[] orders1 = (DBRow[]) load.get("orders", new DBRow[0]);
		isAllPicked = load.get("is_all_picked", 0);
		// replace end
		List<DBRow> poRows = new ArrayList<DBRow>();
		List<DBRow> poList = new ArrayList<DBRow>();
		Iterator<String> iter = poMaps.keySet().iterator();
		while (iter.hasNext()) {
			String ponoStr = iter.next();
			DBRow res = poMaps.get(ponoStr);
			poRows.add(res);
			DBRow pono = new DBRow();
			pono.add("PONo", ponoStr);
			poList.add(pono);
		}
//		System.out.println("loadAppointment:"+row.getString("AppointmentDate"));
//		row.add("orders", orders);
//		row.add("ponos", poRows.toArray(new DBRow[0]));
		row.add("orders", orders1);//替换order的查询部分
		row.add("ponos", poList.toArray(new DBRow[0]));
		row.add("orders_len", orders1.length);
		row.add("ponos_len", poRows.size());
		row.add("order_type", ModuleKey.CHECK_IN_LOAD);
		row.add("order_type_value", moduleKey.getModuleName(ModuleKey.CHECK_IN_LOAD));
		row.add("number", row.getString("LoadNo"));
		row.add("search_number", search_number);
		row.add("search_number_type", search_number_type);
		row.add("is_all_picked", isAllPicked);
		row.remove("OrderNo");
		return row;
	}
	
	
	/**
	 * 通过OrderNo，反查load
	 * @param orderNo
	 * @param companyId
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadSomeInfoByOrderNo(String orderNo, String[] companyId, String[] status) throws Exception
	{
		if(!StrUtil.isBlank(orderNo))
		{
			DBRow[] loads = floorSQLServerMgrZJ.findLoadSomeInfoByPoNoOrOrderNo(orderNo,status, companyId);
			for (int i = 0; i < loads.length; i++)
			{
				DBRow[] loadSome = floorSQLServerMgrZJ.checkMasterInfosByLoadNo(loads[i].getString("LoadNo"), status, loads[i].getString("CompanyID"), loads[i].getString("CustomerID"));
				if(loadSome.length > 0)
				{
					loads[i] = loadSome[0];
				}
			}
			return loads;
		}
		return new DBRow[0];
	}
	
	/**
	 * checkin回车验证
	 * @param orderNo
	 * @param companyId
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow checkOrderTypeAndChangeOrderTypeByNo1(String orderNo, String[] companyId, HttpServletRequest request) throws Exception
	{
		DBRow result = null;
		if(!StrUtil.isBlank(orderNo))
		{
			result = new DBRow();
			String[] status = pickStatusExceptClosed();
			DBRow[] orderTypes = checkOrderInfoByOrderNo(orderNo, companyId, status);
			DBRow[] loads = findLoadSomeInfoByOrderNo(orderNo, companyId, status);
			result.add("orders", orderTypes);
			result.add("loads", loads);
		}
		return result;
	}
	
	/**
	 * checkin回车验证
	 * @param orderNo
	 * @param companyId
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow checkOrderTypeAndChangeOrderTypeByNo(String orderNo, String[] companyId) throws Exception
	{
		DBRow result = null;
		if(!StrUtil.isBlank(orderNo))
		{
			result = new DBRow();
			String[] status = pickStatusExceptClosed();
			DBRow[] orderTypes = checkOrderInfoByOrderNo(orderNo, companyId, status);
			result.add("infos", orderTypes);
		}
		return result;
	}
	
	
	/**
	 * 通过no(可能是bol，也可能是ctn)查询receipts
	 * @param No
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByContainerBolNo(String No, long adid,String[] companyID) throws Exception
	{
		DBRow result = null;
		DBRow[] results = new DBRow[0];
		if(!StringUtil.isBlank(No))
		{
			ModuleKey moduleKey = new ModuleKey();
			result = new DBRow();
			String[] status = deliveryStatusExceptClosed();
			results = floorSQLServerMgrZJ.findReceiptsByContainerNo(No, status, companyID);
			if(results.length == 0)
			{
				results = floorSQLServerMgrZJ.findReceiptsByBolNo(No,status,companyID);
			}
			for (int i = 0; i < results.length; i++) 
			{
				results[i].add("number", No);
				results[i].add("search_number", No);
				results[i].add("search_number_type", results[i].get("order_type", 0));
				results[i].add("order_type_value", moduleKey.getModuleName(results[i].get("order_type", 0)));
			}
		}
//		result.add("deliverys", results);
		result.add("infos", results);
		return result;
	}
	
	
	
	/**
	 * 通过OrderNo，companyID，customerID，查询模版名称
	 */
	@Override
	public DBRow[] findBillOfLadingTemplateByOrderNo(long orderNo, String companyId, String customerId, long adid,HttpServletRequest request) throws Exception
	{
		if(orderNo > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findBillOfLadingTemplateByOrderNo(orderNo, companyId, customerId, status);
		}
		return new DBRow[0];
	}
	
	/**
	 * 判断当前账号有没有打印关闭状态单据的bill of lading
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public boolean checkIsTestPrint(long adid, HttpServletRequest request) throws Exception
	{
		boolean is = false;
		if(adid <= 0 && null != request)
		{
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
			adid = adminLoggerBean.getAdid();
		}
		if(adid  > 0)
		{
			DBRow[] adgids = floorAdminMgr.findAdminRoleByAdid(adid);
			DBRow[] rights = floorAdminMgr.findAdminAthAction(adgids,adid,0,GlobalKey.CHECK_IN_PRINT_TEST);
			if(rights.length > 0)
			{
				is = true;
			}
		}
		return is;
	}
	
	public String[] pickStatusExceptClosed()
	{
		//open,picked,closed,Imported,Picking,Shipped
		String[] status = new String[6];
		status[0] = WmsOrderStatusKey.OPEN;
		status[1] = WmsOrderStatusKey.PICKED;
		status[2] = WmsOrderStatusKey.IMPORTED;
		status[3] = WmsOrderStatusKey.PICKING;
		status[4] = WmsOrderStatusKey.SHIPPED;
		status[5] = WmsOrderStatusKey.STAGING;

		return status;
	}
	
	public String[] deliveryStatusExceptClosed()
	{
		//Open,Closed,Imported,Received
		String[] status = new String[3];
		status[0] = WmsOrderStatusKey.OPEN;
		status[1] = WmsOrderStatusKey.IMPORTED;
		status[2] = WmsOrderStatusKey.RECEIVED;
		return status;
	}
	
	public String[] getStatusExceptClosed(int pickOrDeliver, long adid, HttpServletRequest request) throws Exception
	{
		if(!checkIsTestPrint(adid, request))
		{
			if(WmsOrderTypeKey.PICKUP == pickOrDeliver)
			{
				return pickStatusExceptClosed();
			}
			else if(WmsOrderTypeKey.DELIVERY == pickOrDeliver)
			{
				return deliveryStatusExceptClosed();
			}
		}
		return new String[0];
	}
	
	/**
	 * 通过MasterBolno字符串，获取最早时间的order的bolNote
	 * @param masterBols
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws Exception
	 */
	public String findFirstTimeOrderBolNoteByMasterBols(String masterBols, String companyID, String customerID) throws Exception
	{
		Long[] masterBolArr = parseStrToLongArr(masterBols);
		DBRow max = null;
		if(masterBolArr.length > 0)
		{
			DBRow[] rows = floorSQLServerMgrZJ.findOrdersNoteByMasterBolNos(masterBolArr, companyID, customerID);
			for (int i = 0; i < rows.length; i++) 
			{
				String note = rows[i].getString("BOLNote").replace(" ", "").toUpperCase();
				if(note.contains("DELIVERYWINDOW"))
				{
					int deliveryWindowStart = note.indexOf("DELIVERYWINDOW")+15;
					String str1 = note.substring(deliveryWindowStart, deliveryWindowStart+22);
					String[] dateArr = str1.split("TO");
					if(dateArr.length > 0)
					{
						rows[i].add("first", dateArr[0]);
						if(dateArr.length == 2)
						{
							rows[i].add("end", dateArr[1]);
						}
					}
				}
				if(max == null)
				{
					max = rows[i];
				}
				else
				{
					if(max.containsKey("first") && rows[i].containsKey("first")){
						if(DateUtil.createDateTime(max.getString("first")).after(DateUtil.createDateTime(rows[i].getString("first"))))
						{
							max = rows[i];
						}
					}
				}
			}
		}
		if(max != null)
		{
			return max.getString("BOLNote");
		}
		return "";
	}
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] findOrdersSomeInfoWmsByOrderNo(long OrderNo,String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		if(OrderNo > 0)
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			return floorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNo(OrderNo,CompanyID, CustomerID,status);
		}
		else
		{
			return new DBRow[0];
		}
	}
	@Override
	public DBRow[] getCustomerServices(String startTime,String endTime,String companyId,String customerId) throws Exception{
		return floorSQLServerMgrZJ.getCustomerServices(startTime, endTime, companyId, customerId);
	}
	
	/**
	 * 通过OrderNo，更新orderNo的ProNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateOrderPronoByOrderNo(final long orderNo, final String companyId, final String customerId, final String status, final String prono
			,final String shipDate,final String seals,final String vehicle,final String dockName,final String carrierName,final String userUpdated) throws Exception
	{
		sqlServerTransactionTemplate
		.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try 
				{
					if(orderNo > 0 && !StringUtil.isBlank(companyId) && !StringUtil.isBlank(customerId))
					{
						DBRow[] orderRows = floorSQLServerMgrZJ.findOrderByOrderNo(orderNo, customerId, companyId, null);
						if(orderRows.length > 0)
						{
							String ProNo = orderRows[0].getString("ProNo");
							String VehicleNo = orderRows[0].getString("VehicleNo");
							String[] statuses = pickStatusExceptClosed();
							DBRow row = new DBRow();
							if(!StrUtil.isBlank(status))
							{
								row.add("Status", status);
							}
							if(!StrUtil.isBlank(prono))
							{
								if(StrUtil.isBlank(ProNo))
								{
									row.add("ProNo", prono);
								}
							}
							if(!StringUtil.isBlank(seals))
							{
								row.add("Seals", seals);
							}
							if(!StringUtil.isBlank(vehicle))
							{
								if(StrUtil.isBlank(VehicleNo))
								{
									row.add("VehicleNo", vehicle);
								}
							}
							if(!StringUtil.isBlank(shipDate))
							{
								SimpleDateFormat dfShipped = new SimpleDateFormat("yyyy-MM-dd");
								String formateShipDate = dfShipped.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(shipDate));
	                        	System.out.println("======Shpping Date :"+formateShipDate+"=======");
								row.add("ShippedDate", formateShipDate);
								row.add("DateUpdated", shipDate);
							}
							if(!StringUtil.isBlank(dockName))
							{
								row.add("DockID", dockName);
							}
							if(!StrUtil.isBlank(userUpdated))
							{
								row.add("UserUpdated", userUpdated);
							}

							// fixed wms-190
							syncLoadNoAndCarrierIdToWMS(row, orderRows[0], orderNo, companyId, customerId);

							/*
						if(!StringUtil.isBlank(carrierName))
						{
							DBRow carrier = floorSQLServerMgrZJ.findCarrierInfoByCarrierName(carrierName, companyId);
							if(null != carrier)
							{
								row.add("CarrierID", carrier.getString("CarrierID"));
							}
						}
							 */
							floorSQLServerMgrZJ.updateOrder(row, orderNo, companyId, customerId, statuses);
						}
					}
				} 
				catch (Exception e) 
				{
					throw new RuntimeException(e);
				}
			}
		});
	}

	/**
	 * fixed WMS-190
	 * when order's CarrierID or LoadNo is null updating them from masterBol.
	 */
	private void syncLoadNoAndCarrierIdToWMS(DBRow row, DBRow orderInfo, final long orderNo, String companyId, String customerID) {
		String carrierID = orderInfo.getString("CarrierID", null);
		String loadNo = orderInfo.getString("LoadNo", null);

		try {
			DBRow[] masterBolInfo = floorSQLServerMgrZJ.findMasterBolOrLoadByOrderNo(orderNo, companyId, customerID, null);

			if (masterBolInfo.length < 1)
				return;

			if (StringUtil.isBlank(loadNo)) {
				String mLoadNo = masterBolInfo[0].getString("LoadNo", null);
				if (!StringUtil.isBlank(mLoadNo)) {
					row.add("LoadNo", mLoadNo);
				}
			}

			long masterBolNo = masterBolInfo[0].get("MasterBOLNo", 0);

			if (StringUtil.isBlank(carrierID) && masterBolNo > 0) {
				DBRow[] bolInfo = floorSQLServerMgrZJ.findMasterBolsByMasterBolNo(masterBolNo, companyId, customerID, null);
				if (bolInfo.length < 1)
					return;

				String mCarrierID = bolInfo[0].getString("CarrierId", null);
				if (!StringUtil.isBlank(mCarrierID)) {
					row.add("CarrierID", mCarrierID);
				}
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 通过OrderNo，更新orderNo的ProNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateMasterPronoByOrderNo(final long MasterBOLNo, final String companyId, final String customerId, final String status, final String prono
			,final String shipDate,final String seals,final String vehicle,final String dockName,final String carrierName,final String userUpdated) throws Exception
	{
		sqlServerTransactionTemplate
		.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try 
				{
					if(MasterBOLNo > 0 && !StringUtil.isBlank(companyId) && !StringUtil.isBlank(customerId))
					{
						DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsByMasterBolNo(MasterBOLNo, companyId, customerId, null);
						if(masterBols.length > 0)
						{
							String[] statuses = pickStatusExceptClosed();
							DBRow row = new DBRow();
							if(!StrUtil.isBlank(status))
							{
								row.add("Status", status);
							}
							 //System.out.println("proB 2222 " + prono);

							if(!StrUtil.isBlank(prono))
							{
								 //System.out.println("proB h1 " + prono);
								 //System.out.println(masterBols[0].getString("ProNo") + "   hherer");
								if(StrUtil.isBlank(masterBols[0].getString("ProNo")))
								{
									//System.out.println("proB h2 " + prono);
									row.add("ProNo", prono);
								}
							}
							if(!StringUtil.isBlank(seals))
							{
								row.add("Seals", seals);
							}
							if(!StringUtil.isBlank(vehicle))
							{
								if(StrUtil.isBlank(masterBols[0].getString("VehicleNo")))
								{
									row.add("VehicleNo", vehicle);
								}
							}
							if(!StringUtil.isBlank(shipDate))
							{
								row.add("ShippedDate", shipDate);
								row.add("DateUpdated", shipDate);
							}
							if(!StringUtil.isBlank(dockName))
							{
								row.add("DockID", dockName);
							}
							if(!StrUtil.isBlank(userUpdated))
							{
								row.add("UserUpdated", userUpdated);
							}
//						if(!StringUtil.isBlank(carrierName))
//						{
//							row.add("CarrierID", carrierName);
//						}
							floorSQLServerMgrZJ.updateMasterBol(row, MasterBOLNo, companyId, customerId, statuses);
						}
					}
				} 
				catch (Exception e) 
				{
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	
	/**
	 * 查预约到达时间为某天的Load
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderNoAppointTimeBetweenTime(String startTime, String endTime, String[] companyIds, long adid, HttpServletRequest request) throws Exception
	{
		String[] status = pickStatusExceptClosed();
		
		LinkedHashMap<String, DBRow> map = new LinkedHashMap<String, DBRow>();
		DBRow[] loadSuppliers = floorSQLServerMgrZJ.findOrderNoAppointTimeBetweenTime(startTime,endTime,status, companyIds);
		for (int i = 0; i < loadSuppliers.length; i++) 
		{
			String orderNo = loadSuppliers[i].getString("OrderNo");
			String companyId = loadSuppliers[i].getString("CompanyID");
			String supplier = loadSuppliers[i].getString("SupplierID");
			String orderNoCompanyId = orderNo +"_"+ companyId;
			if(map.containsKey(orderNoCompanyId))
			{
				DBRow row = map.get(orderNoCompanyId);
				String suppliers = row.getString("SupplierIDs");
				if(!StrUtil.isBlank(supplier)&&suppliers.indexOf(supplier) == -1)
				{
					suppliers += ","+supplier;
					row.add("SupplierIDs", suppliers);
				}
			}
			else
			{
				DBRow row = loadSuppliers[i];
				if(!StrUtil.isBlank(supplier))
				{
					row.add("SupplierIDs", ","+supplier);
				}
				map.put(orderNoCompanyId, row);
			}
		}
		List<DBRow> list = new ArrayList<DBRow>();
		Iterator<Entry<String, DBRow>> it = map.entrySet().iterator();
		while(it.hasNext()){
			Entry<String, DBRow> entry = it.next();
			list.add(entry.getValue());
 		}
		DBRow[] rows = (DBRow[])list.toArray(new DBRow[0]);
		
		return rows;
	}

	
	
	/**
	 * Parking list
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow findOrderSomeInfosByOrderNo(long OrderNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderSomeInfosByOrderNo(OrderNo, companyId, customerId, status);
	}
	
	
	/**
	 * Parking list items
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderItemsSomeInfoGroupByOrderNo(long OrderNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
		return floorSQLServerMgrZJ.findOrderItemsSomeInfoGroupByOrderNo(OrderNo, companyId, customerId, status);
	}
	
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow findLoadNoOrdersPONoInfoByLoadMasterBolNoStr(String loadNo,String MasterBOLNos, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception
	{
		DBRow result = new DBRow();
		
		Map<String, DBRow> map = new HashMap<String, DBRow>();
		Map<String, Double> itemPallet = new HashMap<String, Double>();
		List<DBRow>orderItemList = new ArrayList<DBRow>();
		Long[] masterBolArr = parseStrToLongArr(MasterBOLNos);
		if(!StringUtil.isBlank(loadNo))
		{
			String[] status = null;//getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			DBRow[] ponos = floorSQLServerMgrZJ.findOrderPonoInfosByLoadNoMasterBolNo(loadNo,masterBolArr, companyId, customerId,status);
			for (int i = 0; i < ponos.length; i++) 
			{
				DBRow pono = handleOrderPonoItemsAndPonos(ponos[i]);
				DBRow[] items_order = (DBRow[]) pono.get("items_order", new DBRow[0]);
				for (int j = 0; j < items_order.length; j++) 
				{
					orderItemList.add(items_order[j]);
					String itemId = items_order[j].getString("ItemID");
					double palletCount = items_order[j].get("palletCount", 0d);
					int caseQty = items_order[j].get("orderLineCase", 0);
					int weightLine = items_order[j].get("weightItemAndPallet", 0);
					String orderNo = items_order[j].getString("OrderNo");
					/*if(map.containsKey(itemId))
					{
						DBRow row = map.get(itemId);
						row.add("palletCount", row.get("palletCount", 0d)+palletCount);
						row.add("orderLineCase", row.get("orderLineCase", 0)+caseQty);
						row.add("weightItemAndPallet", row.get("weightItemAndPallet", 0)+weightLine);
						row.add("weightItemAndPallet", row.get("weightItemAndPallet", 0)+weightLine);
						String orderNos = row.getString("OrderNos");
						String spit = orderNos.equals("")?"":",";
						orderNos+=spit+"'"+orderNo+"'";
						row.add("OrderNos", orderNos);
					}
					else
					{
						items_order[j].add("OrderNos", "'"+orderNo+"'");
						map.put(itemId, items_order[j]);
						itemPallet.put(itemId, palletCount);
					}*/
				}
				/*
				long orderNo = ponos[i].get("OrderNo", 0L);
				String companyID = ponos[i].getString("CompanyID");
				String customerID = ponos[i].getString("CustomerID");
				int orderCase = 0;
				int orderWeight = 0;
				DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoCompany(orderNo, companyID,customerID);
				for (int j = 0; j < orderLines.length; j++)
				{
					String itemId = orderLines[j].getString("ItemID");
					int lineNo = orderLines[j].get("LineNo", 0);
					DBRow[] orderLinePlates = floorSQLServerMgrZJ.findOrderLinePlatesByOrderNoCompany(orderNo, companyID, lineNo);
					int lineQty = 0;
					for (int k = 0; k < orderLinePlates.length; k++)
					{
						lineQty += orderLinePlates[k].get("ShippedQty", 0);
					}
					orderLines[j].add("ShippedQty", lineQty);
					//处理case数量
					DBRow caseInfo = findOrderCaseQtyByItemIdQty(itemId, companyID, lineQty, customerID);
					int caseQty = caseInfo.get("case_sum_qty", 0);
					double PoundPerPackage = orderLines[j].get("PoundPerPackage", 0d);
					double PalletWeight = orderLines[j].get("PalletWeight", 0d);
					double pallets = handlePalletsByCustomerId(customerID, orderLines[j].get("CustomerPallets", 0d), orderLines[j].get("Pallets", 0d));
					int weightLine = (int) Math.ceil(caseQty * PoundPerPackage + PalletWeight * pallets);
					orderLines[j].add("weightLine", weightLine);
					orderCase += caseQty;
					orderWeight += weightLine;
//					System.out.println(orderNo+","+orderLines[j].getString("ItemID")+","+caseQty+","+PoundPerPackage+","+pallets+","+PalletWeight);
					if(map.containsKey(itemId))
					{
						DBRow row = map.get(itemId);
						row.add("palletCount", row.get("palletCount", 0d)+pallets);
						row.add("orderLineCase", row.get("orderLineCase", 0)+caseQty);
						row.add("weightItemAndPallet", row.get("weightItemAndPallet", 0)+weightLine);
						itemPallet.put(itemId, itemPallet.get(itemId)+pallets);
					}
					else
					{
						DBRow row = new DBRow();
						row.add("CommodityDescription", orderLines[j].getString("CommodityDescription2"));
						row.add("NMFC", orderLines[j].getString("NMFC2"));
						row.add("FreightClass", orderLines[j].getString("FreightClass2"));
						row.add("palletCount", pallets);
						row.add("orderLineCase", caseQty);
						row.add("weightItemAndPallet", weightLine);
						map.put(itemId, row);
						itemPallet.put(itemId, pallets);
					}
				}
				ponos[i].add("orderLineCaseSum", orderCase);
				ponos[i].add("weightItemAndPallet", orderWeight);
				
				*/
				
			}
			
			itemPallet = sortMapByValue(itemPallet);
			DBRow[] items = new DBRow[map.size()];
			int index = 0;
		    for (Map.Entry<String, Double> MapEnt : itemPallet.entrySet()) {
		    	DBRow row = map.get(MapEnt.getKey());
		    	items[index++] = row;
		    }
			result.add("ponos", ponos);
			result.add("items", items);
			DBRow[] orderItems = (DBRow[])orderItemList.toArray(new DBRow[0]);
			result.add("order_items", orderItems);
			return result;
		}
		return null;
	}
	
	public DBRow handleOrderPonoItemsAndPonos(DBRow order) throws Exception
	{
		Map<String, DBRow> map = new HashMap<String, DBRow>();
		Map<String, Double> itemPallet = new HashMap<String, Double>();
		long orderNo = order.get("OrderNo", 0L);
		String companyID = order.getString("CompanyID");
		String customerID = order.getString("CustomerID");
		int orderCase = 0;
		int orderWeight = 0;
		DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoCompany(orderNo, companyID,customerID);
		for (int j = 0; j < orderLines.length; j++)
		{
			String itemId = orderLines[j].getString("ItemID");
			int lineNo = orderLines[j].get("LineNo", 0);
			DBRow[] orderLinePlates = floorSQLServerMgrZJ.findOrderLinePlatesByOrderNoCompany(orderNo, companyID, lineNo);
			int lineQty = 0;
			String combineType ="1";
			for (int k = 0; k < orderLinePlates.length; k++)
			{
				lineQty += orderLinePlates[k].get("ShippedQty", 0);
				combineType = orderLinePlates[k].getString("CombineType");
			}
			orderLines[j].add("ShippedQty", lineQty);
			//处理case数量
			DBRow caseInfo = findOrderCaseQtyByItemIdQty(itemId, companyID, lineQty, customerID);
			int caseQty = combineType.equals("2")?lineQty:caseInfo.get("case_sum_qty", 0);
			double PoundPerPackage = orderLines[j].get("PoundPerPackage", 0d);
			double PalletWeight = orderLines[j].get("PalletWeight", 0d);
			double pallets = handlePalletsByCustomerId(customerID, orderLines[j].get("CustomerPallets", 0d), orderLines[j].get("Pallets", 0d));
			double palletCount = orderLines[j].get("Pallets", 0d);
			int weightLine = (int) Math.ceil(caseQty * PoundPerPackage + PalletWeight * pallets);
			orderLines[j].add("weightLine", weightLine);
			orderCase += caseQty;
			orderWeight += weightLine;
//			System.out.println(orderNo+","+orderLines[j].getString("ItemID")+","+caseQty+","+PoundPerPackage+","+pallets+","+PalletWeight);
			if(map.containsKey(itemId))
			{
				DBRow row = map.get(itemId);
				row.add("palletCount", row.get("palletCount", 0d)+palletCount);
				row.add("orderLineCase", row.get("orderLineCase", 0)+caseQty);
				row.add("weightItemAndPallet", row.get("weightItemAndPallet", 0)+weightLine);
				itemPallet.put(itemId, itemPallet.get(itemId)+pallets);
			}
			else
			{
				DBRow row = new DBRow();
				row.add("OrderNo", orderNo);
				row.add("CommodityDescription", orderLines[j].getString("CommodityDescription2"));
				row.add("NMFC", orderLines[j].getString("NMFC2"));
				row.add("FreightClass", orderLines[j].getString("FreightClass2"));
				row.add("palletCount", palletCount);
				row.add("orderLineCase", caseQty);
				row.add("weightItemAndPallet", weightLine);
				row.add("ItemID", itemId);
				map.put(itemId, row);
				itemPallet.put(itemId, pallets);
			}
		}
		order.add("orderLineCaseSum", orderCase);
		order.add("weightItemAndPallet", orderWeight);
		itemPallet = sortMapByValue(itemPallet);
		DBRow[] items = new DBRow[map.size()];
		int index = 0;
	    for (Map.Entry<String, Double> MapEnt : itemPallet.entrySet()) {
	    	DBRow row = map.get(MapEnt.getKey());
	    	row.add("palletCount", Math.ceil(row.get("palletCount", 0d)));
	    	items[index++] = row;
	    }
	    
	    order.add("items_order", items);
	    
	    return order;
	
	}
	
	
	public double handlePalletsByCustomerId(String customerId, double customerPallets, double pallets)
	{
		double palletQty = 0d;
		if(customerInfoForVizio().contains(customerId))
		{
			palletQty = customerPallets;
		}
		else
		{
			palletQty = pallets;
		}
		if(0 == palletQty)
		{
			palletQty = pallets;
		}
		return palletQty;
	}
	
	
	public List<String> customerInfoForVizio()
	{
		List<String> vizio = new ArrayList<String>();
		vizio.add("VIZIO");
		vizio.add("VZO");
		vizio.add("VIZIO2");
		vizio.add("VIZIO3");
		return vizio;
	}
	
	
    public Map<String, Double> sortMapByValue(Map<String, Double> oriMap) {  
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();  
        if (oriMap != null && !oriMap.isEmpty()) {  
            List<Map.Entry<String, Double>> entryList = new ArrayList<Map.Entry<String, Double>>(oriMap.entrySet());  
            Collections.sort(entryList,  
                    new Comparator<Map.Entry<String, Double>>() {  
                        public int compare(Entry<String, Double> entry1,  
                                Entry<String, Double> entry2) {  
                            double value1 = 0, value2 = 0;  
                            try {  
                                value1 = entry1.getValue();  
                                value2 = entry2.getValue();  
                            } catch (NumberFormatException e) {  
                                value1 = 0;  
                                value2 = 0;  
                            }  
                            if(value2 > value1)
                            {
                            	return 1;
                            }
                            else if(value2 < value1)
                            {
                            	return -1;
                            }
                            else
                            {
                            	return 0;
                            }
                        }  
                    });  
            Iterator<Map.Entry<String, Double>> iter = entryList.iterator();  
            Map.Entry<String, Double> tmpEntry = null;  
            while (iter.hasNext()) {  
                tmpEntry = iter.next();  
                sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());  
            }  
        }  
        return sortedMap;  
    }  
	
    
    /**consolidate
	 * 列出order信息
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersInfoByOrderTypeForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID,String status_one ,long adid, HttpServletRequest request)throws Exception
	{
		if(!StringUtil.isBlank(order_no) && 0 != order_type)
		{
			if(WmsOrderTypeKey.LOAD_NO == order_type)
			{
				DBRow[] rows = findLoadedMasterAndOrdersForConsolidate(order_no, CompanyID, CustomerID, status_one, adid, request);
//				System.out.println(StringUtil.convertDBRowArrayToJsonString(rows));;
				return rows;
			}
			else if(WmsOrderTypeKey.MASTER_BOL_NO == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					DBRow[] rows = findLoadedMasterAndOrdersByMasterBolForConsolidate(Long.parseLong(order_no), CompanyID, CustomerID,status_one, adid, request);
//					System.out.println(StringUtil.convertDBRowArrayToJsonString(rows));;
					return rows;
				}
			}
			else if(WmsOrderTypeKey.ORDER == order_type)
			{
				if(!StringUtil.isBlankAndCanParseLong(order_no))
				{
					DBRow[] rows = findLoadedMasterAndOrdersByOrderNoForConsolidate(Long.parseLong(order_no), CompanyID, CustomerID,status_one, adid, request);
//					System.out.println(StringUtil.convertDBRowArrayToJsonString(rows));;
					return rows;
				}
			}
		}
		return new DBRow[0];
	}
    
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrdersForConsolidate(String loadNo, String CompanyID, String CustomerID,String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = null;
			if(!StrUtil.isBlank(status_one))
			{
				status = new String[1];
				status[0] = status_one;
			}
			else
			{
				status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			}
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByLoadNoForTicket(loadNo, CompanyID, CustomerID,status);
					//findMasterBolsByLoadNo(loadNo, CompanyID, CustomerID);
			//有些load没有MasterBols
			if(masterBols.length > 0)
			{
				for (int i = 0; i < masterBols.length; i++) 
				{
					DBRow masterBolNoRow = masterBols[i];
					long masterBolNo = masterBolNoRow.get("MasterBOLNo", 0L);
					String companyId = masterBolNoRow.getString("CompanyID");
					String customerId = masterBolNoRow.getString("CustomerID");
					DBRow[] loadOrders = floorSQLServerMgrZJ.findOrderInfoByMasterBol(masterBolNo, companyId, customerId,status);
					DBRow result = handleOrdersAndItemsForConsolidate(loadOrders);
					masterBolNoRow.append(result);
				}
				return masterBols;
			}
			else
			{
				DBRow[] loadOrders = floorSQLServerMgrZJ.findOrdersSomeInfoWmsByLoadNoForTicket(loadNo, CompanyID, CustomerID,status);
				DBRow result = handleOrdersAndItemsForConsolidate(loadOrders);
				if(result != null)
				{
					loadOrders = (DBRow[])result.get("load_orders", new DBRow[0]);
				}
				return loadOrders;
			}
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	private DBRow handleOrdersAndItemsForConsolidate(DBRow[] loadOrders) throws Exception
	{
		DBRow result = new DBRow();
		int palletTotal = 0;
		int caseTotal = 0;
		int itemTotal = 0;
		
		for(int j = 0; j < loadOrders.length; j ++)
		{
			DBRow loadOrderRow = loadOrders[j];
			long OrderNo = loadOrderRow.get("OrderNo", 0L);
			
			//托盘列表
			List<DBRow> platesList = new ArrayList<DBRow>();
			
//			double orderPalletTotalCustomer = 0d;//客户托盘数
			int palletTotalOrder = 0;//实际装货托盘数
			int caseTotalOrder = 0;
	  		int itemTotalOrder = 0;
			
	  		//order的items
			DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoCompany(OrderNo, loadOrderRow.getString("CompanyID"), loadOrderRow.getString("CustomerID"));
			for (int i = 0; i < orderLines.length; i++) 
			{
				int orderLineShippedQty = 0;
				int orderLineShippedCase = 0;
				//order items 的plates
				DBRow[] orderLinePlates = floorSQLServerMgrZJ.findOrderLinePlatesByOrderNoCompanyNoGroup(OrderNo, loadOrderRow.getString("CompanyID"),orderLines[i].get("LineNo", 0));
						//.findOrderLinePlatesByOrderNoCompany(OrderNo, loadOrderRow.getString("CompanyID"), orderLines[i].get("LineNo", 0));
				for (int k = 0; k < orderLinePlates.length; k++)
				{
					DBRow orderLinePlate = orderLinePlates[k];
					orderLinePlate.add("ItemID", orderLines[i].getString("ItemID"));
					//托盘上的case 数量
					DBRow qtyInfoRow 	= findOrderCaseQtyByItemIdQty(orderLines[i].getString("ItemID"),loadOrderRow.getString("CompanyID"), orderLinePlate.get("ShippedQty", 0), loadOrderRow.getString("CustomerID"));
					int caseSumQty		= qtyInfoRow.get("case_sum_qty", 0);
					orderLinePlate.add("case_sum_qty", caseSumQty);
					orderLineShippedQty += orderLinePlate.get("ShippedQty", 0);
					orderLineShippedCase += caseSumQty;
					platesList.add(orderLinePlate);
				}
				orderLines[i].add("LineShippedQty", orderLineShippedQty);
				orderLines[i].add("LineShippedCase", orderLineShippedCase);
				orderLines[i].add("pallet_int", (int)Math.ceil(orderLines[i].get("Pallets", 0d)));
				orderLines[i].add("order_line_plates", orderLinePlates);
				palletTotalOrder += (int)Math.ceil(orderLines[i].get("Pallets", 0d));
				caseTotalOrder += orderLineShippedCase;
				itemTotalOrder += orderLineShippedQty;
			}
			loadOrderRow.add("case_total", caseTotalOrder);
	  		loadOrderRow.add("item_total", itemTotalOrder);
	  		loadOrderRow.add("pallet_total", palletTotalOrder);
	  		loadOrderRow.add("order_plates", platesList.toArray(new DBRow[0]));
	  		//loadOrderRow.add("order_lines", orderLines);//Order的Line，值中含plate信息
	  		caseTotal += caseTotalOrder;
	  		itemTotal += itemTotalOrder;
	  		palletTotal += palletTotalOrder;
		}
		result.add("load_orders", loadOrders);
		result.add("case_total", caseTotal);
		result.add("item_total", itemTotal);
		result.add("pallet_total", palletTotal);
		return result;
	}
    
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrdersByMasterBolForConsolidate(long masterBolNo, String CompanyID, String CustomerID,String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = null;
			if(!StrUtil.isBlank(status_one))
			{
				status = new String[1];
				status[0] = status_one;
			}
			else
			{
				status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			}
			DBRow[] masterBols = floorSQLServerMgrZJ.findMasterBolsSomeByMasterBolNoForTicket(masterBolNo, CompanyID, CustomerID,status);
			//有些load没有MasterBols
			if(masterBols.length > 0)
			{
				for (int i = 0; i < masterBols.length; i++) 
				{
					DBRow masterBolNoRow = masterBols[i];
					//long masterBolNo = masterBolNoRow.get("MasterBOLNo", 0L);
					String companyId = masterBolNoRow.getString("CompanyID");
					String customerId = masterBolNoRow.getString("CustomerID");
					DBRow[] loadOrders = floorSQLServerMgrZJ.findOrderInfoByMasterBol(masterBolNo, companyId, customerId,status);
					DBRow result = handleOrdersAndItemsForConsolidate(loadOrders);
					masterBolNoRow.append(result);
				}
				return masterBols;
			}
			return new DBRow[0];
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 查询提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrdersByOrderNoForConsolidate(long orderNo, String CompanyID, String CustomerID,String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = null;
			if(!StrUtil.isBlank(status_one))
			{
				status = new String[1];
				status[0] = status_one;
			}
			else
			{
				status = getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
			}
			DBRow[] loadOrders = floorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNoForTicket(orderNo, CompanyID, CustomerID,status);
			DBRow result = handleOrdersAndItemsForConsolidate(loadOrders);
			if(result != null)
			{
				loadOrders = (DBRow[])result.get("load_orders", new DBRow[0]);
			}
			return loadOrders;
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 查出所有AccountID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllAccountIDs() throws Exception
	{
		return floorSQLServerMgrZJ.findAllAccountIDs();
	}
	
	/**
	 * 查出所有SupplierID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllSupplierIDs() throws Exception
	{
		return floorSQLServerMgrZJ.findAllSupplierIDs();
	}
	
	/**
	 * 查出所有CustomerIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllCustomerIDs() throws Exception
	{
		return floorSQLServerMgrZJ.findAllCustomerIDs();
	}
	
	
	/**
	 * 通过 loadNo获取Orders
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		return floorSQLServerMgrZJ.findOrderInfosByLoadNo(loadNo, companyId, customerId, status);
	}
    
	
	/**
	 * @param pono
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrderInfoByPoNo(String pono, String companyId, String customerId, String status)throws Exception
	{
		return floorSQLServerMgrZJ.findOrderInfoByPoNo(pono, companyId, customerId, status);
	}
	
	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow findOrderInfoByOrderNo(long orderNo, String companyId, String customerId, String status)throws Exception
	{
		return floorSQLServerMgrZJ.findOrderInfoByOrderNo(orderNo, companyId, customerId, status);
	}
	
	/**
	 * 通过numberType、number等查询相应单据的shipto信息
	 * @param para
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月3日 下午4:29:37
	 */
	public DBRow[] findShipToInfoByNumberAndType(DBRow para) throws Exception
	{
		return floorSQLServerMgrZJ.findOrderLinePlatesByTime(para.get("startTime", ""),para.get("endTime", ""),para.get("company_id", ""));
	}
	
	
	/**
	 * 通过OrderLinePlates得到title
	 * @param rows
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月4日 下午7:10:32
	 */
	public String handleSupplierIdsByOrderLinePlateRows(DBRow[] rows) throws Exception
	{
		String str = "";
		Set<String> set = new HashSet<String>();
		for (int i = 0; i < rows.length; i++) 
		{
			set.add(rows[i].getString("SupplierID"));
		}
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			str += string+",";
		}
		if(str.length() > 0)
		{
			str = str.substring(0, str.length()-1);
		}
		return str;
	}
	
	/**
	 * 通过OrderLinePlates得到Item
	 * @param rows
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月4日 下午7:10:32
	 */
	public String handleItemsByOrderLinePlateRows(DBRow[] rows) throws Exception
	{
		String str = "";
		Set<String> set = new HashSet<String>();
		for (int i = 0; i < rows.length; i++) 
		{
			set.add(rows[i].getString("ItemID"));
		}
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			str += string+",";
		}
		if(str.length() > 0)
		{
			str = str.substring(0, str.length()-1);
		}
		return str;
	}
	
	
	
	/**
	 * 根据单据类型、单据号，查询order信息
	 * @param number_type 单据类型
	 * @param number 单据号
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午8:48:33
	 */
	public DBRow[] findOrdersByNumberAndType(int number_type, String number, String companyId, String customerId) throws Exception
	{
		DBRow[] rows = new DBRow[0];
		
		if(number_type == ModuleKey.CHECK_IN_LOAD)
		{
			rows = floorSQLServerMgrZJ.findOrderAllInfosByLoadNo(number, new String[]{companyId}, customerId, null);
			for (int i = 0; i < rows.length; i++) 
			{
				String supplierIds = findSupplierIdsByOrderNoCompany(rows[i].get("OrderNo", 0L) , rows[i].getString("CompanyID"));
				rows[i].add("supplier_ids", supplierIds);
			}
		}
		else if(number_type == ModuleKey.CHECK_IN_PONO)
		{
			rows = floorSQLServerMgrZJ.findOrdersAllInfoByPONo(number, new String[]{companyId}, customerId, null);
			for (int i = 0; i < rows.length; i++) 
			{
				String supplierIds = findSupplierIdsByOrderNoCompany(rows[i].get("OrderNo", 0L) , rows[i].getString("CompanyID"));
				rows[i].add("supplier_ids", supplierIds);
			}
		}
		else if(number_type == ModuleKey.CHECK_IN_ORDER)
		{
			rows = floorSQLServerMgrZJ.findOrdersAllInfoByOrderNo(Long.parseLong(number), new String[]{companyId}, customerId, null);
			for (int i = 0; i < rows.length; i++) 
			{
				String supplierIds = findSupplierIdsByOrderNoCompany(rows[i].get("OrderNo", 0L) , rows[i].getString("CompanyID"));
				rows[i].add("supplier_ids", supplierIds);
			}
		}
		return rows;
	}
	
	/**
	 * 通过OrderNo，companyID获取suppliers
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 下午3:41:34
	 */
	public String findSupplierIdsByOrderNoCompany(long orderNo, String companyId) throws Exception
	{
		String supplierIds = ",";
		DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoAndCompanyId(orderNo , companyId);
		for (int j = 0; j < orderLines.length; j++) {
			supplierIds += orderLines[j].getString("SupplierID");
		}
		supplierIds = supplierIds.substring(1);
		return supplierIds;
	}
	
	/**
	 * 通过OrderNo，companyid，查询SupplierIds，从plate和line上抓
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 上午10:43:27
	 */
	public String findSupplierIdsByOrderNoCompanyID(long orderNo, String companyId) throws Exception
	{
		String supplierIds = ",";
		DBRow[] orderLines = floorSQLServerMgrZJ.findOrderLinesByOrderNoAndCompanyId(orderNo , companyId);
		for (int j = 0; j < orderLines.length; j++) {
			if(!StrUtil.isBlank(orderLines[j].getString("SupplierID")) && supplierIds.indexOf(","+orderLines[j].getString("SupplierID")+",") == -1)
			{
				supplierIds += orderLines[j].getString("SupplierID")+",";
			}
		}
		DBRow[] orderLinePlates = floorSQLServerMgrZJ.findOrderLinePlatesByOrderNoAndCompanyId(orderNo, companyId);
		for (int i = 0; i < orderLinePlates.length; i++) {
			if(!StrUtil.isBlank(orderLinePlates[i].getString("SupplierID")) && supplierIds.indexOf(","+orderLinePlates[i].getString("SupplierID")+",") == -1)
			{
				supplierIds += orderLinePlates[i].getString("SupplierID")+",";
			}
		}
		
		if(supplierIds.length() > 0)
		{
			supplierIds = supplierIds.substring(1);
			if(supplierIds.length() > 0)
			{
				supplierIds = supplierIds.substring(0, supplierIds.length()-1);
			}
		}
		return supplierIds;
	}
	
	
	/**
	 * 通过OrderNo查询order信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午2:10:07
	 */
	public DBRow[] findOrderByOrderNo(long orderNo, String companyId) throws Exception
	{
		return floorSQLServerMgrZJ.findOrderByOrderNo(orderNo, companyId);
	}
	
	
	/**
	 * 通过bolNo，ContainerNo查询
	 * @param bolNo
	 * @param containerNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param SupplierID
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月13日 下午4:45:36
	 */
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID,String SupplierID) throws Exception
	{
		if(!StrUtil.isBlank(bolNo) || !StrUtil.isBlank(containerNo))
		{
			return floorSQLServerMgrZJ.findReceiptsByBolNoOrContainerNo(bolNo, containerNo, CompanyID, CustomerID, SupplierID, null);
		}
		return new DBRow[0];
	}
	
	/**
	 * checkFnOrderSerialnoFinalStatus
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月18日 下午5:14:20
	 */
	public String checkFnOrderSerialnoFinalStatus(long orderNo, String companyId) throws Exception
	{
		return floorSQLServerMgrZJ.checkFnOrderSerialnoFinalStatus(orderNo, companyId);
	}
	
	/**
	 * Check Load SnScaned
	 * @param LoadNo
	 * @param masterBolArr
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月20日 上午11:22:18
	 */
	public DBRow CheckLoadFnOrderSerialnoFinalStatus(String LoadNo, Long[] masterBolArr,  String companyId, String customerId) throws Exception
	{
		DBRow[] orders = floorSQLServerMgrZJ.findMasterBolLinesByMasterBolArr(LoadNo,masterBolArr, companyId, customerId,null);
		DBRow result = new DBRow();
		int isAllPicked = YesOrNotKey.YES;
		for (int i = 0; i < orders.length; i++)
		{
			String check = checkFnOrderSerialnoFinalStatus(orders[i].get("OrderNo", 0L), orders[i].getString("CompanyID"));
			if(isAllPicked == YesOrNotKey.YES && !"C".equals(check))
			{
				isAllPicked = YesOrNotKey.NO;
			}
			orders[i].add("is_all_picked", check);
		}
		result.add("is_all_picked", isAllPicked);
		result.add("orders", orders);
			
		return result;
	}
	
	
	/**
	 * Check Po SnScaned
	 * @param Po
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月20日 上午11:22:18
	 */
	public DBRow CheckPoFnOrderSerialnoFinalStatus(String PoNo,  String companyId, String customerId) throws Exception
	{
		String[] companyIds = new String[1];
		companyIds[0] = companyId;
		DBRow[] orders = floorSQLServerMgrZJ.findOrdersSomeInfoByPONo(PoNo, companyIds, customerId, null);
		DBRow result = new DBRow();
		int isAllPicked = YesOrNotKey.YES;
		for (int i = 0; i < orders.length; i++)
		{
			String check = checkFnOrderSerialnoFinalStatus(orders[i].get("OrderNo", 0L), orders[i].getString("CompanyID"));
			if(isAllPicked == YesOrNotKey.YES && !"C".equals(check))
			{
				isAllPicked = YesOrNotKey.NO;
			}
			orders[i].add("is_all_picked", check);
		}
		result.add("is_all_picked", isAllPicked);
		result.add("orders", orders);
		return result;
	}
	
	
	/**
	 * checkOrderFnOrderSerialnoFinalStatus
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月18日 下午5:14:20
	 */
	public DBRow checkOrderFnOrderSerialnoFinalStatus(long orderNo, String companyId) throws Exception
	{
		DBRow result = new DBRow();
		int isAllPicked = YesOrNotKey.YES;
		String check = checkFnOrderSerialnoFinalStatus(orderNo, companyId);
		if(isAllPicked == YesOrNotKey.YES && !"C".equals(check))
		{
			isAllPicked = YesOrNotKey.NO;
		}
		result.add("is_all_picked", check);
		return result;
	}
	
	
	/**

	 * 通过loadCompany,ponoCompany,orderCompany查询DN
	 * @param loadCompany
	 * @param ponoCompany
	 * @param orderCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午3:46:09
	 */
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany) throws Exception
	{
		return floorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany(loadCompany, ponoCompany, orderCompany);
	}
	
	@Override
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany,String refNo) throws Exception {
		return floorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany(loadCompany, ponoCompany, orderCompany,refNo);
	}
	
	
	/**

	 * 通过 companys，customers， carrier，ship_time查询Order
	 * @param companys
	 * @param customers
	 * @param carrier
	 * @param ship_time
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午4:45:35
	 */
	public DBRow[] findOrderByCompanyCustomerCarrierDate(String[] companys, String[] customers, String[] carrier, String ship_time) throws Exception
	{
		DBRow[] rows = floorSQLServerMgrZJ.findOrderByCompanyCustomerCarrierDate(companys, customers, carrier, ship_time);
		for (int i = 0; i < rows.length; i++)
		{
			//Order的Pallets
//			DBRow[] itemLines = floorSQLServerMgrZJ.findOrderPalletByOrderNo(rows[i].get("OrderNo", 0L),"", rows[i].getString("CompanyID"),null);
//			int pallets = 0;
//			if(itemLines.length > 0)
//			{
//				pallets = itemLines[0].get("Pallets", 0);
//			}
//			DBRow[] codeRows = new DBRow[pallets];
//			for (int j = 0; j < pallets; j++) 
//			{
//				DBRow codeNoRow = new DBRow();
//				String code = "87"+ MoneyUtil.fillZeroByRequire(rows[i].get("OrderNo", 0L), 7)+ MoneyUtil.fillZeroByRequire(j+1, 3);
//				codeNoRow.add("code_no", code);
//				codeRows[j] = codeNoRow;
//			}
//			rows[i].add("pallets", codeRows);
//			rows[i].add("pallets_count", codeRows.length);
			//Order的TrackingNo
			DBRow[] trackingNos = floorSQLServerMgrZJ.findOrderTrackingNosByOrderNoCompany(rows[i].get("OrderNo", 0L), rows[i].getString("CompanyID"));
			if(trackingNos.length == 0){
				DBRow tn = new DBRow();
				tn.add("TrackingNo", rows[i].get("ProNO",""));
				trackingNos = new DBRow[]{tn};
			}
			rows[i].add("tracking_nos", trackingNos);
			rows[i].add("tracking_count", trackingNos.length);
		}
		return rows;
	}
	
	
	//通过bolNo,containerNo、Receipt查询Receipts
	public DBRow[] findReceiptsByReceiptBolNoOrContainerNo(long receiptNo, String bolNo, String containerNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception
	{
		if(!StringUtil.isBlank(bolNo) || !StringUtil.isBlank(containerNo))
		{
			String[] status = getStatusExceptClosed(WmsOrderTypeKey.DELIVERY, adid, request);
			return floorSQLServerMgrZJ.findReceiptsByReceiptNoBolCtnr(bolNo, containerNo,receiptNo, CompanyID,CustomerID,null);
		}
		else
		{
			return new DBRow[0];
		}
	}
	
	
	public void setFloorSQLServerMgrZJ(FloorSQLServerMgrZJ floorSQLServerMgrZJ) {
		this.floorSQLServerMgrZJ = floorSQLServerMgrZJ;
	}

	public TransactionTemplate getSqlServerTransactionTemplate() {
		return sqlServerTransactionTemplate;
	}

	public void setSqlServerTransactionTemplate(
			TransactionTemplate sqlServerTransactionTemplate) {
		this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
	}
	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}
	public DBRow[] getMasterBOLNos( DBRow[] filterRows) throws Exception{
		return this.floorSQLServerMgrZJ.getMasterBOLNos(filterRows);
	}
	
	public DBRow[] findPOTypeOfDN(String companyId) throws Exception{
		return this.floorSQLServerMgrZJ.findPOTypeOfDN(companyId);
	}
}

