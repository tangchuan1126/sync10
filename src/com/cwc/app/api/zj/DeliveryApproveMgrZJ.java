package com.cwc.app.api.zj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ccc.FloorComputeProductMgrCCC;
import com.cwc.app.floor.api.zj.FloorDeliveryApproveMgrZJ;
import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.iface.ll.BatchMgrIFaceLL;
import com.cwc.app.iface.zj.DeliveryApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.DeliveryWarehouseMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreEditLogMgrIFaceZJ;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.key.ApproveDeliveryKey;
import com.cwc.app.key.DeliveryOrderKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class DeliveryApproveMgrZJ implements DeliveryApproveMgrIFaceZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorDeliveryMgrZJ floorDeliveryMgrZJ;
	private FloorDeliveryApproveMgrZJ floorDeliveryApproveMgrZJ;
	private ProductMgr productMgr;
	private DeliveryWarehouseMgrIFaceZJ deliveryWarehouseMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorPurchaseMgr floorPurchaseMgr;
	private PurchaseIFace purchaseMgr;
	private FloorComputeProductMgrCCC floorComputeProductMgr;
	private ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ;
	private BatchMgrIFaceLL batchMgrLL;
	/**
	 * 交货单申请完成
	 * @param request
	 * @throws Exception
	 */
	public void addDeliveryApprove(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long delivery_order_id = StringUtil.getLong(request,"delivery_order_id");
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			addDeliveryApproveSub(delivery_order_id, adminLoggerBean);
//			DBRow[] differences = floorDeliveryMgrZJ.checkDeliveryOrderApprove(delivery_order_id);
//			
//			String commit_account = adminLoggerBean.getAccount();
//			String commit_date = DateUtil.NowStr();
//			int different_count = differences.length;
//			
//			DBRow dbrow = new DBRow();
//			dbrow.add("delivery_order_id",delivery_order_id);
//			dbrow.add("commit_account",commit_account);
//			dbrow.add("commit_date",commit_date);
//			dbrow.add("different_count",different_count);
//			
//			
//			long da_id = floorDeliveryApproveMgrZJ.addDeliveryApprove(dbrow);
//			
//			for (int i = 0; i < differences.length; i++) 
//			{
//				String product_name = differences[i].getString("product_name");
//				String product_code = differences[i].getString("product_barcode");
//				long product_id = differences[i].get("product_id",0l);
//				float delivery_count = differences[i].get("delivery_count",0f);
//				float delivery_reap_count = differences[i].get("delivery_reap_count",0f);
//				
//				DBRow difference = new DBRow();
//				difference.add("product_name",product_name);
//				difference.add("product_code",product_code);
//				difference.add("product_id",product_id);
//				difference.add("delivery_count",delivery_count);
//				difference.add("reap_count",delivery_reap_count);
//				difference.add("da_id",da_id);
//				
//				floorDeliveryApproveMgrZJ.addDeliveryApproveDetail(difference);
//			}
//			
//			DBRow para = new DBRow();
//			para.add("delivery_order_status",DeliveryOrderKey.APPROVEING);
//			floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, para);//修改交货单为待审核状态
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void addDeliveryApproveSub(long delivery_order_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		DBRow[] differences = floorDeliveryMgrZJ.checkDeliveryOrderApprove(delivery_order_id);
		
		String commit_account = adminLoggerBean.getEmploye_name();
		String commit_date = DateUtil.NowStr();
		int different_count = differences.length;
		
		DBRow dbrow = new DBRow();
		dbrow.add("delivery_order_id",delivery_order_id);
		dbrow.add("commit_account",commit_account);
		dbrow.add("commit_date",commit_date);
		dbrow.add("different_count",different_count);
		
		
		long da_id = floorDeliveryApproveMgrZJ.addDeliveryApprove(dbrow);
		
		for (int i = 0; i < differences.length; i++) 
		{
			String product_name = differences[i].getString("product_name");
			String product_code = differences[i].getString("product_barcode");
			long product_id = differences[i].get("product_id",0l);
			float delivery_count = differences[i].get("delivery_count",0f);
			float delivery_reap_count = differences[i].get("delivery_reap_count",0f);
			
			DBRow difference = new DBRow();
			difference.add("product_name",product_name);
			difference.add("product_code",product_code);
			difference.add("product_id",product_id);
			difference.add("delivery_count",delivery_count);
			difference.add("reap_count",delivery_reap_count);
			difference.add("da_id",da_id);
			
			floorDeliveryApproveMgrZJ.addDeliveryApproveDetail(difference);
		}
		
		DBRow para = new DBRow();
		para.add("delivery_order_status",DeliveryOrderKey.APPROVEING);
		para.add("updatedate", DateUtil.DateStr());
		para.add("updateby", adminLoggerBean.getAdid());
		para.add("updatename", adminLoggerBean.getEmploye_name());
		floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, para);//修改交货单为待审核状态
		DeliveryOrderKey deliveryOrderKey = new DeliveryOrderKey();
		
		((DeliveryApproveMgrIFaceZJ)AopContext.currentProxy()).approveDeliveryOrderForJbpm(da_id);//工作流拦截交货单审核
	}
	
	/**
	 * 工作流拦截交货单审核
	 * @param da_id
	 * @return
	 * @throws Exception
	 */
	public long approveDeliveryOrderForJbpm(long da_id)
		throws Exception
	{
		return (da_id);
	}

	/**
	 * 获得的交货审核单
	 * @param ps_id
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allDeliveryApprove(long ps_id, PageCtrl pc,int approve_status, String sorttype) 
		throws Exception 
	{
		try 
		{
			return (floorDeliveryApproveMgrZJ.allDeliveryApprove(ps_id, pc, approve_status, sorttype));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	/**
	 * 交货审核
	 * @param da_id
	 * @param dod_ids
	 * @param notes
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void approveDeliveryDifferent(long da_id, long[] dod_ids,String[] notes, AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		DBRow modpara = new DBRow();
		for (int i = 0; i < dod_ids.length; i++) 
		{
			modpara.add("note",notes[i]);
			modpara.add("approve_account",adminLoggerBean.getEmploye_name());
			modpara.add("approve_status",ApproveDeliveryKey.APPROVE);
			
			floorDeliveryApproveMgrZJ.modDeliveryApproveDetail(dod_ids[i],modpara);
		}
		
		DBRow[] noApprove = floorDeliveryApproveMgrZJ.getDeliveryApproveDetailsWithStatus(da_id,ApproveDeliveryKey.WAITAPPROVE);//查找未审核的不同
		DBRow[] hasApprove = floorDeliveryApproveMgrZJ.getDeliveryApproveDetailsWithStatus(da_id,ApproveDeliveryKey.APPROVE);//查找已审核的不同
		
		
		DBRow modApproveDelivery = new DBRow();
		long delivery_order_id = floorDeliveryApproveMgrZJ.getDetailDeliveryApproveByDaid(da_id).get("delivery_order_id",0l);
		if(noApprove.length == 0)//差异都已审核过，变更采购单审核状态
		{
			modApproveDelivery.add("approve_status",1);
			modApproveDelivery.add("approve_account",adminLoggerBean.getEmploye_name());
			modApproveDelivery.add("approve_date",DateUtil.NowStr());
			
			modApproveDelivery.add("difference_approve_count",hasApprove.length);
			floorDeliveryApproveMgrZJ.modDeliveryApprove(da_id,modApproveDelivery);
			
			//交货单更改状态
			
			
			DBRow updatepara = new DBRow();
			DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);//获得交货单
			TDate tDate = new TDate();
			TDate endDate = new TDate(deliveryOrder.getString("delivery_date")+" 00:00:00");
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			updatepara.add("all_over", diffDay);//获取结束天数
			updatepara.add("delivery_order_status",DeliveryOrderKey.FINISH);		
			
			floorDeliveryMgrZJ.modDelveryOrder(delivery_order_id, updatepara);
			stockIn(delivery_order_id, adminLoggerBean,ProductStoreOperationKey.IN_STORE_DELIVERY_AUDIT);//入库
		}
		else
		{
			modApproveDelivery.add("difference_approve_count",hasApprove.length);
			floorDeliveryApproveMgrZJ.modDeliveryApprove(da_id, modApproveDelivery);
		}
	}

	/**
	 * 查看交货审核单详细
	 * @param da_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryApproverDetailsByDaid(long da_id, PageCtrl pc)
		throws Exception 
	{
		return floorDeliveryApproveMgrZJ.getDeliveryApproverDetailsByDaid(da_id,null);
	}

	public void stockIn(long delivery_order_id,AdminLoginBean adminLoggerBean, int product_storep_oeration) throws Exception {
		try {
			DBRow[] deliveryOrderDetails = deliveryWarehouseMgrZJ.getDeliveryByDeliveryOrderId(delivery_order_id);
			DBRow deliveryOrder = floorDeliveryMgrZJ.getDetailDelveryOrderById(delivery_order_id);
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(deliveryOrder.get("delivery_purchase_id",0l));//确定对应的采购单
			for (int i = 0; i < deliveryOrderDetails.length; i++) 
			{
				float count = Float.valueOf(deliveryOrderDetails[i].getString("dw_count"));//获得交货数量
				ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
				inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
				inProductStoreLogBean.setOid(delivery_order_id);
				inProductStoreLogBean.setBill_type(ProductStoreBillKey.DELIVERY_ORDER);
				inProductStoreLogBean.setOperation(product_storep_oeration);
				DBRow product = null;
						//商品条码可不惟一 zyj
						//productMgr.getDetailProductByPcode(deliveryOrderDetails[i].getString("dw_product_barcode"));//为了获得商品ID
				if (product!=null) //以防商品被删除
				{
					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,deliveryOrder.get("delivery_psid",0l),product.get("pc_id", 0l),count,0);//库存添加
					TDate td = new TDate();
					long datemark = td.getDateTime();
					String pcode = product.getString("p_code");
					long adid = adminLoggerBean.getAdid();
					int type = InOutStoreKey.IN_STORE_PURCHASE;
					productMgr.addInProductLog(pcode, deliveryOrder.get("delivery_psid",0l), count, adid,datemark,1, type);//入库日志
					if(deliveryOrderDetails[i].getFieldNames().size()==4)
					{
						String barcode = deliveryOrderDetails[i].getString("product_barcod");
						String position = deliveryOrderDetails[i].getString("storage");
						String machine_id = deliveryOrderDetails[i].getString("machine_id");
//						productMgr.addStorageLocation(deliveryOrder.get("delivery_psid",0l),product.get("pc_id",0l),barcode,count,position,machine_id);
					}
				}
			}
			
			deliveryWarehouseMgrZJ.delDeliveryWareHouseByDeliveryOrderNumber(deliveryOrder.getString("delivery_order_number"));//清除虚拟交货数据
			
			purchaseMgr.incomingSub(deliveryOrderDetails,purchase.get("purchase_id",0l), "", adminLoggerBean);//采购单入库
			batchMgrLL.batchDeliveryOrderStockin(delivery_order_id, adminLoggerBean);
			
			String returnStr,returnStr2;
			returnStr = this.floorComputeProductMgr.computePriceForDelivery(delivery_order_id);
			   if(!"".equals(returnStr))
			   {
			    throw new Exception(returnStr);
			   }
			   returnStr2 = this.floorComputeProductMgr.computeFreightForDelivery(delivery_order_id);
			   if(!"".equals(returnStr2))
			   {
			    throw new Exception(returnStr2);
			   }
		   for (int i = 0; i < deliveryOrderDetails.length; i++) 
		   {
			   DBRow product = new DBRow();
			   //商品条码可不惟一 zyj
					   //productMgr.getDetailProductByPcode(deliveryOrderDetails[i].getString("dw_product_barcode"));//为了获得商品ID
			   productStoreEditLogMgrZJ.addProductStoreLogForPcidAndPsid(product.get("pc_id",0l), deliveryOrder.get("delivery_psid",0l), adminLoggerBean, 1);
		   }
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"stockIn",log);
		} 
	}
	public void setFloorDeliveryMgrZJ(FloorDeliveryMgrZJ floorDeliveryMgrZJ) {
		this.floorDeliveryMgrZJ = floorDeliveryMgrZJ;
	}

	public void setFloorDeliveryApproveMgrZJ(
			FloorDeliveryApproveMgrZJ floorDeliveryApproveMgrZJ) {
		this.floorDeliveryApproveMgrZJ = floorDeliveryApproveMgrZJ;
	}
	
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	public void setDeliveryWarehouseMgrZJ(
			DeliveryWarehouseMgrIFaceZJ deliveryWarehouseMgrZJ) {
		this.deliveryWarehouseMgrZJ = deliveryWarehouseMgrZJ;
	}
	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorComputeProductMgr(
			FloorComputeProductMgrCCC floorComputeProductMgr) {
		this.floorComputeProductMgr = floorComputeProductMgr;
	}
	public void setProductStoreEditLogMgrZJ(
			ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ) {
		this.productStoreEditLogMgrZJ = productStoreEditLogMgrZJ;
	}
	public void setBatchMgrLL(BatchMgrIFaceLL batchMgrLL) {
		this.batchMgrLL = batchMgrLL;
	}
}
