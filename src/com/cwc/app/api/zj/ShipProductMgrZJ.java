package com.cwc.app.api.zj;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;

import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.zj.FloorShipProductMgrZJ;
import com.cwc.app.iface.zj.ShipProductMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class ShipProductMgrZJ implements ShipProductMgrIFaceZJ {

	private FloorShipProductMgrZJ floorShipProductMgrZJ;
	static Logger log = Logger.getLogger("ACTION");
	

	/**
	 * 获得今天发货的外部订单号（只包含USPS）
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTodayShipProduct() 
		throws Exception 
	{
		return (floorShipProductMgrZJ.getTodayShipProduct());
	}
	
	/**
	 * 上传全部外部运单号
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String upload(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String msg="";
			
			upload.setFileName("DeliverCode"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
							
			if (flag==2)
			{
				throw new FileTypeException();//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				msg = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadPurchaseDetail",log);
		}
	}
	
	/**
	 * 上传excel转换DBRow
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelDBRow(String filename) 
		throws Exception
	{
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		HashMap fieldNames = new HashMap();
		fieldNames.put(0,"delivery_code");
		
		
		String path =  Environment.getHome() + "upl_excel_tmp/" + filename;
		InputStream is = new FileInputStream(path);
		Workbook rwb = Workbook.getWorkbook(is);

		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数

		for(int i=1;i<rsRows;i++)
		{
		   DBRow pro = new DBRow();

		   
		   boolean result = false;//判断是否纯空行数据
		   for (int j=0;j<rsColumns;j++)
		   {
				if(fieldNames.containsKey(j))
				{
					String content = rs.getCell(j,i).getContents().trim();
					if(!content.equals(""))
					{
						Pattern pattern = Pattern.compile("\\d*");
						Matcher matcher = pattern.matcher(content);
						if(matcher.matches())
						{
							pro.add(fieldNames.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
							result = true;
						}
					}
					
				}
		   }
		   
		   if(result)//不是纯空行就加入到DBRow
		   {
			   al.add(pro);   
		   }
		}
		  
		return (al.toArray(new DBRow[0]));
	}
	
	/**
	 * 比较上传外部运单号excel
	 * @return
	 * @throws Exception
	 */
	public DBRow[] errorDeliveryCode(HttpServletRequest request)
		throws Exception
	{
		String filename=StringUtil.getString(request,"tempfilename");
		DBRow[] allDelivery_code = excelDBRow(filename);
		DBRow[] todayDelivery_code = floorShipProductMgrZJ.getTodayShipProduct();
		
		HashMap todayMap = new HashMap();//实际发货外部运单号
		for (int i = 0; i < todayDelivery_code.length; i++) 
		{
			todayMap.put(todayDelivery_code[i].getString("delivery_code"),todayDelivery_code[i].getString("delivery_code"));
		}
		
		ArrayList<DBRow> errorlist = new ArrayList<DBRow>();
		for(int j = 0; j<allDelivery_code.length; j++)
		{
			if(!todayMap.containsKey(allDelivery_code[j].getString("delivery_code")))//判断上传文件内外单号是否为今天实际发货外部运单号
			{
				//不包含，加入集合
				errorlist.add(allDelivery_code[j]);
			}
		}
		
		return (errorlist.toArray(new DBRow[0]));
		
	}
	
	public void setFloorShipProductMgrZJ(FloorShipProductMgrZJ floorShipProductMgrZJ) {
		this.floorShipProductMgrZJ = floorShipProductMgrZJ;
	}
}
