package com.cwc.app.api.zj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.storageApprove.HasWaitApproveAreaException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorStorageApproveMgrZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zj.StorageApproveMgrIFaceZJ;
import com.cwc.app.key.ApproveStatusKey;
import com.cwc.app.key.ContainerLocationDifferentTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductStorePhysicalOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.RelationBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageApproveMgrZJ implements StorageApproveMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorStorageApproveMgrZJ floorStorageApproveMgrZJ;
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private FloorProductStoreMgr productStoreMgr;
	private FloorProductMgr floorProductMgr;
	
	public int storageTakeStock(long area_id, AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		try 
		{
			int differentsCount = 0;//商品数量差异
			DBRow[] differentSlcs = floorStorageApproveMgrZJ.getStorageLocationSlcDifferent(area_id);
			if (differentSlcs.length>0) 
			{
				//添加
				DBRow storageApproveArea = new DBRow();
				storageApproveArea.add("area_id",area_id);
				storageApproveArea.add("post_date",DateUtil.NowStr());
				storageApproveArea.add("commit_account",adminLoggerBean.getAdid());
				storageApproveArea.add("difference_location_count",differentSlcs.length);
				storageApproveArea.add("approve_status",ApproveStatusKey.WAITAPPROVE);
				storageApproveArea.add("ps_id",adminLoggerBean.getPs_id());
				
				long saa_id = floorStorageApproveMgrZJ.addStorageApproveArea(storageApproveArea);
				
				for (int i = 0; i < differentSlcs.length; i++) 
				{
					long slc_id = differentSlcs[i].get("slc_id",0l);
					
					DBRow[] differents = floorStorageApproveMgrZJ.getStorageLocationDifferentsBySlc(slc_id);
					
					differentsCount += differents.length;
					//添加位置审核
					DBRow storageApproveLocation = new DBRow();
					storageApproveLocation.add("slc_id",differentSlcs[i].get("slc_id",0l));
					storageApproveLocation.add("different_count",differents.length);
					storageApproveLocation.add("saa_id",saa_id);
					storageApproveLocation.add("ps_id",adminLoggerBean.getPs_id());
					
					long sal_id = floorStorageApproveMgrZJ.addStorageApproveLocation(storageApproveLocation);
					
					for (int j = 0; j < differents.length; j++) 
					{
						//添加位置商品差异
						DBRow storageLocationDifferent = new DBRow();
						storageLocationDifferent.add("actual_store",differents[j].get("takestock_quantity",0f));
						storageLocationDifferent.add("sys_store",differents[j].get("physical_quantity",0f));
						storageLocationDifferent.add("sal_id",sal_id);
						storageLocationDifferent.add("approve_status",ApproveStatusKey.WAITAPPROVE);
						storageLocationDifferent.add("pc_id",differents[j].get("pc_id",0l));
						storageLocationDifferent.add("slc_id",differents[j].get("slc_id",0l));
						storageLocationDifferent.add("ps_id",adminLoggerBean.getPs_id());

						floorStorageApproveMgrZJ.addStorageLocationDifferents(storageLocationDifferent);
					}
				}
			}
			return differentsCount;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 获得区域内的所有位置的不同
	 */
	public DBRow[] getDifferentsForArea(long area_id)
		throws Exception
	{
		try 
		{
			return floorStorageApproveMgrZJ.getStorageLocationAreaDifferents(area_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDifferentsForArea",log);
		}
	}
	
	/**
	 * 区域审核过滤
	 * @param ps_id
	 * @param area_id
	 * @param status
	 * @param sortby
	 * @param sort
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterStorageApproveArea(long ps_id,long area_id,int status,String sortby,boolean sort,PageCtrl pc)
		throws Exception
	{
		try
		{
			return floorStorageApproveMgrZJ.filterStorageApproveArea(ps_id, area_id, status, sortby, sort, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterStorageApproveArea",log);
		}
	}
	
	/**
	 * 根据区域审核获得仓库位置审核
	 * @param saa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageApproveLocationBySaaId(long saa_id)
		throws Exception
	{
		try 
		{
			return floorStorageApproveMgrZJ.getStorageApproveLocationBySaaId(saa_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStorageApproveLocationBySaaId",log);
		}
	}
	
	/**
	 * 根据仓库审核查询盘点不同
	 * @param sal_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationDifferentsBySal(long sal_id)
		throws Exception
	{
		try 
		{
			return floorStorageApproveMgrZJ.getStorageLocationDifferentsBySal(sal_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 审核
	 */
	public void approveStorageApproveDifferents(HttpServletRequest request)
		throws Exception 
	{
		long con_id = StringUtil.getLong(request,"con_id");
		long sac_id = StringUtil.getLong(request,"sac_id");
		long ps_id = StringUtil.getLong(request,"ps_id");
		long slc_id = StringUtil.getLong(request,"slc_id");
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		
		Map<String,Object> searchFor = new HashMap<String, Object>();
		searchFor.put("con_id",con_id);
		RelationBean[] reb = productStoreMgr.searchLocates(0,NodeType.Container, searchFor);
		String lot_number = (String) reb[0].getRelProps().get("lot_number");
		long title_id = Long.parseLong(reb[0].getRelProps().get("title_id").toString());
		long time_number = Long.parseLong(reb[0].getRelProps().get("time_number").toString());
		
		long pc_id = 0;
		if (sac_id!=0)
		{
			String[] sldIdsString = request.getParameterValues("sld_id");
			long[] sld_ids = new long[sldIdsString.length];
			String[] notes = new String[sld_ids.length];
			
			
			for (int i = 0; i < sldIdsString.length; i++) 
			{
				sld_ids[i] = Long.parseLong(sldIdsString[i]);
				notes[i] = StringUtil.getString(request,"note_"+sldIdsString[i]);
			}
			
			DBRow storageApproveContainer = floorStorageApproveMgrZJ.getDetailStorageApproveContainer(sac_id);
			
			long saa_id = storageApproveContainer.get("saa_id",0l);
			ps_id = storageApproveContainer.get("ps_id",0l);
			slc_id = storageApproveContainer.get("slc_id",0l);
			
			for (int i = 0; i < sld_ids.length; i++) 
			{
				DBRow storageLocationDifferent = floorStorageApproveMgrZJ.getDetailStorageLocationDifferent(sld_ids[i]);
				pc_id = storageLocationDifferent.get("pc_id",0l);
				float actual_store = storageLocationDifferent.get("actual_store",0f);
				float sys_store = storageLocationDifferent.get("sys_store",0f);
				float quantity = actual_store-sys_store;
				
				DBRow paraDifferent = new DBRow();
				paraDifferent.add("approve_status",ApproveStatusKey.APPROVE);
				paraDifferent.add("note",notes[i]);
				floorStorageApproveMgrZJ.modStorageLocationDifferent(sld_ids[i],paraDifferent);
				
				int physicalOperation;
				
				if (quantity>0)
				{
					physicalOperation = ProductStorePhysicalOperationKey.LayUp;
				}
				else
				{
					physicalOperation = ProductStorePhysicalOperationKey.PickUp;
				}
				//记录库存修改日志，方法内无库存加减，因图数据库关系，采用copytree的方式
				productStoreMgrZJ.differentContainerApprove(ps_id, slc_id, pc_id, (int) quantity, con_id, title_id, physicalOperation,ProductStoreOperationKey.IN_OUT_STORE_REVISE,ProductStoreBillKey.STORAGE_APPROVE,saa_id, lot_number,"","", adminLoggerBean);
			}
		}
		
		//从临时库内拷贝容器树到实例库
		productStoreMgrZJ.differentContainerApproveCopyTree(ps_id, con_id,slc_id,pc_id,title_id,lot_number,time_number);
		productStoreMgr.containers(0,con_id);
		
		
		//修改容器审核
		DBRow paraStorageApproveContainer = new DBRow();
		paraStorageApproveContainer.add("approve_status",ApproveStatusKey.APPROVE);
		paraStorageApproveContainer.add("approve_adid",adminLoggerBean.getAdid());
		paraStorageApproveContainer.add("approve_date",DateUtil.NowStr());
		
		floorStorageApproveMgrZJ.modStorageApproveContainer(sac_id, paraStorageApproveContainer);
		
		//修改位置审核
		long sal_id = this.approveStorageApproveLocationBySac(sac_id, adminLoggerBean);
		//区域审核
		this.approveStorageApproveAreaBySal(sal_id, adminLoggerBean);
		
		
	}
	
	/**
	 * 仓库区域审核
	 * @param sal_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	private void approveStorageApproveAreaBySal(long sal_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		DBRow storageApproveLocation = floorStorageApproveMgrZJ.getDetailStorageApproveLocation(sal_id);
		
		long saa_id = storageApproveLocation.get("saa_id",0l);
		
		DBRow storageApproveArea = floorStorageApproveMgrZJ.getDetailStorageApproveArea(saa_id);
		int difference_location_count = storageApproveArea.get("difference_location_count",0);
		int difference_approve_location_count = floorStorageApproveMgrZJ.getStorageApproveContainerCountBySaa(saa_id);
		long ps_id = storageApproveArea.get("ps_id",0l);
		long slc_area= storageApproveArea.get("area_id",0l);
		
		DBRow paraStorageApproveArea = new DBRow();
		paraStorageApproveArea.add("difference_approve_location_count",difference_approve_location_count);
		if (difference_location_count==difference_approve_location_count) 
		{
			paraStorageApproveArea.add("approve_status",ApproveStatusKey.APPROVE);
			paraStorageApproveArea.add("approve_account",adminLoggerBean.getAdid());
			paraStorageApproveArea.add("approve_date",DateUtil.NowStr());
		}
		
		floorStorageApproveMgrZJ.modStorageApproveArea(saa_id, paraStorageApproveArea);
		
		if (paraStorageApproveArea.get("approve_status",0)==ApproveStatusKey.APPROVE)
		{
			productStoreMgr.deleteOnArea(0,slc_area);
		}
	}
	
	private long approveStorageApproveLocationBySac(long sac_id,AdminLoginBean adminLoggerBean)
			throws Exception
	{
		DBRow storageApproveContainer = floorStorageApproveMgrZJ.getDetailStorageApproveContainer(sac_id);
		
		long sal_id = storageApproveContainer.get("sal_id",0l);
		
		DBRow storageApproveLocation = floorStorageApproveMgrZJ.getDetailStorageApproveLocation(sal_id);
		int difference_container_count = storageApproveLocation.get("different_container_count",0);
		int different_approve_container_count = floorStorageApproveMgrZJ.getStorageApproveContainerCountBySal(sal_id);
		
		DBRow paraStorageApproveLocation = new DBRow();
		paraStorageApproveLocation.add("different_approve_container_count",different_approve_container_count);
		if (difference_container_count==different_approve_container_count)
		{
			paraStorageApproveLocation.add("approve_status",ApproveStatusKey.APPROVE);
			paraStorageApproveLocation.add("approve_adid",adminLoggerBean.getAdid());
			paraStorageApproveLocation.add("approve_date",DateUtil.NowStr());
		}
		
		floorStorageApproveMgrZJ.modStorageApproveLocation(sal_id,paraStorageApproveLocation);
		
		return sal_id;
	}
	
	/**
	 * 根据区域ID返回差异容器库存(主要是容器关系)
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] storageContainerDifferentByArea(long area_id)
			throws Exception
	{
		try
		{
			DBRow[] location = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id, null, null, null);
			ArrayList<DBRow> differentContainer = new ArrayList<DBRow>();
			long ps_id = 0;
			for (int i = 0; i < location.length; i++) 
			{
				long slc_id = location[i].get("slc_id",0l);
				ps_id = location[i].get("slc_psid",0l);
				
				Set<Long> differents = productStoreMgr.compareTree(ps_id,0,slc_id);
				for (Long con_id : differents) 
				{
					Map<String,Object> searchFor = new HashMap<String, Object>();
					searchFor.put("con_id",con_id);
					
					RelationBean[] sysLocates = productStoreMgr.searchLocates(ps_id,NodeType.Container,searchFor);
					RelationBean[] tempLocates = productStoreMgr.searchLocates(0,NodeType.Container,searchFor);
					
					int different_status = sysLocates.length-tempLocates.length;
					
					int different_type = 0;
					
					if (different_status==0)
					{
						different_type = ContainerLocationDifferentTypeKey.Different;
					}
					else if (different_status>0)
					{
						different_type = ContainerLocationDifferentTypeKey.Missing;
					}
					else if (different_status<0)
					{
						different_type = ContainerLocationDifferentTypeKey.Exceed;
					}

					DBRow row = new DBRow();
					row.add("slc_id",slc_id);
					row.add("con_id",con_id);
					row.add("ps_id",ps_id);
					row.add("different_type",different_type);
					
					differentContainer.add(row);
				}
			}
			return differentContainer.toArray(new DBRow[0]);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"storageContainerDifferentByArea",log);
		}
	}
	
	/**
	 * 根据区域获得容器商品数量不正确的内容,由图数据库比较
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] storageContainerProductCountDifferentByArea(long area_id)
			throws Exception
	{
		try
		{
			ArrayList<DBRow> areaProductDifferent = new ArrayList<DBRow>();
			DBRow[] location = floorLocationMgrZJ.getLocationCatalogsByAreaId(area_id, null, null, null);
			long ps_id = 0;
			for (int i = 0; i < location.length; i++) 
			{
				long slc_id = location[i].get("slc_id",0l);
				ps_id = location[i].get("slc_psid",0l);
				DBRow[] differents = productStoreMgr.compareLocatesQuantity(ps_id,0, slc_id);
				
				for (int j = 0; j < differents.length; j++) 
				{
					long pc_id = differents[j].get("pc_id",0l);
					long con_id = differents[j].get("con_id",0l);
					int sys_store = differents[j].get("src_quantity",0);
					int actual_store = differents[j].get("dest_quantity",0);

					DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
					DBRow differentContainerProduct = new DBRow();
					differentContainerProduct.add("sys_store",sys_store);
					differentContainerProduct.add("actual_store",actual_store);
					differentContainerProduct.add("pc_id",pc_id);
					differentContainerProduct.add("ps_id",ps_id);
					differentContainerProduct.add("con_id",con_id);
					differentContainerProduct.add("slc_id",slc_id);
					differentContainerProduct.add("ps_id",ps_id);
					differentContainerProduct.add("location",location[i].getString("slc_position_all"));
					differentContainerProduct.add("p_name",product.getString("p_name"));
					
					areaProductDifferent.add(differentContainerProduct);
				}
			}
			
			return areaProductDifferent.toArray(new DBRow[0]);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"storageContainerProductCountDifferentByArea",log);
		}
	}
	
	public int storageTakeStockNew(long area_id, AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			int different_product_count = 0;//差异的商品数量
			DBRow area = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			long ps_id = area.get("area_psid",0l);
			long saa_id = 0;
			
			DBRow[] hasWaitApproveArea = floorStorageApproveMgrZJ.getWaitStorageApproveArea(area_id);
			
			if (hasWaitApproveArea.length>0)
			{
				throw new HasWaitApproveAreaException();
			}
			
			
			DBRow[] areaContainerProductDifferents = this.storageContainerProductCountDifferentByArea(area_id);
			DBRow[] areaContainerDifferents = this.storageContainerDifferentByArea(area_id);
			
			if (areaContainerProductDifferents.length>0||areaContainerDifferents.length>0)
			{
				saa_id = this.addStorageApproveArea(area_id, adminLoggerBean);
				
				for (int i = 0; i < areaContainerProductDifferents.length; i++) 
				{
					long con_id = areaContainerProductDifferents[i].get("con_id",0l);
					long pc_id = areaContainerProductDifferents[i].get("pc_id",0l);
					int actual_store = areaContainerProductDifferents[i].get("actual_store",0);
					int sys_store = areaContainerProductDifferents[i].get("sys_store",0);
					long slc_id = areaContainerProductDifferents[i].get("slc_id",0l);
					
					this.addStorageLocationDifferents(saa_id, actual_store, sys_store, pc_id,con_id,slc_id, ps_id);
				}
				
				for (int i = 0; i < areaContainerDifferents.length; i++) 
				{
					long con_id = areaContainerDifferents[i].get("con_id",0l);
					long slc_id = areaContainerDifferents[i].get("slc_id",0l);
					int different_type = areaContainerDifferents[i].get("different_type",0);
					
					
					this.addStorageLocationDifferentsContainer(saa_id, con_id, slc_id, ps_id,different_type);
				}
				
				DBRow[] storageApproveSlcs = floorStorageApproveMgrZJ.getLocationDifferentSlc(saa_id);
				for (int i = 0; i < storageApproveSlcs.length; i++) 
				{
					long slc_id = storageApproveSlcs[i].get("slc_id",0l);
					DBRow[] storageApproveContainers = floorStorageApproveMgrZJ.getLocationDifferentContainer(saa_id, slc_id);
					
					DBRow storageApproveSlc = new DBRow();
					storageApproveSlc.add("slc_id",slc_id);
					storageApproveSlc.add("different_container_count",storageApproveContainers.length);
					storageApproveSlc.add("different_approve_container_count",0);
					storageApproveSlc.add("saa_id",saa_id);
					storageApproveSlc.add("approve_status",ApproveStatusKey.WAITAPPROVE);
					storageApproveSlc.add("ps_id",ps_id);

					long sal_id = floorStorageApproveMgrZJ.addStorageApproveLocation(storageApproveSlc);
					
					for (int j = 0; j < storageApproveContainers.length; j++) 
					{
						long con_id = storageApproveContainers[j].get("con_id",0l);
						DBRow[] storageLocationDifferents = floorStorageApproveMgrZJ.getLocationDifferents(saa_id, con_id);
						
						DBRow storageApproveContainer = new DBRow();
						
						storageApproveContainer.add("different_count",storageLocationDifferents.length);
						storageApproveContainer.add("different_approve_count",0);
						storageApproveContainer.add("approve_con_id",con_id);
						storageApproveContainer.add("approve_status",ApproveStatusKey.WAITAPPROVE);
						storageApproveContainer.add("slc_id",slc_id);
						storageApproveContainer.add("ps_id",ps_id);
						storageApproveContainer.add("sal_id",sal_id);
						storageApproveContainer.add("saa_id",saa_id);
						
						long sac_id = floorStorageApproveMgrZJ.addStorageApproveContainer(storageApproveContainer);
						
						floorStorageApproveMgrZJ.modStorageLocationDifferentSac(saa_id,con_id,sac_id);
					}
				}
				
				DBRow storageApproveAreaPara = new DBRow();
				storageApproveAreaPara.add("difference_location_count",storageApproveSlcs.length);
					
				floorStorageApproveMgrZJ.modStorageApproveArea(saa_id, storageApproveAreaPara);
			}
			return different_product_count;
		} 
		catch (HasWaitApproveAreaException e)
		{
			throw e;
		}
		
		catch (Exception e) 
		{
			throw new SystemException(e,"storageTakeStockNew",log);
		}
	}
	
	/**
	 * 添加差异商品
	 * @param saa_id
	 * @param actual_store
	 * @param sys_sotre
	 * @param pc_id
	 * @param con_id
	 * @param slc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public long addStorageLocationDifferents(long saa_id,int actual_store,int sys_sotre,long pc_id,long con_id,long slc_id,long ps_id)
		throws Exception
	{
		try 
		{
			DBRow storageLocationDifferent = new DBRow();
			storageLocationDifferent.add("actual_store",actual_store);
			storageLocationDifferent.add("sys_store",sys_sotre);
			storageLocationDifferent.add("saa_id",saa_id);
			storageLocationDifferent.add("approve_status",ApproveStatusKey.WAITAPPROVE);
			storageLocationDifferent.add("pc_id",pc_id);
			storageLocationDifferent.add("slc_id",slc_id);
			storageLocationDifferent.add("con_id",con_id);
			storageLocationDifferent.add("ps_id",ps_id);

			return floorStorageApproveMgrZJ.addStorageLocationDifferents(storageLocationDifferent);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addStorageLocationDifferents",log);
		}
	}
	
	/**
	 * 添加关系差异容器
	 * @param saa_id
	 * @param con_id
	 * @param slc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public long addStorageLocationDifferentsContainer(long saa_id,long con_id,long slc_id,long ps_id,int different_type)
			throws Exception
	{
		try 
		{
			DBRow row = new DBRow();
			row.add("saa_id",saa_id);
			row.add("con_id",con_id);
			row.add("slc_id",slc_id);
			row.add("ps_id",ps_id);
			row.add("different_type",different_type);
			
			return floorStorageApproveMgrZJ.addStorageLocationDifferentsContainer(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addStorageLocationDifferentsContainer",log);
		}
	}
	
	
	public long addStorageApproveArea(long area_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			DBRow storageApproveArea = new DBRow();
			storageApproveArea.add("area_id",area_id);
			storageApproveArea.add("post_date",DateUtil.NowStr());
			storageApproveArea.add("commit_account",adminLoggerBean.getAdid());
			storageApproveArea.add("difference_location_count",1);
			storageApproveArea.add("approve_status",ApproveStatusKey.WAITAPPROVE);
			storageApproveArea.add("ps_id",adminLoggerBean.getPs_id());
			
			return floorStorageApproveMgrZJ.addStorageApproveArea(storageApproveArea);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addStorageApproveArea",log);
		}
	}
	
	public DBRow[] getStorageApproveContainers(long saa_id,long slc_id)
		throws Exception
	{
		try
		{
			return floorStorageApproveMgrZJ.getStorageApproveContainers(saa_id, slc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStorageApproveContainers",log);
		}
	}
	
	public DBRow[] getLocationDifferents(long saa_id,long con_id)
		throws Exception
	{
		try 
		{
			return floorStorageApproveMgrZJ.getLocationDifferents(saa_id, con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLocationDifferents",log);
		}
	}
	
	public DBRow[] getContainerTreeToDTree(long ps_id,long con_id)
			throws Exception
	{
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		
		JSONObject containerTree = productStoreMgr.containerTree(ps_id, con_id, null, 0L, 0l, null, null);

//		DBRow container = new DBRow();
//		container.add("id",con_id);
//		container.add("parent_id",0);
//		container.add("text",containerTree.getString("container"));
//		list.add(container);
		if (!containerTree.toString().equals("{}"))
		{
			jsonContainerTreeToDBRow(containerTree,list,0);
		}
		return list.toArray(new DBRow[0]);
		
	}
	
	public ArrayList<DBRow> jsonContainerTreeToDBRow(JSONObject containerJson,ArrayList<DBRow> list,long parent_con_id)
			throws Exception
	{
		try 
		{
			long con_id = containerJson.getLong("con_id");
			String containertext = containerJson.getString("container");
			if (containertext.equals(""))
			{
				containertext = "N/A";
			}
			DBRow container = new DBRow();
			container.add("id",con_id);
			container.add("text",containertext);
			container.add("parent_id",parent_con_id);
			
			list.add(container);
			
			if (containerJson.has("children"))
			{
				JSONArray containerTree = containerJson.getJSONArray("children");
				for (int i = 0; i < containerTree.length(); i++) 
				{
					JSONObject innerContainerJson = containerTree.getJSONObject(i);
					jsonContainerTreeToDBRow(innerContainerJson, list, con_id);
				}
			}
			return list;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"jsonContainerTreeToDBRow",log);
		}
	}
	
	@Override
	public DBRow getDetailStorageApproveArea(long saa_id)
		throws Exception 
	{
		try
		{
			return floorStorageApproveMgrZJ.getDetailStorageApproveArea(saa_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailStorageApproveArea",log);
		}
	}
	
	@Override
	public DBRow getDetailStorageLocationDifferentsContainer(long saa_id,long con_id) 
		throws Exception 
	{
		try 
		{
			return floorStorageApproveMgrZJ.getDetailStorageLocationDifferentsContainer(saa_id, con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailStorageLocationDifferentsContainer",log);
		}
	}

	public void setFloorStorageApproveMgrZJ(
			FloorStorageApproveMgrZJ floorStorageApproveMgrZJ) {
		this.floorStorageApproveMgrZJ = floorStorageApproveMgrZJ;
	}

	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
}
