package com.cwc.app.api.zj;

import org.apache.log4j.Logger;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorReturnProductMgrZJ;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.iface.zj.OrderMgrIFaceZJ;
import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.OrderRateKey;
import com.cwc.app.key.PaymentMethodKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ServiceOrderStatusKey;
import com.cwc.app.key.TransactionType;
import com.cwc.app.lucene.OrderIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class OrderMgrZJ implements OrderMgrIFaceZJ {

	private FloorOrderMgrZJ floororderMgrZJ; 
	private FloorOrderMgr floororderMgr;
	static Logger log = Logger.getLogger("ACTION");
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private ProductMgr productMgr;
	private FloorReturnProductMgrZJ floorReturnProductMgrZJ;
	private FloorBillMgrZr floorBillMgrZr;
	private FloorProductMgr floorProductMgr;
	private FloorOrderMgrZr floorOrderMgrZr;
	private FloorServiceOrderMgrZyj floorServiceOrderMgrZyj;

	/**
	 * 根据订单编号返回订单产生的订单任务个数
	 * @param order_id
	 * @return
	 * @throws Exception
	 */
	public int getOrderTaskSum(long order_id) 
	throws Exception 
	{
		try 
		{
			return (floororderMgrZJ.getOrderTaskSum(order_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderTaskSum",log);
		}
	}
	
	/**
	 * payper_email留坏评次数
	 * @param client_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getOrderBadReview(String client_id) 
	throws Exception 
	{
		try 
		{
			int bad = floororderMgrZJ.getOrderBadReview(client_id,OrderRateKey.BAD);
			int normal = floororderMgrZJ.getOrderBadReview(client_id,OrderRateKey.NORMAL);
			int ebay_complain = floororderMgrZJ.getOrderBadReview(client_id,OrderRateKey.EBAY_COMPLAIN);
			int paypal_complain = floororderMgrZJ.getOrderBadReview(client_id,OrderRateKey.PAYPAL_COMPLAIN);
			
			DBRow data = new DBRow();
			data.add("bad", bad);
			data.add("normal", normal);
			data.add("ebay_complain", ebay_complain);
			data.add("paypal_complain", paypal_complain);
			
			return(data);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderBadReview",log);
		}
	}
	
	/**
	 * 批量修改订单
	 * @param orders
	 * @throws Exception
	 */
	public void modOrdersHandle(long[] oids,int handle)
		throws Exception
	{
		for (int i = 0; i < oids.length; i++)
		{
			DBRow modOrder = new DBRow();
			modOrder.add("handle",handle);//待发货
			floororderMgr.modPOrder(oids[i],modOrder);
		}
	}
	
	
	/**
	 * 根据订单号获得与此订单可能能合并发货的订单
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getResembleOrderByOid(long oid)
		throws Exception
	{
		try 
		{
			DBRow orderDelivery = floororderMgr.getDetailOrderDeliveryInfoByOid(oid); 
			
			String address_name = orderDelivery.getString("address_name").toLowerCase();
			long ccid = orderDelivery.get("ccid",0l);
			long pro_id = orderDelivery.get("pro_id",0l);
			String address_city = orderDelivery.getString("address_city").toLowerCase();
			String zip = orderDelivery.getString("address_zip").toLowerCase();
			
			return (floororderMgrZJ.getResembleOrder(address_name, ccid, pro_id, address_city, zip));
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getResembleOrderByOid",log);
		}
	}
	
	public void checkOrderShip(long oid)
		throws Exception
	{
		DBRow order = floororderMgr.getDetailPOrderByOid(oid);
		
		int handle = order.get("handle",0);
		
		if(handle==HandleKey.ALLOUTBOUND)
		{
			DBRow[] wayBillOrders = floorWayBillOrderMgrZJ.getUnfinishedWayBillOrderByOid(oid);
			
			if(wayBillOrders==null||wayBillOrders.length==0)
			{
				long all_cost = System.currentTimeMillis()- new TDate(order.getString("post_date")).getDateTime();
				
				DBRow para = new DBRow();
				para.add("handle",HandleKey.DELIVERIED);
				para.add("all_cost",all_cost);
				para.add("delivery_date",DateUtil.NowStr());//订单完整发货时间
				
				floororderMgr.modPOrder(oid,para);
			}
		}
	}
	
	/**
	 * 不需客户付款的质保发货订单
	 * @param business
	 * @return
	 */
	public long saveWarrantyOrder(String business,float quantity,String mc_currency,String client_id,long poid,long ccid,long pro_id,String address_name,String address_street,String address_city,String address_state,String address_zip,String tel,long bill_id,long sid)
		throws Exception
	{
		try
		{
			DBRow order = floororderMgr.getDetailPOrderByOid(poid);
			
			DBRow devOrder = new DBRow();
			devOrder.add("business",business);
			devOrder.add("quantity",quantity);
			devOrder.add("mc_gross",0);
			devOrder.add("mc_currency",mc_currency);
			devOrder.add("post_date",DateUtil.NowStr());
			devOrder.add("client_id",client_id);
			devOrder.add("auction_buyer_id",null!=order?order.getString("auction_buyer_id"):"");
			
			devOrder.add("order_source",OrderMgr.ORDER_SOURCE_WANRRANTY);
			devOrder.add("handle",HandleKey.WAIT4_RECORD);
			devOrder.add("handle_status",HandStatusleKey.DOUBT);
			devOrder.add("pay_type",PaymentMethodKey.Warranty);
			
			
			devOrder.add("pro_id",pro_id);
			
			if(pro_id!=-1)//省份并非other
			{
				DBRow detailProvince = productMgr.getDetailProvinceByProId(pro_id);
				if(detailProvince!=null)
				{
					address_state = detailProvince.getString("pro_name");
				}
			}

			devOrder.add("ccid",ccid);
			
			DBRow detailCountry = floororderMgr.getDetailCountryCodeByCcid(ccid);
			
			devOrder.add("address_country",detailCountry.getString("c_country"));
			
			devOrder.add("address_name",address_name);
			devOrder.add("address_street",address_street);
			devOrder.add("address_city",address_city);
			devOrder.add("address_state",address_state);
			devOrder.add("address_zip",address_zip);
			devOrder.add("tel",tel);
			
			long oid = floororderMgr.addPOrder(devOrder);
			
			OrderIndexMgr.getInstance().addIndex("",devOrder.getString("client_id"),oid,"",devOrder.getString("auction_buyer_id"),"","","");
			
			
			long ps_id = productMgr.getPriorDeliveryWarehouse(ccid, pro_id);//最优发货
			
			floororderMgr.delPOrderItem(oid);
			
			DBRow[] items = floorBillMgrZr.getBillItemByBillOrderId(bill_id);
			
			for (int i = 0; i < items.length; i++) 
			{
				DBRow orderItem = new DBRow();
				orderItem.add("oid",oid);
				orderItem.add("pid",items[i].get("pc_id",0l));
				//float quantity = Float.parseFloat(cartProducts[i].getString("cart_quantity"));
				
				orderItem.add("quantity",items[i].get("quantity",0f));//购买数量
				orderItem.add("wait_quantity",items[i].get("quantity",0f));//代发货数量
				
				DBRow product = productMgr.getDetailProductByPcid(items[i].get("pc_id",0l));
				orderItem.add("catalog_id",product.get("catalog_id",0l));
				
				orderItem.add("unit_price",product.get("unit_price",0d));
				orderItem.add("gross_profit",product.get("gross_profit", 0d));
				orderItem.add("weight",product.get("weight", 0f));
				orderItem.add("name",product.getString("p_name"));
				orderItem.add("unit_name",product.getString("unit_name"));
				orderItem.add("product_type",items[i].get("product_type",0));
				orderItem.add("lacking",ProductStatusKey.UNKNOWN);
				orderItem.add("prefer_ps_id",ps_id);
				orderItem.add("modality",1);//1代表原状代发形式
				long plan_ps_id = ps_id; 
				
				DBRow storageTransd = floorProductMgr.getStorageTransd(ps_id,product.get("catalog_id", 0l));
				if(storageTransd!=null)
				{
					plan_ps_id = storageTransd.get("d_sid",0l);
					orderItem.add("modality",storageTransd.get("modality_id",0));
				}
				orderItem.add("plan_ps_id",plan_ps_id);
				floororderMgr.addPOrderItemHasId(orderItem);
			}
			
			//创建发货订单后关联质保请求
			DBRow serviceOrder = new DBRow();
			serviceOrder.add("warranty_oid",oid);
			serviceOrder.add("status",ServiceOrderStatusKey.CREATEORDER);
			floorServiceOrderMgrZyj.updateServiceOrder(serviceOrder, sid);
			
			//创建我们付款的客户0元交易
			
			DBRow bill = floorBillMgrZr.getBillOrderByBillId(bill_id);

			DBRow trade = new DBRow();
			trade.add("client_id",devOrder.getString("client_id"));
			trade.add("relation_type",0);
			trade.add("relation_id",oid);
			trade.add("parent_txn_id ",devOrder.getString("txn_id"));
			trade.add("is_create_order",1);
			trade.add("transaction_type",TransactionType.Payment);
			trade.add("delivery_address_name",devOrder.getString("address_name"));
			trade.add("delivery_address_street",devOrder.getString("address_street"));
			trade.add("delivery_address_city",devOrder.getString("address_city"));
			trade.add("delivery_address_state",devOrder.getString("address_state"));
			trade.add("delivery_address_zip",devOrder.getString("address_zip"));
			trade.add("delivery_address_country_code",devOrder.getString("address_country_code"));
			trade.add("delivery_address_country",devOrder.getString("address_country"));
			trade.add("delivery_pro_id",devOrder.get("pro_id",0l));
			trade.add("delivery_ccid_id",devOrder.get("ccid",0l));
			trade.add("payer_email",bill.getString("payer_email"));
			trade.add("mc_gross",0d);
			
			floorOrderMgrZr.insertTrade(trade);
			
			
			return (oid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"saveWarrantyOrder",log);
		}
	}
	
	/**
	 * 运单追踪
	 * @param st
	 * @param en
	 * @param product_line_id
	 * @param catalog_id
	 * @param p_name
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param internal_tracking_status
	 * @param daytype
	 * @param lastday
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackingOrder(String st,String en,long product_line_id,long catalog_id,String p_name,long ca_id,long ccid,long pro_id,int internal_tracking_status,int daytype,int lastday,int extended_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			long pc_id = 0;
			if (product!=null) 
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return floororderMgrZJ.trackingOrder(st, en, product_line_id, catalog_id, pc_id, ca_id, ccid, pro_id, internal_tracking_status, daytype, lastday,extended_day,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"trackingOrder",log);
		}
	}

	public void setFloororderMgrZJ(FloorOrderMgrZJ floororderMgrZJ) {
		this.floororderMgrZJ = floororderMgrZJ;
	}

	public void setFloororderMgr(FloorOrderMgr floororderMgr) {
		this.floororderMgr = floororderMgr;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setFloorReturnProductMgrZJ(
			FloorReturnProductMgrZJ floorReturnProductMgrZJ) {
		this.floorReturnProductMgrZJ = floorReturnProductMgrZJ;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorBillMgrZr(FloorBillMgrZr floorBillMgrZr) {
		this.floorBillMgrZr = floorBillMgrZr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}

	public void setFloorServiceOrderMgrZyj(
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj) {
		this.floorServiceOrderMgrZyj = floorServiceOrderMgrZyj;
	}
	
}
