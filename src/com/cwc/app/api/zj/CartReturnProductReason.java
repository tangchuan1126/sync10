package com.cwc.app.api.zj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.iface.zj.CartReturnProductReasonIFace;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.ebay.soap.eBLBaseComponents.WarrantyDurationDetailsType;
import com.fr.third.org.apache.poi.hssf.record.formula.functions.Product;

public class CartReturnProductReason implements CartReturnProductReasonIFace {

	private FloorProductMgr floorProductMgr;
	private DBRow[] productReason = null;
	private float sumQuantity;
	
	/**
	 * 清空购物车
	 */
	public void clearReturnProductReasonCart(HttpSession session)
		throws Exception 
	{
		if(session.getAttribute(Config.cartReturnProductReason)!=null)
		{
			session.removeAttribute(Config.cartReturnProductReason);
		}
	}

	/**
	 * 加入购物车
	 */
	public void putToReturnProductReasonCart(HttpServletRequest request)
		throws Exception 
	{
		String p_name = StringUtil.getString(request,"p_name");
		
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		
		if(product==null)
		{
			throw new Exception();
		}
		
		long pc_id = product.get("pc_id",0l);
		float quantity = StringUtil.getFloat(request,"quantity");
		String reason = StringUtil.getString(request,"reason");
		int warranty_type = StringUtil.getInt(request,"warranty_type");
		
		removeReturnProductReason(StringUtil.getSession(request), pc_id);//删除重复商品
		
		put2CartReturnProductReason(StringUtil.getSession(request),p_name,pc_id,quantity,reason,warranty_type);//将商品质保放入质保明细购物车
		
	}
	
	public void putToReturnProductReasonCartForSelect(HttpServletRequest request)
		throws Exception
	{
		String value = StringUtil.getString(request,"value");
		long remove_pc_id = 0;
		long pc_id = 0;
		
		HttpSession session = StringUtil.getSession(request);
		
		if(value.contains("_"))//如果是XXX_XXX形式，那说明选择的是套装中的散件，要从购物车中清除套装商品再加入散件商品
		{
			String[] ids = value.split("_");
			
			remove_pc_id = Long.parseLong(ids[0]);
			this.removeReturnProductReason(session,remove_pc_id);
			
			pc_id = Long.parseLong(ids[1]);
		}
		else
		{
			pc_id = Long.parseLong(value);
			
			DBRow[] productUnions = floorProductMgr.getProductUnionsBySetPid(pc_id);
			
			for (int i = 0; i < productUnions.length; i++) 
			{
				this.removeReturnProductReason(session, productUnions[i].get("pid",0l));
			}
		}
		
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		
		put2CartReturnProductReason(session, product.getString("p_name"),pc_id, 1, "", 0);
	}

	/**
	 * 凑购物车内移除
	 */
	public void removeReturnProductReasonCart(HttpServletRequest request)
		throws Exception 
	{
		long pc_id = StringUtil.getLong(request,"pc_id");
		
		removeReturnProductReason(StringUtil.getSession(request), pc_id);
	}
	
	/**
	 * 从购物车去除商品质保原因
	 * @param session
	 * @param pc_id
	 * @throws Exception
	 */
	private void removeReturnProductReason(HttpSession session,long pc_id)
		throws Exception
	{
		if ( session.getAttribute(Config.cartReturnProductReason) != null )
		{
			DBRow rowOld;
			ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
			
			for ( int i=0;i<al.size(); i++ )
			{
				rowOld = (DBRow)al.get(i);
				if ( StringUtil.getLong(rowOld.getString("cart_pid")) == pc_id)
				{
					al.remove(i);
				}
			}
			
			session.setAttribute(Config.cartReturnProductReason,al);
		}
	}
	
	public void modReturnQuantityReason(HttpServletRequest request)
		throws Exception
	{
		String[] pcids = request.getParameterValues("pc_id");
		
		HttpSession session = StringUtil.getSession(request);
		
		if ( session.getAttribute(Config.cartReturnProductReason) != null )
		{
			if(pcids!=null&&pcids.length>0)
			{
				ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
				
				for (int i = 0; i < pcids.length; i++) 
				{
					for (int j = 0; j < al.size(); j++)
					{
						DBRow productReason = (DBRow)al.get(j);
						if(productReason.getString("cart_pid").equals(pcids[i]))
						{
							float quantity = StringUtil.getFloat(request,pcids[i]+"_quantity");
							String return_reason = StringUtil.getString(request,"return_reason_"+pcids[i]);
							int warranty_type = StringUtil.getInt(request,pcids[i]+"_waranty_type");
							
							productReason.add("cart_quantity",quantity);
							productReason.add("return_reason",return_reason);
							productReason.add("warranty_type",warranty_type);
							
							al.set(j,productReason);
							
							break;
						}
					}
				}
				
				session.setAttribute(Config.cartReturnProductReason,al);
			}
		}
	}
	
	private void put2CartReturnProductReason(HttpSession session,String p_name,long pc_id,float quantity,String reason,int warranty_type)
		throws Exception
	{
		ArrayList<DBRow> al = null;
		
		if ( pc_id != 0 &&quantity>0)
		{
			
			//购物车每条记录属性
			DBRow row = new DBRow();
			
			row.add("p_name",p_name);
			row.add("cart_pid",pc_id);//商品ID
			row.add("cart_quantity",quantity);
			row.add("return_reason",reason);
			row.add("warranty_type",warranty_type);
			
			if (session.getAttribute(Config.cartReturnProductReason) == null )
			{
				
				
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				boolean nOrR = true;
				int listIndex = 0;
				al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
				for (int i = 0; i < al.size(); i++) 
				{
					if(al.get(i).get("cart_pid",0l)==pc_id)
					{
						listIndex = i;
						nOrR = false;
						break;
					}
				}
				if(nOrR)
				{
					al.add(row);
				}
				else
				{
					al.set(listIndex,row);
				}
				
			}
			session.setAttribute(Config.cartReturnProductReason,al);			
		}
	}


	@Override
	public void flush(HttpSession session) 
		throws Exception 
	{
		DBRow[] productReasons = this.getSimpleProducts(session);
		
		//必须深度克隆，否则使用的只是引用，会把大量数据加到session中
		//数组不能直接克隆，需要一个个对象克隆，否则也是克隆地址引用
		DBRow newProductReasons[] = null;
		ArrayList newProductsAL = new ArrayList();
		
		float sum_quantity = 0;
		for (int i=0; i<productReasons.length; i++)
		{
			
			sum_quantity += productReasons[i].get("cart_quantity",0f);
			
			DBRow newProductReason = new DBRow();
			newProductReason.append(productReasons[i]);
			newProductsAL.add(newProductReason);
		}
		newProductReasons = (DBRow[])newProductsAL.toArray(new DBRow[0]);
		
		this.productReason = newProductReasons;
		this.sumQuantity = sum_quantity;
	}
	
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartReturnProductReason) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow = (DBRow)al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public DBRow[] getProductReason() 
	throws Exception
	{
		return productReason;
	}

	public float getSumQuantity() {
		return sumQuantity;
	}
}
