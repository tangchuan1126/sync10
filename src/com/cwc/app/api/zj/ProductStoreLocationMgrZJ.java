package com.cwc.app.api.zj;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.beans.log.ProductStorePhysicalLogBean;
import com.cwc.app.exception.bcs.CanNotPutProductException;
import com.cwc.app.exception.bcs.PickUpCountMoreException;
import com.cwc.app.exception.bcs.PutLocationSerialNumberRepeatException;
import com.cwc.app.exception.location.LocationCatalogHasExitsException;
import com.cwc.app.exception.location.LocationCatalogNotExitsException;
import com.cwc.app.exception.productStorage.CanNotSerialNumberReserveException;
import com.cwc.app.exception.serialnumber.SerialNumberAlreadyInStoreException;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutListDetailMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutboundOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStoreLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStoreMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductStorePhysicalLogMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.floor.api.zyj.FloorContainerMgrZyj;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductStorePhysicalOperationKey;
import com.cwc.app.key.TransportPutKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ProductStoreLocationMgrZJ implements ProductStoreLocationMgrIFaceZJ 
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductStoreLocationMgrZJ floorProductStoreLocationMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private FloorOutListDetailMgrZJ floorOutListDetailMgrZJ;
	private FloorProductStorePhysicalLogMgrZJ floorProductStorePhysicalLogMgrZJ;
	private FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ;
	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	private FloorProductStoreMgrZJ floorProductStoreMgrZJ;
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	private FloorLogMgrIFace logMgr;
	
	/**
	 * 记录物理操作日志
	 * @throws Exception
	 */
	public void addProductStorePhysicalLog(long system_bill_id,int system_bill_type,long operation_slc_id,float quantity,long ps_id,long pc_id,long operation_adid,int operation_type,String serial_number,long lp_id,String machine)
		throws Exception
	{
		try 
		{
			DBRow productStorePhysicalLog = new DBRow();
			productStorePhysicalLog.add("system_bill_id",system_bill_id);
			productStorePhysicalLog.add("system_bill_type",system_bill_type);
			productStorePhysicalLog.add("operation_slc_id",operation_slc_id);
			productStorePhysicalLog.add("quantity",quantity);
			productStorePhysicalLog.add("ps_id",ps_id);
			productStorePhysicalLog.add("pc_id",pc_id);
			productStorePhysicalLog.add("operation_adid",operation_adid);
			productStorePhysicalLog.add("operation_type",operation_type);
			productStorePhysicalLog.add("operation_time",DateUtil.NowStr());
			productStorePhysicalLog.add("operation_machine_id",machine);
			
			if (serial_number!=null&&!serial_number.equals("")) 
			{
				productStorePhysicalLog.add("serial_number",serial_number);
			}
			if(lp_id!= 0)
			{
				productStorePhysicalLog.add("lp_id",lp_id);
			}
			
			floorProductStorePhysicalLogMgrZJ.addProductStorePhysicalLog(productStorePhysicalLog);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductStorePhysicalLog",log);
		}
	}
	/**
	 * 放货
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @param system_bill_id
	 * @param system_bill_type
	 * @param lay_up_adid
	 * @param location
	 * @param psl_id
	 * @param lr_id
	 * @throws Exception
	 */
	public void putProductToLocation(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long lay_up_adid,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean) 
		throws Exception 
	{
		try 
		{
			DBRow putLog = floorProductStorePhysicalLogMgrZJ.getProductStorePhysicalLogLayUpBySN(system_bill_id,system_bill_type,serial_number);
			
			if(putLog != null)
			{
				throw new PutLocationSerialNumberRepeatException();
			}
			
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			DBRow storeLocationCatalog = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(ps_id,location);
			if(storeLocationCatalog ==null)
			{
				////system.out.println(ps_id+"------"+location);
				storeLocationCatalog = new DBRow();
//				throw new LocationCatalogNotExitsException();
			}
			
			long slc_id = storeLocationCatalog.get("slc_id",0l);
			long time_number = System.currentTimeMillis();
			
			DBRow container = floorContainerMgrZYZ.getDetailContainer(lp_id);
			
			//容器库存
			DBRow firstProductStorageContainer = floorProductStoreMgrZJ.getDetailProductStoreContainer(pc_id, slc_id, title_id,lp_id, lot_number);
			if (firstProductStorageContainer!=null)
			{
				floorProductStoreMgrZJ.addSubProductStorageContainerAvailableCount(firstProductStorageContainer.get("psc_id",0l),quantity);
				floorProductStoreMgrZJ.addSubProductStorageContainerPhysicalQuantity(firstProductStorageContainer.get("psc_id",0l),quantity);
			}
			else
			{
				DBRow productStorageContainer = new DBRow();
				
				productStorageContainer.add("pc_id",pc_id);
				productStorageContainer.add("slc_id",slc_id);
				productStorageContainer.add("con_id",lp_id);
				productStorageContainer.add("title_id",title_id);
				productStorageContainer.add("available_count",quantity);
				productStorageContainer.add("physical_count",quantity);
				productStorageContainer.add("lot_number",lot_number);
				productStorageContainer.add("container_type",container.get("container_type",0));
				productStorageContainer.add("container_type_id",container.get("type_id",0l));
				productStorageContainer.add("time_number",time_number);
				
				
				floorProductStoreMgrZJ.addProductStorageContainer(productStorageContainer);
			}
			
			//位置库存
			DBRow firstProductStorageLocation = floorProductStoreMgrZJ.getDetailProductStoreLocation(pc_id, slc_id, title_id, lot_number);
			if (firstProductStorageLocation==null)
			{
				DBRow productStorageLocation = new DBRow();
				
				productStorageLocation.add("ps_id",ps_id);
				productStorageLocation.add("pc_id",pc_id);
				productStorageLocation.add("p_code",product.getString("p_code"));
				productStorageLocation.add("physical_quantity",quantity);
				productStorageLocation.add("slc_id",slc_id);
				productStorageLocation.add("position",location);
				productStorageLocation.add("theoretical_quantity",quantity);
				productStorageLocation.add("time_number",time_number);
				productStorageLocation.add("title_id",title_id);
				productStorageLocation.add("lot_number",lot_number);
				productStorageLocation.add("system_bill_id",system_bill_id);
				productStorageLocation.add("system_bill_type",system_bill_type);
				
				floorProductStoreMgrZJ.addProductStoreLocation(productStorageLocation);
			}
			else
			{
				floorProductStoreMgrZJ.addSubProductStoreLocationPhysicalQuantity(firstProductStorageLocation.get("ps_location_id",0l), quantity);
				floorProductStoreMgrZJ.addSubProductStoreLocationTheoreticalQuantity(firstProductStorageLocation.get("ps_location_id",0l),quantity);
			}
			//title库存
			DBRow firstProductStorageTitle = floorProductStoreMgrZJ.getDetailProductStorageTitle(pc_id, ps_id, title_id, lot_number);
			if (firstProductStorageTitle == null)
			{
				DBRow productStorageTitle = new DBRow();
				productStorageTitle.add("pc_id",pc_id);
				productStorageTitle.add("ps_id",ps_id);
				productStorageTitle.add("available_count",quantity);
				productStorageTitle.add("physical_count",quantity);
				productStorageTitle.add("title_id",title_id);
				productStorageTitle.add("lot_number",lot_number);

				floorProductStoreMgrZJ.addProductStorageTitle(productStorageTitle);
			}
			else
			{
				floorProductStoreMgrZJ.addSubProductStorageTitleAvailableCount(firstProductStorageTitle.get("pst_id",0l),quantity);
				floorProductStoreMgrZJ.addSubProductStorageTitlePhysicalCount(firstProductStorageTitle.get("pst_id",0l),quantity);
			}
			
			//总库存
			DBRow firstProductStorage = floorProductStoreMgrZJ.getDetailProductStorage(pc_id, ps_id);
			if (firstProductStorage == null)
			{
				DBRow productStorage = new DBRow();
				productStorage.add("pc_id",pc_id);
				productStorage.add("store_count",quantity);
				productStorage.add("physical_count",quantity);
				productStorage.add("cid",ps_id);
				
				floorProductStoreMgrZJ.addProductStorage(productStorage);
			}
			else
			{
				floorProductStoreMgrZJ.addSubProductStorageStoreCount(firstProductStorage.get("pid",0l),quantity);
				floorProductStoreMgrZJ.addSubProductStoragePhysicalCount(firstProductStorage.get("pid",0l), quantity);
			}
			
			/**
			 * 记录物理操作日志
			 */
			this.addProductStorePhysicalLog(system_bill_id, system_bill_type, slc_id, quantity, ps_id, pc_id, lay_up_adid,ProductStorePhysicalOperationKey.LayUp,serial_number,lp_id,machine);
			
			if(!serial_number.equals(""))
			{
				//序列号入库
				serialNumberMgrZJ.putSerialProduct(serial_number,pc_id,slc_id);
			}	
		} 
		catch(SerialNumberAlreadyInStoreException e)
		{
			throw e;
		}
		catch(PutLocationSerialNumberRepeatException e)
		{
			throw e;
		}
		catch(Exception e) 
		{
			throw new SystemException(e,"putProductToLocation",log);
		}
	}
	
	public void initProductStoreLocation(long lay_up_adid)
		throws Exception
	{
		try 
		{
			DBRow[] storeLocation = floorProductStoreLocationMgrZJ.getAllStoreLocation();
			
			for (int i = 0; i < storeLocation.length; i++) 
			{
//				this.putProductToLocation(storeLocation[i].get("ps_id",0l),storeLocation[i].get("pc_id",0l),storeLocation[i].get("quantity",0f),0,0,lay_up_adid,storeLocation[i].getString("position"),"",0,"",null);
				////system.out.println(storeLocation[i].get("ps_id",0l)+"---"+storeLocation[i].get("pc_id",0l)+"---"+storeLocation[i].get("quantity",0f)+"---"+storeLocation[i].getString("position"));
			}
			
			DBRow[] sumCount = floorProductStoreLocationMgrZJ.productStoreLocationSum();
			for (int i = 0; i < sumCount.length; i++) 
			{
				DBRow productStorage = floorProductMgr.getDetailProductProductStorageByPcid(sumCount[i].get("ps_id",0l),sumCount[i].get("pc_id",0l));
				
				DBRow para = new DBRow();
				para.add("store_count",sumCount[i].get("quantity",0f));
				floorProductMgr.modProductStorage(productStorage.get("pid",0l),para);
				
				
			}
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 先进先出预保留理论库存
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @param system_bill_id
	 * @param system_bill_type
	 * @param adid
	 * @return slc_area(单据所属区域)
	 * @throws Exception
	 */
	public void reserveTheoreticalFIFO(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long adid)
		throws Exception
	{
		try 
		{
			DBRow[] productStoreLocations = floorProductStoreLocationMgrZJ.getOldestLocationTheoretical(ps_id, pc_id);
			int i = 0;
			do 
			{
				DBRow storeLocation = productStoreLocations[i];
				float theoretical_quantity = storeLocation.get("theoretical_quantity",0f);
				float use_quantity;
				/**
				 * 修改用于出库的批次开始
				 **/
				//需求量大于可使用量,直接减去可使用量
				if(quantity>theoretical_quantity)
				{
					use_quantity = theoretical_quantity;
					quantity -= theoretical_quantity;
					floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocations[i].get("ps_location_id",0l),use_quantity*-1);
				}
				else//需求数量不大于可用数量,直接减去需求量
				{
					use_quantity = quantity;
					quantity -= quantity;
					floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocations[i].get("ps_location_id",0l),use_quantity*-1);
				}
				
				DBRow outListDetail = new DBRow();
				outListDetail.add("out_list_pc_id",pc_id);
				outListDetail.add("out_list_slc_id",productStoreLocations[i].get("slc_id",0l));
				outListDetail.add("out_list_area_id",productStoreLocations[i].get("slc_area",0l));
				outListDetail.add("pick_up_quantity",use_quantity);
				outListDetail.add("system_bill_id",system_bill_id);
				outListDetail.add("system_bill_type",system_bill_type);
				outListDetail.add("create_adid",adid);
				outListDetail.add("create_time",DateUtil.NowStr());
				outListDetail.add("ps_location_id",productStoreLocations[i].get("ps_location_id",0l));
				floorOutListDetailMgrZJ.addOutListDetail(outListDetail);
				i++;
			} 
			while (quantity!=0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveTheoreticalFIFO",log);
		}
	}
	
	/**
	 * 序列号预保留库存
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @param system_bill_id
	 * @param system_bill_type
	 * @param adid
	 * @param serial_number
	 * @throws Exception
	 */
	public void reserveTheoreticalSNFO(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long adid,String serial_number)
		throws Exception
	{
		try 
		{
			if (quantity !=1) 
			{
				throw new CanNotSerialNumberReserveException();
			}
			
			DBRow serialProduct = serialNumberMgrZJ.getDetailSerialProduct(serial_number);
			
			if(serialProduct==null||serialProduct.get("at_slc_id",0l)==0)
			{
				throw new CanNotSerialNumberReserveException();
			}
			
			DBRow productStoreLocation = floorProductStoreLocationMgrZJ.getDetailProductStoreLocation(ps_id,serialProduct.get("at_slc_id",0l), pc_id);
			
			if(productStoreLocation.get("theoretical_quantity",0f)==0)
			{
				throw new CanNotSerialNumberReserveException();
			}
			
			floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocation.get("ps_location_id",0l),quantity*-1);
			
			DBRow outListDetail = new DBRow();
			outListDetail.add("out_list_pc_id",pc_id);
			outListDetail.add("out_list_slc_id",productStoreLocation.get("slc_id",0l));
			outListDetail.add("out_list_area_id",productStoreLocation.get("slc_area",0l));
			outListDetail.add("pick_up_quantity",quantity);
			outListDetail.add("system_bill_id",system_bill_id);
			outListDetail.add("system_bill_type",system_bill_type);
			outListDetail.add("create_adid",adid);
			outListDetail.add("create_time",DateUtil.NowStr());
			outListDetail.add("ps_location_id",productStoreLocation.get("ps_location_id",0l));
			outListDetail.add("out_list_serial_number",serial_number);
			floorOutListDetailMgrZJ.addOutListDetail(outListDetail);
		} 
		catch (CanNotSerialNumberReserveException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveTheoreticalSN",log);
		}
	}
	
	public void reserveTheoretical(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long adid,String serial_number,String reserve_type)
		throws Exception
	{
		try 
		{
			if (reserve_type.equals("FIFO"))
			{
				this.reserveTheoreticalFIFO(ps_id, pc_id, quantity, system_bill_id, system_bill_type, adid);
			}
			else
			{
				this.reserveTheoreticalSNFO(ps_id, pc_id, quantity, system_bill_id, system_bill_type, adid, serial_number);
			}
		}
		catch (CanNotSerialNumberReserveException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveTheoretical",log);
		}
	}
	
	/**
	 * 根据系统单据获得单据归属区域
	 * @param system_bill_id
	 * @param system_bill_type
	 * @return
	 * @throws Exception
	 */
	public long getAreaForBill(long system_bill_id,int system_bill_type)
		throws Exception
	{
		try
		{
			return floorOutListDetailMgrZJ.getBillArea(system_bill_id, system_bill_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAreaForBill",log);
		}
	}
	
	/**
	 * 业务单据取消，取消拣货明细
	 * @param system_bill_id
	 * @param system_bill_type
	 * @throws Exception
	 */
	public void cancelOutListDetail(long system_bill_id,int system_bill_type)
		throws Exception
	{
		try 
		{
			/**
			 * 业务单据取消，将扣掉的理论库存退回去
			 */
			DBRow[] outListDetais = floorOutListDetailMgrZJ.getOutListDetailByBill(system_bill_id, system_bill_type);
			for (int i = 0; i < outListDetais.length; i++) 
			{
				floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(outListDetais[i].get("ps_location_id",0l), outListDetais[i].get("pick_up_quantity",0f));
			}
			/**
			 * 删除拣货明细
			 */
			floorOutListDetailMgrZJ.delOutListDetail(system_bill_id, system_bill_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelOutListDetail",log);
		}
	}
	
	/**
	 * 获得系统单据的保留明细
	 * @param system_bill_id
	 * @param system_bill_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetails(long system_bill_id,int system_bill_type)
		throws Exception
	{
		try 
		{
			return floorOutListDetailMgrZJ.getOutListDetailByBill(system_bill_id, system_bill_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOutListDetails",log);
		}
	}
	
	/**
	 * 拣货
	 * @param ps_location_id
	 * @param quantity
	 * @throws Exception
	 */
	public void pickUpPhysical(long ps_id,long slc_id,long pc_id,float quantity,long pick_up_adid,long out_id,String serial_number,long lp_id,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long opation_con_id)
		throws Exception
	{
		try 
		{
			DBRow outListDetail = null;
				
			if(foType.toUpperCase().equals("FIFO"))
			{
				outListDetail = floorOutboundOrderMgrZJ.getOutStorebillOrderDetailByPcOutSlc(pc_id,out_id,slc_id,from_type,from_type_id,from_con_id,pick_type,pick_type_id,pick_con_id);
			}
			else if (foType.toUpperCase().equals("SNFO"))
			{
				outListDetail = floorOutboundOrderMgrZJ.getOutStorebillOrderDetailByPcOutSlcSN(pc_id,out_id,slc_id,serial_number);
			}

			long osod_id = outListDetail.get("osod_id",0l);
			
			float need_execut_quantity = outListDetail.get("need_execut_quantity",0f);
			float pickquantiy = quantity;
			if(quantity>need_execut_quantity)
			{
				throw new PickUpCountMoreException();
			}
			//减少容器库存
			DBRow productStoreContainer = floorProductStoreMgrZJ.getDetailProductStoreContainer(pc_id,slc_id,0,opation_con_id,"");
			floorProductStoreMgrZJ.addSubProductStorageContainerPhysicalQuantity(productStoreContainer.get("psc_id",0l),pickquantiy*-1);
			
			//减少位置库存
			DBRow[] productStoreLocations = floorProductStoreLocationMgrZJ.getOldestLocationPhysical(ps_id, slc_id, pc_id);
			floorProductStoreMgrZJ.addSubProductStoreLocationPhysicalQuantity(productStoreLocations[0].get("ps_location_id",0l),pickquantiy*-1);
			
			long title_id = productStoreLocations[0].get("title_id",0l);
			
			
			floorOutboundOrderMgrZJ.decOutboundOrderDetailNeedExecutQuantity(osod_id,pickquantiy);//拣货单待执行数量减少
			//记录物理操作日志
			ProductStorePhysicalLogBean productPhysicalLogBean = new ProductStorePhysicalLogBean();
			productPhysicalLogBean.setCatalogs(new long[0]);
			productPhysicalLogBean.setLot_number("");
			productPhysicalLogBean.setLp_id(opation_con_id);
			productPhysicalLogBean.setMachine(machine);
			productPhysicalLogBean.setOperation_adid(pick_up_adid);
			productPhysicalLogBean.setOperation_type(ProductStorePhysicalOperationKey.PickUp);
			productPhysicalLogBean.setPc_id(pc_id);
			productPhysicalLogBean.setProduct_line_id(0);
			productPhysicalLogBean.setPs_id(ps_id);
			productPhysicalLogBean.setQuantity((int)pickquantiy*-1);
			productPhysicalLogBean.setSerial_number(serial_number);
			productPhysicalLogBean.setSlc_id(slc_id);
			productPhysicalLogBean.setSystem_bill_id(out_id);
			productPhysicalLogBean.setSystem_bill_type(ProductStoreBillKey.PICK_UP_ORDER);
			productPhysicalLogBean.setTitle_id(title_id);
			
			
			
		//	logMgr.physicalProductStoreLog(productPhysicalLogBean,false);
			
			this.addProductStorePhysicalLog(out_id,ProductStoreBillKey.PICK_UP_ORDER,slc_id, quantity, ps_id, pc_id,pick_up_adid,ProductStorePhysicalOperationKey.PickUp,serial_number,lp_id,machine);
			
			if (!serial_number.equals(""))
			{
				//记录序列号出库
				serialNumberMgrZJ.outSerialProduct(serial_number, pc_id);
			}
		} 
		catch (PickUpCountMoreException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"pickUpPhysical",log);
		}
	}
	
	public ArrayList<DBRow> pickUpException(long out_id,long slc_id,long pc_id,long ps_id,float quantity,ProductStoreLogBean deInProductStoreLogBean)
		throws Exception
	{
		
		try 
		{
			//将位置上数据标0
			DBRow storeLocationError = floorProductStoreLocationMgrZJ.getDetailProductStoreLocation(ps_id, slc_id, pc_id);
			DBRow storeLocationPara = new DBRow();
			storeLocationPara.add("theoretical_quantity",0f);
			storeLocationPara.add("physical_quantity",0f);
			floorProductStoreLocationMgrZJ.modProductStoreLocation(storeLocationError.get("ps_location_id",0l),storeLocationPara);
			//拣货单明细待拣数为0，标记差异数
			DBRow outStoreBillOrderDetail = floorOutboundOrderMgrZJ.getDetailOutStoreBillOrderDetail(out_id, slc_id, pc_id);
			long osod_id = outStoreBillOrderDetail.get("osod_id",0l);
			DBRow paraOutStoreBillOrderDetail = new DBRow();
			paraOutStoreBillOrderDetail.add("need_execut_quantity",0);
			paraOutStoreBillOrderDetail.add("exception_quantity",quantity);
			floorOutboundOrderMgrZJ.modOutStoreBillOrderDetail(osod_id, paraOutStoreBillOrderDetail);
			
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			DBRow[] productStoreLocations = floorProductStoreLocationMgrZJ.getOldestLocationTheoretical(ps_id,pc_id);
			int i = 0;
			float pickquantity = quantity;
			do 
			{
				try 
				{
					DBRow storeLocation = productStoreLocations[i];
					float theoretical_quantity = storeLocation.get("theoretical_quantity",0f);
					float use_quantity;
					/**
					 * 修改用于出库的批次开始
					 **/
					//需求量大于可使用量,直接减去可使用量
					if(quantity>theoretical_quantity)
					{
						use_quantity = theoretical_quantity;
						quantity -= theoretical_quantity;
						floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocations[i].get("ps_location_id",0l),theoretical_quantity*-1);
					}
					else//需求数量不大于可用数量,直接减去需求量
					{
						use_quantity = quantity;
						quantity -= quantity;
						floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocations[i].get("ps_location_id",0l),quantity*-1);
					}
					
					long new_slc_id = productStoreLocations[i].get("slc_id",0l);
					DBRow locationCatalog = floorLocationMgrZJ.getDetailLocationCatalogById(new_slc_id);
					
					DBRow outStoreBillDetail = new DBRow();
					outStoreBillDetail.add("slc_id",new_slc_id);
					outStoreBillDetail.add("pc_id",pc_id);
					outStoreBillDetail.add("quantity",use_quantity);
					outStoreBillDetail.add("need_execut_quantity",use_quantity);
					outStoreBillDetail.add("out_id",out_id);
					outStoreBillDetail.add("area_id",locationCatalog.get("slc_area",0l));
					String serialNumber = productStoreLocations[i].getString("out_list_serial_number");
					if (serialNumber.length()>0)
					{
						outStoreBillDetail.add("serial_number",serialNumber);
					}
					
					
					floorOutboundOrderMgrZJ.addOutStoreBillOrderDetail(outStoreBillDetail);
					
					outStoreBillDetail.add("slc_position_all",locationCatalog.getString("slc_position_all"));
					list.add(outStoreBillDetail);
					i++;
				} 
				catch (ArrayIndexOutOfBoundsException e) //数组越界，库存商品无法满足需求，拣货数应等于拣货数-还未减数量
				{
					pickquantity = pickquantity-quantity;
					break;
				}
			} 
			while (quantity!=0);
			
			floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean, ps_id, pc_id,pickquantity);
			
			return list;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"pickUpException",log);
		}
	}
	
	/**
	 * 获得转运单的放货日志
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorePhysicaLogLayUpForTransport(long transport_id)
		throws Exception
	{
		try 
		{
			return floorProductStorePhysicalLogMgrZJ.getProductStorePhysicaLogLayUpForTransport(transport_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStorePhysicaLogLayUpForTransport",log);
		}
	}
	
	/**
	 * 审核操作库存
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @param quantity
	 * @param sld_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void takeStockApprove(long ps_id,long slc_id,long pc_id,float quantity,long sld_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		DBRow storageLocationCatalog = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
		int system_bill_type = ProductStoreBillKey.STORAGE_APPROVE;
		int operation_type = 0;
		if (quantity>0)
		{
			operation_type = ProductStorePhysicalOperationKey.LayUp;
			DBRow firstProductStoreLocation = floorProductStoreLocationMgrZJ.getFirstProductStoreLocation(pc_id, ps_id, slc_id);
			
			long time_number = System.currentTimeMillis();
			if (firstProductStoreLocation!=null)
			{
//				time_number = firstProductStoreLocation.get("time_number",0l);
				
				floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationPhysical(firstProductStoreLocation.get("ps_location_id",0l),quantity);
				floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(firstProductStoreLocation.get("ps_location_id",0l),quantity);
			}
			else
			{
				/**
				 * 位置放货
				 */
				DBRow productStoreLocation = new DBRow();
				productStoreLocation.add("ps_id",ps_id);
				productStoreLocation.add("pc_id",pc_id);
				productStoreLocation.add("p_code",product.getString("p_code"));
				productStoreLocation.add("physical_quantity",quantity);
				productStoreLocation.add("theoretical_quantity",quantity);
				productStoreLocation.add("position",storageLocationCatalog.getString("slc_position_all"));
				productStoreLocation.add("slc_id",slc_id);
				productStoreLocation.add("time_number",time_number);
				productStoreLocation.add("system_bill_id",sld_id);
				productStoreLocation.add("system_bill_type",system_bill_type);
				
				floorProductStoreLocationMgrZJ.addProductStoreLocation(productStoreLocation);
			}
		}
		else if(quantity<0)
		{
			operation_type = ProductStorePhysicalOperationKey.PickUp;
			DBRow[] productStoreLocations = floorProductStoreLocationMgrZJ.getOldestLocationPhysical(ps_id, slc_id, pc_id);
			floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationPhysical(productStoreLocations[0].get("ps_location_id",0l),quantity);
			floorProductStoreLocationMgrZJ.inOrOutProductStoreLocationTheoretical(productStoreLocations[0].get("ps_location_id",0l),quantity);
		}
		else
		{
			throw new Exception("quantity == 0");
		}
		/**
		 * 记录物理操作日志
		 */
		this.addProductStorePhysicalLog(sld_id,system_bill_type, slc_id, quantity, ps_id, pc_id,adminLoggerBean.getAdid(),operation_type,"",0,"");
		
		//修改理论库存
		ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
		inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
		inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
		inProductStoreLogBean.setOid(sld_id);
		inProductStoreLogBean.setBill_type(system_bill_type);
		inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_OUT_STORE_REVISE);
		
		floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean, ps_id, pc_id, quantity, 0);
	}
	
	

	public void setFloorProductStoreLocationMgrZJ(
			FloorProductStoreLocationMgrZJ floorProductStoreLocationMgrZJ) {
		this.floorProductStoreLocationMgrZJ = floorProductStoreLocationMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setFloorOutListDetailMgrZJ(
			FloorOutListDetailMgrZJ floorOutListDetailMgrZJ) {
		this.floorOutListDetailMgrZJ = floorOutListDetailMgrZJ;
	}

	public void setFloorProductStorePhysicalLogMgrZJ(
			FloorProductStorePhysicalLogMgrZJ floorProductStorePhysicalLogMgrZJ) {
		this.floorProductStorePhysicalLogMgrZJ = floorProductStorePhysicalLogMgrZJ;
	}
	public void setFloorOutboundOrderMgrZJ(
			FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ) {
		this.floorOutboundOrderMgrZJ = floorOutboundOrderMgrZJ;
	}
	
	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ) {
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}
	public void setFloorProductStoreMgrZJ(
			FloorProductStoreMgrZJ floorProductStoreMgrZJ) {
		this.floorProductStoreMgrZJ = floorProductStoreMgrZJ;
	}
	public void setFloorContainerMgrZYZ(FloorContainerMgrZYZ floorContainerMgrZYZ) {
		this.floorContainerMgrZYZ = floorContainerMgrZYZ;
	}
	public void setLogMgr(FloorLogMgrIFace logMgr) {
		this.logMgr = logMgr;
	}

}
