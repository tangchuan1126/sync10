package com.cwc.app.api;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class ExpressCheckoutOrder 
{
	public static DBRow getOrder(HttpServletRequest request,DBRow porder)
	{
		int num_cart_items = StringUtil.getInt(request,"num_cart_items");
		String item_name = "";
		String item_number = "";
		String quantity = "";
		
		for (int i=1; i<=num_cart_items; i++)
		{
			item_name += StringUtil.getString(request,"item_name"+String.valueOf(i))+"|";
			item_number += StringUtil.getString(request,"item_number"+String.valueOf(i))+"|";
			quantity += StringUtil.getString(request,"quantity"+String.valueOf(i))+"|";
		}
		
		porder.add("business",StringUtil.getString(request,"receiver_email"));
		porder.add("item_name",item_name);
		porder.add("item_number",item_number);
		porder.add("quantity",quantity);
		
		
		return(porder);
	}
}
