package com.cwc.app.api.zr;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.api.zzq.OpenFireSetBean;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.EntryTaskHasFinishException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.SmallParcelNotCloseOrderException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zj.FloorCheckInMgrZJ;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zr.FloorCheckInMgrZr;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.wfh.CheckInMgrIfaceWfh;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.LoadBarUseMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zzq.PushMessageByOpenfireMgrIfaceZzq;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInEntryPriorityKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.OrderSystemTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.SetReportScreenParamsTypeKey;
import com.cwc.app.key.SetReportScreenTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.key.WaitingTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.CheckInEntryPermissionUtil;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.service.android.action.mode.HoldThirdValue;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class CheckInMgrZr implements CheckInMgrIfaceZr {

	
	
 	
	public static int TimeSetOut = -1; // 大屏幕的时候-1 表示永远不过期

	private static Logger log = Logger.getLogger("ACTION");

	private FloorCheckInMgrZwb floorCheckInMgrZwb;

	private CheckInMgrIfaceZwb checkInMgrZwb;

	private FloorCheckInMgrZr floorCheckInMgrZr;

	private FloorCheckInMgrZJ floorCheckInMgrZJ;
	
	private FloorCheckInMgrWfh floorCheckInMgrWfh;

	private StorageDoorMgrIfaceZr storageDoorMgrZr;

	private ScheduleMgrIfaceZR scheduleMgrZr;

	private AdminMgrIFace adminMgr;

	private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr;

	private YMSMgrAPI ymsMgrAPI;

	private FloorFileMgrZr floorFileMgrZr;

	private FileMgrIfaceZr fileMgrZr;

	private LoadBarUseMgrIfaceZr loadBarUseMgrZr;

	private FloorAdminMgr fam ;
	
	private AndroidPrintMgrIfaceZr androidPrintMgrZr ;
	
	private CheckInMgrIfaceWfh checkInMgrWfh;
	
  	
	ModuleKey moduleKey = new ModuleKey();
	CheckInChildDocumentsStatusTypeKey checkInChildDocumentsStatusTypeKey = new CheckInChildDocumentsStatusTypeKey();
	CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
	OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
	FileWithCheckInClassKey checkInFileClassKey = new FileWithCheckInClassKey();
	WaitingTypeKey waitingTypeKey = new WaitingTypeKey();
	
	
	public static final Integer ValidateResultTypeOfDataValidate = 1; // 验证导入数据，输入不合法
	public static final Integer ValidateResultTypeOfDataExsits = 2; // 数据在数据库已经存在了
	public static final Integer ValidateResultTypeOfNoError = 3; // 没有错误的情况

	public static final boolean isCanSkipSelectEquipment = true;// 这个查询用于设置当前EntryTask
																// 如果只有一个设备是否在Android界面弹出选择设备的设置
																// cancel

	public static final int RequestAll = 1; // android 请求Spot/Door 请求系统所有的
	public static final int RequestFreeAndSelf = 2; // android 请求Spot/Door
													// 请求当前可用的(free+自己可用用的)

	public static final String LogTaskOccuipedDoor = "Task Occupied Door"; // task
	public static final String LogTaskReleasedDoor = "Task Released Door"; // task
	public static final String LogTaskReleasedSpot = "Task Released Spot"; // task

	public static final String LogTaskOccuipedSpot = "Task Occupied Spot"; // task
	public static final String LogEquipmentOccuipedDoor = "Equipment Occupied Door";
	public static final String LogTaskReservedDoor = "Task Reserved Door ";
	public static final String LogEquipmentReleasedDoor = "Equipment Released Door ";
	public static final String LogEquipmentReleasedSpot = "Equipment Released Spot ";
	
	public static final String LogEquipmentOccuipedSpot = "Equipment Occupied Spot ";
	
	public static final int ForgetTask = 1 ;
	 
	public static final int UserOnlineTimeOut = 2 * 60 * 1000 ; 	//设置人员的不在线的时间
	
	public OpenFireSetBean openFireSetBean ;
	
	/**
	 * 2014-11-24 getEntryDetailHasDoor
	 */
	@Override
	public DBRow getDockCloseDetailByEntryId(DBRow data,
			AdminLoginBean adminLoginBean) throws Exception {
		try {
			long entry_id = data.get("dlo_id", 0l);
			long equipment_id = data.get("equipment_id", 0l);
			DBRow result = new DBRow();
			boolean isfresh = data.get("isfresh", 0) == 1 ? true : false;
			DBRow entryRow = queryEntryRow(entry_id, adminLoginBean, isfresh);
			DBRow[] details = checkInMgrZwb.getEntryDetailHasResources(entry_id,equipment_id);
			DBRow[] detailNumbers = fixDetailDatas(details);
			DBRow append = new DBRow();
			append.add("entry_id", entry_id);
			append.add("out_seal", entryRow.getString("out_seal"));
			append.add("details", detailNumbers);
			result.add("entry", append);
			result.add("rel_type", entryRow.get("rel_type", 0)); // Delivery ,
																	// pickup
			result.add("is_live", entryRow.get("isLive", 0));
			// 张睿添加时间的装换
			long ps_id = adminLoginBean.getPs_id();
			String appointment_time = entryRow.getString("appointment_time");
			appointment_time = DateUtil.showLocalparseDateTo24Hours(appointment_time, ps_id);
			result.add("appointment_time", appointment_time + "");
			result.add("driver_license",entryRow.getString("gate_driver_liscense"));
			result.add("license_number",entryRow.getString("gate_liscense_plate"));
			result.add("transport_company", entryRow.getString("company_name"));
			return result;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch (DockCheckInFirstException e) {
			throw e;
		} catch (CheckInEntryIsLeftException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "queryEntryRow", log);
		}
	}
 
	/**
	 * 下面一个方法重载	
	 * @param detail_id
	 * @param rl_id
	 * @param row
	 * @param adid
	 * @param entry_id
	 * @param resourcesType
	 * @param equipment_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月20日
	 */
	@Override
	public void finishDetail(DBRow detailRow, long rl_id, DBRow row, long adid, int resourcesType)
			throws Exception {
		try{
			long equipment_id = detailRow.get("equipment_id", 0l);
			DBRow equipment = getEquipment(equipment_id);
			finishDetail(detailRow, rl_id, row, adid,   resourcesType, equipment);
		}catch(Exception e){
			throw new SystemException(e, "finishDetail", log);
		}
 		
	}
	
	
 
	/**
	 * 查询一共是扫描了多少个Pallet ，如果是 Exception ， 那么删除扫描的Pallet记录
	 * @param detailRow , updateRow 包含了更新的状态
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月8日
	 */
	private static final int Load = 1 ;
	private static final int Receive = 2 ;

	private String finishDetailHandScanPallet(DBRow detailRow ,DBRow updateRow) throws Exception{
		try{
			int number_status = updateRow.get("number_status", 0);
			int number_type = detailRow.get("number_type", 0);
			int loadOrReceive = 0 ;
			long dlo_detail_id = detailRow.get("dlo_detail_id", 0l);
			if(number_type == ModuleKey.CHECK_IN_LOAD  
					 ||	number_type ==  ModuleKey.CHECK_IN_ORDER 
					 ||	number_type == ModuleKey.CHECK_IN_PONO
					 || number_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS){
						
					loadOrReceive = Load ;	
			} 
			if(number_type == ModuleKey.CHECK_IN_BOL 
					||  number_type == ModuleKey.CHECK_IN_CTN
					||  number_type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS){
					loadOrReceive = Receive ;
			}
			/*if(number_status == CheckInChildDocumentsStatusTypeKey.EXCEPTION){
				if(loadOrReceive == Receive){
					floorCheckInMgrZr.deleteReceivePalletBy(dlo_detail_id);
				}
				if(loadOrReceive == Load){
					floorCheckInMgrZr.deleteLoadPalletBy(dlo_detail_id);
				}
				return "" ;
			}*/
			if(number_status == CheckInChildDocumentsStatusTypeKey.CLOSE || number_status == CheckInChildDocumentsStatusTypeKey.PARTIALLY){
				if(loadOrReceive == Receive){
					if(number_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS){
						int count = floorCheckInMgrZr.getReceivePalletCountBy(dlo_detail_id);
						return ",Receive Scan Pallet Count:" + count ;
					}else{
						DBRow rn = floorCheckInMgrWfh.findReceiptNoIdByDetailId(dlo_detail_id);
						DBRow count_row = null;
						if(rn!=null){
							count_row = floorCheckInMgrWfh.findReceivePalletConutByRnId(rn.get("receipt_id", 0L));
						}
						count_row = count_row==null?new DBRow():count_row;
						if(count_row!=null){
							int count = count_row.get("pallet_count", 0);
							return ",Receive Scan Pallet Count:" + count ;
						}
					}
 				}
				if(loadOrReceive == Load){
					int count = floorCheckInMgrZr.getLoadPalletCountBy(dlo_detail_id);
					return ",Load Scan Pallet Count:" + count ;

				}
			}
			return "" ;
		}catch(Exception e){
			throw new SystemException(e, "finishDetailHandScanPallet", log);
		}
	} 
	
	
	/**
	 * 完成某个单据的时候吧关联关系中的数据 删除 + 同时写回(rl_id + occupancy_type) 记录日志
	 * 检查当前的单据号是对应的整个Task都关闭，关闭了 写zhanyanjie的order_system 表finish_time , 和状态
	 * @param detail_id
	 * @param rl_id
	 * @param row
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月3日
	 */
	@Override
	public void finishDetail(DBRow detailRow, long rl_id, DBRow row, long adid,  int resourcesType , DBRow equipment ) throws Exception {
		try {
			long entry_id = detailRow.get("dlo_id", 0l);
			long detail_id = detailRow.get("dlo_detail_id", 0l);
			String closeType = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(row.get("number_status", 0));
			int checkInLogType = CheckInLogTypeKey.AndroidCloseTask ;
			row.add("occupancy_type", resourcesType);
			if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION){
				checkInLogType = CheckInLogTypeKey.AndroidExceptionTask ;
			}
			if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY){
				checkInLogType = CheckInLogTypeKey.AndroidPartiallyTask ;
			}
			String nowTime = DateUtil.NowStr();
			row.add("finish_time", nowTime);
			
			 
			String appendScanPallet = finishDetailHandScanPallet(detailRow, row); //处理扫描的Pallet 同时返回Log
			
			if(isEquipmentCheckOut(equipment)){
				String note = closeType+"  Task" ;
				String data = "Forget Task "+closeType+": ," + getLogDataOfRelation(SpaceRelationTypeKey.Task, detail_id) ;
				if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION
						|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY ){
					data += ",Note: "+ row.getString("note");
				}
				data += appendScanPallet ;
				checkInMgrZwb.addCheckInLog(adid, note, entry_id, checkInLogType, nowTime, data);
			}else{
				row.add("rl_id", rl_id); // close 的时候把rl_id 写到detail表中
 				finishDetailLogs(resourcesType, rl_id,
						SpaceRelationTypeKey.Task, detail_id, adid, entry_id,
						checkInLogType,  resourcesType == OccupyTypeKey.DOOR ?  LogTaskReleasedDoor : LogTaskReleasedSpot, OccupyStatusTypeKey.RELEASED,row,appendScanPallet);
 				
				ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Task,
						detail_id, adid);
			}
			floorCheckInMgrZwb.updateDetailByIsExist(detail_id, row);
			// 同时吧任务 表中的记录也应该完成了
			finishDetailFinishSchedule(detail_id, adid,entry_id);
			
			//LR 表中的数据 & 状态的改变
			finishOrderSystem(detailRow,row.get("number_status", 0));
			//close wms_load_order & wms_load_order_masterbol    wfh 2015-4-27
			if(detailRow.get("number_type", 0)!=moduleKey.CHECK_IN_DELIVERY_ORTHERS && detailRow.get("number_type", 0)!=moduleKey.CHECK_IN_CTN && detailRow.get("number_type", 0)!=moduleKey.CHECK_IN_BOL && detailRow.get("number_type", 0)!=moduleKey.SMALL_PARCEL)
				this.finishWmsLoadOrder(detailRow);
			
			//finish small_parcel 的order
			if(detailRow.get("number_type", 0)!=ModuleKey.CHECK_IN_SAMSUNG_PLATE){
				finishSmallParcelOrder(detailRow);
			}
			 
		} catch (Exception e) {
			throw new SystemException(e, "finishDetail", log);
		}
	}
	/**
	 * 关闭smallParcel的order
	 * @param detailRow
	 * @throws Exception
	 * @author wfh
	 */
	private void finishSmallParcelOrder(DBRow detailRow) throws Exception{
		long detail_id = detailRow.get("dlo_detail_id", 0);
		DBRow[] scanOrder = floorCheckInMgrWfh.findWMSLoadOrder(detail_id+"");
		String order_ids = "";
		for (DBRow order : scanOrder) {
			order_ids+= order.get("wms_order_id", "") + ",";
		}
		if(! "".equals(order_ids)){
			order_ids = order_ids.substring(0,order_ids.length()-1);
			//关闭Order
			DBRow upRow = new DBRow();
			upRow.add("status", "Closed");
			floorCheckInMgrWfh.updateWMSLoadOrder(order_ids,upRow);
		}
	}
	/**
	 * 在web端closeload时关闭wms_load_order & wms_load_order_master_bol
	 * @param detailRow
	 * @throws Exception
	 */
	private void finishWmsLoadOrder(DBRow detailRow) throws Exception{
		DBRow[] detials = floorCheckInMgrWfh.findTaskByNumber(detailRow.get("number",""), detailRow.get("number_type",0));
		boolean closeFlag = true;
		String detailIds = "";
		for (DBRow detail : detials) {
			detailIds += detail.get("dlo_detail_id", "") + ",";
			if(detail.get("number_status", 0)!=CheckInChildDocumentsStatusTypeKey.CLOSE){
				closeFlag = false;
			}
		}
		
		//将detail对应的wms_load_order & wms_load_order_masterbol 关闭
		if(closeFlag && detailRow.get("number_status", 0)== CheckInChildDocumentsStatusTypeKey.CLOSE){
			if(!detailIds.equals("")){
				detailIds = detailIds.substring(0,detailIds.length()-1);
				//查找这个load关联的master，所有与这个load相关的detail
				DBRow[] masterBols = floorCheckInMgrWfh.findWMSLoadOrderMasterBol(detailIds);
				String masterBol_ids = "";
				for (DBRow masterBol : masterBols) {
					masterBol_ids += masterBol.get("wms_master_bol_id", "") + ",";
				}
				if(!"".equals(masterBol_ids)){
					masterBol_ids = masterBol_ids.substring(0,masterBol_ids.length()-1);
				}
				
				//获取masterBOL 下所有的 order_id  
				DBRow[] orders = floorCheckInMgrWfh.findWMSLoadOrderByMasterBOL(masterBol_ids);
				String order_ids = "";
				for (DBRow order : orders) {
					order_ids+= order.get("wms_order_id", "") + ",";
				}
				
				if(! "".equals(order_ids)){
					order_ids = order_ids.substring(0,order_ids.length()-1);
				}
				//如果有order 则关闭Order
				DBRow upRow = new DBRow();
				upRow.add("status", "Closed");
				if(!"".equals(order_ids)){
					floorCheckInMgrWfh.updateWMSLoadOrder(order_ids,upRow);
				}
				//如果有则关闭masterBOL
				if(!"".equals(masterBol_ids)){
					floorCheckInMgrWfh.updateWMSLoadOrderMasterBOL(masterBol_ids,upRow);
				}
			}
				
			
		}
	}
	/**
	 * finish Task的时候记录的日志
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	private void finishDetailLogs(int resources_type,
			long resources_id, int relation_type, long relation_id, long adid,
			long entry_id, int log_type, String note , int occupyStatusTypeKey , DBRow updateDetailRow , String appendScanPallet)  throws Exception{
		try{
			String data = getLogDataOfRelation(relation_type, relation_id);
			data += "," + getLogDataOfResouces(resources_id, resources_type);
			int number_status = updateDetailRow.get("number_status", 0) ;
			if(number_status== CheckInChildDocumentsStatusTypeKey.EXCEPTION
					|| number_status == CheckInChildDocumentsStatusTypeKey.PARTIALLY ){
				data += ", Note : "+ updateDetailRow.getString("note");
			}
			data += appendScanPallet ;
			checkInMgrZwb.addCheckInLog(adid, note, entry_id, log_type,DateUtil.NowStr(), data);
		}catch(Exception e){
			throw new SystemException(e, "finishDetailLogs", log);
		}
	}
	/**
	 * 关闭zhangyanjie的OrderSystem
	 * @param detailRow
	 * @param number_status
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月7日
	 */
	private void finishOrderSystem(DBRow detailRow , int number_status) throws Exception{
		try{
			long LR_ID = detailRow.get("LR_ID", 0l);
			int count = floorCheckInMgrZr.countRLIdNotFinishTask(LR_ID);
			if(count == 0 ){
				//关闭order_system
				DBRow updateRow = new DBRow();
				updateRow.add("number_status", number_status);
				if(number_status == CheckInChildDocumentsStatusTypeKey.CLOSE 
						|| number_status == CheckInChildDocumentsStatusTypeKey.EXCEPTION){ //只有在Close的时候才会写finish_time的数据
					updateRow.add("finish_time", DateUtil.NowStr());
				}
				floorCheckInMgrZr.updateOrderSystem(LR_ID, updateRow);
				int number_type = detailRow.get("number_type", 0);
				// zhangrui 2015-03-03 调用生成PDF的功能
				if( number_type == ModuleKey.CHECK_IN_LOAD 
						 ||	number_type ==  ModuleKey.CHECK_IN_ORDER 
						 ||	number_type == ModuleKey.CHECK_IN_PONO){
				//androidPrintMgrZr.addCreatePdfTask(detailRow.get("dlo_id", 0l), detailRow.get("dlo_detail_id", 0l));
				}
			}
		}catch(Exception e){
			throw new SystemException(e, "finishOrderSystem", log);
		}
	}
	private void finishDetailFinishSchedule(long detail_id, long adid , long entry_id)
			throws Exception {
		try {
			/**
			 * 查询当前Detail 有关的任务(没有完成的)然后全部Finish
			 */
			DBRow[] schedules =   floorCheckInMgrZr.getScheduleDetails(detail_id,entry_id);//scheduleMgrZr.getCheckInModelDetailNotFinishSchedule(detail_id, entry_id);
			if (schedules != null && schedules.length > 0) {
				for (DBRow temp : schedules) {
					if(temp.get("schedule_state", 0.0d) != ScheduleFinishKey.ScheduleFinish){
						long schedule_id = temp.get("schedule_id", 0l);
						finishSchedule(schedule_id, adid);
					}else{
						//如果是 Loader, 直接CheckOut，然后再Close ， 那么也需要更新
						if(temp.get("associate_process", 0) == ProcessKey.CHECK_IN_WAREHOUSE){
							long schedule_id = temp.get("schedule_id", 0l);
							finishSchedule(schedule_id, adid);
						}
						
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "finishDetailFinishSchedule", log);
		}
	}

	/**
	 * 2014-11-24 getEntryDetailHasDoor, close 的时候应该把门的信息写入到 detail表中
	 */
	@Override
	public DBRow DockCloseBill(DBRow data, AdminLoginBean adminLoggerBean)
			throws Exception {
		try {
			DBRow result = new DBRow();
			long entry_id = data.get("dlo_id", 0l);
			long rl_id = data.get("rl_id", 0l); // 叫android回传给我
			long detail_id = data.get("detail_id", 0l);
			long equipment_id = data.get("equipment_id", 0l);
			int resources_type = data.get("resources_type", 0);
			String note = data.get("note", "");
//			DBRow[] details = checkInMgrZwb.getEntryDetailHasResources(entry_id,equipment_id);
	 
			DBRow currentLoadRow = floorCheckInMgrZwb.selectDetailByDetailId(detail_id);
			int nofityFlag = checkInMgrZwb.closeDetailReturnFlag(equipment_id, entry_id, detail_id) ; //checkInMgrZwb.returnDockCloseNotifyFlag(currentLoadRow.get("number", ""), details, currentLoadRow);

//			if (nofityFlag == CheckInMgrZwb.LoadCloseNotifyInputSeal) {
//				result.add("loadbars", checkInMgrZwb.selectLoadBar());
//			}
//			String nowTime = DateUtil.NowStr();
			
			//task是smallParce的时候执行
			if(currentLoadRow.get("number_type", 0)==ModuleKey.SMALL_PARCEL){
				//清掉其他carrier扫描的order
				this.checkInMgrWfh.freshOtherCarrierOrder(currentLoadRow.get("small_parcel_carrier_id", 0L), detail_id);
				
				//验证smallParcel扫过的order下的pallet时候都扫描完了  wfh  2015-5-13
				DBRow[] scanOrder = floorCheckInMgrWfh.findWMSLoadOrder(detail_id+"");
				boolean closeFlag = true;
				String mess = "";
				//验证order是否都装完了 都装完了才可以close
				for (DBRow order : scanOrder) {
					DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPallet(order.get("wms_order_id", 0L));
					for (DBRow pallet : pallets) {
						if(pallet.get("scan_adid", 0l) == 0){
							closeFlag = false;
							if(!mess.contains(order.get("order_numbers", "")))
								mess += "Order: "+order.get("order_numbers", "")+"\n";
						}
					}
				}
				if(!closeFlag && scanOrder.length>0){
					mess += "Please scan all pallet!";
					throw new SmallParcelNotCloseOrderException(mess);
				}
			}
			if(currentLoadRow != null && (currentLoadRow.get("number_status",0)==CheckInChildDocumentsStatusTypeKey.EXCEPTION || currentLoadRow.get("number_status",0)==CheckInChildDocumentsStatusTypeKey.PARTIALLY)){
				DBRow numStatus  =  new DBRow();
				numStatus.add("number_status", data.get("number_status", 0l));
				this.checkInMgrZwb.updateDetailByIsExist(detail_id, numStatus);
			}else{
				DBRow row = new DBRow();
				row.add("number_status", data.get("number_status", 0l));
				row.add("note", note);
				finishDetail(currentLoadRow, rl_id, row, adminLoggerBean.getAdid(),resources_type);

				result.add("nofityFlag", nofityFlag);
			}
			
			// 处理文件
//			String filePath = data.getString("filePath");
//			if (!StringUtil.isBlank(filePath)) {
//				String fileDescBasePath = Environment.getHome()
//						+ "upload/check_in/";
//				File dir = new File(filePath);
//				if (dir.isDirectory()) {
//					File[] files = dir.listFiles();
//					for (File file : files) {
//						// String fileName =
//						// StringUtil.getFileName(file.getName());
//						checkInMgrZwb.addFile("dock_close_" + entry_id + "_"+ file.getName(), entry_id,FileWithTypeKey.OCCUPANCY_MAIN, 0,adminLoggerBean.getAdid(), nowTime);
//						// FileUtil.copyProductFile(file, fileDescBasePath);
//						FileUtil.checkInAndroidCopyFile(file, "dock_close_"+ entry_id + "_", fileDescBasePath);
//					}
//				}
//			}

/**			String logdata = "";
			String lognote = "";
			String number = currentLoadRow.getString("number");

			if (!number.equals("")) {

				lognote = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(data.get("number_status", 0));
				logdata += moduleKey.getModuleName(currentLoadRow.get("number_type", 0)) + ":" + number + ",";
				if (data.get("number_status", 0) == 4) {
					logdata += "Reason:" + note;
				}

			}
			checkInMgrZwb.addCheckInLog(adminLoggerBean.getAdid(), lognote,entry_id, CheckInLogTypeKey.ANDROID_DOCK_CLOSE, nowTime,logdata);*/

			return result;
		} catch (SmallParcelNotCloseOrderException e){
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "DockCloseBill", log);
		}
	}

	@Override
	public DBRow mutiDockClose(DBRow data, AdminLoginBean adminLoggerBean)
			throws Exception {
		try {
			DBRow result = new DBRow();
			String detail_ids = data.getString("detail_ids");
			long rl_id = data.get("rl_id", 0l); // android 回传给我
			long equipment_id = data.get("equipment_id", 0l);
			int resourcesType = data.get("resourcesType", 0);
			if (!StringUtil.isNull(detail_ids)) {
				String nowTime = DateUtil.NowStr();
				String[] arrayDetailIds = detail_ids.split(",");
				long entry_id = data.get("dlo_id", 0l);
				for (String temp : arrayDetailIds) {
 					String note = data.get("note", "");
					if (StringUtil.isNull(temp)) {
						continue;
					}
					long detail_id = Long.parseLong(temp);

					DBRow currentLoadRow = floorCheckInMgrZwb.selectDetailByDetailId(detail_id);
					int nofityFlag = checkInMgrZwb.closeDetailReturnFlag(equipment_id, entry_id, detail_id) ; // returnDockCloseNotifyFlag(currentLoadRow.get("number", ""), details,currentLoadRow);
					if (nofityFlag == CheckInMgrZwb.LoadCloseNotifyInputSeal) {
						result.add("loadbars", checkInMgrZwb.selectLoadBar());
					}

					DBRow row = new DBRow();
					row.add("number_status", data.get("number_status", 0l));
					row.add("note", note);
					finishDetail(currentLoadRow, rl_id, row,adminLoggerBean.getAdid(), resourcesType);

					result.add("nofityFlag", nofityFlag);
					// 处理文件

					String logdata = "";
					String lognote = "";
					String number = currentLoadRow.getString("number");

					if (!number.equals("")) {

						lognote = checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(data.get("number_status", 0));
						logdata += moduleKey.getModuleName(currentLoadRow.get("number_type", 0)) + ":" + number + ",";
						if (data.get("number_status", 0) == 4) {
							logdata += "Reason:" + note;
						}

					}
					checkInMgrZwb.addCheckInLog(adminLoggerBean.getAdid(),lognote, entry_id,CheckInLogTypeKey.ANDROID_DOCK_CLOSE, nowTime,logdata);

				}
				String filePath = data.getString("filePath");
				if (!StringUtil.isBlank(filePath)) {
					String fileDescBasePath = Environment.getHome()+ "upload/check_in/";
					File dir = new File(filePath);
					if (dir.isDirectory()) {
						File[] files = dir.listFiles();
						for (File file : files) {
							// String fileName =
							// StringUtil.getFileName(file.getName());
							checkInMgrZwb.addFile("dock_close_" + entry_id+ "_" + file.getName(), entry_id,FileWithTypeKey.OCCUPANCY_MAIN, 0,adminLoggerBean.getAdid(), nowTime);
							// FileUtil.copyProductFile(file, fileDescBasePath);
							FileUtil.checkInAndroidCopyFile(file, "dock_close_"+ entry_id + "_", fileDescBasePath);
						}
					}
				}

			}
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "mutiDockClose", log);
		}
	}

	/**
	 * 因为停留+离开/释放门 都是这一个方法 release door
	 * 的时候，应该把当前door门下面的所有task表对应的,关联关系删除。(已经在Close的时候删除了)
	 * 
	 * 
	 * equipment_id Inyard equipment_id LeavingInyard
	 */
	@Override
	public long DockCloseReleaseDoor(DBRow data, AdminLoginBean adminLoggerBean)
			throws Exception {
		try {
			
			int main_status = data.get("status", 0); // 3 停留 ， 4 是离开
			
			if (main_status > 0) {
				this.taskProcessingEquipmentLeavingInyard(data, adminLoggerBean); ;
			} else {
				this.taskProcessingReleaseDoor(data, adminLoggerBean);
			}
			return 0l;
		} catch (Exception e) {
			throw new SystemException(e, "DockCloseReleaseDoor", log);
		}
	}

	/**
	 * 李俊昊 ， 回来的时候页面刷新同时也不想弹出isLeft的界面
	 * 
	 * @param entry_id
	 * @param adminLoginBean
	 * @param isRefresh
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月21日
	 */
	private DBRow queryEntryRow(long entry_id, AdminLoginBean adminLoginBean,
			boolean isRefresh) throws Exception {
		try {
			DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
			CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
			String wareHoseCheckInTime = entryRow.getString("warehouse_check_in_time");

			if (StringUtil.isNull(wareHoseCheckInTime)) { // 这里判断是否// 在手持设备上做了DockCheckIn，有点奇怪依靠主单据去做的，// zhangrui 09-24
				throw new DockCheckInFirstException();
			}
			boolean isLeft = !isRefresh
					&& CheckInEntryPermissionUtil.isEntryLeft(entryRow)
					&& !floorCheckInMgrZwb.isEntryDetailHasProcessing(entryRow.get("dlo_id", 0l));
			if (isLeft) {
				throw new CheckInEntryIsLeftException();
			}
			return entryRow;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch (DockCheckInFirstException e) {
			throw e;
		} catch (CheckInEntryIsLeftException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "queryEntryRow", log);
		}

	}

	/**
	 * [{ door: load:[{
	 * 
	 * }] }]
	 * 
	 * @param arrays
	 * @return
	 * @throws Exception
	 */
	private DBRow[] fixDetailDatas(DBRow[] arrays) throws Exception {
		CheckInChildDocumentsStatusTypeKey typeKey = new CheckInChildDocumentsStatusTypeKey();
		List<DBRow> arrayList = new ArrayList<DBRow>();
		Map<String, List<DBRow>> maps = new HashMap<String, List<DBRow>>();
		if (arrays != null && arrays.length > 0) {
			for (DBRow row : arrays) {

				String number = row.getString("number");
				int numberType = row.get("number_type", 0);

				DBRow insertRow = new DBRow();
				String doorName = row.getString("doorId");
				List<DBRow> innerLoadNumbers = maps.get(doorName);

				innerLoadNumbers = innerLoadNumbers == null ? new ArrayList<DBRow>(): innerLoadNumbers;
				insertRow.add("number", number);
				insertRow.add("status", typeKey.getContainerTypeKeyValue(row.get("number_status", -1)));
				insertRow.add("status_int", row.get("number_status", -1));

				// 徐佳升级过后执行的东西
				insertRow.add("customer_id", row.getString("customer_id"));
				insertRow.add("company_id", row.getString("company_id"));
				insertRow.add("staging_area_id",row.getString("staging_area_id"));
				insertRow.add("number_type", row.get("number_type", 0));
				insertRow.add("account_id", row.getString("account_id"));
				insertRow.add("freight_term", row.getString("freight_term"));
				insertRow.add("dlo_detail_id", row.get("dlo_detail_id", 0l));
				insertRow.add("number_type", numberType);
				insertRow.add("occupancy_status",row.get("occupancy_status", 0));
				insertRow.add("rl_id", row.get("rl_id", 0));

				innerLoadNumbers.add(insertRow);
				maps.put(doorName, innerLoadNumbers);
			}
		}

		Iterator<Entry<String, List<DBRow>>> it = maps.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<DBRow>> entry = it.next();
			DBRow inner = new DBRow();
			DBRow[] loads = entry.getValue().toArray(new DBRow[0]);
			inner.add("loads", loads);
			inner.add("door", entry.getKey());
			inner.add("door_id",loads != null && loads.length > 0 ? loads[0].get("rl_id", 0) : 0);
			arrayList.add(inner);
		}
		return arrayList.toArray(new DBRow[0]);
	}

	@Override
	public void dockCloseUpPhoto(DBRow data, DBRow loginRow) throws Exception {
		try {
			String filePath = data.getString("filePath");
			long main_id = data.get("dlo_id", 0l);
			if (!StringUtil.isBlank(filePath)) {
				String fileDescBasePath = Environment.getHome()+ "upload/check_in/";
				File dir = new File(filePath);
				if (dir.isDirectory()) {
					File[] files = dir.listFiles();
					for (File file : files) {
						// String fileName =
						// StringUtil.getFileName(file.getName());
						checkInMgrZwb.addFile("dock_close_" + main_id + "_"+ file.getName(), main_id,FileWithTypeKey.OCCUPANCY_MAIN, 0,loginRow.get("adid", 0l), DateUtil.NowStr());
						// FileUtil.copyProductFile(file, fileDescBasePath);
						FileUtil.checkInAndroidCopyFile(file, "dock_close_"+ main_id + "_", fileDescBasePath);
					}
				}

			}
		} catch (Exception e) {
			throw new SystemException(e, "dockCloseUpPhoto", log);
		}
	}

	/**
	 * 把验证过后的数据插入到数据库中
	 */
	@Override
	public void addLoadBarSet(DBRow[] loadSetRows) throws Exception {

	}

	/**
	 * 给定一个数组 { company_id, customer_id, title, shipto, freight_term, load_bar }
	 * 1.都是字符串. company_id,customer_id,freight_term,load_bar 都是必须的，没有这些数据报错.
	 * 2.title 如果填写了，那么要验证是否系统当中存在
	 * 3.如果有shipto的值，那么在查询的时候，会用上面的company_id,customer_id,freight_term个值+ shipto
	 * 一块查询 ， 4.导入的数据如果有相同的CompanyId，CustomerId,freight_term，load_bar
	 * 那么只会查询一次，第二条数据如果在出现上面的值那么是不应该查询的 5.要一次验证一条信息的所有的错误
	 * 
	 */
	@Override
	public void validateLoadBarSet(DBRow[] loadSetRows) throws Exception {
		try {
			if (loadSetRows != null && loadSetRows.length > 0) {
				Map<String, Long> customerIdContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题,只会在在查询的出结果，然后有人改变了数据库的值
				Map<String, Long> companyIdContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题
				Map<String, Long> titleContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题
				Map<String, Long> shipToContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题
				Map<String, Long> freightTermContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题
				Map<String, Long> loadBarContents = new HashMap<String, Long>(); // 设置成静态的，应该也不会有问题

				for (DBRow loadSet : loadSetRows) {
					if (loadSet != null) {
						HoldThirdValue<Boolean, String, Integer> returnCheckValue = checkLoadBarSet(
								loadSet, customerIdContents, companyIdContents,
								titleContents, shipToContents,
								freightTermContents, loadBarContents);
						loadSet.add("validate_type", returnCheckValue.c);
						loadSet.add("validate_message", returnCheckValue.b);
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "validateLoadBarSet", log);
		}

	};

	/**
	 * 调用这个方法的时候这个String值对应的Id肯定是存在的
	 * 
	 * @param loadSet
	 * @param companyIdCheck
	 * @param customerIdCheck
	 * @param freightTermCheck
	 * @param loadBarCheck
	 * @param checkShipTo
	 * @param checkTitle
	 * @author zhangrui
	 * @Date 2014年11月6日
	 */
	private void wrapLadSetRows(DBRow loadSet,
			HoldThirdValue<Boolean, String, Long> companyIdCheck,
			HoldThirdValue<Boolean, String, Long> customerIdCheck,
			HoldThirdValue<Boolean, String, Long> freightTermCheck,
			HoldThirdValue<Boolean, String, Long> loadBarCheck,
			HoldThirdValue<Boolean, String, Long> checkShipTo,
			HoldThirdValue<Boolean, String, Long> checkTitle) {

		loadSet.add("company_key", companyIdCheck.c);
		loadSet.add("customer_key", customerIdCheck.c);
		loadSet.add("title_id", checkTitle.c);
		loadSet.add("freight_term_id", freightTermCheck.c);
		loadSet.add("load_bar_id", loadBarCheck.c);
		loadSet.add("ship_to_id", checkShipTo.c);

	}

	/**
	 * null 的检查，数据是否合法的检查 , 是否在数据库的设置已经有了对应的关系
	 * 
	 * @param loadSet
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日 返回Boolean, 如果有错误的信息拼接在一起 , ErrorType
	 */
	private HoldThirdValue<Boolean, String, Integer> checkLoadBarSet(
			DBRow loadSet, Map<String, Long> customerIdContents,
			Map<String, Long> companyIdContents,
			Map<String, Long> titleContents, Map<String, Long> shipToContents,
			Map<String, Long> freightTermContents,
			Map<String, Long> loadBarContents) throws Exception {

		String company_id = loadSet.getString("company_id");
		String customer_id = loadSet.getString("customer_id");
		String title = loadSet.getString("title");
		String shipto = loadSet.getString("shipto");
		String freight_term = loadSet.getString("freight_term");
		String load_bar = loadSet.getString("load_bar");

		// 判断数据库是否存在,对应输入的值
		HoldThirdValue<Boolean, String, Long> companyIdCheck = checkCompanyId(company_id, companyIdContents);
		HoldThirdValue<Boolean, String, Long> customerIdCheck = checkCustomerId(customer_id, customerIdContents);
		HoldThirdValue<Boolean, String, Long> freightTermCheck = checkFreightTerm(freight_term, freightTermContents);
		HoldThirdValue<Boolean, String, Long> loadBarCheck = checkLoadBar(load_bar, loadBarContents);
		HoldThirdValue<Boolean, String, Long> checkShipTo = checkShipTo(shipto,shipToContents);
		HoldThirdValue<Boolean, String, Long> checkTitle = checkTitle(title,titleContents);
		List<HoldThirdValue<Boolean, String, Long>> returnArrays = new ArrayList<HoldThirdValue<Boolean, String, Long>>();

		returnArrays.add(companyIdCheck);
		returnArrays.add(customerIdCheck);
		returnArrays.add(freightTermCheck);
		returnArrays.add(loadBarCheck);
		returnArrays.add(checkShipTo);
		returnArrays.add(checkTitle);

		HoldDoubleValue<Boolean, String> returnCheckResult = returnCheckResult(returnArrays);
		if (returnCheckResult.a) {
			wrapLadSetRows(loadSet, companyIdCheck, customerIdCheck,freightTermCheck, loadBarCheck, checkShipTo, checkTitle);
			HoldDoubleValue<Boolean, DBRow> isInDataBase = isCheckLoadBarSetInDataBase(loadSet);
			if (isInDataBase.a) {// 这个设置是否在数据库已经存在了
				return new HoldThirdValue<Boolean, String, Integer>(false,"Is Exsits , Load Bar Is "+ isInDataBase.b.getString("load_bar_name"),
						ValidateResultTypeOfDataExsits);
			} else {
				return new HoldThirdValue<Boolean, String, Integer>(true, null,ValidateResultTypeOfNoError);
			}
		}
		return new HoldThirdValue<Boolean, String, Integer>(
				returnCheckResult.a, returnCheckResult.b,
				ValidateResultTypeOfDataValidate); // 肯定是失败了
	}

	/**
	 * 检查当前的Set是否在数据库存在了。 如果存在，那么返回 存在的DBRow
	 * 
	 * @param loadSet
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月6日
	 */
	private HoldDoubleValue<Boolean, DBRow> isCheckLoadBarSetInDataBase(
			DBRow loadSet) throws Exception {
		try {

			long company_key = loadSet.get("company_key", 0l);
			long customer_key = loadSet.get("customer_key", 0l);
			long title_id = loadSet.get("title_id", 0l);
			long freight_term_id = loadSet.get("freight_term_id", 0l);
			long load_bar_id = loadSet.get("load_bar_id", 0l);
			long ship_to_id = loadSet.get("ship_to_id", 0l);
			DBRow result = floorCheckInMgrZr.getLoadSetBy(company_key,
					customer_key, freight_term_id, ship_to_id, load_bar_id);

			return new HoldDoubleValue<Boolean, DBRow>(result == null ? false: true, result);
		} catch (Exception e) {
			throw new SystemException(e, "isCheckLoadBarSetInDataBase", log);
		}
	}

	/**
	 * 检查这个Set是否可以添加进去
	 * 
	 * @param resultArrays
	 * @return
	 * @author zhangrui
	 * @Date 2014年11月6日
	 */
	private HoldDoubleValue<Boolean, String> returnCheckResult(
			List<HoldThirdValue<Boolean, String, Long>> resultArrays) {
		boolean returnFlag = true;
		StringBuffer returnMessage = new StringBuffer();
		if (resultArrays != null) {
			for (HoldThirdValue<Boolean, String, Long> temp : resultArrays) {
				returnFlag = returnFlag && temp.a;
				if (!temp.a) {
					returnMessage.append(temp.b + "\n");
				}
			}
		}
		return new HoldDoubleValue<Boolean, String>(returnFlag,
				returnMessage.toString());
	}

	/**
	 * @param company_id
	 * @param contents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日 返回<boolean, 错误信息>
	 */
	private HoldThirdValue<Boolean, String, Long> checkCompanyId(
			String company_id, Map<String, Long> contents) throws Exception {
		if (StringUtil.isNull(company_id)) {
			return new HoldThirdValue<Boolean, String, Long>(false,"CompanyId is NULL", 0L);
		}
		Long companyKey = contents.get(company_id);
		if (companyKey == null || companyKey == 0l) {
			companyKey = queryCompanyKey(company_id);
			if (companyKey == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"CompanyId Invalidate", 0L);
			}
			contents.put(company_id, companyKey);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null, companyKey);
	}

	/**
	 * 验证CustomerId
	 * 
	 * @param customer_id
	 * @param customerContents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日
	 */
	private HoldThirdValue<Boolean, String, Long> checkCustomerId(
			String customer_id, Map<String, Long> customerContents)
			throws Exception {
		if (StringUtil.isNull(customer_id)) {
			return new HoldThirdValue<Boolean, String, Long>(false,"CustomerId is NULL", 0L);
		}
		Long customerKey = customerContents.get(customer_id);
		if (customerKey == null || customerKey == 0l) {
			customerKey = queryCustomerKey(customer_id);
			if (customerKey == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"CustomerId Invalidate", 0L);
			}
			customerContents.put(customer_id, customerKey);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null,
				customerKey);
	}

	/**
	 * shipto 不是必须的，但是如果有输入的话，那么必须验证是否在系统存在
	 * 
	 * @param shipto
	 * @param shiptoContents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日
	 */
	private HoldThirdValue<Boolean, String, Long> checkTitle(String title,
			Map<String, Long> titleContents) throws Exception {
		if (StringUtil.isNull(title)) {
			return new HoldThirdValue<Boolean, String, Long>(true, null, 0L);
		}
		Long titleId = titleContents.get(title);
		if (titleId == null || titleId == 0l) {
			titleId = queryTitle(title);
			if (titleId == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"Title Invalidate.", 0L);
			}
			titleContents.put(title, titleId);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null, titleId);
	}

	/**
	 * shipto 不是必须的，但是如果有输入的话，那么必须验证是否在系统存在
	 * 
	 * @param shipto
	 * @param shiptoContents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日
	 */
	private HoldThirdValue<Boolean, String, Long> checkShipTo(String shipto,
			Map<String, Long> shiptoContents) throws Exception {
		if (StringUtil.isNull(shipto)) {
			return new HoldThirdValue<Boolean, String, Long>(true, null, 0L);
		}
		Long shiptoId = shiptoContents.get(shipto);
		if (shiptoId == null || shiptoId == 0l) {
			shiptoId = queryShipto(shipto);
			if (shiptoId == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"(Account)ShipTo Invalidate.", 0L);
			}
			shiptoContents.put(shipto, shiptoId);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null, shiptoId);
	}

	/**
	 * @param freight_term
	 * @param shiptoContents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日
	 */
	private HoldThirdValue<Boolean, String, Long> checkFreightTerm(
			String freight_term, Map<String, Long> freightTermContents)
			throws Exception {
		if (StringUtil.isNull(freight_term)) {
			return new HoldThirdValue<Boolean, String, Long>(false,"Freight Term is NULL", 0L);
		}
		Long freightTermId = freightTermContents.get(freight_term);
		if (freightTermId == null || freightTermId == 0l) {
			freightTermId = queryFreigtTerm(freight_term);
			if (freightTermId == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"Freight Term Invalidate.", 0L);
			}
			freightTermContents.put(freight_term, freightTermId);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null,
				freightTermId);
	}

	/**
	 * @param load_bar
	 * @param loadBarContents
	 * @return
	 * @author zhangrui
	 * @throws Exception
	 * @Date 2014年11月6日
	 */
	private HoldThirdValue<Boolean, String, Long> checkLoadBar(String load_bar,
			Map<String, Long> loadBarContents) throws Exception {
		if (StringUtil.isNull(load_bar)) {
			return new HoldThirdValue<Boolean, String, Long>(false,"Load Bar is NULL", 0L);
		}
		Long loadBarId = loadBarContents.get(load_bar);
		if (loadBarId == null || loadBarId == 0l) {
			loadBarId = queryLoadBar(load_bar);
			if (loadBarId == null) {
				return new HoldThirdValue<Boolean, String, Long>(false,"Load Bar Invalidate.", 0L);
			}
			loadBarContents.put(load_bar, loadBarId);
		}
		return new HoldThirdValue<Boolean, String, Long>(true, null, loadBarId);
	}

	private Long queryCompanyKey(String company_id) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getCompany(company_id);
			return resultRow == null ? null : resultRow.get("company_key", 0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryCompanyKey", log);
		}
	}

	private Long queryCustomerKey(String customer_id) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getCustomer(customer_id);
			return resultRow == null ? null : resultRow.get("customer_key", 0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryCustomerKey", log);
		}
	}

	private Long queryTitle(String title) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getTitle(title);
			return resultRow == null ? null : resultRow.get("title_id", 0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryTitle", log);
		}
	}

	private Long queryShipto(String shipto) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getShipTo(shipto);
			return resultRow == null ? null : resultRow.get("ship_to_id", 0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryShipto", log);
		}
	}

	private Long queryFreigtTerm(String freightTerm) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getFreightTerm(freightTerm);
			return resultRow == null ? null : resultRow.get("freight_term_id",0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryFreigtTerm", log);
		}
	}

	private Long queryLoadBar(String load_bar) throws Exception {
		try {
			DBRow resultRow = floorCheckInMgrZr.getLoadBar(load_bar);
			return resultRow == null ? null : resultRow.get("load_bar_id", 0l);
		} catch (Exception e) {
			throw new SystemException(e, "queryLoadBar", log);
		}
	}

	@Override
	public DBRow[] goingToWareHouse(long ps_id, PageCtrl pc) throws Exception {
 		try { 
 			 DBRow[] returnDatas =  floorCheckInMgrZJ.noAssignLabor(ps_id, pc);
 			 CheckInEntryPriorityKey checkInEntryPriorityKey = new CheckInEntryPriorityKey();
			 //添加是否is_last
			 if(returnDatas != null && returnDatas.length > 0){
				DBRow psTimeSet =  floorCheckInMgrZr.getSetScreenParam(ps_id, SetReportScreenTypeKey.NeedAssign, SetReportScreenParamsTypeKey.HandleTime); //floorCheckInMgrZwb.selectPsTimeSet(ps_id);
				int timeOut = psTimeSet != null ?  psTimeSet.get("total_time", TimeSetOut) : TimeSetOut ;	//-1表示永远不过期
				 for(DBRow temp :  returnDatas){
					 int total_time = temp.get("total_time", 0);
					 temp.add("is_late",total_time > timeOut && timeOut != TimeSetOut ? 1 : 0);
					 String waitingType = "";
					 if(temp.get("waiting", 0)==WaitingTypeKey.EarlyWaiting){
						 waitingType ="Early";
					 }else if(temp.get("waiting", 0)==WaitingTypeKey.LateWaiting){
						 waitingType ="Late";
					 }else if(temp.get("waiting", 0)==WaitingTypeKey.WarehouseWaitng){
						 waitingType ="Warehouse";
					 }else if(temp.get("waiting", 0)==WaitingTypeKey.NoAppointment){
						 waitingType ="Waiting";
					 }else{
						 waitingType ="";
					 }
					 temp.add("priority", checkInEntryPriorityKey.getCheckInEntryPriorityKey(temp.get("priority", 0)));
					 temp.add("waiting", waitingType);
				 }
			 }
			 return returnDatas ;
		} catch (Exception e) {
			throw new SystemException(e, "goingToWareHouse", log);
		}
	}

	@Override
	public DBRow[] remainJobs(long ps_id, PageCtrl pc) throws Exception {
		try{
			 
			DBRow[] returnDatas = floorCheckInMgrZJ.remainJobsDoor(ps_id, pc);
			 //添加是否is_last
			 if(returnDatas != null && returnDatas.length > 0){
				DBRow psTimeSet =  floorCheckInMgrZr.getSetScreenParam(ps_id, SetReportScreenTypeKey.RemainJobs, SetReportScreenParamsTypeKey.HandleTime); //floorCheckInMgrZwb.selectPsTimeSet(ps_id);
				int timeOut = psTimeSet != null ?  psTimeSet.get("total_time", TimeSetOut) : TimeSetOut ;	//-1表示永远不过期
				 for(DBRow temp :  returnDatas){
					 int total_time = temp.get("total_time", 0);
					 temp.add("is_late",total_time > timeOut && timeOut != TimeSetOut ? 1 : 0);
				 }
			 }
			 return returnDatas ;
		 } catch (Exception e) {
			throw new SystemException(e, "goingToWareHouse", log);
		}
	}
	/**
	 * 通过number_type 去判断
	 * 当不是Load/Po/Order 没有 总数 和
	 * 当CTNR 收货的时候 会显示成100%
	 * @param detail_id
	 * @param number_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月5日
	 */
	private HoldDoubleValue<Integer, Integer> getLoadOrReceivePalletAndTotalPallet(DBRow detail) throws Exception{
		try{
			long detail_id = detail.get("dlo_detail_id", 0l);
			int number_type = detail.get("number_type", 0) ;
			long lr_id = detail.get("lr_id", 0l);
			int scanPallet = 0 ;
			int totalPallet = 0 ;
			 //正常装货的情况
			 if(number_type == ModuleKey.CHECK_IN_LOAD  
					 ||	number_type ==  ModuleKey.CHECK_IN_ORDER 
					 ||	number_type == ModuleKey.CHECK_IN_PONO){
				 scanPallet =  floorCheckInMgrZwb.loadDoneConut(detail_id);
				 totalPallet = floorCheckInMgrZwb.totalConut(lr_id);
				 return new HoldDoubleValue<Integer, Integer>(scanPallet, totalPallet);
			 }
			 //收货的情况
			 if(number_type == ModuleKey.CHECK_IN_BOL 
					 || number_type == ModuleKey.CHECK_IN_CTN
					 || number_type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS){
				 
				 //totalPallet = floorCheckInMgrZr.getCommonReceivePallet(lr_id);
				 scanPallet =  floorCheckInMgrZr.receiveCountPallet(detail_id);
				 totalPallet = floorCheckInMgrZr.receiveAllPallet(detail_id);
				 return new HoldDoubleValue<Integer, Integer>(scanPallet, totalPallet);

			 }
			 //装货的一般的情况 不是Load/Order/Po
			 if(number_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS){
				 totalPallet =  floorCheckInMgrZr.getCommonLoadPallet(lr_id);
				 return new HoldDoubleValue<Integer, Integer>(scanPallet, totalPallet);
			 }
			 return new HoldDoubleValue<Integer, Integer>(scanPallet, totalPallet);
		}catch(Exception e){
			throw new SystemException(e, "taskTotalPallet", log);
		}
	}
	
	private void caculateTaskInOrOut(DBRow temp){
		int number_type = temp.get("number_type", 0);
		if(number_type == ModuleKey.CHECK_IN_LOAD 
				|| number_type == ModuleKey.CHECK_IN_PONO
				|| number_type == ModuleKey.CHECK_IN_ORDER
				|| number_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS){
			temp.add("rel_type", "Out");
			return ;
		}
		if(number_type == ModuleKey.CHECK_IN_BOL 
				|| number_type == ModuleKey.CHECK_IN_CTN
				|| number_type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS){
			temp.add("rel_type", "In");
			return ;
		}
		temp.add("rel_type", "??");

	}
//sss
	@Override
	public DBRow[] workingOnWarehouse(long ps_id,int lr_type, PageCtrl pc) throws Exception {
		try{
			 DBRow[] returnDatas = floorCheckInMgrZJ.workingOnWarehouse(ps_id,lr_type, pc);
			 if(returnDatas != null && returnDatas.length > 0 )
			 {
				 DBRow psTimeSet = floorCheckInMgrZr.getSetScreenParam(ps_id, SetReportScreenTypeKey.UnderProcessing, SetReportScreenParamsTypeKey.HandleTime); // floorCheckInMgrZwb.selectPsTimeSet(ps_id);
				 for(DBRow temp : returnDatas){
				 	//添加is_late,total_pallet,doneCount
					long dlo_detail_id =  temp.get("dlo_detail_id",0l);
					int timeOut = psTimeSet != null ?  psTimeSet.get("value", TimeSetOut) : TimeSetOut ;	//-1表示永远不过期
					int total_time = temp.get("total_time", 0);
					temp.add("is_late",total_time > timeOut && timeOut != TimeSetOut ? 1 : 0);
					//total_pallet , doneCount
					HoldDoubleValue<Integer, Integer> scanAndTotalPallet = getLoadOrReceivePalletAndTotalPallet(temp);
					int load_count= scanAndTotalPallet.a ;
					int total_count= scanAndTotalPallet.b ;//this.floorCheckInMgrZwb.totalConut(temp.get("lr_id", 0l));
					
					temp.add("total_pallet", total_count>0?total_count:"??");
					if(total_count>0){
						double doneCount=load_count/ (total_count * 1.0d);
						int fixCount = (int)(doneCount * 100) ;
						temp.add("doneCount",fixCount >= 100 ? 100 : fixCount );
					}else{
						temp.add("doneCount",0);
					}
					caculateTaskInOrOut(temp);
					temp.add("number_type",this.moduleKey.getModuleName(temp.get("number_type",0)));
					if(lr_type==1){
						int linesQty = this.floorCheckInMgrZJ.getReceiptLinesCount(temp.get("receipt_no", 0l), temp.get("company_id", ""));
						temp.add("linesQty",linesQty);
					}
					
				 }
			 }
			 return returnDatas ;
		}catch(Exception e){
 			throw new SystemException(e, "workingOnWarehouse", log);
		}
	}

	@Override
	public DBRow[] cargoOnSpot(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.cargoOnSpot(ps_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "cargoOnSpot", log);
		}
	}

	@Override
	public DBRow[] emptyCTNR(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.emptyCTNR(ps_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "emptyCTNR", log);
		}
	}

	@Override
	public DBRow[] cargoOnDoor(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.cargoOnDoor(ps_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "cargoOnDoor", log);
		}
	}
	@Override
	public DBRow[] ghostCTNR(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.noSpaceRelationEquipment(ps_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "cargoOnDoor", log);
		}
	}

	@Override
	public DBRow[] parking3rdPartyNone(long ps_id, PageCtrl pc)
			throws Exception {
		try {
			return floorCheckInMgrZJ.parking3rdPartyNone(ps_id, pc);

		} catch (Exception e) {
			throw new SystemException(e, "parking3rdPartyNone", log);
		}
	}

	@Override
	public DBRow[] goingToWindow(long ps_id, PageCtrl pc) throws Exception {
		try {
			DBRow[] returnDatas =  floorCheckInMgrZJ.goingToWindow(ps_id, pc);
			CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
			 if(returnDatas != null && returnDatas.length > 0){
					DBRow psTimeSet =  floorCheckInMgrZr.getSetScreenParam(ps_id, SetReportScreenTypeKey.IncomingToWindow, SetReportScreenParamsTypeKey.HandleTime); //floorCheckInMgrZwb.selectPsTimeSet(ps_id);
					int timeOut = psTimeSet != null ?  psTimeSet.get("total_time", TimeSetOut) : TimeSetOut ;	//-1表示永远不过期
					 for(DBRow temp :  returnDatas){
						 int total_time = temp.get("total_time", 0);
						 temp.add("is_late",total_time > timeOut && timeOut != TimeSetOut ? 1 : 0);
						 temp.add("equipment_purpose",checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(temp.get("equipment_purpose", 0)));
					 }
			 }
			return returnDatas ;
		} catch (Exception e) {
			throw new SystemException(e, "goingToWindow", log);
		}
	}
	
	public DBRow[] noTaskEntry(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.noTaskEntry(ps_id, pc);

		} catch (Exception e) {
			throw new SystemException(e, "goingToWindow", log);
		}
	}
	@Override
	public DBRow[] loadReceiveCloseToday(long ps_id ,PageCtrl pc) throws Exception{
		try{
			DBRow[] returnArray = null ;
			String startTime = null ;
			String endTime = DateUtil.NowStr() ;
			String endLocalTime =  DateUtil.showLocationTime(new Date(), ps_id);
			if(!StringUtil.isNull(endLocalTime)){
				String[] timeArray = endLocalTime.split(" ");
				if(timeArray.length > 1){
					String prefixTime =  timeArray[0] ;
					String timeStr = prefixTime + " " + "00:00:00";	
					startTime = DateUtil.showUTCTime(timeStr, ps_id);
				}
			}
			if(!StringUtil.isNull(startTime) && !StringUtil.isNull(endTime)){
 				returnArray = floorCheckInMgrZr.loadReceiveCloseToday(ps_id, startTime, endTime,pc);
				returnArray = fixLoadReceiveCloseToday(returnArray);
 			}
			return returnArray;
		}catch(Exception e){
			throw new SystemException(e, "loadReceiveCloseToday", log);
		}
	 } 
	@Override
	public DBRow[] loadReceiveNotFinish(long ps_id, PageCtrl pc)
			throws Exception {
		try{
			DBRow[] datas =  floorCheckInMgrZr.notFinishLoadReceive(ps_id, pc) ;
			CheckInLiveLoadOrDropOffKey checkInLiveLoadOrDropOffKey = new CheckInLiveLoadOrDropOffKey();
			if(datas != null && datas.length > 0){
				DBRow[] returnRow = new DBRow[datas.length];
				String endTime = DateUtil.NowStr();
				DBRow psTimeSet =  floorCheckInMgrZr.getSetScreenParam(ps_id, SetReportScreenTypeKey.LoadReceive, SetReportScreenParamsTypeKey.HandleTime); //floorCheckInMgrZwb.selectPsTimeSet(ps_id);

				for(int index = 0 , count = datas.length ; index < count ; index++ ){
					DBRow temp = datas[index];
					DBRow newTemp = new DBRow();
					fixBaseLoadReceiveDatas(temp, newTemp);
					long total_time = caculteMini(temp.getString("create_time"), endTime) ;
					newTemp.add("total_time", total_time);
					returnRow[index] = newTemp ;
					
					int timeOut = psTimeSet != null ?  psTimeSet.get("total_time", TimeSetOut) : TimeSetOut ;	//-1表示永远不过期
					newTemp.add("is_late",total_time > timeOut && timeOut != TimeSetOut ? 1 : 0);
					newTemp.add("rel_type",temp.get("order_type",0)==1?"In":"Out");
					newTemp.add("equipment_purpose",checkInLiveLoadOrDropOffKey.getCheckInLiveLoadOrDropOffKey(temp.get("equipment_purpose",0)));
					 
				}
				return returnRow ;
			}
			return new DBRow[0];
		}catch(Exception e){
			throw new SystemException(e, "loadReceiveNotFinish", log);
		}
 	}
	 
	private void fixBaseLoadReceiveDatas(DBRow temp ,  DBRow newTemp){
		OrderSystemTypeKey orderSystemTypeKey = new OrderSystemTypeKey();
		
		String customer_id = temp.getString("customer_id") ;
		customer_id = StringUtil.isBlank(customer_id) ? "NA" : customer_id ;
		
		String title = temp.getString("title") ;
		title = StringUtil.isBlank(title) ? "NA" : title ;
		
		String company_id = temp.getString("company_id") ;
		company_id = StringUtil.isBlank(company_id) ? "NA" : company_id ;
		
		String account_id = temp.getString("account_id") ;
		account_id = StringUtil.isBlank(account_id) ? "NA" : account_id ;
		
		
		newTemp.add("customer_id", customer_id);
		newTemp.add("title",title);
		newTemp.add("company_id", company_id);
		newTemp.add("account_id",account_id);
		newTemp.add("number", temp.getString("number") );
		String systemType = orderSystemTypeKey.getTypeValue(temp.get("system_type", 0)) ;
		systemType = StringUtil.isBlank(systemType) ? "??" : systemType;
		newTemp.add("system_type", systemType);
		newTemp.add("entry_id", temp.get("entry_id", 0L));
	}
	private DBRow[] fixLoadReceiveCloseToday(DBRow[] returnArray) throws Exception{
		try{
			if(returnArray != null && returnArray.length > 0){
				DBRow[] arrays = new DBRow[returnArray.length];
				for(int index = 0 , count = returnArray.length ; index < count ; index++ ){
					DBRow temp = returnArray[index];
					DBRow newTemp = new DBRow();
 					newTemp.add("total_time", caculteMini(temp.getString("starttime"), temp.getString("finishtime")));
					newTemp.add("labor", temp.getString("labor") );
					fixBaseLoadReceiveDatas(temp, newTemp);
					newTemp.add("number_status",checkInChildDocumentsStatusTypeKey.getContainerTypeKeyValue(temp.get("number_status", 0)) );
					arrays[index] = newTemp ;
				}
				return arrays ;
			}
			return new DBRow[0];
		}catch(Exception e){
			throw new SystemException(e, "fixLoadReceiveCloseToday", log);
		}
	}
 
	private long caculteMini(String startTime ,  String endTime) {
		long caculteTime  = -1l;
		try{
			//如果start_time 为null ||  或者时间格式错误 那么返回-1
			if(!(StringUtil.isBlank(startTime) || StringUtil.isBlank(endTime))){
				long endLongTime =	 DateUtil.createDateTime(endTime).getTime() ;
				long startLongTime =  DateUtil.createDateTime(startTime).getTime() ;
				caculteTime = (endLongTime - startLongTime)/60000;
			}
		}catch(Exception e){}
		return caculteTime ;
	}
	@Override
	public DBRow[] waitingList(long ps_id, PageCtrl pc) throws Exception {
		try {
			 DBRow[] returnDatas = floorCheckInMgrZJ.waitingList(ps_id, pc);
			 CheckInEntryPriorityKey checkInEntryPriorityKey = new CheckInEntryPriorityKey();
			 for(DBRow temp :  returnDatas){
				 String waitingType = "";
				 if(temp.get("waiting", 0)==WaitingTypeKey.EarlyWaiting){
					 waitingType ="Early";
				 }else if(temp.get("waiting", 0)==WaitingTypeKey.LateWaiting){
					 waitingType ="Late";
				 }else if(temp.get("waiting", 0)==WaitingTypeKey.WarehouseWaitng){
					 waitingType ="Warehouse";
				 }else if(temp.get("waiting", 0)==WaitingTypeKey.NoAppointment){
					 waitingType ="Waiting";
				 }else{
					 waitingType ="";
				 }
				 temp.add("priority", checkInEntryPriorityKey.getCheckInEntryPriorityKey(temp.get("priority", 0)));
				 temp.add("waitingType", waitingType);
			 }
			 return returnDatas;
		} catch (Exception e) {
			throw new SystemException(e, "waitingList", log);
		}
	}
 	@Override
	public DBRow[] forgetCloseTask(long ps_id, PageCtrl pc) throws Exception {
		try {
			 DBRow[] returnDatas = floorCheckInMgrZJ.forgetCloseTask(ps_id, pc);
			 for(DBRow temp : returnDatas){
				 temp.add("number_type",this.moduleKey.getModuleName(temp.get("number_type",0)));
			 }
			 return returnDatas;
		} catch (Exception e) {
			throw new SystemException(e, "forgetCloseTask", log);
		}
	}

	@Override
	public DBRow[] leaving(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.leaving(ps_id, pc);

		} catch (Exception e) {
			throw new SystemException(e, "leaving", log);
		}
	}

	@Override
	public DBRow[] forgetCheckOut(long ps_id, PageCtrl pc) throws Exception {
		try {
			return floorCheckInMgrZJ.forgetCheckOut(ps_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "forgetCheckOut", log);
		}
	}
	@Override
	public DBRow[] noTaskLabor(long ps_id, long[] adgids,long[] adgroles,PageCtrl pc)
			throws Exception {
		try{
			return floorCheckInMgrZJ.noTaskLabor(ps_id, adgids,adgroles, pc);
		}catch(Exception e){
			throw new SystemException(e, "noTaskLabor", log);
		}
 	}
	@Override
	public DBRow[] remainJobsLabor(long ps_id, PageCtrl pc) throws Exception {
		try{
			return floorCheckInMgrZJ.remainJobsLabor(ps_id, pc);
		}catch(Exception e){
			throw new SystemException(e, "remainJobsLabor", log);
		}
	}
	/**
	 * 1.android assign 任务的操作 2.首先是Task对应的任务应该占用 3.把Task对应的物理设备也占用这个门
	 * 4.同时应该把Task对应的设备的车头或者车尾，标志位处理中 5.同时更新主表的记录，warehouseCheckIn的时间
	 * 6.Assign的时候同时写上了Handle_time在Task表中 7.但是在删除任务的时候，是没有去删除handle-time
	 * 
	 * 8.如果设备占用是LiveLoad，那么应该把这个LiveLoa的车头也移动到这个位置来（也就是占用当前Assign的门）
	 * 释放物理资源，都是应该先删除然后再占用 9.添加日志 Doo1 task 1 e1 task 2 e1 task 3 e1
	 * 
	 * Doo2 task 4 e1 task 5 e2 task 6 e2
	 * 
	 * e1 在 spot1 e2 在 spot2
	 * 
	 * 
	 * 如果DockCheckIn assign了一个任务 task2. 结果 ：task2 占用;e1释放Spot1 占用door1;
	 * 如果DockCheckIn assign了door1下面，所有的任务.结果: task1,2,3 占用;e1 释放Spot1占用door1
	 * 如果DockCheckIn assign了Door2下面，所有的任务.结果: task4,5,6占用 ;e1
	 * 释放Spot1占用door2;e2释放spot2占用dook2 如果DockCheckIn 同时assign了Door1，Door2的任务.
	 * 结果: task1,2,3,4,5,6 占用.e1释放资源spot1,e1占用资源Dock1，Dock2;e2释放spot2,占用Door2;
	 * 
	 * 
	 * (e1 + target(door_id)) 进行计算，只要有一个不一样那么就应该添加占用的记录
	 * 
	 * 
	 * 2014-11-29 改动
	 * 如果是Assign的任务所在的设备是LiveLoad，那么会移动这个LiveLoad设备的车头和这个一起移动到Assign的门上
	 * 例子中Assign的时候是可以移动多个设备的，但是现在是选择一个设备后，然后Assign这个设备下面的Task，所以只会移动一个设备
	 * 但是还是可能出现一个设备多个门上工作，但是在操作关系表的时候都是会先删除原有的记录，所以一个设备多个门 &&
	 * 同时Assign的结果，设备只会Occupied最后一个门
	 * 
	 * 
	 */
	/*@Override
	public DBRow dockCheckInAssignTask(HttpServletRequest request,
			AdminLoginBean adminLoginBean) throws Exception {
		try {
			// 创建时间
			// 改变子单据的状态 处理中
			long adid = adminLoginBean.getAdid();
			String createTime = DateUtil.NowStr();
			String value = StringUtil.getString(request, "values");
			JSONObject jsons = new JSONObject(value);
			JSONArray details = jsons.getJSONArray("datas");
			long entry_id = StringUtil.getLong(request, "entry_id");
			long equipment_id = 0l;
			List<HoldDoubleValue<Long, Long>> taskIdsAndDoorIds = new ArrayList<HoldDoubleValue<Long, Long>>();
			Set<Long> doorIds = new HashSet<Long>();
			if (details != null && details.length() > 0) {
				for (int i = 0; i < details.length(); i++) {
					JSONObject json = StringUtil.getJsonObjectFromArray(details, i);
					long door_id = StringUtil.getJsonLong(json, "door_id");
					long detail_id = StringUtil.getJsonLong(json, "detail_id");
					equipment_id = StringUtil.getJsonLong(json, "equipment_id"); // request
																					// 添加这个参数
					taskIdsAndDoorIds.add(new HoldDoubleValue<Long, Long>(detail_id, door_id));
					doorIds.add(door_id); // 2014-11-29 equpment_id只会有一个了

					DBRow upDetailrow = new DBRow();
					upDetailrow.add("number_status", 2); // 更新子单据状态为处理中
					upDetailrow.add("handle_time", DateUtil.NowStr());

					floorCheckInMgrZwb.updateOccupancyDetails(String.valueOf(detail_id), upDetailrow);
					dockCheckInAssignTaskHandleScheduleAndLog(json,adminLoginBean, createTime, request);
				}
				handAssignTask(entry_id, taskIdsAndDoorIds, doorIds, adid,equipment_id);
			}
			return dockCheckInSearchEntryDetail(entry_id, equipment_id,adminLoginBean);

		} catch (Exception e) {
			throw new SystemException(e, "androidAddWareHouseNotice", log);
		}
	}*/

 

	/**
	 * 日志获取 Task/Eq relation 的信息 log日志用
	 * 
	 * @param relation_type
	 * @param relation_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月3日
	 */
	private String getLogDataOfRelation(int relation_type, long relation_id)
			throws Exception {
		try {
			if (relation_type == SpaceRelationTypeKey.Equipment) {
				DBRow equipment = getEquipment(relation_id);
				if (equipment != null) {
					String returnValue = equipment.getString("equipment_type_value")+ ":"+ equipment.getString("equipment_number");
					return returnValue;
				}
			} else {
				DBRow detail = floorCheckInMgrZwb.selectDetailByDetailId(relation_id);
				if (detail != null) {
					String returnValue = getLogDateOfDetailRow(detail);
					return returnValue;
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getLogDataOfRelation", log);
		}
	}
	private String getLogDateOfDetailRow(DBRow detail) throws Exception{
		try{
			String returnValue = moduleKey.getModuleName(detail.get("number_type", 0))+ ":"	+ detail.getString("number");
			return returnValue ;
		}catch(Exception e){
			throw new SystemException(e, "getLogDateOfDetailRow", log);
		}
	}

	/**
	 * 日志获取资源的数据 Dock/Spot
	 * 
	 * @param resources_id
	 * @param resources_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月3日
	 */
	private String getLogDataOfResouces(long resources_id, int resources_type)
			throws Exception {
		try {
			if (resources_type == OccupyTypeKey.DOOR) {
				DBRow dock = floorCheckInMgrZwb.findDoorById(resources_id);
				if (dock != null) {
					return "Door:" + dock.getString("doorId");
				}
			}
			if (resources_type == OccupyTypeKey.SPOT) {
				DBRow spot = floorCheckInMgrZwb.findStorageYardControl(resources_id);
				if (spot != null) {
					return "Spot:" + spot.getString("yc_no");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getLogDataOfResouces", log);
		}
	}

	/**
	 * 添加 Task/Eqiment 操作资源 door/Spot 记录日志
	 * 先操作日志，然后再释放
	 * @param resources_type
	 * @param resources_id
	 * @param relation_type
	 * @param relation_id
	 * @param adid
	 * @param entry_id
	 * @param log_type
	 * @param note
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月3日
	 */
 	private void logAddTaskOrEqOperationSpaceLog(int resources_type,
			long resources_id, int relation_type, long relation_id, long adid,
			long entry_id, int log_type, String note , int occupyStatusTypeKey ) throws Exception {
		try {
			DBRow exits = ymsMgrAPI.getSpaceResorceRelation(relation_type, relation_id);
			//首先查询一次,如果当前的设备 or task 占用的状态和这次的一样就不要添加了
			/*if(exits != null){
				if((resources_id == exits.get("resources_id", 0l) && 
						resources_type == exits.get("resources_type", 0)) && occupyStatusTypeKey == exits.get("occupy_status", 0)){
						return ;
				}
 			}*/
			String data = getLogDataOfRelation(relation_type, relation_id);
			data += "," + getLogDataOfResouces(resources_id, resources_type);
			checkInMgrZwb.addCheckInLog(adid, note, entry_id, log_type,DateUtil.NowStr(), data);
		} catch (Exception e) {
			throw new SystemException(e, "addTaskOrEqOperationSpaceLog", log);
		}
	}

	 
	/**
	 * 如果设备是头尾 && 对应的Entry是LiveLoad，那么把车头也一同移动到某个门上 同时写入check_in_warehouse_time
	 * 
	 * 单纯的changeDoor 的时候 是不需要改变Equipment的状态的,同时y
	 * @param equipment
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
 	private void equipmentOccuipedMoveLiveTractor(DBRow equipment, long adid,
			long resources_id, long entry_id , int checkInLogType,int resources_type) throws Exception {
		try {
			if (equipment != null && equipment.get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.LIVE) {
				// 找到这个Entry对应的车头，然后占用当前 的目标
				long check_in_entry_id = equipment.get("check_in_entry_id", 0l);
				if (check_in_entry_id != 0l) {
					DBRow tractor = ymsMgrAPI.getEntryTractor(check_in_entry_id);
					if (tractor != null) {
						DBRow updateRow = new DBRow();
						if (StringUtil.isNull(tractor.getString("check_in_warehouse_time"))) {
							updateRow.add("check_in_warehouse_time",DateUtil.NowStr()); // 更新 equipment_id的// check_in_warehouse_time的时间
						}
						
						updateRow.add("equipment_status",CheckInMainDocumentsStatusTypeKey.PROCESSING);
						ymsMgrAPI.updateEquipment(tractor.get("equipment_id", 0l), updateRow);
						
						
					 
						logAddTaskOrEqOperationSpaceLog(resources_type,
								resources_id, SpaceRelationTypeKey.Equipment,
								tractor.get("equipment_id", 0l), adid,
								entry_id,
								checkInLogType,
								resources_type ==  OccupyTypeKey.DOOR ?  LogEquipmentOccuipedDoor :  LogEquipmentOccuipedSpot
								,OccupyStatusTypeKey.OCUPIED);

						
						if(resources_type ==  OccupyTypeKey.DOOR ){
							equipmentOperationDoorSpace(check_in_entry_id, adid,
									tractor.get("equipment_id", 0l), resources_id,
									OccupyStatusTypeKey.OCUPIED);
						}else{
							equipmentOperationSpotSpace(check_in_entry_id, adid,
									tractor.get("equipment_id", 0l), resources_id,
									OccupyStatusTypeKey.OCUPIED);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "equipmentOccuipedMoveLiveTractor",log);
		}
	}


	/**
	 * 通过adids 返回人名的组合
	 * 
	 * @param adids
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月25日
	 */
	private String getOperations(String adids) throws Exception {
		StringBuffer returnValue = new StringBuffer();
		if (!StringUtil.isBlank(adids)) {
			String[] arraysIds = adids.split(",");
			for (String adid : arraysIds) {
				long adidLong = Long.parseLong(adid);
				if (adidLong > 0l) {
					DBRow row = adminMgr.getDetailAdmin(adidLong);
					if (row != null) {
						returnValue.append(",").append(	row.getString("employe_name"));
					}
				}

			}

		}
		return returnValue.length() > 1 ? returnValue.substring(1) : "";
	}

	/**
	 * List { resource_id : sd_id/yc_id resource_name: doorId/yc_no
	 * resource_type: door/spot details:[ {}, {} ] }
	 * 
	 * HoldDoubleValue<resource_id,resource_type>
	 * 如果是当前的门下面的任务都没有出于Unpocess的，那么这个门就不要添加到这个集合里面了 同时过滤出停车位的数据,但是停车位的下面的Task
	 * 是不可以分配的,必须是要移动到门上以后才可以做
	 * 
	 * @param detailsHasDoorOrSpot
	 * @return
	 * @author zhangrui
	 * @Date 2014年11月25日
	 */
	@SuppressWarnings("unchecked")
	private Map<HoldDoubleValue<Long, Integer>, DBRow> filterHasDoorOrSpotAndTaskNumberStatus(
			DBRow[] detailsHasDoorOrSpot, Integer[] filterNumberStatus,
			long equi) throws Exception {
		try {
 			Map<HoldDoubleValue<Long, Integer>, DBRow> hashMap = new HashMap<HoldDoubleValue<Long, Integer>, DBRow>();
			if (detailsHasDoorOrSpot != null && detailsHasDoorOrSpot.length > 0) {
				for (DBRow temp : detailsHasDoorOrSpot) {
					checkInMgrZwb.returnFixResourcesIdAndResourcesType(temp);
					
					long resources_id = temp.get("resources_id", 0l);
					int number_status = temp.get("number_status", 0);
					int resources_type = temp.get("resources_type", 0);
					int relation_type = temp.get("relation_type", 0);
 					if (/*resources_id != 0l &&*/ relation_type == SpaceRelationTypeKey.Task) {
						HoldDoubleValue<Long, Integer> key = new HoldDoubleValue<Long, Integer>(resources_id, resources_type);
						DBRow data = hashMap.get(key);
						if (data == null) {
							data = new DBRow();
							data.add("resources_id", resources_id);
							data.add("resources_type", resources_type);
							String resourcesTypeValue = resources_type == OccupyTypeKey.DOOR ? getDoorName(resources_id): getSpotName(resources_id);
							data.add("resources_type_value", resourcesTypeValue);
						}

						if ((isArrayContain(filterNumberStatus, number_status))) {
							List<DBRow> details = (ArrayList<DBRow>) data.get("details");
							details = (details == null ? new ArrayList<DBRow>(): details);
							details.add(temp);
							data.add("details", details);
							hashMap.put(key, data);
						}
					}
				}
			}

			return hashMap;
		} catch (Exception e) {
			throw new SystemException(e, "filterHasDoorAndTaskNumberStatus",log);
		}
	}

	/*
	 * public <T> boolean isArrayContaine(T[] a, T c){ return true ; }
	 */

	public <T> boolean isArrayContain(T[] arrays, T a) {
		if (arrays != null) {
			for (T temp : arrays) {
				if (temp.equals(a)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * warehouse主管 分配任务的时候过滤
	 * 
	 * @param detailsHasDoorOrSpot
	 * @param filterNumberStatus
	 * @param equi
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月6日
	 */
	private Map<HoldDoubleValue<Long, Integer>, DBRow> filterDoorAndSpotNeedSuperAssignTask(
			DBRow[] detailsHasDoorOrSpot) throws Exception {
		try {
			Map<HoldDoubleValue<Long, Integer>, DBRow> hashMap = new HashMap<HoldDoubleValue<Long, Integer>, DBRow>();
			if (detailsHasDoorOrSpot != null && detailsHasDoorOrSpot.length > 0) {
				for (DBRow temp : detailsHasDoorOrSpot) {
					long resources_id = temp.get("resources_id", 0l);
					int resources_type = temp.get("resources_type", 0);
					int relation_type = temp.get("relation_type", 0);

					if (resources_id != 0l && relation_type == SpaceRelationTypeKey.Task) {
						HoldDoubleValue<Long, Integer> key = new HoldDoubleValue<Long, Integer>(resources_id, resources_type);
						DBRow data = hashMap.get(key);
						if (data == null) {
							data = new DBRow();
							data.add("resources_id", resources_id);
							data.add("resources_type", resources_type);
							String resourcesTypeValue = resources_type == OccupyTypeKey.DOOR ? getDoorName(resources_id)
									: getSpotName(resources_id);
							data.add("resources_type_value", resourcesTypeValue);
						}
						List<DBRow> details = (ArrayList<DBRow>) data.get("details");
						details = (details == null ? new ArrayList<DBRow>(): details);
						details.add(temp);
						data.add("details", details);
						hashMap.put(key, data);

					}
				}
			}
			return hashMap;
		} catch (Exception e) {
			throw new SystemException(e, "filterHasDoorAndTaskNumberStatus",
					log);
		}
	}

	/**
	 * 根据门的ID 获取门的名字 如果没有查询到返回"";
	 * 
	 * @param sd_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月25日
	 */
	@Override
	public String getDoorName(long sd_id) throws Exception {
		try {
			if (sd_id != 0l) {
				DBRow doorRow = floorCheckInMgrZwb.findDoorById(sd_id);
				if (doorRow != null) {
					return doorRow.getString("doorId");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getDoorName", log);
		}
	}

	/**
	 * 根据yc_id 去获取一个SpotNo
	 * 
	 * @param yc_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月29日
	 */
	@Override
	public String getSpotName(long yc_id) throws Exception {
		try {
			if (yc_id != 0l) {
				DBRow spotRow = floorCheckInMgrZwb.findStorageYardControl(yc_id);
				if (spotRow != null) {
					return spotRow.getString("yc_no");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getSpotName", log);
		}
	}

	/**
	 * 返回一个DBRow[]的数组
	 * 
	 * @return
	 * @author zhangrui
	 * @Date 2014年11月25日 2014-11-25
	 */
	private DBRow[] findDetailsHasDoorOrSpot(long entry_id, long equipment_id,
			Integer[] filterNumberStatus) throws Exception {
		try {
			DBRow[] hasDoorDetails = floorSpaceResourcesRelationMgr.getTaskByEntryIdWithOccpuyStatus(entry_id, 0, equipment_id);
 			// zhanjie 返回的方法肯定是有门 和 spot的但是我只是需要Door的数据，Spot的Assign任务的先不考虑
			// 现在已经考虑了
			List<DBRow> returnRows = new ArrayList<DBRow>();
			if (hasDoorDetails != null && hasDoorDetails.length > 0) {
				Map<HoldDoubleValue<Long, Integer>, DBRow> result = filterHasDoorOrSpotAndTaskNumberStatus(hasDoorDetails, filterNumberStatus, equipment_id);
				if (result != null && result.size() > 0) {
					Iterator<Entry<HoldDoubleValue<Long, Integer>, DBRow>> it = result.entrySet().iterator();
					while (it.hasNext()) {
						Entry<HoldDoubleValue<Long, Integer>, DBRow> entry = it.next();
						DBRow row = entry.getValue();
						if (row != null) {
							List<DBRow> details = (ArrayList<DBRow>) (row.get("details"));
							if (details != null && details.size() > 0) {
								returnRows.add(row);
							}
						}
					}
				}
			}
			return returnRows.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "findDetailsHasDoor", log);
		}

	}

	// android warehouse 数据 2014-11-24 , selectDoorByInfoId ,
	// selectDoorByInfoId调用zhanjie的方法 返回这个门 + 门的detail

	/**
	 * 根据一个EntryId 去获取这个Entry下面所有的Task对应的设备 group by
	 * 
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月28日
	 */
	@Override
	public DBRow[] getEntryTaskEquipment(long entry_id) throws Exception {
		try {
			DBRow[] equipments = floorCheckInMgrZr.getEnetryTaskEquipments(entry_id);
			fixEquipments(equipments);
			return equipments;
		} catch (Exception e) {
			throw new SystemException(e, "getEntryTaskEquipment", log);
		}
	}

	/**
	 * 移除某些equipment属性
	 * 
	 * @param equipments
	 * @author zhangrui
	 * @throws Exception 
	 * @Date 2014年11月29日
	 */
	private void fixEquipments(DBRow... equipments) throws Exception {
		if (equipments != null && equipments.length > 0) {
			for (DBRow temp : equipments) {
//				temp.remove("check_out_entry_id");
				temp.remove("check_out_time");
				//temp.remove("equipment_purpose");
				//temp.remove("equipment_status");
				temp.remove("check_in_time");
				temp.add("equipment_type_value",
						checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(temp.get("equipment_type", 0)));
				
				
				//处理时间
				long ps_id = temp.get("ps_id", 0l);
 				String check_in_window_time	 = temp.getString("check_in_window_time");
				String check_in_warehouse_time	 = temp.getString("check_in_warehouse_time");
				String check_out_warehouse_time	 = temp.getString("check_out_warehouse_time");
				 
				if(!StringUtil.isBlank(check_in_window_time)){
					check_in_window_time = DateUtil.showLocalparseDateToNoYear24Hours(check_in_window_time, ps_id);
				}
				if(!StringUtil.isBlank(check_in_warehouse_time)){
					check_in_warehouse_time = DateUtil.showLocalparseDateToNoYear24Hours(check_in_warehouse_time, ps_id);
				}
				if(!StringUtil.isBlank(check_out_warehouse_time)){
					check_out_warehouse_time = DateUtil.showLocalparseDateToNoYear24Hours(check_out_warehouse_time, ps_id);
				}
				temp.add("check_in_window_time", check_in_window_time);
				temp.add("check_in_warehouse_time", check_in_warehouse_time);
				temp.add("check_out_warehouse_time", check_out_warehouse_time);

			}
		}
	}

 
	/**
	 * 通过equipment_id 去过滤 equipment ，同时移除某些不需要的属性
	 * 
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月29日
	 */
	private DBRow getEquipment(long equipment_id) throws Exception {
		try {
			DBRow equipment = ymsMgrAPI.getDetailEquipment(equipment_id);
			if (equipment != null) {
				DBRow[] fixArrays = new DBRow[] { equipment };
				fixEquipments(fixArrays);
				return fixArrays[0];
			}
			return null;
		} catch (Exception e) {
			throw new SystemException(e, "getEquipment", log);
		}
	}

	 

 

	/**
	 * dock check in 1.首先要选择设备过后才可以，选择Task进行Assign 2.【设备】 来自于所有Task distinct 设备
	 * ，查询出来
	 * 
	 */
/*	@Override
	public DBRow dockCheckInSearchEntryDetail(long infoId, long equipment_id,
			AdminLoginBean adminLoginBean) throws Exception {
		try {
			DBRow result = new DBRow();
			DBRow mainRow = checkInMgrZwb.findMainById(infoId);
			CheckInEntryPermissionUtil.checkEntry(mainRow, adminLoginBean);
			if (mainRow != null) {
				result.add("out_seal", mainRow.get("out_seal", ""));
				result.add("in_seal", mainRow.get("in_seal", ""));
			} else {
				throw new CheckInNotFoundException();
			}

			result.add("rel_type", mainRow.get("rel_type", ""));
			DBRow[] rows = findDetailsHasDoorOrSpot(infoId,equipment_id,new Integer[] { CheckInChildDocumentsStatusTypeKey.UNPROCESS });
			for (DBRow tempRow : rows) {
				List<DBRow> arrayList = (ArrayList<DBRow>) tempRow.get("details", new ArrayList<DBRow>());
				tempRow.remove("details");
				List<DBRow> fixArrayLis = new ArrayList<DBRow>();
				for (DBRow detail : arrayList) {
					DBRow loadR = new DBRow();
					loadR.add("id", detail.get("number", ""));
					loadR.add("dlo_detail_id", detail.get("dlo_detail_id", 0l));
					loadR.add("number_type", detail.get("number_type", ""));
					loadR.add("order_no", detail.get("order_no", ""));
					loadR.add("po_no", detail.get("po_no", ""));
					loadR.add("customer_id", detail.get("customer_id", ""));
					loadR.add("company_id", detail.get("company_id", ""));
					loadR.add("equipment_id", detail.get("equipment_id", 0l));
					loadR.add("srr_id", detail.get("srr_id", 0l));
					if (!StringUtil.isBlank(detail.get("order_no", "")) && detail.get("number_type", 0) == ModuleKey.CHECK_IN_PONO) {
						loadR.add("type", 1);
					}
					if (!StringUtil.isBlank(detail.get("po_no", "")) && detail.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER) {
						loadR.add("type", 2);
					}
					fixArrayLis.add(loadR);
				}
				tempRow.add("load_list", fixArrayLis.toArray(new DBRow[0]));
			}
			result.add("tree", rows);
			List<DBRow> shList = new ArrayList<DBRow>(); // 返回的任务的数据 =
															// (当前equipment_id
															// 不等于
															// Unprocessed的数据)
			DBRow[] detailRow = floorCheckInMgrZr.getAssignTaskEquipmentTask(infoId, equipment_id);
			for (int i = 0; detailRow != null && i < detailRow.length; i++) {
				DBRow[] shRow = this.floorCheckInMgrZwb.selectSchedule(detailRow[i].get("dlo_detail_id", 0l),ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
				for (int a = 0; a < shRow.length; a++) {
					shList.add(shRow[a]);
				}
			}
			result.add("schedule", shList.toArray(new DBRow[shList.size()]));
			return result;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "dockCheckInSearchEntryDetail", log);
		}
	}*/

	/**
	 * task 操作门
	 *  删除或添加
	 * @param entry_id
	 * @param adid
	 * @param detail_id
	 * @param sd_id
	 * @param occupy_status
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月27日
	 */
	private void taskOperationResourcesSpace(Long entry_id, Long adid,Long detail_id, Long resources_id, int occupy_status , int resources_type) throws Exception {
		
		ymsMgrAPI.operationSpaceRelation(resources_type, resources_id,
				SpaceRelationTypeKey.Task, detail_id, ModuleKey.CHECK_IN,
				entry_id, occupy_status, adid);
	}

	/**
	 * 设备操作门的方法
	 * 删除后在添加
	 * @param entry_id
	 * @param adid
	 * @param equipment_id
	 * @param sd_id
	 * @param occupy_status
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月29日
	 */
 	private void equipmentOperationDoorSpace(Long entry_id, Long adid,Long equipment_id, Long sd_id, int occupy_status) throws Exception {
	
		ymsMgrAPI.operationSpaceRelation(OccupyTypeKey.DOOR, sd_id,
				SpaceRelationTypeKey.Equipment, equipment_id,
				ModuleKey.CHECK_IN, entry_id, occupy_status, adid);
	} 
	private void equipmentOperationSpotSpace(Long entry_id, Long adid,Long equipment_id, Long resouces_id, int occupy_status) throws Exception {
		
		ymsMgrAPI.operationSpaceRelation(OccupyTypeKey.SPOT, resouces_id,
				SpaceRelationTypeKey.Equipment, equipment_id,
				ModuleKey.CHECK_IN, entry_id, occupy_status, adid);
	}

	/**
	 * 1.dock checkin 的时候 提交in_seal,out_seal 2.处理图片,out_seal,in_seal .
	 * 3.处理warehouse_check_in_time 的时间交给assign任务去处理(以前是交给 submit的时候处理) 4.
	 */
	@Override
	public void dockCheckInSubmit(long entry_id, long adid,
			String delivery_seal, String pickup_seal, String filePath,
			boolean isAndroid, HttpServletRequest request) throws Exception {
		try {
			String createTime = DateUtil.NowStr();
			DBRow mainRow = new DBRow();
			StringBuffer logData = new StringBuffer();
			if (!delivery_seal.equals("")) {
				mainRow.add("in_seal", delivery_seal);
				logData.append(",Delivery Seal:" + delivery_seal);
			}
			if (!pickup_seal.equals("")) {
				mainRow.add("out_seal", pickup_seal);
				logData.append(",Pickup Seal:" + pickup_seal);

			}
			// 根据主单据id 查询主单据状态
			floorCheckInMgrZwb.modCheckIn(entry_id, mainRow); // 更新主表
			checkInMgrZwb.addCheckInLog(adid, "Update Entry Status", entry_id,
					isAndroid ? CheckInLogTypeKey.ANDROID_DOCK_CHECKIN: CheckInLogTypeKey.WAREHOUSE, createTime,
					pickup_seal.length() > 0 ? pickup_seal.substring(1) : "");
			if (isAndroid) {
				//checkInMgrZwb.androidSaveMoveFile(filePath, adid, entry_id,"dock_check_in_" + entry_id + "_");
				checkInMgrZwb.androidSaveMoveFile(filePath, entry_id,"dock_check_in_" + entry_id + "_", request.getRequestedSessionId());
			} else {
				//checkInMgrZwb.addFiles(request, entry_id, adid);
			}
		} catch (Exception e) {
			throw new SystemException(e, "dockCheckInSubmit", log);
		}
	}

 
	private DBRow[] getAllDoors(long ps_id) throws Exception {
		try {
			return floorCheckInMgrZr.getAllDoor(ps_id);
		} catch (Exception e) {
			throw new SystemException(e, "getAllDoors", log);
		}
	}

	private DBRow[] getAllSpots(long ps_id) throws Exception {
		try {
			return floorCheckInMgrZr.getAllSpot(ps_id);
		} catch (Exception e) {
			throw new SystemException(e, "getAllSpots", log);
		}
	}

	/**
	 * 返回结构 door 
	 * { 
	 * 	doorZoneId: 
	 * 	doorZoneName:
	 *  doors:[ { doorId: doorName: } ] 
	 *  }
	 * 
	 * spot 
	 * { 
	 * 	spotZoneId: 
	 * 	spotZoneName: 
	 * 	spots[ { spotId: spotName: } ]
	 * }
	 * 
	 * @param ps_id
	 * @param info_id
	 * @param request_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private final static int ResourcesTypeALL = -1 ;
	@Override
	public HoldDoubleValue<DBRow[], DBRow[]> androidGetResources(long ps_id, long entry_id, int request_type , int resources_type) throws Exception {
		try {
			DBRow[] doors = null;
			DBRow[] spots = null;
			//door
			if(resources_type  == OccupyTypeKey.DOOR || resources_type == ResourcesTypeALL ){
				if (request_type == RequestAll) {
					doors = getAllDoors(ps_id);
  				} else {
 					doors = fixAndroidSelectCanUseDoor(entry_id, ps_id, 0l);
 				}
				doors = fixAndroidSelectDoor(doors,false);
 			}
			//spot
			if(resources_type == OccupyTypeKey.SPOT || resources_type == ResourcesTypeALL){
				if (request_type == RequestAll) {
 					spots = getAllSpots(ps_id);
 				} else {
  					spots = fixAndroidSelectCanUseSpot(entry_id, ps_id, 0l);
 				}
 				spots = fixAndroidSelectSpot(spots,false);
			}
			
			return new HoldDoubleValue<DBRow[], DBRow[]>(doors, spots);
		} catch (Exception e) {
			throw new SystemException(e, "androidGetResources", log);
		}
	}
	/**
	 * 返回占用资源的信息
	 * 
	 *{
	 * 
	 * doorzoneid:
	 * doorzonename:
	 * doors:[
	 * 	{
	 * 	 "doorname": "69",
         "doorid": 74 ,
         "Task":12
         "Equipments:"12
	 * 	} 
	 * ]
	 * }
	 * @param ps_id
	 * @param resources_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月26日
	 */
	@Override
	public HoldDoubleValue<DBRow[], DBRow[]> androidGetNotFreeResources(long ps_id, int resources_type) throws Exception {
		try{
			DBRow[] notFreeDoor = null ;
			DBRow[] notFreeSpot = null ;
			
			if(resources_type  == OccupyTypeKey.DOOR || resources_type == ResourcesTypeALL ){
				 notFreeDoor = floorCheckInMgrZr.getNotFreeDoor(ps_id);
				 notFreeDoor = fixAndroidSelectDoor(notFreeDoor,true);
			}
			if(resources_type == OccupyTypeKey.SPOT || resources_type == ResourcesTypeALL){
				notFreeSpot = floorCheckInMgrZr.getNotFreeSpot(ps_id);
				notFreeSpot = fixAndroidSelectSpot(notFreeSpot,true);
			}

			return new HoldDoubleValue<DBRow[], DBRow[]>(notFreeDoor, notFreeSpot);
		}catch(Exception e){
			throw new SystemException(e, "androidGetNotFreeResources", log);
		}
 	}

	private DBRow[] fixAndroidSelectCanUseSpot(long entry_id, long ps_id,
			long area_id) throws Exception {
		try {

			DBRow[] spots = floorSpaceResourcesRelationMgr.getCanUseSpot(entry_id, ps_id, 0l);
			if (spots != null && spots.length > 0) {
				for (DBRow temp : spots) {
					removeSpotDataComeFormDatabase(temp);
					temp.add("area_name",getSpotAreaName(temp.get("area_id", 0l)));
				}
				return spots;
			}
			return null;

		} catch (Exception e) {
			throw new SystemException(e, "fixAndroidSelectCanUseSpot", log);
		}
	}
	/*1.Load/Receive  Close Today 是没有大屏幕的 & 时间怎么计算
	2.Close Today
	3.*/
	/**
	 * 因为floorSpaceResourcesRelationMgr.getCanUseDoor
	 * 返回的数据是没有Area的信息的,所以我需要自己查询一次 & 同时调整数据
	 * 
	 * @param entry_id
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private DBRow[] fixAndroidSelectCanUseDoor(long entry_id, long ps_id,
			long area_id) throws Exception {
		try {
			DBRow[] doors = floorSpaceResourcesRelationMgr.getCanUseDoor(entry_id, ps_id, 0l);
			if (doors != null && doors.length > 0) {
				for (DBRow temp : doors) {
					removeDoorDataComeFromDataBase(temp);
					temp.add("area_name",getDoorAreaName(temp.get("area_id", 0l)));
				}
				return doors;
			}
			return null;
		} catch (Exception e) {
			throw new SystemException(e, "fixAndroidSelectCanUseDoor", log);
		}
	}

	private String getDoorAreaName(long area_id) throws Exception {
		try {
			if (area_id != 0l) {
				DBRow doorArea = floorCheckInMgrZr.getDoorAreaBy(area_id);
				if (doorArea != null) {
					return doorArea.getString("area_name");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getDoorAreaName", log);
		}
	}

	private String getSpotAreaName(long area_id) throws Exception {
		try {
			if (area_id != 0l) {
				DBRow doorArea = floorCheckInMgrZr.getSpotAreaBy(area_id);
				if (doorArea != null) {
					return doorArea.getString("area_name");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getSpotAreaName", log);
		}
	}

	/**
	 * 因为Door数据库里面都是使用* 所以先移除某些数据
	 * 
	 * @param temp
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private void removeDoorDataComeFromDataBase(DBRow temp) {
		temp.remove("associate_type");
		temp.remove("sd_status");
		temp.remove("height");
		temp.remove("occupied_status");
		temp.remove("width");
		temp.remove("latlng");

		temp.remove("associate_id");
		temp.remove("y");
		temp.remove("x");

	}

	/**
	 * 因为Spot数据库里面都是使用* 所以先移除某些数据
	 * 
	 * @param temp
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private void removeSpotDataComeFormDatabase(DBRow temp) {

		temp.remove("yc_status");
		temp.remove("height");
		temp.remove("width");
		temp.remove("latlng");

		temp.remove("angle");
		temp.remove("patrol_time");
		temp.remove("associate_type");
		temp.remove("associate_id");
		temp.remove("y");
		temp.remove("x");
	}

 
	public DBRow getDoorInArrays(List<DBRow> arrays , long resources_id){
		if(arrays != null && arrays.size() > 0){
			for(DBRow temp : arrays){
				if(temp.get("doorid", 0l) == resources_id){
					return temp ;
				}
			}
		}
		return null ;
	}
	public DBRow getSpotInArrays(List<DBRow> arrays , long resources_id){
		if(arrays != null && arrays.size() > 0){
			for(DBRow temp : arrays){
				if(temp.get("spotId", 0l) == resources_id){
					return temp ;
				}
			}
		}
		return null ;
	}
	/**
	 * door { doorZoneId: doorZoneName: doors:[ { doorId: doorName: } ] }
	 * 
	 * @param doors
	 * @return
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private DBRow[] fixAndroidSelectDoor(DBRow[] doors,boolean isAppendnotFree) {
		if (doors != null && doors.length > 0) {
			Map<HoldDoubleValue<Long, String>, List<DBRow>> returnMap = new HashMap<HoldDoubleValue<Long, String>, List<DBRow>>();
			for (DBRow door : doors) {
				long doorId = door.get("sd_id", 0l);
				String doorName = door.getString("doorId");
				long areaId = door.get("area_id", 0l);
				String areaName = door.getString("area_name");
				if(areaId <= 0l || StringUtil.isNull(areaName)){
					continue ;
				}
				HoldDoubleValue<Long, String> key = new HoldDoubleValue<Long, String>(areaId, areaName);
				List<DBRow> insertValue = returnMap.get(key);
				insertValue = (insertValue == null ? new ArrayList<DBRow>(): insertValue);

				door.remove("sd_id");
				door.remove("doorId");
				door.remove("area_id");
				door.remove("area_name");
				if(isAppendnotFree && door.get("relation_type", -1) == SpaceRelationTypeKey.Equipment ){
					int totalEquipments = door.get("total", 0);
					door.add("equipment", totalEquipments);
					door.remove("total");
					DBRow tempDoor = getDoorInArrays(insertValue, doorId);
					if(tempDoor != null){
						tempDoor.add("equipment", totalEquipments);
 						continue ;
					}
					 
				}
				if(isAppendnotFree && door.get("relation_type", -1) == SpaceRelationTypeKey.Task ){
					int totalTasks = door.get("total", 0);
					door.add("task", totalTasks);
					door.remove("total");
					DBRow tempDoor = getDoorInArrays(insertValue, doorId);
					if(tempDoor != null){
						tempDoor.add("task", totalTasks);
 						continue ;
					}
				}
				door.add("doorId", doorId);
				door.add("doorName", doorName);
				door.remove("relation_type");
				insertValue.add(door);
				returnMap.put(key, insertValue);
			}

			if (!returnMap.isEmpty()) {
				List<DBRow> returnArray = new ArrayList<DBRow>();
				Iterator<Entry<HoldDoubleValue<Long, String>, List<DBRow>>> it = returnMap.entrySet().iterator();
				while (it.hasNext()) {
					Entry<HoldDoubleValue<Long, String>, List<DBRow>> entry = it.next();
					HoldDoubleValue<Long, String> key = entry.getKey();
					DBRow insertTemp = new DBRow();
					insertTemp.add("doorZoneId", key.a);
					insertTemp.add("doorZoneName", key.b);
					List<DBRow> doorsArray = entry.getValue();
					Collections.sort(doorsArray, new StorageDoorComparator());
					insertTemp.add("doors", doorsArray.toArray(new DBRow[0]));
					returnArray.add(insertTemp);
				}
				return returnArray.toArray(new DBRow[0]);
			}
		}
		return null ;
	}

	/**
	 * spot { spotZoneId: spotZoneName: spots[ { spotId: spotName: } ] }
	 * 
	 * @param spots
	 * @return
	 * @author zhangrui
	 * @Date 2014年12月1日
	 */
	private DBRow[] fixAndroidSelectSpot(DBRow[] spots , boolean isAppendnotFree) {
		if (spots != null && spots.length > 0) {
			Map<HoldDoubleValue<Long, String>, List<DBRow>> returnMap = new HashMap<HoldDoubleValue<Long, String>, List<DBRow>>();
			for (DBRow spot : spots) {
				long spotId = spot.get("yc_id", 0l);
				String spotName = spot.getString("yc_no");
				long areaId = spot.get("area_id", 0l);
				String areaName = spot.getString("area_name");
				if(areaId <= 0l || StringUtil.isNull(areaName)){
					continue ;
				}
				HoldDoubleValue<Long, String> key = new HoldDoubleValue<Long, String>(areaId, areaName);
				List<DBRow> insertValue = returnMap.get(key);
				insertValue = (insertValue == null ? new ArrayList<DBRow>(): insertValue);
				spot.remove("yc_id");
				spot.remove("yc_no");
				spot.remove("area_id");
				spot.remove("area_name");
				
				if(isAppendnotFree && spot.get("relation_type", -1) == SpaceRelationTypeKey.Equipment ){
					
					int totalEquipments = spot.get("total", 0);
					spot.add("equipment", totalEquipments);
					spot.remove("total");
					DBRow tempSpot = getSpotInArrays(insertValue, spotId);
					if(tempSpot != null){
						tempSpot.add("equipment", totalEquipments);
 						continue ;
					}
					
 				}
				if(isAppendnotFree && spot.get("relation_type", -1) == SpaceRelationTypeKey.Task ){
					int totalTasks = spot.get("total", 0);
					spot.add("task", totalTasks);
					spot.remove("total");
					DBRow tempSpot = getSpotInArrays(insertValue, spotId);
					if(tempSpot != null){
						tempSpot.add("task", totalTasks);
 						continue ;
					}
				}
				

				spot.add("spotId", spotId);
				spot.add("spotName", spotName);
				spot.remove("relation_type");
				insertValue.add(spot);
				returnMap.put(key, insertValue);
			}

			if (!returnMap.isEmpty()) {
				List<DBRow> returnArray = new ArrayList<DBRow>();
				Iterator<Entry<HoldDoubleValue<Long, String>, List<DBRow>>> it = returnMap.entrySet().iterator();
				while (it.hasNext()) {
					Entry<HoldDoubleValue<Long, String>, List<DBRow>> entry = it
							.next();
					HoldDoubleValue<Long, String> key = entry.getKey();
					DBRow insertTemp = new DBRow();
					insertTemp.add("spotZoneId", key.a);
					insertTemp.add("spotZoneName", key.b);
					List<DBRow> spotArray = entry.getValue();
					Collections.sort(spotArray, new StorageSpotComparator());
					insertTemp.add("spots", spotArray.toArray(new DBRow[0]));
					returnArray.add(insertTemp);
				}
				return returnArray.toArray(new DBRow[0]);
			}
		}
		return null;
	}

	/**
	 * 通过EntryId去查询当前还没有wareHouse的主管没有分配的Task 这里是通过EntryId来查询的，不用人为准
	 */
	@Override
	public DBRow[] getWareHouseManagerAssignTaskBy(long entry_id)
			throws Exception {
		try {
			DBRow[] returnArrays = floorCheckInMgrZr.needWareHouseAssignTask(entry_id);
			return returnArrays;
		} catch (Exception e) {
			throw new SystemException(e, "getWareHouseManagerAssignTaskBy", log);
		}
	}

	/**
	 * 得到待分配
	 */
	@Override
	public DBRow[] getAssignTaskList(long adid) throws Exception {
		try {
			DBRow[] returnArrays = floorCheckInMgrZr.getAssignTaskList(adid);
			if (returnArrays != null) {
				for (DBRow temp : returnArrays) {
					temp.add("equipment_type_value",checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(temp.get("equipment_type", 0)));
				}
				return returnArrays;
			}
			return null;
		} catch (Exception e) {
			throw new SystemException(e, "fixAssignTaskRows", log);
		}
	}


	 
	private long[] convertStringArrayToLongArray(String[] strArrays) {
		if (strArrays != null) {
			long[] returnLong = new long[strArrays.length];
			for (int index = 0, count = strArrays.length; index < count; index++) {
				returnLong[index] = Long.parseLong(strArrays[index]);
			}
			return returnLong;
		}
		return null;
	}

	/**
	 * 这里是直接完成某个任务(那么同时更新 字表的完成)
	 * 
	 * finishSchedule 的时候写上任务的结束时间
	 * @param schedule_id
	 * @param adid
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月6日
	 */
	@Override
	public void finishSchedule(long schedule_id, long adid) throws Exception {
		//
		DBRow updateRow = new DBRow();
		updateRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
		updateRow.add("end_time", DateUtil.NowStr());
		
		scheduleMgrZr.simpleUdateSchedule(schedule_id, updateRow);
		DBRow updateSubRow = new DBRow();
		updateSubRow.add("is_task_finish", 1);
 		updateSubRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish); // 谁完成的不知道如果执行人 和 完成不是一样的情况
		//任务完成的时候写入 是谁给完成的
		updateSubRow.add("schedule_finish_adid", adid);
		scheduleMgrZr.simpleUpdateScheduleSub(schedule_id, updateSubRow);
	}

	/**
	 * 给warehouse的人分配任务的时候返回的Context
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月6日
	 */
	private String getAssignTaskToWareHouseScheduleContext(DBRow row)
			throws Exception {
		try {
			// 【 OTHERS : FFF 】 To 【Door : 69 】 In 【 Trailer : SD2 】
			String formate = " 【%1s : %2s 】 To 【%3s : %4s】 In 【%5s : %6s】";
			if (row != null) {
				String numberType = moduleKey.getModuleName(row.get("number_type", 0));
				String number = row.getString("number");
				String resourcesType = occupyTypeKey.getOccupyTypeKeyName(row.get("resources_type", 0));
				String resourcesValue = (row.get("resources_type", 0) == OccupyTypeKey.DOOR) ? getDoorName(row.get("resources_id", 0l)) : getSpotName(row.get(	"resources_id", 0l));
				String equipmentTypeValue = checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(row.get("equipment_type", 0));
				String equipmentNumber = row.getString("equipment_number");

				return String.format(formate, numberType, number,
						resourcesType, resourcesValue, equipmentTypeValue,
						equipmentNumber);
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e,
					"getAssignTaskToWareHouseScheduleContext", log);
		}
	}

	/**
	 * 通过Detail的Id去获取这个Detail,对应的给warehouse管理员 . Assign Task 的任务
	 *  
	 *  1.有可能是门卫给supervisor发的 
	 *  2.有可能是window的人给supervisor发的
	 *  3.有可能是门卫给window的人发的
	 *  
	 * @param dlo_detail_id
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月22日
	 */
	private DBRow[] getWareHouseManagerAssignTask(long dlo_detail_id ) throws Exception{
		try{
			return floorCheckInMgrZr.getWareHouseManagerAssignTask(dlo_detail_id);
		}catch(Exception e){
			throw new SystemException(e, "getWareHouseManagerAssignTask", log);
		}
	}
	private void finishSupverVisorTask(long adid , DBRow detail ) throws Exception{
		try{
			// 如果是为完成的情况 然后把check的任务标记为完成 
			DBRow[] schedules =  getWareHouseManagerAssignTask(detail.get("dlo_detail_id", 0l));//;scheduleMgrZr.getScheduleBy(detail.get("dlo_detail_id", 0l), ProcessKey.CHECK_IN_WINDOW, ModuleKey.CHECK_IN);
			if(schedules != null && schedules.length > 0){
				for(DBRow temp : schedules){
					if(temp.get("schedule_state", 0.0d) < ScheduleFinishKey.ScheduleFinish){
						finishSchedule(temp.get("schedule_id", 0l), adid);
					}
				}
			}
		}catch(Exception e){
			throw new SystemException(e, "finishSupverVisorTask", log);
		}
		
	}
	/**
	 * detail 中的数据是包含有设备 ＋　占用资源
	 * 
	 * @param detail
	 * @param execute_user_id
	 * @param adid
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月9日
	 */
	private void assignWareHouseTask(DBRow detail, String execute_user_id,
			long adid) throws Exception {
		try {
			if (detail != null) {
				scheduleMgrZr.addSchedule(ScheduleModel.getCheckInScheduleModel(
								detail.get("dlo_id", 0l),
								detail.get("dlo_detail_id", 0l),
								ProcessKey.CHECK_IN_WAREHOUSE,
								adid,
								execute_user_id,
								getAssignTaskToWareHouseScheduleContext(detail),
								detail.getString("number"),
								detail.get("number_type", 0)));
				
				finishSupverVisorTask(adid, detail);
				//添加一个assign_task的时间，如果detail时间已经存在了，那么就不用重新写这个时间了
				if(StringUtil.isNull(detail.getString("task_assign_loader_time"))){
					DBRow updateDetailRow = new DBRow();
					updateDetailRow.add("task_assign_loader_time", DateUtil.NowStr());
					floorCheckInMgrZwb.updateDetailByIsExist(detail.get("dlo_detail_id", 0l), updateDetailRow);
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "assignWareHouseTask", log);
		}
	}

	/**
	 * 给warehouse的主管添加任务
	 * @param detail
	 * @param execute_user_id
	 * @param adid
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	private void assignWareHouseManganerTask(DBRow detail, String execute_user_id,
			long adid) throws Exception {
		try {
			if (detail != null) {
				scheduleMgrZr.addSchedule(ScheduleModel.getCheckInScheduleModel(
								detail.get("dlo_id", 0l),
								detail.get("dlo_detail_id", 0l),
								ProcessKey.CHECK_IN_WINDOW,
								adid,
								execute_user_id,
								getAssignTaskToWareHouseScheduleContext(detail),
								detail.getString("number"),
								detail.get("number_type", 0)));
			}
		} catch (Exception e) {
			throw new SystemException(e, "assignWareHouseTask", log);
		}
	}
	
	/**
	 * super 管理员 assign CheckInWIndow 的Task 1. 给Loader 添加任务 WareHouseCheckIn的任务
	 * 2. 当前这个WareHouse的管理员的任务完成（schedule_id）
	 */
	/*@Override
	public void singleWareHouseManagerAssignTask(String execute_user_ids,
			String schedule_ids, DBRow loginRow) throws Exception {
		try {
			long adid = loginRow.get("adid", 0l);
			if (!StringUtil.isNull(schedule_ids) && !StringUtil.isNull(execute_user_ids)) {
				String[] scheduleIdstr = schedule_ids.split(",");
				long[] scheduleIds = convertStringArrayToLongArray(scheduleIdstr);
				for (long schedule_id : scheduleIds) {
					finishSchedule(schedule_id, adid); // finish 当前任务
					assignWareHouseTask(schedule_id, execute_user_ids, adid); // 给warehouse的人添加任务
				}
			}

		} catch (Exception e) {
			throw new SystemException(e, "singleAssignTask", log);
		}
	}*/
	/**
	 * Assign Task 的时候改变门的操作
	 * Assign Task 会改变处理中的任务,该到新的门 
	 * 如果是设备离开了，那么不应该有改门的操作
	 */
	@Override
	public void assignTaskMoveToDoor(long entry_id, long equipment_id,
			long sd_id, int moved_resource_type, long move_resouce_id, long adid)
			throws EquipmentHadOutException , Exception {
		try {
			if(isEquipmentCheckOut(equipment_id)){
				throw new EquipmentHadOutException();
			}
			
			// 把某些details移动到当前选择的门上
			StringBuffer noteData = new StringBuffer();
			DBRow[] details = ymsMgrAPI.getRelationByResouceAndEntryIdAndEquipmentId(entry_id,
							move_resouce_id, moved_resource_type, equipment_id,
							SpaceRelationTypeKey.Task);
			if (details != null) {
				noteData.append("Change Task To Door: " + getDoorName(sd_id)+ ",");
				DBRow updateRow = new DBRow();
				updateRow.add("resources_id", sd_id);
				updateRow.add("resources_type", OccupyTypeKey.DOOR);
				for (DBRow temp : details) {
					if (!checkInMgrZwb.isEntryDetailIsFinish(temp)) {
						noteData.append(moduleKey.getModuleName(temp.get("number_type",0))).append(":");
						noteData.append(temp.getString("number") + ",");
						long srr_id = temp.get("srr_id", 0l);
						ymsMgrAPI.updateSpaceResourcesRelation(srr_id,updateRow);
					}
				}
				String operator_time = DateUtil.NowStr();
				checkInMgrZwb.addCheckInLog(adid, "Change Task To Door",
						entry_id, CheckInLogTypeKey.AndroidAssignTask,
						operator_time, noteData.toString());
			}

		}catch(EquipmentHadOutException e){
			throw  e;
		}catch (Exception e) {
			throw new SystemException(e, "assignTaskMoveToDoor", log);
		}
	}
	/**
	 * 在AssignTask 可以更改Task的处理的资源
	 * 
	 * 1.当时如果一个人在正在AndroidTaskProcessing操作 ，然后这边有改变了门，那么会有问题。在Close的时候记录的数据还是 在AndroidTaskProcessing中的为准
	 * 
	 * 
	 */
	@Override
	public void assignTaskChangeDoorByDetails(String detail_ids,
			long resouces_id, int resouces_type, long equipment_id , AdminLoginBean adminLoginBean , long entry_id)
			throws EquipmentHadOutException, Exception {
		try{
			if(isEquipmentCheckOut(equipment_id)){
				throw new EquipmentHadOutException();
			}
			if(!StringUtil.isNull(detail_ids)){
				String[] detailArrayIds= detail_ids.split(",");
				if(detailArrayIds != null && detailArrayIds.length > 0){
					StringBuffer noteData = new StringBuffer() ;
					for(String detailStr :  detailArrayIds){
						long dlo_detail_id = Long.parseLong(detailStr);
						
						DBRow updateRow = new DBRow();
						updateRow.add("resources_type", resouces_type);
						updateRow.add("resources_id", resouces_id);

						DBRow currentLoadRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
						if(currentLoadRow != null){
							if (!checkInMgrZwb.isEntryDetailIsFinish(currentLoadRow)) {
								int updateCount = floorCheckInMgrZr.updateSpaceBy(  dlo_detail_id , SpaceRelationTypeKey.Task ,updateRow);
								if(updateCount > 0){
									noteData.append(moduleKey.getModuleName(currentLoadRow.get("number_type",0))).append(":");
									noteData.append(currentLoadRow.getString("number") + ",");
								}
							}
						}
						
					}
					if(noteData.length() > 0 ){
						noteData.append(resouces_type == OccupyTypeKey.DOOR ? ("Door:"+ getDoorName(resouces_id)) : "Spot:"+getSpotName(resouces_id));
					}
					String operator_time = DateUtil.NowStr();
					checkInMgrZwb.addCheckInLog(adminLoginBean.getAdid(), "Change Task To Door",
							entry_id, CheckInLogTypeKey.AndroidAssignTask,
							operator_time, noteData.toString());
				}
			}
		}catch(EquipmentHadOutException e){
			throw e ;
		}catch(Exception e){
			throw new SystemException(e, "assignTaskChangeDoorByDetails", log);
		}
	}
	
	/**
	 * Execute Task List() [ 
	 * { 
	 *  resources_type: door/spot resources_type_value;
	 * 	resources_id: sd_id/yc_id resources_name:
	 * 	equiment_id: equiment_type: tractor/tralor
	 *  equiment_number:
	 *  task_number: entry_id :
	 * }
	 * ] resource 下某个 设备 几个Task
	 * 
	 * 按照 equipment_id , resources_id ，resources_type 分类
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月8日
	 */
	@Override
	public DBRow[] executeTaskList(long adid) throws Exception {
		try {
 			DBRow[] returnArray = floorCheckInMgrZr.executeTaskList(adid);
			if (returnArray != null && returnArray.length > 0) {
				for (DBRow temp : returnArray) {
					fixTaskRow(temp);
				}
				return returnArray;
			}
			return null;
		} catch (Exception e) {
			throw new SystemException(e, "loaderTaskList", log);
		}
	}

	/**
	 * 这个Task里面填充数据 主要是resouces_type_value,resources_name , equipment_type_value
	 * 
	 * @param temp
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月9日
	 */
	private void fixTaskRow(DBRow temp) throws Exception {
		try {

			String equipment_type_value = checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(temp.get("equipment_type", 0));
			String resources_type_value = occupyTypeKey.getOccupyTypeKeyName(temp.get("resources_type", 0));
			long resources_id = temp.get("resources_id", 0l);
			String resources_name = temp.get("resources_type", 0) == OccupyTypeKey.DOOR ? getDoorName(resources_id): getSpotName(resources_id);

			temp.add("equipment_type_value", equipment_type_value);
			temp.add("resources_name", resources_name);
			temp.add("resources_type_value", resources_type_value);

		} catch (Exception e) {
			throw new SystemException(e, "fixTaskRow", log);
		}
	}
	
	// >>>>>>>>>>>>>>>>>>Assign Task Start <<<<<<<<<<<<<<<<<<<<<<<<
	private int getEquipmentAssignedTaskCount(long equipment_id) throws Exception{
		try{
			if(equipment_id > 0l){
				return floorCheckInMgrZr.getEquipmentAssignedTaskCount(equipment_id);
			}
			return 0 ;
		}catch(Exception e){
			throw new SystemException(e, "getEquipmentAssignTask", log);
		}
	}
	/**
	 * 通过EntryID Assign Task ， 分组Group by equipment_id , equipment_type {
	 * entry_id: equipment_number: equipment_type: equipment_id: total_task:5 &
	 * task 出于Unprocessed|Processing的 }
	 */
	@Override
	public DBRow[] assginTaskListByEntry(long entry_id,
			AdminLoginBean adminLoginBean) throws NoRecordsException,
			NoPermiessionEntryIdException, CheckInNotFoundException, Exception {
		try {
			DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
			CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);

			DBRow[] rows = floorCheckInMgrZr.getAssignTaskListByEntry(entry_id);
			if (rows != null && rows.length > 0) {
				DBRow[] returnRows = new DBRow[rows.length];
				for (int index = 0, count = rows.length; index < count; index++) {
					DBRow temp = new DBRow();
					temp.add("entry_id", rows[index].get("dlo_id", 0l));
					temp.add("equipment_number",rows[index].getString("equipment_number"));
					temp.add("equipment_id",rows[index].get("equipment_id", 0l));
					temp.add("equipment_type",rows[index].get("equipment_type", 0));
					int resources_type = rows[index].get("resources_type",0) ;
					long resources_id = rows[index].get("resources_id",0l) ;
					int occupy_status = rows[index].get("occupy_status", 0);
					temp.add("resources_type", resources_type);
					temp.add("resources_id", resources_id);
					temp.add("occupy_status", occupy_status);
					if(resources_id > 0l){
						temp.add("resources_name", resources_type == OccupyTypeKey.DOOR ? getDoorName(resources_id) : getSpotName(resources_id));
					}
					temp.add("total_task", rows[index].get("total_task", 0));
					temp.add("rel_type", rows[index].get("rel_type", 0));
					temp.add("assign_task", getEquipmentAssignedTaskCount(rows[index].get("equipment_id", 0l)));

					returnRows[index] = temp;
				}
				return returnRows;
			} else {
				throw new NoRecordsException();
			}
		} catch (NoRecordsException e) {
			throw e;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "assginTaskListByEntry", log);
		}
	}

	/**
	 * 通过EntryID + equipment 去过滤这个Entry当前设备下，还没有Close的任务 　
	 * 1.有可能没有管理员 　+　所以任务压根就没有分配。 
	 * 2.列出所有没有Close的detail ， 只不过有分配过的，那么现实一个人名。没有分配的就没有人名 
	 * 3.如果他重新勾选人，那么就update 原来的任务的表的记录。
	 * 4.没有记录的那么assign 
	 * 5.通过number_stutas 去过滤
	 * (processing + unprocess)
	 * 
	 */
	@Override
	public DBRow assginTaskDetaillByEntryAndEquipment(long entry_id, long equiment_id ,boolean isAndroid ) throws Exception {
		try {
			DBRow result = new DBRow();
			DBRow[] rows = findDetailsHasDoorOrSpot(entry_id, equiment_id,CheckInChildDocumentsStatusTypeKey.getAllStatus());
 			for (DBRow tempRow : rows) {
				List<DBRow> arrayList = (ArrayList<DBRow>) tempRow.get("details", new ArrayList<DBRow>());
				tempRow.remove("details");
				List<DBRow> fixArrayLis = new ArrayList<DBRow>();
				for (DBRow detail : arrayList) {
					DBRow loadR = new DBRow();
					loadR.add("id", detail.get("number", ""));
					loadR.add("dlo_detail_id", detail.get("dlo_detail_id", 0l));
					loadR.add("number_type", detail.get("number_type", ""));
					loadR.add("order_no", detail.get("order_no", ""));
					loadR.add("po_no", detail.get("po_no", ""));
					loadR.add("customer_id", detail.get("customer_id", ""));
					loadR.add("company_id", detail.get("company_id", ""));
					loadR.add("equipment_id", detail.get("equipment_id", 0l));
					loadR.add("number_status", detail.get("number_status", 0));
					loadR.add("execute_user",getWareHouseCheckInExecuteUserNames(detail.get("dlo_detail_id", 0l)).b);
				/*	if (!StringUtil.isBlank(detail.get("order_no", "")) && detail.get("number_type", 0) == ModuleKey.CHECK_IN_PONO) {
						loadR.add("type", 1);
					}
					if (!StringUtil.isBlank(detail.get("po_no", "")) && detail.get("number_type", 0) == ModuleKey.CHECK_IN_ORDER) {
						loadR.add("type", 2);
					}*/
					fixArrayLis.add(loadR);
				}
				Collections.sort(fixArrayLis, new Comparator<DBRow>() {
					@Override
					public int compare(DBRow o1, DBRow o2) {
 						return o1.get("number_status", 0) > o2.get("number_status", 0) ? 1: -1 ;
					}
				});
				tempRow.add("load_list", fixArrayLis.toArray(new DBRow[0]));
				if(!isAndroid){
					DBRow[] schedules = getWareHouseAssignScheduleByEntry(entry_id);
					tempRow.add("schedules", schedules);
				}
			}
			result.add("tree", rows);
			DBRow equipment =  floorCheckInMgrZr.getEquipmentWithResources(equiment_id);
 			DBRow convertEquipment = new DBRow();
			int resources_type  = equipment.get("resources_type", 0);
			long resources_id  = equipment.get("resources_id", 0l);
			if(resources_id > 0l ){
				if(resources_type == OccupyTypeKey.DOOR){
					convertEquipment.add("resources_name", getDoorName(resources_id));
				}else{
					convertEquipment.add("resources_name", getSpotName(resources_id));
				}
			}
			convertEquipment.add("resources_id", resources_id);
			convertEquipment.add("resources_type", resources_type);
			convertEquipment.add("rel_type", equipment.get("rel_type", 0));
			convertEquipment.add("equipment_status", equipment.get("equipment_status", 0));
			convertEquipment.add("equipment_purpose", equipment.get("equipment_purpose", 0));
			convertEquipment.add("occupy_status", equipment.get("occupy_status", 0));

			result.add("equipment", convertEquipment);
			return result;
		} catch (Exception e) {
			throw new SystemException(e,"assginTaskDetaillByEntryAndEquipment", log);
		}
	}
	
	private DBRow[] getWareHouseAssignScheduleByEntry(long entry_id) throws Exception{
		try{
			return floorCheckInMgrZr.getWareHouseAssignScheduleByEntry(entry_id);
			
		}catch(Exception e){
			throw new SystemException(e,"getWareHouseAssignScheduleByEntry", log);
		}
	}
	/**
	 * 返回ids,usernames
	 */
	private HoldDoubleValue<String, String> getWareHouseCheckInExecuteUserNames(
			long dlo_detail_id) throws Exception {
		try {
			 
			StringBuffer userNames = new StringBuffer("");
			StringBuffer userIds = new StringBuffer("");
			DBRow[] userInfos = scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(dlo_detail_id,ModuleKey.CHECK_IN, ProcessKey.CHECK_IN_WAREHOUSE);
			if (userInfos != null && userInfos.length > 0) {
				for (DBRow temp : userInfos) {
					userNames.append(",").append(temp.getString("employe_name"));
					userIds.append(",").append(	temp.get("schedule_execute_id", 0l));
				}
			}
			return new HoldDoubleValue<String, String>(userIds.length() > 0 ? userIds.substring(1) : "",userNames.length() > 0 ? userNames.substring(1) : "");
		} catch (Exception e) {
			throw new SystemException(e, "getWareHouseCheckInExecuteUserNames",log);
		}
	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TaskProcessing
	// Start<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
	private int getIsForgateCloseDetail(DBRow detail ){
		 int is_forget_task =  detail.get("is_forget_task", 0) ;
		 return is_forget_task == 0 ? 0 : 1 ;
	}
	/**
	 * 不管Detail是否Close，那么都是需要计算出来的
	 * close 的时候会删除关联表的记录
	 * 所以门就关联不出来了
	 * 
	 * 添加Equipment的所在的位置
	 * @param arrays
	 * @return
	 * @author zhangrui
	 * @throws Exception 
	 * @Date   2014年12月12日
	 */
	private DBRow[] fixTaskProcessingListByEntryDetailsDatas(DBRow[] arrays) throws Exception{
		Map<HoldThirdValue<Long, Integer, Long>, DBRow> dataMaps = new HashMap<HoldThirdValue<Long,Integer,Long>, DBRow>();
		if(arrays != null && arrays.length > 0){
			for(DBRow temp : arrays){
				int resources_type = (temp.get("resources_type", 0) == 0 ? temp.get("occupancy_type", 0) : temp.get("resources_type", 0));
				long resources_id = (temp.get("resources_id", 0) == 0 ? temp.get("rl_id", 0) : temp.get("resources_id", 0));
				long equipment_id = temp.get("equipment_id", 0);
				
				if(resources_type == 0 || resources_id == 0 || equipment_id == 0){
					//continue ;		//数据有错误
				}
				/*temp.add("resources_type", resources_type);
				temp.add("resources_id", resources_id);
				temp.add("equipment_id", equipment_id);*/
				
				HoldThirdValue<Long, Integer, Long> resourcesAndEquipmentKey = new HoldThirdValue<Long, Integer, Long>(resources_id, resources_type, equipment_id);
				
				DBRow data = dataMaps.get(resourcesAndEquipmentKey);
				data = (data == null ?  new DBRow() : data) ;
				int count = data.get("count", 0);
				count++ ;
				data.add("count", count);
				 
				int insertForateClose = data.get("is_forget_close", 0);			 
				data.add("is_forget_close", getIsForgateCloseDetail(temp) +  insertForateClose ); //计算出当前门下 忘记CLose的数量
				if(checkInMgrZwb.isEntryDetailIsFinish(temp)){
					int finish_task = data.get("finish_task", 0);
					finish_task++ ;
					data.add("finish_task", finish_task);
				}
				
 				dataMaps.put(resourcesAndEquipmentKey, data);
			}
		}
		if(!dataMaps.isEmpty()){
			List<DBRow> array = new ArrayList<DBRow>();
			Iterator<Entry<HoldThirdValue<Long, Integer, Long>, DBRow>> it = dataMaps.entrySet().iterator();
			while(it.hasNext()){
				 Entry<HoldThirdValue<Long, Integer, Long>, DBRow> resourcesAndEquipmentEntry =  it.next() ;
				 DBRow temp = new DBRow();
				 temp.add("resources_id", resourcesAndEquipmentEntry.getKey().a);
				 temp.add("resources_type", resourcesAndEquipmentEntry.getKey().b);
				 temp.add("equipment_id", resourcesAndEquipmentEntry.getKey().c);
				 temp.add("total_task", resourcesAndEquipmentEntry.getValue().get("count", 0));
				 temp.add("finish_task", resourcesAndEquipmentEntry.getValue().get("finish_task", 0));
				 temp.add("is_forget_close", resourcesAndEquipmentEntry.getValue().get("is_forget_close", 0));
				 DBRow equipment = getEquipment(resourcesAndEquipmentEntry.getKey().c) ;
				 temp.add("equipment_type", equipment.get("equipment_type", 0));
				 temp.add("equipment_number", equipment.getString("equipment_number"));
				 temp.add("rel_type", equipment.getString("rel_type"));

				 array.add(temp);
			}
			return array.toArray(new DBRow[0]);
		}
		return new DBRow[0] ;
	}
	/**
	 * 查询某个EntryId下面所有的按照 （资源+设备） 分类的Task 不管Task是否已经关闭了
	 * 不管是否关闭，然后都可以计算出来
	 * 如果有forgate_close 的数据那么传递这个参数给前段 is_forgate_close : 1
	 */
	@Override
	public DBRow[] taskProcessingListByEntry(long entry_id,
			AdminLoginBean adminLoginBean)
			throws NoPermiessionEntryIdException,CheckInEntryIsLeftException , CheckInNotFoundException,
			Exception {
		try {

			DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
			CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
			boolean isLeft = isCheckEntryIsLeft(entry_id);
			// 所有的设备已经离开 && 没有 Forget Close Task
			if(isLeft){
				int forgetTasks = floorCheckInMgrZr.countForgetDetails(entry_id);
				if(forgetTasks <= 0){
					throw  new CheckInEntryIsLeftException();
				}
			}
			DBRow[] returnArrays = floorSpaceResourcesRelationMgr.getTaskByEntryIdWithOccpuyStatus(entry_id, 0, 0l);
			//然后按照 spot ,door ,equipment_id 分组
			returnArrays = fixTaskProcessingListByEntryDetailsDatas(returnArrays);
			
			if (returnArrays != null && returnArrays.length > 0) {
				for (DBRow temp : returnArrays) {
					fixTaskRow(temp);
				}
			}
			
			return returnArrays;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch(CheckInEntryIsLeftException e){
			throw e;
		}catch (Exception e) {
			throw new SystemException(e, "taskProcessingListByEntry", log);
		}
	}
    /**
     * 
     */
	@Override
	public DBRow[] entryEquipmentWithResouces(long entry_id) throws Exception {
		try{
			DBRow[] returnDatas = floorCheckInMgrZr.getEquipmentByEntryAndTotalTask(entry_id);
			return fixEquipmentWithResouces(returnDatas);
		}catch(Exception e){
			throw new SystemException(e, "entryEquipmentWithResouces", log);
		}
	}
	
	private DBRow[] fixEquipmentWithResouces(DBRow[] returnDatas) throws Exception{
		if(returnDatas != null && returnDatas.length > 0){
			DBRow[] fixDatas = new DBRow[returnDatas.length];
			for(int index = 0 , count = fixDatas.length ; index < count ; index++ ){
				DBRow temp = returnDatas[index];
 				long resources_id = temp.get("resources_id", 0l);
				int resources_type = temp.get("resources_type", 0);
				int equipment_type = temp.get("equipment_type", 0);
				
 				String resouces_type_value = occupyTypeKey.getOccupyTypeKeyName(resources_type);
		
				
				DBRow insertRow = new DBRow();
				insertRow.add("resources_id", resources_id);
				insertRow.add("resources_type", resources_type);
				insertRow.add("resources_type_value", resouces_type_value);
				appendResoucesName(insertRow);
				insertRow.add("occupy_status", temp.get("occupy_status", 0));
				
				insertRow.add("equipment_status", temp.get("equipment_status", 0));
				insertRow.add("rel_type", temp.get("rel_type", 0));
				insertRow.add("equipment_id", temp.get("equipment_id", 0l));
				insertRow.add("equipment_type", equipment_type);
				insertRow.add("equipment_number", temp.getString("equipment_number"));
				insertRow.add("equipment_type_value",checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment_type) );
				
				insertRow.add("total_task", temp.get("total_task", 0));
				
				fixDatas[index] = insertRow ;
			}
			return fixDatas ;
		}
		return new DBRow[]{};
	}
	
	
	private int getSortNumberStatus(DBRow detail){
		 int number = detail.get("number_status", 0);
		 switch (number) {
		 	case -1: number = 0 ; break;
		 	case CheckInChildDocumentsStatusTypeKey.UNPROCESS: number = 0 ; break;
		 	case CheckInChildDocumentsStatusTypeKey.PROCESSING: number = 0 ; break;
		 	case CheckInChildDocumentsStatusTypeKey.CLOSE: number = 1 ; break;
		 	case CheckInChildDocumentsStatusTypeKey.EXCEPTION: number = 1 ; break;
		 	case CheckInChildDocumentsStatusTypeKey.PARTIALLY: number = 1 ; break;
 

 		}
		return number ;
	}
	
	private HoldDoubleValue<String, String> getTaskCloseUser(
			long dlo_detail_id) throws Exception {
		try {
			 
			StringBuffer userNames = new StringBuffer("");
			StringBuffer userIds = new StringBuffer("");
			DBRow[] userInfos =  floorCheckInMgrZr.getCheckInCloseTaskUserInfos(dlo_detail_id);
			if (userInfos != null && userInfos.length > 0) {
				for (DBRow temp : userInfos) {
					userNames.append(",").append(temp.getString("employe_name"));
					userIds.append(",").append(	temp.get("adod", 0l));
				}
			}
			return new HoldDoubleValue<String, String>(userIds.length() > 0 ? userIds.substring(1) : "",userNames.length() > 0 ? userNames.substring(1) : "");
		} catch (Exception e) {
			throw new SystemException(e, "getTaskCloseUser",log);
		}
	} 
	/**
	 * @param entry_id
	 * @param equipment_id
	 * @param resources_type
	 * @param resources_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月9日
	 * 
	 *       获取任务的列表，以detail表中的数据为准，如果是已经close的那么也显示出来
	 * 
	 *       如果是还没有分配 || 分配了人但是不是当前的这个人 -> take over(只能)
	 * 
	 *       如果是分配了人那么也可以显示一下当前的分配给谁来执行这个任务
	 *       
	 *       door or spot 的上面的数据 这次都应该查询出来，只不过在 Task Processing 的界面应该提示一下,首先移动到门
	 *       
	 *       Close了的Task 应该显示出来是谁close的
	 *       
	 *       
	 *       
	 *       张睿添加 takeover flag 2015-02-04
	 *       
	 */
	private static final int NeedTakeOver = 1 ;			//需要Take Over
	private static final int UnNeedTakeOver = 0 ;		//不需要Take Over
	@Override
	public DBRow[] getTaskProcessingDetailByEntryAndEquipmentAndResouces(
			long entry_id, long equipment_id, int resources_type,
			long resources_id, AdminLoginBean adminLoginBean) throws Exception {
		try {
			DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
			CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
			long adid = adminLoginBean.getAdid();

			DBRow[] result = floorCheckInMgrZr.getDoorDetailsWithCloseBy(entry_id, resources_id, equipment_id,resources_type);

			if (result != null && result.length > 0) {
				DBRow[] returnArrays = new DBRow[result.length];
				for (int index = 0, count = result.length; index < count; index++) {
					DBRow temp = result[index];
					long dlo_detail_id = temp.get("dlo_detail_id", 0l);
					DBRow insertTemp = new DBRow();
					int number_status = temp.get("number_status", 0);
					insertTemp.add("number_status",number_status);
					insertTemp.add("takeoverflag", UnNeedTakeOver);
					if(number_status ==  CheckInChildDocumentsStatusTypeKey.UNPROCESS 
							|| number_status ==  CheckInChildDocumentsStatusTypeKey.PROCESSING ){
						HoldDoubleValue<String, String> userIdsAndUserNames = getWareHouseCheckInExecuteUserNames(dlo_detail_id);
						if (!StringUtil.isNull(userIdsAndUserNames.a)) {
							insertTemp.add("executer", userIdsAndUserNames.b);
							if (userIdsAndUserNames.a.indexOf(String.valueOf(adid)) == -1) {
								insertTemp.add("takeoverflag", NeedTakeOver); // 当前没有分配给他
							}
						} else {
							insertTemp.add("executer", "Need Assign");
							insertTemp.add("takeoverflag", NeedTakeOver); // 还没有分配
						}
					}else{
						insertTemp.add("executer", getTaskCloseUser(dlo_detail_id).b);
					}
					insertTemp.add("receipt_no", temp.getString("receipt_no"));
					insertTemp.add("number", temp.getString("number"));
					insertTemp.add("dlo_detail_id", dlo_detail_id);
					insertTemp.add("number_type", temp.get("number_type", 0l));
					insertTemp.add("customer_id", temp.getString("customer_id"));
					insertTemp.add("company_id", temp.getString("company_id"));
					insertTemp.add("freight_term", temp.getString("freight_term"));
					insertTemp.add("lr_id", temp.get("lr_id", 0l));
					insertTemp.add("ic_id", temp.get("ic_id", 0l));
					insertTemp.add("staging_area_id", temp.getString("staging_area_id"));
					insertTemp.add("account_id", temp.getString("account_id"));
					insertTemp.add("supplier_id", temp.getString("supplier_id"));
					insertTemp.add("freight_term", temp.getString("freight_term"));
					insertTemp.add("is_forget_task", temp.get("is_forget_task",0));
					//查询window note 内容
					String note = "";
					DBRow[] notices = checkInMgrZwb.windowFindSchedule(dlo_detail_id, new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
					if(notices.length > 0){
			 			String[] notes = notices[0].getString("schedule_detail").split("Note :");
			 			if(notes.length > 1){
			 				note = notes[1];
			 				if(note.startsWith(" ")){
			 					note = note.substring(1);
			 				}
			 			}
					}
					insertTemp.add("note",note);
					
					returnArrays[index] = insertTemp;
				}
 				Arrays.sort(returnArrays, new Comparator<DBRow>() {
					@Override
					public int compare(DBRow o1, DBRow o2) {
						
						int number_status1 =  getSortNumberStatus(o1);
						int number_status2 =  getSortNumberStatus(o2);

 						return number_status1 > number_status2  ? 1: (number_status2 == number_status1 ? 0 : -1);
					}
				});
				return returnArrays;
			}
			return null;
		} catch (NoPermiessionEntryIdException e) {
			throw e;
		} catch (CheckInNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e,"getTaskProcessingDetailByEntryAndEquipmentAndResouces",log);
		}
	}

	/**
	 * 通过Entry搜索 然后assign任务 有可能重新添加任务 有可能是修改原来的任务到新的人 execute_user_ids到时候只能是一个人
	 * HoldDoubleValue<Long, Integer> Long detail_id , interger 1: 表示是update
	 * task （表示的是修改任务的执行人Single Select(执行人)）, 0 : 表示的是新的assign任务
	 * 
	 * 应该查询这个Task 是否是已经分配了人了 ，分配了人那么就是Update ， 其余的就是添加
	 */
	@Override
	public void assignTaskByEntry(String execute_user_ids,
			List<HoldDoubleValue<Long, Integer>> datas, AdminLoginBean adminLoginBean)
			throws EntryTaskHasFinishException , Exception {
		try {
			long adid = adminLoginBean.getAdid();
			if (datas != null && datas.size() > 0&& !StringUtil.isNull(execute_user_ids)) {
				
				List<DBRow> detailRows = new ArrayList<DBRow>();
				long entry_id = 0l;
				for (HoldDoubleValue<Long, Integer> temp : datas) {
					long dlo_detail_id = temp.a;
					int isupdate = temp.b;
					boolean isAssignToWorker = floorCheckInMgrZr.isEntryDetailHasAssignWarehouseWorkTask(dlo_detail_id);
					DBRow detailRow = floorCheckInMgrZr.getEntryDetailRow(dlo_detail_id);
					detailRows.add(detailRow);
					if(checkInMgrZwb.isEntryDetailIsFinish(detailRow)){
						throw new EntryTaskHasFinishException();
					}
					entry_id = detailRow.get("dlo_id", 0l);
					if (!isAssignToWorker) { // assign getEntryDetailRow
						assignWareHouseTask(detailRow, execute_user_ids, adid);
 					}else { // update
 						finishSupverVisorTask(adid, detailRow);
						// 通过detail查询对应的scheduleId
						DBRow[] schedule = scheduleMgrZr.getScheduleBy(dlo_detail_id,  ProcessKey.CHECK_IN_WAREHOUSE , ModuleKey.CHECK_IN );// getScheduleByAssociate(dlo_detail_id, ModuleKey.CHECK_IN,ProcessKey.CHECK_IN_WAREHOUSE);
						if(schedule != null && schedule.length > 0){
							for(DBRow row : schedule){
								DBRow updateRow = new DBRow();
								updateRow.add("schedule_execute_id", execute_user_ids);
								scheduleMgrZr.simpleUpdateScheduleSub(row.get("schedule_id", 0l), updateRow);
							}
						}
					}
				}
				assignTaskLogs(detailRows, entry_id, adid,execute_user_ids); //添加Assign的日志
			}

		}catch(EntryTaskHasFinishException e){
			throw  e ;
		}catch (Exception e) {
			throw new SystemException(e, "singleAssignTask", log);
		}
	}
	/**
	 * Assign  Task的时候记录日志
	 * @param ararys
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月17日
	 */
	private void assignTaskLogs(List<DBRow> ararys , long entry_id ,long adid , String adids  ) throws Exception{
		try{
			StringBuffer logs = new StringBuffer() ;
			String userNames = getOperations(adids);
			for(DBRow row : ararys){
				logs.append(",");
				String numberTypeValue = moduleKey.getModuleName(row.get("number_type", 0));
				logs.append(numberTypeValue).append(":").append(row.getString("number"));
			}
			 logs.append(",Loader:").append(userNames);
			 checkInMgrZwb.addCheckInLog(adid,
					"Android Assign Task ", entry_id,
					CheckInLogTypeKey.AndroidAssignTask, DateUtil.NowStr(),
					logs.toString().substring(1));
		}catch(Exception e){
			throw new SystemException(e,"assignTaskLogs",log);
		}
	}
	/**
	 * 选择几个details然后开始工作 1.当前设备DockCheckIn ， 把设备移动到当前的门上 ，
	 * 如果设备是LiveLoad的那么把车头也移动到这个门上 2.如果当前的设备有DockCheckIn的时间，那么就不要更新这个时间了
	 * 3.开始这些Details，写入handle_time , processing 这些details 4.details 在资源表中占用资源
	 * 5.记录设备 和 details 的占用日志
	 * 
	 * 
	 * 
	 * 
	 * @param equipment_id
	 * @param entry_id
	 * @param details
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	@Override
	public void taskProcessingStartDetails(long equipment_id, long entry_id,
			List<HoldDoubleValue<Long, String>> details, long resources_id, long adid,int resources_type)
			throws Exception {
		try {
			if (details != null && details.size() > 0) {
				DBRow equiment = getEquipment(equipment_id);
				for (HoldDoubleValue<Long, String> temp : details) {
					taskProcessingStartHandDetails(temp.a, resources_id, adid,entry_id,resources_type,equiment);
				}
				if(equiment != null && !isEquipmentCheckOut(equiment)){ //设备没有离开 才会处理设备
					taskProcessingStartHandEquipmentOccupied(equiment, resources_id,adid, entry_id,resources_type);
					//更新主表的warehouse_check_in_time(只能更新一次)，warehouse_check_in_operator，warehouse_check_in_operate_time
					handEntryCheckInMainWarehouseCheckIn(entry_id, adid);
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "taskProcessingStartDetails", log);
		}
	}
	/**
	 * 停留或者是离开的时候，写回WareHouseCheckOutTime
	 * @param entry_id
	 * @param adid
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月18日
	 */
	private void handEntryCheckInMainWarehouseCheckOut(long entry_id , long adid) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("warehouse_check_out_time", DateUtil.NowStr());
			updateRow.add("warehouse_check_out_operator", adid);
 			floorCheckInMgrZwb.modCheckIn(entry_id, updateRow);
		}catch(Exception e){
			throw new SystemException(e, "handEntryCheckInMainWarehouseCheckOut", log);
		}
	}
	/**
	 * 更新warehouse_check_in_time, 只能更新一次
	 * warehouse_check_in_operate_time 可以更新henduoci
	 * @param entry_id
	 * @param adid
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月17日
	 */
	private void handEntryCheckInMainWarehouseCheckIn(long entry_id , long adid) throws Exception{
		try{
			DBRow entry = floorCheckInMgrZwb.getEntryIdById(entry_id);
			DBRow updateRow = new DBRow();
			if(StringUtil.isNull(entry.getString("warehouse_check_in_time"))){
				updateRow.add("warehouse_check_in_time", DateUtil.NowStr());
			}
			updateRow.add("warehouse_check_in_operate_time", DateUtil.NowStr());
			updateRow.add("warehouse_check_in_operator", adid);
 			floorCheckInMgrZwb.modCheckIn(entry_id, updateRow);

		}catch(Exception e){
			throw new SystemException(e, "handEntryCheckInMainWarehouseCheckIn", log);
		}
	}
 
	/**
	 * 
	 * 1.当前设备DockCheckIn ， 把设备移动到当前的门上 ， 如果设备是LiveLoad的那么把车头也移动到这个门上
	 * 2.如果当前的设备有DockCheckIn的时间，那么就不要更新这个时间了
	 * 
	 * tractor_status 车头(如果是车头已经走了 Left 了，那么不更新了) status 车尾 每次都更新 ? 不用判断是否已经离开
	 * 
	 * @param equipment_id
	 * @param sd_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	private void taskProcessingStartHandEquipmentOccupied(DBRow equiment,long resources_id, long adid, long entry_id,int resources_type) throws Exception {
		try {
				long equipment_id = equiment.get("equipment_id", 0l);
				String check_in_warehouse_time = equiment.getString("check_in_warehouse_time");
				DBRow updateRow = new DBRow();
				if (StringUtil.isNull(check_in_warehouse_time)) {
					updateRow.add("check_in_warehouse_time", DateUtil.NowStr());
				}
				int equipment_status = CheckInMainDocumentsStatusTypeKey.PROCESSING;
				if (equiment.get("equipment_type", 0) == EquipmentTypeKey.Tractor 
						&& equiment.get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEFT) { // 车头
					updateRow.add("equipment_status", equipment_status);
					ymsMgrAPI.updateEquipment(equipment_id, updateRow);
				}
				if (equiment.get("equipment_type", 0) == EquipmentTypeKey.Container) {// 车尾
					updateRow.add("equipment_status", equipment_status);
					ymsMgrAPI.updateEquipment(equipment_id, updateRow);
					equipmentOccuipedMoveLiveTractor(equiment, adid, resources_id,entry_id,CheckInLogTypeKey.AndroidStartTask,resources_type); // 如果是LiveLoad ， 那么有车头的情况也是应该把只成占用 +
										// processing
				}
				// 设备操作资源
				logAddTaskOrEqOperationSpaceLog(resources_type, resources_id,
						SpaceRelationTypeKey.Equipment, equipment_id, adid,
						entry_id, CheckInLogTypeKey.AndroidStartTask,
						resources_type == OccupyTypeKey.DOOR ? LogEquipmentOccuipedDoor : LogEquipmentOccuipedSpot, OccupyStatusTypeKey.OCUPIED);
				
				if(resources_type == OccupyTypeKey.DOOR){
					equipmentOperationDoorSpace(entry_id, adid, equipment_id,resources_id, OccupyStatusTypeKey.OCUPIED);
				}else{
					equipmentOperationSpotSpace(entry_id, adid, equipment_id,resources_id, OccupyStatusTypeKey.OCUPIED);
				}

		} catch (Exception e) {
			throw new SystemException(e,"taskProcessingStartHandEquipmentOccupied", log);
		}
	}

	/**
	 * 3.开始这些Details，写入handle_time , processing 这些details 4.details 在资源表中占用资源
	 * 
	 * @param detail_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	private void taskProcessingStartHandDetails(long detail_id, long resources_id,long adid, long entry_id , int resources_type , DBRow equiment) throws Exception {
		try {
			DBRow upDetailrow = new DBRow();
			upDetailrow.add("number_status",CheckInChildDocumentsStatusTypeKey.PROCESSING); // 更新子单据状态为处理中
			upDetailrow.add("handle_time", DateUtil.NowStr());
			floorCheckInMgrZwb.updateDetailByIsExist(detail_id, upDetailrow);				//task 表处理中
			
			//同时写入schedule_id 的开始时间
			DBRow updateScheduleRow = new DBRow();
			updateScheduleRow.add("start_time", DateUtil.NowStr());
			scheduleMgrZr.simpleUpdateScheduleBy(detail_id, ProcessKey.CHECK_IN_WAREHOUSE, ModuleKey.CHECK_IN, updateScheduleRow);
			
			if(!isEquipmentCheckOut(equiment)){	//设备离开了。就不要更新
				
				logAddTaskOrEqOperationSpaceLog(resources_type, resources_id,								//日志
						SpaceRelationTypeKey.Task, detail_id, adid, entry_id,
						CheckInLogTypeKey.AndroidStartTask,
						resources_type == OccupyTypeKey.DOOR ? LogTaskOccuipedDoor : LogTaskOccuipedSpot
						,OccupyStatusTypeKey.OCUPIED);
			
				taskOperationResourcesSpace(entry_id, adid, detail_id, resources_id, OccupyStatusTypeKey.OCUPIED,resources_type);	//占用
			}else{
				//记录一个日志
				String note =  "Start Task";
				String data =  "Forget Task Start," + getLogDataOfRelation(SpaceRelationTypeKey.Task, detail_id) ;
				
				checkInMgrZwb.addCheckInLog(adid, note, entry_id,
						CheckInLogTypeKey.AndroidStartTask, DateUtil.NowStr(), data);

			}
		} catch (Exception e) {
			throw new SystemException(e, "taskProcessingStartHandDetails", log);
		}
	}

	/**
	 * 关闭某个单据(Detail) 1.写入Close At & finish Time 2.释放detail占用的资源
	 * 
	 * 
	 * @param dlo_detail_id
	 * @param entry_id
	 * @param sd_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	@Override
	public int taskProcessingCloseDetail(DBRow data,
			AdminLoginBean adminLoggerBean) throws Exception {
		try {
			String detail_ids = data.getString("detail_id");
			String filePath = data.getString("filePath");
			long adid = adminLoggerBean.getAdid();
			long entry_id = data.get("entry_id", 0l);
			long rl_id = data.get("resources_id", 0l);
 			long equipment_id = data.get("equipment_id", 0l);
 			int resourcesType = data.get("resources_type", 0);
 			String scan_numbers = data.get("scan_numer", "");
 			int closefrom = data.get("closefrom", 0);   //1,原来的Load ,后面Load Exception ，也是走统一的接口
 		
 			// 处理图片
			//androidHandFile(filePath, adid, entry_id,FileWithCheckInClassKey.PhotoTaskProcessing);
			
			//文件上传到文件服务器里面   --by chenchen
    		DBRow r = new DBRow();
			r.add("file_with_id", entry_id);
			r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
			r.add("file_with_class", FileWithCheckInClassKey.PhotoTaskProcessing);
    		fileMgrZr.uploadDirToFileServ(filePath, r, data.getString("session_id"), 
    				checkInFileClassKey.getFileWithTypeId(FileWithCheckInClassKey.PhotoTaskProcessing)+ "_"+entry_id+"_");
 			
 			//验证 task为smallParcel时 order没有扫描完不可以关闭任务  wfh
 			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(detail_ids);
 			if(task.length == 1){
 				if(task[0].get("number_type", 0) == ModuleKey.SMALL_PARCEL){
 					//清掉其他carrier扫描的order
 					checkInMgrWfh.freshOtherCarrierOrder(task[0].get("small_parcel_carrier_id", 0L), Long.parseLong(detail_ids));
 					//关闭扫描的order
 					checkInMgrWfh.scanPalletOrTrackNo(scan_numbers, Long.parseLong(detail_ids), adid);
 					
 					//验证 task为smallParcel时 order没有扫描完不可以关闭任务  wfh
 					DBRow[] scanOrder = floorCheckInMgrWfh.findWMSLoadOrder(String.valueOf(task[0].get("dlo_detail_id", 0L)));
 					boolean closeFlag = true;
 					String mess = "";
 					//验证order是否都装完了 都装完了才可以close
 					for (DBRow order : scanOrder) {
 						DBRow[] pallets = floorCheckInMgrWfh.findwmsLoadOrderPallet(order.get("wms_order_id", 0L));
 						for (DBRow pallet : pallets) {
 							if(pallet.get("scan_adid", 0l) == 0){
 								closeFlag = false;
 								if(!mess.contains(order.get("order_numbers", "")))
 									mess += "Order: "+order.get("order_numbers", "")+"\n";
 							}
 						}
 					}
 					
 					if(!closeFlag && scanOrder.length>0){
 						mess += "Please scan all pallet!";
 						throw new SmallParcelNotCloseOrderException(mess);
 					}
 				}
 			}
 			if(closefrom == 1){
 				DBRow loginRow = new DBRow();
 	 			data.add("close_type", data.get("number_status", 0));
 	 			data.add("dlo_detail_id", detail_ids);
 	 			loginRow.add("adid", adminLoggerBean.getAdid());
 	 			loginRow.add("ps_id", adminLoggerBean.getPs_id());
 	 			loginRow.add("adgid", adminLoggerBean.getAdgid());
 				return checkInMgrZwb.exceptionLoad(data, adminLoggerBean);
 			}
 			DBRow equipment =   getEquipment(equipment_id);
 			if (!StringUtil.isNull(detail_ids)) {
				long[] detailIds = convertStringArrayToLongArray(detail_ids.split(","));
				for (long detail_id : detailIds) {
					if(detail_id <= 0l){
						continue ;
					}
					String note = data.getString("note");
					DBRow row = new DBRow();
					row.add("number_status", data.get("number_status", 0l));
					row.add("note", note);
					DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(detail_id);
					finishDetail(detailRow, rl_id, row,adminLoggerBean.getAdid(),resourcesType,equipment );
					
				}
				return checkInMgrZwb.closeDetailReturnFlag(equipment, entry_id, detailIds[detailIds.length-1]);
			}
			
			return CheckInMgrZwb.LoadCloseNoifyNormal ;
		}catch (SmallParcelNotCloseOrderException e){
			throw e;
		}catch (Exception e) {
			throw new SystemException(e, "taskProcessingStartHandDetails", log);
		}
	}

	/**
	 * android处理图片
	 * 
	 * @param filePath
	 * @param adid
	 * @param entry_id
	 * @param file_with_class
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	/*
	private void androidHandFile(String filePath, long adid, long entry_id,
			int file_with_class) throws Exception {
		if (!StringUtil.isBlank(filePath)) {
			String nowTime = DateUtil.NowStr();
			String fileDescBasePath = Environment.getHome()+ "upload/check_in/";
			File dir = new File(filePath);
			if (dir.isDirectory()) {
				File[] files = dir.listFiles();
				for (File file : files) {
					// String fileName = StringUtil.getFileName(file.getName());
					checkInMgrZwb.addFile(checkInFileClassKey.getFileWithTypeId(file_with_class)+ "_"+ entry_id + "_" + file.getName(),
							entry_id, FileWithTypeKey.OCCUPANCY_MAIN,
							file_with_class, adid, nowTime);
					// FileUtil.copyProductFile(file, fileDescBasePath);
					FileUtil.checkInAndroidCopyFile(file,checkInFileClassKey.getFileWithTypeId(file_with_class)+ "_"	+ entry_id + "_", fileDescBasePath);
				}
			}
		}
	}
*/
	@Override
	public DBRow[] getCheckInFile(long entry_id, int file_with_class)
			throws Exception {
		try {
			return fileMgrZr.androidGetFileUri(entry_id, ModuleKey.CHECK_IN,file_with_class,0l,null);
		} catch (Exception e) {
			throw new SystemException(e, "getCheckInFile", log);
		}
	}

	/**
	 * 把details(Task)移动到选择的一个门上, 记录日志
	 * 把资源表中的当前Task的记录，中的resource_type,resouce_id， Update 。
	 *  不会把Task 关联 记录保存为【占用】 他需要在Assign的时候我才会记录为【占用】 
	 *  不能移动 已经完成的Task， 只能移动出于Unprocess +Processing 中的数据。(查询数据库) 
	 *  我只是更新，所以原来Processing 是出于占用状态那么现在还是出于占用状态。
	 * 
	 * 
	 * 
	 * task processing 改门
	 * 	如果有没有完成的任务，那么把这些任务都该到当前的选择的门上。
	 *  同时设备也移动到当前的门上。
	 * 	如果设备已经离开了，那么就不用更新了。
	 * 
	 */
	@Override
	public void taskProcessingMoveTaskToDoor(long entry_id, long equipment_id,
			long sd_id, int moved_resource_type, long move_resouce_id,
			AdminLoginBean adminLoggerBean) throws Exception {
		try {
			// 把某些details移动到当前选择的门上
			StringBuffer noteData = new StringBuffer(); 
			//
			DBRow[] details = ymsMgrAPI.getRelationByResouceAndEntryIdAndEquipmentId(entry_id,move_resouce_id, moved_resource_type, equipment_id,SpaceRelationTypeKey.Task);
			if (details != null && details.length > 0) {
				noteData.append("Change Task To Door:"+getDoorName(sd_id)+",");
				DBRow updateRow = new DBRow();
				updateRow.add("resources_id", sd_id);
				updateRow.add("resources_type", OccupyTypeKey.DOOR);
				boolean isMoveTask = false ;
				for (DBRow temp : details) {
					if (!checkInMgrZwb.isEntryDetailIsFinish(temp)) {
						isMoveTask = true ;
						noteData.append(moduleKey.getModuleName(temp.get("number_type",0))).append(":");
						noteData.append(temp.getString("number") + ",");
						long srr_id = temp.get("srr_id", 0l);
						ymsMgrAPI.updateSpaceResourcesRelation(srr_id,updateRow);
					}
					
				}
				//只有move了Task才会记录日志
				if(isMoveTask){
					String operator_time = DateUtil.NowStr();
					checkInMgrZwb.addCheckInLog(adminLoggerBean.getAdid(),
							"Change Task To Door", entry_id,
							CheckInLogTypeKey.AndroidTaskProcessing, operator_time,
							noteData.toString());
				}
			}
			
			// 把当前的Equipment占用当前的资源 + liveLoad的车尾(那么把车头移动到一起)
			DBRow equipment = getEquipment(equipment_id) ;
			if(equipment.get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEFT ){
				equipmentOperationDoorSpace(entry_id, adminLoggerBean.getAdid(),equipment_id, sd_id, OccupyStatusTypeKey.OCUPIED);
			}
			changeToDoorMoveLiveTractor(equipment,adminLoggerBean.getAdid(), sd_id, entry_id,CheckInLogTypeKey.AndroidTaskProcessing,OccupyTypeKey.DOOR);
		} catch (Exception e) {
			throw new SystemException(e, "taskProcessingMoveTaskToDoor", log);
		}
	}
	/**
	 * change to door 的时候改变设备
	 * 如果是设备已经Left，LEAVING了就不用改变了
	 * 设备已经LEAVING 或者是
	 * @param equipment
	 * @param adid
	 * @param resources_id
	 * @param entry_id
	 * @param checkInLogType
	 * @param resources_type
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	private void changeToDoorMoveLiveTractor(DBRow equipment, long adid,
			long resources_id, long entry_id , int checkInLogType,int resources_type ) throws Exception{
		try{
			if (equipment != null && equipment.get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.LIVE) {
				// 找到这个Entry对应的车头，然后占用当前 的目标
				long check_in_entry_id = equipment.get("check_in_entry_id", 0l);
				if (check_in_entry_id != 0l) {
					DBRow tractor = ymsMgrAPI.getEntryTractor(check_in_entry_id);
					if (tractor != null && tractor.get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEFT && 
							tractor.get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEAVING) {
					 
						logAddTaskOrEqOperationSpaceLog(resources_type,
								resources_id, SpaceRelationTypeKey.Equipment,
								tractor.get("equipment_id", 0l), adid,
								entry_id,
								checkInLogType,
								resources_type ==  OccupyTypeKey.DOOR ?  LogEquipmentOccuipedDoor :  LogEquipmentOccuipedSpot
								,OccupyStatusTypeKey.OCUPIED);
						
						
						
						//操作资源
						if(resources_type ==  OccupyTypeKey.DOOR ){
							equipmentOperationDoorSpace(check_in_entry_id, adid,
									tractor.get("equipment_id", 0l), resources_id,
									OccupyStatusTypeKey.OCUPIED);
						}else{
							equipmentOperationSpotSpace(check_in_entry_id, adid,
									tractor.get("equipment_id", 0l), resources_id,
									OccupyStatusTypeKey.OCUPIED);
						}
					}
				}
			}
		
		}catch(Exception e){
			throw new SystemException(e, "changeToDoorMoveLiveTractor", log);
		}
	}
	/**
	 * taskProcessing equipment checkin
	 * 
	 * @param equipment_id
	 * @param sd_id
	 * @param adid
	 * @param entry_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月10日
	 */
	/*
	 * @Override public void taskProcessingEquipmentCheckInSubmit(long
	 * equipment_id , long sd_id , long adid , long entry_id , String in_seal
	 * ,String out_seal , String filePath) throws Exception{ try{
	 * taskProcessingStartHandEquipmentOccupied(equipment_id, sd_id, adid,
	 * entry_id); DBRow updateRow = new DBRow();
	 * if(!StringUtil.isNull(in_seal)){ updateRow.add("seal_delivery", in_seal);
	 * } if(!StringUtil.isNull(out_seal)){ updateRow.add("seal_pick_up",
	 * out_seal); } if(!updateRow.isEmpty()){ //update equipment
	 * ymsMgrAPI.updateEquipment(equipment_id, updateRow); } //处理图片
	 * androidHandFile(filePath, adid, entry_id,
	 * FileWithCheckInClassKey.PhotoDockCheckIn);
	 * 
	 * }catch(Exception e){ throw new
	 * SystemException(e,"taskProcessingEquipmentCheckInSubmit",log); } }
	 * 
	 * @Override public DBRow taskProcessingEquipmentCheckIn(long equipment_id,
	 * long entry_id) throws EquipmentNotFindException , Exception { try{ DBRow
	 * result = new DBRow(); DBRow eq = getEquipment(equipment_id); if(eq !=
	 * null){ result.add("in_seal", eq.getString("seal_delivery"));
	 * result.add("out_seal", eq.getString("seal_pick_up")); }else{ throw new
	 * EquipmentNotFindException(); } result.add("files",
	 * getCheckInFile(entry_id, FileWithCheckInClassKey.PhotoDockCheckIn));
	 * return result ; }catch(EquipmentNotFindException e){ throw e ;
	 * }catch(Exception e){ throw new
	 * SystemException(e,"taskProcessingEquipmentCheckIn",log); } }
	 */
	@Override
	public DBRow getEquipmentSeals(long equipment_id) throws Exception {
		try {
			DBRow result = new DBRow();
			DBRow eq = getEquipment(equipment_id);
			if (eq != null) {
				result.add("in_seal", eq.getString("seal_delivery"));
				result.add("out_seal", eq.getString("seal_pick_up"));
				result.add("equipment_status", eq.get("equipment_status", 0));
				result.add("rel_type", eq.get("rel_type", 0));
				result.add("equipment_number", eq.getString("equipment_number"));
			}
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "getEquipmentSeals", log);
		}
	}
	
	@Override
	public boolean isEquipmentCheckOut(long equipment_id) throws Exception {
		try{
			return isEquipmentCheckOut(getEquipment(equipment_id));
		}catch(Exception e){
			throw new SystemException(e, "isEquipmentCheckOut", log);
		}
 	}
	@Override
	public boolean isEquipmentCheckOut(DBRow equipment) {
		if (/*!StringUtil.isNull(equipment.getString("check_out_time"))
				|| equipment.get("check_out_entry_id", 0l) > 0l
				||*/ equipment.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.LEFT) {
			return true;
		}
		return false;
	}

	/**
	 * 检查当前设备是否还有Task没有关闭 ? 有(当前的设备还有其他的Task)那么就是还不能输入Seal 处理图片
	 * 【注意】 session_id没有传进来，无法上传文件，因为这个方法没人用，暂时不处理，谁要用记得要处理一下   ---chenchen
	 */
	@Override
	public int taskProcessingFinish(long equipment_id, long entry_id,
			long sd_id, String filePath, long adid)
			throws EquipmentHadOutException, Exception {
		try {
			//androidHandFile(filePath, adid, entry_id,FileWithCheckInClassKey.PhotoTaskProcessing);
			/* 
			//文件上传到文件服务器里面   --by chenchen
    		DBRow r = new DBRow();
			r.add("file_with_id", entry_id);
			r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
			r.add("file_with_class", FileWithCheckInClassKey.PhotoDockClose);
    		fileMgrZr.uploadDirToFileServ(filePath, r, data.getString("session_id"), 
    				checkInFileClassKey.getFileWithTypeId(FileWithCheckInClassKey.PhotoDockClose)+ entry_id+"_");
    		*/
			// 检查设备是否已经离开了
			DBRow equipment = ymsMgrAPI.getDetailEquipment(equipment_id);
			if (isEquipmentCheckOut(equipment)) {
				throw new EquipmentHadOutException();
			}

			int countNotFinish = floorCheckInMgrZr.countEquipmentAtDoorNotFinishTask(equipment_id, entry_id);
			//> 0 表示还不是最后一个
			return countNotFinish;
		} catch (EquipmentHadOutException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "taskProcessingFinish", log);
		}
	}

	/**
	 * 提交数据 处理图片,处理seal， 处理设备的seal
	 * 返回他应该提示停留或者是离开的标志
	 * @param data
	 * @param adminLoginBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月12日
	 */
	@Override
	public int taskProcessingFinishSubmit(DBRow data , AdminLoginBean adminLoginBean )throws Exception{
  		try{
  			int returnFlag = CheckInMgrZwb.LoadCloseNotifyReleaseDoor ;
  			long equipment_id = data.get("equipment_id", 0l);
    		long adid = adminLoginBean.getAdid() ;
   			long entry_id = data.get("entry_id", 0l);
   			String filePath = data.getString("filePath");
			String in_seal = data.getString("in_seal");
			String out_seal = data.getString("out_seal");
			DBRow updateRow = new DBRow();
			if(!StringUtil.isNull(out_seal)){
				updateSealIsUse(out_seal);
				updateRow.add("seal_pick_up", out_seal);
			}
			if(!StringUtil.isNull(in_seal)){
				updateRow.add("seal_delivery", in_seal);
			}
			if(!updateRow.isEmpty()){ //更新设备的seal
				ymsMgrAPI.updateEquipment(equipment_id, updateRow);
			}
			if(isEquipmentCheckOut(equipment_id)){
				returnFlag = CheckInMgrZwb.LoadCloseForgateNotifyReleaseDoorOrStayOrLeave ;
			}else{
				int countNotFinish = floorCheckInMgrZr.countEquipmentAtDoorNotFinishTask(equipment_id, entry_id);
				if(countNotFinish <= 0){
					returnFlag = CheckInMgrZwb.LoadCloseNotifyStayOrLeave ;
				}
			}
			//处理图片
   			//androidHandFile(filePath, adid, entry_id, FileWithCheckInClassKey.PhotoDockClose);
   			
   			//文件上传到文件服务器里面   --by chenchen
    		DBRow r = new DBRow();
			r.add("file_with_id", entry_id);
			r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
			r.add("file_with_class", FileWithCheckInClassKey.PhotoDockClose);
    		fileMgrZr.uploadDirToFileServ(filePath, r, data.getString("session_id"), 
    				checkInFileClassKey.getFileWithTypeId(FileWithCheckInClassKey.PhotoDockClose)+"_"+entry_id+"_");
    		
   			return returnFlag ;
  		}catch(Exception e){
  			throw new SystemException(e,"taskProcessingFinishSubmit",log);
  		}
  	}
	
	/**
	 * taskProcessing 
	 * ReleaseDoor
	 * @param sd_id
	 * @param equipment_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	@Override
	public void taskProcessingReleaseDoor(DBRow data , AdminLoginBean adminLoggerBean) throws Exception{
		try{
			long equipment_id = data.get("equipment_id", 0l);
			long resources_id = data.get("resources_id", 0l);
			int resources_type = data.get("resources_type", 0);
	
			long adid = adminLoggerBean.getAdid() ;
			long entry_id = data.get("entry_id", 0l);
			
			DBRow equipment = getEquipment(equipment_id);
			if(equipment != null){
				//如果是LiveLoad && 设备本身是车尾 && 有车头 。那么车头也要释放资源
				logAddTaskOrEqOperationSpaceLog(resources_type, resources_id, SpaceRelationTypeKey.Equipment, equipment_id, adid, entry_id,
						CheckInLogTypeKey.DOCK_CLOSE,  resources_type  == OccupyTypeKey.DOOR ?  LogEquipmentReleasedDoor :LogEquipmentReleasedSpot , OccupyStatusTypeKey.RELEASED);
				ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id,adminLoggerBean.getAdid());
				liveloadTractorReleaseDoor(equipment,adminLoggerBean,resources_type,resources_id,entry_id);
			}
		}catch(Exception e){
  			throw new SystemException(e,"taskProcessingReleaseDoor",log);
		}
	}
	//如果是LiveLoad && 设备本身是车尾 && 有车头 。那么车头也要释放资源
	private void liveloadTractorReleaseDoor(DBRow equipment ,AdminLoginBean adminLoggerBean ,int resources_type,long resources_id,long entry_id) throws Exception{
		try{
			if(equipment != null && equipment.get("equipment_type", 0) == CheckInTractorOrTrailerTypeKey.TRAILER
					&& equipment.get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.LIVE){
				long check_in_entry_id = equipment.get("check_in_entry_id", 0l);
				if (check_in_entry_id != 0l) {
					DBRow tractor = ymsMgrAPI.getEntryTractor(check_in_entry_id);
					if(tractor != null){
						long adid = adminLoggerBean.getAdid() ;
 						logAddTaskOrEqOperationSpaceLog(resources_type, 
								resources_id, SpaceRelationTypeKey.Equipment, tractor.get("equipment_id", 0l), adid, 
								entry_id, CheckInLogTypeKey.DOCK_CLOSE, 
								resources_type == OccupyTypeKey.DOOR ? LogEquipmentReleasedDoor : LogEquipmentReleasedSpot,OccupyStatusTypeKey.RELEASED);
						ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, tractor.get("equipment_id", 0l) ,adid);

					}
				}
			
			}
		}catch(Exception e){
  			throw new SystemException(e,"liveloadTractor",log);
		}
	} 
	
	/**
	 * 设备离开（check_out_time , liveload 的车头chec_out_time）+ 释放门（资源表中）
	 * 停留(只更新 状态 liveload 的车头 停留)
	 * 都记录日志
	 * @param data
	 * @param adminLoggerBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	@Override
	public void taskProcessingEquipmentLeavingInyard(DBRow data , AdminLoginBean adminLoggerBean) throws Exception{
		try{
			long resources_id = data.get("resources_id", 0l);
 			int resources_type = data.get("resources_type", 0);
 			long entry_id = data.get("entry_id", 0l);
 			long adid = adminLoggerBean.getAdid() ;
			long equipment_id = data.get("equipment_id", 0l);
			int status = data.get("status", 0);
			DBRow  equipment = ymsMgrAPI.getDetailEquipment(equipment_id);
			equipmentLeavingOrStay(equipment, adid, resources_id,resources_type ,entry_id, status);
			leavingOrStayEquipmentLiveLoadTrailer(equipment, adid, resources_id, resources_type , entry_id, status);
			// 添加Entry的check_out_warehouse_time
			handEntryCheckInMainWarehouseCheckOut(entry_id, adid);
		}catch(Exception e){
  			throw new SystemException(e,"taskProcessingEquipmentLeavingInyard",log);
		}
	}
	/**
	 * live load 的车头停留或者离开
	 * 本身是LiveLoad && 本身是车尾 
 	 * @param equipment
	 * @param adid
	 * @param sd_id
	 * @param entry_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	private void leavingOrStayEquipmentLiveLoadTrailer(DBRow equipment, long adid,long resources_id,int resources_type , long entry_id ,int status) throws Exception {
		try {
			if(equipment != null && equipment.get("equipment_type", 0) == CheckInTractorOrTrailerTypeKey.TRAILER
					&& equipment.get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.LIVE){
				// 找到这个Entry对应的车头，然后占用当前 的目标
				long check_in_entry_id = equipment.get("check_in_entry_id", 0l);
				if (check_in_entry_id != 0l) {
					DBRow tractor = ymsMgrAPI.getEntryTractor(check_in_entry_id);
 					equipmentLeavingOrStay(tractor, adid, resources_id, resources_type , check_in_entry_id, status);
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "leavingOrStayEquipmentLiveLoadTrailer",log);
		}
	}
	/**
	 * 设备离开或者是停留
	 * @param equipment
	 * @param adid
	 * @param sd_id
	 * @param entry_id
	 * @param status
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	private void equipmentLeavingOrStay(DBRow equipment, long adid,long resources_id, int resources_type, long entry_id ,int status) throws Exception{
		try{
			if(equipment != null && 
					equipment.get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEFT){
				long equipment_id = equipment.get("equipment_id", 0l);
				DBRow updateRow = new DBRow();
				updateRow.add("equipment_status", status); //更新状态 
				if(status == CheckInMainDocumentsStatusTypeKey.LEAVING 
						|| status == CheckInMainDocumentsStatusTypeKey.INYARD){ //离开 or 停留的时候写上warehouset-check-out
					updateRow.add("check_out_warehouse_time", DateUtil.NowStr());
  					if(status == CheckInMainDocumentsStatusTypeKey.LEAVING){  //leaving 时候才会释放门
 						logAddTaskOrEqOperationSpaceLog(resources_type == OccupyTypeKey.DOOR ? OccupyTypeKey.DOOR : OccupyTypeKey.SPOT, resources_id, 
 								SpaceRelationTypeKey.Equipment,
 								equipment_id,
 								adid, 
 								entry_id, 
 								CheckInLogTypeKey.DOCK_CLOSE, 
 								resources_type == OccupyTypeKey.DOOR ? LogEquipmentReleasedDoor : LogEquipmentReleasedSpot,OccupyStatusTypeKey.RELEASED);
						ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id,adid);
					}
	 				//equipmentOperationDoorSpace(entry_id, adid, equipment_id, sd_id, OccupyStatusTypeKey.RELEASED); //leaving 是需要 释放资源的？ ＩＮＹＡＲＤ是否释放?
				}
				ymsMgrAPI.updateEquipment(equipment_id, updateRow);
			}
		}catch(Exception e){
			throw new SystemException(e, "equipmentLeavingOrStay",log);
		}
	}
	/**
	 * 更新当前的Seal使用了
	 * 
	 * @param seals
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月12日
	 */
	public void updateSealIsUse(String seals) throws Exception {
		try {
			if (!StringUtil.isNull(seals)) {
				DBRow updateRow = new DBRow();
				updateRow.add("used_time", DateUtil.NowStr());
				updateRow.add("seal_status", 2);
				floorCheckInMgrZwb.updateSealUsed(seals, updateRow);
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateSealIsUse", log);
		}

	}

	@Override
	public long addLoadBarUse(DBRow data, AdminLoginBean adminLoginBean)
			throws AddLoadBarUseFoundException, Exception {
		try {
			DBRow inserRow = new DBRow();

			inserRow.add("dlo_id", data.get("entry_id", 0l));
			inserRow.add("equipment_id", data.get("equipment_id", 0l));
			inserRow.add("load_bar_id", data.get("load_bar_id", 0l));
			inserRow.add("count", data.get("count", 0));
			inserRow.add("last_opertion_time", DateUtil.NowStr());
			inserRow.add("last_opertion_adid", adminLoginBean.getAdid());
			return loadBarUseMgrZr.addLoadBarUse(inserRow);
		} catch (AddLoadBarUseFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "addLoadBarUse", log);
		}
	}

	/**
	 * taskProcessing 的时候，如果当前的任务没有分配| 分配人不是他自己，那么需要把改成他自己
	 * 
	 * @param data
	 * @param adminLoginBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月11日
	 */
	@Override
	public void taskProcessingTakeOver(DBRow data, AdminLoginBean adminLoginBean)
			throws Exception {
		try {
			String details = data.getString("detail_id");
			if(StringUtil.isBlank(details)){
				return ;
			}
			List<DBRow> entryDetails = new ArrayList<DBRow>();
			long[] detailLongs = convertStringArrayToLongArray(details.split(","));
			for(long dlo_detail_id : detailLongs){
				if(dlo_detail_id <= 0l){
					continue ;
				}
				DBRow detailRow =  floorCheckInMgrZr.getEntryDetailRow(dlo_detail_id) ; // floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
				entryDetails.add(detailRow);
				//getScheduleByAssociate
				DBRow[] schedule = scheduleMgrZr.getScheduleBy(dlo_detail_id, ProcessKey.CHECK_IN_WAREHOUSE, ModuleKey.CHECK_IN) ;//(dlo_detail_id, ModuleKey.CHECK_IN,ProcessKey.CHECK_IN_WAREHOUSE);
				if (schedule != null && schedule.length > 0) {
					DBRow updateRow = new DBRow();
					updateRow.add("schedule_execute_id", adminLoginBean.getAdid());
					for(DBRow temp : schedule){
						scheduleMgrZr.simpleUpdateScheduleSub(temp.get("schedule_id", 0l), updateRow);
					}
					finishSupverVisorTask(adminLoginBean.getAdid(), detailRow);  //对应的情况是 gate->warehouse->window 修改task
				} else { // 自己给自己安排 getEntryDetailRow
 					detailRow.add("resources_type", data.get("resources_type", 0));
 					detailRow.add("resources_id", data.get("resources_id", 0l));
 					assignWareHouseTask(detailRow,String.valueOf(adminLoginBean.getAdid()),adminLoginBean.getAdid());
				}
			}
			addTakeOverLogs(adminLoginBean.getAdid(), data, entryDetails);
		} catch (Exception e) {
			throw new SystemException(e, "taskProcessingTakeOver", log);
		}
	}
	/**
	 * In 【Tractor:12122】
	 *  Door:232
	 *  Load:121222
	 *  Order:22323
	 * Operator:【Fei Han】
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	private void addTakeOverLogs(long adid , DBRow data , List<DBRow> entryDetails) throws Exception{
		try{
			StringBuffer details = new StringBuffer("");
			long equipment_id = data.get("equipment_id", 0l);
			long resources_id = data.get("resources_id", 0l);
			int resources_type = data.get("resources_type", 0);
			long entry_id = data.get("entry_id",0l);
	 		details.append(",In 【"+	getLogDataOfRelation(SpaceRelationTypeKey.Equipment, equipment_id)+"】");
	 		details.append(",").append(getLogDataOfResouces(resources_id, resources_type));
	 		for(DBRow detail : entryDetails){
	 			details.append(",").append(getLogDateOfDetailRow(detail)) ;
	 		}
	 		 
			checkInMgrZwb.addCheckInLog(adid, "Loader Take Over Tasks", entry_id, CheckInLogTypeKey.AndroidTaskProcessing, DateUtil.NowStr(), details.substring(1));
		}catch(Exception e){
			throw new SystemException(e, "addTakeOverLogs", log);
		}
	}
	
	/**
	 * shuttle 移动到Door
	 * 设备移动到门(那么会把停车位上的任务)
	 * 那么会把当前设备在停车位上面的任务 (unprocess || processing) 任务移动到门上
	 * 同时给他选择的人发送一个(Tasks)任务
	 * @param entry_id
	 * @param equipment_id
	 * @param sd_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	@Override
	public void androidMoveToDoor(long entry_id , long equipment_id , long sd_id, AdminLoginBean adminLoginBean , String exeucut_use_ids)throws Exception{
		try{
			int checkInLogTypeKey = CheckInLogTypeKey.ANDROID_SHUTTLE ;
			long adid = adminLoginBean.getAdid();
			logAddTaskOrEqOperationSpaceLog(OccupyTypeKey.DOOR, sd_id, SpaceRelationTypeKey.Equipment,
					equipment_id, adid, entry_id,checkInLogTypeKey, LogEquipmentOccuipedDoor,OccupyStatusTypeKey.OCUPIED);
			equipmentOperationDoorSpace(entry_id, adid, equipment_id, sd_id, OccupyStatusTypeKey.OCUPIED);
			//车头是否考虑
			//移动在Spot上面的所有的任务
			DBRow[] datas = floorCheckInMgrZr.getEquipmentAtSpotNotFinishDetails(equipment_id);
			if(datas != null && datas.length > 0){
				for(DBRow detail : datas){
					//如果当前detail以前已经Assign了任务 或者是已经指派给了mangaer，那么先删除，然后再重新添加
					deleteDetailTask(detail, adminLoginBean);
					assignWareHouseManganerTask(detail,exeucut_use_ids,adminLoginBean.getAdid());
					
					//记录日志
					logAddTaskOrEqOperationSpaceLog(OccupyTypeKey.DOOR, sd_id, SpaceRelationTypeKey.Task,
							detail.get("dlo_detail_id", 0l), adid, entry_id,checkInLogTypeKey, LogTaskReservedDoor, 
							OccupyStatusTypeKey.RESERVERED);
					// 操作关系表
					taskOperationResourcesSpace(entry_id, adid, detail.get("dlo_detail_id", 0l),
							sd_id, OccupyStatusTypeKey.RESERVERED, OccupyTypeKey.DOOR);
					
				}
			}
			//如果是车尾  && LiveLoad  移动车头
			shuttleMoveLiveLoadTractor(equipment_id, adid, sd_id, OccupyTypeKey.DOOR, entry_id,  checkInLogTypeKey);
		}catch(Exception e){
			throw new SystemException(e, "androidMoveToDoor", log);
		}
	}
	private <T> boolean isNullArrays(T[] arrays){
		return arrays == null || arrays.length  < 1 ;
	}
	/**
	 * 
	 * 删除当前detail 对应的52,53的任务
	 * @param detail
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	private void deleteDetailTask(DBRow detail , AdminLoginBean adminLoginBean ) throws Exception{
		try{
 			long detail_id = detail.get("dlo_detail_id", 0l);
			if(detail_id > 0l){
				DBRow[] schedulesManager =  getWareHouseManagerAssignTask(detail_id) ; // scheduleMgrZr.getScheduleBy(detail_id, ProcessKey.CHECK_IN_WINDOW, ModuleKey.CHECK_IN);
				if(!isNullArrays(schedulesManager)){
					for(DBRow temp : schedulesManager){
						scheduleMgrZr.deleteScheduleByScheduleId(temp.get("schedule_id", 0l), adminLoginBean);
					}
				}
				DBRow[] schedulesLoader = scheduleMgrZr.getScheduleBy(detail_id, ProcessKey.CHECK_IN_WAREHOUSE, ModuleKey.CHECK_IN);
				if(!isNullArrays(schedulesLoader)){
					for(DBRow temp : schedulesLoader){
						scheduleMgrZr.deleteScheduleByScheduleId(temp.get("schedule_id", 0l), adminLoginBean);
					}
				}
			}
		}catch(Exception e){
			throw new SystemException(e, "deleteDetailTask", log);
		}
	}
	/**
	 * shuttle移动到停车位
	 * 
	 * @param entry_id
	 * @param equipment_id
	 * @param yc_id
	 * @param adminLoginBean
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	@Override
	public void androidMoveToSpot(long entry_id , long equipment_id , long yc_id, AdminLoginBean adminLoginBean) throws Exception{
		try{
			long adid = adminLoginBean.getAdid();
	
			
			logAddTaskOrEqOperationSpaceLog(OccupyTypeKey.SPOT, yc_id, SpaceRelationTypeKey.Equipment,
					equipment_id, adid, entry_id, CheckInLogTypeKey.ANDROID_SHUTTLE, LogEquipmentOccuipedSpot, OccupyStatusTypeKey.OCUPIED);
			
			ymsMgrAPI.operationSpaceRelation(OccupyTypeKey.SPOT, yc_id,
					SpaceRelationTypeKey.Equipment, equipment_id,
					ModuleKey.CHECK_IN, entry_id, OccupyStatusTypeKey.OCUPIED, adid);
			
			//Live load 的车头是否考虑
			shuttleMoveLiveLoadTractor(equipment_id, adid, yc_id, OccupyTypeKey.SPOT, entry_id,  CheckInLogTypeKey.ANDROID_SHUTTLE);
			
		}catch(Exception e){
			throw new SystemException(e, "androidMoveToSpot", log);
		}
	}
	private void shuttleMoveLiveLoadTractor(long equipment_id , long adid , long resouces_id , int resouces_type , long entry_id , int checkInLogType ) throws Exception{
		try{
			DBRow equipment = getEquipment(equipment_id);
			if (equipment != null && equipment.get("equipment_type", 0) == EquipmentTypeKey.Container && equipment.get("equipment_purpose", 0) == CheckInLiveLoadOrDropOffKey.LIVE) {
				long check_in_entry_id = equipment.get("check_in_entry_id", 0l);
				if (check_in_entry_id != 0l) {
					DBRow tractor = ymsMgrAPI.getEntryTractor(check_in_entry_id);
					if (tractor != null) {

						
 						if(resouces_type == OccupyTypeKey.SPOT){
							
							logAddTaskOrEqOperationSpaceLog(resouces_type,
									resouces_id, SpaceRelationTypeKey.Equipment,
									tractor.get("equipment_id", 0l), adid,
									entry_id,
									checkInLogType,
									LogEquipmentOccuipedSpot,OccupyStatusTypeKey.OCUPIED);
							
							equipmentOperationSpotSpace(check_in_entry_id, adid,
								tractor.get("equipment_id", 0l), resouces_id,
								OccupyStatusTypeKey.OCUPIED);
						}else{
							logAddTaskOrEqOperationSpaceLog(resouces_type,
									resouces_id, SpaceRelationTypeKey.Equipment,
									tractor.get("equipment_id", 0l), adid,
									entry_id,
									checkInLogType,
									LogEquipmentOccuipedDoor,OccupyStatusTypeKey.OCUPIED);
							
							equipmentOperationDoorSpace(entry_id, adid, tractor.get("equipment_id", 0l), resouces_id, OccupyStatusTypeKey.OCUPIED); 
						}
					}
			}
		}
		}catch(Exception e){
			throw new SystemException(e, "shuttleMoveLiveLoadTractor", log);
		}
		

	}
	@Override
	public void updateEquipmentSeal(DBRow data, AdminLoginBean adminLoginBean)
			throws Exception {
		try{
			long equipment_id = data.get("equipment_id", 0l);
			String in_seal = data.getString("in_seal");
			String out_seal = data.getString("out_seal");
			DBRow updateRow = new DBRow();
 			updateSealIsUse(out_seal);
			updateRow.add("seal_pick_up", out_seal);
 			updateRow.add("seal_delivery", in_seal);
			if(!updateRow.isEmpty()){ //更新设备的seal
				ymsMgrAPI.updateEquipment(equipment_id, updateRow);
			}
				
		}catch(Exception e){
			throw new SystemException(e, "updateEquipmentSeal", log);
		}
 		
	}
	
	@Override
	public DBRow[] windowCheckInList(long ps_id ,PageCtrl pc) throws Exception {
		try{
			 
			 	DBRow[] goingtowindow = goingToWindow(ps_id,pc);				//列表的数据先怎么写
 			 
				 TreeSet<Long> entryIds = new TreeSet<Long>(new Comparator<Long>() {
					@Override
					public int compare(Long o1, Long o2) {
	 					return o2.compareTo(o1);
					}
				});
 				 if(goingtowindow != null && goingtowindow.length > 0){
					 for(DBRow temp : goingtowindow){
						 if(temp.get("dlo_id", 0l) > 0l){
							 entryIds.add(temp.get("dlo_id", 0l));
						 }
					 }
				 }
 				 List<DBRow> returnList = new ArrayList<DBRow>();
 				 Iterator<Long>  its = entryIds.iterator();
 				 while(its.hasNext()){
 					 Long entry_id = its.next() ;
 					 returnList.add(floorCheckInMgrZwb.findMainById(entry_id));
 				 }
 				 DBRow[] returnArrays = returnList.toArray(new DBRow[0]);
				 return fixWindowCheckInList(returnArrays);
	  
 		}catch(Exception e){
			throw new SystemException(e, "windowCheckInList", log);
		}
 	}
	
	private DBRow[] fixWindowCheckInList(DBRow[] entrys) throws Exception{
		if(entrys != null && entrys.length > 0){
			 DBRow[] returnArrays = new DBRow[entrys.length];
			 for(int index = 0 , count = entrys.length ; index < count ; index++ ){
				 DBRow entryTemp = entrys[index];
				 DBRow temp = new DBRow();
				 temp.add("gate_driver_name", entryTemp.getString("gate_driver_name"));
				 String time = "" ;
				 try{
					 time =  DateUtil.showLocalparseDateToNoYear24Hours(entryTemp.getString("gate_check_in_time"), entryTemp.get("ps_id", 0l));
				 }catch(Exception e){
 				 }
				 temp.add("gate_check_in_time", time );
				 temp.add("company_name", entryTemp.getString("company_name") );
				 temp.add("entry_id", entryTemp.get("dlo_id", 0l));
				 temp.add("ps_id", entryTemp.get("ps_id",0l));
				 temp.add("equipments", floorCheckInMgrZr.getEntryEquipmentsNoResouces(entryTemp.get("dlo_id", 0l)));
				 returnArrays[index] = temp ;
			 }
			 return returnArrays ;
		}
		return new DBRow[]{} ;
	}

	private int getSortEquipmentPurpose(DBRow entryEquipment){
		  int equipment_purpose = entryEquipment.get("equipment_purpose", 0);
		  
		  //CheckInLiveLoadOrDropOffKey 因为这个类的里面的排序是 LiveLoad，DropOff ， PickUp 是好的 ,先处理
		  return equipment_purpose ;
	}
	private void appendResoucesName(DBRow temp) throws Exception{
		try{
			long resources_id =  temp.get("resources_id", 0l);
			int resources_type = temp.get("resources_type", 0);
			if(resources_type ==  OccupyTypeKey.DOOR){
				temp.add("resources_name", getDoorName(resources_id));
			}
			if(resources_type == OccupyTypeKey.SPOT){
				temp.add("resources_name", getSpotName(resources_id));
			}
			temp.add("resources_id", resources_id);
			temp.add("resources_type", resources_type);
			temp.add("resources_name", temp.getString("resources_name"));
		}catch(Exception e){
			throw new SystemException(e, "appendResoucesName", log);
		}
	}
	
	/**
	 * 通过EntryId 去查询自己的 设备 ， 以及自己PickUp的设备
	 */
	@Override
	public DBRow[] windowCheckInEntryEquipmentList(long entry_id)
			throws Exception {
		try{
			//排序 LiveLoad ， Drop Off , Pick Up
			
			List<DBRow> returnEquipments = new ArrayList<DBRow>();
			DBRow[] entryEquipments =  floorCheckInMgrZr.getEquipmentByEntryAndTotalTask(entry_id);
			DBRow[] entryPickUpEquipments =   floorCheckInMgrZr.getEntryPickUpEequipmentAndTotalTask(entry_id);
			if(entryEquipments != null && entryEquipments.length > 0){
				for(DBRow temp : entryEquipments){
					appendResoucesName(temp);
					returnEquipments.add(temp);
				}
 			}
			if(entryPickUpEquipments != null && entryPickUpEquipments.length > 0){
				for(DBRow temp : entryPickUpEquipments){
					temp.add("equipment_purpose", CheckInLiveLoadOrDropOffKey.PICK_UP);  //设置为Pick Up
 					appendResoucesName(temp);
					returnEquipments.add(temp);
				}
			}
			
			if(returnEquipments != null){
				Collections.sort(returnEquipments, new Comparator<DBRow>(){
					@Override
					public int compare(DBRow o1, DBRow o2) {
						int equipent_type1 = o1.get("equipment_type", 0);
						int equipent_type2 = o2.get("equipment_type", 0);
						
						if(equipent_type1 == equipent_type2){
							
							int equipment_purpose1 = getSortEquipmentPurpose(o1);
							int equipment_purpose2 =  getSortEquipmentPurpose(o2);
	 						return equipment_purpose1 > equipment_purpose2 ? 1 : (equipment_purpose1 == equipment_purpose2 ? 0 : -1) ;
						}else{
							return equipent_type1  > equipent_type2 ? 1: (equipent_type2 == equipent_type1 ? 0 : -1);
						}
					}
				});
			}
			fixWindowCheckInListEquipments(returnEquipments);
			
			return returnEquipments.toArray(new DBRow[0]) ;
		}catch(Exception e){
			throw new SystemException(e, "windowCheckInEntryEquipmentList", log);
		}
 	}
	
	private void fixWindowCheckInListEquipments(List<DBRow> arrays){
		if(arrays != null && arrays.size() > 0){
			for(DBRow temp : arrays ){
				temp.remove("phone_equipment_number");
				temp.remove("patrol_create");
				temp.remove("patrol_lost");
//				temp.remove("rel_type");
				temp.remove("check_in_window_time");
				temp.remove("ps_id");
				
				temp.remove("srr_id");
				temp.remove("relation_type");
				temp.remove("relation_id");
				temp.remove("module_type");
				temp.remove("module_id");
				temp.add("check_out_entry_id", temp.get("check_out_entry_id", 0L));;
				
 			}
		}
	}
	
	private String detailFinishUser(long dlo_detail_id) throws Exception{
		try{
			//getTaskFinishUser
			StringBuffer users = new StringBuffer();
			DBRow[] usersInfos =  floorCheckInMgrZr.getTaskFinishUser(dlo_detail_id);
			if(usersInfos != null && usersInfos.length > 0){
				for(DBRow temp : usersInfos){
					users.append(",").append(temp.getString("employe_name"));
				}
 			}
			return users.length() > 0 ? users.substring(1) : ""  ;
		}catch(Exception e){
			throw new SystemException(e, "detailFinishUser", log);
		}
	}
	
	
	private String detailFinishUserIds(long dlo_detail_id) throws Exception{
		try{
			//getTaskFinishUser
			StringBuffer users = new StringBuffer();
			DBRow[] usersInfos =  floorCheckInMgrZr.getTaskFinishUser(dlo_detail_id);
			if(usersInfos != null && usersInfos.length > 0){
				for(DBRow temp : usersInfos){
					users.append(",").append(temp.getString("schedule_finish_adid"));
				}
 			}
			return users.length() > 0 ? users.substring(1) : ""  ;
		}catch(Exception e){
			throw new SystemException(e, "detailFinishUserIds", log);
		}
	}
	
	
	private String detailSupervizorUsers(long dlo_detail_id) throws Exception{
		try{
			//getTaskFinishUser
			StringBuffer users = new StringBuffer();
			DBRow[] usersInfos =  floorCheckInMgrZr.getTaskSupervizorUsers(dlo_detail_id);
			if(usersInfos != null && usersInfos.length > 0){
				for(DBRow temp : usersInfos){
					users.append(",").append(temp.getString("employe_name"));
				}
 			}
			return users.length() > 0 ? users.substring(1) : ""  ;
		}catch(Exception e){
			throw new SystemException(e, "detailSupervizorUsers", log);
		}
	}
	
	private String detailSupervizorUserIds(long dlo_detail_id) throws Exception{
		try{
			//getTaskFinishUser
			StringBuffer users = new StringBuffer();
			DBRow[] usersInfos =  floorCheckInMgrZr.getTaskSupervizorUsers(dlo_detail_id);
			if(usersInfos != null && usersInfos.length > 0){
				for(DBRow temp : usersInfos){
					users.append(",").append(temp.getString("schedule_execute_id"));
				}
 			}
			return users.length() > 0 ? users.substring(1) : ""  ;
		}catch(Exception e){
			throw new SystemException(e, "detailSupervizorUserIds", log);
		}
	}
	
	
	private DBRow[] getWindowCheckInTaskList(long entry_id , long equipment_id , DBRow entry, boolean notifyWindow) throws Exception{
		try{
			
			
			DBRow[] rows = findDetailsHasDoorOrSpot(entry_id, equipment_id,CheckInChildDocumentsStatusTypeKey.getAllStatus());
 			for (DBRow tempRow : rows) {
 				//2014-12-27 area_id
 				if(tempRow.get("resources_type", 0) == OccupyTypeKey.SPOT){
 					long resources_id = tempRow.get("resources_id", 0l);
 					DBRow spot = floorCheckInMgrZwb.findStorageYardControl(resources_id);
 					if(spot != null){
 						tempRow.add("area_id", spot.get("area_id",0l));
 					}
 				}else{
 					long resources_id = tempRow.get("resources_id", 0l);
 					DBRow door = floorCheckInMgrZwb.findDoorById(resources_id);
 					if(door != null){
 						tempRow.add("area_id", door.get("area_id",0l));
 					}
 				}
				List<DBRow> arrayList = (ArrayList<DBRow>) tempRow.get("details", new ArrayList<DBRow>());
				tempRow.remove("details");
				List<DBRow> fixArrayLis = new ArrayList<DBRow>();
				for (DBRow detail : arrayList) {
					String customer_id = detail.get("customer_id", "");
					customer_id  = StringUtil.isNull(customer_id) ? "NA" : customer_id ;
					
					String company_id = detail.get("company_id", "");
					company_id  = StringUtil.isNull(company_id) ? "NA" : company_id ;
					
					String title = detail.get("supplier_id", "");
 					title  = StringUtil.isNull(title) ? "NA" : title ;
					
					DBRow loadR = new DBRow();
					loadR.add("number", detail.getString("number"));
					loadR.add("detail_id", detail.get("dlo_detail_id", 0l));
					loadR.add("number_type", detail.get("number_type", ""));
 					loadR.add("customer_id", customer_id);
					loadR.add("company_id", company_id);
					loadR.add("title", title);
 					loadR.add("number_status", detail.get("number_status", 0));
 					String closeUser  = "";
 					String closeUserIds = "";
 					if(notifyWindow)
 					{
 						closeUser = detailSupervizorUsers(detail.get("dlo_detail_id", 0l));
 						closeUserIds = detailSupervizorUserIds(detail.get("dlo_detail_id", 0l));
 					}
 					else
 					{
 						if(checkInMgrZwb.isEntryDetailIsFinish(detail)){ 
 							closeUser = detailFinishUser(detail.get("dlo_detail_id", 0l));
 							closeUserIds = detailFinishUserIds(detail.get("dlo_detail_id", 0l));
 						} 
 					}
					loadR.add("close_user",closeUser);
					loadR.add("close_user_ids", closeUserIds);
 					loadR.add("finish_time",DateUtil.showLocalparseDateToNoYear24Hours(detail.getString("finish_time"), entry.get("ps_id", 0l)));
					fixArrayLis.add(loadR);
				}
				tempRow.add("load_list", fixArrayLis.toArray(new DBRow[0]));
			}
			return rows ;
		}catch(Exception e){
			throw new SystemException(e, "getWindowCheckInTaskList", log);
		}
	}
	
	@Override
	public DBRow windowCheckInGetEquipmentInfos(long equipment_id, long dlo_id) throws  NoRecordsException ,Exception {
		try{
			DBRow returnRow = new DBRow();
			//equipment
			DBRow equipment =  floorCheckInMgrZr.getEquipmentWithResources(equipment_id);
			if(equipment == null){
				throw new NoRecordsException();
			}
			boolean notifyWindow = false;
			if(equipment.get("check_in_entry_id", 0L)==dlo_id)
			{
				notifyWindow = true;
			}
			fixEquipments(equipment);
 			appendResoucesName(equipment);  //添加Resrouces Name 
			
			long entry_id = equipment.get("check_in_entry_id", 0l);
			DBRow entryRow = floorCheckInMgrZwb.findMainById(entry_id);
			DBRow[] arrays = fixWindowCheckInList(new DBRow[]{entryRow});
			//tasks
			returnRow.add("equipment", equipment);
			returnRow.add("entry", arrays[0]);
			returnRow.add("tasks", getWindowCheckInTaskList(entry_id, equipment_id,arrays[0],notifyWindow));
			
			return returnRow ;
		}catch(NoRecordsException e){
			throw e ;
		}catch(Exception e){
			throw new SystemException(e, "windowCheckInGetEquipmentInfos", log);
		}
 	}
	


	
	/**
	 * 通过判断当前Entry 所有的设备他自己带走的(Window Check In PickUp CTNR) + 自己本身的
	 * 
	 * 1.判断本身的设备是否都离开
	 * 2.判断自己带走的设备是否都离开
	 * 
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月24日
	 */
	@Override
	public void checkEntryIsLeft(long entry_id) throws CheckInEntryIsLeftException ,  Exception{
		try{
			boolean isCheckOut = isCheckEntryIsLeft(entry_id);
			if(isCheckOut){
				throw new CheckInEntryIsLeftException();
			}
		}catch(CheckInEntryIsLeftException e){
			throw e ;
		}catch(Exception e){
			throw new SystemException(e, "isEntryCheckOut", log);
		}
	}
	/**
	 * 重载 有的时候不需要抛出异常
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月7日
	 */
	private boolean isCheckEntryIsLeft(long entry_id) throws Exception{
		try{
			boolean isCheckOut = true ;
			DBRow[] entryEquipments =  floorCheckInMgrZr.getEntryEequipment(entry_id);
			if(entryEquipments != null && entryEquipments.length > 0){
				for(DBRow equipment : entryEquipments){
					if(!isEquipmentCheckOut(equipment)){
						isCheckOut = false ;
						break ;
					}
				}
			}else{
				 //说明一个设备也没有
				isCheckOut = false  ;
			}
			return isCheckOut ;
		}catch(Exception e){
			throw new SystemException(e, "isEntryCheckOut", log);
		}
	}
	/**
	 * 都是返回Entry的信息 ，然后调用 
	 * 
	 */
	private final static int WindowCheckInSearchEntry  = 1;
	private final static int WindowCheckInSearchEquipment  = 2;
	private final static int WindowCheckInSearchOrderNumbers  = 3;
	@Override
	public DBRow[] getWindowCheckInSearchValue(String searchValue, int searchType , AdminLoginBean adminLoginBean) throws NoPermiessionEntryIdException ,CheckInNotFoundException ,CheckInEntryIsLeftException , Exception {
		try{
			if(searchType  == WindowCheckInSearchEntry){
				long entry_id = Long.parseLong(searchValue);
				DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
				CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
				checkEntryIsLeft(entry_id);
				return fixWindowCheckInList(new DBRow[]{entryRow});
			}
			/**
			 * 查询没有离开的设备
			 */
			if(searchType == WindowCheckInSearchEquipment){
				//通过设备查询到 Entry  fixWindowCheckInList , 查询没有离开的设备
				long ps_id = adminLoginBean.getPs_id() ;
				DBRow[] entrys = floorCheckInMgrZr.getEntryByNotLeftEquipment(searchValue,ps_id);
				return fixWindowCheckInList(entrys);
			}
			return new DBRow[]{};
		}catch(CheckInEntryIsLeftException e){
			throw e ;
		}catch(NoPermiessionEntryIdException e){
			throw e ;
		}catch (CheckInNotFoundException e) {
			throw e ;
 		}catch(Exception e){
			throw new SystemException(e, "getWindowCheckInSearchValue", log);
		}
 	}
	
	@Override
	public DBRow getWindowCheckInEntryInfos(long entry_id) throws Exception {
		try{
			DBRow entry = floorCheckInMgrZwb.findMainById(entry_id);
			if(entry != null){
				DBRow temp = new DBRow();
				temp.add("gate_driver_liscense", entry.getString("gate_driver_liscense"));
				temp.add("gate_driver_name", entry.getString("gate_driver_name"));
				temp.add("mc_dot", entry.getString("mc_dot"));
				temp.add("company_name", entry.getString("company_name"));
				temp.add("entry_id",entry_id);
				temp.add("priority",entry.get("priority", 0));
				entry = temp ;
			}
			return entry ;
		}catch(Exception e){
			throw new SystemException(e, "windowCheckInGetEquipmentInfos", log);
		}
 	}
	
	@Override
	public void convertCarrierPhoneNumber() throws Exception {
		try{
			
			DBRow[] carriers = floorCheckInMgrZr.getAllCarrier();
			if(carriers != null && carriers.length > 0){
				for(DBRow temp : carriers){
					String phoneCarriers = StringUtil.convertStr2PhotoNumber(temp.getString("carrier"));
					long id = temp.get("id", 0l);
					DBRow updateRow = new DBRow();
					updateRow.add("phone_carrier", phoneCarriers);
					floorCheckInMgrZr.updateCarrier(id, updateRow);
				}
			}
		}catch(Exception e){
			throw new SystemException(e, "convertCarrierPhoneNumber", log);
		}
	}
	
	@Override
	public DBRow[] getCarrierByPhoneNumber(String searchValue) throws Exception {
		try{
			if(StringUtil.isNumber(searchValue)){	//数字查询
				DBRow[] carriers = floorCheckInMgrZr.getCarrierByPhoneNumber(searchValue);
				fixCarrier(carriers);
				return carriers ;
			}else{	//字母查询
				DBRow[] carriers = floorCheckInMgrZr.getCarrierByCarrier(searchValue);
				fixCarrier(carriers);
				return carriers ;
			}
		}catch(Exception e){
			throw new SystemException(e, "getCarrierByPhoneNumber", log);
		}
 	}
	
	private void fixCarrier(DBRow[] carriers){
 		if(carriers != null && carriers.length > 0){
			for(DBRow temp : carriers){
				temp.remove("id");
				temp.remove("scac");
				temp.remove("phone_carrier");
 			}
		}
 	}
	
	/**
	 * 检查mc_dot ,carrier 
	 * 
	 * 1.
	 * @param mc_dot
	 * @param carrier
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月25日
	 */
	
	private final static int ALLMATCH = 1 ;				//全部匹配 			mcdot && carrier	直接添加不用提示
	private final static int ONLYMCDOTMATCH = 2 ;		//只有DOT匹配		mcdot && !carrier	返回系统的值 和 填入的值，让他选择更新Carrier 也可以不用更新
	private final static int ONLYCARRIERMATCH = 3 ;		//只有CARRIER匹配	!mcdot && carrier	返回系统的值 和填入的值 ，让他选择更新McDot , 也可以不用更新
	private final static int NOTMATCH = 4 ;				//都不匹配			!mcdot && !carrier	提示他是否添加这组关系到数据库
	private final static int INValidate =  5 ;
	
 
	private DBRow checkMcDotAndCarrier(String mc_dot , String carrier) throws Exception{
		try{
			DBRow result = new DBRow();
			result.add("flag", INValidate);
  			if(!StringUtil.isBlank(mc_dot) && !StringUtil.isBlank(carrier)){
 				mc_dot = mc_dot.toUpperCase() ;
 				carrier = carrier.toUpperCase();
 				
 				DBRow inputDate = new DBRow();
  				inputDate.add("carrier", carrier);
  				inputDate.add("mc_dot", mc_dot);
  				result.add("inputvalue", inputDate);
 				
 				
  				if(isMcDotAndCarrierAllMath(mc_dot,carrier)){
  					result.add("flag",ALLMATCH);
  					return result ;
  				}else{
 					HoldDoubleValue<Boolean, DBRow>  mcDotMatch = isOnlyMcDotMatch(mc_dot, carrier);
 					if(mcDotMatch.a){
 	  					result.add("flag",ONLYMCDOTMATCH);
 	  					result.add("systemvalue", mcDotMatch.b);
 	  					return result ;
   					}
 					HoldDoubleValue<Boolean, DBRow>  carrierMatch = isOnlyCarrierMatch(mc_dot, carrier);
 					if(carrierMatch.a){
  						result.add("flag",ONLYCARRIERMATCH);
 	  					result.add("systemvalue", carrierMatch.b);
 	  					return result;
 					}
 					result.add("flag", NOTMATCH);
 					return result;
  				}
 			}
			return result ;
		}catch(Exception e){
			throw new SystemException(e, "checkMcDotAndCarrier", log);
		}
	}
	private boolean isMcDotAndCarrierAllMath(String mc_dot , String carrier) throws Exception{
		try{
			int countAllMatch = floorCheckInMgrZr.getCarrierAndMcDot(carrier,mc_dot);
			if(countAllMatch > 0) {
				return true  ;
			}
			return false ;
		}catch(Exception e){
			throw new SystemException(e, "isMcDotAndCarrierAllMath", log);
		}
	}
	
	/**
	 * 只有 McDot 匹配
	 * @param mc_dot
	 * @param carrier
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月25日
	 */
	private HoldDoubleValue<Boolean, DBRow> isOnlyMcDotMatch(String mc_dot , String carrier) throws Exception{
		try{
			DBRow[] carriers = floorCheckInMgrZr.getMcDotNotCarrier(carrier,mc_dot);
			if(carriers != null && carriers.length > 0){
				return new HoldDoubleValue<Boolean, DBRow>(true,carriers[0]);
			}
			return new HoldDoubleValue<Boolean, DBRow>(false, null);
		}catch(Exception e){
			throw new SystemException(e, "isOnlyMcDotMatch", log);
		}
	}
	/**
	 * 只有carrier匹配
	 * @param mc_dot
	 * @param carrier
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月25日
	 */
	private HoldDoubleValue<Boolean, DBRow> isOnlyCarrierMatch(String mc_dot , String carrier) throws Exception{
		try{
			DBRow[] carriers = floorCheckInMgrZr.getCarrierNotMcDot(carrier, mc_dot);
			if(carriers != null && carriers.length > 0){
				return new HoldDoubleValue<Boolean, DBRow>(true,carriers[0]);
			}
			return new HoldDoubleValue<Boolean, DBRow>(false, null);
		}catch(Exception e){
			throw new SystemException(e, "isOnlyMcDotMatch", log);
		}
	}
	

	@Override
	public DBRow[] getSearchMcDot(String mc_dot) throws Exception {
		try{
			return floorCheckInMgrZr.getMcdot(mc_dot);
		}catch(Exception e){
			throw new SystemException(e, "getSearchMcDot", log);
		}
 	}
	
	/**
	 * android window check in 的时候更新 Driver Info
	 * 
	 * 先更新Driver Info 
	 * 然后再Check McDot And Carrier
	 */
	@Override
	public DBRow windowCheckInUpdateEntryDriverInfo(DBRow updateRow , long entry_id , AdminLoginBean adminLoginBean)
			throws Exception {
		try{
			String mc_dot = updateRow.getString("mc_dot");
			String carrier = updateRow.getString("company_name");
			String filePath = updateRow.getString("file_path");
			String sessionId = updateRow.getString("session_id");
			updateRow.remove("file_path");
			updateRow.remove("session_id");
			floorCheckInMgrZwb.modCheckIn(entry_id, updateRow);
			DBRow  returnRow = checkMcDotAndCarrier(mc_dot, carrier);	
			/*if(!StringUtil.isNull(filePath)){
	   			androidHandFile(filePath, adminLoginBean.getAdid(), entry_id, FileWithCheckInClassKey.PhotoGateCheckIn);
			}*/
			
			//文件上传到文件服务器里面   --by chenchen
    		DBRow r = new DBRow();
			r.add("file_with_id", entry_id);
			r.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
			r.add("file_with_class", FileWithCheckInClassKey.PhotoGateCheckIn);
    		fileMgrZr.uploadDirToFileServ(filePath, r, sessionId, 
    				checkInFileClassKey.getFileWithTypeId(FileWithCheckInClassKey.PhotoGateCheckIn)+"_"+entry_id+"_");
    		
			return returnRow ;
		}catch(Exception e){
			throw new SystemException(e, "getSearupdateRowchMcDot", log);
		}
	}
	/**
	 * 	private final static int ALLMATCH = 1 ;				//全部匹配 			mcdot && carrier	直接添加不用提示
	private final static int ONLYMCDOTMATCH = 2 ;		//只有DOT匹配		mcdot && !carrier	返回系统的值 和 填入的值，让他选择更新Carrier 也可以不用更新
	private final static int ONLYCARRIERMATCH = 3 ;		//只有CARRIER匹配	!mcdot && carrier	返回系统的值 和填入的值 ，让他选择更新McDot , 也可以不用更新
	private final static int NOTMATCH = 4 ;				//都不匹配			!mcdot && !carrier	提示他是否添加这组关系到数据库
	private final static int INValidate =  5 ;
	 * @param data
	 * @author zhangrui
	 * @Date   2014年12月26日
	 */
	@Override
	public void handMcDotAndCarrier(DBRow data) throws Exception{
		int flag = data.get("flag", INValidate);
		if(flag == NOTMATCH){	//说明添加一组记录到数据库中
			DBRow insertRow = new DBRow();
			insertRow.add("carrier", data.getString("carrier"));
 			insertRow.add("mc_dot", data.getString("mc_dot"));
			insertRow.add("phone_carrier", StringUtil.convertStr2PhotoNumber(data.getString("carrier")));
			floorCheckInMgrZr.addMcDotAndCarrier(insertRow);
			return ;
		}
		if(flag == ONLYCARRIERMATCH){//说明是更新McDot
			long id = data.get("id", 0l);
			DBRow updateRow = new DBRow();
			updateRow.add("mc_dot", data.getString("mc_dot"));
			floorCheckInMgrZr.updateMcDotAndCarrier(id,updateRow);
			return  ;
		}
		if(flag == ONLYMCDOTMATCH){ //说明是更新carrier
			DBRow updateRow = new DBRow();
			long id = data.get("id", 0l);
			updateRow.add("carrier", data.getString("carrier"));
			updateRow.add("phone_carrier", StringUtil.convertStr2PhotoNumber(data.getString("carrier")) );
			floorCheckInMgrZr.updateMcDotAndCarrier(id,updateRow);
			return ;
		}
	}
	
	
	 
	/**
	 * Load/Receive 流程计算颜色
	 * screenSetParams 里面有设置红，黄，对应的百分比以及这条单子（处理的允许的最长时间） 
	 * @param datas
	 * @param key 
	 * @param screenSetParams
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	private final static int LoadReceiveProcessShowColorRed = 1 ;			//显示红色
	private final static int LoadReceiveProcsssShowColorYellow = 2; 		//显示黄色
	private final static int LoadReceiveProcsssShowColorGreen = 3; 		//显示绿色

	private HoldThirdValue<Integer, Integer,Integer> loadReceiveProcessCaculateColor(DBRow[] datas ,   DBRow handleTimeParam , DBRow redColorParam , DBRow yellowColorParam){
		int handleTime = handleTimeParam != null ?(int) handleTimeParam.get("value", 0.0d) : 0 ;
		int redColorValue = redColorParam != null ? (int) redColorParam.get("value", 0.0d) : 0;	
		int yellowColorValue = yellowColorParam != null ?(int)  yellowColorParam.get("value", 0) : 0;	//yellow color < red color
		 
		int color = LoadReceiveProcsssShowColorGreen ;
		int count = 0 ;
		int total = 0 ;
		if(datas != null && datas.length > 0){
			total = datas.length ;
			for(DBRow temp : datas){
				if(handleTime > 0 && temp.get("total_time", 0) > handleTime){
					count ++ ;
 				}
			}

			int result = (int)((count / (datas.length * 1.0d)) * 100) ;
			if(result > redColorValue){
				color =  LoadReceiveProcessShowColorRed ;
			}else if(result > yellowColorValue){
				color = LoadReceiveProcsssShowColorYellow ;
			}
		}
 		return new HoldThirdValue<Integer, Integer, Integer>(count, total , color);
	}
	@Override
	public DBRow getLoadReceiveProcess(long ps_id) throws Exception {
		try{
 			DBRow result = new DBRow();
 			result.add("incomingtowindow", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.IncomingToWindow));
			result.add("loadReceive", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.LoadReceive));
			result.add("NeedAssign", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.NeedAssign));
			result.add("RemainJobs", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.RemainJobs));
			result.add("UnderProcessing", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.UnderProcessing));
			result.add("CloseToday", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.CLOSETODAY));
			result.add("forgetclose",  floorCheckInMgrZr.getForgetCloseTasks(ps_id));
			
			DBRow[] onSpots = cargoOnSpot(ps_id, null);
			result.add("cargoonspot", onSpots != null ? onSpots.length : 0);

			DBRow[] onDoors = cargoOnDoor(ps_id, null);
			result.add("cargoondoor", onDoors != null ? onDoors.length : 0);

			//result.add("NeedAssign", loadReceiveProcessInColor(ps_id,SetReportScreenTypeKey.));
			return result ;
		}catch(Exception e){
			throw new SystemException(e, "getLoadReceiveProcess", log);
		}
	}
	
	/**
	 * goingtowindow 屏幕的计算 ()
	 * @param ps_id
	 * @param reportScreenTypeKey
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	private DBRow loadReceiveProcessInColor(long ps_id,int reportScreenTypeKey) throws Exception{
		try{
			DBRow[] datas = null ;
			if(reportScreenTypeKey == SetReportScreenTypeKey.IncomingToWindow){
				datas = goingToWindow(ps_id, null) ;
			}
			if(reportScreenTypeKey == SetReportScreenTypeKey.LoadReceive){
				datas = floorCheckInMgrZr.getSetReportScreenLoadReceive(ps_id);
			}
			if(reportScreenTypeKey == SetReportScreenTypeKey.NeedAssign){
				datas = floorCheckInMgrZr.getSetReportScreenNeedAssignTask(ps_id);
 			}
			if(reportScreenTypeKey == SetReportScreenTypeKey.RemainJobs){
				datas = floorCheckInMgrZr.getSetReportScreenRemainJobs(ps_id);
			}
			if(reportScreenTypeKey == SetReportScreenTypeKey.UnderProcessing){
				datas = floorCheckInMgrZr.getSetReportScreenUnderProcessing(ps_id);
			}
			if(reportScreenTypeKey == SetReportScreenTypeKey.CLOSETODAY){
				
				//系统new出来的是UTC的时间 , -> Local 时间 (2014-12-30 12:12:23) 取出时间 2014-12-30 00:00:00 -> 转换成UTC的时间 （开始时间）
				//结束时间 就是 new 出来的时间
				String startTime = null ;
				String endTime = DateUtil.NowStr() ;
				String endLocalTime =  DateUtil.showLocationTime(new Date(), ps_id);
				if(!StringUtil.isNull(endLocalTime)){
					String[] timeArray = endLocalTime.split(" ");
					if(timeArray.length > 1){
						String prefixTime =  timeArray[0] ;
						String timeStr = prefixTime + " " + "00:00:00";	
						startTime = DateUtil.showUTCTime(timeStr, ps_id);
					}
				}
				if(!StringUtil.isNull(startTime) && !StringUtil.isNull(endTime)){
					datas = floorCheckInMgrZr.getSetReportScreenCloseTodayNumber(ps_id,startTime,endTime);
				}
			}
			return caculateCountTotalColor(ps_id, reportScreenTypeKey, datas) ;
 		}catch(Exception e){
			throw new SystemException(e, "loadReceiveProcessIncomingToWindowColor", log);
		}
	}
	/**
	 * datas 里面一定要包含total_time 计算出来一共花费了多少的时间
	 * @param ps_id
	 * @param reportScreenTypeKey
	 * @param datas
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	private DBRow caculateCountTotalColor(long ps_id , int reportScreenTypeKey,DBRow[] datas) throws Exception{
		try{
			HoldThirdValue<DBRow, DBRow, DBRow> params = getLoadReceiveScreenParamSet(ps_id, reportScreenTypeKey);
			HoldThirdValue<Integer, Integer, Integer> result = loadReceiveProcessCaculateColor(datas,params.a, params.b, params.c);
			DBRow returnVlaue = new DBRow();
			returnVlaue.add("count", result.a);
			returnVlaue.add("total", result.b);
			returnVlaue.add("color", result.c);
			return returnVlaue ;
		}catch(Exception e){
			throw new SystemException(e, "caculateCountTotalColor", log);
		}
		
	}
	/**
	 * 添加或者是保存数据
	 * @param data
	 * @author zhangrui
	 * @Date   2014年12月31日
	 */
	@Override
	public  void saveOrUpdateSetLoadReceiveScreemParams(DBRow data) throws Exception {
		 try{
			 long ps_id  = data.get("ps_id", 0l);
			 int red = data.get("red", 0);
			 int yellow = data.get("yellow", 0);
			 int handleTime = data.get("handle_time", 0);
			 int setReportScreenTypeKey = data.get("screen_type_key", 0);
			 
			 //hand report screen 
 			 DBRow reportScreen =  floorCheckInMgrZr.getReportScreen(ps_id, setReportScreenTypeKey);
  			 long set_report_screen_id = 0l ;
 			 if(reportScreen == null){//add
				 DBRow insertRow = new DBRow();
				 insertRow.add("ps_id", ps_id);
				 insertRow.add("report_screen_key", setReportScreenTypeKey);
				 set_report_screen_id = floorCheckInMgrZr.addReportScreen(insertRow);
			 } else{
				 set_report_screen_id = reportScreen.get("set_report_screen_id", 0l);
 			 }
			 
 			 //handle report params
 			handLoadReceiveScreemParam(red, set_report_screen_id, SetReportScreenParamsTypeKey.Red);
 			handLoadReceiveScreemParam(yellow, set_report_screen_id, SetReportScreenParamsTypeKey.Yellow);
 			handLoadReceiveScreemParam(handleTime, set_report_screen_id, SetReportScreenParamsTypeKey.HandleTime);

		 }catch(Exception e){
			 throw new SystemException(e, "saveOrUpdateSetLoadReceiveScreemParams", log);
		 }
		
	}
	private void handLoadReceiveScreemParam(int value ,long set_report_screen_id , int type_key) throws Exception{
		try{
			DBRow param = floorCheckInMgrZr.getReportScreenParam(set_report_screen_id, type_key);
			DBRow addParam = new DBRow();
			addParam.add("set_report_screen_id", set_report_screen_id);
			addParam.add("value", value);
			addParam.add("type_key", type_key);
			if(param == null){
				floorCheckInMgrZr.addReportScreenParam(addParam);
			}else{
				floorCheckInMgrZr.updateReportScreenParam(param.get("set_report_screen_param_id", 0l), addParam);
			}
		}catch(Exception e){
			 throw new SystemException(e, "handLoadReceiveScreemParam", log);
		}
	}
	
	/**
	 * 返回LoadReceive Screen 设置的参数
	 * @param ps_id
	 * @param screenType
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	@Override
	public HoldThirdValue<DBRow, DBRow, DBRow> getLoadReceiveScreenParamSet(long  ps_id , int screenTypekey ) throws Exception{
		try{
			
			DBRow[] params = floorCheckInMgrZr.getSetReportScreenParam(ps_id,screenTypekey);
			DBRow handleTimeParam = null ;
			DBRow redColorParam = null ;
			DBRow yellowColorParam  = null ;
			if(params != null && params.length > 0){
				for(DBRow temp : params){
					int typeKey = temp.get("type_key", 0);
					switch (typeKey) {
						case SetReportScreenParamsTypeKey.HandleTime : handleTimeParam =  temp ; break;
						case SetReportScreenParamsTypeKey.Red : redColorParam =  temp ; break;
						case SetReportScreenParamsTypeKey.Yellow : yellowColorParam =  temp ; break;
					}
				}
 			}
			return new HoldThirdValue<DBRow, DBRow, DBRow>(handleTimeParam, redColorParam, yellowColorParam);
		}catch(Exception e){
			throw new SystemException(e, "getLoadReceiveScreenParamSet", log);
		}
	}
	/*@Override
	public void refreshLastRefreshTime(long adid) throws Exception {
		try{
			long time = new Date().getTime(); 
			DBRow updateRow = new DBRow();
			updateRow.add("last_refresh_time", time);
			fam.modifyAdmin(adid, updateRow);
		}catch(Exception e){
			throw new SystemException(e, "refreshLastRefreshTime", log);
		}
	}*/
	
	@Override
	public boolean isUserOnLine(DBRow row) throws Exception {
		try{
			if(row != null){
				int isOnline = row.get("is_online", 0);
 				if( isOnline == 1 ){
					return true ;
				}
			}
			return false ;
		}catch(Exception e){
			throw new SystemException(e, "isUserOnLine", log);
		}
	}
	@Override
	public boolean isUserOnLine(long adid) throws Exception {
		try{
			DBRow user = fam.getDetailAdmin(adid);
			return isUserOnLine(user);
		}catch(Exception e){
			throw new SystemException(e, "isUserOnLine", log);
		}
 		 
	}
	
	
	@Override
	public boolean checkAndroidLoginIsOnLine(String account) throws Exception {
		try{
			String users = getOnlineUserString();
			if(users != null){
				return users.indexOf(account)  >= 0 ;
			}
			return false ;
		}catch(Exception e){
			throw new SystemException(e, "isUserOnLine", log);
		}
 	}
	
 	@Override
	public DBRow[] countMemberUncompletedTask(int ps_id, int proJsId, int adgid, int[] processKeys) throws Exception {
		refreshOnLineUser();
 		DBRow[] datas = floorCheckInMgrZr.countMemberUncompletedTask(String.valueOf(ps_id) , String.valueOf(proJsId) , String.valueOf(adgid) , processKeys);
		if(datas != null && datas.length > 0){
			for(DBRow data : datas){
				data.add("UserOnLine", isUserOnLine(data) ? 1 : 0);
			}
		}
		return datas;
	}
 	

 	@Override
 	public String getOnlineUserString(){
 		try{
 			String onLineUserUrl =   "http://"+openFireSetBean.getAddress()+":9090/plugins/onlineusers/list" ;
			URL url = new URL(onLineUserUrl);
			URLConnection connection = url.openConnection();
			InputStream in  = connection.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuffer buffer = new StringBuffer();
			String str = reader.readLine() ;
			while(str != null){
				buffer.append(str);
				str = reader.readLine() ;
			}
			reader.close();
			String datas = buffer.toString();
			return datas.toString() ;
 		}catch(Exception e){
 			
 		}
		return "";
 		
 	}
 	/**
 	 * 获取所有的在线的人员
 	 * (现在实现的方式不太好) . 理想的方式 如果可以应该是在 Openfire 登录或者是下线了 ，操作admin表
 	 * 
 	 * 1.把在线的UserAccount 保存在 onLineAccounts 
 	 * 2.获取在线的Account然后再对比。
 	 * 3.更新下线的人 + 更新上线的
 	 * @author zhangrui
 	 * @Date   2015年1月28日
 	 */
 	private void refreshOnLineUser(){
 		try{
 		  	 
 			String datas =  getOnlineUserString();
 			List<String> onlineAccount = new ArrayList<String>();
 			if(datas != null && datas.length() > 2){
 				datas = datas.replace("\"", "");
 				datas = datas.substring(1, datas.length()-1);
 				if(datas.length() > 0){
 					String[] onlineUsers = datas.split(",");
 					for(String temp : onlineUsers){
 						String[] tempAccountArrays = temp.split("@");
 						if(tempAccountArrays != null && tempAccountArrays.length > 0){
 							onlineAccount.add(tempAccountArrays[0]);
 						}
 					}
 				}
 	 		}
 			
 			floorCheckInMgrZr.updateALLUserOffLine();
 			if(onlineAccount != null && onlineAccount.size() > 0){
 				DBRow updateRow = new DBRow();
 				updateRow.add("is_online", 1);
 				for(String temp : onlineAccount){
 		 			floorCheckInMgrZr.updateAdminByAccount(temp,updateRow);
 				}
 			}
 		}catch(Exception e){
 			log.error("Query online user Error");
 		}
 	}
 	/**
 	 * 查询仓库下superior的在线状态
 	 * <p>1. 若有在线的superior, 返回仓库下在线的superior
 	 * <p>2. 若无在线的superior, 返回仓库下所有的superior
 	 * @see com.cwc.app.iface.zr.CheckInMgrIfaceZr#getPresenceOfSuperior(java.lang.String)
 	 */
 	@Override
 	public DBRow[] getPresenceOfSuperior(long ps_id,long adgid) throws Exception {
 		refreshOnLineUser();
 		DBRow[] superoirs = floorCheckInMgrZr.selectUsersBy((int)ps_id, 15,(int) adgid, null);
 	 
 		List<DBRow> onLineSuperoirs = new ArrayList<DBRow>();
 		
		if(superoirs != null && superoirs.length > 0){
			for(DBRow superoir : superoirs){
				boolean onLine = isUserOnLine(superoir);
				superoir.add("UserOnLine", onLine ? 1 : 0);
				if(onLine){
					onLineSuperoirs.add(superoir);
				}
			}
		}
		return (onLineSuperoirs.size() < 1) ? superoirs : onLineSuperoirs.toArray(new DBRow[]{});
	}
 	/**
 	 * 当前这个人 需要Assign的Task
 	 */
 	@Override
 	public DBRow[] countMemberTaskRelEqptTask(long adid) throws Exception {
 		try{
 			DBRow[] datas =  floorCheckInMgrZr.getAdidNeedAssignEntry(adid);
 			if(datas != null && datas.length > 0){
 				DBRow[] result = new DBRow[datas.length];
 				for(int index = 0 , count = datas.length ; index < count ; index++ ){
 					DBRow temp = datas[index];
 					DBRow insertTemp = new DBRow();
 					long entry_id = temp.get("associate_main_id", 0l);
 					HoldDoubleValue<Integer, Integer> returnValue = getTotalAndNotAssignTask(entry_id);
 					insertTemp.add("entry_id", entry_id);
 					insertTemp.add("task_total", returnValue.a);
 					insertTemp.add("task_assigned", returnValue.b);
 					insertTemp.add("equipments", floorCheckInMgrZr.getEntryEquipmentsNoResouces(entry_id));
 					result[index] = insertTemp ;
 				}
 				return result ;
 			}else{
 				return new DBRow[0];
 			}
  		}catch(Exception e){
			throw new SystemException(e, "countMemberTaskRelEqptTask", log);
 		}
	}
 	/**
 	 * 返回 总共Task的数量以及已经分配的Task的数量
 	 * @param entry_id
 	 * @return
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2015年1月15日
 	 */
	private HoldDoubleValue<Integer, Integer> getTotalAndNotAssignTask(long entry_id ) throws Exception{
		try{
			int total = 0 ;
			int assign = 0 ;
			DBRow[] datas = floorCheckInMgrZr.getEntryDetailsByEntryId(entry_id);
			if(datas != null && datas.length > 0){
				total = datas.length ;
				for(DBRow temp : datas){
					String task_assign_loader_time = temp.getString("task_assign_loader_time");
					if(!StringUtil.isBlank(task_assign_loader_time)){
						assign++ ;
					}
				}
			}
			return new HoldDoubleValue<Integer, Integer>(total, assign);
		}catch(Exception e){
			throw new SystemException(e, "getTotalAndNotAssignTask", log);
		}
	}
	
	public DBRow[] getTaskProcessingByAdid(long adid) throws Exception{
		try{
 			DBRow[] datas =  floorCheckInMgrZr.getAdidTaskProcessingEntry(adid);
 			if(datas != null && datas.length > 0){
 				DBRow[] result = new DBRow[datas.length];
 				for(int index = 0 , count = datas.length ; index < count ; index++ ){
 					DBRow temp = datas[index];
 					DBRow insertTemp = new DBRow();
 					long entry_id = temp.get("associate_main_id", 0l);
 					HoldDoubleValue<Integer, Integer> returnValue = getTotalAndNotCloseTask(entry_id);
 					insertTemp.add("entry_id", entry_id);
 					insertTemp.add("task_total", returnValue.a);
 					insertTemp.add("task_closed", returnValue.b);
 					insertTemp.add("equipments", floorCheckInMgrZr.getEntryEquipmentsNoResouces(entry_id));
 					result[index] = insertTemp ;
 				}
 				return result ;
 			}else{
 				return new DBRow[0];
 			}
		}catch(Exception e){
			throw new SystemException(e, "getTaskProcessingByAdid", log);
		}
	}
	/**
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月15日
	 */
	private HoldDoubleValue<Integer, Integer> getTotalAndNotCloseTask(long entry_id ) throws Exception{
		try{
			int total = 0 ;
			int closed = 0 ;
			DBRow[] datas = floorCheckInMgrZr.getEntryDetailsByEntryId(entry_id);
			if(datas != null && datas.length > 0){
				total = datas.length ;
				for(DBRow temp : datas){
					int number_status = temp.get("number_status", 0);
					if(!(number_status == CheckInChildDocumentsStatusTypeKey.PROCESSING 
							|| number_status == CheckInChildDocumentsStatusTypeKey.UNPROCESS)){
						closed++ ;
					}
 					 
				}
			}
			return new HoldDoubleValue<Integer, Integer>(total, closed);
		}catch(Exception e){
			throw new SystemException(e, "getTotalAndNotCloseTask", log);
		}
	}
 	@Override
 	public DBRow[] searchGateDriverLiscense(String searchValue)
 			throws Exception {
 		try{
 			DBRow[] returnArrays = null ;
 			if(StringUtil.isNumber(searchValue)){//数字查询
 				returnArrays = floorCheckInMgrZr.searchGateDriverLiscenseByPhoneNumber(searchValue);
			}else{	//字母查询
				returnArrays = floorCheckInMgrZr.searchGateDriverLiscenseBy(searchValue);
			}
 			 return returnArrays == null ? new DBRow[0] : returnArrays ;
 		}catch(Exception e){
			throw new SystemException(e, "searchGateDriverLiscense", log);
 		}
  	}
	
 	@Override
 	public DBRow[] getEntryEquipmentsWithResources(long entry_id) throws Exception {
 		try{
 			DBRow[] results = floorCheckInMgrZr.getEntryEquipmentJoinResouces(entry_id);
 			if(results != null && results.length > 0){
 				List<DBRow> returnArray = new ArrayList<DBRow>();
  				for(int index = 0 , count = results.length ; index < count ; index++ ){
 					DBRow temp = results[index];
 					if(StringUtil.isNull(temp.getString("equipment_number"))){
 						continue ;
 					}
 					DBRow insertTemp = new DBRow();
 					insertTemp.add("equipment_type",temp.get("equipment_type",0));
 					
 					insertTemp.add("equipment_number",temp.getString("equipment_number"));
 					int resources_type = temp.get("resources_type",0) ;
 					long resources_id = temp.get("resources_id",0l) ;
 					insertTemp.add("resources_type",resources_type);
 					insertTemp.add("resources_id",resources_id);
 					insertTemp.add("occupy_status", temp.get("occupy_status", 0));
 					if(resources_type ==  OccupyTypeKey.DOOR){
 	 					insertTemp.add("resources_name", getDoorName(resources_id));
 					}
 					if(resources_type ==  OccupyTypeKey.SPOT){
 	 					insertTemp.add("resources_name", getSpotName(resources_id));
 					}
 					returnArray.add(insertTemp);
 				}
 				return returnArray.toArray(new DBRow[0]) ;
 			}
 			return new DBRow[0];
 		}catch(Exception e){
			throw new SystemException(e, "getEntryEquipmentsWithResources", log);
 		}
  	}
 	
	@Override
	public DBRow[] getTaskScanPallets(long detail_id ,  int number_type) throws Exception {
		try{
			DBRow[] datas = null ;
			DBRow[] task = floorCheckInMgrWfh.findTaskByIds(String.valueOf(detail_id));
			long ps_id = task[0].get("ps_id", 0L);
			if(number_type == ModuleKey.CHECK_IN_BOL 
					|| number_type == ModuleKey.CHECK_IN_CTN
					){
					  datas = floorCheckInMgrWfh.findReceivePallet(detail_id);
					  //floorCheckInMgrZr.getReceiveScanPallet(detail_id);
					  //更改字段与下面保持一致
					  for (DBRow data : datas) {
						String pallet_type_count = "";
						String pallet_number = "(94)"+StringUtil.formatNumber("#000000000000", data.get("container", 0L));
						data.add("pallet_number", pallet_number);
					//	data.add("pallet_type_count", 1);
					}
			}
			if(number_type == ModuleKey.CHECK_IN_LOAD 
					|| number_type == ModuleKey.CHECK_IN_ORDER
					|| number_type == ModuleKey.CHECK_IN_PICKUP_ORTHERS
					|| number_type == ModuleKey.CHECK_IN_PONO){
				
				  datas = floorCheckInMgrZr.getLoadScanPallet(detail_id);
			}
			if(number_type == ModuleKey.CHECK_IN_DELIVERY_ORTHERS){
				datas = floorCheckInMgrZr.getReceiveScanPallet(detail_id);
			}
			return fixReturnScanPallet(datas, false,ps_id);
		}catch(Exception e){
			throw new SystemException(e, "getTaskScanPallets", log);
		}
 	}
	/**
	 * 返回hlb 数据 ，  用资源来分组
	 * @param datas
	 * @param isLoad
	 * @return
	 * @author zhangrui
	 * @Date   2015年1月16日
	 */
	private DBRow[] fixReturnScanPallet(DBRow[] datas , boolean isLoad,long ps_id) throws Exception {
		try{
			DBRow[] returnArray = null ;
			if(datas != null && datas.length > 0 ){
				
	 			Map<HoldThirdValue<Long,Integer,String>, List<DBRow>> hashMapDatas = new HashMap<HoldThirdValue<Long,Integer,String>, List<DBRow>>();
				Map<HoldDoubleValue<Long, Integer> , String> resourcesNamesMap = new HashMap<HoldDoubleValue<Long, Integer> , String>();
	 			for(DBRow temp : datas){
					long resources_id = temp.get("resources_id", 0l);
					int resources_type = temp.get("resources_type", 0);
					HoldDoubleValue<Long, Integer> resouces =  new HoldDoubleValue<Long, Integer>(resources_id, resources_type);
					String insertMapResoucesName = resourcesNamesMap.get(resouces);
					if(insertMapResoucesName == null){
						String resouces_name = resources_type == OccupyTypeKey.SPOT ?  getSpotName(resources_id) : getDoorName(resources_id);
						resourcesNamesMap.put(resouces, resouces_name);
						insertMapResoucesName = resouces_name;
					}
					HoldThirdValue<Long, Integer, String> key = new HoldThirdValue<Long, Integer, String>(resources_id, resources_type, insertMapResoucesName);
					List<DBRow> insertDatas = hashMapDatas.get(key);
					if(insertDatas == null){
						insertDatas = new ArrayList<DBRow>();
					}
					temp.add("scan_time", DateUtil.showLocalparseDateTo24Hours(temp.get("scan_time", ""), ps_id));
					insertDatas.add(temp);
					hashMapDatas.put(key, insertDatas);
				}
	 			
	 			if(hashMapDatas.size() > 0){
	 				Iterator<Entry<HoldThirdValue<Long, Integer, String>, List<DBRow>>>  it = 
	 						hashMapDatas.entrySet().iterator() ;
	 				returnArray  = new DBRow[hashMapDatas.size()] ;
 					int index = 0 ;
	 				while(it.hasNext()){
 	 					Entry<HoldThirdValue<Long, Integer, String>, List<DBRow>> entry =	it.next() ;
 	 					DBRow temp = new DBRow();
 	 					HoldThirdValue<Long, Integer, String> keyOfResouces = entry.getKey();
 	 					temp.add("resources_name", keyOfResouces.c);
 	 					temp.add("resources_type", keyOfResouces.b);
 	 					temp.add("datas", entry.getValue().toArray(new DBRow[0]));

 	 					returnArray[index] = temp ;
 	 							index++ ;
	 				}
	 			}
	 			
			}
			return returnArray ;
		}catch(Exception e){
			throw new SystemException(e, "fixReturnScanPallet", log);
		}
		
	}
	@Override
	public boolean isEquipmentHasResouces(long resouces_id, int resouces_type,
			long equipment_id) throws Exception {
		try{
			int count = floorCheckInMgrZr.getEquipmentAndResouces(  equipment_id ,   resouces_id ,   resouces_type);
			return count > 0 ;
		}catch(Exception e){
			throw new SystemException(e, "isEquipmentHasResouces", log);
		}
 	}
 
	@Override
	public DBRow[] getEquipmentJoinResouces(String equipment_number,
			int equipment_type) throws Exception {
		try{
			DBRow[]  returnDatas = floorCheckInMgrZr.getUnLeftEquipmentJoinResouces(equipment_number,equipment_type);
			if(returnDatas != null && returnDatas.length > 0 ){
				for(DBRow data : returnDatas) {
					long rs_type = data.get("resources_type", 0L);
					if(rs_type == OccupyTypeKey.DOOR){
						//data.add("doorId", getDoorName(data.get("resources_id", 0L)));
						data.add("resources_name", getDoorName(data.get("resources_id", 0L)));
					}
					if(rs_type == OccupyTypeKey.SPOT){
						//data.add("yc_no", getSpotName(data.get("resources_id", 0L)));
						data.add("resources_name", getSpotName(data.get("resources_id", 0L)));
					}
				}
			}
			return returnDatas;
		}catch(Exception e){
			throw new SystemException(e, "getEquipmentJoinResouces", log);
		}
 	 
	}
	/**
	 * 查询Entry的车头 ，如果车头离开了，那么Entry就离开了
	 */
	@Override
	public void checkGateCheckOutEntryHasLeft(long entry_id)
			throws CheckInEntryIsLeftException, CheckInNotFoundException, Exception {
 			try{
 				DBRow entry =  floorCheckInMgrZwb.findMainById(entry_id) ; 
 				if(entry == null){
 					throw new CheckInNotFoundException();
 				}
 				String check_out_time = entry.getString("check_out_time");
 				if(!StringUtil.isBlank(check_out_time)){
 					throw new CheckInEntryIsLeftException();
 				}
 				
 			}catch(CheckInEntryIsLeftException e){
 				throw  e ;
 			}catch(Exception e){
 				throw new SystemException(e, "checkGateCheckOutEntryHasLeft", log);
 			}
	}
	
	/**
	 * 添加设备 checkOut的日志
	 * 同时CheckOut 设备
	 * @param equipmentJoinSpaceRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月25日
	 */
	private String checkOutEquipmentAndLogs(DBRow equipmentJoinSpaceRow)  throws Exception {
		try{
			//checkout 设备
			long equipment_id = equipmentJoinSpaceRow.get("equipment_id", 0l);
			DBRow updateEqipmentRow = new DBRow();
			updateEqipmentRow.add("check_out_time", DateUtil.NowStr());
			updateEqipmentRow.add("equipment_status", CheckInMainDocumentsStatusTypeKey.LEFT);
			floorCheckInMgrZr.updateEquipmentBy(equipment_id,updateEqipmentRow); 
			
			String formateStr = "%s:%s CheckOut" ;
			String equipmentLogs =  String.format(formateStr, 
					checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipmentJoinSpaceRow.get("equipment_type", 0))
					,equipmentJoinSpaceRow.getString("equipment_number")) ; 
			
			int resources_type = equipmentJoinSpaceRow.get("resources_type", 0);
			long resources_id = equipmentJoinSpaceRow.get("resources_id", 0l);
			if(resources_type > 0 && resources_id > 0){
				String formateRelease = "Release:【%s:%s】";
				String resoucesLogs = "";
				if(resources_type == OccupyTypeKey.DOOR){
					resoucesLogs = String.format(formateRelease, "Door",getDoorName(resources_id));
				}else{
					resoucesLogs = String.format(formateRelease, "Spot",getDoorName(resources_id));
				}
				equipmentLogs  = equipmentLogs + ","+resoucesLogs ;
			}
			return equipmentLogs ; 
			  
		}catch(Exception e){
			 throw new SystemException(e, "getCheckOutEquipmentLogs", log);
		}
	}
	/**
	 * 		Forget Task【Load:343434】
	 * @param equipmentJoinSpaceRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月25日
	 */
	private String checkOutEquipmentTaskAndLogs(DBRow equipmentJoinSpaceRow) throws Exception{
		try{
			long equipment_id = equipmentJoinSpaceRow.get("equipment_id", 0l);
			DBRow[] tasks = floorCheckInMgrZr.getEquipmentNotFinishTasks(equipment_id);
			StringBuffer sb = new StringBuffer();
			if(tasks != null && tasks.length > 0){
				String formate = " 		Forget Task【1s%:2s%】" ;
				DBRow updateTaskRow = new DBRow();
				updateTaskRow.add("is_forget_task", ForgetTask);
				for(DBRow temp : tasks){
					String numberTypeString = moduleKey.getModuleName(temp.get("number_type", 0));
					sb.append(",").append("		Forget Task【"+numberTypeString+":"+temp.getString("number")+"】");
					long detail_id = temp.get("dlo_detail_id", 0l);
					floorCheckInMgrZwb.updateDetailByIsExist(detail_id, updateTaskRow);
 				}
			}
 			return sb.toString();
		}catch(Exception e){
			throw new SystemException(e, "checkOutEquipmentTaskAndLogs", log);
		}
	}
	/**
	 * 1.checkOut 设备 ， 设备上面如果有没有完成的Task  标记成Forget 
	 * 2.设备CheckOutTime ， 但是没有CheckOut的原因,无CheckOut 类型 ， 没有check_out_entry_id
	 * 3.标记状态为离开。
	 * 4.释放设备占用的资源
	 * 5.记录日志
	 * @param equipmentis
	 * @param operationType	 那里操作的 GateCheckIn
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月25日
	 */
	@Override
	public void checkOutSameEquipments(String equipmentiids ,   long entry_id  ,long adid ) throws Exception{
		try{
			
			/**
			 * tractor:223223 CheckOut
			 * Release:【Door:35】
			 * 		Forget Task【Load:343434】
			 * 		Forget Task【Load:343434】
			 * 		Forget Task【Load:343434】
			 *	    Forget Task【Load:343434】
			 *
			 *
			 */
			StringBuffer logs = new StringBuffer();
			
			if(!StringUtil.isBlank(equipmentiids)){
				String[] equipments = equipmentiids.split(",");
				for(String equipmentStrId : equipments){
					long equipment_id = Long.parseLong(equipmentStrId);
					DBRow equipmentJoinSpaceRow =  floorCheckInMgrZr.getEquipmentWithResources(equipment_id);
					//设备CheckOut
					String equipmentLogs = checkOutEquipmentAndLogs(equipmentJoinSpaceRow);
					logs.append(equipmentLogs);
					
					//标记TaskForget 
					String taskLogs = checkOutEquipmentTaskAndLogs(equipmentJoinSpaceRow);
					logs.append(taskLogs);
				}
			}
			if(logs.length() > 0){
				checkInMgrZwb.addCheckInLog(adid, "Gate Check In", entry_id, CheckInLogTypeKey.GATEIN, DateUtil.NowStr(), logs.toString());
			}
		}catch(Exception e){
			throw new SystemException(e, "checkOutSameEquipments", log);
		}
	}
	@Override
	public void convertGateDriverLiscense() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * GateCheckOut 的时候添加设备通过EntryId搜索 ,
	 * 搜索这个EntryId 没有离开的 车尾设备，同时带出他所在的资源
	 * @param entry_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年2月12日
	 */
	@Override
	public List<DBRow> searchGateCheckOutEquipmentByEntryId(long entry_id , AdminLoginBean adminLoginBean) throws Exception{
		try{
		 
			DBRow[] equipments = floorCheckInMgrZr.getEntryEquipmentJoinResouces(entry_id);
			List<DBRow> result = new ArrayList<DBRow>();
			if(equipments != null && equipments.length > 0){
				for(DBRow temp : equipments){
 					if(temp.get("equipment_type", 0) == CheckInTractorOrTrailerTypeKey.TRAILER
							&& temp.get("equipment_status", 0 ) != CheckInMainDocumentsStatusTypeKey.LEFT
							&& temp.get("ps_id", 0 )  == adminLoginBean.getPs_id()){
							int resoucesType =	temp.get("resources_type", 0) ;
							long resources_id = temp.get("resources_id", 0l);
							temp.add("location", "");
							temp.add("resources_name", "");
							
							
							//copy 
							DBRow[] sealRows = checkInMgrZwb.getTaskByEquipmentId(temp.get("equipment_id", 0l));
							if(sealRows.length > 0)
							{
								temp.add("is_disabled", "false");
								temp.add("now_seal", sealRows[0].get("seal_pick_up","NA"));
							}
							else
							{
								temp.add("is_disabled", "true");
							}
							
							
							
							if(resources_id > 0l){
								if(resoucesType == OccupyTypeKey.DOOR){
									String doorName = getDoorName(resources_id) ;
									temp.add("location", "Door:"+ doorName);				
									temp.add("resources_name", doorName);

								}
								if(resoucesType == OccupyTypeKey.SPOT){
									String spotName = getSpotName(resources_id) ;
									temp.add("location", "Spot:"+ spotName);
									temp.add("resources_name", spotName);
								}
							}
							temp.add("equipment_type_msg", "TRAILER");
							temp.add("is_search", 0);
							result.add(temp);
					}
				}
			}
			return result ;
		}catch(Exception e){
			throw new SystemException(e,"searchGateCheckOutEquipmentByEntryId",log);
		}
	}
	
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setFloorCheckInMgrZr(FloorCheckInMgrZr floorCheckInMgrZr) {
		this.floorCheckInMgrZr = floorCheckInMgrZr;
	}

	public void setFloorCheckInMgrZJ(FloorCheckInMgrZJ floorCheckInMgrZJ) {
		this.floorCheckInMgrZJ = floorCheckInMgrZJ;
	}

	public void setStorageDoorMgrZr(StorageDoorMgrIfaceZr storageDoorMgrZr) {
		this.storageDoorMgrZr = storageDoorMgrZr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}

	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}

	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr) {
		this.floorFileMgrZr = floorFileMgrZr;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}

	public void setLoadBarUseMgrZr(LoadBarUseMgrIfaceZr loadBarUseMgrZr) {
		this.loadBarUseMgrZr = loadBarUseMgrZr;
	}

	public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}

	public void setFam(FloorAdminMgr fam) {
		this.fam = fam;
	}

	public void setOpenFireSetBean(OpenFireSetBean openFireSetBean) {
		this.openFireSetBean = openFireSetBean;
	}

	public void setAndroidPrintMgrZr(AndroidPrintMgrIfaceZr androidPrintMgrZr) {
		this.androidPrintMgrZr = androidPrintMgrZr;
	}
	
	public void setCheckInMgrWfh(CheckInMgrIfaceWfh checkInMgrWfh) {
		this.checkInMgrWfh = checkInMgrWfh;
	}
	
	
}
