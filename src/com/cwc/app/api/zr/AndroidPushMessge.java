package com.cwc.app.api.zr;

import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

public class AndroidPushMessge { 

 	private int moduleKey  = 0 ; 	//代表是那个模块的
	private int processKey  = 0 ; 	//具体模块下的，具体业务
	private long associate_id = 0l  ; //关联的业务的ID
	private long associate_main_id = 0l ;	//消息关联的主的Id
 	private String content ;				//消息的内容
 	private String title ; 					//定义消息的Title
 	private int messageType ; 				//消息的类型 , 1:执行类,2 通知类 , 3:消息导致android执行一段代码(待定) 2015-01-29 这个选项待定
	private JSONObject data = new JSONObject();
	 /**
	  * 定义返回的PushMessage的类型
	  * {
	  * 			moduleKey : 
	  * 			processKey :
	  * 			associate_id:
	  * 			data:json
	  * }
	  * @param moduleKey
	  * @param processKey
	  * @param associate_id
	  * @param data
	 * @throws JSONException 
	  */
	private AndroidPushMessge( int moduleKey, int processKey,
			long associate_id ,String title , long associate_main_id , String content , String header , int message_type) throws JSONException {
		super();
 		this.moduleKey = moduleKey;
		this.processKey = processKey;
		this.associate_id = associate_id;
 		this.content = content ;
 	
 		data.put("process_key", String.valueOf(processKey));
 		data.put("associate_id", String.valueOf(associate_id));
 		data.put("associate_main_id", String.valueOf(associate_main_id));
 		data.put("message_type", String.valueOf(message_type)) ;
 		data.put("title", String.valueOf(title));			//显示到Title
 		data.put("header", String.valueOf(header));			//显示到Header
 		data.put("content", content);
 		data.put("module_key", String.valueOf(moduleKey));

		 
 	}

	
	public static AndroidPushMessge getPushMessage(DBRow schedule) throws JSONException{
		int associate_process  =  schedule.get("associate_process", 0);
		int messageType = 2 ;
		if(associate_process == ProcessKey.GateHasTaskNotifyWindow || 
				associate_process == ProcessKey.CHECK_IN_WINDOW ||
				associate_process== ProcessKey.CHECK_IN_WAREHOUSE ||
				associate_process== 61 || //assign    supervisor
				associate_process== 62		//assign	scan
				){
			messageType = 1 ;
		}
		String header = "Schedule ["+schedule.get("schedule_id", 0l)+"]" ;
		return new AndroidPushMessge(
				schedule.get("associate_type", 0),
				schedule.get("associate_process", 0),
				schedule.get("associate_id", 0),
				schedule.getString("schedule_overview"),
				schedule.get("associate_main_id",0l),
				schedule.getString("schedule_detail"),
				"",
				messageType);
		
	}
	
  


	public int getModuleKey() {
		return moduleKey;
	}


	public int getProcessKey() {
		return processKey;
	}


	public long getAssociate_id() {
		return associate_id;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public JSONObject getData() {
		return data;
	}


	public int getMessageType() {
		return messageType;
	}
	
	
 }
