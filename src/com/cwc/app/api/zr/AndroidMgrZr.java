package com.cwc.app.api.zr;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.zip.DataFormatException;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cwc.app.api.zl.ProductBaseDataMgrZYZ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.android.ContainerNotFullException;
import com.cwc.app.exception.android.ContainerTypeNotFoundException;
import com.cwc.app.exception.android.ProductPictureDeleteException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.qa.ProductNotFindException;
import com.cwc.app.floor.api.zr.FloorAndroidMgrZr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.ContainerProductMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.app.iface.zr.ClpTypeMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.TempContainerMgrIfaceZr;
import com.cwc.app.iface.zyj.ContainerMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ProductFileTypesKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.ContainerNode;
import com.cwc.util.FileUtil;
import com.cwc.util.Tree;
import com.fr.data.core.DataUtils;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class AndroidMgrZr implements AndroidMgrIfaceZr{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorAndroidMgrZr floorAndroidMgrZr ;
	
	private FileMgrIfaceZr fileMgrZr ;
	
	private ProductMgrIFace productMgr;
	
	private CatalogMgrIFace catalogMgr;
	
	private FloorTransportMgrZr floorTransportMgrZr ;
	
	private ProductBaseDataMgrIFaceZYZ baseDataMgrZyz; 
	
	/*166635 , BAL/D/12V/35W ,	BALLAST/D/12V/35W , 101761
	155116 , HD/BR/180 , HDMI/BRAIDED/180 , 101761*/
	
	private ContainerMgrIFaceZyj containerMgrZyj ;
	
	private ContainerProductMgrIFaceZJ containerProductMgrZj ;
	
	private BoxTypeMgrIfaceZr boxTypeMgrZr ;
	
	private ClpTypeMgrIfaceZr clpTypeMgrZr ;
	 
	private TempContainerMgrIfaceZr tempContainerMgrZr;
	 
	public void setContainerProductMgrZj( ContainerProductMgrIFaceZJ containerProductMgrZj) {
		this.containerProductMgrZj = containerProductMgrZj;
	}

	
	public void setContainerMgrZyj(ContainerMgrIFaceZyj containerMgrZyj) {
		this.containerMgrZyj = containerMgrZyj;
	}

	
	public void setFloorAndroidMgrZr(FloorAndroidMgrZr floorAndroidMgrZr) {
		this.floorAndroidMgrZr = floorAndroidMgrZr;
	}

	public void setBaseDataMgrZyz(ProductBaseDataMgrIFaceZYZ baseDataMgrZyz) {
		this.baseDataMgrZyz = baseDataMgrZyz;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}
	
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	@Override
	public DBRow[] queryTransportOutBoundByTransportId(long transport_id)
			throws Exception {
		 try{
			 floorAndroidMgrZr.updateTransportState(TransportOrderKey.RECEIVEING,transport_id);
			 return floorAndroidMgrZr.getTransportOutBound(transport_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"queryTransportOutBoundByTransportId",log);
		}
	}

	@Override
	public DBRow[] getProductCodeForTransport(long transport_id) throws Exception {
		try{
			 return floorAndroidMgrZr.getProductCodeForTransport(transport_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getProductCodeForTransport",log);
		}
	}

	@Override
	public DBRow[] getProductForTransport(long transport_id) throws Exception {
		try{
			 return floorAndroidMgrZr.getProductForTransport(transport_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getProductForTransport",log);
		}
	}

	@Override
	public DBRow[] getSerialProductForTransport(long transport_id) throws Exception {
		try{
			 return floorAndroidMgrZr.getSerialProductForTransport(transport_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getSerialProductForTransport",log);
		}
	}

	/**
	 * 1.首先查询改Lp上的商品。然后计算出该商品的大小 体积
	 * 2.然后查询出这个LP 的type .
	 * 3.根据type计算这个LP 还剩多少的大小
	 *  体积cm3(返回结果换算成m3)  重量kg
	 */
	@Override
	public DBRow getLpInfo(long cp_lp_id) throws Exception {
			DBRow result = new DBRow();
			result.add("flag", "success");
		try{
			DBRow LpInfo  = floorAndroidMgrZr.getContainerTypeInfo(cp_lp_id);
			if(LpInfo == null) {
				result.add("flag", "nolp");
				return result ;
			}
			DBRow lpTotalInfo = floorAndroidMgrZr.getContainerProductTotalInfo(cp_lp_id);
			double total_weight = lpTotalInfo.get("total_weight", 0.0d);
			double total_v = lpTotalInfo.get("total_v", 0l) / 1000.0; 	//cm3 商品的是mm3,所以除以了1000.0
			
			double lpMaxLoad = LpInfo.get("max_load", 0.0d);
			double lpV = LpInfo.get("max_height", 0.0d) * LpInfo.get("length", 0.0d) * LpInfo.get("width", 0.0d); //cm3
			
			result.add("left_weight", (lpMaxLoad - total_weight) );
			result.add("left_volume", (lpV - total_v)/ 1000000);
			result.add("lp", cp_lp_id);
			result.add("type_name", LpInfo.getString("type_name"));
			result.add("lp_name", LpInfo.getString("container"));
			return result ;
		}catch (Exception e) {
			 throw new SystemException(e,"getLpInfo",log);
		}
 		 
	}
	@Override
	public DBRow getLpInfo(String lpName) throws Exception {
			DBRow result = new DBRow();
			result.add("flag", "success");
		try{
			DBRow LpInfo  = floorAndroidMgrZr.getContainerTypeInfo(lpName);
			if(LpInfo == null) {
				result.add("flag", "nolp");
				return result ;
			}
			DBRow lpTotalInfo = floorAndroidMgrZr.getContainerProductTotalInfo(LpInfo.get("con_id", 0l));
			double total_weight = lpTotalInfo.get("total_weight", 0.0d);
			double total_v = lpTotalInfo.get("total_v", 0l) / 1000.0; 	//cm3 商品的是mm3,所以除以了1000.0
			
			double lpMaxLoad = LpInfo.get("max_load", 0.0d);
			double lpV = LpInfo.get("max_height", 0.0d) * LpInfo.get("length", 0.0d) * LpInfo.get("width", 0.0d); //cm3
			
			result.add("left_weight", (lpMaxLoad - total_weight) );
			result.add("left_volume", (lpV - total_v)/ 1000000);
			result.add("lp", LpInfo.get("con_id", 0l));
			result.add("type_name", LpInfo.getString("type_name"));
			result.add("lp_name", LpInfo.getString("container"));
			return result ;
		}catch (Exception e) {
			 throw new SystemException(e,"getLpInfo",log);
		}
 		 
	}

	@Override
	public DBRow[] getCheckInTransport(long psid) throws Exception {
		try{
			return floorAndroidMgrZr.getCheckInTransport(psid);
		}catch (Exception e) {
			 throw new SystemException(e,"getCheckInTransport",log);
		}
 	 
	}
	
	public DBRow getSupplier(long purchase_id)throws Exception{
		try{
			return this.floorAndroidMgrZr.getSupplier(purchase_id);
		}catch(Exception e){
			throw new SystemException(e,"getSupplier",log);
		}
	}
	
//	public DBRow[] getPositionedTransportAddDoors(long id) throws Exception{
//		try{
//			return floorAndroidMgrZr.getPositionedTransportAddDoors(id);
//		}catch (Exception e) {
//			 throw new SystemException(e,"getCheckInTransport",log);
//		}
//	}

	@Override
	public DBRow[] getOutboundTransport(long psid) throws Exception {
		try{
			return floorAndroidMgrZr.getTransportNotPurcharsId(psid);
		}catch (Exception e) {
			 throw new SystemException(e,"getOutboundTransport",log);		
		}
	}

	@Override
	public DBRow[] getTransportDetail(long transport_id) throws Exception {
		try{
			return floorAndroidMgrZr.getTransportDetailBy(transport_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getTransportDetail",log);		
		}
	}

	@Override
	public DBRow[] getTransportReceived(long psid ) throws Exception {
		try{
			return floorAndroidMgrZr.getPositionedTransport(psid);
		}catch (Exception e) {
			throw new SystemException(e,"getTransportReceived",log);
		}
		 
	}

	@Override
	public DBRow[] getPositionProduct(long transport_id) throws Exception {
		try{
		
			return floorAndroidMgrZr.getPositionProduct(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getPositionProduct",log);
		}
	}

	@Override
	public DBRow[] getPositionProductCode(long transport_id) throws Exception {
		try{
			
			return floorAndroidMgrZr.getPositionProductCode(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getPositionProductCode",log);
		}
	}

	@Override
	public DBRow[] getPositionSerialProduct(long transport_id) throws Exception {
		try{
			return floorAndroidMgrZr.getPositionSerialProduct(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getPositionSerialProduct",log);
		}
  
	}

	@Override
	public DBRow[] getTransportWarehouseProduct(long transport_id)
			throws Exception {
		try{
			return floorAndroidMgrZr.getTransportWarehouse(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getTransportWarehouseProduct",log);
		}
 	}

	@Override
	public void handleUploadFile(String file_with_id, String file_with_type,
			String file_with_class, String upload_adid,String upload_time, String path , List<FileItem> fileItems) throws Exception {
		 try{
			 String fileFolderTb = Environment.getHome().replace("\\", "/")+"."+ "/upload/thumbnail/";
			 for(FileItem item : fileItems){
				 if(!item.isFormField()){
					 StringBuffer filePath = new StringBuffer();
					 filePath.append(Environment.getHome()).append("upload/").append(path).append("/");
					 File file = new File(filePath.toString(), item.getName());
					 if(file.exists()){file.delete();}
					 item.write(file);
					 int successFlag =  makeSmallImage(file, fileFolderTb  +  item.getName() );
					 if(successFlag == 0){
						 throw new SystemException("make small Image Failed");
					 }
					 DBRow fileRow = new DBRow();
					 fileRow.add("file_name", item.getName());
					 fileRow.add("file_with_id", file_with_id);
					 fileRow.add("file_with_type", file_with_type);
					 fileRow.add("file_with_class", file_with_class.trim().length() < 1 ? 0 :Integer.parseInt(file_with_class.trim()));
					 fileRow.add("upload_adid", upload_adid);
					 fileRow.add("upload_time", upload_time);
					 fileMgrZr.saveFile(fileRow);
				 }
			 }
			 
		 }catch (Exception e) {
			throw new SystemException(e,"handleUploadFile",log);
		}
	}

	@Override
	public DBRow[] getOutboundProductInfo(long transport_id) throws Exception {
		try{
			return floorAndroidMgrZr.getOutboundProductInfo(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getOutboundProductInfo",log);

 		}
 	}

	@Override
	public DBRow[] getProductCodeForOutboundTransport(long transport_id)
			throws Exception {
		try{
			return floorAndroidMgrZr.getProductCodeForOutboundTransport(transport_id);
		}catch (Exception e) {
			throw new SystemException(e,"getProductCodeForOutboundTransport",log);
		}
 	}

	@Override
	public DBRow[] getSerialProductForOutboundTransport(long transport_id) throws Exception {
	 	try{
			return floorAndroidMgrZr.getSerialProductForOutboundTransport(transport_id);
	 	}catch (Exception e) {
			throw new SystemException(e,"getSerialProductForOutboundTransport",log);
		}
	}

	@Override
	public DBRow[] getDoorNameByTransportId(long transport_id , int type) throws Exception {
		try{
			DBRow[] result = floorAndroidMgrZr.getDoorName(transport_id , type);
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"getDoorNameByTransportId",log);
 		}
	}

	@Override
	public DBRow[] getLocationNameByTransportId(long transport_id , int type) throws Exception {
		try{
			DBRow[] result = floorAndroidMgrZr.getLocationName(transport_id,type);
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"getLocationNameByTransportId	",log);
 		}
	}

	@Override
	public DBRow[] getTransportByMachine(long ps_id) throws Exception {
		try{
			return floorAndroidMgrZr.getTransportByMachine(ps_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getOutboundTransport",log);		
		}
	}
		@Override
	public DBRow[] getProductFileBy( long maxPcid, int limit)
			throws Exception {
			try{
				DBRow[] rows = floorAndroidMgrZr.getProductFileBy( maxPcid, limit);
				
				//在查询file文件。如果没有商品本身的图片那么就随意下载一张图片(优先选择商品本身)
				if(rows != null && rows.length > 0){
					Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
					int file_with_type = FileWithTypeKey.PRODUCT_SELF_FILE ;
					int product_file_type = ProductFileTypesKey.self ;
					for(DBRow product : rows){
						
						
						//查询pcode,title(这里以后改)
						long pc_id = product.get("pc_id", 0l);
						DBRow result = productMgr.getDetailProductByPcid(pc_id);
						if(result != null){
							product.add("union_flag", result.get("union_flag", 0l));
							product.add("volume", result.get("volume", 0.0d));
							product.add("p_code", result.getString("p_code"));
							product.add("catalog_id", result.get("catalog_id", 0L));						
						}
						//查询图片
						DBRow file = floorAndroidMgrZr.getProductPictureBy(pc_id, file_with_type, product_file_type);
						if(file == null){
							file = floorAndroidMgrZr.getProductPictureBy(pc_id, file_with_type, null);
						}
						if(file != null){
							product.add("file_name", file.getString("file_name"));
						}
						
						if(result != null){
							DBRow allFather[] = tree.getAllFather(result.get("catalog_id",0l));
							String catalog_title="";
							for (int i=0; i<allFather.length-1; i++)
						  	{
								catalog_title += allFather[i].getString("title")+",";
						  	}
							
							DBRow catalog = catalogMgr.getDetailProductCatalogById(result.get("catalog_id",0l));
							if(catalog!=null)
						  	{
								catalog_title +=catalog.getString("title");
								
						  	}
							product.add("catalog_title", catalog_title);
						}
						
					}
				}
				return rows;
			}catch (Exception e) {
				throw new SystemException(e,"getProductFileBy	",log);
			}
			
	}
	@Override
	public DBRow[] getProductFileDifferent(long pcid, String fileNames) throws Exception {
		try{
 			List<DBRow> listArray = new ArrayList<DBRow>();
 			DBRow[] all = floorAndroidMgrZr.getAllFile(pcid, FileWithTypeKey.PRODUCT_SELF_FILE);
			if(all != null && all.length > 0)
				for(DBRow temp : all ){
					String tempName = temp.getString("file_name");
					if(fileNames.indexOf(tempName) == -1){
						listArray.add(temp);
					}
				}
			return  listArray.toArray(new DBRow[listArray.size()]);
		}catch (Exception e) {
			throw new SystemException(e,"getProductFileDifferent	",log);
 		}
 	}
	@Override
	public DBRow[] getFileByFileWithIdAndFileWithType(long file_with_id,
			int file_with_type , String fileNames) throws Exception {
		try{
			List<DBRow> listArray = new ArrayList<DBRow>();
 			DBRow[] all = fileMgrZr.getFilesByFileWithIdAndFileWithType(file_with_id, file_with_type);
			if(all != null && all.length > 0)
				for(DBRow temp : all ){
					String tempName = temp.getString("file_name");
					if(fileNames.indexOf(tempName) == -1){
						listArray.add(temp);
					}
				}
			return  listArray.toArray(new DBRow[listArray.size()]);
		}catch (Exception e) {
			throw new SystemException(e,"getFileByFileWithIdAndFileWithType",log);
 		}
 	}

	@Override
	public void clearDoorAndLocation(long transportId, int type) throws Exception {
		try{
			 /**
			  * 释放门的操作
			  */
			floorAndroidMgrZr.clearDoor(transportId, type, DateUtil.NowStr());
			floorAndroidMgrZr.clearLocation(transportId, type,  DateUtil.NowStr());
		}catch (Exception e) {
			throw new SystemException(e,"clearDoorAndLocation",log);
 		}
	}

	@Override
	public long addNewLp(String lpName , long bill_id , int bill_type) throws Exception {
		try{
			if(floorAndroidMgrZr.countContainerTypeAndBillId(lpName, bill_id, bill_type) > 0){
				return 0l ;
			}
			return floorAndroidMgrZr.addNewLp(lpName, bill_id, bill_type);
			
		}catch (Exception e) {
			throw new SystemException(e,"addNewLp",log);
		}
	}

	@Override
	public long addBaseProductByzip(String parentDir,AdminLoginBean adminLoggerBean ) throws Exception {
		long pid = 0l;
		try{
			File dirs = new File(parentDir);
			if(dirs.exists()){
				File[] listFile = dirs.listFiles();
				List<File> pictures = new ArrayList<File>();
				File productXml = null ;
				File fileXml =  null ;
				if(listFile != null){
					for(File tempFile : listFile){
						if(tempFile.getName().equals("product.xml")){
							productXml = tempFile;
						}else if(tempFile.getName().equals("fileXml.xml")){
							fileXml = tempFile;
						}else{
 							pictures.add(tempFile);
						}
					}
				}
				if(productXml == null){
					throw new ProductNotFindException();
				}
				pid = createProduct(productXml,adminLoggerBean,fileXml,pictures);
			}
			return pid ;
		}catch(ProductNameIsExistException e)//商品名称已经存在
		{
			throw e;
		}
		catch(ProductCodeIsExistException	e)//商品条码已经存在
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"addBaseProductByzip",log);
 		}
 	}
	private long createProduct(File productXml ,AdminLoginBean	adminLoggerBean , File fileXml ,List<File> pictures) throws ProductNameIsExistException,ProductCodeIsExistException ,XMLDataErrorException, Exception{
		long pc_id = 0l ;
		try{
			//解析xml 文件
			String productXmlSt = getPost(new FileInputStream(productXml));
			pc_id = baseDataMgrZyz.addProductXML(productXmlSt, adminLoggerBean);
			//添加条码信息(addProduct的时候已经添加) //zhangrui
			//baseDataMgrZyz.saveProductCodeByXml(productXmlSt, pc_id, adminLoggerBean.getAdid());
			//添加图片信息,解析FileXML文件。然后读取图片文件
			
			saveProductFile(fileXml,pictures,pc_id,adminLoggerBean.getAdid());
			removeFile( pictures); 
			return pc_id ;
		}catch(ProductNameIsExistException e)//商品名称已经存在
		{
			throw e;
		}
		catch(ProductCodeIsExistException	e)//商品条码已经存在
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"productXml",log);
 		} 
	}
	private void removeFile(List<File>  pictures) throws Exception {
		try{
			if(pictures != null){
			
			 StringBuffer filePath = new StringBuffer();
			 filePath.append(Environment.getHome()).append("upload/").append("product").append("/");
			 for(File file : pictures){
				 copyProductFile(file,filePath.toString());
			 }
			}
		}catch (Exception e) {
			throw new SystemException(e,"removeFile",log);
		}
	}
	private void copyProductFile(File file , String parentPath) throws Exception{
		 OutputStream outputStream  = null ;
		 FileInputStream inputStream = null ;
		try{
			 File Outfile = new File(parentPath, file.getName());
			 if(Outfile.exists()){Outfile.delete();}
			 Outfile.createNewFile();
			 inputStream = new FileInputStream(file);
			 outputStream = new FileOutputStream(Outfile);
			 int length = -1;
			 byte[] buffered = new byte[1024 * 50];
			 while((length = inputStream.read(buffered)) != -1){
				 outputStream.write(buffered, 0, length);
			 }
		}catch (Exception e) {
			throw new SystemException(e,"copyProductFile",log);
 
 		}finally{
 			if(outputStream != null){outputStream.close();}
 			if(inputStream != null){inputStream.close();}
 		}
	}
	private void saveProductFile(File fileXml ,List<File> pictures , long pc_id , long adid ) throws Exception{
		if(fileXml == null){return ;}
		DocumentBuilderFactory docBuilderFactory = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try{
			String upload_time = DateUtil.NowStr();
 			docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.parse(fileXml);
			Element root = doc.getDocumentElement();
 			
 			NodeList FileItem = root.getElementsByTagName("FileItem");
			for(int index = 0 , count = FileItem.getLength() ; index < count ; index++ ){
				Element temp =(Element)FileItem.item(index);
				String FileName = getXmlElementStringValue(temp.getElementsByTagName("FileName"));
				String FileWithType = getXmlElementStringValue(temp.getElementsByTagName("FileWithType"));
				String FileWithClass = getXmlElementStringValue(temp.getElementsByTagName("FileWithClass"));
				DBRow tempRow = new DBRow();
				tempRow.add("pc_id", pc_id);
				tempRow.add("file_name", FileName);
				tempRow.add("file_with_id", pc_id);
				tempRow.add("file_with_type", FileWithType);
				tempRow.add("product_file_type", FileWithClass.trim().length() < 1 ? 0 :Integer.parseInt(FileWithClass.trim()));
				tempRow.add("upload_adid", adid);
				tempRow.add("upload_time", upload_time);
				floorTransportMgrZr.addProductFile(tempRow);
 			}
	         
			}catch (Exception e) {
				throw new SystemException(e,"saveProductFile",log);
	 		}finally{
	 			doc = null ;
	 			docBuilder = null ;
	 			docBuilderFactory = null ;
 	 		}
	}
	
	@Override
	public DBRow containerLoadProduct(String containerStr) throws Exception {
		DBRow result = new DBRow();
		try{
			//1.去temp_container
			DBRow container = null ;
			container = tempContainerMgrZr.findContainerByNumber(containerStr);
			if(container == null){
 				//去Container查询
				container = containerMgrZyj.findContainerByNumber(containerStr);
			}
 			if(container == null){
				 throw new ContainerNotFoundException();
			}
			 
			 
 			long fixTitleId = container.get("title_id", 0l);
			String fixTitleName = container.getString("title_name");
			String fixLotNumber = container.getString("lot_number");	//在为null的时候JSon没有返回这个字段
			container.add("title_id", fixTitleId);
			container.add("title_name", fixTitleName);
 			container.add("lot_number", fixLotNumber);
			int containerType = container.get("container_type", 0);
			long type_id = container.get("type_id", 0L);
			long lp_id = container.get("con_id", 0l);
			//查找containerType
			if(containerType == ContainerTypeKey.TLP){
				handTLpContainerType(type_id, container);
			}else if(containerType == ContainerTypeKey.CLP){
				handCLpContainerType(type_id, container);
			}/*else if(containerType == ContainerTypeKey.BLP){
				handBLpContainerType(type_id, container);
			}else if(containerType == ContainerTypeKey.ILP){
				handILpContainerType(type_id, container);
			}*///去掉ILP和BLP
			result.add("ret", BCSKey.SUCCESS);
			result.add("container", new DBRow[]{container});
 			return result;
		}catch (ContainerNotFoundException e) {
			throw e ;
 		}
		catch (Exception e) {
			throw new SystemException(e,"containerLoadProduct",log);
 		}
 	}
	private void handBLpContainerType(long type_id,DBRow container) throws Exception{
		try{
			DBRow containerTypeInfo = containerMgrZyj.findBoxTypeRelateInfoByTypeId(type_id);
			container.add("containerId", container.get("con_id", 0l));
			container.add("containerHasSn", container.get("is_has_sn", 0));
			container.add("containerType", "BLP");
			if(containerTypeInfo == null){return ;}
		
			container.add("subContainerType",  containerTypeInfo.get("box_inner_type", 0) == 0 ? "Product":"ILP");
			container.add("subContainerTypeId",  containerTypeInfo.get("box_inner_type", 0l));
			if(containerTypeInfo.get("box_inner_type", 0) == 0){
				//直接放的是product
				container.add("subCount", 0);
			}else{
				container.add("subCount", containerTypeInfo.get("box_total", 0)  );
			}
			container.add("containerPcId", containerTypeInfo.get("box_pc_id", 0l));
			container.add("containerPcName", containerTypeInfo.getString("p_name"));
			container.add("containerPcCount",containerTypeInfo.get("box_total_piece", 0));
 		}catch (Exception e) {
			throw new SystemException(e,"handBlpContainerType",log);
 		}
	}
	private void handCLpContainerType(long type_id , DBRow container) throws Exception{
		try{
			DBRow containerTypeInfo = containerMgrZyj.findClpTypeRelateInfoByTypeId(type_id);
			container.add("containerId", container.get("con_id", 0l));
			container.add("containerHasSn", container.get("is_has_sn", 0));
			container.add("containerType", "CLP");
			if(containerTypeInfo.get("sku_lp_box_type", 0) == 0){
				container.add("subContainerType", "Product");
				container.add("subContainerTypeId", 0);
				container.add("subCount", 0 );

			}else{
				container.add("subContainerType", "BLP");
				container.add("subContainerTypeId", containerTypeInfo.get("sku_lp_box_type", 0));
				container.add("subCount", containerTypeInfo.get("sku_lp_total_box", 0) );
			}
			container.add("containerPcId",  containerTypeInfo.get("sku_lp_pc_id", 0l));
			container.add("containerPcName", containerTypeInfo.getString("p_name") );
			container.add("containerPcCount", containerTypeInfo.get("sku_lp_total_piece", 0));
			
		}catch (Exception e) {
			throw new SystemException(e,"handBlpContainerType",log);
		}
	}
	
	private void handILpContainerType(long type_id , DBRow container) throws Exception{
		try{
			DBRow containerTypeInfo = containerMgrZyj.findInnerBoxTypeRelateInfoByTypeId(type_id);
			StringBuffer re = new StringBuffer();
			container.add("containerId", container.get("con_id", 0l));
			container.add("containerHasSn", container.get("is_has_sn", 0));
			container.add("containerType", "ILP");
			container.add("subContainerType", "Product");
			container.add("subContainerTypeId", 0);
			container.add("subCount", 0);
			if(containerTypeInfo  == null){return ;}
			 
			
			container.add("containerPcId", containerTypeInfo.get("ibt_pc_id", 0l));
			container.add("containerPcName", containerTypeInfo.getString("p_name"));
			container.add("containerPcCount", containerTypeInfo.get("ibt_total", 0));
	 
		}catch (Exception e) {
			throw new SystemException(e,"handILpContainerType",log);
		}
	}
	 /**
	  * 插入到ContainerProduct
	  * 插入到Container_loading
	  * 删除还不完整
	  * container_child_list 的数据还没有添加()
	  */
	@Override
	public DBRow submitContainer(DBRow data) throws Exception {
		DBRow result = new DBRow();
		try{
			
			//1.删除数据
			//2.添加数据
			
			//containerProduct
			List<DBRow> datas =(ArrayList<DBRow>)data.get("datas", new Object());
			if(datas != null){
				for(DBRow temp : datas){
					//删除ContainerProduct
					floorAndroidMgrZr.deleteContainerProduct(temp.get("con_id", 0l));
 					DBRow[] products =  (DBRow[])temp.get("products", new Object());
					if(products != null && products.length > 0 ){
						double totalQty = 0.0d ;
						for(DBRow innerProduct  : products){
							totalQty += innerProduct.get("cp_quantity", 0.0d);
							floorAndroidMgrZr.addContainerProduct(innerProduct);
						}
						
						checkContainerAndProduct(temp,totalQty);
					}
				}
			}
			
			//ContainerLoading
			ContainerNode rootNode =(ContainerNode) data.get("rootNode", new ContainerNode());
			if(rootNode != null && rootNode.getContainerId() != 0l){
				List<ContainerNode> arrayList = (ArrayList<ContainerNode>)data.get("listContainerNode", new Object());
				for(ContainerNode containerNode : arrayList){
					if(containerNode.getSubContainer() != null && containerNode.getSubContainer().size() > 0){
						
						List<ContainerNode> nodes = containerNode.getSubContainer();
						for(ContainerNode node : nodes){
							DBRow row = node.getContainerRow();
							row.remove("products");
							//删除ContainerLoading
							floorAndroidMgrZr.deleteContainerLoading(row.get("con_id", 0l),row.get("parent_con_id", 0l));
							floorAndroidMgrZr.addContainerLoading(row);
						}
					}
				}
			}
			//处理ContainerChildList..1.先删除2.插入ChildList的数据
			List<DBRow> childList  = (List<DBRow>)data.get("childList", new ArrayList<DBRow>());
			if(childList.size() >  0){
				deleteContainerChildList(rootNode.getContainerId());
				for(DBRow child : childList){
					floorAndroidMgrZr.addChildListItem(child);
				}
			}
			result.add("ret", BCSKey.SUCCESS);
			return result ;
		}catch (Exception e) {
			throw new SystemException(e,"submitContainer",log);
 		}
 		 
	}
	/**
	 * 根据一个Con_id去删除ChildList中的数据
	 * @param con_id
	 * @throws Exception
	 */
	public void deleteContainerChildList(long con_id) throws Exception
	{
		try{
			DBRow[] childList = floorAndroidMgrZr.getChildListBySearchRootId(con_id);
			if(childList != null && childList.length > 0 ){
				for(DBRow temp : childList){
					floorAndroidMgrZr.deleteChildListBySearchRootId(temp.get("cont_id", 0l));
				}
			}
		}catch (Exception e) {
			throw new SystemException(e,"deleteContainerChildList",log);
		}
	}
	/**
	 * 用来检查ContainerProduct 和 商品的数量
	 * 1.如果是TLP那么就不检查
	 * 2.如果是CLP,ILP,BLP,那么严格的检查.
	 * 3.同时检查成功过后那么就应该更新Container上面的title,logNumber , isFull ,has sn 
	 * 4.has sn 的判断目前根据Container 中的所有的商品来判断
	 * 5.如果有不满足提交的直接抛出异常
	 * 6.通常来说只要是可以提交数据上来数据都是满的。只是在做一次检查也可以不做。
	 */
	public void checkContainerAndProduct(DBRow container , double totalQty) throws Exception{
	 try{	
		double containerTotalPiece = 0.0d ;
		long containerId =	container.get("con_id", 0l);
		long containerTypeId = container.get("container_type_id", 0l);
		int containerType = container.get("container_type", 0);
		long title_id =  container.get("title_id", 0l);
 		if(title_id == 0l){
			throw new NullPointerException();
		}
	  
		if(containerType == ContainerTypeKey.CLP){
			DBRow containerTypeRow = clpTypeMgrZr.getCLpTypeByTypeId(containerTypeId);
 			containerTotalPiece = containerTypeRow.get("sku_lp_total_piece", 0) * 1.0d ;
		 
		}
		/*
		if(containerType == ContainerTypeKey.BLP){
			DBRow containerTypeRow = boxTypeMgrZr.getBoxTypeBuId(containerTypeId);
			 
			containerTotalPiece = containerTypeRow.get("box_total_piece", 0) * 1.0d ;
		}
		if(containerType == ContainerTypeKey.ILP){
			DBRow containerTypeRow = boxTypeMgrZr.findIlpByIlpId(containerTypeId);
			containerTotalPiece = containerTypeRow.get("ibt_total", 0) * 1.0d ;
		}*///去掉ILP和BLP
		if(containerTotalPiece != totalQty  &&  containerType != ContainerTypeKey.TLP){
			 throw new ContainerNotFullException();
		}
		//然后更新Container表中的数据 is_full
		DBRow updateContainer = new DBRow();
		if(containerType != ContainerTypeKey.TLP){
			updateContainer.add("is_full", ContainerProductState.FULL);
		}
		updateContainer.add("title_id", title_id);
		updateContainer.add("lot_number", container.getString("lot_number"));
		
		
		floorAndroidMgrZr.updateContainer(containerId, updateContainer);
		container.remove("title_id");
		container.remove("lot_number");
	 }catch(Exception e) {
		throw new SystemException(e,"submitContainer",log);
 	 }
	}
	
	private void handTLpContainerType(long type_id , DBRow container) throws Exception{
		try{
			
	   
				//TLP	 sdf
			container.add("containerId", container.get("con_id", 0l));			 
			container.add("containerHasSn", container.get("is_has_sn", 0));
			container.add("containerType", "TLP");
			container.add("subContainerType", "ALL");
			container.add("subContainerTypeId", -1);
			container.add("subCount", -1);
			container.add("containerPcId", -1);
			container.add("containerPcName", "");
			container.add("containerPcCount", -1);
 
		}catch (Exception e) {
			throw new SystemException(e,"handTLpContainerType",log);
		}
	}
	/**
	 * 1.container 是否存在
	 * 2.products是直接放在这个Container上面的
	 * 		a.读取subContainers 中的商品  + products 保存在 container_product表
	 * 		b.添加subContainers 的数据到Container_loading
	 * 		c.添加subContainers 对应的container_child_list的数据到container_child_list中
	 * 		
	 */
	@Override
	public void appendContainer(long container_id,  List<DBRow> subContainers, List<DBRow> products) throws Exception {
 		try{
			DBRow parentContainer =  floorAndroidMgrZr.getContainerById(container_id);
			if(parentContainer == null){
				 throw new ContainerNotFoundException();
			}
			List<DBRow> addContainerProducts = new ArrayList<DBRow>();
			List<DBRow> addContainerLoading = new ArrayList<DBRow>();
			List<DBRow> addContainerChildList = new ArrayList<DBRow>();
			//add container_product start
			if(subContainers != null && subContainers.size() > 0 ){
				for(DBRow row : subContainers){
					//读取Container_product的数据
					DBRow subContainer =  floorAndroidMgrZr.getContainerById(row.get("container_id", 0l));
					if(subContainer == null ){
						 throw new ContainerNotFoundException();
					}
					DBRow[] subProducts  =  floorAndroidMgrZr.getContainerProducts(container_id);
					// containerProducts
					addContainerProdutsInList(addContainerProducts, subProducts,parentContainer);
					// container loading
					addContainerLoading(addContainerLoading, subContainer, parentContainer);
 					// subContainer的child_list
					addContainerChildList(addContainerChildList,subContainer,parentContainer);
 				}		
			}
			if(products != null && products.size() > 0 ){
				addContainerProducts.addAll(products);
			}
			 
			//save 数据
			for(DBRow row : addContainerProducts){
				floorAndroidMgrZr.addContainerProduct(row);
			}
			for(DBRow row : addContainerLoading){
				floorAndroidMgrZr.addContainerLoading(row);
			}
			for(DBRow row : addContainerChildList){
				floorAndroidMgrZr.addChildListItem(row);
			}
 		}catch (ContainerNotFoundException e) {
			throw e;
		}catch (Exception e) {
			throw new SystemException(e,"appendContainer",log);
		}
 	}
	/**
	 * 原来的levv + 1
	 * search_root_con_id变为parent_container_id 
	 * cont_id 为child_list的数据
	 * parent_con_id 如果是为0 变成ParentContainerId
	 * @param addContainerChildList
	 * @param subContainer
	 * @param parentContainer
	 * @throws Exception
	 */
	private void addContainerChildList(List<DBRow> addContainerChildList , DBRow subContainer , DBRow parentContainer) throws Exception{
		DBRow[] datas = floorAndroidMgrZr.getContainerChildList(subContainer.get("con_id", 0l));
		if(datas != null && datas.length > 0 ){
			for(DBRow row : datas){
				DBRow containerChildList = new DBRow();
				int levv = row.get("row", 0);
				containerChildList.add("search_root_con_id", parentContainer.get("con_id", 0l));
				containerChildList.add("cont_id", row.get("cont_id", 0l));
				if(row.get("parent_con_id", 0) == 0){
					containerChildList.add("parent_con_id", parentContainer.get("con_id", 0l));
				}
				containerChildList.add("parent_con_id", parentContainer.get("con_id", 0l));
				containerChildList.add("levv", levv + 1);
				addContainerChildList.add(containerChildList);
			}
		}
	}
	private void addContainerLoading(List<DBRow> addContainerLoading , DBRow subContainer , DBRow parentContainer){
		DBRow containerLoading = new DBRow();
		containerLoading.add("con_id",subContainer.get("con_id", 0l));
		containerLoading.add("container_type",subContainer.get("container_type", 0) );
		containerLoading.add("container_type_id", subContainer.get("type_id", 0l));
		containerLoading.add("parent_con_id", parentContainer.get("con_id", 0l));
		containerLoading.add("parent_container_type", parentContainer.get("container_type", 0));
		containerLoading.add("parent_container_type_id", parentContainer.get("type_id", 0l));
		addContainerLoading.add(containerLoading);
	}
	private void addContainerProdutsInList(List<DBRow> arrayList , DBRow[] appends,DBRow parentContainer){
		if(appends != null && appends.length > 0){
			for(DBRow row : appends){
				row.remove("cp_id");
				String cl_lp_name = row.getString("cp_lp_name");
				row.add("cp_lp_name", parentContainer.getString("container") + "->"+cl_lp_name);
				arrayList.add(row);
			}
		}
	}
	
	/**
	 * 返回商品的基础信息(包括基础的信息 ,pictureInfo,ProductCodeInfo)
	 * baseInfo{name:xxxx;id:xxxx;weight:1212;height:12121}
	 * pictrueInfo[]
	 * productCodeInfo[]
	 * 生成商品图片的缩略图(只会生成一次)
	 * DBRow[] all = floorAndroidMgrZr.getAllFile(pcid, FileWithTypeKey.PRODUCT_SELF_FILE);
	 */
	@Override
	public DBRow getBaseProductInfo(long p_id) throws Exception {
		try{
			DBRow product = floorAndroidMgrZr.getProductInfo(p_id);
			//product.remove("weight");
			if(product == null || product.get("pc_id", 0l) == 0l){
				throw new ProductNotFindException();
			}
			DBRow[] productCodeInfo = floorAndroidMgrZr.getProductCode(p_id);
			DBRow[] pictrueInfo = floorAndroidMgrZr.getProductPictrueInfo(p_id);
			DBRow[] titles = floorAndroidMgrZr.getProductTitles(p_id);
			createProductSmallImage(p_id);
			product.add("productCodeInfo", productCodeInfo);
		 	product.add("pictures", pictrueInfo);
			product.add("titles", titles);
			return product ;
		}catch(ProductNotFindException e){
			throw e;
		}catch (Exception e) {
			throw new SystemException(e,"getBaseProductInfo",log);
		}
 	}
	private void createProductSmallImage(long pc_id) throws Exception{
		try{
			 DBRow[] all = floorAndroidMgrZr.getAllFile(pc_id, FileWithTypeKey.PRODUCT_SELF_FILE);
			 if(all != null && all.length > 0){
				String fileFolderOr= Environment.getHome().replace("\\", "/")+"."+ "/upload/product/";
				String fileFolderTb = Environment.getHome().replace("\\", "/")+"."+ "/upload/thumbnail/";
					for(DBRow temp : all){
						//生成XML.生成缩略图。
						 
						if(temp.getString("file_name").length() > 0){
							String fileName = temp.getString("file_name");
							File tb = new File(fileFolderTb + fileName);
							if(!tb.exists()){
								File or = new File(fileFolderOr+fileName);
								if(or.exists()){
									//生成缩略图
									makeSmallImage(or, fileFolderTb+fileName);
								}
							} 
						}
						
					}
					
				}
			 
		}catch (Exception e) {
			throw new SystemException(e,"createProductSmallImage",log);
 		}
	}
	//生成缩略图
	private  int makeSmallImage(File srcImageFile,String dstImageFileName) throws Exception {
		int flag = 0 ;
        FileOutputStream fileOutputStream = null;
        JPEGImageEncoder encoder = null;
        BufferedImage tagImage = null;
        Image srcImage = null;
        try{
            srcImage = ImageIO.read(srcImageFile);
            int srcWidth = srcImage.getWidth(null);//原图片宽度
            int srcHeight = srcImage.getHeight(null);//原图片高度
            int dstMaxSize = 150;//目标缩略图的最大宽度/高度，宽度与高度将按比例缩写
            int dstWidth = srcWidth;//缩略图宽度
            int dstHeight = srcHeight;//缩略图高度
            float scale = 0;
            //计算缩略图的宽和高
            if(srcWidth>dstMaxSize){
                dstWidth = dstMaxSize;
                scale = (float)srcWidth/(float)dstMaxSize;
                dstHeight = Math.round((float)srcHeight/scale);
            }
            srcHeight = dstHeight;
            if(srcHeight>dstMaxSize){
                dstHeight = dstMaxSize;
                scale = (float)srcHeight/(float)dstMaxSize;
                dstWidth = Math.round((float)dstWidth/scale);
            }
            //生成缩略图
            tagImage = new BufferedImage(dstWidth,dstHeight,BufferedImage.TYPE_INT_RGB);
            tagImage.getGraphics().drawImage(srcImage,0,0,dstWidth,dstHeight,null);
            fileOutputStream = new FileOutputStream(dstImageFileName);
            encoder = JPEGCodec.createJPEGEncoder(fileOutputStream);
            encoder.encode(tagImage);
            fileOutputStream.close();
            fileOutputStream = null;
            flag =  1 ;
        }finally{
            if(fileOutputStream!=null){
                try{
                    fileOutputStream.close();
                }catch(Exception e){
                }
                fileOutputStream = null;
            }
            encoder = null;
            tagImage = null;
            srcImage = null;
          
        }
        return flag ;
    }
	@Override
	public DBRow[] getLoginTitles(long adid) throws Exception {
		try{
			return floorAndroidMgrZr.getLoginTitles(adid);
		}catch (Exception e) {
			throw new SystemException(e,"getLoginTitles",log);
		}
 	}
	@Override
	public void deleteProductFile(long pf_id) throws Exception {
		try{
			 
			boolean flag = floorAndroidMgrZr.deleteProductFile(pf_id);
			if(!flag){throw new ProductPictureDeleteException();}
		 
		}catch (ProductPictureDeleteException e) {
			throw e ;
		}catch (Exception e) {
			throw new SystemException(e,"deleteProductFile",log);
		}
 	}
	/**
	 * 添加商品的条码
	 * 1.首先检查是否是主条码 && 不是其他商品的主条码(报错)
	 * 2.如果是这个商品已经了该条码类型,那么原来的那个条码就是OLD.
	 */
	@Override
	public DBRow[] addProductCode(DBRow insertRow,DBRow loginBean) throws Exception {
		try{
			String p_code =  insertRow.getString("p_code");
			long pc_id = insertRow.get("pc_id", 0l);
			int code_type = insertRow.get("code_type", 0);
			
			if(pc_id == 0l || p_code.trim().length() < 1 || code_type == 0){
				throw new DataFormatException();
			}
			if(CodeTypeKey.Main == code_type){
				int count = floorAndroidMgrZr.getProductMainCodeByPcid(pc_id, code_type, p_code);
				if(count > 0){
					throw new ProductCodeIsExistException();
				}
			}
			DBRow updateRow = floorAndroidMgrZr.getProductCode(pc_id,code_type);
			//先插入数据,如果updateRow不为null,那么更新updateRow 为OLD
			insertRow.add("create_adid", loginBean.get("create_adid", loginBean.get("adid", 0l)));
			insertRow.add("create_time", DateUtil.NowStr());
			long id = floorAndroidMgrZr.addProductCode(insertRow);
			if(id == 0l){
				throw new SystemException();
			}
			if(updateRow != null && updateRow.get("pcode_id", 0l) != 0l){
				DBRow upddate = new DBRow();
				upddate.add("code_type", CodeTypeKey.Old);
				floorAndroidMgrZr.updateProductCode(updateRow.get("pcode_id", 0l), upddate);
			}
			return floorAndroidMgrZr.getProductCodeByPcid(pc_id);
		}catch (DataFormatException e) {
			throw e ;
 		}catch (ProductCodeIsExistException e) {
			 throw e ;
		}catch (Exception e) {
			throw new SystemException(e,"addProductCode",log);
		}
	}

	@Override
	public void updateSimpleProduct(DBRow updateRow) throws Exception {
 		try{
 			String p_name = updateRow.getString("p_name");
 			//首先查询是否P_name已经重复了
 			int count =	floorAndroidMgrZr.countPName(p_name);
 			if(count > 0){
 				
 			}
 		}catch (Exception e) {
			throw new SystemException(e,"updateSimpleProduct",log);
 		}
	}
	
	@Override
	public DBRow[] getInboundTransportList(DBRow loginDBRow ) throws Exception {
		try{
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			DBRow[] data = getCheckInTransport(loginDBRow.get("ps_id", 0l));
			if(data!=null && data.length > 0){
				for(DBRow row : data){
					DBRow temp = new DBRow();
					temp.add("transport_id", row.get("transport_id", 0l));
					temp.add("fromwarehouse", row.getString("fromwarehouse"));
					temp.add("towarehouse", row.getString("towarehouse"));
					//查询装运单 所在门 和区域 
					DBRow[] door = getDoorNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.DELEIVER );
					DBRow[] location = getLocationNameByTransportId(row.get("transport_id", 0l),TransportRegistrationTypeKey.DELEIVER);
					//door,door - location,location 
					String doorStr = "" ;
					String locationStr = "";
					if(door != null){
						for(DBRow tempDoor : door){
							doorStr += ","+tempDoor.getString("doorId");
						}
					}
					if(location != null){
						for(DBRow tempLocation : location){
							locationStr+= "," + tempLocation.getString("location_name");
						}
					}
					temp.add("area", (doorStr.length() > 0 ? doorStr.substring(1) : "") + "|"+(locationStr.length() > 0 ? locationStr.substring(1) : ""));	// 在那个门卸货门
					temp.add("state", "CHECKIN");
					temp.add("title_id", row.get("title_id", 0l));
					temp.add("title_name", row.getString("title_name"));
					temp.add("purchase_id", row.get("purchase_id", 0l));
					if(row.get("purchase_id", 0l) != 0l){
						//说明是Transport从交货单来的
						DBRow supplier = getSupplier(row.get("purchase_id", 0l));
						if(supplier != null){temp.add("fromwarehouse", supplier.getString("sup_name"));}
					}
					list.add(temp);
				}
			}
			return list.toArray(new DBRow[list.size()]);
 		
		}catch (Exception e) {
			throw new SystemException(e,"getInboundTransportList",log);
 		}
	}


	@Override
	public DBRow[] getPositionTransportList(DBRow loginDBRow) throws Exception {
		 //收货放货时候查询的transport
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		try{
			
			DBRow[] data =  getTransportReceived(loginDBRow.get("ps_id", 0l));
			if(data!=null && data.length > 0){
				for(DBRow row : data){
					DBRow temp = new DBRow();
					temp.add("transport_id", row.get("transport_id", 0l));
					temp.add("fromwarehouse", row.getString("fromwarehouse"));
					temp.add("towarehouse", row.getString("towarehouse"));
					temp.add("area", "");	 	
					temp.add("state", "RECEIVED");
					temp.add("title_id", row.get("title_id", 0l));
					temp.add("title_name", row.getString("title_name"));
					temp.add("purchase_id",row.get("purchase_id", 0l));
					if(row.get("purchase_id", 0l) != 0l){
						//说明是Transport从交货单来的
						DBRow supplier = getSupplier(row.get("purchase_id", 0l));
						if(supplier != null){temp.add("fromwarehouse", supplier.getString("sup_name"));}
					}
					list.add(temp);
				}
 			}
			return list.toArray(new DBRow[list.size()]);
		}catch (Exception e) {
			throw new SystemException(e,"getPositionTransportList",log);
 		}
 	}
	/**
	 * 下载收货的时候的基础数据
	 * {ret:1;err:2;productCode:[{},{},{}] ; productInfo:[{},{}];datas[{},{},{}]}
	 */
	@Override
	public DBRow getInboundBaseInfo(long transport_id) throws Exception {
		
		try{
			DBRow result = new DBRow();
			//datas
			DBRow[] data = queryTransportOutBoundByTransportId(transport_id);
			//productInfo
			DBRow[] productInfo =  getProductForTransport(transport_id);
			//productCode
			DBRow[] productCode =  getProductCodeForTransport(transport_id);
			productCode = fixProductCode(productCode, productInfo);
			result.add("data", data);
			result.add("productInfo", productInfo);
			result.add("productCode", productCode);
			return  result;
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"getInboundBaseInfo",log);	
		 }
	}
	/**
	 * 给每一个商品都添加上PCID-PCID-类型的Barcode
	 * @param productCodes
	 */
	private DBRow[] fixProductCode(DBRow[] productCodes,DBRow[] productInfos){
		List<DBRow> arrayList = new ArrayList<DBRow>();
		if(productInfos != null && productInfos.length > 0 ){
 			for(DBRow productInfo : productInfos ){
 				DBRow barCode = new DBRow();
 				barCode.add("p_code", productInfo.get("pc_id", 0l));
 				barCode.add("pc_id", productInfo.get("pc_id", 0l));
 				barCode.add("code_type", 0 );
 				barCode.add("p_name", productInfo.getString("p_name"));
 				arrayList.add(barCode);
			}
		}
		if(productCodes != null){
			for(DBRow temp : productCodes){
				arrayList.add(temp);
			}
		}
		return arrayList.toArray(new DBRow[arrayList.size()]);
	}
	
	@Override
	public DBRow getPositionBaseInfo(long transport_id) throws Exception {
		 try{
			 DBRow result = new DBRow();
			 
			 //position product info
			 DBRow[] productInfo = 	getPositionProduct(transport_id);
			 //position product code
			 DBRow[] productCode = getPositionProductCode(transport_id);
			 //
			 DBRow[] datas = getTransportWarehouseProduct(transport_id);
			 
			 productCode = fixProductCode(productCode, productInfo);
			
			 result.add("data", datas);
			 result.add("productInfo", productInfo);
			 result.add("productCode", productCode);
			 
			 return result;
		 }catch (Exception e) {
			 throw new SystemException(e,"getPositionBaseInfo",log);	
 		}	 
	}
	/**
	 * 返回一个Product图片的地址
	 */
	@Override
	public File getProductMainPicture(long pc_id) throws Exception {
		//不加默认了。加上默认android端的缓存会以为这个默认的图片就是他本身的图片
		//	String fileName = "no_img.png" ;					//默认是一张系统的图片
		String fileName = "" ;
		String fileFolderTb = Environment.getHome().replace("\\", "/")+"."+ "/upload/thumbnail/";
 		String fileFolderOr= Environment.getHome().replace("\\", "/")+"."+ "/upload/product/";

		File parentDir = new File(fileFolderTb);
		File parentProduct = new File(fileFolderOr);
		try{
			DBRow[] pictures = floorAndroidMgrZr.getProductPictrueInfo(pc_id);
			if(pictures != null){
				boolean isExist = false ;			//表示是否是已经生成了一张小的图片
				for(DBRow temp : pictures){
					File tempFile = new File(parentDir,temp.getString("file_name"));
					if(tempFile.exists()){
						isExist = true ;
						fileName = temp.getString("file_name");
						break ;
					}
				}
				if(!isExist){				//这个时候要生成一张小的图片
					for(DBRow temp : pictures){
						File tempFile = new File(parentProduct,temp.getString("file_name"));
						if(tempFile.exists()){
							int successFlag = makeSmallImage(tempFile, fileFolderTb+temp.getString("file_name"));							
							if(successFlag == 1){fileName = temp.getString("file_name") ;break ;}
						}
					}
				}
			}
			if(fileName.trim().length() > 0 ){
				return new File(parentDir,fileName);
			}
			return null;
		}catch (Exception e) {
			 throw new SystemException(e,"getProductMainPicture",log);
 		}
 	}
	
	public DBRow[] getFilesAndCreateSmall(long file_with_id ,  int file_with_type , String sys_file_dir) throws Exception{
		try{
 			DBRow[] all = floorAndroidMgrZr.getAllFileByFileWithIdAndFileWithType(file_with_id, file_with_type);
 			 
 			
 			if(all != null){
 				//首先查询small图片是否存在,如果不存在的那么 --> 利用大图片去生成小的图片
 				String fileFolderTb = Environment.getHome().replace("\\", "/")+"."+ "/upload/thumbnail/";
 		 		String fileFolderOr= Environment.getHome().replace("\\", "/")+"."+ "/upload/"+sys_file_dir+"/";
  		 		    
 				for(DBRow row : all){
 					String fileName = row.getString("file_name");
 					File small = new File(fileFolderTb,fileName);
 					if(!small.exists()){
 						File big = new File(fileFolderOr,fileName);
 						if(big.exists()){
 							makeSmallImage(big, fileFolderTb+fileName);
 						}
 					}
 				}
 			}
 			 
			return all ;
		}catch (Exception e) {
			 throw new SystemException(e,"getFilesAndCreateSmall",log);

 		}
	}

	@Override
	public void deleteFileTableFile(long file_id) throws Exception {
 		try{
 			floorAndroidMgrZr.deleteFile(file_id);
 		}catch (Exception e) {
			 throw new SystemException(e,"deleteFileTableFile",log);
		}
	}
	@Override
	public DBRow[] getBaseProductBy(PageCtrl page) throws Exception {
		try{
			return floorAndroidMgrZr.getProductBy(page);
		}catch (Exception e) {
			 throw new SystemException(e,"getBaseProductBy",log);
 		}
 	}
	@Override
	public DBRow[] getBaseProductBy(long maxPcId , int length ,String flag) throws Exception {
		try{
			return floorAndroidMgrZr.getProductBy(maxPcId,length,flag);
		}catch (Exception e) {
			 throw new SystemException(e,"getBaseProductBy",log);
 		}
 	}

	@Override
	public double getContainerTypeTotalPiece(int containerType, long containerTypeId) throws Exception {
		try{
			double total = 0.0d; 
			/*
			 if(containerType == ContainerTypeKey.BLP ){
				  DBRow blpType = floorAndroidMgrZr.getBlpType(containerTypeId);
				  if(blpType != null){
					  total = blpType.get("box_total_piece", 0.0d);
				  }
			 }
			 if(containerType == ContainerTypeKey.ILP ){
				  DBRow ilpType = floorAndroidMgrZr.getILpType(containerTypeId);
				  if(ilpType != null){
					  total = ilpType.get("ibt_total", 0.0d);
				  }
			 }*///去掉ILP和BLP
			 if(containerType == ContainerTypeKey.CLP ){
				  DBRow clpType = floorAndroidMgrZr.getClpType(containerTypeId);
				  if(clpType != null){
					  total = clpType.get("sku_lp_total_piece", 0.0d);
				  }
			 }
			 return total ;
		}catch (Exception e) {
			 throw new SystemException(e,"getContainerTypeTotalPiece",log);
 		}
	}


	private String getPost(InputStream inputStream) throws Exception{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData,"UTF-8"));
	}
	public   String getXmlElementStringValue(NodeList nodeList) {
		if(nodeList != null && nodeList.getLength() > 0){
			Node node = nodeList.item(0);
			if(node != null){
				Node tempNode =node.getFirstChild();
				if(tempNode != null){
					return tempNode.getNodeValue();
				}
			}
		}
		return  "";
	}
	
	/**
	 * 通过容器ID，删除容器与商品的关系：container_product
	 * zyj
	 */
	public void deleteContainerProduct(long cp_lp_id) throws Exception
	{
		try
		{
			floorAndroidMgrZr.deleteContainerProduct(cp_lp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteContainerProduct(long cp_lp_id)",log);
		}
	}

	/**
	 * 添加容器与商品的关系：container_product
	 * zyj
	 */
	public void addContainerProduct(DBRow row) throws Exception
	{
		try
		{
			floorAndroidMgrZr.addContainerProduct(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addContainerProduct(DBRow row)",log);
		}
	}
	
	/**
	 * 通过子容器ID和父容器ID删除容器关系
	 * zyj
	 */
	public void deleteContainerLoading(long con_id , long parent_con_id ) throws Exception
	{
		try
		{
			floorAndroidMgrZr.deleteContainerLoading(con_id, parent_con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteContainerLoading(long con_id , long parent_con_id )",log);
		}
	}
	
	/**
	 * 添加容器与容器的关系
	 * zyj
	 */
	public void addContainerLoading(DBRow data) throws Exception
	{
		try
		{
			floorAndroidMgrZr.addContainerLoading(data);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addContainerLoading(DBRow data)",log);
		}
	}
	
	/**
	 * 通过searchId和容器ID，删除容器的平铺表关系
	 * zyj
	 */
	public void deleteChildListBySearchRootAndContId(long search_root_id, long cont_id) throws Exception
	{
		try
		{
			floorAndroidMgrZr.deleteChildListBySearchRootAndContId(search_root_id, cont_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteChildListBySearchRootAndContId(long search_root_id, long cont_id)",log);
		}
	}
	
	/**
	 * 添加容器的平铺表关系
	 * zyj
	 */
	public void addChildListItem(DBRow row)throws Exception
	{
		try
		{
			floorAndroidMgrZr.addChildListItem(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addChildListItem(DBRow row)",log);
		}
	}
	
	
	/**
	 * 通过searhcId和levv删除容器平铺表
	 * @param search_root_id
	 * @param levv
	 * @param isLevvEqual true：levv=isLevvEqual；false:levv>=isLevvEqual
	 * zyj
	 * @throws Exception
	 */
	public void deleteChildListBySearchRootIdLevv(long search_root_id, int levv, boolean isLevvEqual) throws Exception
	{
		try
		{
			floorAndroidMgrZr.deleteChildListBySearchRootIdLevv(search_root_id, levv, isLevvEqual);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteChildListBySearchRootIdLevv(long search_root_id, int levv)",log);
		}
	}

	@Override
	public DBRow[] getContainerInfoByPcId(long pc_id) throws Exception {
		try{
			List<DBRow> returnRow =  new ArrayList<DBRow>();
			//查询ILP
			DBRow[] ilpTypes = boxTypeMgrZr.selectIlp(pc_id);
			DBRow[] blpTypes = boxTypeMgrZr.getBoxSkuTypeByPcId(pc_id);
			DBRow[] clpTypes = clpTypeMgrZr.getClpTypeByPcid(pc_id);
			collectionsAddArray(returnRow, fixCLPType(clpTypes));
			collectionsAddArray(returnRow, fixBLPType(blpTypes));
			collectionsAddArray(returnRow, fixILPType(ilpTypes));
			return returnRow.toArray(new DBRow[0]);
		}catch (Exception e) {
			throw new SystemException(e,"getContainerInfoByPcId(long pc_id)",log);

 		}
 	}
	
	private <T> void  collectionsAddArray(Collection<T> collection , T... it ){
		if(it != null){
			 for(T t : it){
				 collection.add(t);
			 }
		}
 	}
	/**
	 * containerType 
	 * total
	 * @param types
	 * @return
	 */
	private DBRow[] fixILPType(DBRow[] types){
		if(types != null){
			DBRow[] returnDBRows = new DBRow[types.length];
			for(int index = 0 , count = types.length ; index < count ; index++ ){
				DBRow ilpRow = new DBRow();
				DBRow ilp =	types[index];
				ilpRow.add("type_id", ilp.get("ibt_id", 0l));				//type_id
				ilpRow.add("pc_id", ilp.get("ibt_pc_id", 0l));				//pc_id
				ilpRow.add("total", ilp.get("ibt_total", 0));				//total_piece
				ilpRow.add("total_length", ilp.get("ibt_total_length", 0));	//length piece
				ilpRow.add("total_width", ilp.get("ibt_total_width", 0));	//width piece
				ilpRow.add("total_height", ilp.get("ibt_total_height", 0)); //height piece
				ilpRow.add("own", ilp.get("ibt_length", 0)+"x"+ilp.get("ibt_width", 0)+"x"+ilp.get("ibt_height", 0));//自身大小
//				ilpRow.add("container_type", ContainerTypeKey.ILP);			//ilp //去掉ILP和BLP
				ilpRow.add("inner_type", ContainerTypeKey.Original);		//能够包含的类型	
				ilpRow.add("subtotal", 0);									//如果是包含的其他容器，那么表示其他容器数量的数量

				returnDBRows[index] = ilpRow ;
			}
			return returnDBRows ;
		}
		return new DBRow[]{} ;

	}
	public static int getSingleValue(){
		int value = new Random().nextInt(100)  ;
		if(value % 2 == 1 ){
			return value ;
		}else{
			return getSingleValue();
		} 
	}
	public static int getDoubleValue(){
		int value = new Random().nextInt(100)  ;
		if(value % 2 == 0 ){
			return value ;
		}else{
			return getDoubleValue();
		} 
	}
	public static void main(String[] args) {
		List<DBRow> arrayList = new ArrayList<DBRow>();
		for(int index = 0 , count = 10 ; index < count ; index++ ){
			DBRow row = new DBRow();
			int value = (index == 9) ? getSingleValue() : getDoubleValue();
			row.add("yc_id",value );
			arrayList.add(row);
		}
		/**
		 * 1.查询一个最小的单数，如果一个单数都没有了，那么返回一个最小的双数,
		 * 2.首先让整个DBRow进行一个排序
		 * 3.然后再进行查找, 排序过后然后查找最小的单数，没有单数，那么返回index = 0 的数据，他是双数
		 */
		 
		Collections.sort(arrayList,new Comparator<DBRow>() {
			@Override
			public int compare(DBRow o1, DBRow o2) {
 				int returnInt = 0 ;
				if(o1.get("yc_id", 0) > o2.get("yc_id", 0)){
					returnInt = 1 ;
 				 };
 				 if(o1.get("yc_id", 0) < o2.get("yc_id", 0)){
 					returnInt =  -1 ;
 				 }
 				if(o1.get("yc_id", 0) == o2.get("yc_id", 0)){
 					returnInt = 0 ;
				 }
 				return returnInt ;
			}
		});
		for(DBRow row : arrayList){
			////system.out.println(row.get("yc_id", 0));
		}
		//
		DBRow returnRow = null ;
		boolean isSingle =  false ;
 		for(DBRow row : arrayList){
			if(row.get("yc_id", 0) % 2 == 1){
				returnRow = row ;
				isSingle = true ;
  				break ;
			} 
 		}
 		returnRow = isSingle ? returnRow : arrayList.get(0);
 		
 		////system.out.println(returnRow.get("yc_id", 0) + "--------------------");
	}
	private DBRow[] fixBLPType(DBRow[] types){
		if(types != null){
			DBRow[] returnDBRows = new DBRow[types.length];
			for(int index = 0 , count = types.length ; index < count ; index++ ){
				DBRow blpRow = new DBRow();
				DBRow blp =	types[index];
				blpRow.add("type_id", blp.get("box_type_id", 0l));
				blpRow.add("pc_id", blp.get("box_pc_id", 0l));
				blpRow.add("total", blp.get("box_total_piece", 0));
				blpRow.add("total_length", blp.get("box_total_length", 0));
				blpRow.add("total_width", blp.get("box_total_width", 0));
				blpRow.add("total_height", blp.get("box_total_height", 0));
				blpRow.add("own", blp.get("box_length", 0)+"x"+blp.get("box_width", 0)+"x"+blp.get("box_height", 0));//自身大小
//				blpRow.add("container_type", ContainerTypeKey.BLP);//去掉ILP和BLP
				long inner_type = blp.get("box_inner_type", 0l) ;
				if(inner_type != 0l){
					blpRow.add("inner_type", inner_type);	
					blpRow.add("subtotal", blp.get("box_total", 0));				//如果是包含的其他容器，那么表示其他容器数量的数量
				}else{
					blpRow.add("inner_type", 0);	
					blpRow.add("subtotal", 0);				//直接包含商品
				}

				returnDBRows[index] = blpRow ;
			}
			return returnDBRows ;
		}
		return new DBRow[]{} ;

	}
	private DBRow[] fixCLPType(DBRow[] types){

		if(types != null){
			DBRow[] returnDBRows = new DBRow[types.length];
			for(int index = 0 , count = types.length ; index < count ; index++ ){
				DBRow clpRow = new DBRow();
				DBRow clp =	types[index];
				clpRow.add("type_id", clp.get("sku_lp_type_id", 0l));
				clpRow.add("pc_id", clp.get("sku_lp_pc_id", 0l));
				clpRow.add("total", clp.get("sku_lp_total_piece", 0));
				clpRow.add("total_length", clp.get("sku_lp_box_length", 0));
				clpRow.add("total_width", clp.get("sku_lp_box_width", 0));
				clpRow.add("total_height", clp.get("sku_lp_box_height", 0));
				clpRow.add("own", covertDoubleToInt(clp.get("length", 0.0d))+"x"+covertDoubleToInt(clp.get("width", 0.0d))+"x"+covertDoubleToInt(clp.get("height", 0.0d)));//自身大小
				clpRow.add("container_type", ContainerTypeKey.CLP);
				long inner_type =  clp.get("sku_lp_box_type", 0l) ;
				if(inner_type != 0l){
					clpRow.add("inner_type",inner_type);		
					clpRow.add("subtotal", clp.get("sku_lp_total_box", 0));		
				}else{
					clpRow.add("inner_type",0);		
					clpRow.add("subtotal",0);		

				}
				returnDBRows[index] = clpRow ;
			}
			return returnDBRows ;
		}
		return new DBRow[]{} ;
	}
	 
	
	
	private int covertDoubleToInt(double value){
		int returnInt = 0 ;
		try{
			returnInt = (int)value;
		}catch (Exception e) {
 		}
		return returnInt ;
	}
	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}
	

	public void setClpTypeMgrZr(ClpTypeMgrIfaceZr clpTypeMgrZr) {
		this.clpTypeMgrZr = clpTypeMgrZr;
	}


	public void setTempContainerMgrZr(TempContainerMgrIfaceZr tempContainerMgrZr) {
		this.tempContainerMgrZr = tempContainerMgrZr;
	}


	




	
}
