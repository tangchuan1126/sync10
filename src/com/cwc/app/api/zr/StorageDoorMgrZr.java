package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorStorageDoorZr;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;

public class StorageDoorMgrZr implements StorageDoorMgrIfaceZr {
	/**
	 * 	row.add(String.valueOf(OccupyStatusTypeKey.RESERVERED), "Reserved");	//保留
		row.add(String.valueOf(OccupyStatusTypeKey.OCUPIED), "Occupied");		//占用
		row.add(String.valueOf(OccupyStatusTypeKey.RELEASED), "Released"); 	//释放 也就是Free	
	 */
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorStorageDoorZr floorStorageDoorZr ;
	 
	
	private static final String RESERVERED = "Reserved";
	private static final String OCUPIED = "Occupied";
	private static final String RELEASED = "Released";
	
	
	private void addDoorLog(long sd_id , long adid , String type , long dlo_id) throws Exception{
		//
		if(sd_id > 0l){
			DBRow spotRow =  floorStorageDoorZr.getDoorInfoBySdId(sd_id);
			if(spotRow != null){
		  		DBRow row = new DBRow();
				row.add("operator_id", adid);
				row.add("note", type + " : "+ spotRow.getString("doorId"));
				row.add("dlo_id", dlo_id);
				row.add("log_type", CheckInLogTypeKey.DoorLog);
				row.add("operator_time",DateUtil.NowStr());
				row.add("data", "");
				
				floorStorageDoorZr.addCheckInLog(row);
			}
		}
		
	}
 	
	@Override
	public void freeDoor(long sd_id , long adid , long associate_id , int associate_type ) throws Exception { //添加日志
		try{
			if(sd_id > 0l){
				DBRow updateRow = new DBRow();
				updateRow.add("associate_id", 0);
				updateRow.add("associate_type", 0);
				updateRow.add("occupied_status", OccupyStatusTypeKey.RELEASED);
				floorStorageDoorZr.updateDoor(sd_id, updateRow);
				
				addDoorLog(sd_id, adid, RELEASED, associate_id);
				
			}
		}catch(Exception e){
			throw new SystemException(e, "freeDoor " + sd_id, log);
		}
	}
	private void fixDoorRows(DBRow[] rows){
		if(rows != null){
			for(DBRow row : rows){
				row.remove("sd_status");
				row.remove("patrol_time");
				row.remove("latlng");
				row.remove("height");
				row.remove("width");
				row.remove("angle");
				row.remove("y");
				row.remove("x");
				row.remove("ps_id");
			}
		}
	}
	@Override
	public DBRow[] getDoorByPsIdAndOccupiedStatusAndZoneId(long ps_id, int occupied_status , long zone_id) throws Exception {
		try{
			return floorStorageDoorZr.getDoorByPsIdAndOccupiedStatusZoneId(ps_id, occupied_status, zone_id);
		}catch(Exception e){
			throw new SystemException(e, "getDoorByPsIdAndOccupiedStatus ps_id :" + ps_id + "occupied_status :  "+ occupied_status, log);
		}
 	}
 	@Override
	public void ocupiedDoor(long sd_id, int associate_type, long associate_id , long adid)
			throws Exception {
 			try{
 				DBRow updateRow = new DBRow();
 				updateRow.add("associate_type", associate_type);
 				updateRow.add("associate_id", associate_id);
 				updateRow.add("occupied_status", OccupyStatusTypeKey.OCUPIED);
 				floorStorageDoorZr.updateDoor(sd_id, updateRow);
 				addDoorLog(sd_id, adid, OCUPIED, associate_id);
 			}catch(Exception e){
 				throw new SystemException(e, "ocupiedDoor " + sd_id + " associate_type : " + associate_type + " associate_id : "+ associate_id, log);
 			}
	}
	@Override
	public void reserveredDoor(long sd_id, int associate_type, long associate_id , long adid)
			throws Exception {
 			try{
 				DBRow updateRow = new DBRow();
 				updateRow.add("associate_type", associate_type);
 				updateRow.add("associate_id", associate_id);
 				updateRow.add("occupied_status", OccupyStatusTypeKey.RESERVERED);
 				floorStorageDoorZr.updateDoor(sd_id, updateRow);
 				addDoorLog(sd_id, adid, RESERVERED, associate_id);
 			}catch(Exception e){
 				throw new SystemException(e, "reserveredDoor sd_id :" + sd_id + "associate_type :  "+ associate_type + " associate_id :"+associate_id, log);
 			}
	}
	@Override
	public DBRow[] getFreeDoor(long ps_id , long zone_id) throws Exception {
		try{
			 DBRow[] returnRows =getDoorByPsIdAndOccupiedStatusAndZoneId(ps_id, OccupyStatusTypeKey.RELEASED,zone_id); 
			 fixDoorRows(returnRows);
			 return returnRows ;
		}catch(Exception e){
			throw new SystemException(e, "ps_id " + ps_id, log);
		}
 	}
	@Override
	public DBRow[] getAvailableDoorsBy(long ps_id, int associate_type,
			long associate_id) throws Exception {
		try{
			return getAvailableDoorsByZoneId(ps_id, 0l, associate_id, associate_type);
		}catch(Exception e){
			throw new SystemException(e, "getAvailableDoorsBy associate_type ,ps_id ,associate_id  " + ps_id + " ", log);
		}
 	}
	@Override
	public DBRow[] getAvailableDoorsByZoneId(long ps_id, long zone_id, long associate_id, int associate_type) throws Exception {
		try{
			List<DBRow> returnArray = new ArrayList<DBRow>();
			DBRow[] frees = getFreeDoor(ps_id,zone_id);
			Collections.addAll(returnArray, frees);
			if(associate_id > 0l){
				DBRow[] append = floorStorageDoorZr.getDoorsBySelf(associate_id,associate_type,zone_id);
				appendDoorBySelf(append);
				Collections.addAll(returnArray, append);
			}
			//排序
			Collections.sort(returnArray,new StorageDoorComparator());
			return returnArray.toArray(new DBRow[0]);
		}catch(Exception e){
			throw new SystemException(e, "getAvailableDoorsByZoneId", log);
		}
 	}
	/**
	 * 多添加一个参数，表示这些是他自己，原来占用的，所以他这次选择也是可以用的 . addflag(怕在页面上有不同的显示)
	 * @param rows
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	private void appendDoorBySelf(DBRow[] rows){
		fixDoorRows(rows);
		if(rows != null && rows.length > 0){
			for(DBRow row : rows){
				row.add("addflag", "BySelf"); //他原来自己占用的
			}
		}
	}
	
	/**
	 * 返回doorId一个单数最大，和一个双数最小的
	 * 因为从小到大就是排好序的。
	 */
	@Override
	public HoldDoubleValue<DBRow, DBRow> getMinSinleAndMinDoubleBySortedArray(DBRow[] doors) throws Exception {
		try{
			DBRow minDoubleRow = null ;
			DBRow minSinleRow = null ;
			if(doors != null){
				for(DBRow row : doors){
					int doorName = row.get("doorId", 0);
					if(doorName % 2 == 0 && minDoubleRow == null ){
						minDoubleRow = row ;
						continue ;
					}
					if(doorName % 2 == 1 && minSinleRow == null){
						minSinleRow = row ;
						continue ;
					}
					if(minSinleRow != null  && minDoubleRow != null){
						break ;
					}
				}
			}
			return new  HoldDoubleValue<DBRow, DBRow>(minSinleRow, minDoubleRow);
		}catch(Exception e){
			throw new SystemException(e, "getMinSinleAndMinDoubleBySortedArray", log);
 		}
	}
	 
	 
	@Override
	public DBRow[] getUsedDoors(long ps_id, long zone_id) throws Exception {
		try{
			DBRow[] returnRows = floorStorageDoorZr.getUsedDoors(ps_id,zone_id);
			fixDoorRows(returnRows);
			return returnRows ;
		}catch(Exception e){
			throw new SystemException(e, "getUsedDoors", log);
		}
	}
	@Override
	public DBRow getCheckInDoorDetailInfo(long sd_id) throws Exception {
		try{
			return floorStorageDoorZr.getCheckInModuleDoorInfo(sd_id);
		}catch(Exception e){
			throw new SystemException(e, "getCheckInDoorDetailInfo", log);
		}
 	}
	/**
	 * 可以用的门
	 * 
	 * doorInfo 没有对应的associate_id  + associate_type ;
	 * 或者是associate_id 和 associate_type 等于传入的值
	 */
	@Override
	public boolean isCanUseDoor(long sd_id, int associate_type, long associate_id) throws Exception {
		try{
			DBRow doorInfo = floorStorageDoorZr.getDoorInfoBySdId(sd_id);
			if(doorInfo != null){
				long dataBaseAssociateId = doorInfo.get("associate_id", 0l);
				long dataBaseType = doorInfo.get("associate_type", 0);
				
				if(dataBaseAssociateId <= 0l ){
					return true ;
				}
				if(associate_type == dataBaseType && associate_id == dataBaseAssociateId){
					return true ;
				} 
			}
	 		return false;
		}catch(Exception e){
			throw new SystemException(e, "isCanUseDoor", log);
		}

	}
/**
	 * 判断门是否被占用
	 * @param doorId（reserved,Occupid）
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月16日 
	 */
	public boolean checkDoorIsFree(long doorId) throws Exception {
		try
		{	
			if(doorId > 0)
			{
				DBRow door = floorStorageDoorZr.getDoorInfoBySdId(doorId);
				if(null != door)
				{
					if(door.get("occupied_status", OccupyStatusTypeKey.RELEASED) == OccupyStatusTypeKey.RELEASED
							&& door.get("associate_type", 0) == 0 && door.get("associate_id", 0L) == 0)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch(Exception e)
		{
			throw new SystemException(e, "checkDoorIsUsed", log);
		}
	}


	/**
	 * 判断门是否被当前单据占用（reserved,Occupid）
	 * @param doorId
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月16日
	 */
	public boolean checkDoorIsUsedByAssociation(long doorId,
			int associate_type, long associate_id) throws Exception {
		try
		{	
			if(doorId > 0)
			{
				DBRow door = floorStorageDoorZr.getDoorInfoBySdId(doorId);
				if(null != door)
				{
					if(door.get("associate_type", 0) == associate_type && door.get("associate_id", 0) == associate_id
							&& door.get("occupied_status", 0) != OccupyStatusTypeKey.RELEASED)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch(Exception e)
		{
			throw new SystemException(e, "checkDoorIsUsedByAssociation", log);
		}
	}
	
	/**
	 * 通过门ID，获取门的详细信息
	 * @param doorId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 上午8:29:26
	 */
	public DBRow findDoorDetaiByDoorId(long doorId) throws Exception
	{
		try
		{
			if(doorId > 0)
			{
				return floorStorageDoorZr.getDoorInfoBySdId(doorId);
			}
			return null;
		}
		catch(Exception e)
		{
			throw new SystemException(e, "findDoorDetaiByDoorId", log);
		}
	}
	@Override
	public DBRow[] getUseDoorByAssociateIdAndType(long associate_id, int associate_type) throws Exception {
		try{
			return floorStorageDoorZr.getUseDoorByAssociateIdAndType(associate_id, associate_type);
		}catch(Exception e){
			throw new SystemException(e, "getUseDoorByAssociateIdAndType", log);
		}
	}
@Override
	public void freeDoorsByAssociateId(long associate_id, int associate_type) throws Exception {
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("associate_id", 0);
			updateRow.add("associate_type", 0);
			updateRow.add("occupied_status", OccupyStatusTypeKey.RELEASED);
			floorStorageDoorZr.freeDoorsByAssociateId(associate_id,associate_type,updateRow);
		}catch(Exception e){
			throw new SystemException(e, "freeDoorsByAssociateId", log);
		}
	} 	


	/**
	 * 通过门ID，关联信息查询门
	 * @param doorId
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:07:33
	 */
	public DBRow findDoorByDoorIdAssociation(long doorId,  int associate_type, long associate_id  ) throws Exception
	{
		try{
			return floorStorageDoorZr.findDoorByDoorIdAssociation(doorId, associate_type, associate_id);
		}catch(Exception e){
			throw new SystemException(e, "findDoorByDoorIdAssociation", log);
		}
	}
	
	@Override
	public DBRow[] getCheckInModuleByAssociateId(long associate_id,
			int occupancy_status) throws Exception {
		try{
			DBRow[] datas = floorStorageDoorZr.getDoorsCheckInModuleByAssociateId(associate_id,occupancy_status);
			fixDoorRows(datas);
			return datas ;
		}catch(Exception e){
			throw new SystemException(e, "getCheckInModuleByAssociateId", log);
		}
 	}
	
	public void setFloorStorageDoorZr(FloorStorageDoorZr floorStorageDoorZr) {
		this.floorStorageDoorZr = floorStorageDoorZr;
	}
	
}
