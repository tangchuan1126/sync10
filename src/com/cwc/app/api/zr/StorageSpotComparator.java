package com.cwc.app.api.zr;

import java.util.Comparator;

import com.cwc.db.DBRow;

/**
 * 先把这个类拿出来。现在是根据门的编号(Int)去做的排序
 * 他们有需求通过String去做
 * @author win7zr
 *
 */
public class StorageSpotComparator implements Comparator<DBRow>{

	@Override
	public int compare(DBRow o1, DBRow o2) {
		
		int o1SpotId = o1.get("yc_no",0) ;
		int o2SpotId = o2.get("yc_no",0) ;
		return  o1SpotId  > o2SpotId ? 1 : (o1SpotId == o2SpotId ? 0 : -1);
		
	}


}
