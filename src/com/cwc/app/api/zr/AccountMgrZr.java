package com.cwc.app.api.zr;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorAccountMgrZr;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.app.key.PerformStatusKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class AccountMgrZr implements AccountMgrIfaceZr
{
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorAccountMgrZr floorAccountMgrZr ;

	public void setFloorAccountMgrZr(FloorAccountMgrZr floorAccountMgrZr)
	{
		this.floorAccountMgrZr = floorAccountMgrZr;
	}

	@Override
	public DBRow addAccount(long account_with_id,HttpServletRequest request) throws Exception
	{ 
		try{
			 //读取账号的信息
			int index = 1 ;
			String account_name = StringUtil.getString(request, "account_name_"+index);
			int account_with_type =  StringUtil.getInt(request, "account_with_type");//AccountKey.SUPPLIER;
			while(account_name.length() > 0 ){
				
				String account_number = StringUtil.getString(request, "account_number_"+index);
				String account_blank = StringUtil.getString(request, "account_blank_"+index);
				String account_phone = StringUtil.getString(request, "account_phone_"+index);
				String account_swift = StringUtil.getString(request, "account_swift_"+index);
				String account_address = StringUtil.getString(request, "account_address_"+index);
				String account_blank_address = StringUtil.getString(request, "account_blank_address_"+index);
				String account_currency = StringUtil.getString(request, "account_currency_"+index);
				
				DBRow account = new DBRow();
				account.add("account_number", account_number);
				account.add("account_blank", account_blank);
				account.add("account_phone", account_phone);
				account.add("account_swift", account_swift);
				account.add("account_address", account_address);
				account.add("account_blank_address", account_blank_address);
				account.add("account_name", account_name);
				account.add("account_currency", account_currency);
				account.add("account_with_type", account_with_type);
				account.add("account_with_id", account_with_id);
				floorAccountMgrZr.addAccount(account);
				index++ ;
				account_name = StringUtil.getString(request, "account_name_"+index);
			}
			
			DBRow result = new DBRow();
			
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"addAccount",log);
		}
	}

	@Override
	public DBRow[] getAccountByAccountIdAndAccountWithType(long account_with_id, int account_with_type)
			throws Exception
	{
		 try{
			 return floorAccountMgrZr.getAccountByWithIdAndWithType(account_with_id, account_with_type); 
		 }catch (Exception e) {
			 throw new SystemException(e,"getAccountByAccountIdAndAccountWithType",log);
		}
	}

	@Override
	public void addAccountByPage(HttpServletRequest request) throws Exception
	{
		 try{
		 
			 long account_with_id = StringUtil.getLong(request, "account_with_id");
			 int account_with_type  = StringUtil.getInt(request, "account_with_type");
			 String account_name= StringUtil.getString(request, "account_name");
			 String account_number = StringUtil.getString(request, "account_number");
			 String account_blank = StringUtil.getString(request, "account_blank");
			 String account_swift = StringUtil.getString(request, "account_swift");
			 String account_phone = StringUtil.getString(request, "account_phone");
			 String account_address = StringUtil.getString(request, "account_address");
			 String account_blank_address = StringUtil.getString(request, "account_blank_address");
			 String account_currency = StringUtil.getString(request, "account_currency");
			 DBRow insertRow = new DBRow();
			 insertRow.add("account_name", account_name);
			 insertRow.add("account_number", account_number);
			 insertRow.add("account_blank", account_blank);
			 insertRow.add("account_swift", account_swift);
			 insertRow.add("account_phone", account_phone);
			 insertRow.add("account_address", account_address);
			 insertRow.add("account_blank_address", account_blank_address);
			 insertRow.add("account_with_type", account_with_type);
			 insertRow.add("account_with_id", account_with_id);
			 insertRow.add("account_currency", account_currency);
			 
			 floorAccountMgrZr.addAccount(insertRow);
		 }catch (Exception e) {
			 throw new SystemException(e,"addAccountByPage",log);
		}
	}

	@Override
	public void updateAccount(long account_id, DBRow updateRow)
			throws Exception
	{
		try{
			 floorAccountMgrZr.updateAccount(account_id, updateRow);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateAccount",log);
		}
		
	}
	@Override
	public void deleteAccount(long account_id) throws Exception {
		try{
			floorAccountMgrZr.deleteAccountById(account_id);
		}catch (Exception e) {
			throw new SystemException(e,"deleteAccount",log);
		}
	}

	@Override
	public DBRow getAccountById(long account_id) throws Exception
	{
		try{
			return floorAccountMgrZr.getAccountById(account_id);
		}catch (Exception e) {
			throw new SystemException(e,"getAccountById",log);

		}
	}
}
