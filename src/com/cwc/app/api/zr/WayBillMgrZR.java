package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorWaybillLogMgrZJ;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.floor.api.zr.FloorWayBillOrderMgrZR;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.iface.zr.WayBillMgrIfaceZR;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.key.WaybillInternalOperKey;
import com.cwc.app.lucene.zr.WayBillIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class WayBillMgrZR implements WayBillMgrIfaceZR{

	static Logger log = Logger.getLogger("ACTION");
	
	
	private FloorWayBillOrderMgrZR  floorWayBillOrderMgrZR;
	private FloorWaybillLogMgrZJ floorWaybillLogMgrZJ;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private SystemConfigIFace systemConfig;
	

	@Override
	public DBRow[] getWalbill(DBRow queryRow,PageCtrl pc) throws Exception {
		 try{
				return floorWayBillOrderMgrZR.getWalbillByRow(queryRow,pc);
			 }catch (Exception e) {
				 throw new SystemException(e,"getWalbill",log);
			}
	}

	@Override
	public long addNewStoreBill(DBRow row) throws Exception {
		try{
			
			return floorWayBillOrderMgrZR.addNewStoreBill(row);
		 }catch (Exception e) {
			 throw new SystemException(e,"getWalbill",log);
		}
	}

	@Override
	public DBRow assignStore(DBRow row) throws Exception {
		/*
		 * 1. 首先是根据ps_id去查询当前有没有唯一的一个开放的出库单,如果有直接放在里面
		 * 2. 如果是根据ps_id去查询.如果没有直接创建一个然后放在里面
		 */
		DBRow result = new DBRow();
		long id = Long.parseLong(row.getString("ps_id"));
		DBRow[] rows = floorWayBillOrderMgrZR.getStoresByPsIdAndState(id,"1");
		if(rows.length == 0 ){
			//创建一个新的然后添加信息
			DBRow storeRow = new DBRow();
			storeRow.add("ps_id", row.getString("ps_id"));
			long storeId = floorWayBillOrderMgrZR.addNewStoreBill(storeRow);
			floorWayBillOrderMgrZR.updateWayBillStore(storeId, row.getString("ids"));
			result.add("info","新创建一个出库单,然后加入了运单");
			result.add("flag", "success");
			result.add("state","addinnew");
			result.add("stroe", storeId +"");
		}
		if(rows.length > 1){
			result.add("info", "请你选择出库单");
			result.add("flag", "success");
			result.add("state", "selectstore");
		}
		if(rows.length == 1){
			//直接添加到唯一的里面
			long storeId = Long.parseLong(rows[0].getString("out_id"));
			floorWayBillOrderMgrZR.updateWayBillStore(storeId, row.getString("ids"));
			result.add("info", "添加到唯一里面");
			result.add("flag", "success");
			result.add("state", "addinonlyone");
			result.add("stroe", storeId+"");
		}
		return result;
	}

	@Override
	public DBRow[] getAllStoreIsOpenAndPsId(long psId) throws Exception {
		try{
			
			return floorWayBillOrderMgrZR.getAllStoreIsOpenAndPsId(psId);
		 }catch (Exception e) {
			 throw new SystemException(e,"getAllStoreIsOpenAndPsId",log);
		}
	}
	 
	@Override
	public void updateWallBillByOutId(String ids, long outId) throws Exception {
		try{
			floorWayBillOrderMgrZR.updateWayBillStore(outId,ids);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateWallBillByOutId",log);
		}
	}

	@Override
	public DBRow[] getAllOrderItemsInWayBillById(long wayBillid)
			throws Exception {
		try{
			return  floorWayBillOrderMgrZR.getAllItemInWayBillById(wayBillid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getAllOrderItemsInWayBillById",log);
		}
	}

	@Override
	public DBRow splitWayBill(String ids, String numbers, long wayId)
			throws Exception {
		//要重新的计算运费等等
		DBRow result = new DBRow();
		try{
			//
			DBRow wayBill =  floorWayBillOrderMgrZR.getWayBillById(wayId);
			if(wayBill != null){
				wayBill.remove("waybill_id");
				wayBill.add("parent_waybill_id", wayId);
				//添加一个运单
				long newAddWayId = floorWayBillOrderMgrZR.addWayBillByDBRow(wayBill);
				// 更新WOI中的
				String[] idArray = ids.split(",");
				String[] numberArray = numbers.split(",");
				for(int index = 0 , count = idArray.length ; index < count ;index++ ){
					floorWayBillOrderMgrZR.updateWayItemByWayId(idArray[index], numberArray[index], wayId, newAddWayId);
				}
				//读取新的数据和已经修改的数据
				//result = floorWayBillOrderMgrZR.getWayItemsByWayId(wayId);
			}
			return  result;
		 }catch (Exception e) {
			 throw new SystemException(e,"splitWayBill",log);
		}
	}

	@Override
	public DBRow[] getSplitInfoByWayBillId(long wayBillid) throws Exception {
		try{
			return  floorWayBillOrderMgrZR.getSplitInfoById(wayBillid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getSplitInfoByWayBillId",log);
		}
	}
	/**
	 * 1. 查询出该运单下的所有Item,然后把Item加回到原来的运单
	 * 2. 如果原来的运单是没有改Item 然后就Insert，有的话就把原来的增加Item的数量。这个时候要注意状态的改变
	 * 3. 然后删除取消运单的下的Item。
	 * 4. 然后这个运单删除
	 * 5. 把原来运单中的quantity,shipping_cost,prodcut_cost,weight,saleroom 按照比例计算
	 * 6. 更改原始运单中的状态
	 */
	@Override
	public void deleteSplitWayBillBy(long wayBillId ,long parentWayBillId,AdminLoginBean adminLoggerBean) throws Exception {
		 try
		 {
			 DBRow[] rows = floorWayBillOrderMgrZR.getAllItemInWayBillById(wayBillId);
			 
			 if(null != rows && rows.length > 0)
			 {
				 for(int index = 0 , count = rows.length ; index < count ; index++ )
				 {
					 floorWayBillOrderMgrZR.cencelWayBillItems(rows[index],parentWayBillId);
				 }
			 }
			 
			 
			DBRow waybillOrder = floorWayBillOrderMgrZR.getWayBillById(wayBillId);
				
			DBRow waybillLog = new DBRow();
			waybillLog.add("waybill_id",wayBillId);
			waybillLog.add("waybill_internal_status",WaybillInternalOperKey.Cancel);
			waybillLog.add("trackingNumber",waybillOrder.getString("tracking_number"));
			waybillLog.add("sc_id",waybillOrder.get("sc_id",0l));
			waybillLog.add("operator_adid",adminLoggerBean.getAdid());
			waybillLog.add("operator_time",DateUtil.NowStr());
			floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
			
			wayBillMgrZJ.addOrderNoteWithWaybill(wayBillId,WaybillInternalOperKey.Cancel, adminLoggerBean);
			

				
			floorWayBillOrderMgrZR.deleteWayBillById(wayBillId);
			WayBillIndexMgr.getInstance().deleteIndex(wayBillId);
			int state = floorWayBillOrderMgrZR.updateWayBillStatus(parentWayBillId);
			
			if(state == WayBillOrderStatusKey.WAITPRINT)
			{
				DBRow parentRow = floorWayBillOrderMgrZR.getWayBillById(parentWayBillId);
				DBRow parentWaybillLog = new DBRow();
				parentWaybillLog.add("waybill_id",parentWayBillId);
				parentWaybillLog.add("waybill_internal_status",WaybillInternalOperKey.Recover);
				parentWaybillLog.add("trackingNumber",parentRow.getString("tracking_number"));
				parentWaybillLog.add("sc_id",parentRow.get("sc_id",0l));
				parentWaybillLog.add("operator_adid",adminLoggerBean.getAdid());
				parentWaybillLog.add("operator_time",DateUtil.NowStr());
				floorWaybillLogMgrZJ.addWaybillLog(waybillLog);
				
				wayBillMgrZJ.addOrderNoteWithWaybill(parentWayBillId,WaybillInternalOperKey.Recover, adminLoggerBean);
				
				DBRow tracePara = new DBRow();
				tracePara.add("trace_date",DateUtil.NowStr());
				tracePara.add("trace_flag",1);
				floorWayBillOrderMgrZJ.modWayBillOrderById(parentWayBillId, tracePara);
				
			}
			 
		 }
		 catch (Exception e)
		 {
			 throw new SystemException(e,"deleteSplitWayBillBy",log);
		 }
		
	}

	@Override
	public DBRow[] getWayOrderItem(String ids, String numbers) throws Exception {
		 
		try{
			 String[] arrayId = ids.split(",");
			 DBRow[] rows = new DBRow[arrayId.length];
			 String[] arrayNumber = numbers.split(",");
			 if(arrayId  != null && arrayId.length > 0){
				 for(int index = 0 , count = arrayId.length ; index < count ; index++ ){
					 DBRow row = floorWayBillOrderMgrZR.getWayBillOrderItemInfo(arrayId[index]);
					 row.add("cart_quantity",arrayNumber[index]);
					 rows[index] = row;
				 }
			 }
			 return rows;
		 }catch (Exception e) {
			 throw new SystemException(e,"getWayOrderItem",log);
		}
	}

	@Override
	public DBRow getWaybillInfo(long wayBillId) throws Exception {
		try{
			return floorWayBillOrderMgrZR.getWayBillById(wayBillId);
		 }catch (Exception e) {
			 throw new SystemException(e,"getWaybillInfo",log);
		}
	}

	@Override
	public DBRow getSaleRoomByWalBillId(long wayBillId) throws Exception {
		try{
			return floorWayBillOrderMgrZR.getWayBillSaleRoomById(wayBillId);
		 }catch (Exception e) {
			 throw new SystemException(e,"getSaleRoomByWalBillId",log);
		}
	}

	@Override
	public DBRow getUserNameById(long adid)
			throws Exception {
		try{
			return floorWayBillOrderMgrZR.getUserNameByAdid(adid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getStateAndUserInfoBy",log);
		}
		 
	}
	// 这个是指定状态的
	@Override
	public void updateWayBillStatus(long wayBillId, DBRow updateRow)
			throws Exception {
		try{
			floorWayBillOrderMgrZR.updateWayBillStatus(wayBillId, updateRow);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateWayBillStatus",log);
		}
	}

	@Override
	public DBRow getPostDateByOid(long oid) throws Exception {
		try{
			return floorWayBillOrderMgrZR.getPostDateByOid(oid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getPostDateByOid",log);
		}
	}

	@Override
	public DBRow[] getOutBoundByPsId(long ps_id, PageCtrl pc) throws Exception {
		
		try{
			return floorWayBillOrderMgrZR.getOutBoundListByPsId(ps_id, pc);
		 }catch (Exception e) {
			 throw new SystemException(e,"getOutBoundByPsId",log);
		}
	 
	}

	@Override
	public DBRow[] getWayBillListByOutId(long out_id) throws Exception {
		try{
			return floorWayBillOrderMgrZR.getWayBillByOutId(out_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getWayBillListByOutId",log);
		}
	}

	@Override
	public DBRow[] getOutBoundByRow(DBRow queryRow, PageCtrl pc)
			throws Exception {
		
		String cmd = queryRow.getString("cmd");
		try{
			if(cmd != null && cmd.length() > 0 ){
				if(cmd.equals("query")){
					long outId = queryRow.get("out", 0l);
					return floorWayBillOrderMgrZR.getOutBoundByNumber(outId,pc);
				}
				if(cmd.equals("filter")){
					return floorWayBillOrderMgrZR.getOutBoundByFilter(queryRow,pc);
				}
				return null;	
			}else{
				return  null ;
			}
		}catch (Exception e) {
			throw new SystemException(e,"getOutBoundByRow",log); 
		}
	 
	}

	@Override
	public void updateOutBoundByRow(DBRow updateRow) throws Exception {
		try{
			floorWayBillOrderMgrZR.updateBoundByRow(updateRow);
		}catch (Exception e) {
			throw new SystemException(e,"updateOutBoundByRow",log); 
		}
		
	}

	@Override
	public DBRow[] getWayBillByOutIdPc(long out_id, PageCtrl pc)
			throws Exception {
		try{
			return floorWayBillOrderMgrZR.getWaybillByOutIdPc(out_id, pc);
		}catch (Exception e) {
			throw new SystemException(e,"getWayBillByOutIdPc",log); 
		}
	}
	@Override
	public DBRow[] getWayBillBySearchKey(String searchKey,int search_mode,PageCtrl pc)
			throws Exception {
		try
		{
			int page_count = systemConfig.getIntConfigValue("page_count");
			return  WayBillIndexMgr.getInstance().getSearchResults(searchKey,search_mode,page_count,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillBySearchKey",log); 
		}
	}
	
	public DBRow[] getWayBillNoteBy(long waybillId,int  number) 
		throws Exception 
	{
		try
		{
			return floorWayBillOrderMgrZR.getWaybillNoteByWayBillId(waybillId, number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWayBillBySearchKey",log); 
		}
	}
	
	public void setFloorWaybillLogMgrZJ(FloorWaybillLogMgrZJ floorWaybillLogMgrZJ) {
		this.floorWaybillLogMgrZJ = floorWaybillLogMgrZJ;
	}
	
	public void setFloorWayBillOrderMgrZR(
			FloorWayBillOrderMgrZR floorWayBillOrderMgrZR) {
		this.floorWayBillOrderMgrZR = floorWayBillOrderMgrZR;
	}

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
}
