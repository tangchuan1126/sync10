package com.cwc.app.api.zr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zr.FloorBoxTypeZr;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zr.BoxSkuPsMgrIfaceZr;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.app.iface.zr.ContainerMgrIfaceZr;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.ImportBoxErrorKey;
import com.cwc.app.key.MoreLessOrEqualKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class BoxTypeMgrZr implements BoxTypeMgrIfaceZr {

	
	/**
	 * xls.中每个字段对应的Index的值
	 */
	private static final int box_product_name = 0 ; 
	private static final int container_type_name = 1 ;
	private static final int box_type_name = 2 ;
	private static final int box_total_length = 3 ;
	private static final int box_total_width = 4 ;
	private static final int box_total_height = 5 ;
	
	private static final int box_length = 6 ;
	private static final int box_width = 7 ;
	private static final int box_height = 8 ;
	private static final int box_weight  = 9;
	
	 
	
	static Logger log = Logger.getLogger("ACTION");

	private FloorBoxTypeZr floorBoxTypeZr;

	private ProductMgrIFace productMgr;

	private ContainerMgrIfaceZr containerMgrZr ;
	
	private BoxSkuPsMgrIfaceZr boxSkuPsMgrZr;
	
	private	FloorProductStoreMgr productStoreMgr;
 	
	


	@Override
	public DBRow[] getAllBoxTypeByPage(PageCtrl page) throws Exception {
		try {

			return floorBoxTypeZr.getAllBoxTypeByPage(page);
		} catch (Exception e) {
			throw new SystemException(e, "getAllBoxTypeByPage", log);
		}
	}

	@Override
	public DBRow addBoxTypeAdd(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		result.add("flag", "error");
		try {
			// 获取数据
			int box_total_length = StringUtil.getInt(request,
					"box_total_length");
			int box_total_width = StringUtil.getInt(request, "box_total_width");
			int box_total_height = StringUtil.getInt(request,
					"box_total_height");

			double box_length = StringUtil.getDouble(request, "box_length");
			double box_width = StringUtil.getDouble(request, "box_width");
			double box_height = StringUtil.getDouble(request, "box_height");
			int container_type_id = StringUtil.getInt(request, "container_type_id");

			double box_weight = StringUtil.getDouble(request, "box_weight");

			int box_total = box_total_length * box_total_width
					* box_total_height;

			if (box_total < 1) {
				result.add("flag", "dataFormat");
				return result;
			}
			String box_type_name = StringUtil.getString(request,
					"box_type_name");
			String box_product_name = StringUtil.getString(request,
					"box_product_name");
			String length_uom = StringUtil.getString(request, "length_uom");
			String weight_uom = StringUtil.getString(request, "weight_uom");
			// 查询PCID。如果没有那么报错
			long box_pc_id = 0l;
			if (box_product_name.trim().length() > 1) {
				DBRow row = productMgr
						.getDetailProductByPname(box_product_name);
				if (row != null) {
					box_pc_id = row.get("pc_id", 0l);
				}
			}
			if (box_pc_id == 0l) {

				result.add("flag", "productNotFound");
			} else {
				DBRow data = new DBRow();
				data.add("box_type_name", box_type_name);
				data.add("box_total_length", box_total_length);
				data.add("box_total_width", box_total_width);
				data.add("box_total_height", box_total_height);
				data.add("box_pc_id", box_pc_id);
				data.add("box_total", box_total);
				data.add("box_length", box_length);
				data.add("box_width", box_width);
				data.add("box_height", box_height);
				data.add("box_weight", box_weight);
				data.add("container_type_id", container_type_id);
				data.add("length_uom", length_uom);
				data.add("weight_uom", weight_uom);
				
				data.add("container_type", 2);
				floorBoxTypeZr.addBoxType(data);
				result.add("flag", "success");
			}
			return result;

		} catch (Exception e) {
			throw new SystemException(e, "addBoxTypeAdd", log);
		}
	}

	@Override
	public void deleteBoxType(HttpServletRequest request) throws Exception {
		try {
			long type_id = StringUtil.getLong(request, "type_id");
			deleteBoxTypeById(type_id);
		} catch (Exception e) {
			throw new SystemException(e, "deleteBoxType", log);
		}
	}

	@Override
	public void deleteBoxTypeById(long type_id) throws Exception {
		try {
			floorBoxTypeZr.deleteBoxType(type_id);
		} catch (Exception e) {
			throw new SystemException(e, "deleteBoxTypeById", log);
		}

	}

	@Override
	public DBRow updateBoxType(HttpServletRequest request) throws Exception {

		DBRow result = new DBRow();
		result.add("flag", "error");
		try {
			// 获取数据
			int box_total_length = StringUtil.getInt(request,
					"box_total_length");
			int box_total_width = StringUtil.getInt(request, "box_total_width");
			int box_total_height = StringUtil.getInt(request,
					"box_total_height");
			long type_id = StringUtil.getLong(request, "box_type_id");

			double box_length = StringUtil.getDouble(request, "box_length");
			double box_width = StringUtil.getDouble(request, "box_width");
			double box_height = StringUtil.getDouble(request, "box_height");

			double box_weight = StringUtil.getDouble(request, "box_weight");
			
			int container_type_id = StringUtil.getInt(request, "container_type_id");

			int box_total = box_total_length * box_total_width
					* box_total_height;

			if (box_total < 1) {
				result.add("flag", "dataFormat");
				return result;
			}
			String box_type_name = StringUtil.getString(request,
					"box_type_name");
			String box_product_name = StringUtil.getString(request,
					"box_product_name");
			// 查询PCID。如果没有那么报错
			long box_pc_id = 0l;
			if (box_product_name.trim().length() > 1) {
				DBRow row = productMgr
						.getDetailProductByPname(box_product_name);
				if (row != null) {
					box_pc_id = row.get("pc_id", 0l);
				}
			}
			if (box_pc_id == 0l) {

				result.add("flag", "productNotFound");
			} else {
				DBRow data = new DBRow();
				data.add("box_type_name", box_type_name);
				data.add("box_total_length", box_total_length);
				data.add("box_total_width", box_total_width);
				data.add("box_total_height", box_total_height);
				data.add("box_pc_id", box_pc_id);
				data.add("box_total", box_total);
				data.add("box_length", box_length);
				data.add("box_width", box_width);
				data.add("box_height", box_height);
				data.add("box_weight", box_weight);
				data.add("container_type_id", container_type_id);
				updateBoxType(type_id, data);
				result.add("flag", "success");
			}
			return result;

		} catch (Exception e) {
			throw new SystemException(e, "addBoxTypeAdd", log);
		}

	}

	@Override
	public void updateBoxType(long type_id, DBRow data) throws Exception {
		try {
			floorBoxTypeZr.updateBoxType(type_id, data);
		} catch (Exception e) {
			throw new SystemException(e, "updateBoxType", log);
		}

	}

	@Override
	public DBRow getBoxTypeBuId(long type_id) throws Exception {
		try {
			return floorBoxTypeZr.getBoxTypeById(type_id);
		} catch (Exception e) {
			throw new SystemException(e, "getBoxTypeBuId", log);
		}

	}

	@Override
	public DBRow[] checkBoxXls(String fileName) throws Exception  {
		List<DBRow> errorRows = new ArrayList<DBRow>();
		boolean hasData =  false ; 	// 是否包含有数据
		String path = "";
		InputStream is = null ;
		Workbook wrb = null ;
		path = Environment.getHome() + "upl_imags_tmp/" + fileName.trim();
		try {
			is = new FileInputStream(new File(path));
			wrb = Workbook.getWorkbook(is);
			Sheet sheet= wrb.getSheet(0);
			Map<String,DBRow> products = new HashMap<String,DBRow>();
			Map<String,DBRow> containers = new HashMap<String, DBRow>();
			if(sheet != null){
				int count = sheet.getRows();
				if(count > 1){
					hasData = true ;
					for(int row = 1 , rowCount = count ; row < rowCount ; row++ ){
						Cell[] arrays = sheet.getRow(row);
						DBRow boxTyperow = new DBRow();
						for(int cellIndex = 0 ; arrays != null && cellIndex < arrays.length ; cellIndex++ ){
							String cellValue = arrays[cellIndex].getContents() ;
							switch (cellIndex) {
								case box_product_name :{boxTyperow.add("box_product_name", cellValue);} break ;
								case container_type_name:{boxTyperow.add("container_type_name", cellValue);}break ;
								case box_type_name :{boxTyperow.add("box_type_name", cellValue);} break ;
							
								case box_total_length :{boxTyperow.add("box_total_length", cellValue);} break ;
								case box_total_width :{boxTyperow.add("box_total_width", cellValue);} break ;
								case box_total_height :{boxTyperow.add("box_total_height", cellValue);} break ;
							
								case box_length :{boxTyperow.add("box_length", cellValue);} break ;
								case box_width :{boxTyperow.add("box_width", cellValue);} break ;
								case box_height :{boxTyperow.add("box_height", cellValue);} break ;
								case box_weight :{boxTyperow.add("box_weight", cellValue);} break ;
								default : break;
							}
 						}
						boolean isCheck = checkDBRowData(boxTyperow,products,containers) ;
						if(!isCheck){boxTyperow.add("errorIndex", row+1); errorRows.add(boxTyperow);}
	 				}
				}
				
			}
			return errorRows.toArray(new DBRow[errorRows.size()]);
		} catch (FileNotFoundException e) {
			 throw e ;
		}catch (Exception e) {
			throw new SystemException(e, "checkBoxXls", log);
		}finally{
			try{
				if(is != null){is.close();}
				if(wrb != null){wrb.close();}
			}catch (Exception e) {}
		}
		 
		 
 	}
	/**
	 * 1.pcid 
	 * 2.l,w,g
	 * 3.length , width , hight 
	 * 4.weight 
	 * 检查是出这条记录的所有的错误. 如果商品前面已经查询过了，就不应该查询
	 * @param row
	 */
	private boolean checkDBRowData(DBRow row , Map<String,DBRow> products,Map<String,DBRow> containers) throws Exception{
		boolean isValidate = true ;
		List<String> errors = new ArrayList<String>(); 
		try{
			String productName = row.getString("box_product_name").trim();
			String box_type_name  = row.getString("box_type_name").trim();
			String container_type_name = row.getString("container_type_name").trim();
			
			int box_total_length = row.get("box_total_length", 0);
			int box_total_width  = row.get("box_total_width", 0);
			int box_total_height = row.get("box_total_height", 0);
			
			double box_length = row.get("box_length", 0.0d);
			double box_width = row.get("box_width", 0.0d);
			double box_height = row.get("box_height", 0.0d);
			
			double box_weight = row.get("box_weight", 0.0d);
			
			
			if(box_type_name == null || box_type_name.trim().length() < 1){
				isValidate  = false ;
				errors.add(ImportBoxErrorKey.BOXTYPENAME);
			} 
			
			//商品是否存在的检查
			if(productName != null && productName.trim().length() > 0 && products.get(productName.trim()) == null){
				
				DBRow productRow = productMgr.getDetailProductByPname(productName.trim());
			 
				if(productRow != null){
					products.put(productName.trim(), productRow);
					row.add("box_pc_id", productRow.get("pc_id", 0l));
				}else{isValidate = false ;errors.add(ImportBoxErrorKey.PRODUCTNOTFOUND);}
			}else{
				if(products.get(productName.trim()) == null){
					isValidate = false ;errors.add(ImportBoxErrorKey.PRODUCTNOTFOUND);
				}else{
					row.add("box_pc_id", products.get(productName.trim()).get("pc_id", 0l));
				}
			}
			//盒子的类型是否存在.同样的container_type_name只会查询一次。
			if(container_type_name != null && container_type_name.trim().length() > 0 && containers.get(container_type_name) == null){
				DBRow container =  floorBoxTypeZr.getContainerByName(container_type_name);
				if(container != null){
					containers.put(container_type_name, container);
					row.add("container_type_id", container.get("type_id", 0l));
				}else{
					isValidate = false ; errors.add(ImportBoxErrorKey.CONTAINERTYPE);
				}
			}else{
				if(containers.get(container_type_name.trim()) == null){
					isValidate = false ;errors.add(ImportBoxErrorKey.CONTAINERTYPE);
				}else{
					row.add("container_type_id", containers.get(container_type_name.trim()).get("type_id", 0l));
				}
			}
			
			//盒子容量的检查
			if(box_total_length * box_total_width * box_total_height < 1){
				isValidate = false ;
				errors.add(ImportBoxErrorKey.QUANTITYERROR);
			}else{
				row.add("box_total", box_total_length * box_total_width * box_total_height);
			}
			//检查盒子体积
			if(box_length * box_width * box_height < 1){
				isValidate = false ;
				errors.add(ImportBoxErrorKey.VOLUMEERROR);
			}
			if(box_weight <= 0.0d){
				isValidate = false ;
				errors.add(ImportBoxErrorKey.WEIGHTERROR);
			}
			if(!isValidate){row.add("erros", errors);}
			return isValidate;
		}catch (Exception e) {
			throw new SystemException(e, "checkDBRowData", log);
 		}
	}

	@Override
	public void addBoxByFile(HttpServletRequest request) throws Exception {
		String fileName = StringUtil.getString(request, "file_name");
		String path = "";
		InputStream is = null ;
		Workbook wrb = null ;
		path = Environment.getHome() + "upl_imags_tmp/" + fileName.trim();
		List<DBRow> rows = new ArrayList<DBRow>();
		try{
			Map<String,DBRow> products = new HashMap<String,DBRow>();
			Map<String,DBRow> containers = new HashMap<String, DBRow>();

			is = new FileInputStream(new File(path));
			wrb = Workbook.getWorkbook(is);
			Sheet sheet= wrb.getSheet(0);
			if(sheet != null){
				int count = sheet.getRows();
				if(count > 1){
 					for(int row = 1 , rowCount = count ; row < rowCount ; row++ ){
						Cell[] arrays = sheet.getRow(row);
						DBRow boxTyperow = new DBRow();
						for(int cellIndex = 0 ; arrays != null && cellIndex < arrays.length ; cellIndex++ ){
							String cellValue = arrays[cellIndex].getContents() ;
							switch (cellIndex) {
								case box_product_name :{boxTyperow.add("box_product_name", cellValue);} break ;
								case container_type_name : {boxTyperow.add("container_type_name", cellValue);}break ;
								case box_type_name :{boxTyperow.add("box_type_name", cellValue);} break ;
							
								case box_total_length :{boxTyperow.add("box_total_length", cellValue);} break ;
								case box_total_width :{boxTyperow.add("box_total_width", cellValue);} break ;
								case box_total_height :{boxTyperow.add("box_total_height", cellValue);} break ;
							
								case box_length :{boxTyperow.add("box_length", cellValue);} break ;
								case box_width :{boxTyperow.add("box_width", cellValue);} break ;
								case box_height :{boxTyperow.add("box_height", cellValue);} break ;
								case box_weight :{boxTyperow.add("box_weight", cellValue);} break ;
								default : break;
							}
 						}
						boolean isCheck = checkDBRowData(boxTyperow,products,containers) ;
						if(isCheck){rows.add(boxTyperow);}
	 				}
 					if(rows.size() > 0 ){
 						addBoxTypes(rows);
 					}
				}
				
			}
		 
		}catch (Exception e) {
			throw new SystemException(e, "addBoxByFile", log);
		}finally{
			try{
				if(is != null){is.close();}
				if(wrb != null){wrb.close();}
			}catch (Exception e) {}
		}
 	}
	private void addBoxTypes(List<DBRow> arrayList) throws Exception{
		try{
			for(DBRow temp : arrayList){
				temp.remove("box_product_name");
				temp.remove("container_type_name");
				floorBoxTypeZr.addBoxType(temp);
			}
			
		}catch (Exception e) {
			throw new SystemException(e, "addBoxTypes", log);

 		}
	}

	@Override
	public DBRow[] getBoxContainerType(int container_type) throws Exception {
		try{
			return floorBoxTypeZr.getBoxContainerType(container_type);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxContainerType", log);
		}
	}

	@Override
	public DBRow[] queryBoxTypeBySearchKeyAndContainerType(String search_key , long container_type_id , PageCtrl pc)
			throws Exception {
		try{
			DBRow[] results = floorBoxTypeZr.getBoxTypeByNameAndContainerType(search_key, container_type_id, pc);
 			return results;
		}catch (Exception e) {
			throw new SystemException(e, "queryBoxTypeBySearchKeyAndContainerType", log);
		} 
	}

	@Override
	public DBRow exportBoxType(HttpServletRequest request) throws Exception {
		try{
			DBRow result = new DBRow();
			String cmd = StringUtil.getString(request, "cmd").trim();
			if(cmd.endsWith("template")){
  				result.add("flag", "success");
				result.add("fileurl", "upload/template/export_box_sku_template.xls");
				return result ;
			}
			if(cmd.equals("query")){
				long pc_id = StringUtil.getLong(request, "pc_id");
				String search_key = StringUtil.getString(request, "search_key");
				long container_type_id = StringUtil.getLong(request, "container_type_id");
				DBRow[] rows = queryBoxTypeBySearchKeyAndContainerType(search_key, container_type_id, null);
				
				String path = createExportFile(rows);
				result.add("flag", "success");
				result.add("fileurl", path);
				return result;
			}
			if(cmd.equals("all")){
				DBRow[] rows = queryBoxTypeBySearchKeyAndContainerType("", 0, null);
				String path = createExportFile(rows);
				result.add("flag", "success");
				result.add("fileurl", path);
				return result;
			}
			return null;
		}catch (Exception e) {
			throw new SystemException(e, "exportBoxType", log);
 		}
 	 
	}
	private String createExportFile(DBRow[] rows) throws Exception {
		String filePath = "" ;
		HSSFWorkbook wb = null ;
		FileOutputStream os = null ;
		try{
			if(rows != null && rows.length > 0){
					POIFSFileSystem fs = null;   //获取模板
					fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/upload/template/export_box_sku_template.xls"));
					wb = new HSSFWorkbook(fs);
					HSSFSheet sheet = wb.getSheetAt(0);
					HSSFRow row = sheet.getRow(0);
					HSSFCellStyle style = wb.createCellStyle();
					style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
					style.setLocked(false);
					style.setWrapText(true);
					
					
					HSSFCellStyle stylelock = wb.createCellStyle();
					stylelock.setLocked(true); //创建一个锁定样式
					stylelock.setAlignment(HSSFCellStyle.ALIGN_CENTER);
					Map<Long,DBRow> products = new HashMap<Long, DBRow>();
					for(int i = 0 ; i< rows.length ; i++){
						row = sheet.createRow((int)i+1);
						sheet.setDefaultColumnWidth(30);
						DBRow box = rows[i];
						//设置excel列的值
						//查询一次product_name 如果是查询过一次就不应该查询了
						DBRow product = getProduct(box, products);
  						row.createCell(box_product_name).setCellValue(product != null ? product.getString("p_name") : "");
						row.getCell(box_product_name).setCellStyle(style);
						
						row.createCell(container_type_name).setCellValue(box.getString("type_name"));
						row.getCell(container_type_name).setCellStyle(style);
						
						row.createCell(box_type_name).setCellValue(box.getString("box_type_name"));
						row.getCell(box_type_name).setCellStyle(style);
						//l,w.h 各放几个
						row.createCell(box_total_length).setCellValue(box.get("box_total_length",0));
						row.getCell(box_total_length).setCellStyle(style);
						
						row.createCell(box_total_width).setCellValue(box.get("box_total_width",0));
						row.getCell(box_total_width).setCellStyle(style);
						
						row.createCell(box_total_height).setCellValue(box.get("box_total_height",0));
						row.getCell(box_total_height).setCellStyle(style);
 						
						//length , width , height 
						
						row.createCell(box_length).setCellValue(box.get("box_total_length",0.0d));
						row.getCell(box_length).setCellStyle(style);
						
						row.createCell(box_width).setCellValue(box.get("box_width",0.0d));
						row.getCell(box_width).setCellStyle(style);
						
						row.createCell(box_height).setCellValue(box.get("box_height",0.0d));
						row.getCell(box_height).setCellStyle(style);
					 
						
						//weight
						row.createCell(box_weight).setCellValue(box.get("box_weight",0.0d));
						row.getCell(box_weight).setCellStyle(style);
			  
					 
					}
					
					filePath = "upl_excel_tmp/export_box_sku_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
					os= new FileOutputStream(Environment.getHome()+filePath);
					wb.write(os);  
				  
				}
			 
			return filePath ;
		}catch (Exception e) {
			throw new SystemException(e, "exportBoxType", log);
		}finally{
			if(os != null){ os.close();}
 		}
	}
	 private DBRow getProduct(DBRow box , Map<Long,DBRow> products) throws Exception{
		 try{
			 long pcid = box.get("box_pc_id", 0l);
			 DBRow product = null ;
			 if(products.get(pcid) != null){
				 return products.get(pcid);
			 }
		 
			if(pcid != 0l  ){
				 product = productMgr.getDetailProductByPcid(pcid);
 				 if(product != null){
					 products.put(pcid, product);
					 return product ;
				 }
				 
			}
			return product;
		 }catch (Exception e) {
			throw new SystemException(e, "getProduct", log);
		}
	 }

	@Override
	public DBRow[] getBoxTypesByPcId(long pcid, int limit) throws Exception {
		try{
			return  floorBoxTypeZr.getBoxTypesByPcId(pcid,limit) ;
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypesByPcId", log);

 		}
 	}

	@Override
	public DBRow[] getBoxTypesByPcIdAndContainerTypeId(long pcid, long container_type_id) throws Exception {
		try{
			return floorBoxTypeZr.getBoxTypesByPcIdAndContainerTypeId(pcid, container_type_id);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypesByPcIdAndContainerTypeId", log);
		}
 	}

	@Override
	public DBRow addBoxSkuTypeAdd(HttpServletRequest request) throws Exception {
		try{


			DBRow result = new DBRow();
			result.add("flag", "error");
			try {
				// 获取数据
				long box_pc_id = StringUtil.getLong(request, "box_pc_id");
				int box_total_length = StringUtil.getInt(request,"box_total_length");
				int box_total_width = StringUtil.getInt(request, "box_total_width");
				int box_total_height = StringUtil.getInt(request,"box_total_height");
                int bh_type = StringUtil.getInt(request,"bh_type");
				double box_length = StringUtil.getDouble(request, "box_length");
				double box_width = StringUtil.getDouble(request, "box_width");
				double box_height = StringUtil.getDouble(request, "box_height");

				double box_weight = StringUtil.getDouble(request, "box_weight");
				int is_has_sn = StringUtil.getInt(request, "is_has_sn");
				int container_type_id = StringUtil.getInt(request, "container_type_id");
				long to_ps_id = StringUtil.getLong(request, "to_ps_id");
				String length_uom = StringUtil.getString(request, "length_uom");
				String weight_uom = StringUtil.getString(request, "weight_uom");
				int box_total = box_total_length * box_total_width
						* box_total_height;
				if (box_total < 1) {
					result.add("flag", "dataFormat");
					return result;
				}
				String box_type_name = StringUtil.getString(request,"box_type_name");
				 
 				//如果bh_type不等于0 里面装的是ilp 
				int product_total=0;
				if(bh_type!=0)
				{
					DBRow ilp=this.floorBoxTypeZr.findIlpByIlpId(bh_type);
					product_total=box_total*ilp.get("stack_length_qty", 0)*ilp.get("stack_width_qty", 0)*ilp.get("stack_height_qty", 0);
				}
				 
				if (box_pc_id == 0l) {

					result.add("flag", "productNotFound");
				} else {
					//验证此BLP类型是否存在 
					int cn = floorBoxTypeZr.findBlpTypeByBasicLWW(box_pc_id, container_type_id,bh_type, box_total_length, box_total_width, box_total_height);
					if(0 == cn)
					{
						DBRow data = new DBRow();
						data.add("stack_length_qty", box_total_length);
						data.add("stack_width_qty", box_total_width);
						data.add("stack_height_qty", box_total_height);
						data.add("pc_id", box_pc_id);
						data.add("inner_total_lp", box_total);
						if(bh_type==0)
						{
							
							data.add("inner_total_pc", box_total);
						}
						else
						{
							data.add("inner_total_pc", product_total);
							data.add("inner_pc_or_lp", bh_type);
						}
						data.add("ibt_length", box_length);
						data.add("ibt_width", box_width);
						data.add("ibt_height", box_height);
						data.add("ibt_weight", box_weight);
						data.add("is_has_sn", is_has_sn);
						data.add("basic_type_id", container_type_id);
						data.add("sort",getBoxTypeSort(box_pc_id));
						data.add("length_uom", length_uom);
						data.add("weight_uom", weight_uom);
						
						data.add("container_type", 2);
						long box_type_id = floorBoxTypeZr.addBoxType(data);
						
						if(to_ps_id > 0)
						{
							DBRow row = new DBRow();
							row.add("lp_pc_id", box_pc_id);
							row.add("lp_type_id", box_type_id);
							row.add("ship_to_id", to_ps_id);
							int box_sort = boxSkuPsMgrZr.getMaxIndexBy(box_pc_id, to_ps_id) + 1;
							row.add("ship_to_sort", box_sort);
							boxSkuPsMgrZr.addBoxSkuPs(row);
						}
						
						result.add("flag", "success");
					}
					else
					{
						result.add("flag", "this_type_exist");
					}
				}
				return result;

			} catch (Exception e) {
				throw new SystemException(e, "addBoxTypeAdd", log);
			}

		
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypesByPcIdAndContainerTypeId", log);
 		}
 	}

	@Override
	public DBRow updateBoxSkuType(HttpServletRequest request) throws Exception {

		DBRow result = new DBRow();
		result.add("flag", "error");
		try {
			// 获取数据
 			int box_total_length = StringUtil.getInt(request,"box_total_length");
			int box_total_width = StringUtil.getInt(request, "box_total_width");
			int box_total_height = StringUtil.getInt(request,"box_total_height");
			long type_id = StringUtil.getLong(request, "box_type_id");

			double box_length = StringUtil.getDouble(request, "box_length");
			double box_width = StringUtil.getDouble(request, "box_width");
			double box_height = StringUtil.getDouble(request, "box_height");

			double box_weight = StringUtil.getDouble(request, "box_weight");
			
			int container_type_id = StringUtil.getInt(request, "container_type_id");

			int box_total = box_total_length * box_total_width
					* box_total_height;

			if (box_total < 1) {
				result.add("flag", "dataFormat");
				return result;
			}
			String box_type_name = StringUtil.getString(request, "box_type_name");
			 
			// 查询PCID。如果没有那么报错
			 
		 
				DBRow data = new DBRow();
				data.add("box_type_name", box_type_name);
				data.add("box_total_length", box_total_length);
				data.add("box_total_width", box_total_width);
				data.add("box_total_height", box_total_height);
 				data.add("box_total", box_total);
				data.add("box_length", box_length);
				data.add("box_width", box_width);
				data.add("box_height", box_height);
				data.add("box_weight", box_weight);
				data.add("container_type_id", container_type_id);
				updateBoxType(type_id, data);
				result.add("flag", "success");
		 
			return result;

		} catch (Exception e) {
			throw new SystemException(e, "addBoxTypeAdd", log);
		}

	}

	@Override
	public DBRow exportBoxSkuType(HttpServletRequest request) throws Exception {
		try{

			DBRow result = new DBRow();
			String cmd = StringUtil.getString(request, "cmd").trim();
			if(cmd.endsWith("template")){
  				result.add("flag", "success");
				result.add("fileurl", "upload/template/export_box_sku_template.xls");
				return result ;
			}
			if(cmd.equals("query")){
				long pcid = StringUtil.getLong(request, "pc_id");
				long container_type_id = StringUtil.getLong(request, "container_type_id");
				DBRow[] rows = floorBoxTypeZr.getBoxTypesGetPNameAndContainerName(pcid, container_type_id);

				String path = createExportFile(rows);
				result.add("flag", "success");
				result.add("fileurl", path);
				return result;
			}
			if(cmd.equals("all")){
				long pcid = StringUtil.getLong(request, "pc_id");

				DBRow[] rows = floorBoxTypeZr.getBoxTypesGetPNameAndContainerName(pcid, 0);
				String path = createExportFile(rows);
				result.add("flag", "success");
				result.add("fileurl", path);
				return result;
			}
			return null;
		
		}catch (Exception e) {
			throw new SystemException(e, "addBoxTypeAdd", log);
 		}
 	}

	@Override
	public DBRow[] getBoxSkuTypeByPcId(long pc_id) throws Exception {
		try{
			return floorBoxTypeZr.getBoxSkuByPcId(pc_id);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxSkuTypeByPcId", log);
		}
	}

	@Override
	public DBRow addBoxContainer(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow data = new DBRow();
 			String hardwareId = StringUtil.getString(request, "hardwareId");
			long type_id  = StringUtil.getLong(request, "box_type_id");
			long container_type=StringUtil.getLong(request,"container_type");
			int is_has_sn = StringUtil.getInt(request, "is_has_sn");
			//查询hardwareId 是否是已经存在了
 			data.add("hardwareId", hardwareId);
 			/*
 			if(container_type==2){
				data.add("container_type", ContainerTypeKey.BLP);
 			}else if(container_type==4){
				data.add("container_type", ContainerTypeKey.ILP);
 			}
			*/
 			data.add("container_type", container_type);
			data.add("type_id", type_id );
			data.add("is_has_sn", is_has_sn);
			if(!StringUtil.isBlank(hardwareId))
			{
				if(floorBoxTypeZr.isExitsHardwareId(hardwareId)){
					result.add("flag", "error");
					result.add("message", "hardwareIdExits");
					return result;
	 			}
			}
			long id = addBoxContainer(data);
   			result.add("flag", "success");
			result.add("id", id);
 			return result; 
 		}catch (Exception e) {
			throw new SystemException(e, "addBoxContainer", log);
 		}
 	}
	private long addBoxContainer(DBRow row) throws Exception {
		try{
			DBRow insertRow = containerMgrZr.addContainer(row);
 			return insertRow != null ? insertRow.get("con_id", 0l) : 0l;
		}catch (Exception e) {
			throw new SystemException(e, "addBoxContainer", log);
 		}
	}

	@Override
	public DBRow[] getBoxContainerBy(PageCtrl page, long box_type_id,long container_type) throws Exception {
		try{
			return floorBoxTypeZr.getBoxContainer(page, box_type_id,container_type);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxContainerBy", log);

 		}
	}

	@Override
	public void deleteBoxContainer(long con_id) throws Exception {
 		try{
 			floorBoxTypeZr.deleteBoxContainer(con_id);
 		}catch (Exception e) {
			throw new SystemException(e, "deleteBoxContainer", log);

 		}
	}

	@Override
	public DBRow updateBoxContainer(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		try{

			DBRow data = new DBRow();
			String container = StringUtil.getString(request, "container");
			String hardwareId = StringUtil.getString(request, "hardwareId");
			long con_id = StringUtil.getLong(request, "con_id");
 			//查询hardwareId 是否是已经存在了
			data.add("container", container);
			data.add("hardwareId", hardwareId);
 			
			if(floorBoxTypeZr.isExitsHardwareId(hardwareId,con_id)){
				result.add("flag", "error");
				result.add("message", "hardwareIdExits");
				return result;
 			}
			updateBoxContainer(con_id, data);
			result.add("flag", "success");
			result.add("id", con_id);
			return result; 
 		
		}catch (Exception e) {
			throw new SystemException(e, "updateBoxContainer", log);
 		}
	}
	private void updateBoxContainer(long con_id ,DBRow data) throws Exception {
		try{
			floorBoxTypeZr.updateBoxContainer(con_id, data);
		}catch (Exception e) {
			throw new SystemException(e, "updateBoxContainer", log);
		}
	}

	@Override
	public DBRow[] getBoxContainerBySearchKey(PageCtrl page, long box_type_id, String search_key,long containerType) throws Exception {
		try{
			return floorBoxTypeZr.getBoxContainerBySearchKey(page, box_type_id,search_key,containerType);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxContainerBySearchKey", log);
 		}
 	 
	}

	@Override
	public boolean isExitsHardwareId(String hardwareId) throws Exception {
		try{
			return floorBoxTypeZr.isExitsHardwareId(hardwareId);
		}catch (Exception e) {
			throw new SystemException(e, "isExitsHardwareId", log);
 		}
	}

	@Override
	public String getBlpTypeName(long box_type_id) throws Exception {
		String blpTypeName = "";
		try{
			DBRow row = floorBoxTypeZr.getBoxTypeDetailById(box_type_id);
			if(row != null){
				blpTypeName = getBlpTypeName(row);
			}
			return blpTypeName ;
		}catch (Exception e) {
			throw new SystemException(e, "getBlpTypeName", log);
 		}
 
	}
	/**
	 * tatoo_2*3*4
	 */
	@Override
	public String getBlpTypeName(DBRow row) throws Exception {
		StringBuffer blpTypeName = new StringBuffer();
	 //	blpTypeName.append(row.getString("p_name")).append("_")
		blpTypeName.append(row.get("stack_length_qty", 0)).append("*")
		.append(row.get("stack_width_qty", 0)).append("*")
		.append(row.get("stack_height_qty", 0));
 		return blpTypeName.toString();
	}

	@Override
	public int getBoxTypeSort(long box_pc_id) throws Exception {
		try{
			return floorBoxTypeZr.countNumBy(box_pc_id) + 1;
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypeSort", log);
 		}
 		
	}

	@Override
	public void exchangeBoxTypeSort(long change_box_type_id,
			long change_to_box_type_id) throws Exception {
		try{
			DBRow changeRow = floorBoxTypeZr.getBoxTypeById(change_box_type_id);
			DBRow changeToRow = floorBoxTypeZr.getBoxTypeById(change_to_box_type_id);
			int changeIndex = changeRow.get("sort", 0);
			int changeToIndex =changeToRow.get("sort", 0);
			DBRow updateRow = new DBRow();
			//change
			updateRow.add("sort", changeToIndex );
			floorBoxTypeZr.updateBoxType(changeRow.get("lpt_id", 0), updateRow);
			//changeto
			updateRow.add("sort", changeIndex);
			floorBoxTypeZr.updateBoxType(changeToRow.get("lpt_id", 0), updateRow);
			
		}catch (Exception e) {
			throw new SystemException(e, "exchangeBoxTypeSort", log);
 		}
	}

	@Override
	public boolean isExitsHardwareId(String hardwareId, long con_id)
			throws Exception {
		try{
			return floorBoxTypeZr.isExitsHardwareId(hardwareId, con_id);
		}catch (Exception e) {
			throw new SystemException(e, "isExitsHardwareId", log);

 		}
 	}

	@Override
	public String getBlpTypeNameNeedPName(DBRow row) throws Exception {
			StringBuffer blpTypeName = new StringBuffer();
		 	blpTypeName.append(row.getString("p_name")).append("_")
			.append(row.get("stack_length_qty", 0)).append("*")
			.append(row.get("stack_width_qty", 0)).append("*")
			.append(row.get("stack_height_qty", 0));
	 		return blpTypeName.toString();
	}

	@Override
	public String getBlpTypeNameNeedPName(long box_type_id) throws Exception {
		try{
			DBRow row = floorBoxTypeZr.getBoxTypeDetailById(box_type_id);
			return getBlpTypeNameNeedPName(row);
		}catch (Exception e) {
			throw new SystemException(e, "getBlpTypeNameNeedPName", log);
		}
 	}
	
	//查询ilp根据商品
	public DBRow[] selectIlp(long productId)throws Exception{
		try{
			return this.floorBoxTypeZr.selectIlp(productId);
		}catch(Exception e){
			throw new SystemException(e,"selectIlp",log);
		}
	}
	//查询ilp类型
	public DBRow[] selectIlpType(long productId)throws Exception{
		try{
			return this.floorBoxTypeZr.selectIlpType(productId);
		}catch(Exception e){
			throw new SystemException(e,"selectIlpType",log);
		}
	}
	//查询ilp容器类型
	public DBRow[] selectContainerType()throws Exception{
		try{
			return this.floorBoxTypeZr.selectContainerType();
		}catch(Exception e){
			throw new SystemException(e,"selectContainerType",log);
		}
	}
	
	//添加ilp 类型
	public DBRow addBoxIlp(HttpServletRequest request)throws Exception{
		try{
			DBRow result = new DBRow();
			result.add("flag", "error");
			try {
				// 获取数据
				long box_pc_id = StringUtil.getLong(request, "box_pc_id");
				int box_total_length = StringUtil.getInt(request,"box_total_length");
				int box_total_width = StringUtil.getInt(request, "box_total_width");
				int box_total_height = StringUtil.getInt(request,"box_total_height");
				double box_length = StringUtil.getDouble(request, "box_length");
				double box_width = StringUtil.getDouble(request, "box_width");
				double box_height = StringUtil.getDouble(request, "box_height");
				double box_weight = StringUtil.getDouble(request, "box_weight");
				int is_has_sn = StringUtil.getInt(request, "is_has_sn");
				
				int container_type_id = StringUtil.getInt(request, "container_type_id");
				int length_uom = StringUtil.getInt(request, "length_uom");
				int weight_uom = StringUtil.getInt(request, "weight_uom");
				String lp_name = StringUtil.getString(request, "lp_name");
				String weight = StringUtil.getString(request, "weight");
				int box_total = box_total_length * box_total_width
						* box_total_height;

				if (box_total < 1) {
					result.add("flag", "dataFormat");
					return result;
				}		 
 					 
				if (box_pc_id == 0l) {
					result.add("flag", "productNotFound");
				} else {
					//验证此种ILP类型是否添加  基础类型，摆放方式，是否有SN
					int cn = floorBoxTypeZr.findIlpTypeByBasicLWW(box_pc_id, container_type_id, box_total_length, box_total_width, box_total_height);
					if(0 == cn)
					{
						DBRow data = new DBRow();
//						data.add("ibt_pc_id", box_pc_id);
//						data.add("ibt_total_length", box_total_length);
//						data.add("ibt_total_width", box_total_width);
//						data.add("ibt_total_height", box_total_height);
//						data.add("ibt_total", box_total);	
//						data.add("ibt_length", box_length);
//						data.add("ibt_width", box_width);
//						data.add("ibt_height", box_height);
//						data.add("ibt_weight", box_weight);
//						data.add("is_has_sn", is_has_sn);
//						data.add("ibt_lp_type", container_type_id);
//						data.add("length_uom", length_uom);
//						data.add("weight_uom", weight_uom);
						
						data.add("pc_id", box_pc_id);
						data.add("stack_length_qty", box_total_length);
						data.add("stack_width_qty", box_total_width);
						data.add("stack_height_qty", box_total_height);
						data.add("inner_total_pc", box_total);	
						data.add("ibt_length", box_length);
						data.add("ibt_width", box_width);
						data.add("ibt_height", box_height);
						data.add("ibt_weight", box_weight);
						data.add("is_has_sn", is_has_sn);	
						data.add("basic_type_id", container_type_id);
						data.add("length_uom", length_uom);
						data.add("weight_uom", weight_uom);
						data.add("container_type", 4);
						data.add("lp_name",lp_name);
						data.add("total_weight",weight);
						
						floorBoxTypeZr.addBoxIlp(data);
						result.add("flag", "success");
					}
					else
					{
						result.add("flag", "this_type_exist");
					}
				}
				return result;

			} catch (Exception e) {
				throw new SystemException(e, "addBoxIlp", log);
			}
		}catch(Exception e){
			throw new SystemException(e,"addBoxIlp",log);
		}
	}
	
	//根据id查询ilp
	public DBRow findIlpByIlpId(long ilpId)throws Exception{
		try{
			return this.floorBoxTypeZr.findIlpByIlpId(ilpId);
		}catch(Exception e){
			throw new SystemException(e,"findIlpByIlpId",log);
		}
	}
	
	public DBRow getCascadeClpType(long id) throws Exception {
		
		try{
			
			DBRow result = floorBoxTypeZr.findIlpByIlpId(id);
			
			if(result != null && result.get("inner_pc_or_lp", 0L) != 0){
				
				DBRow subResult = this.getCascadeClpType(result.get("inner_pc_or_lp", 0L));
				
				if(subResult != null){
					
					result.put("subPlateType", subResult);
				}
			}
			
			return result;
			
		}catch(Exception e){
			throw new SystemException(e,"getCascadeClpType(long id)",log);
		}
	}
	
	//ilp 类型查询
	public DBRow[] seachIlpContainerType(long productId,long ilp_container_type)throws Exception{
		try{
			return this.floorBoxTypeZr.seachIlpContainerType(productId, ilp_container_type);
		}catch(Exception e){
			throw new SystemException(e,"seachIlpContainerType",log);
		}
	}
	
	//更新ilp
	public DBRow updateIlpSkuType(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		result.add("flag", "error");
		try {
			// 获取数据
 			int box_total_length = StringUtil.getInt(request,"ibt_total_length");
			int box_total_width = StringUtil.getInt(request, "ibt_total_width");
			int box_total_height = StringUtil.getInt(request,"ibt_total_height");
			long type_id = StringUtil.getLong(request, "box_type_id");

			double box_length = StringUtil.getDouble(request, "ibt_length");
			double box_width = StringUtil.getDouble(request, "ibt_width");
			double box_height = StringUtil.getDouble(request, "ibt_height");
			double box_weight = StringUtil.getDouble(request, "ibt_weight");
			
			int container_type_id = StringUtil.getInt(request, "container_type_id");

			int box_total = box_total_length * box_total_width
					* box_total_height;

			if (box_total < 1) {
				result.add("flag", "dataFormat");
				return result;
			}
			 
			// 查询PCID。如果没有那么报错
				DBRow data = new DBRow();
				data.add("ibt_total_length", box_total_length);
				data.add("ibt_total_width", box_total_width);
				data.add("ibt_total_height", box_total_height);
 				data.add("ibt_total", box_total);
				data.add("ibt_length", box_length);
				data.add("ibt_width", box_width);
				data.add("ibt_height", box_height);
				data.add("ibt_weight", box_weight);
				data.add("ibt_lp_type", container_type_id);
				this.floorBoxTypeZr.updateIlpType(type_id, data);
				result.add("flag", "success");
		 
			return result;

		} catch (Exception e) {
			throw new SystemException(e, "addBoxTypeAdd", log);
		}
	}
	
	//删除ilp
	public void deleteIlpType(HttpServletRequest request) throws Exception {
		try {
			long ibt_id = StringUtil.getLong(request, "ibt_id");
			this.floorBoxTypeZr.deleteIlpType(ibt_id);
		} catch (Exception e) {
			throw new SystemException(e, "deleteBoxType", log);
		}
	}

	/**zyj
	 * 验证添加的ILP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findIlpTypeByBasicLWW(long pc_id,long basic_type, int len, int wid, int hei)throws Exception
	{
		try
		{
			return floorBoxTypeZr.findIlpTypeByBasicLWW(pc_id, basic_type, len, wid, hei);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findIlpTypeByBasicLWW", log);
		}
	}
	/**zyj
	 * 验证添加的BLP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findBlpTypeByBasicLWW(long pc_id,long basic_type,long inner_type, int len, int wid, int hei)throws Exception{
		try
		{
			return floorBoxTypeZr.findBlpTypeByBasicLWW(pc_id, basic_type,inner_type, len, wid, hei);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findBlpTypeByBasicLWW", log);
		}
	}
	
	
	/**zyj
	 * 查询所有ILP基础容器类型，再加上所有ILP基础容器类型这一项，用于页面选择
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectContainerTypeAddAllSel(int container_type)throws Exception
	{
		try
		{
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
			DBRow[] containerRow=floorBoxTypeZr.getBoxContainerType(container_type);
//			DBRow row = new DBRow();
//			row.add("type_id", "0");
//			row.add("type_name", "Packaging Type...");
//			row.add("length", 0.0f);
//			row.add("width", 0.0f);
//			row.add("height", 0.0f);
//			row.add("weight", 0.0f);
//			DBRow[] row0 = new DBRow[1];
//			row0[0] = row;
//			DBRow[] containerRows = new DBRow[containerRow.length + 1];
//			System.arraycopy(row0, 0, containerRows, 0, row0.length);
//			System.arraycopy(containerRow, 0, containerRows, row0.length, containerRow.length);
			return containerRow;
		}catch(Exception e){
			throw new SystemException(e,"selectContainerType",log);
		}
	}
	
	/**zyj
	 * 通过商品ID，查询BOX TYPE or  SHIP　TO
	 * @param pcid
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public DBRow getBoxTypesOrShiptoByPcId(long pcid, int limit) throws Exception {
		try{
			DBRow result = null;
			DBRow[] box_re = new DBRow[0];
			DBRow[] bps_re = new DBRow[0];
			int is_more = MoreLessOrEqualKey.EQUAL;
			String ship_to_name = "";
			//查询出box容器类型
			DBRow[] boxs = floorBoxTypeZr.getBoxTypesByPcIdAndContainerTypeId(pcid,0) ;
			//是否存在shipTo
			DBRow[] shipToCounts = boxSkuPsMgrZr.findShipToBoxCountByPcid(pcid, 0, 0);
			if(boxs.length > limit || shipToCounts.length > 1 || (boxs.length > 0 && shipToCounts.length > 0))
			{
				is_more = MoreLessOrEqualKey.MORE;
			}
			//如果存在非特供的box容器类型，最多显示limit个
			if(boxs.length > 0)
			{
				if(boxs.length > limit)
				{
					box_re = new DBRow[limit];
					System.arraycopy(boxs, 0, box_re, 0, limit);
				}
				else
				{
					box_re = new DBRow[boxs.length];
					System.arraycopy(boxs, 0, box_re, 0, boxs.length);
				}
			}
			//如果不存在非特供，显示特供box容器类型
			else
			{
				//是否存在去某个shipTo恰巧有limit个
				DBRow[] shipToCountEquals = boxSkuPsMgrZr.findShipToBoxCountByPcid(pcid, limit, MoreLessOrEqualKey.EQUAL);
				//存在，取一个
				if(shipToCountEquals.length > 0)
				{
					DBRow shipToCountEqual = shipToCountEquals[0];
					long ship_to_id = shipToCountEqual.get("blp_ship_to_id", 0L);
					DBRow[] boxPsIndex = boxSkuPsMgrZr.getAllBoxSkuByPcIdAndPsId(pcid,ship_to_id, 0);
					bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
					System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
					ship_to_name = shipToCountEqual.getString("ship_to_name");
					if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
					{
						is_more = MoreLessOrEqualKey.MORE;
					}
				}
				//不存在，取小于limit个的
				else
				{
					DBRow[] shipToCountLesses = boxSkuPsMgrZr.findShipToBoxCountByPcid(pcid, limit, MoreLessOrEqualKey.LESS);
					//存在小于limit个的，取一个
					if(shipToCountLesses.length > 0)
					{
						DBRow shipToCountLess = shipToCountLesses[0];
						long ship_to_id = shipToCountLess.get("blp_ship_to_id", 0L);
						DBRow[] boxPsIndex = boxSkuPsMgrZr.getAllBoxSkuByPcIdAndPsId(pcid,ship_to_id, 0);
						bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
						System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
						ship_to_name = shipToCountLess.getString("ship_to_name");
						if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
						{
							is_more = MoreLessOrEqualKey.MORE;
						}
					}
					//不存在小于limit个的，取大于的
					else
					{
						DBRow[] shipToCountMores = boxSkuPsMgrZr.findShipToBoxCountByPcid(pcid, limit, MoreLessOrEqualKey.MORE);
						if(shipToCountMores.length > 0)
						{
							DBRow shipToCountMore = shipToCountMores[0];
							long ship_to_id = shipToCountMore.get("blp_ship_to_id", 0L);
							DBRow[] boxPsIndex = boxSkuPsMgrZr.getAllBoxSkuByPcIdAndPsId(pcid,ship_to_id, 0);
							bps_re = new DBRow[boxPsIndex.length>limit?limit:boxPsIndex.length];
							System.arraycopy(boxPsIndex, 0, bps_re, 0, bps_re.length);
							ship_to_name = shipToCountMore.getString("ship_to_name");
							if(is_more != MoreLessOrEqualKey.MORE && boxPsIndex.length>5)
							{
								is_more = MoreLessOrEqualKey.MORE;
							}
						}
					}
				}
			}
			if(box_re.length > 0 || bps_re.length > 0)
			{
				result = new DBRow();
				result.add("ship_to_name", ship_to_name);
				result.add("box_types", box_re);
				result.add("box_ship_tos", bps_re);
				result.add("is_more", is_more);
//				//system.out.println(pcid+","+box_re.length+","+bps_re.length+","+is_more);
			}
			return result;
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypesOrShiptoByPcId", log);

 		}
 	}
	

	public void updateLock(long id, long lockId) throws Exception {
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("active", lockId );
		
			floorBoxTypeZr.updateLock(id,updateRow);
			
			//DBRow tocon=this.floorClpTypeMgrZr.findContainerProperty(sku_lp_box_type);
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("con_id", id);
			searchFor.put("type_id", id);
			
			
			Map<String,Object> toContainerNode = new HashMap<String, Object>();
			toContainerNode.put("con_id", id);
			toContainerNode.put("type_id", id);
			toContainerNode.put("active", lockId);
			
			productStoreMgr.updateNode(1,NodeType.Container,searchFor,toContainerNode);//修改容器节点锁定状态
			
		}catch (Exception e) {
			throw new SystemException(e, "updateBoxContainer", log);
		}
	}
	

	
	
	

	/**
	 * 通过商品ID和容器名称查询容器类型
	 * @param pc_id
	 * @param lpName
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月11日 下午6:41:26
	 */
	public DBRow findLpTypeByPcidLpTypeName(long pc_id, String lpName) throws Exception
	{
		try
		{
			return floorBoxTypeZr.findLpTypeByPcidLpTypeName(pc_id, lpName);
		}
		catch (Exception e) {
			throw new SystemException(e, "findLpTypeByPcidLpTypeName", log);

 		}
	}
	
	/**
	 * 查询商品的CLP信息
	 */
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id) throws Exception {
		try{
			return floorBoxTypeZr.getBoxTypesInfo(pc_id, container_type_id, title_id, ship_to_id);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id):", log);
 		}
	}
	
	/**
	 * 查找容器时添加active维度
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-23
	 */
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id, int active) throws Exception {
		try{
			return floorBoxTypeZr.getBoxTypesInfo(pc_id, container_type_id, title_id, ship_to_id, active);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id, int active):", log);
 		}
	}

	/**
	 * 查找容器时添加active维度
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-23
	 */
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id,int customer_id,int active) throws Exception {
		try{
			return floorBoxTypeZr.getBoxTypesInfo(pc_id, container_type_id, title_id, ship_to_id, customer_id,active);
		}catch (Exception e) {
			throw new SystemException(e, "getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id, int active):", log);
 		}
	}
	
	/**
	 * 根据title和shipTo查询clp的信息
	 */
	public DBRow[] getClpTypesByTitleAndShipTo(long pc_id, long title_id, long ship_to_id) throws Exception {
		try{
			return floorBoxTypeZr.getClpTypesByTitleAndShipToFloor(pc_id, title_id, ship_to_id);
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypesByTitleAndShipTo(long pc_id, long title_id, long ship_to_id):", log);
 		}
	}
	
	/**
	 * 添加active维度查询
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-23
	 */
	public DBRow[] getClpTypesByTitleAndShipTo(long pc_id, long title_id, long customer_id, long ship_to_id, long active) throws Exception {
		try{
			return floorBoxTypeZr.getClpTypesByTitleAndShipToFloor(pc_id, title_id, customer_id, ship_to_id, active);
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypesByTitleAndShipTo(long pc_id, long title_id, long ship_to_id, long active):", log);
 		}
	}
	
	public void setContainerMgrZr(ContainerMgrIfaceZr containerMgrZr) {
		this.containerMgrZr = containerMgrZr;
	}

	public void setFloorBoxTypeZr(FloorBoxTypeZr floorBoxTypeZr) {
		this.floorBoxTypeZr = floorBoxTypeZr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setBoxSkuPsMgrZr(BoxSkuPsMgrIfaceZr boxSkuPsMgrZr) {
		this.boxSkuPsMgrZr = boxSkuPsMgrZr;
	}
	
	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
