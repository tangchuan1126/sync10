package com.cwc.app.api.zr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.floor.api.zyj.FloorShortMessageMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.iface.zr.ScheduleReplayMgrIfaceZR;
import com.cwc.app.iface.zr.SysNotifyIfaceZr;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.jms.action.SendMail;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.WorkFlowTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.ScheduleTime;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.mongodb.util.Hash;
import com.sun.star.uno.RuntimeException;


public  class ScheduleMgrZr implements ScheduleMgrIfaceZR{

	static Logger log = Logger.getLogger("ACTION");
	
	
	public final static double ScheduleFinishState = 10.0d ;
	
	private FloorScheduleMgrZr floorScheduleMgrZr;
	private ShortMessageMgrZyjIFace shortMessageMgrZyj ;
	private SysNotifyIfaceZr sysNotifyMgrZr;
	private ScheduleReplayMgrIfaceZR scheduleReplayMgrZr;
	private AdminMgrIFace adminMgr ;
	public void setScheduleReplayMgrZr(ScheduleReplayMgrIfaceZR scheduleReplayMgrZr) {
		this.scheduleReplayMgrZr = scheduleReplayMgrZr;
	}



	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}



	public void setSysNotifyMgrZr(SysNotifyIfaceZr sysNotifyMgrZr) {
		this.sysNotifyMgrZr = sysNotifyMgrZr;
	}



	public void setFloorScheduleMgrZr(FloorScheduleMgrZr floorScheduleMgrZr) {
		this.floorScheduleMgrZr = floorScheduleMgrZr;
	}
	
	 
	
	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}


	//改掉如果是新加了提醒，在调用提醒的功能目前支持dwr,shortMessage,email
	//如果是重复任务也只提醒一条。没做到到了时间点然后在去提醒
	@Override
	public long addSchedule(DBRow row,HttpServletRequest  request) throws Exception {
		try{
 
			String schedule_join_execute_idStr = row.getString("schedule_join_execute_id");
			String execute_user_idStr = row.getString("execute_user_id");
			long adid = row.get("assign_user_id", 0l);
			row.add("create_time", DateUtil.NowStr());
			long id = floorScheduleMgrZr.addSchedule(row);
			row.add("schedule_join_execute_id", schedule_join_execute_idStr);
			row.add("execute_user_id", execute_user_idStr);
			
			row.add("associate_id", 0l);
			row.add("associate_type", 0);
			String emailTitle = row.getString("schedule_overview");
			// 读取join_user  和execute_user
			DBRow[] joinUserRows = floorScheduleMgrZr.getScheduleJoinInfo(id);
			DBRow[] executeUserRows = floorScheduleMgrZr.getScheduleSub(id);
			if(joinUserRows != null && joinUserRows.length > 0){
				StringBuffer sb = new StringBuffer("");
				for(int index = 0 , count = joinUserRows.length ; index < count ; index++ ){
					long userId = joinUserRows[index].get("schedule_join_execute_id", 0l);
					if(userId >0l){
						sb.append(",").append(userId);
					}
				}
				if(sb.length() > 1){
					row.add("schedule_join_execute_id", sb.toString().substring(1));
				}
			}
			if(executeUserRows != null && executeUserRows.length > 0){
				StringBuffer sb = new StringBuffer("");
				for(int index = 0 , count = executeUserRows.length ; index < count ; index++ ){
					long userId = executeUserRows[index].get("schedule_execute_id", 0l);
					if(userId >0l){
						sb.append(",").append(userId);
					}
				}
				if(sb.length() > 1){
					row.add("execute_user_id", sb.toString().substring(1));
				}
			}
			sysNotifyMgrZr.handleSheduleNotifify(row, emailTitle, adid);  //任务的分配人，就是当前操作人
			 
			return id;
		}catch(Exception e){
			throw new SystemException(e,"addSchedule",log);
		}
	}
	 

	@Override
	public void updateSchedule(long id,DBRow row,HttpServletRequest request) throws Exception {
		try{
			String deleteusers = row.getString("deleteusers");
			String addusers = row.getString("addusers");
			String del_join_user_id = row.getString("del_join_user_id");
			String add_join_user_id = row.getString("add_join_user_id");
			// 首先是
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 

			 String notChange = row.getString("notchange"); 
			 
			 floorScheduleMgrZr.updateSchedule(id, row);
			 DBRow shedule = floorScheduleMgrZr.getScheduleById(id);
			 if(notChange.length() > 0 ){
				 sysNotifyMgrZr.handleUpdateScheduleSendEmail(notChange, shedule, request);
			 }
			 
			 sysNotifyMgrZr.handleScheduleCancle(deleteusers+","+del_join_user_id, shedule,adminLoggerBean) ;
			 shedule.add("execute_user_id", addusers);
			 shedule.add("schedule_join_execute_id", add_join_user_id);
			 sysNotifyMgrZr.handleSheduleNotifify(shedule, shedule.getString("schedule_overview"), adminLoggerBean.getAdid());
		}catch(Exception e){
			throw new SystemException(e,"addSchedule",log);
		}
	}
	
	@Override
	public void updateSchedule(long id,DBRow row,AdminLoginBean adminLoggerBean) throws Exception {
		try{
			String deleteusers = row.getString("deleteusers");
			String addusers = row.getString("addusers");
			String del_join_user_id = row.getString("del_join_user_id");
			String add_join_user_id = row.getString("add_join_user_id");
			// 首先是
			long adid = adminLoggerBean.getAdid() ;
			 String notChange = row.getString("notchange"); 
			 
			 floorScheduleMgrZr.updateSchedule(id, row);
			 DBRow shedule = floorScheduleMgrZr.getScheduleById(id);
			 if(notChange.length() > 0 ){
				 sysNotifyMgrZr.handleUpdateScheduleSendEmail(notChange, shedule, adminLoggerBean);
			 }
			 
			 sysNotifyMgrZr.handleScheduleCancle(deleteusers+","+del_join_user_id, shedule,adminLoggerBean) ;
			 shedule.add("execute_user_id", addusers);
			 shedule.add("schedule_join_execute_id", add_join_user_id);
			 sysNotifyMgrZr.handleSheduleNotifify(shedule, shedule.getString("schedule_overview"), adid);
		}catch(Exception e){
			throw new SystemException(e,"addSchedule",log);
		}
	}
	/**
	 * 通过ID和任务修改任务
	 */
	public void udpateScheduleAndScheduleInfo(long id, DBRow row) throws Exception
	{
		try
		{
			floorScheduleMgrZr.updateSchedule(id, row);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"udpateScheduleAndScheduleInfo",log);
		}
	}

	@Override
	public DBRow[] getAllScheduleByExecuteUserIdAndTime(DBRow row)
			throws Exception {
		try{
			return  floorScheduleMgrZr.getScheduleByMonthAndExecuteUserId(row,false);
		}catch(Exception e){
			throw new SystemException(e,"getAllScheduleByExecuteIdAndTime",log);
		}
	}

	@Override
	public DBRow[] getScheduleByUnAssign(DBRow row) throws Exception {
		try{ 
			return  floorScheduleMgrZr.getUnAssignSchedule(row);
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getScheduleByUnAssign",log);
		}
	}

	@Override
	public long copySchedule(long id, DBRow row) throws Exception {
		try{
			DBRow result = floorScheduleMgrZr.getScheduleById(id);
			if(null != result){
				result.add("start_time", row.getString("start_time"));
				result.add("end_time", row.getString("end_time"));
				result.add("is_all_day",row.getString("is_all_day"));
				result.add("is_schedule", row.getString("is_schedule"));
				return floorScheduleMgrZr.addSchedule(result);
			}else{
				return 0l;
			}
			
		}catch (Exception e) {
			throw new SystemException(e,"copySchedule",log);
		}
	}

	@Override
	public DBRow[] getAllUserInfo() throws Exception {
		try{
			return floorScheduleMgrZr.getAllUserInfoAndDept();
		}catch (Exception e) {
			throw new SystemException(e,"copySchedule",log);
		}
	}

	@Override
	public DBRow getScheduleById(long id) throws Exception {
		try{
			DBRow result = floorScheduleMgrZr.getScheduleById(id);
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"getScheduleById",log);
		}
	}

	@Override
	public DBRow[] getScheduleAndUserInfoById(long id) throws Exception {
		try{
			DBRow[] result = floorScheduleMgrZr.getScheduleAndUserInfoById(id);
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"getScheduleById",log);
		}
	}

	@Override
	public DBRow[] getScheduleByDetailAndUserId(DBRow row) throws Exception {
		try{
			return floorScheduleMgrZr.getScheduleByExecuteUserIdAndDetail(row);
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getScheduleByDetailAndUserId",log);
		}
	}

	@Override
	public DBRow[] getManagerIdsByAdgid(long adgid) throws Exception {
		try{
			return floorScheduleMgrZr.getMangerByAdgid(adgid);
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getMangerByAdgid",log);
		}
	 
	}
	
	@Override
	public DBRow[] getMangerIds() throws Exception {
		try{
			return floorScheduleMgrZr.getManagerInfo();
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getMangerByAdgid",log);
		}
	}

	@Override
	public DBRow[] getScheduleJoinInfo(long schedule_id) throws Exception {
		try{
			return floorScheduleMgrZr.getScheduleJoinInfo(schedule_id);
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getScheduleJoinInfo",log);
		}
	}

	@Override
	public DBRow[] getScheduleJoinSchedule(DBRow row) throws Exception {
		try{
			return floorScheduleMgrZr.getScheduleByScheduleJoin(row,false);
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getScheduleJoinSchedule",log);
		}
	}

	@Override
	public DBRow getScheduleByAjax(DBRow row) throws Exception {
		 // 获取join表和Sub表当中的数据
		DBRow resultRow = new DBRow();
		try{
			// 获取有重复ID 并且重复任务的nerverStopFlag 为1的同时要查询出他的最大的时间点是哪里
			// 然后根据最大的时间点去计算是否要新生成repeat任务
			floorScheduleMgrZr.addRepeatScheduleByIsNerverStop(row);
			 DBRow[] joinRows = floorScheduleMgrZr.getScheduleByScheduleJoin(row,true);
			 DBRow[] subRows = floorScheduleMgrZr.getScheduleByMonthAndExecuteUserId(row,true);
			 
			 resultRow.add("join", joinRows);
			 resultRow.add("sub", subRows);
			 resultRow.add("flag","success");
			 return resultRow;
	 	}catch (Exception e) {
	 		throw new SystemException(e,"getScheduleByAjax",log);
		}
	 	
	}
	// 生成重复的任务。根据Row中的信息
	@Override
	public DBRow[] addScheduleRepeat(DBRow row) throws Exception {
		try{
			String schedule_id  = row.getString("schedule_id");
			long id = Long.parseLong(schedule_id);
			DBRow repeatRow = floorScheduleMgrZr.getRepeatScheduleById(id);
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(id);
			// 得到repeat的一些信息，然后根据这些信息去计算当前时间段是不是要添加repeat任务得到一些时间的触发点
			List<String> timeList = new ScheduleTime().getScheduleTime(repeatRow,scheduleRow,row);
			floorScheduleMgrZr.addRepeatSchedule(row, scheduleRow, timeList , Long.parseLong(repeatRow.getString("repeat_schedule_id")));
			// 查询新生成的任务就是带有重复Id的。和request中传递过来的时间段去查询刚刚生成的任务
			if("notshow".equals(row.getString("show"))){
				return null;
			}
			return floorScheduleMgrZr.getNewCreateRpeatScheduleByRow(row , Long.parseLong(repeatRow.getString("repeat_schedule_id")));
		}catch (Exception e) {
			 throw new SystemException(e,"addScheduleRepeat",log);
		}
	}

	@Override
	public DBRow getRepeatInfoById(long id) throws Exception {
		 try{
			return floorScheduleMgrZr.getRepeatInfoById(id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getRepeatInfoById",log);
		}
	}

	@Override
	public DBRow[] updateRepeatScheduleBy(long id, DBRow row) throws Exception {
		try{
			//先更新schedule表中的那条记录 也就是 is_schedule == 3 && repeat_id = long。
			long repeat_id = Long.parseLong(row.getString("repeat_schedule_id"));
			DBRow repeatRow = floorScheduleMgrZr.getRepeatInfoById(repeat_id);
			row.remove("repeat_schedule_id");
			long schedule_id = Long.parseLong(repeatRow.getString("schedule_id"));
			DBRow  comeFromRequest = new DBRow();
			comeFromRequest.add("end",row.getString("end"));
			//repeat_start_time 当前任务开始的时间
			String currentScheduleStartTime = row.getString("current_schedule_start");
			String currentScheduleEndTime = row.getString("current_schedule_end");
			repeatRow.add("repeat_start_time", currentScheduleStartTime);
			row.remove("end");
			row.remove("current_schedule_start");
			row.remove("current_schedule_end");
			row.add("is_schedule", "3");
			row.add("schedule_state","0");
			floorScheduleMgrZr.updateSchedule(schedule_id, row);
			// 先将scheduleRow的更新
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(schedule_id);
			// 然后删除任务current_schedule_start 以后的
			int count = floorScheduleMgrZr.deleteScheduleByRepeatIdAndStartTime(repeat_id, currentScheduleStartTime);
		 
			List<String> timeList =  new ScheduleTime().getScheduleTime(repeatRow, scheduleRow, comeFromRequest);
			List<String> sub = null;
			if(count >= 1 && !( repeatRow.getString("repeat_never_flag") != null && repeatRow.getString("repeat_never_flag").equals("1") )){
				 sub = timeList.subList(0, count - 1);
				 
			} else{
				sub = timeList;
			}
			sub.add(currentScheduleStartTime+","+currentScheduleEndTime);
			
			floorScheduleMgrZr.addRepeatSchedule(repeatRow, scheduleRow, sub, repeat_id);
			//根据时间去查询新添加的任务 返回到页面然后。页面删除以前repeatId的任务 && 时间before
			//添加新的repeatSchedule
			DBRow[] rows = floorScheduleMgrZr.getNewAddScheduleByUpdate(currentScheduleStartTime, repeat_id);
		 
			return rows;
		}catch (Exception e) {
			 throw new SystemException(e,"updateRepeatScheduleBy",log);
		}
		
	}
 
	@Override
	public DBRow[] getWalbill(DBRow queryRow,PageCtrl pc) throws Exception {
		 try{
				return floorScheduleMgrZr.getWalbillByRow(queryRow,pc);
			 }catch (Exception e) {
				 throw new SystemException(e,"getWalbill",log);
			}
	}

	@Override
	public long addNewStoreBill(DBRow row) throws Exception {
		try{
			
			return floorScheduleMgrZr.addNewStoreBill(row);
		 }catch (Exception e) {
			 throw new SystemException(e,"getWalbill",log);
		}
	}

	@Override
	public DBRow assignStore(DBRow row) throws Exception {
		/*
		 * 1. 首先是根据ps_id去查询当前有没有唯一的一个开放的出库单,如果有直接放在里面
		 * 2. 如果是根据ps_id去查询.如果没有直接创建一个然后放在里面
		 */
		DBRow result = new DBRow();
		long id = Long.parseLong(row.getString("ps_id"));
		DBRow[] rows = floorScheduleMgrZr.getStoresByPsIdAndState(id,"1");
		if(rows.length == 0 ){
			//创建一个新的然后添加信息
			DBRow storeRow = new DBRow();
			storeRow.add("ps_id", row.getString("ps_id"));
			long storeId = floorScheduleMgrZr.addNewStoreBill(storeRow);
			floorScheduleMgrZr.updateWayBillStore(storeId, row.getString("ids"));
			result.add("info","新创建一个出库单,然后加入了运单");
			result.add("flag", "success");
			result.add("state","addinnew");
			result.add("stroe", storeId +"");
		}
		if(rows.length > 1){
			result.add("info", "请你选择出库单");
			result.add("flag", "success");
			result.add("state", "selectstore");
		}
		if(rows.length == 1){
			//直接添加到唯一的里面
			long storeId = Long.parseLong(rows[0].getString("out_id"));
			floorScheduleMgrZr.updateWayBillStore(storeId, row.getString("ids"));
			result.add("info", "添加到唯一里面");
			result.add("flag", "success");
			result.add("state", "addinonlyone");
			result.add("stroe", storeId+"");
		}
		return result;
	}

	@Override
	public DBRow[] getAllStoreIsOpenAndPsId(long psId) throws Exception {
		try{
			
			return floorScheduleMgrZr.getAllStoreIsOpenAndPsId(psId);
		 }catch (Exception e) {
			 throw new SystemException(e,"getAllStoreIsOpenAndPsId",log);
		}
	}

	@Override
	public void updateWallBillByOutId(String ids, long outId) throws Exception {
		try{
			floorScheduleMgrZr.updateWayBillStore(outId,ids);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateWallBillByOutId",log);
		}
	}

	@Override
	public DBRow[] getAllOrderItemsInWayBillById(long wayBillid)
			throws Exception {
		try{
			return  floorScheduleMgrZr.getAllItemInWayBillById(wayBillid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getAllOrderItemsInWayBillById",log);
		}
	}

	@Override
	public DBRow splitWayBill(String ids, String numbers, long wayId)
			throws Exception {
		//要重新的计算运费等等
		DBRow result = new DBRow();
		try{
			//
			DBRow wayBill =  floorScheduleMgrZr.getWayBillById(wayId);
			if(wayBill != null){
				wayBill.remove("waybill_id");
				wayBill.add("parent_waybill_id", wayId);
				//添加一个运单
				long newAddWayId = floorScheduleMgrZr.addWayBillByDBRow(wayBill);
				// 更新WOI中的
				String[] idArray = ids.split(",");
				String[] numberArray = numbers.split(",");
				for(int index = 0 , count = idArray.length ; index < count ;index++ ){
					floorScheduleMgrZr.updateWayItemByWayId(idArray[index], numberArray[index], wayId, newAddWayId);
				}
				//读取新的数据和已经修改的数据
				//result = floorScheduleMgrZr.getWayItemsByWayId(wayId);
			}
			return  result;
		 }catch (Exception e) {
			 throw new SystemException(e,"splitWayBill",log);
		}
	}

	@Override
	public DBRow[] getSplitInfoByWayBillId(long wayBillid) throws Exception {
		try{
			return  floorScheduleMgrZr.getSplitInfoById(wayBillid);
		 }catch (Exception e) {
			 throw new SystemException(e,"getSplitInfoByWayBillId",log);
		}
	}

	@Override
	public void deleteSplitWayBillBy(long wayBillId ,long parentWayBillId) throws Exception {
		 try{
			 //1. 根据wayBillId 查询出来所有的的items然后把items的数据都更新到原来的WayBill中。也就是parent_id
			 //2. 最后要把Item中的数据删除
			 //3. 将wayBillId的运单的parent_waybill_id 值为-1
			 DBRow[] rows = floorScheduleMgrZr.getAllItemInWayBillById(wayBillId);
			 if(null != rows && rows.length > 0){
				 for(int index = 0 , count = rows.length ; index < count ; index++ ){
					 floorScheduleMgrZr.cencelWayBillItems(rows[index],parentWayBillId);
				 }
			 }
			 floorScheduleMgrZr.updateWayBillSetParentWayBillId(wayBillId);
		 }catch (Exception e) {
			 throw new SystemException(e,"deleteSplitWayBillBy",log);
		}
		
	}

	
	/**
	 * @param start_time 
	 * @param end_time
	 * @param assign_user_id
	 * @param execute_user_id
	 * @param schedule_join_execute_id
	 * @param associate_id
	 * @param associate_type
	 * @param emailTitle
	 * @param context
	 * @param isNeedDwr
	 * @param isNeedEmail
	 * @param isNeedShortMessage
	 * @param request
	 * @param isArangeNow
	 * @param associate_process
	 * @throws Exception
	 */
	public void addScheduleByExternalArrangeNow(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type, String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage, HttpServletRequest request ,boolean isArangeNow, int associate_process) throws Exception {
		try{
			
			innerAddSchedule(start_time, end_time, assign_user_id, execute_user_id, schedule_join_execute_id, 
					associate_id, associate_type, emailTitle, context, isNeedDwr, 
					isNeedEmail, isNeedShortMessage, assign_user_id, isArangeNow, associate_process,0l,1);
		}
		catch (Exception e) 
		{
			 throw new SystemException(e,"addScheduleByExternalArrangeNow",log);
		}
	} 
	
	
	/**
	 * @param start_time 
	 * @param end_time
	 * @param assign_user_id
	 * @param execute_user_id
	 * @param schedule_join_execute_id
	 * @param associate_id
	 * @param associate_type
	 * @param emailTitle
	 * @param context
	 * @param isNeedDwr
	 * @param isNeedEmail
	 * @param isNeedShortMessage
	 * @param request
	 * @param isArangeNow
	 * @param associate_process
	 * @throws Exception
	 */
	public void addScheduleByExternalArrangeNow(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type, String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage, AdminLoginBean adminLoggerBean ,boolean isArangeNow, int associate_process) throws Exception {
		try{
			long adid = adminLoggerBean.getAdid() ;
			innerAddSchedule(start_time, end_time, assign_user_id, execute_user_id, schedule_join_execute_id, 
					associate_id, associate_type, emailTitle, context, isNeedDwr, isNeedEmail, isNeedShortMessage, adid, isArangeNow, associate_process,0l,ScheduleModel.unAllDayFlag);
		}catch (Exception e) {
			 throw new SystemException(e,"addScheduleByExternalArrangeNow",log);
		}
	} 
	
	/**
	 * 因为里面有其他人添加的方法。现在都统一使用一个方法去添加记录
	 * @param start_time
	 * @param end_time
	 * @param assign_user_id
	 * @param execute_user_id
	 * @param schedule_join_execute_id
	 * @param associate_id
	 * @param associate_type
	 * @param emailTitle
	 * @param context
	 * @param isNeedDwr
	 * @param isNeedEmail
	 * @param isNeedShortMessage
	 * @param adid
	 * @param isArangeNow
	 * @param associate_process
	 * @throws Exception
	 * @author zhangrui
	 * @return 
	 * @Date   2014年12月4日
	 */
	private long innerAddSchedule(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type, String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage, long adid,boolean isArangeNow, int associate_process , long associate_main_id , int is_all_day) throws Exception{
		try{

			  DBRow scheduleRow = new DBRow();
			  if(start_time.trim().length() > 0){
				  scheduleRow.add("start_time",start_time );	
			  }//开始时间
			  if(end_time.trim().length() > 0){
				  scheduleRow.add("end_time",end_time );	
			  }//结束时间
 			  scheduleRow.add("assign_user_id",assign_user_id );					//任务安排人
			  scheduleRow.add("execute_user_id", execute_user_id);					// 任务执行人
			  scheduleRow.add("schedule_join_execute_id",schedule_join_execute_id); // 任务参与人
			  scheduleRow.add("schedule_overview", emailTitle);
			  scheduleRow.add("schedule_state","0");
			  scheduleRow.add("is_all_day", is_all_day);
			  scheduleRow.add("is_schedule",isArangeNow?"1":"0");
			  scheduleRow.add("schedule_detail",context);
			  scheduleRow.add("schedule_is_note",1);
			  scheduleRow.add("is_need_replay",1);
			  scheduleRow.add("is_update", 0);
			  scheduleRow.add("associate_type", associate_type);					//业务关联	ModuleKey 的取值
			  scheduleRow.add("associate_id", associate_id);	
			  scheduleRow.add("schedule_is_note", isNeedDwr?1:0);
			  scheduleRow.add("sms_email_notify", isNeedEmail?1:0);
			  scheduleRow.add("sms_short_notify", isNeedShortMessage?1:0);
			  scheduleRow.add("associate_process", associate_process);
			  scheduleRow.add("associate_main_id", associate_main_id);
			  scheduleRow.add("create_time", DateUtil.NowStr());
			  //具体的业务Id
			  //首先创建任务
			  long schedule_id = floorScheduleMgrZr.addSchedule(scheduleRow);
			  scheduleRow.add("execute_user_id", execute_user_id);					 
			  scheduleRow.add("schedule_join_execute_id",schedule_join_execute_id); 
			  
			  sysNotifyMgrZr.handleSheduleNotifify(scheduleRow, emailTitle,adid);
			  return schedule_id;
		}catch(Exception e){
			throw new SystemException();
		}
	}
	

	@Override
	public DBRow getScheduleByAssociate(long associate_id, int associate_type,
			int associate_process) throws Exception {
		try{
			return floorScheduleMgrZr.getScheduleByAssociate(associate_id, associate_type, associate_process);
		}catch (Exception e) {
			 throw new SystemException(e,"getScheduleByAssociate",log);
		}
		 
	}
	
	/**
	 * 通过关联Id,关联类型,获得所有的任务
	 */
	@Override
	public DBRow[] getScheduleByAssociateIdAndType(long associate_id , int[] associate_types) throws Exception
	{
		try 
		{
			return floorScheduleMgrZr.getScheduleByAssociateIdAndType(associate_id, associate_types);
		} 
		catch (Exception e)
		{
			 throw new SystemException(e,"getScheduleByAssociateIdAndType",log);
		}
	}

	@Override
	public DBRow[] getScheduleExecuteUsersInfoByAssociate(long associate_id,
			int associate_type, int associate_process) throws Exception {
		try{
			return floorScheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(associate_id, associate_type, associate_process);
		}catch (Exception e) {
			 throw new SystemException(e,"getScheduleExecuteUsersInfoByAssociate",log);
		}
	}



	@Override
	public void addScheduleReplayExternal(long associate_id,
			int associate_type, int associate_process, String context,boolean isCompelte ,HttpServletRequest  request , String configName)
			throws Exception {
		try{
			// 	1.首先查询出来当前的任务是不是已经安排了。如果是没有的话，那么就要取得当前的时间作为开始时间，结束时间为系统配置的时间
			//	  根据传入进来的configName去查询周期。然后结束时间就 = 当前时间 + 周期
			//	2.把任务is_schedule = 1 
			//
			//  3.发送一条跟进的日志
			TDate  tdate = new TDate();
			 
			DBRow schedule =  floorScheduleMgrZr.getScheduleByAssociate(associate_id, associate_type, associate_process);
			if(schedule == null) return ;
			if(schedule != null && !schedule.getString("is_schedule").equals("1")){
				DBRow updateScheduleRow = new DBRow();
				updateScheduleRow.add("start_time", tdate.getCurrentTime());
				
				SystemConfigIFace scf = (SystemConfigIFace)MvcUtil.getBeanFromContainer("proxySystemConfig");
				double 	t = Double.parseDouble(scf.getStringConfigValue(configName));
				long addTime =new Double(t * 3600 * 24 * 1000).longValue() ;
				long endTime = (new Date().getTime()+addTime);
				if(t < 1.0){
					updateScheduleRow.add("is_all_day", "0");
				}
				updateScheduleRow.add("end_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(endTime)));
				updateScheduleRow.add("is_schedule", 1);
				floorScheduleMgrZr.simpleUpdateSchedule(schedule.get("schedule_id", 0l), updateScheduleRow);
			}
			//然后在做日志跟进的处理
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			// 如果是isCompelte ture
			if(isCompelte){
				// 这个是时候是要求所有的都完成 主schedule上的完成 分支上的也是完成
				updateScheduleAndSubScheduleById(schedule.get("schedule_id", 0l));
			}
			DBRow row = new DBRow();
			row.add("sch_replay_context", context);
			row.add("schedule_id", schedule.get("schedule_id", 0l));
			row.add("sch_state", isCompelte?"10":"0");
			row.add("sch_replay_type", 1);
			row.add("employe_name",adminLoggerBean.getEmploye_name());
			row.add("adid",adminLoggerBean.getAdid() );
			scheduleReplayMgrZr.addScheduleReplay(row, request);
			
 		}catch (Exception e) {
			 throw new SystemException(e,"getScheduleExecuteUsersInfoByAssociate",log);
		}
		
	}
	
	@Override
	public void addScheduleReplayExternal(long associate_id,
			int associate_type, int associate_process, String context,boolean isCompelte ,AdminLoginBean adminLoggerBean , String configName, String is_need_notify_executer)
			throws Exception {
		try{
			// 	1.首先查询出来当前的任务是不是已经安排了。如果是没有的话，那么就要取得当前的时间作为开始时间，结束时间为系统配置的时间
			//	  根据传入进来的configName去查询周期。然后结束时间就 = 当前时间 + 周期
			//	2.把任务is_schedule = 1 
			//
			//  3.发送一条跟进的日志
			TDate  tdate = new TDate();
			 
			DBRow schedule =  floorScheduleMgrZr.getScheduleByAssociate(associate_id, associate_type, associate_process);
			if(schedule == null) return ;
			if(schedule != null && !schedule.getString("is_schedule").equals("1")){
				DBRow updateScheduleRow = new DBRow();
				updateScheduleRow.add("start_time", tdate.getCurrentTime());
				
				SystemConfigIFace scf = (SystemConfigIFace)MvcUtil.getBeanFromContainer("proxySystemConfig");
				double 	t = Double.parseDouble(scf.getStringConfigValue(configName));
				long addTime =new Double(t * 3600 * 24 * 1000).longValue() ;
				long endTime = (new Date().getTime()+addTime);
				if(t < 1.0){
					updateScheduleRow.add("is_all_day", "0");
				}
				updateScheduleRow.add("end_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(endTime)));
				updateScheduleRow.add("is_schedule", 1);
				floorScheduleMgrZr.simpleUpdateSchedule(schedule.get("schedule_id", 0l), updateScheduleRow);
			}
			//然后在做日志跟进的处理
			// 如果是isCompelte ture
			if(isCompelte){
				// 这个是时候是要求所有的都完成 主schedule上的完成 分支上的也是完成
				updateScheduleAndSubScheduleById(schedule.get("schedule_id", 0l));
			}
			DBRow row = new DBRow();
			row.add("sch_replay_context", context);
			row.add("schedule_id", schedule.get("schedule_id", 0l));
			row.add("sch_state", isCompelte?"10":"0");
			row.add("sch_replay_type", 1);
			row.add("employe_name",adminLoggerBean.getEmploye_name());
			row.add("adid",adminLoggerBean.getAdid() );
			scheduleReplayMgrZr.addScheduleReplay(row, adminLoggerBean, is_need_notify_executer);
			
 		}catch (Exception e) {
			 throw new SystemException(e,"getScheduleExecuteUsersInfoByAssociate",log);
		}
		
	}

	private void updateScheduleAndSubScheduleById(long schedule_id) throws Exception {
		try{
			//更新主单据上的总的进度10,表示的完成
			DBRow updateRow = new DBRow();
			updateRow.add("schedule_state", "10"); 
			floorScheduleMgrZr.simpleUpdateSchedule(schedule_id, updateRow);
			//更新sub_scchedule上进度10,表示的完成
			DBRow row = new DBRow();
			row.add("schedule_state", "10");
			floorScheduleMgrZr.updateScheduleSubByScheduleId(schedule_id, row);
			
		}catch (Exception e) {
			 throw new SystemException(e,"updateScheduleAndSubScheduleById",log);
		}
	}

	@Override
	public void updateScheduleExternal(long associate_id, int associate_type,
			int associate_process, String context,String userId, HttpServletRequest request,boolean isNeedEmail ,boolean isNeedShortMessage ,boolean isNeedDwr)
			throws Exception {
		 try{
			 //计算出来那些是计算出来那些是新添加的,删除,不变的
			 DBRow rowTemp = floorScheduleMgrZr.getScheduleByAssociate(associate_id, associate_type, associate_process);
		
			 long schedule_id = rowTemp.get("schedule_id", 0l);
			 String oldIds = floorScheduleMgrZr.getScheduleSubIds(schedule_id);
			 DBRow result = getDiffrentOfIdStrs(oldIds,userId);
			 rowTemp.add("schedule_is_note", isNeedDwr?"1":"0");
			 rowTemp.add("sms_short_notify", isNeedShortMessage?"1":"0");
			 rowTemp.add("sms_email_notify", isNeedEmail?"1":"0");
			 rowTemp.add("schedule_detail", context);
			 rowTemp.add("deleteusers", result.getString("deleteusers"));
			 rowTemp.add("addusers", result.getString("addusers"));
			 rowTemp.add("notchange", result.getString("notchange"));
			 this.updateSchedule(schedule_id, rowTemp, request);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateScheduleExternal",log);
		}
		
	}
	
	@Override
	public void updateScheduleExternal(long associate_id, int associate_type,
			int associate_process, String context,String userId, AdminLoginBean adminLoggerBean,boolean isNeedEmail ,boolean isNeedShortMessage ,boolean isNeedDwr)
			throws Exception {
		 try{
			 //计算出来那些是计算出来那些是新添加的,删除,不变的
			 DBRow rowTemp = floorScheduleMgrZr.getScheduleByAssociate(associate_id, associate_type, associate_process);
		
			 long schedule_id = rowTemp.get("schedule_id", 0l);
			 String oldIds = floorScheduleMgrZr.getScheduleSubIds(schedule_id);
			 DBRow result = getDiffrentOfIdStrs(oldIds,userId);
			 rowTemp.add("schedule_is_note", isNeedDwr?"1":"0");
			 rowTemp.add("sms_short_notify", isNeedShortMessage?"1":"0");
			 rowTemp.add("sms_email_notify", isNeedEmail?"1":"0");
			 rowTemp.add("schedule_detail", context);
			 rowTemp.add("deleteusers", result.getString("deleteusers"));
			 rowTemp.add("addusers", result.getString("addusers"));
			 rowTemp.add("notchange", result.getString("notchange"));
			 this.updateSchedule(schedule_id, rowTemp, adminLoggerBean);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateScheduleExternal",log);
		}
	}
	private DBRow getDiffrentOfIdStrs(String oldStr , String newStr){
		DBRow result = new DBRow();
		String[] oldArray = oldStr.split(",");
		String[] array = newStr.split(",");
		// 找出删除的。新添加的。不变的
		Set<String> set = new HashSet<String>();
		Set<String> set1 = new HashSet<String>();
		Set<String> set2 = new HashSet<String>();
		for(int index = 0 , count = array.length ; index < count ; index++ ){
			set.add(array[index]);
			set1.add(array[index]);
			
		}
		for(int index = 0, count = oldArray.length ;index < count ;index++ ){
			set.remove(oldArray[index]);
			set2.add(oldArray[index]);
		}
		result.add("addusers",  getValueFromSet(set));
	 
		Iterator<String> it = set.iterator();
		while(it.hasNext()){
			set1.remove(it.next());
		}
 
		result.add("notchange",  getValueFromSet(set1));
		Iterator<String> it2 = set1.iterator();
		while(it2.hasNext()){
 
		 	set2.remove(it2.next());  
	  
		}
		result.add("deleteusers",  getValueFromSet(set2));
 
		return result;
		
	}
	private String getValueFromSet(Set<String> set){
		StringBuffer result = new StringBuffer();
		if(set != null ){
			Iterator<String> it = set.iterator();
			while(it.hasNext()){
				result.append(",").append(it.next());
			}
		}
		return result.length() > 1 ? result.substring(1):"";
	}

	@Override
	public void deleteScheduleByScheduleId(long schedule_id,
			HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		deleteScheduleByScheduleId(schedule_id, adminLoggerBean);		
	}

	@Override
	public void deleteScheduleByScheduleId(long schedule_id,AdminLoginBean adminLoggerBean) throws Exception {
		try{
			// 查询出所有需要发送的人,然后根据原来的数据去判断是不是要发送取消的信息
			// 
			DBRow rowTemp = floorScheduleMgrZr.getScheduleById(schedule_id);
			
			String subIds = floorScheduleMgrZr.getScheduleSubIds(schedule_id);
			StringBuffer ids = new StringBuffer();
			ids.append(subIds);
			DBRow[] joins = floorScheduleMgrZr.getScheduleJoinInfo(schedule_id);
			if(joins != null && joins.length > 0 ){
				for(DBRow row : joins){
					ids.append(",").append(row.get("schedule_join_execute_id", 0l));
				}
			}
			sysNotifyMgrZr.handleScheduleCancle(ids.toString(), rowTemp, adminLoggerBean);
			floorScheduleMgrZr.deleteScheduleById(schedule_id);
		}catch (Exception e) {
			throw new SystemException(e,"deleteScheduleByScheduleId",log);
		}
	}



	@Override
	public DBRow[] deleteMutiRepeatScheduleById(long id, String flag)
			throws Exception {
		 try{
			return floorScheduleMgrZr.deleteMutiRepeatSchedule(id, flag);
		}catch(Exception e){
			throw new SystemException(e,"deleteMutiRepeatScheduleById",log);
		}
	}
	@Override
	public DBRow[] getScheduleNotComplete(long adid , int number) throws Exception
	{
		try
		{
			return  floorScheduleMgrZr.getScheduleNumberNotComplete(adid, number) ;
		}catch (Exception e) {
			throw new SystemException(e,"getScheduleNotComplete",log);
		}
	 
	}


	/**
	 * 订单差评任务
	 * 给客服和产品经理都创建这个多人的任务
	 */
	@Override
	public void addScheduleByWorkFlowOrder(HttpServletRequest request)
			throws Exception {
		 try{
			 String nowDate = DateUtil.DatetimetoStr(new Date()) ;
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 long adid = adminLoggerBean.getAdid(); 
			 long oid = StringUtil.getLong(request, "oid");
			 DBRow schedule = new DBRow();
			 schedule.add("is_task", 1);
			 schedule.add("task_type", WorkFlowTypeKey.ORDER_ASSESS);
			 schedule.add("assign_user_id", adid);
			 
			 schedule.add("start_time", nowDate);
			 schedule.add("end_time", nowDate);
			 schedule.add("schedule_state", 0);
			 schedule.add("schedule_is_note", 0);
			 schedule.add("is_need_replay", 0);
			 schedule.add("is_update", 0);
			 schedule.add("is_all_day", 1);
			 schedule.add("is_schedule", 1);
			 schedule.add("schedule_overview", "订单["+oid+"]任务");
			 schedule.add("repeat_id",0l);
			 schedule.add("associate_id", oid);
			 schedule.add("schedule_detail", "订单["+oid+"]任务");
			 DBRow[] userIds = floorScheduleMgrZr.getKeFuProductMangerUserIds();
			 StringBuffer ids = new StringBuffer("");
			 if(userIds != null){
				 for(DBRow  row : userIds){
					 ids.append(",").append(row.get("adid", 0l));
				 }
			 }
			 schedule.add("execute_user_id",ids.length() > 1 ? ids.substring(1):ids.toString());
			 floorScheduleMgrZr.addSchedule(schedule);
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"addScheduleByWorkFlowOrder",log);
		}
		
	}



	@Override
	public DBRow[] getScheduleWorkFlow(long adid , int finish_type) throws Exception {
		 try{
			 return floorScheduleMgrZr.getWorkFlowSchedule(adid,finish_type);
		 }catch (Exception e) {
			 throw new SystemException(e,"getScheduleWorkFlow",log);
		}
	}


	 /**
	 * 完成workFlow.
	 * 把sub_schedule标志成完成
	 * 重新计算任务的进度.
	 */
	 
	@Override
	public void finishWorkFlow(HttpServletRequest request) throws Exception {
		 try{
			 long schedule_id = StringUtil.getLong(request, "schedule_id");
			 long schedule_sub_id = StringUtil.getLong(request, "schedule_sub_id");
			 DBRow updateSub = new DBRow();
			 updateSub.add("schedule_state", 10);
			 updateSub.add("is_task_finish", 1);
			 
			 DBRow updateSchedule = new DBRow();			 	
			 //任务进度的重新计算,把结果保存
			 DBRow[] rows = floorScheduleMgrZr.getSubScheduleByScheduleId(schedule_id);
			 int sum = 0;
			 for(DBRow r : rows){
			    	String value = r.getString("schedule_state");
				    sum += Integer.parseInt((value != null && value.length() > 0) ? value :"0");
			 }
			 double d     =  sum /(double) rows.length ;
			 BigDecimal b  =  new BigDecimal(d);    
			 double  result =  b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 
			 updateSchedule.add("schedule_state", result);
	
			 floorScheduleMgrZr.updateScheduleSub(schedule_sub_id, updateSub);
			 floorScheduleMgrZr.simpleUpdateSchedule(schedule_id, updateSchedule);
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"finishWorkFlow",log);
		}
		
	}



	@Override
	public void addWorkFlowSchedule(String start_time, String end_time,
			long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type, String emailTitle, String context,
			boolean isNeedDwr, boolean isNeedEmail, boolean isNeedShortMessage,
			HttpServletRequest request, boolean isArrangeNow,
			int associate_process, int is_task, int task_type) throws Exception {
		 	try{
		 		  DBRow scheduleRow = new DBRow();
				  if(start_time.trim().length() > 0){
					  scheduleRow.add("start_time",start_time );	
				  }//开始时间
				  if(end_time.trim().length() > 0){
					  scheduleRow.add("end_time",end_time );	
				  }//结束时间
											
				  scheduleRow.add("assign_user_id",assign_user_id );					//任务安排人
				  scheduleRow.add("execute_user_id", execute_user_id);					// 任务执行人
				  scheduleRow.add("schedule_join_execute_id",schedule_join_execute_id); // 任务参与人
				  scheduleRow.add("schedule_overview", emailTitle);
				  scheduleRow.add("schedule_state","0");
				  scheduleRow.add("is_all_day", 1);
				  scheduleRow.add("is_schedule",isArrangeNow?"1":"0");
				  scheduleRow.add("schedule_detail",context);
				  scheduleRow.add("schedule_is_note",1);
				  
				  scheduleRow.add("is_need_replay",1);
				  scheduleRow.add("is_update", 0);
				  scheduleRow.add("associate_type", associate_type);					//业务关联	ModuleKey 的取值
				  scheduleRow.add("associate_id", associate_id);	
				  scheduleRow.add("schedule_is_note", isNeedDwr?1:0);
				  scheduleRow.add("sms_email_notify", isNeedEmail?1:0);
				  scheduleRow.add("sms_short_notify", isNeedShortMessage?1:0);
				  scheduleRow.add("associate_process", associate_process);
				  //具体的业务Id
				  //首先创建任务
				  scheduleRow.add("is_task", is_task);
				  scheduleRow.add("task_type", task_type);
				  floorScheduleMgrZr.addSchedule(scheduleRow);
				  scheduleRow.add("execute_user_id", execute_user_id);					 
				  scheduleRow.add("schedule_join_execute_id",schedule_join_execute_id); 
				 
				  sysNotifyMgrZr.handleSheduleNotifify(scheduleRow, emailTitle, assign_user_id);
		 		
		 	}catch (Exception e) {
		 		throw new SystemException(e,"addWorkFlowSchedule",log);
			}
		
	}



	@Override
	public void finishRefundSchedule(HttpServletRequest request)
			throws Exception {
		  try{
			long schedule_id = StringUtil.getLong(request, "schedule_id");
			long schedule_sub_id = StringUtil.getLong(request, "schedule_sub_id");
			floorScheduleMgrZr.updateSubScheduleFinish(schedule_id);
			DBRow updateSchedule = new DBRow();
			updateSchedule.add("schedule_state", "10");
			floorScheduleMgrZr.simpleUpdateSchedule(schedule_id, updateSchedule);
	 
		  }catch (Exception e) {
			throw new SystemException(e,"finishRefundSchedule",log);
		}
		
	}



	@Override
	public DBRow getTaskFlowCount(long adid) throws Exception {
		 try{
			 DBRow result = new DBRow();
			 DBRow notFinish = floorScheduleMgrZr.countWorkFlowSchedule(adid, 0);
			 result.add("not_finish_count",notFinish != null ? notFinish.get("sum_count", 0) : 0 );
			 
			 
			 DBRow finish = floorScheduleMgrZr.countWorkFlowSchedule(adid, 1);
			 result.add("finish_count",finish != null ? finish.get("sum_count", 0) : 0 );
			 return result;
		 }catch (Exception e) {
			 throw new SystemException(e,"getTaskFlowCount",log);
		}
	}



	@Override
	public String getAdminMangerUserIds() throws Exception {
		try{
			DBRow[] users = floorScheduleMgrZr.getMangerUserIds();
			StringBuffer userIds = new StringBuffer("");
			for(DBRow user  : users ){
				userIds.append(",").append(user.get("adid", 0l));
			}
			return userIds.length() > 1 ?  userIds.substring(1) : "" ;
		}catch (Exception e) {
			 throw new SystemException(e,"getAdminMangerUserIds",log);
		}
	}

	@Override
	public DBRow[] getAllNotFinishScheduleByAdid(long adid , int assign , long schedule_id ,  int pageSize) throws Exception {
		try{
			return floorScheduleMgrZr.getNotFinishSchedule(adid,assign,schedule_id,pageSize);
		}catch(Exception e){
			 throw new SystemException(e,"getAllScheduleByAdid",log);
		}
 	}
	@Override
	public int countNotFinishSchedule(long adid) throws Exception {
		try{
			return floorScheduleMgrZr.countNotFinishScheduleByAdid(adid);
		}catch(Exception e){
			 throw new SystemException(e,"countNotFinishSchedule",log);
		}
	}
	@Override
	public long addSchedule(ScheduleModel scheduleModel) throws Exception {
		
		try{
			 
			long id = innerAddSchedule(
					scheduleModel.getStartTime(), 
					scheduleModel.getEndTime(),
					scheduleModel.getAssignUserId(),
					scheduleModel.getExecuteUserId(),
					scheduleModel.getScheduleJoinExecuteId(),
					scheduleModel.getAssociateId(),
					scheduleModel.getAssociateType(),
					scheduleModel.getEmailTitle(), 
					scheduleModel.getContext(), 
					scheduleModel.isNeedDwr(),
					scheduleModel.isNeedEmail(), 
					scheduleModel.isNeedShortMessage(),
					scheduleModel.getAssignUserId(), 
					scheduleModel.isArrangeNow(), 
					scheduleModel.getAssociateProcess(),	
					scheduleModel.getAssociateMainId(),
					scheduleModel.getIsAllDay());
			return id;
 			
		}catch(Exception e){
			 throw new SystemException(e,"addSchedule",log);
		}
	}
	
	/**
	 * @param adid
	 * @param schedule_id
	 * @param queryType 1:向某个Id以后的查找,2 向某个ID前面查找
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	@Override
	public DBRow[] queryScheduleUnFinishAndNoAssociate(DBRow loginRow) throws Exception {
		try{
			long adid = loginRow.get("adid", 0l);
			long ps_id = loginRow.get("ps_id", 0l);
			DBRow[] schedules = floorScheduleMgrZr.queryScheduleUnFinishAndNoAssociate(adid);
			DBRow[] returnArray = fixAndroidScheduleData(schedules,ps_id);
			return returnArray ;
		}catch(Exception e){
			 throw new SystemException(e,"queryTask",log);
		}
 	}
	
	private DBRow[] fixAndroidScheduleData(DBRow[] schedules ,long ps_id) throws Exception{
		if(schedules != null){
			DBRow[] returnArrays = new DBRow[schedules.length];
			for(int index = 0 , count = schedules.length ; index < count ; index++ ){
				DBRow row = schedules[index];
				DBRow temp = new DBRow();
 				temp.add("title", row.getString("schedule_overview"));
				temp.add("header","Schedule ["+row.get("schedule_id", 0l)+"]");
				temp.add("schedule_detail", row.getString("schedule_detail"));
				temp.add("assign_user_id", row.get("assign_user_id",0l));
				temp.add("assign_user_name", row.getString("employe_name"));
				temp.add("create_time",DateUtil.showLocalparseDateToNoYear24Hours(row.getString("create_time"), ps_id));
  				returnArrays[index] = temp ;
 			}
			return returnArrays ;
		}
		return null ;
	}
	public static void main(String[] args) {
		
		
		ScheduleMgrZr scheduleMgrZr = new ScheduleMgrZr();
		System.out.println(scheduleMgrZr.getScheduleTaskTypeValue(ProcessKey.SPECIAL_TASK ));
	}
	private String getScheduleTaskTypeValue(int associate_process){
  		String returnValue = "General Tasks";
  		switch (associate_process) {
		case ProcessKey.CHECK_IN_WINDOW:
			returnValue = "Assign Tasks";
			break;
		case ProcessKey.CHECK_IN_WAREHOUSE:
			returnValue = "Loading/Receiving";
			break;
		case ProcessKey.FinanceApplyPayment :
			returnValue = "Finance Apply";
			break ;
		case ProcessKey.GateHasTaskNotifyWindow :
			returnValue = "Window Task";
			break ;
		case ProcessKey.INVENTORYCYCLECOUNTTASK :
 		case ProcessKey.INVENTORYFINDTASK :
 		case ProcessKey.INVENTORYVERIFICATION :
			returnValue = "Inventory Task";
			break ;
 
		case ProcessKey.CongfigChangeTask:		//assign supervisor
//			returnValue = "CC Task";
			returnValue = "Pre CC&Scan";
			break;
		case ProcessKey.ScanTask:		//assign scan
//			returnValue = "Scan Task";
			returnValue = "CC&Scan Task";
			break;
		case ProcessKey.MovementTask: 
			returnValue = "Movement Task";
			break;
	   case ProcessKey.SPECIAL_TASK:		//Speical Task
			returnValue = "Special Tasks";
			break;
  		}
  			
  		return returnValue ;
  	}
  	 
	/**
	 * 1.添加处理因为Gate 发送给WarehouseMangaer 添加了一个Key值 
	 * 
	 */
	private DBRow[]  fixScheduleType(DBRow[] schedules){
		List<DBRow> returnRow = new ArrayList<DBRow>(); 
		if(schedules != null && schedules.length > 0){
			for(DBRow temp : schedules){
				if(temp.get("associate_process", 0) == ProcessKey.GateNotifyWareHouse){
					temp.add("associate_process", ProcessKey.CHECK_IN_WINDOW);
				}
			}
 			//{ASSOCIATE_PROCESS=52, TOTAL=1}
			Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
			for(DBRow temp : schedules){
			
				Integer associate_process = temp.get("ASSOCIATE_PROCESS", 0);
				Integer total =  temp.get("TOTAL", 0);
				Integer mapTotal = hashMap.get(associate_process) ;
				if(mapTotal == null){
					mapTotal = 0 ;
				}
				hashMap.put(associate_process, total + mapTotal);
			}
 			if(hashMap.size() > 0){
 				Iterator<Entry<Integer, Integer>> it =  hashMap.entrySet().iterator();
 				while(it.hasNext()){
	 				DBRow temp = new DBRow();
	 				Entry<Integer, Integer> entry =  it.next() ;
	 				temp.add("ASSOCIATE_PROCESS", entry.getKey());
	 				temp.add("TOTAL", entry.getValue());
	 				returnRow.add(temp);
 				}
 			}
		}
			return returnRow.toArray(new DBRow[0]);
	}
  	@Override
  	public DBRow[] getNotFinishScheduleType(long adid) throws Exception {
  		try{
  			DBRow[] schedules =	floorScheduleMgrZr.getNotFinishScheduleTypeBy(adid);
  		 	schedules = fixScheduleType(schedules);
  			 
  			if(schedules != null){
  				for(DBRow temp :  schedules){
  					int associate_process = temp.get("associate_process", 0);
  					temp.add("title", getScheduleTaskTypeValue(associate_process));
  				}
  				
  				Collections.sort(Arrays.asList(schedules) , new Comparator<DBRow>() {
					@Override
					public int compare(DBRow o1, DBRow o2) {
 						return o1.get("associate_process", 0) > o1.get("associate_process", 0) ? 1 : -1 ;
					}
				});
  			}
  			
  		
  			return schedules ;
  		}catch(Exception e){
  			throw new SystemException(e,"getNotFinishScheduleType",log);
  		}
   	}
  	@Override
  	public void simpleUdateSchedule(long schedule_id, DBRow updateRow) throws Exception {
   		try{
   			floorScheduleMgrZr.simpleUpdateSchedule(schedule_id, updateRow);
   		}catch(Exception e){
  			throw new SystemException(e,"updateSchedule",log);
   		}
  	}
  	@Override
  	public void simpleUpdateScheduleSub(long schedule_id, DBRow updateRow)
  			throws Exception {
  		try{
  			floorScheduleMgrZr.updateScheduleSubByScheduleId(schedule_id, updateRow);
  		}catch(Exception e){
  			throw new SystemException(e,"updateScheduleSub",log);
  		}
  	}
  	/**
  	 * 其他业务模块获取 schedule的方法
  	 */
  	public DBRow[] getScheduleBy(long associate_id, int associate_process, int associate_type) throws Exception {
  		try{
  			return floorScheduleMgrZr.getScheduleBy(associate_id, associate_process, associate_type);
  		}catch(Exception e){
  			throw new SystemException(e,"getScheduleBy",log);
  		}
  	};
  /*	@Override
  	public DBRow[] getScheduleExecuteUsers(long associate_id,
  			int associate_process, int associate_type) throws Exception {
  		try{
  			return floorScheduleMgrZr.getExcuteUserInfos(associate_id,  associate_process,  associate_type);
  		}catch(Exception e){
  			throw new SystemException(e,"getScheduleExecuteUsers",log);
  		}
   	}*/
  	
  	
  	@Override
  	public DBRow newSchedule(long adid) throws Exception {
  		try{
  			return floorScheduleMgrZr.getLastSchedule(adid);
  		}catch(Exception e){
  			throw new SystemException(e,"newSchedule",log);
  		}
   	}
  	@Override
  	public void simpleUpdateScheduleBy(long associate_id, int associate_process,
  			int associate_type , DBRow updateRow) throws Exception {
  		try{
  			floorScheduleMgrZr.simpleUpdateScheduleBy(  associate_id,   associate_process,   associate_type ,  updateRow);
  		}catch(Exception e){
  			throw new SystemException(e,"simpleUpdateSchedule",log);
  		}
  	}
  	@Override
  	public DBRow[] getCheckInModelDetailNotFinishSchedule(long detail_id , long entry_id)
  			throws Exception {
  		try{
  			return floorScheduleMgrZr.getCheckInModelDetailNotFinishSchedule(detail_id,entry_id);
  		}catch(Exception e){
  			throw new SystemException(e,"getCheckInModelDetailNotFinishSchedule",log);
  		}
   	}
  	
  	/**
	 * 通过module和processkeys查询schedule数量
	 * @param module
	 * @param module_id
	 * @param processKeys
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午6:30:49
	 */
	public int findScheduleAssociateCountByAssociateMainIdType(int module, long module_id, int[] processKeys) throws Exception
	{
		return floorScheduleMgrZr.findScheduleAssociateCountByAssociateMainIdType(module, module_id, processKeys);
				
	}
	/* (非 Javadoc)
	* <p>Title: finishScheduleByAssociateParams</p>
	* <p>Description: </p>
	* @param associate_type
	* @param associate_process
	* @param associate_id
	* @param adid
	* @return
	* @throws Exception
	* @see com.cwc.app.iface.zr.ScheduleMgrIfaceZR#finishScheduleByAssociateParams(int, int, int, long)
	*/ 
	@Override
	public void finishScheduleByAssociateParams(int associate_type,
			int associate_process, long associate_id, long adid)
			throws Exception {
		try{
 
			DBRow[] scheduls =  getScheduleBy(associate_id, associate_process, associate_type);
			if(scheduls != null && scheduls.length > 0){
				DBRow updateRow = new DBRow();
				updateRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
				updateRow.add("end_time", DateUtil.NowStr());
				DBRow updateSubRow = new DBRow();
				updateSubRow.add("is_task_finish", 1);
		 		updateSubRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish); // 谁完成的不知道如果执行人 和 完成不是一样的情况
				//任务完成的时候写入 是谁给完成的
				updateSubRow.add("schedule_finish_adid", adid);
				for(DBRow e : scheduls){
					long schedule_id = e.get("schedule_id", 0l);
					simpleUdateSchedule(schedule_id, updateRow);
					simpleUpdateScheduleSub(schedule_id, updateSubRow);
				}
			}
		}catch(Exception e){
  			throw new SystemException(e,"finishScheduleByAssociateParams",log);
		}
 	 
	}
}
	