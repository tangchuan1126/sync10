package com.cwc.app.api.zr;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.iface.zj.ShipToMgrIFaceZJ;
import com.cwc.app.iface.zr.BoxTypeMgrIfaceZr;
import com.cwc.app.iface.zr.ClpTypeMgrIfaceZr;
import com.cwc.app.iface.zr.ContainerMgrIfaceZr;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ClpTypeMgrZr implements ClpTypeMgrIfaceZr{

	static Logger log = Logger.getLogger("ACTION");
	
	private BoxTypeMgrIfaceZr boxTypeMgrZr; 
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	private	FloorProductStoreMgr productStoreMgr;	
	private ContainerMgrIfaceZr containerMgrZr ;
    private ShipToMgrIFaceZJ shipToMgrZJ ;
    private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
   
	
	
	/**
	 * 对于没有盒子类型的就是商品本身 
	 * 		sku_lp_total_box = l * w * g ;
	 * 		sku_lp_total_piece = l * w * g ;
	 * 如果是有盒子的类型那么
	 * 		sku_lp_total_box = l * w * g ;
	 * 		sku_lp_total_piece l * w *g * 盒子上面商品的数量
	 */
	@Override
	public DBRow addClpType(HttpServletRequest request) throws Exception {
		DBRow result  = new DBRow();
		try{

			
			DBRow row = new DBRow();
			long sku_lp_pc_id = StringUtil.getLong(request, "sku_lp_pc_id");
			long sku_lp_title_id = StringUtil.getLong(request, "sku_lp_title_id");
			long sku_lp_to_ps_id = StringUtil.getLong(request, "sku_lp_to_ps_id");
			long lp_type_id = StringUtil.getLong(request, "lp_type_id");
 			long sku_lp_box_type = StringUtil.getLong(request, "sku_lp_box_type");	//盒子类型
			
			int sku_lp_total_box = 0;
			int sku_lp_total_piece = 0 ;
			
			int sku_lp_box_length = StringUtil.getInt(request, "sku_lp_box_length");
			int sku_lp_box_width = StringUtil.getInt(request, "sku_lp_box_width");
			int sku_lp_box_height = StringUtil.getInt(request, "sku_lp_box_height");
			int is_has_sn = StringUtil.getInt(request, "is_has_sn");
			String length_uom = StringUtil.getString(request, "length_uom");
			String weight_uom = StringUtil.getString(request, "weight_uom");
			String lp_name = StringUtil.getString(request, "lp_name");
			double box_length = StringUtil.getDouble(request, "box_length");
			double box_width = StringUtil.getDouble(request, "box_width");
			double box_height = StringUtil.getDouble(request, "box_height");
			double box_weight = StringUtil.getDouble(request, "box_weight");
			double weight = StringUtil.getDouble(request, "weight");
			int level = 0;
			
			
			if(sku_lp_box_type  == 0l){
				sku_lp_total_box = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ;//商品的数量
				sku_lp_total_piece = sku_lp_total_box;	//商品的数量
				level = 1;
			}else{
				DBRow boxSkuType = boxTypeMgrZr.getBoxTypeBuId(sku_lp_box_type);
				if(boxSkuType.get("active", 0) != 1 ){
					result.add("flag", "error");
					result.add("message", "InnerType_Error");
					return result;
				}else{
					int totalBoxSkuType = boxSkuType.get("inner_total_pc", 0);
					sku_lp_total_box  = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ; //盒子的数量
					sku_lp_total_piece = sku_lp_total_box * totalBoxSkuType ; //商品的数量
					
					if(boxSkuType.get("pc_id", 0l) != sku_lp_pc_id){
						//盒子上面的商品 和 页面传递过来的pc_id 不等.W
						result.add("flag", "error");
						result.add("message", "skuNotMatch");
						return result;
					}
					level = boxSkuType.get("level", 0)+1;					
				}
			}
			
			if(sku_lp_total_box == 0){
				result.add("flag", "error");
				result.add("message", "skuLpTotalBoxLetter");
				return result;
			}
			if(sku_lp_total_piece == 0 ){
				result.add("flag", "error");
				result.add("message", "skuLpTotalPieceLetter");
				return result;
			}
			
			int cn = floorClpTypeMgrZr.findClpTypeByBasicLWW(sku_lp_pc_id, lp_type_id,sku_lp_box_type, sku_lp_box_length, sku_lp_box_width, sku_lp_box_height);
			//验证clp type重复
			if(cn == 0){
			/*set DBRow*/
			DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id,0);
			int sort=sortRow.get("sort", 0);
			row.add("sort",sort+1);
			
			row.add("pc_id", sku_lp_pc_id);
		//	row.add("lp_title_id", sku_lp_title_id);
			row.add("basic_type_id", lp_type_id);
			row.add("inner_pc_or_lp", sku_lp_box_type);		
			row.add("stack_length_qty", sku_lp_box_length);
			row.add("stack_width_qty", sku_lp_box_width);
			row.add("stack_height_qty", sku_lp_box_height);
			row.add("is_has_sn", is_has_sn);
			row.add("inner_total_lp", sku_lp_total_box);
			row.add("inner_total_pc", sku_lp_total_piece);
			row.add("length_uom", length_uom);
			row.add("weight_uom", weight_uom);
			row.add("container_type", 1);
			row.add("lp_name",lp_name);
			row.add("active",1);
			row.add("ibt_length",box_length);
			row.add("ibt_width",box_width);
			row.add("ibt_height",box_height);
			row.add("ibt_weight",box_weight);
			row.add("total_weight",weight);
			row.add("level", level);
			long id = addClpTypeDBRow(row);

			//图数据库
		//	DBRow con=this.floorClpTypeMgrZr.findContainerProperty(lp_type_id);
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",id);
			containerNode.put("type_id",id);			
			containerNode.put("pc_id",sku_lp_pc_id);
			containerNode.put("sort",sort+1);
			containerNode.put("basic_type_id",lp_type_id);
			containerNode.put("inner_pc_or_lp",sku_lp_box_type);
			containerNode.put("stack_length_qty", sku_lp_box_length);
			containerNode.put("stack_width_qty", sku_lp_box_width);
			containerNode.put("stack_height_qty", sku_lp_box_height);			
			containerNode.put("is_has_sn",is_has_sn);
			containerNode.put("inner_total_lp",sku_lp_total_box);
			containerNode.put("inner_total_pc",sku_lp_total_piece);
			containerNode.put("length_uom",length_uom);
			containerNode.put("weight_uom",weight_uom);		
			containerNode.put("container_type",1);
			containerNode.put("lp_name",lp_name);
			containerNode.put("active",1);
			containerNode.put("ibt_length",box_length);
			containerNode.put("ibt_width",box_width);
			containerNode.put("ibt_height",box_height);
			containerNode.put("ibt_weight",box_weight);
			containerNode.put("total_weight",weight); 
			containerNode.put("level", level);

		//	con_id, container, container_type, is_full, is_has_sn, type_id
			// clp type里面直接放的商品
			if(sku_lp_box_type  == 0l){
				DBRow[] containerProduct=this.floorClpTypeMgrZr.findProductNode(sku_lp_pc_id);
				DBRow containerProductLine=this.floorClpTypeMgrZr.findProductLineNode(sku_lp_pc_id);
				 DBRow[] hasTitle = proprietaryMgrZyj.getProductHasTitleList(sku_lp_pc_id);
				 
				for(int i = 0; i < containerProduct.length; i++){
					
					Map<String,Object> productNode = new HashMap<String, Object>();
					productNode.put("pc_id",sku_lp_pc_id);
					
					String title_id = "";
					
					for(int j = 0; j<hasTitle.length; j++){
						
						title_id += ","+hasTitle[j].get("title_id",0l);
					}
					
					productNode.put("title_id",title_id.equals("")?title_id:title_id.substring(1));
				//	productNode.put("product_line",containerProductLine.get("product_line_id",0l));
					productNode.put("p_name",containerProduct[i].getString("p_name"));
				//	productNode.put("catalogs",containerProduct[i].get("catalog_id",0l));
					
					productNode.put("unit_name",containerProduct[i].getString("unit_name"));
					productNode.put("unit_price",containerProduct[i].getString("unit_price"));
					productNode.put("catalog_id",containerProduct[i].getString("catalog_id"));
					productNode.put("orignal_pc_id", containerProduct[i].get("orignal_pc_id",0l));
					productNode.put("width",containerProduct[i].getString("width"));
					productNode.put("weight",containerProduct[i].getString("weight"));
					productNode.put("heigth", containerProduct[i].getString("heigth"));
					productNode.put("length", containerProduct[i].getString("length"));
					productNode.put("length_nom",containerProduct[i].get("length_uom",0l));
					productNode.put("weight_nom",containerProduct[i].get("weight_uom",0l));
					productNode.put("price_uom",containerProduct[i].getString("price_uom"));
					//productNode.put("p_code", containerProduct[i].getString("p_code"));
					productNode.put("union_flag",containerProduct[i].get("union_flag",0));
					productNode.put("alive",containerProduct[i].get("alive",0));
					
					Map<String,Object> relProps = new HashMap<String, Object>();
					relProps.put("quantity",sku_lp_total_piece);
				//	relProps.put("locked_quantity",sku_lp_total_piece);
					relProps.put("stack_length_qty",sku_lp_box_length);
					relProps.put("stack_width_qty",sku_lp_box_width);
					relProps.put("stack_height_qty",sku_lp_box_height);
					relProps.put("total_weight",weight);
					relProps.put("weight_nom",weight_uom);
					
					
					Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
					searchContainerNodes.put("con_id", id);
					Map<String,Object> searchProductNodes = new HashMap<String, Object>();
					searchProductNodes.put("pc_id", sku_lp_pc_id);
					DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
					DBRow[]  existProductNode =	productStoreMgr.searchNodes(1, NodeType.Product,searchProductNodes);
					
					if(existContainerNode.length>0){
						productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加商品节点
					}else{
						productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
					}
					if(existProductNode.length>0){
						productStoreMgr.updateNode(1,NodeType.Product,searchProductNodes,productNode);//添加商品节点
					}else{
						productStoreMgr.addNode(1,NodeType.Product,productNode);//添加商品节点
					}
					
				
				
					
					productStoreMgr.addRelation(1,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);//添加容易与商品的关系
					
					
				}
			}else{
			/*	Map<String,Object> fromContainerNode = new HashMap<String, Object>();
				fromContainerNode.put("con_id",lp_type_id);
				
				Map<String,Object> toContainerNode = new HashMap<String, Object>();
				toContainerNode.put("con_id",sku_lp_total_box);
				*/
				
				
				DBRow tocon=this.floorClpTypeMgrZr.findContainerProperty(sku_lp_box_type);

				
				Map<String,Object> toContainerNode = new HashMap<String, Object>();
				toContainerNode.put("con_id",sku_lp_box_type);
				toContainerNode.put("type_id",sku_lp_box_type);	
				
			    toContainerNode.put("pc_id",tocon.get("pc_id",01));			  
		        //toContainerNode.put("sort",tocon.get("sort",01));
			    toContainerNode.put("basic_type_id",tocon.get("basic_type_id",01));
				toContainerNode.put("inner_pc_or_lp",tocon.get("inner_pc_or_lp",01));
				toContainerNode.put("is_has_sn",tocon.get("is_has_sn",01));
				toContainerNode.put("inner_total_lp",tocon.get("inner_total_lp",01));
				toContainerNode.put("inner_total_pc",tocon.get("inner_total_pc",01));
				//toContainerNode.put("length_uom",tocon.get("length_uom",01));
				//toContainerNode.put("weight_uom",tocon.get("weight_uom",01));		
				toContainerNode.put("container_type",tocon.get("container_type",01));
			    toContainerNode.put("lp_name",tocon.getString("lp_name"));
				toContainerNode.put("active",tocon.get("active",01));
				toContainerNode.put("ibt_length",tocon.get("ibt_length",01));
				toContainerNode.put("ibt_width",tocon.get("ibt_width",01));
				toContainerNode.put("ibt_height",tocon.get("ibt_height",01));
				//	toContainerNode.put("ibt_weight",tocon.get("ibt_weight",01));
				//toContainerNode.put("total_weight",tocon.get("total_weight",01));
				toContainerNode.put("stack_length_qty", tocon.get("stack_length_qty",01));
				toContainerNode.put("stack_width_qty", tocon.get("stack_width_qty",01));
				toContainerNode.put("stack_height_qty", tocon.get("stack_height_qty",01)); 
				
				
				//
				Map<String,Object> ContainerNodeConcern = new HashMap<String, Object>();
				ContainerNodeConcern.put("quantity", sku_lp_total_piece);
				ContainerNodeConcern.put("stack_length_qty", sku_lp_box_length);
				ContainerNodeConcern.put("stack_width_qty", sku_lp_box_width);
				ContainerNodeConcern.put("stack_height_qty", sku_lp_box_height);
				ContainerNodeConcern.put("total_weight", weight);
				
				//parent clp type
				Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
				searchContainerNodes.put("con_id", id);
				
				//inner clp type
				Map<String,Object> searchToContainerNodes = new HashMap<String, Object>();
				searchToContainerNodes.put("con_id", sku_lp_box_type);
				
				DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
				DBRow[]  existToContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchToContainerNodes);

				if(existContainerNode.length>0){
					productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加父节点
				}else{
					productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加父容器节点
				}
				
				if(existToContainerNode.length>0){
					productStoreMgr.updateNode(1,NodeType.Container,searchToContainerNodes,toContainerNode);//添加子节点
				}else{
					productStoreMgr.addNode(1,NodeType.Container,toContainerNode);//添加子容器节点
				}
				
				//父-->子
				productStoreMgr.addRelation(1,NodeType.Container,NodeType.Container,containerNode,toContainerNode,ContainerNodeConcern);
			}
			
		//productStoreMgr.clearGraphDB(1, false);

			

 			result.add("flag", "success");
 			result.add("message", id);
			}else{
				result.add("flag", "error");
				result.add("message", "this_type_exist");
			}
 			return result;
		}catch (Exception e) {
			throw new SystemException(e, "addClpType", log);
		}
 	 }
	
	private long addClpTypeDBRow(DBRow row) throws Exception {
		try{
			return floorClpTypeMgrZr.addClpType(row);
		}catch (Exception e) {
			throw new SystemException(e, "addClpTypeDBRow", log);
		}
	}

	@Override
	public void deleteClpType(long sku_lp_type_id) throws Exception {
		try{
			floorClpTypeMgrZr.deleteClpType(sku_lp_type_id);
		}catch (Exception e) {
			throw new SystemException(e, "deleteClpType", log);
		}
	}

	@Override
	public DBRow[] getCLpTypeBy(long ps_id , long pc_id) throws Exception {
		try
		{
			return floorClpTypeMgrZr.getCLpTypeBy(ps_id,pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getCLpTypeBy", log);

 		}
 	}

	@Override
	public DBRow getCLpTypeByTypeId(long sku_lp_type_id) throws Exception {
		try{
			return floorClpTypeMgrZr.getClpTypeByTypeId(sku_lp_type_id);
		}catch (Exception e) {
			throw new SystemException(e, "getCLpTypeByTypeId", log);
 		}
 	}

	@Override
	public DBRow updateClpType(HttpServletRequest request) throws Exception {
		DBRow result  = new DBRow();

		try{	
			DBRow row = new DBRow();
			long sku_lp_type_id = StringUtil.getLong(request, "sku_lp_type_id");
			long sku_lp_pc_id = StringUtil.getLong(request, "sku_lp_pc_id");
			long sku_lp_title_id = StringUtil.getLong(request, "sku_lp_title_id");
			long sku_lp_to_ps_id = StringUtil.getLong(request, "sku_lp_to_ps_id");
			long lp_type_id = StringUtil.getLong(request, "lp_type_id");
 			long sku_lp_box_type = StringUtil.getLong(request, "sku_lp_box_type");	//盒子类型
			
			int sku_lp_total_box = 0;
			int sku_lp_total_piece = 0 ;
			
			int sku_lp_box_length = StringUtil.getInt(request, "sku_lp_box_length");
			int sku_lp_box_width = StringUtil.getInt(request, "sku_lp_box_width");
			int sku_lp_box_height = StringUtil.getInt(request, "sku_lp_box_height");
			
			if(sku_lp_box_type  == 0l){
				sku_lp_total_box = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ;//商品的数量
				sku_lp_total_piece = sku_lp_total_box;	//商品的数量
			}else{
				DBRow boxSkuType = boxTypeMgrZr.getBoxTypeBuId(sku_lp_box_type);
				int totalBoxSkuType = boxSkuType.get("box_total", 0);
				sku_lp_total_box  = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ; //盒子的数量
				sku_lp_total_piece = sku_lp_total_box * totalBoxSkuType ; //商品的数量
				
				if(boxSkuType.get("box_pc_id", 0l) != sku_lp_pc_id){
					//盒子上面的商品 和 页面传递过来的pc_id 不等.
					result.add("flag", "error");
					result.add("message", "skuNotMatch");
					return result;
				}
			}
			
			if(sku_lp_total_box == 0){
				result.add("flag", "error");
				result.add("message", "skuLpTotalBoxLetter");
				return result;
			}
			if(sku_lp_total_piece == 0 ){
				result.add("flag", "error");
				result.add("message", "skuLpTotalPieceLetter");
				return result;
			}
			
			/*set DBRow*/
			row.add("sku_lp_pc_id", sku_lp_pc_id);
			row.add("sku_lp_title_id", sku_lp_title_id);
			row.add("sku_lp_to_ps_id", sku_lp_to_ps_id);
			row.add("lp_type_id", lp_type_id);
			row.add("sku_lp_box_type", sku_lp_box_type);
		
			row.add("sku_lp_box_length", sku_lp_box_length);
			row.add("sku_lp_box_width", sku_lp_box_width);
			row.add("sku_lp_box_height", sku_lp_box_height);
			
			row.add("sku_lp_total_box", sku_lp_total_box);
			row.add("sku_lp_total_piece", sku_lp_total_piece);
			
			updateCLpType(sku_lp_type_id,row);
 			result.add("flag", "success");
 			result.add("message", sku_lp_type_id);
 			return result;
		
		}catch (Exception e) {
			throw new SystemException(e, "updateClpType", log);
		}
 	}
	private void updateCLpType(long sku_lp_type_id , DBRow row) throws Exception{
		try{
			floorClpTypeMgrZr.updateCLpType(sku_lp_type_id, row);
		}catch (Exception e) {
			throw new SystemException(e, "updateCLpType", log);
		}
	}

	
	@Override
	public DBRow[] getClpContainerBySearchKey(PageCtrl pc, long sku_lp_type_id, String search_key) throws Exception {
		try{
			return floorClpTypeMgrZr.getClpContainerBySearchKey(pc, sku_lp_type_id, search_key);
		}catch (Exception e) {
			throw new SystemException(e, "getClpContainerBySearchKey", log);
 		}
 	}

	@Override
	public DBRow[] getClpContainerBy(PageCtrl pc, long sku_lp_type_id)
			throws Exception {
		try{
			return floorClpTypeMgrZr.getClpContainerBy(pc, sku_lp_type_id);
		}catch (Exception e) {
			throw new SystemException(e, "getClpContainerBy", log);
		}
 	}

	@Override
	public DBRow getClpTypeAndPsInfo(long sku_lp_type_id) throws Exception {
		try{
			return  floorClpTypeMgrZr.getClpTypeAndPsInfo(sku_lp_type_id);
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypeAndPsInfo", log);
 		}
 	}

	@Override
	public String getClpTypeName(long sku_lp_type_id) throws Exception {
		
		String returnValue = "" ;
		try{
			DBRow clpDetailRow = floorClpTypeMgrZr.getClpTypeAndPsInfo(sku_lp_type_id);
	 		if(clpDetailRow != null){
	 			returnValue = getClpTypeName(clpDetailRow);
	 		}
	 		return returnValue;
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypeName", log);
 		}
		 
	}
	/**
	 * 如果是装有盒子的CLP
	 * Tatto_GZ_BLP_2*3*6
	 * 如果没有盒子直接装有商品情况
	 * Tatto_GZ_2*3*6
	 */
	@Override
	public String getClpTypeName(DBRow clpType) throws Exception {
		StringBuffer clpName = new StringBuffer("");
		if(clpType.get("inner_pc_or_lp", 0) == 0){
			//属于商品
		//	clpName.append(clpType.getString("p_name")).append("_");
			clpName.append(clpType.getString("title")).append("_");
			clpName.append(clpType.get("stack_length_qty", 0)).append("*");
			clpName.append(clpType.get("stack_width_qty", 0)).append("*");
			clpName.append(clpType.get("stack_height_qty", 0));
		}else{
			//clpName.append(clpType.getString("p_name")).append("_");
			clpName.append(clpType.getString("title")).append("_");
			clpName.append("BLP_");
			clpName.append(clpType.get("stack_length_qty", 0)).append("*");
			clpName.append(clpType.get("stack_width_qty", 0)).append("*");
			clpName.append(clpType.get("stack_height_qty", 0));
		}
 		return clpName.toString();
	}

	@Override
	public DBRow addClpContainer(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow addClpContainer = new DBRow();
			String container = StringUtil.getString(request, "container");
			String hardwareId = StringUtil.getString(request, "hardwareId");
			long type_id = StringUtil.getLong(request, "type_id");
			
			addClpContainer.add("container", container);
			addClpContainer.add("hardwareId", hardwareId);
			addClpContainer.add("type_id",type_id);
			addClpContainer.add("container_type",  ContainerTypeKey.CLP);
			//检查hardwareId 是否已经存在了
			if(!StringUtil.isBlank(hardwareId))
			{
				boolean isExit = boxTypeMgrZr.isExitsHardwareId(hardwareId);
				if(isExit){
					result.add("flag", "error");
					result.add("message", "hardwareIdExits");
					return result;
				}
			}
			
			long id = addClpContainer(addClpContainer);
   			
			result.add("flag", "success");
			result.add("id", id);
			return result;
		}catch (Exception e) {
			throw new SystemException(e, "addClpContainer", log);
 		}
 	}
	private long addClpContainer(DBRow row) throws Exception{
		try{
			DBRow insertRow = containerMgrZr.addContainer(row);
 			return insertRow != null ? insertRow.get("con_id", 0l) : 0l;
		}catch (Exception e) {
			throw new SystemException(e, "addClpContainer", log);
		}
	}

	@Override
	public DBRow updateClpContainer(HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow addClpContainer = new DBRow();
			String container = StringUtil.getString(request, "container");
			String hardwareId = StringUtil.getString(request, "hardwareId");
			long con_id = StringUtil.getLong(request, "con_id");
			boolean isExit = boxTypeMgrZr.isExitsHardwareId(hardwareId, con_id);
			if(isExit){
				result.add("flag", "error");
				result.add("message", "hardwareIdExits");
				return result;
			}
			addClpContainer.add("container", container);
			addClpContainer.add("hardwareId", hardwareId);
 			floorClpTypeMgrZr.updateClpContainer(con_id, addClpContainer);
 			result.add("flag", "success");
 			return result;
		}catch (Exception e) {
			throw new SystemException(e, "updateClpContainer", log);
		}
	}

	@Override
	public String getClpTypeNameNeedPName(DBRow clpType) throws Exception {

		StringBuffer clpName = new StringBuffer("");
		if(clpType.get("inner_pc_or_lp", 0) == 0){
			//属于商品
			clpName.append(clpType.getString("p_name")).append("_");
			if(!StrUtil.isBlank(clpType.getString("title")))
			{
				clpName.append(clpType.getString("title")).append("_");
			}
			clpName.append(clpType.get("stack_length_qty", 0)).append("*");
			clpName.append(clpType.get("stack_width_qty", 0)).append("*");
			clpName.append(clpType.get("stack_height_qty", 0));
		}else{
			clpName.append(clpType.getString("p_name")).append("_");
			if(!StrUtil.isBlank(clpType.getString("title")))
			{
				clpName.append(clpType.getString("title")).append("_");
			}
			clpName.append(clpType.get("stack_length_qty", 0)).append("*");
			clpName.append(clpType.get("stack_width_qty", 0)).append("*");
			clpName.append(clpType.get("stack_height_qty", 0));
		}
 		return clpName.toString();
	
 	}

	@Override
	public String getClpTypeNameNeedPName(long clp_type_id) throws Exception {
		String returnValue = "" ;
		try{
			DBRow clpDetailRow = floorClpTypeMgrZr.getClpTypeAndPsInfo(clp_type_id);
	 		if(clpDetailRow != null){
	 			returnValue = getClpTypeNameNeedPName(clpDetailRow);
	 		}
	 		return returnValue;
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypeNameNeedPName", log);
 		}
		
 	}
	/**
	 * 因为CLP表中没有容器本身的大小，所以这里查询的是基础容器的大小
	 */
	@Override
	public DBRow[] getClpTypeByPcid(long pc_id) throws Exception {
		try{
			 return floorClpTypeMgrZr.getClpTypeByPcid(pc_id);
		}catch (Exception e) {
			throw new SystemException(e, "getClpTypeByPcid", log);
 		}	
	}
	@Override
	public DBRow findContainerById(long id) throws Exception {
		return this.floorClpTypeMgrZr.findContainerById(id);
	}
	
	//查询容器下的商品
	public DBRow findContainerProduct(long pc_id)throws Exception{
		return this.floorClpTypeMgrZr.findContainerProduct(pc_id);
	}
	
    //根据id查询 clp
    public DBRow selectContainerClpById(long id)throws Exception{
    	return this.floorClpTypeMgrZr.selectContainerClpById(id);
    }
    
    //根据id查询blp
	public DBRow selectContainerBlpById(long id)throws Exception{
		return this.floorClpTypeMgrZr.selectContainerBlpById(id);
	}
	
	 //根据id查询ilp
    public DBRow selectContainerIlpById(long id)throws Exception{
    	return this.floorClpTypeMgrZr.selectContainerIlpById(id);
    }
    
    //根据clp id 查询仓库关系
    public DBRow[] selectPsByClpId(long id)throws Exception{
	   return this.floorClpTypeMgrZr.selectPsByClpId(id);
    }
    
    //根据IlpTypeId查询ilp和商品信息
    public DBRow findIlpTypeProductByIlpTypeId(long id)throws Exception
    {
    	try
    	{
    		return floorClpTypeMgrZr.findIlpTypeProductByIlpTypeId(id);
    	}
    	catch (Exception e)
    	{
    		throw new SystemException(e, "findIlpTypeProductByIlpTypeId", log);
    	}	
    }
    
    //根据BlpTypeId查询blp和商品信息
    public DBRow findBlpTypeProductByBlpTypeId(long id)throws Exception
    {
    	try
    	{
    		return floorClpTypeMgrZr.findBlpTypeProductByBlpTypeId(id);
    	}
    	catch (Exception e)
    	{
    		throw new SystemException(e, "findBlpTypeProductByBlpTypeId", log);
    	}	
    }
    
    //根据id 查询 title ship to
    public DBRow[] findLpTliteAndShipTo(long id,int title_id,int ship_to_id)throws Exception
    {
    	try
    	{
    		return floorClpTypeMgrZr.getLpTitleAndShipTo(id,title_id,ship_to_id);
    	}
    	catch (Exception e)
    	{
    		throw new SystemException(e, "findLpTliteAndShipTo", log);
    	}	
    }
    
    //根据id 查询 title ship to
    public DBRow[] findLpTliteAndShipTo(long id,int title_id,int customerId,int ship_to_id)throws Exception {
    	
    	try{
    		
    		return floorClpTypeMgrZr.getLpTitleAndShipTo(id,title_id,customerId,ship_to_id);
    		
    	} catch (Exception e) {
    		throw new SystemException(e, "findLpTliteAndShipTo", log);
    	}	
    }
    
    public DBRow[] findClpTitle(long id,int title_id,int ship_to_id)throws Exception
    {
    	try
    	{
   		
    		 return floorClpTypeMgrZr.getCLPTypeForShipTo(id, 0, title_id);
   		
    	}
    	catch (Exception e)
    	{
    		throw new SystemException(e, "findClpTitle", log);
    	}	
    }
    
    public DBRow[] findClpShipTo(long id,int title_id,int ship_to_id)throws Exception
    {
    	try
    	{
   		
    		 return floorClpTypeMgrZr.getCLPTypeForShipTo(id, ship_to_id, 0);
   		
    	}
    	catch (Exception e)
    	{
    		throw new SystemException(e, "findClpShipTo", log);
    	}	
    }
    
    
    @Override
   	public DBRow addLpTitleShipTo(HttpServletRequest request) throws Exception {
   		DBRow result  = new DBRow();
   		try{
   			DBRow row = new DBRow();
   			long id = StringUtil.getLong(request, "post_lp_type_id");
   			long sku_lp_pc_id = StringUtil.getLong(request, "post_pc_id");
   			long sku_lp_title_id = StringUtil.getLong(request, "sku_lp_title_id");
   			long sku_lp_to_ps_id = StringUtil.getLong(request, "sku_lp_to_ps_id");
   			long customerId = StringUtil.getLong(request, "customer_id");
   			if(sku_lp_title_id > 0 || sku_lp_to_ps_id > 0 || customerId > 0)
   			{
	   			//验证TITLE Ship to 是否存在
				//查询等于b   	删除
				DBRow[] clpvalues =	floorClpTypeMgrZr.getCLPTypeTitleShipTo(id,sku_lp_to_ps_id,sku_lp_title_id,customerId);
				if(clpvalues.length > 0)
				{
					result.add("flag", "exist");
					return result;
				}
				//插入你这条数据
				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id,0);
				int sort=sortRow.get("sort", 0);
				row.add("lp_type_id",id);
	 			row.add("ps_id",sku_lp_to_ps_id);
	 			row.add("ship_to_sort",sort+1);
	 			row.add("lp_pc_id",sku_lp_pc_id);
	 			row.add("ship_to_id", sku_lp_to_ps_id);
	 			row.add("title_id", sku_lp_title_id);
	 			row.add("customer_id", customerId);
	 		    long clp_ps_id=this.floorClpTypeMgrZr.addClpPs(row);
	 		    result.add("flag", "success");
	 			result.add("message", id);
   			}
			return result;
			
   		}catch (Exception e) {
   			throw new SystemException(e, "addLpTitleShipTo", log);
   		}
    }
    
    
    
//    public DBRow addLpTitleShipTo(HttpServletRequest request) throws Exception {
//   		DBRow result  = new DBRow();
//   		try{
//   			
//   			DBRow row = new DBRow();
//   			long id = StringUtil.getLong(request, "post_lp_type_id");
//   			long sku_lp_pc_id = StringUtil.getLong(request, "post_pc_id");
//   			long sku_lp_title_id = StringUtil.getLong(request, "sku_lp_title_id");
//   			long sku_lp_to_ps_id = StringUtil.getLong(request, "sku_lp_to_ps_id");
//   			long from_ship_to_value = StringUtil.getLong(request, "from_ship_to_value");
//   			long from_title_value = StringUtil.getLong(request, "from_title_value");
//   			
//   			////system.out.println("from_ship_to_value: "+from_ship_to_value+" from_title_value: "+from_title_value);
//   			//验证TITLE Ship to 是否存在
//   			if(from_ship_to_value==0 && from_title_value!=0){
//   				//查询等于b   	删除
//   				DBRow[] clpvalues =	floorClpTypeMgrZr.getCLPTypeTitleShipTo(id,0,from_title_value);
//   				for(DBRow clpvalue: clpvalues){
//   					floorClpTypeMgrZr.deleteClpTitleShipTo(clpvalue.get("lp_title_ship_id",0l));
//   				}
//   				//插入你这条数据
//   				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id);
//				int sort=sortRow.get("sort", 0);
//				
//				row.add("lp_type_id",id);
//	 			row.add("ps_id",sku_lp_to_ps_id);
//	 			row.add("ship_to_sort",sort+1);
//	 			row.add("lp_pc_id",sku_lp_pc_id);
//	 			row.add("ship_to_id", sku_lp_to_ps_id);
//	 			row.add("title_id", sku_lp_title_id);
//	 		    long clp_ps_id=this.floorClpTypeMgrZr.addClpPs(row);
//	 		    result.add("flag", "success");
//  	 			result.add("message", id);
//   			}else if(from_ship_to_value!=0 && from_title_value==0){
//   				//查询等于a 	删除
//   				DBRow[] clpvalues =	floorClpTypeMgrZr.getCLPTypeTitleShipTo(id,from_ship_to_value,0);
//   				for(DBRow clpvalue: clpvalues){
//   					floorClpTypeMgrZr.deleteClpTitleShipTo(clpvalue.get("lp_title_ship_id",0l));
//   				}
//   				//插入你这条数据
//   				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id);
//				int sort=sortRow.get("sort", 0);
//				
//				row.add("lp_type_id",id);
//	 			row.add("ps_id",sku_lp_to_ps_id);
//	 			row.add("ship_to_sort",sort+1);
//	 			row.add("lp_pc_id",sku_lp_pc_id);
//	 			row.add("ship_to_id", sku_lp_to_ps_id);
//	 			row.add("title_id", sku_lp_title_id);
//	 		    long clp_ps_id=this.floorClpTypeMgrZr.addClpPs(row);
//	 		    result.add("flag", "update");
//  	 			result.add("message", id);
//   			}else if(from_ship_to_value!=0&&from_title_value!=0){
//   				 DBRow[] clpexist = floorClpTypeMgrZr.getCLPTypeTitleShipTo(id,from_ship_to_value,from_title_value);
//   				 if(clpexist.length>0){
//   					result.add("flag", "exist");
//   	   	 			result.add("message", id);
//   	   	 		return result;
//   				 }else{
//	   				DBRow[] clpvalues =	floorClpTypeMgrZr.getCLPTypeTitleAndShipTo(id,from_ship_to_value,from_title_value);
//	   				for(DBRow clpvalue: clpvalues){
//	   					floorClpTypeMgrZr.deleteClpTitleShipTo(clpvalue.get("lp_title_ship_id",0l));
//	   				}
//	   				 }
//   				
//   				//插入你这条数据
//   				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id);
//				int sort=sortRow.get("sort", 0);
//				
//				row.add("lp_type_id",id);
//	 			row.add("ps_id",sku_lp_to_ps_id);
//	 			row.add("ship_to_sort",sort+1);
//	 			row.add("lp_pc_id",sku_lp_pc_id);
//	 			row.add("ship_to_id", sku_lp_to_ps_id);
//	 			row.add("title_id", sku_lp_title_id);
//	 		    long clp_ps_id=this.floorClpTypeMgrZr.addClpPs(row);
//	 			result.add("flag", "success");
//   	 			result.add("message", id);
//   			}else{
//   			
//   			
//   			
//   		
//
//   				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id);
//   				int sort=sortRow.get("sort", 0);
//   				
//   				row.add("lp_type_id",id);
//   	 			row.add("ps_id",sku_lp_to_ps_id);
//   	 			row.add("ship_to_sort",sort+1);
//   	 			row.add("lp_pc_id",sku_lp_pc_id);
//   	 			row.add("ship_to_id", sku_lp_to_ps_id);
//   	 			row.add("title_id", sku_lp_title_id);
//   	 		    long clp_ps_id=this.floorClpTypeMgrZr.addClpPs(row);
//   	 			
//   	 			result.add("flag", "success");
//   	 			result.add("message", id);
//   		
//   			}
//   		
//
//    	
//    			return result;
//   		}catch (Exception e) {
//   			throw new SystemException(e, "addLpTitleShipTo", log);
//   		}
//    	 }
    
	@Override
	public void deleteLpTitleAndShipTo(long lp_title_ship_id) throws Exception {
		try{
			floorClpTypeMgrZr.deleteLpTitleAndShipTo(lp_title_ship_id);
		}catch (Exception e) {
			throw new SystemException(e, "deleteClpType", log);
		}
	}
	
	/**
	 * 导出产品的CLP以及title和shipTo
	 */
	public String ajaxDownCLPAndTitleAndShipTo(HttpServletRequest request) throws Exception {
		
		String cmd = StringUtil.getString(request,"cmd");
		String name= StringUtil.getString(request,"seachValue");
		String title_id = StringUtil.getString(request, "titleId");
		String productLineId = StringUtil.getString(request, "lineId");
		String catalogId = StringUtil.getString(request, "catalogId");
		int productType = StringUtil.getInt(request, "productType");

		// 获取所有产品
		DBRow[] productRows = new DBRow[0];
		
		if(cmd.equals("seach")){
			productRows = proprietaryMgrZyj.filterProductAndRelateInfoByPcLineCategoryTitleId(true,catalogId,productLineId,productType, 0, 0,title_id, 0L, 0, 0,0,0, null, request);
		}else if(cmd.equals("name")){
			productRows = proprietaryMgrZyj.findDetailProductRelateInfoLikeSearch(true, name, title_id, 0L, 0, 0,-1,0,0, null, request);
		}else{
			productRows = proprietaryMgrZyj.findAllProductsRelateInfoByTitleAdid(true, title_id, 0L, 0, 0, null, request);
		}
		
		//获得模板
		XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product/ExportProductCLPsTitlesAndShipTos.xlsm"));//根据模板创建工作文件
		XSSFSheet sheet = workbook.getSheetAt(0);//获得模板的第一个sheet
		XSSFRow row = sheet.getRow(0);//得到Excel工作表的行 
		int rowCount = 0;
		
		//根据产品查询每个产品使用的clp以及title shipTo
		for(int j=0; j<productRows.length; j++ ){
			
			DBRow[] productsClps = boxTypeMgrZr.getBoxTypes(productRows[j].get("pc_id",0),0,0,0);
			
			for(int i=0; i<productsClps.length; i++) {
				
				String titleIds = productsClps[i].getString("title_ids");
	      		String shipToIds = productsClps[i].getString("ship_to_ids");
	      		
	      		if(StringUtils.isNotEmpty(titleIds)){
	      			
	    			String[] titleIdArr = titleIds.split(",");
	    			String[] shipToIdArr = shipToIds.split(",");
	    			
	    			for (int t=0; t<titleIdArr.length; t++) {
	    				
	    				rowCount++;
	    				DBRow title = proprietaryMgrZyj.findProprietaryByTitleId(Long.valueOf(titleIdArr[t]));
	    				DBRow shipTo = shipToMgrZJ.getShipToById(Long.valueOf(shipToIdArr[t]));
	    				String titleName=title==null?"":title.getString("title_name");
	    				String shipToName=shipTo==null?"":shipTo.getString("ship_to_name");
	    				row = addSheetRow(sheet, row, productRows[j], productsClps[i], titleName, shipToName, rowCount);
	    			}
	    			
	      		} else {
	      			
	      			rowCount++;
	      			//row = addSheetRow(sheet, row, productRows[j].getString("p_name"), productsClps[i], "", "", rowCount);
	      			row = addSheetRow(sheet, row, productRows[j], productsClps[i], "", "", rowCount);
	      		}	
			}
		}
		
		String path = "upl_excel_tmp/ExportProductCLPsTitlesAndShipTos_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsm";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		workbook.write(fout);
		fout.close();
		return (path);
	}
	
	/*
	 * 创建excel中sheet的行的数据
	 */
	private XSSFRow addSheetRow(XSSFSheet sheet, XSSFRow row, DBRow productRows, DBRow cLPRow, String titleName, String shipToName, int rowCount) throws Exception {
		try{
			row = sheet.createRow(rowCount);
			row.createCell(0).setCellValue(0);
			row.createCell(1).setCellValue(productRows.getString("p_name"));
			row.createCell(2).setCellValue(cLPRow.getString("lp_name"));
			row.createCell(3).setCellValue(productRows.getString("sku"));
			row.createCell(4).setCellValue(productRows.getString("lot_no"));
			row.createCell(5).setCellValue(cLPRow.getString("type_name"));
			if(cLPRow.get("inner_pc_or_lp",0l) > 0){ 
				DBRow innerLp = boxTypeMgrZr.findIlpByIlpId(cLPRow.get("inner_pc_or_lp",0l));
	  			row.createCell(6).setCellValue(innerLp==null?"":innerLp.getString("lp_name"));
			}else{
				row.createCell(6).setCellValue("Product");
			}
			row.createCell(7).setCellValue(cLPRow.get("stack_length_qty",0l));
			row.createCell(8).setCellValue(cLPRow.get("stack_width_qty",0l));
			row.createCell(9).setCellValue(cLPRow.get("stack_height_qty",0l));
			row.createCell(10).setCellValue(cLPRow.get("stack_length_qty",0l)*cLPRow.get("stack_width_qty",0l)*cLPRow.get("stack_height_qty",0l));
			row.createCell(11).setCellValue(titleName);
			row.createCell(12).setCellValue(shipToName);
			row.createCell(13).setCellValue(new YesOrNotKey().getYesOrNotKeyName(cLPRow.get("is_has_sn",0)).substring(0,1));
			row.createCell(14).setCellValue("Success");
			return row;
		}catch (Exception e) {
			throw new Exception("addSheetRow(XSSFSheet sheet, XSSFRow row, DBRow cLPRow,String titleName, String shipToName, int rowCount):" + e);
		}
	}
	
	@Override
	public int findClpTypeCnByPackagingType(long pkg_id) throws Exception {
		try
		{
			return floorClpTypeMgrZr.findClpTypeCnByPackagingType(pkg_id);
		}catch (Exception e) {
			throw new Exception("findClpTypeCnByPackagingType:" + e);
		}
	}
	//type_id 获取con_id
	   public DBRow findContainerCon(long type_id)throws Exception
	    {
	    	try
	    	{
	   		
	    		 return floorClpTypeMgrZr.findContainerType(type_id);
	   		
	    	}
	    	catch (Exception e)
	    	{
	    		throw new SystemException(e, "findClpShipTo", log);
	    	}	
	    }
	   
	   /**
		 * 功能：获取所需clp及数量
		 * @param title
		 * @param shipToid
		 * @param pc_id
		 * @param orderQty
		 * @param pallets
		 * @return
		 */	
		public DBRow getPoundPerPackage(long title, long shipToid, long pc_id,int orderQty, float pallets){
			DBRow recommand = new DBRow();
			
			//托盘数不能为小数 
			if(pallets!=-1 && pallets%1==0 && orderQty!=-1 && pc_id!=-1){
				
				try {
					DBRow[] recommands = floorClpTypeMgrZr.getPoundPerPackage(title, shipToid, pc_id, orderQty, pallets);
					
					if(recommands!=null && recommands.length>=1){
						recommand = calculateAndRecommandData(recommands, orderQty, pallets);
					}
					
					//若title或shipto不为空，且没有找到合适的容器，重新查询title和shipto都为空
					if((recommand==null || recommand.isEmpty()) && (title!=0 || shipToid!=0)){
						
						recommands = floorClpTypeMgrZr.getPoundPerPackage(0, 0, pc_id, orderQty, pallets);
						
						if(recommands!=null && recommands.length>=1){
							recommand = calculateAndRecommandData(recommands, orderQty, pallets);
						}
					}
				} catch (Exception e) {
					recommand = new DBRow();
				}			
			}
			
			return recommand;
		}
		
		/**
		 * 功能：计算并生成推荐结果
		 * @param recommands
		 * @return DBRow
		 */
		private DBRow calculateAndRecommandData(DBRow[] recommands, int orderQty, float pallets){
			DBRow recommand = new DBRow();
			
			if(recommands!=null && recommands.length>=1){
				DBRow first = recommands[0];
				
				WeightUOMKey weightUOMKey = new WeightUOMKey();
				
				recommand.add("pc_id", first.get("pc_id", 0L));
				recommand.add("p_weight", first.get("p_weight", 0F));//recommanfindContainerTypeAndClpTyped
				recommand.add("total_weight", first.get("clp_type_weight", 0F) * pallets);
				
				DBRow clpType = new DBRow();
				clpType.add("clp_type_id", first.get("clp_type_id", 0L));
				clpType.add("clp_type_name", first.getString("clp_type_name"));
				clpType.add("clp_type_weight", first.get("clp_type_weight", 0F));
				clpType.add("weight_uom", weightUOMKey.getWeightUOMKey(first.get("weight_uom", 0)));
				clpType.add("clp_qty", pallets);
				clpType.add("pc_qty", first.get("pc_qty", 0));
				clpType.add("clp_packaging_weight", first.get("clp_packaging_weight", 0F));
				
				DBRow[] clpTypes = new DBRow[]{clpType};
				
				recommand.add("clp_types", clpTypes);
			}
			
			return recommand;
		}
		
		
		//根据type id 查询 license_plate_tpye and container_type
		public Map findContainerTypeAndCLpType(List<Integer> type_id) throws Exception {
			try{
		
		
				Map<String, Object> type = new HashMap();
				for(int i= 0;i<type_id.size();i++){
					
					DBRow ClpTypevalue = floorClpTypeMgrZr.findContainerTypeAndClpType(type_id.get(i));
					if(ClpTypevalue!=null){
						String typeid = ClpTypevalue.get("lpt_id","");
						type.put(typeid,ClpTypevalue );
					}
					
					 
				}				
		 	
		 		return type;
			}catch (Exception e) {
				throw new SystemException(e, "getClpTypeName", log);
	 		}
			 
		}

	   
	
	public void setContainerMgrZr(ContainerMgrIfaceZr containerMgrZr) {
		this.containerMgrZr = containerMgrZr;
	}
    
	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}
	 public void setFloorClpTypeMgrZr(FloorClpTypeMgrZr floorClpTypeMgrZr) {
		this.floorClpTypeMgrZr = floorClpTypeMgrZr;
	}
	
	public void setBoxTypeMgrZr(BoxTypeMgrIfaceZr boxTypeMgrZr) {
		this.boxTypeMgrZr = boxTypeMgrZr;
	}
	
	public void setShipToMgrZJ(ShipToMgrIFaceZJ shipToMgrZJ) {
		this.shipToMgrZJ = shipToMgrZJ;
	}
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public FloorProductStoreMgr getProductStoreMgr() {
		return productStoreMgr;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	
}
