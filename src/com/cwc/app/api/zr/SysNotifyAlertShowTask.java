package com.cwc.app.api.zr;

import java.util.Collection;
import java.util.Iterator;

import org.directwebremoting.Browser;
import org.directwebremoting.Container;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ServerContextFactory;
import org.directwebremoting.extend.ScriptSessionManager;

import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class SysNotifyAlertShowTask implements Runnable
{

	private DBRow[] executeUsers;
	private String content ;
	 
	public SysNotifyAlertShowTask(DBRow[] executeUsers,String content)
	{
		super();
		this.executeUsers = executeUsers;
		this.content = content.replaceAll("\n", "<br />").replaceAll("\r", "<br />");
	}

	@Override
	public void run()
	{
		//调用页面的Javascript方法
		Container container = ServerContextFactory.get().getContainer();
		ScriptSessionManager manager = container.getBean(ScriptSessionManager.class);
		Collection<ScriptSession> sessions  = manager.getAllScriptSessions();
	 
		ScriptBuffer script = new ScriptBuffer();
		script.appendScript("window.top.showSystemNotifyContent(\""+StringUtil.replaceEnter(content)+"\")");
		for(ScriptSession scriptSession : sessions) {
			Object adid = scriptSession.getAttribute("adid");
			if(adid != null && Long.parseLong(adid.toString()) != 0l && isInExecuteUsers(Long.parseLong(adid.toString()))){
				scriptSession.addScript(script);
			}
			
	     }
	}
	
	public void setExecuteUsers(DBRow[] executeUsers)
	{
		this.executeUsers = executeUsers;
	}
	public boolean isInExecuteUsers(long adid){
		boolean flag = false; 
		if(executeUsers != null && executeUsers.length > 0 ){
			for(DBRow row : executeUsers){
				if(row.get("adid", 0l) == adid){
					flag = true ;
					break ;
				}
				
			}
		}
		return flag ;
		
	}

}
