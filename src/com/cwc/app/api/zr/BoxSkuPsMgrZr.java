package com.cwc.app.api.zr;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.condition.Http;

import com.cwc.app.floor.api.zr.FloorBoxSkuPsMgrZr;
import com.cwc.app.iface.zr.BoxSkuPsMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class BoxSkuPsMgrZr implements BoxSkuPsMgrIfaceZr{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorBoxSkuPsMgrZr floorBoxSkuPsMgrZr ;

 

	public void setFloorBoxSkuPsMgrZr(FloorBoxSkuPsMgrZr floorBoxSkuPsMgrZr) {
		this.floorBoxSkuPsMgrZr = floorBoxSkuPsMgrZr;
	}
	


	@Override
	public DBRow[] getAllBoxProductByPage(PageCtrl page) throws Exception {
		try{
				
			return floorBoxSkuPsMgrZr.getAllBoxProductByPage(page) ;
		}catch (Exception e) {
			 throw new SystemException(e,"getAllBoxProductByPage",log);	
		}
 	}



	@Override
	public void addBoxSkuPs(HttpServletRequest request) throws Exception {
		try{
			long box_pc_id = StringUtil.getLong(request, "box_pc_id");
			long to_ps_id = StringUtil.getLong(request, "to_ps_id");
			long box_type_id = StringUtil.getLong(request, "box_type_id");
 			DBRow row = new DBRow();
			row.add("box_pc_id", box_pc_id);
			row.add("box_type_id", box_type_id);
			row.add("blp_ship_to_id", to_ps_id);
			int box_sort = floorBoxSkuPsMgrZr.getMaxIndexBy(box_pc_id, to_ps_id) + 1;
			row.add("box_sort", box_sort);
			addBoxSkuPs(row);
  		}catch (Exception e) {
			 throw new SystemException(e,"addBoxSkuPs",log);
 		}
	}


	/**
	 *  
	 */
	@Override
	public void addBoxSkuPs(DBRow row) throws Exception {
 		try{
 			floorBoxSkuPsMgrZr.addBoxSkuPs(row);
 		}catch (Exception e) {
			 throw new SystemException(e,"row",log);
		}
	}

	@Override
	public void deleteBoxSkuPs(long box_type_ps_id) throws Exception {
 		try{
 			floorBoxSkuPsMgrZr.deleteBoxSkuPs(box_type_ps_id);
 		}catch (Exception e) {
			 throw new SystemException(e,"deleteBoxSkuPs",log);
 		}
	}



	@Override
	public DBRow getFirstBoxSkuPs(HttpServletRequest requset) throws Exception {
		try{
			long pc_id = StringUtil.getLong(requset, "box_pc_id");
			long ps_id = StringUtil.getLong(requset,"to_ps_id");
			return 	floorBoxSkuPsMgrZr.getBoxSkuPsByPsIdAndPcid(pc_id, ps_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getFirstBoxSkuPs",log);
		}
 	}



	@Override
	public DBRow[] getAllBoxSkuByPcId(long pc_id) throws Exception {
		try{
			return floorBoxSkuPsMgrZr.getAllBoxSkuByPcId(pc_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getAllBoxSkuByPcId",log);
		}
 	}



	@Override
	public int getIndexByPsIdPcId(HttpServletRequest request) throws Exception {
		try{
			long pc_id = StringUtil.getLong(request, "box_pc_id");
			long ps_id = StringUtil.getLong(request, "to_ps_id");
			return floorBoxSkuPsMgrZr.getMaxIndexBy(pc_id, ps_id) + 1;
		}catch (Exception e) {
			 throw new SystemException(e,"getIndexByPsIdPcId",log);
 		}
 		 
	}



	@Override
	public boolean validateBoxSkuPsExits(HttpServletRequest request)
			throws Exception {
		try{
 			long pc_id = StringUtil.getLong(request, "box_pc_id");
			long ps_id = StringUtil.getLong(request, "to_ps_id");
			long box_type_id = StringUtil.getLong(request, "box_type_id");
			int count = floorBoxSkuPsMgrZr.countNumBy(pc_id, ps_id, box_type_id);
			return !(count == 0) ;
		}catch (Exception e) {
			 throw new SystemException(e,"validateBoxSkuPsExits",log);
 		}
 	 
	}



	@Override
	public void exchangeBoxSkuPsSort(HttpServletRequest requset)
			throws Exception {
			try{
				long exchange = StringUtil.getLong(requset, "exchange");
				long exchange_to = StringUtil.getLong(requset, "exchange_to");
				DBRow exchangeRow = floorBoxSkuPsMgrZr.getBoxSkuPsBy(exchange);
				DBRow exchangeToRow = floorBoxSkuPsMgrZr.getBoxSkuPsBy(exchange_to);
			
				DBRow updateRow = new DBRow();
				//
				updateRow.add("ship_to_sort", exchangeToRow.get("ship_to_sort", 0));
				//system.out.println("exchange:"+exchange+"ship_to_sort:"+exchangeRow.get("ship_to_sort", 0));
				floorBoxSkuPsMgrZr.updateBoxSkuPs(exchange, updateRow);
				
				updateRow.add("ship_to_sort", exchangeRow.get("ship_to_sort", 0));	
				//system.out.println("exchange_to:"+exchange_to+"ship_to_sort:"+exchangeToRow.get("ship_to_sort", 0));
				floorBoxSkuPsMgrZr.updateBoxSkuPs(exchange_to, updateRow);
			}catch (Exception e) {
				 throw new SystemException(e,"exchangeBoxSkuPsSort",log);

 			}
	}



	@Override
	public DBRow[] getAllBoxSkuByPcIdAndPsId(long pc_id, long to_ps_id, long basic_container_type)
			throws Exception {
			try{
				return  floorBoxSkuPsMgrZr.getAllBoxSkuByPcIdPsId(pc_id, to_ps_id,basic_container_type);
			}catch (Exception e) {
				 throw new SystemException(e,"getAllBoxSkuByPcIdAndPsId",log);
			}
  
	}
	
	/**
	 * 商品到某个仓库box最大排序
	 * @param box_pc_id
	 * @param to_ps_id
	 * @return
	 * @throws Exception
	 */
	public int getMaxIndexBy(long box_pc_id, long to_ps_id)throws Exception {
		try{
			return  floorBoxSkuPsMgrZr.getMaxIndexBy(box_pc_id, to_ps_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getAllBoxSkuByPcIdAndPsId",log);
		}
	}
	
	public DBRow[] findShipToNamesByBoxShipTo(long box_pc_id)throws Exception 
	{
		try{
			return  floorBoxSkuPsMgrZr.findShipToNamesByBoxShipTo(box_pc_id);
		}catch (Exception e) {
			 throw new SystemException(e,"getAllBoxSkuByPcIdAndPsId",log);
		}
	}
	 
	public DBRow[] findShipToBoxCountByPcid(long pc_id, int limit, int type) throws Exception
	{
		try{
			return  floorBoxSkuPsMgrZr.findShipToBoxCountByPcid(pc_id, limit, type);
		}catch (Exception e) {
			 throw new SystemException(e,"findShipToBoxCountByPcid",log);
		}
	}
	
}
