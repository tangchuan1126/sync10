package com.cwc.app.api.zr;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.floor.api.zr.FloorScheduleReplayMgrZr;
import com.cwc.app.iface.zr.ScheduleReplayMgrIfaceZR;
import com.cwc.app.iface.zr.SysNotifyIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ScheduleReplayMgrZr implements ScheduleReplayMgrIfaceZR{


	static Logger log = Logger.getLogger("ACTION");
	
	public FloorScheduleReplayMgrZr floorScheduleReplayMgrZr;
	public FloorScheduleMgrZr floorScheduleMgrZr;
	public SysNotifyIfaceZr sysNotifyMgrZr ;
	
	public void setSysNotifyMgrZr(SysNotifyIfaceZr sysNotifyMgrZr) {
		this.sysNotifyMgrZr = sysNotifyMgrZr;
	}

 

	public void setFloorScheduleMgrZr(FloorScheduleMgrZr floorScheduleMgrZr) {
		this.floorScheduleMgrZr = floorScheduleMgrZr;
	}



	public void setFloorScheduleReplayMgrZr(
			FloorScheduleReplayMgrZr floorScheduleReplayMgrZr) {
		this.floorScheduleReplayMgrZr = floorScheduleReplayMgrZr;
	}

	
	 
	@Override
	public long addScheduleReplay(DBRow row , HttpServletRequest request) throws Exception {
		long id = 0l;
		try{
			//在schedule_sub表更新信息。
			 
			DBRow updateScheduleSeb = new DBRow();
			String schedule_sub_idString = row.getString("schedule_sub_id");
			if(null != schedule_sub_idString && schedule_sub_idString.length() > 0){
				long schedule_sub_id = Long.parseLong(schedule_sub_idString); 
				
				updateScheduleSeb.add("schedule_state", row.getString("sch_state"));
				floorScheduleMgrZr.updateScheduleSub(schedule_sub_id, updateScheduleSeb);
				row.remove("schedule_sub_id");
		 
				//在schedule表中计算出总的进度。修改进度 这里就是直接根据页面上计算过来的值进行修改
				
				DBRow updateSchedule = new DBRow();
				updateSchedule.add("schedule_state", row.getString("total_state"));
				long scheduleId = Long.parseLong(row.getString("schedule_id"));
				floorScheduleMgrZr.updateSchedule(scheduleId, updateSchedule);
				
				row.remove("total_state");
				 id = floorScheduleReplayMgrZr.addScheduleReplay(row);
			}else{
				row.remove("schedule_sub_id");
				row.remove("total_state");
				 id = floorScheduleReplayMgrZr.addScheduleReplay(row);
			}
			// 根据schedule_id去查询任务，如果任务是需要通知的那么在回复或者是跟进的时候都是需要发送 根据任务上的提醒去选择做什么
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(row.get("schedule_id", 0l));
			if(scheduleRow != null){
				//添加回复,在replay表添加信息 
				sysNotifyMgrZr.handleScheduleReplay(row.get("schedule_id", 0l), scheduleRow.getString("schedule_overview"), row.getString("sch_replay_context"), request);
			}
			return id;
			
		}catch(Exception e){
			throw new SystemException(e,"addSchedule",log);
		}
	}
	
	@Override
	public long addScheduleReplay(DBRow row ,AdminLoginBean adminLoggerBean, String is_need_notify_executer) throws Exception {
		long id = 0l;
		try{
			//在schedule_sub表更新信息。
			 
			DBRow updateScheduleSeb = new DBRow();
			String schedule_sub_idString = row.getString("schedule_sub_id");
			if(null != schedule_sub_idString && schedule_sub_idString.length() > 0){
				long schedule_sub_id = Long.parseLong(schedule_sub_idString); 
				
				updateScheduleSeb.add("schedule_state", row.getString("sch_state"));
				floorScheduleMgrZr.updateScheduleSub(schedule_sub_id, updateScheduleSeb);
				row.remove("schedule_sub_id");
		 
				//在schedule表中计算出总的进度。修改进度 这里就是直接根据页面上计算过来的值进行修改
				
				DBRow updateSchedule = new DBRow();
				updateSchedule.add("schedule_state", row.getString("total_state"));
				long scheduleId = Long.parseLong(row.getString("schedule_id"));
				floorScheduleMgrZr.updateSchedule(scheduleId, updateSchedule);
				
				row.remove("total_state");
				 id = floorScheduleReplayMgrZr.addScheduleReplay(row);
			}else{
				row.remove("schedule_sub_id");
				row.remove("total_state");
				 id = floorScheduleReplayMgrZr.addScheduleReplay(row);
			}
			// 根据schedule_id去查询任务，如果任务是需要通知的那么在回复或者是跟进的时候都是需要发送 根据任务上的提醒去选择做什么
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(row.get("schedule_id", 0l));
			if(scheduleRow != null){
				//添加回复,在replay表添加信息 
				sysNotifyMgrZr.handleScheduleReplay(row.get("schedule_id", 0l), scheduleRow.getString("schedule_overview"), row.getString("sch_replay_context"), adminLoggerBean, is_need_notify_executer);
			}
			return id;
			
		}catch(Exception e){
			throw new SystemException(e,"addSchedule",log);
		}
	}

	@Override
	public DBRow[] getRowsByScheduleId(long id) throws Exception {
		try{
			return floorScheduleReplayMgrZr.getRowsByScheduleId(id);
		}catch(Exception e){
			throw new SystemException(e,"getRowsByScheduleId",log);
		}
	}



	@Override
	public long addSimpleScheduleReplay(DBRow replay) throws Exception {
		 try{
			return  floorScheduleReplayMgrZr.addScheduleReplay(replay);
		 }catch (Exception e) {
			 throw new SystemException(e,"addSimpleScheduleReplay",log);
		}
	 
	}
	
	
}
