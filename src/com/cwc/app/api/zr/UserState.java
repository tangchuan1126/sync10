package com.cwc.app.api.zr;

public class UserState {

	private String userId ;
	private boolean isStop ;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public boolean isStop() {
		return isStop;
	}
	public void setStop(boolean isStop) {
		this.isStop = isStop;
	}
	public UserState(String userId, boolean isStop) {
		super();
		this.userId = userId;
		this.isStop = isStop;
	}
	
	
}
