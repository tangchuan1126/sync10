package com.cwc.app.api.zr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.directwebremoting.Browser;
import org.directwebremoting.WebContextFactory;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorScheduleMgrZr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.AndroidPushMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.SysNotifyIfaceZr;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.iface.zzq.PushMessageByOpenfireMgrIfaceZzq;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.outface.core.ParameterException;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.email.Attach;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class SysNotifyMgrZr implements SysNotifyIfaceZr {

	
	static Logger log = Logger.getLogger("ACTION");
	 

	private FloorScheduleMgrZr floorScheduleMgrZr;
	private ShortMessageMgrZyjIFace shortMessageMgrZyj ;
	private AdminMgrIFace admin;
 	private AndroidPushMgrIfaceZr androidPushMgrZr  ;
	private PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq ;
 	private String  downFileUrlNoSession  ;
	
	
	public void setFloorScheduleMgrZr(FloorScheduleMgrZr floorScheduleMgrZr) {
		this.floorScheduleMgrZr = floorScheduleMgrZr;
	}
	
	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}
	
	public void setAdmin(AdminMgrIFace admin) {
		this.admin = admin;
	}
	
	private  AdminLoginBean convertObjectToAdminLoginBean(Object obj){
		Map<String,Object> linkedMap = (Map<String,Object>)obj;
		AdminLoginBean loginBean = new AdminLoginBean();
		loginBean.setAccount(linkedMap.get("account").toString());
		//{account=admin, adid=100198, adgid=10000, loginDate=2014-11-10 09:29:31, province=0, city=0, ps_id=1000005, employe_name=系统管理, email=110106315@qq.com, attach=null, titles=[{TITLE_ID=251, TITLE_NAME=AMTRAN01}], login=true, loginRightPath=true}
		////system.out.println(linkedMap.get("adid").toString() + "adid ...");
 		loginBean.setAdid((Integer)linkedMap.get("adid"));
 		loginBean.setAdgid((Integer)linkedMap.get("adgid"));
 		loginBean.setEmploye_name((String)linkedMap.get("employe_name"));
 		loginBean.setLoginDate((String)linkedMap.get("loginDate"));
 		loginBean.setPs_id((Integer)linkedMap.get("ps_id"));
 		return loginBean ;
	}
	
 
	
	/**
	 * 短信关联的是任务
	 * 
	 * 任务关联的是 具体的业务模块
	 */
	@Override
	public void handleScheduleAddShortMessage(DBRow[] executeUsers,
 			long adid , DBRow schedule) throws Exception {
		try{ 
			StringBuffer accounts = new StringBuffer();
			if(executeUsers != null && executeUsers.length > 0){
				for(DBRow temp :  executeUsers){
					accounts.append(",").append(temp.getString("account"));
				}
			}
			if(accounts.length() > 0){
				AndroidPushMessge androidPushMessage =  AndroidPushMessge.getPushMessage(schedule);
				pushMessageByOpenfireMgrZzq.sysSendNotice(accounts.toString().substring(1), androidPushMessage);
			}
		 /*先注释掉
		  * 	AndroidPushMessge androidPushMessage =  AndroidPushMessge.getPushMessage(schedule);
			
			androidPushMgrZr.pushMessaeToDevice(executeUsers, androidPushMessage, adid); */
			 /*//receiver_phone
			 DBRow[] receivers = null ;
			 if(executeUsers != null && executeUsers.length > 0 ){
				 receivers = new DBRow[executeUsers.length];
				 for(int index = 0 , count = executeUsers.length ; index < count ; index++ ){
					 DBRow tempReceiver = new DBRow();
					 tempReceiver.add("receiver_id", executeUsers[index].getString("adid"));
					 tempReceiver.add("receiver_name", executeUsers[index].getString("employe_name"));
					 tempReceiver.add("receiver_phone", executeUsers[index].getString("mobilePhone"));
					 receivers[index] = tempReceiver;
				 }
			 }
			
			 shortMessageMgrZyj.addShortMessage(associate_type, associate_id, context, receivers, adid);*/
		 
		}catch (Exception e) {
			throw new SystemException(e,"handleScheduleAddShortMessage",log);
		}
		
	}

	@Override
	public void handleScheduleSendEmail(DBRow[] executeUsers, String emailTitle,
			String context, DBRow[] assignUserRow,
			DBRow[] scheduleJoinExecuteRow) throws Exception {
		try{
			 ModuleKey moduleKey = new ModuleKey();  
		 	// executeUsers 邮件的接受人
		 	//scheduleJoinExecuteRow 为抄送的人  
		    User user = new User("vvmecom@126.com","vvmecom+","smtp.126.com",25,"vvmecom@126.com",MimeUtility.encodeText(assignUserRow[0].getString("employe_name"), "UTF-8", "B")  ,assignUserRow[0].getString("email"));
		   
		    MailAddress mailAddress = null ;
		    if(scheduleJoinExecuteRow != null && scheduleJoinExecuteRow.length > 0){
			    mailAddress = new MailAddress(getEmailAddressFromDBRowArray(executeUsers),getEmailAddressFromDBRowArray(scheduleJoinExecuteRow));
		    }else{
		    	mailAddress = new MailAddress(getEmailAddressFromDBRowArray(executeUsers));
		    }
		    if(executeUsers == null || (executeUsers != null && executeUsers.length < 1)){return ;}
		    MailBody mailBody = new MailBody(emailTitle.toString(),StringUtil.ascii2Html(context.toString()),true,null);
		    Mail mail = new Mail(user);
		    mail.setAddress(mailAddress);
		    mail.setMailBody(mailBody);
		    mail.send();
		}catch (Exception e) {
			 throw new SystemException(e,"handleScheduleSendEmail",log);
		}
		
	}
	private String getEmailAddressFromDBRowArray(DBRow[] arrays) throws Exception {
		try{
			 StringBuffer emailSendTo = new StringBuffer("");
			 	if(arrays != null && arrays.length > 0){
			 		for(int index = 0 , count = arrays.length ; index < count ; index++ ){
			 			DBRow temp = arrays[index];
			 			if(temp != null && temp.getString("email").length() > 0){
			 				emailSendTo.append(",").append(temp.getString("email"));
			 			}
			 		}
			 	}
			 	return emailSendTo.length() > 1 ? emailSendTo.substring(1):"";
		}catch (Exception e) {
			 throw new SystemException(e,"getEmailAddressFromDBRowArray",log);
		}
	}
	@Override
	public void handleSheduleNotifify(DBRow shedule, String emailTitle) throws Exception {
		try{
			 TDate date = new TDate();
		 
			 boolean isNeedShortMessage = shedule.get("sms_short_notify", 0) == 1 ? true:false ;
			 boolean isNeedEmail = shedule.get("sms_email_notify", 0) == 1 ? true:false ;
			 boolean isNeedDwr = shedule.get("is_need_replay", 0) == 1 ? true:false ;
			 boolean isArrange = shedule.get("is_schedule", 0) == 1 ? true:false ;
			 
			 String assign_user_id  = shedule.getString("assign_user_id");
			 String execute_user_id  = shedule.getString("execute_user_id");
			 String schedule_join_execute_id  = shedule.getString("schedule_join_execute_id");
			 int associate_type = shedule.get("associate_type", 0);
			 long associate_id = shedule.get("associate_id", 0l);
			 int associate_process = shedule.get("associate_process", 0);		// 那个业务流程
			 
			 long associate_main_id = shedule.get("associate_main_id", 0l);
			 
			 DBRow[] assignUsersRow = floorScheduleMgrZr.getAdminUserByAdid(assign_user_id+"");
			 DBRow[] executeUsersRow =  floorScheduleMgrZr.getAdminUserByAdid(execute_user_id);
			 DBRow[] joinUsersRow =  floorScheduleMgrZr.getAdminUserByAdid(schedule_join_execute_id);
			 // 如果是没有安排的就是叫预设
			 // 	1.有关联类型的那么就是要走关联类型的那套
			 //		2.没有的就是走任务那套的内容
			 StringBuffer context = new StringBuffer();
			 StringBuffer executeNames = new StringBuffer("");
			 if(executeUsersRow != null && executeUsersRow.length > 0){
				 for(int index = 0 ,count = executeUsersRow.length ; index < count ;index++ ){
					 DBRow userTemp = executeUsersRow[index];
					 executeNames.append(",").append(userTemp.getString("employe_name"));
				 }
			 }
			 	 StringBuffer joinNames = new StringBuffer("");
				 if(joinUsersRow != null && joinUsersRow.length > 0){
					 for(int index = 0 , count = joinUsersRow.length ; index < count ; index++ ){
						 DBRow userTemp = joinUsersRow[index];
						 joinNames.append(",").append(userTemp.getString("employe_name"));
					 }
				 }
				// context.append(assignUsersRow[0].getString("employe_name")).append("将");
				// context.append(emailTitle);
				// context.append("分配给");
//				 String endTime = date.getFormateTime(shedule.getString("end_time"), "MM-dd HH:mm");
//				 if(executeNames.length() > 1){
//					 context.append(executeNames.toString().substring(1));
//					 if(0 == endTime.trim().length())
//					 {
//						 context.append("\n");
//					 }
//				 }
//				 if(endTime.trim().length() > 0){
//					 context.append("请于").append(endTime).append("前完成\n");
//				 } 
				 context.append(shedule.getString("schedule_detail")+"\n");
//				 if(joinNames.length() > 1){
//					 context.append("参与人:").append(joinNames.toString().substring(1));
//				 }
				 if(isNeedShortMessage){
					 handleScheduleAddShortMessage(executeUsersRow, Long.parseLong(assign_user_id) , shedule );}
				 if(isNeedEmail){
					 handleScheduleSendEmail(executeUsersRow,emailTitle, context.toString(),assignUsersRow,joinUsersRow);}
				 if(isNeedDwr){
					 handleDwrSend(executeUsersRow);
				
				 
				 }
		}catch (Exception e) {
			throw new SystemException(e,"handleSheduleNotifify",log);
		}
		
	}


	/**
	 * 任务页面提醒。在页面的左下角去调用一个javascript方法.这个方法会弹开一个页面.页面会加载最新没有完成的任务
	 * (包括没有安排的任务) 
	 * 	用户登录成功过后。会在dwr的session中写入一个值 
	 *  然后在推送的时候根据executeUserRow中的值比较如果有的话。那么就应该执行一个推送
	 *  单纯的页面提醒。所以不影响事务回滚
	 */
	@Override
	public void handleSheduleNotifify(DBRow shedule, String emailTitle,
			long adid) throws Exception {
		try{
			 TDate date = new TDate();
			 boolean isNeedShortMessage = shedule.get("sms_short_notify", 0) == 1 ? true:false ;
			 boolean isNeedEmail = shedule.get("sms_email_notify", 0) == 1 ? true:false ;
			 boolean isNeedDwr = shedule.get("is_need_replay", 0) == 1 ? true:false ;
			 boolean isArrange = shedule.get("is_schedule", 0) == 1 ? true:false ;
			 String assign_user_id  = shedule.getString("assign_user_id");
			 String execute_user_id  = shedule.getString("execute_user_id");
			 String schedule_join_execute_id  = shedule.getString("schedule_join_execute_id");
			 int associate_type = shedule.get("associate_type", 0);
			 long associate_id = shedule.get("associate_id", 0l);
			 int associate_process = shedule.get("associate_process", 0);		// 那个业务流程
			 long associate_main_id = shedule.get("associate_main_id", 0l);

			 DBRow[] assignUsersRow = floorScheduleMgrZr.getAdminUserByAdid(assign_user_id+"");
			 DBRow[] executeUsersRow =  floorScheduleMgrZr.getAdminUserByAdid(execute_user_id);
			 DBRow[] joinUsersRow =  floorScheduleMgrZr.getAdminUserByAdid(schedule_join_execute_id);
			 // 如果是没有安排的就是叫预设
			 // 	1.有关联类型的那么就是要走关联类型的那套
			 //		2.没有的就是走任务那套的内容
			 StringBuffer context = new StringBuffer();
			 StringBuffer executeNames = new StringBuffer("");
			 if(executeUsersRow != null && executeUsersRow.length > 0){
				 for(int index = 0 ,count = executeUsersRow.length ; index < count ;index++ ){
					 DBRow userTemp = executeUsersRow[index];
					 executeNames.append(",").append(userTemp.getString("employe_name"));
				 }
			 }
			 	 StringBuffer joinNames = new StringBuffer("");
				 if(joinUsersRow != null && joinUsersRow.length > 0){
					 for(int index = 0 , count = joinUsersRow.length ; index < count ; index++ ){
						 DBRow userTemp = joinUsersRow[index];
						 joinNames.append(",").append(userTemp.getString("employe_name"));
					 }
				 }
//				 context.append(assignUsersRow[0].getString("employe_name")).append("将");
//				 context.append(emailTitle);
//				 context.append("分配给");
//				 String endTime = date.getFormateTime(shedule.getString("end_time"), "MM-dd HH:mm");
//				 if(executeNames.length() > 1){
//					 context.append(executeNames.toString().substring(1));
//					 if(0 == endTime.trim().length())
//					 {
//						 context.append("\n");
//					 }
//				 }
//				 if(endTime.trim().length() > 0){
//					 context.append("请于").append(endTime).append("前完成\n");
//				 } 
				 context.append(shedule.getString("schedule_detail")+"\n");
//				 if(joinNames.length() > 1){
//					 context.append("参与人:").append(joinNames.toString().substring(1));
//				 }
				 if(isNeedShortMessage){handleScheduleAddShortMessage(executeUsersRow, adid, shedule);}
				 if(isNeedEmail){handleScheduleSendEmail(executeUsersRow,emailTitle, context.toString(),assignUsersRow,joinUsersRow);}
				 if(isNeedDwr){handleDwrSend(executeUsersRow);}
		}catch (Exception e) {
			throw new SystemException(e,"handleSheduleNotifify",log);
		}
		
	}
	
	
	 
	
	/**
	 * 任务页面提醒。在页面的左下角去调用一个javascript方法.这个方法会弹开一个页面.页面会加载最新没有完成的任务
	 * (包括没有安排的任务) 
	 * 	用户登录成功过后。会在dwr的session中写入一个值 
	 *  然后在推送的时候根据executeUserRow中的值比较如果有的话。那么就应该执行一个推送
	 *  单纯的页面提醒。所以不影响事务回滚
	 */
	@Override
	public void handleDwrSend(DBRow[] executeUsersRow) throws Exception {
		try
		{
			String page = ConfigBean.getStringValue("systenFolder")+"administrator/top.html";
			if(executeUsersRow != null && executeUsersRow.length > 0 ){
				Browser.withPage(page, new SysNotifyPageTask(executeUsersRow));
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleDwrSend",log);
		}finally{}
	}
	/**
	 * 页面提醒文字。文字会依次的显示在页面的右上角.executeUsersRow需要提醒的userDBRows.
	 * content 是提醒的内容。可以是html格式的
	 * 单纯的页面提醒。所以不影响事务回滚
	 */
	@Override
	public void hanleDwrSendAlertShow(DBRow[] executeUsersRow, String content)
			throws Exception
	{
		try
		{
			String page = ConfigBean.getStringValue("systenFolder")+"administrator/top.html";
			if(executeUsersRow != null && executeUsersRow.length > 0 ){
				Browser.withPage(page, new SysNotifyAlertShowTask(executeUsersRow,content));
			}
		}catch (Exception e) {
			throw new SystemException(e,"hanleDwrSendAlertShow",log);
		}finally{}
		
	}
	@Override
	public void setSysNofifySessionFlag(String adid) throws Exception
	{
		WebContextFactory.get().getScriptSession().setAttribute("adid", adid); 
	}
	/**
	 * 跟进的时候给创建人发送信息
	 */
	@Override
	public void handleScheduleReplay(long schedule_id, String title,
			String context,HttpServletRequest request) throws Exception {
		try{
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(schedule_id);
			boolean flag = scheduleRow.getString("is_need_replay").equals("1") ?true:false; 
			if(flag){
 				 long associate_main_id = scheduleRow.get("associate_main_id", 0l);
				 List<Long> notifyUsers = new ArrayList<Long>();
					//表示的是需要回复的
				 boolean isNeedShortNotify = scheduleRow.get("sms_short_notify",0) == 1 ? true:false;
				 boolean isNeedEmailNotify = scheduleRow.get("sms_email_notify",0) == 1 ? true:false;
				 boolean isNeedDwrNotify = scheduleRow.get("schedule_is_note",0) == 1 ? true:false;
				 //得到任务的安排人
				 long adid = scheduleRow.get("assign_user_id", 0l);
				 notifyUsers.add(adid);
				 // 获取当前登录人
				 AdminLoginBean adminLoggerBean = admin.getAdminLoginBean(request.getSession()); 
				 long loginAdid = adminLoggerBean.getAdid();
				 StringBuffer sb = new StringBuffer();
				 sb.append(adminLoggerBean.getEmploye_name()).append("跟进了").append(":\n");
				 sb.append(context);
				 context = sb.toString();
				 
				 String isNeedNotifyExecuter = StringUtil.getString(request, "is_need_notify_executer");
			     
			     if(isNeedNotifyExecuter.equals("true")){
				     String executeIds = floorScheduleMgrZr.getScheduleSubIds(schedule_id);
				     if(executeIds.length() > 0 ){
					      String[]  arrayId = executeIds.split(",");
					      for(String s :  arrayId){
						       long tempLong = Long.parseLong(s);
						       if(tempLong != 0l){
						    	   notifyUsers.add(tempLong);
						       }
					      }
				     }
			     }
			     
			     DBRow[] notifyUsersRow = new DBRow[notifyUsers.size()];
			     for(int index = 0 , count = notifyUsers.size() ; index <count ; index++ ){
			      notifyUsersRow[index] = admin.getDetailAdmin(notifyUsers.get(index));
			      
			     }
			     
				 // context   谁跟进了Title:+内容
				 String titleAClick = "<a href='javascript:void(0)' onclick='window.top.sheduleAClick("+schedule_id+","+scheduleRow.get("is_schedule", 0)+")'>"+title+"</a>" ;
				 
				 StringBuffer pageAlertContext = new StringBuffer("你好,<span style='color:#f60;'>"+adminLoggerBean.getEmploye_name()+"</span>");
				 pageAlertContext.append("跟进了<br />").append(titleAClick+"任务").append(".").append(context);
		 
				 if(isNeedShortNotify){
					 this.handleScheduleAddShortMessage(notifyUsersRow, loginAdid ,scheduleRow);
				 }
				 if(isNeedEmailNotify){
					 
					 this.handleScheduleSendEmail(notifyUsersRow, title, context, new DBRow[]{admin.getDetailAdmin(loginAdid)}, null);
				 }
				 if(isNeedDwrNotify){
					 this.hanleDwrSendAlertShow(notifyUsersRow,pageAlertContext.toString());
				 }
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleScheduleReplay",log);
		}
	}
	
	/**
	 * 跟进的时候给创建人发送信息
	 */
	@Override
	public void handleScheduleReplay(long schedule_id, String title,
			String context,AdminLoginBean adminLoggerBean, String isNeedNotifyExecuter) throws Exception {
		try{
			DBRow scheduleRow = floorScheduleMgrZr.getScheduleById(schedule_id);
			boolean flag = scheduleRow.getString("is_need_replay").equals("1") ?true:false; 
			if(flag){
				long associate_main_id = scheduleRow.get("associate_main_id", 0l);
				List<Long> notifyUsers = new ArrayList<Long>();
					//表示的是需要回复的
				 boolean isNeedShortNotify = scheduleRow.get("sms_short_notify",0) == 1 ? true:false;
				 boolean isNeedEmailNotify = scheduleRow.get("sms_email_notify",0) == 1 ? true:false;
				 boolean isNeedDwrNotify = scheduleRow.get("schedule_is_note",0) == 1 ? true:false;
				 //得到任务的安排人
				 long adid = scheduleRow.get("assign_user_id", 0l);
				 notifyUsers.add(adid);
				 // 获取当前登录人
				 long loginAdid = adminLoggerBean.getAdid();
				 StringBuffer sb = new StringBuffer();
				 sb.append(adminLoggerBean.getEmploye_name()).append("跟进了").append(":\n");
				 sb.append(context);
				 context = sb.toString();
				 
				 if(isNeedNotifyExecuter.equals("true")){
				     String executeIds = floorScheduleMgrZr.getScheduleSubIds(schedule_id);
				     if(executeIds.length() > 0 ){
					      String[]  arrayId = executeIds.split(",");
					      for(String s :  arrayId){
						       long tempLong = Long.parseLong(s);
						       if(tempLong != 0l){
						    	   notifyUsers.add(tempLong);
						       }
					      }
				     }
			     }
			     
			     DBRow[] notifyUsersRow = new DBRow[notifyUsers.size()];
			     for(int index = 0 , count = notifyUsers.size() ; index <count ; index++ ){
			      notifyUsersRow[index] = admin.getDetailAdmin(notifyUsers.get(index));
			      
			     }
				 // context   谁跟进了Title:+内容
				 String titleAClick = "<a href='javascript:void(0)' onclick='window.top.sheduleAClick("+schedule_id+","+scheduleRow.get("is_schedule", 0)+")'>"+title+"</a>" ;
				 
				 StringBuffer pageAlertContext = new StringBuffer("你好,<span style='color:#f60;'>"+adminLoggerBean.getEmploye_name()+"</span>");
				 pageAlertContext.append("跟进了<br />").append(titleAClick+"任务").append(".").append(context);
		 
				 if(isNeedShortNotify){
					 this.handleScheduleAddShortMessage(notifyUsersRow,  adid ,scheduleRow);
				 }
				 if(isNeedEmailNotify){
					 
					 this.handleScheduleSendEmail(notifyUsersRow, title, context, new DBRow[]{admin.getDetailAdmin(loginAdid)}, null);
				 }
				 if(isNeedDwrNotify){
					 this.hanleDwrSendAlertShow(notifyUsersRow,pageAlertContext.toString());
				 }
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleScheduleReplay",log);
		}
	}
	

	
	@Override
	public void handleScheduleCancle(String usersId, DBRow shedule ,AdminLoginBean adminLoggerBean)
			throws Exception {
		try{
			String is_need_replay = shedule.getString("is_need_replay");
			if(usersId.length() > 0){
				 boolean isNeedShortNotify = shedule.get("sms_short_notify",0) == 1 ? true:false;
				 boolean isNeedEmailNotify = shedule.get("sms_email_notify",0) == 1 ? true:false;
				 boolean isNeedDwrNotify = shedule.get("schedule_is_note",0) == 1 ? true:false;
				 long schedule_id = shedule.get("schedule_id", 0l);
				 long associate_main_id = shedule.get("associate_main_id", 0l);
				 
				 String[] arrayId = usersId.split(",");
				 long loginAdid = adminLoggerBean.getAdid();
				 if(arrayId != null && arrayId.length > 0){
					 for(int index = 0 , count = arrayId.length ; index < count ; index++ ){
						 String idStrTemp = arrayId[index];
						 if(idStrTemp.trim().length() > 0){
							 long idLong = Long.parseLong(idStrTemp.trim());
							 if(idLong > 0l ){
								//组织邮件内容
								 StringBuffer context = new StringBuffer("你好,"+adminLoggerBean.getEmploye_name()+"取消了你的").append(shedule.getString("schedule_overview")).append("任务\n") ;
								 context.append(shedule.getString("schedule_detail"));
								 StringBuffer pageAlertContext = new StringBuffer("你好,<span style='color:#f60;'>"+adminLoggerBean.getEmploye_name()+"</span>取消了你的").append("<a href='javascript:void(0)'>"+shedule.getString("schedule_overview")+"</a>").append("任务.\n");
								 if(isNeedShortNotify){
									 this.handleScheduleAddShortMessage(new DBRow[]{admin.getDetailAdmin(idLong)},adminLoggerBean.getAdid(),shedule);
								 }
								 if(isNeedEmailNotify){
									 this.handleScheduleSendEmail(new DBRow[]{admin.getDetailAdmin(idLong)}, shedule.getString("schedule_overview"), context.toString(), new DBRow[]{admin.getDetailAdmin(loginAdid)}, null);
								 }
								 if(isNeedDwrNotify){
									 this.hanleDwrSendAlertShow(new DBRow[]{admin.getDetailAdmin(idLong)},pageAlertContext.toString());
								 }
							 }
						 }
					 }
				 }
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleScheduleCancle",log);
		}
	}
	@Override
	public void handleUpdateScheduleSendEmail(String usersId, DBRow shedule,
			HttpServletRequest request) throws Exception {

		try{
			String is_need_replay = shedule.getString("is_need_replay");
			if(usersId.length() > 0){
				 long associate_main_id = shedule.get("associate_main_id", 0l);
				 boolean isNeedShortNotify = shedule.get("sms_short_notify",0) == 1 ? true:false;
				 boolean isNeedEmailNotify = shedule.get("sms_email_notify",0) == 1 ? true:false;
				 boolean isNeedDwrNotify = shedule.get("schedule_is_note",0) == 1 ? true:false;
				 long schedule_id = shedule.get("schedule_id", 0l);
				 long adid = shedule.get("assign_user_id", 0l);
				 String[] arrayId = usersId.split(",");
				 AdminLoginBean adminLoggerBean = admin.getAdminLoginBean(request.getSession()); 
				 long loginAdid = adminLoggerBean.getAdid();
				 if(arrayId != null && arrayId.length > 0){
					 for(int index = 0 , count = arrayId.length ; index < count ; index++ ){
						 String idStrTemp = arrayId[index];
						 if(idStrTemp.trim().length() > 0){
							 long idLong = Long.parseLong(idStrTemp.trim());
							 if(idLong > 0l ){
								//组织邮件内容
								 StringBuffer context = new StringBuffer("你好,"+adminLoggerBean.getEmploye_name()+"修改了你的").append(shedule.getString("schedule_overview")).append("任务\n") ;
								 StringBuffer pageAlertContext = new StringBuffer("你好,<span style='color:#f60;'>"+adminLoggerBean.getEmploye_name()+"</span>&nbsp;修改了你的 <br />");
								 
								 pageAlertContext.append("<a href='javascript:void(0)' onclick='window.top.sheduleAClick("+schedule_id+","+shedule.get("is_schedule", 0)+")' >").append(shedule.getString("schedule_overview")).append("</a>").append("任务.\n");
 								 context.append(shedule.getString("schedule_detail"));
								 if(isNeedShortNotify){
									 this.handleScheduleAddShortMessage(new DBRow[]{admin.getDetailAdmin(idLong)}, loginAdid, shedule);
								 }
								 if(isNeedEmailNotify){
									 this.handleScheduleSendEmail(new DBRow[]{admin.getDetailAdmin(idLong)}, shedule.getString("schedule_overview"), context.toString(), new DBRow[]{admin.getDetailAdmin(loginAdid)}, null);
								 }
								 if(isNeedDwrNotify){
									 this.hanleDwrSendAlertShow(new DBRow[]{admin.getDetailAdmin(idLong)},pageAlertContext.toString());
								 }
							 }
						 }
					 }
					 
					 
				 }
				 
				
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleUpdateScheduleSendEmail",log);
		}
	}

	
	@Override
	public void handleUpdateScheduleSendEmail(String usersId, DBRow shedule,
			AdminLoginBean adminLoggerBean) throws Exception {
		try{
			String is_need_replay = shedule.getString("is_need_replay");
			if(usersId.length() > 0){
				 long associate_main_id = shedule.get("associate_main_id", 0l);
				 boolean isNeedShortNotify = shedule.get("sms_short_notify",0) == 1 ? true:false;
				 boolean isNeedEmailNotify = shedule.get("sms_email_notify",0) == 1 ? true:false;
				 boolean isNeedDwrNotify = shedule.get("schedule_is_note",0) == 1 ? true:false;
				 long schedule_id = shedule.get("schedule_id", 0l);
				 String[] arrayId = usersId.split(",");
				 long loginAdid = adminLoggerBean.getAdid();
				 if(arrayId != null && arrayId.length > 0){
					 for(int index = 0 , count = arrayId.length ; index < count ; index++ ){
						 String idStrTemp = arrayId[index];
						 if(idStrTemp.trim().length() > 0){
							 long idLong = Long.parseLong(idStrTemp.trim());
							 if(idLong > 0l ){
								//组织邮件内容
								 StringBuffer context = new StringBuffer("你好,"+adminLoggerBean.getEmploye_name()+"修改了你的").append(shedule.getString("schedule_overview")).append("任务\n") ;
								 StringBuffer pageAlertContext = new StringBuffer("你好,<span style='color:#f60;'>"+adminLoggerBean.getEmploye_name()+"</span>&nbsp;修改了你的 <br />");
								 pageAlertContext.append("<a href='javascript:void(0)' onclick='window.top.sheduleAClick("+schedule_id+","+shedule.get("is_schedule", 0)+")' >").append(shedule.getString("schedule_overview")).append("</a>").append("任务.\n");
 								 context.append(shedule.getString("schedule_detail"));
								 if(isNeedShortNotify){
									 this.handleScheduleAddShortMessage(new DBRow[]{admin.getDetailAdmin(idLong)}, loginAdid,shedule);
								 }
								 if(isNeedEmailNotify){
									 this.handleScheduleSendEmail(new DBRow[]{admin.getDetailAdmin(idLong)}, shedule.getString("schedule_overview"), context.toString(), new DBRow[]{admin.getDetailAdmin(loginAdid)}, null);
								 }
								 if(isNeedDwrNotify){
									 this.hanleDwrSendAlertShow(new DBRow[]{admin.getDetailAdmin(idLong)},pageAlertContext.toString());
								 }
							 }
						 }
					 }
				 }
	 
			}
		}catch (Exception e) {
			throw new SystemException(e,"handleUpdateScheduleSendEmail",log);
		}
	}
	
	/**
	 *  给的是FileId 
	 *  在这里获取下来保存到一个地方
 	 */
	private Attach[] getEmailAttach(String  fileIds) throws ParameterException , Exception{
		if(fileIds != null && fileIds.length() > 0 ){
			String[] filePaths = fileIds.split(",");
	 		Attach[] returnValue = new Attach[filePaths.length];
	 		for(int index = 0 , count = returnValue.length  ; index < count ; index++ ){
	 			String fileUrl = downFileUrlNoSession  + filePaths[index];
	 			
	 			FileMgrIfaceZr fileMgrZr = (FileMgrIfaceZr) MvcUtil.getBeanFromContainer("proxyFileMgrZr");
				DBRow row = fileMgrZr.getFileByFileId(Long.parseLong(filePaths[index]));
				if(row == null){
					throw new ParameterException("File Not Found.");
				}
				String destination =  Environment.getHome() + "upl_excel_tmp/" + row.getString("file_name");
	 			FileUtils.copyURLToFile(new URL(fileUrl) , new File(destination));
	 			
	 			returnValue[index] = new Attach(destination);
	 		}
			return returnValue ;
		}
		return null ;
	}
	@Override
	public void handleSystemNotify(DBRow data) throws ParameterException ,  Exception {
		try{
			long sender = data.get("sender", 0l);
			String receivers =  data.getString("receivers");
			String cc =  data.getString("ccreceivers");
			String subtitle = data.getString("subtitle");
			boolean isNeedEmail =  data.get("needemail", 0) == 1;
			String fileids  =  data.getString("fileids");
			String emailContent = data.getString("emailcontent");
 			validateSystemNotifyParams(data);
 			
 			DBRow rowsender = (DBRow)data.get("rowsender", new DBRow());
			if(isNeedEmail){
			    User user = new User("vvmecom@126.com","vvmecom+","smtp.126.com",25,"vvmecom@126.com", MimeUtility.encodeText(rowsender.getString("employe_name"), "UTF-8", "B") , rowsender.getString("email"));
			    MailAddress mailAddress = new MailAddress(receivers,cc);
			    MailBody mailBody = new MailBody(subtitle.toString(),emailContent,true, getEmailAttach(fileids));
			    Mail mail = new Mail(user);
			    mail.setAddress(mailAddress);
			    mail.setMailBody(mailBody);
			    mail.send();
			}
			
		}catch(ParameterException e){
			throw  e ;
		}catch(Exception e){
			throw new SystemException(e,"handleSystemNotify",log);
		}
	}
 
	 
	
	public static String SenderInvalidate = "Sender [%s] Invalidate ";
	public static String ReceiverInvalidate = "Receiver [%s] Invalidate";
	public static String ReceiverEmailAddressInvalidate = "Receiver [%s] Email [%s]  Invalidate ";
	public static String CCReiverInvalidate = "CC Receiver [%s] Email [%s]  Invalidate ";
	public static String TitleInvalidate = "Title Invalidate ";

	
	/**
	 * 
	* @Title: validateSystemNotifyParams
	* @Description: TODO(检查参数是否都是合法的，如果有需要发送邮件，那么检查邮件的邮箱地址)
	* @param @param data
	* @param @throws ParameterException
	* @param @throws Exception    设定文件
	* @return void    返回类型
	* @throws
	 */
	private void validateSystemNotifyParams(DBRow data) throws  ParameterException , Exception {
		long sender = data.get("sender", 0l);
		String receivers =  data.getString("receivers");
		String cc =  data.getString("ccreceivers");
		String subtitle = data.getString("subtitle");
		boolean isNeedEmail = false ; 
		if(StringUtil.isBlank(subtitle)){
			throw new ParameterException(String.format(TitleInvalidate));
		}
		if(sender == 0l ){
			throw new ParameterException("Sender Is Null");
		}
		DBRow row = admin.getDetailAdmin(sender);
		if(row == null){
			throw new ParameterException(String.format(SenderInvalidate,sender));
		}
		data.add("rowsender", row);
		if(receivers == null || receivers.length() < 1){
			throw new ParameterException("Receiver Is Null");
		}
		if(isNeedEmail){
			String[]  receiversEmail = receivers.split(",");
			for(String email : receiversEmail){
 				if(!validationMailAddress(email)){
					throw new ParameterException(String.format(ReceiverEmailAddressInvalidate,email , email));
				}
			}
 			//检查CC的Email
			if(cc != null && cc.length() > 0){	
				String[]  ccsEmail = cc.split(",");
				for(String email : ccsEmail){
	 				if(!validationMailAddress(email)){
						throw new ParameterException(String.format(ReceiverEmailAddressInvalidate,email , email));
					}
				}
			}
		}
		
	}
	
	  public static boolean validationMailAddress(String emailAddress) {  
	    	//电子邮件  
    	 String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
    	 Pattern regex = Pattern.compile(check);  
    	 Matcher matcher = regex.matcher(emailAddress);
    	 boolean thisToMatched = matcher.matches();
    	 return thisToMatched ;
	  }
	public void setAndroidPushMgrZr(AndroidPushMgrIfaceZr androidPushMgrZr) {
		this.androidPushMgrZr = androidPushMgrZr;
	}
	public void setPushMessageByOpenfireMgrZzq(
			PushMessageByOpenfireMgrIfaceZzq pushMessageByOpenfireMgrZzq) {
		this.pushMessageByOpenfireMgrZzq = pushMessageByOpenfireMgrZzq;
	}

	public String getDownFileUrlNoSession() {
		return downFileUrlNoSession;
	}

	public void setDownFileUrlNoSession(String downFileUrlNoSession) {
		this.downFileUrlNoSession = downFileUrlNoSession;
	}

 
	
	
}
