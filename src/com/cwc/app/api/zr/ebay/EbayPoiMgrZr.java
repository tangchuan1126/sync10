package com.cwc.app.api.zr.ebay;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.cwc.app.iface.zr.ebay.EbayPoiIfaceZr;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.ebay.soap.eBLBaseComponents.AmountType;
import com.ebay.soap.eBLBaseComponents.BrandMPNType;
import com.ebay.soap.eBLBaseComponents.CalculatedShippingDiscountType;
import com.ebay.soap.eBLBaseComponents.CalculatedShippingRateType;
import com.ebay.soap.eBLBaseComponents.DiscountProfileType;
import com.ebay.soap.eBLBaseComponents.InternationalShippingServiceOptionsType;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityListType;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityType;
import com.ebay.soap.eBLBaseComponents.MeasureType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.NameValueListType;
import com.ebay.soap.eBLBaseComponents.ProductListingDetailsType;
import com.ebay.soap.eBLBaseComponents.SalesTaxType;
import com.ebay.soap.eBLBaseComponents.ShippingDetailsType;
import com.ebay.soap.eBLBaseComponents.ShippingPackageCodeType;
import com.ebay.soap.eBLBaseComponents.ShippingServiceOptionsType;
import com.ebay.soap.eBLBaseComponents.VariationType;
import com.ebay.soap.eBLBaseComponents.VariationsType;
 

public class EbayPoiMgrZr  implements  EbayPoiIfaceZr{
	
	public static Logger logSuccess = Logger.getLogger("ebay_success");
	public static Logger logError = Logger.getLogger("ebay_error");
	public static Logger log = Logger.getLogger("ACTION");
	
	
	
	@Override
	public int createNewSheetAndSheetTitle(Workbook wb , Sheet sheet , String[] tilteName ,Integer width) throws Exception{
		try{
			
			Row row = sheet.createRow(0);
			CellStyle style = wb.createCellStyle();
			Font font = wb.createFont();
			font.setFontHeightInPoints((short)9);
			 
			font.setFontName("华文中宋");
			 
			style.setAlignment(CellStyle.ALIGN_CENTER);
		    style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			style.setFont(font);
			style.setBorderBottom(CellStyle.BORDER_THIN);
		    style.setBottomBorderColor(IndexedColors.GREEN.getIndex());
		    style.setBorderLeft(CellStyle.BORDER_THIN);
		    style.setLeftBorderColor(IndexedColors.GREEN.getIndex());
		    style.setBorderRight(CellStyle.BORDER_THIN);
		    style.setRightBorderColor(IndexedColors.GREEN.getIndex());
		    style.setBorderTop(CellStyle.BORDER_MEDIUM_DASHED);
		    style.setTopBorderColor(IndexedColors.GREEN.getIndex());
		    style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    row.setHeightInPoints(25);
		    boolean isWidthNull = false;
		    if(width == null || width == 0){
		    	isWidthNull = true;
		    }
			for(int index = 0 , count = tilteName.length ; index < count ; index++ ){
				Cell temp = row.createCell(index);
				temp.setCellValue(tilteName[index]);
				temp.setCellStyle(style);
				sheet.setColumnWidth(index, isWidthNull?10000:width);
			}
			return tilteName.length;
		}catch (Exception e) {
			throw new SystemException(e,"createNewSheetAndSheetTitle",log);
		}
		
	}
	
	
	@Override
	public void setData(Sheet sheet , List<String[]> list , boolean isNeedSpcialColor, boolean isMerge,Workbook  wb){
		CellStyle one = null ;
		CellStyle two = null;
		int currentRowIndex = sheet.getLastRowNum();
		int oldRowIndex = currentRowIndex;
		DataFormat  format = wb.createDataFormat();
		
		for(int rowIndex = 0 , rowCount = list.size() ; rowIndex < rowCount ; rowIndex++ ){
			String[] temp = list.get(rowIndex);
			Row tempRow = sheet.createRow(++currentRowIndex);
		
			
			if(isNeedSpcialColor){
				 
				one = wb.createCellStyle();
				one.setDataFormat(format.getFormat("@"));
				one.setWrapText(true); 
				HSSFPalette palette1 = ((HSSFWorkbook) wb).getCustomPalette();
				palette1.setColorAtIndex(IndexedColors.WHITE.getIndex(), (byte)  255,(byte) 255, (byte) 102 );
		 		one.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		 		one.setFillPattern(CellStyle.SOLID_FOREGROUND);
		 		two = wb.createCellStyle();
				HSSFPalette palette2 = ((HSSFWorkbook) wb).getCustomPalette();
				palette2.setColorAtIndex(IndexedColors.YELLOW.getIndex(), (byte)  204,(byte) 255, (byte) 153 );
				two.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				two.setFillPattern(CellStyle.SOLID_FOREGROUND);
				
				two.setDataFormat(format.getFormat("@"));
				two.setWrapText(true); 
			}
			 
			for(int colIndex =  0 , colCount = temp.length ; colIndex <  colCount ; colIndex++ ){
				Cell tempCell = tempRow.createCell(colIndex);
				tempCell.setCellValue(temp[colIndex]);
				if(colIndex > 0 && isNeedSpcialColor){
					if(colIndex % 2 == 0){
						tempCell.setCellStyle(one);
					}else{
						tempCell.setCellStyle(two);
					}
					
				}
				
			}
		}
		if(isMerge){
			sheet.addMergedRegion(new CellRangeAddress(
					oldRowIndex+1, //first row (0-based)
					currentRowIndex, //last row  (0-based)
		            0, //first column (0-based)
		            0  //last column  (0-based)
		    ));
			sheet.addMergedRegion(new CellRangeAddress(
					oldRowIndex+1, //first row (0-based)
					currentRowIndex, //last row  (0-based)
		            1, //first column (0-based)
		            1  //last column  (0-based)
		    ));
			Cell cell = sheet.getRow(oldRowIndex+1).getCell(0);
			Cell cell1 = sheet.getRow(oldRowIndex+1).getCell(1);
			CellStyle cellStyle = wb.createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			cell.setCellStyle(cellStyle);
			cell1.setCellStyle(cellStyle);
		}
  	
	}
	@Override
	public void setDataString(Sheet sheet , List<String> list) throws Exception{
		try{
			int currentRowIndex = sheet.getLastRowNum();
			 
			Row tempRow = sheet.createRow(++currentRowIndex);
			for(int colIndex =  0 , colCount = list.size() ; colIndex <  colCount ; colIndex++ ){
				tempRow.createCell(colIndex).setCellValue(list.get(colIndex));
			}
		}catch (Exception e) {
			throw new SystemException(e,"setDataString",log);
		}
		
	}
 
	@Override
	public void createVariationsSetAndVariations(VariationsType variationsType , Workbook wb , String itemNumber ,String title , Sheet variationsSet , Sheet variations , boolean isCreateSheet) throws Exception{
		try{
			int totalNameValue = 0 ;	 
			if(variationsType != null){
				
				// variationSpecificsSet
				NameValueListArrayType variationSpecificsSet =  variationsType.getVariationSpecificsSet();
				NameValueListType[]  set = variationSpecificsSet.getNameValueList();
				totalNameValue = set.length ;
				if(set != null && set.length > 0){
				 
					List<String> titleNameList = new ArrayList<String>();
					List<String> value = new ArrayList<String>();
					value.add(itemNumber);
					titleNameList.add("Item Number");
					titleNameList.add("title");
 
					value.add(title);
					for(int index = 0 , count = set.length ; index < count ; index++  ){
						titleNameList.add("Name"+(index+1));
						titleNameList.add("Value"+(index+1));
					 
						
						String[] values = set[index].getValue();
						if(values != null){
							value.add(set[index].getName());
							StringBuffer sb = new StringBuffer();
							for(String s : values){
								sb.append("/,").append(s);
							}
							value.add( sb.length() > 0 ? sb.substring(2) :"");
						} 
						
			 
		 			}
					
					if(variationsSet.getRow(0) != null  ){
						if(variationsSet.getRow(0).getLastCellNum()  < titleNameList.size()){
							 createNewSheetAndSheetTitle(wb, variationsSet, titleNameList.toArray(new String[titleNameList.size()]), 10000);
						}
					}else{
						 createNewSheetAndSheetTitle(wb, variationsSet, titleNameList.toArray(new String[titleNameList.size()]), 10000);
					}
					
				 
			
					setDataString(variationsSet,value);
				}
				// set Data setDataString
				
				
				
				VariationType[] variationTypes = variationsType.getVariation();
				if(variationTypes != null && variationTypes.length > 0){
					//Set Variations title
					List<String> titleName = new ArrayList<String>();
					titleName.add("Item Number");
					titleName.add("title");
					titleName.add("SKU");
					titleName.add("currencyID");
					titleName.add("Price");
					titleName.add("Quantity");
					for(int index =0  ; index < totalNameValue ; index++ ){
						titleName.add("Name");
						titleName.add("Value");
					}
	 
						this.createNewSheetAndSheetTitle(wb, variations, titleName.toArray(new String[titleName.size()]), 7000);
				 
					// set Variations Data ;
					List<String[]> values = new ArrayList<String[]>();
					 
					for(int k = 0 , count = variationTypes.length ; k < count ; k++ ){
						List<String> tempArrayList = new ArrayList<String>();
						VariationType	temp = variationTypes[k];
						NameValueListArrayType  specifics = temp.getVariationSpecifics();
						tempArrayList.add(itemNumber) ;
						tempArrayList.add(title);
						tempArrayList.add(temp.getSKU()) ;
						tempArrayList.add(temp.getStartPrice().getCurrencyID()+"");
						tempArrayList.add(temp.getStartPrice().getValue()+"");
					 
						tempArrayList.add(temp.getQuantity()+"");
					 
							 
							if(specifics != null){
								NameValueListType[] nameValueListTypes = specifics.getNameValueList();
							
								if(nameValueListTypes != null && nameValueListTypes.length > 0){
									for(NameValueListType t : nameValueListTypes){		
										tempArrayList.add(t.getName() );
										tempArrayList.add(StringUtil.convertStringArrayToString(t.getValue()));
									}
								}
							}
							values.add(tempArrayList.toArray( new String[tempArrayList.size()]));
					}
				 
					 
					this.setData(variations, values,true,true,wb);
					
				}
			}
		}catch (Exception e) {
			throw new SystemException(e,"createVariationsSetAndVariations",log);
		}
		
		
		
		
		
	}
	
	
	//itemSpecifics
	@Override
	public void createItemSpecifics( NameValueListArrayType arrayType ,Workbook wb , Sheet sheet , String itemNumber , String title , boolean isCreateSheet) throws Exception{
		try{
			 
			//createNewSheetAndSheetTitle(wb, sheet, tilteName, widths)
			List<String[]> values = new ArrayList<String[]>();
			if(arrayType != null){
				NameValueListType[] type = arrayType.getNameValueList();
				List<String> tilteNameList = new ArrayList<String>();
				//得到Title
	 
				tilteNameList.add("Item Number");
				tilteNameList.add("title");
				List<String> value = new ArrayList<String>();
				List<Integer> widths = new ArrayList<Integer>();
				widths.add(7000);
				value.add(itemNumber);
				value.add(title);
				for(int j = 0 , count = type.length ; j < count ; j++){
					tilteNameList.add("Name" + (j+1));
					tilteNameList.add("Value" + (j+1));
					//tilteNameList.add("Source" + (j+1));
					value.add(type[j].getName());
					value.add(StringUtil.convertStringArrayToString(type[j].getValue()));
					//value.add(type[j].getSource().value());
					 
				}
		 
				values.add(value.toArray(new String[value.size()]));
				if(sheet.getRow(0) != null  ){
					if(sheet.getRow(0).getLastCellNum()  < tilteNameList.size()){
						createNewSheetAndSheetTitle(wb, sheet, tilteNameList.toArray(new String[tilteNameList.size()]), 5000);
					}
				}else{
					createNewSheetAndSheetTitle(wb, sheet, tilteNameList.toArray(new String[tilteNameList.size()]), 5000);
				}
				setData(sheet , values,true,false,wb);
			  
			} 
		}catch (Exception e) {
			throw new SystemException(e,"createItemSpecifics",log);
		}
		
	}
	
	
	/// 处理CompatibilityList
	@Override
	public void createNewSheetByItemCompatibilityListType(ItemCompatibilityListType itemCompatibilityListType , Workbook wb , Sheet sheet ,String itemNumber,String title  , boolean isCreateSheet) throws Exception{
		try{
	 
			if(itemCompatibilityListType != null){
				 
				ItemCompatibilityType[] itemCompatibilityType =  itemCompatibilityListType.getCompatibility();
				if(itemCompatibilityType != null && itemCompatibilityType.length > 0){
					List<String[]> values = new ArrayList<String[]>();
					int nameCellToatal = 3; 
					
					List<String> tilteName = new ArrayList<String>();
					for(ItemCompatibilityType t : itemCompatibilityType){
						List<String> value = new ArrayList<String>();
						int maxCell = 3;
						
						 String compatibilityNotes = t.getCompatibilityNotes() == null ? "" : t.getCompatibilityNotes();
						 value.add(itemNumber);
						 value.add(title);
						 value.add(compatibilityNotes);
						 NameValueListType[]  nameValueListType = t.getNameValueList();
						 if(nameValueListType != null && nameValueListType.length > 0){
							 // 取出一行数据来 设置title
							
							
							for(NameValueListType nameValueType : nameValueListType){
								
									String name = nameValueType.getName();
									if(name != null && name.length() > 0){
										maxCell += 2;
										value.add(name);
										value.add(StringUtil.convertStringArrayToString(nameValueType.getValue()));
									}
									
								 
	 						}
							 if(maxCell >  nameCellToatal){
								 nameCellToatal = maxCell;
								 
							 }
						 }
						 values.add(value.toArray(new String[value.size()]));  
					}
					tilteName.add("ItemNumber");
					tilteName.add("title");
					tilteName.add("compatibilityNotes");
					for(int index = 0 , count = (nameCellToatal - 3) /2 ; index < count ; index++  ){
						tilteName.add("Name");
						tilteName.add("Value");
					}
					createNewSheetAndSheetTitle(wb,sheet,tilteName.toArray(new String[tilteName.size()]),null);
					this.setData(sheet, values,false,true,wb);
				}
			}
		}catch (Exception e) {
			throw new SystemException(e,"createNewSheetByItemCompatibilityListType",log);
		}
		
	}


	@Override
	public String[] readXLSItemNumbers(String filePath) throws Exception {
		try{
			List<String> itemNumbers = new ArrayList<String>();
			FileInputStream is = new FileInputStream(filePath);
			HSSFWorkbook wb = new HSSFWorkbook(is);
			Sheet sheet =	wb.getSheet("BestMatchItem");
			if(sheet != null){
				for(int index = 2 , end = sheet.getLastRowNum() ; index <= end ; index++ ){
					Row temp = sheet.getRow(index);
					Cell cell = temp.getCell(0);
	 
					itemNumbers.add(cell.getRichStringCellValue().toString());
				}
			}
			return itemNumbers.toArray(new String[itemNumbers.size()]);
		}catch (Exception e) {
			throw new SystemException(e,"readXLSItemNumbers",log);
		}
	
	}


	@Override
	public void createShipping(Workbook wb, String itemNumber, String title ,Sheet sheet,
			ShippingDetailsType shippingDetails) throws Exception {
		try{
			

			
			List<String> tilteNameList = new ArrayList<String>(); 
			tilteNameList.add("ItemNumber");
			tilteNameList.add("title");
			int totalTilte = 0;
			List<String> value = new ArrayList<String>();
			value.add(itemNumber);
			value.add(title);
			//getInternationalShippingServiceOption
			//getCalculatedShippingDiscount
			//getCalculatedShippingRate
			//getShippingServiceOptions
			//getInternationalShippingServiceOption
			ShippingServiceOptionsType[] shippingServiceOptionsType = shippingDetails.getShippingServiceOptions();
			if(shippingServiceOptionsType != null && shippingServiceOptionsType.length > 0){
				for(int index = 0 , count = shippingServiceOptionsType.length ; index < count ; index++ ){
				 
					value.add("ShippingServiceOptionsType.ShippingService" + (index+1));
					ShippingServiceOptionsType type = shippingServiceOptionsType[index] ;
					
					
					
					value.add(type.getShippingService());
					if(type.getShippingServiceAdditionalCost() != null){
						value.add("ShippingServiceOptionsType.ShippingServiceAdditionalCost" + (index+1));
						value.add(type.getShippingServiceAdditionalCost().getCurrencyID().value()+","+type.getShippingServiceAdditionalCost().getValue());

					}
					if(type.getShippingServiceCost() != null){
						value.add("ShippingServiceOptionsType.ShippingServiceCost" + (index+1));
						value.add(type.getShippingServiceCost().getCurrencyID().value()+","+type.getShippingServiceCost().getValue());
					 
					}
					
					if(type.getShippingServicePriority() != null){
						value.add("ShippingServiceOptionsType.ShippingServicePriority" + (index+1));
						value.add(type.getShippingServicePriority()+"");	
					}
					if(type.getShippingTimeMin() != null){
						value.add("ShippingServiceOptionsType.ShippingTimeMin" + (index+1));
						value.add(type.getShippingTimeMin() + "");
					}
					if(type.getShippingTimeMax() != null){

						value.add("ShippingServiceOptionsType.ShippingTimeMax" + (index+1));
						value.add(type.getShippingTimeMax() + "");
					}
					
					
				}
			}
			InternationalShippingServiceOptionsType[] itypes = shippingDetails.getInternationalShippingServiceOption();
			if(itypes != null && itypes.length > 0){
				for(int index =0 , count = itypes.length ; index < count ; index++ ){
					InternationalShippingServiceOptionsType type = itypes[index];
					value.add("InternationalShippingServiceOptionsType.ShippingService" + (index+1));				
					value.add(type.getShippingService());
					
					if(type.getShippingServiceCost() != null){
						value.add("InternationalShippingServiceOptionsType.ShippingServiceCost" + (index+1));
						value.add(type.getShippingServiceCost().getCurrencyID().value()+","+type.getShippingServiceCost().getValue());
					}
					if( type.getShippingServicePriority() != 0l ){
						value.add("InternationalShippingServiceOptionsType.ShippingServicePriority" + (index+1));
						value.add(type.getShippingServicePriority()+"");
					}
				 
					String[] values = type.getShipToLocation();
					if(values != null && values.length > 0){
						value.add("InternationalShippingServiceOptionsType.ShipToLocation" + (index+1));
						value.add(StringUtil.convertStringArrayToString(type.getShipToLocation()));
					}
				
				
					
				}
			}
			//SalesTax SalesTaxType
			SalesTaxType  salesTaxType = shippingDetails.getSalesTax() ;
			if(salesTaxType != null){
				value.add("SalesTaxType.SalesTaxPercent");
				value.add(salesTaxType.getSalesTaxPercent()+"");
				value.add("SalesTaxType.SalesTaxState");
				value.add(salesTaxType.getSalesTaxState());
				value.add("SalesTaxType.ShippingIncludedInTax");
				value.add(salesTaxType.isShippingIncludedInTax()+"");
				value.add("SalesTaxType.SalesTaxAmount");
				AmountType  amountType = salesTaxType.getSalesTaxAmount();
				if(amountType != null){
					value.add(amountType.getCurrencyID().value() + "," + amountType.getValue());
				}else{
					value.add("");
				}
				
			 
			}
			if(shippingDetails.getShippingType() != null){
				value.add("ShippingType");
				value.add(shippingDetails.getShippingType().value());
			}
			if(shippingDetails.getExcludeShipToLocation() != null ){
				value.add("ExcludeShipToLocation");
				value.add(StringUtil.convertStringArrayToString(shippingDetails.getExcludeShipToLocation()));
			}
			if(shippingDetails.getPaymentInstructions() != null){
				value.add("PaymentInstructions");
				value.add(shippingDetails.getPaymentInstructions());
			}
			CalculatedShippingDiscountType internationalCalculatedShippingDiscount =  shippingDetails.getInternationalCalculatedShippingDiscount();
			if(internationalCalculatedShippingDiscount != null){
					String discountName = internationalCalculatedShippingDiscount.getDiscountName().value();
					value.add("InternationalDiscountName");
					value.add(discountName);
					DiscountProfileType[] profileTypeArray = internationalCalculatedShippingDiscount.getDiscountProfile();
					if(profileTypeArray != null && profileTypeArray.length > 0){
						for(int index = 0 , count = profileTypeArray.length  ; index < count ; index++ ){
							DiscountProfileType profileType  = profileTypeArray[index] ;
							value.add("InternationalDiscountProfileID"+(index+1));
							value.add(profileType.getDiscountProfileID());
							value.add("InternationalDiscountProfileName"+(index+1));
							value.add(profileType.getDiscountProfileName());
							value.add("InternationalMappedDiscountProfileID" + (index+1));
							value.add(profileType.getMappedDiscountProfileID());
							MeasureType weightOff =  profileType.getWeightOff();
							if(weightOff != null){
								String va = weightOff.getMeasurementSystem().value()+"," + weightOff.getUnit() +","+ weightOff.getValue();
								value.add("International WeightOff" + (index+1));
								value.add(va);
							}	 
					}
					
				}
				
			}
			CalculatedShippingDiscountType 	calculatedShippingDiscount = shippingDetails.getCalculatedShippingDiscount();
			if( calculatedShippingDiscount != null ){
					String discountName = calculatedShippingDiscount.getDiscountName().value();
					value.add("discountName");
					value.add(discountName);
					DiscountProfileType[] profileTypeArray = calculatedShippingDiscount.getDiscountProfile();
					if(profileTypeArray != null && profileTypeArray.length > 0){
						for(int index = 0 , count = profileTypeArray.length  ; index < count ; index++ ){
							DiscountProfileType profileType  = profileTypeArray[index] ;
							value.add("DiscountProfileID"+(index+1));
							value.add(profileType.getDiscountProfileID());
							value.add("DiscountProfileName"+(index+1));
							value.add(profileType.getDiscountProfileName());
							value.add("MappedDiscountProfileID" + (index+1));
							value.add(profileType.getMappedDiscountProfileID());
							MeasureType weightOff =  profileType.getWeightOff();
							if(weightOff != null){
								String va = weightOff.getMeasurementSystem().value()+"," + weightOff.getUnit() +","+ weightOff.getValue();
								value.add("WeightOff" + (index+1));
								value.add(va);
							}	 
					}
					
				}
			}
			CalculatedShippingRateType  calculatedShippingRateType  = shippingDetails.getCalculatedShippingRate();
			if(calculatedShippingRateType != null){
				String originatingPostalCode  = calculatedShippingRateType.getOriginatingPostalCode();
				MeasureType  depthType = calculatedShippingRateType.getPackageDepth();
				MeasureType  lengthType = calculatedShippingRateType.getPackageLength();
				MeasureType  widthType = calculatedShippingRateType.getPackageWidth();
				AmountType  handlingCost = calculatedShippingRateType.getPackagingHandlingCosts();
				ShippingPackageCodeType  shippingPackageCodeType = calculatedShippingRateType.getShippingPackage() ;
				MeasureType  weightMajor= calculatedShippingRateType.getWeightMajor();
				MeasureType  weightMinor = calculatedShippingRateType.getWeightMinor();
				
				value.add("originatingPostalCode");
				value.add(originatingPostalCode);
				if(depthType != null){
					value.add("PackageDepth");
					value.add(depthType.getMeasurementSystem().value()+","+depthType.getUnit() + "," +depthType.getValue());
				}
				if(lengthType != null){
					value.add("PackageLength");
					value.add(lengthType.getMeasurementSystem().value()+","+lengthType.getUnit() + "," +lengthType.getValue());	
				}
				if(widthType != null){
					value.add("PackageWidth");
					value.add(widthType.getMeasurementSystem().value()+","+widthType.getUnit() + "," +widthType.getValue());
				}
				if(handlingCost != null){
					value.add("PackagingHandlingCosts");
					value.add(handlingCost.getCurrencyID().value()+","+handlingCost.getValue());
				}
				if(shippingPackageCodeType != null){
					value.add("ShippingPackage");
					value.add(shippingPackageCodeType.value());
				}
				if(weightMajor != null){
					value.add("WeightMajor");
					value.add(weightMajor.getMeasurementSystem().value()+","+weightMajor.getUnit()+","+weightMajor.getValue());
				}
				if(weightMinor != null){
					value.add("WeightMinor");
					value.add(weightMinor.getMeasurementSystem().value()+","+weightMinor.getUnit()+","+weightMinor.getValue());
				}
			}
			
			
			
			
		
		
			int titleLength = value.size() / 2 ;
			for(int index = 0 ; index < titleLength ; index++ ){
				tilteNameList.add("Name");
				tilteNameList.add("Value");
			}
			if(sheet.getRow(0) != null  ){
				if(sheet.getRow(0).getLastCellNum()  < tilteNameList.size()){
					createNewSheetAndSheetTitle(wb, sheet, tilteNameList.toArray(new String[tilteNameList.size()]), 7000);
				}
			}else{
				createNewSheetAndSheetTitle(wb, sheet, tilteNameList.toArray(new String[tilteNameList.size()]), 7000);
			}
			//declare_r; 
			List<String[]> list = new ArrayList<String[]>();
			list.add(value.toArray(new String[value.size()]));
			setData(sheet, list, true	, false, wb);
 
		
		}catch (Exception e) {
			throw new SystemException(e,"createShipping",log);
		}
		 
		
	}


	@Override
	public void createProductListingDetails(Workbook wb, String itemNumber,
			String title, Sheet sheet, ProductListingDetailsType type)
			throws Exception {
		try{
			if(type == null){
				return ;
			}
			List<String> titleName = new ArrayList<String>();
			List<String> value = new ArrayList<String>();
			titleName.add("itemNumber");
			titleName.add("title");
			titleName.add("Brand");
			titleName.add("MPN");
			titleName.add("CopyRight");
			
			value.add(itemNumber);
			value.add(title);
			BrandMPNType  mnpType = type.getBrandMPN();
			if(mnpType != null){
				value.add(mnpType.getBrand());
				value.add(mnpType.getMPN());
				
			}
			String[] arrayCopyRight = type.getCopyright();
			if(arrayCopyRight != null && arrayCopyRight.length > 0){
				value.add(StringUtil.convertStringArrayToString(arrayCopyRight));
			}
			titleName.add("DetailsURL");
			titleName.add("EAN");
			titleName.add("GTIN");
			titleName.add("ISBN");
			titleName.add("ProductID");
			titleName.add("StockPhotoURL");
 
			titleName.add("UPC");
			titleName.add("ProductDetailsURL");
  
			value.add(type.getDetailsURL());
			value.add(type.getEAN());
			value.add(type.getGTIN());
			value.add(type.getISBN());
			value.add(type.getProductID());
			value.add(type.getStockPhotoURL());
			value.add(type.getUPC());
			value.add(type.getProductDetailsURL());
			 
			if(sheet.getRow(0) != null  ){
				if(sheet.getRow(0).getLastCellNum()  < titleName.size()){
					createNewSheetAndSheetTitle(wb, sheet, titleName.toArray(new String[titleName.size()]), 7000);
				}
			}else{
				createNewSheetAndSheetTitle(wb, sheet, titleName.toArray(new String[titleName.size()]), 7000);
			}
			setDataString(sheet,value);
		}catch (Exception e) {
			throw new SystemException(e,"createProductListingDetails",log);
		}
		
	}


	 
}
 