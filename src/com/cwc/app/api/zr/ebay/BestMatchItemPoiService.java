 package com.cwc.app.api.zr.ebay;

import java.sql.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

 
 

 

import com.cwc.app.iface.zr.ebay.BaseSamplePoi;
import com.cwc.model.BestMatchItem;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;
 

public class BestMatchItemPoiService extends  BaseSamplePoi<BestMatchItem>{
	
	
	
	 
	 

	@Override
	public Class getClazz() {
		return BestMatchItem.class;
	}

	@Override
	protected void setData(Sheet sheet, Workbook wb, List<BestMatchItem> list) {
		 
		 CellStyle cellStyle2 = wb.createCellStyle();
		 DataFormat format = wb.createDataFormat();
         cellStyle2.setDataFormat(format.getFormat("@"));
        
		if(list != null && list.size() > 0){
			int index = 2;	
			
			for(BestMatchItem item :  list){
				 
				
				Row row = sheet.createRow(index);
				row.setHeightInPoints(20);
				Cell itemId = row.createCell(0);
				
				itemId.setCellType(HSSFCell.CELL_TYPE_STRING);
				
				
				itemId.setCellStyle(cellStyle2);
				itemId.setCellValue(item.getItemId());
				
				 
				
				row.createCell(1).setCellValue(item.getItemRank());
				
				Cell itemName = row.createCell(2);
				itemName.setCellValue(item.getItemName());
			 
				
				Cell sellerId = row.createCell(3);
				sellerId.setCellValue(item.getSellerId());
			 
				
				Cell price = row.createCell(4);
				price.setCellValue(item.getPrice());
				 
				
				Cell curreny = row.createCell(5);
				curreny.setCellValue(item.getCurreny());
		 
				
				Cell quantitySold = row.createCell(7);
				quantitySold.setCellValue(item.getQuantitySold());
				
				
				
				row.createCell(6).setCellValue(item.getQuantityAvailable());
				row.createCell(8).setCellValue(item.getPrimaryCategoryName());
				row.createCell(9).setCellValue(item.getSecondaryCategoryName());
				row.createCell(10).setCellValue(item.getExpeditedShipping());
				
				index++ ; 
			}
		}
	}
	 
	private   CellStyle getCellStyle(Workbook wb , int index){
		CellStyle one = null;
		CellStyle two = null;
		if(one == null){
			 Font font = wb.createFont();
			 font.setColor(IndexedColors.WHITE.getIndex());
			one = wb.createCellStyle();
			one.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			one.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
			one.setFillPattern(CellStyle.SOLID_FOREGROUND);
			one.setFont(font);
			two = wb.createCellStyle();
			two.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		}
 
		return index % 2 == 1?one:two;
	}
 
	 

}
