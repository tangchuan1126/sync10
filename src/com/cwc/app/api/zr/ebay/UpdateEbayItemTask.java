package com.cwc.app.api.zr.ebay;

import java.io.File;
import java.util.*;

import org.directwebremoting.Browser;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;

 

import com.cwc.app.api.zr.CommonFileMgrZr;
import com.cwc.app.api.zr.EbayMgrZr;
import com.cwc.app.api.zr.UserState;
import com.cwc.app.iface.qll.EbayMgrIFace;
import com.cwc.app.iface.zr.CommonFileMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.util.EbayContextUtil;
import com.ebay.soap.eBLBaseComponents.AbstractResponseType;
import com.ebay.soap.eBLBaseComponents.ErrorHandlingCodeType;
import com.ebay.soap.eBLBaseComponents.ErrorType;
import com.ebay.soap.eBLBaseComponents.FeesType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.WarningLevelCodeType;
public class UpdateEbayItemTask implements Runnable {
 
	private String id ;
	private String loginUser ;  // ebay account token user id eg smart_tattoo token ;
	private UserState userState;
 
	private CommonFileMgrIfaceZr comonFileMgrZr = new CommonFileMgrZr(); 
	private EbayMgrZr ebayMgrZr = new EbayMgrZr();
 
	public UpdateEbayItemTask(String id, String loginUser,
			UserState userState  ) {
		super();
		this.id = id;
		this.loginUser = loginUser;
		this.userState = userState;
	 
	}


	@Override
	public void run() {
		// 首先读取文件 。如果文件读取成功要返回成功的信息。如果是失败了，也要返回信息
		// 先只支持一个文件的修改。
		boolean isFileOk =  true ;
		try{
			Collection<ScriptSession> sessions =  Browser.getTargetSessions();
			if(id != null && loginUser.length() > 0){
				List<ItemType> itemTypeList = ebayMgrZr.getItemTypeByFileId(id);
				if(itemTypeList != null && itemTypeList.size() > 0){
					int updateSuccessIndex = 0 ;
					int updateFailIndex = 0 ;
					
					for(int index = 0 , count = itemTypeList.size() ; index < count ; index++ ){
						ScriptBuffer script = new ScriptBuffer();
						StringBuffer sb = new StringBuffer();
						String message = "";
						String messageFlag = "";
						ItemType item  = null ;
						com.ebay.sdk.call.ReviseItemCall  veryAddCall = new  com.ebay.sdk.call.ReviseItemCall(EbayContextUtil.getContext(loginUser));
						try{
							veryAddCall.setErrorHandling(ErrorHandlingCodeType.ALL_OR_NOTHING);
							item = itemTypeList.get(index);
 							if(!userState.isStop()){	
								veryAddCall.setWarningLevel(WarningLevelCodeType.HIGH);
								veryAddCall.setVerifyOnly(false);
								 
								// 这里应该修改一些东西
								if(item.getVariations() != null  ){
									//dress 情况
									item.setStartPrice(null);
									item.setQuantity(null);
								}
								veryAddCall.setItemToBeRevised(item);
								FeesType  type = veryAddCall.reviseItem();
								AbstractResponseType responseType =  veryAddCall.getResponseObject();
								String ack = responseType.getAck().value();
								if(!ack.toLowerCase().equals("success")){
									ErrorType[]  errorTypes = responseType.getErrors();
									StringBuffer errorMessage= new StringBuffer();
									for(ErrorType e : errorTypes){
										errorMessage.append(e.getLongMessage() + " ,");
									}
									message = "'"+errorMessage.toString().replaceAll("\n", "")+"'";
									messageFlag = "'error'";
									updateFailIndex ++ ; 
								}else{
									updateSuccessIndex++ ;
									message = "'"+item.getItemID() + ".Reviseed OK'";
									messageFlag = "'success'";
								}
							 
								 
								sb.append("showUpdateProcess(").append(index).append(","+updateSuccessIndex).append(","+updateFailIndex)
								.append(","+count).append(","+message).append(",'"+ item.getItemID()).append("'," +messageFlag)
								.append(")");
							} else{
								sb = new StringBuffer();
								sb.append("stopUpdateProcess()");
							}
						}catch (Exception e) {
							//showUpdateProcess(0,0,0,1,,'181010792515',)
							updateFailIndex ++ ; 
							messageFlag = "'error'";
							message = "'"+e.getMessage().replaceAll("\n", "")+"'";
							sb.append("showUpdateProcess(").append(index).append(","+updateSuccessIndex).append(","+updateFailIndex)
							.append(","+count).append(","+message).append(",'"+ item.getItemID()).append("'," +messageFlag)
							.append(")");
						}finally{
						 
							script.appendScript(sb.toString());
							for(ScriptSession scriptSession : sessions) {
				  	 	        	scriptSession.addScript(script);
				  	 	     }
						}
						
				  
					}
					
				}else{
					//表示文件不存在或者市里面的信息不对
					isFileOk = false;
					ScriptBuffer script = new ScriptBuffer();
					script.appendScript("fileNotFound()");
	  	 	        for(ScriptSession scriptSession : sessions) {
	  	 	        	scriptSession.addScript(script);
	  	 	        }
				}
 			}
		}catch (Exception e) {
			 
		}
		
	}

}
