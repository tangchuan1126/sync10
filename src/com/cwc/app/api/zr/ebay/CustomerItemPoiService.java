package com.cwc.app.api.zr.ebay;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import com.cwc.app.iface.zr.ebay.BaseSamplePoi;
import com.cwc.app.iface.zr.ebay.EbayPoiIfaceZr;
import com.cwc.exception.SystemException;
import com.cwc.model.BaseItem;
import com.cwc.util.ReflectUtil;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityListType;

import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.VariationsType;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFDataFormat;

 

public class CustomerItemPoiService extends BaseSamplePoi<BaseItem> {
	
	static Logger log = Logger.getLogger("ACTION");
	
	private EbayPoiIfaceZr ebayPoiMgrZr ;
	
	
	 
	public CustomerItemPoiService() {
		super();
	}



	public CustomerItemPoiService(EbayPoiIfaceZr ebayPoiMgrZr) {
		super();
		this.ebayPoiMgrZr = ebayPoiMgrZr;
	}



	@Override
	public Class getClazz() {
		return BaseItem.class;
	}
	
	 

	public void setEbayPoiMgrZr(EbayPoiIfaceZr ebayPoiMgrZr) {
		this.ebayPoiMgrZr = ebayPoiMgrZr;
	}



	@Override
	protected void setData(Sheet sheet, Workbook wb, List<BaseItem> list ) throws Exception{
		try{
			sheet.createFreezePane(2, 0);
			HSSFCellStyle cellStyle=  (HSSFCellStyle) wb.createCellStyle();
			DataFormat  format = wb.createDataFormat();
			cellStyle.setDataFormat(format.getFormat("@"));
			cellStyle.setWrapText(true); 
			if(list != null && list.size() > 0){
				
				for(BaseItem item :  list){
					int index = sheet.getLastRowNum() + 1;	
					Row row = sheet.createRow(index);
					 
					row.setHeightInPoints(20);
					int cellIndex = 0 ;
					Cell itemNumberCell =	row.createCell(cellIndex);
					
					
					itemNumberCell.setCellValue(item.getItemNumber());
					itemNumberCell.setCellStyle(cellStyle);
					itemNumberCell.setCellType(HSSFCell.CELL_TYPE_STRING);
					row.createCell(++cellIndex).setCellValue(item.getTitle());
					row.createCell(++cellIndex).setCellValue(item.getQuantity());
					row.createCell(++cellIndex).setCellValue(item.getPrimaryCategory());
					row.createCell(++cellIndex).setCellValue(item.getSecondCategory());
					row.createCell(++cellIndex).setCellValue(item.getLocation());
					row.createCell(++cellIndex).setCellValue(item.getCountry());
					row.createCell(++cellIndex).setCellValue(item.getSku());
					row.createCell(++cellIndex).setCellValue(item.getPostalCode());
					row.createCell(++cellIndex).setCellValue(item.getHitCounter());
					row.createCell(++cellIndex).setCellValue(item.getListingType());
					row.createCell(++cellIndex).setCellValue(item.getDispatchTimeMax());
					row.createCell(++cellIndex).setCellValue(item.getListingDuration());
					row.createCell(++cellIndex).setCellValue(item.getSite());
					row.createCell(++cellIndex).setCellValue(item.getShipToLocations());
					row.createCell(++cellIndex).setCellValue(item.getPaymentMethods());
					row.createCell(++cellIndex).setCellValue(item.getPayPalEmailAddress());
					row.createCell(++cellIndex).setCellValue(item.getConditionID());
					row.createCell(++cellIndex).setCellValue(item.getConditionDisplayName());
					row.createCell(++cellIndex).setCellValue(item.getIsBestOfferEnabled());
					row.createCell(++cellIndex).setCellValue(item.getIsSkypeEnabled());
					row.createCell(++cellIndex).setCellValue(item.getIsAutoPay());
					row.createCell(++cellIndex).setCellValue(item.getIsDisableBuyerRequirements());
					row.createCell(++cellIndex).setCellValue(item.getWatchCount().equalsIgnoreCase("null") ? "" : item.getWatchCount());
					row.createCell(++cellIndex).setCellValue(item.getDistance());
					row.createCell(++cellIndex).setCellValue(item.getBuyerProtection());
					row.createCell(++cellIndex).setCellValue(item.getEbayNotes());
					row.createCell(++cellIndex).setCellValue(item.getFloorPrice());
					row.createCell(++cellIndex).setCellValue(item.getSubTitle());
					row.createCell(++cellIndex).setCellValue(item.getReservePrice());
					row.createCell(++cellIndex).setCellValue(item.getBuyItNowPrice());
					row.createCell(++cellIndex).setCellValue(item.getCeilingPrice());
					row.createCell(++cellIndex).setCellValue(item.getClassifiedAdPayPerLeadFee());
					row.createCell(++cellIndex).setCellValue(item.getGroupCategoryID());
					row.createCell(++cellIndex).setCellValue(item.getPaymentAllowedSite());
					row.createCell(++cellIndex).setCellValue(item.getStartPrice());
					
					List<Method> methodList = ReflectUtil.getSellerInfoMethod(BaseItem.class);
					for(Method method: methodList){
						String value =(String)method.invoke(item, new Object[]{});
						Cell temp = row.createCell(++cellIndex);
					//	temp.setCellStyle(cellStyle2);
						temp.setCellValue(value);
					}
					
					// Sheet
					// isCreateSheet 表示的是不是第一个itemType进来这个时候是要进行创建Title 和 sheet的
					boolean isCreateSheet = wb.getSheet("VariationsSet") == null;
					Sheet variationsSet = isCreateSheet?  wb.createSheet("VariationsSet") : wb.getSheet("VariationsSet");
					Sheet variations = isCreateSheet ? wb.createSheet("Variations") : wb.getSheet("Variations");
					Sheet itemSpecificsSheet = isCreateSheet ? wb.createSheet("ItemSpecifics") : wb.getSheet("ItemSpecifics");
					Sheet compatibilityList = isCreateSheet ? wb.createSheet("CompatibilityList") : wb.getSheet("CompatibilityList");
					Sheet shippingSheet = isCreateSheet ? wb.createSheet("shippingSheet") : wb.getSheet("shippingSheet");
					Sheet productListingDetails = isCreateSheet ? wb.createSheet("ProductListingDetails") : wb.getSheet("ProductListingDetails");

					variationsSet.createFreezePane(2, 0);
					variations.createFreezePane(2, 0);
					itemSpecificsSheet.createFreezePane(2, 0);
					compatibilityList.createFreezePane(2, 0);
					shippingSheet.createFreezePane(2, 0);
					
					
					// 处理 	itemSpecifics
					NameValueListArrayType  arrayType = item.getItemSpecifics();
					
				 	ebayPoiMgrZr.createItemSpecifics(arrayType, wb, itemSpecificsSheet,item.getItemNumber(),item.getTitle(),isCreateSheet); // createItemSpecifics
					
					// 处理variations
					VariationsType  variationsType = item.getVariations();
					ebayPoiMgrZr.createVariationsSetAndVariations(variationsType, wb,item.getItemNumber(),item.getTitle(),variationsSet,variations,isCreateSheet);
					 
					
					// 处理 CompatibilityList
					ItemCompatibilityListType  itemCompatibilityListType  = item.getItemCompatibilityList();
					ebayPoiMgrZr.createNewSheetByItemCompatibilityListType(itemCompatibilityListType, wb, compatibilityList,item.getItemNumber(),item.getTitle(),isCreateSheet);
					ebayPoiMgrZr.createShipping(wb,item.getItemNumber(), item.getTitle(),shippingSheet, item.getShippingDetails());
					ebayPoiMgrZr.createProductListingDetails(wb, item.getItemNumber(), "ProductListingDetails", productListingDetails, item.getProductListingDetails());
					}
				}
		}catch (Exception e) {
			throw new SystemException(e,"setData",log);
		}
		
	}
		
		
}
 
