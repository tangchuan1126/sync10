package com.cwc.app.api.zr.ebay;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.cwc.app.floor.api.zr.FloorEbayMgrZr;
import com.cwc.app.iface.zr.ebay.BestMatchItemDetailIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.model.BestMatchItem;
import com.cwc.util.AckKey;
import com.cwc.util.IoUtil;
import com.fr.base.Inter;
 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 * @author Administrator
 * BestMatchItemDetailsService
 * 输入name, keyword,topNumber,sellerId
 * 1.name 主要是用来存放某个人检索的文件夹，存放发送的xml
 * name 
 * 		send
 * 		save
 * 2.有日志的记录。检索的xml 存放的路径和名称
 * 
 * 
 */
public class BestMatchItemDetailsMgrZr  implements BestMatchItemDetailIfaceZr {
 
	 
	 public static final String POST_URL = "https://svcs.ebay.com/services/search/BestMatchItemDetailsService/v1";
	 public static String filePath = "";
	 public static final String separator = "/";
	 public static final String createXMLNameSpace = "http://www.ebay.com/marketplace/search/v1/services";
	 public static Logger logSuccess = Logger.getLogger("ebay_success");
	 public static Logger logError = Logger.getLogger("ebay_error");
	 public static Logger log = Logger.getLogger("ACTION");
	 
	 
	 
	 BestMatchItemPoiService bestMatchItemPoiService = new BestMatchItemPoiService();
	 
	 public  FloorEbayMgrZr floorEbayMgrZr;
	 
	 public void setFloorEbayMgrZr(FloorEbayMgrZr floorEbayMgrZr) {
			this.floorEbayMgrZr = floorEbayMgrZr;
		}
	/**
	  * 1.首先创建检索的xml
	  * 	根据name去查看有没有文件夹
	  * 	创建xml
	  * 2.记录日记
	  * 3.检索
	  * 4.记录检索的日志
	  * 5.处理成xls文件
	  * @throws Exception
	  */
	 @Override
	 public String bestMatch(String sellerId , String name ,String keyWord,int topNumber , String[] sellerIds , String ebaySite) throws Exception{
		try{ 
			
			 
			long createTime= createXML(name,keyWord,topNumber,sellerIds);
			String fileInXMLPath = filePath + separator+ "ebay_post_"+createTime+".xml";
			String fileOutXMLPath = filePath +  separator+ "ebay_save_"+createTime+".xml";
			String xlsOutPath = filePath +  separator+ "ebay_"+createTime+".xls";
			
			String returnPath = "ebay_"+createTime+".xls";
			
			String xml = postXML(sellerId ,fileInXMLPath ,fileOutXMLPath,ebaySite);
			 
 			 List<BestMatchItem> list = handleResponse(xml);
 			 String title = name + "  " +ebaySite + "   "+keyWord + "  Top :" + topNumber +"  "+ new Date(createTime);
			 bestMatchItemPoiService.createXLS(list,xlsOutPath,title,"BestMatchItem");
			 return returnPath;
		}catch (Exception e) {
			throw new SystemException(e,"bestMatch",log);
		}	
	 
	 }
	 private List<BestMatchItem> handleResponse(String xml) throws Exception {
		try{  
		 Document document = null ;
		 document = DocumentHelper.parseText(xml);
		 Element rootElt = document.getRootElement();
		 Element ack  = rootElt.element("ack");
		 List<BestMatchItem> bestMatchItems = new ArrayList<BestMatchItem>();
		 if(ack.getStringValue().trim().equals(AckKey.Success)){
			 Element searchResult =  rootElt.element("searchResult");
			 int count = Integer.parseInt(searchResult.attribute("count").getText());
			 if(count < 1){
				 return bestMatchItems;
			 }
			 Element searchItemGroup = 	searchResult.element("searchItemGroup");
			 Iterator items = searchItemGroup.elementIterator("item");
			 while(items.hasNext()){
				 
				 Element item = (Element) items.next(); 
					String itemId =  	item.element("itemId").getStringValue();
					String itemRank =  getElementValue(item.element("itemRank"));
					
					// 读取 itemRank,
					
					String itemName =   getElementValue(item.element("title")) ;
					Element seller = item.element("sellerInfo");
					String sellerId = seller.element("sellerUserName").getStringValue();
					 
					Element sellingStatus = item.element("sellingStatus");
					Element currentPrice  = sellingStatus.element("currentPrice");
					String currency = currentPrice.attributeValue("currencyId");
					String price = currentPrice.getStringValue();
			 
					Element quantitySoldElement = item.element("quantitySold");
				
					
					String qyantitySold  =getElementValue(quantitySoldElement);//quantitySoldElement.getStringValue();
					String quantityAvailable = getElementValue(item.element("quantityAvailable"));
					//quantityAvailable,primaryCategoryName,secondaryCategoryName,expeditedShipping
					Element primaryCategory = item.element("primaryCategory"); 
					String primaryCategoryName = "";
					String secondaryCategoryName = "";
					if(primaryCategory != null){
					 
						primaryCategoryName = getElementValue(primaryCategory.element("categoryName"));
					}
					Element secondaryCategory = item.element("secondaryCategory"); 
					if(secondaryCategory != null){ 
						secondaryCategoryName = getElementValue(secondaryCategory.element("categoryName"));
					}
					Element shippingInfo = item.element("shippingInfo");
					String expeditedShipping = "";
					if(shippingInfo != null) {
						expeditedShipping = getElementValue(shippingInfo.element("expeditedShipping"));
					}
				BestMatchItem match = new BestMatchItem(itemId,itemRank,itemName,sellerId,Double.parseDouble(price),currency,qyantitySold, quantityAvailable,primaryCategoryName,secondaryCategoryName,expeditedShipping);
				bestMatchItems.add(match);
			 }  
		 } else{
			 logError.error("no data from ebay");
		 }
		 return bestMatchItems;
		}catch (Exception e) {
			log.error("handleResponse error");
			throw new SystemException(e,"handleResponse",log);
		}
	 }
	 private String getElementValue(Element element) {
		 return element == null ? "" :element.getStringValue();
	 }
	 private String postXML(String sellerId ,String fileInXMLPath ,  String fileOutXMLPath , String ebaySite) throws Exception {
		 try {
			 DBRow tokenRow = floorEbayMgrZr.getEbayTokenBySellerId(sellerId);
			 if(tokenRow == null){
				 throw new RuntimeException("ebay token is null");
			 }
			String token = tokenRow.getString("user_token");
			IoUtil io = new IoUtil();
			String content = io.readFile(fileInXMLPath);
			////system.out.println(content);
		 	URL url = null ;
			url = new URL(POST_URL);
		    URLConnection uc = null;
		    OutputStreamWriter out = null;
		    BufferedReader rd = null;
	        uc = url.openConnection();
	        uc.setDoInput(true);
	        uc.setDoOutput(true);  
	        uc.setRequestProperty("CONTENT-TYPE", "text/xml");
            uc.setRequestProperty("X-EBAY-SOA-OPERATION-NAME", "findBestMatchItemDetailsByKeywords");
            uc.setRequestProperty("X-EBAY-SOA-SECURITY-TOKEN",token);
            uc.setRequestProperty("X-EBAY-SOA-SERVICE-VERSION","1.1.0");
            uc.setRequestProperty("X-EBAY-SOA-GLOBAL-ID",ebaySite);
            out = new OutputStreamWriter(uc.getOutputStream(),"utf-8");
            out.write(content);
            out.flush();
            out.close();
 
            rd = new BufferedReader(new InputStreamReader(uc.getInputStream(),"utf-8"));
            String responseStr;
            StringBuffer result = new StringBuffer();
            while ( (responseStr = rd.readLine()) != null) {
            	result.append(responseStr);
            }
            rd.close();
            io.writeFile(result.toString(), fileOutXMLPath);
            logSuccess.info(fileOutXMLPath + "post XML success");
	        return result.toString();
			 
	     } catch (IOException e) {
	  
        	logError.error(filePath + "   post XML Error");	
        	throw new SystemException(e,filePath + "   post XML Error",log);
         
	     }
		 
		 
		 
		 
	 }
	 
	 private long createXML(String name , String keyWord , int topNumber , String[] sellerIds) throws Exception {
		 try{
		 
			 long timeLong = new Date().getTime();
			 Document document = DocumentHelper.createDocument();
			 Element root = document.addElement("findBestMatchItemDetailsByKeywordsRequest",createXMLNameSpace);
			 
			 
			 Element keywordsEl =  root.addElement("keywords");
			 keywordsEl.setText(keyWord);
		 
			 
			 Element entriesPerPageEL =  root.addElement("entriesPerPage");
			 entriesPerPageEL.setText(topNumber+"");
			 
			 Element siteResultsPerPageEL =  root.addElement("siteResultsPerPage");
			 siteResultsPerPageEL.setText(topNumber+"");
			 Element outputSelectorEL1 =  root.addElement("outputSelector");
			 outputSelectorEL1.setText("FirstPageSummary");
			 Element outputSelectorEL2 =  root.addElement("outputSelector");
			 outputSelectorEL2.setText("SellerInfo");
			  
			 if(sellerIds != null && sellerIds.length > 0){
				 Element postSearchSellerFilter = root.addElement("postSearchSellerFilter");
				 for(String sellerName : sellerIds){
					 if(sellerName != null && sellerName.length() > 0){
						 postSearchSellerFilter.addElement("sellerUserName").setText(sellerName);
					 }
				 }
			 }
			 
	
			 String sendfilePath = filePath+separator +"ebay_post_"+timeLong+".xml";
			 new IoUtil().writeXMLToFile(sendfilePath, document);
			 logSuccess.info("  send file success " + sendfilePath);
			 return timeLong;
		 }catch (Exception e) {
			 logError.error( keyWord + topNumber );
			 
			 throw new SystemException(e,filePath + "  create XML error",log);
		}
	 }
	   
	 private void createFileDir(String name) throws Exception{
 		 File send = new File(filePath+separator+ name +separator+"send");
		 File save = new File(filePath+separator+ name +separator+"save");
		 File xls = new File(filePath+separator+ name+separator +"xls");
		 if(!send.exists()){
			 send.mkdirs();
		 }
		 if(!save.exists()){
			 save.mkdirs();
		 }
		 if(!xls.exists()){
			 xls.mkdirs();
		 }
	 }
	@Override
	public DBRow[] getAllEbayUserOnPaypal20() throws Exception {
		try{
			return  floorEbayMgrZr.getAllSellerOnPayPal20();
		}catch (Exception e) {
			throw new SystemException(e,"getAllEbayUserOnPaypal20",log);
		}
	}
	
}
