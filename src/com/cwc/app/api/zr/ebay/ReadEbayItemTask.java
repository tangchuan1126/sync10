package com.cwc.app.api.zr.ebay;

import java.util.Collection;

import org.directwebremoting.Browser;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import java.util.List;
import java.util.ArrayList;
import com.cwc.app.api.zr.EbayMgrZr;
import com.cwc.app.api.zr.UserState;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.ebay.BaseSamplePoi;
import com.cwc.model.BaseItem;

/**
 * 读取一个ItemNumber的数据然后放到一个List中
 * 如果是中途停止了操作或者是已经读取完了。那么就要生成xls.文件
 * 
 * @author Administrator
 *
 */
public class ReadEbayItemTask implements Runnable{
	
	private String id;
	private String token ;
 
	private String filePath ;
	private String title ;
	private UserState userState;
	private EbayMgrZrIFace ebayMgrZr = new EbayMgrZr();
	private BaseSamplePoi<BaseItem> customerItemPoiService = new CustomerItemPoiService(new EbayPoiMgrZr()) ;
 
	public void setEbayMgrZr(EbayMgrZrIFace ebayMgrZr) {
		this.ebayMgrZr = ebayMgrZr;
	}
	public void setCustomerItemPoiService(
			BaseSamplePoi<BaseItem> customerItemPoiService) {
		this.customerItemPoiService = customerItemPoiService;
	}

	private List<BaseItem> dataList = new ArrayList<BaseItem>();
	
	public ReadEbayItemTask() {
		super();
	}
	public ReadEbayItemTask(String id, String token,String filePath , String title , UserState userState) {
		super();
		this.id = id;
		this.token = token;
		this.filePath = filePath;
		this.title = title;
		this.userState = userState;
	}
//customerItemPoiService.createXLS(dataList, filePath, title, "Items");
		//script.appendScript("readComplete()");
	@Override
	public void run() {
	 
		 int readSuccess = 0 ;
		 int readError = 0 ;
		 boolean isStop = false;
	  	 try {
	  		if(id != null && id.length() > 0){
	  			String[] idArray = id.split(",");
	  			for(int index = 0 , count = idArray.length ; index < count ;   ){
	  				ScriptBuffer script = new ScriptBuffer();
	  				try{
		  				isStop = this.userState.isStop();
		  				if(!isStop){
		  					BaseItem item = ebayMgrZr.readEbayItemByNumberAndToken(idArray[index],token);
		  					dataList.add(item);
		  			 
		  					readSuccess++;
			  	 	        if(index+1 == count) {isStop = true;};
		  				} else{
		  					break;
		  				}
	  				}catch (Exception e) {
	  					readError ++ ;
					} finally{
					
		  	 	        script.appendScript("readIndex(").appendScript((index+1)+"").appendScript(",").appendScript(""+count).appendScript(","+readSuccess);
						script.appendScript("," + readError);
						script.appendScript(");");
						Collection<ScriptSession> sessions =  Browser.getTargetSessions();
		  	 	        for(ScriptSession scriptSession : sessions) {
		  	 	        	scriptSession.addScript(script);
		  	 	        }
		  	 	        
		  	 	        index++;
					}
	  			}
	  			if(isStop){
	  				 customerItemPoiService.createXLS(dataList, filePath, title, "Items");
	  				 ScriptBuffer script = new ScriptBuffer();
	  				 script.appendScript("readComplete("+readSuccess+")");
	  				 Collection<ScriptSession> sessions =  Browser.getTargetSessions();
	  	 	         for(ScriptSession scriptSession : sessions) {
	  	 	        		scriptSession.addScript(script);
	  	 	         }
	  			}
	  			
	  			
	  		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 
	
	public UserState getUserState() {
		return userState;
	}
	public void setUserState(UserState userState) {
		this.userState = userState;
	}
	public List<BaseItem> getDataList() {
		return dataList;
	}

	public void setDataList(List<BaseItem> dataList) {
		this.dataList = dataList;
	}
}
