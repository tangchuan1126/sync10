package com.cwc.app.api.zr;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

 



import org.apache.log4j.Logger;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeManager;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import com.cwc.app.iface.zr.ConvertFileToSwfMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ConvertFileToSwfMgrZr implements ConvertFileToSwfMgrIfaceZr
{
	private static Logger log = Logger.getLogger("ACTION");
	private static final String DOC = "doc";  
	private static final String DOCX = "docx";  
	private static final String XLS = "xls";  
	private static final String XLSX = "xlsx";  
	private static final String PDF = "pdf";  
	private static final String PPT = "ppt";  
 
	private static String OO_HOME = null ;
	private static String OO_HOST = null ;
	private static String OO_PORT = null ;
	private static OfficeManager officeManager = null ;
 
	private static boolean isServerOk = false ;
	
 
 
	@Override
	public DBRow conver(String srcFilePath, String desFilePath,String tempPdfPath) throws Exception
	{
		try{
			DBRow result = new DBRow();
			File srcFile = new File(srcFilePath);
			if(srcFile.exists()){
				String fileName =  srcFile.getName();
				if(fileName != null && fileName.length() > 0 ){
					String suffix = StringUtil.getSuffix(fileName).toLowerCase();
					
					if(suffix.equals(DOC) || suffix.equals(DOCX) || suffix.equals(XLS) || suffix.equals(XLSX) || suffix.equals(PPT)){
						//需要转换成PDF
						//if(isServerOk){
							String pdfFileName = fileName.substring(0,fileName.lastIndexOf(".")+1)+"pdf";
							srcFilePath  = this.office2PDF(srcFile,tempPdfPath+pdfFileName);
							 
						//}
					}
					//如果上传的就是PDF
					 if(suffix.equals(PDF) || new File(srcFilePath).exists()){
						 convertPdf2Swf(srcFilePath,desFilePath);
						 result.add("flag", "success");
					 }
				}
			}else{
				result.add("flag", "error");
				result.add("message", "文件不存在");
			}
			return result;
		}catch (Exception e) {
			throw new Exception("ConvertFileToSwfMgrZr.conver(row):"+e);
		}
	}
	private String office2PDF(File inputFile, String destFileName) throws Exception {  
		   File outputFile = new File(destFileName);  
	        try {  
	         
	       //     String command = OO_HOME + "program/soffice.exe -headless -accept=\"socket,host=" + OO_HOST + ",port=" + OO_PORT + ";urp;\"";  
	      //      Process pro = Runtime.getRuntime().exec(command);  
	        	startService();
	            OpenOfficeConnection connection =  new SocketOpenOfficeConnection(OO_HOST, Integer.parseInt(OO_PORT));  
	            connection.connect();  
	            DocumentConverter converter = new OpenOfficeDocumentConverter(connection);  
	            converter.convert(inputFile, outputFile);  
	            connection.disconnect();  
	        //    pro.destroy();  
	        } catch (Exception e) {  
	        	throw new SystemException(e, "office2PDF", log);
	        }
	        return destFileName;  
	  }  
	private   void convertPdf2Swf(String sourceFile, String outFile) throws Exception{  
        String command =  "/usr/local/bin/./pdf2swf" + " " + sourceFile + " -o " + outFile + " -s flashversion=9 ";  
        try {  
            Process process = Runtime.getRuntime().exec(command);  
            loadStream(process.getInputStream());  
            loadStream(process.getErrorStream());  
            loadStream(process.getInputStream()); 
            process.destroy();  
        } catch (Exception e) {  
        	throw new SystemException(e, "convertPdf2Swf", log);
        }  
    }  
	private  String loadStream(InputStream in) throws IOException{  
        int ptr = 0;  
        in = new BufferedInputStream(in);  
        StringBuffer buffer = new StringBuffer();  
          
        while ((ptr=in.read())!= -1){  
            buffer.append((char)ptr);  
        }  
        return buffer.toString();  
    }  
	 public void startService() {  
		 	if(isServerOk){
		 		return ;
		 	}
		 	readOpenOfficeConfig();
	        DefaultOfficeManagerConfiguration configuration =   new DefaultOfficeManagerConfiguration(); 
	       
	        try {    
	            configuration.setOfficeHome(OO_HOME);	
	            configuration.setPortNumbers(Integer.parseInt(OO_PORT)); 	  
	            configuration.setTaskExecutionTimeout(1000 * 60 * 5L);	 
	            configuration.setTaskQueueTimeout(1000 * 60 * 60 * 24L); 
	            configuration.setMaxTasksPerProcess(5);
	            officeManager = configuration.buildOfficeManager();  
	            officeManager.start();
	            isServerOk = true ;
	            log.info("openOffice start up success."); 
	            System.err.println("start ok");
	        } catch (Exception ce) {  
	        	log.info("openOffice start up failed:" + ce); 
	        	isServerOk = false ;
	        }  
	    }  
	 public void stopService(){
		 if(officeManager != null){
		 
			 officeManager.stop();
			 log.info("openOffice转换服务shut down.");  
		 }
	 }
	private static void readOpenOfficeConfig() {
		try{
			InputStream in =  new BufferedInputStream(new FileInputStream(Environment.getHome()+"/WEB-INF/conf/OpenOfficeService.properties"));
			ResourceBundle rb = new PropertyResourceBundle(in);
			OO_HOME = rb.getString("oo_home");  
			OO_HOST = rb.getString("oo_host");  
			OO_PORT = rb.getString("oo_port");
		}catch (Exception e) {
			log.error("openOffice config not exits:");
			isServerOk = false ;
		}
	}
	 
	
}
