package com.cwc.app.api.zr;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

 




import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;

import com.cwc.app.api.ExpressMgr;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.emailtemplate.velocity.quote.BillEmailPageString;
import com.cwc.app.emailtemplate.velocity.quote.QuoteEmailPageString;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zr.BillMgrIfaceZR;
import com.cwc.app.iface.zr.InvoiceMgrIfaceZR;
import com.cwc.app.key.BillTypeKey;
import com.cwc.app.lucene.zr.BillIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Attach;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.reportcenter.ReportCenter;
import com.cwc.util.StringUtil;

public class BillMgrZr implements BillMgrIfaceZR{

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorBillMgrZr floorBillMgrZr;
	private ExpressMgr expressMgr;
	private FloorAdminMgr fam ;
	private OrderMgr orderMgr;
	private ProductMgr productMgr;
	private InvoiceMgrIfaceZR invoiceMgrZr;
	private SystemConfigIFace systemConfig;

	@Override
	public long addBill(DBRow billRow , List<DBRow> billItems) throws Exception {
		try{
			long billId = floorBillMgrZr.addBill(billRow);
			if(billItems != null && billItems.size() > 0 ){
				for(DBRow row : billItems){
					row.add("bill_id",billId);
					floorBillMgrZr.addBillItem(row);
				}
				
			}
			BillIndexMgr.getInstance().addIndex(billId, billRow.getString("client_id"), billRow.get("porder_id", 0l), billRow.getString("payer_email"), billRow.getString("address_name"));
			return billId;
		}catch (Exception e) {
			throw new SystemException(e,"addBill",log);
		}
		
	}

	@Override
	public void updateBill(long billId,DBRow billRow, List<DBRow> itemList)
			throws Exception {
		try{
			
			floorBillMgrZr.updateBill(billId, billRow);
			if(itemList != null && itemList.size() > 0){
				List<Long> ids = new ArrayList<Long>();
				double quantity = 0.0d;
				for(DBRow itemRow : itemList){
				 
				 	quantity += itemRow.get("quantity", 0.0d);
					long itemId = floorBillMgrZr.saveOrUpdateBillItem(itemRow.get("bill_item_id", 0l), itemRow,billId);
					ids.add(itemId);
				}
			   floorBillMgrZr.deleteItemsByNotInIdsAndUpdateBillQuantity(ids, quantity, billId);	
			}
			BillIndexMgr.getInstance().updateIndex(billId, billRow.getString("client_id"),billRow.get("porder_id", 0l) ,billRow.getString("payer_email") , billRow.getString("address_name"));
		}catch (Exception e) {
			throw new SystemException(e,"updateBill",log);
		}

	}


	@Override
	public DBRow[] getAllBill(PageCtrl pc) throws Exception {
		try{
			return floorBillMgrZr.getAllBill(pc); 
		}catch (Exception e) {
			throw new SystemException(e,"getAllBill",log);
		}
	}


	@Override
	public DBRow[] getItemsByBillId(long billId) throws Exception {
		try{
			return floorBillMgrZr.getBillItemByBillOrderId(billId);
		}catch (Exception e) {
			throw new SystemException(e,"getItemsByBillId",log);
		}
	}


	@Override
	public DBRow[] getAccountInfoList(long key) throws Exception {
		try{
			return floorBillMgrZr.getAccountInfoList(key);
		}catch (Exception e) {
			throw new SystemException(e,"getAccountInfoList",log);
		}
	}


	@Override
	public void deleletBillByBillId(long billId) throws Exception {
		try{
			  floorBillMgrZr.deleteBillById(billId);
			   
			  BillIndexMgr.getInstance().deleteIndex(billId);
		}catch (Exception e) {
			throw new SystemException(e,"deleletBillByBillId",log);
		}
		
	}

	//为PDF提供数据
	@Override
	public DBRow getPDFDatas(long billId) throws Exception {
		
		try{
			DBRow result = new DBRow();
			ArrayList<DBRow> itemsAL = new ArrayList<DBRow>();
			DBRow billOrder =  floorBillMgrZr.getBillByBillId(billId);
			DBRow billOrderItems[] = floorBillMgrZr.getBillItemByBillOrderId(billId);
			DBRow detailQuoteAccount = fam.getDetailAdmin(billOrder.get("create_adid",0l));
			
			 
			// 这里应该判断Bill的类型如果是PayForOrder 或者是 PayForShippingFee的时候要特殊的处理
			if(billOrder.get("bill_type", 0) == BillTypeKey.order){
				if(billOrderItems == null ||( billOrderItems != null && billOrderItems.length < 1)){
					//pay for order
					 DBRow row = new DBRow();
					 row.add("quantity",1.0d);
					 row.add("unit_name","");
					 row.add("name","Pay For Order["+billOrder.get("porder_id",0l)+"]");
					 row.add("actual_price",billOrder.get("order_fee",0.0d));
					 row.add("total",billOrder.get("order_fee",0.0d));
					 itemsAL.add(row);
				}
			}
			//shipping 如何是Item 为的0的话,那么就是 为 运费付款
			if(billOrder.get("bill_type",0) == BillTypeKey.shipping && ( billOrderItems != null && billOrderItems.length < 1)){
				 
				DBRow shippingItem = new DBRow();
				shippingItem.add("quantity",1.0d);
				shippingItem.add("unit_name","");
				shippingItem.add("name","For Shipping Fee");
				shippingItem.add("actual_price",0.0d);
				shippingItem.add("total",0.0d);
				itemsAL.add(shippingItem);
			}
			boolean isEbay =  billOrder.get("bill_type", 0) == BillTypeKey.ebay ? true : false;
			for (int i=0; i<billOrderItems.length; i++)
			{
				 
				// 如果是Ebay的话应该吧 item_number加在Name后面
				//封装普通商品数据
				DBRow item = new DBRow();
				item.add("quantity", billOrderItems[i].getString("quantity"));
				item.add("unit_name", billOrderItems[i].getString("unit_name"));
				item.add("name", billOrderItems[i].getString("name") + (isEbay?" Number : " + billOrderItems[i].getString("item_number"):""));
				item.add("actual_price", String.valueOf(MoneyUtil.round(billOrderItems[i].get("actual_price", 0.0d), 2)));
 				item.add("total", String.valueOf(MoneyUtil.round(billOrderItems[i].get("amount", 0.0d), 2)));
				itemsAL.add(item);
				//如果是定制商品，封装定制商品数据
				DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( billOrderItems[i].get("pc_id",0l) );
				for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
				{
					DBRow customItem = new DBRow();
					customItem.add("name", "  - "+customProductsInSet[customProductsInSet_i].getString("p_name")+" X "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
					itemsAL.add(customItem);
				}
				
				 
			}
			
			//报价单基本数据
			HashMap<String, String> paras = new HashMap<String, String>();
			
			String date = billOrder.getString("post_date");
			String expiration_date = billOrder.getString("expiration_date");
			
			//date 和  expiration_date 有的时候weikong
			date = StringUtil.isNull(date) ? DateUtil.NowStr() : date ;
			expiration_date =  StringUtil.isNull(expiration_date) ? DateUtil.NowStr() : expiration_date ;
		
			paras.put("qoid",String.valueOf(billId));
			paras.put("date",date);
			paras.put("expiration_date",expiration_date);
			 
			paras.put("note", billOrder.getString("note"));
			DBRow detailCompany = expressMgr.getDetailCompany(billOrder.get("sc_id",0l));
			if (detailCompany==null)
			{
				paras.put("shipping","?");
			}
			else
			{
				paras.put("shipping",detailCompany.getString("name"));
			}
			
			String address2 = "";
			if (billOrder.getString("address_city").equals("")==false)
			{
				address2 += billOrder.getString("address_city")+",";
			}
			if (billOrder.getString("address_state").equals("")==false)
			{
				address2 += billOrder.getString("address_state")+",";
			}
			if (billOrder.getString("address_zip").equals("")==false)
			{
				address2 += billOrder.getString("address_zip")+",";
			}
			address2 += orderMgr.getDetailCountryCodeByCcid(billOrder.get("ccid",0l)).getString("c_country");
			
			paras.put("create_account",billOrder.getString("create_account")+" / Email: "+detailQuoteAccount.getString("email")+" / MSN: "+detailQuoteAccount.getString("msn"));
			paras.put("name",billOrder.getString("address_name"));
			paras.put("address1",billOrder.getString("address_street"));
			paras.put("address2",address2);
			paras.put("tel",billOrder.getString("tel"));
			paras.put("email",billOrder.getString("client_id"));
			
			paras.put("subtotal",String.valueOf(MoneyUtil.round(billOrder.get("subtotal",0d), 2)));
			paras.put("shipping_fee",String.valueOf(MoneyUtil.round(billOrder.get("shipping_fee",0d),2)));
			paras.put("total",StringUtil.formatNumber("#0.00", billOrder.get("total_discount",0.0d)));
			paras.put("saving",String.valueOf(MoneyUtil.round(billOrder.get("save",0d), 2)));
			
			
			//输出数据
			result.add("view_path", ReportCenter.REPORT_VIEW_ROOTPATH+"quote/invoice_detail.jasper");//绑定数据的模板绝对路径
			result.add("paras", paras);
			result.add("items", (DBRow[])itemsAL.toArray(new DBRow[0]));
			
			return(result);
		}catch (Exception e) {
			throw new SystemException(e,"getPDFDatas",log);
		} 
	}
	@Override
	public DBRow getBillByBillId(long billId) throws Exception {
		 try{
			 return floorBillMgrZr.getBillByBillId(billId);
		 }catch (Exception e) {
			throw new SystemException(e,"getBillByBillId",log);
		 } 
	 
	}
	
	@Override
	public DBRow sendBillViaEmail(HttpServletRequest request) throws Exception {
		FileOutputStream fos = null;
		DBRow returnResult = new DBRow();
		try
		{
			long billId = StringUtil.getLong(request, "bill_id");
			DBRow billOrder = floorBillMgrZr.getBillByBillId(billId);
		 
			String quotationPDFTmpFolder = "quotation_pdf_tmp";//存放报表附件的临时文件夹
			String quotationPDFPath;//PDF文件完整路径
			
			//先生成报价单PDF附件文件到临时目录
			File tmpFolder = new File(Environment.getHome()+quotationPDFTmpFolder);
			if (tmpFolder.exists()==false)//不存在则创建
			{
				tmpFolder.mkdir();
			}
			quotationPDFPath = Environment.getHome()+quotationPDFTmpFolder+"/VisionariInvoice"+billId+".pdf";
			//如果之前已经生成PDF，则先删除
			File pdfFile = new File(quotationPDFPath);
			if (pdfFile.exists())
			{
				pdfFile.delete();
			}
			//准备报表数据
			DBRow result = this.getPDFDatas(billId);
			String reporViewTemplatetFilePath = result.getString("view_path");
			DBRow dataRows[] = (DBRow[])result.get("items",new Object());
			HashMap<String, String> paras = (HashMap<String, String>)result.get("paras",new Object());
			//生成报表
			ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows); 
			JasperPrint printer = reportCenter.getPrinter();
			//生成PDF
			byte[] bReport = JasperExportManager.exportReportToPdf(printer);
			fos = new FileOutputStream(quotationPDFPath,true); 
			fos.write(bReport);   
			fos.flush(); 
			
			//发送邮件
			Attach attach = new Attach(quotationPDFPath);  
			
			BillEmailPageString returnVoiceEmailPageString = new BillEmailPageString(billId);
			User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
            MailAddress mailAddress = new MailAddress(billOrder.getString("client_id"));
            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,attach);

            Mail mail = new Mail(user);
            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            returnResult.add("flag", "error");
            mail.send();  // send报错了Catch不了。
            returnResult.add("flag", "success");
		}
		catch (Exception e)
		{
			returnResult.add("flag", "error");
			e.printStackTrace();
			throw new SystemException(e,"sendBillViaEmail",log);
			
		}finally{
			try{
				 fos.close();
			}catch (Exception e) {
				 
			}
		}
		return returnResult;
		
	}  
	@Override
	public boolean hasAuthority(long adid, long createAdid) throws Exception {
		/* 1.如果createAdid == adid 那么就是true
		   2.如果是总经理 那么也是 true;
		   3.如果adid是改createAdid 所在部门的主管或者是副主管那么就是true	
		*/
		if(adid != 0l && createAdid != 0l){
			if(adid == createAdid){
				return true;
			}
			
			//首先查出createAdid所在部门的 主管或者是副主管的 ids .然后看adid是否在这里面
			DBRow[] rows = floorBillMgrZr.getUserByAdid(createAdid);
			if(rows != null && rows.length > 0){
				
				for(DBRow row : rows){
					if(row.get("adid", 0l) == adid){
						 return true;
					}
				}
			}
			
			//是否是总经理
			return floorBillMgrZr.isMainManager(adid);
			
			
			 
		} 
		return false;
		 
	}
	@Override
	public DBRow[] filterBill(HttpServletRequest request , PageCtrl page) throws Exception {
		try{
			String st = StringUtil.getString(request, "st");
			String end = StringUtil.getString(request, "end");
			long createAdid = StringUtil.getLong(request, "create_adid");
			int bill_status = StringUtil.getInt(request, "bill_status", 0);
			long porder_id = StringUtil.getLong(request, "porder_id",0l);
			String client_id = StringUtil.getString(request, "client_id");
			int key_type = StringUtil.getInt(request, "key_type");
			int bill_type = StringUtil.getInt(request, "bill_type");
			int account_payee = StringUtil.getInt(request, "account_payee");
			String account = StringUtil.getString(request, "account");
			String account_name = StringUtil.getString(request, "account_name");
 
			return   floorBillMgrZr.filterBill(st, end, createAdid, bill_status, porder_id, client_id, page,key_type,bill_type,account_payee,account,account_name);
		}catch (Exception e) {
			throw new SystemException(e,"filterBill",log);
		}
		
	}
	@Override
	public DBRow getBillAndBillItemByBillId(long bill_id) throws Exception {
		try{
			DBRow row = new DBRow();
			DBRow billRow = this.getBillByBillId(bill_id);
			if(billRow != null && billRow.get("bill_id", 0l) != 0l){
				DBRow[] bills =  new DBRow[1];
				bills[0] = billRow;
				row.add("bill", bills);
				row.add("items", this.getItemsByBillId(bill_id));
				row.add("flag", "success");
			}else{
				row.add("flag", "error");
				row.add("msg", "Input Number Error!");
			}
			
			return row;
		}catch(Exception e){
			throw new SystemException(e,"getBillAndBillItemByBillId",log);
		}
	 
	}
	@Override
	public DBRow[] getProductUnionByPcid(long pc_id) throws Exception {
		try{
			 return floorBillMgrZr.getUnoinProduct(pc_id);
		}catch(Exception e){
			throw new SystemException(e,"getProductUnionByPcid",log);
		}
	}
	
	public DBRow[] getBillBySearchKey(String searchKey,int search_mode,PageCtrl pc)
		throws Exception 
		{
			try
			{
				int page_count = systemConfig.getIntConfigValue("page_count");
				
				return  BillIndexMgr.getInstance().getSearchResults(searchKey.toLowerCase(),search_mode,page_count,pc);
			}
			catch(Exception e)
			{
				throw new SystemException(e,"getBillBySearchKey",log);
			}
	}
	@Override
	public long addBillByOrder(DBRow billRow, List<DBRow> itemList)
			throws Exception {
		try{
			 long id = this.addBill(billRow, itemList);
			  // 更新order 表中的字段
			 long porder_id = billRow.get("porder_id", 0l);
			 DBRow updateOrderRow = new DBRow();
			 updateOrderRow.add("is_pay_for_order", 1);
			 floorBillMgrZr.updateOrder(porder_id, updateOrderRow);
			 BillIndexMgr.getInstance().addIndex(id, billRow.getString("client_id"), billRow.get("porder_id", 0l), billRow.getString("payer_email"), billRow.getString("address_name"));
			 return id;
		}catch(Exception e){
			throw new SystemException(e,"addBillByOrder",log);
		}
	}

	@Override
	public DBRow getBillItemByClientIdAndBillId(long billId, String clientId)
			throws Exception {
		try{
		 
			 return floorBillMgrZr.getBillItemByIdAndEmail(billId, clientId);
		}catch(Exception e){
			throw new SystemException(e,"getBillItemByClientIdAndBillId",log);
		}
	}
	public void setFloorBillMgrZr(FloorBillMgrZr floorBillMgrZr) {
		this.floorBillMgrZr = floorBillMgrZr;
	}
	
 
	 
	public void setInvoiceMgrZr(InvoiceMgrIfaceZR invoiceMgrZr) {
		this.invoiceMgrZr = invoiceMgrZr;
	}

	public void setExpressMgr(ExpressMgr expressMgr) {
		this.expressMgr = expressMgr;
	}

	public void setFam(FloorAdminMgr fam) {
		this.fam = fam;
	}

	public void setOrderMgr(OrderMgr orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	

	

	

	

	

	



	

 
	
	
	
}
