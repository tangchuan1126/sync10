package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorStorageYardControlZr;
import com.cwc.app.iface.zr.StorageYardControlIfaceZr;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInSpotStatusTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;

public class StorageYardControlMgrZr implements StorageYardControlIfaceZr {
	/**
 		row.add(String.valueOf(OccupyStatusTypeKey.OCUPIED), "Occupied");		//占用
		row.add(String.valueOf(OccupyStatusTypeKey.RELEASED), "Released"); 	//释放 也就是Free	
	 */
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorStorageYardControlZr floorStorageYardSpotZr ;
 
	public static final String RELEASED = "RELEASED";
	public static final String OCUPIED = "OCUPIED";
	
	private void addSpotLog(long yc_id , long adid , String type , long dlo_id) throws Exception{
		//
		if(yc_id > 0l){
			DBRow spotRow =  floorStorageYardSpotZr.getSpotInfoBySdId(yc_id);
			if(spotRow != null){
		  		DBRow row = new DBRow();
				row.add("operator_id", adid);
				row.add("note", type + " : "+ spotRow.getString("yc_no"));
				row.add("dlo_id", dlo_id);
				row.add("log_type", CheckInLogTypeKey.SpotLog);
				row.add("operator_time",DateUtil.NowStr());
				row.add("data", "");
				floorStorageYardSpotZr.addCheckInLog(row);
			}
		}
		
	}
	@Override
	public void freeSpot(long yc_id ,long adid , long associate_id , int associate_type) throws Exception {
		try{
			if(yc_id > 0l){
				DBRow updateRow = new DBRow();
				updateRow.add("associate_id", 0);
				updateRow.add("associate_type", 0);
				updateRow.add("yc_status", CheckInSpotStatusTypeKey.RELEASED);
				floorStorageYardSpotZr.updateSpot(yc_id, updateRow);
				addSpotLog(yc_id, adid, RELEASED, associate_id);   //现在只有CheckIn的
			}
		}catch(Exception e){
			throw new SystemException(e, "freeSpot " + yc_id, log);
		}
	}
	private void fixSpotRows(DBRow[] rows){
		if(rows != null){
			for(DBRow row : rows){
 				row.remove("patrol_time");
				row.remove("latlng");
				row.remove("height");
				row.remove("width");
				row.remove("angle");
				row.remove("y");
				row.remove("x");
				row.remove("ps_id");
			}
		}
	}
	@Override
	public DBRow[] getSpotByPsIdAndOccupiedStatusAndZoneId(long ps_id, int occupied_status , long zone_id) throws Exception {
		try{
			return floorStorageYardSpotZr.getSpotByPsIdAndOccupiedStatusZoneId(ps_id, occupied_status, zone_id);
		}catch(Exception e){
			throw new SystemException(e, "getByPsIdAndOccupiedStatus ps_id :" + ps_id + "occupied_status :  "+ occupied_status, log);
		}
 	}
 	@Override
	public void ocupiedSpot(long yc_id, int associate_type, long associate_id , long adid)
			throws Exception {
 			try{
 				DBRow updateRow = new DBRow();
 				updateRow.add("associate_type", associate_type);
 				updateRow.add("associate_id", associate_id);
 				updateRow.add("yc_status", CheckInSpotStatusTypeKey.OCUPIED);
 				floorStorageYardSpotZr.updateSpot(yc_id, updateRow);
				addSpotLog(yc_id, adid, OCUPIED, associate_id);   //现在只有CheckIn的
 			}catch(Exception e){
 				throw new SystemException(e, "ocupiedSpot " + yc_id + " associate_type : " + associate_type + " associate_id : "+ associate_id, log);
 			}
	}
 
	@Override
	public DBRow[] getFreeSpot(long ps_id , long zone_id) throws Exception {
		try{
			 DBRow[] returnRows =getSpotByPsIdAndOccupiedStatusAndZoneId(ps_id, CheckInSpotStatusTypeKey.RELEASED,zone_id); 
			 fixSpotRows(returnRows);
			 return returnRows ;
		}catch(Exception e){
			throw new SystemException(e, "ps_id " + ps_id, log);
		}
 	}
	@Override
	public DBRow[] getAvailableSpotsBy(long ps_id, int associate_type,
			long associate_id) throws Exception {
		try{
			return getAvailableSpotsByZoneId(ps_id, 0l, associate_id, associate_type);
		}catch(Exception e){
			throw new SystemException(e, "getAvailableSpotsBy associate_type ,ps_id ,associate_id  " + ps_id + " ", log);
		}
 	}
	@Override
	public DBRow[] getAvailableSpotsByZoneId(long ps_id, long zone_id, long associate_id, int associate_type) throws Exception {
		try{
			List<DBRow> returnArray = new ArrayList<DBRow>();
			DBRow[] frees = getFreeSpot(ps_id,zone_id);
			Collections.addAll(returnArray, frees);
			if(associate_id > 0l){
				DBRow[] append = floorStorageYardSpotZr.getSpotsBySelf(associate_id,associate_type,zone_id);
				appendSpotBySelf(append);
				Collections.addAll(returnArray, append);
			}
			//排序
			Collections.sort(returnArray,new StorageSpotComparator());
			return returnArray.toArray(new DBRow[0]);
		}catch(Exception e){
			throw new SystemException(e, "getAvailableSpotsByZoneId", log);
		}
 	}
	/**
	 * 多添加一个参数，表示这些是他自己，原来占用的，所以他这次选择也是可以用的 . addflag(怕在页面上有不同的显示)
	 * @param rows
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	private void appendSpotBySelf(DBRow[] rows){
		fixSpotRows(rows);
		if(rows != null && rows.length > 0){
			for(DBRow row : rows){
				row.add("addflag", "BySelf"); //他原来自己占用的
			}
		}
	}
	
	/**
	 * 返回doorId一个单数最小，和一个双数最小的
	 * 因为从小到大就是排好序的。
	 */
	@Override
	public HoldDoubleValue<DBRow, DBRow> getMinSinleAndMinDoubleBySortedArray(DBRow[] spots) throws Exception {
		try{
			DBRow minDoubleRow = null ;
			DBRow minSinleRow = null ;
			if(spots != null){
				for(DBRow row : spots){
					int spotName = row.get("yc_no", 0);
					if(spotName % 2 == 0 && minDoubleRow == null ){
						minDoubleRow = row ;
						continue ;
					}
					if(spotName % 2 == 1 && minSinleRow == null){
						minSinleRow = row ;
						continue ;
					}
					if(minSinleRow != null  && minDoubleRow != null){
						break ;
					}
				}
			}
			return new  HoldDoubleValue<DBRow, DBRow>(minSinleRow, minDoubleRow);
		}catch(Exception e){
			throw new SystemException(e, "getMinSinleAndMinDoubleBySortedArray", log);
 		}
	}
	 
	 
	@Override
	public DBRow[] getUsedSpots(long ps_id, long zone_id) throws Exception {
		try{
			DBRow[] returnRows = floorStorageYardSpotZr.getUsedSpots(ps_id,zone_id);
			fixSpotRows(returnRows);
			return returnRows ;
		}catch(Exception e){
			throw new SystemException(e, "getUsedSpots", log);
		}
	}
	@Override
	public DBRow getCheckInSpotsDetailInfo(long yc_id) throws Exception {
		try{
			return floorStorageYardSpotZr.getCheckInModuleSpotInfo(yc_id) ;
 		}catch(Exception e){
			throw new SystemException(e, "getCheckInSpotDetailInfo", log);
		}
 	}
	/**
	 * 可以用的门
	 * 
	 * spotInfo 没有对应的associate_id  + associate_type ;
	 * 或者是associate_id 和 associate_type 等于传入的值
	 */
	@Override
	public boolean isCanUseSpot(long yc_id, int associate_type, long associate_id) throws Exception {
		try{
			DBRow spotInfp = floorStorageYardSpotZr.getSpotInfoBySdId(yc_id);
			if(spotInfp != null){
				long dataBaseAssociateId = spotInfp.get("associate_id", 0l);
				long dataBaseType = spotInfp.get("associate_type", 0);
 				if(dataBaseAssociateId <= 0l){
					return true ;
				}
				if(associate_type == dataBaseType && associate_id == dataBaseAssociateId){
					return true ;
				} 
			}
	 		return false;
		}catch(Exception e){
			throw new SystemException(e, "isCanUseSpot", log);
		}

	}
	
	/**
	 * 通过spotId获取spot详细信息
	 * @param yc_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午4:33:15
	 */
	public DBRow findSpotDetaiBySpotId(long yc_id) throws Exception {
		try{
			if(yc_id > 0)
			{
				return floorStorageYardSpotZr.getSpotInfoBySdId(yc_id);
			}
			return null;
		}catch(Exception e){
			throw new SystemException(e, "findSpotDetaiBySpotId", log);
		}
	}
	@Override
	public DBRow[] getUseSpotByAssociateIdAndType(long associate_id,
			int associate_type) throws Exception {
		try{
			return floorStorageYardSpotZr.getUseSpotByAssociateIdAndType(associate_id, associate_type);
		}catch(Exception e){
			throw new SystemException(e, "getUseSpotByAssociateIdAndType", log);
		}
 	}
@Override
	public void freeSpotsByAssociateId(long associate_id, int associate_type)
			throws Exception {
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("associate_id", 0);
			updateRow.add("associate_type", 0);
			updateRow.add("yc_status", CheckInSpotStatusTypeKey.RELEASED);  //要加日志
			floorStorageYardSpotZr.freeSpotsByAssociateId(associate_id,associate_type,updateRow);
		}catch(Exception e){
			throw new SystemException(e, "freeDoorsByAssociateId", log);
		}
	}
	/**
	 * 通过spotId、关联信息获取停车位信息
	 * @param spotId
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:13:36
	 */
	public DBRow findSpotsBySpotIdAssociateId(long spotId , int associate_type, long associate_id) throws Exception
	{
		try
		{
			return floorStorageYardSpotZr.findSpotsBySpotIdAssociateId(spotId, associate_type,associate_id);
		}
		catch(Exception e)
		{
			throw new SystemException(e, "findSpotsBySpotIdAssociateId", log);
		}
	}
	
	@Override
	public DBRow[] getCheckInModuleByAssociateId(long dlo_id, int associate_type) throws Exception {
		try{
			return floorStorageYardSpotZr.getSpotsCheckInModuleByAssociateId(dlo_id,associate_type);
		}catch(Exception e){
			throw new SystemException(e, "getCheckInModuleByAssociateId", log);
		}
 	}
	public void setFloorStorageYardSpotZr(
			FloorStorageYardControlZr floorStorageYardSpotZr) {
		this.floorStorageYardSpotZr = floorStorageYardSpotZr;
	}

	
}
