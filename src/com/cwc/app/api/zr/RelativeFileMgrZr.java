package com.cwc.app.api.zr;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zwb.UploadAndDownloadPDF;
import com.cwc.app.floor.api.zr.FloorRelativeFileMgrZr;
import com.cwc.app.iface.zr.RelativeFileMgrIfaceZr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class RelativeFileMgrZr implements RelativeFileMgrIfaceZr{

	static Logger log = Logger.getLogger("ACTION");
	
	public FloorRelativeFileMgrZr floorRelativeFileMgrZr ;
	
    private UploadAndDownloadPDF uploadAndDownloadPDF;
	
    private String upLoadFileUrl="";
	@Override
	public long addRelativeFile(DBRow row) throws Exception {
 		try{
 			row.add("create_time", DateUtil.NowStr());
 			return floorRelativeFileMgrZr.addRelativeFile(row);
 		}catch (Exception e) {
			throw new SystemException(e,"addRelativeFile",log);
		}
	}
	/**
	 * 	 因为有可能多次签名，那么多次签名的数据是没有saveOrUpdate的，都是存在数据库当中的。所以查询的时候会有多条的数据，那么返回Id最大的一条数据
	 */
	@Override
	public DBRow queryReativeFile(String relative_value , int type) throws Exception {
 		try{
 			DBRow[] rows = floorRelativeFileMgrZr.getRelativeFile(relative_value, type);
 			if(rows != null && rows.length > 0 ){
 				return rows[0];
 			}
 			return null ;
 		}catch (Exception e) {
			throw new SystemException(e,"queryReativeFile",log);
		}
	}
	/**
	 * 
	 * @param file_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public void updateFileWithType(String file_id) throws Exception{
		try{
 			floorRelativeFileMgrZr.updateFileWithType(file_id);
 		}catch (Exception e) {
			throw new SystemException(e,"updateFileWithType",log);
		}
	};
	/**
	 * 
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public void batchUploadSignature(HttpServletRequest request) throws Exception {
		//查询表中所有数据
		DBRow[] row=floorRelativeFileMgrZr.batchUploadSignature();
		String temp_path=null,file_id=null;
		String sessionId=request.getRequestedSessionId();
		if(row!=null&&row.length>0){
			for (DBRow dbRow : row) {
				String relative_file_path=dbRow.get("relative_file_path", null);temp_path=Environment.getHome().replace("\\", "/");
				if(!StringUtil.isBlank(relative_file_path)){
					temp_path=temp_path+relative_file_path;
					if(new File(temp_path).exists()){
						//step 1 上传
						file_id=uploadAndDownloadPDF.upLoadPDF(upLoadFileUrl, temp_path, sessionId);
						//step 2 更新
						if(!StringUtil.isBlank(file_id)){
							floorRelativeFileMgrZr.updateRelativeFilePath(dbRow.get("relative_id", null), file_id);
						}
					}
					
				}
			}
		}
		
	};
	
	public void setFloorRelativeFileMgrZr(FloorRelativeFileMgrZr floorRelativeFileMgrZr) {
		this.floorRelativeFileMgrZr = floorRelativeFileMgrZr;
	}
	
	public void setUploadAndDownloadPDF(UploadAndDownloadPDF uploadAndDownloadPDF) {
		this.uploadAndDownloadPDF = uploadAndDownloadPDF;
	}

	public void setUpLoadFileUrl(String upLoadFileUrl) {
		this.upLoadFileUrl = upLoadFileUrl;
	}
}
