package com.cwc.app.api.zr;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.jasper.compiler.JavacErrorDetail;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContextFactory;

import com.cwc.app.api.zr.ebay.ReadEbayItemTask;
import com.cwc.app.api.zr.ebay.UpdateEbayItemTask;
import com.cwc.app.floor.api.zr.FloorEbayMgrZr;
import com.cwc.app.iface.zr.CommonFileMgrIfaceZr;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.iface.zr.ebay.BaseSamplePoi;
import com.cwc.app.iface.zr.ebay.EbayPoiIfaceZr;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.model.BaseItem;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.AckKey;
import com.cwc.util.EbayContextUtil;
import com.ebay.sdk.ApiAccount;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.ApiException;
import com.ebay.sdk.SdkException;
import com.ebay.sdk.TimeFilter;
import com.ebay.sdk.eBayAccount;
import com.ebay.sdk.call.AddMemberMessageAAQToPartnerCall;
import com.ebay.sdk.call.FetchTokenCall;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.sdk.call.GetItemTransactionsCall;
import com.ebay.sdk.call.GetSessionIDCall;
import com.ebay.soap.eBLBaseComponents.AbstractResponseType;
import com.ebay.soap.eBLBaseComponents.AmountType;
import com.ebay.soap.eBLBaseComponents.BestOfferDetailsType;
import com.ebay.soap.eBLBaseComponents.BestOfferStatusCodeType;
import com.ebay.soap.eBLBaseComponents.BestOfferTypeCodeType;
import com.ebay.soap.eBLBaseComponents.BuyerPaymentMethodCodeType;
import com.ebay.soap.eBLBaseComponents.BuyerProtectionCodeType;
import com.ebay.soap.eBLBaseComponents.CategoryType;
import com.ebay.soap.eBLBaseComponents.CountryCodeType;
import com.ebay.soap.eBLBaseComponents.CurrencyCodeType;
import com.ebay.soap.eBLBaseComponents.DepositTypeCodeType;
import com.ebay.soap.eBLBaseComponents.DetailLevelCodeType;
import com.ebay.soap.eBLBaseComponents.DistanceType;
import com.ebay.soap.eBLBaseComponents.ErrorType;
import com.ebay.soap.eBLBaseComponents.HitCounterCodeType;
import com.ebay.soap.eBLBaseComponents.InternationalShippingServiceOptionsType;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityListType;
import com.ebay.soap.eBLBaseComponents.ItemCompatibilityType;
import com.ebay.soap.eBLBaseComponents.ItemPolicyViolationType;
import com.ebay.soap.eBLBaseComponents.ItemSpecificSourceCodeType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.ListingDetailsType;
import com.ebay.soap.eBLBaseComponents.ListingTypeCodeType;
import com.ebay.soap.eBLBaseComponents.MemberMessageType;
import com.ebay.soap.eBLBaseComponents.MessageTypeCodeType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.NameValueListType;
import com.ebay.soap.eBLBaseComponents.PaymentDetailsType;
import com.ebay.soap.eBLBaseComponents.QuestionTypeCodeType;
import com.ebay.soap.eBLBaseComponents.ReturnPolicyType;
import com.ebay.soap.eBLBaseComponents.SalesTaxType;
import com.ebay.soap.eBLBaseComponents.ShippingDetailsType;
import com.ebay.soap.eBLBaseComponents.ShippingServiceOptionsType;
import com.ebay.soap.eBLBaseComponents.ShippingTypeCodeType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.ebay.soap.eBLBaseComponents.TransactionType;
import com.ebay.soap.eBLBaseComponents.VariationType;
import com.ebay.soap.eBLBaseComponents.VariationsType;
import com.ebay.soap.eBLBaseComponents.WarningLevelCodeType;
import com.cwc.util.StringUtil;


public class EbayMgrZr implements EbayMgrZrIFace {
	
	private static Logger log = Logger.getLogger("ACTION");
	public static Logger logSuccess = Logger.getLogger("ebay_success");
	public static Logger logError = Logger.getLogger("ebay_error");
	public static Logger logMpn = Logger.getLogger("ebay_mpn");
	private static String finditemAdvancedUrl = "http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsAdvanced&SERVICE-VERSION=1.11.0&SECURITY-APPNAME=Visionar-8d75-480d-aa32-a66d3a89574b&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&outputSelector=SellerInfo";
	public static String baseFilePath; //=  Environment.getHome().replace("\\", "/")+"."+"/upl_excel_tmp";

	private static String url = "http://open.api.ebay.com/shopping?callname=GetSingleItem&responseencoding=JSON&appid=Visionar-8d75-480d-aa32-a66d3a89574b&siteid=0&version=515&IncludeSelector=Details,ItemSpecifics&ItemID=";// 获取seller_id
																																																					// 的时候用的URL
	private static String ebayUrl = "https://api.ebay.com/wsapi"; // 登录时候用的URL
	
	private FloorEbayMgrZr floorEbayMgrZr;
	private EbayPoiIfaceZr ebayPoiMgrZr;
	private BaseSamplePoi<BaseItem> customerItemPoiService ;
	private CommonFileMgrIfaceZr commonFileMgrZr;
	private OrderMgrIfaceZR orderMgrZr;
	
	public void setCommonFileMgrZr(CommonFileMgrIfaceZr commonFileMgrZr) {
		this.commonFileMgrZr = commonFileMgrZr;
	}

	public void setEbayPoiMgrZr(EbayPoiIfaceZr ebayPoiMgrZr) {
		this.ebayPoiMgrZr = ebayPoiMgrZr;
	}

	public void setCustomerItemPoiService(
			BaseSamplePoi<BaseItem> customerItemPoiService) {
		this.customerItemPoiService = customerItemPoiService;
	}


	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr)
	{
		this.orderMgrZr = orderMgrZr;
	}

	public void setFloorEbayMgrZr(FloorEbayMgrZr floorEbayMgrZr) {
		this.floorEbayMgrZr = floorEbayMgrZr;
	}
	
	
	@Override
	public Map<String,String> getSellerId(String itemNumber,long trade_item_id) throws Exception {
		try {
			Map<String,String> map = new HashMap<String, String>();
			PrintWriter out = null;
			BufferedReader in = null;
 
			URL realUrl = new URL(url + itemNumber);

			URLConnection conn = realUrl.openConnection();
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line = in.readLine();
			JSONObject jsonObj = JSONObject.fromObject(line);
			
			JSONObject  item = jsonObj.getJSONObject("Item");
			String result = item.getJSONObject("Seller").getString("UserID");
			
			if(item==null||!(result.length()>0))
			{
				throw new java.net.ConnectException();
			}
			
			
			JSONObject itemSpecifics =  item.getJSONObject("ItemSpecifics");
			String mpn = "";
			if(itemSpecifics != null){
				JSONArray  nameValueList  = itemSpecifics.getJSONArray("NameValueList");
				Iterator<JSONObject>  it = nameValueList.iterator();
				while(it.hasNext()){
					JSONObject obj = it.next();
					String name = obj.getString("Name");
					if(name != null){
						if(name.indexOf("MPN") != -1 || name.indexOf("Manufacturer Part Number") != -1){
							mpn = obj.getJSONArray("Value").get(0).toString();
 							logMpn.info(new TDate().getCurrentTime()+" trade_item_id:"+trade_item_id +"---itemNumber:"+itemNumber +"---MPN: " + mpn);
						}
					}
					  
		 
				}
				
			}
			in.close();
			map.put("sellerId", result);
			map.put("MPN", mpn);
			return map;
		} 
		catch (Exception e)
		{
			throw new Exception(e);
		}
	}

	@Override
	public DBRow getEbayTokenBySellerId(String sellerId) throws Exception {
		try {
			return floorEbayMgrZr.getEbayTokenBySellerId(sellerId);
		} catch (Exception e) {
			throw new SystemException(e, "getEbayTokenBySellerId", log);
		}
	}
	
	
		
	 
	
	// 时间问题的解决
	@Override
	public void updateTransactionsFeeSKU(String sellerId , String itemNumber , String  ebayTxnId ,long itemId , String mpn , long oid) throws Exception {
		try
		{
			boolean isNeedUpdateBuyerId = false;
			com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("orderMgr");
			DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
			if(detailOrder.get("auction_buyer_id", "").trim().length() < 1 ){
				//已经是Ebay的单子  && auction_buyer_id == "";
				isNeedUpdateBuyerId = true ;
			}
				
			ApiContext apiContext = new ApiContext(); 
			ApiCredential cred = apiContext.getApiCredential();  
			DBRow token = floorEbayMgrZr.getEbayTokenBySellerId(sellerId);
			if(token == null)
			{
				throw new RuntimeException("ebay_token is null" + "sellerId " + sellerId  + " oid :  " + oid  );
			}
			else
			{
				// 然后登录获取TransactionsFee
				cred.seteBayToken(token.getString("user_token"));
		        apiContext.setApiServerUrl(ebayUrl);      //正式环境的时候也需要修改     
		        apiContext.getApiLogging().setLogSOAPMessages(false);//Ebay交易信息是否输出
		        apiContext.setSite(SiteCodeType.US);
		       
		        GetItemTransactionsCall itemCall = new GetItemTransactionsCall(apiContext);
		        itemCall.setIncludeFinalValueFe(true);
		        itemCall.setIncludeFinalValueFee(true);
		        itemCall.setIncludeVariations(false);
		      
		        itemCall.setItemID(itemNumber);
		        itemCall.setTransactionID(ebayTxnId);
 
		        DetailLevelCodeType[] type = new DetailLevelCodeType[1];
		        type[0] = DetailLevelCodeType.ITEM_RETURN_ATTRIBUTES;
		        itemCall.setDetailLevel(type);
		        // 开始是现在时间的前一天
		         Calendar start = Calendar.getInstance();
		         
		        start.add(Calendar.DAY_OF_MONTH, -30);
		        Calendar end = Calendar.getInstance();
		        itemCall.setModifiedTimeFilter(new TimeFilter(start,end));
		        itemCall.setOutputSelector(new String[]{"Item.SKU,TransactionArray.Transaction.FinalValueFee,Item.Variations.Variation.SKU,TransactionArray.Transaction.Buyer"});		       
		        TransactionType[] arrayTypes = 	itemCall.getItemTransactions();
	 
		       
		    
		        if(arrayTypes == null)
		        {
		        	throw new java.net.ConnectException();
		        }
		       String buyerId = arrayTypes[0].getBuyer().getUserID() ;
		       AmountType amountType = arrayTypes[0].getFinalValueFee();
		       double FVF = amountType.getValue()  ;
		       String currency = amountType.getCurrencyID().value();
		       String sku = itemCall.getItem().getSKU();
		       DBRow row = new DBRow();
		       
		       row.add("final_value_fee", FVF);
		       row.add("sku", sku);
		       row.add("currency", currency);
 		       if(isNeedUpdateBuyerId){
		    	   orderMgr.modBuyerId(oid,buyerId);
		       }
		       floorEbayMgrZr.updateTradeItem(itemId, row);
		       
			}
			 
		}
		catch (Exception e) 
		{
//			throw new SystemException(e,"getTransactionsFee",log);
			throw new Exception(e);
		}
	}

	@Override
	public void getItemCall() throws Exception {
		 try{
			 	ApiContext apiContext = new ApiContext(); 
				ApiCredential cred = apiContext.getApiCredential(); 
				cred.seteBayToken("AgAAAA**AQAAAA**aArerteAAAA**1O+PTg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFlIKlCZGDqQWdj6x9nY+seQ**UDABAA**AAMAAA**ndT7Qcg9VHO9JGp0WtQ9qDGAxQ7iW5MH83c3x0meeGBAeC5kqWZMlbpzmqjwjZV/kjtYA3hM2WHxZeenz8CI+w6IokIYzE7uISHbqheaP8e7yBxB6zrkIGlLRVlySbJitE28OaRGi7zKAcqbSG0w+tElhaH7rC4pHKIQ4vj401Dh6X02FVqDQ2QzQTphWQoQxZVM7gEUZ2DHdMBMDWwbxu5Am8sUi1lXNxUP8VzjKObPBBBouTSldOvYV0L5UvV8Y0PmjnWjo+9Nwlb8YkWWndH0M0zEOPwPpqEKk77cWOdPykj5G4ocOA5lMMLtV+KkXsdZzO1JIXAldoU815a5VhMiN/9JfxC1jGXCUVX8F7U5XfyvAC0/eFNxeMg0gveOC55Ps3H68YR1jgGqKoyauY7lgKjZtfSbkpCbLcdFiwgly0LuhhnCaH6Sb5eFvs2k/zBBQQKuVCYe4pViADBx/02W+homam+y+yNtP0+BdxOg4VW4s2cj8dH5N2M4EB1aGGT3FhANLmLFNU1v1ANdhQX4oeDqqwgbwXqrhWVn7II0DI9oQ1gOSTVxX9Oyqan3wCm9+7WX6QLbPcAL480VGsZrLu6yKEQ4eUz5qx4uvmQhF0d60AoDopqlrkiXGX5Ozk09jXRM+1/8XniFNbZRXeDqbKADtJGSKza6vm/UM+12ua2jjtMd1IRUW+dJSv6qYeUMj6WwdAvGv5t5ppEYhTGXBSr4mxxzrH8g6KYTd9e2lQSP9Rvmxb58R9bI5f5x");
				apiContext.setApiServerUrl("https://api.ebay.com/wsapi");      //正式环境的时候也需要修改     
			    apiContext.getApiLogging().setLogSOAPMessages(true);//Ebay交易信息是否输出
			    apiContext.setSite(SiteCodeType.US);
			    GetItemCall itemCall = new GetItemCall(apiContext);
			    itemCall.setIncludeItemCompatibilityList(true);
			    itemCall.setItemID("110043671232");
			  
		 }catch (Exception e) {
				throw new SystemException(e,"getTransactionsFee",log);	 
		 }
		
	}

	 
	@Override
	public void getMoreInfoByItemNumbers(String[] itemNumbers, String filePath, String title,String token) throws Exception {
		try{
		 	ApiContext apiContext =  EbayContextUtil.getContext(token);
		    GetItemCall itemCall = new GetItemCall(apiContext);
		    itemCall.setIncludeItemCompatibilityList(true);
		    DetailLevelCodeType[] detailLevels = new DetailLevelCodeType[] {
		    		DetailLevelCodeType.RETURN_ALL,
		            DetailLevelCodeType.RETURN_SUMMARY,
		            DetailLevelCodeType.RETURN_MESSAGES,
		            DetailLevelCodeType.ITEM_RETURN_CATEGORIES
		    };
		    itemCall.setIncludeItemSpecifics(true);
		    itemCall.setIncludeItemCompatibilityList(true);
		    itemCall.setIncludeCrossPromotion(true);
		    itemCall.setDetailLevel(detailLevels);
		    itemCall.setIncludeItemSpecifics(true);
		    List<BaseItem> list= new ArrayList<BaseItem>();
		    for(String number : itemNumbers){
		    	 ItemType type = itemCall.getItem(number);
		    	 
		    	 // 检查是不是都返回的是Success.如果是的话才进行下面的操作
		    	 BaseItem ss =  new BaseItem(
			 				type.getItemID(),
			 				type.getTitle() , 
			 				type.getCountry(),
			 				type.getHitCounter(),
			 				type.getListingType(),
			 				type.getLocation(),
			 				type.getDispatchTimeMax() == null ? 0 : type.getDispatchTimeMax(),
			 				type.getListingDuration(),
			 				type.getSite(),
			 				type.getShipToLocations(),
			 				type.getPaymentMethods(),
			 				type.getPayPalEmailAddress(),
			 				type.getConditionID(),
			 				type.getConditionDisplayName(),
			 				type.getPrimaryCategory(),
			 				type.getItemSpecifics(),
			 				type.getVariations(),
			 				type.getItemCompatibilityList(),
			 				type.getShippingDetails(),
			 				type.getSecondaryCategory(),
			 				type.getQuantity(),
			 				type.isBestOfferEnabled()+"",
			 				type.isSkypeEnabled()+"",
			 				type.isAutoPay()+"",
			 				type.isDisableBuyerRequirements()+"",
			 				type.getSKU(),
			 				type.getPostalCode(),
			 				type.getWatchCount() + "",
			 				type.getDistance() ,
			 				type.getBuyerProtection(),
			 				type.getEBayNotes(),
			 				type.getFloorPrice(),
			 				type.getSubTitle(),
			 				type.getReservePrice(),
			 				type.getBuyItNowPrice(),
			 				type.getCeilingPrice(),
			 				type.getClassifiedAdPayPerLeadFee(),
			 				type.getGroupCategoryID(),
			 				type.getPaymentAllowedSite(),
			 				type.getProductListingDetails(),
			 				type.getStartPrice()
			 		);
		    	 ss.setItemPolicyViolation(type.getItemPolicyViolation());
		    	 ss.setBestOfferDetails(type.getBestOfferDetails());
		    	 ss.setListingDetailsType(type.getListingDetails());
		    	 ss.setSeller(type.getSeller());
			     ss.setReturnPolicy(type.getReturnPolicy());
			     ss.setPaymentDetailsType(type.getPaymentDetails());
		    	 list.add(ss);
		    }
		    
		    
		    customerItemPoiService.createXLS(list, filePath, title,"Items");
		}catch (Exception e) {
			logError.error(filePath + "get more ItemNumber info error");
			throw new SystemException(e,"getMoreInfoByItemNumbers",log);	 
		}
		
	}



	@Override
	public DBRow getMoreInfoByXML(String filePath, String title , String outPutFile,String loginSellerId)
			throws Exception {
		try{
			
			DBRow loginSellerRow = floorEbayMgrZr.getEbayTokenBySellerId(loginSellerId);
			if(loginSellerRow == null){
				throw new RuntimeException("Login Seller Id Error");
			}
			String[] itemNumbers = this.ebayPoiMgrZr.readXLSItemNumbers(filePath);
			if(itemNumbers != null && itemNumbers.length > 0 ){
				getMoreInfoByItemNumbers(itemNumbers, outPutFile, title,loginSellerRow.getString("user_token"));
			}else{
				
			}
			
		}catch (Exception e) {
			throw new SystemException(e,"getMoreInfoByXML",log);	 
		}
		return null;
	}


	
	/**
	 * 简化 每次读取50个单子
	 */
	@Override
	public DBRow findItemsAdvanced(String sellerId, String keywords,int startCount ,int endCount,
			String site , String fileName, String loginOnSellerId)  throws Exception {
		 int totalCount = 0;
		 DBRow tokenRow =  floorEbayMgrZr.getEbayTokenBySellerId(loginOnSellerId);
		 if(tokenRow == null){
			 throw new RuntimeException("Login On Seller Id Error.");
		 }
		 String token = tokenRow.getString("user_token");
		 try{
			 // 得到urls 计算从开始数量到结束数量的URLs 
			int count = endCount / 50 ;
			int index = startCount / 50;
			Map<String, Integer> urls = new HashMap<String, Integer>(); 
			for(    ; index <  count ; index++ ){
				urls.put(this.findItemsAdvancedGetUrl(keywords, sellerId, index + 1, 50), 50);
			}
			if(endCount - count * 50 > 0){
				urls.put(this.findItemsAdvancedGetUrl(keywords, sellerId, count + 1, 50), endCount - count * 50);
			}
			
			Map<JSONObject,Integer> map = new HashMap<JSONObject,Integer>();
			if(urls.size() > 0 ){
				Set<String> set = urls.keySet() ;
				
				for(String s : set){
					String url = s ;
					////system.out.println(s + " : " + urls.get(s).toString());
					int length = urls.get(s);
					map.put(findItemsAdvancedSendUrlAndReturnJson(s,site), length);
				}
			}
			String title = new StringBuffer(" Seller Id:").append(sellerId).append("  KeyWords :").append(keywords).append(" Start :"+ startCount  + "  End : " + endCount).append(" Site :"+site).toString();
			DBRow row = null;
			row = findItemsAdvancedHandle(map,title,fileName,token);
			totalCount = row.get("total", 0);
			if(totalCount == 0 ){
				row.add("flag", "error");
				row.add("message", "no data found");
			}else{
				row.add("flag", "success");
				row.add("total", totalCount);
				row.add("file", fileName);
			}
			return row;
		 }catch (Exception e) {
			 throw new SystemException(e,"findItemsAdvanced",log);
		}
	
	}
	
	
	private DBRow findItemsAdvancedHandle(Map<JSONObject,Integer> map , String title , String fileName,String token) throws Exception{
		DBRow result = new DBRow();
		try{
			if(map != null && map.size() > 0 ){
				Set<JSONObject> jsons = map.keySet();
				// 处理信息然后去取放在一个List<Model>里面
				 // 因为List的时候是有重复的单子的情况,那么就只能用set去装了
				 Set<String> itemNumberSet = new HashSet<String>();
			 
				 Set<Map.Entry<JSONObject, Integer>> entry = map.entrySet();
				 for(Map.Entry<JSONObject, Integer> e : entry){
					int totalCount = e.getValue();
					JSONArray array = e.getKey().getJSONArray("findItemsAdvancedResponse");
					JSONObject o = array.getJSONObject(0);
					String ack = o.getJSONArray("ack").getString(0);
					if(!ack.equals(AckKey.Success)){
						 
					}else{
						if(array.getJSONObject(0).getJSONArray("searchResult").getJSONObject(0).getLong("@count") > 0){
				 
						 Iterator<JSONObject>  it =  array.getJSONObject(0).getJSONArray("searchResult").getJSONObject(0).getJSONArray("item").listIterator();
		 
			  			 while(it.hasNext()){
			  				 /*if(itemNumberSet.size() > totalCount){
								 break;
							 }*/
							 JSONObject x = it.next();
							 String itemId =getValueFromJSONArray( x.getJSONArray("itemId")) ; 
					 
							 itemNumberSet.add(itemId);
						 }
						}
						
					}
					
				}
				 
				 result.add("total", itemNumberSet.size());
				 result.add("ids", StringUtil.convertStringArrayToString(itemNumberSet.toArray(new String[itemNumberSet.size()])) );
				 result.add("path", baseFilePath + fileName);
				 result.add("title", title);
				 result.add("token", token);
			//	 this.getMoreInfoByItemNumbers(itemNumberSet.toArray(new String[itemNumberSet.size()]),baseFilePath + fileName,title,token);
			//	return itemNumberSet.size();
			}
		//	return 0;
			return result;
			
		}catch (Exception e) {
			 throw new SystemException(e,"findItemsAdvancedHandle",log);
		}
	}
	private String getValueFromJSONArray(JSONArray  array){
		if(array != null){
			return array.get(0).toString();
		}
		return  "";
	}
	private JSONObject findItemsAdvancedSendUrlAndReturnJson(String url , String ebaySite) throws Exception{
		try{
			
			URL realUrl = new URL(url);
			BufferedReader in = null;
			URLConnection conn = realUrl.openConnection();
			HttpURLConnection httpURLConnection = (HttpURLConnection) realUrl.openConnection();
			// httpURLConnection.addRequestProperty("X-EBAY-SOA-GLOBAL-ID", ebaySite);
			in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			String line = in.readLine();
			in.close();

			logSuccess.info("send url : " + url + " success");
			JSONObject jsonObj = JSONObject.fromObject(line);
			return jsonObj;
		}catch (Exception e) {
			logError.error("send url : " + url + "error");
			e.printStackTrace();
			 throw new SystemException(e,"findItemsAdvancedSendUrlAndReturnJson",log);
		}
	 
		
	}
	
	
	private  String findItemsAdvancedGetUrl(String keyWord,String sellerId,int pageNumber , int prePageCount) throws Exception {
		
		StringBuffer str = new StringBuffer(this.finditemAdvancedUrl);
		if(keyWord != null && keyWord.trim().length() > 0 ){
			str.append("&keywords=").append(keyWord);
		}
		if(sellerId != null && sellerId.trim().length() > 0){
			str.append("&itemFilter(0).name=Seller&itemFilter(0).value=").append(sellerId);
		}
		pageNumber = pageNumber == 0 ? 1 : pageNumber;
		str.append("&paginationInput.pageNumber=").append(pageNumber);
		str.append("&paginationInput.entriesPerPage=").append(prePageCount);
		 
		if(StringUtil.isNull(sellerId) || StringUtil.isNull(keyWord)){
			throw new RuntimeException("seller 和 Key Word 不能同时为空");
		}
		 
		return  str.toString();
	}

	@Override
	public void getMoreInfoByItemNumbersAndNotAjax(String itemNumbers,
			String filePath, String title, String token, String userId)
			throws Exception {
			if(itemNumbers != null){
				 
				ScriptSession scriptSession = WebContextFactory.get().getScriptSession(); 
				String page = WebContextFactory.get().getCurrentPage();
				UserState userState = new UserState(userId,false);
				UserScriptSessionFilter filter = new  UserScriptSessionFilter(userState);
				scriptSession.setAttribute("userState", userState);
				Browser.withPageFiltered(page, filter,new ReadEbayItemTask(itemNumbers,token,filePath,title,userState));
			 }
	}
	@Override
	public void setReadStop(String userId) throws Exception {
		  Collection<ScriptSession> sessions =  Browser.getTargetSessions();
	        for(ScriptSession scriptSession : sessions) {
	        	Object object = scriptSession.getAttribute("userState");
	        	if(object != null){
	        		UserState userState = (UserState) object;
	        		if(userState.getUserId().equals(userId)){
	        			userState.setStop(true);
	        			 
	            		scriptSession.setAttribute("userState", userState);
	        		}
	        	
	        	}
	        }
		
	}

	@Override
	public BaseItem readEbayItemByNumberAndToken(String itemNumber, String token)
			throws Exception {
		try{ 	
			ApiContext apiContext =  EbayContextUtil.getContext(token);
		    GetItemCall itemCall = new GetItemCall(apiContext);
		    itemCall.setIncludeItemCompatibilityList(true);
		    DetailLevelCodeType[] detailLevels = new DetailLevelCodeType[] {
		    		DetailLevelCodeType.RETURN_ALL,
		            DetailLevelCodeType.RETURN_SUMMARY,
		            DetailLevelCodeType.RETURN_MESSAGES,
		            DetailLevelCodeType.ITEM_RETURN_CATEGORIES
		    };
		    itemCall.setIncludeItemSpecifics(true);
		    itemCall.setIncludeItemCompatibilityList(true);
		    itemCall.setIncludeCrossPromotion(true);
		    itemCall.setDetailLevel(detailLevels);
		    itemCall.setIncludeItemSpecifics(true);
		    ItemType type = itemCall.getItem(itemNumber);
		    	 
		    	 // 检查是不是都返回的是Success.如果是的话才进行下面的操作
	    	 BaseItem ss =  new BaseItem(
		 				type.getItemID(),
		 				type.getTitle() , 
		 				type.getCountry(),
		 				type.getHitCounter(),
		 				type.getListingType(),
		 				type.getLocation(),
		 				type.getDispatchTimeMax() == null ? 0 : type.getDispatchTimeMax(),
		 				type.getListingDuration(),
		 				type.getSite(),
		 				type.getShipToLocations(),
		 				type.getPaymentMethods(),
		 				type.getPayPalEmailAddress(),
		 				type.getConditionID(),
		 				type.getConditionDisplayName(),
		 				type.getPrimaryCategory(),
		 				type.getItemSpecifics(),
		 				type.getVariations(),
		 				type.getItemCompatibilityList(),
		 				type.getShippingDetails(),
		 				type.getSecondaryCategory(),
		 				type.getQuantity(),
		 				type.isBestOfferEnabled()+"",
		 				type.isSkypeEnabled()+"",
		 				type.isAutoPay()+"",
		 				type.isDisableBuyerRequirements()+"",
		 				type.getSKU(),
		 				type.getPostalCode(),
		 				type.getWatchCount() + "",
		 				type.getDistance() ,
		 				type.getBuyerProtection(),
		 				type.getEBayNotes(),
		 				type.getFloorPrice(),
		 				type.getSubTitle(),
		 				type.getReservePrice(),
		 				type.getBuyItNowPrice(),
		 				type.getCeilingPrice(),
		 				type.getClassifiedAdPayPerLeadFee(),
		 				type.getGroupCategoryID(),
		 				type.getPaymentAllowedSite(),
		 				type.getProductListingDetails(),
		 				type.getStartPrice()
		 		);
	    	 ss.setItemPolicyViolation(type.getItemPolicyViolation());
	    	 ss.setBestOfferDetails(type.getBestOfferDetails());
	    	 ss.setListingDetailsType(type.getListingDetails());
	    	 ss.setSeller(type.getSeller());
		     ss.setReturnPolicy(type.getReturnPolicy());
		     ss.setPaymentDetailsType(type.getPaymentDetails());
		   
		    
		   return ss;
		}catch (Exception e) {
			logError.error(itemNumber + "Read Error");
			throw new SystemException(e,"getMoreInfoByItemNumbers",log);	 
		}
	}

	@Override
	public void setUpdateEbayItem(String ids, String loginSellerId,
			String systemUserId) throws Exception {
		try{
			if(ids != null){
				 
				ScriptSession scriptSession = WebContextFactory.get().getScriptSession(); 
				String page = WebContextFactory.get().getCurrentPage();
				UserState userState = new UserState(systemUserId,false);
				UserScriptSessionFilter filter = new  UserScriptSessionFilter(userState);
		 
				scriptSession.setAttribute("userState", userState);
				Browser.withPageFiltered(page, filter,new UpdateEbayItemTask(ids,loginSellerId,userState));
			 
			 }
		}catch (Exception e) {
			throw new SystemException(e,"setUpdateEbayItem",log);
		}
		
	}

	@Override
	public List<ItemType> getItemTypeByFileId(String fileId) throws Exception {
		// TODO Auto-generated method stub
		try{
			if(fileId != null && fileId.length() > 0){
				CommonFileMgrIfaceZr commonFilebean = (CommonFileMgrIfaceZr)MvcUtil.getBeanFromContainer("commonFileMgrZr");  
				DBRow fileRow = commonFilebean.getCommonFileById(Long.parseLong(fileId));
				List<ItemType> arrayList = new ArrayList<ItemType>();
				if(fileRow != null){
					FileInputStream input = new FileInputStream(fileRow.getString("file_path"));
				 
					HSSFWorkbook wb = new HSSFWorkbook(input);
					HSSFSheet items =	wb.getSheet("Items");
					HSSFSheet variationSetSheet =	wb.getSheet("VariationsSet");
					HSSFSheet variationsSheet =	wb.getSheet("Variations");
					HSSFSheet itemSpcificsSheet =	wb.getSheet("ItemSpecifics");
					HSSFSheet shippingSheet =	wb.getSheet("shippingSheet");
					HSSFSheet compatibiltyList =	wb.getSheet("CompatibilityList");
				 
					
					int rowLength = items.getLastRowNum();
					
					for(int rowIndex = 2 ; rowIndex <= rowLength ; rowIndex++ ){
						ItemType item =  setItemBasicInfo(items.getRow(rowIndex));
						
						arrayList.add(item);
				
					}
					// 设置variations  和 variationSet
					if(variationSetSheet.getLastRowNum() > 0){
						setVariationsSet(arrayList,variationSetSheet,variationsSheet);
					}
					// 设置ItemSpcifics
					if(itemSpcificsSheet.getLastRowNum() > 0){
						setItemSpcifics(arrayList,itemSpcificsSheet);
					}
					if(shippingSheet.getLastRowNum() > 0){
						setShipping(arrayList, shippingSheet);
					}
					if(compatibiltyList.getLastRowNum() > 0){
						this.setCompatibiltyList(arrayList, compatibiltyList);
					}
				}
				return arrayList ;
			}
			return null;
		}catch (Exception e) {
			throw new SystemException(e,"getItemTypeByFileId",log);	
		}
	}
 
	private ItemType setItemBasicInfo( Row row) throws Exception { 
		ItemType item = new ItemType();
		 item.setCurrency( CurrencyCodeType.USD);
		 
		int cellLength = row.getLastCellNum();
		Cell itemNumberCell = row.getCell(0);
	  
		Cell titleCell = row.getCell(1);
		// baseInfo
		Cell quantityCell = row.getCell(2);
		Cell primaryCategoryCell = row.getCell(3);
		Cell secondaryCategoryCell = row.getCell(4);
		Cell locationCell = row.getCell(5);
		Cell countryCell = row.getCell(6);
		Cell skuCell = row.getCell(7);
		Cell postalCode = row.getCell(8);
		Cell hitCounter = row.getCell(9);
		Cell listingType = row.getCell(10);
		Cell dispatchTimeMaxCell = row.getCell(11);
		Cell listingDurationCell = row.getCell(12);
		Cell siteCell = row.getCell(13);
		Cell shipToLocationsCell = row.getCell(14);
		Cell paymentMethodCell = row.getCell(15);
		Cell payPalEmailAddressCell = row.getCell(16);
		Cell conditionIDCell = row.getCell(17);
		Cell conditionDisplayNameCell = row.getCell(18);
		
		
		Cell isBestOfferEnabledCell = row.getCell(19);
		Cell isSkypeEnabledCell = row.getCell(20);
		Cell isAutoPayCell = row.getCell(21);
		Cell isDisableBuyerRequirementsCell = row.getCell(22);
		Cell watchCountCell = row.getCell(23);
		Cell distanceCell = row.getCell(24);
		Cell buyerProtectionCell = row.getCell(25); 
		Cell ebayNotesCell = row.getCell(26);
		Cell floorPriceCell = row.getCell(27);
		Cell subTitleCell = row.getCell(28);
		Cell reservePriceCell = row.getCell(29);
		Cell buyItNowPriceCell = row.getCell(30);
		Cell ceilingPriceCell = row.getCell(31);
		Cell classifiedAdPayPerLeadFeeCell = row.getCell(32);
		Cell groupCategoryIDCell = row.getCell(33);
		Cell paymentAllowedSiteCell = row.getCell(34);
		Cell startPriceCell = row.getCell(35);
		 
		//returnPolicy 58 开始为returnPolicy
		 int readCellIndex = 57;
	 
		Cell refundOptionCell = row.getCell(readCellIndex++);
	 
		Cell refundCell = row.getCell(readCellIndex++);
	 
		Cell returnsWithinOptionCell = row.getCell(readCellIndex++);
		Cell returnsWithinCell = row.getCell(readCellIndex++);
		Cell returnsAcceptedOptionCell = row.getCell(readCellIndex++);
		Cell returnsAcceptedCell = row.getCell(readCellIndex++);
		Cell descriptionCell = row.getCell(readCellIndex++);
	 
		Cell shippingCostPaidByOptionCell = row.getCell(readCellIndex++);
		Cell shippingCostPaidByCell = row.getCell(readCellIndex++);
		Cell warrantyDurationCell = row.getCell(readCellIndex++);
		Cell warrantyDurationOptionCell = row.getCell(readCellIndex++);
		Cell warrantyOfferedCell = row.getCell(readCellIndex++);
		Cell warrantyOfferedOptionCell = row.getCell(readCellIndex++);
		Cell warrantyTypeCell = row.getCell(readCellIndex++);
		Cell warrantyTypeOptionCell = row.getCell(readCellIndex++);
		
		
		
		
		
		//ListingDetailsType
		Cell isAdultCell = row.getCell(readCellIndex++);
		Cell isBindingAuctionCell = row.getCell(readCellIndex++);
		Cell isBuyItNowAvailableCell  = row.getCell(readCellIndex++);
		Cell isCheckoutEnabledCell = row.getCell(readCellIndex++);
		Cell minimumBestOfferPriceCell = row.getCell(readCellIndex++);
		Cell bestOfferAutoAcceptPriceCell = row.getCell(readCellIndex++);
	 
		
		//bestOffer
		Cell bestOfferCell = row.getCell(readCellIndex++);
		Cell bestOfferCountCell = row.getCell(readCellIndex++);
		Cell bestOfferStatusCell = row.getCell(readCellIndex++);
		Cell bestOfferTypeCell = row.getCell(readCellIndex++);
		Cell bestOfferEnabledCell = row.getCell(readCellIndex++);
		 
		//itemPolicyViolationId
		Cell itemPolicyViolationIdCell = row.getCell(readCellIndex++);
		Cell itemPolicyViolationTextCell = row.getCell(readCellIndex++);
		
		
		
		//PaymentDetailsType
		Cell payMentDetailDaysToFullPaymentCell = row.getCell(readCellIndex++);
		Cell payMentDetailDepositAmountCell = row.getCell(readCellIndex++);
		Cell payMentDetailDepositTypeCell = row.getCell(readCellIndex++);
		Cell payMentHoursToDepositCell = row.getCell(readCellIndex++);
		
	 
 		item.setItemID(returnValueFromCell(itemNumberCell));
		item.setTitle(titleCell.getStringCellValue());
		item.setQuantity(covertDoubleToInt(quantityCell.getNumericCellValue()));
		
		CategoryType  primary = new CategoryType();
		primary.setCategoryID(returnValueFromCell(primaryCategoryCell).toString());
		item.setPrimaryCategory(primary);
		
		CategoryType  secondary = new CategoryType();
		String secondaryCategoryId = returnValueFromCell(secondaryCategoryCell).toString();
		if(secondaryCategoryId.length() > 0 ){
			secondary.setCategoryID(secondaryCategoryId);
			item.setSecondaryCategory(secondary);
		}
		
		item.setLocation(returnValueFromCell(locationCell).toString());
		item.setCountry(CountryCodeType.fromValue(returnValueFromCell(countryCell).toString()));
		
		item.setSKU(returnValueFromCell(skuCell).toString());
		
		item.setPostalCode(returnValueFromCell(postalCode).toString());
		item.setHitCounter(HitCounterCodeType.fromValue(returnValueFromCell(hitCounter).toString()));
		item.setListingType(ListingTypeCodeType.fromValue(returnValueFromCell(listingType).toString()));
		item.setDispatchTimeMax(covertDoubleToInt(dispatchTimeMaxCell.getNumericCellValue()));
		item.setListingDuration(returnValueFromCell(listingDurationCell).toString());
		item.setSite(SiteCodeType.fromValue(returnValueFromCell(siteCell).toString()));
		item.setShipToLocations(StringUtil.convertStringToStringArray(returnValueFromCell(shipToLocationsCell).toString()));
		
		// payment method 
		String[] paymentMethodArray = StringUtil.convertStringToStringArray(returnValueFromCell(paymentMethodCell).toString());
		BuyerPaymentMethodCodeType[] paymentMethodType = new BuyerPaymentMethodCodeType[paymentMethodArray.length];
		for(int index = 0 , count = paymentMethodArray.length ; index < count ; index++ ){
			paymentMethodType[index] = BuyerPaymentMethodCodeType.fromValue(paymentMethodArray[index]);
		}
		item.setPaymentMethods(paymentMethodType);
		
		item.setPayPalEmailAddress(returnValueFromCell(payPalEmailAddressCell).toString());
		
		item.setConditionID(covertDoubleToInt(conditionIDCell.getNumericCellValue()));
		item.setConditionDisplayName(returnValueFromCell(conditionDisplayNameCell).toString());
		String isBestOfferString = handleBoolean(isBestOfferEnabledCell);
		if(isBestOfferString.length() > 0){
			item.setBestOfferEnabled(isBestOfferString.equals("false")?false:true);
		}
		
		String isSkypeEnabledString = handleBoolean(isSkypeEnabledCell);
		if(isSkypeEnabledString.length() > 0){
			item.setSkypeEnabled(isSkypeEnabledString.equals("false")?false:true);
			}
		
		String isAutoPayString = handleBoolean(isAutoPayCell);
		if(isAutoPayString.length() > 0){
			item.setAutoPay(isAutoPayString.equals("false")?false:true);
			}
 
		
		String isDisableBuyerRequirementsString = handleBoolean(isDisableBuyerRequirementsCell);
		if(isDisableBuyerRequirementsString.length() > 0){
			item.setDisableBuyerRequirements(isDisableBuyerRequirementsString.equals("false")?false:true);
			}
		String distanceString =	returnValueFromCell(distanceCell);
		if(returnValueFromCell(distanceCell).length() > 0 && distanceString.split(",").length == 2){
			String[] array = distanceString.split(",");
			DistanceType distanceType = new DistanceType();
			distanceType.setDistanceUnit( array[0]);
			distanceType.setDistanceMeasurement(Integer.parseInt(array[0]));
			item.setDistance(distanceType);
		}
		item.setBuyerProtection(BuyerProtectionCodeType.fromValue(returnValueFromCell(buyerProtectionCell)));
		item.setEBayNotes(returnValueFromCell(ebayNotesCell));
	
		String floorPriceString = returnValueFromCell(floorPriceCell);
		AmountType floorPriceAmount = handAmountTypeString(floorPriceString);
		if(floorPriceAmount != null){
			item.setFloorPrice(floorPriceAmount);
		}
		
		item.setSubTitle(returnValueFromCell(subTitleCell));
		
		
		
		String reservePriceString = returnValueFromCell(reservePriceCell);
		AmountType reservePriceAmount = handAmountTypeString(reservePriceString);
		if(reservePriceAmount != null){
			item.setReservePrice(reservePriceAmount);
		}
		
		String buyItNowPriceString = returnValueFromCell(buyItNowPriceCell);
		AmountType buyItNowPriceAmount = handAmountTypeString(buyItNowPriceString);
		if(buyItNowPriceAmount != null){
			item.setBuyItNowPrice(buyItNowPriceAmount);
		}
		
		
		String ceilingPriceString = returnValueFromCell(ceilingPriceCell);
		AmountType ceilingPriceAmount = handAmountTypeString(ceilingPriceString);
		if(ceilingPriceAmount != null){
			item.setCeilingPrice(ceilingPriceAmount);
		}
		
		String classifiedAdPayPerLeadFeeString = returnValueFromCell(classifiedAdPayPerLeadFeeCell);
		AmountType classifiedAdPayPerLeadAmount = handAmountTypeString(classifiedAdPayPerLeadFeeString);
		if(classifiedAdPayPerLeadAmount != null){
			item.setClassifiedAdPayPerLeadFee(classifiedAdPayPerLeadAmount);
		}
		
		item.setGroupCategoryID(returnValueFromCell(groupCategoryIDCell));
		
		
		String paymentAllowedSiteString = returnValueFromCell(paymentAllowedSiteCell);
		if(paymentAllowedSiteString.length() > 0 && paymentAllowedSiteString.split(",").length > 0 ){
			String array[] = paymentAllowedSiteString.split(",");
			SiteCodeType codeType[] = new SiteCodeType[array.length];
			for(int index = 0 , count = array.length ; index < count ; index++ ){
					codeType[index] = SiteCodeType.fromValue(array[index]);
			}
			item.setPaymentAllowedSite(codeType);
		}
		
		String startPriceString = returnValueFromCell(startPriceCell);
		if(startPriceString.length() > 0){
			AmountType amount = handAmountTypeString(startPriceString);
			item.setStartPrice(amount);
		}
		 ReturnPolicyType  returnPolicy = new ReturnPolicyType();
		 
		
	 	String refundOptionString = returnValueFromCell(refundOptionCell);
 
		if(refundOptionString.length() > 0 ){
			 returnPolicy.setRefundOption(refundOptionString);
		}
		String refundCellString = returnValueFromCell(refundCell);
		if(refundCellString.length() > 0 ){
			 returnPolicy.setRefund(refundCellString);
		}
	 	 
	 
		String returnsWithinOptionCellString = returnValueFromCell(returnsWithinOptionCell);
		if(returnsWithinOptionCellString.length() > 0){
			returnPolicy.setReturnsWithinOption(returnsWithinOptionCellString);
		}
		String returnsWithinCellString = returnValueFromCell(returnsWithinCell) ;
		if(returnsWithinCellString.length() > 0){
			returnPolicy.setReturnsWithin(returnsWithinCellString);
		}
		String returnsAcceptedOptionCellString = returnValueFromCell(returnsAcceptedOptionCell);
		if(returnsAcceptedOptionCellString.length() > 0 ){
			returnPolicy.setReturnsAcceptedOption(returnsAcceptedOptionCellString);
		}
		String returnsAcceptedCellString = returnValueFromCell(returnsAcceptedCell);
		if(returnsAcceptedCellString.length() > 0){
			returnPolicy.setReturnsAccepted(returnsAcceptedCellString);
		}
		
		String descriptionCellString =  returnValueFromCell(descriptionCell);
		if(descriptionCellString.length() >0){
			returnPolicy.setDescription(descriptionCellString);
		}
		String shippingCostPaidByOptionCellString = returnValueFromCell(shippingCostPaidByOptionCell);
		if(shippingCostPaidByOptionCellString.length() > 0){
			returnPolicy.setShippingCostPaidByOption(shippingCostPaidByOptionCellString);
		}
		String shippingCostPaidByCellString = returnValueFromCell(shippingCostPaidByCell);
		if(shippingCostPaidByCellString.length() > 0){
			returnPolicy.setShippingCostPaidBy(shippingCostPaidByCellString);
		}
		String warrantyDurationCellString = returnValueFromCell(warrantyDurationCell);
		if(warrantyDurationCellString.length() > 0){
		 	returnPolicy.setWarrantyDuration(warrantyDurationCellString);
		}
		
		String warrantyDurationOptionCellString = returnValueFromCell(warrantyDurationOptionCell);
		if(warrantyDurationOptionCellString.length() > 0){
			returnPolicy.setWarrantyDurationOption(warrantyDurationOptionCellString);
		}
		
		String warrantyOfferedCellString = returnValueFromCell(warrantyOfferedCell);
		if(warrantyOfferedCellString.length() > 0){
		 	returnPolicy.setWarrantyOffered(warrantyOfferedCellString);
		}
		
		String warrantyOfferedOptionCellString = returnValueFromCell(warrantyOfferedOptionCell);
		if(warrantyOfferedOptionCellString.length() > 0){
			returnPolicy.setWarrantyOfferedOption(warrantyOfferedOptionCellString);
		}
		String warrantyTypeCellString = returnValueFromCell(warrantyTypeCell);
		if(warrantyTypeCellString.length() > 0){
			 returnPolicy.setWarrantyType(warrantyTypeCellString);
		}
		String warrantyTypeOptionCellString = returnValueFromCell(warrantyTypeOptionCell);
		if(warrantyTypeOptionCellString.length() > 0 ){
			returnPolicy.setWarrantyTypeOption(warrantyTypeOptionCellString);
		}  
 
		item.setReturnPolicy(returnPolicy);
		 
		
		//.setListingDetails( )
		ListingDetailsType listingDetailType = new ListingDetailsType();
		
		String isAdultCellString = handleBoolean(isAdultCell);
		if(isAdultCellString.length() > 0){
			listingDetailType.setAdult(isAdultCellString.equalsIgnoreCase("false") ?  false :true);
			}
		
		String isBindingAuctionString = handleBoolean(isBindingAuctionCell);
		if(isBindingAuctionString.length() > 0){
			listingDetailType.setBindingAuction(isBindingAuctionString.equalsIgnoreCase("false") ?  false :true);
			}
		
		String isBuyItNowAvailableString = handleBoolean(isBuyItNowAvailableCell);
		if(isBuyItNowAvailableString.length() > 0){
			listingDetailType.setBuyItNowAvailable(isBuyItNowAvailableString.equalsIgnoreCase("false") ?  false :true);
			}
		
		String isCheckoutEnabledString = handleBoolean(isCheckoutEnabledCell);
		if(isCheckoutEnabledString.length() > 0){
			listingDetailType.setCheckoutEnabled(isCheckoutEnabledString.equalsIgnoreCase("false") ?  false :true);
			}
		
		String minimumBestOfferPriceString = returnValueFromCell(minimumBestOfferPriceCell);
		AmountType minimumBestOfferPriceAmount = handAmountTypeString(minimumBestOfferPriceString);
		if(minimumBestOfferPriceAmount != null){
			listingDetailType.setMinimumBestOfferPrice(minimumBestOfferPriceAmount);
		} 
		
		String bestOfferAutoAcceptPriceString = returnValueFromCell(bestOfferAutoAcceptPriceCell);
		AmountType bestOfferAutoAcceptPriceAmount= handAmountTypeString(bestOfferAutoAcceptPriceString);
		if(bestOfferAutoAcceptPriceAmount != null){
			listingDetailType.setBestOfferAutoAcceptPrice(bestOfferAutoAcceptPriceAmount);
		}

		item.setListingDetails(listingDetailType);
		
		// bestOffer
		BestOfferDetailsType bestOfferDetail = new BestOfferDetailsType();
		String bestOfferString = returnValueFromCell(bestOfferCell);
		AmountType bestOfferAmount = handAmountTypeString(bestOfferString);
		if(bestOfferAmount != null){
			bestOfferDetail.setBestOffer(bestOfferAmount);
		}
		String bestOfferCountString = returnValueFromCell(bestOfferCountCell);
		if(bestOfferCountString.length() > 0 ){
			bestOfferDetail.setBestOfferCount(Integer.parseInt(bestOfferCountString));
		}
		String bestOfferStatusString = returnValueFromCell(bestOfferStatusCell);
		if(bestOfferStatusString.length() > 0 ){
			bestOfferDetail.setBestOfferStatus(BestOfferStatusCodeType.fromValue(bestOfferStatusString));
		}
		String bestOfferTypeString = returnValueFromCell(bestOfferTypeCell);
		if(bestOfferTypeString.length() > 0){
			
			bestOfferDetail.setBestOfferType(BestOfferTypeCodeType .fromValue(bestOfferTypeString));
		}
		String bestOfferEnabledString = handleBoolean(bestOfferEnabledCell);
		if(bestOfferEnabledString.length() > 0 ){
			bestOfferDetail.setBestOfferEnabled(bestOfferEnabledString.equalsIgnoreCase("false") ?  false : true);
		}
		item.setBestOfferDetails(bestOfferDetail);
		
		ItemPolicyViolationType  itemPolicyViolationType = new ItemPolicyViolationType();
		String policyIdString  = returnValueFromCell(itemPolicyViolationIdCell);
		if(policyIdString.length() > 0 ){
			itemPolicyViolationType.setPolicyID(Long.parseLong(policyIdString));
		}
		String policyText = returnValueFromCell(itemPolicyViolationTextCell);
		if(policyText.length() > 0 ){
			itemPolicyViolationType.setPolicyText(policyText);
		}
		item.setItemPolicyViolation(itemPolicyViolationType);
		
		
		//PaymentDetailsType
		PaymentDetailsType paymentDetailsType = new PaymentDetailsType();
		String payMentDetailDaysToFullPaymentString = returnValueFromCell(payMentDetailDaysToFullPaymentCell);
		if(payMentDetailDaysToFullPaymentString.length() > 0 ){
			paymentDetailsType.setDaysToFullPayment(Integer.parseInt(payMentDetailDaysToFullPaymentString));
		}
		String payMentDetailDepositAmountString = returnValueFromCell(payMentDetailDepositAmountCell);
		AmountType payMentDetailDepositAmount = handAmountTypeString(payMentDetailDepositAmountString);
		if(payMentDetailDepositAmount != null){
			paymentDetailsType.setDepositAmount(payMentDetailDepositAmount);
		}
		String payMentDetailDepositTypeString = returnValueFromCell(payMentDetailDepositTypeCell);
		if(payMentDetailDepositTypeString.length() > 0){
			paymentDetailsType.setDepositType(DepositTypeCodeType.fromValue(payMentDetailDepositTypeString));
		}
		String payMentHoursToDepositString = returnValueFromCell(payMentHoursToDepositCell);
		if(payMentHoursToDepositString.length() > 0 ){
			paymentDetailsType.setHoursToDeposit(Integer.parseInt(payMentHoursToDepositString));
		}
		item.setPaymentDetails(paymentDetailsType);
		return item;
 
	}

	public void setVariationsSet(List<ItemType> arrayList , Sheet variationSetSheet , Sheet variationsSheet) throws Exception{
		try{
			/**
			 * 1.找到对应的RowIndex.
			 * 2.读取RowIndex的内容然后填充值.
			 */ 
			int rowLength = variationSetSheet.getLastRowNum();
			int rowLengthOfVariation =  variationsSheet.getLastRowNum();
			
			for(ItemType type : arrayList){
				VariationsType variationsType = new VariationsType();
				String itemNumberInType = type.getItemID();
				int fixRowIndex = -1 ;
			 
				// 找到VariationsSet 中匹配的位置
				for(int index = 1 ; index <= rowLength ; index++ ){
					Row row = variationSetSheet.getRow(index);
					String itemNumber = (returnValueFromCell(row.getCell(0)));
					if(itemNumberInType.equals(itemNumber)){
						fixRowIndex = index;
						break;
					}
				}
				// 找到Variations 中匹配的位置
				List<Row> variationsList = new ArrayList<Row>();
				for(int index = 1 ; index <= rowLengthOfVariation ; index++ ){
					Row row = variationsSheet.getRow(index);
					String itemNumber = (returnValueFromCell(row.getCell(0)));
					if(itemNumberInType.equals(itemNumber)){
						variationsList.add(row);
					}
				}
				
				if(fixRowIndex != -1){
					Row row = variationSetSheet.getRow(fixRowIndex);
					int cellLength = row.getLastCellNum();
					int arrayLength =( cellLength - 2 ) / 2;
					String[] names = new String[arrayLength];
					String[] values = new String[arrayLength];
				 
					for(int index =  2 , count = cellLength ; index < count ; index+=2 ){
						names[(index - 2) / 2] = returnValueFromCell(row.getCell(index));
						values[(index - 2) / 2] = returnValueFromCell(row.getCell(index+1));
					}
					NameValueListArrayType nameValueList =  handleNameValueListArrayType(names,values,true,"/,");
					if(nameValueList != null){
						variationsType.setVariationSpecificsSet(nameValueList);
					}
 				}
				if(variationsList.size() > 0 ){
					VariationType[] types = new VariationType[variationsList.size()];
					int rowIndex = 0;
					for(Row row : variationsList){
			
						VariationType temp = new VariationType();
						int cellLength = row.getLastCellNum();
						temp.setSKU(returnValueFromCell(row.getCell(2)));
						AmountType amountType = new AmountType();
						amountType.setCurrencyID(CurrencyCodeType.fromValue(returnValueFromCell(row.getCell(3))));
						amountType.setValue(Double.parseDouble(returnValueFromCell(row.getCell(4))));
						temp.setStartPrice(amountType);
						temp.setQuantity(Integer.parseInt(returnValueFromCell(row.getCell(5))));
						String[] names = new String[(cellLength - 6) / 2];
						String[] values  = new String[(cellLength - 6) / 2];
						for(int index = 6 , count = cellLength ; index < count ; index+=2 ){
							names[(index - 6) / 2] = returnValueFromCell(row.getCell(index));
							values[(index - 6) / 2] = returnValueFromCell(row.getCell(index+1));
						
						}
						NameValueListArrayType nameValue = handleNameValueListArrayType(names , values,true,"/,");
						if(nameValue != null){
							temp.setVariationSpecifics(nameValue);
						}
						types[rowIndex] = temp;
						rowIndex++;
					}
					variationsType.setVariation(types);
				}	
				type.setVariations(variationsType);
			}
			
			
		}catch (Exception e) {
			throw new SystemException(e,"setItemBasicInfo",log);	
		}
	}
	private NameValueListArrayType handleNameValueListArrayType(String[] names , String[] values , boolean flag , String split)  throws Exception{
		try{
			if(names.length != values.length){
				return null;
			}
			NameValueListArrayType nameValueList = new NameValueListArrayType();
			NameValueListType[] listTypeArray = new NameValueListType[names.length];
			for(int index = 0 , count = names.length ; index < count ; index++ ){
				NameValueListType temp = new NameValueListType();
				temp.setName(names[index]);
				if(flag){
					String[] valueArray = values[index].split(split);
					 
					temp.setValue(valueArray);
				}else{
					String[] valueArray = new String[1];
					valueArray[0] = values[index];
					temp.setValue(valueArray);
				}
				temp.setSource(ItemSpecificSourceCodeType.ITEM_SPECIFIC);
				listTypeArray[index] = temp;
			}
			nameValueList.setNameValueList(listTypeArray);
			return nameValueList;
		}catch (Exception e) {
			throw new SystemException(e,"handleNameValueListArrayType",log);	
		}
		 
		
		
	}

	private String returnValueFromCell(Cell cell) {
		Object value = null;
		try{
			value =  cell.getStringCellValue();
		 
		}catch (Exception e) {
			try {
				value = cell.getNumericCellValue();
				if((value+"").indexOf("E") != -1){
					BigDecimal b = new BigDecimal(value+"");     
					value = b.toPlainString();
				}else{
					value = covertDoubleToInt(Double.parseDouble(value+""));
				}
			} catch (Exception e_) {
				value = "";
			}
			
		}finally{
			if(value == null) value = "";
		}
		 
		return value.toString();
	}
	private int covertDoubleToInt(double value){
		return (int)Math.ceil(value);
	}
	private AmountType handAmountTypeString(String value) {
		if(value.length() > 0 && value.split(",").length == 2){
			String[] array = value.split(",");
			AmountType amount = new AmountType();
			amount.setCurrencyID(CurrencyCodeType.fromValue(array[0]));
			amount.setValue(Double.parseDouble(array[1]));
			return  amount;
		}
		return null;
	}
	private String handleBoolean(Cell cell){
		String value = returnValueFromCell(cell).toString();
		if(value.equalsIgnoreCase("null") || value.length() < 1){
			return "";
		}else {
			return value;
		}
	}
	
	public void setItemSpcifics(List<ItemType> arrayList , Sheet itemSpcifics ) throws Exception {
		try{
			int rowLength = itemSpcifics.getLastRowNum();
			if(rowLength < 1){
				return ;
			}
			for(ItemType type : arrayList){
				String itemNumberInType = type.getItemID();
				Row temp = null;
				for(int index = 0 , count = rowLength ; index <= count ; index++ ){
					temp = itemSpcifics.getRow(index);
					if(returnValueFromCell(temp.getCell(0)).equals(itemNumberInType)){
						break ;
					}
				}
				if(temp != null){
					// 设置setItemSpcifics
					int cellLength = temp.getLastCellNum();
					String[] names = new String[(cellLength-2) / 2];
					String[] values = new String[(cellLength-2) / 2];
					for(int index = 2 , count = cellLength; index < count ; index+=2 ){
						names[(index - 2) / 2] = returnValueFromCell(temp.getCell(index));
						values[(index - 2) / 2] = returnValueFromCell(temp.getCell(index+1));
					}
					NameValueListArrayType nameValue = handleNameValueListArrayType(names,values,true,",");
					if(nameValue != null){
						NameValueListType[]  listNamesType = nameValue.getNameValueList();
						 
						type.setItemSpecifics(nameValue);
					}
				}
			}
		}catch (Exception e) {
			throw new SystemException(e,"setItemSpcifics",log);
	 
		}
	}
	private void setShipping(List<ItemType> arrayList , Sheet shippingSheet) throws Exception{
		try{
			int rowLength = shippingSheet.getLastRowNum();
			if(rowLength <= 0){
				return ;
			}
			for(ItemType type : arrayList){
				String itemNumberInType = type.getItemID();
				Row temp = null ;
			 
				for(int index =0 , count = rowLength ; index <= count ; index++ ){
					temp = shippingSheet.getRow(index);
					if(returnValueFromCell(temp.getCell(0)).equals(itemNumberInType)){
						break ;
					}
					temp = null ;
				}
				if(temp != null){
					int cellLength = temp.getLastCellNum();
					ShippingDetailsType shippingDetailType = new ShippingDetailsType();
				//	shippingDetailType.sets
					Map<String,String> ShippingServiceOptionsType = new HashMap<String,String>();
					Map<String,String> InternationalOptionsType = new HashMap<String,String>();
					Map<String,String> SalesTaxTypeTypeMap = new HashMap<String,String>();
					int j = 2 ;
					// 读取 ShippingServiceOptionsType,InternationalShipping
					for( int count = cellLength ; j < count ; j+= 2  ){
						String name = returnValueFromCell(temp.getCell(j));
						if(name.startsWith("ShippingServiceOptionsType")){
 							String perproty = name.substring(name.indexOf(".")+1) ; 
							ShippingServiceOptionsType.put(perproty, returnValueFromCell(temp.getCell(j+1)));
						}else if(name.startsWith("InternationalShipping")){
							String perproty = name.substring(name.indexOf(".")+1) ; 
							InternationalOptionsType.put(perproty, returnValueFromCell(temp.getCell(j+1)));
						} else if(name.startsWith("SalesTaxType")){
							String perproty = name.substring(name.indexOf(".")+1);
							SalesTaxTypeTypeMap.put(perproty, returnValueFromCell(temp.getCell(j+1)));
						}
						if(!(name.startsWith("SalesTaxType") || name.startsWith("ShippingServiceOptionsType") || name.startsWith("InternationalShipping"))){
							break ;
						}
					}
					// 设置其他的值
					for(;j < cellLength ; j++ ){
						String name = returnValueFromCell(temp.getCell(j));
						if(name.length() > 0 ){
							if(name.startsWith("ShippingType")){
								String value = returnValueFromCell(temp.getCell(j+1));	
								if(value.length() > 0){
									shippingDetailType.setShippingType(ShippingTypeCodeType.fromValue(value.trim()));
								}
							}
							if(name.startsWith("ExcludeShipToLocation")){
								String value = returnValueFromCell(temp.getCell(j+1));	
								if(value != null && value.length() > 0 && value.split(",").length > 0){
									shippingDetailType.setExcludeShipToLocation(value.split(",")); 
								}
							}
							if(name.startsWith("PaymentInstructions")){
								String value = returnValueFromCell(temp.getCell(j+1));
								if(value != null && value.length() > 0){
									shippingDetailType.setPaymentInstructions(value);
								}
							}
						}
					}
					// 设置SalesTaxType 
					if(SalesTaxTypeTypeMap.size() > 0){
						SalesTaxType salesTaxType = new SalesTaxType();
						
						String salesTaxState = (SalesTaxTypeTypeMap.get("SalesTaxState"));
						String shippingIncludedInTax = SalesTaxTypeTypeMap.get("ShippingIncludedInTax");
						String salesTaxPercent = SalesTaxTypeTypeMap.get("SalesTaxPercent");
						String salesTaxAmount = SalesTaxTypeTypeMap.get("SalesTaxAmount");
						AmountType amount = handAmountTypeString(salesTaxAmount);
						if(amount != null){
							salesTaxType.setSalesTaxAmount(amount);
						}
						if(salesTaxPercent.length() > 0){
							salesTaxType.setSalesTaxPercent(Float.parseFloat(salesTaxPercent.trim()));
						}
						if(salesTaxState.length() > 0){
							salesTaxType.setSalesTaxState(salesTaxState);
						}
						if(shippingIncludedInTax.length() > 0 ){
							salesTaxType.setShippingIncludedInTax(shippingIncludedInTax.trim().equalsIgnoreCase("false")?false: true);
						}
						shippingDetailType.setSalesTax(salesTaxType);
					}
					 
					// 设置shippingDetail
					if(ShippingServiceOptionsType.size() > 0 ){
						int length = ShippingServiceOptionsType.size() / 5;
						ShippingServiceOptionsType[] arrayShippingType = new ShippingServiceOptionsType[length];
						for(int index = 0 , count = length ; index < count ; index++ ){
							//{ShippingServiceCost2=USD,0.0, ShippingService1=FedExHomeDelivery, ShippingService2=Pickup, ShippingTimeMax1=5, ShippingServicePriority2=2, ShippingServicePriority1=1, ShippingServiceCost1=USD,0.0, ShippingServiceAdditionalCost1=USD,0.0, ShippingServiceAdditionalCost2=USD,0.0, ShippingTimeMin1=1}

							ShippingServiceOptionsType typeTemp = new ShippingServiceOptionsType();
							String ShippingServiceAdditionalCostString = ShippingServiceOptionsType.get("ShippingServiceAdditionalCost" + (index+1));
							String shippingServiceCostString = ShippingServiceOptionsType.get("ShippingServiceCost"+(index+1));
							String ShippingServicePriorityString = ShippingServiceOptionsType.get("ShippingServicePriority"+(index+1));
							String shippingTimeMinString = ShippingServiceOptionsType.get("ShippingTimeMin" + (index+1));
							String ShippingTimeMaxString = ShippingServiceOptionsType.get("ShippingTimeMax" + (index+1));
							typeTemp.setShippingService(ShippingServiceOptionsType.get("ShippingService"+(index+1)).toString());
							AmountType amount = handAmountTypeString(shippingServiceCostString);
						 
							if(amount != null){
								typeTemp.setShippingServiceCost(amount);
							}
							if(ShippingServiceAdditionalCostString != null && ShippingServiceAdditionalCostString.length() > 0){
								AmountType amountTemp = handAmountTypeString(ShippingServiceAdditionalCostString);
								if(amountTemp != null){
									typeTemp.setShippingServiceAdditionalCost(amountTemp);
								}
							}
							if(ShippingServicePriorityString != null && ShippingServicePriorityString.length() > 0){
								typeTemp.setShippingServicePriority(Integer.parseInt(ShippingServicePriorityString));
							}

							if(shippingTimeMinString != null && shippingTimeMinString.length() > 0 ){
								typeTemp.setShippingTimeMin(Integer.parseInt(shippingTimeMinString));
							}
							if(ShippingTimeMaxString != null && ShippingTimeMaxString.length() > 0 ){
								typeTemp.setShippingTimeMin(Integer.parseInt(ShippingTimeMaxString));
							}
							arrayShippingType[index] = typeTemp;
						}
						shippingDetailType.setShippingServiceOptions(arrayShippingType);
						
					}
					
					if(InternationalOptionsType.size() > 0 ){
						int length = InternationalOptionsType.size() / 4;
						InternationalShippingServiceOptionsType[] arrayType = new InternationalShippingServiceOptionsType[length];
						for(int index = 0 , count = length ; index < count ; index++ ){
							InternationalShippingServiceOptionsType typeTemp = new InternationalShippingServiceOptionsType();
							typeTemp.setShippingService(InternationalOptionsType.get("ShippingService"+(index+1)).toString());
							String shippingServiceCostString = InternationalOptionsType.get("ShippingServiceCost"+(index+1));
							String ShippingServicePriorityString = InternationalOptionsType.get("ShippingServicePriority"+(index+1));
							String ShipToLocation = InternationalOptionsType.get("ShipToLocation"+(index+1));
							AmountType amount = handAmountTypeString(shippingServiceCostString);
							if(amount != null){
								typeTemp.setShippingServiceCost(amount);
							}
							if(ShippingServicePriorityString.length() > 0){
								typeTemp.setShippingServicePriority(Integer.parseInt(ShippingServicePriorityString));
							}
						
							if(ShipToLocation.length() > 0 ){
								String[] array = ShipToLocation.split(",");
								typeTemp.setShipToLocation(array);
							}
							arrayType[index] = typeTemp;
						}
						shippingDetailType.setInternationalShippingServiceOption(arrayType);
					}
				 
					type.setShippingDetails(shippingDetailType);
				 
			 
				}
			
			}
		}catch (Exception e) {
			throw new SystemException(e,"setItemSpcifics",log);
		}
	}
	public void setCompatibiltyList(List<ItemType> arrayList , Sheet compatibiltyList) throws Exception{
		try{
			
			
			 
 			int rowLength = compatibiltyList.getLastRowNum();
			if(rowLength <= 0){
				return ;
			}
 
			for(ItemType type : arrayList ){
				ItemCompatibilityListType  listType = new ItemCompatibilityListType ();
				
				List<Row> arrayListRow = new ArrayList<Row>();
				String itemNumberInType = type.getItemID();
			 
				for(int index = 1; index < rowLength ;  index++ ){
					
					 Row tempRow = compatibiltyList.getRow(index);
					 if(returnValueFromCell(tempRow.getCell(0)).trim().equals(itemNumberInType) ){
						 arrayListRow.add(tempRow);
					 }
				}
			 
				if(arrayListRow.size() > 0){
					ItemCompatibilityType[] itemCompatibilityType = new ItemCompatibilityType[arrayListRow.size()];
					for(int index = 0 , count = arrayListRow.size() ; index < count ; index++ ){
						ItemCompatibilityType itemCompatibilityTemp = new ItemCompatibilityType();
						Row row = arrayListRow.get(index);
						itemCompatibilityTemp.setCompatibilityNotes(returnValueFromCell(row.getCell(2)) );
						int lastCellLength = row.getLastCellNum();
			 
						String[] names = new String[(lastCellLength - 3)/2];
						String[] values = new String[(lastCellLength - 3)/2];
						for(int cellIndex = 3 , cellCount = lastCellLength ;  cellIndex < cellCount ; cellIndex+=2 ){
							names[(cellIndex - 3)/2] = returnValueFromCell(row.getCell(cellIndex));
							values[(cellIndex - 3)/2]  = returnValueFromCell(row.getCell(cellIndex+1));
						}
						NameValueListType[] listTypeArray = new NameValueListType[names.length];
						for(int j = 0 , length = names.length ; j < length ; j++ ){
							NameValueListType temp = new NameValueListType();
							temp.setName(names[j]);
							String[] valueArray = new String[1];
							valueArray[0] = values[j];
							temp.setValue(valueArray);
							temp.setSource(ItemSpecificSourceCodeType.ITEM_SPECIFIC);
							listTypeArray[j] = temp;
						}
						itemCompatibilityTemp.setNameValueList(listTypeArray);
					 
						itemCompatibilityType[index] = itemCompatibilityTemp;
						 
					}
					 
					listType.setCompatibility(itemCompatibilityType);
				 
				}
				type.setItemCompatibilityList(listType);
			}
			
		}catch (Exception e) {
			throw new SystemException(e,"setCompatibiltyList",log);
		}
		
		
	}

	@Override
	public void updateTradeItem(long trade_item_id, DBRow row) throws Exception {
		try{
			floorEbayMgrZr.updateTradeItem(trade_item_id, row);
		}catch (Exception e) {
			throw new SystemException(e,"updateTradeItem",log);
		}
	}

	@Override
	public DBRow sendEbayMessage(long oid , String content  )
			throws Exception
	{
		DBRow order = orderMgrZr.getDetailById(oid, "porder", "oid");
		DBRow result = new DBRow();
		try{
			String seller_id =  order.getString("seller_id");
			String item_number = order.getString("item_number");
			if(item_number.split(",").length > 1)
			{
			  item_number = item_number.split(",")[0];
			}
			
			String buyer_id =  order.getString("auction_buyer_id");
			String itemName =  order.getString("item_name");
			String subject = "About " +  itemName+ " Question";
			String token =  this.getEbayTokenBySellerId(seller_id).getString("user_token");
			ApiContext context = EbayContextUtil.getContext(token);
		 
			
			AddMemberMessageAAQToPartnerCall call = new AddMemberMessageAAQToPartnerCall(context);
			call.setItemID(item_number);
			call.setWarningLevel(WarningLevelCodeType.HIGH);
		 
			MemberMessageType memberMessage = new MemberMessageType();
			memberMessage.setBody(content);
			memberMessage.setSubject(subject);
			memberMessage.setMessageType(MessageTypeCodeType.CONTACT_EBAY_MEMBER);
			memberMessage.setQuestionType(QuestionTypeCodeType.GENERAL);
			memberMessage.setRecipientID(new String[]{buyer_id});
			call.setMemberMessage(memberMessage);
			call.addMemberMessageAAQToPartner();
			AbstractResponseType  type = call.getResponseObject();
			
			if(type.getAck().toString().toLowerCase().equals("success")){
				result.add("flag", "success");
			}else{
				result.add("flag", "error");
				ErrorType[] errors = type.getErrors();
				StringBuffer error = new StringBuffer("");
				if(errors != null && errors.length > 0 ){
					for(int index = 0 , count = errors.length ; index < count ; index++ ){
						error.append(errors[index].getShortMessage()+"<br />");
					}
					result.add("message", error.toString());
				}
			}
			return result;
		
		}catch (Exception e) {
		 
			throw new SystemException(e,"sendEbayMessage",log);
		}
		 
	}

	@Override
	public long addEbayAccount( String user_id,
			String user_token, String password) throws Exception
	{
 		try{
			DBRow row = new DBRow() ;
			row.add("user_id", user_id);
			row.add("user_token", user_token);
			row.add("password", password);
			return floorEbayMgrZr.addEbayAccount(row);
		}catch (Exception e) {
			throw new SystemException(e,"addEbayAccount",log);
		}
 
	}

	@Override
	public DBRow[] getAllEbayAccount() throws Exception
	{
		try{
			return floorEbayMgrZr.getAllEbayAccount();
		}catch (Exception e) {
			throw new SystemException(e,"getAllEbayAccount",log);
		}
	}

	@Override
	public void updateEbayAccount(long ebay_account_id, String user_id,
			String user_token , String password) throws Exception
	{
		try{
			DBRow row = new DBRow() ;
			row.add("user_id", user_id);
			row.add("user_token", user_token);
			row.add("password", password);
			 floorEbayMgrZr.updateEbayAccount(ebay_account_id, row);
		}catch (Exception e) {
			throw new SystemException(e,"getAllEbayAccount",log);
		}
	}

	@Override
	public DBRow getEbayAccountById(long ebay_account_id) throws Exception
	{
		try{
			return floorEbayMgrZr.getEbayAccountById(ebay_account_id);
		}catch (Exception e) {
			throw new SystemException(e,"getEbayAccountById",log);
		}
	}
	/**
	 * 先在sandbox下测试
	 */
	@Override
	public DBRow getSessionId() throws Exception {
		try{
			String apiServerUrl = "https://api.sandbox.ebay.com/wsapi";
			String ru_name = "vvme-vvme7e8ef-0503--afjlvmzhi" ;
			String developer = "b8a31621-ff06-416e-a903-38f665f67499";
			String application = "vvme7e8ef-0503-4fb6-93eb-bf6638d5c46";
			String certificate = "bc23194f-ac55-4a43-8210-59d4f381a752";
			ApiContext apiContext = new ApiContext();  
	        apiContext.setApiServerUrl(apiServerUrl);  
	        ApiCredential credential = new ApiCredential();  
	        ApiAccount account = new ApiAccount();  
	        account.setApplication(application);  
	        account.setDeveloper(developer);  
	        account.setCertificate(certificate);  
	        credential.setApiAccount(account);  
	        apiContext.setApiCredential(credential);  
	        GetSessionIDCall call = new GetSessionIDCall();  
	        apiContext.getApiLogging().setLogSOAPMessages(false);//Ebay交易信息是否输出
	        call.setApiContext(apiContext);  
	        call.setRuName(ru_name);  
	        String sessionID = call.getSessionID();  
	        DBRow result = new DBRow();
	        result.add("session_id", sessionID);
	        result.add("ru_name", ru_name);
	        return result;  
		}catch (Exception e) {
			throw new SystemException(e,"getSessionId",log);
		}
	}

	@Override
	public DBRow getEbayTokenAndUpdateEbayToken(long ebay_account_id,
			String session_id, String run_name) throws Exception  
	{
		DBRow result = new DBRow();
		try{
			
			DBRow ebay = this.getEbayAccountById(ebay_account_id);
			String apiServerUrl = "https://api.sandbox.ebay.com/wsapi";
			String epsServerUrl = "https://api.sandbox.ebay.com/ws/api.dll";
		    String signInUrl = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn";
			FetchTokenCall call = new FetchTokenCall();
			String developer = "b8a31621-ff06-416e-a903-38f665f67499";
			String application = "vvme7e8ef-0503-4fb6-93eb-bf6638d5c46";
			String certificate = "bc23194f-ac55-4a43-8210-59d4f381a752";
			
			ApiContext apiContext = new ApiContext();  
			apiContext.setTimeout(180000);
			apiContext.setApiServerUrl(apiServerUrl);  
			apiContext.setEpsServerUrl(epsServerUrl);
			apiContext.setSignInUrl(signInUrl);
			
			ApiAccount ac = new ApiAccount();
		
			
			
			ac.setDeveloper(developer);
			ac.setApplication(application);
			ac.setCertificate(certificate);
		 
			
	        eBayAccount accountEbay = new eBayAccount();
	        accountEbay.setPassword(ebay.getString("password"));
	        accountEbay.setUsername(ebay.getString("user_id"));
	        ApiCredential apiCred = new ApiCredential();
	        apiCred.seteBayAccount(accountEbay);
		  //  apiContext.getApiLogging().setLogSOAPMessages(false);//Ebay交易信息是否输出
	        
	    	apiCred.setApiAccount(ac);
	        apiContext.setApiCredential(apiCred);  
	        call.setApiContext(apiContext);
	        call.setSessionID(session_id);
	       
	       
	        String ebayToken = call.fetchToken() ;
	        DBRow updateEbayRow = new DBRow();
	        updateEbayRow.add("user_token", ebayToken);
	        floorEbayMgrZr.updateEbayAccount(ebay_account_id, updateEbayRow);
	        result.add("flag", "success");
		}catch (ApiException e) {
			result.add("flag", "error");
			result.add("message", e.getMessage());
		}catch (Exception e) {
			throw new SystemException(e,"getEbayTokenAndUpdateEbayToken",log);
		} 
		 return result;
	}

	@Override
	public DBRow getEbaySellerIdAndUpdateOrder(long oid, String item_number)
			throws Exception
	{
		try{
			// 获取seller Id,然后更新
			//mpn如果有的话也要更新
			DBRow result = new DBRow();
			if(item_number.split(",").length > 1){
				item_number = item_number.split(",")[0];
			}
			Map<String,String> map = this.getSellerId(item_number,0);
			String  sellerId = map.get("sellerId");
			if(sellerId != null && sellerId.length() > 0 ){
				com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("orderMgr");
				orderMgr.modSellerId(oid,sellerId);
				String mpn = map.get("MPN");
				if(mpn != null && mpn.length() > 0 ){
					
					DBRow temp = new DBRow();
					temp.add("seller_id", sellerId);
					temp.add("mpn", mpn);
					DBRow[] rows = orderMgrZr.getTradeItemByOid(oid);
					if(rows != null && rows.length > 0 ){
						for(DBRow tradeTemp : rows){
							updateTradeItem(tradeTemp.get("trade_item_id", 0l), temp);
						}
					}
				}
				result.add("flag", "success");
				result.add("seller_id", sellerId);
			}else{
				result.add("flag", "error");
				result.add("message", "请检查ItemNumber是否正确");
			}
			return result;
		}catch (Exception e) {
			throw new SystemException(e,"getEbaySellerIdAndUpdateOrder",log);
		}
		 
	}
}
