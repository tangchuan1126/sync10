package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorScheduleSetMgrZr;
import com.cwc.app.iface.zr.ScheduleSetMgrIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ScheduleSetMgrZr implements ScheduleSetMgrIfaceZR {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorScheduleSetMgrZr floorScheduleSetMgrZr;
	
	public void setFloorScheduleSetMgrZr(FloorScheduleSetMgrZr floorScheduleSetMgrZr) {
		this.floorScheduleSetMgrZr = floorScheduleSetMgrZr;
	}

	@Override
	public DBRow getScheduleSet() throws Exception {
		try{
			return floorScheduleSetMgrZr.getOnlyOneRow();
		}catch(Exception e){
			throw new SystemException(e,"getScheduleSet",log);
		}
	}

	@Override
	public void updateScheduleSet(long id, DBRow row) throws Exception {
		try{
			  floorScheduleSetMgrZr.updateSchedule(id, row);
		}catch(Exception e){
			throw new SystemException(e,"updateScheduleSet",log);
		}
	}

}
