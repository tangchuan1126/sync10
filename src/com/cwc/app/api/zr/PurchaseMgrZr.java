package com.cwc.app.api.zr;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorPurchaseMgrZr;
import com.cwc.app.iface.zr.PurchaseMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.PurchaseArriveKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class PurchaseMgrZr implements PurchaseMgrIfaceZr{
	static Logger log = Logger.getLogger("ACTION");
	private FloorPurchaseMgrZr floorPurchaseMgrZr;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorPurchaseMgr fpm;
	
	
	/**
	 * purchase_id,
	 * followup_content,
	 * followup_ptype,
	 * eta
	 */
	@Override
	public void AddPurchaseLog(HttpServletRequest request) throws Exception {
		try{
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			String followup_content = StringUtil.getString(request,"followup_content");
			String eta = StringUtil.getString(request, "eta") ;
			int followupptype = StringUtil.getInt(request,"followup_ptype");
			int followupTypeSub = StringUtil.getInt(request, "followup_ptype_sub");
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String follower = adminLoggerBean.getEmploye_name();
			
			DBRow param = new DBRow();
			TDate tDate = new TDate();
			tDate.addDay(+2);
			param.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			param.add("updatetime",DateUtil.NowStr());
			fpm.updatePurchase(purchase_id, param);
			// 如果是交货跟进，ETA改变了。那么就要更新detail 上的所有的ETA
			if(7 == followupptype){
				if(eta.length() > 0 ){
					floorPurchaseMgrZr.updatePurchaseDetailEta(eta, purchase_id);
				}
			}
			long follower_id = adminLoggerBean.getAdid();
			addPurchasefollowuplogs(followupptype,followupTypeSub,eta, purchase_id,followup_content,follower_id, follower);
			String processTypeStr = "";
			if(6 == followupptype)
			{
				processTypeStr  = "[价格跟进]";
			}
			else
			{
				processTypeStr  = "[交货跟进]";
			}
			scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
					 ProcessKey.PURCHASE_PURCHASERS , follower+"确认了采购单:"+purchase_id+","+processTypeStr+followup_content , false, request, "");
		}catch (Exception e) {
			throw new SystemException(e, "AddPurchaseLog", log);
		}
		
	}
	public void setFpm(FloorPurchaseMgr fpm) {
		this.fpm = fpm;
	}

	//日志
	/**
	 * @param followup_type 日志记录类型1跟进日志2转账日志3上传excel
	 */
	private long addPurchasefollowuplogs(int followup_type,int followTypeSub,String followup_expect_date, long purchase_id,String followup_content,long follower_id,String follower)
		throws Exception
	{
		DBRow dbrow=new DBRow();
		dbrow.add("followup_type", followup_type);
		dbrow.add("followup_type_sub", followTypeSub);
		dbrow.add("purchase_id", purchase_id);
		dbrow.add("followup_content",followup_content);
		dbrow.add("follower_id",follower_id);
		dbrow.add("follower",follower);
		dbrow.add("followup_date",DateUtil.NowStr());
		dbrow.add("followup_expect_date", followup_expect_date);
		return (fpm.addPurchasefollowuplogs(dbrow));
	}
	@Override
	public String getLastPurchaseDetailEta(long purchase_id) throws Exception {
		try{
			 if(purchase_id == 0l){
				 return "" ;
			 }
			return floorPurchaseMgrZr.getLastDetailEta(purchase_id) ;
		}catch (Exception e) {
			throw new SystemException(e, "getLastPurchaseDetailEta", log);
		}
	}

	@Override
	public DBRow[] getPurchaseLogsByPurchaseIdAndType(long purchase, int[] types)
			throws Exception {
		try{
			return floorPurchaseMgrZr.getPurchaseLogsByPurchaseIdAndType(purchase, types);
		}catch (Exception e) {
			throw new SystemException(e, "getPurchaseLogsByPurchaseIdAndType", log);
		}
	}
	public void setFloorPurchaseMgrZr(FloorPurchaseMgrZr floorPurchaseMgrZr) {
		this.floorPurchaseMgrZr = floorPurchaseMgrZr;
	}
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
}
