package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.app.floor.api.zr.FloorLoadBarUseMgrZr;
import com.cwc.app.iface.zr.LoadBarUseMgrIfaceZr;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class LoadBarUseMgrZr implements LoadBarUseMgrIfaceZr {

	
	private FloorLoadBarUseMgrZr floorLoadBarUseMgrZr ;
	
	static Logger log = Logger.getLogger("ACTION");
	
	@Override
	public long addLoadBarUse(DBRow inserRow) throws AddLoadBarUseFoundException ,Exception {
		try{
			long equipment_id = inserRow.get("equipment_id", 0l);
			long load_bar_id = inserRow.get("load_bar_id", 0l);
			int count = inserRow.get("count", 0);
			if(equipment_id == 0l || load_bar_id == 0l || count < 0 ){
				throw new AddLoadBarUseFoundException();
			}
			long load_bar_use_id = 0l ;
			DBRow loadBarUseRow = floorLoadBarUseMgrZr.getLoadBarUse( equipment_id ,  load_bar_id);
			if(loadBarUseRow != null){
				int other = loadBarUseRow.get("count", 0);
				DBRow update = new DBRow();
				update.add("count", other + count);
				floorLoadBarUseMgrZr.updateLoadBarUse(loadBarUseRow.get("load_bar_use_id", 0l), update);
				load_bar_use_id = loadBarUseRow.get("load_bar_use_id", 0l);
			}else{
				load_bar_use_id = floorLoadBarUseMgrZr.addLoadBarUse(inserRow);
			}
			return load_bar_use_id ;
		}catch(AddLoadBarUseFoundException e ){
			throw e ;
		}catch (Exception e) {
			 throw new SystemException(e,"addLoadBarUse",log);
		}
	}
	@Override
	public void saveOrUpdateLoadUse(long equipment_id, long load_bar_id , DBRow insertRow)
			throws Exception {
		try{
		 
		}catch(Exception e){
			 throw new SystemException(e,"saveOrUpdateLoadUse",log);
		}
	}
	@Override
	public DBRow[] getLoadBarUseInfo(long equipment_id , long entry_id) throws Exception {
		try{
			return floorLoadBarUseMgrZr.getLoadBarGroupInfo(equipment_id, entry_id) ;
		}catch(Exception e){
			 throw new SystemException(e,"getLoadBarUseInfo",log);
		}
 	}
 	@Override
	public DBRow[] androidGetLoadBarUse(long equipment_id , long entry_id) throws Exception{
		try{
			DBRow[] datas = getLoadBarUseInfo(equipment_id, entry_id);
			if(datas != null && datas.length > 0){
				DBRow[] returnRows = new DBRow[datas.length] ;
				for(int index = 0 , count = datas.length ; index < count ; index++ ){
					DBRow insertRow = new DBRow();
					if(datas[index].get("total", 0) < 1){
						continue ;
					}
					insertRow.add("load_bar_name", datas[index].getString("load_bar_name"));
					insertRow.add("count", datas[index].get("total", 0));
					insertRow.add("load_bar_id", datas[index].get("load_bar_id", 0l));
					returnRows[index] = insertRow ;
				}
				return returnRows ;
			}
			return null ;
		}catch(Exception e){
			 throw new SystemException(e,"androidGetLoadBarUse",log);
		}
	}
	@Override
	public void androidUpdateLoadBarUse(DBRow inserRow , AdminLoginBean adminLoginBean) throws AddLoadBarUseFoundException, Exception{
		try{
			long equipment_id = inserRow.get("equipment_id", 0l);
			long load_bar_id = inserRow.get("load_bar_id", 0l);
			int count = inserRow.get("count", 0);
			if(equipment_id == 0l || load_bar_id == 0l || count < 0 ){
				throw new AddLoadBarUseFoundException();
			}
			if(count == 0){
				//删除对应的一条记录
				floorLoadBarUseMgrZr.deleteLoadBarUse(equipment_id,load_bar_id);
				return ;
			}
			DBRow loadBarUseRow = floorLoadBarUseMgrZr.getLoadBarUse( equipment_id ,  load_bar_id);
			if(loadBarUseRow != null){
 				DBRow update = new DBRow();
				update.add("count", count);
				update.add("last_opertion_time", DateUtil.NowStr());
				update.add("last_opertion_adid", adminLoginBean.getAdid());
				floorLoadBarUseMgrZr.updateLoadBarUse(loadBarUseRow.get("load_bar_use_id", 0l), update);
			}
		}catch(AddLoadBarUseFoundException e){
			throw e ;
		}catch(Exception e){
			 throw new SystemException(e,"androidUpdateLoadBarUse",log);
		}
	}
	
	public void setFloorLoadBarUseMgrZr(FloorLoadBarUseMgrZr floorLoadBarUseMgrZr) {
		this.floorLoadBarUseMgrZr = floorLoadBarUseMgrZr;
	}
	 
	
	
}
