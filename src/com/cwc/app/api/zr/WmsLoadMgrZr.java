package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorWmsLoadMgrZr;
import com.cwc.app.iface.zr.WmsLoadMgrZrIface;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class WmsLoadMgrZr implements WmsLoadMgrZrIface {

	static Logger log = Logger.getLogger("ACTION");

	private FloorWmsLoadMgrZr floorWmsLoadMgrZr ;
 	

	@Override
	public long addLoad(DBRow load) throws Exception {
 		try{
  	 		long load_id = floorWmsLoadMgrZr.addWmsLoad(load);
  	 		return load_id ;
 		}catch (Exception e) {
			throw new SystemException(e,"addLoad",log);
 		}
	}
	
	@Override
	public long addWmsLoadOrder(DBRow orderRow) throws Exception {
		try{
			return floorWmsLoadMgrZr.addWmsLoadOrder(orderRow);
		}catch (Exception e) {
			throw new SystemException(e,"addOrder",log);
		}
	}

	@Override
	public void updateOrderStatus(long order_id,DBRow updateRow ) throws Exception {
 		try{
 			floorWmsLoadMgrZr.updateLoadOrder(updateRow, order_id);
 		}catch (Exception e) {
			throw new SystemException(e,"updateOrderStatus",log);
 		}
	}

 

	@Override
	public DBRow queryLoadBy(String loadnumber, String customer_id, String company_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.getLoadBy(loadnumber, company_id, customer_id);
		}catch (Exception e) {
			throw new SystemException(e,"queryLoadBy",log);
		}
 	}
	@Override
	public DBRow[] queryOrderByLoad(long load_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.getOrderByLoadId(load_id);
		}catch (Exception e) {
			throw new SystemException(e,"queryOrderByLoad",log);
 		}
 	}
	
	@Override
	public void deleteWmsMasterBolInfo(long wms_master_bol_id) throws Exception {
		try{
			  floorWmsLoadMgrZr.deleteWmsOrderByMasterBolId(wms_master_bol_id);
			  
			  floorWmsLoadMgrZr.deleteWmsMastBol(wms_master_bol_id);
		}catch (Exception e) {
			throw new SystemException(e,"queryOrderByLoad",log);
 		}
	}
	@Override
	public DBRow getWmsLoadInfo(long wms_load_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.getLoadInfoByLoadId(wms_load_id);
		}catch (Exception e) {
			throw new SystemException(e,"queryOrderByLoad",log);
 		}
 	}
	
	@Override
	public long addWmsMastBolRow(DBRow wmsMasterBolRow) throws Exception {
		try{
			return floorWmsLoadMgrZr.addWmsMastBolRow(wmsMasterBolRow);
		}catch (Exception e) {
			throw new SystemException(e,"addWmsMastBolRow",log);
 		}
	}
	@Override
	public DBRow[] getMastBolRowByLoadId(long wms_load_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.getWmsMastBol(wms_load_id);
		}catch (Exception e) {
			throw new SystemException(e,"getMastBolRowByLoadId",log);
		}
 	}
	
	@Override
	public DBRow queryMasterBolBy(String master_bol_no, String customer_id, String company_id) throws Exception {
 		 try{
 			return floorWmsLoadMgrZr.getMasterBolBy(master_bol_no, customer_id, company_id);
 		 }catch (Exception e) {
 			throw new SystemException(e,"queryMasterBolBy",log);
		}
	}
	@Override
	public DBRow queryOrderBy(long wms_master_bol_id, String order_number)
			throws Exception {
		try{
 			return floorWmsLoadMgrZr.getOrderBy(wms_master_bol_id, order_number);
 		 }catch (Exception e) {
 			throw new SystemException(e,"queryOrderBy",log);
		}
	}
	
	@Override
	public long addPalletTypeRow(long dlo_detail_id,String order_number,int order_type,int order_system,String wms_pallet_type,int wms_pallet_type_count,String wms_pallet_number,String wms_consolidate_pallet_number,int wms_consolidate_reason,long wms_order_id , int delivery_or_pick_up , long lr_id ,  long resources_id , int resources_type , long scan_adid   ) 
		throws Exception 
	{
		try
		{
			DBRow palletTypeCountRow = new DBRow();
			palletTypeCountRow.add("dlo_detail_id",dlo_detail_id);
			palletTypeCountRow.add("order_number",order_number);
			palletTypeCountRow.add("order_type",order_type);
			palletTypeCountRow.add("order_system",order_system);
			palletTypeCountRow.add("wms_pallet_type",wms_pallet_type);
			palletTypeCountRow.add("wms_pallet_type_count",wms_pallet_type_count);
			palletTypeCountRow.add("scan_time", DateUtil.NowStr());
			if (wms_pallet_number.equals("")) 
			{
				palletTypeCountRow.add("wms_pallet_number","pallet");
			}
			else
			{
				palletTypeCountRow.add("wms_pallet_number",wms_pallet_number);
			}
			
			palletTypeCountRow.add("wms_consolidate_pallet_number",wms_consolidate_pallet_number);
			palletTypeCountRow.add("wms_consolidate_reason",wms_consolidate_reason);
			palletTypeCountRow.add("wms_order_id",wms_order_id);
			palletTypeCountRow.add("delivery_or_pick_up", delivery_or_pick_up);
			palletTypeCountRow.add("lr_id", lr_id);
			palletTypeCountRow.add("resources_id", resources_id);
			palletTypeCountRow.add("resources_type", resources_type);
			palletTypeCountRow.add("scan_adid", scan_adid);

 			return floorWmsLoadMgrZr.addPalletTypeCount(palletTypeCountRow);
 		 }
		catch (Exception e) 
		{
 			throw new SystemException(e,"addPalletTypeRow",log);
		}
	}
	@Override
	public long saveOrUpdatePalletTypeRow(DBRow palletRow) throws Exception {
		try{
			long wms_order_id = palletRow.get("wms_order_id", 0l);
			String pallet_number = palletRow.getString("wms_pallet_number");
			DBRow row = floorWmsLoadMgrZr.getPalletTypeBy(wms_order_id,pallet_number);
			if(row != null){
				long wms_order_type_id =  row.get("wms_order_type_id", 0l);
				floorWmsLoadMgrZr.updatePalletTypeBy(wms_order_type_id,palletRow);
				return wms_order_type_id ;
 			}else{
 				//return addPalletTypeRow(palletRow);
 				return 0l;
 			}
		}catch(Exception e){
 			throw new SystemException(e,"addPalletTypeRow",log);

		}
	}
 
	
	 
 
 
	 
	
	@Override
	public DBRow getOrderByLRIDAndOrderNumbers(long LR_ID, String order_number) throws Exception {
		try{
			return floorWmsLoadMgrZr.getOrderByLRIDAndOrderNumbers(LR_ID, order_number);
 		}catch (Exception e) {
	 		throw new SystemException(e,"getOrderByDetailIdAndOrderNumbers",log);
	 	}
	}
	
	@Override
	public DBRow[] getOrderInfoByLrId(long lr_id, long master_bol_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.getOrderInfoByLrId(lr_id, master_bol_id);
 		}catch (Exception e) {
	 		throw new SystemException(e,"getOrderInfoByEntryDetailId",log);
	 	}
	}
	@Override
	public long saveOrUpdateWmsMasterBolRow(DBRow wmsMasterBolRow ) throws Exception {
		try{
			String master_bol_no = wmsMasterBolRow.getString("master_bol_no");
			long lr_id = wmsMasterBolRow.get("lr_id", 0l);
			DBRow masterBol = floorWmsLoadMgrZr.getMasterBolByEntryDetailIdAndMasterBolNo(lr_id , master_bol_no);
			long wms_master_bol_id = 0l ;
			if(masterBol != null && masterBol.get("wms_master_bol_id", 0l) > 0){
				wms_master_bol_id = masterBol.get("wms_master_bol_id", 0l) ;
				//wmsMasterBolRow.remove("status");
				floorWmsLoadMgrZr.updateMasterBol(wms_master_bol_id,wmsMasterBolRow);
			}else{
				wms_master_bol_id = floorWmsLoadMgrZr.addWmsMastBolRow(wmsMasterBolRow);
			}
			return wms_master_bol_id ;
 		}catch (Exception e) {
	 		throw new SystemException(e,"getOrderInfoByEntryDetailId",log);
	 	}
	}
	
	@Override
	public void deleteOrderInfoAndPalletInfoByOrderId(long wms_order_id) throws Exception {
 		try{
 			floorWmsLoadMgrZr.deleteWmsOrderBy(wms_order_id);
 			floorWmsLoadMgrZr.deleteOrderPalletTypeBy(wms_order_id);
 		}catch(Exception e){
	 		throw new SystemException(e,"deleteOrderInfoAndPalletInfoByOrderId",log);
 		}
	}
	@Override
	public void updateWmsLoadOrderPalletTypeBy(long wms_order_id, String pallet_number, DBRow udpateRow) throws Exception {
		try{
			  floorWmsLoadMgrZr.updateWmsLoadOrderPalletTypeBy(wms_order_id,pallet_number,udpateRow);
 		}catch (Exception e) {
	 		throw new SystemException(e,"getOrderByDetailIdAndOrderNumbers",log);
	 	}
	}
	
	@Override
	public void updateWmsLoadOrderPalletType(long wms_order_type_id,
			DBRow udpateRow) throws Exception {
		try{
			floorWmsLoadMgrZr.updateWmsLoadOrderPalletTypeBy(wms_order_type_id, udpateRow);
		}catch(Exception e){
	 		throw new SystemException(e,"updateWmsLoadOrderPalletType",log);
		}
 		
	}
	@Override
	public DBRow[] getOrderPalletInfoByWmsOrderId(long dlo_detail_id,long wms_order_id) throws Exception {
		try{
			 return floorWmsLoadMgrZr.getOrderPalletInfoBy(dlo_detail_id,wms_order_id);
		}catch (Exception e) {
	 		throw new SystemException(e,"getOrderByDetailIdAndOrderNumbers",log);
	 	}
	}
	@Override
	public void updateLoadOrderStatus(long lr_id, String master_bol_no , String pro_no) throws Exception {
 		try{
 			DBRow updateRow = new DBRow();
 			updateRow.add("status", WmsOrderStatusKey.CLOSED); //order 用输入的为准
 			updateRow.add("pro_no", pro_no);		//master_bol添加上
 			floorWmsLoadMgrZr.udpateMaseterBolByNoAndLrId(lr_id, master_bol_no, updateRow);//跟新masterBOL
 			updateRow.remove("pro_no");		//master_bol添加上

 			floorWmsLoadMgrZr.updateOrderStaticByLrIdAndMasterBol(lr_id,master_bol_no,updateRow);//更新masterBOl 下面的Order
 			
 		}catch (Exception e) {
	 		throw new SystemException(e,"updateLoadOrderStatus",log);
	 	}
	} 
	@Override
	public void updateOrderByLrId(long lr_id) throws Exception {
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("status", WmsOrderStatusKey.CLOSED);
			floorWmsLoadMgrZr.updateOrderStaticByLrId(lr_id,updateRow);
		}catch(Exception e){
	 		throw new SystemException(e,"updateOrderByDetailId",log);
		}
	}
	
	@Override
	public void updateOrderProNo(long lr_id, String master_bol_no,
			String pro_no) throws Exception {
		try{
			floorWmsLoadMgrZr.updateOrderProNoByLrId(lr_id,master_bol_no,pro_no);
		}catch(Exception e){
	 		throw new SystemException(e,"updateOrderProNo",log);
		}
	}
	@Override
	public DBRow[] getMasterBolBy(long LR_ID) throws Exception {
		try{
			return floorWmsLoadMgrZr.getMasterBolBy(LR_ID);
		}catch(Exception e){
	 		throw new SystemException(e,"getMasterBolBy",log);
		}
	}
	@Override
	public DBRow[] getOrderInfoByLrIdAndMasterBol(long lr_id, String master_bol_no)
			throws Exception {
			try{
				return floorWmsLoadMgrZr.getOrderInfoByDetailIdAndMasterBol(lr_id, master_bol_no);
			}catch(Exception e){
		 		throw new SystemException(e,"getOrderBy",log);
			}
	}
	@Override
	public DBRow getMasterBolByLrIdAndMasterBolNo(long lr_id, String master_bol_no) throws Exception {
		try{
			return floorWmsLoadMgrZr.getMasterBolByLrIdAndMasterBolNo(lr_id,master_bol_no);
 		}catch(Exception e){
	 		throw new SystemException(e,"getMasterBolByLrIdAndMasterBolNo",log);
		}
 	}
	@Override
	public void subtractMasterBolTotalPallet(long wms_master_bol_id)
			throws Exception {
		try{
				DBRow masterBol = floorWmsLoadMgrZr.getMasterBolByWmsMasterBolId(wms_master_bol_id);
				DBRow updateRow = new DBRow();
				updateRow.add("total_pallets", masterBol.get("total_pallets", 0) - 1);
				floorWmsLoadMgrZr.updateMasterBolBy(wms_master_bol_id,updateRow);
		}catch(Exception e){
	 		throw new SystemException(e,"subtractMasterBolTotalPallet",log);
		}
	}
	@Override
	public int upateOrder(long wms_order_id, DBRow updateRow) throws Exception {
		try{
			return floorWmsLoadMgrZr.updateOrder(wms_order_id,updateRow);
 		}catch(Exception e){
	 		throw new SystemException(e,"upateOrder",log);
		}
	}
	
	@Override
	public int updateMasterBol(long wms_master_bol_id, DBRow updateRow) throws Exception {
		try{
			return floorWmsLoadMgrZr.updateMasterBol(wms_master_bol_id, updateRow);
 		}catch(Exception e){
	 		throw new SystemException(e,"updateMasterBol",log);
		}
	}
	
	@Override
	public DBRow getWmsOrderConsolidateNumber(long wms_order_id) throws Exception {
		try{
			return  floorWmsLoadMgrZr.getOrderConsolidateRow(wms_order_id) ;
 		}catch(Exception e){
	 		throw new SystemException(e,"getWmsOrderConsolidateNumber",log);
		}	 
	}
	
	@Override
	public int deletePalletBy(long wms_order_type_id) throws Exception {
		try{
			return floorWmsLoadMgrZr.deletePallet(wms_order_type_id);
		}catch(Exception e){
	 		throw new SystemException(e,"deletePalletBy",log);
		}
 	}
	
	@Override
	public DBRow getPalletByNumber(String order_number, long dlo_detail_id,
			String pallet_no) throws Exception {
		try{
			return	floorWmsLoadMgrZr.getPalletBy(  dlo_detail_id ,   order_number ,   pallet_no) ;
		}catch(Exception e){
	 		throw new SystemException(e,"getPalletByNumber",log);
		}
 	}
	@Override
	public int deletePalletBy(String order_number, long dlo_detail_id,
			String pallet_no) throws Exception {
		try{
			DBRow pallet =	getPalletByNumber(order_number, dlo_detail_id, pallet_no);
			if(pallet != null){
				return deletePalletBy(pallet.get("wms_order_type_id", 0l));  
			}
			return 0 ;
		}catch(Exception e){
	 		throw new SystemException(e,"deletePalletBy",log);
		}
 	}
	
	
	/**
	 * 查询所有的WmsLoadOrders
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午12:45:01
	 */
	public DBRow[] findWmsLoadOrders(long start, long end) throws Exception
	{
		try{
			return floorWmsLoadMgrZr.findWmsLoadOrders(start, end);
		}catch(Exception e){
	 		throw new SystemException(e,"findWmsLoadOrders",log);
		}
	}
	
	@Override
	public void deleteScanPalletType(long dlo_detail_id) throws Exception {
		try{
			floorWmsLoadMgrZr.deleteTaskLoadPallet(dlo_detail_id);
		}catch(Exception e){
	 		throw new SystemException(e,"deleteScanPalletType",log);
		}
	}
	
	
	@Override
	public int commonLoadDelete(long wms_order_type_id) throws Exception {
		try{
			return  floorWmsLoadMgrZr.commonLoadDelete(wms_order_type_id) ;
		}catch(Exception e){
  			throw new SystemException(e,"commonLoadDelete",log);
		}
	}
	@Override
	public long commonLoadPallet(DBRow insertRow) throws Exception {
		 try{
			insertRow.add("scan_time", DateUtil.NowStr());
			return floorWmsLoadMgrZr.commonLoadPallet(insertRow); 
		 }catch(Exception e){
			 throw new SystemException(e,"commonLoadPallet",log);
		 }
	}
	@Override
	public DBRow[] commonLoadRefresh(long lr_id) throws Exception {
		try{
			DBRow[] datas = floorWmsLoadMgrZr.getCommonLoadRefresh(lr_id);
			datas = fixCommonLoadRefreshDatas(datas);
			return datas ;
		}catch(Exception e){
  			throw new SystemException(e,"commonLoadRefresh",log);
		}
 	}
	
	private DBRow[] fixCommonLoadRefreshDatas(DBRow[] datas) throws Exception{
		if(datas != null && datas.length > 0){
			DBRow[] returnArray = new DBRow[datas.length] ;
			for(int index = 0 , count = datas.length ; index < count ; index++ ){
				DBRow loadRow = datas[index];
				DBRow temp = new DBRow();
				temp.add("wms_order_type_id", loadRow.get("wms_order_type_id",0l));
				temp.add("wms_pallet_type", loadRow.getString("wms_pallet_type"));
				temp.add("count", loadRow.getString("wms_pallet_type_count"));
				temp.add("wms_pallet_number", loadRow.getString("wms_pallet_number"));
				returnArray[index] = temp ;
			}
			return returnArray ;
		}
		return new DBRow[0] ;
	}
	
	@Override
	public void commonLoadReload(long lr_id, long detail_id) throws Exception {
		try{
			 floorWmsLoadMgrZr.commonLoadDeletePalletBy(lr_id);
		}catch(Exception e){
  			throw new SystemException(e,"commonLoadReload",log);
		}
	}
	@Override
	public int commonLoadModify(long wms_order_type_id, DBRow updateRow)
			throws Exception {
		try{
			return floorWmsLoadMgrZr.commonLoadModify(wms_order_type_id, updateRow);
		}catch(Exception e){
  			throw new SystemException(e,"commonLoadModify",log);
		}
 	}
	 /**
	  * checkInIndex查询pallet用的方法  wfh  按照detailId查询masterBol
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	@Override
	public DBRow[] getMasterBolInfoByDetailId(long dlo_detail_id)
			throws Exception {
		try{
			return floorWmsLoadMgrZr.getMasterBolInfoByDetailId(dlo_detail_id);
		}catch(Exception e){
  			throw new SystemException(e,"getMasterBolInfoByDetailId",log);
		}
	}
	
	 /**
	  * checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	@Override
	public DBRow[] getOrderByDetailAndMasterBol(long dlo_detail_id, String masterBolNo) throws Exception {
		try{
			return floorWmsLoadMgrZr.getOrderByDetailAndMasterBol(dlo_detail_id, masterBolNo);
		}catch(Exception e){
  			throw new SystemException(e,"getOrderByDetailAndMasterBol",log);
		}
	}
	
	/**
	  * checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	@Override
	public DBRow[] getOrderByDetailAndMasterBolIncludeZeroPallet(long dlo_detail_id, String masterBolNo) throws Exception {
		try{
			return floorWmsLoadMgrZr.getOrderByDetailAndMasterBolIncludeZeroPallet(dlo_detail_id, masterBolNo);
			
		}catch(Exception e){
 			throw new SystemException(e,"getOrderByDetailAndMasterBolIncludeZeroPallet",log);
		}
	}
	
	
	public void setFloorWmsLoadMgrZr(FloorWmsLoadMgrZr floorWmsLoadMgrZr) {
		this.floorWmsLoadMgrZr = floorWmsLoadMgrZr;
	}
	 
}
