package com.cwc.app.api.zr;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorCheckReturnProductItemMgrZr;
import com.cwc.app.floor.api.zr.FloorReturnProductItemsFix;
import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.floor.api.zyj.FloorReturnProductOrderMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.CheckReturnProductItemMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.ReturnProductItemsFixMgrIfaceZr;
import com.cwc.app.key.ReturnProductCheckItemTypeKey;
import com.cwc.app.key.ReturnProductLogTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class CheckReturnProductItemMgrZr implements CheckReturnProductItemMgrIfaceZr {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr ;
	private ReturnProductItemsFixMgrIfaceZr returnProductItemsFixMgrZr ;
	private AdminMgrIFace adminMgr ;
	private FileMgrIfaceZr fileMgrZr ;
	private FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj;
	private FloorReturnProductMgrZr floorReturnProductMgrZr;
	
	/**
	 * 一个rpi_id只能是有一个check
	 */
	@Override
	public long addCheckReturnProductItem(HttpServletRequest request)
			throws Exception {
		try{
			String check_product_context = StringUtil.getString(request, "check_product_context");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			long rp_id = StringUtil.getLong(request, "rp_id");
		 
			DBRow[] checks = floorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id);
			if(checks != null && checks.length > 0){
				throw new Exception("已经存在一个测试");
			}
			DBRow checkRow = new DBRow();
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long adid = adminLoggerBean.getAdid(); 
			
			String nowDate = DateUtil.DatetimetoStr(new Date()) ;
			
			checkRow.add("create_employee_name", adminLoggerBean.getEmploye_name());
			checkRow.add("check_product_context", check_product_context);
			checkRow.add("create_adid", adid);
			checkRow.add("create_date", nowDate);
			checkRow.add("rpi_id", rpi_id);
			checkRow.add("rp_id", rp_id);
			checkRow.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
			
			floorCheckReturnProductItemMgrZr.addCheckReturnProductItem(checkRow);
			// 然后更新 return_product_item_fix表中的数据表示这个需要需要Test
			returnProductItemsFixMgrZr.updateReturnProductItemFixIsNeedCheck(rp_id, rpi_id);
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adminLoggerBean.getAdid(), DateUtil.NowStr());
			
			return 0l;
		}catch (Exception e) {
			throw new SystemException(e,"addCheckReturnProductItem",log);
		}
	}



	@Override
	public DBRow[] getCheckReturnProductItemByRpIId(long rpi_id)
			throws Exception {
	 
		try{
			return  floorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id) ;	
		}catch (Exception e) {
			throw new SystemException(e,"getCheckReturnProductItemByRpIId",log);
		}
	}



	@Override
	public DBRow getCheckReturnProductItemById(long check_return_product_id)
			throws Exception {

		try{
			return  floorCheckReturnProductItemMgrZr.getCheckRetutrnProductItemById(check_return_product_id) ;	
		}catch (Exception e) {
			throw new SystemException(e,"getCheckReturnProductItemById",log);
		}
	}


	//填写测试结果
	@Override
	public void replyCheckReturnProductItem(HttpServletRequest request)
			throws Exception {
		try{
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long adid = adminLoggerBean.getAdid(); 
			String nowDate = DateUtil.DatetimetoStr(new Date());
			String result_context = StringUtil.getString(request, "result_context");
			String file_names = StringUtil.getString(request, "file_names");
			long check_return_product_id = StringUtil.getLong(request, "check_return_product_id");
			long rp_id = StringUtil.getLong(request, "rp_id");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			boolean hasFile = false ;
			DBRow checkReturnProductItemRow = new DBRow();
			checkReturnProductItemRow.add("handle_date", nowDate);
			checkReturnProductItemRow.add("handle_adid", adid);
			checkReturnProductItemRow.add("handle_employ_name", adminLoggerBean.getEmploye_name());
			checkReturnProductItemRow.add("result_context", result_context);
			checkReturnProductItemRow.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK_FINISH);
			
			// 如果有文件的情况
			// 移动文件到对应的目录
			if(file_names.trim().length() > 0){
				 hasFile = true ;
				 String[] arrayFile = file_names.split(",");
				 String sn  = StringUtil.getString(request, "sn");
				 String file_with_type = StringUtil.getString(request, "file_with_type");
				 String path = StringUtil.getString(request, "path");
				 String baseUrl = Environment.getHome()+"upl_imags_tmp/";
				 for(String tempFileName : arrayFile){
					 DBRow tempFile = new DBRow();
					 tempFile.add("file_with_id", check_return_product_id);
					 tempFile.add("file_with_type", file_with_type);
					 tempFile.add("upload_adid", adid);
					 tempFile.add("upload_time", nowDate);
					 StringBuffer realyFileName = new StringBuffer(sn);
 					 String suffix = StringUtil.getSuffix(tempFileName);
 					 realyFileName.append("_").append(check_return_product_id).append("_").append(tempFileName.replace("."+suffix, ""));
 					 int indexFile = fileMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
 					 if(indexFile != 0){
 						realyFileName.append("_").append(indexFile);
 					 }
	 					
	 				 realyFileName.append(".").append(suffix);
					 tempFile.add("file_name",  realyFileName.toString());
					 fileMgrZr.saveFile(tempFile);
					 FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
				 }
				 	
			}
			checkReturnProductItemRow.add("has_result_file", hasFile?"1":"0");
			floorCheckReturnProductItemMgrZr.updateCheckReturnProductItem(check_return_product_id, checkReturnProductItemRow);
			returnProductItemsFixMgrZr.updateReturnProductItemAndReturnProductCheckIsFinish(rp_id, rpi_id);
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]填写商品测试结果", ReturnProductLogTypeKey.CHECK, adminLoggerBean.getAdid(), DateUtil.NowStr());
			
		}catch (Exception e) {
			throw new SystemException(e,"replyCheckReturnProductItem",log);
		}
	}



	@Override
	public void updateCheckReturnProductItem(HttpServletRequest request)
			throws Exception {
		 try{
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 long check_return_product_id = StringUtil.getLong(request, "check_return_product_id");
			 String check_product_context = StringUtil.getString(request, "check_product_context");
			 DBRow updateRow = new DBRow();
			 updateRow.add("check_product_context", check_product_context) ;
			 floorCheckReturnProductItemMgrZr.updateCheckReturnProductItem(check_return_product_id, updateRow);
			 DBRow checkRow = floorCheckReturnProductItemMgrZr.getCheckRetutrnProductItemById(check_return_product_id);
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(checkRow.get("rp_id", 0L));
			//增加退货单日志
			 floorReturnProductOrderMgrZyj.addReturnProductLog(checkRow.get("rp_id", 0L),checkRow.get("rpi_id", 0L),0,0, returnRow.get("status", 0), "退货单["+checkRow.get("rp_id", 0L)+"]更新商品测试", ReturnProductLogTypeKey.CHECK, adminLoggerBean.getAdid(), DateUtil.NowStr());
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"updateCheckReturnProductItem",log);
		}
		
	}
	
	public void setFloorCheckReturnProductItemMgrZr(
			FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr) {
		this.floorCheckReturnProductItemMgrZr = floorCheckReturnProductItemMgrZr;
	}
	public void setReturnProductItemsFixMgrZr(
			ReturnProductItemsFixMgrIfaceZr returnProductItemsFixMgrZr) {
		this.returnProductItemsFixMgrZr = returnProductItemsFixMgrZr;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}
	public void setFloorReturnProductOrderMgrZyj(
			FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj) {
		this.floorReturnProductOrderMgrZyj = floorReturnProductOrderMgrZyj;
	}
	public void setFloorReturnProductMgrZr(
			FloorReturnProductMgrZr floorReturnProductMgrZr) {
		this.floorReturnProductMgrZr = floorReturnProductMgrZr;
	}

}
