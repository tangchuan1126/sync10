package com.cwc.app.api.zr;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.iface.zr.InvoiceMgrIfaceZR;
import com.cwc.app.key.BillTypeKey;
import com.cwc.app.key.InvoiceStateKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.paypal.svcs.services.InvoiceService;
import com.paypal.svcs.types.common.AckCode;
import com.paypal.svcs.types.common.BaseAddress;
import com.paypal.svcs.types.common.ErrorData;
import com.paypal.svcs.types.common.RequestEnvelope;
import com.paypal.svcs.types.pt.BusinessInfoType;
import com.paypal.svcs.types.pt.CreateAndSendInvoiceRequest;
import com.paypal.svcs.types.pt.CreateAndSendInvoiceResponse;
import com.paypal.svcs.types.pt.CreateInvoiceRequest;
import com.paypal.svcs.types.pt.CreateInvoiceResponse;
import com.paypal.svcs.types.pt.InvoiceItemListType;
import com.paypal.svcs.types.pt.InvoiceItemType;
import com.paypal.svcs.types.pt.InvoiceType;
import com.paypal.svcs.types.pt.PaymentTermsType;
import com.paypal.svcs.types.pt.SendInvoiceRequest;
import com.paypal.svcs.types.pt.SendInvoiceResponse;
import com.paypal.svcs.types.pt.UpdateInvoiceRequest;
import com.paypal.svcs.types.pt.UpdateInvoiceResponse;
// 账单
public class InvoiceMgrZr implements InvoiceMgrIfaceZR{
	
	
	static Logger log = Logger.getLogger("ACTION");
	
	private static String CreateAndSendInvoice = "https://svcs.paypal.com/Invoice/CreateAndSendInvoice/";
	private static String CreateInvoice = "https://svcs.paypal.com/Invoice/CreateInvoice/";
	private static String SendInvoice = "https://svcs.paypal.com/Invoice/SendInvoice/";
	private static String UpdateInvoice = "https://svcs.paypal.com/Invoice/UpdateInvoice/";
	private FloorBillMgrZr floorBillMgrZr;
	
	public void setFloorBillMgrZr(FloorBillMgrZr floorBillMgrZr) {
		this.floorBillMgrZr = floorBillMgrZr;
	}


	@Override
	public DBRow createInvoiceByBillId(long billId , String cmd) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow billDBRow = floorBillMgrZr.getBillOrderByBillId(billId);
			if(billDBRow != null) {
				InvoiceType invoiceType = new InvoiceType();
				setBasicInfoInvoice(billDBRow,invoiceType);
				// 商品Item
				List<InvoiceItemType> items = new ArrayList<InvoiceItemType>();
				InvoiceItemListType invoiceItem = new InvoiceItemListType();
				
				//查询出BillId下的Item
				boolean isOrder = billDBRow.get("bill_type", 0) == BillTypeKey.order ? true : false;
				boolean isEbay =  billDBRow.get("bill_type", 0) == BillTypeKey.ebay ? true : false;
				DBRow[] rows =  floorBillMgrZr.getBillItemByBillOrderId(billId);
				if(rows != null && rows.length > 0){
					
					for(DBRow itemRow : rows){
 						InvoiceItemType invoiceItemType = new InvoiceItemType();
						
						invoiceItemType.setQuantity(Double.valueOf(itemRow.get("quantity", 0.0d)));
						invoiceItemType.setUnitPrice( Double.valueOf(itemRow.get("actual_price", 0.0d)));
						String name = itemRow.getString("name");
					      if(name.length() > 30){
					       name = name.substring(0,30);
					      }
					      invoiceItemType.setName(name);
						 
						if(isEbay){
							invoiceItemType.setDescription("Item number : " + itemRow.getString("item_number"));
						}
						items.add(invoiceItemType);
					} 
				}else{
					if(isOrder){
						items.add(new InvoiceItemType("For Order ["+billDBRow.get("porder_id",0l)+"]",1.0d,billDBRow.get("order_fee", 0.0d)));
					}else{
						items.add(new InvoiceItemType("For Shipping Fee",0.1,0.0d));
					}
				}
				invoiceItem.setItem(items);
				invoiceType.setItemList(invoiceItem);
				
				
				
				// invoice response
				RequestEnvelope env = new RequestEnvelope("en_US");
				StringBuffer i = new StringBuffer("");
				/*i.append("acct1.UserName = vvmecom_api1.gmail.com"+"\n");
				i.append("acct1.Password = N5TJZ8LFGJ34VPLL"+"\n");
				i.append("acct1.Signature = AuuSVEoHUR9fGurf89MA7qN5C8UZAXkTmNMRBRYJeRKctKg9jhc8bOBS"+"\n");
				i.append("acct1.AppId = APP-1RH22298NN310454W"+"\n");
				i.append("http.ConnectionTimeOut=3000"+"\n");
				i.append("http.Retry=2"+"\n");
				i.append("http.ReadTimeOut=60000"+"\n");
				i.append("http.MaxConnection=100"+"\n");
				i.append("http.TrustAllConnection=false"+"\n");		
				
			  
		 
				i.append("http.MaxConnection=100"+"\n");
				i.append("service.Binding=NV"+"\n");
				i.append("service.EndPoint="+CreateAndSendInvoice+"\n");*/
			 
				//InputStream inputStream = new ByteArrayInputStream(i.toString().getBytes());
 
				InputStream inputStream = getInputStream(CreateAndSendInvoice);
				InvoiceService invoiceSrvc = new InvoiceService(inputStream);
				
				 
				AckCode state = null;
				String invoiceId = null;
				Iterator iterator = null;
				if(cmd.equals("create")){
					CreateInvoiceRequest createRequest = new CreateInvoiceRequest(env, invoiceType);
				 
					CreateInvoiceResponse createResp = invoiceSrvc.createInvoice(createRequest);
					state = createResp.getResponseEnvelope().getAck();
					invoiceId = createResp.getInvoiceID();
					  iterator = createResp.getError().iterator();
				}else{
					CreateAndSendInvoiceRequest createRequest = new CreateAndSendInvoiceRequest(env, invoiceType);

					CreateAndSendInvoiceResponse createResp = invoiceSrvc.createAndSendInvoice(createRequest);
					state = createResp.getResponseEnvelope().getAck();
					invoiceId = createResp.getInvoiceID();
					iterator = createResp.getError().iterator();
				}
			 
 				
 				if (state.equals(AckCode.SUCCESS) || state.equals(AckCode.SUCCESSWITHWARNING)) {
					// 成功的时候就要更行Bill表的InvoiceID,invoice_state
					DBRow updateRow = new DBRow();
				 
					updateRow.add("invoice_id", invoiceId);
					if(cmd.equals("create")){
						updateRow.add("invoice_state", InvoiceStateKey.CreateInvoiceed);
					}else{
						updateRow.add("invoice_state", InvoiceStateKey.SendInvoiceed);
					}
					floorBillMgrZr.updateBill(billId, updateRow);
					result.add("flag", "success");
					result.add("invoice_id", invoiceId);
					result.add("message", "success"); 
				} else {
					result.add("flag", "error");
					StringBuffer errorInfo = new StringBuffer();
					
					int index = 0;
					while (iterator.hasNext()) {
						index++;
						ErrorData error = (ErrorData) iterator.next();						 
						errorInfo.append(index+ ".  ").append(error.getErrorId()+error.getMessage()+"<br />");
					}
					result.add("message",errorInfo);
				}
				 
			}
		}catch (Exception e) {
			result.add("flag", "error");
			throw new SystemException(e,"createInvoiceByBillId",log);
		} 
		return result;
	}
	
	 
	// 发送Invoice 只能是已经create的情况下才可能的
	@Override
	public DBRow sendInvoiceByInvoiceId(long billId) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow billOrder = floorBillMgrZr.getBillOrderByBillId(billId);
		
			if(billOrder != null && billOrder.getString("invoice_id").length() > 0){
				// 发送Invoice
				if(billOrder.get("invoice_state", 1) != InvoiceStateKey.CreateInvoiceed){
					 throw new RuntimeException("invoice state error,must be createInvoiceed!!");
				}
				RequestEnvelope env = new RequestEnvelope("en_US");
				SendInvoiceRequest sendInvoiceRequest = new SendInvoiceRequest(env, billOrder.getString("invoice_id"));
			 
				 
			 
			 
				InputStream inputStream = getInputStream(SendInvoice);
				//InvoiceService invoiceSrvc = new InvoiceService("D:/invoice/sdk_config.properties");
				InvoiceService invoiceSrvc = new InvoiceService(inputStream);
				SendInvoiceResponse sendResp = invoiceSrvc.sendInvoice(sendInvoiceRequest);
				AckCode state = sendResp.getResponseEnvelope().getAck();
				////system.out.println("Ack:" + sendResp.getResponseEnvelope().getAck());
				if (state.equals(AckCode.SUCCESS) || state.equals(AckCode.SUCCESSWITHWARNING)) {
					// 应该更新一个状态 是一发送Invoice
					DBRow updateRow = new DBRow();
					updateRow.add("invoice_state", InvoiceStateKey.SendInvoiceed);
					floorBillMgrZr.updateBill(billId, updateRow);
					result.add("flag", "success");
					result.add("message", "success");
				} else {
					result.add("flag", "error");
					StringBuffer errorInfo = new StringBuffer();
					Iterator iterator = sendResp.getError().iterator();
					int index = 0;
					while (iterator.hasNext()) {
						index++;
						ErrorData error = (ErrorData) iterator.next();
						////system.out.println("<br/>" + error.getDomain() +" : "+error.getErrorId() + error.getMessage());
						errorInfo.append(index+ ".  ").append(error.getMessage()+"<br />");
					}
					result.add("message",errorInfo);
				}
				
			}else{
				result.add("flag", "error");
			}
		}catch(Exception e){
			throw new SystemException(e,"sendInvoiceByInvoiceId",log);
		}
		return result;
	}
	// 给invoiceType里面设置基本信息
	private void setBasicInfoInvoice(DBRow billRow , InvoiceType invoiceType) throws Exception{
		try{
			invoiceType.setMerchantEmail("vvmecom@gmail.com");
			 
			
			// rate_type === currencyCode 
			invoiceType.setCurrencyCode(billRow.getString("rate_type"));
		 
			// total_discount 
			invoiceType.setDiscountAmount(billRow.get("total_discount", 0.0d));
			String createDate = billRow.getString("create_date");
			//invoiceType.setDueDate(createDate.length() > 19 ? createDate.substring(0, 19):DateUtil.NowStr());
			////system.out.println("createDate : " + (createDate.length() > 19 ? createDate.substring(0, 19):DateUtil.NowStr()));		
			// invoiceType.setInvoiceDate(DateUtil.NowStr());
			// invoiceType.setDueDate("2012-06-12");
			invoiceType.setMerchantMemo(billRow.getString("memo_note"));
			invoiceType.setNote(billRow.getString("note"));
		 
			invoiceType.setNumber(billRow.getString("bill_id")); // 先不设置
			invoiceType.setPayerEmail( billRow.getString("payer_email"));
			
			invoiceType.setPaymentTerms(PaymentTermsType.DUEONRECEIPT);
			invoiceType.setShippingAmount(billRow.get("shipping_fee", 0.0d));
			
			// seller address info
			BusinessInfoType businessInfoType = new BusinessInfoType();
		
			businessInfoType.setBusinessName("Visionari LLC");
		 
			businessInfoType.setWebsite("http://www.vvme.com");
			businessInfoType.setPhone("877-365-VVME(8863)");
			BaseAddress sellerAddress  = new BaseAddress();
			sellerAddress.setCity("Monterey Park");
			sellerAddress.setCountryCode("US");
			sellerAddress.setLine1("2570 Corporate Place E103,");
			sellerAddress.setState("CA");
			businessInfoType.setAddress(sellerAddress);
			
			
			invoiceType.setMerchantInfo(businessInfoType);
			invoiceType.setLogoUrl("https://www.paypal.com/us/cgi-bin/?cmd=_stream-logo-image&id=1071352");
	 
			// payer address info
			BusinessInfoType payerBusinessInfo = new BusinessInfoType();
			BaseAddress payerAddress = new BaseAddress();
			payerAddress.setCity(billRow.getString("address_city"));
			payerAddress.setCountryCode(billRow.getString("address_country_code"));
			payerAddress.setLine1(billRow.getString("address_name") + " , " + billRow.getString("address_street"));
			payerAddress.setState(billRow.getString("address_state"));
	 
			payerBusinessInfo.setAddress(payerAddress);
			payerBusinessInfo.setPhone(billRow.getString("tel"));
			invoiceType.setShippingInfo(payerBusinessInfo);
			DBRow shippingCompany = floorBillMgrZr.getShippingInfoByScId(billRow.get("sc_id", 0l));
			if(shippingCompany != null){
				invoiceType.setShippingTaxName(shippingCompany.getString("name"));
				 invoiceType.setShippingTaxRate(0.0d);
			}
		 
		}catch(Exception e){
			throw new SystemException(e,"setBasicInfoInvoice",log);
		}
	}


	@Override
	public DBRow updateInvoiceByBillId(long billId) throws Exception {
		DBRow result = new DBRow();
		try{
			DBRow billDBRow = floorBillMgrZr.getBillOrderByBillId(billId);
			if(billDBRow != null) {
				InvoiceType invoiceType = new InvoiceType();
				setBasicInfoInvoice(billDBRow,invoiceType);
				// 商品Item
				List<InvoiceItemType> items = new ArrayList<InvoiceItemType>();
				InvoiceItemListType invoiceItem = new InvoiceItemListType();
				
				//查询出BillId下的Item. 如果是ORder的话就不用查询了
				boolean isOrder = billDBRow.get("bill_type", 0) == BillTypeKey.order ? true : false;
			 
				DBRow[] rows =  floorBillMgrZr.getBillItemByBillOrderId(billId);
				if(rows != null && rows.length > 0){
					boolean isEbay =  billDBRow.get("bill_type", 0) == BillTypeKey.ebay ? true : false;
					if(rows != null && rows.length > 0){
						
						for(DBRow itemRow : rows){
	 						InvoiceItemType invoiceItemType = new InvoiceItemType();
							
							invoiceItemType.setQuantity(Double.valueOf(itemRow.get("quantity", 0.0d)));
							invoiceItemType.setUnitPrice( Double.valueOf(itemRow.get("actual_price", 0.0d)));
							 String name = itemRow.getString("name");
						      if(name.length() > 30){
						       name = name.substring(0,30);
						      }
						      invoiceItemType.setName(name);
							if(isEbay){
								invoiceItemType.setDescription("item_number : " + itemRow.getString("item_number"));
							}
							items.add(invoiceItemType);
						} 
					}
				}else{
					if(isOrder){
						items.add(new InvoiceItemType("For Order ["+billDBRow.get("porder_id",0l)+"]",1.0d,billDBRow.get("order_fee", 0.0d)));
					}else{
						items.add(new InvoiceItemType("No product",0.1,0.0d));
					}
				}
				invoiceItem.setItem(items);
				invoiceType.setItemList(invoiceItem);
				
				
				RequestEnvelope env = new RequestEnvelope("en_US");
			
				 
				InputStream inputStream =  this.getInputStream(UpdateInvoice);
				InvoiceService invoiceSrvc = new InvoiceService(inputStream);
				AckCode state = null;
				String invoiceId = null;
				Iterator iterator = null;
				String invoiceID = billDBRow.getString("invoice_id");
				UpdateInvoiceRequest req = new UpdateInvoiceRequest(env, invoiceID,invoiceType);
 
				UpdateInvoiceResponse resp = invoiceSrvc.updateInvoice(req);
				state = resp.getResponseEnvelope().getAck();
				invoiceId = resp.getInvoiceID();
				iterator = resp.getError().iterator();
	 
 				if (state.equals(AckCode.SUCCESS) || state.equals(AckCode.SUCCESSWITHWARNING)) {
					// 成功的时候就要更行Bill表的InvoiceID,invoice_state
					DBRow updateRow = new DBRow();
				 
					updateRow.add("invoice_id", invoiceId);
					 
					updateRow.add("invoice_state", billDBRow.get("invoice_state", 0) - 3);
					floorBillMgrZr.updateBill(billId, updateRow);
					result.add("flag", "success");
					result.add("invoice_id", invoiceId);
					result.add("message", "success"); 
				} else {
					result.add("flag", "error");
					StringBuffer errorInfo = new StringBuffer();
					
					int index = 0;
					while (iterator.hasNext()) {
						index++;
						ErrorData error = (ErrorData) iterator.next();
						errorInfo.append(index+ ".  ").append(error.getMessage()+"<br />");
					}
					result.add("message",errorInfo);
				}
				 
			}
		}catch (Exception e) {
			result.add("flag", "error");
			throw new SystemException(e,"updateInvoiceByBillId",log);
		} 
		return result;
	}
	 
	
	private InputStream getInputStream(String type) throws Exception{
		try{
			DBRow payPalAccount = floorBillMgrZr.getPaypalAccountById(1l);
			StringBuffer i = new StringBuffer("");
			i.append("acct1.UserName = "+payPalAccount.getString("account_name")+"\n");
			i.append("acct1.Password = "+payPalAccount.getString("account_pwd")+"\n");
			i.append("acct1.Signature = "+payPalAccount.getString("account_signature")+"\n");
			i.append("acct1.AppId = "+payPalAccount.getString("app_id")+"\n");
			i.append("http.ConnectionTimeOut=3000"+"\n");
			i.append("http.Retry=2"+"\n");
			i.append("http.ReadTimeOut=60000"+"\n");
			i.append("http.MaxConnection=100"+"\n");
			i.append("http.TrustAllConnection=false"+"\n");		
			
		  
	 
			i.append("http.MaxConnection=100"+"\n");
			i.append("service.Binding=NV"+"\n");
			i.append("service.EndPoint="+type+"\n");
			InputStream inputStream = new ByteArrayInputStream(i.toString().getBytes());
			return inputStream;
		}catch (Exception e) {
 
			throw new SystemException(e,"getInputStream",log);
		} 
		
	 
	}
}
