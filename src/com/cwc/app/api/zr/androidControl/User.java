package com.cwc.app.api.zr.androidControl;

import java.util.ArrayList;
import java.util.List;

public class User implements PermissionSource{

	private long userId ;
	private String userName ;
	
	private List<Role> userRoles = new ArrayList<Role>();
	private List<AndroidPermission> personalPermission  = new ArrayList<AndroidPermission>(); 	 
	private List<AndroidPermission> subtractPermission  = new ArrayList<AndroidPermission>();		 
	
	
	
	
	
	
	
	public User(long userId, String userName) {
		super();
		this.userId = userId;
		this.userName = userName;
	}
 
	public List<AndroidPermission> getUserAllPermission(){
		PermissionArrayList array = new PermissionArrayList();
		for(Role role : userRoles){
			List<AndroidPermission> rolePermission	 = role.getRolesPermission();
			for(AndroidPermission permission : rolePermission){
				array.add(permission);
			}
		}
		for(AndroidPermission permission : personalPermission){
			array.add(permission);
		}
		for(AndroidPermission permission : subtractPermission){
			array.remove(permission);
		}
		return array ;
	}
	
	public void addSubtractPermission(AndroidPermission permission){
		subtractPermission.add(permission);
	}
	
	public void addUserRole(Role role){
		userRoles.add(role);
	}
	
	public void addPersonalPermission(AndroidPermission permission){
		permission.setFrom(this);
		personalPermission.add(permission);
	}
	
	
	@Override
	public String form() {
 		return "[Personal]";
	}
}
