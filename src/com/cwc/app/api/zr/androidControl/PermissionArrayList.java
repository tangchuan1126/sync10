package com.cwc.app.api.zr.androidControl;

import java.util.ArrayList;
import java.util.Collection;

import com.cwc.service.android.action.mode.HoldDoubleValue;

/**
 * permissionArrayList
 * @author win7zr
 *
 */
public class PermissionArrayList extends ArrayList<AndroidPermission> {
	
	
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public boolean add(AndroidPermission e) {
		HoldDoubleValue<Boolean, AndroidPermission> holdValue =  containsPermission(e);
 		if(holdValue.a){
 			AndroidPermission 	permission = holdValue.b ;
 			permission.getTempPermissionSource().add(e.getFrom());
 			return true ;
 		} else{
 			e.getTempPermissionSource().add(e.getFrom());
  			return super.add(e);
 		}
	}

	public HoldDoubleValue<Boolean, AndroidPermission> containsPermission(AndroidPermission e){
		int index = indexOf(e) ;
		if(index > -1){
			return new HoldDoubleValue<Boolean, AndroidPermission>(true, get(index));
		}
		return new HoldDoubleValue<Boolean, AndroidPermission>(false, null) ;
	}
	
	
	
	@Override
	public boolean addAll(Collection<? extends AndroidPermission> c) {
		 throw new RuntimeException("Not support");
	}
	@Override
	public void add(int index, AndroidPermission element) {
		throw new RuntimeException("Not support");
	}
}
