package com.cwc.app.api.zr.androidControl;

import java.util.ArrayList;
import java.util.List;

public class AndroidPermission {

	
	private long permissionId ;
	private long parentPermissionId ;
	private String permissionName ;
	private String description ;
	private String url ;
	private int index = 0 ;
	private PermissionSource from ;
	private List<PermissionSource> tempPermissionSource = new ArrayList<PermissionSource>();
	
	//是否选中了
	private boolean isCheck = false ;
	private List<AndroidPermission> subPermission ; 
	
	public AndroidPermission(long permissionId, String permissionName , String description ,long parentPermissionId , int index) {
		super();
		this.permissionId = permissionId;
		this.permissionName = permissionName;
		this.description = description ;
		this.parentPermissionId = parentPermissionId ;
		this.index = index ;
	}

	
	
	public long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(long permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public PermissionSource getFrom() {
		return from;
	}

	public void setFrom(PermissionSource from) {
		this.from = from;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof AndroidPermission){
			AndroidPermission permission = (AndroidPermission)obj ;
			if(permission.getPermissionId() == permissionId){
				return true ;
			}
		}
 		return super.equals(obj);
	}

	public String getSource(){
		StringBuilder b = new StringBuilder();
		if(tempPermissionSource != null && tempPermissionSource.size() > 0 ){
			for(PermissionSource source : tempPermissionSource){
				b.append(","+source.form());
			}
		}
		return b.length() > 1 ? b.substring(1):"" ;
	}
	
	public List<PermissionSource> getTempPermissionSource() {
		return tempPermissionSource;
	}

	public String getDescription() {
		return description;
	}



	public long getParentPermissionId() {
		return parentPermissionId;
	}


	public void setParentPermissionId(long parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}



	public List<AndroidPermission> getSubPermission() {
		return subPermission;
	}


	public void setSubPermission(List<AndroidPermission> subPermission) {
		this.subPermission = subPermission;
	}
	
}
