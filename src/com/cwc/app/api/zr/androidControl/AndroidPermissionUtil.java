package com.cwc.app.api.zr.androidControl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.json.types.JsonArray;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

public class AndroidPermissionUtil {

	
	/**
	 * 把Android permission数组装换成树形的结构,过来的 数据是按照parent_id 排序的根节点是0
	 * @param array
	 * @return
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	public static List<AndroidPermission> convertArrayToTreeType(AndroidPermission[] array){
		LinkedList<AndroidPermission> listArrays = new LinkedList<AndroidPermission>();
		listArrays.addAll(Arrays.asList(array));
		List<AndroidPermission> rootPermissions = findParentSubPermission(listArrays, 0);
		for(AndroidPermission root : rootPermissions){
			tree(listArrays, root);
		}
		JSONArray jsonArray = new JSONArray(rootPermissions);
		 
  		return rootPermissions  ;
	}
	/**
	 * 构造Tree
	 * @param listArrays
	 * @param node
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	private static void tree(LinkedList<AndroidPermission> listArrays ,AndroidPermission node){
		long permissionId = node.getPermissionId() ;
		List<AndroidPermission> subPermission =	findParentSubPermission(listArrays, permissionId);
 		node.setSubPermission(subPermission);
		for(AndroidPermission permission : subPermission){
			tree(listArrays, permission);
		}
	}
	 /**
	  * 找一个ParentPermission的SubPermission
	  * @param listArrays
	  * @param parentId
	  * @return
	  * @author zhangrui
	  * @Date   2014年9月29日
	  */
	private static List<AndroidPermission>  findParentSubPermission(LinkedList<AndroidPermission> listArrays , long parentId){
 		List<AndroidPermission> list = new ArrayList<AndroidPermission>();
		if(listArrays != null && listArrays.size() > 0){
			for(int index = 0 , count = listArrays.size() ; index < count ; index++ ){
				AndroidPermission permission = listArrays.get(index);
					if(permission.getParentPermissionId() == parentId){
						list.add(listArrays.get(index));
					} 
					
					 
			}
		}
		return list  ;
	}
	/**
	 * 
	 [{
			name:"Access Control",
			sub:[
			    {
				name: "Gate",
				sub:[
				         	{name:"Gate CheckIn"},
				         	{name:"Gate Check Out", sub:[]},
				         	{name:"Photo Caputrue", sub:[]}
				    ]
				},
				{
					name: "Yard",
					sub:[
					         	{name:"Patrol ",sub:[]},
					         	{name:"Shuttle", sub:[]} 
					    ]
				},
				{
					name: "Ware House",
					sub:[
					         	{name:"Dock Check In ",sub:[]},
					         	{name:"Load-Work", sub:[]} 
					    ]
				}
			]
		},
		{
			name:"Access ",
			sub:[
			     {name:"ces"}
			    ]
		}]
	 */
	public static JSONArray convertPermissionToJson(List<AndroidPermission> permissions) throws JSONException{
		JSONArray returnArray = new JSONArray();
		if(permissions != null && permissions.size() > 0){
			for(AndroidPermission permission : permissions){
				JSONObject jsonObject = new JSONObject() ;
				jsonObject.put("id", permission.getPermissionId());
				jsonObject.put("name", permission.getPermissionName());
				List<AndroidPermission> subPermissioin = permission.getSubPermission() ;
				if(subPermissioin != null && subPermissioin.size() > 0){
					JSONArray sub = convertPermissionToJson(subPermissioin);
					jsonObject.put("sub", sub);
				}
				returnArray.put(jsonObject);
			}
		}
		return returnArray ;
		
	}
	/**
	 * 检查这个Tree 是否正确 , 只要ParentPermission 不是为0l
	 * @param array
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	public static void checkAndroidPermission(List<AndroidPermission> list){
		if(list != null && list.size() > 0){
			for(AndroidPermission permission : list){
				if(permission.getParentPermissionId() != 0l){
					throw new RuntimeException("permission : " + permission.getPermissionId() + " set Error");
				}
			}
		}
	}
	/**
	 * convert DBRow[] to androidPermission[]
	 * @param arrays
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	public static AndroidPermission[] convertDBRowToAndroidPermission(DBRow[] arrays) throws Exception {
		if(arrays != null && arrays.length > 0){
			AndroidPermission[] returnArray = new AndroidPermission[arrays.length];
			for(int index =  0 , count = arrays.length ; index < count ; index++){
				DBRow tempRow = arrays[index] ;
 				AndroidPermission temp = new AndroidPermission(tempRow.get("id", 0l), 
 						tempRow.getString("title"), 
 						tempRow.getString("title"),tempRow.get("parent_id", 0l), tempRow.get("id", 0));
 				returnArray[index] = temp ;
			}
			return returnArray ;
		}
 		return null;
	}
	/**
	 * android 设置登录
	 * @param loginRow
	 * @param request
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月30日
	 */
	public static AdminLoginBean  androidSetSession(DBRow loginRow , HttpServletRequest request) throws Exception {
		 
		
		AdminLoginBean adminLoggerBean = new AdminLoginBean();
		adminLoggerBean.setAccount(loginRow.getString("LoginAccount"));
 		adminLoggerBean.setAdid(loginRow.get("adid",0l));
	 
		adminLoggerBean.setEmploye_name(loginRow.getString("employe_name"));
		adminLoggerBean.setIsLogin();
		DBRow[] warehouses = (DBRow[])loginRow.get("warehouses", new Object());
		adminLoggerBean.setDepartment((DBRow[])loginRow.get("departments", new Object()));
		adminLoggerBean.setWarehouse(warehouses);
		adminLoggerBean.setPs_id((warehouses[0].get("WAREID", 0l)));
		
		HttpSession session = request.getSession(true);
		session.setAttribute(Config.adminSesion,adminLoggerBean);
		return adminLoggerBean;
	}
	
	
	
}
