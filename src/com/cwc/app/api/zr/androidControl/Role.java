package com.cwc.app.api.zr.androidControl;

import java.util.ArrayList;
import java.util.List;

public class Role implements PermissionSource{

	private long roleId ;
	private String roleName ;
	
	private List<AndroidPermission> rolesPermission = new ArrayList<AndroidPermission>();

	
	public Role(long roleId, String roleName) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public List<AndroidPermission> getRolesPermission() {
		return rolesPermission;
	}

	public void addRolePermission(AndroidPermission permission){
		permission.setFrom(this);
		rolesPermission.add(permission);
	}
	
	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public void setRolesPermission(List<AndroidPermission> rolesPermission) {
		this.rolesPermission = rolesPermission;
	}
	@Override
	public String form() {
 		return "[Role:"+roleName+"]";
	}
	
	
}
