package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorCommonFileMgrZr;
import com.cwc.app.iface.zr.CommonFileMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class CommonFileMgrZr implements CommonFileMgrIfaceZr{
	
	private static Logger log = Logger.getLogger("ACTION");
	private FloorCommonFileMgrZr floorCommonFileMgrZr;
	
	 
	public void setFloorCommonFileMgrZr(FloorCommonFileMgrZr floorCommonFileMgrZr) {
		this.floorCommonFileMgrZr = floorCommonFileMgrZr;
	}


	@Override
	public long addCommonFile(DBRow row) throws Exception {
		try{
			return floorCommonFileMgrZr.addFile(row);
		}catch (Exception e) {
			throw new SystemException(e, "addCommonFile", log);
		}
	}


	@Override
	public DBRow getCommonFileById(long id) throws Exception {
		 try{
			 return floorCommonFileMgrZr.getCommonFileByFileId(id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getCommonFileById", log);
		}
	}

	
}
