package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorPayMentMethodMgrZr;
import com.cwc.app.iface.zr.PayMentMethodIfaceZR;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class PayMentMethodMgrZr implements PayMentMethodIfaceZR{

	static Logger log = Logger.getLogger("ACTION");
	private FloorPayMentMethodMgrZr floorPayMentMethodZr;
	
	
	public void setFloorPayMentMethodZr(FloorPayMentMethodMgrZr floorPayMentMethodZr) {
		this.floorPayMentMethodZr = floorPayMentMethodZr;
	}

	@Override
	public void addPayMentMethod(DBRow row) throws Exception {
		try {
			floorPayMentMethodZr.addPayMethod(row);
		} catch (Exception e) {
			throw new SystemException(e, "addPayMentMethod", log);
		}
		
	}

	@Override
	public DBRow[] getAllPayMentMethod(PageCtrl page) throws Exception {
		try {
			return	floorPayMentMethodZr.getAllPayMentMethod(page);
		} catch (Exception e) {
			throw new SystemException(e, "addPayMentMethod", log);
		}
	}

	@Override
	public DBRow[] getPayMentMethodByKey(PageCtrl page, int key , int isTip)
			throws Exception {
		try {
			return floorPayMentMethodZr.getAllPayMentMethodByKey(key, page,isTip);
		} catch (Exception e) {
			throw new SystemException(e, "getPayMentMethodByKey", log);
		}
	}

	@Override
	public void updatePayMentMethod(DBRow row, int key) throws Exception {
		try {
			floorPayMentMethodZr.updatePayMentMethodByDBRow(row, key);
		} catch (Exception e) {
			throw new SystemException(e, "updatePayMentMethod", log);
		}
	}

	@Override
	public boolean isHasTipPayMent(int key) throws Exception {
		try {
			return floorPayMentMethodZr.isHasTipPayMentMethod(key);
		} catch (Exception e) {
			throw new SystemException(e, "updatePayMentMethod", log);
		}
	}

	@Override
	public void deletePayMentMethod(long id) throws Exception {
		try {
			  floorPayMentMethodZr.deletePayMentMethod(id);
		} catch (Exception e) {
			throw new SystemException(e, "deletePayMentMethod", log);
		}
	}
}
