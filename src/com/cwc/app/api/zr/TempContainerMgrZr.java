package com.cwc.app.api.zr;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zr.FloorAndroidMgrZr;
import com.cwc.app.floor.api.zr.FloorTempContainerMgrZr;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zr.TempContainerMgrIfaceZr;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.RelationBean;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class TempContainerMgrZr implements TempContainerMgrIfaceZr{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorTempContainerMgrZr floorTempContainerMgrZr ;
	
	private FloorAndroidMgrZr floorAndroidMgrZr ;
	
 	
 	
	private FloorProductStoreMgr productStoreMgr;
	
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	
	private FloorProductMgr floorProductMgr;
	
	@Override
	public DBRow findContainerByNumber(String number) throws Exception {
		try
		{
			return floorTempContainerMgrZr.findContainerByNumber(number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findContainerByNumber(String number)",log);
		}
	}
	@Override
	public void scatterContainer(long parentContainerId, long container_id) throws Exception {
		try{
			  
		 }catch (Exception e) {
			 throw new SystemException(e,"scatterContainer",log);
		}
	}

	/**
	 * 盘点添加临时容器，添加完临时容器后像图数据库临时库添加容器节点
	 */
	public long addTempTLPContainer(String container, long title_id)
		throws Exception 
	{
		try
		{
			 DBRow insertRow = new DBRow();
 			 insertRow.add("container", container);
			 insertRow.add("hardwareId", "");
			 insertRow.add("type_id", -1);
			 insertRow.add("container_type", ContainerTypeKey.TLP);
			 insertRow.add("is_full", ContainerProductState.FULL);		//TLP 始终是满的
			 insertRow.add("is_has_sn", ContainerHasSnTypeKey.NOSN)	;
			 
			 insertRow.add("title_id", title_id);
 			 long id = floorTempContainerMgrZr.addTempContainer(insertRow);
 			 
 			 //像图数据库添加节点
 			 Map<String,Object> containerNode = new HashMap<String, Object>();
 			 containerNode.put("con_id",id);
 			 containerNode.put("container_type",ContainerTypeKey.TLP);
 			 containerNode.put("type_id",-1);
 			 containerNode.put("container",container);
 			 containerNode.put("is_full",ContainerProductState.FULL);
 			 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
 			 productStoreMgr.addNode(0,NodeType.Container,containerNode);
 			 
			 return id ;
		 }
		catch (Exception e) 
		{
			throw new SystemException(e,"addTempTLPContainer",log);
		}
	}
	@Override
	public long addContainer(DBRow container) throws Exception {
		try{
			 container.add("is_full", ContainerProductState.EMPTY);
			 container.add("is_has_sn", ContainerHasSnTypeKey.NOSN);
			 long id = floorTempContainerMgrZr.addTempContainer(container);
			
			 //像图数据库添加节点
			 Map<String,Object> containerNode = new HashMap<String, Object>();
			 containerNode.put("con_id",id);
 			 containerNode.put("container_type",container.get("container_type", 0));
 			 containerNode.put("type_id",container.get("type_id", 0l));
 			 containerNode.put("container",container.getString("container"));
 			 containerNode.put("is_full",ContainerProductState.EMPTY);
 			 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
			 productStoreMgr.addNode(0,NodeType.Container,containerNode);
			return id ;
		}catch (Exception e) {
			throw new SystemException(e,"addContainer",log);
		}
 	}
	 
	
	/**
	 * @author zhanjie
	 * 使用图数据看方式提交盘点扫描的托盘数据，未做容器是否满的校验
	 */
	public DBRow submitContainerTemp(Map<String,Object> data,long ps_id)
		throws Exception 
	{
		try 
		{
			
			
			Long[] allContainerIds = (Long[]) data.get("allContainerIds");
			for (int i = 0; i < allContainerIds.length; i++) 
			{
				//验证临时容器是否存在
				checkTempContainerIsExist(allContainerIds[i],ps_id);
				
				Map<String,Object> containerNode = new HashMap<String, Object>();
				containerNode.put("con_id",allContainerIds[i]);
				//对临时库内原容器与商品的关系解除
				productStoreMgr.removeRelations(0l,NodeType.Container,NodeType.Product,containerNode,new HashMap<String,Object>(),new HashMap<String,Object>());
				//对临时库内容器与容器关系解除
//				productStoreMgr.removeRelations(0l,NodeType.Container,NodeType.Container,containerNode,containerNode,new HashMap<String,Object>());
			}
			
			DBRow[] containerProduct =  (DBRow[])data.get("products");
			for (int i = 0; i < containerProduct.length; i++) 
			{
				Map<String,Object> containerNode = new HashMap<String, Object>();
				containerNode.put("con_id",containerProduct[i].get("cp_lp_id",0l));
				
				
				long pc_id = containerProduct[i].get("cp_pc_id",0l);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				HashMap mangoDBProduct = productStoreMgrZJ.productMongoDB(pc_id);
				long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
				long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
				
				Map<String,Object> productNode = new HashMap<String, Object>();
				productNode.put("pc_id",pc_id);
				productNode.put("title_id",containerProduct[i].get("title_id",0l));
				productNode.put("product_line",product_line_id);
				productNode.put("catalogs",catalogs);
				productNode.put("p_name",product.getString("p_name"));
				productNode.put("union_flag",product.get("union_flag",0));
				productStoreMgr.addNode(0,NodeType.Product,productNode);//添加商品节点

				//移除商品节点不参与搜索的属性
				productNode.remove("product_line");
				productNode.remove("catalogs");
				productNode.remove("union_flag");
				
				Map<String,Object> relProps = new HashMap<String, Object>();
				relProps.put("quantity",(int)containerProduct[i].get("cp_quantity",0d));
				relProps.put("locked_quantity",0);
				
				productStoreMgr.addRelation(0l,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);
				
				
			}
			
			DBRow[] containers = (DBRow[]) data.get("containers");
			for (int i = 0; i < containers.length; i++) 
			{
				Map<String,Object> fromContainerNode = new HashMap<String, Object>();
				fromContainerNode.put("con_id",containers[i].get("parent_con_id",0l));
				
				Map<String,Object> toContainerNode = new HashMap<String, Object>();
				toContainerNode.put("con_id",containers[i].get("con_id",0l));
				
				if(containers[i].get("parent_con_id",0l) == 0l || containers[i].get("con_id",0l) == 0l){
					continue ;
				}
				RelationBean[] relations = productStoreMgr.searchRelations(0,NodeType.Container,NodeType.Container,new HashMap<String, Object>(), toContainerNode,new HashMap<String,Object>());
				//删除原子容器与父容器的关系
				if (relations.length>1)
				{
					throw new Exception();
				}
				else if (relations.length==1)
				{
					RelationBean relation = relations[0];
					Map<String,Object> fromNode = relation.getFromNodeProps();
					Map<String,Object> toNode = relation.getToNodeProps();
					
					HashMap<String,Object> parentContainerNode = new HashMap<String, Object>();
 					parentContainerNode.put("con_id", Long.parseLong(fromNode.get("con_id").toString()));
					
					
					HashMap<String,Object> sonContainerNode = new HashMap<String, Object>();
					sonContainerNode.put("con_id",(Long.parseLong(toNode.get("con_id").toString())));
					
					productStoreMgr.removeRelations(0l,NodeType.Container,NodeType.Container,parentContainerNode,sonContainerNode,new HashMap<String,Object>());
				}
				
				productStoreMgr.addRelation(0,NodeType.Container,NodeType.Container,fromContainerNode,toContainerNode,new HashMap<String,Object>());
			}
			
			DBRow result = new DBRow();
			result.add("ret", BCSKey.SUCCESS);
			return result ;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"submitContainer",log);
		}
	}
	
	 

	/**
	 * 临时表中有没有这个Container,如果没有那么就需要从真实的Container表中拷贝这些记录,图数据库一样
	 * @param con_id
	 * @throws Exception
	 */
	private void checkTempContainerIsExist(long con_id,long ps_id) 
		throws Exception
	{
		try
		{
			DBRow row = floorTempContainerMgrZr.getContainer(con_id);
			if(row == null)
			{
				row = floorAndroidMgrZr.getContainer(con_id);
				if(row != null)
				{
					floorTempContainerMgrZr.copyToTempContainer(row);	
				}
			}
			
			String result = productStoreMgr.containerTree(0, con_id, null, 0L, 0l, null, null).toString();
			if (result.equals("{}"))
			{
				result = productStoreMgr.containerTree(ps_id, con_id, null, 0L, 0l, null, null).toString();
				if (result.equals("{}"))
				{
					throw new ContainerNotFoundException();
				}
				else
				{
					productStoreMgr.copyTree(ps_id,0,con_id,null);
					productStoreMgr.removeTree(ps_id, con_id);
				}
			}
		}
		catch (ContainerNotFoundException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkTempContainerIsExist",log);
		}
	}
 

	public void setFloorAndroidMgrZr(FloorAndroidMgrZr floorAndroidMgrZr) {
		this.floorAndroidMgrZr = floorAndroidMgrZr;
	}
	
	public void setFloorTempContainerMgrZr(FloorTempContainerMgrZr floorTempContainerMgrZr) {
		this.floorTempContainerMgrZr = floorTempContainerMgrZr;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productstoreMgrZJ) {
		this.productStoreMgrZJ = productstoreMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
}
