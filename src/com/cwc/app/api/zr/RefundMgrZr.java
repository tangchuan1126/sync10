package com.cwc.app.api.zr;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorRefundMgrZr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zr.RefundMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleReplayMgrIfaceZR;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.RefundResultTypeKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.WorkFlowTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.freight.action.ccc.UpdateFreightCost;
import com.cwc.util.StringUtil;

public class RefundMgrZr implements RefundMgrIfaceZr{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorRefundMgrZr floorRefundMgrZr ;
	private OrderMgrIFace orderMgr;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private ScheduleReplayMgrIfaceZR scheduleReplayMgrZr ;
	private AdminMgrIFace adminMgr; 
	
	
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setScheduleReplayMgrZr(ScheduleReplayMgrIfaceZR scheduleReplayMgrZr) {
		this.scheduleReplayMgrZr = scheduleReplayMgrZr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setFloorRefundMgrZr(FloorRefundMgrZr floorRefundMgrZr) {
		this.floorRefundMgrZr = floorRefundMgrZr;
	}

	@Override
	public long addRefund(HttpServletRequest request) throws Exception {
		try{
			HttpSession ses = StringUtil.getSession(request);
			String refundType = StringUtil.getString(request, "refundType"); 
			String currencyCode =StringUtil.getString(request, "currencyCode");
			long create_adid = StringUtil.getLong(request, "create_adid");
			String userName = adminMgr.getDetailAdmin(create_adid).getString("employe_name");
			String note = StringUtil.getString(request, "note");
			String txn_id = StringUtil.getString(request, "tranID");
			long oid = StringUtil.getLong(request, "oid");
			double totalAmount = StringUtil.getDouble(request, "totalAmount");
			double amount = StringUtil.getDouble(request, "amount");
			
			DBRow refundRow = new DBRow();
			refundRow.add("refund_type", refundType);
			refundRow.add("create_adid", create_adid);
			refundRow.add("note", note);
			refundRow.add("txn_id", txn_id);
			refundRow.add("total_amount", totalAmount);
			refundRow.add("oid", oid);
			refundRow.add("amount", amount);
			refundRow.add("currency_code", currencyCode);
			refundRow.add("create_date",   DateUtil.DatetimetoStr(new Date()));
			
			refundRow.add("refund_result", RefundResultTypeKey.APPLY);
			
			
			String orderNote="";
			if(!refundType.equals("Full")){
				orderNote ="申请部分退款："+amount+" "+currencyCode;
			}else{
				orderNote ="申请全额退款："+StringUtil.getDouble(request, "totalAmount")+" "+currencyCode;
			}
			
			orderMgr.addPOrderNotePrivate(StringUtil.getLong(request, "oid"), orderNote,TracingOrderKey.OTHERS, ses,0);
			long refund_id =  floorRefundMgrZr.addRefund(refundRow);

			//给客服的主管或者是副主管添加任务
			DBRow[] ids = floorRefundMgrZr.getServiceMangnerIds();
			StringBuffer sbIds = new StringBuffer();
			
			for(DBRow id : ids){
				sbIds.append(",").append(id.get("adid", 0l));
			}
			String time = DateUtil.DatetimetoStr(new Date()) ;
			if(sbIds.length() > 1){
				String emailTitle = userName+"为 订单["+oid+"] " + orderNote;
				scheduleMgrZr.addWorkFlowSchedule(time, time, create_adid, sbIds.substring(1), "", refund_id, ModuleKey.WORK_FLOW_REFUND , emailTitle, emailTitle,true, false, false, request, true, 0, 1, WorkFlowTypeKey.DRWABACK);
			}
			
			
			
			return refund_id;
		}catch (Exception e) {
			throw new SystemException(e,"addRefund",log);
		}
	}

	@Override
	public DBRow getRefundByRefundId(long refund_id) throws Exception {
		try{
			return floorRefundMgrZr.getRefundRowById(refund_id);
		}catch (Exception e) {
			throw new SystemException(e,"getRefundByRefundId",log);
		}
		 
	}
	/**
	 * 驳回申请
	 * 	1.对应的退款申请状态改变。
	 *  2.同时对应退款申请的任务都应该为完成。所有任务的执行人都完成(客服的主管)
	 *  3.标记任务完成的时候。给任务添加一条跟进的信心。表示谁操作这个任务。然后让他完成了。
	 *  4.给退款申请的创建人创建一个任务。(任务的创建人为他自己)
	 *  5.该
	 */
	@Override
	public void refundReject(HttpServletRequest request) throws Exception {
		try{
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			
		 	long handle_adid = adminLoggerBean.getAdid(); 
			
			long refund_id = StringUtil.getLong(request, "refund_id");
			long schedule_id = StringUtil.getLong(request, "schedule_id");
			long schedule_sub_id = StringUtil.getLong(request, "schedule_sub_id");
			long oid = StringUtil.getLong(request, "oid");
			long create_adid = StringUtil.getLong(request, "create_adid");
			String refund_text = StringUtil.getString(request, "reject");
			
			DBRow updateRefund = new DBRow();
			updateRefund.add("refund_result", RefundResultTypeKey.REJECT);
			 
			floorRefundMgrZr.updateRefund(updateRefund, refund_id);
			//对应任务的完成
			scheduleMgrZr.finishRefundSchedule(request);
			// 添加一个回复
			DBRow replay = new DBRow();
			replay.add("sch_replay_context", adminLoggerBean.getEmploye_name() + " 驳回了该退款申请");
			replay.add("sch_replay_time", DateUtil.DatetimetoStr(new Date()));
			replay.add("sch_replay_type", 1);
			replay.add("schedule_id", schedule_id);
			replay.add("adid", handle_adid);
			replay.add("employe_name", adminLoggerBean.getEmploye_name());
			replay.add("sch_state", 10);
			scheduleReplayMgrZr.addSimpleScheduleReplay(replay);
			//添加一条refund_reject
			DBRow refundReject = new DBRow();
			refundReject.add("reject_text",refund_text );
			refundReject.add("refund_id", refund_id);
			refundReject.add("oid", oid);
			refundReject.add("reject_time",DateUtil.DatetimetoStr(new Date()) );
			refundReject.add("reject_adid", handle_adid);
			refundReject.add("reject_name", adminLoggerBean.getEmploye_name());
			floorRefundMgrZr.addRefundReject(refundReject);
			
			//给该申请退款的创建人创建一个任务(同样是退款的任务关联同一个退款单据)
			addApplyerRejectSchedule(schedule_id, handle_adid, adminLoggerBean.getEmploye_name() ,create_adid, request);
			
			
		}catch (Exception e) {
			throw new SystemException(e,"refundReject",log);
		}
		
	}

	private void addApplyerRejectSchedule(long schedule_id,long handle_adid ,String hand_userName , long create_adid , HttpServletRequest request) throws Exception {
		try{
			 String time = DateUtil.DatetimetoStr(new Date());
			 DBRow schedule = scheduleMgrZr.getScheduleById(schedule_id);
			 String content =  hand_userName + "驳回了" + schedule.getString("schedule_overview") ;
			 scheduleMgrZr.addWorkFlowSchedule(time, time, handle_adid, create_adid+"", "", schedule.get("associate_id", 0l), schedule.get("associate_type", 0), content,content, true, false, false, request, true, 0 , 1, schedule.get("task_type", 0));
			
		}catch (Exception e) {
			throw new SystemException(e,"addApplyerRejectSchedule",log);
		}
		
	}

	@Override
	public DBRow[] getAllRejectReasonsByRefundId(long refund_id)
			throws Exception {
		try{
			return	floorRefundMgrZr.getRefundRejuectByRefundId(refund_id);
		}catch (Exception e) {
			throw new SystemException(e,"getAllRejectReasonsByRefundId",log);
		}
		 
	}

	@Override
	public void refundAgree(HttpServletRequest request) throws Exception {
		 try{
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 long create_adid = StringUtil.getLong(request, "create_adid");
			 long handle_adid = adminLoggerBean.getAdid(); 
			 //更新refund 
			 long refund_id = StringUtil.getLong(request, "refund_id");
			 long schedule_id = StringUtil.getLong(request, "schedule_id");
			 
			 DBRow refundRow = new DBRow();
			 refundRow.add("refund_result", RefundResultTypeKey.AGREE);
			 floorRefundMgrZr.updateRefund(refundRow, refund_id);
			//对应任务的完成
			scheduleMgrZr.finishRefundSchedule(request);
			
			// 添加一个回复
			DBRow replay = new DBRow();
			replay.add("sch_replay_context", adminLoggerBean.getEmploye_name() + " 同意了该退款申请");
			replay.add("sch_replay_time", DateUtil.DatetimetoStr(new Date()));
			replay.add("sch_replay_type", 1);
			replay.add("schedule_id", schedule_id);
			replay.add("adid", handle_adid);
			replay.add("employe_name", adminLoggerBean.getEmploye_name());
			replay.add("sch_state", 10);
			scheduleReplayMgrZr.addSimpleScheduleReplay(replay); 
			addAgreeRefundSchedule(schedule_id, handle_adid, adminLoggerBean.getEmploye_name(), create_adid, request);
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"refundAgree",log);
		}
	}
	private void addAgreeRefundSchedule(long schedule_id,long handle_adid ,String hand_userName , long create_adid , HttpServletRequest request) throws Exception {
		try{
			 String time = DateUtil.DatetimetoStr(new Date());
			 DBRow schedule = scheduleMgrZr.getScheduleById(schedule_id);
			 String content =  hand_userName + "同意了" + schedule.getString("schedule_overview") ;
			 scheduleMgrZr.addWorkFlowSchedule(time, time, handle_adid, create_adid+"", "", schedule.get("associate_id", 0l), schedule.get("associate_type", 0), content,content, true, false, false, request, true, 0 , 1, schedule.get("task_type", 0));
			
		}catch (Exception e) {
			throw new SystemException(e,"addApplyerRejectSchedule",log);
		}
		
	}
}
