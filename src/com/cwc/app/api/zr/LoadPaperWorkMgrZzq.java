package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.iface.zr.LoadPaperWorkMgrIfaceZzq;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public class LoadPaperWorkMgrZzq implements LoadPaperWorkMgrIfaceZzq{
	static Logger log = Logger.getLogger("ACTION");
	
	public DBUtilAutoTran dbUtilAutoTran;

	@Override
	public DBRow[] findPalletCombinationFromByOrder(String order_no,
			String CompanyID, String CustomerID, long adid,
			HttpServletRequest request) throws Exception {
		DBRow[] dbrows = fFindPalletCombinationFrom(order_no, CompanyID, CustomerID);
		//assemble new DBRow[]
		Map<String, List<DBRow>> datas = new HashMap<String, List<DBRow>>();
		String plate_no = dbrows[0].getString("plate_no");
		for(DBRow palletRow: dbrows){
			String order_no_from = palletRow.getString("order_no_from");
			String item_id = palletRow.getString("item_id");
			String shipped_qty_changed = palletRow.getString("shipped_qty_changed");
			
			//DBRow[] orderArray;
			List<DBRow> orderArray;
			if(datas.containsKey(order_no_from)){
				orderArray = datas.get(order_no_from);
			}else{
				orderArray = new ArrayList<DBRow>();
			}
			
			DBRow row = new DBRow();
			row.put("item_id", item_id);
			row.put("shipped_qty_changed", shipped_qty_changed);
			
			orderArray.add(row);
			datas.put(order_no_from, orderArray);
		}
		Iterator<String> keys = datas.keySet().iterator();
		DBRow[] data = new DBRow[datas.size()];
		int i = 0;
		DBRow tempRow;
		if(keys.hasNext()){
			String key = keys.next();
			tempRow = new DBRow();
			tempRow.add("order_no", key);
			tempRow.add("details", (DBRow[])datas.get(key).toArray(new DBRow[datas.size()]));
			tempRow.add("plate_no", plate_no);
			data[i] = tempRow;
			i = i++;
		}
		return data;
	}
	
	public DBRow[] fFindPalletCombinationFrom(String order_no,
			String CompanyID, String CustomerID) throws Exception{
		StringBuffer conditions = new StringBuffer();
		conditions.append(" and order_no = " + order_no );
		conditions.append(" and company_id = " + "'" + CompanyID + "'");
		conditions.append(" and supplier_id = " + "'" + CustomerID + "'");
		try{
			String sql = "select * from consolidate_order_plates where plate_no != plate_no_from " + conditions.toString();
			////system.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectUseDoor"+e);
		 }
	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
