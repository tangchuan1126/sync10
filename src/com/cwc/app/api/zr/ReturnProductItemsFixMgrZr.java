package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zr.FloorCheckReturnProductItemMgrZr;
import com.cwc.app.floor.api.zr.FloorReturnProductItemsFix;
import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.floor.api.zyj.FloorReturnProductOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.ReturnProductItemsFixMgrIfaceZr;
import com.cwc.app.iface.zr.ReturnProductMgrIfaceZr;
import com.cwc.app.iface.zr.ReturnProductSubItemMgrIfaceZr;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ReturnProductCheckItemTypeKey;
import com.cwc.app.key.ReturnProductItemMatchKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductLogTypeKey;
import com.cwc.app.key.ReturnProductPictureRecognitionkey;
import com.cwc.app.key.ServiceOrderStatusKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class ReturnProductItemsFixMgrZr implements ReturnProductItemsFixMgrIfaceZr
{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorReturnProductItemsFix floorReturnProductItemsFix ;
	private AdminMgrIFace adminMgr ;
	private FileMgrIfaceZr fileMgrZr ;
	private FloorProductMgr floorProductMgr; 
	private ReturnProductMgrIfaceZr returnProductMgrZr;
	private ReturnProductSubItemMgrIfaceZr returnProductSubItemMgrZr;
	private FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr ;
	private FloorServiceOrderMgrZyj floorServiceOrderMgrZyj;
	private FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj;
	private FloorReturnProductMgrZr floorReturnProductMgrZr;
	private FloorCatalogMgr floorCatalogMgr;
	
	/**
	 *	1.首先放到session 当中。
	 *	2.然后在一起提交
	 */
	@Override
	public long addReturnProductItmesFix(HttpServletRequest request)
			throws Exception
	{
		try{
			//如果是type为is_exits表示的是先创建的退货单。然后现在是补齐商品图片信息
			String is_exits = StringUtil.getString(request, "return_product_item_is_exits");
			boolean isCreateReturnProduct = is_exits.equals("yes");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			String note = StringUtil.getString(request, "note");
			 
			String file_names = StringUtil.getString(request, "file_names");
			long rp_id = StringUtil.getLong(request, "rp_id");
			int quantity = StringUtil.getInt(request, "quantity");
			DBRow data = new DBRow() ;
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long adid = adminLoggerBean.getAdid(); 
			String nowDate = DateUtil.DatetimetoStr(new Date()) ;
			data.add("create_adid", adid);
			data.add("create_date", nowDate);
			data.add("rp_id", rp_id);
			data.add("note", note);
			if(isCreateReturnProduct){
				data.add("is_relation_product", ReturnProductItemMatchKey.RELATION);
				data.add("rpi_id",rpi_id );
			}else{
				data.add("is_relation_product", ReturnProductItemMatchKey.NOT_RELATION);
			}
			data.add("quantity", quantity);
			
			long fix_rpi_id = floorReturnProductItemsFix.addReturnProdcutItemsFix(data);
			
			 if(file_names.length() > 0 ){
				 String[] arrayFile = file_names.split(",");
				 String sn  = StringUtil.getString(request, "sn");
				 String file_with_type = StringUtil.getString(request, "file_with_type");
				 String path = StringUtil.getString(request, "path");
				 String baseUrl = Environment.getHome()+"upl_imags_tmp/";
				 for(String tempFileName : arrayFile){
					 DBRow tempFile = new DBRow();
					 tempFile.add("file_with_id", fix_rpi_id);
					 tempFile.add("file_with_type", file_with_type);
					 tempFile.add("upload_adid", adid);
					 tempFile.add("upload_time", nowDate);
					 StringBuffer realyFileName = new StringBuffer(sn);
 					 String suffix = StringUtil.getSuffix(tempFileName);
 					 realyFileName.append("_").append(fix_rpi_id).append("_").append(tempFileName.replace("."+suffix, ""));
 					 int indexFile = fileMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
 					 if(indexFile != 0){
 						realyFileName.append("_").append(indexFile);
 					 }
	 					
	 				 realyFileName.append(".").append(suffix);
					 tempFile.add("file_name",  realyFileName.toString());
					 fileMgrZr.saveFile(tempFile);
					 FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
				 }
				 	
			 }
 			 if(isCreateReturnProduct){
				 //这个时候更新ReturnProductItemZr上面的数据
				 DBRow updateReturnProductItem = new DBRow();
				 updateReturnProductItem.add("items_fix_rpi_id", fix_rpi_id);
				 updateReturnProductItem.add("is_merge_item", 0);
				 updateReturnProductItem.add("is_receive_item", 1);
				 floorReturnProductItemsFix.updateReturnProductItem(updateReturnProductItem, rpi_id);
			 }
			//更改主单据状态,如果原来已经收集完成，改成收集未完成
 			DBRow returnRow	= floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
 			if(1 == returnRow.get("gather_picture_status", 0))
 			{
 				DBRow returnOrderRow = new DBRow();
 				returnOrderRow.add("gather_picture_status", 0);
 				floorReturnProductMgrZr.updateReturnProduct(returnOrderRow, rp_id);
 			}
			 //增加退货单日志
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,fix_rpi_id, returnRow.get("status", 0), "退货单["+rp_id+"]图片采集", ReturnProductLogTypeKey.PICTURE, adminLoggerBean.getAdid(), DateUtil.NowStr());
			return 0l ;
		}catch (Exception e) {
			throw new SystemException(e, "addReturnProductItmesFix", log);
		}
	}



	@Override
	public DBRow[] getReturnProductItmesFix(long rp_id) throws Exception
	{
		try{
			  return floorReturnProductItemsFix.getReturnProdcutItemsFixByRPId(rp_id) ;
		}catch (Exception e) {
			throw new SystemException(e, "getReturnProductItmesFix", log);
		}
	}



	@Override
	public void addReturnProductItems(HttpServletRequest request)
			throws Exception
	{
		try{
			//添加商品到session中。
			String p_name = StringUtil.getString(request,"p_name");
			
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			
			if(product==null)
			{
				throw new Exception();
			}
			long pc_id = product.get("pc_id",0l);
			float quantity = StringUtil.getFloat(request,"quantity");
			String reason = StringUtil.getString(request,"reason");
			int warranty_type = StringUtil.getInt(request,"warranty_type");
			
			removeReturnProductReason(StringUtil.getSession(request), pc_id);//删除重复商品
			
			put2CartReturnProductReason(StringUtil.getSession(request),p_name,pc_id,quantity,reason,warranty_type);//将商品质保放入质保明细购物车
			
			   
		}catch (Exception e) {
			throw new SystemException(e, "addReturnProductItems", log);
		}
		
	}
	private void removeReturnProductReason(HttpSession session,long pc_id)throws Exception{
		if (session.getAttribute(Config.returnProductItem) != null ){
				DBRow rowOld;
				ArrayList<DBRow> al = (ArrayList<DBRow>)session.getAttribute(Config.returnProductItem);
				
				for ( int i=0;i<al.size(); i++ )
				{
					rowOld =  al.get(i);
					if ( StringUtil.getLong(rowOld.getString("cart_pid")) == pc_id)
					{
						al.remove(i);
					}
				}
			
				session.setAttribute(Config.returnProductItem,al);
		}
		
	}
	private void put2CartReturnProductReason(HttpSession session,String p_name,long pc_id,float quantity,String reason,int warranty_type) throws Exception{
		ArrayList<DBRow> al = null;
	
		if ( pc_id != 0 &&quantity>0)
		{
			
			//购物车每条记录属性
			DBRow row = new DBRow();
			
			row.add("p_name",p_name);
			row.add("cart_pid",pc_id);//商品ID
			row.add("cart_quantity",quantity);
			row.add("return_reason",reason);
			row.add("warranty_type",warranty_type);
			
			if (session.getAttribute(Config.returnProductItem) == null )
			{
				
				
				al = new ArrayList<DBRow>();
				al.add(row);			
			}
			else
			{
				boolean nOrR = true;
				int listIndex = 0;
				al = (ArrayList)session.getAttribute(Config.returnProductItem);
				for (int i = 0; i < al.size(); i++) 
				{
					if(al.get(i).get("cart_pid",0l)==pc_id)
					{
						listIndex = i;
						nOrR = false;
						break;
					}
				}
				if(nOrR)
				{
					al.add(row);
				}
				else
				{
					al.set(listIndex,row);
				}
				
			}
			session.setAttribute(Config.returnProductItem,al);			
		}
	}



	@Override
	public DBRow  flushSession(HttpSession session) throws Exception
	{
		DBRow result = new DBRow();
		DBRow[] productReasons = this.getSimpleProducts(session);
		 
		DBRow newProductReasons[] = null;
		ArrayList<DBRow> newProductsAL = new ArrayList<DBRow>();
		
		float sum_quantity = 0;
		for (int i=0; i<productReasons.length; i++)
		{
			
			sum_quantity += productReasons[i].get("cart_quantity",0f);
			
			DBRow newProductReason = new DBRow();
			newProductReason.append(productReasons[i]);
			newProductsAL.add(newProductReason);
		}
		newProductReasons = (DBRow[])newProductsAL.toArray(new DBRow[0]);
		
		result.add("data", newProductReasons);
		result.add("sumQuantity", sum_quantity);
		return result;
	 
	}
	@Override
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.returnProductItem) != null )
		{
			ArrayList<DBRow> al = (ArrayList<DBRow>)session.getAttribute(Config.returnProductItem);
			
			//深层克隆
			ArrayList<DBRow> newAL = new ArrayList<DBRow>();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow =  al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}



	@Override
	public void clearAllReturnProductCart(HttpSession session) throws Exception
	{
		if(session.getAttribute(Config.returnProductItem)!=null)
		{
			session.removeAttribute(Config.returnProductItem);
		}
	}



	@Override
	public void clearReturnProductCart(HttpServletRequest request) throws Exception
	{
		long pc_id = StringUtil.getLong(request,"pc_id");
		removeReturnProductReason(request.getSession(), pc_id);
	}



	@Override
	public void updateReturnProductCart(HttpServletRequest request) throws Exception
	{
		String[] pcids = request.getParameterValues("pc_id");
		
		HttpSession session = StringUtil.getSession(request);
		
		if ( session.getAttribute(Config.returnProductItem) != null )
		{
			if(pcids!=null&&pcids.length>0)
			{
				ArrayList al = (ArrayList)session.getAttribute(Config.returnProductItem);
				
				for (int i = 0; i < pcids.length; i++) 
				{
					for (int j = 0; j < al.size(); j++)
					{
						DBRow productReason = (DBRow)al.get(j);
						if(productReason.getString("cart_pid").equals(pcids[i]))
						{
							float quantity = StringUtil.getFloat(request,pcids[i]+"_quantity");
							String return_reason = StringUtil.getString(request,"return_reason_"+pcids[i]);
							int warranty_type = StringUtil.getInt(request,pcids[i]+"_waranty_type");
							
							productReason.add("cart_quantity",quantity);
							productReason.add("return_reason",return_reason);
							productReason.add("warranty_type",warranty_type);
							
							al.set(j,productReason);
							
							break;
						}
					}
				}
				
				session.setAttribute(Config.returnProductItem,al);
			}
		}
	}
	@Override
	public void putToReturnProductReasonCartForSelect(HttpServletRequest request) throws Exception{
		String value = StringUtil.getString(request,"value");
		long remove_pc_id = 0;
		long pc_id = 0;
		
		HttpSession session = StringUtil.getSession(request);
		
		if(value.contains("_"))//如果是XXX_XXX形式，那说明选择的是套装中的散件，要从购物车中清除套装商品再加入散件商品
		{
			String[] ids = value.split("_");
			remove_pc_id = Long.parseLong(ids[0]);
			removeReturnProductReason(session,remove_pc_id);
			pc_id = Long.parseLong(ids[1]);
		}
		else
		{
			pc_id = Long.parseLong(value);
			
			DBRow[] productUnions = floorProductMgr.getProductUnionsBySetPid(pc_id);
			
			for (int i = 0; i < productUnions.length; i++) 
			{
				this.removeReturnProductReason(session, productUnions[i].get("pid",0l));
			}
		}
		
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		
		put2CartReturnProductReason(session, product.getString("p_name"),pc_id, 1, "", 0);
	}
	/**
	 * 判断购物车是否为空
	 * @return
	 * @throws Exception 
	 */
	public boolean isEmpty(HttpSession session) throws Exception
	{
		if ( session.getAttribute(Config.cartWarrantySession) == null )
		{
			return(true);
		}
		else
		{
			return( getSimpleProducts(session).length==0 );
		}
	}



	@Override
	public void addReturnProductItemsFormSession(HttpServletRequest request)
			throws Exception
	{
		 try{
			String create_time =  DateUtil.DatetimetoStr(new Date()); 
			long rp_id = StringUtil.getLong(request, "rp_id");
			 //从session中读取数据,然后插入到数据库中
			 DBRow[] rows = getSimpleProducts(request.getSession());
			 if(rows != null && rows.length > 0 ){
				 for(DBRow rowTemp : rows){
					 
					 DBRow dataTemp = new DBRow() ;
					 
					 
					 dataTemp.add("create_time", create_time);
					 dataTemp.add("pc_id",  rowTemp.get("cart_pid", 0l));
					 dataTemp.add("warranty_type",  rowTemp.get("warranty_type",0));
					 dataTemp.add("return_reason", rowTemp.getString("return_reason"));
					 dataTemp.add("quantity", rowTemp.get("cart_quantity", 0.0f));
					 dataTemp.add("rp_id", rp_id);
					 
					 floorReturnProductItemsFix.addReturnProductItemReason(dataTemp);
					 
				 }
				 //清空session
				 this.clearAllReturnProductCart(request.getSession());
			 }
			 
		 }catch (Exception e) {
				throw new SystemException(e, "addReturnProductItemsFormSession", log);
		}
	}
 
	@Override
	public DBRow[] getReturnProductReason(long rp_id) throws Exception
	{
		try{
			return floorReturnProductItemsFix.getReturnProductReasonByRpId(rp_id);
		}catch (Exception e) {
			throw new SystemException(e, "getReturnProductReason", log);
		}
	}


	/**
	 * 为图片关联上商品。然后update图片上的信息。表示这个关联已经有了
	 * 关联上的商品的信息保存在return_product_items_zr上.
	 * 
	 */
	@Override
	public long addReturnProductItemFormPictrue(HttpServletRequest request)
			throws Exception
	{ 
		try{
			String p_name = StringUtil.getString(request, "p_name");
			long items_fix_rpi_id = StringUtil.getLong(request, "items_fix_rpi_id");
			long rp_id = StringUtil.getLong(request,"rp_id");
			double quantity = StringUtil.getDouble(request, "quantity");
			int handle_result_request  = StringUtil.getInt(request, "handle_result_request");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product==null)
			{
				throw new RuntimeException("Product "+p_name+" not found.");
			}
			DBRow returnProductItem = new DBRow() ;
			returnProductItem.add("p_name", p_name);
			returnProductItem.add("pid", product.get("pc_id", 0l));
			returnProductItem.add("quantity", 1.0);
			returnProductItem.add("unit_name", product.getString("unit_name"));
			returnProductItem.add("product_type", product.get("union_flag", 0) + 1);
			returnProductItem.add("rp_id", rp_id);
			returnProductItem.add("quantity", quantity);
			returnProductItem.add("items_fix_rpi_id", items_fix_rpi_id);
			returnProductItem.add("handle_result_request", handle_result_request);
			returnProductItem.add("is_receive_item", 1); //表示收到退货。
			long id = floorReturnProductItemsFix.addReturnProductItem(returnProductItem);
			
			//然后更新return_product_items_fix表示这组图片关联上
			DBRow updateItemFix = new DBRow() ;
			updateItemFix.add("is_relation_product",ReturnProductItemMatchKey.RELATION);
			updateItemFix.add("rpi_id",id);
			floorReturnProductItemsFix.updateReturnProductItemFix(updateItemFix, items_fix_rpi_id);
			 
			return id;
		}catch (Exception e) {
			throw new SystemException(e, "addReturnProductItemFormPictrue", log);
		}
	}


	private boolean isAllItemPictrueAllRelation(long rp_id) throws Exception {
		try{
			boolean flag = true ;
			DBRow[] rows = floorReturnProductItemsFix.getReturnProdcutItemsFixByRPId(rp_id);
			if(rows != null && rows.length > 0 ){
				for(DBRow row : rows){
					if(row.get("is_relation_product", ReturnProductItemMatchKey.NOT_RELATION) == ReturnProductItemMatchKey.NOT_RELATION){
						flag = false ;
						break ;
					}
				}
			}else{flag = false;}
			return flag;
		}catch (Exception e) {
			throw new SystemException(e, "isAllItemPictrueAllRelation", log);
		}
	}
	@Override
	public DBRow[] getReturnProductItemByRPIdAndFixId(long items_fix_rpi_id,
			long rp_id) throws Exception
	{
		try{
			 return floorReturnProductItemsFix.getReturnProductItemByRpIdAndFixId(items_fix_rpi_id, rp_id);
		}catch (Exception e) {
			throw new SystemException(e, "getReturnProductItemByRPIdAndFixId", log);
		}
	}



	@Override
	public void setReturnProductItemFixNotMatch(HttpServletRequest request)
			throws Exception
	{
		try{
			long fix_rpi_id = StringUtil.getLong(request, "fix_rpi_id");
			DBRow updateRow = new DBRow() ;
			updateRow.add("is_relation_product", ReturnProductItemMatchKey.NOT_MATCH);
			floorReturnProductItemsFix.updateReturnProductItemFix(updateRow, fix_rpi_id);
		}catch (Exception e) {
			throw new SystemException(e, "setReturnProductItemFixNotMatch", log);
		}
	}



	@Override
	public DBRow[] getReturnItemsByRpId(long rp_id) throws Exception
	{
		try{
			return floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
		}catch (Exception e) {
			throw new SystemException(e, "getReturnItemsByRpId", log);
		} 
	}



	@Override
	public void updateReturnProductItem(HttpServletRequest request)
			throws Exception
	{
		try{
		String p_name = StringUtil.getString(request, "p_name");
		long rpi_id = StringUtil.getLong(request, "rpi_id");
		double quantity = StringUtil.getDouble(request, "quantity");
		int handle_result_request = StringUtil.getInt(request, "handle_result_request");
		DBRow product = floorProductMgr.getDetailProductByPname(p_name);
		
		if(product==null)
		{
			throw new RuntimeException("Product "+p_name+" not found.");
		}
		DBRow returnProductItem = new DBRow() ;
		returnProductItem.add("p_name", p_name);
		returnProductItem.add("pid", product.get("pc_id", 0l));
		 
		returnProductItem.add("unit_name", product.getString("unit_name"));
		returnProductItem.add("product_type", product.get("union_flag", 0) + 1);
		returnProductItem.add("quantity", quantity);
		returnProductItem.add("handle_result_request", handle_result_request);
		floorReturnProductItemsFix.updateReturnProductItem(returnProductItem, rpi_id);		
		}catch (Exception e) {
			throw new SystemException(e, "updateReturnProductItem", log);
		} 
	}



	@Override
	public void deleteReturnProductItem(HttpServletRequest request)
			throws Exception
	{
		 try{
			 // 删除已经关联上的商品.然后改变该退货单的状态为关联没有完成.图片信息表上update 为
			 long rpi_id = StringUtil.getLong(request, "rpi_id");
			 long rp_id = StringUtil.getLong(request, "rp_id");
			 String fix_rpid_id = StringUtil.getString(request, "fix_rpid_id");
			 //删除商品明细
			 floorReturnProductItemsFix.deleteReturnProductItem(rpi_id);
			 //更新明细图片
			 if(fix_rpid_id.trim().length() > 0 && fix_rpid_id.split(",") != null) {
				 String[] array = fix_rpid_id.split(",");
				 for(String tempId : array ){
					 DBRow updateItemFix = new DBRow();
					 updateItemFix.add("is_relation_product", ReturnProductItemMatchKey.NOT_RELATION);
					 updateItemFix.add("rpi_id",0l);
					 floorReturnProductItemsFix.updateReturnProductItemFix(updateItemFix, Long.parseLong(tempId));
				 }
			 }
			 //删除该商品上对应的测试的信息
			 deleteCheckReturnProductInfo(rpi_id);
			 
			 //更新主单据.图片关联没有完成
			 //如果没有了需要测试的图片.那么就要更新主单据上面的信息
			 DBRow updateReturnProductRow = new DBRow();
			 updateReturnProductRow.add("status", ReturnProductKey.WAITING);//StorageReturnProductStatusKey.NONE_FINISH
			 updateReturnProductRow.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionNoneFinish);
			 updateReturnProductRow.add("return_product_check", returnProductReturncheck(rp_id));
			 returnProductMgrZr.updateReturnProduct(updateReturnProductRow, rp_id);
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			 //增加退货单日志
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]删除已识别商品", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
			 
		 }catch (Exception e) {
			 throw new SystemException(e, "deleteReturnProductItem", log);
		}
		
	}


	//图片页面关联上退货商品.
	/**
	 * 传入数据file_id ,rp_id
	 * 
	 * 1.每次都是新建立一个return_product_items_fix数据(id)。
	 * 2.修改file 表中的对应file_with_id的值(id).
	 * 3.创建return_product_item_zr(关联上数据)
	 * 4.检查更新数据.整个退货单是否都关联上
	 *  
	 * 
	 */
	@Override
	public void relationReturnProduct(HttpServletRequest request)
			throws Exception
	{
		try{
			String p_name = StringUtil.getString(request, "p_name");
			String fix_rpi_ids = StringUtil.getString(request, "fix_rpi_ids");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product==null){
				throw new RuntimeException("Product "+p_name+" not found.");
			}
			 
			//1.每次都是新建立一个return_product_items_fix数据(id)。
			long rp_id = StringUtil.getLong(request, "rp_id");
			DBRow data = new DBRow() ;
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long adid = adminLoggerBean.getAdid(); 
			String nowDate = DateUtil.DatetimetoStr(new Date()) ;
			data.add("create_adid", adid);
			data.add("create_date", nowDate);
			data.add("rp_id", rp_id);
		 
			data.add("is_relation_product", ReturnProductItemMatchKey.RELATION);
			long fix_rpi_id = floorReturnProductItemsFix.addReturnProdcutItemsFix(data);
			
			//2.修改file 表中的对应file_with_id的值(id).
			String file_ids = StringUtil.getString(request, "file_ids");
			String[] fileArray = file_ids.split(",");
			if(fileArray != null && fileArray.length > 0){
				for(String strFileId : fileArray){
					long file_id = Long.parseLong(strFileId);
					if(file_id != 0l){
						DBRow tempFileRow = new DBRow();
						tempFileRow.add("file_with_id",fix_rpi_id );
						fileMgrZr.updateFile(tempFileRow, file_id);
						
					}
				}
			}
			//3.创建return_product_item_zr(关联上数据)
			DBRow returnProductItem = new DBRow() ;
			returnProductItem.add("p_name", p_name);
			returnProductItem.add("pid", product.get("pc_id", 0l));
			returnProductItem.add("quantity", 1.0);
			returnProductItem.add("unit_name", product.getString("unit_name"));
			returnProductItem.add("product_type", product.get("union_flag", 0) + 1);
			returnProductItem.add("rp_id", rp_id);
			returnProductItem.add("items_fix_rpi_id", fix_rpi_id);
			floorReturnProductItemsFix.addReturnProductItem(returnProductItem);
			//删除已经没有file文件的returnProductItemFix
			deleteNotHavePictrueReturnProductFixItem(fix_rpi_ids);
		}catch (Exception e) {
			throw new SystemException(e, "relationReturnProduct", log);
		}
		
	}
	private void deleteNotHavePictrueReturnProductFixItem(String fix_rpi_ids) throws Exception {
		try{
			String[] array = fix_rpi_ids.split(",");
			if(array != null && array.length > 0 ){
				for(int index = 0 , count = array.length ; index < count ; index++ ){
					long file_with_id =Long.parseLong(array[index]);
					DBRow[] rows = fileMgrZr.getFilesByFileWithIdAndFileWithType(file_with_id, FileWithTypeKey.RETURN_PRODUCT_DETAIL);
					if(rows.length < 1){
						//删除一条记录
						floorReturnProductItemsFix.deleteReturnProductItemsFix(file_with_id);
					}
				}
				
			}
		}catch (Exception e) {
			throw new SystemException(e, "deleteNotHavePictrueReturnProductFixItem", log);
		}
		
		
	}



	@Override
	public void deleteReturnProductItemFix(HttpServletRequest request)
			throws Exception
	{
		try{
			long fix_rpi_id = StringUtil.getLong(request, "fix_rpi_id");
			DBRow fixRow = floorReturnProductItemsFix.gerReturnProductItemFixByFixRpiId(fix_rpi_id);
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(fixRow.get("rp_id", 0L));
			floorReturnProductItemsFix.deleteReturnProductItemsFix(fix_rpi_id);
			fileMgrZr.deleteFilesByFileWidthIdAndFileWithType(fix_rpi_id, FileWithTypeKey.RETURN_PRODUCT_DETAIL);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(returnRow.get("rp_id", 0L),0,0,fix_rpi_id, returnRow.get("status", 0), "退货单["+returnRow.get("rp_id", 0L)+"]删除明细文件", ReturnProductLogTypeKey.PICTURE, adminLoggerBean.getAdid(), DateUtil.NowStr());
			
		}catch (Exception e) {
			throw new SystemException(e, "deleteReturnProductItemFix", log);
		}
	}


	/**
	 * 1.只有是有图片 && 所有的图片都关联完成 才会放回true
	 */
	@Override
	public boolean isRelationOver(long rp_id) throws Exception
	{
		try{
			DBRow[] items = floorReturnProductItemsFix.getReturnProdcutItemsFixByRPId(rp_id);
			if(items != null && items.length > 0){
				for(DBRow tempRow : items){
					if(tempRow.get("is_relation_product", 0) != ReturnProductItemMatchKey.RELATION){
						return false;
					}
				}
				return true ;
			}else{
				return false ;
			}
		 
		}catch (Exception e) {
			throw new SystemException(e, "isRelationOver", log);
		}
	
	}

	/** 
	 *		1. 多个图片合并成一个商品
	 *		2. 读取多个fix_rpi_id 
	 *		3.
	 *
	 */

	@Override
	public void mergeReturnProductItems(HttpServletRequest request)
			throws Exception {
		try{
			String p_name = StringUtil.getString(request, "p_name");
			String fix_rpi_ids = StringUtil.getString(request, "fix_rpi_ids");
			double quantity  = StringUtil.getDouble(request, "quantity");
			int handle_result_request = StringUtil.getInt(request, "handle_result_request");
			long rp_id = StringUtil.getLong(request, "rp_id");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			int product_type = StringUtil.getInt(request, "product_type");
	 		int all_select = StringUtil.getInt(request, "all_select");
	 		int is_test = StringUtil.getInt(request, "is_test");
	 		long rpi_id = 0l;
			if(product==null){
				throw new RuntimeException("Product "+p_name+" not found.");
			}
		 
			//创建退货的商品
			//2.创建return_product_item_zr(关联上数据)
			if(fix_rpi_ids.length() > 0 && fix_rpi_ids.split(",").length > 0){
				DBRow returnProductItem = new DBRow() ;
				returnProductItem.add("p_name", p_name);
				returnProductItem.add("pid", product.get("pc_id", 0l));
				returnProductItem.add("quantity", quantity);
				returnProductItem.add("unit_name", product.getString("unit_name"));
				returnProductItem.add("product_type", product.get("union_flag", 0) + 1);
				returnProductItem.add("rp_id", rp_id);
				returnProductItem.add("is_merge_item", 1);
				returnProductItem.add("handle_result_request",handle_result_request);
				returnProductItem.add("is_receive_item", 1); //表示收到了退件。
				 rpi_id = floorReturnProductItemsFix.addReturnProductItem(returnProductItem);
				//更新fix_rpi_id 。is_relation_product.关联上
				String[] array = fix_rpi_ids.split(",");
				for(String fix_rpi_id : array){
					long fixId = Long.parseLong(fix_rpi_id);
					DBRow tempFixReturnProductItem = new DBRow();
					tempFixReturnProductItem.add("rpi_id", rpi_id);
					tempFixReturnProductItem.add("is_relation_product", ReturnProductItemMatchKey.RELATION);
					floorReturnProductItemsFix.updateReturnProductItemFix(tempFixReturnProductItem, fixId);
				}
			}
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			//添加是否需要测试
			if(is_test == 1){
	 			//添加这个明细需要测试
	 			addCheckReturnProductItem(request,rpi_id, rp_id);
	 			//增加退货单日志
				floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 		}
			
			//添加subItem
			addReturnProductSubItem(request, rpi_id);
			//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]合并商品图片", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
			
		}catch (Exception e) {
			throw new SystemException(e, "mergeReturnProductItems", log);
		}
	}



	@Override
	public DBRow getReturnProductItemByRpiId(long rpi_id) throws Exception {
		try{
			
			return floorReturnProductItemsFix.getReturnProductItemByRpiId(rpi_id);
		}catch (Exception e) {
			throw new SystemException(e, "getReturnProductItemByRpiId", log);
		}
	}



 



	@Override
	public void updateReturnProductItemFixIsNeedCheck(long rp_id, long rpi_id)
			throws Exception {
		 try{
			 
		
			DBRow updateRetrunProductItemFixRow = new DBRow();
			//更新Return_Product_Item_zr
		 	updateRetrunProductItemFixRow.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
		 	floorReturnProductItemsFix.updateReturnProductItem(updateRetrunProductItemFixRow, rpi_id);
		 	//更新return_product_fix
		 	DBRow updateReturnProduct = new DBRow();
		 	updateReturnProduct.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
		 	
		 	returnProductMgrZr.updateReturnProduct(updateReturnProduct, rp_id);
		 	
		 }catch(Exception e) {
				throw new SystemException(e, "updateReturnProductItemFixIsNeedCheck", log);
		 }
	}



	@Override
	public void deleteCheckReturnProductInfo(long rpi_id) throws Exception {
		 try{
			 DBRow[] checks = getCheckReturnProductByRpiId(rpi_id);
			 if(checks != null && checks.length > 0){
				 for(DBRow row : checks){
					 floorReturnProductItemsFix.deleteCheckReturnProduct(row.get("check_return_product_id", 0l));
					 fileMgrZr.deleteFilesByFileWidthIdAndFileWithType(row.get("check_return_product_id", 0l), FileWithTypeKey.RETURN_CHECK_PRODUCT);
				 }
			 }
			 
			 
		 }catch (Exception e) {
			 throw new SystemException(e, "deleteCheckReturnProductInfo", log); 
		}		
	}
	private DBRow[] getCheckReturnProductByRpiId(long rpi_id) throws Exception {
		try{
			return floorReturnProductItemsFix.getCheckReturnProductItem(rpi_id);
		}catch (Exception e) {
			 throw new SystemException(e, "getCheckReturnProductByRpiId", log); 
		}
	}

	// 获取主单据上的return_product_check的状态
	private int returnProductReturncheck(long rp_id) throws Exception {
		try{
			DBRow[] rows = floorReturnProductItemsFix.getReturnProductCheckByRPId(rp_id);
			if(rows == null || rows.length < 1) {
				return ReturnProductCheckItemTypeKey.OUTCHECK ;
			}
			boolean isFinish = true ;
			for(DBRow row : rows){
				if(row.get("return_product_check", ReturnProductCheckItemTypeKey.CHECK) != ReturnProductCheckItemTypeKey.CHECK_FINISH){
					isFinish = false ;
					break ;
				}
			}
			return isFinish?ReturnProductCheckItemTypeKey.CHECK_FINISH:ReturnProductCheckItemTypeKey.CHECK;
			
		}catch (Exception e) {
			 throw new SystemException(e, "returnProductReturncheck", log); 
		}
	}
	 
	@Override
	public void updateReturnProductItemAndReturnProductCheckIsFinish(
			long rp_id, long rpi_id) throws Exception {
		try{
			 DBRow updateReturnProductRow = new DBRow();
			 updateReturnProductRow.add("return_product_check",ReturnProductCheckItemTypeKey.CHECK_FINISH);
			 floorReturnProductItemsFix.updateReturnProductItem(updateReturnProductRow, rpi_id);
			 //检查所有的returnProductItem是否都是已经测试完成了。
			 DBRow[] rows = floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
			 boolean flag = true ;
			 if(rows != null  && rows.length > 0 ){
				 for(DBRow rowTemp : rows){
					 if(rowTemp.get("return_product_check", ReturnProductCheckItemTypeKey.CHECK) != ReturnProductCheckItemTypeKey.CHECK_FINISH){
						 flag = false ;
						 break;
					 }
				 }
			 }
			 if(flag){
				 DBRow updateReturnProduct = new DBRow();
				 updateReturnProduct.add("return_product_check",ReturnProductCheckItemTypeKey.CHECK_FINISH );
				 returnProductMgrZr.updateReturnProduct(updateReturnProduct, rp_id);
				 
			 }
			
		}catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductItemAndReturnProductCheckIsFinish", log); 
		}
		
	}



	@Override
	public void updateReturnProductItemHandResult(HttpServletRequest request)
			throws Exception {
		 try{
			 DBRow updateRow = new DBRow();
		
			 int handle_result = StringUtil.getInt(request, "handle_result");
			 long rpi_id = StringUtil.getLong(request, "rpi_id");
			 updateRow.add("handle_result", handle_result);
			 floorReturnProductItemsFix.updateReturnProductItem(updateRow, rpi_id);
			 
		 }catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductItemHandResult", log);
		}
		
	}



	@Override
	public DBRow returnProductItemRecognitionFinish(HttpServletRequest request)
			throws Exception {
		 try{
			 DBRow result = new DBRow();
			 long rp_id = StringUtil.getLong(request, "rp_id");
			 DBRow[] rows = floorReturnProductItemsFix.getReturnProdcutItemsFixByRPId(rp_id);
			 if(rows == null || rows.length < 1 ){
				 result.add("flag", "noproduct");
			 }
			 boolean flag = true ;
			 for(DBRow row : rows){
				 if(row.get("is_relation_product", ReturnProductItemMatchKey.NOT_RELATION) == ReturnProductItemMatchKey.NOT_RELATION){
					 flag = false;
					 result.add("flag", "notfinish");
					 break ;
				 } 
			 }
			 
			 if(flag){
				 DBRow updateRow = new DBRow();
				 updateRow.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionFinish);
				 returnProductMgrZr.updateReturnProduct(updateRow, rp_id);
				 result.add("flag", "success");
			 }
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			 //增加退货单日志
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,0,0,0, returnRow.get("status", 0), "退货单["+returnRow.get("rp_id", 0L)+"]商品识别完成", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
			 
			 return result;
		 }catch (Exception e) {
			 throw new SystemException(e, "returnProductItemRecognitionFinish", log);
		}
		
	}



	@Override
	public DBRow[] getReturnProductItemfixByRpiId(long rpi_id) throws Exception {
		 try{
			 return floorReturnProductItemsFix.getReturnProductItemFixByRpiId(rpi_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getReturnProductItemfixByRpiId", log);
		}
	}



	@Override
	public DBRow getReturnProductItemfFixByFixRpiId(long fix_rpi_id) throws Exception {
		 try{
			  return floorReturnProductItemsFix.gerReturnProductItemFixByFixRpiId(fix_rpi_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getReturnProductItemfFixByFixRpiId", log);
		}
	}



	@Override
	public void updateReturnProductItemFix(HttpServletRequest request)
			throws Exception {
		 try{
				long fix_rpi_id = StringUtil.getLong(request, "fix_rpi_id");
				String note = StringUtil.getString(request, "note");
				String file_names = StringUtil.getString(request, "file_names");
				double quantity = StringUtil.getDouble(request, "quantity");
				DBRow updateReturnProductItemFix = new DBRow();
				updateReturnProductItemFix.add("quantity", quantity);
				updateReturnProductItemFix.add("note", note);
				
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
				long adid = adminLoggerBean.getAdid(); 
				if(file_names.trim().length() > 0 ){
					String nowDate = DateUtil.DatetimetoStr(new Date()) ;
					 String[] arrayFile = file_names.split(",");
					 String sn  = StringUtil.getString(request, "sn");
					 String file_with_type = StringUtil.getString(request, "file_with_type");
					 String path = StringUtil.getString(request, "path");
					 String baseUrl = Environment.getHome()+"upl_imags_tmp/";
					 for(String tempFileName : arrayFile){
						 DBRow tempFile = new DBRow();
						 tempFile.add("file_with_id", fix_rpi_id);
						 tempFile.add("file_with_type", file_with_type);
						 tempFile.add("upload_adid", adid);
						 tempFile.add("upload_time", nowDate);
						 StringBuffer realyFileName = new StringBuffer(sn);
	 					 String suffix = StringUtil.getSuffix(tempFileName);
	 					 realyFileName.append("_").append(fix_rpi_id).append("_").append(tempFileName.replace("."+suffix, ""));
	 					 int indexFile = fileMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
	 					 if(indexFile != 0){
	 						realyFileName.append("_").append(indexFile);
	 					 }
		 					
		 				 realyFileName.append(".").append(suffix);
						 tempFile.add("file_name",  realyFileName.toString());
						 fileMgrZr.saveFile(tempFile);
						 FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
					 }
				}
				floorReturnProductItemsFix.updateReturnProductItemFix(updateReturnProductItemFix, fix_rpi_id);
				DBRow fixRow = floorReturnProductItemsFix.gerReturnProductItemFixByFixRpiId(fix_rpi_id);
				DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(fixRow.get("rp_id", 0L));
				//增加退货单日志
				floorReturnProductOrderMgrZyj.addReturnProductLog(returnRow.get("rp_id", 0L),fixRow.get("rpi_id", 0L),0,fix_rpi_id, returnRow.get("status", 0), "退货单["+returnRow.get("rp_id", 0L)+"]图片采集", ReturnProductLogTypeKey.PICTURE, adminLoggerBean.getAdid(), DateUtil.NowStr());
				
		 }catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductItemFix", log);
		}
	}


	/**
	 * 改变主单据上面的信息(表明这个退件是没有收到)
	 * 同时表明商品明细上,关联的图片的字段为-1
	 */
	@Override
	public void updateReturnProductItemZrNoPictrue(HttpServletRequest request)
			throws Exception {
		try{
			long rp_id = StringUtil.getLong(request, "rp_id");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			
			DBRow updateReturnProductRow = new DBRow();
			updateReturnProductRow.add("items_fix_rpi_id", -1);
			updateReturnProductRow.add("is_receive_item", -1) ; //未收到退件。
			DBRow updateReturn= new DBRow();
			updateReturn.add("status", ReturnProductKey.LITTLE_PRODUCT);//StorageReturnProductStatusKey.LITTLE_PRODUCT
			floorReturnProductItemsFix.updateReturnProduct(rp_id, updateReturn);
			floorReturnProductItemsFix.updateReturnProductItem(updateReturnProductRow, rpi_id);
			DBRow returnRowOriginal	= returnProductMgrZr.getReturnProductByRpId(rp_id);
		  	if(null != returnRowOriginal && 0 != returnRowOriginal.get("sid", 0L))
		  	{
		  		//更新服务单状态
		  		DBRow serviceRow	= new DBRow();
		  		serviceRow.add("status", ServiceOrderStatusKey.LITTLE_PRODUCT);//部分退货
		  		floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, returnRowOriginal.get("sid", 0L));
		  	}
		  	AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]未收到退件商品", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
		  	
		}catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductItemZrNoPictrue", log);
		}
		
	}



	@Override
	public DBRow recognitionReturnProductPictureFinishByStorage(
			HttpServletRequest request) throws Exception {
		try{
			
			long rp_id = StringUtil.getLong(request, "rp_id");
			//查询这个退货单对应的return_product_item
			//遍历return_product_item.如果有没有对应的items_fix_rpi_id 那么就是没有识别完成。然后提示
			//如果items_fix_rpi_id 为-1。那么就是表示的是这个return_product_item没有对应的图片获取到,但是可以让退货单图片识别完成
			DBRow[] items = floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
			DBRow result = new DBRow();
			boolean flag = true ;
			if(items != null && items.length > 0 ){
				for(DBRow row : items){
					if(row.get("items_fix_rpi_id",0l) == 0l){
						flag = false ;
						break ;
					}	
				}
			}
			if(flag){
				 DBRow updateRow = new DBRow();
				 updateRow.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionFinish);
				 returnProductMgrZr.updateReturnProduct(updateRow, rp_id);
				result.add("flag", "success");
			}else{
				result.add("flag", "notfinish");
			}
			return result;
	 
		}catch (Exception e) {
			 throw new SystemException(e, "recognitionReturnProductPictureFinishByStorage", log);
		}
		
		
	}



	@Override
	public void addNewReturnProductItemZr(HttpServletRequest request) throws Exception {
		try{
	 	
		}catch (Exception e) {
			throw new SystemException(e, "addNewReturnProductItemZr", log);
		}
		
	}


	/**
	 * 新的添加退货商品
	 * 	1.退货商品在return_product_item_zr上有
	 * 	2.退货商品在return_product_sub_items表上有
	 *  3.如果是套装散件的那么在return_product_sub_items会保存
	 *  4.如果不是套装那么在return_product_sub_items也会有
	 */
	@Override
	public void relationOneReturnProduct(HttpServletRequest request)
			throws Exception {
	 	 try{
 	 		int product_type = StringUtil.getInt(request, "product_type");
	 		int all_select = StringUtil.getInt(request, "all_select");
	 		int is_test = StringUtil.getInt(request, "is_test");
	 		//添加退货商品
	 		long rpi_id = this.addReturnProductItemFormPictrue(request);
	 		long rp_id = StringUtil.getLong(request,"rp_id");
	 		DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
	 		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
	 		long items_fix_rpi_id = StringUtil.getLong(request, "items_fix_rpi_id");
	 		if(is_test == 1){
	 			//添加这个明细需要测试
	 			addCheckReturnProductItem(request,rpi_id, rp_id);
	 			//增加退货单日志
				floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,items_fix_rpi_id, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 		}
	 		addReturnProductSubItem(request, rpi_id);
	 		//增加退货单日志
			floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,items_fix_rpi_id, returnRow.get("status", 0), "退货单["+rp_id+"]识别商品图片", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 		 
	 		 
	 	 }catch (Exception e) {
	 		throw new SystemException(e, "relationOneReturnProduct", log);
		}
	}

	/**
	 * 一个rpi_id只能是有一个check
	 */
 
	private long addCheckReturnProductItem(HttpServletRequest request,long rpi_id, long rp_id)
			throws Exception {
		try{
			String check_product_context = StringUtil.getString(request, "check_product_context");
		 
			DBRow[] checks = floorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id);
			if(checks != null && checks.length > 0){
				throw new Exception("已经存在一个测试");
			}
			DBRow checkRow = new DBRow();
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long adid = adminLoggerBean.getAdid(); 
			
			String nowDate = DateUtil.DatetimetoStr(new Date()) ;
			
			checkRow.add("create_employee_name", adminLoggerBean.getEmploye_name());
			checkRow.add("check_product_context", check_product_context);
			checkRow.add("create_adid", adid);
			checkRow.add("create_date", nowDate);
			checkRow.add("rpi_id", rpi_id);
			checkRow.add("rp_id", rp_id);
			checkRow.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
			
			floorCheckReturnProductItemMgrZr.addCheckReturnProductItem(checkRow);
			// 然后更新 return_product_item_fix表中的数据表示这个需要需要Test
			updateReturnProductItemFixIsNeedCheck(rp_id, rpi_id);
			return 0l;
		}catch (Exception e) {
			throw new SystemException(e,"addCheckReturnProductItem",log);
		}
	}



	@Override
	public void handleReturnProductItem(HttpServletRequest request) throws Exception {
		try{
			long rp_id = StringUtil.getLong(request, "rp_id");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			String[] rpsi_ids =	request.getParameterValues("rpsi_id");
			if(rpsi_ids != null && rpsi_ids.length > 0 ){
				for(String rpsi_id : rpsi_ids){
					// 更新退货sub上面的信息
					double quality_nor = StringUtil.getDouble(request, rpsi_id+"_nor");
					double quality_damage = StringUtil.getDouble(request,rpsi_id+ "_damage");
					double quality_package_damage = StringUtil.getDouble(request, rpsi_id+"_package_damage");
					////system.out.println("quality_nor :" + quality_nor + "\n" +" quality_damage : "+ quality_damage + "\n" + "quality_package_damage : " + quality_package_damage );
					DBRow updateSubItemRow = new DBRow();
					updateSubItemRow.add("quality_nor", quality_nor);
					updateSubItemRow.add("quality_damage", quality_damage);
					updateSubItemRow.add("quality_package_damage", quality_package_damage);
					returnProductSubItemMgrZr.updateSubReturnProductItem(updateSubItemRow,Long.parseLong(rpsi_id) );
				}
				DBRow updateItemZr = new DBRow();
				updateItemZr.add("is_return_handle",1);
				floorReturnProductItemsFix.updateReturnProductItem(updateItemZr, rpi_id);
				
			}
			
		}catch (Exception e) {
			throw new SystemException(e,"handleReturnProductItem",log);
		}	
	}
	private void addReturnProductSubItem(HttpServletRequest request,long rpi_id) throws Exception{
		try{
			int product_type = StringUtil.getInt(request, "product_type");
	 		int all_select = StringUtil.getInt(request, "all_select");
	 		int is_test = StringUtil.getInt(request, "is_test");
			//如果是套装商品,需要在return_product_sub_items上面添加子明细.a如果是套装下的所有的散件(那么全部添加上).b如果只是一部分的散件(那么就是这一部分)
	 		//如果不是套装商品,需要在return_product_sub_items也要加上这个商品
	 		if(product_type == 1){
	 			if(all_select == 1){
	 				int index = 1 ;
	 				long pid = StringUtil.getLong(request, "pid_"+index);
	 				while(pid != 0l){
	 					String p_name = StringUtil.getString(request, "p_name_"+index);
	 					double quantity = StringUtil.getDouble(request, "quantity_"+index);
	 					String unit_name = StringUtil.getString(request, "unit_name_"+index);
	 					DBRow tempRow = new DBRow();
	 					tempRow.add("p_name", p_name);
	 					tempRow.add("pid", pid);
	 					tempRow.add("quantity", quantity); 
	 					tempRow.add("unit_name", unit_name); 
	 					tempRow.add("rpi_id", rpi_id);
	 					returnProductSubItemMgrZr.addReturnProductSubItem(tempRow);
	 					index++;
	 					pid = StringUtil.getLong(request, "pid_"+index);
	 				}	
	 			}else{
	 				//表示可能只有部分的散件是退件.
	 				int index = 1 ;
	 				long pid = StringUtil.getLong(request, "pid_"+index);
	 				boolean isCheck = StringUtil.getInt(request, "is_checked_"+index) == 1;
	 				while(pid != 0l){
	 					if(isCheck){
		 					String p_name = StringUtil.getString(request, "p_name_"+index);
		 					double quantity = StringUtil.getDouble(request, "quantity_"+index);
		 					String unit_name = StringUtil.getString(request, "unit_name_"+index);
		 					DBRow tempRow = new DBRow();
		 					tempRow.add("p_name", p_name);
		 					tempRow.add("pid", pid);
		 					tempRow.add("quantity", quantity); 
		 					tempRow.add("unit_name", unit_name); 
		 					tempRow.add("rpi_id", rpi_id);
		 					returnProductSubItemMgrZr.addReturnProductSubItem(tempRow);
	 					}
	 					index++;
	 					isCheck = StringUtil.getInt(request, "is_checked_"+index) == 1;
	 					pid = StringUtil.getLong(request, "pid_"+index);
	 				}	
	 			}
	 		}else{
	 				String p_name = StringUtil.getString(request, "p_name");
					double quantity = StringUtil.getDouble(request, "quantity");
					String unit_name = StringUtil.getString(request, "unit_name");
					long pid = StringUtil.getLong(request, "pid");
					DBRow subItem = new DBRow();
					subItem.add("p_name", p_name);
					subItem.add("quantity", quantity);
					subItem.add("unit_name", unit_name);
					subItem.add("rpi_id", rpi_id);
					subItem.add("pid",pid);
					returnProductSubItemMgrZr.addReturnProductSubItem(subItem);
	 		}

			
		}catch (Exception e) {
			throw new SystemException(e,"addReturnProductSubItem",log);
		}
	}


	/**
	 * 在客服创建的退货单上加商品
	 */
	@Override
	public void RmaPictureRelationReturnProductItem(HttpServletRequest request)
			throws Exception {
		try
		{
			long fix_rpi_id = StringUtil.getLong(request, "items_fix_rpi_id");
			long rpi_id = StringUtil.getLong(request, "rpi_id");
			long rp_id = StringUtil.getLong(request,"rp_id");
			int is_test = StringUtil.getInt(request, "is_test");
			if(0 != rpi_id)
			{
				DBRow upRow = new DBRow();
				upRow.add("is_relation_product", ReturnProductItemMatchKey.RELATION);
				upRow.add("rpi_id", rpi_id);
				DBRow updateItemZr = new DBRow();
				updateItemZr.add("is_receive_item", 1);
				int handle_result_request  = StringUtil.getInt(request, "handle_result_request");
				updateItemZr.add("handle_result_request", handle_result_request);
				floorReturnProductItemsFix.updateReturnProductItem(updateItemZr, rpi_id);
				floorReturnProductItemsFix.updateReturnProductItemFix(upRow, fix_rpi_id);
				DBRow[] checks = floorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id);
				if(0 == checks.length)
				{
					if(is_test == 1)
					{
						addCheckReturnProductItem(request,rpi_id, rp_id);
					}
				}
				else
				{
					if(is_test != 1)
					{
						//如果无需测试，删除并更改状态
						floorReturnProductOrderMgrZyj.deleteReturnProductCheckByRpiId(rpi_id);
						DBRow updateRetrunProductItemFixRow = new DBRow();
					 	updateRetrunProductItemFixRow.add("return_product_check", ReturnProductCheckItemTypeKey.OUTCHECK);
					 	floorReturnProductItemsFix.updateReturnProductItem(updateRetrunProductItemFixRow, rpi_id);
					}
				}
			}
			else
			{
		 		//添加退货商品
		 		String p_name = StringUtil.getString(request, "p_name");
				long items_fix_rpi_id = StringUtil.getLong(request, "items_fix_rpi_id");
				double quantity = StringUtil.getDouble(request, "quantity");
				int handle_result_request  = StringUtil.getInt(request, "handle_result_request");
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				if(product==null)
				{
					throw new RuntimeException("Product "+p_name+" not found.");
				}
				DBRow returnProductItem = new DBRow() ;
				returnProductItem.add("p_name", p_name);
				returnProductItem.add("pid", product.get("pc_id", 0l));
				returnProductItem.add("quantity", 1.0);
				returnProductItem.add("unit_name", product.getString("unit_name"));
				returnProductItem.add("product_type", product.get("union_flag", 0) + 1);
				returnProductItem.add("rp_id", rp_id);
				returnProductItem.add("quantity", quantity);
				returnProductItem.add("items_fix_rpi_id", items_fix_rpi_id);
				returnProductItem.add("handle_result_request", handle_result_request);
				returnProductItem.add("is_receive_item", 1); //表示收到退货。
				returnProductItem.add("is_product_not_on_rma", 1);//代表此商品非之前RMA上的商品
				rpi_id = floorReturnProductItemsFix.addReturnProductItem(returnProductItem);
				addReturnProductSubItem(request, rpi_id);//添加商品名细
				
				//然后更新return_product_items_fix表示这组图片关联上
				DBRow updateItemFix = new DBRow() ;
				updateItemFix.add("is_relation_product",ReturnProductItemMatchKey.RELATION);
				updateItemFix.add("rpi_id",rpi_id);
				floorReturnProductItemsFix.updateReturnProductItemFix(updateItemFix, items_fix_rpi_id);
				if(is_test == 1){
		 			//添加这个明细需要测试
		 			addCheckReturnProductItem(request,rpi_id, rp_id);
		 		}
			}
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 //增加退货单日志
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,fix_rpi_id, returnRow.get("status", 0), "退货单["+rp_id+"]识别商品", ReturnProductLogTypeKey.RECOGNITION, adminLoggerBean.getAdid(), DateUtil.NowStr());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"RmaPictureRelationReturnProductItem",log);
		}
		
	}


	/**
	 * 退件全部处理完成
	 * 查询所有的return_product_item_zr 是否都退件处理完成.
	 * 如果有没有收到退件的,那么就是
	 * 通知修改主单据上的信息.状态为退货处理完成
	 * 能够点击退件处理完成.那么一定是识别完成(就是没有多余的图片没有识别)
	 * .如果有遇到没有收到退货的情况是不能让其退件处理完成的
	 */
	@Override
	public DBRow returnProductItemHandleFinish(HttpServletRequest request)
			throws Exception {
		  try{
			  long rp_id = StringUtil.getLong(request, "rp_id");
//			  DBRow[] returnProductItemZrs = floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
			  DBRow result = new DBRow();
//			  boolean flag = true ;
//			  if(returnProductItemZrs != null && returnProductItemZrs.length > 0){
//				  for(DBRow item : returnProductItemZrs){
					  //如果有没有收到退件的,或者是没有处理完成的退货单也可以处理完成，只是入库操作时不处理这些退货明细
//					  if(item.get("is_receive_item", 0) == -1 || item.get("is_return_handle", 0) == 0){
//						  flag = false ;
//						  break ;
//					  }
					  
//				  }
//			  }else{
//				  result.add("flag", "no_product"); 
//			  }
//			  if(flag){
				  	//更新主单据上面的信息
				  	DBRow updateReturnRow = new DBRow();
				  	updateReturnRow.add("status",ReturnProductKey.FINISH);//StorageReturnProductStatusKey.RETURN_PRODUCT_HANDLE_FINISH
				  	returnProductMgrZr.updateReturnProduct(updateReturnRow, rp_id);
				  	DBRow returnRowOriginal	= returnProductMgrZr.getReturnProductByRpId(rp_id);
				  	if(null != returnRowOriginal && 0 != returnRowOriginal.get("sid", 0L))
				  	{
				  		//更新服务单状态
				  		DBRow serviceRow	= new DBRow();
				  		serviceRow.add("status", ServiceOrderStatusKey.FINISH_HANDLE);//处理退货完成
				  		floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, returnRowOriginal.get("sid", 0L));
				  	}
				  	result.add("flag", "success");
//			  }
//		  	  else{
//					result.add("flag", "not_finish");
//			  }
			  return result;
		  }catch (Exception e) {
			  throw new SystemException(e,"returnProductItemHandleFinish",log);
		}
		
	}


	public void setFloorServiceOrderMgrZyj(
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj) {
		this.floorServiceOrderMgrZyj = floorServiceOrderMgrZyj;
	}

	public void setFloorReturnProductOrderMgrZyj(
			FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj) {
		this.floorReturnProductOrderMgrZyj = floorReturnProductOrderMgrZyj;
	}

	public void setFloorReturnProductMgrZr(
			FloorReturnProductMgrZr floorReturnProductMgrZr) {
		this.floorReturnProductMgrZr = floorReturnProductMgrZr;
	}
	
	public void setFloorCheckReturnProductItemMgrZr(
			FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr) {
		this.floorCheckReturnProductItemMgrZr = floorCheckReturnProductItemMgrZr;
	}

	public void setReturnProductSubItemMgrZr(
			ReturnProductSubItemMgrIfaceZr returnProductSubItemMgrZr) {
		this.returnProductSubItemMgrZr = returnProductSubItemMgrZr;
	}

	public void setReturnProductMgrZr(ReturnProductMgrIfaceZr returnProductMgrZr)
	{
		this.returnProductMgrZr = returnProductMgrZr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr)
	{
		this.floorProductMgr = floorProductMgr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr)
	{
		this.fileMgrZr = fileMgrZr;
	}

	public void setFloorReturnProductItemsFix(
			FloorReturnProductItemsFix floorReturnProductItemsFix)
	{
		this.floorReturnProductItemsFix = floorReturnProductItemsFix;
	}



	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
}
