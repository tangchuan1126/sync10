package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.exception.android.ContainerTypeNotFoundException;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zr.FloorContainerMgrZr;
import com.cwc.app.iface.zr.ContainerMgrIfaceZr;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ContainerMgrZr implements ContainerMgrIfaceZr{
	
	private static Logger log = Logger.getLogger("ACTION");

	private FloorContainerMgrZr floorContainerMgrZr ;
	
	private FloorProductStoreMgr productStoreMgr;

	/**
	 * 添加数据到Container 表 && 添加到图数据库
	 */
	@Override
	public DBRow  addContainer(DBRow container) throws Exception {
		try{
		
			if(container.get("is_full",0) == 0 ){
				container.add("is_full", ContainerProductState.EMPTY);
			}
			if(container.get("is_has_sn",0) == 0 ){
				 container.add("is_has_sn", ContainerHasSnTypeKey.NOSN);
 			}
			 DBRow insertRow = floorContainerMgrZr.addContainer(container);
			/* Commented out 库存计算用图数据库，生成容器时不扣库，不向图数据库写数据
			 //像图数据库添加节点
			 Map<String,Object> containerNode = new HashMap<String, Object>();
			 containerNode.put("con_id",insertRow.get("con_id",0l));
 			 containerNode.put("container_type",container.get("container_type", 0));
 			 containerNode.put("type_id",container.get("type_id", 0l));
 			 containerNode.put("container",container.getString("container"));
 			 containerNode.put("is_full",ContainerProductState.EMPTY);
 			 containerNode.put("is_has_sn",ContainerHasSnTypeKey.NOSN);
			 productStoreMgr.addNode(0,NodeType.Container,containerNode);*/
			 return insertRow ;
		}catch (Exception e) {
			throw new SystemException(e, "addContainer", log);
 		}
 	}
	/**
	 * 复制DBRow 但是不能包含DBRow[];
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow copyDBRow(DBRow row) throws Exception{
		DBRow returnRow  = null ;
		if(row != null){
			returnRow = new DBRow();
			List<String> fieldNames = row.getFieldNames();
			for(String field : fieldNames){
				Object object =	row.get(field, new Object());
				if(object instanceof DBRow){
					returnRow.add(field, copyDBRow((DBRow)object));
				}else{
					returnRow.add(field, object);
				}
			}
		}
		return returnRow ;
	}
	 
	@Override
	public DBRow[] createContainer(long container_type_id, int container_type, int number , long title_id , String lotnumber) throws Exception {
		DBRow containerTypes = getContainerType(container_type_id, container_type);
		List<DBRow> returnArray = new ArrayList<DBRow>();
		for(int index = 0 ; index < number ; index++ ){
	  		DBRow copyDBRow = copyDBRow(containerTypes);	 //用复制的DBRow去做
			while(copyDBRow != null && copyDBRow.get("containerTypeId", 0l) != 0l){
				long containerTypeId  = copyDBRow.get("containerTypeId", 0l) ;
				long containerType  = copyDBRow.get("containerType", 0l) ;
				int selftCount = copyDBRow.get("totalSelf", 1);
			    for(int indexSelf = 0 ; indexSelf < selftCount ; indexSelf++ ){
					//添加本身的Lable
					DBRow container = new DBRow();
					container.add("type_id", containerTypeId);
					container.add("container_type", containerType);
					container.add("title_id", title_id);
					container.add("lot_number", lotnumber);
					container.add("is_has_sn", copyDBRow.get("is_has_sn", 0));
					if(containerType == ContainerTypeKey.TLP){
						container.add("is_full", ContainerProductState.FULL);
					}
					 
					DBRow insertRow = addContainer(container);
					returnArray.add(insertRow);
					
			    }
				//打印子Label
				int totalSub = copyDBRow.get("totalInner", 1);
				copyDBRow = (DBRow)copyDBRow.get("innerDBRow", new DBRow());
				copyDBRow.add("totalSelf", totalSub);
			}
		}
		return returnArray.toArray(new DBRow[0]);
	}
	/*
	  *1.  首先去查询 container_type_id 查询是否包含子container，如果包含那么直接查询出来。
	  *DBRow{ 						//容器类型
	  *		containerTypeId :		//容器的类型ID
	  *		containerType :			//容器的类型
	  *		totalInner : 			//子容器总的数量
	  *		totalSelf :             //自己需要打印多少个
	  *		innerContainerTypeId:	//子容器类型ID
	  *		innerContainerType :	//总容器类型		
	  *		innerDBRow:DBRow		//子容器的类型
	  *	}
	  */
	private DBRow getContainerType(long container_type_id, int container_type ) throws Exception{
		try{
			 DBRow row = new DBRow();

		/*	 if(container_type ==  ContainerTypeKey.ILP){
				 DBRow ilp =  floorContainerMgrZr.getfixILPType(container_type_id);
				 row.add("containerTypeId", container_type_id);
				 row.add("containerType", ContainerTypeKey.ILP);
				 if(ilp != null){
					 row.add("is_has_sn", ilp.get("is_has_sn", 0));
				 }
				 return  row ;
			 }*///去掉ILP和BLP
			 if(container_type == ContainerTypeKey.TLP){
 				 row.add("containerTypeId", container_type_id);
				 row.add("containerType", ContainerTypeKey.TLP);
				 return  row ;
			 }
			 if(container_type == ContainerTypeKey.CLP){
				  row = floorContainerMgrZr.getfixCLPType(container_type_id);
				 if(row == null){
					 throw new ContainerTypeNotFoundException();
				 }
				 if(row.get("innerContainerTypeId", 0l) != 0l){
					 DBRow inner = getContainerType(row.get("innerContainerTypeId", 0l), row.get("innerContainerType", 0));
					 row.add("innerDBRow", inner);
				 }
			 }
		/*	 if(container_type == ContainerTypeKey.BLP){
				 row = floorContainerMgrZr.getfixBLPType(container_type_id);
				 if(row == null){
					 throw new ContainerTypeNotFoundException();
				 }
				 if(row.get("innerContainerTypeId", 0l) != 0l){
					 DBRow inner = getContainerType(row.get("innerContainerTypeId", 0l), row.get("innerContainerType", 0));
					 row.add("innerDBRow", inner);
				 }
			 }*///去掉ILP和BLP
			 return row ;
		}catch (Exception e) {
			throw new SystemException(e, "getContainerType", log);
 		}
	}
	
	
	@Override
	public DBRow[] getContainerBaseType(int containerType) throws Exception {
		try{
			return floorContainerMgrZr.getContainerBaseTypes(containerType);
		}catch (Exception e) {
			throw new SystemException(e, "getContainerBaseType", log);
		}
 	}
	
	public void setFloorContainerMgrZr(FloorContainerMgrZr floorContainerMgrZr) {
		this.floorContainerMgrZr = floorContainerMgrZr;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
