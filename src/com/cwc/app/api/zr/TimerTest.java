package com.cwc.app.api.zr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.quartz.CronExpression;

public class TimerTest {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		//格式化日期的表达式，方便看观监测时间   
		////system.out.println("2012-05-01 00:00:00.0".substring(0, 19));
		String dateExp = "yyyy-MM-dd HH:mm:ss";   
		SimpleDateFormat sdf = new SimpleDateFormat(dateExp);   
		//cron 
		String exp = "0 0/30 10 ? 3,4,6 *";   
		CronExpression cronExp = new CronExpression(exp);   
	 
		//表示当前时间的日历对象   
		Calendar calendar = Calendar.getInstance();   
		calendar.set(2012, 3, 12,0,0,0); 
		//表示结束时间的日历对象，也即两分钟后   
		Calendar endCalendar = Calendar.getInstance();   
		endCalendar.set(2012, 3, 16 , 23,59,59); 
 
 
		////system.out.println("开始时间:"+sdf.format(calendar.getTime())+"结束时间:"+sdf.format(endCalendar.getTime()));   
		  
		////system.out.println("列表开始");   
		while(true){   
		//获取距指定时间最近的一次触发时间   
		Date triggerDate = cronExp.getNextValidTimeAfter(calendar.getTime());   
		//如果永远不能再触发了，则退出   
		if(triggerDate==null){   
		break;   
		}   
		//如果触发时间超过结束时间，则退出   
		if(triggerDate.after(endCalendar.getTime())){   
		    break;   
		}   
		////system.out.println(sdf.format(triggerDate));   
		//让指定时间跳过下次触发时间，以计算出下下次得触发时间   
		calendar.setTime(triggerDate);   
		calendar.add(Calendar.SECOND, 1);   
		}   
		////system.out.println("列表结束");   
		}   
	}
 
