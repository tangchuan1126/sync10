package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorReturnProductSubItemsMgrZr;
import com.cwc.app.iface.zr.ReturnProductSubItemMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ReturnProductSubItemMgrZr implements ReturnProductSubItemMgrIfaceZr{
	
	private static Logger log = Logger.getLogger("ACTION");
	private FloorReturnProductSubItemsMgrZr floorReturnProductSubItemsMgrZr;
	
	@Override
	public long addReturnProductSubItem(DBRow row) throws Exception {
		try{
			long id  = floorReturnProductSubItemsMgrZr.addReturnProductSubItem(row);
			return id;
		}catch (Exception e) {
			throw new SystemException(e, "addReturnProductSubItem", log);
		}
	}



	@Override
	public DBRow[] getReturnProductSubItemsByRpiId(long rpi_id)
			throws Exception {
		try{
			return floorReturnProductSubItemsMgrZr.getReturnProductSubItem(rpi_id);
		}catch (Exception e) {
			throw new SystemException(e, "getReturnProductSubItemsByRpiId", log);
		}
	}



	@Override
	public void updateSubReturnProductItem(DBRow updateRow,long rpsi_id) throws Exception {
		try{
			floorReturnProductSubItemsMgrZr.updateSubReturnProductItem(updateRow, rpsi_id);
		}catch (Exception e) {
			throw new SystemException(e, "updateSubReturnProductItem", log);
		}
		
	}
	public void setFloorReturnProductSubItemsMgrZr(
			FloorReturnProductSubItemsMgrZr floorReturnProductSubItemsMgrZr) {
		this.floorReturnProductSubItemsMgrZr = floorReturnProductSubItemsMgrZr;
	}

}
