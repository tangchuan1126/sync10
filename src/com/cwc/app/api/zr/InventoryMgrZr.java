package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.iface.zr.InventoryMgrIfaceZr;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class InventoryMgrZr implements InventoryMgrIfaceZr {

	static Logger log = Logger.getLogger("ACTION");

	private FloorLocationMgrZJ floorLocationMgrZJ;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;  
	
	
	@Override
	public DBRow[] getOperatorArea(long ps_id) throws Exception {
		try{
			DBRow[] result = null ;
			DBRow[] areas = floorLocationMgrZJ.getLocationAreaByPsid(ps_id);
			if(areas != null){
				result = new DBRow[areas.length];
				for(int index = 0 , count = areas.length ; index < count ; index++ ){
					DBRow row = new DBRow();
					row.add("areaName", areas[index].getString("title")+areas[index].getString("area_name"));
					row.add("areaId", areas[index].getString("area_id"));
					row.add("psId", ps_id);
					long areaId = areas[index].get("area_id", 0l) ;
					int countAreaWaitingApprove = floorLocationMgrZJ.countAreaWaiting(areaId);
					if(countAreaWaitingApprove > 0 ){
						row.add("state", 2);
					}else{
						row.add("state", 1);
					}
					result[index] = row ;
				}
			} 
			return result ;
		}catch (Exception e) {
			throw new SystemException(e,"InventoryMgrZr.getOperatorArea",log);
		}
 	}
	@Override
	public DBRow getTitleByName(String title) throws Exception {
		try{
			return proprietaryMgrZyj.findProprietaryByTitleName(title) ;
		}catch (Exception e) {
			throw new SystemException(e,"InventoryMgrZr.getTitleByName",log);
		}	
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	

	

	
}
