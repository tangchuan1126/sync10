package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.api.zr.androidControl.AndroidPermission;
import com.cwc.app.floor.api.zr.FloorAndroidControlMgrZr;
import com.cwc.app.iface.zr.AndroidControlMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class AndroidControlMgrZr implements AndroidControlMgrIfaceZr {

	static Logger log = Logger.getLogger("ACTION");

	private FloorAndroidControlMgrZr floorAndroidControlMgrZr ;
	
	@Override
	public long addPersonalPermission(DBRow insertPersonal) throws Exception {
		try{
			return floorAndroidControlMgrZr.addAdidPermission(insertPersonal);
		}catch(Exception e){
			throw new SystemException(e,"addPersonalPermission",log);
		}
	}

	@Override
	public void addAdgidPermission(DBRow[] insertAdgid) throws Exception {
		try{
			if(insertAdgid != null){
				for(DBRow row : insertAdgid){
					floorAndroidControlMgrZr.addRolePermission(row);
				}
			}
		}catch(Exception e){
			throw new SystemException(e,"addAdgidPermission",log);
		}
	}

	@Override
	public AndroidPermission[] convertDBRowToAndroidPermission(DBRow[] arrays) throws Exception {
		if(arrays != null && arrays.length > 0){
			AndroidPermission[] returnArray = new AndroidPermission[arrays.length];
			for(int index =  0 , count = arrays.length ; index < count ; index++){
				DBRow tempRow = arrays[index] ;
 				AndroidPermission temp = new AndroidPermission(tempRow.get("id", 0l), 
 						tempRow.getString("title"), 
 						tempRow.getString("title"),
 						tempRow.get("parent_id", 0l),
 						tempRow.get("id", 0));
 				returnArray[index] = temp ;
			}
			return returnArray ;
		}
 		return null;
	}
	@Override
	public AndroidPermission[] fixAndroidPermissionTree(AndroidPermission[] arrays) throws Exception {
 		return null;
	}
	@Override
	public DBRow[] getAllPermission() throws Exception {
		try{
			 return floorAndroidControlMgrZr.getAllPermission();
		}catch(Exception e){
			throw new SystemException(e,"getAllPermission",log);
		}
	}

	public void setFloorAndroidControlMgrZr(FloorAndroidControlMgrZr floorAndroidControlMgrZr) {
		this.floorAndroidControlMgrZr = floorAndroidControlMgrZr;
	}
	
	
	
}
