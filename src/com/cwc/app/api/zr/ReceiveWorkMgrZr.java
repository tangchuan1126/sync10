
package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.SumsangPalletNoNotFoundException;
import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.floor.api.zr.FloorReceiveMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zyj.FloorCheckInMgrZyj;
import com.cwc.app.floor.api.zyj.FloorOrderSystemMgr;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.ReceiveWorkMgrIfaceZr;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.ContainerImportStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.CheckInEntryPermissionUtil;
import com.cwc.util.StringUtil;

public class ReceiveWorkMgrZr implements ReceiveWorkMgrIfaceZr {

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorCheckInMgrZwb floorCheckInMgrZwb ;
	
	
	private CheckInMgrIFaceZyj checkInMgrZyj ;
	
	
	private CheckInMgrIfaceZwb checkInMgrZwb ;
	
	
	private FloorFolderInfoMgrWp floorFolderInfoMgrWp ;
	
	private FloorCheckInMgrZyj floorCheckInMgrZyj ;
	
	private CheckInMgrIfaceZr checkInMgrZr ;
	
 	private FloorReceiveMgrZr floorReceiveMgrZr ;
	
	private YMSMgrAPI ymsMgrApi ;
	
    private FloorOrderSystemMgr floorOrderSystemMgr ;

	
	@Override
	public long addReceiveInfo(DBRow receiveRow) throws Exception {
 		try{
 			return 0l ;
 		}catch(Exception e){
  			throw new SystemException(e,"addReceiveInfo",log);
 		}
	}
	@Override
	public void addReceiveInfo(long ic_id , long dlo_detail_id , long entry_id, DBRow[] receiveRows, DBRow loginRow ,String filePath) throws Exception {
 		try{
 			//添加图片的处理
 			long adid = loginRow.get("adid", 0l);
// 			checkInMgrZyj.batchAddContainerPlatesReceived(dlo_detail_id, ic_id, receiveRows);
 			checkInMgrZyj.batchAddContainerPlatesReceived(ModuleKey.CHECK_IN_ENTRY_DETAIL,dlo_detail_id, ic_id, receiveRows, adid, DateUtil.NowStr(), loginRow.get("ps_id", 0L));
 			checkInMgrZwb.addCheckInLog(adid, "Android Receive", entry_id, CheckInLogTypeKey.ANDROID_SUMSANG_RECEIVE,DateUtil.NowStr(),"");
 			//checkInMgrZwb.androidSaveMoveFile(filePath, adid, entry_id,"sumsang_receive_");
 			checkInMgrZwb.androidSaveMoveFile(filePath, entry_id,"sumsang_receive_", loginRow.getString("session_id"));
 			updateContainerInfoReceiveQty(ic_id); //添加收货的梳理
 			
 		}catch(Exception e){
  			throw new SystemException(e,"addReceiveInfo",log);
 		}
	}
	
	@Override
	public DBRow getSumSangCtnrDetails(long entry_id  , AdminLoginBean adminLoginBean ) throws NoPermiessionEntryIdException, CheckInNotFoundException ,DockCheckInFirstException, Exception{
 		try{
			DBRow entryRow  = floorCheckInMgrZwb.getEntryIdById(entry_id);
		
			CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
			String wareHoseCheckInTime = entryRow.getString("warehouse_check_in_time") ;
			if(StringUtil.isNull(wareHoseCheckInTime)){	//这里判断是否 在手持设备上做了DockCheckIn，有点奇怪依靠主单据去做的， zhangrui 09-24
				//throw new DockCheckInFirstException(); zhangrui 2014-10-13 有可能没有做dockCheckIN
			}
			int status = entryRow.get("status", 0);
			//if(status != CheckInMainDocumentsStatusTypeKey.LEFT ){	//测试的时候_zhangrui
				DBRow[] details = checkInMgrZwb.getEntryDetailHasResources(entry_id,0l);
				DBRow[] loadNumbers = fixSumSangCtnrDatas(details);
				DBRow append = new DBRow();
				append.add("entry_id", entry_id);
	 			append.add("details", loadNumbers);
	 			append.add("account", loadNumbers != null ? loadNumbers.length : 0);
	 			return append ;
	 		//}
		}catch(NoPermiessionEntryIdException e){
			throw e ;
		}catch (CheckInNotFoundException e) {
			throw e ;
		} 
	}
	@Override
	public DBRow[] getStatingByPsId(long ps_id) throws Exception {
		try{
			DBRow[] staging = floorFolderInfoMgrWp.getExcelStagingData(String.valueOf(ps_id));
 			return fixstaging(staging);
		}catch(Exception e){
  			throw new SystemException(e,"getStatingByPsId",log);
		}
	}
	private DBRow[] fixstaging(DBRow[] arrays){
		List<DBRow> returnList = new ArrayList<DBRow>();
		if(arrays != null && arrays.length > 0){
			for(DBRow area : arrays){
				DBRow insertRow = new DBRow();
				insertRow.add("location_id", area.get("id", 0l));
				insertRow.add("location_name", area.getString("location_name"));
				returnList.add(insertRow);
			}
		}
		return returnList.toArray(new DBRow[0]);
	}
	/**
  	 * [{
  	 *   door:
  	 *   load:[{
  	 *   	
  	 *   }]
  	 * }]
  	 * @param arrays
  	 * @return
  	 */
	private DBRow[] fixSumSangCtnrDatas(DBRow[] arrays){
		CheckInChildDocumentsStatusTypeKey typeKey = new CheckInChildDocumentsStatusTypeKey();
		List<DBRow> arrayList = new ArrayList<DBRow>();
		Map<String, List<DBRow>> maps = new HashMap<String, List<DBRow>>();
		if(arrays != null && arrays.length > 0 ){
			for(DBRow row : arrays){
				
				
				String number = row.getString("number");
				int numberType = row.get("number_type", 0);
				long ic_id = row.get("ic_id", 0l);	//三星的
				if( ic_id > 0l &&( numberType == ModuleKey.CHECK_IN_BOL || numberType == ModuleKey.CHECK_IN_CTN )){
				//if(numberType == ModuleKey.CHECK_IN_BOL || numberType == ModuleKey.CHECK_IN_CTN   ){
	
					DBRow insertRow = new DBRow();
					String doorName = row.getString("doorId");
					List<DBRow> innerLoadNumbers =	maps.get(doorName);
					
					innerLoadNumbers =  innerLoadNumbers == null ? new ArrayList<DBRow>() : innerLoadNumbers ; 
					insertRow.add("number", number);
					insertRow.add("status", typeKey.getContainerTypeKeyValue(row.get("number_status", -1)));
					insertRow.add("status_int", row.get("number_status", -1));
					
					//徐佳升级过后执行的东西
 					insertRow.add("customer_id", row.getString("customer_id"));
					insertRow.add("company_id", row.getString("company_id"));
					insertRow.add("number_type", row.get("number_type", 0));
					insertRow.add("account_id", row.getString("account_id"));
					insertRow.add("dlo_detail_id", row.get("dlo_detail_id", 0l));
					insertRow.add("ic_id", row.get("ic_id", 0l));

					innerLoadNumbers.add(insertRow);
					maps.put(doorName, innerLoadNumbers);
				}
			}
		}
		Iterator<Entry<String, List<DBRow>>> it =	maps.entrySet().iterator();
		while(it.hasNext()){
			 Entry<String, List<DBRow>> entry = it.next();
			 DBRow inner = new DBRow();
			 inner.add("ctnrs", entry.getValue().toArray(new DBRow[0]));
			 inner.add("door", entry.getKey());
			 arrayList.add(inner);
		}
		return arrayList.toArray(new DBRow[0]);
	}
	
	
	@Override
	public DBRow[] getReceivedSumSangDetails(long dlo_detail_id, long ic_id) throws Exception {
		try{
			return checkInMgrZyj.findPlatesByIcid(ic_id, null);
		}catch(Exception e){
  			throw new SystemException(e,"getCtnrDetails",log);
		}
	}
	
	@Override
	public void finishReceived(DBRow data ,DBRow loginRow) throws Exception {
		try{
			long dlo_detail_id = data.get("dlo_detail_id", 0l);
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow == null){
				throw new SystemException() ;
			}
			long ic_id = data.get("ic_id", 0l);
			long entry_id = data.get("entry_id", 0l);
			 long adid = loginRow.get("adid", 0l);
			 long equipment_id = detailRow.get("equipment_id", 0l);
 			 int resources_type = data.get("resources_type", 0);
			 long resources_id = data.get("resources_id", 0);
			//改变子单据的状态为Close
			 DBRow upDetailrow=new DBRow();
			 upDetailrow.add("number_status",CheckInChildDocumentsStatusTypeKey.CLOSE);   //更新子单据状态为处理中 ,close
			 upDetailrow.add("finish_time", DateUtil.NowStr()); //在装货完成的时候也应该写这个时间
			 checkInMgrZr.finishDetail(detailRow, resources_id, upDetailrow, adid,resources_type);
 			   
			//改变import_container_info 中的状态ds
			checkInMgrZyj.updateContainerInfoStatus(ContainerImportStatusKey.RECEIVED, ic_id);
 			checkInMgrZwb.addCheckInLog(adid, "Android Received", entry_id, CheckInLogTypeKey.ANDROID_SUMSANG_RECEIVED,DateUtil.NowStr(),"");
 			//checkInMgrZwb.androidSaveMoveFile(data.getString("filePath"), adid, entry_id, "dock_close_");
 			checkInMgrZwb.androidSaveMoveFile(data.getString("filePath"), entry_id, "dock_close_", data.getString("session_id"));
 			 ymsMgrApi.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id, adid);
 			}catch(Exception e){
  			throw new SystemException(e,"getCtnrDetails",log);
		}
	}
	
	@Override
	public DBRow getPalletInfo(String pallet_no) throws SumsangPalletNoNotFoundException  , Exception {
			DBRow row = floorCheckInMgrZyj.getPlateInfoBy(pallet_no);
			if(row == null){
				throw new SumsangPalletNoNotFoundException();
			}
			DBRow returnRow = new DBRow();
			returnRow.add("pallet_number", row.getString("plate_no"));
			returnRow.add("area", row.getString("location_name"));
			returnRow.add("box_number", row.get("box_number",0));
			returnRow.add("weight", row.get("weight",0.0d));
			returnRow.add("icp_id", row.get("icp_id", 0l));
			return returnRow ;
 	}
	@Override
	public DBRow getContainerInfo(long ic_id) throws Exception {
 		return floorCheckInMgrZyj.getContainerInfoBy(ic_id);
	}
	@Override
	public void updatePallet(DBRow[] rows) throws Exception {
		for(DBRow row : rows){
			long icp_id =  row.get("icp_id", 0l);
			row.remove("icp_id");
			floorCheckInMgrZyj.updateContainerPlateByIcpId(icp_id, row);
			checkInMgrZyj.updatePlateShippedInfoByAssociate(ModuleKey.CHECK_IN_SAMSUNG_PLATE, icp_id, 0L
					, "", 0, 0L, row.get("staging_area", 0L));
		}
 	}
	@Override
	public void updateContainerInfoReceiveQty(long ic_id) throws Exception {
 		int sum = floorCheckInMgrZyj.getSumOfContainReceiveTotalQty(ic_id);
 		DBRow updateRow = new DBRow();
 		updateRow.add("recevie_qty",sum);
 		floorCheckInMgrZyj.updateContainerInfoByIcid(ic_id, updateRow);
	}
	
	@Override
	public void receivedException(DBRow data ,long adid) throws Exception {
 		try{
 			
 			long ic_id = data.get("ic_id", 0l);
 			long dlo_detail_id = data.get("dlo_detail_id", 0l); 
 			String reason = data.getString("reason");
 			long entry_id = data.get("entry_id",0l);
 			String filePath = data.getString("filePath");
 			
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
 			if(detailRow == null){
 				throw new SystemException();
 			}
  			DBRow upDetailrow=new DBRow();
			upDetailrow.add("number_status",CheckInChildDocumentsStatusTypeKey.EXCEPTION);   //更新子单据状态为处理中 ,Exception
			upDetailrow.add("finish_time", DateUtil.NowStr()); //在装货完成的时候也应该写这个时间
			upDetailrow.add("note", reason);
			
			 long equipment_id = detailRow.get("equipment_id", 0l);
 			 int resources_type = data.get("resources_type", 0);
			 long resources_id = data.get("resources_id", 0);
			 
			checkInMgrZr.finishDetail(detailRow, resources_id, upDetailrow, adid,resources_type);
 			 
			//改变import_container_info 中的状态
 			DBRow updateContainerInfo = new DBRow();
			updateContainerInfo.add("status", ContainerImportStatusKey.RECEIVED);
			floorCheckInMgrZyj.updateContainerInfoByIcid(ic_id, updateContainerInfo);
  			
			//日志
			checkInMgrZwb.addCheckInLog(adid, "Android Received", entry_id, CheckInLogTypeKey.ANDROID_SUMSANG_RECEIVED,DateUtil.NowStr(),"Exception : " +reason );
 			//保存文件
			//checkInMgrZwb.androidSaveMoveFile(filePath, adid, entry_id, "dock_close_");
			checkInMgrZwb.androidSaveMoveFile(filePath, entry_id, "dock_close_", data.getString("session_id"));
 			ymsMgrApi.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id, adid);
 		}catch(Exception e){
  			throw new SystemException(e,"receivedException",log);
 		}
	}
	
	
	
	 
	//三星装货 异常
	@Override
	public void sumsangLoadException(DBRow data, DBRow loginRow) throws Exception {
		try{
			long ic_id = data.get("ic_id", 0l);
 			long dlo_detail_id = data.get("dlo_detail_id", 0l); 
 			String reason = data.getString("reason");
 			long entry_id = data.get("entry_id",0l);
 			String filePath = data.getString("filePath");
 			long adid =  loginRow.get("adid", 0l) ;
 			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
 			if(detailRow == null){
 				throw new SystemException();
 			}
 			DBRow upDetailrow=new DBRow();
			upDetailrow.add("number_status",CheckInChildDocumentsStatusTypeKey.EXCEPTION);   //更新子单据状态为处理中 ,Exception
			upDetailrow.add("finish_time", DateUtil.NowStr()); //在装货完成的时候也应该写这个时间
			upDetailrow.add("note", reason);

			floorCheckInMgrZwb.updateDetail(dlo_detail_id, upDetailrow);
 			//日志
 			checkInMgrZwb.addCheckInLog(adid, "Android Load Exception", entry_id, CheckInLogTypeKey.ANDROID_DOCK_CLOSE,DateUtil.NowStr(),"Exception : " + reason);
			//container info 状态
 			checkInMgrZyj.updateContainerInfoStatus(ContainerImportStatusKey.SHIPPED, ic_id);
 		
			//释放门
 			DBRow releaseDoor = new DBRow();
 			releaseDoor.add("occupancy_status",OccupyStatusTypeKey.RELEASED); 
 			floorCheckInMgrZwb.releasedDoor(entry_id, detailRow.get("rl_id", 0l), releaseDoor);	//现在是 直接释放门了。
			
 			//文件
 			//checkInMgrZwb.androidSaveMoveFile(filePath,adid, entry_id, "dock_close_");
 			checkInMgrZwb.androidSaveMoveFile(filePath, entry_id, "dock_close_", data.getString("session_id"));
		}catch(Exception e){
  			throw new SystemException(e,"sumsangLoadException",log);
		}
	}
	/**
	 * 三星提交装货的接口,
	 * 主要是处理图片，记录日志，改变Checkind状态,container_import 状态
	 */
	@Override
	public void sumsangLoadFinish(DBRow data , DBRow loginRow) throws Exception {
		try{
			long adid = loginRow.get("adid", 0l);
			long entry_id = data.get("entry_id", 0l);
			long dlo_detail_id = data.get("dlo_detail_id", 0l);
			long ic_id = data.get("ic_id", 0l);
			int close_type = data.get("close_type", 0);
			String reason = data.getString("reason");
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
  			if(detailRow == null){
  				throw new SystemException();
  			}
  			if(entry_id <= 0l){
  				entry_id = detailRow.get("dlo_id", 0l);
  			}
			String filePath = data.getString("filePath");
			long rl_id = detailRow.get("rl_id", 0l);
			 long equipment_id = detailRow.get("equipment_id", 0l);
 			 int resources_type = data.get("resources_type", 0);
			 long resources_id = data.get("resources_id", 0);
			 
			DBRow upDetailrow = new DBRow();
			upDetailrow.add("number_status",close_type);   //更新子单据状态为处理中 ,Exception
			upDetailrow.add("finish_time", DateUtil.NowStr()); //在装货完成的时候也应该写这个时间
			upDetailrow.add("note", reason);
			checkInMgrZr.finishDetail(detailRow, resources_id, upDetailrow, adid,resources_type);
			
			//释放门,现在不清楚三星装货完成过后后面还有那些操作。就直接释放掉门,close 单据 ，
			ymsMgrApi.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, detailRow.get("equipment_id", 0l), adid);
 			//日志
 			if(close_type == CheckInChildDocumentsStatusTypeKey.EXCEPTION ){
 				checkInMgrZwb.addCheckInLog(adid, "Android Load Finish", entry_id, CheckInLogTypeKey.ANDROID_DOCK_CLOSE,DateUtil.NowStr(),"Exception:" + reason);
 			}else{
 				checkInMgrZwb.addCheckInLog(adid, "Android Load Finish", entry_id, CheckInLogTypeKey.ANDROID_DOCK_CLOSE,DateUtil.NowStr(),"");
 			}
 				//containerInfo
  			checkInMgrZyj.updateContainerInfoStatus(ContainerImportStatusKey.SHIPPED, ic_id);
 			//文件
  			//checkInMgrZwb.androidSaveMoveFile(filePath, adid, entry_id, "dock_close_");
  			checkInMgrZwb.androidSaveMoveFile(filePath, entry_id, "dock_close_", data.getString("session_id"));

			
		}catch(Exception e){
  			throw new SystemException(e,"loadSubSangSubmit",log);
		}
	}
	//next 提交的接口
	@Override
	public void SumsangLoadPhoto(DBRow data, DBRow loginRow) throws Exception {
		try{
			long entry_id = data.get("entry_id", 0l);
			long adid = loginRow.get("adid", 0l);
			String filePath = data.getString("filePath");
  			//checkInMgrZwb.androidSaveMoveFile(filePath, adid, entry_id, "Load_");
  			checkInMgrZwb.androidSaveMoveFile(filePath, entry_id, "Load_", data.getString("session_id"));
		}catch(Exception e){
  			throw new SystemException(e,"loadSubSangSubmit",log);
		}
	}
	@Override
	public void updatePallet(long icp_id, DBRow updateRow) throws Exception {
		try{
			floorCheckInMgrZyj.updateContainerPlateByIcpId(icp_id, updateRow);
		}catch(Exception e){
  			throw new SystemException(e,"updatePallet",log);
		}
	}
	@Override
	public void reloadSumsang(long ic_id , long dlo_detail_id) throws Exception {
		try{
			checkInMgrZyj.updateBatchContainerPlatesShipByIcid(ic_id, 0l); //
			checkInMgrZyj.clearPlateLevelProductInventaryShipped(ModuleKey.CHECK_IN_ENTRY_DETAIL, dlo_detail_id);
		}catch(Exception e){
  			throw new SystemException(e,"updatePallet",log);
		}
	}
	@Override
	public long addCommonReceived(DBRow data) throws Exception {
		try{
			data.add("scan_time", DateUtil.NowStr());
			return floorReceiveMgrZr.addCommonReceivePallet(data);
		}catch(Exception e){
  			throw new SystemException(e,"addCommonReceived",log);
		}
 	}
	@Override
	public DBRow[] getCommonReceived(long lr_id) throws Exception {
		try{
			DBRow[] datas = floorReceiveMgrZr.getCommonReceivePallet(lr_id);
			fixReceiveDatas(datas);
			return datas;
		}catch(Exception e){
  			throw new SystemException(e,"getCommonReceived",log);
		}
	}
	private void fixReceiveDatas(DBRow[] datas) {
		if(datas != null && datas.length > 0){
			for(DBRow temp : datas){
				temp.remove("order_number") ;
				temp.remove("order_type") ;
				temp.remove("order_system") ;
				temp.remove("dlo_detail_id") ;
				temp.remove("order_live") ;
				temp.remove("delivery_or_pick_up") ;
				temp.remove("lr_id") ;
				temp.remove("scan_time");
				
			}
		}
	}
	
	
	@Override
	public DBRow getOrderSystemInfo(long lr_id) throws Exception {
		try{
			DBRow orderSystem = floorOrderSystemMgr.findOrderSystemByNumberTypeInfos(lr_id);
			DBRow returnRow = new DBRow();
			returnRow.add("system_type",orderSystem.get("system_type", 0));
			returnRow.add("order_type", orderSystem.get("ORDER_TYPE", 0));
			returnRow.add("order_level", orderSystem.get("DOC_LEVEL", 0));
			return returnRow ;
		}catch(Exception e){
  			throw new SystemException(e,"getOrderSystemInfo",log);
		}
	}
	
	
	@Override
	public int commonReceiveDelete(long receive_pallet_id) throws Exception {
		try{
			return	floorReceiveMgrZr.deleteCommonReceivePallet(receive_pallet_id);
		}catch(Exception e){
  			throw new SystemException(e,"commonReceiveDelete",log);
		}
 	}
 
	@Override
	public int updateCommonReceive(long  commonReceiveId , DBRow updateRow) throws Exception {
		try{
			return floorReceiveMgrZr.updateCommonReceivePalletType(commonReceiveId, updateRow);
		}catch(Exception e){
  			throw new SystemException(e,"updateCommonReceivePalletType",log);
		}
	}
	
	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}
	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public void setFloorCheckInMgrZyj(FloorCheckInMgrZyj floorCheckInMgrZyj) {
		this.floorCheckInMgrZyj = floorCheckInMgrZyj;
	}
	public void setYmsMgrApi(YMSMgrAPI ymsMgrApi) {
		this.ymsMgrApi = ymsMgrApi;
	}
	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}
	public void setFloorReceiveMgrZr(FloorReceiveMgrZr floorReceiveMgrZr) {
		this.floorReceiveMgrZr = floorReceiveMgrZr;
	}
	public void setFloorOrderSystemMgr(FloorOrderSystemMgr floorOrderSystemMgr) {
		this.floorOrderSystemMgr = floorOrderSystemMgr;
	}
	
	
}
