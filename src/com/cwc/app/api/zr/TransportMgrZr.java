package com.cwc.app.api.zr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zwb.PreparePurchaseMgrZwb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zwb.PreparePurchaseMgrIfaceZwb;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class TransportMgrZr implements TransportMgrIfaceZr{

	static Logger log = Logger.getLogger("ACTION"); 
	
	private FloorTransportMgrZr floorTransportMgrZr;
	
	private FloorTransportMgrLL floorTransportMgrLL;
	
	private AdminMgrIFace adminMgr;
	
	private ScheduleMgrIfaceZR scheduleMgrZr;
	
	private PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb;
	
	@Override
	public DBRow getSupplierWarehouse(long supplier_id) throws Exception {
		try{
			return floorTransportMgrZr.getSupplierWarehouse(supplier_id);
		}catch (Exception e) {
			throw new SystemException(e, "getSupplierWarehouse", log);
		}
	}

	@Override
	public DBRow getSendPsByFromPsTypeAndSendPsid(long from_ps_type,
			long send_psid) throws Exception {
		try{
			if(Integer.parseInt(from_ps_type+"") == ProductStorageTypeKey.SupplierWarehouse){
				return floorTransportMgrZr.getSupplierWarehouse(send_psid);
			}else{
				return floorTransportMgrZr.getSystemPs(send_psid);
			}
			
		}catch (Exception e) {
			throw new SystemException(e, "getSendPsByFromPsTypeAndSendPsid", log);
		}
	}
	
	// 添加Transport 日志
	@Override
	public void addTransportLog(HttpServletRequest request) throws Exception {
		try{
			 
			//张睿修改 在表transport_logs 中添加字段 activity_id 这里的stage 就是这个字段的key值.
			// transport_id ， transport_content(跟进内容),eta(具体内容的完成时间),transport_type 跟进类型主Key,stage
			//stage表示的第二级跟进的key的value
			//transport_type 操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
			long transport_id = StringUtil.getLong(request,"transport_id");
			String transport_content = StringUtil.getString(request,"transport_content");
			String eta = StringUtil.getString(request, "eta");
			int transport_type = StringUtil.getInt(request,"transport_type");
			 
			int stage = StringUtil.getInt(request,"stage");
			/*DBRow transportRow = floorTransportMgrLL.getTransportById(transport_id+"");
			TDate tDate = new TDate();
			TDate endDate = new TDate(transportRow.getString("transport_date")+" 00:00:00");
			
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd hh:mm:ss"), "dd");
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			if(transport_type== 6) {
  				row.add("clearance", stage);
				if(stage==ClearanceKey.FINISH) {
					row.add("clearance_over", diffDay);
				}
			}
			if(transport_type== 7) {
 				row.add("declaration", stage);
				if(stage==DeclarationKey.FINISH) {
					row.add("declaration_over", diffDay);
				}
			}*/
			/*if(transport_type == 8){
				//标签的完成时间是 从当前的时间 - 创建的时间
				TransportTagKey tagKey = new TransportTagKey();
				if(stage == TransportTagKey.FINISH){
					row.add("tag_over", diffDay);
				}
			}*/
			/*if(transport_type == 9){
				//跟进可改状态
				TransportCertificateKey certificateKey = new TransportCertificateKey();
				if(stage == certificateKey.FINISH){
					row.add("certificate_over", diffDay);
				}
			}*/
		/*	if(transport_type== 1) {
				TransportOrderKey transportOrderKey = new TransportOrderKey();
				
				 
				if(stage==transportOrderKey.AlREADYRECEIVE) {
					row.add("transport_status", stage);	
					row.add("deliveryed_date", tDate.getCurrentTime()); //确认收货
				}
			}*/
			/*
			 * 运费是不该主信息状态
			 * if(transport_type== 5) {
				// 运费完成花费时间 == 当前时间 - 
				TransportStockInSetKey stockInSetKey = new TransportStockInSetKey();
 				row.add("stock_in_set", stage);
				if(stage==stockInSetKey.SHIPPINGFEE_TRANSFER_FINISH) {
					row.add("stock_in_set_over", diffDay);
				}
			}*/
			 
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			insertLogs(transport_id, transport_content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),transport_type,eta,stage );
			//更新主表状态
			//floorTransportMgrLL.updateTransport(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addTransportLog", log);
		}
		
	}
	/**
	 * 加跟进日志
	 * @param transport_id
	 * @param transport_content
	 * @param adid
	 * @param employe_name
	 * @param transport_type
	 * @param timeComplete
	 * @param stage
	 * @return
	 * @throws Exception
	 */
	public long insertLogs(long transport_id ,String transport_content , long adid ,String employe_name, int transport_type , String timeComplete ,int stage) throws Exception{
		try{
			
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			row.add("transport_content", transport_content);
			row.add("transporter_id", adid);
			row.add("transporter", employe_name);
			row.add("transport_type", transport_type);
			if(timeComplete != null && timeComplete.trim().length()> 0){
				row.add("time_complete", timeComplete);
			}
	 
		 
			row.add("activity_id", stage);
			row.add("transport_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			
			long id = floorTransportMgrZr.addTransportLog(row);
			
			
			
			// 同时更新 主信息上的 updatedate,updateby(id),updatename。
			DBRow transportRow = new DBRow();
			transportRow.add("updatedate", currentTime);
			transportRow.add("updateby", adid);
			transportRow.add("updatename", employe_name);
			floorTransportMgrZr.updateTransportByIdAndDBRow(transport_id, transportRow);
			return id;
			
		}catch (Exception e) {
			throw new SystemException(e, "insertLogs", log);
		}
	}
	
	/**
	 * 保存转运单的日志
	 */
	@Override
	public long insertTransportLogs(long transport_id ,String transport_content , long adid ,String employe_name, int transport_type , String eta ,int stage) throws Exception
	{
		try
		{
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			row.add("transport_content", transport_content);
			row.add("transporter_id", adid);
			row.add("transporter", employe_name);
			row.add("transport_type", transport_type);
			if(eta != null && eta.trim().length()> 0){
				row.add("time_complete", eta);
			}
			row.add("activity_id", stage);
			row.add("transport_date", currentTime);
			
			return floorTransportMgrZr.addTransportLog(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e, "insertTransportLogs", log);
		}
	}
	
	/**
	 * 上传上来的文件命名:T_certificate_(文件名称)_index
	 * 多个文件上传
	 * 文件移动到另外的位置
	 * 
	 */
	@Override
	public void addTransportCertificateAddFile(HttpServletRequest request) throws Exception {
		try{
	 
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long file_with_id = StringUtil.getLong(request,"file_with_id",0l);
			int file_with_type = StringUtil.getInt(request,"file_with_type",0);
			int file_with_class =  StringUtil.getInt(request,"file_with_class",0);
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
 			String file_names = StringUtil.getString(request, "file_names");
 			String[] fileNameArray = null ;
 			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
 			StringBuffer logContext = new StringBuffer();
 			if(file_names.trim().length() > 0 ){
 				fileNameArray = file_names.trim().split(",");
 			}
 			if(fileNameArray != null && fileNameArray.length > 0){
 				for(String tempFileName : fileNameArray){
 					//真是的文件名称
 					StringBuffer realyFileName = new StringBuffer(sn);
 					String suffix = this.getSuffix(tempFileName);
 					realyFileName.append("_").append(file_with_id).append("_").append(tempFileName.replace("."+suffix, ""));
 					int indexFile = floorTransportMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
 					if(indexFile != 0){
 						realyFileName.append("_").append(indexFile);
 					}
 					
 					realyFileName.append(".").append(suffix);
 					logContext.append(",").append(realyFileName.toString());
 					// 插入file 表中
 					this.addTransportFile(realyFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
 					// 移动文件
 					FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
 				}
 		 
 			}
 			//添加日志记录一条
 			String context = "" ;
 			if(logContext.length() > 1 ){
 				context = logContext.substring(1);
 			}
			this.insertLogs(file_with_id, getTransportCertificateFromConfigJsp(file_with_class)+"上传文件"+context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),9, null, file_with_class);
 
		}catch (Exception e) {
			throw new SystemException(e, "addTransportCertificateAddFile", log);
		}
		
	}
	public String getTransportCertificateFromConfigJsp(int fileWithClass) throws Exception {
		try{
			SystemConfigIFace  systemConfig = (SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig");
			String value = systemConfig.getStringConfigValue("transport_certificate");
			String[] arraySelected = value.split("\n");
			//将arraySelected组成一个List
			ArrayList<String> selectedList= new ArrayList<String>();
			for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
					String tempStr = arraySelected[index];
					if(tempStr.indexOf("=") != -1){
						String[] tempArray = tempStr.split("=");
						String tempHtml = tempArray[1];
						selectedList.add(tempHtml);
					}
			}
			
			
			return selectedList.get(fileWithClass-1);
		}catch (Exception e) {
			throw new SystemException(e, "getTransportCertificateFromConfigJsp", log);
		}
	}
	/**
	 * 删除一个文件
	 */
	@Override
	public void commonFileDelete(HttpServletRequest request) throws Exception {
		try{
			String tableName = StringUtil.getString(request, "table_name");
			long fileId = StringUtil.getLong(request,"file_id");
			String folder = StringUtil.getString(request,"folder"); //一般不会删除具体的文件
			String pk = StringUtil.getString(request, "pk");
			floorTransportMgrZr.deleteFileBy(fileId, tableName, pk); 
		}catch (Exception e) {
			throw new SystemException(e, "commonFileDelete", log);
		}
	}
	/**
	 * 返回要文件的真实路径
	 * 查询出来真实的文件的名称。然后在那个具体的路径下
	 */
	@Override
	public String getRealPathByFileId(HttpServletRequest request) throws Exception {
		try{
			
			 return null ;
		}catch (Exception e) {
			throw new SystemException(e, "getRealPathByFileId", log);
		}
	}
	 
	@Override
	public void handleTransportClearance(HttpServletRequest request) throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//如果是完成那么是要记录文件路径,记录日志.更新主表的记录。
			// 移动文件的位置。如果是有待上传的文件
			// 文件的命名规范为 T_clearance_阶段状态_232323_index.jpg;
			// 清关文件支持多个
			 
			long file_with_id = StringUtil.getLong(request, "transport_id");
			int  file_with_class = StringUtil.getInt(request, "clearance");
			
			int file_with_type = StringUtil.getInt(request,"file_with_type");
		  
 			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
 			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = ( fileNames.trim().length() > 0 ? true:false ) && (file_with_class == ClearanceKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer();
			if(fileNames.trim().length() > 0 ){
				String[] fileNameArray =  fileNames.split(",");
				if(fileNameArray != null && fileNameArray.length > 0){
					for(String tempFileName : fileNameArray){
						StringBuffer context = new StringBuffer();
						context.append(StringUtil.getString(request,"context"));
						// 获取真实的名字,把文件该名称
						// 清关文件支持多个,文件名称T_clearance_transportId_index
						
						//1.移动文件
						String suffix = getSuffix(tempFileName);
						String  tempUrl =  baseTempUrl+tempFileName;
						
						StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
						.append("_").append(tempFileName.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
						if(indexFileName != 0){
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						fileNameSb.append(",").append(realFileName);
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
					
						FileUtil.moveFile(tempUrl,url.toString());
						
						// file表上添加一条记录
						this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
					}
					
				}
			}
			//添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				// 如果不是上传文件。那么是要有eta的
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),6, eta, file_with_class);

			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),6, null, file_with_class);
			}

			//更新主transport上的信息表示当前清关已经完成
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("clearance", file_with_class);
			if(file_with_class == ClearanceKey.FINISH){
				DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
				updateTransportRow.add("clearance_over", this.getCostTimeOfTransportProcess(file_with_id,6,ClearanceKey.CLEARANCEING,transportRow.getString("transport_date")));
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);
			 
					
			//任务的跟进
			boolean isCompelte = (file_with_class == ClearanceKey.FINISH);
			String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, 6, scheduleContext, isCompelte, request, "transport_clearance_period");
			 
		}catch (Exception e) {
			throw new SystemException(e, "handleTransportClearance", log);
		}
		
	}
	/**
	 * 得到流程所花费的时间
	 * 查询日志中的记录的第一条日志作为开始的点。
	 * 如果没有那么就是用创建时间的作为开始的点
	 * @param transport_id
	 * @return
	 */
	private double getCostTimeOfTransportProcess(long transport_id , int transport_type , int activity_id , String transportCreateTime) throws Exception {
		try{
			DBRow firstLogClearance = floorTransportMgrZr.getFirstTransportIngLogTime(transport_id, transport_type, activity_id);
			String firstTime = "";
			if(firstLogClearance != null){
				firstTime = firstLogClearance.getString("transport_date");
			}
			if(firstTime.trim().length() < 1){
				firstTime = transportCreateTime;
			}
			TDate tDate = new TDate();
			TDate endDate = new TDate(firstTime);
			// 流程的完成时所需时间
		     double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");;
			return diffDay;
		}catch (Exception e) {
			throw new SystemException(e, "getTransportClearance", log);
		}
	}
 
	 
	@Override
	public void handleTransportDeclaration(HttpServletRequest request)
			throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_declaration_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "transport_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "declaration");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class == DeclarationKey.FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
						}
	 
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),7, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),7, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("declaration", file_with_class);
			if(file_with_class == DeclarationKey.FINISH){
				DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
			 	double diffDay = this.getCostTimeOfTransportProcess(file_with_id,7,DeclarationKey.DELARATING,transportRow.getString("transport_date"));
				updateTransportRow.add("declaration_over", diffDay);
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);

			boolean isCompelte = (file_with_class == DeclarationKey.FINISH);
			String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, 7,scheduleContext, isCompelte, request, "transport_declaration_period");
 
		}catch (Exception e) {
			throw new SystemException(e, "handleTransportDeclaration", log);
		}
	}
	 
	/**
	 * 多个商品。多个文件
	 */
	@Override
	public void handleTransportProductPictureUp(HttpServletRequest request)
			throws Exception {
		try{
			//如果是多个商品的 那么注意多个商品都关联上.就是循环的添加
			//有多个文件的时候循环的插入到product_file表.
			//文件的命名为T_product_上传文件的名称.jpg
			//如果是上传文件名有重复的那么就是要加上_index.jpg;
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			String pc_id = StringUtil.getString(request,"pc_id");	 //商品的Id
			String[] arrayPcId = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request, "transport_id");	//关联的业务Id
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签 比如 是商品本身，称重ID等等。
			String file_with_className = StringUtil.getString(request,"file_with_class_name"); //是商品本身，称重等等
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
		    String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null ;
			if(fileNames.trim().length() > 0 ){
				fileNameArray = fileNames.trim().split(",");
			}
			
			if(fileNameArray != null && fileNameArray.length > 0){
				StringBuffer logFileNameContent = new StringBuffer();
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				for(String tempFileName : fileNameArray){
					String tempSuffix = getSuffix(tempFileName);
					String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
					
					int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
					if(indexFile != 0){realyFileName.append("_").append(indexFile);}
					realyFileName.append(".").append(tempSuffix);
					logFileNameContent.append(",").append(realyFileName.toString());
					
					String  tempUrl =  baseTempUrl+tempFileName;
					//移动文件到指定的
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
				
					FileUtil.moveFile(tempUrl,url.toString());
					for(int index = 0 , count = arrayPcId.length ; index < count ; index++){
						DBRow file = new DBRow();
						file.add("file_name",realyFileName.toString());
						file.add("file_with_id",file_with_id);
						file.add("file_with_type",file_with_type);
						file.add("product_file_type",file_with_class);
						file.add("upload_adid",adminLoggerBean.getAdid());
						file.add("upload_time",DateUtil.NowStr());
						file.add("pc_id", arrayPcId[index]);
						floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
					}
				}
			}
		}catch (Exception e) {
			throw new SystemException(e, "handleTransportProductPictureUp", log);
		}
	}
	@Override
	public DBRow[] getAllProductFileByPcId(String pc_id, long file_with_type, long file_with_id) throws Exception {
		try{
			return floorTransportMgrZr.getAllProductFileByPcId(pc_id, file_with_type, file_with_id);
		}catch (Exception e) {
			throw new SystemException(e, "getAllProductFileByPcId", log);
		}
	}

	@Override
	public void updateTransport(long id, DBRow row) throws Exception {
		try{
			row.remove("transport_id");
			floorTransportMgrZr.updateTransportByIdAndRow(id,row);
			//应该要记录日志文件
			
		}catch (Exception e) {
			throw new SystemException(e, "updateTransport", log);
		}
		
	}

	@Override
	public void updateTransportCertificateAndLogs(HttpServletRequest request)
			throws Exception {
		 try{
		 	long transportId = StringUtil.getLong(request, "transport_id");
			DBRow row = new DBRow();
			String transport_date = StringUtil.getString(request, "transport_date");
			TDate tDate = new TDate();
			TDate endDate = new TDate(transport_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			row.add("certificate_over", diffDay); // 单证减去开始时间
			row.add("certificate",TransportCertificateKey.FINISH);
			this.updateTransport(transportId, row);
			
			//记录日志
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
	  
			this.insertLogs(transportId, "[单证上传完成]", adminLoggerBean.getAdid(),  adminLoggerBean.getEmploye_name(), 9, null, TransportCertificateKey.FINISH);
			scheduleMgrZr.addScheduleReplayExternal(transportId, ModuleKey.TRANSPORT_ORDER, 9, "转运单:"+transportId+"[单证上传完成]", true, request, "transport_certificate_period");

		 }catch (Exception e) {
			 throw new SystemException(e, "updateTransportCertificateAndLogs", log);
		}
		
	}
	
	@Override
	public void updateTransportProductFileAndLogs(HttpServletRequest request)
			throws Exception {
		try{
			long transportId = StringUtil.getLong(request, "transport_id");
			DBRow row = new DBRow();
			String transport_date = StringUtil.getString(request, "transport_date");
			TDate tDate = new TDate();
			TDate endDate = new TDate(transport_date);
			
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			row.add("product_file_over", diffDay);
			row.add("product_file",TransportProductFileKey.FINISH);
			floorTransportMgrZr.updateTransportByIdAndDBRow(transportId, row);
			//记录日志
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
	  
			this.insertLogs(transportId, "[商品文件]采集完成", adminLoggerBean.getAdid(),  adminLoggerBean.getEmploye_name(), 10, null, TransportProductFileKey.FINISH);
			scheduleMgrZr.addScheduleReplayExternal(transportId, ModuleKey.TRANSPORT_ORDER, 10, "转运单"+transportId+"[商品文件]采集完成", true, request, "transport_product_file_period");

			
		 }catch (Exception e) {
			 throw new SystemException(e, "updateTransportProductFileAndLogs", log);
		}
		
	}
 
	@Override
	public DBRow getFileByFileWithIdANdFileTypeFileClass(String tableName , long file_with_id , long file_with_class , int file_with_type)
			throws Exception {
		 try{
			 	return floorTransportMgrZr.getFileByFileWithIdAndFileWithClass(  tableName ,   file_with_id ,   file_with_class ,   file_with_type);
			  
		 	}catch (Exception e) {
			 throw new SystemException(e, "getFileByFileWithIdANdFileTypeFileClass", log);
		 }
	}
	/**
	 * 获取仓库 ，获取角色
	 * getAllUserAndDept 重载方法
	 * 刘鹏远
	 * @param proJsId
	 * @param ps_id
	 * @param group admin_group用户类型分组
	 */
	public DBRow[] getAllUserAndDept(long proJsId ,long ps_id, long group) throws Exception{
		try{
			 //获取仓库 ，获取角色
 			if(group == 0){
				//group == 0 则调用之前的方法
				return getAllUserAndDept( proJsId , ps_id );
			}else{
				return floorTransportMgrZr.getAllUserAndDept(proJsId , ps_id,  group);
			}
		 }catch (Exception e) {
			 throw new SystemException(e, "fGetAllUseAndDpt", log);
		}
	}
	
	@Override
	public DBRow[] getAllUserAndDept( long proJsId ,long ps_id ) throws Exception {
		 try{
			 //获取仓库 ，获取角色
			 
			 return floorTransportMgrZr.getAllUserAndDept(proJsId,ps_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getAllUserAndDept", log);
		}
	}
	
	public void hanleTransportQualityInspection(HttpServletRequest request)
			throws Exception {
		 try{
			 
			 
			 AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			 
				//重新命名图片文件T_quality_inspection文件的名称_index.jpg
				//移动图片位置
				//多个文件上传的时候要循环的处理
				long file_with_id  = StringUtil.getLong(request, "transport_id");
				int file_with_type = StringUtil.getInt(request, "file_with_type");
				int file_with_class  = StringUtil.getInt(request, "quality_inspection");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				
				String fileNames = StringUtil.getString(request, "file_names");
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				boolean isFileUp = (fileNames.trim().length() > 0 ? true:false )   ;
				StringBuffer fileNameSb = new StringBuffer(""); 
				if(isFileUp){
					if(fileNames.trim().length() > 0 ){
						String[] fileNameArray =  fileNames.split(",");
						if(fileNameArray != null && fileNameArray.length > 0){
							for(String tempFileName : fileNameArray){
								StringBuffer context = new StringBuffer();
								context.append(StringUtil.getString(request,"context"));
								String suffix = getSuffix(tempFileName);
								String tempUrl =  baseTempUrl+tempFileName;
								
								StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
								.append("_").append(tempFileName.replace("."+suffix, ""));
								int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
								if(indexFileName != 0){
									realFileName.append("_").append(indexFileName);
								}
								realFileName.append(".").append(suffix);
								 
								fileNameSb.append(",").append(realFileName);
								StringBuffer url = new StringBuffer();
								url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
								FileUtil.moveFile(tempUrl,url.toString());
								// file表上添加一条记录
								this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
							
							}
		 
						}
					}
				}
				// 添加日志
				String context = StringUtil.getString(request, "context");
				if(!isFileUp){
					String eta = StringUtil.getString(request, "eta");
					this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),11, eta, file_with_class);
				}else{
					this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),11, null, file_with_class);
				}	
				
				DBRow updateTransportRow = new DBRow();
				updateTransportRow.add("quality_inspection", file_with_class);
				if( file_with_class == TransportQualityInspectionKey.FINISH ){
					//如果是报告完成的话那么就是整体完成
					DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
					//质检是用开始时间来计算的
				 	double diffDay = this.getDiffTimeByTransportCreateTime(transportRow.getString("transport_date"));

					updateTransportRow.add("quality_inspection_over", diffDay);
				}
				floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);
				

				boolean isCompelte = (file_with_class == TransportQualityInspectionKey.FINISH);
				String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
				scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, 11,scheduleContext, isCompelte, request, "transport_quality_inspection");
		 
		 }
		 catch (Exception e) 
		 {
			 throw new SystemException(e, "hanleTransportQualityInspection", log);
		 }
	}
	private double getDiffTimeByTransportCreateTime(String transport_date) throws Exception{
		try {
			TDate tDate = new TDate();
			TDate endDate;
	
			endDate = new TDate(transport_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			return diffDay;
		} catch (Exception e) {
			 throw new SystemException(e, "getDiffTimeByTransportCreateTime", log);
		}
		
	}
	private void addTransportLogs(long file_with_id , String transport_content , long transporter_id , String  transporter ,int  transport_type , int file_with_class , String transport_date) throws Exception {
		try{
			DBRow logRow = new DBRow();
			logRow.add("transport_id", file_with_id);
			logRow.add("transport_content", transport_content);
			logRow.add("transporter_id", transporter_id);
			logRow.add("transporter", transporter);
			logRow.add("transport_type", transport_type);  
			 
		 
			logRow.add("activity_id", file_with_class);
			logRow.add("transport_date", transport_date);
			
			
			floorTransportMgrZr.addTransportLog(logRow);
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	/**
	 * 文件不能为重复的。就是说文件如果有了，那么就会覆盖掉原来数据库中的数据
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	// 在File表中添加一个字段
	public void addTransportFileLogs(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception{
		try{
 
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addFileNotExits(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	/**
	 * 文件可以带多个的
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	public void addTransportFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception {
		try{
			 
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addTransportCertificateFile(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	//普通的质检跟进
	private String hanleTransportQualityInspectionByAjax(HttpServletRequest request) throws Exception {
		try{
			String backUrl = StringUtil.getString(request, "backurl");
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
	 
			int quality_inspection = StringUtil.getInt(request, "quality_inspection");
			long transport_id = StringUtil.getLong(request, "transport_id");
			String eta = StringUtil.getString(request, "eta");
		 
			this.insertLogs(transport_id,  StringUtil.getString(request, "context"), adminLoggerBean.getAdid(),
					adminLoggerBean.getEmploye_name(), 11, eta, quality_inspection);
			
			//更新主transport 信息
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("quality_inspection", quality_inspection);
			if(quality_inspection == TransportQualityInspectionKey.FINISH){
				DBRow transportRow = floorTransportMgrLL.getTransportById(transport_id+"");
			 	double diffDay = this.getCostTimeOfTransportProcess(transport_id,11,TransportQualityInspectionKey.NEED_QUALITY,transportRow.getString("transport_date"));

				updateTransportRow.add("quality_inspection_over", diffDay);
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(transport_id, updateTransportRow);
 
			return backUrl;
		}catch (Exception e) {
			 throw new SystemException(e, "hanleTransportQualityInspectionByAjax", log);
		}
	}
	
	
	public void hanleTransportStockInSet(HttpServletRequest request)
			throws Exception {
		try{
			
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_declaration_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "transport_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "stock_in_set"); 
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class ==  TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
						}
	 
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),5, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),5, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("stock_in_set", file_with_class);
			if(file_with_class == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
				//如果是运费转账完成的话
				DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
			 
			 	double diffDay = this.getCostTimeOfTransportProcess(file_with_id,5,TransportStockInSetKey.SHIPPINGFEE_SET,transportRow.getString("transport_date"));

				updateTransportRow.add("stock_in_set_over", diffDay);
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);

			boolean isCompelte = (file_with_class == TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH);
			String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, 5,scheduleContext, isCompelte, request, "transport_stock_in_set_period");
			
		 
	 
		}catch (Exception e) {
			 throw new SystemException(e, "hanleTransportStockInSet", log);
		}
	}
	
 
	@Override
	public DBRow[] getFileByFilwWidthIdAndFileType(String tableName,
			long file_with_id, int file_with_type) throws Exception {
		 try{
			 return floorTransportMgrZr.getFileByFilwWidthIdAndFileType(tableName, file_with_id, file_with_type);
		 }catch (Exception e) {
			 throw new SystemException(e, "getFileByFilwWidthIdAndFileClass", log);
		}
	}

	
	@Override
	public DBRow[] getProductFileByFileTypeAndWithId(long file_with_id,
			int file_with_type, int product_file_type, PageCtrl pc)
			throws Exception {
		 try{
			 return floorTransportMgrZr.getProductFileByWithIdAndWithTypeFileType(file_with_id, file_with_type, product_file_type, pc);
		 }catch (Exception e) {
			 throw new SystemException(e, "getFileByFilwWidthIdAndFileClass", log);
		}
	 
	}
	
	
	
	@Override
	public void handleTransportTag(HttpServletRequest request)
			throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_tag_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "transport_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "tag");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			if(TransportTagKey.FINISH_PURCHASE==file_with_class)
			{
				file_with_class = TransportTagKey.FINISH;
			}
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String fileNamesSelect = StringUtil.getString(request, "file_names_select");
			if(!"".equals(fileNames) && !"".equals(fileNamesSelect))
			{
				fileNames += "," + fileNamesSelect;
			}
			if(fileNames.startsWith(","))
			{
				fileNames = fileNames.substring(1,fileNames.length()-1);
			}
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class == TransportTagKey.FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer realFileName = new StringBuffer();
							if(tempFileName.endsWith("_select"))
							{
								realFileName.append(tempFileName.subSequence(0, tempFileName.length()-7));
							}
							else
							{
								StringBuffer context = new StringBuffer();
								context.append(StringUtil.getString(request,"context"));
								String suffix = getSuffix(tempFileName);
								String tempUrl =  baseTempUrl+tempFileName;
								
								realFileName = new StringBuffer(sn).append("_").append(file_with_id)
								.append("_").append(tempFileName.replace("."+suffix, ""));
								int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
								if(indexFileName != 0){
									realFileName.append("_").append(indexFileName);
								}
								realFileName.append(".").append(suffix);
								
								fileNameSb.append(",").append(realFileName);
								StringBuffer url = new StringBuffer();
								url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
								FileUtil.moveFile(tempUrl,url.toString());
							}
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
						}
	 
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),8, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),8, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("tag", file_with_class);
			if(file_with_class == TransportTagKey.FINISH){
				//如果是运费转账完成的话
				DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
			 
			 	double diffDay = this.getDiffTimeByTransportCreateTime(transportRow.getString("transport_date"));

				updateTransportRow.add("tag_over", diffDay);
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);

			boolean isCompelte = (file_with_class == TransportTagKey.FINISH);
			String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, 8,scheduleContext.toString(), isCompelte, request, "transport_tag_period");
		 
		}catch (Exception e) {
			 throw new SystemException(e, "hanleTransportStockInSet", log);
		}
	}
	
	
	 

	@Override
	public DBRow[] getTransportLogs(long transport_id, int number)
			throws Exception
	{
		 try
		 {
			return floorTransportMgrZr.getTransportLogs(transport_id, number); 
		 }catch (Exception e) {
			 throw new SystemException(e, "getTransportLogs", log);
		}
	}
	@Override
	public void transportCertificateFollowUp(HttpServletRequest request)
			throws Exception
	{
		try
		{
			long transport_id = StringUtil.getLong(request, "transport_id");
			String context = "转运单:"+transport_id+StringUtil.getString(request, "context") ;
			String eta = StringUtil.getString(request, "eta");
			int certificate = StringUtil.getInt(request, "certificate");
			boolean isover = false; 
			if(certificate == TransportCertificateKey.FINISH){
				eta = null ;
				DBRow row = new DBRow();
				String transport_date = StringUtil.getString(request, "transport_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(transport_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("certificate_over", diffDay); // 单证减去开始时间
				row.add("certificate",TransportCertificateKey.FINISH);
				this.updateTransport(transport_id, row);
				isover = true; 
			}
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(transport_id, context, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 9, eta, certificate);
			scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, 9, context, isover, request, "transport_certificate_period");

		}catch (Exception e)
		{
			 throw new SystemException(e, "transportCertificateFollowUp", log);
		}
		
	}
	@Override
	public void transportProductFileFollowUp(HttpServletRequest request)
			throws Exception
	{
		try
		{
			long transport_id = StringUtil.getLong(request, "transport_id");
			String context = "转运单:"+transport_id+StringUtil.getString(request, "context") ;
			String eta = StringUtil.getString(request, "eta");
			int productFile = StringUtil.getInt(request, "product_file");
			boolean isover = false; 
			if(productFile == TransportProductFileKey.FINISH){
				eta = null ;
				DBRow row = new DBRow();
				String transport_date = StringUtil.getString(request, "transport_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(transport_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("product_file_over", diffDay); // 商品图片减去开始时间
				row.add("product_file",TransportProductFileKey.FINISH);
				this.updateTransport(transport_id, row);
				isover = true; 
			}
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(transport_id, context, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),10, eta, productFile);
			scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER,10, context, isover, request, "transport_product_file_period");

		}catch (Exception e)
		{
			 throw new SystemException(e, "transportProductFileFollowUp", log);
		}
		
		
	}
	@Override
	public String getProcessSpendTime(long transport_id, int processKey,int activity_id)
			throws Exception
	{
		 try
		 {
			 DBRow row = floorTransportMgrZr.getFirstTransportIngLogTime(transport_id, processKey, activity_id);
			 if(row != null){
				 String transport_date = row.getString("transport_date");
				 return transport_date;
			 }
			 return "";
		 }catch (Exception e)
		 {
			 throw new SystemException(e, "getProcessSpendTime", log);
		}
	}
	@Override
	public DBRow[] getProductByPcIds(String pc_ids) throws Exception
	{
		try
		{
			return  floorTransportMgrZr.getProductByPcIds(pc_ids);
		}catch (Exception e) 
		{
			throw new SystemException(e, "getProductByPcIds", log);
		}
	 
	}
	@Override
	public void transportApplyMoney(HttpServletRequest request)
			throws Exception
	{
		try{
			preparePurchaseMgrZwb.transportApplyMoney(request);
		}catch (Exception e) {
			throw new SystemException(e, "transportApplyMoney", log);
		}
		
	}
	@Override
	public DBRow  getFileNumberByFileWithIdAndFileWithType(String table,long file_with_id,int file_with_type) throws Exception
	{
		try{
			
			return floorTransportMgrZr.getNumberFileByFileWithId(table,file_with_id, file_with_type);
		}catch (Exception e) {
			throw new SystemException(e, "transportApplyMoney", log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	@Override
	public void fixedScheduleAndPurchase() throws Exception
	{
		 DBRow[] rows = floorTransportMgrZr.getSchedule() ;
		 if(rows != null && rows.length > 0 ){
			 for(DBRow scheduleTemp : rows){
				 
				int associate_process = scheduleTemp.get("associate_process", 0);
				if(associate_process == 25){
			 
					long associate_id = scheduleTemp.get("associate_id", 0l);
			 
					
					DBRow row = floorTransportMgrZr.getApplyMoneny(associate_id, FinanceApplyTypeKey.PURCHASE_ORDER, 100009);
					if(row != null){
						DBRow scTemp = new DBRow();
 						scTemp.add("associate_id",row.get("apply_id", 0l));
						scTemp.add("associate_type", ModuleKey.APPLY_MONEY);
						long schedule_id = scheduleTemp.get("schedule_id", 0l);
				 
						floorTransportMgrZr.updateSchdule(scTemp, schedule_id);
					} 
				}
				if(associate_process == 26){
					String value = scheduleTemp.getString("schedule_overview");
					long associate_id = Long.parseLong(value.substring(value.length() - 6 ));
					 
					//采购单申请货款 100001 交货单货款
				 	DBRow row = floorTransportMgrZr.getApplyMoneny(associate_id, 6, 100001);
 					 if(row != null){
						DBRow scTemp = new DBRow();
						
						scTemp.add("associate_id",row.get("apply_id", 0l));
						scTemp.add("associate_type", ModuleKey.APPLY_MONEY);
						long schedule_id = scheduleTemp.get("schedule_id", 0l);
					 
						floorTransportMgrZr.updateSchdule(scTemp, schedule_id);	
 					}  
					
				}
				 
				 
			 }
		 }
			 
		 }
	@Override
	public Map<Integer, DBRow> getProductFileAndTagFile(long pc_id , long fileWithId,int file_with_type) throws Exception
	{
		Map<Integer,DBRow> map = new HashMap<Integer, DBRow>();
		DBRow[]  rows = floorTransportMgrZr.getFilesOfProduct(pc_id,  fileWithId,file_with_type);
		if(rows != null && rows.length > 0 ){
			for(int index = 0 , count = rows.length ; index < count ; index++ ){
				DBRow tempFileRow =  rows[index];
				int product_file_type = 0 ;
	 
				product_file_type = tempFileRow.get("product_file_type", 0);
				DBRow innerRow = map.get(product_file_type);
				if(innerRow == null){
					innerRow = new DBRow();
				}
				int countFile = innerRow.get("count", 0);
				countFile++;
				innerRow.add("count", countFile);
				map.put(product_file_type, innerRow);
			}
		}
		return map;
	}
	
	public void setPreparePurchaseMgrZwb(
			PreparePurchaseMgrIfaceZwb preparePurchaseMgrZwb)
	{
		this.preparePurchaseMgrZwb = preparePurchaseMgrZwb;
	}
	
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
 
	
}
