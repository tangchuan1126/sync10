package com.cwc.app.api.zr;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorStorageLocationAreaZr;
import com.cwc.app.iface.zr.CheckInPatrolMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.sun.star.uno.RuntimeException;

public class CheckInPatrolMgrZr implements CheckInPatrolMgrIfaceZr{

	static Logger log = Logger.getLogger("ACTION");
	
	private FloorStorageLocationAreaZr floorStorageLocationAreaZr ;
	
	private CheckInMgrIfaceZwb checkInMgrZwb ;
  
	
	@Override
	public DBRow[] getSpotArea(long ps_id) throws Exception {
 		try{
 			 DBRow[] row=floorStorageLocationAreaZr.getStorageLocationAreaByPsId(ps_id, 3);
 			 String patrolDone ="patrolDone";
 			 if(row.length>0){
 				for (int i = 0; i < row.length; i++) {
 					if(row[i].get("patrol_time", "").equals("") && row[i].get("area_id", 0)>0){
 						patrolDone="";
 					}
 					if(row[i].get("area_id", 0)==0){
 						row[i].add("patrol_time", patrolDone);
 					}
 				 }
 			 }
 			 
 			 return row;
 		}catch (Exception e) {
			throw new SystemException(e, "getSpotArea", log);
		}
	}
	
	@Override
	public DBRow[] getDockCheckDetailsInfo(long zone_id, long ps_id, int status) throws Exception {
	 	try{
			DBRow[] result =  checkInMgrZwb.rtOccupiedDoor(ps_id,zone_id,status,null); 
			DBRow[] fixResult = null ;
			if(result != null && result.length > 0 ){
				//查询子单据的状态和值
				fixResult =  new DBRow[result.length] ; 
				for(int index = 0 , count = result.length ; index < count ; index++ ){
				   DBRow fixRow = new DBRow();
				   DBRow row = result[index] ; 
				   fixRow.add("gate_liscense_plate", row.getString("gate_liscense_plate")) ;
				   fixRow.add("gate_driver_liscense", row.getString("gate_driver_liscense")) ;
				   fixRow.add("gate_container_no", row.getString("gate_container_no")) ;
				   fixRow.add("dlo_id", row.get("dlo_id",0l)) ;
				   fixRow.add("door_id", row.getString("sd_id")) ;		//SD_ID (doorId)
 				   fixRow.add("door_name", row.getString("doorId")) ;	//DOORID	(doorName)
 				   fixRow.add("area_id", row.getString("area_id")) ;
 				   fixRow.add("occupied_status", row.getString("occupied_status")) ;
 				   fixRow.add("trailer_status", row.get("status",0l)) ;
 				   fixRow.add("tractor_status", row.get("tractor_status",0l)) ;
 				   fixRow.add("rel_type", row.get("rel_type",0l)) ;
 				   // android 页面不显示 numbers的信息了
 				   //DBRow[] numbers = checkInMgrZwb.selectLotNumberByDoorId(row.get("rl_id",0),result[index].get("dlo_id",0));
				   //addDockDeitail(fixRow, numbers);
				   fixResult[index] = fixRow ;
				}
			}
			return fixResult ;
 		}catch (Exception e) {
			throw new SystemException(e, "getDockCheckDetailsInfo", log);
		}
 	}
	
	private void addDockDeitail(DBRow row  , DBRow[] details){
		if(details != null && details.length > 0 ){
			DBRow[] fixDetails = new DBRow[details.length] ;
			for(int index = 0 , count = details.length ; index < count ; index++ ){
				DBRow fixDetail = getFixDockDetail(details[index]);
 				fixDetails[index] = fixDetail ;
			}
			row.add("details", fixDetails);
		}
	}
	
	/**
	 * 因为数据库中
	 * load_number	，ctn_number ，bol_number ， others_number （是不同的字段 & 同时会显示出来不同的 TYPE）
	 * 
	 * @param row
	 * @return
	 */
	private static String[] numberNames = {"load_number","ctn_number","bol_number","others_number"} ;
	private static String[] numberTypes = {"LOAD","CTNR","BOL","OTHERS"};
	
	private DBRow getFixDockDetail(DBRow row){
		DBRow fixDetail = new DBRow();
		int index = getNumberIndexHasValue(row);
   		if(index != -1){
 			fixDetail.add("type", numberTypes[index]);
			fixDetail.add("numbers", row.getString(numberNames[index]));
		}
		return fixDetail ;
	}
	private int getNumberIndexHasValue(DBRow row){
		boolean flag  = false ;
		int returnIndex = 0 ;
 		for(int index = 0 , count = numberNames.length ; index < count ; index++ ){
			if(!StringUtil.isNull(row.getString(numberNames[index]))){
				returnIndex = index ;
				flag = true ;
				break ;
			}
		}
 		if(flag)
 			return returnIndex;
 		else
 			return -1 ;
 			//throw new RuntimeException("detail no numbers.");
 
		
	}
	
	
	
	public void setFloorStorageLocationAreaZr(FloorStorageLocationAreaZr floorStorageLocationAreaZr) {
		this.floorStorageLocationAreaZr = floorStorageLocationAreaZr;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	@Override
	public DBRow[] getDoorArea(long ps_id) throws Exception {
		try{
			DBRow[] row =floorStorageLocationAreaZr.getDoorAreaByPsId(ps_id, 2);
			String patrolDone ="patrolDone";
			 if(row.length>0){
				for (int i = 0; i < row.length; i++) {
					if(row[i].get("patrol_time", "").equals("") && row[i].get("area_id", 0)>0){
						patrolDone="";
					}
					if(row[i].get("area_id", 0)==0){
						row[i].add("patrol_time", patrolDone);
					}
				 }
			 }
 			return row;
 		}catch (Exception e) {
			throw new SystemException(e, "getSpotArea", log);
		}
	}
	
}
