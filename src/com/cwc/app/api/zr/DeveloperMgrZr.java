package com.cwc.app.api.zr;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorDeveloperMgrZr;
import com.cwc.app.iface.zr.DeveloperMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class DeveloperMgrZr implements DeveloperMgrIfaceZr {

	private static Logger log = Logger.getLogger("ACTION");
	private FloorDeveloperMgrZr floorDeveloperMgrZr;
	
	 
	public void setFloorDeveloperMgrZr(FloorDeveloperMgrZr floorDeveloperMgrZr) {
		this.floorDeveloperMgrZr = floorDeveloperMgrZr;
	}


	@Override
	public long addDeveloper(HttpServletRequest request) throws Exception {
		try{
			DBRow row = new DBRow();
			return floorDeveloperMgrZr.addDeveloper(row);
		}catch (Exception e) {
			throw new SystemException(e, "addDeveloper", log);
		}
	}


	@Override
	public DBRow[] getAllDeveloper(PageCtrl pc) throws Exception {
		try{
			return floorDeveloperMgrZr.getAllDeveloper(pc);
		}catch (Exception e) {
			throw new SystemException(e, "getAllDeveloper", log);
		}
	}

}
