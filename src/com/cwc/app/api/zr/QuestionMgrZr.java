package com.cwc.app.api.zr;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.JaroWinklerDistance;
import org.apache.lucene.search.spell.LevensteinDistance;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.Lookup.LookupResult;
import org.apache.lucene.search.suggest.tst.TSTLookup;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.floor.api.zr.FloorQuestionMgrZr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.ConvertFileToSwfMgrIfaceZr;
import com.cwc.app.iface.zr.FileIndexMgrIfaceZr;
import com.cwc.app.iface.zr.QuestionMgrIfaceZr;
import com.cwc.app.iface.zwb.QuestionCatalogMgrIfaceZwb;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.lucene.zr.QuestionIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.PathUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public  class QuestionMgrZr implements QuestionMgrIfaceZr
{
	
 
	private static Logger log = Logger.getLogger("ACTION");
	
	private FloorQuestionMgrZr floorQuestionMgrZr;
	
	private FloorFileMgrZr floorFileMgrZr ;
	
	private AdminMgrIFace adminMgr;
	
	
	private ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr;
	
	private QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb;
	
	private PathUtil pathUtil;
	
	
	
	public void setConvertFileToSwfMgrZr(ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr)
	{
		this.convertFileToSwfMgrZr = convertFileToSwfMgrZr;
	}

 

	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr)
	{
		this.floorFileMgrZr = floorFileMgrZr;
	}

	 
	public void setFloorQuestionMgrZr(FloorQuestionMgrZr floorQuestionMgrZr)
	{
		this.floorQuestionMgrZr = floorQuestionMgrZr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}
	

	public void setQuestionCatalogMgrZwb(QuestionCatalogMgrIfaceZwb questionCatalogMgrZwb) 
	{
		this.questionCatalogMgrZwb = questionCatalogMgrZwb;
	}

	/**
	 * 1.保存数据到knowledge表中.
	 * 2.保存文件到file表中.(file 表要扩展)
	 *	一下都是后台处理.前台可以关闭
	 * 3.文件建立索引.
	 * 4.后台处理文件的转换.
	 */
	@Override
	public void addQuestionIndex(HttpServletRequest request,long question_id,long product_id ,long product_catalog_id , long question_catalog_id ) throws Exception
	{
		
		try{
			//得到登录人
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
 
			String question_title = StringUtil.getString(request, "question_title");
			String content = StringUtil.getString(request, "ajaxContent"); 
			String fileNames = StringUtil.getString(request, "file_names");
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			int fileWithType = StringUtil.getInt(request, "file_with_type");
			this.handleFileAndCreateIndex(fileNames, sn, question_id, fileWithType, path, adminLoggerBean, question_title, content, product_id , product_catalog_id ,  question_catalog_id );
			 
		}catch (Exception e) {
			throw new SystemException(e,"addKnowledge",log);
		}
	 
	}
	
	//文件创建索引。文件重新命名，文件复制到指定文件夹
	/*
	 * 1.产品ID
	 * 2.产品分类Id
	 * 3.产品问题分类Id (如果是没有产品问题分类Id那么就要传入产品线Id)
	 */
	private void handleFileAndCreateIndex(String fileNames ,String sn , long question_id , int fileWithType,String path ,AdminLoginBean adminLoggerBean ,String question_title ,String content , long product_id ,long product_catalog_id , long question_catalog_id ) throws Exception {
		try{
		 
			StringBuffer sb = new StringBuffer("");
			//在文件中添加file.把文件从临时文件夹中移动到正式的文件夹中
			//注意文件名不能重复
			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
			if(fileNames.length() > 0 ){
				String[] fileNameArray = fileNames.split(",");
				if(fileNameArray != null && fileNameArray.length > 0 ){
				
					for(String fileNameTemp : fileNameArray){	
						StringBuffer realyFileName = new StringBuffer(sn);
	 					String suffix = StringUtil.getSuffix(fileNameTemp);
	 					realyFileName.append("_").append(question_id).append("_").append(fileNameTemp.replace("."+suffix, ""));
	 					int indexFile = floorFileMgrZr.getIndexOfFileByFileName(realyFileName.toString());
	 					if(indexFile != 0){
	 						realyFileName.append("_").append(indexFile);
	 					}
	 					
						String desFilePath = realyFileName.append(".")+"swf";
						realyFileName.append(suffix);
	 					long fileId = this.addFile(realyFileName.toString(), question_id, fileWithType, 0, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 					String realFilePath =  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString();
	 					FileUtil.moveFile(baseUrl+fileNameTemp,  realFilePath);
	 					sb.append(FileUtil.readOfficeFile(new File(realFilePath)));
	 					//转换文件 
	 					convertFileToSwfMgrZr.conver(realFilePath,Environment.getHome() +"upload/"+path+"/"+ desFilePath, Environment.getHome()+"upload/"+path+"/");
	 					
	 					//update file 把file的file_is_convert == 1.保存文件保存的路径
	 					DBRow fileTempRow = new DBRow() ;
	 					fileTempRow.add("file_is_convert", 1);
	 					fileTempRow.add("file_convert_file_path", "upload/"+path+"/"+ desFilePath);
	 					floorFileMgrZr.updateFileById(fileId, fileTempRow);
					}
					
				}
			}
			//创建索引
			QuestionIndexMgr.getInstance().addIndex(sb.toString(), question_title, content, question_id,product_id,product_catalog_id,question_catalog_id);
		}catch (Exception e) {
			throw new SystemException(e,"handleFileAndCreateIndex",log);
		}
	}
	
	private long addFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception {
		try{
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			file.add("file_is_convert", 0);
			file.add("file_convert_file_path", "");
			return floorFileMgrZr.addFile(file);
		}catch (Exception e) {
			throw new SystemException(e,"addFile",log);
		}
		
		
	}

  
	@Override
	public void deleteQuestionIndexById(long knowledgeId) throws Exception
	{
		try{
			
			//删除回答
			DBRow AnswerRows = questionCatalogMgrZwb.getAnswerByQuestionId(knowledgeId);
			//删除提醒
			questionCatalogMgrZwb.detQuestionCallAllByQuestionId(knowledgeId);
			 if(AnswerRows!=null){
				long answer_id = AnswerRows.get("answer_id", 0l);
				questionCatalogMgrZwb.deleteQuestionAnswer(answer_id,knowledgeId);
			 }
			 
			//删除主记录
			//删除索引
			//删除file表中的记录,文件不删除
			DBRow[] fillRows = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(knowledgeId, FileWithTypeKey.KNOWLEDGE);
			if(fillRows != null && fillRows.length > 0 ){
				for(int index = 0 , count = fillRows.length ; index < count ; index++ ){
						long fileId = fillRows[index].get("file_id", 0l);
						
						floorFileMgrZr.deleteFile(fileId);	
				}
			}
			floorQuestionMgrZr.deleteQuestionById(knowledgeId);

			//删除索引
			QuestionIndexMgr.getInstance().deleteIndex(knowledgeId);				
				
		}catch (Exception e) {
			throw new SystemException(e,"deleteKnowledgeById",log);
		}
	}

	@Override
	public void deleteIndexFile(long fileId) throws Exception
	{
		 try{
			 QuestionIndexMgr.getInstance().deleteIndex(fileId);
			 floorFileMgrZr.deleteFile(fileId);	 
		 }catch (Exception e) {
			 throw new SystemException(e,"deleteIndexFile",log);
		}
	}
	//更新如果是有新的文件上传那么就要建立索引，读取原来的所有文件的所有的内容
	//上传新的文件建立索引
	//读取原来所有文件的内容建立索引
	//1.更新索引。2.最后更新主单据信息
	@Override
	public void updateQuestionIndex(HttpServletRequest request) throws Exception
	{
		 try{
			 
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
 
			 String question_title = StringUtil.getString(request, "question_title");
			 String content = StringUtil.getString(request, "ajaxContent"); 
			 String fileNames = StringUtil.getString(request, "file_names");
			 String sn = StringUtil.getString(request, "sn");
			 String path = StringUtil.getString(request, "path");
			 int fileWithType =  FileWithTypeKey.KNOWLEDGE;
			 
			 long product_id = StringUtil.getLong(request, "product_id");
			 long product_catalog_id = StringUtil.getLong(request, "product_catalog_id");
			 long question_catalog_id = StringUtil.getLong(request, "question_catalog_id");
		 	 long question_id = StringUtil.getLong(request, "question_id");
		 	 StringBuffer sb = new StringBuffer("");
		 	
		 	 DBRow[] files = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(question_id, FileWithTypeKey.KNOWLEDGE);
		 	 for(int index = 0 , count = files.length ; index < count ; index++ ){
		 		 String realFilePath =  Environment.getHome()+"upload/"+path+"/"+files[index].getString("file_name");
		 		 File fileTemp = new File(realFilePath);
		 		 sb.append(FileUtil.readOfficeFile(fileTemp));
 		 	 }
		 	 if(fileNames != null && fileNames.trim().length() > 0 ){
 		 		 //处理新上传的文件
		 		 sb.append(this.handleNewFileConvertMove(fileNames, sn, question_id, fileWithType, path, adminLoggerBean));
		 	 }
		 	QuestionIndexMgr.getInstance().updateIndex(question_id, question_title, content, sb.toString(), product_id, product_catalog_id, question_catalog_id);
	 	     //更新时间
			 Date date=(Date)Calendar.getInstance().getTime();
			 SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 String createTime = dateformat.format(date);
		 	 DBRow updateKnowledge = new DBRow();
			 updateKnowledge.add("contents",content);
			 updateKnowledge.add("question_title", question_title);
			 updateKnowledge.add("question_update_time",createTime);
			 
			 floorQuestionMgrZr.updateQuestionById(question_id, updateKnowledge);
			 
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"updateKnowledge",log);
		}
		
	}
	
	
	private String handleNewFileConvertMove(String fileNames,String sn , long knowledgeId , int fileWithType , String path ,AdminLoginBean adminLoggerBean) throws Exception {
		StringBuffer sb = new StringBuffer("");
		try{
 
			//在文件中添加file.把文件从临时文件夹中移动到正式的文件夹中
			//注意文件名不能重复
			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
			if(fileNames.length() > 0 ){
				String[] fileNameArray = fileNames.split(",");
				if(fileNameArray != null && fileNameArray.length > 0 ){
					
					for(String fileNameTemp : fileNameArray){	
						StringBuffer realyFileName = new StringBuffer(sn);
	 					String suffix = StringUtil.getSuffix(fileNameTemp);
	 					realyFileName.append("_").append(knowledgeId).append("_").append(fileNameTemp.replace("."+suffix, ""));
	 					int indexFile = floorFileMgrZr.getIndexOfFileByFileName(realyFileName.toString());
	 					if(indexFile != 0){
	 						realyFileName.append("_").append(indexFile);
	 					}
	 					
						String desFilePath = realyFileName.append(".")+"swf";
						realyFileName.append(suffix);
	 					
						long fileId = this.addFile(realyFileName.toString(), knowledgeId, fileWithType, 0, adminLoggerBean.getAdid(), DateUtil.NowStr());
	 					String realFilePath =  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString();
	 					FileUtil.moveFile(baseUrl+fileNameTemp,  realFilePath);
	 					sb.append(FileUtil.readOfficeFile(new File(realFilePath)));
	 					//转换文件 
	 					convertFileToSwfMgrZr.conver(realFilePath,Environment.getHome() +"upload/"+path+"/"+ desFilePath, Environment.getHome()+"upload/"+path+"/");
	 					
	 					//update file 把file的file_is_convert == 1.保存文件保存的路径
	 					DBRow fileTempRow = new DBRow() ;
	 					fileTempRow.add("file_is_convert", 1);
	 					fileTempRow.add("file_convert_file_path", "upload/"+path+"/"+ desFilePath);
	 					floorFileMgrZr.updateFileById(fileId, fileTempRow);
					}
  				}
			}
			return sb.toString();
		}catch (Exception e) {
			 throw new SystemException(e,"updateKnowledge",log); 
		}
	}
	@Override
	public DBRow[] fileIndexSpellCheck(String keyWorld) throws Exception
	{	
			DBRow[] rows = null ;
		 try{
			 	String INDEX_FILE_PATH = pathUtil.getLucenePath()+File.separator+"question_index/";
			 	String INDEX_FILE_PATH_SPELL = pathUtil.getLucenePath()+File.separator+"file_index_spell/";
				
			 
				Directory	directory = FSDirectory.open(new File(INDEX_FILE_PATH));
			    DirectoryReader ireader = DirectoryReader.open(directory);
			     
			    
			    Directory spellIndexDirectory = FSDirectory.open(new File(INDEX_FILE_PATH_SPELL));
		 
			  
				SpellChecker spellchecker = new SpellChecker(spellIndexDirectory ,new LevensteinDistance() );
			 
				Analyzer analyzer = new IKAnalyzer(); 
				 
				IndexWriterConfig writerConfig= new IndexWriterConfig(Version.LUCENE_40,analyzer);
				
			    spellchecker.indexDictionary(new LuceneDictionary(ireader, "contents"),writerConfig,false);
			    
			    String[] values = spellchecker.suggestSimilar(keyWorld, 5 );
			    if(values != null  && values.length > 0 ){
			    	rows = new DBRow[values.length];
			    	for(int index = 0 ,count = values.length ; index < count ; index++ ){
			    		DBRow temp = new DBRow();
			    		temp.add("value", values[index]);
			    		rows[index] = temp ;
			    	}
			    }
			    spellchecker.close();
			    return rows ;
		 }catch (Exception e) {
			 throw new SystemException(e,"fileIndexAutoComplete",log);
		}
		 
	}

	@Override
	public DBRow[] fileIndexSuggest(String keyWorld) throws Exception
	{
		DBRow[] rows = null ;
		try{
			Lookup lookup =  new TSTLookup();
			String INDEX_FILE_PATH = pathUtil.getLucenePath()+File.separator+"question_index/";
			File file = new File(INDEX_FILE_PATH);
			if(!file.exists() || file.list().length < 1){
				return null;
			}
			Directory	directory = FSDirectory.open(file);
			 
		    DirectoryReader ireader = DirectoryReader.open(directory);
		    Dictionary dictionary = new LuceneDictionary(ireader, "content");
		  
		    lookup.build(dictionary);
		    List<LookupResult> results = lookup.lookup(keyWorld, true, 5);
		    if(results  != null && results.size() > 0 ){
		    	rows = new DBRow[results.size()];
		    	for(int index =0 , count = results.size() ; index < count ; index++ ){
		    		DBRow temp = new DBRow();
		    		temp.add("value", results.get(index).key);
		    		rows[index] = temp;
		    	}
		    	
		    }
		   
		    ireader.close();
		    directory.close();
		    return rows ;
		}catch (Exception e) {
			 throw new SystemException(e,"fileIndexSuggest",log);
		}
	}

	@Override
	public DBRow[] getSubProductCatalogByProductLineId(long product_line_id)
			throws Exception
	{
		 try{
			 return floorQuestionMgrZr.getAllProductCatelogByProductLineId(product_line_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getSubProductCatalogByProductLineId",log);
		}
	}

	@Override
	public DBRow getQuestionById(long question_id) throws Exception
	{
		 try{
			 return floorQuestionMgrZr.getQuestionById(question_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"getQuestionById",log);
		} 
	}
	 
	public String getQuestionFilesContent(long question_id) throws Exception {
		try{
			 DBRow[] files = floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(question_id, FileWithTypeKey.KNOWLEDGE);
		 	 String path = "knowledge" ;
		 	 if(files == null || files.length < 1){return "" ;}
		 	 StringBuffer sb = new StringBuffer();
		 	 for(int index = 0 , count = files.length ; index < count ; index++ ){
		 		 String realFilePath =  Environment.getHome()+"upload/"+path+"/"+files[index].getString("file_name");
		 		 File fileTemp = new File(realFilePath);
		 		 sb.append(FileUtil.readOfficeFile(fileTemp));
 		 	 }
		 	 return sb.toString();
		}catch (Exception e) {
			 throw new SystemException(e,"getQuestionFilesContent",log);
		}
	}



	public void setPathUtil(PathUtil pathUtil) {
		this.pathUtil = pathUtil;
	}
}
