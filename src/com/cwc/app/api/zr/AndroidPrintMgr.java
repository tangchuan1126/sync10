package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ScriptSessionFilter;
import org.directwebremoting.ScriptSessions;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.PrintServerNoPrintException;
import com.cwc.app.exception.printTask.AndroidPrintServerNameExitsException;
import com.cwc.app.exception.printTask.AndroidPrintServerUnLineException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zj.FloorTitleMgrZJ;
import com.cwc.app.floor.api.zr.FloorPrintTaskMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.checkin.PdfUtilIFace;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.PdfTypeKey;
import com.cwc.app.key.PrintTaskKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.AndroidUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public class AndroidPrintMgr implements AndroidPrintMgrIfaceZr{
	
	public static int RefreshTimes = 10 ;		//刷新的时间间隔
	public static String NotifyFlag = "print_server_id" ;	
	public static final int Letter = 1 ;		//1表示letter
	public static final int Label = 0 ; 		//0表示label  
	public static final int LabelOrletter = -1 ; 		//0表示label  
	public static final int ShippingLabelPrintLoadNumber = 1 ;
	public static final int ShippingLabelPrintOrderNumber = 2 ;
	public static final String  Available = "Available";
	public static final String  UnAvailable = "UnAvailable";

	
	static Logger log = Logger.getLogger("ACTION");

	
	private FloorPrintTaskMgrZr floorPrintTaskMgrZr ;
	
	private FloorTitleMgrZJ floorTitleMgrZJ ;
	
	private FloorAdminMgr floorAdminMgr ;
	
	private GoogleMapsMgrIfaceCc googleMapsMgrCc ;

	private  FloorCheckInMgrZwb floorCheckInMgrZwb ;
	
	private AdminMgrIFace adminMgr ;
	private PdfUtilIFace pdfUtilIFace=null;
	
	@Autowired
	public HttpServletRequest request;  
	


	public void setPdfUtilIFace(PdfUtilIFace pdfUtilIFace) {
		this.pdfUtilIFace = pdfUtilIFace;
	}


	@Override
	public void addCreatePdfTask(final long entry_id , final long detail_id) throws Exception{
		try{
			System.out.println("=========================add Create Pdf Task========================");

			/*String page = ConfigBean.getStringValue("systenFolder")+"check_in/check_in_pdf_server.html";
			Browser.withPage(page, new Runnable() {
				@Override
				public void run() {
 					ScriptSessions.addFunctionCall("createPDF",entry_id,detail_id);
 				}
			});*/
		}catch(Exception e){
			throw new SystemException(e,"addCreatePdfTask",log);
		}
	}
	
	@Override
	public void addCreatePdfTask(final long entry_id , final long detail_id,HttpServletRequest req) throws Exception{
		try{
			System.out.println("=========================Create pdf on server========================");
			this.pdfUtilIFace.createPdf(entry_id+"", detail_id+"", req,false);
		}catch(Exception e){
			throw new SystemException(e,"addCreatePdfTask",log);
		}
	}
	
	
	@Override
	public void addPrintTask(final DBRow row) throws Exception {
		//调用页面的Javascript方法
		System.out.println("=========================add Print Task========================");
		try{
			checkAddPrintTask(StringUtil.getLong(row.getString(NotifyFlag)));
			long print_task_id = floorPrintTaskMgrZr.addPrintTask(row);
			row.add("print_task_id", print_task_id);
			final String notifyPrintServer = row.getString(NotifyFlag) ;
			//DBRow[] willPrints = floorPrintTaskMgrZr.queryPrintTask(notifyWho, PrintTaskKey.ToPrint);
 			final String appendJsons = StringUtil.convertDBRowsToJsonString(row);
   			String page = ConfigBean.getStringValue("systenFolder")+"administrator/print/print_task_list.html";
  			Browser.withPageFiltered(page, new ScriptSessionFilter() {
				@Override
				public boolean match(ScriptSession scriptSession) {
					String printServer = (String)scriptSession.getAttribute(NotifyFlag) ;
					if(printServer != null && printServer.equals(notifyPrintServer)){
						return true ;
					}
					return false;
				}
			}, new Runnable() {
				@Override
				public void run() {
 					ScriptSessions.addFunctionCall("countPrintTask", appendJsons,row.getString("who"));
 				}
			});
		}catch (AndroidPrintServerUnLineException e) {
			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addPrintTask",log);
 		}
	}
	@SuppressWarnings("unused")
	@Override
	public void addPrintTask(DBRow[] datas) throws Exception {
		//调用页面的Javascript方法
		try{
			if(datas != null && datas.length > 0){
				long print_server_id = StringUtil.getLong(datas[0].getString(NotifyFlag));
				final String notifyPrintServer = datas[0].getString(NotifyFlag) ;
				final String who = datas[0].getString("who");
				
				checkAddPrintTask(print_server_id);	//检查是否能够打印
				
				for(DBRow row : datas){
					long print_task_id = floorPrintTaskMgrZr.addPrintTask(row);
					row.add("print_task_id", print_task_id);
				}
				final String appendJsons = StringUtil.convertDBRowsToJsonString(datas);
	   			String page = ConfigBean.getStringValue("systenFolder")+"administrator/print/print_task_list.html";
	  			Browser.withPageFiltered(page, new ScriptSessionFilter() {
					@Override
					public boolean match(ScriptSession scriptSession) {
						String printServer = (String)scriptSession.getAttribute(NotifyFlag) ;
						if(printServer != null && printServer.equals(notifyPrintServer)){
							return true ;
						}
						return false;
					}
				}, new Runnable() {
					@Override
					public void run() {
	 					ScriptSessions.addFunctionCall("countPrintTask", appendJsons,who);
	 				}
				});
			}
		}catch (AndroidPrintServerUnLineException e) {
				throw e ;
	 	}catch (Exception e) {
				throw new SystemException(e,"addPrintTask",log);
	  }
  }
	
	/**
	 * 检查当前的printServer
	 * 1.是否数据库存在，当前是否可以浏览器当前是否push到
	 * 2.因为无法判断浏览器是否是关闭这种情况,printServer 里面有时间字段，这个字段会每隔RefreshTimes 秒更新，如果android打印的时候，当前时间在这个之间那么
	 * 就是可以使用的
	 * @param row
	 * @throws Exception
	 */
	private void checkAddPrintTask(long print_server_id) throws AndroidPrintServerUnLineException , Exception {
   		boolean flag = false ;
 		long nowTime = new Date().getTime() ;
 		DBRow data = floorPrintTaskMgrZr.getPrintServerById(print_server_id);
 
 		if(data != null){
 			flag = nowTime -   data.get("last_refresh_time",0l)  <= 5 * RefreshTimes * 1000 ;
 		}
		if(!flag)
			throw new AndroidPrintServerUnLineException();
	}
	
	 
	
	@Override
	public int countPrintTask(String who, int status) throws Exception {
		try{
			 return floorPrintTaskMgrZr.countPrintTask(who, status);
		}catch (Exception e) {
			throw new SystemException(e,"countPrintTask",log);
		}
	}
	
 
	@Override
	public DBRow queryPrintTask(String print_server_id, int status , HttpSession httpSession) throws Exception {
		DBRow returnRow = new DBRow();
		try{
			 DBRow updateServerData = new DBRow();
			 updateServerData.add("last_refresh_time",String.valueOf(new Date().getTime()));
			 floorPrintTaskMgrZr.updatePrintServer(StringUtil.getLong(print_server_id), updateServerData);
			 DBRow printServer = floorPrintTaskMgrZr.getPrintServerById(StringUtil.getLong(print_server_id));
			 if(printServer != null){
				 long adid = printServer.get("adid", 0l);
				 boolean flag = isSetPrintServerOnLine(httpSession, adid);
				 returnRow.add("flag", String.valueOf(flag));
				 returnRow.add("who", print_server_id);
			 }
			 DBRow[] datas = floorPrintTaskMgrZr.queryPrintTaskByPrintId(StringUtil.getLong(print_server_id), status);
			 DBRow[] printFailed = floorPrintTaskMgrZr.queryPrintTaskByPrintIdOrderbyTime(StringUtil.getLong(print_server_id), PrintTaskKey.Failed);
			 returnRow.add("datas", datas);
			 returnRow.add("printFailed", printFailed);
			 return returnRow ;
		}catch (Exception e) {
			throw new SystemException(e,"queryPrintTask",log);
		}
	}
	
	@Override
	public void updatePrintTaskState(long task_id, int status) throws Exception {
		try{
			 floorPrintTaskMgrZr.updatePrintTaskState(task_id, status);
		}catch (Exception e) {
			throw new SystemException(e,"updatePrintTaskState",log);
		}
	}
 
	@Override
	public DBRow[] getAllAndroidPrinterServerByPsId(long ps_id) throws Exception {
		try{
			return floorPrintTaskMgrZr.getAllAndroidPrintServerByPsId(ps_id); 
		}catch (Exception e) {
			throw new SystemException(e,"getAllAndroidPrinterServer",log);
		}
	}
	/**
	 * 
	 * @param row
	 * @throws Exception
	 */
	@Override
	public long addAndroidPrintServer(DBRow row,DBRow addRow) throws Exception {
		try{
			String androidPrintServerName = row.getString("printer_server_name"); 
			int count = floorPrintTaskMgrZr.countAndroidPrintServerNameBy(androidPrintServerName);
			if(count > 0){
				throw new AndroidPrintServerNameExitsException();
			}
		long service_id= floorPrintTaskMgrZr.addAnroidPrintServer(row);
		if(service_id!=0l){
			String[] area_ids=addRow.getString("area_id").split(",");
			googleMapsMgrCc.deletePrinterArea(service_id);
			for(int i=0;i<area_ids.length;i++){
				DBRow areaPrinter=new DBRow();
				areaPrinter.add("area_id",area_ids[i]);
				areaPrinter.add("service_id",service_id);
				googleMapsMgrCc.addPrinterArea(areaPrinter);
	  	  }
		}
		return service_id;
 		}catch (AndroidPrintServerNameExitsException e) {
 			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addAndroidPrintServer",log);
		}
	}
	
	@Override
	public  long  addPrinterArea(DBRow row) throws Exception {
		return googleMapsMgrCc.addPrinterArea(row);
	}
	
	@Override
	public  long  deletePrinterArea(long p_id) throws Exception {
		return googleMapsMgrCc.deletePrinterArea(p_id);
	}
	
	@Override
	public void deleteAndroidPrintServer(long printer_server_id)
			throws Exception {
		try{
			floorPrintTaskMgrZr.deleteAndroidPrintServer(printer_server_id); 
			googleMapsMgrCc.deletePrinterArea(printer_server_id);
		}catch (Exception e) {
			throw new SystemException(e,"deleteAndroidPrintServer",log);
		}
	}
	@Override
	public DBRow getLoginUserHasPrint(long adid) throws Exception {
		try{
			 return floorPrintTaskMgrZr.getPrintServerByAdid(adid);
		}catch (Exception e) {
			throw new SystemException(e,"getLoginUserHasPrint",log);
		}
	}
	@Override
	public boolean isSetPrintServerOnLine(HttpSession httpSession , long adid ) throws Exception {
 		Object obj = adminMgr.getAdminLoginBean(httpSession);
 		 
		if(obj != null){
			return false;
		}
   		DBRow detailAdmin = floorAdminMgr.getDetailAdmin(adid);
   		if(detailAdmin != null){
	 		AdminLoginBean adminLoggerBean = new AdminLoginBean();
	 		
	 		adminLoggerBean.setAccount(detailAdmin.getString("account"));
			adminLoggerBean.setAdgid(detailAdmin.get("adgid",0l));
			adminLoggerBean.setAdid(detailAdmin.get("adid",0l));
			adminLoggerBean.setPs_id(detailAdmin.get("ps_id",0l));
			adminLoggerBean.setEmail(detailAdmin.getString("email"));
			adminLoggerBean.setLoginDate(DateUtil.NowStr());
	 		adminLoggerBean.setEmploye_name(detailAdmin.getString("employe_name"));
			adminLoggerBean.setTitles(floorTitleMgrZJ.getTitlesByAdid(detailAdmin.get("adid",0l)));
			
			adminLoggerBean.setIsLogin();
			httpSession.setAttribute(Config.adminSesion,adminLoggerBean);
			
			return true ;
   		}
   		return false ;
 		
	}
	
	@Override
	public DBRow[] getAllPrintServer() throws Exception {
		try{
			return floorPrintTaskMgrZr.getAllAndroidPrintServer();
 		}catch (Exception e) {
			throw new SystemException(e,"getAllPrintServer",log);
 		}
	}
	@Override
	public Map<String, List<DBRow>> fixAllPrintServer(DBRow[] arrays) {
		Map<String, List<DBRow>> returnMap = null ;
		if(arrays != null && arrays.length > 0 ){
			returnMap = new HashMap<String, List<DBRow>>(); 
			for(DBRow row : arrays){
				String key = row.getString("title");
				if(!StringUtil.isNull(key)){
					key = key.trim();
					List<DBRow> datas =	returnMap.get(key);
					if(datas == null){
						datas = new ArrayList<DBRow>();
						returnMap.put(key, datas);
					}
					datas.add(row);
				}
			}
		}
 		return returnMap;
	}
	@Override
	public void addPackingList(DBRow  data, DBRow loginRow) throws CheckInNotFoundException ,AndroidPrintServerUnLineException , Exception {
		try{
			String  order_nos = data.getString("order_nos");
			long dlo_detail_id = data.get("dlo_detail_id", 0l);
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow == null){
				throw new CheckInNotFoundException();
			}
			String customerId = detailRow.getString("customer_id");
			String companyId = detailRow.getString("company_id");

			if(!StringUtil.isNull(order_nos)){
				JSONArray jsonArray = new JSONArray();
				String[] orders = order_nos.split(",");
					for(String order : orders){
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("order_no", order.trim());
						jsonObject.put("companyId", companyId);
						jsonObject.put("customerId", customerId);
						jsonArray.put(jsonObject);
					}
				DBRow packIngListRow = getPrintTaskOfPackingList(loginRow, data,jsonArray);
				addPrintTask(packIngListRow);
			}
			 
		}catch(AndroidPrintServerUnLineException e){
			throw e ;
		}catch(CheckInNotFoundException e){
			throw e ;
		}catch(Exception e){
			throw new SystemException(e,"addCheckInPrintTask",log);
		}
	}
	private DBRow getPrintTaskOfPackingList(DBRow loginRow , DBRow data,JSONArray jsonArray  ) throws Exception{
		try{
			StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/a4print_packing_list.html?");
			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Letter);
			DBRow returnDBRow = new DBRow();
			urlBuilder.append("jsonString=")
			.append(jsonArray.toString())
			.append("&adid="+loginRow.get("adid", 0l))
			.append("&isprint=1")
			.append("&print_name="+printName);
			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "Print Packing List");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			return returnDBRow ;
		}catch(Exception e){
			throw new SystemException(e,"getPrintTaskOfPackingList",log);
		}
	}
	@Override
	public void addBillOfLoadingPrintTask(DBRow data, DBRow loginRow)
			throws Exception {
		try{
			System.out.println("=================addBillOfLoadingPrintTask================");
			String values =	data.getString("values");
			JSONObject valueJson = StringUtil.getJsonObject(values);
			if(valueJson != null ){
				String outSeal = StringUtil.getJsonString(valueJson, "out_seal");
				String container_no = StringUtil.getJsonString(valueJson, "container_no");
				String entryId = StringUtil.getJsonString(valueJson, "entry_id");
				// zhangrui 2015-03-12 添加
				String detail_id = StringUtil.getJsonString(valueJson, "detail_id");
				if(!StringUtil.isBlank(detail_id)){
					this.addCreatePdfTask(Long.parseLong(entryId), Long.parseLong(detail_id));
				}
				//////
				String who = StringUtil.getJsonString(valueJson, "who");
				long printer_server_id = StringUtil.getJsonLong(valueJson, NotifyFlag);
				JSONArray printBols =  StringUtil.getJsonArrayFromJson(valueJson, "datas");
				if(printBols != null && printBols.length() > 0 ){
					for(int index = 0 , count = printBols.length() ; index < count ; index++ ){
						DBRow insertTask = convertBillOfLoading(StringUtil.getJsonObjectFromArray(printBols, index), outSeal, entryId,who,loginRow,printer_server_id,container_no);
						addPrintTask(insertTask);
					}
				}
			}
 		}catch (PrintServerNoPrintException e) {
 			throw e ;
 		}catch (AndroidPrintServerUnLineException e) {
 			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addCheckInPrintTask",log);
 		}
	}
	
	@Override
	public void addBillOfLoadingPrintTask(DBRow data, DBRow loginRow,HttpServletRequest request)
			throws Exception {
		try{
			System.out.println("=================addBillOfLoadingPrintTask================");
			String values =	data.getString("values");
			JSONObject valueJson = StringUtil.getJsonObject(values);
			if(valueJson != null ){
				String outSeal = StringUtil.getJsonString(valueJson, "out_seal");
				String container_no = StringUtil.getJsonString(valueJson, "container_no");
				String entryId = StringUtil.getJsonString(valueJson, "entry_id");
				// zhangrui 2015-03-12 添加
				String detail_id = StringUtil.getJsonString(valueJson, "detail_id");
				if(!StringUtil.isBlank(detail_id)){
					this.addCreatePdfTask(Long.parseLong(entryId), Long.parseLong(detail_id),request);
				}
				//////
				String who = StringUtil.getJsonString(valueJson, "who");
				long printer_server_id = StringUtil.getJsonLong(valueJson, NotifyFlag);
				JSONArray printBols =  StringUtil.getJsonArrayFromJson(valueJson, "datas");
				if(printBols != null && printBols.length() > 0 ){
					for(int index = 0 , count = printBols.length() ; index < count ; index++ ){
						DBRow insertTask = convertBillOfLoading(StringUtil.getJsonObjectFromArray(printBols, index), outSeal, entryId,who,loginRow,printer_server_id,container_no);
						addPrintTask(insertTask);
					}
				}
			}
 		}catch (PrintServerNoPrintException e) {
 			throw e ;
 		}catch (AndroidPrintServerUnLineException e) {
 			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addCheckInPrintTask",log);
 		}
	}
 
	
	 
	private DBRow convertBillOfLoading(JSONObject jsonObject , String outSeal ,  String entryId , String who , DBRow loginRow , long print_server_id,String container_no) throws PrintServerNoPrintException, Exception{
		DBRow returnDBRow = new DBRow();
 		String url = getCheckInPrintUrlPath(StringUtil.getJsonString(jsonObject, "path"))  ;
		JSONArray jsonArray = new JSONArray() ;
		jsonObject.remove("path");
		jsonArray.put(jsonObject);
		String printName = getPrintServerDefaultPrintName(print_server_id, Letter) ;
		url += "?isprint=1&out_seal=" +outSeal+"&entry_id="+entryId+"&jsonString="+jsonArray.toString()+"&print_name="+printName+"&adid="+loginRow.get("adid", 0l)+"&container_no="+container_no; 
  	 
		returnDBRow.add("url", url);
		returnDBRow.add("user_name",loginRow.getString("employe_name"));
		returnDBRow.add("adid", loginRow.get("adid", 0l));
		returnDBRow.add("state", PrintTaskKey.ToPrint);
 		returnDBRow.add("who", printName);	//具体的打印机
		 
		returnDBRow.add("title", "BillOfLading");
		returnDBRow.add("date", DateUtil.NowStr());
		returnDBRow.add("from_where", "Android");
		returnDBRow.add(NotifyFlag,print_server_id );
		return returnDBRow ;
	}
	 
	private String getPrintServerDefaultPrintName(long print_server_id , int type ) throws PrintServerNoPrintException,SystemException{
		String returnValue = null ;
		try{
			DBRow[] arrays = googleMapsMgrCc.getPrintByPrintServer(print_server_id);
			if(arrays != null && arrays.length > 0 ){
				for(DBRow row : arrays){
					if(row.get("type", -1) == type){
						returnValue =row.getString("name");
						break ;
					}
				}
			}
			if(returnValue != null){
				return returnValue ;
			}else{
				throw new PrintServerNoPrintException();
			}
		}catch (PrintServerNoPrintException e) {
			throw e ;
		}catch (Exception e) {
			throw new SystemException(e,"getPrintName",log);
 		}
	}
	  
	 
	private String getCheckInPrintUrlPath(String path){
		String[] pathArray = path.split("/");
		if(pathArray != null && pathArray.length > 1){
			return ConfigBean.getStringValue("systenFolder") +"check_in/" + pathArray[1];
		}
		return "" ;
	}
	
	@Override
	public DBRow queryWillPrintAndPrintFailTask(long print_server_id) throws Exception {
 		try{
 			DBRow returnDBRow = new DBRow();
 			returnDBRow.add("willPrint", floorPrintTaskMgrZr.queryPrintTask(print_server_id, PrintTaskKey.ToPrint));
 			returnDBRow.add("printFail", floorPrintTaskMgrZr.queryPrintTask(print_server_id, PrintTaskKey.Failed));
 			return returnDBRow ;
 		}catch (Exception e) {
			throw new SystemException(e,"queryWillPrintAndPrintFailTask",log);
 		}
	}
	@Override
	public void updatePrintTask(long print_task_id, DBRow row) throws Exception {
		try{
			floorPrintTaskMgrZr.updatePrintTask(print_task_id, row);
 		}catch (Exception e) {
			throw new SystemException(e,"updatePrintTask",log);
 		}
	}
	
	@Override
	public void loadingTickPrintTask(DBRow data, DBRow loginRow) throws AndroidPrintServerUnLineException , PrintServerNoPrintException, Exception {
		try{
			/*window_check_in_time,entryId,loadNo,
			DockID,company_name,gate_container_no,
			seal,CompanyID,CustomerID,*/
			DBRow returnDBRow = new DBRow();
			StringBuilder urlBuilder = new StringBuilder(getCheckInPrintUrlPath(data.getString("PATH")));
			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Label);
			urlBuilder.append("?window_check_in_time=").append(data.getString("window_check_in_time"))
						 .append("&entryId=").append(data.getString("ENTRYID"))
						 .append("&loadNo=").append(data.getString("LOADNO"))
						 .append("&DockID=").append(data.getString("DockID"))
						 .append("&company_name=").append(data.getString("COMPANY_NAME"))
						 .append("&gate_container_no=").append(data.getString("gate_container_no"))
						 .append("&seal=").append(StringUtil.isNull(data.getString("SEAL")) ? "NA" : data.getString("SEAL"))
						 .append("&CompanyID=").append(data.getString("CompanyID"))
						 .append("&CustomerID=").append(data.getString("CustomerID"))
						 .append("&print_name=").append(printName)
						 .append("&number=").append(data.getString("number"))
						 .append("&number_type=").append(data.getString("number_type"))
						 .append("&order_no=").append(data.getString("order_no"))
						 .append("&adid=").append(loginRow.get("adid",0l))
						 .append("&resources_type=").append(data.getString("resources_type"))//2015-1-5 chaolijuan 添加参数resources_type
						 .append("&resources_id=").append(data.getString("resources_id"));//2015-1-5 chaolijuan 添加参数resources_id
			
	  		returnDBRow.add("url",urlBuilder.toString());
 			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who",printName );
			returnDBRow.add("title", "Loading Tick");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.getString(NotifyFlag));
			
			
			addPrintTask(returnDBRow);
	 
 		}catch(AndroidPrintServerUnLineException e){
 			throw  e ;
 		}catch(PrintServerNoPrintException e){
 			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"loadingTickPrintTask",log);
 		}
	}
	@Override
	public void countingSheetPrintTask(DBRow data, DBRow loginRow)
			throws  AndroidPrintServerUnLineException , PrintServerNoPrintException ,Exception {
		try{
			//{ISPRINT=1, CHECKDATATYPE=LOAD, ENTRYID=100893, MACHINE=Please select the machine number, CHECKLEN=1, WHO=zwb_checkin, NUMBER=125745687, CUSTOMERID=VIZIO, VERSION=1.0.13, PRINT_SERVER_ID=13, LOGINACCOUNT=admin, METHOD=CountingSheetPrintTask, PASSWORD=admin, DOOR_NAME=17, COMPANYID=W12}
			DBRow returnDBRow = new DBRow();
			
	  		StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/"+ data.getString("url"));
	  		//jsonString=[{"checkDataType":"LOAD","number":"125745687","door_name":"17","companyId":"W12","customerId":"VIZIO","number_type":3,"checkLen":1}]&entryId=100893&isprint=1
	  		String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Letter) ;
	  		urlBuilder.append("?jsonString=");
	  		
	  		JSONArray jsonArray = new JSONArray() ;
	  		JSONObject jsonObject = new JSONObject();
	  		jsonObject.put("checkDataType", "PICKUP");
	  		jsonObject.put("number", data.getString("NUMBER"));
	  		jsonObject.put("door_name",data.getString("DOOR_NAME"));
	  		jsonObject.put("companyId", data.getString("COMPANYID"));
	  		jsonObject.put("customerId", data.getString("CUSTOMERID"));
	  		jsonObject.put("number_type", data.getString("NUMBER_TYPE"));
	  		jsonObject.put("order", data.getString("order"));
	  		jsonObject.put("checkLen", "1");
	  		jsonArray.put(jsonObject);
	  		 
	  		urlBuilder.append(jsonArray.toString());
	  		urlBuilder.append("&isprint=1&entryId="+data.getString("ENTRYID")+"&print_name="+printName);
	  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
	  		urlBuilder.append("&resources_type=").append(data.getString("resources_type"));//2015-1-5 chaolijuan 添加参数resources_type
	  		urlBuilder.append("&resources_id=").append(data.getString("resources_id"));//2015-1-5 chaolijuan 添加参数resources_id
	  		urlBuilder.append("&appointmentDate=").append(data.getString("appointmentDate"));//2015-1-9 zhangrui 添加参数resources_id
	  		urlBuilder.append("&containerNo=").append(data.getString("containerNo"));//2015-1-9 zhangrui 添加参数resources_id

	  		
 			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "Counting Sheet");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			addPrintTask(returnDBRow);
 		}catch(PrintServerNoPrintException e){
 			throw  e ;
 		}catch(AndroidPrintServerUnLineException e){
 			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"loadingTickPrintTask",log);
 		}
	}
	@Override
	public void receiptsTicketPrintTask(DBRow data, DBRow loginRow) throws  PrintServerNoPrintException , AndroidPrintServerUnLineException ,  Exception {
 		
		//http://192.168.1.82:8080/Sync10/check_in/print_receipts_wms_by_bol_container.html?entryId=100893&door_name=69&ctnr=EMHU644780
 		DBRow returnDBRow = new DBRow();
  		StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/print_receipts_wms_by_bol_container.html");
   		String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Label) ;
   
   		urlBuilder.append("?isprint=1")
   		.append("&entryId="+data.getString("ENTRYID"))
   		.append("&door_name="+data.getString("door_name"))
   		.append("&"+data.getString("type"))
   		.append("="+data.getString("type_value"))
   		.append("&print_name="+printName)
   		.append("&number="+data.getString("number"))
   		.append("&number_type"+data.getString("number_type"))
   		.append("&adid="+loginRow.get("adid", 0l));
   		
   		
  		returnDBRow.add("url", urlBuilder.toString());
		returnDBRow.add("user_name",loginRow.getString("employe_name"));
		returnDBRow.add("adid", loginRow.get("adid", 0l));
		returnDBRow.add("state", PrintTaskKey.ToPrint);
		returnDBRow.add("who", printName );
		returnDBRow.add("title", "Receipts Ticket");
		returnDBRow.add("date", DateUtil.NowStr());
		returnDBRow.add("from_where", "Android");
		returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
		addPrintTask(returnDBRow);
	}
	
	/**
	 * 1.查询默认的如果有，如果有多个那么默认一个
	 * 2.查询当前登录人 本仓库所有打印服务器
	 */
	@Override
	public DBRow getPrintServerByLoginUser(long ps_id,long area_id, int type) throws Exception {
			DBRow returnRow = new DBRow();
			try{
				DBRow[] rows = googleMapsMgrCc.getPrinterServerByAreaIdandPtype(area_id);
				DBRow defaultPrintServer = null ;
				if(rows != null && rows.length > 0){
					defaultPrintServer = rows[0];
  				}
				DBRow[] allPrintServer = floorPrintTaskMgrZr.queryAllPrintServerPrinterAndZoneBy(ps_id,type);
				if(defaultPrintServer != null ){
					boolean flag = isDefaultServerInAllPrintServer(defaultPrintServer.get("printer_server_id", 0l), allPrintServer);
					returnRow.add("default",flag ? defaultPrintServer.get("printer_server_id", 0l) : 0);
				}else{
					returnRow.add("default",0);
				}
				AndroidUtil.fixPrintServerData(allPrintServer);
				returnRow.add("allPrintServer",allPrintServer);
				return returnRow ;
			}catch (Exception e) {
				throw new SystemException(e,"getPrintServerByLoginUser",log);
			}
	}
	/**
	 * 打印一个Load的情况
	 * @param data
	 * @param loginRow
	 * @throws Exception 
	 */
	private void addShippingLabelLoadNumberPrintTask(DBRow data , DBRow loginRow) throws Exception{
		
   		String customer_id = data.getString("customer_id") ;
  		String company_id = data.getString("company_id");
  		String master_bol_no = data.getString("master_bol_no");
   		String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Label);
   		String loadnumber = data.getString("loadnumber");
   		StringBuilder urlBuilder = new StringBuilder();
   		urlBuilder.append(ConfigBean.getStringValue("systenFolder")+"check_in/shipping_lable.html");
 		urlBuilder.append("?isprint=1");
  		urlBuilder.append("&CustomerID="+customer_id);
	   	urlBuilder.append("&CompanyID="+company_id);
		urlBuilder.append("&load="+loadnumber);
	   	urlBuilder.append("&print_name="+printName);
	   	urlBuilder.append("&adid="+loginRow.get("adid", 0l));
	   	urlBuilder.append("&MasterBOLNo="+master_bol_no);

	   	
	
   		DBRow insertRow = new DBRow();
  		insertRow.add("url", urlBuilder.toString());
		insertRow.add("user_name",loginRow.getString("employe_name"));
		insertRow.add("adid", loginRow.get("adid", 0l));
		insertRow.add("state", PrintTaskKey.ToPrint);
		insertRow.add("who", printName );
		insertRow.add("title", "Shipping  Label");
		insertRow.add("date", DateUtil.NowStr());
		insertRow.add("from_where", "Android");
		insertRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
		
		addPrintTask(insertRow);
   		
	}
	/**
	 * 打印多个order的情况
	 * @param data
	 * @param loginRow
	 * @throws Exception 
	 */
	private void addShippingLabelOrderNumberPrintTask(DBRow data , DBRow loginRow) throws Exception{
		String orders = data.getString("orders");
		if(!StringUtil.isNull(orders)){
			 String[] orderArray = orders.split(",");
			 
			 DBRow[] insertRows = new DBRow[orderArray.length];
			 
	 		 String customer_id = data.getString("customer_id");
	  		 String company_id = data.getString("company_id");
	   		 String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Label);
	   	 
			 for(int index = 0 , count = orderArray.length ; index < count ; index++){
				 DBRow insertRow = new DBRow();
				 StringBuilder urlBuilder = new StringBuilder();
		   		 urlBuilder.append(ConfigBean.getStringValue("systenFolder")+"check_in/shipping_lable_order.html");
		 		 urlBuilder.append("?isprint=1");
		  		 urlBuilder.append("&CustomerID="+customer_id);
			   	 urlBuilder.append("&CompanyID="+company_id);
				 urlBuilder.append("&orderNo="+orderArray[index]);
			   	 urlBuilder.append("&print_name="+printName);
			   	 urlBuilder.append("&adid="+loginRow.get("adid", 0l));
			   	 
		  		 insertRow.add("url", urlBuilder.toString());
				 insertRow.add("user_name",loginRow.getString("employe_name"));
				 insertRow.add("adid", loginRow.get("adid", 0l));
				 insertRow.add("state", PrintTaskKey.ToPrint);
				 insertRow.add("who", printName );
				 insertRow.add("title", "Shipping  Label");
				 insertRow.add("date", DateUtil.NowStr());
				 insertRow.add("from_where", "Android");
				 insertRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
				 
				 insertRows[index] = insertRow ;
			 }
			 addPrintTask(insertRows);
		}
	}
	@Override
	public void addShippingLabelPrintTask(DBRow data, DBRow loginRow) throws Exception {
 		int type = data.get("print_type", 0);
   		if(type == ShippingLabelPrintLoadNumber){
   			addShippingLabelLoadNumberPrintTask(data,loginRow);
   		}else{
   			addShippingLabelOrderNumberPrintTask(data,loginRow);
   		}
 	}
	
	
	private boolean isDefaultServerInAllPrintServer(long defaultServer , DBRow[] allPrintServer){
		boolean flag = false ;
		for(DBRow  row : allPrintServer){
			if(defaultServer == row.get("printer_server_id", 0l)){
				flag = true ;
				break ;
			}
		}
		return flag ;
	}
	//这里是注册那些print_server_id 在线的
	@SuppressWarnings("deprecation")
	@Override
	public boolean setPrintTaskNofityWho(String who) throws Exception {
 		if(!StringUtil.isNull(who)){
 			long printServerId = StringUtil.getLong(who) ;
 			if(printServerId > 0){
 				DBRow row = floorPrintTaskMgrZr.getPrintServerById(printServerId);
 				if(row != null){
 					WebContextFactory.get().getScriptSession().setAttribute(NotifyFlag, who); 	
 					return true ;
 				}
 			}
 		}
 		return false ;
	}
	
	@Override
	public void entryCtnrPrintPlateLabel(DBRow data, DBRow loginRow) throws Exception {
 		try{
 			long entry_id = data.get("entry_id", 0l);
 			int count = data.get("count", 0);
			DBRow entryRow = floorCheckInMgrZwb.getEntryIdById(entry_id);
			long dlo_detail_id = data.get("dlo_detail_id",0l);
			DBRow detail = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);

			String containerNo = entryRow.getString("gate_container_no");
			int printPlateLabelCounted = entryRow.get("print_plate_label_count",0 );
 			StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/plate_lable.html?");
			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l), Label);
			DBRow printTaskRow = new DBRow();
 			urlBuilder.append("adid="+loginRow.get("adid", 0l))
			.append("&isprint=1")
			.append("&entry_id="+entry_id)
			.append("&print_name="+printName)
			.append("&container_no="+containerNo)
 			.append("&start="+printPlateLabelCounted)
 			.append("&gate_check_in_time="+entryRow.getString("gate_check_in_time"))
 			.append("&relative_number="+detail.getString("number"))
			.append("&count="+count);
			
			
			printTaskRow.add("url", urlBuilder.toString());
			printTaskRow.add("user_name",loginRow.getString("employe_name"));
			printTaskRow.add("adid", loginRow.get("adid", 0l));
			printTaskRow.add("state", PrintTaskKey.ToPrint);
			printTaskRow.add("who", printName );
			printTaskRow.add("title", "Print Entry Plate Label");
			printTaskRow.add("date", DateUtil.NowStr());
			printTaskRow.add("from_where", "Android");
			printTaskRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			
 			addPrintTask(printTaskRow);
 			//打印成功过后 更新entry 字段上面的 print_plate_label_count 
 			DBRow updateRow = new DBRow();
 			updateRow.add("print_plate_label_count", printPlateLabelCounted+count);
 			floorCheckInMgrZwb.updateYcIdByDloId(entry_id, updateRow);
  		}catch(AndroidPrintServerUnLineException e){
  			throw e ;
  		}catch(Exception e){
			throw new SystemException(e,"entryCtnrPrintPlateLabel",log);
 		}
	}
	@Override
	public boolean isPrintServerInUse(long print_server_id) throws Exception {
		try{
			boolean  flag = false ;
			DBRow data = floorPrintTaskMgrZr.getPrintServerById(print_server_id);
	 		  //如果当前的时间 - last_refresh_time > 60s 那么就说明他没有使用了
	 		flag = ( new Date().getTime() -   data.get("last_refresh_time",0l) ) <= 60 * 1000  ;
	 		return flag;
		}catch(Exception e){
			throw new SystemException(e,"isPrintServerInUse",log);
		}
	}
	
	@Override
	public DBRow[] getPrintServerByBigScreen(long ps_id, PageCtrl pc) throws Exception {
		try{
			DBRow[] returnRows = floorPrintTaskMgrZr.getAllPrinterBy(ps_id,pc);
			fixPrintServerBigScreen(returnRows);
			return returnRows ;
		}catch(Exception e){
			throw new SystemException(e,"getPrintServerByBigScreen",log);
		}
	}
	/**
	 * {
		printer_name,
		printer_area:
		printer_type :
		status :
	   }
	 * @param datas
	 * @author zhangrui
	 * @Date   2014年11月11日
	 */
	private void fixPrintServerBigScreen(DBRow[] datas){
		if(datas != null && datas.length > 0){
			for(DBRow temp : datas){
				long nowTime = new Date().getTime() ;
				String printer_area  = temp.getString("area_name");
				String printer_name  = temp.getString("name");
				String printer_type  = temp.get("type",0) == 1 ? "Letter" : "Label";
				String status =  ( nowTime -   temp.get("last_refresh_time",0l)  <= 5 * RefreshTimes * 1000 ) ? Available : UnAvailable;
				temp.remove("name");
				temp.remove("area_name");
				temp.remove("type");
				temp.remove("last_refresh_time");
				
				temp.add("printer_area", printer_area);
				temp.add("printer_name", printer_name);
				temp.add("printer_type", printer_type);
				temp.add("status", status);
			}
		}
	}
	@Override
	public HoldDoubleValue<Integer, Integer> getAvailableAndAll(long ps_id) throws Exception {
		try{
			DBRow[] returnRows = floorPrintTaskMgrZr.getAllPrinterBy(ps_id,null);
			if(returnRows != null && returnRows.length > 0){
				int  availableCount = getAvailablePrinter(returnRows);
				return new HoldDoubleValue<Integer, Integer>(availableCount, returnRows.length);
			}
			return new HoldDoubleValue<Integer, Integer>(0,0);
		}catch(Exception e){
			throw new SystemException(e,"getAvailableAndAll",log);
		}
	}
	private int getAvailablePrinter(DBRow[] datas){
		int count = 0 ;
		if(datas != null && datas.length > 0){
			for(DBRow temp : datas){
				long nowTime = new Date().getTime() ;
				String status =  ( nowTime -   temp.get("last_refresh_time",0l)  <= 5 * RefreshTimes * 1000 ) ? Available : UnAvailable;
				if(status.equals(Available)){
					count ++ ;
				}
			}
		}
		return  count ;
	}
	
	@Override
	public void addGatePrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception {
		try{

 			DBRow returnDBRow = new DBRow();
			
	  		StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/check_in_open_print.html");
 	  		String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l),  Label) ;
 	  		
	  		
 	  		urlBuilder.append("?isprint=1&main_id="+data.getString("entry_id")+"&print_name="+printName);
	  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
	  		
 			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "Gate Label");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			addPrintTask(returnDBRow);
			
		}catch (AndroidPrintServerUnLineException e) {
			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addGatePrintLabelTask",log);
 		}
 	
	}
	
	@Override
	public void addTLPPrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception {
		try{

 			DBRow returnDBRow = new DBRow();
			
	  		StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/receive_tlp_print.html");
 			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l),  Label) ;
 	  		
	  		
 	  		urlBuilder.append("?isprint=1&print_name="+printName);
	  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
	  		urlBuilder.append("&palletIds=").append(data.get("palletIds"));
	  		urlBuilder.append("&date=").append(data.get("date"));
	  		
 			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "TLP Label");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			addPrintTask(returnDBRow);
			
		}catch (AndroidPrintServerUnLineException e) {
			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addGatePrintLabelTask",log);
 		}
 	
	}
	
	@Override
	public void addRNPrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception {
		try{

 			DBRow returnDBRow = new DBRow();
			
	  		StringBuilder urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"check_in/receive_RN_print.html");
 			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l),  Label) ;
 	  		
	  		
 	  		urlBuilder.append("?isprint=1&print_name="+printName);
	  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
	  		urlBuilder.append("&company_id=").append(data.get("company_id"));
	  		urlBuilder.append("&receipt_no=").append(data.get("receipt_no"));
	  		urlBuilder.append("&date=").append(data.get("date"));
	  		
 			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "TLP Label");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			addPrintTask(returnDBRow);
			
		}catch (AndroidPrintServerUnLineException e) {
			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addGatePrintLabelTask",log);
 		}
 	
	}
	
	
	@Override
	public void addBasicLabelPrintTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception {
		try{

 			DBRow returnDBRow = new DBRow();
 			
 			int container_type = data.get("container_type", 0);
 			
 			String printName = getPrintServerDefaultPrintName(data.get(NotifyFlag, 0l),  Label);
 			
 			StringBuilder urlBuilder = new StringBuilder();
 			
 			if(container_type == ContainerTypeKey.CLP)
 			{
 				urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template/print_container_c.html");
 				urlBuilder.append("?id="+data.getString("con_id")+"&print_name="+printName);
 		  		urlBuilder.append("&clp_type_id=").append(data.get("type_id", 0l));
 		  		urlBuilder.append("&pc_id=").append(data.get("pc_id", 0l));
 		  		urlBuilder.append("&isprint=1");
 		  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
 			}
 			else
 			{
 				urlBuilder = new StringBuilder(ConfigBean.getStringValue("systenFolder")+"administrator/lable_template/template/print_container_t.html");
 				urlBuilder.append("?id="+data.getString("con_id"));
 				urlBuilder.append("&isprint=1");
 		  		urlBuilder.append("&adid=").append(loginRow.get("adid", 0l));
 			}
	  		
 			returnDBRow.add("url", urlBuilder.toString());
			returnDBRow.add("user_name",loginRow.getString("employe_name"));
			returnDBRow.add("adid", loginRow.get("adid", 0l));
			returnDBRow.add("state", PrintTaskKey.ToPrint);
			returnDBRow.add("who", printName );
			returnDBRow.add("title", "Gate Label");
			returnDBRow.add("date", DateUtil.NowStr());
			returnDBRow.add("from_where", "Android");
			returnDBRow.add(NotifyFlag,data.get(NotifyFlag, 0l));
			addPrintTask(returnDBRow);
			
		}catch (AndroidPrintServerUnLineException e) {
			throw e ;
 		}catch (Exception e) {
			throw new SystemException(e,"addGatePrintLabelTask",log);
 		}
 	
	}
	/**
	 * pdf server 
	 * @author zhangtao
	 * 2015年7月23日 15:54:56
	 * @param data
	 * @param loginRow
	 * @throws AndroidPrintServerUnLineException
	 * @throws Exception
	 *
	 */
	public void sendDataToPDFServerHtml(DBRow data) throws Exception{
		try {
			if(!data.isEmpty()){
				//step1 调用pdfserver上的方法 
				final String html=data.get("PDFHTML", null);
				final long bill_id=data.get("bill_id", 0l);
				final long module_key=data.get("module_key", 0l);
				final long pdf_type_key=data.get("pdf_type_key", 0l);
				final long adid=data.get("adid", 0l);
				if(!StringUtil.isBlank(html)){
					try{
						String page = ConfigBean.getStringValue("systenFolder")+"check_in/pdf_server.html";
						Browser.withPage(page, new Runnable() {
							@Override
							public void run() {
								PdfTypeKey pdfTypeKey=new PdfTypeKey();
			 					ScriptSessions.addFunctionCall("createPdf",html,bill_id,pdfTypeKey.getStatusById(PdfTypeKey.Invoice),module_key,pdf_type_key,adid);
			 				}
						});
					}catch(Exception e){
						throw new SystemException(e,"sendDataToPDFServerHtml",log);
					}
				}
			}
		} catch (Exception e) {
			 throw new SystemException(e,"sendDataToPDFServerHtml",log);
		}
	};
	
	
	
	
	public void setFloorPrintTaskMgrZr(FloorPrintTaskMgrZr floorPrintTaskMgrZr) {
		this.floorPrintTaskMgrZr = floorPrintTaskMgrZr;
	}
	public void setFloorTitleMgrZJ(FloorTitleMgrZJ floorTitleMgrZJ) {
		this.floorTitleMgrZJ = floorTitleMgrZJ;
	}
	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}
	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
	
}
