package com.cwc.app.api.zr;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import seamoonotp.stringchange;
import us.monoid.json.JSONArray;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.iface.zr.ConvertFileToSwfMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

 

public class FileMgrZr implements FileMgrIfaceZr
{
	private static Logger log = Logger.getLogger("ACTION");
	
	protected FloorFileMgrZr floorFileMgrZr;
	
	protected ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr;
	
	protected String downloadFileUrl = null;
	protected String uploadFileUrl = null;
	protected String deleteFileUrl = null;
	protected final String file_download_base = "_fileserv/file/";
	
	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr)
	{
		this.floorFileMgrZr = floorFileMgrZr;
	}
	
	
	public void setDownloadFileUrl(String downloadFileUrl) {
		this.downloadFileUrl = downloadFileUrl;
	}


	public void setConvertFileToSwfMgrZr(
			ConvertFileToSwfMgrIfaceZr convertFileToSwfMgrZr)
	{
		this.convertFileToSwfMgrZr = convertFileToSwfMgrZr;
	}
	
	public void setUploadFileUrl(String uploadFileUrl) {
		this.uploadFileUrl = uploadFileUrl;
	}

	public void setDeleteFileUrl(String deleteFileUrl) {
		this.deleteFileUrl = deleteFileUrl;
	}


	@Override
	public DBRow getFileByFileId(long fileId) throws Exception
	{
		try{
			return floorFileMgrZr.getFileById(fileId);
		}catch (Exception e) {
			throw new SystemException(e, "getFileByFileId", log);
		}
	}

	@Override
	public DBRow[] getFilesByFileWithIdAndFileWithType(long file_with_id,
			int file_with_type) throws Exception
	{
		try{
		 
			return floorFileMgrZr.getAllFileByFileWithIdAndFileWithType(file_with_id,file_with_type);
		}catch (Exception e) {
			throw new SystemException(e, "getFileByFileId", log);
		}
	}

	@Override
	public DBRow convertExitsFile(long file_id, String tableName, String path)
			throws Exception
	{
		DBRow result = null ;
		try{
			
			if(tableName.equals("file")){
				DBRow row = floorFileMgrZr.getFileById(file_id);
				if(row != null && row.get("file_is_convert", 0) == 0 ){
					String basePath = Environment.getHome()+"upload/"+path+"/" ; 
					String desFileName = row.getString("file_name").toString().substring(0,row.getString("file_name").lastIndexOf(".")) + ".swf" ;
					String srcFilePath = basePath + row.getString("file_name").toString();
					String desFilePath = basePath + desFileName;
					 
					String tempPdfPath = basePath;
					result = convertFileToSwfMgrZr.conver(srcFilePath, desFilePath, tempPdfPath);
					//更新file表中的数据
					DBRow fileTempRow = new DBRow() ;
 					fileTempRow.add("file_is_convert", 1);
 					fileTempRow.add("file_convert_file_path", "upload/"+path+"/"+ desFileName);
 					floorFileMgrZr.updateFileById(file_id, fileTempRow);
				}
				
			}else{
				result = new DBRow() ;
				result.add("flag", "error");
				result.add("message", "目前不支持非File表中数据的转换");
				
			}
			return result;
		
		}catch (Exception e) {
			throw new SystemException(e, "convertExitsFile", log);
		}
	 
	}
	
	@Override
	public DBRow convertExitsFileReturnFile(long file_id, String tableName, String path)
			throws Exception
	{
		DBRow result = null ;
		try{
			
			if(tableName.equals("file")){
				DBRow row = floorFileMgrZr.getFileById(file_id);
				if(row != null && row.get("file_is_convert", 0) == 0 ){
					String basePath = Environment.getHome()+path+"/" ; 
					String desFileName = row.getString("file_name").toString().substring(0,row.getString("file_name").lastIndexOf(".")) + ".swf" ;
					String srcFilePath = basePath + row.getString("file_name").toString();
					String desFilePath = basePath + desFileName;
					 
					String tempPdfPath = basePath;
					result = convertFileToSwfMgrZr.conver(srcFilePath, desFilePath, tempPdfPath);
					//更新file表中的数据
					DBRow fileTempRow = new DBRow() ;
 					fileTempRow.add("file_is_convert", 1);
 					fileTempRow.add("file_convert_file_path", path+"/"+ desFileName);
 					floorFileMgrZr.updateFileById(file_id, fileTempRow);
				}
				
			}else{
				result = new DBRow() ;
				result.add("flag", "error");
				result.add("message", "目前不支持非File表中数据的转换");
				
			}
			return result;
		
		}catch (Exception e) {
			throw new SystemException(e, "convertExitsFile", log);
		}
	 
	}


	@Override
	public void UploadScannerFileOnTempFile(HttpServletRequest request)
			throws Exception
	{
		try{
		 
			String contentType = request.getContentType();
			
			if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) 
			{
				DataInputStream in = new DataInputStream(request.getInputStream());
				int formDataLength = request.getContentLength();
				byte dataBytes[] = new byte[formDataLength];
				int byteRead = 0;
				int totalBytesRead = 0;
				
				while (totalBytesRead < formDataLength) 
				{
					byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
					totalBytesRead += byteRead;
				}
				
				in.close();
				
				String file = new String(dataBytes);		
				String saveFile = file.substring(file.indexOf("filename=\"") + 10);		
				saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
				saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,saveFile.indexOf("\""));


				
				int lastIndex = contentType.lastIndexOf("=");
				String boundary = contentType.substring(lastIndex + 1, contentType.length());
				int pos;
				
				pos = file.indexOf("filename=\"");
				pos = file.indexOf("\n", pos) + 1;
				pos = file.indexOf("\n", pos) + 1;
				pos = file.indexOf("\n", pos) + 1;


				int boundaryLocation = file.indexOf(boundary, pos) - 4;
				int startPos = ((file.substring(0, pos)).getBytes()).length;
				int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
				
				String basePath = Environment.getHome() + "upl_imags_tmp/";
				File fileToRecv = new File(basePath+ saveFile);
			 
				if(!fileToRecv.exists())
				{
					fileToRecv.createNewFile();
				}
				FileOutputStream fileOut = new FileOutputStream(fileToRecv);
				
				
				fileOut.write(dataBytes, startPos, (endPos - startPos));
				
				fileOut.flush();
				fileOut.close();
			}
			
			
		}catch (Exception e) {
			throw new SystemException(e, "UploadScannerFileOnTempFile", log);
		}
		
	}

	private DBRow[] getFilesBy(long file_with_id, int file_with_type, int file_with_class , long file_with_detail_id) throws Exception{
		try{
			return floorFileMgrZr.getFileBy(file_with_id,   file_with_type,   file_with_class ,  file_with_detail_id);
		}catch(Exception e){
			throw new SystemException(e, "getFilesBy", log);
		}
	}
	
	@Override
	public DBRow[] getFilesByFileWithIdAndFileWithTypeAndFileWithClass(
			long file_with_id, int file_with_type, int file_with_class)
			throws Exception {
		try{
			return floorFileMgrZr.getAllFileByFileWithIdAndFileWithTypeAndFileWithClass(file_with_id,file_with_type,file_with_class);
		}catch (Exception e) {
			throw new SystemException(e, "getFilesByFileWithIdAndFileWithTypeAndFileWithClass", log);
		}
	}
	@Override//查询商品文件
	public DBRow[] getProductFileByFileTypeAndWithIdAndPcId(long file_with_id,
			int file_with_type, int product_file_type, String pc_Id)
			throws Exception {
		 try{
			 return floorFileMgrZr.getProductFileByWithIdAndWithTypeFileTypeAndPcId(file_with_id, file_with_type, product_file_type, pc_Id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getProductFileByFileTypeAndWithIdAndPcId", log);
		}
	}
	
	
	private String getStoreFileDir(int fileWithType ){
		String returnDir = "" ;
		switch (fileWithType) {
			case FileWithTypeKey.OCCUPANCY_MAIN:
				returnDir = "check_in";
			break;
		}
		return returnDir ;
		
	}
	private String getPrefix(int fileWithType , int fileWithClass){
		if(fileWithType == FileWithTypeKey.OCCUPANCY_MAIN){
			FileWithCheckInClassKey fileWithCheckInClassKey = new FileWithCheckInClassKey();
			if(fileWithClass == FileWithCheckInClassKey.PhotoLoad 
					||  fileWithClass == FileWithCheckInClassKey.PhotoReceive   
					){
				fileWithClass = FileWithCheckInClassKey.PhotoTaskProcessing ;
			}
			return fileWithCheckInClassKey.getFileWithTypeId(fileWithClass);
 		}
		return "" ;
	}

	/**
	 * android 图片控件上传文件
	 */
	@Override
	public DBRow[] androidUpZipPictrue(DBRow row , AdminLoginBean adminLoginBean) throws Exception {
		 try{
			 
			 List<DBRow> returnList = new ArrayList<DBRow>();
 			 int fileWithClass = row.get("file_with_class", 0);
			 long fileWithId = row.get("file_with_id", 0l);
			 int fileWithType = row.get("file_with_type", 0);
			 long file_with_detail_id =  row.get("file_with_detail_id", 0l);
			 String option_file_param = row.getString("option_file_param");
			 String sessionId = row.getString("session_id");
			 file_with_detail_id  = file_with_detail_id == 0l ? fileWithId :file_with_detail_id ;
			 String filePath = row.getString("filePath");
			 String prefix = getPrefix(fileWithType, fileWithClass);
			 String storeFileDir =  getStoreFileDir(fileWithType);
 			 if(!StringUtil.isBlank(filePath)){
 				    String append = prefix+"_"+fileWithId+"_" ;
 				    //String appendUri = "upload/"+storeFileDir+"/" ;
					//String fileDescBasePath = Environment.getHome() + appendUri  ;
					File dir = new File(filePath);
					if(dir.isDirectory()){
						File[] files = dir.listFiles();
						for(File file : files){
							 DBRow tempRow = new DBRow();
 							 //long file_id =  addFile(append+file.getName(), fileWithId, fileWithType, fileWithClass , adminLoginBean.getAdid(), DateUtil.NowStr(),file_with_detail_id,option_file_param);
 	 						 //FileUtil.checkInAndroidCopyFile(file,append , fileDescBasePath);
							 //改为文件服务器上传  --by chenchen
							 DBRow[] rows = uploadToFileServ(file.getAbsolutePath(), sessionId);
							 if(rows != null && rows.length>0){
								 DBRow f = rows[0];
								 long file_id = f.get("file_id", 0l);
								 tempRow.add("uri", file_download_base + file_id);
								 tempRow.add("file_id", file_id);
								 returnList.add(tempRow);
								 //更新file表
								 DBRow r = new DBRow();
								 r.add("file_name", append+file.getName());
								 r.add("file_with_id", fileWithId);
								 r.add("file_with_type", fileWithType);
								 r.add("file_with_class", fileWithClass);
								 r.add("file_with_detail_id", file_with_detail_id);
								 r.add("expire_in_secs", 0);
								 r.add("option_file_param", option_file_param);
								 updateFile(r, file_id);
							 }
 						}	
					}

				}
 			 return returnList.toArray(new DBRow[0]);
		 }catch (Exception e) {
			 throw new SystemException(e, "androidUpZipPictrue", log);
		}
	}
	/**
	 * 得到Uri的地址
	 */
	@Override
	public DBRow[] androidGetFileUri(long file_with_id, int file_with_type,
			int file_with_class,long file_with_detail_id,String option_relative_param) throws Exception {
		try{
			DBRow[] returnRows =  floorFileMgrZr.getFileBy(file_with_id, file_with_type, file_with_class,file_with_detail_id,option_relative_param);
			if(returnRows != null && returnRows.length > 0){
				DBRow[] returnArrays = new DBRow[returnRows.length];
  				 String storeFileDir =  getStoreFileDir(file_with_type);
 				for(int index = 0 , count = returnRows.length ; index < count ; index++ ){
					DBRow tempRow = fixPhoto(returnRows[index], storeFileDir);
					returnArrays[index] = tempRow ;
				} 
				return returnArrays ;
			}
			return null ;
		}catch(Exception e){
			 throw new SystemException(e, "androidGetFileUri", log);
		}
 	}
	
	private long addFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long creator,String upload_time , long file_with_detail_id,String option_file_param) throws Exception {
		try{
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",creator);
			file.add("upload_time",upload_time);
			file.add("file_is_convert", 0);
			file.add("file_convert_file_path", "");
			file.add("file_with_detail_id",file_with_detail_id);
			file.add("option_file_param",option_file_param);

			return floorFileMgrZr.addFile(file);
		}catch (Exception e) {
			throw new SystemException(e,"addFile",log);
		}
	}

	@Override
	public int getIndexOfFilebyFileName(String realyFileName) throws Exception
	{
		try{
			 return floorFileMgrZr.getIndexOfFilebyFileName(realyFileName);
		 }catch (Exception e) {
			 throw new SystemException(e, "getIndexOfFilebyFileName", log);
		}
	}


	@Override
	public long saveFile(DBRow row) throws Exception
	{
		try{
			 return floorFileMgrZr.addFile(row);
		 }catch (Exception e) {
			 throw new SystemException(e, "saveFile", log);
		}
	}


	@Override
	public void updateFile(DBRow row, long file_id) throws Exception
	{
		try{
			   floorFileMgrZr.updateFileById(file_id, row);
		 }catch (Exception e) {
			 throw new SystemException(e, "updateFile", log);
		} 
	}


	@Override
	public void deleteFilesByFileWidthIdAndFileWithType(long file_with_id,
			int file_with_type) throws Exception
	{
		try{
			   floorFileMgrZr.deleteFileByFileWithTypeAndFileWithId(file_with_id, file_with_type);
		 }catch (Exception e) {
			 throw new SystemException(e, "updateFile", log);
		}  
	}

	//改为文件服务器    --by chenchen
	private DBRow fixPhoto(DBRow temp , String storeFileDir ) throws Exception{
		
		if(temp != null){
			DBRow returnRow = new DBRow();
 			//String appendUri = "upload/"+storeFileDir+"/" ;
 			String appendUri = "_fileserv/file/" ;
			returnRow.add("file_name", temp.getString("file_name"));
			//returnRow.add("uri", appendUri + temp.getString("file_name"));
			returnRow.add("uri", appendUri + temp.get("file_id",0l));
			returnRow.add("file_id", temp.get("file_id",0l));
			returnRow.add("file_with_class", temp.get("file_with_class",0l));
			returnRow.add("file_path", temp.getString("file_path"));
			return returnRow ;
		}
		return null ;
	}
	/**
	 * 得到 所有的图片 CheckInModel
	 * [
	 * 	{	
	 * 		dir_name:
	 * 	 	dir_photo_count:
	 * 		photos:
	 * 		[
	 * 			{
	 * 				url:
	 * 				file_name:
 	 * 			}
	 * 		]	
	 *  }
	 * ]
	 * 	
	 * 对于以前的数据是没有分类的，那么添加一个No Group
	 * @param entry_id
  	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月15日
	 */
	@Override
	public DBRow[] getCheckInModelPhotos(long entry_id ) throws Exception {
		try{
			DBRow[] files = getFilesByFileWithIdAndFileWithType(entry_id, FileWithTypeKey.OCCUPANCY_MAIN);
			if(files != null && files.length > 0){
				Map<Integer, List<DBRow>> hashMap = new HashMap<Integer, List<DBRow>>(); 
 				String storeFileDir =  getStoreFileDir(FileWithTypeKey.OCCUPANCY_MAIN);
				for(DBRow temp : files){
					int fileWithClass =  temp.get("file_with_class", 0);
					 fileWithClass = FileWithCheckInClassKey.getFixFileWithClass(fileWithClass);
							
					List<DBRow> values = hashMap.get(fileWithClass); 
					if(values == null){
						values = new ArrayList<DBRow>();
					}
					DBRow fixRow = fixPhoto(temp,storeFileDir);
					if(fixRow != null){
						values.add(fixRow);
					}
					hashMap.put(fileWithClass, values);
				}
				if(hashMap.size() > 0){
					Iterator<Entry<Integer, List<DBRow>>>  it = hashMap.entrySet().iterator();
					List<DBRow> returnRows = new ArrayList<DBRow>();
					while(it.hasNext()){
						Entry<Integer, List<DBRow>>  entry = it.next() ;
						Integer file_with_class = entry.getKey();
						List<DBRow> arrays = entry.getValue();
 						DBRow tempInsertRow = new DBRow();
 						tempInsertRow.add("dir_name",getPrefix(FileWithTypeKey.OCCUPANCY_MAIN, file_with_class));
 						tempInsertRow.add("file_with_class",file_with_class);
 						tempInsertRow.add("dir_photo_count", arrays.size());
 						tempInsertRow.add("photos", arrays);
 						returnRows.add(tempInsertRow);
					}
					//排序
					Collections.sort(returnRows,new Comparator<DBRow>(){
						@Override
						public int compare(DBRow o1, DBRow o2) {
							Integer fixFileClass1 =  FileWithCheckInClassKey.getFixSortFileWithClass(o1.get("file_with_class", 0));
							Integer fixFileClass2 =  FileWithCheckInClassKey.getFixSortFileWithClass(o2.get("file_with_class", 0));
							return fixFileClass1.compareTo(fixFileClass2);
						}
					});
 					return returnRows.toArray(new DBRow[0]);
				}
 			}
			return new DBRow[]{};
		}catch(Exception e){
			 throw new SystemException(e, "getCheckInModelPhotos", log);
		}
 	}
	
	@Override
	public int deleteFileBy(long file_id) throws Exception {
		try{
			return floorFileMgrZr.deleteFile(file_id);
		}catch(Exception e){
			 throw new SystemException(e, "deleteFileBy", log);
		}
	}
	@Override
	public String downloadFromFileServ(String descBasePath, long fileId, String sessionId)throws Exception{
		String result = null;
		DBRow row = this.getFileByFileId(fileId);
		if(row == null){
			return result;
		}
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpGet httpget = new HttpGet(downloadFileUrl+"/"+fileId);
		
			httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
			//httpget.setHeader("Cookie", "JSESSIONID=C89E0C85853AA3E6A6012BE4932A116C" );
			
			HttpResponse response = httpClient.execute(httpget);
			
			StatusLine statusLine = response.getStatusLine();
			int httpStatus=statusLine.getStatusCode();
			
			String filePath = Environment.getHome() + descBasePath + row.getString("file_path");
			File file = new File(filePath);
			if (httpStatus == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				
				InputStream in = entity.getContent();
				try {
					FileOutputStream fout = new FileOutputStream(file);
					int l = -1;
					byte[] tmp = new byte[1024];
					while ((l = in.read(tmp)) != -1) {
						fout.write(tmp, 0, l);
					}
					fout.flush();
					fout.close();
				} finally {
					// 关闭低层流。
					in.close();
				}
				//把底层的流给关闭
				EntityUtils.consume(entity);
				result = filePath;
			}
			else{
				System.out.println("file:["+fileId+"] dose not existed");
			}
		} catch (IOException e) {
			System.out.println("file:["+fileId+"] dose not existed");
			//throw new SystemException(e, "deleteFromFileServ", log);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return result;
	}
	@Override
	public int deleteFromFileServ(long[] fileIds, String sessionId) throws Exception{
		if(fileIds.length == 0){
			return 0;
		}
		String url = deleteFileUrl + "/";
		for (long id : fileIds) {
			url += id+",";
		}
		url = url.substring(0, url.length()-1);
		
		HttpClient httpClient = new DefaultHttpClient();
		int count = 0;
		try {
			HttpDelete httpdelete = new HttpDelete(url);

			httpdelete.setHeader("Cookie", "JSESSIONID=" + sessionId);
			HttpResponse response = httpClient.execute(httpdelete);

			StatusLine statusLine = response.getStatusLine();
			int httpStatus=statusLine.getStatusCode();
			if (httpStatus == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				JSONArray json = new JSONArray(resp);
				count = json.length();
			}
		} catch (ClientProtocolException e) {
			throw new SystemException(e, "deleteFromFileServ", log);
		} catch (IOException e) {
			throw new SystemException(e, "deleteFromFileServ", log);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return count;
	}
	@Override
	public DBRow[] uploadToFileServ(String fullPath, String sessionId) throws Exception {
		DBRow[] files = null;
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpPost httppost = new HttpPost(uploadFileUrl);
			httppost.setHeader("Cookie", "JSESSIONID=" + sessionId);
			FileBody bin = new FileBody(new File(fullPath));
			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("file", bin);
			httppost.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				JSONArray json = new JSONArray(resp);
				files = DBRowUtils.jsonArrayAsDBRowArray(json);
				EntityUtils.consume(resEntity);
			}
		} catch (IOException e) {
			throw new SystemException(e, "uploadToFileServ", log);
		} finally {
			try {
				httpClient.getConnectionManager().shutdown();
			} catch (Exception e) {
				throw new SystemException(e, "uploadToFileServ", log);
			}
		}
		return files;
	}
	@Override
	public void uploadDirToFileServ(String dirPath, DBRow updateFileFields, String sessionId, String prefix) throws Exception{
		if(!StringUtil.isBlank(dirPath)){
			File dir = new File(dirPath);
			if(dir.isDirectory()){
				File[] files = dir.listFiles();
				for(File file : files){
					//上传到文件服务器
					DBRow[] rows = uploadToFileServ(file.getAbsolutePath(), sessionId);
					if(rows != null && rows.length>0){
						DBRow f = rows[0];
						long file_id = f.get("file_id", 0l);
						//更新file表
						if("".equals(updateFileFields.getString("file_name"))){
							updateFileFields.add("file_name", prefix+file.getName());
						}
						updateFileFields.add("expire_in_secs", updateFileFields.get("expire_in_secs", 0l));
						updateFile(updateFileFields, file_id);
					}
				}	
			}

		}
	}
	@Override
	public DBRow[] getCheckInCountingSheet(long entry_id, long detail_id , String option_file_param)
			throws Exception {
		try{
			DBRow[] resultRows  = null ;
			if(!StringUtil.isNull(option_file_param)){
				resultRows = floorFileMgrZr.getFileBy(entry_id, FileWithTypeKey.OCCUPANCY_MAIN, FileWithCheckInClassKey.PhotoCountingSheet, detail_id);
			}else{
				resultRows = floorFileMgrZr.getFileBy(entry_id, FileWithTypeKey.OCCUPANCY_MAIN, FileWithCheckInClassKey.PhotoCountingSheet, detail_id , option_file_param);
			}
			return resultRows ;
		}catch(Exception e){
			 throw new SystemException(e, "getCheckInCountingSheet", log);
		}
 	}
	@Override
	public DBRow[] getCheckInTaskProcessing(long entry_id, long detail_id)throws Exception {
		try{
			DBRow[] resultRows  = null ;
			resultRows = floorFileMgrZr.getFileBy(entry_id, FileWithTypeKey.OCCUPANCY_MAIN, FileWithCheckInClassKey.PhotoTaskProcessing, detail_id);
			return resultRows ;
		}catch(Exception e){
			throw new SystemException(e, "getCheckInCountingSheet", log);
		}
	}
}
