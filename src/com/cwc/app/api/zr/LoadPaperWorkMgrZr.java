package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.DockCheckInFirstException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.UpdatePalletTypeFailedException;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zr.FloorCheckInMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zyj.FloorOrderSystemMgr;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.LoadPaperWorkMgrIfaceZr;
import com.cwc.app.iface.zr.WmsLoadMgrZrIface;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.FileWithCheckInClassKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.LoadConsolidateReasonKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.CheckInEntryPermissionUtil;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.service.android.action.mode.HoldThirdValue;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class LoadPaperWorkMgrZr implements LoadPaperWorkMgrIfaceZr{
	static Logger log = Logger.getLogger("ACTION");
   
    
	private final static int QUERY_LOAD = 1 ;
	
	private final static int QUERY_ORDER = 3 ;

	private static final int ORDER_ONLY_WMS = 1 ;	//order 信息在wms系统中有，我们没有
	private static final int ORDER_BOTH = 2 ; 		//order 信息在wms系统和我们的系统都有
	private static final int ORDER_ONLY_SYNC = 3 ;  //order 信息只有在我们的系统中有，wms没有
		
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
    private SQLServerMgrIFaceZJ sqlServerMgrZJ;
    private WmsLoadMgrZrIface wmsLoadMgrZr ;			//在根据一个Load查询到数据的时候，把load 和 order的数据先保存在本地的数据库中
    private TransactionTemplate sqlServerTransactionTemplate;
    private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr ;
    private FileMgrIfaceZr fileMgrZr ;  
    private FloorOrderSystemMgr floorOrderSystemMgr ;
    private FloorCheckInMgrZr floorCheckInMgrZr ;
    
    /**
     * @param details
     * @return
     * @author zhangrui
     * @Date   2014年9月5日
     * 1.通过detail的信息去获取query_type , query_number ;
     */
    private HoldDoubleValue<Integer, String>  getLoadQueryInfo(DBRow detailRow){
    	
    	int queryType = QUERY_LOAD ;
		String queryNumber = detailRow.getString("number");
		int number_type = detailRow.get("number_type", 0);
    	if(number_type == ModuleKey.CHECK_IN_PONO){	// po的时候使用OrderNumber去查询的
			queryNumber = detailRow.getString("order_no");
			queryType = QUERY_ORDER ;
		}
		if(number_type == ModuleKey.CHECK_IN_ORDER){// order的时候
			queryType = QUERY_ORDER ;
		}
		return new HoldDoubleValue<Integer, String>(queryType , queryNumber);
    }
    
	@Override
	public DBRow CheckSerialnoFinalStatus(long dlo_detail_id,long  masterBol) throws Exception {
		DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
		DBRow result = null;
		if(detailRow.get("number_type", 0)==ModuleKey.CHECK_IN_LOAD){;
			Long[] masterBolArr = new Long[1];
			masterBolArr[0] = masterBol;
				
			result = this.sqlServerMgrZJ.CheckLoadFnOrderSerialnoFinalStatus(detailRow.get("number", ""), masterBolArr, detailRow.get("company_id", ""), detailRow.get("customer_id", ""));
		}else if(detailRow.get("number_type", 0)==ModuleKey.CHECK_IN_ORDER){
			result = this.sqlServerMgrZJ.checkOrderFnOrderSerialnoFinalStatus(detailRow.get("number", 0), detailRow.get("company_id", ""));
		}else if(detailRow.get("number_type", 0)==ModuleKey.CHECK_IN_PONO){
			result = this.sqlServerMgrZJ.CheckPoFnOrderSerialnoFinalStatus(detailRow.get("number", ""), detailRow.get("company_id", ""), detailRow.get("customer_id", ""));
		}
		if(result != null && result.get("is_all_picked",0)==YesOrNotKey.NO){
			DBRow[] orders = (DBRow[])result.get("orders",new DBRow[0]);
			List<DBRow> noScanOrders = new ArrayList<DBRow>();
			if(orders != null){
				for(DBRow order:orders){
					if(!"C".equals( order.get("is_all_picked", ""))){
						DBRow noScanOrder = new DBRow();
						noScanOrder = order;
						noScanOrders.add(noScanOrder);
					}
				}
			}
			
			result.remove("orders");
			result.add("noScanOrders", noScanOrders);
		}
		
		return result;
	}
	@Override
	public DBRow findOrderPalletByLoadNo(long dlo_detail_id, long adid) throws DockCheckInFirstException , Exception {
 
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			DBRow orderSystem = floorOrderSystemMgr.findOrderSystemByNumberTypeInfos(detailRow.get("LR_ID", 0l));
			 
			
			if(detailRow != null){
 				String company_id = detailRow.getString("company_id");
				String customer_id = detailRow.getString("customer_id");
				
				if(!StringUtil.isNull(customer_id) && !StringUtil.isNull(company_id) ){
					HoldDoubleValue<Integer, String> queryInfo =  getLoadQueryInfo(detailRow);
	  				 DBRow  ordersDetail = new DBRow();
 	  				 DBRow[] loadInfos = sqlServerMgrZJ.findOrderPalletForLoading(queryInfo.a,queryInfo.b,company_id,customer_id, adid, null) ;
 	  				 if(queryInfo.a == QUERY_LOAD){ //处理Load的情况
	  					 handQueryByLoad(loadInfos,detailRow) ;
	  				 }else{  					 	//处理Order的情况
	  					 handQueryByOrder(loadInfos,detailRow);
	  				 }
 	  				 if(loadInfos != null){
 	  					for(DBRow loadInfo:loadInfos){
 	 	  					 DBRow checkSns = this.CheckSerialnoFinalStatus(dlo_detail_id, loadInfo.get("master_bol_no", 0l));
 	 	  					 loadInfo.add("is_all_picked", checkSns.get("is_all_picked",0l));
 	 	  					 loadInfo.add("noScanOrders", checkSns.get("noScanOrders",new DBRow[0]));
 	 	  				 }
 	  				 }
 	  				 
	  				 ordersDetail.add("customer_id", customer_id);
					 ordersDetail.add("company_id", company_id);
					 ordersDetail.add("LR_ID", detailRow.get("LR_ID", 0l));
					 if(orderSystem != null){
						ordersDetail.add("system_type",orderSystem.get("system_type", 0));
						ordersDetail.add("order_type", orderSystem.get("ORDER_TYPE", 0));
					 }
 					 ordersDetail.add("load_info",loadInfos);
  	  				 return ordersDetail ;
	  			}
			}
			return null ;
		 
 	}
	@Override
	public DBRow getOrderInfosByMasterBol(long dlo_detail_id,long master_bol_no ,long adid) throws  Exception {
 
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			
			String company_id = detailRow.getString("company_id");
			String customer_id = detailRow.getString("customer_id");
			 DBRow checkSns = this.CheckSerialnoFinalStatus(dlo_detail_id, master_bol_no);
		
			DBRow result = this.sqlServerMgrZJ.addOrderInfoByMasterBol(detailRow.get("number", ""), company_id, customer_id, adid, master_bol_no, null);
			result.add("is_all_picked", checkSns.get("is_all_picked",0l));
			result.add("noScanOrders", checkSns.get("noScanOrders",new DBRow[0]));
			
			return result;
				
				
		 
 	}
	/**
	 *处理Order来的情况,这种是没有Master_bol 的 
	 * @param loadInfos
	 * @param detailRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月9日
	 */
	private void handQueryByOrder(DBRow[] loadInfos,DBRow detailRow) throws Exception{
		try{
 			long lr_id =  detailRow.get("lr_id", 0l) ;
 			for(DBRow row :  loadInfos){
				addLoadOrderInfos(row, 0l, lr_id);
			}
		}catch(Exception e){
  			throw new SystemException(e,"handQueryByOrder",log);
		}
	}
	/**
  	 * 处理wms过来的数据
  	 * 如果对应的Order被cast的了，那么我们这边就应该删除掉，同时删除Pallet type的信息
  	 * 其他的情况不变，应该添加上如果已经扫描的那么应该给返回当前的Pallet number 已经扫描了，加上一个状态.
   	 * @param row
  	 * @throws Exception
  	 */
  	private void handQueryByLoad(DBRow[] masterBolInfos ,  DBRow detail ) throws Exception{
  		try{
 			handLoadMasterBol(masterBolInfos, detail);
  		}catch (Exception e) {
  			throw new SystemException(e,"handWmsLoadOrder",log);
		}
  	}
  	/**
  	 * 1.对于没有Load数据的情况，首先插入到数据库
  	 * 2.order 的状态都是Open
  	 * @param loadInfos
  	 * @param loadRow
  	 * @throws Exception
  	 */
  	private void handLoadMasterBol(DBRow[] mastbolInfos , DBRow detail  ) throws Exception{
  		try{
  			String company_id = detail.getString("company_id");
  			String customer_id = detail.getString("customer_id");
  			long lr_id = detail.get("lr_id", 0l);
   			adWmsMasterBolInfo(mastbolInfos, company_id, customer_id, lr_id);
  		}catch (Exception e) {
  			throw new SystemException(e,"handLoadNumberNotExits",log);
		}
  	}
  	
    /**
     * 如果存在的时候
     * 1.因为他们有可能调整Load上面的Order，所以我会和我本地的数据比较一下，如果只要有一个OrderNo对应不上，如果对应不上那么我会删除我本地load关联的所有的数据
     * 重新添加一次
     * 
     * 2.同时加上WmsOrderId,我们系统当中的
     * 
     * @param loadInfos
     * @param load_id
     * @throws Exception
     */
  	/*private void handLoadNumberExits(DBRow[] wmsMasterBolInfos , long load_id , String customer_id , String company_id) throws Exception{
  		try{
  			DBRow[]  exitsMasterBols =  wmsLoadMgrZr.getMastBolRowByLoadId(load_id);	//sync 数据库中 的数据 删除
  			for(DBRow syncMasterBols : exitsMasterBols){
  				HoldDoubleValue<Boolean, Long> returnHoldValue = isSyncMasterBolNoInWms(syncMasterBols, wmsMasterBolInfos);
  				if(returnHoldValue.a){	
  					//如果存在那么删除
  					wmsLoadMgrZr.deleteWmsMasterBolInfo(returnHoldValue.b);		//删除
  				} 
  			}
  			//删除有的数据，然后再添加
  			adWmsMasterBolInfo(wmsMasterBolInfos, company_id, customer_id, load_id);
  		}catch (Exception e) {
  			throw new SystemException(e,"handLoadNumberExits",log);
		}
  	}*/
	private void adWmsMasterBolInfo(DBRow[] mastbolInfos,String  company_id , String customer_id ,  long lr_id ) throws Exception{
	  	try{
	  		if(mastbolInfos != null && mastbolInfos.length > 0){
				for(int index = 0 , count = mastbolInfos.length ; index < count ; index++ ){
					DBRow mastbolInfo =	mastbolInfos[index];
					DBRow masbolRow = fixMastbolDBRow(mastbolInfo, company_id, customer_id,lr_id);
					long wms_mastbol_id = wmsLoadMgrZr.saveOrUpdateWmsMasterBolRow(masbolRow); //wmsLoadMgrZr.addWmsMastBolRow(masbolRow); //saveOrUpdate.
					//检查当前master_bol 添加master_bol sync系统中的是否已经关闭的状态
					//一个Load，多个MasterBOl的时候，他是先关闭一个MasterBol，然后再关闭另外的一个MasterBol
					//String master_bol_no = mastbolInfo.getString("master_bol_no");
					 mastbolInfo.add("wmsCloseFlag", this.isCloseMasteOrOrder(lr_id, masbolRow.getString("master_bol_no")) ? "1":"0");  //?
					addLoadOrderInfos(mastbolInfo,wms_mastbol_id,lr_id);
				}
			}
	  	}catch (Exception e) {
  			throw new SystemException(e,"adWmsMasterBolInfo",log);
		}
  	}
	/**
  	 * 遍历master_bol 里面Order的信息 ,然后插入到我们系统中的load_order 表
  	 * saveOrUpdate .
  	 * @param mastbolInfo
  	 * @throws Exception
  	 */
  	private void addLoadOrderInfos(DBRow mastbolInfo , long wms_mastbol_id , long lr_id) throws Exception{
  		try{
  			 
   			DBRow[] orderInfos = (DBRow[])mastbolInfo.get("order_info",new DBRow[]{});	//这里是wms过来的Order的数据
   			DBRow[] orderInfosSync = wmsLoadMgrZr.getOrderInfoByLrId(lr_id, wms_mastbol_id);
   			
   			
   			/**
   			 * 1.wms 有，我们没有直接添加OrderInfo
   			 * 2.wms 有，我们也有，那么比较pallet_type 中的数据，是不是已经添加Load了，如果Load了，那么都添加上一个标志位
   			 * 3.wms 没有，我们有，那么说明是wms 系统中把这个Order cut掉了，所以我们应该删除，同时也应该删除如果已经Load的Pallet 的数据
   			 * 4.返回Order的信息还是以wms系统当中的为准，他是几个Order，就是几个Order
   			 * 
   			 * <orerno,type>
   			 */
   			List<HoldThirdValue<Long,Integer,DBRow>> result = fixWmsAndSyncOrderInfo(orderInfos, orderInfosSync);
   			for(HoldThirdValue<Long,Integer,DBRow> value : result){
   				
    			if(value.b == ORDER_ONLY_WMS){
   					directAddOrderInfo(value.c,mastbolInfo.getString("master_bol_no"),wms_mastbol_id,lr_id);
   				}else if(value.b == ORDER_ONLY_SYNC){
   					deleteOrderInfo(value.c);
   				}else{//Order_both 的情况
   					fixOrderInfo(value.c,orderInfosSync);
   				}
    		}
  		}catch (Exception e) {
  			throw new SystemException(e,"addLoadOrderInfos",log);
		}
  	}
  	
  	/**
  	 * 1.首先我要查询出这个Order 我现在已经添加Pallet 表中的数据
  	 * 2.然后再和他回传给我的数据比较，如果给我的Pallet number 我已经扫描了。那么添加一个标志位，如果是被consolidate
  	 * scan_flag = 1表示Scan  2 表示consolidate  
  	 * 如果是有提交的那么应该根据
  	 * @param data
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年9月11日
  	 */
  	private void fixOrderInfo(DBRow data , DBRow[] orderInfosSync) throws Exception{
  		try{ 
  			long order_no = data.get("orderno", 0l);
  			DBRow row = findOrderInfoInSync(order_no, orderInfosSync);
  			if(row != null){
  				String pro_no = row.getString("pro_no");
  				data.add("pro_no", pro_no);
  				//0 是detailId 这个方法中不需要所以传入0在sql中会判断  wfh
  	  			DBRow[] syncPalletNumbers = wmsLoadMgrZr.getOrderPalletInfoByWmsOrderId(0L,row.get("wms_order_id", 0l)); 
   	  			DBRow[] palletNumbers = (DBRow[])data.get("code_nos",new DBRow[]{});
  				for(DBRow palletNumber : palletNumbers){
  					String number =  palletNumber.getString("code_no");
  					HoldDoubleValue<Integer,DBRow>  result = findPalletNumberScanFlag(number, syncPalletNumbers);
  					if(result.a > 0){	//0 的情况就不要在添加了
  						palletNumber.add("scan_flag", result.a);
  						palletNumber.add("scan_detail_id", result.b.getString("dlo_detail_id"));
  						palletNumber.add("pallet_type",result.b.getString("wms_pallet_type") );
  					}
  				}
  			}
  		}catch(Exception e){
  			throw new SystemException(e,"fixOrderInfo",log);
  		}
  	}
  	/**
  	 * 返回当前pallet  scan_flag = 1表示Scan  2 表示consolidate  
  	 * @param number
  	 * @param syncPalletNumbers
  	 * @return
  	 * @author zhangrui
  	 * @Date   2014年9月11日
  	 */
  	private HoldDoubleValue<Integer,DBRow> findPalletNumberScanFlag(String number ,DBRow[] syncPalletNumbers) {
  		int scanFlag = 0 ;
  		DBRow returnRow = null ;
  		for(DBRow scanPallet : syncPalletNumbers){
  			String wms_pallet_number = scanPallet.getString("wms_pallet_number") ;
  			String wms_consolidate_pallet_number = scanPallet.getString("wms_consolidate_pallet_number");
  			if(wms_pallet_number.equalsIgnoreCase(number)){
  				returnRow = scanPallet ;
  				scanFlag = 1;
  				if(!StringUtil.isNull(wms_consolidate_pallet_number)){
  					scanFlag = 2 ;
  				}
  			}
  		}
  		return new HoldDoubleValue<Integer, DBRow>(scanFlag, returnRow) ;
  	}
  	/**
  	 * 找到在Sync系统中对应的OrderNo的DBRow
  	 * @param order_no
  	 * @param orderInfosSync
  	 * @return
  	 * @author zhangrui
  	 * @Date   2014年9月11日
  	 */
  	private DBRow findOrderInfoInSync(long order_no , DBRow[] orderInfosSync){
  		for(DBRow row : orderInfosSync){
  			if(row.get("order_numbers", 0l) == order_no){
  				return row ;
   			}
  		}
  		return null ;
  	}
  	/**
  	 * 删除本地的数据
  	 * @param syncOrderInfoRow
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年9月11日
  	 */
  	private void deleteOrderInfo(DBRow syncOrderInfoRow) throws Exception{
  		try{
  			wmsLoadMgrZr.deleteOrderInfoAndPalletInfoByOrderId(syncOrderInfoRow.get("wms_order_id", 0l));
  		}catch(Exception e){
  			throw new SystemException(e,"deleteOrderInfo",log);
  		}
  	}
  	/**
  	 * 直接添加OrderInfo的数据到WmsOrderInfo 的表
  	 * @param orderInfo
  	 * @param master_bol_no
  	 * @param wms_mastbol_id
  	 * @param dlo_detail_id
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年9月11日
  	 */
  	private void directAddOrderInfo(DBRow orderInfo , String  master_bol_no ,  long wms_mastbol_id , long lr_id) throws Exception{
  		try{
  			 
  				DBRow insertOrderRow = new DBRow();
				insertOrderRow.add("master_bol_id", wms_mastbol_id);
				insertOrderRow.add("master_bol_no", master_bol_no);
				//insertOrderRow.add("status", WmsOrderStatusKey.OPEN);
				insertOrderRow.add("order_numbers",orderInfo.getString("orderno"));
				insertOrderRow.add("pallets", orderInfo.get("pallets",0));
				insertOrderRow.add("staging_area_id", orderInfo.getString("stagingareaid"));
				insertOrderRow.add("pallet_type_id", orderInfo.getString("mpallettypeid"));
				insertOrderRow.add("lr_id", lr_id);
				insertOrderRow.add("case_total", orderInfo.get("case_total", 0));
				insertOrderRow.add("po_no", orderInfo.getString("pono"));			//zhangyanjie 要求添加
				insertOrderRow.add("reference_no", orderInfo.getString("referenceno"));  //zhangyanjie 要求添加
				insertOrderRow.add("mnote", orderInfo.getString("mnote"));  //zhangyanjie 要求添加
				insertOrderRow.add("note", orderInfo.getString("note"));  //zhangyanjie 要求添加

				insertOrderRow.add("account_id", orderInfo.getString("AccountID"));
				insertOrderRow.add("ship_to_id", orderInfo.getString("note"));
				insertOrderRow.add("ship_to_name", orderInfo.getString("ShipToName"));
				insertOrderRow.add("ship_to_address1", orderInfo.getString("ShipToAddress1"));
				insertOrderRow.add("ship_to_address2", orderInfo.getString("ShipToAddress2"));
				insertOrderRow.add("ship_to_city", orderInfo.getString("ShipToCity"));
				insertOrderRow.add("ship_to_state", orderInfo.getString("ShipToState"));
				insertOrderRow.add("ship_to_zip_code", orderInfo.getString("ShipToZipCode"));
				insertOrderRow.add("ship_to_country", orderInfo.getString("ShipToCountry"));
				insertOrderRow.add("ship_to_contact", orderInfo.getString("ShipToContact"));
				insertOrderRow.add("ship_to_phone", orderInfo.getString("ShipToPhone"));
				insertOrderRow.add("ship_to_extension", orderInfo.getString("ShipToExtension"));
				insertOrderRow.add("ship_to_fax", orderInfo.getString("ShipToFax"));
				insertOrderRow.add("ship_to_store_no", orderInfo.getString("ShipToStoreNo"));
				insertOrderRow.add("ship_to_store_no", orderInfo.getString("ShipToStoreNo"));
				insertOrderRow.add("supplier_id", orderInfo.getString("SupplierId"));
				insertOrderRow.add("company_id", orderInfo.getString("CompanyID"));
				insertOrderRow.add("pro_no", orderInfo.getString("pro_no"));

				
				wmsLoadMgrZr.addWmsLoadOrder(insertOrderRow);
  		}catch(Exception e) {
  			throw new SystemException(e,"directAddOrderInfo",log);
		}
  	}
  	/**
  	 * 返回一个List
  	 * HoldDoubleValue<Long,Integer> long 为order_no , Integer 为ONLY_WMS,both,ONLY_SYNC
  	 * 如果Wms当中的元素在Sync 存在那么就是Both，不存在那么就是ONLY_wms 
  	 * 在Sync当中剩下的那么就是Only_sync 系统当中
  	 * @return
  	 * @author zhangrui
  	 * @Date   2014年9月10日
  	 */
  	private List<HoldThirdValue<Long,Integer,DBRow>> fixWmsAndSyncOrderInfo(DBRow[] orderInfoWms , DBRow[] orderInfosSync){
  		List<DBRow> fixOrderInfoSyc  = new ArrayList<DBRow>();
  		 Collections.addAll(fixOrderInfoSyc, orderInfosSync) ;
  		
  		List<HoldThirdValue<Long,Integer,DBRow>>  returnArray = new ArrayList<HoldThirdValue<Long,Integer,DBRow>>();
  		
  		for(DBRow wmsInfo : orderInfoWms){
  			long order_no = wmsInfo.get("orderno", 0l);
  			if(isOrderNoInSyncOrderInfos(order_no, fixOrderInfoSyc)){
   				returnArray.add(new HoldThirdValue<Long, Integer,DBRow>(order_no, ORDER_BOTH,wmsInfo));	//有点问题到时候处理一下
  			}else{
  				returnArray.add(new HoldThirdValue<Long, Integer,DBRow>(order_no, ORDER_ONLY_WMS,wmsInfo));
  			}
  		}
  		//sync 剩下的
  		if(fixOrderInfoSyc != null && fixOrderInfoSyc.size() > 0){
  			for(DBRow sync : fixOrderInfoSyc){
  				returnArray.add(new HoldThirdValue<Long, Integer,DBRow>(sync.get("order_numbers", 0l), ORDER_ONLY_SYNC,sync));
  			}
  		}
  		return returnArray ;
  	}
  	/**
  	 * 判断order_no 是否在 orderInfosSync 里面存在，如果存在 true,否则false
  	 * 1,2,3 [3,4,5]
  	 * @param order_no
  	 * @param orderInfosSync
  	 * @return
  	 * @author zhangrui
  	 * @Date   2014年9月10日
  	 */
  	private boolean isOrderNoInSyncOrderInfos(long order_no , List<DBRow> orderInfosSync){
  		boolean flag = false ;
  		if(orderInfosSync != null && orderInfosSync.size() > 0){
  			for(int index = 0  ,count = orderInfosSync.size() ; index < count ; index++ ){
  				DBRow syncInfo  = orderInfosSync.get(index);
  				if(order_no == syncInfo.get("order_numbers", 0l)){
  					flag = true ;
  					orderInfosSync.remove(index);
   					break ;
  				}
  			}
  		}
  		return flag ;
  	}
	/**
  	 * 组织成master_bol_的数据
  	 * @param mastbolInfo
  	 * @param company_id
  	 * @param customer_id
  	 * @param wms_load_id
  	 * @return
  	 * @throws Exception
  	 */
  	private DBRow fixMastbolDBRow(DBRow mastbolInfo , String company_id , String customer_id ,long lr_id ) throws Exception{
  		try{
  			DBRow returnRow = new DBRow();
  			returnRow.add("master_bol_no", mastbolInfo.getString("master_bol_no"));
   		//	returnRow.add("status", WmsOrderStatusKey.OPEN);
  			returnRow.add("company_id", company_id);
  			returnRow.add("lr_id", lr_id);
  			returnRow.add("customer_id", customer_id);
  			returnRow.add("staging_area_id", mastbolInfo.getString("staging_area_id"));
   			returnRow.add("total_pallets", mastbolInfo.get("total_pallets", 0));
   			returnRow.add("pro_no", mastbolInfo.getString("pro_no")); //zhangrui添加pro_no

   			return returnRow ;
  		}catch (Exception e) {
  			throw new SystemException(e,"fixMastbolDBRow",log);
 		}
  	}
  	
 
  	@Override
	public void addSearchEntryValue(DBRow result , long entry_id , long  equipment_id , AdminLoginBean adminLoginBean ) throws CheckInEntryIsLeftException , DockCheckInFirstException , Exception{
			//search entry .如果没有数据 ， 在searchLoad
		DBRow entryRow  = floorCheckInMgrZwb.getEntryIdById(entry_id);
		if(entryRow == null){
			return ;
		}
		String wareHoseCheckInTime = entryRow.getString("warehouse_check_in_time") ;
	 
		/*if(StringUtil.isNull(wareHoseCheckInTime)){	//这里判断是否 在手持设备上做了DockCheckIn，有点奇怪依靠主单据去做的， zhangrui 09-24
			throw new DockCheckInFirstException();
		}*/
 		CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
 		boolean isLeft = CheckInEntryPermissionUtil.isEntryLeft(entryRow) && !isEntryDetailHasProcessing(entryRow.get("dlo_id", 0l));
 		if(isLeft){
 			throw new CheckInEntryIsLeftException();
 		}
		int status = entryRow.get("status", 0);
		//if(status != CheckInMainDocumentsStatusTypeKey.LEFT ){	//测试的时候_zhangrui
			DBRow[] details =   floorSpaceResourcesRelationMgr.getTaskByEntryIdWithOccpuyStatus(entry_id, 0,equipment_id);//floorCheckInMgrZwb.getEntryDetailHasDoor(entry_id);
			DBRow[] loadNumbers = fixLoadDatas(details);
			DBRow append = new DBRow();
			append.add("entry_id", entry_id);
			append.add("out_seal", entryRow.getString("out_seal"));
			append.add("details", loadNumbers);
			result.add("entry", append);
		//}
	}
    /**
     * 根据门的ID 获取门的名字
     * 如果没有查询到返回"";
     * @param sd_id
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date   2014年11月25日
     */
    private String getDoorName(long sd_id) throws Exception{
    	try{
    		if(sd_id != 0l){
    		  DBRow doorRow = floorCheckInMgrZwb.findDoorById(sd_id);
    		  if(doorRow != null){
    			  return doorRow.getString("doorId");
    		  }
    		}
    		return "" ;
    	}catch(Exception e){
  			throw new SystemException(e,"getDoorName",log);
    	}
    }
    /**
	 * 根据yc_id 去获取一个SpotNo
	 * 
	 * @param yc_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月29日
	 */
	private String getSpotName(long yc_id) throws Exception {
		try {
			if (yc_id != 0l) {
				DBRow spotRow = floorCheckInMgrZwb.findStorageYardControl(yc_id);
				if (spotRow != null) {
					return spotRow.getString("yc_no");
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "getSpotName", log);
		}
	}
	/**
  	 * [{
  	 *   door:
  	 *   load:[{
  	 *   	
  	 *   }]
  	 * }]
  	 * @param arrays
  	 * @return
	 * @throws Exception 
  	 */
	private DBRow[] fixLoadDatas(DBRow[] arrays) throws Exception{
		CheckInChildDocumentsStatusTypeKey typeKey = new CheckInChildDocumentsStatusTypeKey();
		List<DBRow> arrayList = new ArrayList<DBRow>();
		Map<String, List<DBRow>> maps = new HashMap<String, List<DBRow>>();
		if(arrays != null && arrays.length > 0 ){
			for(DBRow row : arrays){
				
				
				String number = row.getString("number");
				int numberType = row.get("number_type", 0);
				if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.UNPROCESS || row.get("resources_type",0) == OccupyTypeKey.SPOT){
					continue;
				}
				if(numberType == ModuleKey.CHECK_IN_LOAD || numberType == ModuleKey.CHECK_IN_PONO || numberType == ModuleKey.CHECK_IN_ORDER  ){
					
					DBRow insertRow = new DBRow();
					String doorName = getDoorName(row.get("resources_id", 0l)) ; // row.getString("doorId");
					List<DBRow> innerLoadNumbers =	maps.get(doorName);
					
					innerLoadNumbers =  innerLoadNumbers == null ? new ArrayList<DBRow>() : innerLoadNumbers ; 
					insertRow.add("number", number);
					insertRow.add("status", typeKey.getContainerTypeKeyValue(row.get("number_status", -1)));
					insertRow.add("status_int", row.get("number_status", -1));
					
					//徐佳升级过后执行的东西
 					insertRow.add("customer_id", row.getString("customer_id"));
					insertRow.add("company_id", row.getString("company_id"));
					insertRow.add("staging_area_id", row.getString("staging_area_id"));
					insertRow.add("number_type", row.get("number_type", 0));
					insertRow.add("account_id", row.getString("account_id"));
					insertRow.add("freight_term", row.getString("freight_term"));
					insertRow.add("dlo_detail_id", row.get("dlo_detail_id", 0l));
					insertRow.add("ic_id", row.get("ic_id", 0l));			//如果是Ic_id那么就是三星的货
					if(numberType == ModuleKey.CHECK_IN_ORDER){
						insertRow.add("append_number", row.getString("po_no"));
						insertRow.add("append_type", ModuleKey.CHECK_IN_PONO);
					}
					if(numberType == ModuleKey.CHECK_IN_PONO){
						insertRow.add("append_number", row.getString("order_no"));
						insertRow.add("append_type", ModuleKey.CHECK_IN_ORDER);
					}
					
					innerLoadNumbers.add(insertRow);
					maps.put(doorName, innerLoadNumbers);
				}
			}
		}
		Iterator<Entry<String, List<DBRow>>> it =	maps.entrySet().iterator();
		while(it.hasNext()){
			 Entry<String, List<DBRow>> entry = it.next();
			 DBRow inner = new DBRow();
			 inner.add("loads", entry.getValue().toArray(new DBRow[0]));
			 inner.add("door", entry.getKey());
			 arrayList.add(inner);
		}
		return arrayList.toArray(new DBRow[0]);
	}
	/**
	 * refresh 的时候不用更新时间和状态的了
	 * 同时考虑如果如果有多个Master_bol的情况那么会
	 * isClearData . 1 是清除数据 ,  0是保存数据
	 * 清除数据的时候，删除已经扫描的数据
	 */
	@Override
	public DBRow refreshOrderInfos(long dlo_detail_id, long adid, String master_bol_no,int isClearData , long lr_id) throws Exception {

		try{
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow != null){
				String company_id = detailRow.getString("company_id");
				String customer_id = detailRow.getString("customer_id");
 				if(!StringUtil.isNull(customer_id) && !StringUtil.isNull(company_id) ){
					HoldDoubleValue<Integer, String> queryInfo =  getLoadQueryInfo(detailRow);
	  				 DBRow  ordersDetail = new DBRow();
 	  				 DBRow[] loadInfos = sqlServerMgrZJ.findOrderPalletForLoading(queryInfo.a,queryInfo.b,company_id,customer_id, adid, null) ;
	  				 if(!StringUtil.isNull(master_bol_no)){
	  					loadInfos =  fixLoadInfosByMasterBol(master_bol_no,loadInfos);
	  				 }
	  				 if(isClearData == 1){ //删除当前这个Task所扫描的记录
	  					 wmsLoadMgrZr.deleteScanPalletType(dlo_detail_id);
	  				 } 
  	  				 if(queryInfo.a == QUERY_LOAD){ //处理Load的情况
	  					 handQueryByLoad(loadInfos,detailRow) ;
	  				 }else{  					 	//处理Order的情况
	  					 handQueryByOrder(loadInfos,detailRow);
	  				 }
	  				 ordersDetail.add("customer_id", customer_id);
					 ordersDetail.add("company_id", company_id);
					 DBRow orderSystem = floorOrderSystemMgr.findOrderSystemByNumberTypeInfos(detailRow.get("LR_ID", 0l));
					 if(orderSystem != null){
							ordersDetail.add("system_type",orderSystem.get("system_type", 0));
							ordersDetail.add("order_type", orderSystem.get("order_type", 0));
					 }
					 ordersDetail.add("load_info",loadInfos);
	  				 return ordersDetail ;
	  			}
			}
			return null ;
		}catch (Exception e) {
  			throw new SystemException(e,"refreshOrderInfos",log);
		}
 	
 	}
	/**
	 * 值返回当前Master_bol_no 对应的loadInfos
	 * @param master_bol_no
	 * @param loadInfos
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	private DBRow[] fixLoadInfosByMasterBol(String master_bol_no , DBRow[] loadInfos )throws Exception{
		try{
			List<DBRow> arrayList = new ArrayList<DBRow>();
			for(DBRow loadInfo :loadInfos){
				if(loadInfo.getString("master_bol_no").equals(master_bol_no)){
					arrayList.add(loadInfo);
					break ;
				}
			}
			return arrayList.toArray(new DBRow[0]) ;
		}catch (Exception e) {
  			throw new SystemException(e,"fixLoadInfosByMasterBol",log);
		}
	}
	private   void checkIsCanUpdate(long number , String customer_id , String company_id) throws Exception{
		try{
			if(number <= 0l  || StringUtil.isNull(customer_id) || StringUtil.isNull(company_id)){
				throw new SystemException();
			}
		}catch(Exception e){ 
  			throw new SystemException(e,"checkIsCanUpdate",log);
		}
	}
	/**
	 * 待测
	 */
	@Override
	public void loadConsolidate(final String from_pallet,final String to_pallet,final int reason ,final long dlo_detail_id,
			final long order_number ,final long adid ,final int current_pallets , 
			final int original_pallets ,final long LR_ID , final int order_type , final int  system_type ,
			final DBRow data , final AdminLoginBean adminLoggerBean) throws Exception {
 
			sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus txStat) {
		 			 try{
		 				 
						DBRow orderRow = wmsLoadMgrZr.getOrderByLRIDAndOrderNumbers(LR_ID, String.valueOf(order_number));
						if(orderRow == null){
							throw new SystemException();
						}
						/*DBRow entryDetail = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
						if(entryDetail != null){
						//更新他们的数据库
							String company_id = entryDetail.getString("company_id");
							String customer_id = entryDetail.getString("customer_id");
							//先注释掉
							checkIsCanUpdate(order_number, customer_id, company_id);
							//sqlServerMgrZJ.updateOrderPalletsByOrderNo(order_number, customer_id, company_id, original_pallets, current_pallets, adid, null);
						}*/
						//跟新我们的数据库
				 		int wms_pallet_type_count = 0 ;////consolidate 我们的数据就没有Pallet type
				 		long resources_id  = data.get("resources_id", 0l);
				 		int resources_type  = data.get("resources_type", 0);
				 		long scan_adid = adminLoggerBean.getAdid() ;
				 		
				 		wmsLoadMgrZr.addPalletTypeRow(dlo_detail_id, String.valueOf(order_number), order_type, system_type, "",
				 				wms_pallet_type_count, from_pallet, to_pallet, reason, 
				 				orderRow.get("wms_order_id", 0l) , CheckInMainDocumentsRelTypeKey.PICK_UP,LR_ID ,
				 				resources_id ,resources_type , scan_adid );
				 		
				 		//同时把这个order 上面的Pallet 数量-1 ， 如果有master_bol 也同样-1
				 		 
				 		DBRow orderPalletNumber = new DBRow();
				 		orderPalletNumber.add("pallets", current_pallets);
 				 		wmsLoadMgrZr.upateOrder(orderRow.get("wms_order_id", 0l), orderPalletNumber);
 				 		
 				 		long wms_master_bol_id = orderRow.get("master_bol_id", 0l);
 				 		if(wms_master_bol_id != 0l){
 				 			//更新MasterBol total_pallets
 				 			wmsLoadMgrZr.subtractMasterBolTotalPallet(wms_master_bol_id);
 				 		}
 				 		DBRow entryDetail = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
 				 		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
 				 		String note = "Order:"+order_number;
 				 		LoadConsolidateReasonKey loadConsolidateReasonKey = new LoadConsolidateReasonKey();
 				 		String noteData = note+","+orderRow.get("pallets", 0)+" --> "+current_pallets+" pallets"+","+loadConsolidateReasonKey.getValueOfKey(reason);
 				 		checkInMgrZwb.addCheckInLog(adid, note, null!=entryDetail?entryDetail.get("dlo_id", 0L):0L, (long)CheckInLogTypeKey.ANDROID_LOAD_CONSOLIDATE, DateUtil.NowStr(), noteData);
		 			 }catch(Exception e){
		 				throw new RuntimeException(e);
		 			 }	
				}
			});
	}
	@Override
	public void loadScanAndPalletTypeChange(DBRow data,	AdminLoginBean adminLoggerBean) throws UpdatePalletTypeFailedException , Exception {
		try{
			String cmd = data.getString("cmd");
			if(cmd.equalsIgnoreCase("load_one_pallet")){
				loadOnePallet(data,adminLoggerBean);
				return ;
			}
			if(cmd.equalsIgnoreCase("change_pro_no")){
				loadPalletUpdateProNo(data);
				return ;
			}
			if(cmd.equalsIgnoreCase("change_pallet_type")){
				loadPalletUpdatePalletType(data);
				return ;
			}
			
		}catch(UpdatePalletTypeFailedException e){
			throw e ;
		}catch(Exception e){
  			throw new SystemException(e,"loadScanAndPalletTypeChange",log);
		}
		
	}
	/**
	 * 添加一个Pallet type
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	private void loadOnePallet(DBRow data ,AdminLoginBean adminLoggerBean) throws Exception{
		
		long dlo_detail_id = data.get("dlo_detail_id", 0l);
		long order_no = data.get("order", 0l);
		String pallet_number = data.getString("pallet_number");
		String pallet_type = data.getString("pallet_type");
		String pro_no = data.getString("pro_no");			//只有最后一个Pallet的时候才会输入pro_no
		long LR_ID = data.get("LR_ID", 0l);
		int resources_type = data.get("resources_type", 0);
		long resources_id = data.get("resources_id", 0);
		long scan_adid = adminLoggerBean.getAdid() ;
		String company_id = data.getString("company_id");		
		
		int count = floorOrderSystemMgr.validatePalletNumnberRepeat(company_id,pallet_number);
		if(count==0){
			DBRow wmsOrder =  wmsLoadMgrZr.getOrderByLRIDAndOrderNumbers(LR_ID, String.valueOf(order_no));
			if(wmsOrder == null){
				throw new SystemException();
			}
			long wms_order_id = wmsOrder.get("wms_order_id",0l) ;
			if(!StringUtil.isNull(pro_no)){	//说明是最后一个Pallet 同时他输入了pro_no那么把这个数据记录到wms_order表中
				DBRow updateOrderRow = new DBRow();
				updateOrderRow.add("pro_no", pro_no);
				wmsLoadMgrZr.updateOrderStatus(wms_order_id, updateOrderRow);
			}
			int order_system = data.get("system_type", 0);
			int order_type = data.get("order_type", 0);
			
			
			int wms_pallet_type_count  = 1 ;
		    wmsLoadMgrZr.addPalletTypeRow(dlo_detail_id,String.valueOf(order_no), order_type,  
		    		order_system, pallet_type, wms_pallet_type_count,
		    		pallet_number, "", 0,wms_order_id,CheckInMainDocumentsRelTypeKey.PICK_UP,LR_ID,   resources_id ,   resources_type ,   scan_adid);
		}
		
	}
	/**
	 * 修改ProNo
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
 	private void loadPalletUpdateProNo(DBRow data) throws Exception {
		try{
			long dlo_detail_id = data.get("dlo_detail_id",0l);
			String pro_no = data.getString("pro_no");
			long order_no = data.get("order", 0l); 
			long LR_ID =  data.get("LR_ID", 0l);
			DBRow wmsOrder = wmsLoadMgrZr.getOrderByLRIDAndOrderNumbers(LR_ID,String.valueOf(order_no));
			if(wmsOrder == null){
				throw new SystemException();
			}
			DBRow updateRow = new DBRow();
			updateRow.add("pro_no", pro_no);
			wmsLoadMgrZr.updateOrderStatus(wmsOrder.get("wms_order_id", 0l), updateRow);
		}catch(Exception e){
  			throw new SystemException(e,"loadPalletUpdateProNo",log);
		}
	}
	/**
	 * 修改Pallet type
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
 	private void loadPalletUpdatePalletType(DBRow data) throws UpdatePalletTypeFailedException , Exception {
		try{
			String pallet_number = data.getString("pallet_number");
			String pallet_type = data.getString("pallet_type");
			long dlo_detail_id = data.get("dlo_detail_id",0l);
			long order_no = data.get("order", 0l); 
			long LR_ID = data.get("lr_id",0l);
			DBRow palletNumber =  wmsLoadMgrZr.getPalletByNumber(String.valueOf(order_no), dlo_detail_id, pallet_number);
			if(palletNumber != null){
				DBRow updateRow = new DBRow();
				updateRow.add("wms_pallet_type", pallet_type);
				wmsLoadMgrZr.updateWmsLoadOrderPalletType(palletNumber.get("wms_order_type_id", 0l), updateRow);
			}else{
				throw new UpdatePalletTypeFailedException();
			}
		}catch(UpdatePalletTypeFailedException e){
			throw e ;
		}catch(Exception e){
  			throw new SystemException(e,"loadPalletUpdatePalletType",log);
		}
	}
 	
 	
 	/**
 	 * 更新我们系统中的数据 order上写入pro_no, order Close 然后再 master_bol上Close
 	 * @param dlo_detail_id
 	 * @param master_bol_no
 	 * @param pro_no
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2014年9月16日
 	 */
 	public void closeSyncDetailInfo(long lr_id , String master_bol_no, String pro_no , DBRow detailRow) throws Exception{
 			try{
	 			//更新我们系统的数据
				/*
				 * DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
				if(detailRow == null){
					throw new SystemException();
				}*/
	  			int number_type = detailRow.get("number_type", 0) ;
				if(number_type == ModuleKey.CHECK_IN_LOAD){
					wmsLoadMgrZr.updateLoadOrderStatus(lr_id, master_bol_no , pro_no);
				}else{
					wmsLoadMgrZr.updateOrderByLrId(lr_id);
				}
				if(!StringUtil.isNull(pro_no)){ //Order 没有ProNo的那么用最后输入的为准
					wmsLoadMgrZr.updateOrderProNo(lr_id, master_bol_no, pro_no);
				}
 			}catch(Exception e){
 	  			throw new SystemException(e,"closeSyncDetailInfo",log);
 			}
			
			
 	}
	/**
	 * 1.关闭我们系统中的数据,同时也会关闭Wms系统中的数据,
	 * 2.我会同时写回order的status ， 和 Pro no
	 * 3.Load 以master_bol_为准 
	 *   a.同时也会更新这个master_bol 下面的Order (循环)
	 *   b.在master-bol 下面的Order 都更新完成过后，那么会更新这个master-bol的pro_no 和 status
	 *   c.应该是这个master_bol_no下面的所有的Order 都close完了，才会close , maseter_bol ,所以要先去掉Partially
	 * 4.order 和 po 
  	 * 	 a.那么只会更新Order的状态
  	 * 
  	 * 5.在更新成功过后，那么我们这这边保存一个值，表示已经成功更新Wms系统的中的数据。下次再次进来就不用更新了
  	 * 6.同时要注意是否只是更新Open状态的数据(zhangyanjie)
  	 * 
	 */
 	@Override
 	public void closeSyncBill(final long dlo_detail_id,final String master_bol_no ,final String pro_no , final long adid) throws Exception {/*
  		  
 		sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus txStat) {
	 			 try{
	 				//更新我们系统的数据
	 				DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
	 	  			if(detailRow == null){
	 	  				throw new SystemException();
	 	  			}
	 	  			String customer_id = detailRow.getString("customer_id");
 	   				String company_id = detailRow.getString("company_id");
	 	   			int number_type = detailRow.get("number_type", 0) ;
	 	   			
	 	  			if(number_type == ModuleKey.CHECK_IN_LOAD){
	 	  				wmsLoadMgrZr.updateLoadOrderStatus(dlo_detail_id, master_bol_no);
	 	  			}else{
	 	  				wmsLoadMgrZr.updateOrderByDetailId(dlo_detail_id);
	 	  			}
	 	  			if(!StringUtil.isNull(pro_no)){
	 	  				wmsLoadMgrZr.updateOrderProNo(dlo_detail_id, master_bol_no, pro_no);
	 	   			}
	 	  			
	 	  			DBRow updateRow = new DBRow();
 	  				updateRow.add("wms_is_close",1);
	 	  			//然后查询出Sync系统下面的Order，master_bol的信息去更新
	 	  			if(number_type == ModuleKey.CHECK_IN_LOAD){
	 	  				
	 	  				DBRow masterBolRow = wmsLoadMgrZr.getMasterBolByDetailIdAndMasterBolNo(dlo_detail_id, master_bol_no);
	 	  				if(masterBolRow != null){
		 	  				Long load_no = detailRow.get("number", 0l);
		 	   				Long masterBol = Long.parseLong(master_bol_no);
		 	   		
		 	   				if(masterBol >= 0l && load_no >= 0l){
		 	   					DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByEntryDetailId(dlo_detail_id,masterBolRow.get("wms_master_bol_id", 0l));
		 	   		 
		 	   					for(DBRow orderInfo : orderInfos){
		 	   						int wms_is_close = orderInfo.get("wms_is_close", 0);
		 	   						long order_no = orderInfo.get("order_numbers", 0l) ;
		 	   						if(wms_is_close <= 0 && order_no > 0l){ //说明没有更新,那么更新Order,pro_no status
		 	   							sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderInfo.getString("pro_no"));
		 	   							sqlServerMgrZJ.updateOrderPronoByOrderNo(orderNo, companyId, customerId, status, prono, shipDate, seals, vehicle, dockName, carrierName);
		 	   							//更新这个Order
		 	   							wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
		 	   						}
		 	   					}
		 	   					//更新master_bol_no
		 	   					int wms_is_close = masterBolRow.get("wms_is_close", 0);
		 	   					if(wms_is_close < 1){ //那么更新master_bol_no,pro_no,status 
		 	   						//sqlServerMgrZJ.updateMasterPronoByOrderNo(masterBol, company_id, customer_id, WmsOrderStatusKey.CLOSED,pro_no);
 		 	   						wmsLoadMgrZr.updateMasterBol(masterBolRow.get("wms_master_bol_id", 0l) , updateRow);  //更新这个MasterBol
		 	   					}
		 	   				 
	 	 	   				}
	 	  				}
	 	   			}else{ //order的情况
	 	   				DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByEntryDetailId(dlo_detail_id,0l);
	   					for(DBRow orderInfo : orderInfos){
	   						int wms_is_close = orderInfo.get("wms_is_close", 0);
 	   						long order_no = orderInfo.get("order_numbers", 0l) ;
	   						if(wms_is_close <= 0 && order_no > 0){ //说明没有更新,那么更新Order,pro_no status
 	   							//sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderInfo.getString("pro_no"));
	   							//更新这个Order
	   							wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
	   						}
	   					}
	 	   				
	 	   			}
	 	  			
			 		
	 			 }catch(Exception e){
		 			throw new RuntimeException(e);
	 			 }	
			}
		});
 	*/}
 	 
 	@Override
 	public boolean isLoadMasterBolAllClose(long lr_id) throws Exception {
 		boolean flag = true ;
 		DBRow[] masterBols = wmsLoadMgrZr.getMasterBolBy(lr_id);
 		if(masterBols != null && masterBols.length > 0){
 			for(DBRow master : masterBols){
 				if(!master.getString("status").equalsIgnoreCase(WmsOrderStatusKey.CLOSED)){
 					flag = false ;
 					break ;
 				}
 			}
 		}
  		return flag;
 	}
	@Override
	public boolean isCloseMasteOrOrder(long lr_id, String master_bol_no)
			throws Exception {
		boolean returnFlag = false ;
		try{
			//这种需要查看这个Master_bol 是否关闭了
  			DBRow masterBolRow = wmsLoadMgrZr.getMasterBolByLrIdAndMasterBolNo(lr_id, master_bol_no);
  			String status = masterBolRow.getString("status");
  			if(masterBolRow != null && status.equalsIgnoreCase(WmsOrderStatusKey.CLOSED)  ){
  				returnFlag = true ;
  			}
  			return returnFlag ;
		}catch(Exception e){
  			throw new SystemException(e,"isCloseMasteOrOrder",log);
		}
	}
	/**
	 * 通过扫描的 pallet  + order 总共的Pallet计算Order是否扫描完成了
	 * @param wms_order_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年2月13日
	 */
	private boolean isOrderScanedOver(DBRow wmsOrderInfo) throws Exception{
		try{
			//floorCheckInMgrZr.get
			long wms_order_id =  wmsOrderInfo.get("wms_order_id", 0l);
			int count =	floorCheckInMgrZwb.countWmsLoadIdScanedPallet(wms_order_id);
			// only work for load exception, if pallets is 0, the pallet should be consolidated to the other order
			if(count == wmsOrderInfo.get("pallets", 0) && wmsOrderInfo.get("pallets", 0) != 0 ){
				return true ;
			}
			return false ;
		}catch(Exception e){
  			throw new SystemException(e,"isOrderScanedOver",log);
		}
	}
 
	//order 上面是否有seal 
	@Override
	public void closeWmsMasterBol(DBRow detailRow, String pro_no ,  long adid , String shipDate , String seals ,String vehicle,String dockName,String carrierName , String userName , int closeType)	throws Exception {
 		try{
  				DBRow updateRow = new DBRow();
				updateRow.add("wms_is_close",1);
				long lr_id = detailRow.get("lr_id", 0l);
 				DBRow[] masterBolRows = wmsLoadMgrZr.getMasterBolBy(lr_id);
				String company_id = detailRow.getString("company_id");
				String customer_id = detailRow.getString("customer_id");
				for(DBRow masterBol : masterBolRows){
					Long masterBolNo = masterBol.get("master_bol_no", 0l);
					String masterProNo = masterBol.getString("pro_no");
   					int wms_is_close_master = masterBol.get("wms_is_close", 0);
 	  				if(masterBolNo >  0l  && wms_is_close_master <= 0 ){
 	  					 //如果有问题注释掉 1066-1117 ， 打开 1124-1148
 	  					if(closeType == CheckInChildDocumentsStatusTypeKey.EXCEPTION){
 	  						
 	  						DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByLrId(lr_id,masterBol.get("wms_master_bol_id", 0l));
 		   					int closeCount = 0 ;  //closedOrderCount 
 		   					for(DBRow orderInfo : orderInfos){
 		   						String orderProNo = orderInfo.getString("pro_no");
 		   						int wms_is_close = orderInfo.get("wms_is_close", 0);
 		   						long order_no = orderInfo.get("order_numbers", 0l) ;
 	 	   						if(order_no > 0l /*&& wms_is_close <= 0*/ &&   isOrderScanedOver(orderInfo)  ){ 
 		   							//sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderInfo.getString("pro_no"));
 		   							checkIsCanUpdate(order_no, customer_id, company_id);
 		   							sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
 		   							//更新是否有Consolidate的Order pallet number
 		   							updateOrderPalletNumber(customer_id, company_id, order_no, orderInfo.get("wms_order_id", 0l) , orderInfo.get("pallets", 0), adid);
 		   							//更新这个Order
 		   							wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
 		   							closeCount ++ ;
 	 	   						}
 		   					}
 		   					//更新master_bol_no
 							checkIsCanUpdate(masterBolNo, customer_id, company_id);
 							if(orderInfos.length == closeCount){
 								sqlServerMgrZJ.updateMasterPronoByOrderNo(masterBolNo, company_id, customer_id,  WmsOrderStatusKey.CLOSED, masterProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
 								wmsLoadMgrZr.updateMasterBol(masterBol.get("wms_master_bol_id", 0l) , updateRow);  //更新这个MasterBol
 							}
 							
 	  					}else {
 	  						
 	  						DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByLrId(lr_id,masterBol.get("wms_master_bol_id", 0l));
  		   					for(DBRow orderInfo : orderInfos){
 		   						String orderProNo = orderInfo.getString("pro_no");
 		   						int wms_is_close = orderInfo.get("wms_is_close", 0);
 		   						long order_no = orderInfo.get("order_numbers", 0l) ;
 		   						
 	 	   						
 	 	   						if(order_no > 0l /*&& wms_is_close <= 0*/ ){ //zhangrui update 2015-03-04
 		   							//sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderInfo.getString("pro_no"));
 		   							checkIsCanUpdate(order_no, customer_id, company_id);
 		   							sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
 		   							//更新是否有Consolidate的Order pallet number
 		   							updateOrderPalletNumber(customer_id, company_id, order_no, orderInfo.get("wms_order_id", 0l) , orderInfo.get("pallets", 0), adid);
 		   							//更新这个Order
 		   							wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
 	 	   						}
 		   					}
 		   					//更新master_bol_no
 							checkIsCanUpdate(masterBolNo, customer_id, company_id);
 							sqlServerMgrZJ.updateMasterPronoByOrderNo(masterBolNo, company_id, customer_id,  WmsOrderStatusKey.CLOSED, masterProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
 							wmsLoadMgrZr.updateMasterBol(masterBol.get("wms_master_bol_id", 0l) , updateRow);  //更新这个MasterBol
 							 
  	  					}
 	  					
 	  					
 	  					
 	  					
 	  					
 	  					
	   					/*DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByLrId(lr_id,masterBol.get("wms_master_bol_id", 0l));
	   					int closeCount = 0 ;  //closedOrderCount 
	   					for(DBRow orderInfo : orderInfos){
	   						String orderProNo = orderInfo.getString("pro_no");
	   						int wms_is_close = orderInfo.get("wms_is_close", 0);
	   						long order_no = orderInfo.get("order_numbers", 0l) ;
	   						
 	   						
 	   						if(order_no > 0l && wms_is_close <= 0 &&   isOrderScanedOver(orderInfo)  ){ 
	   							//sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderInfo.getString("pro_no"));
	   							checkIsCanUpdate(order_no, customer_id, company_id);
	   							sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
	   							//更新是否有Consolidate的Order pallet number
	   							updateOrderPalletNumber(customer_id, company_id, order_no, orderInfo.get("wms_order_id", 0l) , orderInfo.get("pallets", 0), adid);
	   							//更新这个Order
	   							wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
	   							closeCount ++ ;
 	   						}
	   					}
	   					//更新master_bol_no
						checkIsCanUpdate(masterBolNo, customer_id, company_id);
						if(orderInfos.length == closeCount){
							sqlServerMgrZJ.updateMasterPronoByOrderNo(masterBolNo, company_id, customer_id,  WmsOrderStatusKey.CLOSED, pro_no, shipDate, seals, vehicle, dockName, carrierName,userName);
							wmsLoadMgrZr.updateMasterBol(masterBol.get("wms_master_bol_id", 0l) , updateRow);  //更新这个MasterBol
						}*/
					
 	  				
 	  				}
				}
 			
 		}catch(Exception e){
  			throw new SystemException(e,"closeWmsLoad",log);
 		}
	}


	@Override
	public void closeWmsOrder(DBRow detailRow, String pro_no, long adid , String shipDate , String seals ,String vehicle,String dockName,String carrierName , String userName) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("wms_is_close",1);
			long lr_id = detailRow.get("lr_id", 0l);
			String company_id = detailRow.getString("company_id");
			String customer_id = detailRow.getString("customer_id");
			DBRow[] orderInfos =  wmsLoadMgrZr.getOrderInfoByLrId(lr_id,0l);
			for(DBRow orderInfo : orderInfos){
				int wms_is_close = orderInfo.get("wms_is_close", 0);
				String orderProNo = orderInfo.getString("pro_no");
				long order_no = orderInfo.get("order_numbers", 0l) ;
				//if(wms_is_close <= 0 && order_no > 0){ //说明没有更新,那么更新Order,pro_no status
				if(order_no > 0){ 
					checkIsCanUpdate(order_no, customer_id, company_id);
					sqlServerMgrZJ.updateOrderPronoByOrderNo(order_no, company_id, customer_id, WmsOrderStatusKey.CLOSED, orderProNo, shipDate, seals, vehicle, dockName, carrierName,userName);
					//更新这个Order
					updateOrderPalletNumber(customer_id, company_id, order_no, orderInfo.get("wms_order_id", 0l) , orderInfo.get("pallets", 0), adid);
					wmsLoadMgrZr.upateOrder(orderInfo.get("wms_order_id", 0l) , updateRow);
				}
			}
		}catch(Exception e){
  			throw new SystemException(e,"closeWmsOrder",log);
		}
	}


	
	/**
	 * DBRow
	  {
		number:
		number_type:
		datas[
			{
			   master : 2323
			   orders:{
			   	order_no :
			   	pallet:[
			   		{},{}
			   	]
			   } 
		    },
		    {
		    	
		    }
		 ]	
	 }
	 */
/*	@Override
	public DBRow getPalletInfoByDetailId(long dlo_detail_id) throws Exception {
		try{
			DBRow returnRow = new DBRow();
			DBRow detailRow = floorCheckInMgrZwb.selectDetailByDetailId(dlo_detail_id);
			if(detailRow != null){
				String number = detailRow.getString("number") ;
				int number_type = detailRow.get("number_type", 0);
				returnRow.add("number", number);
				returnRow.add("number_type",number_type );
				if(number_type == ModuleKey.CHECK_IN_LOAD){
					addMasterBolInfo(returnRow, dlo_detail_id);
				}else{
					addOrderInfo(returnRow, dlo_detail_id);
				}
				
			}
			
			
			return returnRow ;
		}catch(Exception e){
  			throw new SystemException(e,"getPalletInfoByDetailId",log);
		}
	}*/
	/**
	 * 
	 * 
	 * @author zhangrui
	 * @Date   2014年9月21日
	 */
	/*private void addOrderInfo(DBRow returnRow , long dlo_detail_id) throws Exception {
		try{
			DBRow masterRow = new DBRow();
			DBRow[] masterBols = new DBRow[1];
			masterBols[0] = masterRow ;
			masterRow.add("master", "0");
			masterRow.add("orders", addOrderAndPalletInfo(dlo_detail_id, "0"));
			returnRow.add("datas", masterBols);
		}catch(Exception e){
  			throw new SystemException(e,"addOrderInfo",log);
		}
	}*/
	/**
	 * 添加Load 进来的信息
	 * @param returnRow
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月21日
	 */
	/*private void addMasterBolInfo(DBRow returnRow , long dlo_detail_id) throws Exception {
		try{
		//	DBRow[] masterBolInfos = wmsLoadMgrZr.getMasterBolBy(dlo_detail_id);
			//其他方法会用这个顾使用新的方法
			DBRow[] masterBolInfos = wmsLoadMgrZr.getMasterBolInfoByDetailId(dlo_detail_id);
			if(masterBolInfos != null){
				DBRow[] datas = new DBRow[masterBolInfos.length];
				for(int index = 0 , count = datas.length ; index < count ; index++ ){
					DBRow masterBol = masterBolInfos[index] ;
					String masterBolNo = masterBol.getString("master_bol_no");
					DBRow insertMasterBol = new DBRow();
					insertMasterBol.add("master", masterBolNo);
					insertMasterBol.add("pro_no", masterBol.getString("pro_no"));
					insertMasterBol.add("total_pallets", masterBol.get("total_pallets", 0));
					insertMasterBol.add("orders",addOrderAndPalletInfo(dlo_detail_id , masterBolNo));
					datas[index] = insertMasterBol ;
				}
				returnRow.add("datas", datas);
			}
		
		}catch(Exception e){
  			throw new SystemException(e,"addMasterBolInfo",log);
		}
	}*/
/*	private DBRow[] addOrderAndPalletInfo( long dlo_detail_id , String masterBolNo) throws Exception{
		try{
			DBRow[] insertOrders = null ;
	//		DBRow[] orders = wmsLoadMgrZr.getOrderInfoByDetailIdAndMasterBol(dlo_detail_id, masterBolNo);
			//其他的方法也会用到这个顾使用新得方法 
			DBRow[] orders = wmsLoadMgrZr.getOrderByDetailAndMasterBol(dlo_detail_id, masterBolNo);
			if(orders != null && orders.length > 0){
				insertOrders = new DBRow[orders.length];
				for(int index = 0 , count = orders.length ; index < count ;index++){
					DBRow inserOrder = new DBRow();
					inserOrder.add("order_no", orders[index].getString("order_numbers"));
					inserOrder.add("reference_no", orders[index].getString("reference_no"));
					inserOrder.add("po_no", orders[index].getString("po_no"));
					inserOrder.add("pro_no", orders[index].getString("pro_no"));
					inserOrder.add("pallets", orders[index].get("total_pallets", 0));
					
					long wms_order_id = orders[index].get("wms_order_id", 0l);
					DBRow[] palletInfo = wmsLoadMgrZr.getOrderPalletInfoByWmsOrderId(dlo_detail_id,wms_order_id);
					fixPalletInfo(palletInfo);
					inserOrder.add("pallet",palletInfo);
					insertOrders[index] = inserOrder;
				}
 			}
			return insertOrders ;
		}catch(Exception e){
  			throw new SystemException(e,"addOrderAndPalletInfo",log);
		}
	}*/
	
	private void fixPalletInfo(DBRow[] palletInfos){
		if(palletInfos != null && palletInfos.length > 0){
			for(DBRow pallet : palletInfos){
				pallet.remove("wms_order_id");
				pallet.remove("wms_order_type_id");
			}
		}
	}
	@Override
	public DBRow getConsolidatePalletType(String palletNumber, List<DBRow> consolidateRows) throws Exception {
		try{
			if(consolidateRows != null && consolidateRows.size() > 0){
				for(DBRow row : consolidateRows){
					if(palletNumber.equals(row.getString("wms_consolidate_pallet_number"))){
						return row ;
					}
				}
			}
			return null ;
		}catch(Exception e){
  			throw new SystemException(e,"getConsolidatePalletType",log);
		}
	}
	private void updateOrderPalletNumber(String customer_id , String company_id , long order_no , long wms_order_id , int currentPallet,long adid) throws Exception{
		try{
			DBRow row = wmsLoadMgrZr.getWmsOrderConsolidateNumber(wms_order_id);
			if(row != null && row.get("sum", 0) > 0){
				int sum = row.get("sum", 0) ;
				if(sum > 0){
					int or = sum + currentPallet ;
					sqlServerMgrZJ.updateOrderPalletsByOrderNo(order_no, customer_id, company_id, or, currentPallet, adid, null);
				}
			}
		}catch(Exception e){
  			throw new SystemException(e,"updateOrderPalletNumber",log);

		}
	}
	/**
	 * android 打印的功能
	 *  
	 * 打印的功能是有可能发生在单据的任何时刻
	 * 	1.有可能是关闭之后	（detail表中有记录DoorId）
	 *  2.有可能是windowCheckIn之后 (关联表中有记录,Detail表中)
	 *  3.没有添加过滤 一定是某个equipment
	 * 因为在LoadTick上，是要显示门的信息
	 *  所以我只能是在如果是在停车位上面的任务/没有门的信息，我任务他的门是NA
	 *  
	 *  (如果在Detail表中有 门的信息?那么用detail中的为准 :(用关联关系?关联关系:NA))
	 * @param result
	 * @param entry_id
	 * @param adminLoginBean
	 * @throws CheckInNotFoundException
	 * @throws NoPermiessionEntryIdException
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月2日
	 */
	@Override
	public void getEntryPrint(DBRow result ,long entry_id , AdminLoginBean adminLoginBean) throws CheckInNotFoundException ,NoPermiessionEntryIdException , Exception {
		DBRow entryRow  = floorCheckInMgrZwb.getEntryIdById(entry_id);
		if(entryRow == null){
			return ; 
		}
 		CheckInEntryPermissionUtil.checkEntry(entryRow, adminLoginBean);
 		
		DBRow[] details =  floorSpaceResourcesRelationMgr.getTaskByEntryIdWithOccpuyStatus(entry_id, 0, 0l);// floorCheckInMgrZwb.findOccupancyDetails(entry_id);
		
		DBRow[] loadNumbers = fixPrintData(details);
		 
		DBRow append = new DBRow();
		append.add("entry_id", entry_id);
 		append.add("details", loadNumbers);
		result.add("entry", append);
		
 	}
	/**
	 * (如果在Detail表中有 门的信息?那么用detail中的为准 :(用关联关系?关联关系:NA))
	 * 有可能关闭了，也有可能是在关联关系中
	 * <resources_id,resources_type,resources_name>
	 * @param temp
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月2日
	 */
	private HoldThirdValue<Long, Integer, String> fixPrintResourcesName(DBRow temp) throws Exception{
		try{
			int occupancy_type = temp.get("occupancy_type", 0);
			long rl_id = temp.get("rl_id", 0l);
			int resources_type =(occupancy_type == 0 ? temp.get("RESOURCES_TYPE", 0) : occupancy_type);
			long resources_id = (rl_id == 0 ? temp.get("RESOURCES_ID", 0l)   : rl_id);
			if(resources_type == OccupyTypeKey.DOOR && resources_id != 0l){
				return new HoldThirdValue<Long, Integer, String>( resources_id,resources_type, getDoorName(resources_id));
			}
			if(resources_type ==  OccupyTypeKey.SPOT && resources_id > 0l){
				return new HoldThirdValue<Long, Integer, String>( resources_id,resources_type,getSpotName(resources_id));
			}
			return new HoldThirdValue<Long, Integer, String>(0l, resources_type, "NA");
		}catch(Exception e){
  			throw new SystemException(e,"fixPrintDoorName",log);
		}
	
		
	}
	private DBRow[] fixPrintData(DBRow[] details) throws Exception{
		try{
		 
			
			CheckInChildDocumentsStatusTypeKey typeKey = new CheckInChildDocumentsStatusTypeKey();
			List<DBRow> arrayList = new ArrayList<DBRow>();
			Map<HoldThirdValue<Long,Integer,String>, List<DBRow>> maps = new HashMap<HoldThirdValue<Long,Integer,String>, List<DBRow>>();
			
			Map<Long,HoldThirdValue<String, String,String>> equipmentSeals = new HashMap<Long, HoldThirdValue<String,String,String>>(); 
			CheckInMgrIfaceZr checkInMgrZr  = (CheckInMgrIfaceZr) MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");
			if(details != null && details.length > 0 ){
				for(DBRow row : details){
					
					String number = row.getString("number");
					int numberType = row.get("number_type", 0);
					
	 					 
					DBRow insertRow = new DBRow();
					HoldThirdValue<Long, Integer, String> resourcesIdAndTypeAndName =  fixPrintResourcesName(row);
					List<DBRow> innerLoadNumbers =	maps.get(resourcesIdAndTypeAndName);
					long equipment_id = row.get("equipment_id", 0l);
					
					innerLoadNumbers =  innerLoadNumbers == null ? new ArrayList<DBRow>() : innerLoadNumbers ; 
					insertRow.add("number", number);
					insertRow.add("status", typeKey.getContainerTypeKeyValue(row.get("number_status", -1)));
					insertRow.add("status_int", row.get("number_status", -1));
					
					//徐佳升级过后执行的东西
					insertRow.add("customer_id", row.getString("customer_id"));
					insertRow.add("company_id", row.getString("company_id"));
					insertRow.add("number_type", row.get("number_type", 0));
					insertRow.add("account_id", row.getString("account_id"));
					insertRow.add("dlo_detail_id", row.get("dlo_detail_id", 0l));
					insertRow.add("ic_id", row.get("ic_id", 0l));			//如果是Ic_id那么就是三星的货
					insertRow.add("receipt_no", row.get("receipt_no", 0L));
					//zhangrui 2014-12-15 添加每一个Task的equipment的seal
					
					HoldThirdValue<String, String,String> inOrOutSeals = equipmentSeals.get("equipment_id") ;
					if(inOrOutSeals == null ){
						DBRow  equipmentRowSeals = checkInMgrZr.getEquipmentSeals(equipment_id);
						String tempOutSeals = equipmentRowSeals.getString("out_seal") ;
						String tempInSeals = equipmentRowSeals.getString("in_seal") ;
						tempOutSeals = StringUtil.isBlank(tempOutSeals)? "NA" : tempOutSeals ;
						tempInSeals = StringUtil.isBlank(tempInSeals)? "NA" : tempInSeals ;
						HoldThirdValue<String, String,String> mapValue = new HoldThirdValue<String, String,String>(tempInSeals, tempOutSeals,equipmentRowSeals.getString("equipment_number")) ;
						equipmentSeals.put(equipment_id, mapValue ) ;
						inOrOutSeals = mapValue  ;
					}
					insertRow.add("out_seal", inOrOutSeals.b);
					insertRow.add("in_seal", inOrOutSeals.a);
					insertRow.add("equipment_number", inOrOutSeals.c);
					insertRow.add("appointment_date", row.getString("appointment_date"));

 					if(numberType == ModuleKey.CHECK_IN_ORDER){
						insertRow.add("append_number", row.getString("po_no"));
						insertRow.add("append_type", ModuleKey.CHECK_IN_PONO);
					}
					if(numberType == ModuleKey.CHECK_IN_PONO){
						insertRow.add("append_number", row.getString("order_no"));
						insertRow.add("append_type", ModuleKey.CHECK_IN_ORDER);
					}
					
					innerLoadNumbers.add(insertRow);
					maps.put(resourcesIdAndTypeAndName, innerLoadNumbers);
				 
				}
			}
			Iterator<Entry<HoldThirdValue<Long, Integer, String>, List<DBRow>>> it =	maps.entrySet().iterator();
			while(it.hasNext()){
				 Entry<HoldThirdValue<Long, Integer, String>, List<DBRow>> entry = it.next();
				 DBRow inner = new DBRow();
				 inner.add("loads", entry.getValue().toArray(new DBRow[0]));
				 inner.add("resources_name", entry.getKey().c);
				 inner.add("resources_type", entry.getKey().b);
				 inner.add("resources_id", entry.getKey().a);

				 arrayList.add(inner);
			}
			return arrayList.toArray(new DBRow[0]);
		}catch(Exception e){
  			throw new SystemException(e,"fixPrintData",log);
		}
	}
	
	
	@Override
	public void loadConfirmSubmitPhoto(DBRow data, AdminLoginBean adminLoginBean) throws Exception {
		try{
			long dlo_detail_id = data.get("dlo_detail_id", 0l);
  
			long entry_id =  data.get("entry_id", 0l);
			DBRow fileRow = new DBRow();
			fileRow.add("file_with_id", entry_id);
			fileRow.add("file_with_detail_id", dlo_detail_id);
			fileRow.add("file_with_class",  FileWithCheckInClassKey.PhotoTaskProcessing);
			fileRow.add("file_with_type", FileWithTypeKey.OCCUPANCY_MAIN);
		 
			fileMgrZr.androidUpZipPictrue(fileRow, adminLoginBean) ; 
		}catch(Exception e){
  			throw new SystemException(e,"loadConfirmSubmitPhoto",log);
		}
 		
	}	
	
	@Override
	public void updateDetailNeedHelp(long dlo_detail_id, int flag_help)
			throws Exception {
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("flag_help", flag_help);
			floorCheckInMgrZwb.updateDetailByIsExist(dlo_detail_id, updateRow);
		}catch(Exception e){
  			throw new SystemException(e,"updateDetailNeedHelp",log);
		}
 		
	}
	@Override
	public boolean isEntryDetailHasProcessing(long dlo_id) throws Exception {
		try{
			if(dlo_id > 0l){	
				return floorCheckInMgrZwb.isEntryDetailHasProcessing(dlo_id);
			}
			return false;
		}catch(Exception e){
  			throw new SystemException(e,"isEntryDetailHasProcessing",log);
		}
	}
	@Override
	public int deletePalletNo(long dlo_detail_id, String order_number,
			String pallet_no) throws Exception {
		try{
			return wmsLoadMgrZr.deletePalletBy(order_number, dlo_detail_id, pallet_no);
		}catch(Exception e){
  			throw new SystemException(e,"deletePalletNo",log);
		}
 	}
	

	
	/**
	 * 将wms_load_order信息补全
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午2:37:27
	 */
	public void updateWmsLoadOrderSupplierAddress(long start, long end) throws Exception
	{
		DBRow[] orders = wmsLoadMgrZr.findWmsLoadOrders(start, end);
		for (int i = 0; i < orders.length; i++)
		{
			DBRow entryTask = floorCheckInMgrZwb.selectDetailByDetailId(orders[i].get("dlo_detail_id", 0L));
			String companyId = null!=entryTask?entryTask.getString("company_id"):"";
			String supplierIds = sqlServerMgrZJ.findSupplierIdsByOrderNoCompanyID(orders[i].get("order_numbers", 0L), companyId);
			DBRow[] orderInfos = sqlServerMgrZJ.findOrderByOrderNo(orders[i].get("order_numbers", 0L), companyId);
			if(orderInfos.length > 0)
			{
				DBRow orderInfo = orderInfos[0];
				
				DBRow insertOrderRow = new DBRow();
				insertOrderRow.add("company_id", companyId);
				insertOrderRow.add("account_id", orderInfo.getString("AccountID"));
				insertOrderRow.add("ship_to_id", orderInfo.getString("ShipToID"));
				insertOrderRow.add("ship_to_name", orderInfo.getString("ShipToName"));
				insertOrderRow.add("ship_to_address1", orderInfo.getString("ShipToAddress1"));
				insertOrderRow.add("ship_to_address2", orderInfo.getString("ShipToAddress2"));
				insertOrderRow.add("ship_to_city", orderInfo.getString("ShipToCity"));
				insertOrderRow.add("ship_to_state", orderInfo.getString("ShipToState"));
				insertOrderRow.add("ship_to_zip_code", orderInfo.getString("ShipToZipCode"));
				insertOrderRow.add("ship_to_country", orderInfo.getString("ShipToCountry"));
				insertOrderRow.add("ship_to_contact", orderInfo.getString("ShipToContact"));
				insertOrderRow.add("ship_to_phone", orderInfo.getString("ShipToPhone"));
				insertOrderRow.add("ship_to_extension", orderInfo.getString("ShipToExtension"));
				insertOrderRow.add("ship_to_fax", orderInfo.getString("ShipToFax"));
				insertOrderRow.add("ship_to_store_no", orderInfo.getString("ShipToStoreNo"));
				insertOrderRow.add("supplier_id", supplierIds);
				wmsLoadMgrZr.updateOrderStatus(orders[i].get("wms_order_id", 0L), insertOrderRow);
			}
		}
 	}
	
	
	
	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

	public void setWmsLoadMgrZr(WmsLoadMgrZrIface wmsLoadMgrZr) {
		this.wmsLoadMgrZr = wmsLoadMgrZr;
	}


	public void setSqlServerTransactionTemplate(
			TransactionTemplate sqlServerTransactionTemplate) {
		this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
	}


	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}


	 


	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}


	public void setFloorOrderSystemMgr(FloorOrderSystemMgr floorOrderSystemMgr) {
		this.floorOrderSystemMgr = floorOrderSystemMgr;
	}


	public void setFloorCheckInMgrZr(FloorCheckInMgrZr floorCheckInMgrZr) {
		this.floorCheckInMgrZr = floorCheckInMgrZr;
	}



	

}
