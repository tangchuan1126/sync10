package com.cwc.app.api.zr;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnReceiveEmailPageString;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.floor.api.zyj.FloorReturnProductOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zr.ReturnProductMgrIfaceZr;
import com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ReturnProductCheckItemTypeKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductLogTypeKey;
import com.cwc.app.key.ReturnProductPictureRecognitionkey;
import com.cwc.app.key.ReturnTypeKey;
import com.cwc.app.key.ServiceOrderStatusKey;
import com.cwc.app.key.StorageReturnProductTypeKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.lucene.zyj.ReturnProductOrderIndexMgrZyj;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class ReturnProductMgrZr implements ReturnProductMgrIfaceZr
{
	
	private static Logger log = Logger.getLogger("ACTION");
 
	private FloorReturnProductMgrZr floorReturnProductMgrZr ;
	private AdminMgrIFace adminMgr ;
	private FloorProductMgr floorProductMgr;
	private FileMgrIfaceZr fileMgrZr ;
	private FloorServiceOrderMgrZyj floorServiceOrderMgrZyj;
	private FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj;
	private ProductMgr productMgr;
	private FloorOrderMgr floorOrderMgr;
	private FloorCatalogMgr floorCatalogMgr;
	
	
	@Override
	public long addReturnProductMgrZr(DBRow row) throws Exception
	{
		try{
			return floorReturnProductMgrZr.addReturnProduct(row);
		}catch (Exception e) {
			throw new SystemException(e, "addReturnProductMgrZr", log);
		}
	}



	@Override
	public long addReturnProductMgrZr(HttpServletRequest request)
			throws Exception
	{
		try {
			long rp_id = 0l;
			 DBRow data = new DBRow() ;
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 long adid = adminLoggerBean.getAdid(); 
			
			 data.add("create_user", adid);
			 data.add("create_date", DateUtil.DatetimetoStr(new Date()));
			 data.add("status", ReturnProductKey.WAITING);//StorageReturnProductStatusKey.NONE_FINISH  表示的等待处理
			 
			 data.add("oid", 0);	//目前没有关联订单
			 
			 String note = StringUtil.getString(request, "note");
			 String first_name = StringUtil.getString(request, "first_name");
			 String last_name = StringUtil.getString(request, "last_name");
			 
			 String address_country = StringUtil.getString(request, "address_country");
			 String address_city = StringUtil.getString(request, "address_city");
			 String address_state = StringUtil.getString(request, "address_state");
			 String address_zip = StringUtil.getString(request, "address_zip");
			 String address_street = StringUtil.getString(request, "address_street");
			 String address_country_code = StringUtil.getString(request, "address_country_code");
			 String tel = StringUtil.getString(request, "tel");
			 // add 2013-5-17
			 String return_product_type = StringUtil.getString(request, "return_product_type");
			 String tracking_num = StringUtil.getString(request,"tracking_num");
			 String rma = StringUtil.getString(request, "rma");
			 String express_company = StringUtil.getString(request, "express_company");
			 //add return_product_source
			 String return_product_source = StringUtil.getString(request, "return_product_source");
			 
			 long ps_id = StringUtil.getLong(request, "ps_id");
			 //address 修改
			 
			 long ccid = StringUtil.getLong(request, "ccid");
			 long pro_id = StringUtil.getLong(request, "pro_id");
			 
			 data.add("note", note);
			 data.add("first_name", first_name);
			 data.add("last_name", last_name);
			 data.add("address_street", address_street);
			 data.add("address_country", address_country);
			 data.add("address_city", address_city);
			 data.add("address_state", address_state);
			 data.add("address_zip", address_zip);
			 data.add("address_country_code", address_country_code);
			 data.add("tel", tel);
			 
			 
			 data.add("return_product_type", return_product_type);
			 data.add("tracking_num", tracking_num);
			 data.add("rma", rma);
			 data.add("express_company_", express_company);
			 
			 data.add("return_product_source", return_product_source);
			 data.add("is_print_over", 0);
			 
			 data.add("ccid", ccid);
			 data.add("pro_id", pro_id);
			 data.add("gather_picture_status", 0);//图片采集未完成
			 data.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionNoneFinish);
			 data.add("return_product_check", ReturnProductCheckItemTypeKey.OUTCHECK);
			 data.add("create_type", 1); //仓库创建1,客服创建的2(退货还未到1,退货已到2)
			 data.add("ps_id", ps_id);
			 
			 data.add("is_need_return", ReturnTypeKey.NEED);
			 
			 rp_id = addReturnProductMgrZr(data);
			 DBRow productStoreCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(ps_id);
			 //增加退货单日志
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id, 0,0,0,ReturnProductKey.WAITING, "创建退货单:["+rp_id+"],"+new ReturnTypeKey().getReturnTypeKeyValue(ReturnTypeKey.NEED)+",退货仓库:"+(null!=productStoreCatalog?productStoreCatalog.getString("title"):""), ReturnProductLogTypeKey.CREATE, adid, DateUtil.NowStr());
			 ReturnProductOrderIndexMgrZyj.getInstance().addIndex(rp_id);
			 
			 //处理文件。把文件移动到对应的文件和表中
			 String file_names = StringUtil.getString(request, "file_names");
			 if( Integer.parseInt(return_product_type) == StorageReturnProductTypeKey.HAS_PACK && file_names.length() > 0 ){
				 String[] arrayFile = file_names.split(",");
				 String sn  = StringUtil.getString(request, "sn");
				 String file_with_type = StringUtil.getString(request, "file_with_type");
				 String path = StringUtil.getString(request, "path");
				 String baseUrl = Environment.getHome()+"upl_imags_tmp/";
				 for(String tempFileName : arrayFile){
					 DBRow tempFile = new DBRow();
					 tempFile.add("file_with_id", rp_id);
					 tempFile.add("file_with_type", file_with_type);
					 tempFile.add("upload_adid", adid);
					 tempFile.add("upload_time", DateUtil.DatetimetoStr(new Date()));
					 StringBuffer realyFileName = new StringBuffer(sn);
 					 String suffix = StringUtil.getSuffix(tempFileName);
 					 realyFileName.append("_").append(rp_id).append("_").append(tempFileName.replace("."+suffix, ""));
 					 int indexFile = fileMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
 					 if(indexFile != 0){
 						realyFileName.append("_").append(indexFile);
 					 }
	 					
	 				 realyFileName.append(".").append(suffix);
					 tempFile.add("file_name",  realyFileName.toString());
					 fileMgrZr.saveFile(tempFile);
					 FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
				 }
				 	
				 
			
			 }
			 return rp_id;
		} catch (Exception e) {
			throw new SystemException(e, "addReturnProductMgrZr", log);
		}
		 
		
		
	}



	@Override
	public DBRow[] getReturnProductBypage(PageCtrl pc) throws Exception
	{
		 try{
			 return floorReturnProductMgrZr.getReturnProductBypage( pc);
		 }catch (Exception e) {
			 throw new SystemException(e, "getReturnProductByPage", log);
		}
	}





	@Override
	public void updateReturnProduct(DBRow row,long rp_id) throws Exception
	{
		 try{
			   floorReturnProductMgrZr.updateReturnProduct(row, rp_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getReturnProductByPage", log);
		}
	}





	@Override
	public void updateReturnProductRelationOver(HttpServletRequest request)
			throws Exception
	{
		try{
			long rp_id = StringUtil.getLong(request, "rp_id");
			DBRow row = new DBRow() ;
			row.add("status",ReturnProductKey.WAITING);//StorageReturnProductStatusKey.NONE_FINISH
			floorReturnProductMgrZr.updateReturnProduct(row, rp_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductRelationOver", log);
		}
	}





	@Override
	public void updateReturnProductPrintLabelOver(HttpServletRequest request) throws Exception {
		try{
			long rp_id = StringUtil.getLong(request, "rp_id");
			DBRow row = new DBRow() ;
			row.add("is_print_over","1");
			floorReturnProductMgrZr.updateReturnProduct(row, rp_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "updateReturnProductPrintLabelOver", log);
		}
		
	}





	@Override
	public DBRow getReturnProductByRpId(long rp_id) throws Exception {
		try{
			 
			return floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "getReturnProductByRpId", log);
		}
	}





	@Override
	public DBRow[] filterReturnProductRows(String st, String end,
			long create_adid, int return_product_source, int status,
			int is_print_ove, int return_product_type , PageCtrl pc,int picture_recognition , int check_item_type,int gather_picture_status, int is_need_return) throws Exception {
		try{
			 
			return floorReturnProductMgrZr.filterReturnProductRows(st, end, create_adid, return_product_source, status, is_print_ove, return_product_type, pc,picture_recognition,check_item_type, gather_picture_status,is_need_return);
		 }catch (Exception e) {
			 throw new SystemException(e, "filterReturnProductRows", log);
		}
	}





	@Override
	public void returnProductRecognitionFinish(HttpServletRequest request) throws Exception {
		try{
			 //查询主单据所对应的所有的item_fix(图片).
			
			
 		 }catch (Exception e) {
			 throw new SystemException(e, "returnProductRecognitionFinish", log);
		}
	}



	
	/** 
	 * 退货完成
	 * 应该根据subItem上面的字段的信息。进行库存的修改，同时如果是有关联的订单的东西，如果往订单上写信息
	 * 发邮件等等.参照以前的退货登记
	 */
	@Override
	public void returnProductRegister(HttpServletRequest request) throws Exception {
		try{
			
			long rp_id = StringUtil.getLong(request, "rp_id");
			DBRow upDateRow = new DBRow();
			upDateRow.add("status", ReturnProductKey.FINISHALL);//StorageReturnProductStatusKey.FINISH
			floorReturnProductMgrZr.updateReturnProduct(upDateRow, rp_id);
			
			DBRow detailReturnInfo	= floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
		  	if(null != detailReturnInfo && 0 != detailReturnInfo.get("sid", 0L))
		  	{
		  		//更新服务单状态
		  		DBRow serviceRow	= new DBRow();
		  		serviceRow.add("status", ServiceOrderStatusKey.FINISH_RETURN);//退货完成
		  		floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, detailReturnInfo.get("sid", 0L));
		  	}
		  	AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true) );
			//修改库存数量
		  	//通过退货单ID获取退货子明细
		  	DBRow[] returnItemSubs	= floorReturnProductOrderMgrZyj.getReturnSubItemsByRpid(rp_id);
		  	for (int i = 0; i < returnItemSubs.length; i++)
		  	{
				long pid						= returnItemSubs[i].get("pid", 0L);
				float quality_nor				= returnItemSubs[i].get("quality_nor", 0F);
				float quality_damage			= returnItemSubs[i].get("quality_damage", 0F);
				float quality_package_damage	= returnItemSubs[i].get("quality_package_damage", 0F);
				DBRow detailProduct = floorProductMgr.getDetailProductByPcid(pid);
				//完好商品进库
				if (quality_nor>0)
				{
					ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
					inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
					inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
					inProductStoreLogBean.setOid(rp_id);
					inProductStoreLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RETURN);
					
					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean, detailReturnInfo.get("ps_id", 0l), pid, quality_nor,0);
					//记录入库日志
					ProductMgr productMgr = (ProductMgr)MvcUtil.getBeanFromContainer("productMgr");				
					TDate td = new TDate();
					long datemark = td.getDateTime();
					
					productMgr.addInProductLog(detailProduct.getString("p_code"), detailReturnInfo.get("ps_id", 0l), quality_nor, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_RETURN,detailReturnInfo.get("oid", 0l));
				}
				//功能残损进库
				if (quality_damage>0)
				{
					//增加残损件数目adminLoggerBean.getPs_id()
					floorProductMgr.incProductDamagedCountByPcid(detailReturnInfo.get("ps_id", 0l),pid,quality_damage);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				//包装残损进库
				if (quality_package_damage>0)
				{
					//增加残损件数目
					floorProductMgr.incProductPackageDamagedCountByPcid(detailReturnInfo.get("ps_id", 0l),pid,quality_package_damage);
					
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_package_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				
			}
		  	//加日志
		  	//对相关订单增加备注
			FloorCatalogMgr fcm = (FloorCatalogMgr)MvcUtil.getBeanFromContainer("floorCatalogMgr");
			String orderNote = fcm.getDetailProductStorageCatalogById(adminLoggerBean.getPs_id()).getString("title")+" warehouse 已经收到退件";
			DBRow detailOrder = floorOrderMgr.getDetailPOrderByOid(detailReturnInfo.get("oid",0l));
			
			DBRow noteRow = new DBRow();
			noteRow.add("oid", detailOrder.get("oid", 0L));
			noteRow.add("note", orderNote);
			noteRow.add("account", adminLoggerBean.getAccount());
			noteRow.add("adid", adminLoggerBean.getAdid());
			noteRow.add("trace_type", TracingOrderKey.RETURN_PRODUCT);
			noteRow.add("post_date", DateUtil.NowStr());
			noteRow.add("rel_id", rp_id);
			noteRow.add("trace_child_type",detailOrder.get("internal_tracking_status",0));
			floorOrderMgr.addPOrderNote(noteRow);
			
			//发送确认邮件通知顾客
			ReturnReceiveEmailPageString returnReceiveEmailPageString = new ReturnReceiveEmailPageString(rp_id,detailReturnInfo.get("oid",0l));
			User user = new User(returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getPassWord(),returnReceiveEmailPageString.getHost(),returnReceiveEmailPageString.getPort(),returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getAuthor(),returnReceiveEmailPageString.getRePly());
            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
            MailBody mailBody = new MailBody(returnReceiveEmailPageString,true,null);
            
            Mail mail = new Mail(user);
            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            mail.send();
		  	
		 }catch (Exception e) {
			 throw new SystemException(e, "returnProductRegister", log);
		}
	
	}



	/**
	 * 采集图片完成
	 */
	@Override
	public void gatherPictureStatus(HttpServletRequest request)
			throws Exception {
		 try{
			 long rp_id = StringUtil.getLong(request, "rp_id");
			 DBRow updateRow = new DBRow();
			 updateRow.add("gather_picture_status", 1);
			 floorReturnProductMgrZr.updateReturnProduct(updateRow, rp_id);
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			 //增加退货单日志
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,0,0,0, returnRow.get("status", 0), "退货单["+returnRow.get("rp_id", 0L)+"]图片采集完成", ReturnProductLogTypeKey.PICTURE, adminLoggerBean.getAdid(), DateUtil.NowStr());
			 
		 }catch (Exception e) {
			 throw new SystemException(e, "gatherPictureStatus", log);
		}
	}





	@Override
	public void relationOrder(HttpServletRequest request) throws Exception {
		 try{
			 String type = StringUtil.getString(request, "type");
			 long sid = StringUtil.getLong(request, "sid");
			 long rp_id = StringUtil.getLong(request, "rp_id");
			 DBRow updateRow = new DBRow();
			 String relationStr = "";
			 if(type.equals("sid")){
				 //关联上运单,如果是运单的话,那么关联上oid wid;
				ServiceOrderMgrZyjIFace serviceOrderMgrZyj = (ServiceOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyServiceOrderMgrZyj");
				DBRow serviceOrder = serviceOrderMgrZyj.getServiceOrderDetailBySid(sid);
				long oid = serviceOrder.get("oid", 0l);
				long wid = serviceOrder.get("wid", 0l);
				updateRow.add("oid", oid);
				updateRow.add("wid", wid);
				updateRow.add("sid", sid);
				relationStr = "服务单:"+sid;
				//原服务单为非需要退货，改成需要退货
				if(ReturnTypeKey.NEED != serviceOrder.get("is_need_return", 0))
				{
					DBRow serviceUpRow = new DBRow();
					serviceUpRow.add("is_need_return", ReturnTypeKey.NEED);
					serviceOrderMgrZyj.updateServiceOrderBySid(serviceUpRow, sid);
				}
			 }
			 if(type.equals("oid")){
				 updateRow.add("oid", sid);
				 updateRow.add("wid", 0l);
				 updateRow.add("sid", 0l);
				 relationStr = "订单:"+sid;
			 }
			 if(type.equals("wid")){
				 updateRow.add("wid", sid);
				 updateRow.add("oid", 0l);
				 updateRow.add("sid", 0l);
			 }
			 floorReturnProductMgrZr.updateReturnProduct(updateRow, rp_id);	
			 
			 DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			 //增加退货单日志
			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			 floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,0,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]关联单据,"+relationStr, ReturnProductLogTypeKey.RELATION_ORDER, adminLoggerBean.getAdid(), DateUtil.NowStr());
			 
			 ReturnProductOrderIndexMgrZyj.getInstance().updateIndex(rp_id);
		 }catch (Exception e) {
			 throw new SystemException(e, "relationOrder", log);
		}
		
	}





	@Override
	public DBRow checkReturnProductIsCreateByServices(HttpServletRequest request)
			throws Exception {
		DBRow temp = new DBRow();
		try{
			 long rp_id = StringUtil.getLong(request, "rp_id");
			 return floorReturnProductMgrZr.getReturnProductByIdAndIsCreateByServiceOrder(rp_id);
		}catch (Exception e) {
			 throw new SystemException(e, "checkReturnProductIsCreateByServices", log);
		}
	}

	@Override
	public void returnProductReturnHandleRequest(HttpServletRequest request)
			throws Exception {
		 try{
			 long rp_id = StringUtil.getLong(request, "rp_id");
		 
		 }catch (Exception e) {
			 throw new SystemException(e, "returnProductReturnHandleRequest", log);// TODO: handle exception
		}
		
	}

	public void setFloorServiceOrderMgrZyj(
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj) {
		this.floorServiceOrderMgrZyj = floorServiceOrderMgrZyj;
	}

	public void setFloorReturnProductOrderMgrZyj(
			FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj) {
		this.floorReturnProductOrderMgrZyj = floorReturnProductOrderMgrZyj;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr)
	{
		this.fileMgrZr = fileMgrZr;
	}

	public void setFloorReturnProductMgrZr(
			FloorReturnProductMgrZr floorReturnProductMgrZr)
	{
		this.floorReturnProductMgrZr = floorReturnProductMgrZr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}



	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

 
}
