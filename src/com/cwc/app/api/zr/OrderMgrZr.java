package com.cwc.app.api.zr;

import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.qll.OrdersMgrQLL;
import com.cwc.app.api.zj.QuoteMgrZJ;
import com.cwc.app.exception.order.TxnIdExistException;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorReturnProductMgrZJ;
import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.jms.action.UpdateTradeItemSkuFvfMpnJMS;
import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.BillTypeKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.PaymentMethodKey;
import com.cwc.app.key.TransactionType;
import com.cwc.app.lucene.OrderIndexMgr;
import com.cwc.app.lucene.zj.TradeIndexMgr;
import com.cwc.app.prehandle.OrderPreHandle;
import com.cwc.app.prehandle.order.OrderPreHandleFather;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fedex.ship.stub.Payment;
 
 

public class OrderMgrZr implements OrderMgrIfaceZR{
	
	static Logger log = Logger.getLogger("ACTION");
	static Logger paypallog = Logger.getLogger("PAYPAL");
	static Logger duplicatepaypallog = Logger.getLogger("DUPLICATEPAYPAL");
	
	public static String ORDER_TRADE_TYPE_DIRECT = "direct";	 //直接交易
	public static String ORDER_TRADE_TYPE_INDIRECT= "indirect"; //间接交易
	public static String ORDER_TRADE_TYPE_RELEVANCE = "relevance";	 //关联交易
	public static String ORDER_TRADE_TYPE_PENDING = "pending" ;		// pending交易
	 
	public static String ORDER_SOURCE_EBAY = "EBAY";
	public static String ORDER_SOURCE_EBAY_INVOICE = "EBAY_INVOICE";
	public static String ORDER_SOURCE_INVOICE = "INVOICE"; 			//处理product,shipping
	public static String ORDER_SOURCE_WEBSITE = "WEBSITE";
	public static String ORDER_SOURCE_DIRECTPAY = "DIRECTPAY";
	public static String ORDER_SOURCE_ORDER = "ORDER";
	public static String ORDER_SOURCE_INPERSON = "INPERSON";	// 上门购买
	public static String ORDER_SOURCE_WANRRANTY = "质保单";     // invoice进来如果是质保单需要付款的时候order_Source为这个
	
	private FloorOrderMgr fom ;
	private FloorProductMgr fpm;
	private FloorOrderMgrZr floorOrderMgrZr;
	private FloorBillMgrZr 	floorBillMgrZr;
	private OrderPreHandle orderPreHandle;	
	private OrdersMgrQLL ordersMgrQLL;
	private FloorReturnProductMgrZJ floorReturnProductMgrZJ;
	private QuoteMgrZJ quoteMgrZJ;
 
	DecimalFormatSymbols dfs = new DecimalFormatSymbols();  
	java.text.DecimalFormat   df   = new DecimalFormat("###,###.00",dfs);
	public void setOrdersMgrQLL(OrdersMgrQLL ordersMgrQLL) {
		this.ordersMgrQLL = ordersMgrQLL;
	}
	public void setFom(FloorOrderMgr fom) {
		this.fom = fom;
	}
	public void setFpm(FloorProductMgr fpm) {
		this.fpm = fpm;
	}
	public void setOrderPreHandle(OrderPreHandle orderPreHandle) {
		this.orderPreHandle = orderPreHandle;
	}
	public void setFloorBillMgrZr(FloorBillMgrZr floorBillMgrZr) {
		this.floorBillMgrZr = floorBillMgrZr;
	}
	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}
	@Override
	public synchronized long notifyIPN(HttpServletRequest request) throws SystemException,
			Exception {
			
		String str = printInfoToLog(request);
		String txn_id = StringUtil.getString(request, "txn_id");
		//把参数重新发给paypal验证
		
		
		try{
			
		  String value = "" ; //volidateIPN(txn_id, str);
		
			 //现在验证没有通过
           /*	if(!value.trim().toLowerCase().equals("verified"))
            {
				paypallog.info(txn_id + "Paypal IPN did NOT Verified, Result:" + value);
				throw new Exception();
			}*/
            paypallog.info(txn_id + "Paypal IPN Verified, Result:" + value);
			request.setAttribute("sender_status",value.trim().toLowerCase());//paypal验证参数请求的结果
			long orderId = 0l;
			long address_id = 0l;
			long tradeId = 0l;
			
			String source = "";
			String validateTxnId = StringUtil.getString(request, "txn_id");
			String paymentStatus = StringUtil.getString(request,"payment_status");
		 
			//txn_id 并行
			synchronized (txn_id) 
			{
				if(validateTxnId.length() > 0 ){
					DBRow row = floorOrderMgrZr.getTradeBillByTxnIdAndPaymentStatus(validateTxnId,paymentStatus);
					if(row != null){
						duplicatepaypallog.info("================== Duplicate "+validateTxnId+" Request start ==================");
						this.printLogDuplicate(request);
						duplicatepaypallog.info("================== Duplicate "+validateTxnId+" Request end ==================");
						return 0l;
					}
					
				}
				source = getOrderSource(request);
				
				if (isPermit(request))
				{
					String trade =  tradeType(request);
					 String clientType = handleClient(request);
					 address_id = handleClientAddressInfo(request , clientType );
					 if(trade.equals(OrderMgrZr.ORDER_TRADE_TYPE_DIRECT)){
						 //直接交易 
						
						 /**
						  * 0.判断订单来源 确定是不是插入到订单上
						  * 1.如果是返回的ORDER_SOURCE_EBAY_INVOICE,ORDER_SOURCE_INVOICE那么就需要查询bill_item 表插入到COI 然后要注意字段
						  * 2.如果是返回的ORDER_SOURCE_EBAY 这种是去读的request的对象中的数据
						  * 3.对于WebSite的CIO是在后来与其他的网站通讯的时候才插入的
						  * 4.如果是进来的是payment_status=Completed 这个时候要更新以前的状态为pending的时候的Trade的关联的属性,这个时候要兼容以前的pending已经创建了订单
						  * 
						  */
						
						
						
						 if(source.equals(OrderMgrZr.ORDER_SOURCE_EBAY) || source.equals(OrderMgrZr.ORDER_SOURCE_EBAY_INVOICE) || source.equals(OrderMgrZr.ORDER_SOURCE_INVOICE) || source.equals(OrderMgrZr.ORDER_SOURCE_WANRRANTY) ||source.equals(OrderMgrZr.ORDER_SOURCE_WEBSITE) || source.equals(OrderMgrZr.ORDER_SOURCE_DIRECTPAY)){
							 //在订单上插入一条Order 
							/* DBRow insertOrderRow = new DBRow();
							 insertOrderRow.add("payment_date", StrUtil.getString(request, "payment_date"));
							 insertOrderRow.add("order_source", source);
							 fixDBRowAddSellerAndBuyer(insertOrderRow,request);*/
							 
							 orderId = this.addOrder(request);
							 
							 
			 
							 
						 }
						 //declare
						 tradeId = insertTrade(request,orderId,address_id,source,str);
						
						 
						 if(source.equals(OrderMgrZr.ORDER_SOURCE_EBAY)){
							 insertTradeItem(request,orderId,tradeId);
						 }else if(source.equals(OrderMgrZr.ORDER_SOURCE_EBAY_INVOICE) || source.equals(OrderMgrZr.ORDER_SOURCE_INVOICE) || source.equals(OrderMgrZr.ORDER_SOURCE_WANRRANTY)){
							 long billId = StringUtil.getLong(request, "invoice_number");
							 // 如果是得不到billId 那么说明就是在customer字段中包含有invoice
							 if(billId == 0l && StringUtil.getString(request, "custom").indexOf("invoice:") != -1){
								String customer = StringUtil.getString(request, "custom");
								 int index = customer.indexOf("invoice:");
								 billId = Long.parseLong(customer.substring(index+8));
							 }
							 DBRow billRow = floorBillMgrZr.getBillByBillId(billId);
							 // 这里添加如果是 质保的账单进来
							 if(billRow.get("bill_type",0) == BillTypeKey.warranty ){
								 floorReturnProductMgrZJ.warrantyInvoiceHasPay(billRow.get("bill_id",0l),orderId);
							 }
							
							 insertTradeItemByBillId(billId,orderId,txn_id,tradeId);
							 // 这个时候更新原来的
							 DBRow updateRow = new DBRow();
							 updateRow.add("trade_id", tradeId);
							 updateRow.add("bill_status", BillStateKey.bargain);
							 updateRow.add("porder_id", orderId);
							 floorBillMgrZr.updateBill(billId, updateRow);
						 }
						
					 } else if(trade.equals(OrderMgrZr.ORDER_TRADE_TYPE_INDIRECT)){
						 // 间接交易 	 
					 }else if(trade.equals(OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE)){
						 // 关联交易  
						 /**
						  * 1.那么就把原来交易上的信息查出来 放到新的交易当中
						  * 2.如果关联交易是Order . 或者是Custom 字段中的有ORDER那么就要新建立交易。不会有订单
						  */
						 
						 String txnId = StringUtil.getString(request, "parent_txn_id");
						 //如果是parent_txn_id 为null 那么就要选择txn_id去查询,这种主要是处理客户去paypal开case的时候,进来的报文
						 boolean isDisputeOrder =  this.isDisputeOrder(request);
						 //改变payment_status的时候parent_txn_id 是没有的但是也是关联交易
						 txnId = txnId.length() < 1 ? StringUtil.getString(request, "txn_id"):txnId;
						 boolean isPayForOrder = false;
						 DBRow parentRow = null;
						 
						 if(source.equals(OrderMgrZr.ORDER_SOURCE_ORDER)){
							 // 这个表示的是在付款页面上填写Pay for Order 的情况
							 isPayForOrder = true;
						 }
							 parentRow = floorOrderMgrZr.getTradeBillByTxnIdAndCreateOrder(txnId);
						  
						 if(txnId != null && txnId.length() > 0 && !isPayForOrder){
							
							 if(parentRow == null){
								  //兼容以前,因为以前的订单的创建是没有交易的。这个时候通过TxnId去查询是要报错的
								  //通过txn_id去Order表中查询,然后得到Order 的一些信息添加Trade 通过Order的方式生成Trade
								 
								 DBRow orderRow =  floorOrderMgrZr.getOrderByTxnId(txnId) ;
								  if(orderRow == null ){
									   throw new RuntimeException("Relation Object Not Found.");
								  }else{
									  
									  parentRow = new DBRow();
									  parentRow.add("relation_type", 0);
									  parentRow.add("relation_id", orderRow.get("oid", 0l));
									  parentRow.add("payment_date", orderRow.getString("payment_date"));
									  parentRow.add("txn_id", txnId);
									  parentRow.add("mc_gross", orderRow.get("mc_gross",0.0d));
									  parentRow.add("mc_fee", orderRow.get("mc_fee",0.0d));
									  parentRow.add("mc_shipping", orderRow.get("shipping",0.0d));
									  
									  parentRow.add("tax", orderRow.get("tax",0.0d));
									  parentRow.add("mc_currency", orderRow.getString("mc_currency"));
									  parentRow.add("payment_status", orderRow.getString("payment_status"));
									  
									  parentRow.add("receiver_email", orderRow.getString("business"));
									  parentRow.add("business", orderRow.getString("business"));
									 
									  //parentRow.add("receiver_id", orderRow.get("",0.0d));		报文中才含有
									  parentRow.add("order_source", orderRow.getString("order_source"));
									  parentRow.add("seller_ebay_id", orderRow.getString("seller_id"));
									  parentRow.add("auction_buyer_id", orderRow.getString("auction_buyer_id"));
									  
								 
									  
									  
									  parentRow.add("payer_email", orderRow.getString("client_id"));
									//  parentRow.add("payer_id", orderRow.get("",0.0d));			报文中才含有
									  parentRow.add("delivery_address_name", orderRow.getString("address_name"));
									  parentRow.add("delivery_address_street", orderRow.getString("address_street"));
									  parentRow.add("delivery_address_city", orderRow.getString("address_city"));
									  parentRow.add("delivery_address_state", orderRow.getString("address_state"));
									  parentRow.add("delivery_address_zip", orderRow.getString("address_zip"));
									  parentRow.add("delivery_address_status", orderRow.getString("address_state"));
									  parentRow.add("delivery_address_country_code", orderRow.getString("address_country_code"));
									  parentRow.add("delivery_address_country", orderRow.getString("address_country"));
									  parentRow.add("delivery_pro_id", orderRow.get("pro_id",0));
									  parentRow.add("delivery_ccid_id", orderRow.get("ccid",0));
									  parentRow.add("contact_phone", orderRow.getString("tel"));
									  parentRow.add("post_date", orderRow.getString("post_date"));
									  parentRow.add("transaction_type", TransactionType.Payment);
									  parentRow.add("client_id", orderRow.getString("client_id"));
									  // 表示的是 为null
									  if(orderRow.get("pay_type", 9) == 9){
										  
									  }else{
										  parentRow.add("payment_channel", orderRow.get("pay_type", 0));
										  parentRow.add("payment_method", new PaymentMethodKey().getStatusById(orderRow.get("pay_type", 0)));
									  }
									  parentRow.add("is_create_order", 1);
								 
									  this.insertTradeAndNotExits(parentRow);
									  parentRow = floorOrderMgrZr.getTradeBillByTxnIdAndCreateOrder(txnId);
	 							  }
							 }
							 addTradeByRelation(request,parentRow,str);
							 if(isDisputeOrder){
								 //如果是isDisputeOrder的情况那么是要处理更新原来的订单的状态 
								 this.markDisputeOrder(request);
							 }
						 }else{
							 // addTrade By order
							 addTradeByRelationOrder(request,str,source);
						 }
						 // 兼容以前.. 关联交易要插入relation_order
						 fom.addRelationOrder(this.getSpecialOrder(request));
						 DBRow orgOrder = fom.getDetailPOrderByTxnid(txnId);
						 if(orgOrder != null){
							 updateOrderPaymentStatus(txnId,StringUtil.getString(request, "payment_status"));
						 }
					 }
					 return orderId;
	     		}
				else
				{
					paypallog.info("================== Empty txn_id Request start ==================");
					printLogUnSuccess(request);
					paypallog.info("================== Empty txn_id Request end ==================");
				}
			}
			
				
		}
		catch (Exception e) 
		{
			paypallog.info("=============" + txn_id + " Insert Failuer Start" + "============");
			printLogUnSuccess(request);
			paypallog.info("===========" + txn_id + " Insert Failuer End" + "==============");
			throw new SystemException(e,"paypalNotify",log);
		}
		return 0;
	}
	/**
	 * 
	 * @param request
	 * @param allInfo
	 * @return
	 * @throws SystemException
	 * 这个方法是用于对为Order补款的时候 插入交易 (包括在订单管理界面发送Invoice方式 和在外部界面填写订单号)
	 * 按照关联交易的处理，如果是网页填写Order的情况 那么就要根据订单号。查询出交易
	 * 如果是Invoice PayForOrder的也是同样的处理
	 */
	
	private long addTradeByRelationOrder(HttpServletRequest request, String allInfo,String orderSource) throws SystemException{
		try{
			long billOrderId = StringUtil.getLong(request, "invoice_number");
			 String custom = StringUtil.getString(request, "custom");
			if(billOrderId == 0l){
				 int indexStart = custom.indexOf("invoice:");
				 billOrderId = Long.parseLong(custom.substring(indexStart+8));
			}
			DBRow tradeRow = getTradeDBRow(request,allInfo);
			tradeRow.add("is_create_order", 0);
			long orderId = 0l;
			boolean isInvoice = false;
			long tradeId =  0l;
			if((billOrderId != 0l && StringUtil.getString(request, "txn_type").equalsIgnoreCase("invoice_payment")) || (billOrderId != 0l  && custom.indexOf("invoice")!= -1)){
				DBRow billOrder = this.getDetailById(billOrderId, "bill_order", "bill_id");
				orderId = billOrder.get("porder_id", 0l);
				//要更新BillOrder上的表。把BillOrder 的状态改为交易完成。。同时要添加交易id
				isInvoice = true;
			}else{
				String itemName = StringUtil.getString(request, "item_name1");
				String[] array = itemName.split(":");
				if(array.length >= 1){
					orderId = Long.parseLong(array[1]);
				}
			}
			DBRow parentOrder = floorOrderMgrZr.getOrderRowByOrderId(orderId);
			//应该查询一下是不是有这个订单,如果是有的话.那么就继续。否则就应该报错
			if(parentOrder.get("oid", 0l) == 0l){
				 throw new RuntimeException("["+orderId + "]Order Not Found."); 
		 
			}else{
				tradeRow.add("order_source", orderSource);
				tradeRow.add("relation_type", 0);
				tradeRow.add("relation_id", orderId);
				tradeRow.add("client_id", StringUtil.getString(request, "payer_email"));
				tradeRow.add("parent_txn_id", parentOrder.getString("txn_id"));
				tradeId = this.insertTradeAndNotExits(tradeRow);
			}
			if(isInvoice){
				DBRow udpateBillOrder = new DBRow();
				udpateBillOrder.add("trade_id", tradeId);
				udpateBillOrder.add("bill_status", BillStateKey.bargain);
				floorBillMgrZr.updateBill(billOrderId, udpateBillOrder);
			}
			return tradeId;
		}catch (Exception e) {
			throw new SystemException(e,"addTradeByRelationOrder",log);
		}
	}
	
	
	// 插入关联交易 所有的信息拷贝原来parent_txn_id 上的数据 然后txn_id,parent_txn_id,payment_date
	private long addTradeByRelation(HttpServletRequest request,DBRow parentDBRow, String allInfo) throws SystemException{
		try{
			// 如果是parent_txn_id 为空的话。那么就要把txn_id 当成parent_txn_id。
			String parentTxnId = StringUtil.getString(request, "parent_txn_id");
			if(parentTxnId.trim().length() < 1){
				parentDBRow.add("parent_txn_id", StringUtil.getString(request, "txn_id"));
				parentDBRow.add("txn_id", StringUtil.getString(request, "txn_id"));	
			}else{
				parentDBRow.add("txn_id", StringUtil.getString(request, "txn_id"));	
				parentDBRow.add("parent_txn_id", StringUtil.getString(request, "parent_txn_id"));
			}
			parentDBRow.remove("trade_id");
			
	 
			parentDBRow.add("payment_date", StringUtil.getString(request, "payment_date"));
			parentDBRow.add("reason_code", StringUtil.getString(request, "reason_code"));
			parentDBRow.add("case_id", StringUtil.getString(request, "case_id"));
			parentDBRow.add("case_type", StringUtil.getString(request, "case_type"));
			
			parentDBRow.add("mc_gross", StringUtil.getFloat(request, "mc_gross"));
			parentDBRow.add("mc_fee", StringUtil.getFloat(request, "mc_fee"));
			parentDBRow.add("mc_shipping", StringUtil.getFloat(request, "mc_shipping"));
			parentDBRow.add("mc_handling", StringUtil.getFloat(request, "mc_handling"));
			parentDBRow.add("tax", StringUtil.getFloat(request, "tax"));
			parentDBRow.add("mc_currency", StringUtil.getString(request, "mc_currency"));
			parentDBRow.add("payment_status", StringUtil.getString(request, "payment_status"));
			parentDBRow.add("is_create_order", 0);
			parentDBRow.add("all_info",allInfo);
			parentDBRow.add("post_date",DateUtil.NowStr());
			parentDBRow.add("pending_reason",StringUtil.getString(request, "pending_reason"));
			parentDBRow.add("transaction_type", TransactionType.Complain);
			 
			return this.insertTradeAndNotExits(parentDBRow);
	 
		}catch (Exception e) {
			throw new SystemException(e,"addTradeByRelation",log);
		}
	}
	@Override
	public  void orderPreHandle(long oid) 
	{
		try
		{         
			List<OrderPreHandleFather> orderPreHandleClassList = orderPreHandle.getOrderPreHandleClass();
			for(OrderPreHandleFather oph : orderPreHandleClassList)
			{
				if (oph!=null)
				{
					oph.perform( oid );
				}
			}
		}
		catch (Exception e) 
		{
			log.error("paypalNotify.orderPreHandle error:"+e);
		}
	}
	
	
	private synchronized long addOrder(HttpServletRequest request)  throws SystemException{
		{
			long oid = 0;
			
			try
			{	
				// 创建订单时,Paypal 或者是支付平台本身的传送过来的TXN_ID
				String txn_id = StringUtil.getString(request, "txn_id");
				
				String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
				String payment_status = StringUtil.getString(request, "payment_status");
				
				String custom = StringUtil.getString(request, "custom");
				
				DBRow orgOrder = fom.getDetailPOrderByTxnid( txn_id );
				String memo = StringUtil.getString(request, "memo");
				
				String order_source = this.getOrderSource(request);
					//先判断是否有parent_txn_id或者是投诉订单
					if ( this.haveParentOrder(request) || this.isDisputeOrder(request)){
						fom.addRelationOrder( this.getSpecialOrder(request) );
						if ( this.isDisputeOrder(request) ){
							markDisputeOrder(request);
						}
						else{
							updateOrderPaymentStatus(parent_txn_id,payment_status);
						}
					}
					else if ( orgOrder != null )   //对已经存在的txnid的订单同样插入到关联订单 declare !
					{
						////system.out.println("2222");
						
						fom.addRelationOrder( this.getSpecialOrder(request) );
						updateOrderPaymentStatus(txn_id,payment_status);
					}
					else
					{
//						String txn_id = StrUtil.getString(request,"txn_id");
						String item_name = StringUtil.getString(request,"item_name");
						String item_number = StringUtil.getString(request,"item_number");
						String mc_currency = StringUtil.getString(request,"mc_currency");
						String first_name = StringUtil.getString(request,"first_name");
						String last_name = StringUtil.getString(request,"last_name");
						String auction_buyer_id = StringUtil.getString(request,"auction_buyer_id");
						String business = StringUtil.getString(request,"business");
						String payer_status = StringUtil.getString(request,"payer_status");
						String address_country = StringUtil.getString(request,"address_country");
						String address_city = StringUtil.getString(request,"address_city");
						String address_zip = StringUtil.getString(request,"address_zip");
						String address_street = StringUtil.getString(request,"address_street");
						String address_country_code = StringUtil.getString(request,"address_country_code");
						String address_state = StringUtil.getString(request,"address_state");
						String address_status = StringUtil.getString(request,"address_status");
//						String payment_status = StrUtil.getString(request,"payment_status");
						String payment_date = StringUtil.getString(request,"payment_date");
						String post_date = StringUtil.getString(request,"post_date");
						
						DBRow porder = new DBRow();
						porder.add("txn_id",txn_id);
						porder.add("item_name",item_name);
						porder.add("item_number",item_number);
						porder.add("mc_currency",mc_currency);
						porder.add("first_name",first_name);
						porder.add("last_name",last_name);
						porder.add("auction_buyer_id",auction_buyer_id);
						porder.add("business",business);
						porder.add("payer_status",payer_status);
						porder.add("address_country",address_country);
						porder.add("address_city",address_city);
						porder.add("address_zip",address_zip);
						porder.add("address_street",address_street);
						porder.add("address_country_code",address_country_code);
						porder.add("address_state",address_state);
						porder.add("address_status",address_status);
						porder.add("payment_status",payment_status);
						porder.add("payment_date",payment_date);
						porder.add("post_date",post_date);
						
						// 主要是针对如果是信用卡付款的时候过来的没有item_name 只有item_name1
						// auction_buyer_id 订单上如果通过Custom过来是没有的
						 if (porder.getString("auction_buyer_id").length() < 1 && custom.indexOf("auction_buyer_id=")>=0)
					      {
					       String buyerStr = custom.split("auction_buyer_id=")[1];
					       int indexBuyer = buyerStr.indexOf(",");
					       String buyer = "";
					       if(indexBuyer != -1){
					        buyer  = buyerStr.substring(0, indexBuyer);
					       }else{
					        buyer = buyerStr;
					       }
					       porder.add("auction_buyer_id",buyer);
					      }
						String itemName = StringUtil.getString(request, "item_name");
						String itemNumber = StringUtil.getString(request, "item_number");
						if(itemName.length() < 1){
							int index = 1 ;
							StringBuffer sb = new StringBuffer();
						
							String fixItemName = StringUtil.getString(request, "item_name"+index);
							while(fixItemName.length() > 0){
								index++ ;
								sb.append("," + fixItemName);
								fixItemName  = StringUtil.getString(request, "item_name"+index);
							}
							if(sb.length() > 1){
								porder.add("item_name", sb.toString().substring(1));
							}
						}
						if(itemNumber.length() < 1){
							int index = 1 ;
							StringBuffer sb = new StringBuffer();
							String fixItemNumber = StringUtil.getString(request, "item_number"+index);
							while(fixItemNumber.length() > 0){
								index++ ;
								sb.append("," + fixItemNumber);
								fixItemNumber  = StringUtil.getString(request, "item_number"+index);
							}
							if(sb.length() > 1){
								porder.add("item_number", sb.toString().substring(1));
							}
						}
						String address_name = StringUtil.getString(request, "address_name");
						address_name = StringUtil.replaceString(address_name,"\"", ".");
						address_name = StringUtil.replaceString(address_name,"  ", " ");
						porder.add("address_name",address_name);
						porder.add("client_id", StringUtil.getString(request, "payer_email"));
						if (custom.indexOf("@")>=0 && custom.indexOf("payment_type=paypaldp") == -1)
						{
							String tmpA[] =  custom.split(",");
							
							porder.add("business", tmpA[0]);

							if (tmpA.length>1 && tmpA[1].lastIndexOf(":") != -1) 
							{
								porder.add("vvme_webservice_url", tmpA[1].substring(0,tmpA[1].lastIndexOf(":")));
							}
							
						}
						if (custom.indexOf("payer_phone=")>=0)
						{
							String phoneStr = custom.split("payer_phone=")[1];
							int indexPhone = phoneStr.indexOf(",");
							String phone = "";
							if(indexPhone != -1){
								phone  = phoneStr.substring(0, indexPhone);
							}else{
								phone = phoneStr;
							}
							porder.add("tel",phone);
							if(memo.length() < 1){
								String txnId = porder.getString("txn_id");
								DBRow temp = floorBillMgrZr.getVertualterminalRowByTxnId(txnId);
								if(temp != null){
									memo = temp.getString("note");
								}
							}
						}
						//判断订单来源
						/*String auction_buyer_id = StrUtil.getString(request, "auction_buyer_id");
						String txn_type = StrUtil.getString(request, "txn_type");

						if (!auction_buyer_id.equals(""))
						{
							order_source = this.ORDER_SOURCE_EBAY;
						}
						else
						{
							if (custom.indexOf(":WEBSITE")>=0)
							{
								order_source = this.ORDER_SOURCE_WEBSITE;
							}
							else
							{
								order_source = this.ORDER_SOURCE_DIRECTPAY;
								//获得电话号码
								if (custom.indexOf("payer_phone=")>=0)
								{
									porder.add("tel",custom.split("payer_phone=")[1]);
								}
								
							}
						}*/

						//获得国家代码
						DBRow countryCode = fom.getDetailCountryCodeByCountry(porder.getString("address_country").trim());
						
						if (countryCode==null)
						{
							porder.add("ccid",0);
							porder.add("pro_id",-1);
						}
						else
						{
							if(countryCode.get("ccid", 0l)==11036)//美国订单将邮编截取前5位
							{
								String zip = porder.getString("address_zip");
								if(zip.length()>5)
								{
									zip = zip.substring(0,5);
								}
								porder.add("address_zip",zip);
							}
							
							
							porder.add("ccid",countryCode.get("ccid", 0l));
							if (porder.getString("address_state").trim().equals(""))
							{
								porder.add("pro_id",-1);
							}
							else
							{
									//获得省份ID
									DBRow detailProvince = fpm.getDetailProvinceByNationIDPCode(countryCode.get("ccid", 0l),porder.getString("address_state").trim());//通过国家ID，省份/州缩写查询
									if(detailProvince == null)//判断是否可以通过缩写查询到对应State的ID
									{
										detailProvince = fpm.getDetailProvinceByNationIDPName(countryCode.get("ccid",0l),porder.getString("address_state").trim());//通过国家ID，省份/州缩写查询
									}
									
									if (detailProvince==null)
									{
										porder.add("pro_id",-1);
									}
									else
									{
										porder.add("pro_id",detailProvince.get("pro_id", 0l));
										porder.add("address_state",detailProvince.getString("pro_name"));
									}
							}
						}
						/**
						 * 对ebay_txn_id处理下
						 * 在多item情况下，ebay_txn_id也有多个
						 */
						String ebay_txn_id="";
						if ( StringUtil.getString(request, "item_number").indexOf(",")>0 )
						{
							int inI = 1;
							for (;inI<StringUtil.getString(request, "item_number").split(",").length;inI++)
							{
								ebay_txn_id += StringUtil.getString(request, "ebay_txn_id"+inI) + ",";
							}
							ebay_txn_id += StringUtil.getString(request, "ebay_txn_id"+inI);
						}
						else
						{
							ebay_txn_id = StringUtil.getString(request, "ebay_txn_id1");
						}
						
						porder.add("order_source",order_source);
						porder.add("append_name","");
						porder.add("note",memo);
						porder.add("dispute_flag",0);
						porder.add("mc_gross",StringUtil.getFloat(request, "mc_gross",0));
						porder.add("mc_fee",StringUtil.getFloat(request,"mc_fee",0));
						porder.add("tax",StringUtil.getFloat(request, "tax",0));
						porder.add("shipping",StringUtil.getFloat(request, "shipping",0));
						porder.add("quantity",StringUtil.getFloat(request, "num_cart_items",0));
						porder.add("sender_status",request.getAttribute("sender_status").toString());
						porder.add("handle",HandleKey.WAIT4_RECORD);
						porder.add("handle_status",0);
						porder.add("product_status",0);
						porder.add("ip",request.getRemoteAddr());
						porder.add("post_date",DateUtil.NowStr());
						porder.add("verify_cost",0);
						porder.add("trace_flag",1);
						porder.add("ebay_txn_id",ebay_txn_id);
						porder.add("mc_feee",StringUtil.getFloat(request, "mc_fee",0));
						porder.add("pending_reason",StringUtil.getString(request, "pending_reason"));
						porder.add("custom",custom);
						
						//处理信用卡Filter Pending信息
						StringBuffer fmpfSB = new StringBuffer("");
						for (int zz=1; zz<=17; zz++)
						{
							String fraud_management_pending_filters = StringUtil.getString(request, "fraud_management_pending_filters_"+zz);
							if (fraud_management_pending_filters.trim().equals("")==false)
							{
								fmpfSB.append(fraud_management_pending_filters);
								fmpfSB.append("\n");
							}
							else
							{
								break;
							}
						}
						porder.add("fraud_management_pending_filters",fmpfSB.toString());
						oid = fom.addPOrder(porder);

						//地址验证
//						ordersMgrQLL.addressValidateFedexJMS(oid,porder.getString("address_street"),porder.getString("address_zip"));
					//	OrderIndexMgr.getInstance().addIndex(porder.getString("address_name"),porder.getString("client_id"),oid,porder.getString("txn_id"),porder.getString("auction_buyer_id"),porder.getString("item_number"),"",porder.getString("address_country"));
						fom.addRelationOrder( this.getSpecialOrder(request) );	

					}	
			} 
			catch (Exception e) 
			{
				throw new SystemException(e,"addPOrder",log);
			}
			
			return(oid);
		}

	}
	/**
	 * 投诉订单
	 * @param request
	 * @return
	 */
	private boolean isDisputeOrder(HttpServletRequest request){
		String txn_type = StringUtil.getString(request, "txn_type");
		if (txn_type.equals("new_case")){
			return(true);			
		}
		else{
			return(false);
		}
	}
	
	/**
	 * 封装特殊订单那数据，用来插入到特殊订单
	 * @param request
	 * @return
	 */
	private DBRow getSpecialOrder(HttpServletRequest request)
	{
		String txn_id = StringUtil.getString(request, "txn_id");
		String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
		String payment_status = StringUtil.getString(request,"payment_status");
		String payment_type = StringUtil.getString(request,"payment_type");
		String reason_code = StringUtil.getString(request,"reason_code");
		String case_id = StringUtil.getString(request,"case_id");
		String case_type = StringUtil.getString(request,"case_type");
		
		
		DBRow specialOrder = new DBRow();
		specialOrder.add("payment_status",payment_status);
		specialOrder.add("payment_type",payment_type);
		specialOrder.add("reason_code",reason_code);
		specialOrder.add("case_id",case_id);
		specialOrder.add("case_type",case_type);
		
		if (!parent_txn_id.equals(""))
		{
			specialOrder.add("txn_id",parent_txn_id);
		}
		else
		{
			specialOrder.add("txn_id",txn_id);
		}
		
		specialOrder.add("post_date", DateUtil.NowStr());
		
		return(specialOrder);
	}
	
	/**
	 * 把主订单标记为有投诉
	 * 一边飙红
	 * @param request
	 * @throws Exception
	 */
	private void markDisputeOrder(HttpServletRequest request)
		throws Exception
	{
		String txn_id = StringUtil.getString(request, "txn_id");
		
		try
		{
			DBRow order = new DBRow();
			order.add("dispute_flag", 1);
			
			fom.modPOrder(txn_id, order);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"markDisputeOrder",log);
		}
	}
	
	/**
	 * 把订单状态修改为最后的状态
	 * @param request
	 * @throws Exception
	 */
	private void updateOrderPaymentStatus(String txn_id,String payment_status)
		throws Exception
	{	
		try
		{
			if(payment_status != null && payment_status.length() > 0){
				DBRow order = new DBRow();
				order.add("payment_status", payment_status);
				fom.modPOrder(txn_id, order);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOrderPaymentStatus",log);
		}
	}
	private boolean haveParentOrder(HttpServletRequest request){
		String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
		if (parent_txn_id.equals("")){
			return(false);
		}
		else{
			return(true);
		}
	}
	 // 查询Bill_item 表然后插入COI
	private void insertTradeItemByBillId(long billId ,long oid, String txn_id , long tradeId)  throws SystemException{
		try{
			
			DBRow[] rows = floorBillMgrZr.getBillItemByBillOrderId(billId);
			if(rows != null && rows.length > 0){
				 for(DBRow row : rows){
					 DBRow COI = new DBRow();
					 COI.add("quantity", row.get("quantity", 0.0d));
					 COI.add("item_number", row.get("item_number", 0.0d));
					 COI.add("item_name", row.getString("name"));
					 COI.add("mc_gross", row.get("actual_price", 0.0d));
					 COI.add("oid", oid);
					 COI.add("txn_id", txn_id);
					 COI.add("trade_id", tradeId);
					 floorOrderMgrZr.insertTradeItem(COI);
				 }
			
				
			}
			
			
		}catch (Exception e) {
			throw new SystemException(e,"insertTradeItemByBillId",log);
		}
	}
	 /**
	  * 给COI中插入数据 
	  * @param request
	  * @param oid
	  * @throws SystemException
	  * 1.首先去查找有没有 item_number1。	
	  * 	如果是没有的话那么就读取 交易上的信息把 数据填完整
	  * 	如果是有的话那么就要循环你读取数据。知道是没有找到Item_nuber#.(这个时候是item_number,和item_name是没有效果的不读)
	  */
	private void insertTradeItem(HttpServletRequest request , long oid, long tradeId) throws SystemException{
		try{
			
			String item_number1 = StringUtil.getString(request,"item_number1");
			int index = 1;
			DBRow insertRow  = null ;
			if(item_number1.trim().length() < 1 && StringUtil.getString(request,"item_number").trim().length() < 1){
				return ;
			}
			if(item_number1.trim().length() < 1){
				// 表示的是只有一个Item?(要测试一下)
				  insertRow  = getTradeItem(request,0);
				  insertRow.add("trade_id", tradeId);
				  insertRow.add("oid", oid);
				  floorOrderMgrZr.insertTradeItem(insertRow);
			}else{
				// 多个Item的时候执行
				while(true){
					insertRow = getTradeItem(request,index);
					index ++ ;
					insertRow.add("oid", oid);
					insertRow.add("trade_id", tradeId);
					floorOrderMgrZr.insertTradeItem(insertRow);
					if(StringUtil.getString(request,"item_number" + index).length() < 1){
						break;
					}
				}	
			}
		}catch (Exception e) {
			throw new SystemException(e,"insertTradeItem",log);
		}
	}
	private long insertTradeAndNotExits(DBRow insertTradeRow) throws Exception {
		try{
			// 保证插入交易的时候数据库应该只有一个txn_id的交易
			String txnId = insertTradeRow.getString("txn_id");
			long tradeId = 0l;
			if(txnId.trim().length() > 0){
				DBRow row =	floorOrderMgrZr.getTradeBillByTxnIdAndCreateOrder(txnId);
				if(row == null || insertTradeRow.get("is_create_order", 0l) == 0){
					tradeId = floorOrderMgrZr.insertTrade(insertTradeRow);
					TradeIndexMgr.getInstance().addIndex(tradeId,insertTradeRow.getString("txn_id") ,insertTradeRow.getString("client_id"), insertTradeRow.get("relation_id", 0l), insertTradeRow.getString("payer_email"), insertTradeRow.getString("delivery_address_name"));
				}
				else{ 
					 
					throw new RuntimeException("Txn Is Exits In Trade");
				}
			}else{
				// 如果是同一个报文进来两次的时候。这个时候要处理，不应该是添加交易
				
				
				tradeId = floorOrderMgrZr.insertTrade(insertTradeRow);
				TradeIndexMgr.getInstance().addIndex(tradeId,insertTradeRow.getString("txn_id") ,insertTradeRow.getString("client_id"), insertTradeRow.get("relation_id", 0l), insertTradeRow.getString("payer_email"), insertTradeRow.getString("delivery_address_name"));
			}
			return tradeId;
		}catch(Exception e){
			throw new SystemException(e,"insertTradeAndNotExits",log);
		}
	}
	private DBRow getTradeItem(HttpServletRequest request ,int index ) throws SystemException {
		try{
			DBRow tradeItem = new DBRow();
			String[] array = {"item_number","item_name","ebay_txn_id"};
			String[] floatArray = {"mc_gross","mc_shipping","mc_handling","tax","quantity"};
			String addStr = (index <= 0 ? "":index+"");
			String txnId = StringUtil.getString(request, "txn_id");
			tradeItem.add("txn_id", txnId);
			 
			for(String name : array){
				tradeItem.add(name,StringUtil.getString(request, name+addStr));
			}
			for(String name : floatArray){
				String fixName = name;
				if(name.indexOf("mc_gross") != -1 && index != 0){
					fixName = "mc_gross_";
				}
				// fix 当没有Shipping_#的时候就用总的代替
				if(name.equals("mc_shipping") && index == 0){
					fixName = "shipping";
				}
				tradeItem.add(name,StringUtil.getFloat(request, fixName+addStr));
			}
			// item 中的信息都计算成单价的
			float quantity  = tradeItem.get("quantity", 0.0f);
			float mc_gross = tradeItem.get("mc_gross", 0.0f);
			float unit = mc_gross / quantity;
			tradeItem.add("mc_gross", df.format(unit));
			return tradeItem;
		}catch (Exception e) {
			throw new SystemException(e,"getTradeItem",log);
		}
	}
	private long insertTrade(HttpServletRequest request,long oid,long address_id, String orderSource, String allInfo) throws SystemException {
		try{
			DBRow insertTradeRow = this.getTradeDBRow(request,allInfo);
			// 这里的Client_id 现在是读取request中的payer_email中的值。这个还有一个情况 在关联交易的时候是 读取关联对象的Client_id
			// 因为是建立订单的时候所以 relation_type 是 关联对象类型 0订单 1客服Id
			insertTradeRow.add("client_id", StringUtil.getString(request,"payer_email"));
			insertTradeRow.add("relation_type", 0);
			insertTradeRow.add("relation_id", oid);
			insertTradeRow.add("address_id", address_id);
			insertTradeRow.add("order_source", orderSource);
			insertTradeRow.add("is_create_order", 1);
			return this.insertTradeAndNotExits(insertTradeRow);
		}catch(Exception e){
			throw new SystemException(e,"insertTrade",log);
		}
	}
	
	
	private void insertClientAndNotExits(DBRow insertClientRow) throws Exception {
		try{
			DBRow row = floorOrderMgrZr.getClientRowByEmailId(insertClientRow.getString("client_id"));
			if(row == null){
				floorOrderMgrZr.insertClientByRow(insertClientRow);
			}
			
		}catch(Exception e){
			throw new SystemException(e,"insertClientAndNotExits",log);
		}
	}
	
	private DBRow getTradeDBRow(HttpServletRequest request,String allInfo) throws SystemException{
		try {
			String custom = StringUtil.getString(request, "custom");
			DBRow insertTradeRow = new DBRow();
			insertTradeRow.add("payment_date", StringUtil.getString(request, "payment_date"));
			insertTradeRow.add("mc_gross", StringUtil.getFloat(request, "mc_gross"));
			insertTradeRow.add("mc_fee", StringUtil.getFloat(request, "mc_fee"));
			float shipping = StringUtil.getFloat(request, "mc_shipping");
			if(shipping <= 0){
				shipping = StringUtil.getFloat(request, "shipping");
			}
			insertTradeRow.add("mc_shipping",shipping );
			insertTradeRow.add("mc_handling", StringUtil.getFloat(request, "mc_handling"));
			insertTradeRow.add("tax", StringUtil.getFloat(request, "tax"));
			insertTradeRow.add("mc_currency", StringUtil.getString(request, "mc_currency"));
			insertTradeRow.add("payment_status", StringUtil.getString(request, "payment_status"));
			insertTradeRow.add("payment_type", StringUtil.getString(request, "payment_type"));
			insertTradeRow.add("txn_id", StringUtil.getString(request, "txn_id"));
			insertTradeRow.add("protection_eligibility", StringUtil.getString(request, "protection_eligibility"));
			insertTradeRow.add("receiver_email", StringUtil.getString(request, "receiver_email"));
			insertTradeRow.add("business", StringUtil.getString(request, "business"));
			insertTradeRow.add("receiver_id", StringUtil.getString(request, "receiver_id"));
			insertTradeRow.add("payer_email", StringUtil.getString(request, "payer_email"));
			insertTradeRow.add("payer_id", StringUtil.getString(request, "payer_id"));
			insertTradeRow.add("transaction_type", TransactionType.Payment);
			
			insertTradeRow.add("auction_buyer_id", StringUtil.getString(request, "auction_buyer_id")); 
			insertTradeRow.add("contact_phone", StringUtil.getString(request, "contact_phone"));
			insertTradeRow.add("all_info", allInfo);
			insertTradeRow.add("post_date", DateUtil.NowStr());
			insertTradeRow.add("pending_reason", StringUtil.getString(request, "pending_reason"));
			
			
			//这里要判断Custom字段是不是包含有paypaldp 如果有的话那么就是在付款界面通过信用卡付款
			
			if(custom.indexOf("paypaldp") != -1){
				// 读取Ip地址
				 
				 String idStr = custom.split("ip=")[1];
				 int idIndex = idStr.indexOf(",");
				 String id = "";
				 if(idIndex != -1){
					 id = (idStr.substring(0, idIndex));
				 }else{
					 id = (idStr);
				 }
				insertTradeRow.add("ip_address", id);
				insertTradeRow.add("payment_channel", PaymentMethodKey.CC);
				//在交易上添加上信用卡的信息 . 要测试直接付款。。和 payForOrder的情况
				//信用卡付款的时候的PayerEmail是卡的一些基本的信息。和website保持一致
				String txnId = StringUtil.getString(request, "txn_id");
				if(txnId.length() > 0 ){
					DBRow CreadCard = floorBillMgrZr.getVertualterminalRowByTxnId(txnId);
					if(CreadCard != null){
						insertTradeRow.add("cc_owner", CreadCard.getString("first_name") + " " + CreadCard.getString("last_name"));
						insertTradeRow.add("cc_cvv", CreadCard.getString("cvv2"));
						insertTradeRow.add("cc_number", CreadCard.getString("acct"));
						insertTradeRow.add("cc_expires", CreadCard.getString("expdate"));
						StringBuffer payerEmail = new StringBuffer();
						payerEmail.append(insertTradeRow.getString("cc_owner")).append("|").append(insertTradeRow.getString("cc_number")).append("|").append(insertTradeRow.getString("cc_expires"));
						insertTradeRow.add("payer_email", payerEmail.toString());
						// 读取Note 字段
						insertTradeRow.add("note", CreadCard.getString("note"));
					}
				}
				
				
			}else{
				insertTradeRow.add("payment_channel", PaymentMethodKey.PP);
			}
			
			insertTradeRow.add("payment_method", new PaymentMethodKey().getStatusById(PaymentMethodKey.PP));
			// 新添加字段address_name,address_street,address_city,address_state,address_zip,address_country_code,address_country,address_status
			String[] addressType = new String[]{"delivery_","billing_"};
			String[] addressInfo = new String[]{"address_name","address_street","address_city","address_state","address_zip","address_country_code","address_country","address_status"};
			fixDBRowAddSellerAndBuyer(insertTradeRow,request);
			for(String type : addressType){
				boolean isBilling = type.indexOf("billing") != -1 ? true:false; 
				for(String address : addressInfo){
					String fixAddress = isBilling? type+address : address;
					insertTradeRow.add(type+address, StringUtil.getString(request, fixAddress));
				}
			}
			// 获取delivery_state_id,delivery_country_id,billing_state_id,billing_country_id
			setCcidAndProId(request,insertTradeRow,"delivery_");
			setCcidAndProId(request,insertTradeRow,"billing_");
			
			
			
			return insertTradeRow;
		} catch (Exception e) {
			throw new SystemException(e,"getTradeDBRow",log);
		}
	}
	private void setCcidAndProId(HttpServletRequest request , DBRow row, String type) throws SystemException{
		try{
			boolean isBilling = type.indexOf("billing") != -1 ?true:false;
			String fix = isBilling?"billing_":"";
			
			DBRow result = this.setCcidAndProIdByStringValue(StringUtil.getString(request,fix+"address_country"), 
					StringUtil.getString(request,fix+"address_zip"), StringUtil.getString(request,fix+"address_state"), StringUtil.getString(request,"residence_country"));
			if(result != null){
				row.add(type+"ccid_id", result.get("ccid_id", 0));
				row.add(type+"pro_id", result.get("pro_id", -1));
				if(result.getString("address_zip").length() > 0){
					row.add(type+"address_zip", result.getString("address_zip"));
				}
				if(result.getString("address_state").length() > 0){
					row.add(type+"address_state", result.getString("address_state"));
				}
				
			}
			 
			/*	DBRow countryCode = fom.getDetailCountryCodeByCountry(StrUtil.getString(request,fix+"address_country").trim());
			if (countryCode==null){
				row.add(type+"ccid_id",0);
				row.add(type+"pro_id",-1);
			}
			else{
				if(countryCode.get("ccid", 0l)==11036){
					//美国订单将邮编截取前5位
					String zip = StrUtil.getString(request,fix+"address_zip");
					if(zip.length()>5){
						zip = zip.substring(0,5);
					}
					row.add(type+"address_zip",zip);
				}
				row.add(type+"ccid_id",countryCode.get("ccid", 0l));
				if (StrUtil.getString(request,fix+"address_state").trim().equals("")){
					row.add(type+"pro_id",-1);
				}
				else{
					//获得省份ID
					DBRow detailProvince = fpm.getDetailProvinceByNationIDPCode(countryCode.get("ccid", 0l),StrUtil.getString(request,fix+"address_state").trim());//通过国家ID，省份/州缩写查询
					if(detailProvince == null)	{
						//判断是否可以通过缩写查询到对应State的ID
						detailProvince = fpm.getDetailProvinceByNationIDPName(countryCode.get("ccid",0l),StrUtil.getString(request,fix+"address_state").trim());//通过国家ID，省份/州缩写查询
					}
					
					if (detailProvince==null){
						row.add(type+"pro_id",-1);
					}
					else{
						row.add(type+"pro_id",detailProvince.get("pro_id", 0l));
						row.add(type+"address_state",detailProvince.getString("pro_name"));
					}
				}
			}*/
			
			
		}catch(Exception e){
			throw new SystemException(e,"setCcidAndProId",log);
		}
	}
	
	private void fixDBRowAddSellerAndBuyer(DBRow row, HttpServletRequest request) 
		throws SystemException
	{
		  try
		  {
			  String custom = StringUtil.getString(request, "custom");
			  int buyerId = custom.indexOf("auction_buyer_id=");
			  if(buyerId != -1){
				  String buyerIdStr = custom.split("auction_buyer_id=")[1];
				  int buyerIdIndex = buyerIdStr.indexOf(",");
				  String buyer = "";
				  if(buyerIdIndex != -1){
		     
		      
					  buyer = buyerIdStr.substring(0,buyerIdIndex);
				  }else{
					  buyer = buyerIdStr;
				  }
				  row.add("auction_buyer_id", buyer);
			  }
		 
		  } 
		  catch (Exception e) 
		  {
		   throw new SystemException(e,"fixDBRowAddSellerAndBuyer",log);
		  }
	}
	/**
	 * @param request
	 * @param clientType
	 * @throws SystemException
	 * 2.如果是新添加的Client记录 直接添加一条记录在Client_address_info (client_id)
	 * 			更新的Client记录。这个时候是判断address_name,address_street,contact_phone 是不是都相等。
	 * 			如果不相等的话就是要添加一条记录同时把赋值上去
	 */
	private long handleClientAddressInfo(HttpServletRequest request , String clientType) throws SystemException{
		String client_id = StringUtil.getString(request,"payer_email");  
		try{
			long address_id = 0l;
			if(clientType.equals("insert")){
				DBRow insertRow = getClientAddressInfo(request,client_id);
				address_id = floorOrderMgrZr.insertClientAddressInfo(insertRow);
			}else if(clientType.equals("update")){
				//这个时候就是要判读是不是 应该添加一条。这个时候可能这个client_id对应了很多的记录 都是要做比较的
				// 有一个相等就不添加了
				DBRow[] rows = floorOrderMgrZr.getClientByEmail(client_id);
				String address_name = StringUtil.getString(request, "address_name");
				String address_street = StringUtil.getString(request, "address_street");
				String contact_phone = StringUtil.getString(request, "contact_phone");
				
				if(rows != null) {
					boolean flag = false;
					for(DBRow row : rows){
						if( address_name.equals(row.getString("address_name")) && contact_phone.equals(row.getString("contact_phone")) &&  address_street.equals(row.getString("address_street"))){
							flag = false;	
							address_id = row.get("address_id", 0l);
							break;
						}else{
							flag = true ;
						}
					}
					if(flag){
						DBRow insertRow = getClientAddressInfo(request,client_id);
						address_id = floorOrderMgrZr.insertClientAddressInfo(insertRow);
					}
				}
			 
			}
			return address_id;
		}catch(Exception e){
			throw new SystemException(e,"handleClientAddressInfo",log);
		}
	}
	
	private DBRow getClientAddressInfo(HttpServletRequest request ,String client_id){
		DBRow insertRow = new DBRow();
		
		insertRow.add("address_name", StringUtil.getString(request, "address_name"));
		////system.out.println(StrUtil.getString(request, "address_name") + "address_name");
		insertRow.add("address_street", StringUtil.getString(request, "address_street"));
		insertRow.add("address_city", StringUtil.getString(request, "address_city"));
		insertRow.add("address_state", StringUtil.getString(request, "address_state"));
		insertRow.add("address_zip", StringUtil.getString(request, "address_zip"));
		insertRow.add("address_country_code", StringUtil.getString(request, "address_country_code"));
		insertRow.add("address_country", StringUtil.getString(request, "address_country"));
		insertRow.add("residence_country", StringUtil.getString(request, "residence_country"));
		insertRow.add("contact_phone", StringUtil.getString(request, "contact_phone"));
		insertRow.add("address_status", StringUtil.getString(request, "address_status"));
		insertRow.add("client_id", client_id);
		return insertRow;
	}
	/**
	  * 1.看客户的信息是不是存在,如果是存在的话就更新。如果是不存在的话就要添加一条记录(添加的时候主要要把Client_id 赋值成payer_email)
	  * 	a.根据payer_email 当成Client_id去查询
	  */
	private String handleClient(HttpServletRequest request)  throws SystemException{
		try{
			String payer_email  = StringUtil.getString(request, "payer_email");
			 DBRow clientRow = floorOrderMgrZr.getClientRowByEmailId(payer_email.trim());
			 DBRow updateAndInsertRow = new DBRow();
			 updateAndInsertRow.add("first_name", StringUtil.getString(request, "first_name"));
			 updateAndInsertRow.add("last_name", StringUtil.getString(request, "last_name"));
			 updateAndInsertRow.add("payer_status", StringUtil.getString(request, "payer_status"));
			 if(clientRow != null ){
				 // 这个时候是要更新一条记录的 ?  这个时候是client_id 也是要更新的？

				 String clientId = clientRow.getString("client_id");
				 floorOrderMgrZr.updateClientByClientIdAndRow(clientId, updateAndInsertRow);
				 return "update";
			 }else{
				 // 添加一条记录
				 updateAndInsertRow.add("client_id", payer_email);
				 updateAndInsertRow.add("payer_email", payer_email);
				 updateAndInsertRow.add("payer_id", StringUtil.getString(request, "payer_id"));
				
				 this.insertClientAndNotExits(updateAndInsertRow);
				 return "insert";
			 }
			
		}catch(Exception e){
			throw new SystemException(e,"handleClient",log);
		}
		
	}
	private boolean isPermit(HttpServletRequest request){
		String txn_id = StringUtil.getString(request,"txn_id");
		if (txn_id.trim().equals("")){
			return(false);
		}
		else{
			return(true);
		}
	}
	// 放回一个交易的类型 
	/*
	 *   首先是判断有没有ivnoiceId . && BillType 为ORder 那么就是关联交易。。。如果是在custom 字段中有ORder 字段也是输入关联交易。这种是没有parent_txn_id 的
	 *   如果是改变状态的时候payment_status 这个时候也是没有parent_txn_id 。txn_id和以前的一样.应该按照关联交易处理
	 */
	@Override
	public String tradeType(HttpServletRequest request) throws Exception {
		 String parent_txn_id = StringUtil.getString(request,"parent_txn_id");
		 //应该去数据库里面查询txn_id已经有了。有的话那么就应该是管理交易
		 String txnId = StringUtil.getString(request,"txn_id");
		 DBRow  rows = floorOrderMgrZr.getTradeBillByTxnIdAndCreateOrder(txnId);
		 if(rows != null){
			 return OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE;
		 }
		 
		 if(parent_txn_id.trim().length() < 1){
			 // 直接交易
			 long invoice = StringUtil.getLong(request, "invoice_number");
			 String custom = StringUtil.getString(request,"custom");
			 if(invoice ==0l && custom.indexOf("invoice") != -1){
				  int indexStart = custom.indexOf("invoice:");
				  invoice = Long.parseLong(custom.substring(indexStart+8));
			 }
			 if(invoice != 0 ){
				 DBRow billRow = this.getDetailById(invoice, "bill_order", "bill_id");
				 if(billRow != null && billRow.get("bill_type", 0) == BillTypeKey.order){
					 return OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE;
				 }
			 }
			 // 在付款界面为一个ORder 付款 custom字段是必须包含这个ORDER的
			 if(custom.indexOf("ORDER") != -1){
				return OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE;
			 }
			 // 如果是isDisputeOrder的时候的parent_txn_id 也是为空的 。那么就是关联交易
			 if(this.isDisputeOrder(request)){
				 return OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE;
			 }
			 return OrderMgrZr.ORDER_TRADE_TYPE_DIRECT;
			 
			 
		 }else{
			 
			 return OrderMgrZr.ORDER_TRADE_TYPE_RELEVANCE;
			 //去查询交易表中的记录 根据txn_id得到 Client_Id 。如果是client_id等于 request中的payer_email 那么就是关联交易。否则是间接交易
			 
		 }
		
		 
	}
	@Override
	public DBRow setCcidAndProIdByStringValue(String address_country,
			String address_zip, String address_state, String code) throws Exception {
		DBRow row = new DBRow();
		try{
			 
			DBRow countryCode = fom.getDetailCountryCodeByCountryOrCode(address_country.trim(),code);
			if (countryCode==null){
				row.add("ccid_id",0);
				row.add("pro_id",-1);
			}
			else{
				if(countryCode.get("ccid", 0l)==11036){
					//美国订单将邮编截取前5位
					if(address_zip.length()>5){
						address_zip = address_zip.substring(0,5);
					}
					row.add("address_zip",address_zip);
				}
				row.add("ccid_id",countryCode.get("ccid", 0l));
				if (address_state.trim().equals("")){
					row.add("pro_id",-1);
				}
				else{
					//获得省份ID
					DBRow detailProvince = fpm.getDetailProvinceByNationIDPCode(countryCode.get("ccid", 0l),address_state.trim());//通过国家ID，省份/州缩写查询
					if(detailProvince == null)	{
						//判断是否可以通过缩写查询到对应State的ID
						detailProvince = fpm.getDetailProvinceByNationIDPName(countryCode.get("ccid",0l),address_state.trim());//通过国家ID，省份/州缩写查询
					}
					
					if (detailProvince==null){
						row.add("pro_id",-1);
					}
					else{
						row.add("pro_id",detailProvince.get("pro_id", 0l));
						row.add("address_state",detailProvince.getString("pro_name"));
					}
				}
			}
			return  row;
		}catch (Exception e) {
			throw new SystemException(e,"setCcidAndProIdByStringValue",log);
		} 
	}
 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private void printLogDuplicate(HttpServletRequest request){
		String key ;
		Enumeration en = request.getParameterNames();
		while( en.hasMoreElements() ){
			key = (String)en.nextElement();
			duplicatepaypallog.info(  key+" = "+StringUtil.getString(request,key));
		}
	}
	
	private void printLogUnSuccess(HttpServletRequest request){
		String key ;
		Enumeration en = request.getParameterNames();
		while( en.hasMoreElements() ){
			key = (String)en.nextElement();
			paypallog.info("-----zhangrui----- "+ key+" = "+StringUtil.getString(request,key));
		}
	}
	// 打印信息到log 文件中
	private String printInfoToLog(HttpServletRequest request) throws SystemException,Exception{
		String str = null;
		String paramName;
		String paramValue;
		/**
		 * 在做任何处理前，先把paypal触发过来的参数打印出来
		 * 这样即使后面出现了问题，也能追踪到PAYPAL是否发过来订单，或者追踪系统没有的订单
		 */
		paypallog.info("================== DEBUG  "+StringUtil.getString(request, "payment_status")+" start ==================");
		Enumeration enu = request.getParameterNames();
		String key;
		StringBuffer backSb = new StringBuffer("cmd=_notify-validate");
		paypallog.info("request toString:"+request.toString());
		while( enu.hasMoreElements()){

			paramName = (String)enu.nextElement();
			paramValue = request.getParameter(paramName);//new String(.getBytes("ISO-8859-1"),"utf-8");
			
			backSb.append("&");
			backSb.append(paramName);
			backSb.append("=");
			
			try{
				backSb.append(URLEncoder.encode(paramValue,"utf-8"));
			} 
			catch (UnsupportedEncodingException e){
				throw new SystemException(e,"PaypalNotify UnsupportedEncodingException",log);
			}
			paypallog.info(paramName+" = "+paramValue);
		}
		paypallog.info("================== DEBUG "+StringUtil.getString(request, "payment_status")+" END ==================");
		paypallog.info("");
		paypallog.info("");
		
		str = backSb.toString();
		return str ;
	}
	// 验证IPN 是不是刚才发过来的那个
	private String volidateIPN(String txn_id ,String str) throws SystemException, Exception{
		URLConnection uc=null;
		String res=null;
	 
		try 
		{
			URL u = new URL(ConfigBean.getStringValue("paypal_action_url"));
			uc = u.openConnection();
			uc.setDoOutput(true);
			uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			PrintWriter pw = new PrintWriter(uc.getOutputStream());
			pw.println(str);
			pw.flush();
			pw.close();
			//paypal告诉你验证的结果
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			res = in.readLine();	
			in.close();
		} 
		catch (Exception e) {
			paypallog.error("txn_id:"+txn_id+"Communicating with PAYPAL IPN Verifiy Processing Error");
			throw e;
		}
		if (res==null){
			res="Null";
		}
		return res;
	}
	/**
	 * @param request
	 * @return
	 * @throws SystemException
	 * @throws Exception(目前没有考虑customer 字段的值)
	 */
	private String getOrderSource(HttpServletRequest request) throws SystemException, Exception {
		try{
			String order_source = "";
			//判断订单来源
			String custom = StringUtil.getString(request, "custom");
			String auction_buyer_id = StringUtil.getString(request, "auction_buyer_id");
			String invoiceId = StringUtil.getString(request, "invoice_id");				//paypal return ;
			String invoiceNumber = StringUtil.getString(request, "invoice_number");   	//billId
			String txn_type = StringUtil.getString(request,"txn_type");
			
			String item_number = StringUtil.getString(request,"item_number");
			   //如果是有ItemNumber 的就算是ebay 单子
			if (!auction_buyer_id.equals("") || !item_number.equals(""))
			{
			   order_source = OrderMgrZr.ORDER_SOURCE_EBAY;
			}
			else if(txn_type != null && txn_type.equalsIgnoreCase("invoice_payment") && invoiceId.length() > 0 && invoiceNumber.length() > 0){	
				// 查询出是Ebay 或者是 product,shipping 
				// 对应的是ebay_invoice,invoice
				long billId = Long.parseLong(invoiceNumber);
				
				 return getInvoiceSourceByBillId(billId);
			}else{
				if(custom.indexOf(":WEBSITE")>=0){
					order_source = OrderMgrZr.ORDER_SOURCE_WEBSITE;
				}else{
					//如果是在vvme.com/payment网站上通过付款Invoice。
					if(custom.indexOf("invoice") != -1){
					  int indexStart = custom.indexOf("invoice:");
					  long  billId = Long.parseLong(custom.substring(indexStart+8));
					  order_source = getInvoiceSourceByBillId(billId);
					}else if(custom.indexOf(":EBAY") != -1){
						order_source = OrderMgrZr.ORDER_SOURCE_EBAY;
					} else if(custom.indexOf(":ORDER") != -1){
						order_source = OrderMgrZr.ORDER_SOURCE_ORDER;
					}else{
						// 为了兼容以前的订单 直接付款类的
						order_source = OrderMgrZr.ORDER_SOURCE_DIRECTPAY;
					}
				}
			}
			return order_source;
		}catch (Exception e) {
			throw new SystemException(e,"getOrderSource",log);
		}
	}
	private String getInvoiceSourceByBillId(long billId) throws Exception {
		try{
			DBRow billRow = floorBillMgrZr.getBillByBillId(billId);
			int billType = billRow.get("bill_type", 0);
			if(billType == 0){
				throw new RuntimeException("bill type error ");
			}
			if(billType == BillTypeKey.ebay){
				return OrderMgrZr.ORDER_SOURCE_EBAY_INVOICE;
			}else if(billType == BillTypeKey.order){
				return OrderMgrZr.ORDER_SOURCE_ORDER;
			}else if(billType == BillTypeKey.warranty){
				return OrderMgrZr.ORDER_SOURCE_WANRRANTY;
			}else{
				return OrderMgrZr.ORDER_SOURCE_INVOICE;
			}
		}catch (Exception e) {
			throw new SystemException(e,"getInvoiceSourceByBillId",log);
		}
	}
	@Override
	public DBRow[] getAllTrade(PageCtrl page) throws Exception {
		try{
			 return floorOrderMgrZr.getAllTrade(page);
		}catch (Exception e) {
			throw new SystemException(e,"getAllTrade",log);
		}
	}
	@Override
	public DBRow[] getQuartationOrderAndItemsById(long quartationOrderId)
			throws Exception {
		try{
			 return floorOrderMgrZr.getQuartationOrderAndItemById(quartationOrderId);
		}catch (Exception e) {
			throw new SystemException(e,"getQuartationOrderAndItemsById",log);
		}
	}
	@Override
	public DBRow[] getWarrantyOrderAndItemsById(long warrantyOrderId)
			throws Exception {
		try{
			 return floorOrderMgrZr.getWarrantyOrderAndItemById(warrantyOrderId);
		}catch (Exception e) {
			throw new SystemException(e,"getWarrantyOrderAndItemsById",log);
		}
	}
	/**
	 * 从session获得购物车商品
	 * @return
	 * @throws Exception 
	 */
	private DBRow[] getSimpleProducts(HttpSession session) 
		throws Exception
	{
		if ( session.getAttribute(Config.cartQuoteSession) != null )
		{
			ArrayList al = (ArrayList)session.getAttribute(Config.cartQuoteSession);
			
			//深层克隆
			ArrayList newAL = new ArrayList();
			for (int i=0; i<al.size(); i++)
			{
				DBRow alRow = (DBRow)al.get(i);
				DBRow newRow = new DBRow();
				newRow.append(alRow);
				
				newAL.add(newRow);
			}
			
			return((DBRow[])newAL.toArray(new DBRow[0]));
		}
		else
		{
			return(new DBRow[0]);
		}
	}
	/**
	 * 在这个方法里面去计算商品 成本 ,建议报价 
	 * 然后根据客服自己输入的实际的报价去计算小计
	 *  cart_pid  
		cart_quantity 
	 	cart_product_type 
	 	price_type 
	 */
	@Override
	public DBRow[] flushNew(HttpSession session, String payer_email,long sc_id, long ccid, long pro_id, long ps_id) throws Exception {
		ArrayList<DBRow> cartAl = (ArrayList<DBRow>)session.getAttribute(Config.cartQuoteSession);
		DBRow products[] = this.getSimpleProducts(session);
		if(products != null  && products.length > 0){
			for(DBRow row : products){
				DBRow  product = floorOrderMgrZr.getProductById(row.get("cart_pid", 0l)); 
				row.add("p_name", product.getString("p_name"));
				row.add("unit_name", product.getString("unit_name"));
				//成本
				row.add("unit_price",product.get("unit_price", 0.0d));
				//计算建议报价
				/**
				 * 1.查询一个具体的地区的 具体商品的毛利润  + 成本
				 * 2.如果是定制的商品就是要查询出来这个原始的 (总和成本 + 总和毛利润) -/+ (减去/加上 成本 和毛利润),
				 * 3.先只考虑 不是定制的商品的
				 */
				double advicePrice = 0.0d;
				if(product.get("orignal_pc_id", 0l) != 0l)
				{
				 
					double cc = getSetProductProfit(product.get("pc_id", 0l),product.get("orignal_pc_id", 0l),ccid,pro_id,ps_id);
					advicePrice = product.get("unit_price", 0.0d) + cc;
					//system.out.println(advicePrice);
				}
				else
				{
					long pc_id = product.get("pc_id", 0l);
					DBRow profit = floorOrderMgrZr.getProductProfit(ccid, pc_id);
					double fixProfit = 0; 
					
					if(profit != null )
					{
						fixProfit = profit.get("profit", 0.0d);
					} 
					else
					{
						fixProfit = quoteMgrZJ.getProductProfitValue(ccid, pro_id, pc_id, ps_id, 1);
					}
						
					advicePrice  = fixProfit + product.get("unit_price", 0.0d);
				}
				row.add("weight", product.get("weight", 0.0f));
				//if(row.get("actual_price", 0.0d) <= 0){
					row.add("actual_price", (double)Math.round(advicePrice*100* 1.1)/100);
				//}
				row.add("advice_price", (double)Math.round(advicePrice*100)/100);
				row.add("unit_name", product.getString("unit_name"));
				
				// 长宽高
				row.add("length",product.get("length",0f) );
				row.add("width",product.get("width",0f) );
				row.add("heigth",product.get("heigth",0f) );
	 
				 
 
			}
			 
		}
		return products;
	}
	/**
	 *  根据BillID 重新构造购物车
	 */
	@Override
	public void addItemsInCart(HttpSession session, long billId , long ps_id , double rate)
			throws Exception {
		
		 DBRow[] rows = floorOrderMgrZr.getBillItemByBillOrderId(billId);
		 if(rows != null  && rows.length > 0){
			// ArrayList<DBRow> cartAl = (ArrayList<DBRow>)session.getAttribute(Config.cartQuoteSession);
			 ArrayList<DBRow> cartAl = new ArrayList<DBRow>();
			 for(DBRow row : rows){
				 DBRow cartRow = new DBRow();
				 cartRow.add("cart_pid", row.get("pc_id", 0l));
				 cartRow.add("cart_quantity", row.get("quantity", 0.0f));
				 
				 cartRow.add("price_type", "1");
				 cartRow.add("cart_product_type", row.get("product_type", 0));
				 cartRow.add("amount", row.get("amount", 0.0d) * rate);
				 cartRow.add("actual_price", row.get("actual_price", 0.0d) * rate);
				 cartRow.add("bill_item_id", row.get("bill_item_id", 0l));
				 cartAl.add(cartRow);
			 }
			 
			 session.setAttribute(Config.cartQuoteSession, cartAl);
			 session.setAttribute(Config.cartQuoteSession+"_ps_id", ps_id);
		 }
		
		
 
	}
	// 计算定制商品的建议报价
	/**
	 * 1. 先查询出原始的套装的 毛利润 products_profit
	 * 2. 查询出原始套装的 组成关系
	 * 3. 比较新的套装 于原始套装的差异
	 * 		a.如果是新添加了商品 那么 = products_profit +　newProduct * profit * number
	 * 		b.如果是添加了多的数量 那么 = products_profit +　newProduct * profit * number
	 * 		c.如果是删除了某个物品 那么 = products_profit +　newProduct * profit * number
	 * 		d.如果是减少了多的数量 那么 = products_profit +　newProduct * profit * number
	 */
	private double getSetProductProfit(long pc_id , long orignal_pc_id ,long ccid,long pro_id,long ps_id) throws Exception {
		try{
			DBRow BasicProfitRow = floorOrderMgrZr.getProductProfit(ccid, orignal_pc_id);
			double basicProfit = BasicProfitRow != null ? BasicProfitRow.get("profit", 0.0d) : 0.0d;
			DBRow[] basicDBRows = floorOrderMgrZr.getSetProductsBy(orignal_pc_id);
			DBRow[] newDBRows = floorOrderMgrZr.getSetProductsBy(pc_id);
			// 计算 basicDBRows 和 newDBRows 中数据的差别
			Map<Long,Double> map = new HashMap<Long,Double>();
			for(DBRow row : newDBRows)
			{
				map.put(row.get("pid", 0l), row.get("quantity", 0.0d));
			}
			
			for(DBRow row : basicDBRows)
			{
				long pcId = row.get("pid", 0l);
				if(pcId != 0l)
				{
					if(map.get(pcId) != null)
					{
						// 表示存在 商品 就是添加/减少了数量 
						double count = row.get("quantity", 0.0d);
						Double lastCount = map.get(pcId);
						if(lastCount == count)
						{
							//表示的是没有改变数量
							map.remove(pcId);
						}
						else
						{
							map.put(pcId, Math.abs(lastCount - count));//商品数量改动，不管增加或减少，都增加这个商品对定制套装的毛利
						}
					}
					else
					{
						// 表示不存在  是 已经删除了一个套装
						map.put(row.get("pid", 0l), row.get("quantity", 0.0d));
					}
				}
			}
			// 计算新添加的建议 毛利率
			double addProfit = 0.0d;
			 if(!map.isEmpty())
			 {
				Set<Map.Entry<Long,Double>> entry = map.entrySet();
				for(Iterator<Map.Entry<Long,Double>>  it = entry.iterator() ; it.hasNext() ; ){
					 Map.Entry<Long, Double> en = it.next();
					 
					 DBRow profit = floorOrderMgrZr.getProductProfit(ccid, en.getKey());
					 if(profit != null)
					 {
						 addProfit +=  en.getValue() * profit.get("profit", 0.0d);
					 }
					 else
					 {
						 addProfit += en.getValue() * quoteMgrZJ.getProductProfitValue(ccid, pro_id, en.getKey(), ps_id,1);
					 }
				}
			 }
			 return addProfit + basicProfit;
		}catch (Exception e) {
			throw new SystemException(e,"getSetProductProfitAndUnitPrice",log);
		}
	}
	@Override
	public DBRow getDetailById(long id, String tableName, String pk)
			throws Exception {
		try{
			 return floorOrderMgrZr.getDBRowByIdAndTableName(id,tableName,pk);
		}catch (Exception e) {
			throw new SystemException(e,"getDetailById",log);
		}
	}
	@Override
	public DBRow[] getAddressInfoListByClientId(String client_id)
			throws Exception {
		try{
			 return floorOrderMgrZr.getAddressInfoListByClientId(client_id);
		}catch (Exception e) {
			throw new SystemException(e,"getAddressInfoListByClientId",log);
		}
	}
	@Override
	public DBRow getClientInfoByEmail(String email) throws Exception {
		try{
			 return floorOrderMgrZr.getClientRowByEmailId(email);
		}catch (Exception e) {
			throw new SystemException(e,"getClientInfoByEmail",log);
		}
	}
	
	@Override
	public DBRow addOrderAndTradeByHandle( HttpServletRequest request)
			throws Exception {
		// 从request 对象构造出OrderDBRow 和 TradeDBRow
		// 先插入Order 然后插入交易,在交易上关联订单
		DBRow result = new DBRow();
		try{
			long pro_id = StringUtil.getLong(request, "pro_id");
			int pay_type = StringUtil.getInt(request, "pay_type");
			String payment_status = StringUtil.getString(request,"payment_status");
			String business = StringUtil.getString(request,"business");
			String quantity = StringUtil.getString(request,"quantity");
			String mc_gross = StringUtil.getString(request,"mc_gross");
			String address_name = StringUtil.getString(request,"address_name");
			String address_street = StringUtil.getString(request,"address_street");
			String address_city = StringUtil.getString(request,"address_city");
			String address_state = StringUtil.getString(request,"address_state");
			String address_zip = StringUtil.getString(request,"address_zip");
			String address_country = StringUtil.getString(request,"address_country");
			String tel = StringUtil.getString(request,"tel");
			String ccid = StringUtil.getString(request,"ccid");
			String mc_currency = StringUtil.getString(request,"mc_currency");
			String txn_id = StringUtil.getString(request,"txn_id");
			String order_source = StringUtil.getString(request,"order_source");
			
			
			DBRow orderDBRow = new DBRow();
			
			orderDBRow.add("payment_status",payment_status);
			orderDBRow.add("business",business);
			orderDBRow.add("quantity",quantity);
			orderDBRow.add("mc_gross",mc_gross);
			orderDBRow.add("address_name",address_name);
			orderDBRow.add("address_street",address_street);
			orderDBRow.add("address_city",address_city);
			orderDBRow.add("address_state",address_state);
			orderDBRow.add("address_zip",address_zip);
			orderDBRow.add("address_country",address_country);
			orderDBRow.add("tel",tel);
			orderDBRow.add("ccid",ccid);
			orderDBRow.add("mc_currency",mc_currency);
			orderDBRow.add("txn_id",txn_id);
			orderDBRow.add("order_source",order_source);
			
			//添加客户
			orderDBRow.add("client_id", StringUtil.getString(request, "client_id"));
			// business,pay_type
			String txnId = orderDBRow.getString("txn_id", "");
			if(txnId.length() > 0){
				DBRow volidate =	fom.getDetailPOrderByTxnid(txnId);
				if(volidate != null){
					throw new TxnIdExistException(orderDBRow.getString("txn_id"));
				}
			}
			
			// 检查txn_id是不是已经存在了
			orderDBRow.add("handle",1); 	
			orderDBRow.add("handle_status",0);
			orderDBRow.add("ip",request.getRemoteAddr());
			orderDBRow.add("post_date",DateUtil.NowStr());
			orderDBRow.add("pay_type",pay_type);
			orderDBRow.add("pro_id",pro_id);
			
			long oid = fom.addPOrder(orderDBRow);
			//地址矫正  还有其他的一些东西。
			ordersMgrQLL.addressValidateFedexJMS(oid,orderDBRow.getString("address_street"),orderDBRow.getString("address_zip"));
			OrderIndexMgr.getInstance().addIndex(orderDBRow.getString("address_name"),orderDBRow.getString("client_id"),oid,orderDBRow.getString("txn_id"),orderDBRow.getString("auction_buyer_id"),orderDBRow.getString("item_number"),"",orderDBRow.getString("address_country"));

			
			//添加交易
			DBRow tradeDBRow = new DBRow();
			tradeDBRow.add("order_source", StringUtil.getString(request, "order_source").toLowerCase());
			// 地址信息
			tradeDBRow.add("delivery_address_name", StringUtil.getString(request, "address_name"));
			tradeDBRow.add("delivery_address_street", StringUtil.getString(request, "address_street"));
			tradeDBRow.add("delivery_address_city", StringUtil.getString(request, "address_city"));
			tradeDBRow.add("delivery_address_state", StringUtil.getString(request, "address_state"));
			tradeDBRow.add("delivery_address_zip", StringUtil.getString(request, "address_zip"));
			tradeDBRow.add("delivery_address_country", StringUtil.getString(request, "address_country"));
			tradeDBRow.add("delivery_address_country_code", StringUtil.getString(request, "address_country_code"));
			tradeDBRow.add("delivery_pro_id", StringUtil.getString(request, "pro_id"));
			tradeDBRow.add("delivery_ccid_id", StringUtil.getString(request, "ccid"));
			tradeDBRow.add("payment_status", StringUtil.getString(request, "payment_status"));
			tradeDBRow.add("payment_type", StringUtil.getString(request, "payment_type"));
			tradeDBRow.add("payment_method", StringUtil.getString(request, "payment_method"));
			tradeDBRow.add("payment_channel", StringUtil.getInt(request, "payment_channel"));
			// 收款人账号id 
			tradeDBRow.add("account_payee", StringUtil.getString(request, "account_payee"));
			tradeDBRow.add("contact_phone", StringUtil.getString(request, "tel"));
			tradeDBRow.add("ip_address", request.getRemoteAddr());
			tradeDBRow.add("business", StringUtil.getString(request,"business"));			  //收款账号
			tradeDBRow.add("receiver_email", StringUtil.getString(request,"receiver_email")); // 收款人

			// 设置关联
			tradeDBRow.add("relation_type", 0);
			tradeDBRow.add("relation_id", oid);
			tradeDBRow.add("payment_date", DateUtil.NowStr());
			tradeDBRow.add("txn_id", StringUtil.getString(request, "txn_id"));
			tradeDBRow.add("mc_gross", StringUtil.getDouble(request, "mc_gross"));
			tradeDBRow.add("mc_currency", StringUtil.getString(request, "mc_currency"));
			tradeDBRow.add("payer_email", StringUtil.getString(request, "payer_email"));
			tradeDBRow.add("client_id", StringUtil.getString(request, "payer_email"));
			//手工创建订单的时候 如果没有client_id 应该要去新建立一条数据
			tradeDBRow.add("is_create_order", 1);
			tradeDBRow.add("post_date", DateUtil.NowStr());
			tradeDBRow.add("transaction_type", TransactionType.Payment);
			long tradeId = this.insertTradeAndNotExits(tradeDBRow);
			//	floorOrderMgrZr.insertTrade(tradeDBRow);
			
			result.add("trade_id", tradeId);
			result.add("flag", "success");
			result.add("oid", oid);
			return result;  
		}catch(TxnIdExistException e){
			throw e;
		}catch (Exception e) {
			throw new SystemException(e,"addOrderAndTradeByHandle",log);
		}
		
		
	}
	@Override
	public DBRow getOrderByTxnId(String txn_id) throws Exception {
		try{
			 return fom.getDetailPOrderByTxnid(txn_id);
		}catch (Exception e) {
			throw new SystemException(e,"getOrderByTxnId",log);
		} 
	}
	@Override
	public DBRow[] getRelationTradeByOid(long oid) throws Exception {
		try{
			return floorOrderMgrZr.getRelationTradeByTxnId(oid,false);
		}catch (Exception e) {
			throw new SystemException(e,"getRelationTradeByOid",log);
		} 
	}
	@Override
	public DBRow[] getAllRelationTradeByOid(long oid) throws Exception {
		try{
			return floorOrderMgrZr.getRelationTradeByTxnId(oid,true);
		}catch (Exception e) {
			throw new SystemException(e,"getAllRelationTradeByOid",log);
		} 
	}
	@Override
	public DBRow[] getTradeItemByOid(long oid) throws Exception {
		try{
			return floorOrderMgrZr.getTradeItemsByOid(oid);
		}catch (Exception e) {
			throw new SystemException(e,"getTradeItemByOid",log);
		} 
	}
	@Override
	public void fixPayMentMethodAndPayChannal() throws Exception {
		try{
			PaymentMethodKey paymentMethodKey = new PaymentMethodKey();
			 DBRow[] rows = floorOrderMgrZr.getNeedFixRow();
			 for(DBRow row : rows){
				int pay =  row.get("pay_type",0);
				String payMentMethod = paymentMethodKey.getStatusById(pay);
				row.add("payment_method", payMentMethod);
				long tradeId = row.get("trade_id", 0l);
				row.remove("trade_id");
				 
				row.remove("pay_type");
				row.add("payment_channel", pay);
				floorOrderMgrZr.updateTradeByTradeId(row, tradeId);
			 }
		}catch (Exception e) {
			throw new SystemException(e,"fixPayMentMethodAndPayChannal",log);
		} 
		
	}
	@Override
	public DBRow[] filterTrade(HttpServletRequest request, PageCtrl pc)
			throws Exception {
		try{
			String st = StringUtil.getString(request, "st");
			String end = StringUtil.getString(request,"end");
			 
			int isCreateOrder =  StringUtil.getInt(request, "is_create_order");
			long relationId = StringUtil.getLong(request, "relation_id");
			String reasonCode = StringUtil.getString(request, "reason_code");
			String paymentStatus = StringUtil.getString(request, "payment_status");
			String caseType = StringUtil.getString(request,"case_type");
			return floorOrderMgrZr.filterTrade(pc, st, end,isCreateOrder,relationId,paymentStatus,caseType,reasonCode);
	 
		}catch (Exception e) {
			throw new SystemException(e,"filterTrade",log);
		} 
	}
	@Override
	public DBRow getTradeByTradeId(long tradeId) throws Exception {
		 try{
			 return floorOrderMgrZr.getTradeByTradeId(tradeId);
		}catch (Exception e) {
			throw new SystemException(e,"getTradeByTradeId",log);
		} 
	}
	public void setFloorReturnProductMgrZJ(
			FloorReturnProductMgrZJ floorReturnProductMgrZJ) {
		this.floorReturnProductMgrZJ = floorReturnProductMgrZJ;
	}
	public void setQuoteMgrZJ(QuoteMgrZJ quoteMgrZJ) {
		this.quoteMgrZJ = quoteMgrZJ;
	}
	
	
}
