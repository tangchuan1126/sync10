package com.cwc.app.api.zr;

import org.directwebremoting.ScriptSession;
import org.directwebremoting.ScriptSessionFilter;

public class UserScriptSessionFilter implements ScriptSessionFilter {
	
	private UserState userState ;
	
	public UserScriptSessionFilter(UserState userState){
		 this.userState = userState;
	}
	public boolean match(ScriptSession scriptSession) {
		Object object = scriptSession.getAttribute("userState");
		if(object != null){
			UserState user = (UserState)object;
			if(user.getUserId().equals(userState.getUserId()) && !user.isStop()){
				return true;
			}
			return false;
		}
		return false;
	}

}
