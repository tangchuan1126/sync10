package com.cwc.app.api.zr;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zr.FloorAndroidPushMgrZr;
import com.cwc.app.floor.api.zyj.FloorShortMessageMgrZyj;
import com.cwc.app.iface.zr.AndroidPushMgrIfaceZr;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.StringUtil;


public class AndroidPushMgrZr implements AndroidPushMgrIfaceZr{

	static Logger log = Logger.getLogger("ACTION");

     
    public FloorShortMessageMgrZyj floorShortMessageMgrZyj;
    
    public ShortMessageMgrZyjIFace shortMessageMgrZyj;
  
    private static int MAX_RETRY = 5;
    
    public FloorAndroidPushMgrZr floorAndroidPushMgrZr ;
    
    private static final String  myApiKey = "AIzaSyBaNrce88-oOzaCbg_hwogNASLEo2lYRJ8";

    
    private long addShortMessageInfo(AndroidPushMessge pushMessage , long senderAdid)  throws Exception{
    	try{
    		DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", pushMessage.getModuleKey());
			shortMessageInfo.add("process_id", pushMessage.getProcessKey());
			shortMessageInfo.add("business_id", pushMessage.getAssociate_id());
			shortMessageInfo.add("content", pushMessage.getContent());
			shortMessageInfo.add("send_user", senderAdid);//此值取当前登录系统的用户ID
  			shortMessageInfo.add("send_time", DateUtil.NowStr());
			long shortMessageId = floorShortMessageMgrZyj.addShortMessage(shortMessageInfo);
			return shortMessageId ;
    	}catch(Exception e){
			 throw new SystemException(e,"addShortMessageInfo",log);
    	}
    }
    /**
     * @param devices
     * @param androidPushMessage
     * @throws Exception
     * @author zhangrui
     * @Date   2014年11月10日
     */
    public void pushMessaeToDevice(DBRow[] devices , AndroidPushMessge androidPushMessage , long senderAdid) throws Exception{
    	try{
    		/**
    		 * 1.插入数据到short_message_info 
    		 * 2.发送信息
    		 * 3.插入数据到short_message_receiver 接收人 （同时多添加一个字段，push_server_message_id）google 返回给我的MessageID
    		*/  
    	/*	List<HoldDoubleValue<String, Long>> registerDevices = new ArrayList<HoldDoubleValue<String,Long>>();
    		if(devices != null && devices.length > 0){
    			for(DBRow tempuser : devices){
    				String tempRegisterId = tempuser.getString("register_id");
    				long ps_id = tempuser.get("ps_id", 0l);
    				int message_notify_flag = tempuser.get("message_notify_flag", 1);	//1 表示是接受信息
    				if(!StringUtil.isNull(tempRegisterId) && message_notify_flag == 1 ){
    					registerDevices.add(new HoldDoubleValue<String, Long>(tempRegisterId, ps_id));
    				}
    			}
    			
    			//插入short_message_info表中
    			long syncMessageId =  addShortMessageInfo(androidPushMessage, senderAdid);
    			Message message  =  androidPushMessage.getMessage();
     			if(registerDevices.size() > 0 ){
    				Sender sender = new Sender(myApiKey);
    	    		//MAX_RETRY ? 
    				for(HoldDoubleValue<String, Long> tempReceiveInfo : registerDevices){
    					
    					//message.getData().put("date", DateUtil.getCurrentLocalTimeNoYear(tempReceiveInfo.b));
    					Result result = sender.send(message, tempReceiveInfo.a, MAX_RETRY);
    					DBRow  receiver = findReciverByRegisterId(tempReceiveInfo.a, devices);
	    				if(receiver != null){
	    					//现在考虑的情况是，如果google 认为发送成功了，我就认为发送成功了。同时只会添加(有RegisterID的人) 
	    	    			//同时写会状态，同时写回messageId;
	    	    			int isExcuess = Integer.valueOf(StringUtils.isNotBlank(result.getErrorCodeName())?"1":"2"); 
    	    		   		DBRow tempReceiver = new DBRow();
    	    		   		tempReceiver.add("short_message_id", syncMessageId);
    	    		   		tempReceiver.add("receiver_id", receiver.get("adid", null));
    	    		   		tempReceiver.add("receiver_name", receiver.get("employe_name", null));
    	    		   		//tempReceiver.add("receiver_phone", receiver.get("register_id", null));
    	    		   		tempReceiver.add("is_success",isExcuess);
    	    		   		tempReceiver.add("android_push_message_id",result.getMessageId());
    	    		   		floorShortMessageMgrZyj.addShortMessageReceiver(tempReceiver);
	    				}
    				}
    			 
    			}
    		}
    		*/
    		
    	 }catch(Exception e){
			 throw new SystemException(e,"pushMessaeToDevice",log);
    	}
    }
    private DBRow findReciverByRegisterId(String registerId , DBRow[] receviers) {
    	if(receviers != null && receviers.length > 0){
    		for(DBRow tempRece : receviers){
    			String tempRegister = tempRece.getString("register_id");
    			if(tempRegister.equals(registerId)){
    				return tempRece ;
    			}
    		}
    	}
    	return null ;
    }
	@Override
	public void pushMessageToDevice(long business_id, long module_id,
			String msg, String targetIds, long adid)
			throws Exception {/*
 		DBRow[] rows = floorAndroidPushMgrZr.findRegisterIds(targetIds);
		List<Integer> errorNumb = new ArrayList<Integer>();
		List<String> devices = new ArrayList<String>();
		for(int n = 0; n < rows.length; n++){ 
			if(!StringUtil.isBlank(rows[n].get("register_id", ""))){
				devices.add(rows[n].get("register_id", ""));
			}else {
				errorNumb.add(n);
			}
		}
		//存入发送信息
		DBRow shortMessageInfo = new DBRow();
		shortMessageInfo.add("module_id", module_id);
		shortMessageInfo.add("business_id", business_id);
		shortMessageInfo.add("content", msg);
		shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sendTime = sdf.format(new Date());
		shortMessageInfo.add("send_time", sendTime);
		long shortMessageId = floorShortMessageMgrZyj.addShortMessage(shortMessageInfo);
		//开始发送
		String myApiKey = "AIzaSyBaNrce88-oOzaCbg_hwogNASLEo2lYRJ8";
		Sender sender = new Sender(myApiKey);
		Message message = new Message.Builder().addData("message", msg).build();
  		MulticastResult multicastResult = sender.send(message, devices, MAX_RETRY);
		List<Result> results = multicastResult.getResults();
		//推送后续操作
		int j = 0;
		int isExcuess = 0;
		for(int i = 0; i < devices.size(); i++){
			//存入接收信息
			DBRow receiver = new DBRow();
			receiver.add("short_message_id", shortMessageId);
			receiver.add("receiver_id", rows[i].get("adid", null));
			receiver.add("receiver_name", rows[i].get("employe_name", null));
			receiver.add("receiver_phone", rows[i].get("register_id", null));
			if(errorNumb.contains(i)){
				isExcuess = 3;
			}else{
				isExcuess = Integer.valueOf(StringUtils.isNotBlank(results.get(j).getErrorCodeName())?"1":"2");
				j++;
			}
			receiver.add("is_success", isExcuess);
			floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
			
			String regId = devices.get(i);
	        Result result = results.get(i);
	        
 	        String messageId = result.getMessageId();
			if (messageId != null) {
				String error = result.getErrorCodeName();
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId != null) {
		              // same device has more than on registration id: update it
					log.info("canonicalRegId " + canonicalRegId);
		              //Datastore.updateRegistration(regId, canonicalRegId);
	            }
			}else{
				String error = result.getErrorCodeName();
				if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
	                // application has been removed from device - unregister it
					log.info("Unregistered device: " + result.getMessageId());
	                floorAndroidPushMgrZr.updateRegisterReflection(Long.valueOf(rows[i].get("adid", null)), null);
                } else {
                	log.error("Error sending message to " + result.getMessageId() + ": " + error);
	            }
			}
		}
	*/}

	@Override
	public void updateAdmin(long adid, DBRow updateRow) throws Exception {
		try{
			 floorAndroidPushMgrZr.updateAdmin(adid,updateRow);
		}catch(Exception e){
			 throw new SystemException(e,"queryMessage",log);
		}
	}
	@Override
	public int countMessage(long adid, int status) throws Exception {
		try{
			return floorAndroidPushMgrZr.countMessage(adid, status);
		}catch(Exception e){
			 throw new SystemException(e,"countMessage",log);
		}
 	}
	@Override
	public DBRow[] queryMessage(long short_message_id, int pageCount, long adid, int short_message_status) throws Exception {
 		try{
  		 	return floorAndroidPushMgrZr.queryMessage(short_message_id, pageCount, adid, short_message_status);
 		}catch(Exception e){
			 throw new SystemException(e,"queryMessage",log);
 		}
 	}
	@Override
	public void updateRegisterReflection(long adid, String register)
			throws Exception {
		try{
			floorAndroidPushMgrZr.updateRegisterReflection(adid, register);
		}catch(Exception e){
			 throw new SystemException(e,"updateRegisterReflection",log);
		}
		
	}
	public FloorShortMessageMgrZyj getFloorShortMessageMgrZyj() {
		return floorShortMessageMgrZyj;
	}

	public void setFloorShortMessageMgrZyj(
			FloorShortMessageMgrZyj floorShortMessageMgrZyj) {
		this.floorShortMessageMgrZyj = floorShortMessageMgrZyj;
	}

	public ShortMessageMgrZyjIFace getShortMessageMgrZyj() {
		return shortMessageMgrZyj;
	}

	public void setShortMessageMgrZyj(ShortMessageMgrZyjIFace shortMessageMgrZyj) {
		this.shortMessageMgrZyj = shortMessageMgrZyj;
	}

	public void setFloorAndroidPushMgrZr(FloorAndroidPushMgrZr floorAndroidPushMgrZr) {
		this.floorAndroidPushMgrZr = floorAndroidPushMgrZr;
	}
	
}
