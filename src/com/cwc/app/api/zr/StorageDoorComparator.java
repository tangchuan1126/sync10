package com.cwc.app.api.zr;

import java.util.Comparator;

import com.cwc.db.DBRow;

/**
 * 先把这个类拿出来。现在是根据门的编号(Int)去做的排序
 * 他们有需求通过String去做
 * @author win7zr
 *
 */
public class StorageDoorComparator implements Comparator<DBRow>{

	@Override
	public int compare(DBRow o1, DBRow o2) {
		
		int o1DoorId = o1.get("doorId",0) ;
		int o2DoorId = o2.get("doorId",0) ;
		return  o1DoorId  > o2DoorId ? 1 : (o1DoorId == o2DoorId ? 0 : -1);
		
	}


}
