package com.cwc.app.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import us.monoid.json.JSONObject;

import com.cwc.app.api.qll.EbayMgr;
import com.cwc.app.api.zj.CartWaybill;
import com.cwc.app.api.zj.ProductCodeMgrZJ;
import com.cwc.app.api.zj.ProductStoreLogsDetailMgrZJ;
import com.cwc.app.beans.AddInProductBean;
import com.cwc.app.beans.AddShipProductBean;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuOrderBean;
import com.cwc.app.beans.ebay.qll.CompleteSaleParaBeans;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.exception.location.LocationCatalogNotExitsException;
import com.cwc.app.exception.product.ConvertNotBeSelfException;
import com.cwc.app.exception.product.ConvertNotSameTypeException;
import com.cwc.app.exception.product.ConvertQuantityIncorrectException;
import com.cwc.app.exception.product.CustomProductNotExistException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductIsInStorageException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.exception.product.StorageApproveIsExistException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.FloorSetupLogMgr;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.gql.CustomSeachConditionsIfaceGql;
import com.cwc.app.iface.zj.ProductStoreLocationMgrIFaceZJ;
import com.cwc.app.iface.zj.WayBillMgrIFaceZJ;
import com.cwc.app.iface.zwb.ProductLableTempMgrIfaceWFH;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.CountOperationKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.LableTemplateProductKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.OrderTaskKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.ProductFileTypeKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.SendResponseServerCodeAndMessageException;
import com.cwc.exception.SystemException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class ProductMgr implements ProductMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");
	private String tmp_pro_excel_path = "/administrator/";
	private String tmp_add_pro_excel_name = "tmp$import$add$pro$excel";

	private FloorProductMgr fpm;
	private FloorOrderMgr fom;
	private AdminMgr adminMgr;
	private TaskMgr taskMgr;
	private SystemConfig systemConfig ;
	private FloorExpressMgr floorExpressMgr;
	private WayBillMgrIFaceZJ wayBillMgrZJ;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private ProductCodeMgrZJ productCodeMgrZJ;
	private ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ;
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ;
	private CustomSeachConditionsIfaceGql customSeachConditions;
	private FloorLocationMgrZJ floorLocationMgrZJ;
	private	FloorProductStoreMgr productStoreMgr;
	private FloorSetupLogMgr floorSetupLogMgr;
	
	public void setFloorSetupLogMgr(FloorSetupLogMgr floorSetupLogMgr) {
		this.floorSetupLogMgr = floorSetupLogMgr;
	}

	public void addProductStorage(HttpServletRequest request)
		throws ProductIsInStorageException,Exception
	{
		try
		{
			
			long cid = StringUtil.getLong(request, "cid");
			long pc_id = StringUtil.getLong(request, "pc_id");
			float store_count = StringUtil.getFloat(request,"store_count");
			boolean productIsExist = false;
			
			//检查同一个跟父下是否有相同商品
			DBRow productStorages[] = fpm.getProductStoragesByPcid(pc_id);
			if (productStorages.length>0)
			{
				//先获得当前需要加入到跟分类
				Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
				DBRow currentRoot[] = tree.getAllFather(cid);
				
				for (int i=0; i<productStorages.length; i++)
				{
					DBRow existRoot[] = tree.getAllFather(productStorages[i].get("cid", 0l));
					//跟父相同
					if ( currentRoot[0].get("id", 0l)==existRoot[0].get("id", 0l) )
					{
						productIsExist = true;
						break;
					}
				}
			}
			
			if (!productIsExist)
			{
				String store_station = StringUtil.getString(request,"store_station");
				DBRow row = new DBRow();
				row.add("post_date",DateUtil.NowStr());
				row.add("cid",cid);
				row.add("pc_id",pc_id);
				row.add("store_count",store_count);
				row.add("store_count_alert",0);
				

				long pid = fpm.addProductStorage(row);
			}
			else
			{
				throw new ProductIsInStorageException();
			}
		}
		catch (ProductIsInStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProduct",log);
		}
	}

	/**
	 * 批量修改库存管理
	 * @param request
	 * @throws Exception
	 */
	public void modProductStorage(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)); 
			
			String pid_batch[] = request.getParameterValues("pid_batch");

			for (int i=0; pid_batch!=null&&i<pid_batch.length; i++)
			{
				//更新前，先得到原来的旧数据
				DBRow oldProductStorage = fpm.getDetailProductStorageByPid(StringUtil.getLong(pid_batch[i]));
				
				DBRow row = new DBRow();
				row.add("store_station",StringUtil.getString(request, "store_station_"+pid_batch[i]));
				row.add("store_count",StringUtil.getFloat(request, "store_count_"+pid_batch[i],0));
				row.add("store_count_alert",StringUtil.getFloat(request, "store_count_alert_"+pid_batch[i],0));
				row.add("damaged_count",StringUtil.getFloat(request, "damaged_count_"+pid_batch[i],0));
				row.add("damaged_package_count",StringUtil.getFloat(request, "damaged_package_count_"+pid_batch[i],0));
				fpm.modProductStorage(StringUtil.getLong(pid_batch[i]),row);
				
				//如果库存数发生了变化，则创建库存日志
				if ( oldProductStorage.get("store_count", 0f)!=StringUtil.getFloat(request, "store_count_"+pid_batch[i],0) )
				{
					float changeQuantity = StringUtil.getFloat(request, "store_count_"+pid_batch[i],0)-oldProductStorage.get("store_count", 0f);
					
					ProductStoreLogBean productStoreLogBean = new ProductStoreLogBean();
					productStoreLogBean.setAccount(adminLoggerBean.getAccount());
					productStoreLogBean.setAdid(adminLoggerBean.getAdid());
					productStoreLogBean.setOid(0);
					productStoreLogBean.setQuantity(changeQuantity);
					productStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
					productStoreLogBean.setAdid(adminLoggerBean.getAdid());
					
					
					if ( changeQuantity>0 )
					{
						productStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_MANUAL);			
					}
					else if ( changeQuantity<0 )
					{
						productStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_MANUAL);
					}
					
					Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
					DBRow allFather[] = tree.getAllFather(oldProductStorage.get("cid", 0l));

					//库存进出日志
					productStoreLogBean.setPc_id(oldProductStorage.get("pc_id", 0l));
					productStoreLogBean.setPs_id(allFather[0].get("id", 0l));
					this.addProductStoreLogs(productStoreLogBean);

				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProduct",log);
		}
	}

	/**
	 * 记录进库日志
	 * @param p_name
	 * @param cid
	 * @param quantity
	 * @param adid
	 * @param datemark
	 * @param stat_flag
	 * @param log_type
	 * @throws Exception
	 */
	public void addInProductLog(String p_code,long cid,float quantity,long adid,long datemark,int stat_flag,int log_type)
		throws Exception
	{
		DBRow inProduct = new DBRow();
		
		inProduct.add("name",p_code);
		inProduct.add("cid",cid);
		inProduct.add("quantity",quantity);
		inProduct.add("post_date",DateUtil.NowStr());
		inProduct.add("adid",adid);
		inProduct.add("datemark",datemark);
		inProduct.add("stat_flag",stat_flag);
		inProduct.add("log_type",log_type);
		
		//fpm.addInProduct(inProduct);进出库日志删除，只记录总日志
	}
	
	public void addInProductLog(String p_code,long cid,float quantity,long adid,long datemark,int stat_flag,int log_type,long oid)
		throws Exception
	{
		DBRow inProduct = new DBRow();
		
		inProduct.add("name",p_code);
		inProduct.add("cid",cid);
		inProduct.add("quantity",quantity);
		inProduct.add("post_date",DateUtil.NowStr());
		inProduct.add("adid",adid);
		inProduct.add("datemark",datemark);
		inProduct.add("stat_flag",stat_flag);
		inProduct.add("log_type",log_type);
		inProduct.add("oid",oid);
	
		//fpm.addInProduct(inProduct);进出库日志删除，只记录总日志
	}

	/**
	 * 所有操作包括查询、修改、插入都需要同一个事务中，不然会有奇怪现象
	 */
	public synchronized void addInProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			TDate td = new TDate();
			long datemark = td.getDateTime();
			long current_ps_id = StringUtil.getLong(request, "cid");
			
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			
			String csv_name = StringUtil.getString(request,"csv_name");
			String csvRow[] = getCsv(csv_name);
			
			ArrayList addInProductBeanAL = new ArrayList();
			for (int i=1; i<csvRow.length; i++)
			{
				if (csvRow[i].trim().equals(""))
				{
					continue;
				}
				//addInProductLog(csvRow[i].split(",")[0].trim(),cid,StrUtil.getFloat(csvRow[i].split(",")[1].trim()),adminLoggerBean.getAdid(),datemark,0,InOutStoreKey.IN_STORE_UPLOAD);
				AddInProductBean addInProductBean = new AddInProductBean();
				addInProductBean.setAdid(adminLoggerBean.getAdid());
				addInProductBean.setLogType(InOutStoreKey.IN_STORE_UPLOAD);
				addInProductBean.setPCode(csvRow[i].split(",")[0].trim());
				addInProductBean.setPsId(current_ps_id);
				addInProductBean.setQuantity(StringUtil.getFloat(csvRow[i].split(",")[1].trim()));
				addInProductBean.setStatFlag(0);
				
				addInProductBeanAL.add(addInProductBean);
			}
			
			addInProductSub(current_ps_id,adminLoggerBean,(AddInProductBean[])addInProductBeanAL.toArray(new AddInProductBean[0]),datemark,null);

			/**
			 * 对缺货订单重新抄单
			 * 对合并订单重新分析
			 */
			this.reCheckLackingOrders(request);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addInProduct",log);
		}
	}
	
	/**
	 * 
	 * @param current_ps_id  商品需要进到哪个仓库？
	 * @param adminLoggerBean
	 * @param addInProductBean
	 * @param datemark
	 * @throws Exception
	 */
	public synchronized void addInProductSub(long current_ps_id,AdminLoginBean adminLoggerBean,AddInProductBean addInProductBean[],long datemark,String machine_id)
		throws ProductNotExistException,Exception
	{
		try
		{
			//加入日志表，新加入日志stat_flag=0，数据被统计并更新库存后stat_flag=1
			for (int i=0; i<addInProductBean.length; i++)
			{
				if (fpm.getDetailProductByPcode(addInProductBean[i].getPCode())==null)
				{
					throw new ProductNotExistException();//条码不存在
				}
				addInProductLog(addInProductBean[i].getPCode(),current_ps_id,addInProductBean[i].getQuantity(),adminLoggerBean.getAdid(),datemark,addInProductBean[i].getStatFlag(),addInProductBean[i].getLogType());
			}

			//group by统计商品数
//			DBRow statInProduct[] = fpm.getStatProductsByDatePcid(datemark, datemark, current_ps_id, null);//区分仓库统计，以免多人操作出错
			//DBRow statInProduct[] = fpm.getStatProductsByDatePcid(0, 0, 0, null);
			
			//在product先检查是否在该仓库下有该商品，如果存在即对库存进行累加quantity
			float quantity = 0f;
			long ps_id = 0l;
			long pc_id = 0l;
			for(int i=0; i<addInProductBean.length; i++)
			{
				//从仓库查出商品的库存唯一ID
				DBRow existProduct = fpm.getDetailProductProductStorageByPcid(addInProductBean[i].getPsId(),addInProductBean[i].getPcid());

				if (existProduct!=null)
				{
					quantity = addInProductBean[i].getQuantity();
					ps_id = existProduct.get("cid", 0l);
					pc_id = existProduct.get("pc_id", 0l);

					//记录库存日志
					ProductStoreLogBean productStoreLogBean = new ProductStoreLogBean();
					productStoreLogBean.setAccount(adminLoggerBean.getAccount());
					productStoreLogBean.setAdid(adminLoggerBean.getAdid());
					productStoreLogBean.setOid(0);
					productStoreLogBean.setQuantity(addInProductBean[i].getQuantity());
					productStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
					productStoreLogBean.setOperation(ProductStoreOperationKey.IN_OUT_STORE_FILE);
					
					//根据修改值决定是扣库存还是加库存
					if(quantity>0)
					{
						fpm.incProductStoreCountByPcid(productStoreLogBean, ps_id,pc_id, quantity,0);
					}
					else
					{
						fpm.deIncProductStoreCountByPcid(productStoreLogBean, ps_id,pc_id, Math.abs(quantity));
					}

//					DBRow nowProductStorage = fpm.getDetailProductStorageByPid(existProduct.get("pid", 0l));
//					Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//					DBRow allFather[] = tree.getAllFather(nowProductStorage.get("cid", 0l));
//					//库存进出日志
//					productStoreLogBean.setPc_id(nowProductStorage.get("pc_id", 0l));
//					productStoreLogBean.setPs_id(allFather[0].get("id", 0l));
//					this.addProductStoreLogs(productStoreLogBean);
				}
			}

			//更新完库存后，把今天所有新日志标记为已统计进库
//			DBRow modInProduct = new DBRow();
//			modInProduct.add("stat_flag",1);
//			fpm.modInProductByDatemark(datemark,modInProduct);
			
			//如果商品信息存在条码，则需要插入商品定位信息
			for (int i=0; i<addInProductBean.length; i++)
			{
				if (addInProductBean[i].getPosition()!=null)
				{
//					this.addStorageLocation(current_ps_id,0,addInProductBean[i].getPCode(),addInProductBean[i].getQuantity(),addInProductBean[i].getPosition(),machine_id,null,adminLoggerBean);
				}
			}
			
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addInProductSub",log);
		}
	}

	/**
	 * 记录出货日志
	 * @param p_name
	 * @param cid
	 * @param quantity
	 * @param adid
	 * @param datemark
	 * @param stat_flag	是否已经统计
	 * @param log_type
	 * @throws Exception
	 */
	private void addOutProductLog(String p_code,long cid,float quantity,long adid,long datemark,int stat_flag,int log_type)
		throws Exception
	{
		DBRow inProduct = new DBRow();
		
		inProduct.add("name",p_code);
		inProduct.add("cid",cid);
		inProduct.add("quantity",quantity);
		inProduct.add("post_date",DateUtil.NowStr());
		inProduct.add("adid",adid);
		inProduct.add("datemark",datemark);
		inProduct.add("stat_flag",stat_flag);
		inProduct.add("log_type",log_type);

		//fpm.addOutProduct(inProduct);进出库日志删除，只记录总日志
	}
	
	public synchronized void addOutProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			TDate td = new TDate();
			long datemark = td.getDateTime();
			
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			
			long cid = StringUtil.getLong(request, "cid");
			String csv_name = StringUtil.getString(request,"csv_name");
	
			String csvRow[] = getCsv(csv_name);
			
			//加入日志表，新加入日志stat_flag=0，数据被统计后stat_flag=1
			for (int i=1; i<csvRow.length; i++)
			{
				if (!csvRow[i].trim().equals(""))
				{
					addOutProductLog(csvRow[i].split(",")[0].trim(),cid,StringUtil.getFloat(csvRow[i].split(",")[1].trim()),adminLoggerBean.getAdid(),datemark,0,InOutStoreKey.OUT_STORE_UPLOAD);
				}
			}
	
			//group by统计商品数
//			DBRow statInProduct[] = fpm.getStatOutProductsByDatePcid(0, 0, 0, null);
			
			//不再处理库存数

			//更新完库存后，把今天所有新日志标记为已统计进库
//			DBRow modInProduct = new DBRow();
//			modInProduct.add("stat_flag",1);
//			fpm.modOutProductByDatemark(datemark,modInProduct);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addOutProduct",log);
		}
	}

	/**
	 * 上传订单发货
	 * @param request
	 * @throws Exception
	 */
	public synchronized void addShipProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			//long st = System.currentTimeMillis();
			
			long sc_id;
			long ccid;
			float weight;
			
			TDate td = new TDate();
			long datemark = td.getDateTime();
			
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			
			long current_ps_id = StringUtil.getLong(request, "cid");
			String csv_name = StringUtil.getString(request,"csv_name");
	
			String dev_date = DateUtil.NowStr();
			String csvRow[] = getCsv(csv_name);
			
			ExpressMgrIFace expressMgr = (ExpressMgrIFace)MvcUtil.getBeanFromContainer("expressMgr");				

			ArrayList addShipProductBeanAL = new ArrayList();
			for (int i=1; i<csvRow.length; i++)
			{
				if (csvRow[i].trim().equals(""))
				{
					continue;
				}
				sc_id = fom.getDetailPOrderByOid(StringUtil.getLong(csvRow[i].split(",")[0].trim())).get("sc_id", 0l);
				ccid = fom.getDetailPOrderByOid(StringUtil.getLong(csvRow[i].split(",")[0].trim())).get("ccid", 0l);
				weight = StringUtil.getFloat(csvRow[i].split(",")[2].trim());

				long oid = StringUtil.getLong(csvRow[i].split(",")[0].trim());
				DBRow detailOrder = fom.getDetailPOrderByOid(oid);
				ShippingInfoBean shippingInfoBean = expressMgr.getShippingFee(sc_id, weight, ccid,detailOrder.get("pro_id", 0l));
				
				AddShipProductBean addShipProductBean = new AddShipProductBean();
				addShipProductBean.setDeliveryAccount(csvRow[i].split(",")[3].trim());
				addShipProductBean.setDeliveryCode(csvRow[i].split(",")[1].trim());
				addShipProductBean.setDeliveryOperator(csvRow[i].split(",")[3].trim());
				addShipProductBean.setDeliveryto(shippingInfoBean.getZoneName());
				addShipProductBean.setOid(oid);
				addShipProductBean.setProductWeight(StringUtil.getFloat(csvRow[i].split(",")[2].trim()));
				addShipProductBean.setScid( sc_id );

				addShipProductBeanAL.add(addShipProductBean);
			}
			
			addShipProductSub( current_ps_id, dev_date, datemark, adminLoggerBean, (AddShipProductBean[])addShipProductBeanAL.toArray(new AddShipProductBean[0]));
			
			//long en = System.currentTimeMillis();
			////system.out.println("T: "+(en-st)); 
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addShipProduct",log);
		}
	}
	
	public void upLoadEbayTrackingNumber(long oid,String trackingNumber,long sc_id)
		throws Exception
	{
		DBRow detailCurrentOrder = fom.getDetailPOrderByOid(oid);
		DBRow shipCompany = floorExpressMgr.getDetailCompany(sc_id);
		String shipping_name = shipCompany.getString("name");
		
		//USPS-P与USPS-First要统一成USPS
		if(shipping_name.toLowerCase().contains("usps"))
		{
			shipping_name = "USPS";
		}
		//Fedex的各种服务（FEDEX，FedexIE）统一为FEDEX
		if(shipping_name.toLowerCase().contains("fedex"))
		{
			shipping_name = "FedEx";
		}
		//DHL的各种服务统一成DHL
		if(shipping_name.toLowerCase().contains("dhl"))
		{
			shipping_name = "DHL Global Mail";
		}
		

		if (detailCurrentOrder.getString("order_source").equals(OrderMgr.ORDER_SOURCE_EBAY)&&!detailCurrentOrder.getString("ebay_txn_id").equals(""))
		{
			String item_number;

			if (detailCurrentOrder.getString("item_number").indexOf(",")>0)
			{
				//兼容下老订单数据不一致情况
				if (detailCurrentOrder.getString("ebay_txn_id").indexOf(",")>0)
				{
					upEbayTrackingNumber(oid,detailCurrentOrder.getString("seller_id"),detailCurrentOrder.getString("ebay_txn_id").split(","),detailCurrentOrder.getString("item_number").split(","),shipping_name,trackingNumber);								
				}
			}
			else
			{
				item_number = detailCurrentOrder.getString("item_number");
				upEbayTrackingNumber(oid,detailCurrentOrder.getString("seller_id"),new String[]{detailCurrentOrder.getString("ebay_txn_id")},new String[]{item_number},shipping_name,trackingNumber);
			}
		}
	}
	
	
	public synchronized void addShipProductSub(long current_ps_id,String dev_date,long datemark,AdminLoginBean adminLoggerBean,AddShipProductBean addShipProductBean[])
		throws Exception
	{
		try
		{
			//加入日志表，新加入日志stat_flag=0，数据被统计后stat_flag=1
			for (int i=0; i<addShipProductBean.length; i++)
			{
					DBRow inProduct = new DBRow();
					
					inProduct.add("name",addShipProductBean[i].getOid());
					inProduct.add("delivery_code",addShipProductBean[i].getDeliveryCode());
					inProduct.add("product_weight",addShipProductBean[i].getProductWeight());
					inProduct.add("delivery_operator",addShipProductBean[i].getDeliveryOperator());
					inProduct.add("deliveryto",addShipProductBean[i].getDeliveryto());
					inProduct.add("sc_id",addShipProductBean[i].getScid());
					
					inProduct.add("cid",current_ps_id);
					inProduct.add("quantity",0);
					inProduct.add("post_date",dev_date);
					inProduct.add("adid",adminLoggerBean.getAdid());
					inProduct.add("datemark",datemark);
					inProduct.add("stat_flag",0);
	
					fpm.addShipProduct(inProduct);
					
					//运单发货
					wayBillMgrZJ.shipWayBill(addShipProductBean[i].getOid(),addShipProductBean[i].getProductWeight(),adminLoggerBean.getAdid());
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addShipProductSub",log);
		}
	}
	
	public synchronized void addShipProductSubOld(long current_ps_id,String dev_date,long datemark,AdminLoginBean adminLoggerBean,AddShipProductBean addShipProductBean[])
		throws Exception
	{
		try
		{
			//加入日志表，新加入日志stat_flag=0，数据被统计后stat_flag=1
			for (int i=0; i<addShipProductBean.length; i++)
			{
					DBRow inProduct = new DBRow();
					
					inProduct.add("name",addShipProductBean[i].getOid());
					inProduct.add("delivery_code",addShipProductBean[i].getDeliveryCode());
					inProduct.add("product_weight",addShipProductBean[i].getProductWeight());
					inProduct.add("delivery_operator",addShipProductBean[i].getDeliveryOperator());
					inProduct.add("deliveryto",addShipProductBean[i].getDeliveryto());
					inProduct.add("sc_id",addShipProductBean[i].getScid());
					
					inProduct.add("cid",current_ps_id);
					inProduct.add("quantity",0);
					inProduct.add("post_date",dev_date);
					inProduct.add("adid",adminLoggerBean.getAdid());
					inProduct.add("datemark",datemark);
					inProduct.add("stat_flag",0);

					fpm.addShipProduct(inProduct);

					//上传运单号到ebay
					DBRow detailCurrentOrder = fom.getDetailPOrderByOid(addShipProductBean[i].getOid());
					//log.info("["+addShipProductBean[i].getOid()+"] ["+detailCurrentOrder.getString("order_source")+"] ["+detailCurrentOrder.getString("ebay_txn_id")+"]");
					if (detailCurrentOrder.getString("order_source").equals(OrderMgr.ORDER_SOURCE_EBAY)&&!detailCurrentOrder.getString("ebay_txn_id").equals(""))
					{
						String item_number;

						if (detailCurrentOrder.getString("item_number").indexOf(",")>0)
						{
							//兼容下老订单数据不一致情况
							if (detailCurrentOrder.getString("ebay_txn_id").indexOf(",")>0)
							{
								upEbayTrackingNumber(detailCurrentOrder.get("oid", 0l),detailCurrentOrder.getString("seller_id"),detailCurrentOrder.getString("ebay_txn_id").split(","),detailCurrentOrder.getString("item_number").split(","),detailCurrentOrder.getString("shipping_name"),detailCurrentOrder.getString("ems_id"));								
							}
						}
						else
						{
							item_number = detailCurrentOrder.getString("item_number");
							upEbayTrackingNumber(detailCurrentOrder.get("oid", 0l),detailCurrentOrder.getString("seller_id"),new String[]{detailCurrentOrder.getString("ebay_txn_id")},new String[]{item_number},detailCurrentOrder.getString("shipping_name"),detailCurrentOrder.getString("ems_id"));
						}
						//log.info("[update "+addShipProductBean[i].getOid()+"]");
					}

					//把订单标记为已发货
					long oid = addShipProductBean[i].getOid();
					DBRow modOrder = new DBRow();
					modOrder.add("handle","4");
					modOrder.add("delivery_date",dev_date);
					modOrder.add("delivery_account",addShipProductBean[i].getDeliveryAccount());
					fom.modPOrder(oid, modOrder);

					//如果是合并订单，把所有子订单的状态也要修改
					if ( fom.getDetailPOrderByOid(oid).get("parent_oid", 0l)<0 )
					{
						DBRow sonOrders[] = fom.getSonOrders(oid, null);
						for (int ii=0; ii<sonOrders.length; ii++)
						{
							DBRow sonOrder = new DBRow();
							sonOrder.add("handle","4");
							sonOrder.add("delivery_date",dev_date);
							sonOrder.add("delivery_account",addShipProductBean[i].getDeliveryAccount());
							fom.modPOrder(sonOrders[ii].get("oid", 0l),sonOrder);
							
							if ( sonOrders[ii].getString("order_source").equals(OrderMgr.ORDER_SOURCE_EBAY) )
							{
								if (sonOrders[ii].getString("item_number").indexOf(",")>0)
								{
									//兼容下老订单数据不一致情况
									if (sonOrders[ii].getString("ebay_txn_id").indexOf(",")>0)
									{
										upEbayTrackingNumber(sonOrders[ii].get("oid", 0l),sonOrders[ii].getString("seller_id"),sonOrders[ii].getString("ebay_txn_id").split(","),sonOrders[ii].getString("item_number").split(","),fom.getDetailPOrderByOid(oid).getString("shipping_name"),fom.getDetailPOrderByOid(oid).getString("ems_id"));									
									}
								}
								else
								{
									upEbayTrackingNumber(sonOrders[ii].get("oid", 0l),sonOrders[ii].getString("seller_id"),new String[]{sonOrders[ii].getString("ebay_txn_id")},new String[]{sonOrders[ii].getString("item_number")},fom.getDetailPOrderByOid(oid).getString("shipping_name"),fom.getDetailPOrderByOid(oid).getString("ems_id"));
								}
							}
						}
					}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addShipProductSub",log);
		}
	}
	
	/**
	 * 上传运单号到ebay
	 * @param seller_id
	 * @param txn_id
	 * @param item_numbers
	 * @param shipping_name
	 * @param tracking_number
	 */
	private void upEbayTrackingNumber(long oid,String seller_id,String txn_id[],String item_numbers[],String shipping_name,String tracking_number)
	{
		EbayMgr ebayMgr = (EbayMgr)MvcUtil.getBeanFromContainer("ebayMgr");
		
		for (int i=0; i<item_numbers.length; i++)
		{
			CompleteSaleParaBeans completeSaleParaBeans = new CompleteSaleParaBeans();
			completeSaleParaBeans.setCarrier(shipping_name);
			completeSaleParaBeans.setItemID(item_numbers[i]);
			completeSaleParaBeans.setSellerID(seller_id);
			completeSaleParaBeans.setTrackingNumber(tracking_number);
			completeSaleParaBeans.setTransactionID(txn_id[i]);
			ebayMgr.UpTrackingNumberCallJMS(completeSaleParaBeans);
			
			log.info("oid:"+oid+" - txn_id:"+txn_id[i]+" - item_number:"+item_numbers[i]);
		}
	}

	public DBRow[] getInProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
			//return(fpm.getInProductsByStEnPc(st_datemark, en_datemark,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInProductsByStEnPc",log);
		}
	}
	
	public DBRow[] getOutProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
//			return(fpm.getOutProductsByStEnPc(st_datemark, en_datemark,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getOutProductsByStEnPc",log);
		}
	}
	
	public DBRow[] getShipProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsByStEnPc(st_datemark, en_datemark,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsByStEnPc",log);
		}
	}
	
	public DBRow[] getInProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
			//return(fpm.getInProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInProductsByStEnPcCid",log);
		}
	}
	
	public DBRow[] getInProductsByStEnPcCidLogType(long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
//			return(fpm.getInProductsByStEnPcCidLogType( cid, log_type, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInProductsByStEnPcCidLogType",log);
		}
	}
	
	public DBRow[] getInProductsByStEnPcCidLogTypePname(String p_name,long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
//			return(fpm.getInProductsByStEnPcCidLogTypePname( p_name, cid, log_type, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInProductsByStEnPcCidLogTypePname",log);
		}
	}

	
	public DBRow[] getShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsByStEnPcCid",log);
		}
	}
	
	public DBRow[] getShipProductsByStEnPcCidScid(long cid,long scid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsByStEnPcCidScid( cid, scid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsByStEnPcCidScid",log);
		}
	}
	
	public DBRow[] getShipProductsByStEnPcCidScid(long cid,long scid,long st_datemark,long en_datemark)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsByStEnPcCidScid( cid, scid, st_datemark, en_datemark,null));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsByStEnPcCidScid",log);
		}
	}
	
	public DBRow[] getOutProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
//			return(fpm.getOutProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getOutProductsByStEnPcCid",log);
		}
	}
	
	public DBRow[] getOutProductsByStEnPcCidLogType(long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//删除出入库日志，只记录库存日志
//			return(fpm.getOutProductsByStEnPcCidLogType( cid, log_type, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getOutProductsByStEnPcCidLogType",log);
		}
	}

	public DBRow[] getAmountProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
			//return(fpm.getAmountProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAmountProductsByStEnPcCid",log);
		}
	}

	public DBRow[] getAmountOutProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return null;//进出库日志删除，只记录总日志
			//return(fpm.getAmountOutProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAmountOutProductsByStEnPcCid",log);
		}
	}
	
	public DBRow[] getAmountShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAmountShipProductsByStEnPcCid( cid, st_datemark, en_datemark, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAmountShipProductsByStEnPcCid",log);
		}
	}
	
	public String[] getCsv(String filename)
		throws Exception
	{
//		String content = FileUtil.readFile2String(Environment.getHome().replace("\\", "/")+"."+filename,"utf-8");
//		FileUtil.createFile(Environment.getHome().replace("\\", "/")+"."+filename,"utf-8", "\n"+content);
		String path = Environment.getHome().replace("\\", "/")+"."+filename;
		//system.out.println(path);
		String csvRows[] = FileUtil.readFile2Array(path,"utf-8");
		
		return(csvRows);
	}
	
	/**
	 * 通过仓库查找商品，检查是否存在
	 * 支持仓库无限分级
	 */
	public boolean isIncatalog(String name,long pcid)
		throws Exception
	{
		try
		{
			if ( fpm.getDetailProductProductStorageByPCode(pcid,name)==null )//(ZJ)上传文件入库出库
			{
				return(false);
			}
			else
			{
				return(true);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"isIncatalog",log);
		}
	}

	public DBRow[] getShipProductsFromSearchName(String name,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsFromSearchName(name,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsFromSearchName",log);
		}
	}
	
	
	public DBRow[] getProductSnByPcId(long pc_id)
			throws Exception
		{
			try
			{
				return(fpm.getProductSnByPcId(pc_id));
			}
			catch (Exception e)
			{
				throw new SystemException(e,"getProductSnByPcId",log);
			}
		}
	
	//addProduct//已添加日志
	public DBRow addProduct(HttpServletRequest request) throws ProductCodeIsExistException,ProductNameIsExistException,Exception {
		
		try {
			
			String p_name = StringUtil.getString(request, "p_name").toUpperCase();
			String p_code = StringUtil.getString(request, "p_code").toUpperCase();
			long catalog_id = StringUtil.getLong(request, "catalog_id");
			
			String unit_name = StringUtil.getString(request, "unit_name");
			double unit_price = StringUtil.getDouble(request, "unit_price");
			float gross_profit = StringUtil.getFloat(request, "gross_profit");
			float weight = StringUtil.getFloat(request, "weight");
			float length = StringUtil.getFloat(request,"length");
			float height = StringUtil.getFloat(request,"height");
			float width = StringUtil.getFloat(request,"width");
			String lsn = StringUtil.getString(request,"lsn");
			int length_uom = StringUtil.getInt(request, "length_uom");
			int weight_uom = StringUtil.getInt(request, "weight_uom");
			int price_uom = StringUtil.getInt(request, "price_uom");
			String nmfc_code = StringUtil.getString(request,"nmfc_code");
			long freight_class = StringUtil.getLong(request,"freight_class");
			long customerId = StringUtil.getLong(request,"customerId");
			long titleId = StringUtil.getLong(request,"titleId");
			
			DBRow detailPro = fpm.getDetailProductByPname(p_name);
			
			if (detailPro!=null){
				
				throw new ProductNameIsExistException();	//商品名称已经存在
			}
			
//			detailPro = fpm.getDetailProductByPcode(p_code);
//			
//			if (detailPro!=null){
//				
//				throw new ProductCodeIsExistException();	//商品条码已经存在
//			}
			
			String product_lsn = null;
			if(StringUtil.isBlank(lsn)){
				product_lsn = null;
			}
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
			
			DBRow product = new DBRow();
			product.add("p_name",p_name);
			product.add("lot_no",p_code);
			product.add("catalog_id",catalog_id);
			product.add("unit_name","ITEM");
			product.add("unit_price",unit_price);
			product.add("gross_profit",gross_profit);
			product.add("weight",weight);
			product.add("union_flag",0);
			product.add("sn_size",product_lsn);
			product.add("freight_class",freight_class);
			product.add("length",length);
			product.add("heigth",height);
			product.add("width",width);
			product.add("nmfc_code",nmfc_code);
			product.add("length_uom", length_uom);
			product.add("weight_uom", weight_uom);
			product.add("price_uom", price_uom);
			product.add("volume", length*width*height);
			product.add("alive",1);//允许抄单
			product.add("creator", adminLoggerBean.getAdid());
			product.add("create_time", DateUtil.NowStr());
			
			//添加商品//已添加日志
			long pc_id = fpm.addProduct(product);
			
			/*******************************************************************************************************/
			/** 
			 * 创建的时候, 在创建完成后执行此方法
			 * 更新的时候, 在更新前执行此方法
			 */
			DBRow logRow = new DBRow();
			//create/update/delete
			logRow.put("event", "create");
			//更新的表名
			logRow.put("table", "product");
			//主键字段名
			logRow.put("column", "pc_id");
			//主键ID
			logRow.put("pri_key", pc_id);
			//依赖表
			logRow.put("ref_table", "");
			//依赖表主键字段名
			logRow.put("ref_column", "");
			//依赖表主键ID
			logRow.put("ref_pri_key", "");
			//账号ID
			logRow.put("create_by", adminLoggerBean.getAdid());
			//设备类型//请求的来源
			logRow.put("device", ProductDeviceTypeKey.Web);
			//描述
			logRow.put("describe", "create product "+p_name);
			
			//具体更新的字段内容
			//将DBRow转换为数组
			DBRow[] content = DBRowUtils.dbRowConvertToArray(product);
			logRow.put("content", content);
			
			floorSetupLogMgr.addSetupLog(logRow);
			
			/*******************************************************************************************************/
			
			//添加条码//已添加日志
			productCodeMgrZJ.addProductCodeSubNoLog(pc_id,p_code,CodeTypeKey.Main,adminLoggerBean);
			
			if(customerId > 0){
				
				DBRow customerTitle = new DBRow();
				customerTitle.add("tp_pc_id", pc_id);
				customerTitle.add("tp_title_id", titleId);
				customerTitle.add("tp_customer_id", customerId);
				
				//已添加日志
				long ptId = fpm.addProductTitle(customerTitle);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow ptRow = new DBRow();
				//create/update/delete
				ptRow.put("event", "create");
				//更新的表名
				ptRow.put("table", "title_product");
				//主键字段名
				ptRow.put("column", "tp_id");
				//主键ID
				ptRow.put("pri_key", ptId);
				//依赖表
				ptRow.put("ref_table", "");
				//依赖表主键字段名
				ptRow.put("ref_column", "");
				//依赖表主键ID
				ptRow.put("ref_pri_key", "");
				//账号ID
				ptRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				ptRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				ptRow.put("describe", "create title_product: "+ pc_id +" title: "+ titleId +" customer: "+ customerId +" relation ");
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] content1 = DBRowUtils.dbRowConvertToArray(customerTitle);
				ptRow.put("content", content1);
				
				floorSetupLogMgr.addSetupLog(ptRow);
				
				/*******************************************************************************************************/
			}
			
			//Yuanxinyu 插入商品与sn的关系表, 根据pc_id删除表中记录  先删在新增//未添加日志
			fpm.delProductSn(pc_id);
			
			if(!StringUtil.isBlank(lsn)){
				
				String[] lsnList = lsn.split(",");
				DBRow lsnRow = new DBRow();
				
				for(int i = 0; i < lsnList.length ; i ++){
					
					lsnRow.add("pc_id", pc_id);
					lsnRow.add("sn_size", lsnList[i]);
					if( i == 0){
						// 默认第一个作为default
						lsnRow.add("default_value", 1);
					}else{
						lsnRow.add("default_value", 0);
					}
					
					lsnRow.add("creator", adminLoggerBean.getAdid());
					lsnRow.add("create_time", DateUtil.NowStr());
					
					//插入数据库操作//已添加日志
					long snId = fpm.addProductSn(lsnRow);
					
					/*******************************************************************************************************/
					/** 
					 * 创建的时候, 在创建完成后执行此方法
					 * 更新的时候, 在更新前执行此方法
					 */
					DBRow snRow = new DBRow();
					//create/update/delete
					snRow.put("event", "create");
					//更新的表名
					snRow.put("table", "product_sn");
					//主键字段名
					snRow.put("column", "sn_id");
					//主键ID
					snRow.put("pri_key", snId);
					//依赖表
					snRow.put("ref_table", "product");
					//依赖表主键字段名
					snRow.put("ref_column", "pc_id");
					//依赖表主键ID
					snRow.put("ref_pri_key", pc_id);
					//账号ID
					snRow.put("create_by", adminLoggerBean.getAdid());
					//设备类型//请求的来源
					snRow.put("device", ProductDeviceTypeKey.Web);
					//描述
					snRow.put("describe", "create product_sn "+snId);
					
					//具体更新的字段内容
					DBRow[] content2 = DBRowUtils.dbRowConvertToArray(lsnRow);
					snRow.put("content", content2);
					
					floorSetupLogMgr.addSetupLog(snRow);
					
					/*******************************************************************************************************/
				}
			}
			
			//记录商品添加日志
			DBRow product_log = fpm.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.ADD);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			customSeachConditions.addLableTempByPcId(pc_id,0L);//添加基础模板
			
			//把新商品在所有仓库建库
			/*
			CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
			DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
			for ( int i=0; i<treeRows.length; i++ )
			{
				if (treeRows[i].get("parentid",0)==0)
				{
					String store_station = StringUtil.getString(request,"store_station");
					DBRow store = new DBRow();
					store.add("post_date",DateUtil.NowStr());
					store.add("cid",treeRows[i].get("id",0));
					store.add("pc_id",pc_id);
					store.add("store_count",0);
					store.add("store_count_alert",0);
					fpm.addProductStorage(store);
				}
			}
			*/
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int i = 0; i < pcodes.length; i++) 
			{
				pcodeString += pcodes[i].getString("p_code")+" ";
			}
			
			ProductIndexMgr.getInstance().addIndexAsyn(pc_id, p_name,pcodeString, catalog_id,unit_name,1);
			
			if(!StringUtil.isBlank(lsn)){
				
				int snlength = lsn.split(",").length;
				if(snlength == 1){
					product.put("sn_size", lsn.split(",")[0]);
				}else{
					product.put("sn_size", lsn.split(",")[0] +" ["+lsn.split(",").length+"]");
				}
			}else{
				product.put("sn_size", "");
			}
			
			if(product.get("union_flag",0)==0)
			{
				product.add("unionimg","<img src='../imgs/product.png'/>");
			}
			else
			{
				int unionCount = fpm.getProductUnionCountByPid(pc_id);
				product.add("unionimg","<a href='javascript:void(0)' title='View Suit Relationship'><img src='../imgs/union_product.png' border='0'/><span class='badge'>"+unionCount+"</span></a>");
			}
			
			StringBuffer catalogText = new StringBuffer("<span style='color:#999999'>");
		  	
			product.add("catalog_text",catalogText.toString());
			String alive_text;
		  	if (product.get("alive",0)==1)
			{
		      alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value=' Inactive' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-minus-sign icon-red'></i>&nbsp;Inactive</a><hidden></hidden><br/><br/>"+
		      				"<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+product.getString("pc_id")+")\"  ><i class='icon-tags'></i>&nbsp;Label</a><hidden></hidden><br/><br/>"+
		       				"<a class='buttons' style='width:57px;' value='Manage Codes' onClick=\"manageCodes(this);\"><i class='icon-barcode'></i>&nbsp;Codes</a>";
			}
			else
			{
		       alive_text = "<a name='Submit43' style='width:57px;' class='buttons' value='Active' onClick=\"swichProductAlive('"+product.getString("p_name")+"',"+product.getString("pc_id")+","+product.getString("alive")+")\"><i class='icon-ok-sign icon-green'></i>&nbsp;Active</a><hidden></hidden><br/><br/>"+
		       				"<a class='buttons' style='width:57px;' value='Label' onClick=\"make_tab("+product.getString("pc_id")+")\"  ><i class='icon-tags'></i>&nbsp;Label</a><hidden></hidden><br/><br/>"+
		       				"<a class='buttons' style='width:57px;' value='Manage Codes' onClick=\"manageCodes(this);\"><i class='icon-barcode'></i>&nbsp;Codes</a>";
			}
		  	product.add("alive_text",alive_text);
		  	product.remove("p_img");
			
			StringBuffer html =  new StringBuffer();
			html.append("<ul class='myul'>");
			// 显示出来商品文件的个数 和 商品标签的个数
	  		ArrayList<String> selectedList= new ProductFileTypeKey().getProductFileTypeValue();
		 	for(int indexOfList = 0 ; indexOfList < selectedList.size() ;indexOfList++ ){
		 		int tempCountNumber = 0 ;
	 			html.append("<li style='width:50px;'>");
 				html.append(selectedList.get(indexOfList).trim());
 				html.append(":");
 				html.append(tempCountNumber);
 				html.append("</li>");

		 	}
		 	html.append("</ul>");
		 	product.add("img",html.toString());
			
			product.add("p_codes","");
			product.add("p_code",product.get("lot_no", ""));
			LengthUOMKey lengthUOMKey = new LengthUOMKey();
			WeightUOMKey weightUOMKey = new WeightUOMKey();
			PriceUOMKey priceUOMKey = new PriceUOMKey();
			product.add("length_uom_name", lengthUOMKey.getLengthUOMKey(product.get("length_uom", 0)));
			product.add("weight_uom_name", weightUOMKey.getWeightUOMKey(product.get("weight_uom", 0)));
			product.add("price_uom_name", priceUOMKey.getMoneyUOMKey(product.get("price_uom", 0)));
			
			String length1 = product.getString("length");
			String width1 = product.getString("width");
			String heigth1 = product.getString("heigth");
			
			String weight1 = product.getString("weight");
			String unit_price1 = product.getString("unit_price");
			String volume1 = product.getString("volume");
			DecimalFormat df = new DecimalFormat("#.000");
			volume1 = df.format(Float.parseFloat(volume1));
			product.put("volume",volume1);
			if(!"".equals(length1) && length1 != null && length1.matches("[0-9]\\d*\\.?00*")){
				product.put("length",length1.substring(0,length1.indexOf(".")));
			}
			if(!"".equals(width1) && width1 != null && width1.matches("[0-9]\\d*\\.?00*")){
				product.put("width",width1.substring(0,width1.indexOf(".")));
			}
			if(!"".equals(heigth1) && heigth1 != null && heigth1.matches("[0-9]\\d*\\.?00*")){
				product.put("heigth",heigth1.substring(0,heigth1.indexOf(".")));
			}
			
			if(!"".equals(weight1) && weight1 != null && weight1.matches("[0-9]\\d*\\.?00*")){
				product.put("weight",weight1.substring(0,weight1.indexOf(".")));
			}
			if(!"".equals(unit_price1) && unit_price1 != null && unit_price1.matches("[0-9]\\d*\\.?00*")){
				product.put("unit_price",unit_price1.substring(0,unit_price1.indexOf(".")));
			}
			if(!"".equals(volume1) && volume1 != null && volume1.matches("[0-9]\\d*\\.?00*")){
				product.put("volume",volume1.substring(0,volume1.indexOf(".")));
			}
			
			return product;
		}
		catch (ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (ProductNameIsExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProduct",log);
		}
	}
	
	public DBRow updateProductIndex(long id) throws Exception {
		
		DBRow row = new DBRow();
		
		DBRow product = fpm.getDetailProductByPcid(id);
		
		if( product != null){
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(id);
			
			String pcodeString = "";
			
			for (int i = 0; i < pcodes.length; i++) {
				
				pcodeString += pcodes[i].getString("p_code")+" ";
			}
			
			ProductIndexMgr.getInstance().addIndexAsyn(product.get("pc_id", 0), product.get("p_name", ""), pcodeString, product.get("catalog_id", 0), product.get("unit_name", ""),1);
			
			row.put("ret",BCSKey.SUCCESS);
			row.put("err",0);
			
		} else {
			
			row.put("ret",BCSKey.FAIL);
			row.put("err",90);
		} 
		
		return row;
	}
	
	public DBRow updateProductIndex(DBRow param) throws Exception {
		
		DBRow[] array = null;
		DBRow[] id = param.get("ids") != null ? (DBRow[])param.get("ids") : null;
		
		if(id != null && id.length > 0){
			
			array = new DBRow[id.length];
			
			for(int i = 0; i<id.length; i++){
				
				array[i] = updateProductIndex(id[i].get("id",0));
			}
		}
		
		DBRow result = new DBRow();
		result.put("info", array);
		
		return result;
	}
	
	public DBRow updateProductSn(HttpServletRequest request) throws Exception
	{
		DBRow ret = new DBRow();
		try
		{
			long pc_id = StringUtil.getLong(request, "pc_id");
			String lsn = StringUtil.getString(request, "lsn");
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
			// 根据pc_id删除表中记录  先删在新增
			fpm.delProductSn(pc_id);
			
			if(!StringUtil.isBlank(lsn)){
				String[] lsnList = lsn.split(",");
				DBRow lsnRow = new DBRow();
				for(int i = 0; i < lsnList.length ; i ++){
					
					lsnRow.add("pc_id", pc_id);
					lsnRow.add("sn_size", lsnList[i]);
					
					if( i == 0){
						// 默认第一个作为default
						lsnRow.add("default_value", 1);
						DBRow productSn = new DBRow();
						productSn.add("sn_size", lsnList[i]);
						
						fpm.modProduct(pc_id, productSn);

						/*******************************************************************************************************/
						/** 
						 * 创建的时候, 在创建完成后执行此方法
						 * 更新的时候, 在更新前执行此方法
						 */
						DBRow logRow = new DBRow();
						//create/update/delete
						logRow.put("event", "update");
						//更新的表名
						logRow.put("table", "product");
						//主键字段名
						logRow.put("column", "pc_id");
						//主键ID
						logRow.put("pri_key", pc_id);
						//依赖表
						logRow.put("ref_table", "");
						//依赖表主键字段名
						logRow.put("ref_column", "");
						//依赖表主键ID
						logRow.put("ref_pri_key", "");
						//账号ID
						logRow.put("create_by", adminLoggerBean.getAdid());
						//设备类型//请求的来源
						logRow.put("device", ProductDeviceTypeKey.Web);
						//描述
						logRow.put("describe", "update product "+pc_id);
						
						//具体更新的字段内容
						//将DBRow转换为数组
						DBRow[] content = DBRowUtils.dbRowConvertToArray(productSn);
						logRow.put("content", content);
						
						floorSetupLogMgr.addSetupLog(logRow);
						
						/*******************************************************************************************************/
						
					}else{
						lsnRow.add("default_value", 0);
					}
					lsnRow.add("creator", adminLoggerBean.getAdid());
					lsnRow.add("create_time", DateUtil.NowStr());
					
					//插入数据库操作
					long snId = fpm.addProductSn(lsnRow);
					
					/*******************************************************************************************************/
					/** 
					 * 创建的时候, 在创建完成后执行此方法
					 * 更新的时候, 在更新前执行此方法
					 */
					DBRow _logRow = new DBRow();
					//create/update/delete
					_logRow.put("event", "create");
					//更新的表名
					_logRow.put("table", "product_sn");
					//主键字段名
					_logRow.put("column", "sn_id");
					//主键ID
					_logRow.put("pri_key", snId);
					//依赖表
					_logRow.put("ref_table", "product");
					//依赖表主键字段名
					_logRow.put("ref_column", "pc_id");
					//依赖表主键ID
					_logRow.put("ref_pri_key", pc_id);
					//账号ID
					_logRow.put("create_by", adminLoggerBean.getAdid());
					//设备类型//请求的来源
					_logRow.put("device", ProductDeviceTypeKey.Web);
					//描述
					_logRow.put("describe", "create product_sn "+snId);
					
					//具体更新的字段内容
					//将DBRow转换为数组
					DBRow[] _content = DBRowUtils.dbRowConvertToArray(lsnRow);
					_logRow.put("content", _content);
					
					floorSetupLogMgr.addSetupLog(_logRow);
					
					/*******************************************************************************************************/
				}
				String sn_value = "";
				DBRow[] snList = getProductSnByPcId(pc_id);
				for(DBRow sn : snList){
					long default_value = sn.get("default_value",0l);
					
					if(default_value == 1){
						sn_value = sn.getString("sn_size");
					}
				}
				ret.add("status", "success");
				if(snList.length == 1){
					ret.add("sn", sn_value);
				}else{
					ret.add("sn", sn_value + " ["+snList.length+"]");
				}
				
			}else{
				
				DBRow ps = new DBRow();
				ps.add("sn_size", null);
				fpm.modProduct(pc_id, ps);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow _logRow = new DBRow();
				//create/update/delete
				_logRow.put("event", "update");
				//更新的表名
				_logRow.put("table", "product");
				//主键字段名
				_logRow.put("column", "pc_id");
				//主键ID
				_logRow.put("pri_key", pc_id);
				//依赖表
				_logRow.put("ref_table", "");
				//依赖表主键字段名
				_logRow.put("ref_column", "");
				//依赖表主键ID
				_logRow.put("ref_pri_key", "");
				//账号ID
				_logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				_logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				_logRow.put("describe", "update product "+pc_id);
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] _content = DBRowUtils.dbRowConvertToArray(ps);
				_logRow.put("content", _content);
				
				floorSetupLogMgr.addSetupLog(_logRow);
				
				/*******************************************************************************************************/
				
				ret.add("status", "success");
				ret.add("sn", " ");
			}
			
		} catch (Exception e) {
			ret.add("status", "failed");
			throw new SystemException(e,"updateProductSn",log);
		}
		
		return ret;
	}
	
	public DBRow getDetailProductByPcid(long pcid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductByPcid(pcid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductByPcid",log);
		}
	}
	
	public DBRow getDetailProductStorageByPid(long pid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductStorageByPid(pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductStorageByPid",log);
		}
	}
	
	public DBRow getDetailProductByPname(String pname)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductByPname(pname));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductByPname",log);
		}
	}
	
	//商品条码可不惟一 zyj
	public DBRow[] getDetailProductByPcode(String pcode)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductByPcode(pcode));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductByPcode",log);
		}
	}
	
		
	public DBRow[] getAllProducts(PageCtrl pc)
		throws Exception
	{
		try
		{
//			return (fpm.getAllProductsNoChinese());
			return(fpm.getAllProducts(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllProducts",log);
		}
	}
	
	
	public void modProduct(HttpServletRequest request) throws ProductNameIsExistException,ProductCodeIsExistException,Exception {
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			long pc_id = StringUtil.getLong(request, "pc_id");
			String p_name = StringUtil.getString(request, "p_name");
			p_name = p_name.toUpperCase();
			String p_code = StringUtil.getString(request, "p_code");
			p_code = p_code.toUpperCase();
			long catalog_id = StringUtil.getLong(request, "catalog_id");
			
			String unit_name = StringUtil.getString(request, "unit_name");
			double unit_price = StringUtil.getDouble(request, "unit_price");
			float gross_profit = StringUtil.getFloat(request, "gross_profit");
			float weight = StringUtil.getFloat(request, "weight");
			float volume = StringUtil.getFloat(request,"volume");//商品体积zhanjie
			float length = StringUtil.getFloat(request,"length");
			float height = StringUtil.getFloat(request,"height");
			float width = StringUtil.getFloat(request,"width");
			String lsn = StringUtil.getString(request,"lsn");
			String length_uom = StringUtil.getString(request, "length_uom");
			String weight_uom = StringUtil.getString(request, "weight_uom");
			String price_uom = StringUtil.getString(request, "price_uom");
			String nmfc_code = StringUtil.getString(request,"nmfc_code");
			long freight_class = StringUtil.getLong(request,"freight_class");
			
			//如果商品名称做了修改需要检查是否重复
			DBRow oldDetail = fpm.getDetailProductByPcid(pc_id);
			if (!oldDetail.getString("p_name").equals(p_name))
			{
				DBRow detail = fpm.getDetailProductByPname(p_name);
				if (detail!=null)
				{
					throw new ProductNameIsExistException();
				}
			}
			/*商品条码可不惟一 zyj
			if (!oldDetail.getString("p_code").equals(p_code))
			{
				DBRow detail = fpm.getDetailProductByPcode(p_code);
				if (detail!=null)
				{
					throw new ProductCodeIsExistException();
				}
			}*/
			String product_lsn = null;
			if(StringUtil.isBlank(lsn)){
				product_lsn = null;
			}
			DBRow row = new DBRow();
			row.add("p_name",p_name);
			row.add("unit_name","ITEM");
			row.add("unit_price",unit_price);
			row.add("gross_profit",gross_profit);
			row.add("weight",weight);
			row.add("volume",volume);//添加体积的修改zhanjie
			row.add("freight_class",freight_class);
			row.add("nmfc_code",nmfc_code);
			row.add("sn_size",product_lsn);
			row.add("length",length);
			row.add("heigth",height);
			row.add("width",width);
			row.add("length_uom", length_uom);
			row.add("weight_uom", weight_uom);
			row.add("price_uom", price_uom);
			row.add("volume", length*width*height);
			
			//修改商品//已添加日志
			fpm.modProduct(pc_id,row);

			/*******************************************************************************************************/
			/** 
			 * 创建的时候, 在创建完成后执行此方法
			 * 更新的时候, 在更新前执行此方法
			 */
			DBRow logRow = new DBRow();
			//create/update/delete
			logRow.put("event", "update");
			//更新的表名
			logRow.put("table", "product");
			//主键字段名
			logRow.put("column", "pc_id");
			//主键ID
			logRow.put("pri_key", pc_id);
			//依赖表
			logRow.put("ref_table", "");
			//依赖表主键字段名
			logRow.put("ref_column", "");
			//依赖表主键ID
			logRow.put("ref_pri_key", "");
			//账号ID
			logRow.put("create_by", adminLoggerBean.getAdid());
			//设备类型//请求的来源
			logRow.put("device", ProductDeviceTypeKey.Web);
			//描述
			logRow.put("describe", "update product "+p_name);
			
			//具体更新的字段内容
			//将DBRow转换为数组
			DBRow[] content = DBRowUtils.dbRowConvertToArray(row);
			logRow.put("content", content);
			
			floorSetupLogMgr.addSetupLog(logRow);
			
			/*******************************************************************************************************/
			
			// Yuanxinyu 插入商品与sn的关系表z
			// 根据pc_id删除表中记录  先删在新增
			fpm.delProductSn(pc_id);
			
			if(!StringUtil.isBlank(lsn)){
				
				String[] lsnList = lsn.split(",");
				
				DBRow lsnRow = new DBRow();
				
				for(int i = 0; i < lsnList.length ; i ++){
					
					lsnRow.add("pc_id", pc_id);
					lsnRow.add("sn_size", lsnList[i]);
					if( i == 0){
						// 默认第一个作为default
						lsnRow.add("default_value", 1);
					}else{
						lsnRow.add("default_value", 0);
					}
					lsnRow.add("creator", adminLoggerBean.getAdid());
					lsnRow.add("create_time", DateUtil.NowStr());
					
					//插入数据库操作//已添加日志
					long snId = fpm.addProductSn(lsnRow);
					
					/*******************************************************************************************************/
					/** 
					 * 创建的时候, 在创建完成后执行此方法
					 * 更新的时候, 在更新前执行此方法
					 */
					DBRow snRow = new DBRow();
					//create/update/delete
					snRow.put("event", "create");
					//更新的表名
					snRow.put("table", "product_sn");
					//主键字段名
					snRow.put("column", "sn_id");
					//主键ID
					snRow.put("pri_key", snId);
					//依赖表
					snRow.put("ref_table", "product");
					//依赖表主键字段名
					snRow.put("ref_column", "pc_id");
					//依赖表主键ID
					snRow.put("ref_pri_key", pc_id);
					//账号ID
					snRow.put("create_by", adminLoggerBean.getAdid());
					//设备类型//请求的来源
					snRow.put("device", ProductDeviceTypeKey.Web);
					//描述
					snRow.put("describe", "create product_sn "+snId);
					
					//具体更新的字段内容
					DBRow[] content2 = DBRowUtils.dbRowConvertToArray(lsnRow);
					snRow.put("content", content2);
					
					floorSetupLogMgr.addSetupLog(snRow);
					
					/*******************************************************************************************************/
				}
			}
			
			//记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			if (!oldDetail.getString("p_code").equals(p_code)){
				
				//已添加日志
				productCodeMgrZJ.addProductCodeSub(pc_id, p_code,CodeTypeKey.Main,adminLoggerBean);
			}
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			
			String pcodeString = "";
			
			for (int i = 0; i < pcodes.length; i++) {
				
				pcodeString += pcodes[i].getString("p_code")+" ";
			}
			
			//更新索引
			ProductIndexMgr.getInstance().updateIndexAsyn(pc_id, p_name,pcodeString, catalog_id,unit_name,oldDetail.get("alive", 0));
			
			//如果修改了价格或者重量，则更新所有包含该商品的套装价格和重量//已添加日志
			updateSetPriceAndWeightByPid(pc_id,oldDetail.get("unit_price", 0d),unit_price,oldDetail.get("weight", 0f),weight,adminLoggerBean);
			
		} catch (ProductNameIsExistException e) {
			
			throw new ProductNameIsExistException();
		} catch (ProductCodeIsExistException e) {
			
			throw new ProductCodeIsExistException();
		} catch (Exception e) {
			
			throw new SystemException(e,"modProductCode",log);
		}
	}

	/**
	 * 套装内某个商品重量或价格改变，更新套装的总重量和总价格
	 * @param pid
	 * @param oldPrice
	 * @param newPrice
	 * @param oldWeight
	 * @param newWeight
	 * @throws Exception
	 */
	public void updateSetPriceAndWeightByPid(long pid,double oldPrice,double newPrice,float oldWeight,float newWeight,AdminLoginBean adminLoggerBean) throws Exception{
		
		double tmpUnitprice = 0;
		
		float tmpWeight = 0;
		
		if (oldPrice!=newPrice||oldWeight!=newWeight){
			
			DBRow setPids[] = fpm.getProductUnionsByPid(pid);
			for (int i=0; i<setPids.length; i++){
				
				DBRow productsInSets[] = fpm.getProductsInSetBySetPid(setPids[i].get("set_pid", 0l));
				
				for (int j=0; j<productsInSets.length; j++){
					
					tmpUnitprice += productsInSets[j].get("unit_price", 0d)*productsInSets[j].get("quantity", 0d);
					tmpWeight += productsInSets[j].get("weight", 0f)*productsInSets[j].get("quantity", 0f);
				}
				
				//更新套装的重量和价格
				DBRow newPriceWeight = new DBRow();
				newPriceWeight.add("unit_price", tmpUnitprice);
				newPriceWeight.add("weight", tmpWeight);
				
				fpm.modProduct(setPids[i].get("set_pid", 0l), newPriceWeight);
				
				/*******************************************************************************************************/
				/** 
				 * 创建的时候, 在创建完成后执行此方法
				 * 更新的时候, 在更新前执行此方法
				 */
				DBRow logRow = new DBRow();
				//create/update/delete
				logRow.put("event", "update");
				//更新的表名
				logRow.put("table", "product");
				//主键字段名
				logRow.put("column", "pc_id");
				//主键ID
				logRow.put("pri_key", setPids[i].get("set_pid", 0l));
				//依赖表
				logRow.put("ref_table", "");
				//依赖表主键字段名
				logRow.put("ref_column", "");
				//依赖表主键ID
				logRow.put("ref_pri_key", "");
				//账号ID
				logRow.put("create_by", adminLoggerBean.getAdid());
				//设备类型//请求的来源
				logRow.put("device", ProductDeviceTypeKey.Web);
				//描述
				logRow.put("describe", "create product " + setPids[i].get("set_pid", 0l));
				
				//具体更新的字段内容
				//将DBRow转换为数组
				DBRow[] content = DBRowUtils.dbRowConvertToArray(newPriceWeight);
				logRow.put("content", content);
				
				floorSetupLogMgr.addSetupLog(logRow);
				
				/*******************************************************************************************************/
				
				//修改商品信息，这里是修改关联套装的信息
				DBRow product_log = fpm.getDetailProductByPcid(setPids[i].get("set_pid", 0l));
				
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.PRODUCTUNION);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
				
				tmpUnitprice = 0;
				tmpWeight = 0;
			}
		}
	}

	public void updateSetPriceAndWeightBySetPid(long set_pid,double oldPrice,double newPrice,float oldWeight,float newWeight)
		throws Exception
	{
		double tmpUnitprice = 0;
		float tmpWeight = 0;
		if (oldPrice!=newPrice||oldWeight!=newWeight)
		{
				DBRow productsInSets[] = fpm.getProductsInSetBySetPid(set_pid);
				for (int j=0; j<productsInSets.length; j++)
				{
					tmpUnitprice += productsInSets[j].get("unit_price", 0d)*productsInSets[j].get("quantity", 0d);
					tmpWeight += productsInSets[j].get("weight", 0f)*productsInSets[j].get("quantity", 0f);
				}
				//更新套装的重量和价格
				DBRow newPriceWeight = new DBRow();
				newPriceWeight.add("unit_price", tmpUnitprice);
				newPriceWeight.add("weight", tmpWeight);
				fpm.modProduct(set_pid, newPriceWeight);
				
				//已在引用外边记录日志，因在修改价格与重量前一套装字段被修改，再记录次日志多余
		}
	}

	public DBRow[] getProductByPcid(long pcid,int union_flag,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cids[] = null;
			
			if (pcid>0)
			{
				Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
				ArrayList al = tree.getAllSonNode(pcid);
				al.add(String.valueOf(pcid));
				
				cids = (String[])al.toArray(new String[0]);
			}
	
			return(fpm.getProductInCids( cids, union_flag,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductByPcid",log);
		}
	}
	
//	public DBRow[] getProductNonUsed()
//		throws Exception
//	{
//		try
//		{
//			return(fpm.getProductNonUsed());
//		}
//		catch (Exception e)
//		{
//			throw new SystemException(e,"getProductNonUsed",log);
//		}
//	}
	
	/**
	 * 单独修改库存仓库
	 * @param request 
	 * @throws Exception
	 */
	public void modStoreCatalog(HttpServletRequest request)
		throws ProductIsInStorageException,Exception
	{
		try
		{
			long mod_store_cid = StringUtil.getLong(request, "mod_store_cid");
			long pid = StringUtil.getLong(request, "pid");
			long pcid = StringUtil.getLong(request, "pcid");
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			
			DBRow detailProductStorage = fpm.getDetailProductStorageByPid(pid);
			//先获得之前的跟分类
			DBRow oldRoot[] = tree.getAllFather(detailProductStorage.get("cid", 0l));
			
			boolean productIsExist = false;
			//检查同一个跟父下是否有相同商品
			DBRow productStorages[] = fpm.getProductStoragesByPcid(pcid);
			if (productStorages.length>0)
			{
				//先获得当前需要加入到跟分类
				DBRow targetRoot[] = tree.getAllFather(mod_store_cid);
				
				for (int i=0; i<productStorages.length; i++)
				{
					DBRow existRoot[] = tree.getAllFather(productStorages[i].get("cid", 0l));
					
					//跟父相同
					if ( targetRoot[0].get("id", 0l)==existRoot[0].get("id", 0l)&&existRoot[0].get("id", 0l)!=oldRoot[0].get("id", 0l) )
					{
						productIsExist = true;
						break;
					}
				}
			}

			if (productIsExist)
			{
				throw new ProductIsInStorageException();
			}

			DBRow row = new DBRow();
			row.add("cid",mod_store_cid);
			fpm.modProductStorage(pid,row);
		}
		catch (ProductIsInStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modStoreCatalog",log);
		}
	}

	public DBRow[] getAllProductStorageByPcid(long catalogid,long psid,int type,PageCtrl pc)
		throws Exception
	{
		try
		{
			Tree catalogTree = new Tree(ConfigBean.getStringValue("product_catalog"));
			ArrayList catalogAL = catalogTree.getAllSonNode(catalogid);
			catalogAL.add(String.valueOf(catalogid));
			
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			ArrayList al = tree.getAllSonNode(psid);
			al.add(String.valueOf(psid));
	
			return(fpm.getProductStorageInCids( (String[])catalogAL.toArray(new String[0]), (String[])al.toArray(new String[0]) ,type, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllProductStorageByPcid",log);
		}
	}

	
	public DBRow[] getProductStorages(long ps_id,int type,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow rows[] = fpm.getProductStorages(ps_id,type,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorages",log);
		}
	}
		
	public DBRow[] getNewProductByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow rows[] = fpm.getNewProductByCid(cid,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getNewProductByCid",log);
		}
	}
		
	public DBRow[] getProductByCid(long catalog_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow rows[] = fpm.getProductByCid(catalog_id,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductByCid",log);
		}
	}
	
	public void delProduct(HttpServletRequest request)
		throws ProductInUnionException,ProductHasRelationStorageException,Exception
	{
		try
		{
			long pcid = StringUtil.getLong(request, "pcid");
			
			//商品有组合关系，不能删除
			if (fpm.getProductUnionsById(pcid).length>0)
			{
				throw new ProductInUnionException();
			}
			
			fpm.delProductStorageByPcId(pcid);//先删除库存
			fpm.delProduct(pcid);
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(pcid);
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.DELETE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			ProductIndexMgr.getInstance().deleteIndexAsyn(pcid);
		}
		catch (ProductInUnionException e)
		{
			throw e;
		}
		catch (ProductHasRelationStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProduct",log);
		}
	}
	
	public void delProductStorage(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long pid = StringUtil.getLong(request, "pid");
			fpm.delProductStorage(pid);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductStorage",log);
		}
	}
	
	public DBRow[] getShipProductsByName(String name)
		throws Exception
	{
		try
		{
			return(fpm.getShipProductsByName(name));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShipProductsByName",log);
		}
	}
		
	public DBRow[] getSearchProducts(String key,PageCtrl pc,AdminLoginBean adminLoginBean)
		throws Exception
	{
		try
		{
			DBRow row[] = ProductIndexMgr.getInstance().getSearchResults(key, pc,adminLoginBean);//mergeSearch("merge_info",key,pc);
			return(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchProducts",log);
		}
	}
	
	/**
	 * 单独提供给抄单提示使用
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchProducts4RecordOrder(String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			//只取alive=1
			DBRow filter = new DBRow();
			filter.add("alive", "1");
			DBRow rows[] = ProductIndexMgr.getInstance().getSearchResults(key, filter, pc);
			
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchProducts4RecordOrder",log);
		}
	}
	
	/**
	 * 商品管理使用
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchProducts4CT(String key,PageCtrl pc,AdminLoginBean adminLoginBean)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			DBRow rows[] = ProductIndexMgr.getInstance().getSearchResults(key,pc,adminLoginBean);

			for (int i=0; i<rows.length; i++)
			{
				DBRow detail = fpm.getDetailProductByPcid(StringUtil.getLong(rows[i].getString("pc_id")));
				al.add(detail);
			}
			return((DBRow[])al.toArray(new DBRow[0]));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchProducts4CT",log);
		}
	}

	/**
	 * 支持分类过滤搜索（不支持分页 - 还需要完善）
	 * @param key
	 * @param cid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchProducts4CT(String key,long cid,PageCtrl pc,AdminLoginBean adminLoginBean)
		throws Exception
	{
		try
		{
			ArrayList<String> result = new ArrayList<String>();
			
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			ArrayList sonCatalog = tree.getAllSonNode(cid);
			sonCatalog.add(String.valueOf(cid));
			
			DBRow products[] = ProductIndexMgr.getInstance().getSearchResults( key, null,adminLoginBean);
			for (int i=0; i<products.length; i++)
			{
				DBRow product = fpm.getDetailProductInCids(StringUtil.getLong(products[i].getString("pc_id")), (String[])sonCatalog.toArray(new String[0]));
				
				if (product!=null)
				{
					result.add(product.getString("pc_id"));
				}
			}

			return( fpm.getProductsByPcids((String[])result.toArray(new String[0]), pc) );
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchProducts4CT",log);
		}
	}

	/**
	 * 验证创建的组合关系正确性
	 * @param p_name
	 * @param parentid
	 * @return
	 * @throws ProductUnionSetCanBeProductException
	 * @throws ProductUnionIsInSetException
	 * @throws ProductNotExistException
	 * @throws Exception
	 */
	public long validateProductUnion(long set_pid,long pcid)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		DBRow detailP = fpm.getDetailProductByPcid(pcid);
				//fpm.getDetailProductByPname(p_name);
		
		//检测商品是否存在
		if (detailP==null)
		{
			throw new ProductNotExistException();
		}

		//已经设置了的组合套装不能做子商品
		if (detailP.get("union_flag", 0)==1)
		{
			throw new ProductUnionSetCanBeProductException();
		}
		
		//同一个组合套装下，商品不能重复
		if (fpm.getDetailProductInSet(set_pid, pcid)!=null)
		{
			throw new ProductUnionIsInSetException();
		}
		//已经作为组合的商品，不能变成组合，在其下创建商品
		if (fpm.getProductUnionsByPid(set_pid).length>0)
		{
			throw new ProductCantBeSetException();
		}
		
		return(pcid);
	}
	
	public void validateProductSuit(HttpServletRequest request) throws ProductCantBeSetException,Exception{
		
		long set_pid = StringUtil.getLong(request,"set_pid");
		
		//已经作为组合的商品，不能变成组合，在其下创建商品
		if (fpm.getProductUnionsByPid(set_pid).length>0) {
			
			throw new ProductCantBeSetException();
		}
	}
	
	/**
	 * 验证创建的组合关系正确性
	 * @param p_name
	 * @param parentid
	 * @return
	 * @throws ProductUnionSetCanBeProductException
	 * @throws ProductUnionIsInSetException
	 * @throws ProductNotExistException
	 * @throws Exception
	 */
	public long validateProductUnion(long set_pid,String p_name)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		long pcid = 0;
		DBRow detailP = fpm.getDetailProductByPname(p_name);
		
		//检测商品是否存在
		if (detailP==null)
		{
			throw new ProductNotExistException();
		}
		else
		{
			pcid = detailP.get("pc_id", 0L);
		}

		//已经设置了的组合套装不能做子商品
		if (detailP.get("union_flag", 0)==1)
		{
			throw new ProductUnionSetCanBeProductException();
		}
		
		//同一个组合套装下，商品不能重复
		if (fpm.getDetailProductInSet(set_pid, pcid)!=null)
		{
			throw new ProductUnionIsInSetException();
		}
		//已经作为组合的商品，不能变成组合，在其下创建商品
		if (fpm.getProductUnionsByPid(set_pid).length>0)
		{
			throw new ProductCantBeSetException();
		}
		
		return(pcid);
	}
	
	/**
	 * 创建组合关系
	 * @param request
	 * @throws ProductNotExistException
	 * @throws Exception
	 */
	public void addProductUnion(HttpServletRequest request)throws Exception
	{
		
		try {
			
			long pc_id = StringUtil.getLong(request, "pc_id");
			float quantity = StringUtil.getFloat(request, "quantity");
			long set_pid = StringUtil.getLong(request, "set_pid");
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			addProductUnionSub(pc_id,quantity,set_pid,adminLoggerBean);
			
			
			
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch (ProductCodeIsExistException e)
		{
			throw e;
		}
		catch(OperationNotPermitException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"addProductUnion",log);
		}
	}
	
	//添加组合套装 提交
	public void addProductUnionSub(long pc_id, float quantity, long set_pid,AdminLoginBean adminLoggerBean)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		try
		{
			long pid = validateProductUnion(set_pid,pc_id);		//验证通过后，获得商品ID，验证失败会抛出相应异常

			if (quantity==0)
			{
				quantity = 1;
			}

			DBRow pUnion = new DBRow();
			pUnion.add("set_pid", set_pid);
			pUnion.add("pid", pid);
			pUnion.add("quantity", quantity);

			fpm.addProductUnion(pUnion);
			
			/**
			 * 增加了组合关系后，需要把父商品标记为组合商品
			 * 1 - 组合 
			 */
			DBRow markProSet = new DBRow();
			markProSet.add("union_flag",1);
			fpm.modProduct(set_pid, markProSet);
			
			updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(set_pid);
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("pc_id", set_pid);
			Map<String,Object> productNode = new HashMap<String, Object>();
			productNode.put("pc_id", set_pid);
			productNode.put("union_flag",1);
			productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//修改商品节点
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProductUnion",log);
		}
	}
	
	
	//添加组合套装 提交
		public void addProductUnionSub(String pc_name, float quantity, long set_pid,AdminLoginBean adminLoggerBean)
			throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
		{
			try
			{
				long pid = validateProductUnion(set_pid,pc_name);		//验证通过后，获得商品ID，验证失败会抛出相应异常

				if (quantity==0)
				{
					quantity = 1;
				}

				DBRow pUnion = new DBRow();
				pUnion.add("set_pid", set_pid);
				pUnion.add("pid", pid);
				pUnion.add("quantity", quantity);

				fpm.addProductUnion(pUnion);
				
				/**
				 * 增加了组合关系后，需要把父商品标记为组合商品
				 * 1 - 组合 
				 */
				DBRow markProSet = new DBRow();
				markProSet.add("union_flag",1);
				fpm.modProduct(set_pid, markProSet);
				
				updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
				
				//根据商品ID获得商品信息,记录商品修改日志
				DBRow product_log = fpm.getDetailProductByPcid(set_pid);
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
				
				
			//	//system.out.println("定位添加套装");
			}
			catch (ProductUnionSetCanBeProductException e)
			{
				throw e;
			}
			catch (ProductNotExistException e)
			{
				throw e;
			}
			catch (ProductUnionIsInSetException e)
			{
				throw e;
			}
			catch (ProductCantBeSetException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw new SystemException(e,"addProductUnion",log);
			}
		}

	/**
	 * 删除组合关系
	 * 如果是删除一个组合的商品，需要检测删除后，该组合是否还有商品，如果没有，则把组合也一起删除
	 * @param request
	 * @throws Exception
	 */
	public void delProductUnion(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request,"set_pid");
			long pid = StringUtil.getLong(request,"pid");
			long pc_id = StringUtil.getLong(request,"pc_id");

			if ( set_pid>0&&pid>0 )
			{
				fpm.delProductUnion(set_pid,pid);
			}

			//删除后，需要对组合商品进行一次统计，如果其下没有子商品，需要修改他的组合标志为普通商品
			DBRow productsInSet[] = fpm.getProductsInSetBySetPid(set_pid);
			if (productsInSet.length==0)
			{
				DBRow markNoralPro = new DBRow();
				markNoralPro.add("union_flag","0");
				fpm.modProduct(set_pid, markNoralPro);
			}
			
			updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(set_pid);
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductUnion",log);
		}
	}

	/**
	 * 修改组合关系
	 * @param request
	 * @throws ProductUnionSetCanBeProductException
	 * @throws ProductUnionIsInSetException
	 * @throws ProductNotExistException
	 * @throws Exception
	 */
	public void modProductUnion(HttpServletRequest request)
		throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		try
		{
			String p_name = StringUtil.getString(request, "p_name");
			long set_pid = StringUtil.getLong(request, "set_pid");
			long pid = StringUtil.getLong(request, "pid");
			float quantity = StringUtil.getFloat(request, "quantity");
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			
			this.modProductUnionSub(p_name, set_pid, pid, quantity, adminLoggerBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProductUnion",log);
		}
	}
	//修改商品套装提交
	public void modProductUnionSub(String p_name,long set_pid,long pid,float quantity,AdminLoginBean adminLoggerBean)
	throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception
	{
		try{
			long old_pid = pid;
			
			DBRow detail = fpm.getDetailProductByPcid(pid);

			//判断商品是否有被修改，如果被修改了，需要检测并获得新PID
//			if (!detail.getString("p_name").equals(p_name))
//			{
//				pid = validateProductUnion(set_pid,p_name);		//验证通过后，获得商品ID，验证失败会抛出相应异常
//			}
			
			DBRow pUnion = new DBRow();
			pUnion.add("pid", pid);
			pUnion.add("quantity", quantity);
			fpm.modProductUnion(set_pid,old_pid, pUnion);
			
			updateSetPriceAndWeightBySetPid(set_pid,0,1,0,1);//强制更新套装价格和重量
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(set_pid);
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProductUnionSub",log);
		}
	}
	/**
	 * 获得组合下某个商品的详细信息
	 * @param set_pid
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductInSet(long set_pid,long pid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductInSet( set_pid, pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductInSet",log);
		}
	}

	public DBRow[] getProductsInSetBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			return(fpm.getProductsInSetBySetPid(set_pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductsInSetBySetPid",log);
		}
	}

	/**
	 * 计算产品库存是否足够
	 * 首先要区分是否组合商品
	 * 如果是组合商品库存不够，再拆分组合商品，检查子商品库存是否足够
	 * @param product
	 * @return
	 * @throws Exception 
	 */
//	public boolean isLackingProduct(DBRow product,float quantity,int product_type) 
//		throws Exception
//	{
//		try
//		{
//			boolean isLacking = false;
//			if (product.getString("store_count").equals(""))
//			{
//				product.add("store_count",0);
//			}
//
//			if (product.get("store_count", 0f)<=0||product.get("store_count", 0f)<quantity)
//			{
//				isLacking = true;
//			}
//
//			//如果是组合商品缺货了，需要进一步拆分判断子商品是否有货
//			if (isLacking&&(product_type==ProductTypeKey.UNION_STANDARD||product_type==ProductTypeKey.UNION_STANDARD_MANUAL||product_type==ProductTypeKey.UNION_CUSTOM))	//组合商品
//			{
//				isLacking = false;
//				float currentStoreCount = product.get("store_count", 0f);
//				if (currentStoreCount<0)
//				{
//					currentStoreCount = 0;
//				}
//				
//				float currentNeedQuantity = quantity - currentStoreCount;
//				////system.out.println("1==> "+currentNeedQuantity);
//				
//				//计算组合中每一个子商品需要的库存，只要其中一个不够，该组合即缺货
//				DBRow productsInSet[];
//				
//				if (product_type==ProductTypeKey.UNION_STANDARD||product_type==ProductTypeKey.UNION_STANDARD_MANUAL)//标准套装、手工拼装
//				{
//					productsInSet = fpm.getProductsInSetBySetPid(product.get("pc_id", 0l));
//				}
//				else //定制套装
//				{
//					productsInSet = fpm.getProductsCustomInSetBySetPid(product.get("pc_pc_id", 0l));
//				}
//				
//				for (int i=0; i<productsInSet.length; i++)
//				{
//					float productsInSetCount = productsInSet[i].get("quantity", 0f)*currentNeedQuantity;
//					DBRow productStorage = fpm.getDetailProductStorageByPcid(productsInSet[i].get("pid", 0l));
//					////system.out.println("2==> "+productsInSetCount+" - "+productStorage.get("store_count", 0f));
//					
//					if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
//					{
//						isLacking = true;
//						break;
//					}
//				}
//			}
//			
//			return(isLacking);
//		}
//		catch (Exception e)
//		{
//			throw new SystemException(e,"isLackingProduct",log);
//		}
//	}
	
	/**
	 * 提供给订单管理页面用，用来判断订单中的商品是否缺货
	 * @param pid
	 * @param quantity
	 * @param product_type
	 * @return
	 * @throws Exception
	 */
//	public boolean isLackingProduct4OrderPage(long pid,float quantity,int product_type) 
//		throws Exception
//	{
//		DBRow detailP;
//		
//		if (product_type!=ProductTypeKey.UNION_CUSTOM)
//		{
//			//先从商品和仓库联合查询，如果是NULL，再单独查商品表
//			detailP = fpm.getDetailProductProductStorageByPcid(pid);
//			
//			if (detailP==null)	//	商品还没进库，当然缺货
//			{
//				detailP = fpm.getDetailProductByPcid(pid);
//				detailP.add("store_count",0);
//			}
//			
//			//手工拼装为虚拟商品，他具有标准套装的特性，但是库存是有拼装的散件数决定
//			if (product_type==ProductTypeKey.UNION_STANDARD_MANUAL)
//			{
//				detailP.add("store_count",0);
//			}
//		}
//		else
//		{
//			detailP = fpm.getDetailProductCustomByPcPcid(pid);
//		}
//		
//		return( isLackingProduct(detailP, quantity, product_type) );
//	}
	
	
	
	/**
	 * 关联库存查询商品，获得商品信息和库存信息
	 * @param pcid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductProductStorageByPcid(long root_storage_catalog_id,long pcid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductProductStorageByPcid(root_storage_catalog_id,pcid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductProductStorageByPcid",log);
		}
	}
	
	/**
	 * 通过商品名关联库存查询商品，获得商品信息和库存信息
	 * @param root_storage_catalog_id
	 * @param p_name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductProductStorageByPName(long root_storage_catalog_id,String p_name)
		throws Exception
	{
		
		try
		{
			return(fpm.getDetailProductProductStorageByPName(root_storage_catalog_id,p_name));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductProductStorageByPName",log);
		}
	}

	/**
	 * 保存定制套装(新增和修改通用)
	 * @param pc
	 * @throws Exception
	 */
	public void customProduct(HttpServletRequest request)
		throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request, "set_pid");
			String p_name = StringUtil.getString(request, "p_name");
			String cmd = StringUtil.getString(request, "cmd");
			long pc_pc_id = 0;
			
			CustomCart customCart = (CustomCart)MvcUtil.getBeanFromContainer("customCart");
			Cart cart = (Cart)MvcUtil.getBeanFromContainer("cart");
			
			DBRow cart_products[] = customCart.getDetailProducts(StringUtil.getSession(request),set_pid);
			
			//需要检查下是否对原来套装作出了修改，如果没有修改，直接返回，不保存
			boolean noChangeFlag = true;

			/**
			 * 检查定制套装相对标准套装是否做出了修改
			 * 只在新定制才检查，修改定制不检查
			 */
			if (cmd.equals("add"))
			{
				DBRow oldProductsInSet[] = fpm.getProductsInSetBySetPid(set_pid);
				//数量跟原来相同需要检测
				if (oldProductsInSet.length==cart_products.length)
				{
					ArrayList oldProductsInSetAl = new ArrayList();
					for (int i=0; i<oldProductsInSet.length; i++)
					{
						oldProductsInSetAl.add(oldProductsInSet[i].getString("pc_id")+"_"+oldProductsInSet[i].getString("quantity"));
					}
					
					for (int i=0; i<cart_products.length; i++)
					{
						oldProductsInSetAl.remove(cart_products[i].getString("pc_id")+"_"+cart_products[i].getString("cart_quantity"));
					}
					
					if (oldProductsInSetAl.size()>0)
					{
						noChangeFlag = false;
					}
				}
				else
				{
					noChangeFlag = false;
				}
			}
			else
			{
				noChangeFlag = false;
			}

			/**
			 * 新增或修改定制，做两件事：
			 * 1、把购物车原来标准套装变成定制套装
			 * 2、把临时定制购物车中的定制商品，拷贝到正式定制购物车中（抄单会从这里获得定制数据）
			 */
			if (!noChangeFlag)
			{
				//小购物车商品变化，究竟是新增还是修改？
				if (cmd.equals("add"))
				{
					//保存新定制套装
					DBRow original_product = fpm.getDetailProductByPcid(set_pid);
					
					DBRow productCustom = new DBRow();
					productCustom.add("unit_name",original_product.getString("unit_name"));
					productCustom.add("length",original_product.get("length",0f));
					productCustom.add("width",original_product.get("width",0f));
					productCustom.add("heigth",original_product.get("heigth",0f));
					productCustom.add("orignal_pc_id",set_pid);
					productCustom.add("p_name",p_name+" ▲");
					productCustom.add("catalog_id",fpm.getDetailProductByPcid(set_pid).get("catalog_id", 0l));
					
					pc_pc_id = fpm.addProductCustom(productCustom);
					
					
					DBRow productsCustomInSet[] = cart_products;
					
					
					double custom_unit_price = 0;
					float custom_weight = 0;
					for (int j=0; j<productsCustomInSet.length; j++)
					{
						//插入到定制表
						DBRow customPro = new DBRow();
						customPro.add("set_pid",pc_pc_id);
						customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
						customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
						fpm.addProductCustomUnion(customPro);
						
						DBRow union_product = fpm.getDetailProductByPcid(productsCustomInSet[j].get("pc_id",0l));
						
						double unit_price= union_product.get("unit_price",0d);
						float count = productsCustomInSet[j].get("cart_quantity",0f);
						
						custom_unit_price += unit_price*count;
						custom_weight += union_product.get("weight",0f)*productsCustomInSet[j].get("cart_quantity",0f);
					}
					
					DBRow paraCustomProduct = new DBRow();
					paraCustomProduct.add("unit_price",custom_unit_price);
					paraCustomProduct.add("weight",custom_weight);
					
					fpm.modProduct(pc_pc_id,paraCustomProduct);
					
					//定制商品不记录商品日志
				}
				else
				{
					pc_pc_id = set_pid;//修改，商品ID就是大购物车中的ID
					
					fpm.delProductCustomUnionBySetPid(pc_pc_id);//删除原有定制关系，后边新增
					
					double custom_unit_price = 0;
					float custom_weight = 0;
					
					DBRow productsCustomInSet[] = cart_products;
					
					for (int j=0; j<productsCustomInSet.length; j++)
					{
						//插入到定制表
						DBRow customPro = new DBRow();
						customPro.add("set_pid",pc_pc_id);
						customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
						customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
						fpm.addProductCustomUnion(customPro);
						
						DBRow union_product = fpm.getDetailProductByPcid(productsCustomInSet[j].get("pc_id",0l));
						
						custom_unit_price += union_product.get("unit_price",0d)*productsCustomInSet[j].get("cart_quantity",0f);
						custom_weight += union_product.get("weight",0f)*productsCustomInSet[j].get("cart_quantity",0f);
					}
					
					DBRow paraCustomProduct = new DBRow();
					paraCustomProduct.add("unit_price",custom_unit_price);
					paraCustomProduct.add("weight",custom_weight);
					
					fpm.modProduct(pc_pc_id,paraCustomProduct);
					
//					定制商品不记录商品日志
				}
				
				

				//把大购物车中的普通套装，变成定制套装
				cart.convert2CustomProduct(StringUtil.getSession(request), set_pid, pc_pc_id);
				//把临时定制购物车内容拷贝到最终版
				customCart.copyTempSet2FinalSet(StringUtil.getSession(request), set_pid,pc_pc_id);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"customProduct",log);
		}
	}
	
	/**
	 * 保存定制套装(新增和修改通用)
	 * @param pc
	 * @throws Exception
	 */
	public void customProductQuote(HttpServletRequest request)
		throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request, "set_pid");
			String p_name = StringUtil.getString(request, "p_name");
			String cmd = StringUtil.getString(request, "cmd");
			long pc_pc_id = 0;
			
			CustomCartQuote customCart = (CustomCartQuote)MvcUtil.getBeanFromContainer("customCartQuote");
			CartQuote cart = (CartQuote)MvcUtil.getBeanFromContainer("cartQuote");
			
			DBRow cart_products[] = customCart.getDetailProducts(StringUtil.getSession(request),set_pid);
			
			//需要检查下是否对原来套装作出了修改，如果没有修改，直接返回，不保存
			boolean noChangeFlag = true;

			/**
			 * 检查定制套装相对标准套装是否做出了修改
			 * 只在新定制才检查，修改定制不检查
			 */
			if (cmd.equals("add"))
			{
				DBRow oldProductsInSet[] = fpm.getProductsInSetBySetPid(set_pid);
				//数量跟原来相同需要检测
				if (oldProductsInSet.length==cart_products.length)
				{
					ArrayList oldProductsInSetAl = new ArrayList();
					for (int i=0; i<oldProductsInSet.length; i++)
					{
						oldProductsInSetAl.add(oldProductsInSet[i].getString("pc_id")+"_"+oldProductsInSet[i].getString("quantity"));
					}
					
					for (int i=0; i<cart_products.length; i++)
					{
						oldProductsInSetAl.remove(cart_products[i].getString("pc_id")+"_"+cart_products[i].getString("cart_quantity"));
					}
					
					if (oldProductsInSetAl.size()>0)
					{
						noChangeFlag = false;
					}
				}
				else
				{
					noChangeFlag = false;
				}
			}
			else
			{
				noChangeFlag = false;
			}

			/**
			 * 新增或修改定制，做两件事：
			 * 1、把购物车原来标准套装变成定制套装
			 * 2、把临时定制购物车中的定制商品，拷贝到正式定制购物车中（抄单会从这里获得定制数据）
			 */
			if (!noChangeFlag)
			{

				//小购物车商品变化，究竟是新增还是修改？
				if (cmd.equals("add"))
				{
					//保存新定制套装
					DBRow original_product = fpm.getDetailProductByPcid(set_pid);
					
					DBRow productCustom = new DBRow();
					productCustom.add("unit_name",original_product.getString("unit_name"));
					productCustom.add("length",original_product.get("length",0f));
					productCustom.add("width",original_product.get("width",0f));
					productCustom.add("heigth",original_product.get("heigth",0f));
					productCustom.add("orignal_pc_id",set_pid);
					productCustom.add("p_name",p_name+" ▲");
					productCustom.add("catalog_id",fpm.getDetailProductByPcid(set_pid).get("catalog_id", 0l));
					
					pc_pc_id = fpm.addProductCustom(productCustom);
					
					
					DBRow productsCustomInSet[] = cart_products;
					
					
					double custom_unit_price = 0;
					float custom_weight = 0;
					for (int j=0; j<productsCustomInSet.length; j++)
					{
						//插入到定制表
						DBRow customPro = new DBRow();
						customPro.add("set_pid",pc_pc_id);
						customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
						customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
						fpm.addProductCustomUnion(customPro);
						
						DBRow union_product = fpm.getDetailProductByPcid(productsCustomInSet[j].get("pc_id",0l));
						
						double unit_price= union_product.get("unit_price",0d);
						float count = productsCustomInSet[j].get("cart_quantity",0f);
						
						custom_unit_price += unit_price*count;
						custom_weight += union_product.get("weight",0f)*productsCustomInSet[j].get("cart_quantity",0f);
					}
					
					DBRow paraCustomProduct = new DBRow();
					paraCustomProduct.add("unit_price",custom_unit_price);
					paraCustomProduct.add("weight",custom_weight);
					
					fpm.modProduct(pc_pc_id,paraCustomProduct);
					
					//定制商品不记录商品日志
				}
				else
				{
					pc_pc_id = set_pid;//修改，商品ID就是大购物车中的ID
					
					fpm.delProductCustomUnionBySetPid(pc_pc_id);//删除原有定制关系，后边新增
					
					double custom_unit_price = 0;
					float custom_weight = 0;
					
					DBRow productsCustomInSet[] = cart_products;
					
					for (int j=0; j<productsCustomInSet.length; j++)
					{
						//插入到定制表
						DBRow customPro = new DBRow();
						customPro.add("set_pid",pc_pc_id);
						customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
						customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
						fpm.addProductCustomUnion(customPro);
						
						DBRow union_product = fpm.getDetailProductByPcid(productsCustomInSet[j].get("pc_id",0l));
						
						custom_unit_price += union_product.get("unit_price",0d)*productsCustomInSet[j].get("cart_quantity",0f);
						custom_weight += union_product.get("weight",0f)*productsCustomInSet[j].get("cart_quantity",0f);
					}
					
					DBRow paraCustomProduct = new DBRow();
					paraCustomProduct.add("unit_price",custom_unit_price);
					paraCustomProduct.add("weight",custom_weight);
					
					fpm.modProduct(pc_pc_id,paraCustomProduct);
					
//					定制商品不记录商品日志
				}
				
				

				//把大购物车中的普通套装，变成定制套装
				cart.convert2CustomProduct(StringUtil.getSession(request), set_pid, pc_pc_id);
				//把临时定制购物车内容拷贝到最终版
				customCart.copyTempSet2FinalSet(StringUtil.getSession(request), set_pid,pc_pc_id);
				
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"customProductQuote",log);
		}
	}
	
	/**
	 * 获得定制套装详细信息
	 * @param pcpcid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductCustomByPcPcid(long pcpcid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProductCustomByPcPcid(pcpcid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductCustomByPcPcid",log);
		}
	}

	/**
	 * 获得定制套装内的详细商品
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductsCustomInSetBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			return(fpm.getProductsCustomInSetBySetPid(set_pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductsCustomInSetBySetPid",log);
		}
	}
	
	/**
	 * 创建仓库、国家关系
	 * @param request
	 * @throws Exception
	 */
	public void addStorageCountry(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cid = StringUtil.getLong(request, "cid");
			long sid = StringUtil.getLong(request, "sid");
			
			DBRow row = new DBRow();
			row.add("cid",cid);
			row.add("sid",sid);
			
			fpm.addStorageCountry(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addStorageCountry",log);
		}
	}
		
	/**
	 * 移动国家
	 * @param request
	 * @throws Exception
	 */
	public void moveStorageCountry(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cid = StringUtil.getLong(request, "cid");
			long sid = StringUtil.getLong(request, "sid");
			
			//先删除原来的映射
			fpm.delStorageCountryByCid(cid);
			
			//再重新插入
			DBRow row = new DBRow();
			row.add("cid",cid);
			row.add("sid",sid);
			
			fpm.addStorageCountry(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"moveStorageCountry",log);
		}
	}
	
	/**
	 * 通过仓库ID获得对应国家
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageCountryBySid(long sid)
		throws Exception
	{
		try
		{
			return(fpm.getStorageCountryBySid(sid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageCountryBySid",log);
		}
	}
	
	/**
	 * 获得没有分配仓库关联的国家
	 * @param psid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFreeStorageCountrys(long psid)
		throws Exception
	{
		try
		{
			return(fpm.getFreeStorageCountrys(psid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFreeStorageCountrys",log);
		}
	}
	
	/**
	 * 通过国家，获得发货仓库
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailStorageCountryByCid(long cid)
		throws Exception
	{
		try
		{
			return(fpm.getDetailStorageCountryByCid(cid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailStorageCountryByCid",log);
		}
	}
	
	/**
	 * 通过商品ID，获得所有仓库下的库存信息
	 * @param pcid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoragesByPcid(long pcid)
		throws Exception
	{
		try
		{
			return(fpm.getProductStoragesByPcid(pcid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStoragesByPcid",log);
		}
	}
	
	/**
	 * 获得统计库存
	 * @param psid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatProductStorage(long ps_id,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			return(fpm.getStatProductStorage(ps_id,pscid,catalog_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatProductStorage",log);
		}
	}

	/**
	 * 获得统计库存商品的分类
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatProductStoragePC(long pscid)
		throws Exception
	{
		try
		{
			return(fpm.getStatProductStoragePC(pscid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatProductStoragePC",log);
		}
	}
	
	/**
	 * 批量修改商品
	 * @param request
	 * @throws Exception
	 */
	public void batchModProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String batch_pc_id[] = request.getParameterValues("batch_pc_id");
	
			for (int i=0; batch_pc_id!=null&&i<batch_pc_id.length; i++)
			{
				long pc_id = StringUtil.getLong(batch_pc_id[i]);
				DBRow oldDetail = fpm.getDetailProductByPcid(pc_id);

				double unit_price = StringUtil.getDouble(request, "unit_price_"+batch_pc_id[i]);
				float weight = StringUtil.getFloat(request, "weight_"+batch_pc_id[i]);
				
				DBRow row = new DBRow();
				row.add("p_name",StringUtil.getString(request, "p_name_"+batch_pc_id[i]));
				row.add("p_code",StringUtil.getString(request, "p_code_"+batch_pc_id[i]));
				row.add("unit_name",StringUtil.getString(request, "unit_name_"+batch_pc_id[i]));
				row.add("weight",weight);
				row.add("unit_price",unit_price);
				row.add("gross_profit",StringUtil.getDouble(request, "gross_profit_"+batch_pc_id[i]));

				fpm.modProduct(pc_id,row);
				
				//根据商品ID获得商品信息,记录商品修改日志
				DBRow product_log = fpm.getDetailProductByPcid(pc_id);
				AdminMgr am = new AdminMgr();
				AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
				product_log.add("account",adminLoggerBean.getAdid());
				product_log.add("edit_type",ProductEditTypeKey.UPDATE);
				product_log.add("edit_reason",ProductEditReasonKey.SELF);
				product_log.add("post_date",DateUtil.NowStr());
				floorProductLogsMgrZJ.addProductLogs(product_log);
				
				
				
				//重新修改索引
				DBRow newProduct = fpm.getDetailProductByPcid(StringUtil.getLong(batch_pc_id[i]));
				
				DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(newProduct.get("pc_id",0l));
				String pcodeString = "";
				for (int j = 0; j < pcodes.length; j++) 
				{
					pcodeString += pcodes[j].getString("p_code")+" ";
				}
				
				ProductIndexMgr.getInstance().updateIndexAsyn(newProduct.get("pc_id",0l), newProduct.getString("p_name"),pcodeString,newProduct.get("catalog_id",0l) ,newProduct.getString("unit_name"),newProduct.get("alive", 0));
				
				//如果修改了价格或者重量，则更新所有包含该商品的套装价格和重量
				updateSetPriceAndWeightByPid(pc_id,oldDetail.get("unit_price", 0d),unit_price,oldDetail.get("weight", 0f),weight,adminLoggerBean);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModProduct",log);
		}
	}
	
	/**
	 * 获得统计商品的分类
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatProductPC(long pscid)
		throws Exception
	{
		try
		{
			return(fpm.getStatProductPC(pscid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatProductPC",log);
		}
	}
	
	/**
	 * 获得统计分类下的商品
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatProduct(long catalog_id)
		throws Exception
	{
		try
		{
			return(fpm.getStatProduct(catalog_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatProduct",log);
		}
	}

	/**
	 * 通过商品ID，获得所有套装映射
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductUnionsByPid(long pid)
		throws Exception
	{
		try
		{
			return(fpm.getProductUnionsByPid(pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductUnionsByPid",log);
		}
	}

	/**
	 * 通过套装ID，获得套装下所有商品映射
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductUnionsBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			return(fpm.getProductUnionsBySetPid(set_pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductUnionsBySetPid",log);
		}
	}

	

	/**
	 * 插入库存进出日志
	 * @param productStoreLogBean
	 * @throws Exception
	 */
	public long addProductStoreLogs(ProductStoreLogBean productStoreLogBean)
		throws Exception
	{
		try
		{
			DBRow psLog = new DBRow();
			
			psLog.add("oid",productStoreLogBean.getOid());
			psLog.add("operation",productStoreLogBean.getOperation());
			psLog.add("quantity",productStoreLogBean.getQuantity());
			psLog.add("ps_id",productStoreLogBean.getPs_id());
			psLog.add("pc_id",productStoreLogBean.getPc_id());
			psLog.add("account",productStoreLogBean.getAccount());
			
			psLog.add("post_date", DateUtil.NowStr());
			psLog.add("bill_type",productStoreLogBean.getBill_type());
			psLog.add("adid",productStoreLogBean.getAdid());
			psLog.add("quantity_type",productStoreLogBean.getQuantity_type());
			psLog.add("cancel_psl_id",productStoreLogBean.getCancel_psl_id());
			
//			psLog.add("log_timestamp",System.currentTimeMillis());
//			psLog.add("merge_count",productStoreLogBean.getMerge_count());
//			psLog.add("lacking_quantity",productStoreLogBean.getLacking_quantity());
//			psLog.add("store_count",productStoreLogBean.getStore_count());
			
			return fpm.addProductStoreLogs(psLog);
			
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProductStoreLogs",log);
		}
	}
	
	/**
	 * 验证商品是否已经在发货建库
	 * @param products 字段：pid,product_type
	 * @throws ProductNotCreateStorageException
	 * @throws Exception
	 */
	public void validateProductsCreateStorage(long ps_id,DBRow products[])
		throws ProductNotCreateStorageException,Exception
	{
		try
		{
			/**
			 * 购物车商品只需要验证：普通、套装和定制
			 * 自动拆分因为在加入购物车的时候已经验证了，无需再验证
			 */
			
			for (int i=0; i<products.length; i++)
			{
				long pid = StringUtil.getLong(products[i].getString("cart_pid"));
				long product_type = StringUtil.getInt(products[i].getString("cart_product_type"));
				
				DBRow detailP = fpm.getDetailProductByPcid(pid);
				DBRow detailProductStorage = fpm.getDetailProductProductStorageByPcid(ps_id, pid);
				if (detailProductStorage==null)
				{
					//如果是套装没建库，则检测散件是否建库
					//detailP==null 即定制套装
					if (detailP==null||detailP.get("union_flag", 0)==1)
					{
						//散件有两种情况：
						//标准套装和定制
						DBRow inSetProducts[];
						if (product_type==ProductTypeKey.UNION_CUSTOM)//detailP==null
						{
							inSetProducts = fpm.getProductsCustomInSetBySetPid(pid);
						}
						else
						{
							inSetProducts = fpm.getProductsInSetBySetPid(pid);
						}
						
						for (int j=0; j<inSetProducts.length; j++)
						{
							if ( fpm.getDetailProductProductStorageByPcid(ps_id, inSetProducts[j].get("pc_id", 0l))==null )
							{
								throw new ProductNotCreateStorageException( fpm.getDetailProductByPcid(inSetProducts[j].get("pc_id", 0l)).getString("p_name") );
							}
						}
					}
					else
					{
						throw new ProductNotCreateStorageException(detailP.getString("p_name"));
					}
				}
			}
		}
		catch (ProductNotCreateStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"validateProductsCreateStorage",log);
		}
	}

	/**
	 * 转换商品库存(只能同仓库转换)
	 * @param request
	 * @throws Exception
	 */
	public void convertProduct(HttpServletRequest request)
		throws ConvertQuantityIncorrectException,ProductNotExistException,ConvertNotBeSelfException,ProductNotCreateStorageException,ConvertNotSameTypeException,Exception
	{
		try
		{
			long s_pid = StringUtil.getLong(request,"s_pid");
			long s_pc_id = StringUtil.getLong(request,"s_pc_id");
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			String p_name = StringUtil.getString(request, "p_name");
			float quantity = StringUtil.getFloat(request, "quantity");
			
			DBRow detailSourceP = fpm.getDetailProductByPcid(s_pc_id);
			DBRow detailTargetP = fpm.getDetailProductByPname(p_name);
			DBRow detailSourceProductStorage = fpm.getDetailProductStorageByPid(s_pid);
			
			//源商品库存必须大于0，而且转换数目，必须<=当前库存
			if ( detailSourceProductStorage.get("store_count", 0d)<0||quantity>detailSourceProductStorage.get("store_count", 0d) )
			{
				throw new ConvertQuantityIncorrectException();
			}
			
			//目标商品不存在
			if (detailTargetP==null)
			{
				throw new ProductNotExistException();
			}
			
			//转换商品不能是自己
			if ( detailSourceP.get("pc_id", 0l)==detailTargetP.get("pc_id", 0l) )
			{
				throw new ConvertNotBeSelfException();
			}
			
			//检查转换的是否同一类商品（不能把套装转换成普通商品）
			if ( detailSourceP.get("union_flag", 0)!=detailTargetP.get("union_flag", 0) )
			{
				throw new ConvertNotSameTypeException();
			}
			
			//改货只能改同一个仓库下的
			DBRow allFather[] = tree.getAllFather(detailSourceProductStorage.get("cid",0l));
			long root_storage_catalog_id = allFather[0].get("id", 0l);//获得跟仓库
			
			//检测目标商品是否建库
			DBRow detailTargetProductStorage = fpm.getDetailProductProductStorageByPcid(root_storage_catalog_id, detailTargetP.get("pc_id", 0l));
			if ( detailTargetProductStorage==null )
			{
				throw new ProductNotCreateStorageException();
			}
			
			//源商品库存减少
			TDate td = new TDate();
			long datemark = td.getDateTime();
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));

			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(0);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_CONVER_PRODUCT);
			
			fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean, root_storage_catalog_id, s_pc_id, quantity);
			this.addOutProductLog(detailSourceP.getString("p_code"), root_storage_catalog_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.OUT_STORE_CONVERT_PRODUCT);
			
			//目标商品库存增加
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(0);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_CONVER_PRODUCT);
			
			fpm.incProductStoreCountByPcid(inProductStoreLogBean, root_storage_catalog_id, detailTargetP.get("pc_id", 0l), quantity,0);
			this.addInProductLog(detailTargetP.getString("p_code"), root_storage_catalog_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_CONVERT_PRODUCT);
		}
		catch (ProductNotCreateStorageException e)
		{
			throw e;
		}
		catch (ConvertNotSameTypeException e)
		{
			throw e;
		}
		catch (ConvertNotBeSelfException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ConvertQuantityIncorrectException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"convertProduct",log);
		}
	}
	
	/**
	 * 对现有缺货订单进行重新抄单，重新计算库存
	 * @throws Exception
	 */
	public void reCheckLackingOrders(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			//reCheckLackingOrdersSub(adminLoggerBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"reCheckLackingOrders",log);
		}
	}

	public void reCheckLackingOrdersSub(AdminLoginBean adminLoggerBean) 
		throws Exception
	{
		try
		{
			ArrayList parentOid = new ArrayList();
			DBRow lackingOrders[] = fom.getNormalLackingOrders();//获得正常/归档状态的缺货订单(归档状态为子订单)

			//先把缺货订单库存回退
			for (int i=0; i<lackingOrders.length; i++)
			{
				DBRow orderItems[] = fom.getPOrderItemsByOid(lackingOrders[i].get("oid",0l));
				long oid = lackingOrders[i].get("oid",0l);
				
				for (int j=0; j<orderItems.length; j++)
				{
					//只对原来缺货ITEM计算
					if (orderItems[j].get("lacking", 0)==ProductStatusKey.STORE_OUT)
					{
						ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
						inProductStoreLogBean.setAccount( adminLoggerBean.getAccount() );
						inProductStoreLogBean.setOid(oid);
//						inProductStoreLogBean.setOperation("进库缺货订单 回退 库存");

						//先把库存退掉
						if (orderItems[j].get("product_type", 0)==ProductTypeKey.NORMAL||orderItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD)
						{
							fpm.incProductStoreCountByPcid(inProductStoreLogBean,lackingOrders[i].get("ps_id", 0l),orderItems[j].get("pid", 0l), orderItems[j].get("quantity", 0f),0l);						
						}
						else if ( orderItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )
						{
							DBRow productsInSet[] = fpm.getProductsInSetBySetPid(orderItems[j].get("pid", 0l));
							for (int k=0; k<productsInSet.length; k++)
							{
								fpm.incProductStoreCountByPcid(inProductStoreLogBean,lackingOrders[i].get("ps_id", 0l),productsInSet[k].get("pid", 0l), productsInSet[k].get("quantity", 0f)*orderItems[j].get("quantity", 0f),0l);								
							}
						}
						else if ( orderItems[j].get("product_type", 0)==ProductTypeKey.UNION_CUSTOM )
						{
							DBRow productsCustomInSet[];
							productsCustomInSet = fpm.getProductsCustomInSetBySetPid(orderItems[j].get("pid", 0l));
							
							for (int k=0; k<productsCustomInSet.length; k++)
							{
								fpm.incProductStoreCountByPcid(inProductStoreLogBean,lackingOrders[i].get("ps_id", 0l),productsCustomInSet[k].get("pid", 0l), productsCustomInSet[k].get("quantity", 0f)*orderItems[j].get("quantity", 0f),0l);								
							}
						}
					}
				}
			}
			
			for (int i=0; i<lackingOrders.length; i++)
			{
				DBRow orderItems[] = fom.getPOrderItemsByOid(lackingOrders[i].get("oid",0l));
				long ps_id = lackingOrders[i].get("ps_id",0l);//发货仓库
				long oid = lackingOrders[i].get("oid",0l);
				boolean orderIsLacking = false;
				
				for (int j=0; j<orderItems.length; j++)
				{
					long pid = orderItems[j].get("pid",0l);
					float quantity = orderItems[j].get("quantity",0f);
					long iid = orderItems[j].get("iid",0l);
					
					//只对原来缺货ITEM计算
					if (orderItems[j].get("lacking", 0)==ProductStatusKey.STORE_OUT)
					{
						//重新计算库存
						ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
						deInProductStoreLogBean.setAccount( adminLoggerBean.getAccount() );
						deInProductStoreLogBean.setOid(oid);
//						deInProductStoreLogBean.setOperation("进库缺货订单 提取 库存");

						if (orderItems[j].get("product_type", 0)==ProductTypeKey.NORMAL)
						{
							int lacking = 0;

							DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
								
							if (detailPro==null)
							{
								lacking = 1;
								orderIsLacking = true;
							}
							else
							{
								if (detailPro.get("store_count", 0f)<quantity)
								{
									lacking = 1;
									orderIsLacking = true;
								}
							}
							
							fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,pid, orderItems[j].get("quantity",0f));

							if (lacking==0)
							{
								DBRow modOrderItem = new DBRow();
								modOrderItem.add("lacking","0");
								fom.modPOrderItem(iid, modOrderItem);
							}
						}
						else if (orderItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD)//如果是标准套装缺货，先检查标准套装是否有货，如果没货，再检查散件。
						{
							int lacking = 0;
							boolean standardIsLacking = false;
							boolean manualIsLacking = false;
							
							DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
								
							if (detailPro==null)
							{
								lacking = 1;
								standardIsLacking = true;
							}
							else
							{
								if (detailPro.get("store_count", 0f)<quantity)
								{
									lacking = 1;
									standardIsLacking = true;
								}
							}
							//标准套装有货
							if (lacking==0)
							{
								fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,pid, orderItems[j].get("quantity",0f));//减掉库存
								
								DBRow modOrderItem = new DBRow();
								modOrderItem.add("lacking","0");
								fom.modPOrderItem(iid, modOrderItem);
							}
							else//否则，再检查散件
							{
								lacking = 0;

								DBRow productsInSet[] = fpm.getProductsInSetBySetPid( pid );
								for (int jj=0; jj<productsInSet.length; jj++)
								{
									//检查库存
									DBRow detailPro2 = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
									
									if (detailPro2==null)	//	商品还没进库，当然缺货
									{
										lacking = 1;
										manualIsLacking = true;
									}
									else
									{
										if (detailPro2.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*quantity)
										{
											lacking = 1;
											manualIsLacking = true;
										}
									}
									
										
								}
								
								if (lacking==0)
								{
									//散件有货，扣散件库存
									for (int jj=0; jj<productsInSet.length; jj++)
									{
										fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[jj].get("pid", 0l), productsInSet[jj].get("quantity", 0f)*quantity);										
									}
	
									DBRow modOrderItem = new DBRow();
									modOrderItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);
									modOrderItem.add("lacking","0");
									fom.modPOrderItem(iid, modOrderItem);
								}
								else
								{
									//散件也缺货，扣套装库存
									fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,pid, orderItems[j].get("quantity",0f));//减掉库存
								}
								//标准套装和散件都缺货，该ITES缺货，订单缺货
								if (manualIsLacking&&standardIsLacking)
								{
									orderIsLacking = true;
								}
							}
						}
						else if ( orderItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )
						{
							int lacking = 0;

							DBRow productsInSet[] = fpm.getProductsInSetBySetPid( pid );
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									lacking = 1;
									orderIsLacking = true;
								}
								else
								{
									if (detailPro.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*quantity)
									{
										lacking = 1;
										orderIsLacking = true;
									}
								}
								
								fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[jj].get("pid", 0l), productsInSet[jj].get("quantity", 0f)*quantity);		
							}
							
							if (lacking==0)
							{
								DBRow modOrderItem = new DBRow();
								modOrderItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);
								modOrderItem.add("lacking","0");
								fom.modPOrderItem(iid, modOrderItem);
							}
						}
						else  //定制 
						{
							int lacking = 0;		
							DBRow productsCustomInSet[] = fpm.getProductsCustomInSetBySetPid(pid);
							for (int jj=0; jj<productsCustomInSet.length; jj++)
							{
									//检查库存
									DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[jj].get("pid", 0l));
									if (detailPro==null)	//	商品还没进库，当然缺货
									{
										lacking = 1;
										orderIsLacking = true;
									}
									else
									{
										if (detailPro.get("store_count", 0f)< productsCustomInSet[jj].get("quantity", 0f)*quantity )
										{
											lacking = 1;
											orderIsLacking = true;
										}
									}
									
									fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsCustomInSet[jj].get("pid", 0l), productsCustomInSet[jj].get("quantity", 0f)*quantity);
							}
							
							if (lacking==0)
							{
								DBRow modOrderItem = new DBRow();
								modOrderItem.add("lacking","0");
								fom.modPOrderItem(iid, modOrderItem);
							}
						}
				}
			}//order items循环结束

				if (!orderIsLacking)
				{
					DBRow modOrder = new DBRow();
					modOrder.add("product_status",ProductStatusKey.IN_STORE);
					fom.modPOrder(oid, modOrder);
				}

				//找出缺货订单中的合并订单的父订单号
				if ( lackingOrders[i].get("parent_oid",0l)>0&&!parentOid.contains(lackingOrders[i].get("parent_oid",0l)) )
				{
					parentOid.add( lackingOrders[i].get("parent_oid",0l) );
				}


		}//缺货订单循环结束

			//检查合并订单
			for (int z=0; z<parentOid.size(); z++)
			{
				//如果父订单没有缺货子订单了，则把父订单标记消除
				long oid = StringUtil.getLong(parentOid.get(z).toString());
				DBRow sonOrders[] = fom.getSonOrders(oid, null);
				boolean haveLackingOrder = false;
				for (int i=0; i<sonOrders.length; i++)
				{
					if (sonOrders[i].get("product_status", 0)==ProductStatusKey.STORE_OUT)
					{
						haveLackingOrder = true;
					}
				}
				
				if (!haveLackingOrder)
				{
					DBRow order = new DBRow();
					order.add("parent_oid",-1);
					fom.modPOrder(oid, order);
				}
			}


			//如果订单原来是：正常、缺货、已打印，补货后，状态变成：正常、有货、已打印，则新建订单任务
			for (int i=0; i<lackingOrders.length; i++)
			{
				//跟现在订单状态对比
				DBRow order = fom.getDetailPOrderByOid(lackingOrders[i].get("oid", 0l));
				
				if ( lackingOrders[i].get("handle", 0)==HandleKey.PRINTED&&lackingOrders[i].get("handle_status", 0)==HandStatusleKey.NORMAL )
				{
					if ( order.get("handle", 0)==HandleKey.PRINTED&&order.get("handle_status", 0)==HandStatusleKey.NORMAL&&order.get("product_status", 0)==ProductStatusKey.IN_STORE )
					{
						String t_note = "";
						long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
						long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
						int t_operationsA[] = new int[]{OrderTaskKey.T_OPERATION_RE_PRINT};
						
						taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_AGAIN_INSTORE, t_operationsA, lackingOrders[i].get("oid", 0l), t_note, t_target_admin, t_target_group,lackingOrders[i].get("ps_id", 0l));						
					}
				}
				
				//缺货状态发生了反转，都自动加上订单备注
				if (order.get("product_status", 0)==ProductStatusKey.IN_STORE)
				{
					DBRow delivererInfo = new DBRow();
					
					delivererInfo.add("oid", order.get("oid", 0l));
					delivererInfo.add("note", "重新计算库存，订单有货");
					delivererInfo.add("account", adminLoggerBean.getAccount());
					delivererInfo.add("adid", adminLoggerBean.getAdid());
					delivererInfo.add("post_date", DateUtil.NowStr());
					delivererInfo.add("trace_type", TracingOrderKey.LACKING_TRACE);
					
					fom.addPOrderNote(delivererInfo);
					
					//检测跟踪任务
					OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
					orderMgr.validateTraceService(adminLoggerBean.getAdgid(),order.get("oid", 0l),TracingOrderKey.LACKING_TRACE);//关闭跟中服务
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"reCheckLackingOrdersSub",log);
		}
	}
	
	/**
	 * 增加残损件
	 * @param request
	 * @throws Exception
	 */
	public void addDamageProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long pid = StringUtil.getLong(request, "pid");
			float quantity = StringUtil.getFloat(request, "quantity");
			
			DBRow detailProductStorage = fpm.getDetailProductStorageByPid(pid);
			DBRow detailProduct = fpm.getDetailProductByPcid(detailProductStorage.get("pc_id",0l));
			
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			DBRow allFather[] = tree.getAllFather(detailProductStorage.get("cid",0l));
			long root_storage_catalog_id = allFather[0].get("id", 0l);//获得跟仓库
			
			TDate td = new TDate();
			long datemark = td.getDateTime();
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));

			//出库
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(0);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED);
			fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean, root_storage_catalog_id, detailProductStorage.get("pc_id",0l), quantity);
			//记录出库日志
			this.addOutProductLog(detailProduct.getString("p_code"), root_storage_catalog_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.OUT_STORE_DAMAGE);
			
			//增加残损件数目
			fpm.incProductDamagedCountByPid(pid, quantity);
			
			//记录残损件入库日志
			ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
			inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
			inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
			inProductDamagedCountLogBean.setOid(0);
			inProductDamagedCountLogBean.setPs_id(detailProductStorage.get("cid",0l));
			inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
			inProductDamagedCountLogBean.setQuantity(quantity);
			inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
			inProductDamagedCountLogBean.setPc_id(detailProductStorage.get("pc_id",0l));
			this.addProductStoreLogs(inProductDamagedCountLogBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addDamageProduct",log);
		}
	}

	public void addReturnProduct(HttpServletRequest request)
		throws OrderNotExistException,Exception
	{
		try
		{
			long pid = StringUtil.getLong(request, "pid");
			float quantity = StringUtil.getFloat(request, "quantity");
			long oid = StringUtil.getLong(request, "oid");
			int damaged = StringUtil.getInt(request, "damaged");
			
			//先检查订单是否存在
			if ( fom.getDetailPOrderByOid(oid)==null )
			{
				throw new OrderNotExistException();
			}
			
			DBRow detailProductStorage = fpm.getDetailProductStorageByPid(pid);
			DBRow detailProduct = fpm.getDetailProductByPcid(detailProductStorage.get("pc_id",0l));
			
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			DBRow allFather[] = tree.getAllFather(detailProductStorage.get("cid",0l));
			long root_storage_catalog_id = allFather[0].get("id", 0l);//获得跟仓库
			
			TDate td = new TDate();
			long datemark = td.getDateTime();
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));

			//进库
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(oid);
			if(oid>0)
			{
				inProductStoreLogBean.setBill_type(ProductStoreBillKey.ORDER);
			}
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RETURN);
			fpm.incProductStoreCountByPcid(inProductStoreLogBean, root_storage_catalog_id, detailProductStorage.get("pc_id",0l), quantity,0);
			//记录出库日志
			this.addInProductLog(detailProduct.getString("p_code"), root_storage_catalog_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_RETURN,oid);
			
			//检测已损坏，做一次出库
			if (damaged==1||damaged==2)
			{
				//出库
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(0);
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED);
				fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean, root_storage_catalog_id, detailProductStorage.get("pc_id",0l), quantity);
				//记录出库日志
				this.addOutProductLog(detailProduct.getString("p_code"), root_storage_catalog_id, quantity, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.OUT_STORE_DAMAGE);
				
				if (damaged==1)
				{
					//增加残损件数目
					fpm.incProductDamagedCountByPid(pid, quantity);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(0);
					inProductDamagedCountLogBean.setPs_id(root_storage_catalog_id);
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
					inProductDamagedCountLogBean.setQuantity(quantity);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProductStorage.get("pc_id",0l));
					this.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				else
				{
					fpm.incProductPackageDamagedCountByPid(pid, quantity);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(0);
					inProductDamagedCountLogBean.setPs_id(root_storage_catalog_id);
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
					inProductDamagedCountLogBean.setQuantity(quantity);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProductStorage.get("pc_id",0l));
					this.addProductStoreLogs(inProductDamagedCountLogBean);
				}
			}
			
			//对相关订单做备注
			FloorCatalogMgr fcm = (FloorCatalogMgr)MvcUtil.getBeanFromContainer("floorCatalogMgr");
			
			String checkNote = "["+fcm.getDetailProductStorageCatalogById(root_storage_catalog_id).getString("title") + "] 收到 [完好] "+detailProduct.getString("p_name")+" x "+quantity +" "+detailProduct.getString("unit_name");
			if (damaged==1)
			{
				checkNote = "["+fcm.getDetailProductStorageCatalogById(root_storage_catalog_id).getString("title")+ "] 收到 [损坏] "+detailProduct.getString("p_name")+" x "+quantity + " "+detailProduct.getString("unit_name");
			}
			
			OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
			orderMgr.addPOrderNotePrivate(oid, checkNote, TracingOrderKey.OTHERS, StringUtil.getSession(request),0);
			
		}
		catch (OrderNotExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addReturnProduct",log);
		}
	}
	
	/**
	 * 获得订货商品分类
	 * @param st
	 * @param en
	 * @param keep_days
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookProductsPC(String st,String en,int keep_days,long pscid)
		throws Exception
	{
		try
		{
			return(fpm.getBookProductsPC( st, en, keep_days, pscid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getBookProductsPC",log);
		}
	}
	
	/**
	 * 获得订货商品
	 * @param st
	 * @param en
	 * @param keep_days
	 * @param ps_id
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookProducts(String st,String en,int keep_days,long ps_id,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			return(fpm.getBookProducts( st, en, keep_days, ps_id, pscid, catalog_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getBookProducts",log);
		}
	}
	
	/**
	 * 残损件统计
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatDamagedProducts(long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			return(fpm.getStatDamagedProducts( pscid, catalog_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatDamagedProducts",log);
		}
	}
	
	/**
	 * 残损件统计
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatDamagedProductsPC(long pscid)
		throws Exception
	{
		try
		{
			return(fpm.getStatDamagedProductsPC( pscid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStatDamagedProductsPC",log);
		}
	}
	
	/**
	 * 获得系统级的商品库存变化日志
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreSysLogs(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getProductStoreSysLogs(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStoreSysLogs",log);
		}
	}
	
	/**
	 * 库存日志查询
	 * @param st
	 * @param en
	 * @param oid
	 * @param p_name
	 * @param account
	 * @param do_group_by
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchProductStoreSysLogs(String st,String en,long oid,String p_name,String account,int do_group_by,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getSearchProductStoreSysLogs( st, en, oid, p_name, account, do_group_by, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchProductStoreSysLogs",log);
		}
	}
	
	/**
	 * 库存日志搜索
	 */
	public DBRow[] getSearchProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,String p_name,long bill_id,int bill_type,int operotion,int quantity_type,long ps_id,long adid,int onGroup,int in_or_out,PageCtrl pc)
		throws Exception
	{
		try 
		{
			long pc_id = 0;
			DBRow product = fpm.getDetailProductByPname(p_name);
			if(product != null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return (fpm.getSearchProductStoreSysLogsNew(st, en, product_line_id, catalog_id, pc_id, bill_id, bill_type, operotion, quantity_type, ps_id,adid,onGroup,in_or_out,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchProductStoreSysLogsNew",log);
		}
	}
	
	/**
	 * 检索退货日志
	 * @param st
	 * @param en
	 * @param product_line_id
	 * @param catalog_id
	 * @param p_name
	 * @param bill_id
	 * @param bill_type
	 * @param quantity_type
	 * @param ps_id
	 * @param adid
	 * @param onGroup
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchReturnProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,String p_name,long bill_id,int bill_type,int quantity_type,long ps_id,long adid,int onGroup,PageCtrl pc)
		throws Exception
	{
		try 
		{
			long pc_id = 0;
			DBRow product = fpm.getDetailProductByPname(p_name);
			if(product != null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return (fpm.getSearchReturnProductStoreSysLogsNew(st, en, product_line_id, catalog_id, pc_id, bill_id, bill_type, quantity_type, ps_id, adid, onGroup, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchReturnProductStoreSysLogsNew",log);
		}
	}
	
	/**
	 * 导出退货日志
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String exportReturnStoreSysLogs(HttpServletRequest request)
		throws Exception
	{
		long bill_id = StringUtil.getLong(request,"oid");
		String p_name = StringUtil.getString(request,"p_name");
		long adid = StringUtil.getLong(request,"account");

		String st = StringUtil.getString(request,"input_st_date");
		String en = StringUtil.getString(request,"input_en_date");

		long product_line_id = StringUtil.getLong(request,"filter_productLine");
		long catalog_id = StringUtil.getLong(request,"filter_pcid");
		int bill_type = ProductStoreBillKey.RETURN_ORDER;
		int quantity_type = StringUtil.getInt(request,"quantity_type");
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		long pc_id = 0;
		DBRow product = fpm.getDetailProductByPname(p_name);
		if(product != null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		DBRow[] logs = fpm.getSearchReturnProductStoreSysLogsNew(st, en, product_line_id, catalog_id, pc_id, bill_id, bill_type, quantity_type, ps_id, adid,0,null);
		
		//读取模板
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product/returnLogsTemplate.xlsx"));  
		XSSFSheet sheet= wb.getSheet("Sheet1");
		
		for(int i = 0;i<logs.length;i++)
		{
			
			DBRow productDetail = fpm.getAllDetailProductByPcid(logs[i].get("pc_id",0l));
			
			XSSFRow row = sheet.createRow(i+1);
			
			row.createCell(0).setCellValue(logs[i].getString("title"));
			row.createCell(1).setCellValue(productDetail.getString("p_name"));
			row.createCell(2).setCellValue(productDetail.getString("product_line_name"));
			row.createCell(3).setCellValue(productDetail.getString("catalog_name"));
			
			ProductStoreCountTypeKey  productStoreCountTypeKey = new ProductStoreCountTypeKey();
			row.createCell(4).setCellValue(productStoreCountTypeKey.getProductStoreCountTypeKeyId(logs[i].getString("quantity_type")));
			row.createCell(5).setCellValue(logs[i].get("quantity",0f));
			
			ProductStoreBillKey productStoreBillKey = new ProductStoreBillKey();
			row.createCell(6).setCellValue(productStoreBillKey.getProductStoreBillId(logs[i].getString("bill_type")));
			
			String billString = "";
			if(logs[i].get("oid",0l)!=0)
			{
				billString = logs[i].getString("oid");
			}
			
			row.createCell(7).setCellValue(billString);
			
			row.createCell(8).setCellValue(logs[i].getString("post_date").substring(0,10));
			
			String delivery_date = "";
			
			long oid = 0;
			if(logs[i].get("bill_type",0)==ProductStoreBillKey.ORDER)
			{
				oid = logs[i].get("bill_id",0l);
			}
			else if(logs[i].get("bill_type",0)==ProductStoreBillKey.RETURN_ORDER)
			{
				
				DBRow returnProdutOrder = fom.getDetailReturnProductByRpId(logs[i].get("oid",0l));
				
				if(returnProdutOrder !=null)
				{
					oid = returnProdutOrder.get("oid",0l);
				}
				
			}
			
			if(oid !=0)
			{
				DBRow[] delivery_dates = wayBillMgrZJ.getDeliveryDateByOidandPcid(oid,logs[i].get("pc_id",0l));
				
				if(delivery_dates!=null&&delivery_dates.length>0)
				{
					delivery_date = delivery_dates[0].getString("delivery_date");
					
					if(delivery_date.length()>10)
					{
						delivery_date = delivery_date.substring(0,10);
					}
				}
				
				row.createCell(9).setCellValue(delivery_date);
			}
			
			
			
		}

		//写文件  
		String path = "upl_excel_tmp/exportReturnLogs_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
		OutputStream os= new FileOutputStream(Environment.getHome()+path);
		wb.write(os);  
		os.close();  
		return (path);
	}
	
	
	
	   private String getEncoding(String str) {   
		          String encode = "GB2312";   
		         try {   
		             if (str.equals(new String(str.getBytes(encode), encode))) {   
		                  String s = encode;   
		                 return s;   
		              }   
		          } catch (Exception exception) {   
		          }   
		          encode = "ISO-8859-1";   
		         try {   
		             if (str.equals(new String(str.getBytes(encode), encode))) {   
		                  String s1 = encode;   
		                 return s1;   
		              }   
		          } catch (Exception exception1) {   
		          }   
		          encode = "UTF-8";   
		         try {   
		             if (str.equals(new String(str.getBytes(encode), encode))) {   
		                  String s2 = encode;   
		                 return s2;   
		              }   
		          } catch (Exception exception2) {   
		          }   
		          encode = "GBK";   
		         try {   
		             if (str.equals(new String(str.getBytes(encode), encode))) {   
		                  String s3 = encode;   
		                 return s3;   
		              }   
		          } catch (Exception exception3) {   
		          }   
		         return "";   
		      }   
	
	/**
	 * 导入 商品定位 信息
	 * @param request
	 * @throws Exception
	 */
	public synchronized void addStorageLocation(HttpServletRequest request)
		throws ProductNotExistException,Exception
	{
		try
		{
			String machine_id = "";
			
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			
			String csv_name = StringUtil.getString(request,"csv_name");
			String csvRow[] = getCsv(csv_name);
			
			String path = csv_name;
			int lastSpritPos = path.lastIndexOf("/");
			path = path.substring(lastSpritPos);
			//提取机器ID
			String tmp[] = path.split("_");
			String tmp2[] = tmp[1].split("\\.");
			machine_id = tmp2[0];
			
			//先清空原来机器数据
			fpm.cleanStorageLocationByPsidMachineId(adminLoggerBean.getPs_id(),machine_id);
			for (int i=0; i<csvRow.length; i++)
			{
				////system.out.println(csvRow[i] +"->"+ getEncoding( csvRow[i] ) ); 
				if (csvRow[i].trim().equals("")||csvRow[i].trim().split(",").length!=3)
				{
					continue;
				}
				
				String cols[] = csvRow[i].split(",");
//				商品条码可不惟一 zyj
//				DBRow product = fpm.getDetailProductByPcode(cols[0].trim());
				//商品定位表增加了商品ID字段，此方法为页面上传CSV文件使用
//				this.addStorageLocation(adminLoggerBean.getPs_id(),product.get("pc_id",0l),cols[0].trim(), StrUtil.getFloat(cols[1].trim()), cols[2].trim(), machine_id,null,adminLoggerBean);
			}
	
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addStorageLocation",log);
		}
	}

	public void addStorageLocation(long ps_id,long pc_id,long slc_id,float quantity,String machine_id,String sn,long con_id,long title_id,AdminLoginBean adminLoggerBean)
		throws ProductNotExistException,Exception
	{
		try
		{
			DBRow locationCatalog = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
			
			if (locationCatalog==null)
			{
				throw new LocationCatalogNotExitsException();
			}
			long area_id = locationCatalog.get("area_id",0l);
			DBRow area = floorLocationMgrZJ.getDetailLocationAreaById(area_id);
			
			DBRow product = fpm.getDetailProductByPcid(pc_id);
			if (product==null)
			{
				throw new ProductNotExistException();
			}
			
			//插入数据
			DBRow locationRow = new DBRow();
			locationRow.add("ps_id",ps_id);
			locationRow.add("barcode", product.getString("p_code"));
			locationRow.add("quantity", quantity);
			locationRow.add("position",locationCatalog.getString("slc_position_all"));
			locationRow.add("zone",area.getString("title")+area.getString("area_name"));
			locationRow.add("machine_id", machine_id);
			locationRow.add("takestock_adid",adminLoggerBean.getAdid());
			
			if (sn!=null&&!sn.equals("NULL"))
			{
				locationRow.add("sn",sn);
			}
			
			locationRow.add("pc_id",pc_id);
			locationRow.add("slc_id",locationCatalog.get("slc_id",0l));
			locationRow.add("con_id",con_id);
			locationRow.add("title_id",title_id);
			
			
			fpm.addStorageLocation(locationRow);
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (LocationCatalogNotExitsException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addStorageLocation",log);
		}
	} 
	
	
	/**
	 * 获得所有定位信息
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageLocation(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAllStorageLocation(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllStorageLocation",log);
		}
	}
	
	/**
	 * 获得某个仓库的定位信息
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationByPsId(ps_id,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationByPsId",log);
		}
	}
	
	/**
	 * 用仓库实际库存跟系统库存比较
	 * @param cid
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getIncorrectStorageByCidPsid(long cid,long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			int[] status = new int[1];
			status[0] = WayBillOrderStatusKey.WAITPRINT;//认为待打印的运单为需要补偿的
			//status[1] = WayBillOrderStatusKey.PERINTED;
			return(fpm.getIncorrectStorageByCidPsid(cid, ps_id, pc,status));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getIncorrectStorageByCidPsid",log);
		}
	}
	
	/**
	 * 获得商品仓库位置
	 * @param ps_id
	 * @param barcode
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[]  getStorageLocationByBarcode(long ps_id,long pc_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationByBarcode(ps_id,pc_id,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationByBarcode",log);
		}
	}
	
	/**
	 * 获得商品仓库位置
	 * @param ps_id
	 * @param barcode
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[]  getStorageLocationByBarcodeMachineID(long ps_id,long pc_id,String machine_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationByBarcodeMachineID( ps_id,pc_id, machine_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationByBarcodeMachineID",log);
		}
	}

	/**
	 * 累计商品需要的数量
	 * 累加一个订单某个商品需要的累计数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @return
	 */
	private float getCumProductQuantity(DBRow productCount,long pid)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			return(0);
		}
		else
		{
			return( StringUtil.getFloat(productCount.getString(String.valueOf(pid))) );
		}
	}

	/**
	 * 累计订单某个商品需要的数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addCumProductQuantity(DBRow productCount,long pid,float quantity)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			productCount.add(String.valueOf(pid), quantity);
		}
		else
		{
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}
	
	/**
	 * 缺货商品数量累计
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addStockoutProductQuantity(DBRow productCount,long pid,float quantity)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			productCount.add(String.valueOf(pid), quantity);
		}
		else
		{
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}

	/**
	 * 订单预处理，计算每个仓库是否缺货，每个仓库下商品是否缺货
	 * @param ps_id
	 * @param cart
	 * @return
	 * @throws Exception
	 */
	public PreCalcuOrderBean preCalcuOrderOld(HttpServletRequest request,long ps_id,long oid)
		throws Exception
	{
		try
		{
			boolean orderIsLacking = false;
			ArrayList<Object> result = new ArrayList<Object>();//重新计算封装每个order item，以便前台显示
			
			Cart cart = (Cart)MvcUtil.getBeanFromContainer("cart");//普通商品
			CustomCart customCart = (CustomCart)MvcUtil.getBeanFromContainer("customCart");
			OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
			
			cart.flush(StringUtil.getSession(request));
			DBRow cartProductItems[] = cart.getDetailProducts();//从购物车获得订单主商品信息
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);

			/**
			 * 这里要处理重复抄单，处理逻辑跟抄单一致
			 */
			HashMap need2ReCalculateStoreHM = new HashMap();
			for (int i=0; i<cartProductItems.length; i++)
			{
				need2ReCalculateStoreHM.put(cartProductItems[i].getString("cart_pid")+"|"+cartProductItems[i].getString("cart_product_type")+"|"+cartProductItems[i].getString("cart_quantity"),cartProductItems[i].getString("cart_pid")+"|"+cartProductItems[i].getString("cart_product_type"));
			}
			if ( detailOrder.get("ps_id", 0l)==ps_id&&detailOrder.get("product_status", 0)!=ProductStatusKey.UNKNOWN ) //已经抄过单，并且仓库相同，才需要判断某些商品是否需要重新计算库存
			{
				DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
				//商品唯一标识：pid+product_type+quantity
				//抹掉不需要重新计算库存商品
				for (int i=0; i<oldOrderItems.length; i++)
				{
					//缺货肯定要重新计算
					if ( oldOrderItems[i].get("lacking", 0)==ProductStatusKey.STORE_OUT)
					{
						continue;
					}

					//如果不缺货，普通商品不需要重新计算库存，如果是定制商品，需要做进一步判断购物车的定制内容是否与原来订单的一致
					if ( oldOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM )
					{
						DBRow oldCustomProducts[] = fpm.getProductsCustomInSetBySetPid(oldOrderItems[i].get("pid",0l));
						DBRow newCustomProducts[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), oldOrderItems[i].get("pid",0l));
						
						if (!orderMgr.customProductNotChanged(oldCustomProducts, newCustomProducts))
						{
							continue;
						}
					}

					need2ReCalculateStoreHM.remove( oldOrderItems[i].getString("pid")+"|"+oldOrderItems[i].getString("product_type")+"|"+oldOrderItems[i].getString("quantity") );
				}
			}

			DBRow productCount = new DBRow();//用来累加商品需要总数量，有一种情况：一个单品既存在于购物车，也存在于定制商品或拼装中
			for (int j=0; j<cartProductItems.length; j++)
			{
				if (need2ReCalculateStoreHM.containsValue(cartProductItems[j].getString("cart_pid")+"|"+cartProductItems[j].getString("cart_product_type")))
				{
					long pid = StringUtil.getLong(cartProductItems[j].getString("cart_pid"));
					float quantity = StringUtil.getFloat(cartProductItems[j].getString("cart_quantity"));

					if (cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
					{
						int lacking = ProductStatusKey.IN_STORE;

						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);

						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							orderIsLacking = true;
						}
						else
						{
							//模拟回退库存，如果原来已经抄单，则需要提取原来ITEM的数量
							float old_quantity = 0;
							DBRow oldItem = fom.getDetailOrderItemByOidPid(oid,pid);
							if (oldItem!=null&&detailOrder.get("ps_id", 0l)==ps_id&&oldItem.get("lacking",0)!=ProductStatusKey.UNKNOWN)//同仓库并且扣过库存
							{
								old_quantity = oldItem.get("quantity", 0f);
							}
							////system.out.println("["+ps_id+"] "+detailPro.getString("p_name")+" "+detailPro.get("store_count", 0f)+" - "+old_quantity);
							if (detailPro.get("store_count", 0f)+old_quantity<quantity+this.getCumProductQuantity(productCount, pid))
							{
								lacking = ProductStatusKey.STORE_OUT;
								orderIsLacking = true;
							}
						}
						this.addCumProductQuantity(productCount, pid, quantity);
						
						//对原来的购物车中的item增加一个缺货标记字段
						cartProductItems[j].add("lacking", lacking);
					}
					else if ( cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD||cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//如果是标准套装缺货，先检查标准套装是否有货，如果没货，再检查散件。标准套装退标准套装
					{
						int lacking = ProductStatusKey.IN_STORE;
						boolean standardUnionLacking = false;
						boolean manualUnionLacking = false;
						
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
							
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							standardUnionLacking = true;
						}
						else
						{
							/** 模拟退库存操作  **/
							float old_quantity = 0;
							DBRow oldItem[] = fom.getDetailOrderItemsByOidPid(oid,pid);
							if (detailOrder.get("product_status",0)!=ProductStatusKey.UNKNOWN&&oldItem.length>0&&detailOrder.get("ps_id", 0l)==ps_id&&cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD)//增加了验证订单是否扣过库存再模拟退库存
							{
								old_quantity = oldItem[0].get("quantity", 0f);
							}
							/** 模拟退库存操作  **/
							////system.out.println("["+ps_id+"] "+detailPro.getString("p_name")+" "+detailPro.get("store_count", 0f)+" - "+old_quantity);
							if (detailPro.get("store_count", 0f)+old_quantity<quantity)
							{
								lacking = 1;
								standardUnionLacking = true;
							}
						}
						//标准套装有货
						if (lacking==ProductStatusKey.IN_STORE)
						{
							cartProductItems[j].add("lacking", lacking);
							cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
							////system.out.println(ps_id+":1");
						}
						else//否则，再检查散件
						{
							lacking = ProductStatusKey.IN_STORE;
							
							DBRow productsInSet[] = fpm.getProductsInSetBySetPid( pid );
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									manualUnionLacking = true;
								}
								else
								{
									float old_quantity = 0;
									DBRow oldItem[] = fom.getDetailOrderItemsByOidPid(oid,pid);
									if (detailOrder.get("product_status",0)!=ProductStatusKey.UNKNOWN&&oldItem.length>0&&detailOrder.get("ps_id", 0l)==ps_id&&cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL)//拼装库存退拼装
									{
										old_quantity = oldItem[0].get("quantity", 0f)*productsInSet[jj].get("quantity", 0f);
										////system.out.println(ps_id+" "+oldItem.get("quantity", 0f)+" - "+productUnion.get("quantity", 0f)); 
									}
									if (detailPro2.get("store_count", 0f)+old_quantity< productsInSet[jj].get("quantity", 0f)*quantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = ProductStatusKey.STORE_OUT;
										manualUnionLacking = true;
									}
									
									////system.out.println(ps_id+" "+productsInSet[jj].getString("p_name")+" - "+ detailPro2.get("store_count", 0f)+old_quantity + " - " + productsInSet[jj].get("quantity", 0f)*quantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)) );
								}
								
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*quantity);
							}

							if (lacking==ProductStatusKey.IN_STORE)
							{
								//把item修改为拼装
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD_MANUAL);
								cartProductItems[j].add("lacking", lacking);
								////system.out.println(ps_id+":2");
							}
							else if(lacking==ProductStatusKey.STORE_OUT)
							{
								cartProductItems[j].add("lacking", lacking);
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
								////system.out.println(ps_id+":3");
							}
							
							if (standardUnionLacking&&manualUnionLacking)
							{
								orderIsLacking = true;
							}
						}
					}
					else  //定制 
					{
						int lacking = 0;		
						DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), pid );
						for (int jj=0; jj<productsCustomInSet.length; jj++)
						{
								//检查库存
								DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[jj].get("pc_id", 0l));
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									orderIsLacking = true;
								}
								else
								{
									float old_quantity = 0;
									DBRow oldItem = fom.getDetailOrderItemByOidPid(oid,pid);
									DBRow oldCustomUnion = fpm.getDetailProductCustomUnionBySetPidPid(pid,productsCustomInSet[jj].get("pc_id", 0l));
									if (oldCustomUnion!=null&&oldItem!=null&&detailOrder.get("ps_id", 0l)==ps_id&&detailOrder.get("product_status",0)!=ProductStatusKey.UNKNOWN)//增加了验证订单是否扣过库存再模拟退库存
									{
										old_quantity = oldCustomUnion.get("quantity", 0f)*oldItem.get("quantity", 0f);
									}
									////system.out.println("["+ps_id+"] "+detailPro.getString("p_name")+" "+detailPro.get("store_count", 0f)+" - "+old_quantity);
									if (detailPro.get("store_count", 0f)+old_quantity< StringUtil.getFloat(productsCustomInSet[jj].getString("cart_quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pc_id", 0l)) )
									{
										lacking = ProductStatusKey.STORE_OUT;
										orderIsLacking = true;
									}
								}
								this.addCumProductQuantity(productCount, productsCustomInSet[jj].get("pc_id", 0l), StringUtil.getFloat(productsCustomInSet[jj].getString("cart_quantity"))*quantity);
						}
						
						cartProductItems[j].add("lacking", lacking);
					}
				}
				else
				{
					cartProductItems[j].add("lacking", 0);
				}
				result.add(cartProductItems[j]);
			}
			
			PreCalcuOrderBean preCalcuOrderBean = new PreCalcuOrderBean();
			preCalcuOrderBean.setOrderIsLacking(orderIsLacking);
			preCalcuOrderBean.setResult((DBRow[])result.toArray(new DBRow[0]));
			
			return(preCalcuOrderBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"preCalcuOrder",log);
		}
	}
	
	
	public PreCalcuOrderBean preCalcuOrder(HttpServletRequest request,long ps_id)
		throws Exception
	{
		try
		{
			boolean orderIsLacking = false;
			ArrayList<Object> result = new ArrayList<Object>();//重新计算封装每个order item，以便前台显示
			
			CartWaybill cartWaybill = (CartWaybill)MvcUtil.getBeanFromContainer("cartWaybill");//普通商品
			
			cartWaybill.flush(StringUtil.getSession(request));
			DBRow cartProductItems[] = cartWaybill.getDetailProduct();//从购物车获得订单主商品信息
	
			/**
			 * 这里要处理重复抄单，处理逻辑跟抄单一致
			 */
	
			DBRow productCount = new DBRow();//用来累加商品需要总数量，有一种情况：一个单品既存在于购物车，也存在于定制商品或拼装中
			for (int j=0; j<cartProductItems.length; j++)
			{
					long pid = StringUtil.getLong(cartProductItems[j].getString("cart_pid"));
					float quantity = StringUtil.getFloat(cartProductItems[j].getString("cart_quantity"));
	
					if (cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
					{
						int lacking = ProductStatusKey.IN_STORE;
	
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
	
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							orderIsLacking = true;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity+this.getCumProductQuantity(productCount, pid))
							{
								lacking = ProductStatusKey.STORE_OUT;
								orderIsLacking = true;
							}
						}
						this.addCumProductQuantity(productCount, pid, quantity);
						
						//对原来的购物车中的item增加一个缺货标记字段
						cartProductItems[j].add("lacking", lacking);
					}
					else if ( cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD||cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//如果是标准套装缺货，先检查标准套装是否有货，如果没货，再检查散件。标准套装退标准套装
					{
						int lacking = ProductStatusKey.IN_STORE;
						boolean standardUnionLacking = false;
						boolean manualUnionLacking = false;
						
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
							
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							standardUnionLacking = true;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity)
							{
								lacking = ProductStatusKey.STORE_OUT;
								standardUnionLacking = true;
							}
						}
						//标准套装有货
						if (lacking==ProductStatusKey.IN_STORE)
						{
							cartProductItems[j].add("lacking", lacking);
							cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
							////system.out.println(ps_id+":1");
						}
						else//否则，再检查散件
						{
							float needManualQuantity ;
							//计算需要散件的套数
							if (detailPro.get("store_count", 0f)>0)
							{
								//除了标准套装提供的数量外，还需要瓶装多少套散件
								needManualQuantity = quantity - detailPro.get("store_count", 0f);
							}
							else
							{
								needManualQuantity = quantity;
							}

							lacking = ProductStatusKey.IN_STORE;
							
							DBRow productsInSet[] = fpm.getProductsInSetBySetPid( pid );
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									manualUnionLacking = true;
								}
								else
								{
									
									if (detailPro2.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*needManualQuantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = ProductStatusKey.STORE_OUT;
										manualUnionLacking = true;
									}
									
									////system.out.println(ps_id+" "+productsInSet[jj].getString("p_name")+" - "+ detailPro2.get("store_count", 0f)+old_quantity + " - " + productsInSet[jj].get("quantity", 0f)*quantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)) );
								}
								
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*needManualQuantity);
							}
	
							if (lacking==ProductStatusKey.IN_STORE)
							{
								//把item修改为拼装
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD_MANUAL);
								cartProductItems[j].add("lacking", lacking);
								////system.out.println(ps_id+":2");								
							}
							else if(lacking==ProductStatusKey.STORE_OUT)
							{
								cartProductItems[j].add("lacking", lacking);
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
								////system.out.println(ps_id+":3");
							}
							
							if (standardUnionLacking&&manualUnionLacking)
							{
								orderIsLacking = true;
							}
						}
					}
					else  //定制 
					{
						int lacking = 0;		
						DBRow productsCustomInSet[] = fpm.getProductCustomUnionsBySetId(pid);
						for (int jj=0; jj<productsCustomInSet.length; jj++)
						{
								//检查库存
								DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[jj].get("pid", 0l));
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									orderIsLacking = true;
								}
								else
								{
									if (detailPro.get("store_count", 0f)< StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pid", 0l)) )
									{
										lacking = ProductStatusKey.STORE_OUT;
										orderIsLacking = true;
									}
								}
								this.addCumProductQuantity(productCount, productsCustomInSet[jj].get("pc_id", 0l), StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity);
						}
						
						cartProductItems[j].add("lacking", lacking);
					}
				result.add(cartProductItems[j]);
			}
			PreCalcuOrderBean preCalcuOrderBean = new PreCalcuOrderBean();
			preCalcuOrderBean.setOrderIsLacking(orderIsLacking);
			preCalcuOrderBean.setResult((DBRow[])result.toArray(new DBRow[0]));
			
			return(preCalcuOrderBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"preCalcuOrder",log);
		}
	}
	
	/**
	 * 提交库存差异
	 * @param request
	 * @throws Exception
	 */
	public synchronized void commmitStorageLocalDifference(HttpServletRequest request)
		throws StorageApproveIsExistException,Exception
	{
		try
		{
			long cid = StringUtil.getLong(request,"cid");
			long psid = StringUtil.getLong(request,"psid");
			long sa_id = 0;
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminInfo = am.getAdminLoginBean( StringUtil.getSession(request) );
			
			/**
			 * 先检查是否存在当天未审核库存差异
			 */
			//还需要判断是否存在未审核数据!!
			DBRow detailStorageApprove[] = fpm.getStorageApproveByPsId(psid);
			if ( detailStorageApprove.length==0||detailStorageApprove[0].get("approve_status", 0)==1 )//当天还没有未审核数据或者存在已审核数据，都需要插入新纪录
			{
				//先插入审核信息
				DBRow approveStorage = new DBRow();
				approveStorage.add("post_date", DateUtil.NowStr());
				approveStorage.add("commit_account", adminInfo.getAccount());
				approveStorage.add("approve_account", "");
				approveStorage.add("approve_date", "1999-1-1 0:0:0");
				approveStorage.add("difference_count", 0);
				approveStorage.add("difference_approve_count", 0);
				approveStorage.add("approve_status", 0);//未审核
				approveStorage.add("ps_id", psid);
				sa_id = fpm.addStorageApprove(approveStorage);
			}
			else
			{
				sa_id = detailStorageApprove[0].get("sa_id", 0l);
			}
			//先删除旧数据
			fpm.delStorageLocationDifferenceBySaId(sa_id);
			//获得库存差异数据
			int[] status = new int[1];
			status[0] = WayBillOrderStatusKey.WAITPRINT;//认为待打印的运单为需要补偿的
			
			DBRow storageLocations[] = fpm.getIncorrectStorageByCidPsid( cid, psid, null,status);
			//插入到库存差异表
			for (int i=0; i<storageLocations.length; i++)
			{
				DBRow storageLocationDifference = new DBRow();
				storageLocationDifference.add("storage_name", storageLocations[i].getString("title"));
				storageLocationDifference.add("p_code", storageLocations[i].getString("barcode"));
				storageLocationDifference.add("actual_store", storageLocations[i].get("quantity",0f));
				storageLocationDifference.add("sys_store", storageLocations[i].get("merge_count",0f));
				storageLocationDifference.add("sa_id", sa_id);
				storageLocationDifference.add("note", "");
				storageLocationDifference.add("approve_status", 0);
				storageLocationDifference.add("pc_id",storageLocations[i].get("pc_id",0l));
				
				fpm.addStorageLocationDifference(storageLocationDifference);
			}
			
			//重新UPDATE下数据（因为有可能重复提交，库存差异商品数和提交人都不同）
			DBRow updateApproveStorage = new DBRow();
			updateApproveStorage.add("post_date", DateUtil.NowStr());
			updateApproveStorage.add("commit_account", adminInfo.getAccount());
			updateApproveStorage.add("difference_count", storageLocations.length);
			fpm.modStorageApprove(sa_id, updateApproveStorage);
		}
		catch (StorageApproveIsExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"commmitStorageLocalDifference",log);
		}
	}

	/**
	 * 获得所有需要审核的库存差异
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageApproves(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAllStorageApproves(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllStorageApproves",log);
		}
	}
	
	/**
	 * 待审核按提交日期排序
	 * @param sort true-降序
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageApprovesSortByPostDate(long ps_id,int status,boolean sort,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAllStorageApprovesSortByPostDate( ps_id, status, sort, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllStorageApprovesSortByPostDate",log);
		}
	}
	
	/**
	 * 待审核按审核日期排序
	 * @param sort
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageApprovesSortByApproveDate(long ps_id,int status,boolean sort,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAllStorageApprovesSortByApproveDate( ps_id, status, sort, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllStorageApprovesSortByApproveDate",log);
		}
	}
	
	/**
	 * 根据仓库和状态过滤待审核
	 * @param ps_id
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageApprovesByPsidStatus(long ps_id,int status,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageApprovesByPsidStatus( ps_id, status, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageApprovesByPsidStatus",log);
		}
	}

	/**
	 * 获得差异商品列表信息
	 * @param sa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationDifferencesBySaId(long sa_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationDifferencesBySaId( sa_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationDifferencesBySaId",log);
		}
	}
	
	/**
	 * 校正差异库存
	 * @param request
	 * @throws Exception
	 */
	public synchronized void correctionStore(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String sld_ids[] = request.getParameterValues("sld_ids");
			
			if (sld_ids!=null&&sld_ids.length>0)
			{
				long sa_id = StringUtil.getLong(request,"sa_id");
				long ps_id = StringUtil.getLong(request,"ps_id");
				
				long pc_id;
				
				DBRow product;
				
				double actual_store,sys_store,new_store_count;
				
				AdminMgr am = new AdminMgr();
				AdminLoginBean adminInfo = am.getAdminLoginBean( StringUtil.getSession(request) );

				//更新差异原因
				//标记为已经审核
				for (int i=0; i<sld_ids.length; i++)
				{
					DBRow storeNote = new DBRow();
					storeNote.add("note", StringUtil.getString(request, "note_"+sld_ids[i]));
					storeNote.add("approve_status", 1);
					fpm.modStorageLocationDifference(StringUtil.getLong(sld_ids[i]), storeNote);
				}

				//校正库存数
				for (int i=0; i<sld_ids.length; i++)
				{
					actual_store = StringUtil.getDouble(request, "actual_store_"+sld_ids[i]);
					sys_store = StringUtil.getDouble(request, "sys_store_"+sld_ids[i]);
					pc_id = StringUtil.getLong(request,"pc_id_"+sld_ids[i]);
					product = fpm.getDetailProductByPcid(pc_id);
					
					new_store_count = actual_store-sys_store;
					
					//把系统库存与实际库存同步
					ProductStoreLogBean inORdeProductStoreLogBean = new ProductStoreLogBean();
					inORdeProductStoreLogBean.setAccount(adminInfo.getAccount());
					inORdeProductStoreLogBean.setAdid(adminInfo.getAdid());
					inORdeProductStoreLogBean.setOid(sa_id);
					inORdeProductStoreLogBean.setBill_type(ProductStoreBillKey.STORAGE_APPROVE);
					inORdeProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_OUT_STORE_REVISE);
					if(product!=null)
					{
//						inORdeProductStoreLogBean.setPurchase_unit_price(product.get("unit_price",0d));
					}
					//根据修改值决定是扣库存还是加库存
					if(new_store_count>0)
					{
						fpm.incProductStoreCountByPcid(inORdeProductStoreLogBean, ps_id,pc_id, new_store_count,0);
					}
					else
					{
						fpm.deIncProductStoreCountByPcid(inORdeProductStoreLogBean, ps_id,pc_id, Math.abs(new_store_count));
					}
					
				}

				int notApproveCount = fpm.getNotApproveCountBySaid(sa_id);//检测是否全部已经审核完成
				int approveCount = fpm.getApproveCountBySaid(sa_id);
				
				DBRow approve = new DBRow();
				if (notApproveCount==0)
				{
					approve.add("approve_status",1);//已审核
					approve.add("approve_account",adminInfo.getAccount());//审核人
					approve.add("approve_date",DateUtil.NowStr());//时间
					approve.add("difference_approve_count",approveCount);//已审核数
				}
				else
				{
					approve.add("difference_approve_count",approveCount);//已审核数
				}

				fpm.modStorageApprove(sa_id, approve);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"correctionStore",log);
		}
	}
	
	/**
	 * 允许/不允许抄单
	 * @param request
	 * @throws Exception
	 */
	public void swichProductAlive(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long pc_id = StringUtil.getLong(request, "pc_id");
			int curAlive = StringUtil.getInt(request, "curAlive");
			
			DBRow alive = new DBRow();
			if (curAlive==1)
			{
				alive.add("alive",0);
			}
			else
			{
				alive.add("alive",1);
			}
			fpm.modProduct(pc_id, alive);
			
			Map<String,Object> productNode = new HashMap<String, Object>();
			productNode.put("pc_id",pc_id);
			productNode.put("alive",alive.get("alive"));
			
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("pc_id", pc_id);
			
			productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//修改商品节点alive属性
			
			
			//根据商品ID获得商品信息,记录商品修改日志
			DBRow product_log = fpm.getDetailProductByPcid(pc_id);
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.UPDATE);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			DBRow product = fpm.getDetailProductByPcid(pc_id);
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			
			ProductIndexMgr.getInstance().updateIndexAsyn(pc_id, product.getString("p_name"),pcodeString, product.get("catalog_id",0l), product.getString("unit_name"),product.get("alive",0));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"swichProductAlive",log);
		}
	}
	
	/**
	 * 获得仓库下所有条码扫描机器
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationScannerByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationScannerByPsId(ps_id,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationScannerByPsId",log);
		}
	}

	/**
	 * 通过仓库和扫描机ID获得商品定位信息
	 * @param ps_id
	 * @param machine_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationByPsIdMachineID(long ps_id,String machine_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getStorageLocationByPsIdMachineID(ps_id, machine_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageLocationByPsIdMachineID",log);
		}
	}
			
	/**
	 * 批量删除商品定位信息
	 * @param request
	 * @throws ProductInUnionException
	 * @throws ProductHasRelationStorageException
	 * @throws Exception
	 */
	public void batchDelStorageLocation(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String switchCheckboxSlid[] = request.getParameterValues("switchCheckboxSlid");
			fpm.delStorageLocation(switchCheckboxSlid);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchDelStorageLocation",log);
		}
	}
		
	/**
	 * 批量修改仓库-发货国家关系
	 * @param request
	 * @throws Exception
	 */
	public void batchModStorageCountry(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sid = StringUtil.getLong(request, "sid");
			String cid[] = request.getParameterValues("ccid");
			
			//先删除原来的映射
			fpm.delStorageCountryBySid(sid);
			
			for (int i=0; cid!=null&&i<cid.length; i++)
			{
				//再重新插入
				if ( Long.parseLong(cid[i])>0&&sid>0 )
				{
					DBRow row = new DBRow();
					row.add("cid",cid[i]);
					row.add("sid",sid);
					fpm.addStorageCountry(row);
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModStorageCountry",log);
		}
	}
		
	/**
	 * 批量修改仓库-发货地区关系
	 * @param request
	 * @throws Exception
	 */
	public void batchModStorageProvince(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sid = StringUtil.getLong(request, "sid");
			String pro_id[] = request.getParameterValues("pro_id");
			
			//先删除原来的映射
			fpm.delStorageProvinceBySid(sid);
			
			for (int i=0; pro_id!=null&&i<pro_id.length; i++)
			{
				//再重新插入
				if ( Long.parseLong(pro_id[i])>0&&sid>0 )
				{
					DBRow row = new DBRow();
					row.add("pro_id",pro_id[i]);
					row.add("sid",sid);
					fpm.addStorageProvince(row);
				}

			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModStorageProvince",log);
		}
	}
	
	/**
	 * 通过仓库ID删除国家省份的映射关系
	 * @param request
	 * @throws Exception
	 */
	public void delStorageProvinceBySid(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long psid = StringUtil.getLong(request, "psid");
			fpm.delStorageProvinceBySid(psid);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductStorage",log);
		}
	}
	
	/**
	 * 通过仓库ID删除国家仓库的映射关系
	 * @param request
	 * @throws Exception
	 */
	public void delStorageCountryBySid(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long psid = StringUtil.getLong(request, "psid");
			fpm.delStorageCountryBySid(psid);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delStorageCountryBySid",log);
		}
	}
	
	/**
	 * 获得没有分配仓库关联的地区
	 * @param psid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFreeStorageProvinces(long psid)
		throws Exception
	{
		try
		{
			return(fpm.getFreeStorageProvinces(psid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFreeStorageProvinces",log);
		}
	}
	
	/**
	 * 获得仓库所关联的地区
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageProvinceBySid(long sid)
		throws Exception
	{
		try
		{
			return(fpm.getStorageProvinceBySid(sid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageProvinceBySid",log);
		}
	}
	
	/**
	 * 获得国家下所有的地区
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageProvinceByCcid(long ccid)
		throws Exception
	{
		try
		{
			////system.out.println(ccid);
			return(fpm.getStorageProvinceByCcid(ccid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getStorageProvinceByCcid",log);
		}
	}
	
	/**
	 * 商品翻新进库
	 * @param request
	 * @throws Exception
	 */
	public void renewProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long psid = StringUtil.getLong(request, "psid");
			long pcid = StringUtil.getLong(request, "pcid");
			long pid = StringUtil.getLong(request, "pid");
			float damaged_count_m = StringUtil.getFloat(request, "damaged_count_m");
			float damaged_package_count_m = StringUtil.getFloat(request, "damaged_package_count_m");
			
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			if (damaged_count_m>0)
			{
				fpm.deIncProductDamagedCountByPid(pid, damaged_count_m);
				
				//记录残损件出库日志
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(0);
				deProductDamagedCountLogBean.setPs_id(psid);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_RENEW);
				deProductDamagedCountLogBean.setQuantity(-damaged_count_m);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(pcid);
				this.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			if (damaged_package_count_m>0)
			{
				fpm.deIncProductPackageDamagedCountByPid(pid, damaged_package_count_m);
				
				//记录残损件出库日志
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(0);
				deProductDamagedCountLogBean.setPs_id(psid);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_RENEW);
				deProductDamagedCountLogBean.setQuantity(-damaged_package_count_m);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(pcid);
				this.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			
			if (damaged_count_m+damaged_package_count_m>0)
			{
				ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
				inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				inProductStoreLogBean.setOid(0);
				inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RENEW);
				fpm.incProductStoreCountByPcid(inProductStoreLogBean, psid, pcid, damaged_count_m+damaged_package_count_m,0);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"renewProduct",log);
		}
	}
	
	/**
	 * 获得最大可预拼装的套装数量
	 * 
	 * 计算组合里面的每个散件最多可以拼装多少套套装
	 * 然后获得最小可拼装数最为最大可预拼数量
	 * 
	 * @param set_id  套装ID
	 * @return
	 * @throws Exception
	 */
	public int getPrepareUnionCount(long psid,long set_id)
		throws Exception
	{
		try
		{
			DBRow inSetProducts[] = fpm.getProductsInSetBySetPid(set_id);
			float union_quantity = 0;
			float combination_set_quantity = 0;
			float store_count = 0;
			
			ArrayList combination_set_quantity_list = new ArrayList();
			
			for (int i=0; i<inSetProducts.length; i++)
			{
				store_count = fpm.getDetailProductProductStorageByPcid(psid, inSetProducts[i].get("pc_id",0l)).get("store_count",0f);//当前商品、当前仓库的库存
				union_quantity = inSetProducts[i].get("quantity",0f);
				
				combination_set_quantity = (int)(store_count/union_quantity); 
				//可以拼的套装数>=0
				if (combination_set_quantity<0)
				{
					combination_set_quantity = 0;
				}
				combination_set_quantity_list.add(combination_set_quantity);
			}
			//如果存在可拼装数，则排序选择最少可拼装套装数
			if (combination_set_quantity_list.size()>0)
			{
				int combination_set_quantity_a[] = new int[combination_set_quantity_list.size()];
				for (int i=0; i<combination_set_quantity_list.size(); i++)
				{
					combination_set_quantity_a[i] = (int)MoneyUtil.round(combination_set_quantity_list.get(i).toString(), 0);
				}
				Arrays.sort(combination_set_quantity_a);//升序调整顺序
				return(combination_set_quantity_a[0]);
			}
			else
			{
				return(0);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getPrepareUionCount",log);
		}
	}
	
	/**
	 * 预组装套装
	 * @param request
	 * @throws Exception
	 */
	public void combinationProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long psid = StringUtil.getLong(request, "psid");
			long pcid = StringUtil.getLong(request, "pcid");
			int combination_quantity = StringUtil.getInt(request, "combination_quantity");
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			//减少散件进库
			DBRow inSetProducts[] = fpm.getProductsInSetBySetPid(pcid);
			long[] fromPslIds = new long[inSetProducts.length];
			for (int i=0; i<inSetProducts.length; i++)
			{
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(0);
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_COMBINATION_NORMAL);
				long from_psl_id = fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean, psid, inSetProducts[i].get("pc_id", 0l), combination_quantity*inSetProducts[i].get("quantity", 0f));
				
				fromPslIds[i] = from_psl_id;
			}
			
			//增加主套装数量
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(0);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_COMBINATION_UNION_STANDARD);
			long to_psl_id = fpm.incProductStoreCountByPcid(inProductStoreLogBean, psid, pcid, combination_quantity,0);
			
			//记录批次关系(屏蔽)
			productStoreLogsDetailMgrZJ.combinationToProdcutStoreUnionLog(fromPslIds,to_psl_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"combinnationProduct",log);
		}
	}
	
	/**
	 * 系统自动拼装
	 * @param psid
	 * @param pcid
	 * @param combination_quantity
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void combinationProductSystem(long psid,long pcid,int combination_quantity,int bill_type,long bill_id,AdminLoginBean adminLoggerBean,int operation_type)
		throws Exception
	{
		try
		{	
			
			//减少散件进库
			DBRow inSetProducts[] = fpm.getProductsInSetBySetPid(pcid);
			long[] fromPslIds = new long[inSetProducts.length];
			for (int i=0; i<inSetProducts.length; i++)
			{
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(bill_id);
				deInProductStoreLogBean.setBill_type(bill_type);
				
				
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SYSTEM_COMBINATION);
//				deInProductStoreLogBean.setOperation(operation_type);
				long from_psl_id = fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean, psid, inSetProducts[i].get("pc_id", 0l), combination_quantity*inSetProducts[i].get("quantity", 0f));
				
//				productStoreLocationMgrZJ.reserveTheoretical(psid, inSetProducts[i].get("pc_id", 0l), combination_quantity*inSetProducts[i].get("quantity", 0f), bill_id,bill_type,adminLoggerBean.getAdid());
				
				fromPslIds[i] = from_psl_id;
			}
			
			//增加主套装数量
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(bill_id);
			inProductStoreLogBean.setBill_type(bill_type);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SYSTEM_COMBINATION);
			
			long to_psl_id = fpm.incProductStoreCountByPcid(inProductStoreLogBean, psid, pcid, combination_quantity,0);
			//记录批次关系（屏蔽）
			productStoreLogsDetailMgrZJ.combinationToProdcutStoreUnionLog(fromPslIds,to_psl_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"combinnationProduct",log);
		}
	}
	
	
	/**
	 * 获取最佳发货仓库
	 * @param ccid			订单递送国家
	 * @return
	 * @throws Exception
	 */
	public long getPriorDeliveryWarehouse(long ccid,long pro_id)
		throws Exception
	{
		try
		{
			long ps_id = 0l;
			
			////system.out.println(ccid+" - "+pro_id);
			
			//使用该功能，必须保证国家ID和省份ID已经设置好，这个需要在页面检查保证
			
			/**
			 * 先判断该订单收货国家是否有仓库，有则并且支持到该省份，则国内发货
			 * 没有则国际发货
			 */
			if ( fpm.getDevProductStorageByNative(ccid).length>0 )
			{
				////system.out.println("1");
				DBRow detailStorage = fpm.getDetailDevProductStorageByProvince(ccid, pro_id);//支持发货地区的仓库 
				
				if ( detailStorage!=null)
				{
					////system.out.println("2");
					return(detailStorage.get("id", 0l));
				}
			}
			//国际发货
			
			DBRow detailStorageCountry = fpm.getDetailStorageCountryByCid( ccid );
			if (detailStorageCountry==null)
			{
					////system.out.println("3");
				ps_id = 0;
			}
			else
			{
				ps_id = detailStorageCountry.get("sid",0l);
					////system.out.println("4");
			}
			////system.out.println("------------------------");
			
			return(ps_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getPriorDeliveryWarehouse",log);
		}
	}	
	
	/**
	 * 获得本国所有可以发货仓库
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDevProductStorageByNative(long ccid)
		throws Exception
	{
		try
		{
			return(fpm.getDevProductStorageByNative(ccid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorageByNative",log);
		}
	}
	
	/**
	 * 通过发货国家和地区，获得可以发货仓库
	 * @param pro_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDevProductStorageByProvince(long ccid,long pro_id)
		throws Exception
	{
		try
		{
			return(fpm.getDetailDevProductStorageByProvince( ccid, pro_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailDevProductStorageByProvince",log);
		}
	}
	
	/**
	 * 通过省份代码，获得省份详细信息
	 * @param p_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceByPCode(String p_code)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProvinceByPCode(p_code));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProvinceByPCode",log);
		}
	}
	
	/**
	 * 
	 * @param nation_id
	 * @param p_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceByNationIDPCode(long nation_id,String p_code)
		throws Exception
	{
		try
		{
			if (nation_id>0&&p_code.equals("")==false)
			{
				return(fpm.getDetailProvinceByNationIDPCode( nation_id, p_code));
			}
			else
			{
				return(null);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProvinceByNationIDPCode",log);
		}
	}
	
	/**
	 * 获得省份详细信息
	 * @param pro_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceByProId(long pro_id)
		throws Exception
	{
		try
		{
			return(fpm.getDetailProvinceByProId(pro_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProvinceByProId",log);
		}
	}
		
	/**
	 * 拆散套装
	 * @param request
	 * @throws Exception
	 */
	public void splitProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long psid = StringUtil.getLong(request, "psid");
			long pcid = StringUtil.getLong(request, "pcid");
			int split_quantity = StringUtil.getInt(request, "split_quantity");
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			//减少主套装数量
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(0);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SPLIT_UNION_STANDARD);
			long from_psl_id = fpm.deIncProductStoreCountByPcid(inProductStoreLogBean, psid, pcid, split_quantity);
			
			//增加散件进库
			DBRow inSetProducts[] = fpm.getProductsInSetBySetPid(pcid);
			for (int i=0; i<inSetProducts.length; i++)
			{
				DBRow product = fpm.getDetailProductByPcid(inSetProducts[i].get("pc_id", 0l));
				
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(0);
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL);
//				deInProductStoreLogBean.setPurchase_unit_price(product.get("unit_price",0d));
				
				fpm.incProductStoreCountByPcid(deInProductStoreLogBean, psid, inSetProducts[i].get("pc_id", 0l), split_quantity*inSetProducts[i].get("quantity", 0f),from_psl_id);
			}
			
			wayBillMgrZJ.reCheckLackingWayBillOrdersSub(adminLoggerBean,psid);
			
//			batchMgrLL.batchSplitProduct(psid, pcid, split_quantity, adminLoggerBean.getAdid());
		}
		catch (Exception e)
		{
			throw new SystemException(e,"combinnationProduct",log);
		}
	}
	
	/**
	 * 系统自动拆分（只用于自拼商品扣库存后取消，回退库存,回退批次）
	 * @param psid
	 * @param pcid
	 * @param split_quantity
	 * @param bill_type
	 * @param bill_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void splitProductSystem(long psid,long pcid,int split_quantity,int bill_type,long bill_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			//减少主套装数量
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(bill_id);
			inProductStoreLogBean.setBill_type(bill_type);
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_SYSTEM_SPLIT_UNION_STANDARD);
			fpm.deIncProductStoreCountByPcid(inProductStoreLogBean, psid, pcid, split_quantity);
			
			//增加散件进库
			DBRow inSetProducts[] = fpm.getProductsInSetBySetPid(pcid);
			for (int i=0; i<inSetProducts.length; i++)
			{
				
				
				ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
				deInProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
				deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				deInProductStoreLogBean.setOid(bill_id);
				deInProductStoreLogBean.setBill_type(bill_type);
				deInProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_SYSTEM_SPLIT_UNION_STANDARD);
				
				fpm.incProductStoreCountByPcid(deInProductStoreLogBean, psid, inSetProducts[i].get("pc_id", 0l), split_quantity*inSetProducts[i].get("quantity", 0f),0l);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"combinnationProduct",log);
		}
	}
	
	/**
	 * 增加国家地区
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addCountryArea(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String name = StringUtil.getString(request, "name");
			String note = StringUtil.getString(request, "note");
			long sig_country = StringUtil.getLong(request, "sig_country");
			long sig_state = StringUtil.getLong(request, "sig_state");
			
			DBRow countryArea = new DBRow();
			countryArea.add("name", name);
			countryArea.add("note", note);
			countryArea.add("sig_country", sig_country);
			countryArea.add("sig_state", sig_state);
			return(fpm.addCountryArea(countryArea));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addCountryArea",log);
		}
	}
	
	/**
	 *  修改国家地区
	 * @param request
	 * @throws Exception
	 */
	public void modCountryArea(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long ca_id = StringUtil.getLong(request, "ca_id");
			String name = StringUtil.getString(request, "name");
			String note = StringUtil.getString(request, "note");
			long sig_country = StringUtil.getLong(request, "sig_country");
			long sig_state = StringUtil.getLong(request, "sig_state");
			
			DBRow countryArea = new DBRow();
			countryArea.add("name", name);
			countryArea.add("note", note);
			countryArea.add("sig_country", sig_country);
			countryArea.add("sig_state", sig_state);
			fpm.modCountryArea(ca_id, countryArea);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modCountryArea",log);
		}
	}
	
	/**
	 * 删除国家地区
	 * @param request
	 * @throws Exception
	 */
	public void delCountryAreaByCaId(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long ca_id = StringUtil.getLong(request, "ca_id");
			fpm.delCountryAreaByCaId(ca_id);
			fpm.delCountryAreaMappingByCaId(ca_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delCountryAreaByCaId",log);
		}
	}
	
	/**
	 * 获得已经关联的国家地区
	 * @param ca_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCountryAreaMappingByCaId(long ca_id)
		throws Exception
	{
		try
		{
			return(fpm.getCountryAreaMappingByCaId(ca_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getCountryAreaMappingByCaId",log);
		}
	}
	
	/**
	 * 获得没有被关联国家地区
	 * @param ca_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFreeAreaCountrys(long ca_id)
		throws Exception
	{
		try
		{
			return(fpm.getFreeAreaCountrys( ca_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFreeAreaCountrys",log);
		}
	}
		
	/**
	 * 批量修改仓库-发货国家关系
	 * @param request
	 * @throws Exception
	 */
	public void batchModCountryArea(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long ca_id = StringUtil.getLong(request, "ca_id");
			String cid[] = request.getParameterValues("ccid");
			
			//先删除原来的映射
			fpm.delCountryAreaMappingByCaId(ca_id);
			
			for (int i=0; cid!=null&&i<cid.length; i++)
			{
				//再重新插入
				if ( Long.parseLong(cid[i])>0&&ca_id>0 )
				{
					fpm.addCountryAreaMapping(ca_id, Long.parseLong(cid[i]));
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModCountryArea",log);
		}
	}
	
	/**
	 * 获得所有国家地区设置
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCountryAreas(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fpm.getAllCountryAreas(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllCountryAreas",log);
		}
	}
	
	/**
	 * 获得国家分区详细信息
	 * @param ca_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCountryAreaByCaId(long ca_id)
		throws Exception
	{
		try
		{
			return(fpm.getDetailCountryAreaByCaId(ca_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailCountryAreaByCaId",log);
		}
	}
		
	/**
	 * 保存质保定制套装(新增和修改通用)
	 * @param pc
	 * @throws Exception
	 */
	public void customProductWarranty(HttpServletRequest request)
		throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request, "set_pid");
			String p_name = StringUtil.getString(request, "p_name");
			String cmd = StringUtil.getString(request, "cmd");
			long pc_pc_id = 0;
			
			CustomCartWarranty customCartWarranty = (CustomCartWarranty)MvcUtil.getBeanFromContainer("customCartWarranty");
			CartWarranty cartWarranty = (CartWarranty)MvcUtil.getBeanFromContainer("cartWarranty");
			
			DBRow cart_products[] = customCartWarranty.getDetailProducts(StringUtil.getSession(request),set_pid);
			
			//需要检查下是否对原来套装作出了修改，如果没有修改，直接返回，不保存
			boolean noChangeFlag = true;

			/**
			 * 检查定制套装相对标准套装是否做出了修改
			 * 只在新定制才检查，修改定制不检查
			 */
			if (cmd.equals("add"))
			{
				DBRow oldProductsInSet[] = fpm.getProductsInSetBySetPid(set_pid);
				//数量跟原来相同需要检测
				if (oldProductsInSet.length==cart_products.length)
				{
					ArrayList oldProductsInSetAl = new ArrayList();
					for (int i=0; i<oldProductsInSet.length; i++)
					{
						oldProductsInSetAl.add(oldProductsInSet[i].getString("pc_id")+"_"+oldProductsInSet[i].getString("quantity"));
					}
					
					for (int i=0; i<cart_products.length; i++)
					{
						oldProductsInSetAl.remove(cart_products[i].getString("pc_id")+"_"+cart_products[i].getString("cart_quantity"));
					}
					
					if (oldProductsInSetAl.size()>0)
					{
						noChangeFlag = false;
					}
				}
				else
				{
					noChangeFlag = false;
				}
			}
			else
			{
				noChangeFlag = false;
			}

			/**
			 * 新增或修改定制，做两件事：
			 * 1、把购物车原来标准套装变成定制套装
			 * 2、把临时定制购物车中的定制商品，拷贝到正式定制购物车中（抄单会从这里获得定制数据）
			 */
			if (!noChangeFlag)
			{
				//小购物车商品变化，究竟是新增还是修改？
				if (cmd.equals("add"))
				{
					//保存新定制套装
					DBRow productCustom = new DBRow();
					productCustom.add("orignal_pc_id",set_pid);
					productCustom.add("p_name",p_name+" ▲");
					productCustom.add("catalog_id",fpm.getDetailProductByPcid(set_pid).get("catalog_id", 0l));

					pc_pc_id = fpm.addProductCustom(productCustom);
				}
				else
				{
					pc_pc_id = set_pid;//修改，商品ID就是大购物车中的ID
				}

				//把大购物车中的普通套装，变成定制套装
				cartWarranty.convert2CustomProduct(StringUtil.getSession(request), set_pid, pc_pc_id);
				//把临时定制购物车内容拷贝到最终版
				customCartWarranty.copyTempSet2FinalSet(StringUtil.getSession(request), set_pid,pc_pc_id);
				
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"customProductWarranty",log);
		}
	}
	
	/**
	 * 获得产品的详细，包含产品线ID，产品线名称，分类名称
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllDetailProduct(long pc_id)
		throws Exception
	{
		try 
		{
			return (fpm.getAllDetailProductByPcid(pc_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllDetailProduct",log);
		}
	}
	
	/**
	 * 单据业务回退
	 * @param bill_id
	 * @param bill_type
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void reBackAllBill(long bill_id,int bill_type,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		//
		DBRow[] productStoreLogs =  fpm.getAllProductStoreLogsByOidAndType(bill_id, bill_type);
		
		for (int i = 0; i < productStoreLogs.length; i++)
		{
			long oid = productStoreLogs[i].get("oid",0l);
			double value = productStoreLogs[i].get("quantity",0d);
			long pc_id = productStoreLogs[i].get("pc_id",0l);
			long ps_id = productStoreLogs[i].get("ps_id",0l);
			
			int in_or_de = CountOperationKey.Increase;
			if(value>0)//日志操作>0当初是入库操作，这里要反向操作
			{
				in_or_de = CountOperationKey.Decrease;
			}
			
			//根据日志对日志对库存进行反向操作
			ProductStoreLogBean reProductStoreLogBean = new ProductStoreLogBean();
			reProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
			reProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			reProductStoreLogBean.setOid(oid);
			reProductStoreLogBean.setQuantity(value);
			reProductStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
			reProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			reProductStoreLogBean.setBill_type(bill_type);
			reProductStoreLogBean.setOperation(ProductStoreOperationKey.REBACK);
			
			fpm.rebackProductStoreCountByPcid(reProductStoreLogBean,ps_id, pc_id,Math.abs(value), in_or_de);
			
			//根据日志，回退批次
			productStoreLogsDetailMgrZJ.rebackStoreProductLogsDetail(productStoreLogs[i].get("psl_id",0l), reProductStoreLogBean);
		}
		
		productStoreLocationMgrZJ.cancelOutListDetail(bill_id,bill_type);
	}
	
	/**
	 * 订单缺货商品与数量
	 * @param request
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] outStockProductForOrder(HttpServletRequest request,long ps_id)
		throws Exception
	{
		try
		{
			boolean orderIsLacking = false;
			ArrayList<Object> result = new ArrayList<Object>();//重新计算封装每个order item，以便前台显示
			
			CartWaybill cartWaybill = (CartWaybill)MvcUtil.getBeanFromContainer("cartWaybill");//普通商品
			
			cartWaybill.flush(StringUtil.getSession(request));
			DBRow cartProductItems[] = cartWaybill.getDetailProduct();//从购物车获得订单主商品信息
	
			/**
			 * 这里要处理重复抄单，处理逻辑跟抄单一致
			 */
	
			DBRow productCount = new DBRow();//用来累加商品需要总数量，有一种情况：一个单品既存在于购物车，也存在于定制商品或拼装中
			DBRow productStoreOutCount = new DBRow();
			for (int j=0; j<cartProductItems.length; j++)
			{
					long pid = StringUtil.getLong(cartProductItems[j].getString("cart_pid"));
					float quantity = StringUtil.getFloat(cartProductItems[j].getString("cart_quantity"));
	
					if (cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
					{
						int lacking = ProductStatusKey.IN_STORE;
	
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
	
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							orderIsLacking = true;
							
							//记录商品缺货数量
							this.addStockoutProductQuantity(productStoreOutCount, pid, quantity);
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity+this.getCumProductQuantity(productCount, pid))
							{
								lacking = ProductStatusKey.STORE_OUT;
								orderIsLacking = true;
								
								float stock_quantity = (quantity+this.getCumProductQuantity(productCount, pid))-detailPro.get("store_count", 0f);
								
								//记录商品缺货数量
								this.addStockoutProductQuantity(productStoreOutCount, pid, stock_quantity);
							}
						}
						this.addCumProductQuantity(productCount, pid, quantity);
						
						//对原来的购物车中的item增加一个缺货标记字段
						cartProductItems[j].add("lacking", lacking);
					}
					else if ( cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD||cartProductItems[j].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//如果是标准套装缺货，先检查标准套装是否有货，如果没货，再检查散件。标准套装退标准套装
					{
						int lacking = ProductStatusKey.IN_STORE;
						boolean standardUnionLacking = false;
						boolean manualUnionLacking = false;
						
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pid);
							
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
							standardUnionLacking = true;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity)
							{
								lacking = ProductStatusKey.STORE_OUT;
								standardUnionLacking = true;
							}
						}
						//标准套装有货
						if (lacking==ProductStatusKey.IN_STORE)
						{
							cartProductItems[j].add("lacking", lacking);
							cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
							////system.out.println(ps_id+":1");
						}
						else//否则，再检查散件
						{
							float needManualQuantity ;
							//计算需要散件的套数
							if (detailPro.get("store_count", 0f)>0)
							{
								//除了标准套装提供的数量外，还需要瓶装多少套散件
								needManualQuantity = quantity - detailPro.get("store_count", 0f);
							}
							else
							{
								needManualQuantity = quantity;
							}
	
							lacking = ProductStatusKey.IN_STORE;
							
							DBRow productsInSet[] = fpm.getProductsInSetBySetPid( pid );
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									manualUnionLacking = true;
									
									//记录商品缺货数量
									this.addStockoutProductQuantity(productStoreOutCount,productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*needManualQuantity);
								}
								else
								{
									
									if (detailPro2.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*needManualQuantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = ProductStatusKey.STORE_OUT;
										manualUnionLacking = true;
										
										float stock_quantity = Math.abs(detailPro2.get("store_count", 0f)-(productsInSet[jj].get("quantity", 0f)*needManualQuantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l))));
										//记录商品缺货数量
										this.addStockoutProductQuantity(productStoreOutCount,productsInSet[jj].get("pc_id", 0l),stock_quantity);
									}
								}
								
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*needManualQuantity);
							}
	
							if (lacking==ProductStatusKey.IN_STORE)
							{
								//把item修改为拼装
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD_MANUAL);
								cartProductItems[j].add("lacking", lacking);
								////system.out.println(ps_id+":2");								
							}
							else if(lacking==ProductStatusKey.STORE_OUT)
							{
								cartProductItems[j].add("lacking", lacking);
								cartProductItems[j].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
								////system.out.println(ps_id+":3");
							}
							
							if (standardUnionLacking&&manualUnionLacking)
							{
								orderIsLacking = true;
							}
						}
					}
					else  //定制 
					{
						int lacking = 0;		
						DBRow productsCustomInSet[] = fpm.getProductCustomUnionsBySetId(pid);
						for (int jj=0; jj<productsCustomInSet.length; jj++)
						{
								//检查库存
								DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[jj].get("pid", 0l));
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									orderIsLacking = true;
									
									//记录商品缺货数量
									this.addStockoutProductQuantity(productStoreOutCount, productsCustomInSet[jj].get("pid", 0l),StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity);
								}
								else
								{
									if (detailPro.get("store_count", 0f)< StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pid", 0l)) )
									{
										lacking = ProductStatusKey.STORE_OUT;
										orderIsLacking = true;
										
										float stock_quantity = Math.abs(detailPro.get("store_count", 0f) - (StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pid", 0l))));
										//记录商品缺货数量
										this.addStockoutProductQuantity(productStoreOutCount,productsCustomInSet[jj].get("pid", 0l),stock_quantity);
									}
								}
								this.addCumProductQuantity(productCount, productsCustomInSet[jj].get("pc_id", 0l), StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity);
						}
						
						cartProductItems[j].add("lacking", lacking);
					}
				result.add(cartProductItems[j]);
			}
			
			ArrayList list = productStoreOutCount.getFieldNames();
			
			DBRow[] lackingProducts = new DBRow[list.size()];
			for(int i = 0;i<lackingProducts.length;i++)
			{
				DBRow product = fpm.getDetailProductByPcid(Long.parseLong(list.get(i).toString()));
				
				product.add("lackingCount",productStoreOutCount.get(list.get(i).toString(),0f));
				
				lackingProducts[i] = product;
			} 
			
			return(lackingProducts);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"preCalcuOrder",log);
		}
	}
	
	/**
	 * 运单缺货商品与数量
	 * @param request
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] outStockProductForWaybill(long waybill_id)
		throws Exception
	{
		try
		{
			DBRow waybill = floorWayBillOrderMgrZJ.getWayBillOrderById(waybill_id);
			long ps_id = waybill.get("ps_id",0l);
			
			DBRow[] waybillLackingItems = floorWayBillOrderMgrZJ.getLackingWayBillItems(waybill_id);
	
			/**
			 * 这里要处理重复抄单，处理逻辑跟抄单一致
			 */
	
			DBRow productCount = new DBRow();//用来累加商品需要总数量，有一种情况：一个单品既存在于购物车，也存在于定制商品或拼装中
			DBRow productStoreOutCount = new DBRow();
			for (int j=0; j<waybillLackingItems.length; j++)
			{
					long pc_id = waybillLackingItems[j].get("pc_id",0l);
					float quantity = waybillLackingItems[j].get("quantity",0f);
	
					if (waybillLackingItems[j].get("product_type",0)==ProductTypeKey.NORMAL)
					{
						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pc_id);
	
						if (detailPro==null)
						{
							//记录商品缺货数量
							this.addStockoutProductQuantity(productStoreOutCount, pc_id, quantity);
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity+this.getCumProductQuantity(productCount, pc_id))
							{
								float stock_quantity = (quantity+this.getCumProductQuantity(productCount, pc_id))-detailPro.get("store_count", 0f);
								
								//记录商品缺货数量
								this.addStockoutProductQuantity(productStoreOutCount, pc_id, stock_quantity);
							}
						}
						this.addCumProductQuantity(productCount, pc_id, quantity);
						
					}
					else if(waybillLackingItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD||waybillLackingItems[j].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL)//如果是标准套装缺货，先检查标准套装是否有货，如果没货，再检查散件。标准套装退标准套装
					{
						int lacking = ProductStatusKey.IN_STORE;

						DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,pc_id);
							
						if (detailPro==null)
						{
							lacking = ProductStatusKey.STORE_OUT;
						}
						else
						{
							if (detailPro.get("store_count", 0f)<quantity)
							{
								lacking = ProductStatusKey.STORE_OUT;
							}
						}
						//标准套装有货
						if (lacking==ProductStatusKey.IN_STORE)
						{
							
						}
						else//否则，再检查散件
						{
							float needManualQuantity ;
							//计算需要散件的套数
							if (detailPro.get("store_count", 0f)>0)
							{
								//除了标准套装提供的数量外，还需要瓶装多少套散件
								needManualQuantity = quantity - detailPro.get("store_count", 0f);
							}
							else
							{
								needManualQuantity = quantity;
							}
	
							lacking = ProductStatusKey.IN_STORE;
							
							DBRow productsInSet[] = fpm.getProductsInSetBySetPid(pc_id);
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = fpm.getDetailProductProductStorageByPcid(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2==null)	//	商品还没进库，当然缺货
								{
									lacking = ProductStatusKey.STORE_OUT;
									
									//记录商品缺货数量
									this.addStockoutProductQuantity(productStoreOutCount,productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*needManualQuantity);
								}
								else
								{
									if (detailPro2.get("store_count", 0f)< productsInSet[jj].get("quantity", 0f)*needManualQuantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = ProductStatusKey.STORE_OUT;
										
										float stock_quantity = Math.abs(detailPro2.get("store_count", 0f)-(productsInSet[jj].get("quantity", 0f)*needManualQuantity+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l))));
										//记录商品缺货数量
										this.addStockoutProductQuantity(productStoreOutCount,productsInSet[jj].get("pc_id", 0l),stock_quantity);
									}
								}
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*needManualQuantity);
							}
						}
					}
					else  //定制 
					{
						DBRow productsCustomInSet[] = fpm.getProductCustomUnionsBySetId(pc_id);
						for (int jj=0; jj<productsCustomInSet.length; jj++)
						{
								//检查库存
								DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[jj].get("pid", 0l));
								if (detailPro==null)	//	商品还没进库，当然缺货
								{
									//记录商品缺货数量
									this.addStockoutProductQuantity(productStoreOutCount,productsCustomInSet[jj].get("pid", 0l),StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity);
								}
								else
								{
									if (detailPro.get("store_count", 0f)< StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pid", 0l)) )
									{
										float stock_quantity = Math.abs(detailPro.get("store_count", 0f) - (StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity+this.getCumProductQuantity(productCount, productsCustomInSet[jj].get("pid", 0l))));
										//记录商品缺货数量
										this.addStockoutProductQuantity(productStoreOutCount,productsCustomInSet[jj].get("pid",0l),stock_quantity);
									}
								}
								this.addCumProductQuantity(productCount, productsCustomInSet[jj].get("pc_id", 0l), StringUtil.getFloat(productsCustomInSet[jj].getString("quantity"))*quantity);
						}
					}
			}
			
			ArrayList list = productStoreOutCount.getFieldNames();
			
			DBRow[] lackingProducts = new DBRow[list.size()];
			for(int i = 0;i<lackingProducts.length;i++)
			{
				DBRow product = fpm.getDetailProductByPcid(Long.parseLong(list.get(i).toString()));
				
				product.add("lackingCount",productStoreOutCount.get(list.get(i).toString(),0f));
				
				lackingProducts[i] = product;
			} 
			
			return(lackingProducts);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"preCalcuOrder",log);
		}
	}
	
	
	public void ReBack()
		throws Exception
	{
		DBRow[] today = fpm.todayReback();
		
		for (int i = 0; i < today.length; i++) 
		{
			long oid = today[i].get("oid",0l);
			double value = today[i].get("quantity",0d);
			long pc_id = today[i].get("pc_id",0l);
			long ps_id = today[i].get("ps_id",0l);
			
			int in_or_de = 2;
			if(value>0)
			{
				in_or_de = 1;
			}
			
			fpm.rebackProductStoreCountByPcid(null,ps_id, pc_id,Math.abs(value), in_or_de);
		}
		
	}
	
	/**
	 * 通过条码类型及条码查询条码个数
	 */
	public DBRow[] getProductCodeByTypeAndCode(int codeType, String pcCode) throws Exception
	{
		try
		{
			return fpm.getProductCodeByTypeAndCode(codeType, pcCode);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductCodeByTypeAndCode(int codeType, String code)",log);
		}
	}
	
	@Override
	public DBRow[] getProductByKeyWord(String keyWord) throws Exception {
		Long pcid = convertStringToLong(keyWord);
		Map<Long, DBRow> dataMaps = new HashMap<Long,DBRow>();
		if(pcid != 0l){	//查询ID,现在认为如果单纯是数字的那么就是认为他扫描出来的是商品ID
			DBRow row = fpm.getSimpleProductInfoByPcId(pcid);
			if(row != null){
				putDBRowToMap(dataMaps, row);	
			}
		}
		DBRow[] pNameArrays = fpm.getSimpleProductInfoByPName(keyWord);	//商品名
		DBRow[] pCodeArrays = fpm.getSimpleProductInfoByPCode(keyWord);	//pcode
		
		putDBRowToMap(dataMaps, pNameArrays);	
		putDBRowToMap(dataMaps, pCodeArrays);	
  		return convertMapToDBRow(dataMaps);
	}
	/**
	 * convert map to DBRow[];
	 * @param dataMaps
	 * @return
	 */
	private DBRow[] convertMapToDBRow(Map<Long,DBRow> dataMaps){
		List<DBRow> returnArray = new ArrayList<DBRow>();
		if(dataMaps != null && dataMaps.size() > 0 ){
			Iterator<Entry<Long, DBRow>>  it =  dataMaps.entrySet().iterator();
			while(it.hasNext()){
				returnArray.add(it.next().getValue());
			}
		}
		return returnArray.toArray(new DBRow[0]);
	}
	
	private void putDBRowToMap(Map<Long,DBRow> dataMaps, DBRow... datas){
		if(datas != null){
			for(DBRow row : datas){
				dataMaps.put(row.get("pc_id", 0l), row);
			}
		}
	}
	private Long convertStringToLong(String keyWord){
		Long returnValue = 0l ;
		try{
			returnValue = Long.parseLong(keyWord.trim());
		}catch (Exception e) {
 		}
		return returnValue ;
	}
	public void setFpm(FloorProductMgr fpm)
	{
		this.fpm = fpm;
	}

	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}

	public void setAdminMgr(AdminMgr adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	public void setTaskMgr(TaskMgr taskMgr)
	{
		this.taskMgr = taskMgr;
	}

	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}


	public void setFloorExpressMgr(FloorExpressMgr floorExpressMgr) {
		this.floorExpressMgr = floorExpressMgr;
	}

	public void setWayBillMgrZJ(WayBillMgrIFaceZJ wayBillMgrZJ) {
		this.wayBillMgrZJ = wayBillMgrZJ;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}

	public void setProductCodeMgrZJ(ProductCodeMgrZJ productCodeMgrZJ) {
		this.productCodeMgrZJ = productCodeMgrZJ;
	}

	public void setProductStoreLogsDetailMgrZJ(
			ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ) {
		this.productStoreLogsDetailMgrZJ = productStoreLogsDetailMgrZJ;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setProductStoreLocationMgrZJ(ProductStoreLocationMgrIFaceZJ productStoreLocationMgrZJ) {
		this.productStoreLocationMgrZJ = productStoreLocationMgrZJ;
	}

	public void setFloorLocationMgrZJ(FloorLocationMgrZJ floorLocationMgrZJ) {
		this.floorLocationMgrZJ = floorLocationMgrZJ;
	}

	public void setCustomSeachConditions(
			CustomSeachConditionsIfaceGql customSeachConditions) {
		this.customSeachConditions = customSeachConditions;
	}
	

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}


	public static void main(String[] args) 
	{
//		byte[] year = new byte[4];
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		
//		Date time = new Date();
//		
//		String c = sdf.format(time);
//		////system.out.println(c);
//		byte[]b = c.getBytes();
//		System.arraycopy(b,0,year,0,4);
//		
//		String acc = new String(year);
		////system.out.println(acc);
		String a = "0";
		String[] split = a.split(",");
		//system.out.println(split.length);
	}
	
	public DBRow isProductExistByPname(String name)
			throws Exception
		{
			try
			{
				DBRow row = fpm.isProductExistByPname(name);
				return(row);
			}
			catch (Exception e)
			{
				throw new SystemException(e,"getProductByCid",log);
			}
		}
}



