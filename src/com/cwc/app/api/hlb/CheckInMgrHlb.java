package com.cwc.app.api.hlb;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.api.zyj.CheckInMgrZyj;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.hlb.FloorCheckInMgrHlb;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.service.android.action.mode.HoldThirdValue;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * <p>ClassName：CheckInMgrHlb</p>
 * <p>Description：check_in 模块所调用方法的接口的实现</p>
 * @author huanglianbin
 * @version 1.0 V
 * <p>createTime: 2015 年 2 月 28 日</p>
 */
public class CheckInMgrHlb implements CheckInMgrIfaceHlb{
	
	static Logger log = Logger.getLogger( "ACTION" );
	private FloorCheckInMgrHlb floorCheckInMgrHlb;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private FloorCheckInMgrWfh floorCheckInMgrWfh;
	private CheckInMgrIfaceZr checkInMgrZr;
	
	public void setCheckInMgrZwb( CheckInMgrIfaceZwb checkInMgrZwb ) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
	public void setFloorCheckInMgrHlb( FloorCheckInMgrHlb floorCheckInMgrHlb ) {
		this.floorCheckInMgrHlb = floorCheckInMgrHlb;
	}
	
	public void setFloorCheckInMgrWfh( FloorCheckInMgrWfh floorCheckInMgrWfh ) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}
	
	public void setCheckInMgrZr( CheckInMgrIfaceZr checkInMgrZr ) {
		this.checkInMgrZr = checkInMgrZr;
	}
	
	@Override
	public DBRow findDoorInfoById( String loadNo, long entry_Id ) throws Exception {
		try{
			return this.floorCheckInMgrHlb.findDoorInfoById( loadNo, entry_Id );
		}catch( Exception e ) {
			throw new SystemException( e, "findMainMesByCondition", log );
		}
	}
	
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo)throws Exception {
		if(!StringUtil.isBlank(loadNo)) {
			return floorCheckInMgrHlb.findMasterBolLinesByLoadNo(loadNo);
		} else {
			return new DBRow[0];
		}
	}
	
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo)throws Exception {
		if(!StringUtil.isBlank(loadNo)) {
			return floorCheckInMgrHlb.findOrdersNoAndCompanyByLoadNo(loadNo);
		} else {
			return new DBRow[0];
		}
	}
	
	// // 查询 loading 表
/**	@Override
	public DBRow getDoorByStagingOrTitle(long type,String search_number, long adid, HttpServletRequest request)
			throws Exception {
	
		try{
			
			DBRow result =null;
			if(type==GateCheckLoadingTypeKey.LOAD){
				result= sqlServerMgrZJ.findOrderInfoByLoadNo(search_number, adid, request);

			}else{
				//result= sqlServerMgrZJ.findReceiptByContainerBolNo(search_number, adid, request);
			}
			return result;
		}catch(Exception e){
			throw new SystemException(e,"getLoading",log);
		}
	}*/

	@Override
	public DBRow[] findMcDotByCompanyName(String companyName) throws Exception {
		companyName=companyName.replace("'","''");
		return floorCheckInMgrHlb.findMcDotByCompanyName(companyName);
	}
	
	/*
	 * 根据 mc_dot 查询运输公司名字
	 */
	@Override
	public DBRow[] findCompanyNameByMcDot(String mc_dot) throws Exception {
		return floorCheckInMgrHlb.findCompanyNameByMcDot(mc_dot);
	}	
	/*
	 * 根据车牌号查询驾驶员名字
	 */
	@Override
	public DBRow[] findNameByLicense(String licenseNo) throws Exception {
		return floorCheckInMgrHlb.findNameByLicense(licenseNo);
	}

	/*
	 * 根据运输公司名字更新 mc_dot
	 */
	@Override
	public long ModMcdotByCompanyName(String companyName, DBRow row) throws Exception {
		return floorCheckInMgrHlb.ModMcdotByCompanyName(companyName, row);
	}

	/*
	 * 根据运输公司名字查询  carrier_scac_mcdot 表信息
	 */
	@Override
	public DBRow[] findCarrierInfoByCompanyName(String companyName) throws Exception {
		companyName=companyName.replace("'","''");
		return floorCheckInMgrHlb.findCarrierInfoByCompanyName(companyName);
	}

	/*
	 * 添加 carrier 信息
	 */
	@Override
	public long addCarrier(DBRow carrierInfo) throws Exception {
		// TODO Auto-generated method stub
		return floorCheckInMgrHlb.addCarrier(carrierInfo);
	}

	/*
	 * 根据mc_dot查询  carrier_scac_mcdot 表信息
	 */
	@Override
	public DBRow[] findCarrierInfoByMcdot(String mc_dot) throws Exception {
		return floorCheckInMgrHlb.findCarrierInfoByMcdot(mc_dot);
	}

	/*
	 * 根据 mc_dot更新运输公司名字
	 */
	@Override
	public long ModCompanyNameByMcdot(String mc_dot, DBRow row)
			throws Exception {
		return floorCheckInMgrHlb.ModCompanyNameByMcdot(mc_dot, row);
	}

	/*
	 * carrier 自动提示
	 */
	@Override
	public DBRow[] getSearchCheckInDBCarrierJSON(String carrier) throws Exception {
		carrier=carrier.replace("'","''");
		return floorCheckInMgrHlb.getSearchCheckInDBCarrierJSON(carrier);
	}
	
	/*
	 * mc_dot 自动提示
	 */
	@Override
	public DBRow[] getSearchCheckInDBMcdotJSON(String mc_dot) throws Exception {
		return floorCheckInMgrHlb.getSearchCheckInDBMcdotJSON(mc_dot);
	}
	
	/*
	 *  查询同时匹配 mc_dot 和 carrier 的记录(non-Javadoc)
	 * @see com.cwc.app.iface.hlb.CheckInMgrIfaceHlb#matchNameAndMcdot(java.lang.String, java.lang.String)
	 */
	public DBRow matchNameAndMcdot(String carrierName, String mc_dot) throws Exception {
		return floorCheckInMgrHlb.matchNameAndMcdot(carrierName, mc_dot);
	}	
	/*
	 * 更新 DriverInfo
	 */
	public long updateDrivenInfo(long main_id, DBRow row) throws Exception {
		return floorCheckInMgrHlb.updateDrivenInfo(main_id, row);
	}	
	
	@Override
	public DBRow matchNameAndMcdotInMain(String carrierName, String mc_dot)
			throws Exception {
		return floorCheckInMgrHlb.matchNameAndMcdotInMain(carrierName, mc_dot);
	}

	@Override
	public DBRow[] findMcDotByCompanyNameInMain(String companyName)
			throws Exception {
		return floorCheckInMgrHlb.findMcDotByCompanyNameInMain(companyName);
	}

	@Override
	public DBRow[] findCompanyNameByMcDotInMain(String mc_dot) throws Exception {
		// TODO Auto-generated method stub
		return floorCheckInMgrHlb.findCompanyNameByMcDotInMain(mc_dot);
	}	
	
	public DBRow gateCheckInUpdate(DBRow row, HttpServletRequest request) throws Exception {
		
		
		String companyName = StringUtil.getString(request, "carrierName");
		String mc_dot =  StringUtil.getString(request, "mc_dot");
		String gpsStr = StringUtil.getString(request, "gps_name");
		long gps = StringUtil.getLong(request, "gps");
		int mark = StringUtil.getInt(request, "mark");
		String driver_liscense = StringUtil.getString(request, "driver_liscense");
		String driver_name = StringUtil.getString(request, "driver_name");
		long priority = StringUtil.getLong(request, "priority");
		long main_id = StringUtil.getLong(request, "main_id");
		
		// 查询出数据库中的主表信息
		String companyName_sys = row.getString("company_name");
		String mc_dot_sys =  row.getString("mc_dot");
		long gps_sys = row.get("gps_tracker", 0L);
		String driver_liscense_sys = row.getString("gate_driver_liscense");
		String driver_name_sys = row.getString("gate_driver_name");
		long priority_sys = row.get("priority", 0L);
		
		Boolean isUpdate = true;
		// 和提交的信息进行比对，看是否进行了更新
		if(companyName_sys.equals(companyName) && 
		   mc_dot_sys.equals(mc_dot) &&
		   gps_sys == gps && gpsStr.equals(row.getString("imei"))&&
		   driver_liscense_sys.equals(driver_liscense) &&
		   driver_name_sys.equals(driver_name) &&
		   priority_sys == priority) {
			isUpdate = false;
		}
		DBRow row_update = new DBRow();
		
		if (row != null && isUpdate) {
			String currentTime = DateUtil.NowStr();
			row_update.add("mc_dot", mc_dot);
			row_update.add("company_name", companyName);
			row_update.add("gps_tracker", gps);
			row_update.add("gate_driver_liscense", driver_liscense);
			row_update.add("gate_driver_name", driver_name);
			row_update.add("priority", priority);
//			row_update.add("window_check_in_operate_time", currentTime);
//			if(checkInMgrZyj.whetherWriteWindowCheckInTime(main_id) && row.getString("window_check_in_time").equals("")) {
//				row_update.add("window_check_in_time", currentTime);
//				//获得当前登录账号信息,操作时间
//				AdminMgr adminMgr		= new AdminMgr();
//				AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
//				row_update.add("window_check_in_operator",adminLoginBean.getAdid());
//			}
			//gps
			if(mark==1){
				if(StrUtil.isBlank(gpsStr))
				{
					gps = 0;
				}
				else
				{
					DBRow gpsRow1 = checkInMgrZwb.findAssetByGPSNumber(gpsStr);
					if(gpsRow1==null)
					{
						gps = checkInMgrZwb.addAsset(gpsStr,driver_liscense,0,"","","");
					}
					else
					{
						gps = gpsRow1.get("id", 0L);
						DBRow updateRow = new DBRow();
						updateRow.add("def", driver_liscense);
						checkInMgrZwb.updateGpsInfos(gps,updateRow);
					}
				}
				row_update.add("gps_tracker", gps);
			}else{
				if(gps>0 && !StrUtil.isBlank(gpsStr)){
					DBRow updateRow = new DBRow();
					updateRow.add("def", driver_liscense);
					checkInMgrZwb.updateGpsInfos(gps,updateRow);
					row_update.add("gps_tracker", gps);
				}
				else
				{
					row_update.add("gps_tracker", 0);
				}
			}
			long count = updateDrivenInfo(main_id, row_update);
			
			//更新entry下没有windowCheckIn时间的设备
//			if(checkInMgrZyj.whetherWriteWindowCheckInTime(main_id))
//			{
//				DBRow equipment = new DBRow();
//				equipment.add("check_in_window_time", !StrUtil.isBlank(row.getString("window_check_in_time"))?row.getString("window_check_in_time"):null);
//				floorCheckInMgrHlb.updateEntryEquipmentNoWindowCheckInTime(equipment, main_id);
//			}
			
			if (count > 0) {
				row_update.add("result", "success");
				
			}
			checkInMgrZwb.editCheckInIndex(main_id, "update");
		} else {
			row_update.add("result", "notChange");	
		}
		
		return row_update;
	}
	
	public DBRow getMatchResult(DBRow rows_all, DBRow[] rows_mc_dot, DBRow[] rows_carrier) throws Exception {
		
		DBRow row = new DBRow();
		// 1、mc_dot、carrier 在 csm 中有完全匹配的记录
		if(rows_all != null) {
				row = rows_all;
				row.add("result", "complete_matching");
		} else {
			// 2、mc_dot 在 csm 中有一条记录，
			//   carrier 在 csm 中有一条记录，
			//   但不是完全匹配，
			if(rows_mc_dot.length == 1 && rows_carrier.length == 1) {
				row.add("result", "not_exactly_match");
				row.add("rows_mc_dot", rows_mc_dot);
				row.add("rows_carrier", rows_carrier);
			// 3、 mc_dot 在 csm 中有一条记录，
			//   carrier 在 csm 中没有记录，
			//   mc_dot、carrier 在 csm 中没有完全匹配的记录，
			} else if(rows_mc_dot.length == 1 && rows_carrier.length == 0) {
				row = rows_mc_dot[0];
				row.add("result", "updateByMcdot");
			// 4、mc_dot 在 csm 中没有记录，
			//	 carrier 在 csm 中有一条记录，
			//	 mc_dot、carrier 在 csm 中没有完全匹配的记录，
			} else if(rows_mc_dot.length == 0 && rows_carrier.length == 1 ) {
				row = rows_carrier[0];
				row.add("result", "updateByCarrier");
			
			// 5、 mc_dot 在 csm 中没有记录，
			//   carrier 在 csm 中没有记录，
			//	 mc_dot、carrier 在 csm 中没有完全匹配的记录，
			} else if(rows_mc_dot.length == 0 && rows_carrier.length == 0 ) {
				row.add("result", "not_match_at_all");
			}
		}
		
		return row;
	}

	@Override
	public DBRow[] getTaskScanPallets(long detail_id, int number_type)
			throws Exception {
		try{
			CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");	
			return checkInMgrZr.getTaskScanPallets(detail_id, number_type);
		}catch(Exception e){
			throw new SystemException();
		}
 	}
	
	/**
	 * MethodName：CloseNoticeByEid
	 * Description： 通过 entryId 关闭主单据的所有 window schedule
	 * 	 	1、通过 entryId 查询到 schedule_id
	 * 		2、循环遍历，如果 schedule 未被关闭且是属于 window schedule，调用 CheckInMgrIfaceZr 中关闭 schedule 的方法 -> finishSchedule
	 * 		3、分情况处理对前台进行响应
	 * 			1）有 schedule 未被关闭
	 * 			2）所有 schedule 已经被关闭	
	 * @author: huanglianbin
	 * @param request
	 * @return
	 * @throws Exception 
	 * @see com.cwc.app.iface.hlb.CheckInMgrIfaceHlb#CloseNoticeByEid( long )
	 */
	@Override
	public DBRow CloseNoticeByEid( HttpServletRequest request ) throws Exception {
		// 获得创建人
		AdminMgr adminMgr = ( AdminMgr )MvcUtil.getBeanFromContainer( "adminMgr" );
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean( StringUtil.getSession( request ) );
		long adid = adminLoginBean.getAdid();	
		long entry_id = StringUtil.getLong( request, "entry_id" );
		DBRow[] scheduleRows = floorCheckInMgrWfh.findScheduleByMainId( entry_id, 0 );
		int count = 0;
		DBRow row = new DBRow();
		
		for (int i = 0; i < scheduleRows.length; i++) {
			if ( scheduleRows[i].get( "schedule_state", 0f ) != 10.0 
					&& ( scheduleRows[i].get( "associate_process", 0 ) == ProcessKey.GateHasTaskNotifyWindow 
					|| scheduleRows[i].get( "associate_process", 0 ) == ProcessKey.GateNoTaskNotifyWindow ) ) {
				checkInMgrZr.finishSchedule( scheduleRows[i].get( "schedule_id", 0L ), adid );
				count++;
			}
		}
		
		if( count > 0 ) {
			row.add( "result", "success" );
		} else {
			row.add( "result", "allClose" );
		}		
		
		return row;
	}
	
	/**
	 * MethodName： CanOrNoGateEdit
	 * Description：判断 window 是否发了 schedule
	 * @author: huanglianbin
	 * @param request
	 * @return
	 * @throws Exception 
	 * @see com.cwc.app.iface.hlb.CheckInMgrIfaceHlb#CanOrNoGateEdit( javax.servlet.http.HttpServletRequest )
	 */
	@Override
	public DBRow CanOrNoGateEdit(HttpServletRequest request) throws Exception {
		long entry_id = StringUtil.getLong( request, "entry_id" );
		DBRow[] scheduleRows = floorCheckInMgrWfh.findScheduleByMainId( entry_id, 0 );
		int count = 0;
		DBRow row = new DBRow();
		
		for (int i = 0; i < scheduleRows.length; i++) {
			if ( scheduleRows[i].get( "associate_process", 0 ) == ProcessKey.CHECK_IN_WINDOW ) {
				count++;
			}
		}		
		
		if( count > 0 ) {
			row.add( "result", "no" );
		} else {
			row.add( "result", "can" );
		}		
		
		return row;
	}
	
	
}
