package com.cwc.app.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class ExportPOrder2ExcelServlet extends HttpServlet
{
	static Logger log=null;
	
	public void init(ServletConfig config)
		throws ServletException 
	{
		log = Logger.getLogger("ACTION");
	}
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, IOException
	{
		doPost(request,response) ;
	}

	protected void doPost(HttpServletRequest request,HttpServletResponse response) 
		throws ServletException, IOException 
	{
		try 
		{
			 OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");

			PrintWriter out = response.getWriter();
			
			String d = DateUtil.NowStr();
			d = d.replace(':','-');

			response.setContentType("application/octet-stream");
			response.setHeader("Pragma","No-cache");
			response.setHeader("Cache-Control","no-cache");
			response.setDateHeader("Expires", 0);
			response.setHeader("Content-Disposition", "attachment; filename=\"paypal_orders_" + d + ".csv\"");

			StringBuffer sb = new StringBuffer("");
			sb.append("Syn Date");
			sb.append(",");
			sb.append("Payment Date");
			sb.append(",");
			sb.append("Name");
			sb.append(",");
			sb.append("Status");
			sb.append(",");
			sb.append("Currency");
			sb.append(",");
			sb.append("Gross");
			sb.append(",");
			sb.append("Fee");
			sb.append(",");
			sb.append("Net");
			sb.append(",");
			sb.append("Transaction ID");
			sb.append(",");
			sb.append("Item Name");
			sb.append(",");
			sb.append("Item ID");
			sb.append(",");
			sb.append("Quantity");
			sb.append(",");
			sb.append("Address");
			sb.append("\n");


			DBRow row[] = orderMgr.getOrdersByFilter(request,null);
			String postDate;
			String tmp;
			
			for (int i=0; i<row.length; i++ )
			{
				postDate = row[i].getString("post_date");
				tmp = postDate.split(" ")[0];
				tmp = StringUtil.replaceString(tmp,"-","--");
				
				sb.append("\"");
				sb.append(tmp);
				sb.append("\",\"");
				sb.append(row[i].getString("payment_date"));
				sb.append("\",\"");
				sb.append(row[i].getString("first_name"));
				sb.append(" ");
				sb.append(row[i].getString("last_name"));
				sb.append("\",\"");
				sb.append(row[i].getString("payment_status"));
				sb.append("\",\"");	
				sb.append(row[i].getString("mc_currency"));
				sb.append("\",\"");
				sb.append(row[i].getString("mc_gross"));
				sb.append("\",\"");
				sb.append(row[i].getString("mc_fee"));
				sb.append("\",\"");
				sb.append(StringUtil.formatNumber("#0.00",row[i].get("mc_gross",0d)-row[i].get("mc_fee",0d)));
				sb.append("\",\"");
				sb.append(row[i].getString("txn_id"));
				sb.append("\",\"");
				sb.append(row[i].getString("item_name"));
				sb.append("\",\"");
				sb.append(row[i].getString("item_number"));
				sb.append("\",\"");
				sb.append(row[i].getString("quantity"));
				sb.append("\",\"");

				sb.append(StringUtil.replaceString(row[i].getString("address_name"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_street"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_city"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_state"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_zip"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_country_code"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_country"),"\n",""));
				sb.append(" ");
				sb.append(StringUtil.replaceString(row[i].getString("address_status"),"\n",""));

				sb.append("\"\n");
			}

			//out.write(sb.toString());
			out.print( StringUtil.UTF82ISO(sb.toString()) );
			out.flush();
		} 
		catch (IOException e)
		{
			log.error("ExportPOrder2ExcelServlet IOException:"+e);
		}
		catch (Exception e) 
		{
			log.error("ExportPOrder2ExcelServlet Exception:"+e);
		}
	}
}
