package com.cwc.app.api;

public enum Warehouse {
	
	AMAZON(100200),
	VALLEY(1000005),
	DALLAS(1000006),
	WALNUT(1000007),
	STIMSON(1000008),
	VIABARON(1000009),
	ELWOOD(1000018),
	TURNBULL(1005696),
	WOODBRIDGE(1006858),
	DALLASWILDLIFE(1010808),
	BRADSHAW(1000011),
	BLUEHERON(1000010),
	ENELSON(1000012);
	
	private long id ;
	
	private Warehouse(long id){
		this.id = id;
	}
	
	public static long getWareHourseId(String warehouse){
		long warehouseId = 0;
		try{
			warehouseId = Warehouse.valueOf(warehouse.toUpperCase()).id;
		}catch(Exception ex){}
		return warehouseId;
	}
}
