package com.cwc.app.api.zzz;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReport;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.base.JRBaseLine;
import net.sf.jasperreports.engine.base.JRBasePrintText;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignRectangle;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zzz.FloorQuotedPriceTableMgrZZZ;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zzz.QuotePriceMgrIfaceZZZ;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.*;
import com.cwc.exception.SystemException;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class QuotePriceMgrZZZ implements QuotePriceMgrIfaceZZZ {

	static Logger log = Logger.getLogger("ACTION");
	private FloorQuotedPriceTableMgrZZZ floorQuotedPriceTableMgr;	
	private ProductMgrIFace productMgr;
	private QuoteIFace quoteMgr;
	private SystemConfigIFace systemConfig;
	private ExpressMgrIFace expressMgr;
	public void setExpressMgr(ExpressMgrIFace expressMgr) {
		this.expressMgr = expressMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setQuoteMgr(QuoteIFace quoteMgr) 
	{
		this.quoteMgr = quoteMgr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) 
	{
		this.productMgr = productMgr;
	}

	public void setFloorQuotedPriceTableMgr
	(
			FloorQuotedPriceTableMgrZZZ floorQuotedPriceTableMgr)
	{
		this.floorQuotedPriceTableMgr = floorQuotedPriceTableMgr;
	}
	
	/**
	 * 获取产品分类
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrentProductCategory() 
	throws Exception 
	{
		try
		{
			DBRow row=new DBRow();
			row.add("product_storage_catalog",0);
			return (floorQuotedPriceTableMgr.getProductCategoryByParentId(row));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getProductCategory",log);
			
		}
	}
	/**
	 * 根据ID获取产品类别
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCategoryById(long id)
	throws Exception
	{
		try
		{
			DBRow row=new DBRow();
			row.add("id",id);
			return (floorQuotedPriceTableMgr.getProductCategoryByParentId(row));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getProductCategoryById",log);
		}
		
	}
	/**
	 * 获取产品线
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductLine()
	throws Exception
	{
		try
		{
			return (floorQuotedPriceTableMgr.getProductLine());
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getProductLine",log);
		}
	}
	/**
	 * 获取仓库父级
	 * @return
	 */
	public DBRow[] getParentProductStorageCatalogByParentId()
	throws Exception
	{
		try
		{
			DBRow para=new DBRow();
			para.add("parentid",0);
			return (floorQuotedPriceTableMgr.getProductStorageCatalogByParentid(para));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getPrentProductStorageCatalogByParentId",log);
		}
	}
	/**
	 * 获取父级下的子级仓库
	 * @param parentId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorageCatalogChildrenByParentId(long parentId)
	throws Exception
	{
		try
		{
			DBRow row=new DBRow();
			row.add("parentid", parentId);
			return (floorQuotedPriceTableMgr.getProductStorageCatalogByParentid(row));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getProductStorageCatalogByParentId",log);
		}
	}
	/**
	 * 根据产品线,收货地,发货仓库,打折率生成报价表
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public List getQuoPriceByProductLine(HttpServletRequest request,boolean flag)
	throws Exception
	{
		try
		{			
			long productLine = StringUtil.getLong(request, "productLineId",1l);
			double discountRate = StringUtil.getDouble(request, "discount_rate");			
			int p = StringUtil.getInt(request, "p",1);
						
			//得到所有的翻页当前页码
			String pValues[] = request.getParameterValues("pValue");
			//根据区域ID得到
			DBRow detail = productMgr.getDetailCountryAreaByCaId(StringUtil.getLong(request,"ca_id"));
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			List list = new ArrayList();
			List productQuted = new ArrayList();
			DBRow row[] = floorQuotedPriceTableMgr.getProductCatalogByProductLine(productLine);//获取该产品线的相关类别
			HashMap map=new HashMap();
			
			PageCtrl pc1=null;
			for(DBRow data:row)
			{
				 DBRow dataRow[]=null;
				 pc1 = new PageCtrl();
				 pc1.setPageSize(20);
				 long pcid = data.get("id", 0l);
				 String catalogName = tree.getAllFather(pcid)[0].getString("title");
				 				 
				 if(pValues!=null)
				 {
					 for(String pV:pValues){
						 String pValue[] = pV.split(",");
						 if(pValue[0].equals(catalogName))
						 {
							 pc1.setPageNo(Integer.parseInt(pValue[1]));
							 dataRow = createQuoteList(productMgr.getProductByPcid(pcid, 0, pc1),detail.get("sig_country", 0l),discountRate,list);//根据产品类别ID获取相关产品信息
							 map.put(catalogName, dataRow);
							 String mapKey = catalogName+"1";
							 map.put(mapKey, pc1);
						 }						
					 }
				 }
				 else
				 {
					 pc1.setPageNo(p);
					 DBRow productdata[] =null;
					 if(flag)
					 {
						 productdata = productMgr.getProductByPcid(pcid, 0, null);
					 }
					 else
					 {
						 productdata = productMgr.getProductByPcid(pcid, 0, pc1);
					 }
					 dataRow = createQuoteList(productdata,detail.get("sig_country", 0l),discountRate,list);//根据产品类别ID获取相关产品信息
					 map.put(catalogName, dataRow);
					 String mapKey = catalogName+"1";
					 map.put(mapKey, pc1);					
				 }
			}
			productQuted.add(0, map);
			productQuted.add(1,list);
			
			return productQuted;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getQuoPriceByProductLine",log);
		}
	}
			
	/**
	 * 根据产品名称,收货地,发货仓库,打折率生成报价表
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public List getQuoPriceByProductName(HttpServletRequest request)
	throws Exception
	{
		PageCtrl pc1=new PageCtrl();
		try
		{
			//根据区域ID得到
			DBRow detail = productMgr.getDetailCountryAreaByCaId(StringUtil.getLong(request,"ca_id"));	
			String productName = StringUtil.getString(request, "productName");//产品名称
			double discountRate = StringUtil.getDouble(request, "discount_rate");//折扣
			int p = StringUtil.getInt(request, "p");
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			HashMap map = new HashMap();
			List list = new ArrayList();
			List productQuted = new ArrayList();
			DBRow dataRow [] =null;
			pc1.setPageSize(20);
			pc1.setPageNo(p);
			DBRow product[] = floorQuotedPriceTableMgr.getQuotedPrice(productName, pc1);
			long pcid = product[0].get("catalog_id",0l);
			String catalogName = tree.getAllFather(pcid)[0].getString("title");
		
			dataRow = createQuoteList(product, detail.get("sig_country", 0l), discountRate,list);
			map.put(catalogName, dataRow);
			String mapKey = catalogName+"1";
			map.put(mapKey, pc1);			
			productQuted.add(map);
			productQuted.add(list);
						
			return productQuted;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getQuoPriceByProductName",log);
		}
	}
	/**
	 * 根据产品类型,收货地,发货仓库,打折率获得产品报价
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public List getQuotedPriceByProductCatelog(HttpServletRequest request,boolean flag)
	throws Exception
	{
		try
		{
			int p = StringUtil.getInt(request, "p");
			double discountRate = StringUtil.getDouble(request, "discount_rate");//折扣
			long productCatalogId = StringUtil.getLong(request, "filter_acid");
			HashMap map = new HashMap();
			List list = new ArrayList();
			List productQuted = new ArrayList();
			DBRow product[] = null;
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			PageCtrl pc1 = new PageCtrl();
			pc1.setPageSize(20);
			pc1.setPageNo(p);
			String catalogName = tree.getAllFather(productCatalogId)[0].getString("title");
			
			if(flag)
				product = productMgr.getProductByPcid(productCatalogId, 0, null);
			else
				product = productMgr.getProductByPcid(productCatalogId, 0, pc1);
			DBRow detail = productMgr.getDetailCountryAreaByCaId(StringUtil.getLong(request,"ca_id"));
			
			DBRow quoted[] = createQuoteList(product, detail.get("sig_country", 0l), discountRate,list);
			map.put(catalogName, quoted);
			String key = catalogName+"1";
			map.put(key, pc1);
			productQuted.add(map);
			productQuted.add(list);
			return productQuted;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getQuotedPriceByProductCatelog",log);
		}
	}
	/**
	 * 根据Name获取优惠策略
	 * @return
	 * @throws Exception
	 */
	public DBRow getWholesellDiscountByName()
	throws Exception
	{
		try
		{
			return quoteMgr.getDetailWholeSellDiscountByName("GiveDiscountBySingleOrderGross");
		}
		catch(Exception e)
		{
			throw  new SystemException(e,"getWholesellDiscountByName",log);
		}
		
	}

	/**
	 * 生成报价表
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] createQuoteList(DBRow product[],long countryId,double discountRate,List catalogList)
	throws Exception
	{
		try
		{
			
			DBRow wholeSellDiscount = getWholesellDiscountByName();
			double currency = systemConfig.getDoubleConfigValue("USD");//第一个参数是商品ID第二个是国家ID第三个是折扣
			String[][] rule = quoteMgr.getWholeSellRule2(wholeSellDiscount.getString("discount_policy"));
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			//获取产品的类别名称
			List list = new ArrayList();
			if(discountRate==0)
			{
				discountRate=1.0;
			}
			for(int i=0;i<product.length;i++)
			{
				
				String catalogName = tree.getAllFather(product[i].get("catalog_id",0l))[0].getString("title");
				
			       for(int j=0;j<rule.length;j++)
			       {
			    	   String[] ruleDetail=rule[j];
			    	   
			    	   if(ruleDetail[3]!=null)
			    	   {
			    		   if(!list.contains(ruleDetail[3]))
			    		   {
			    			   list.add(ruleDetail[3]);
			    		   }
			    		   if(catalogName.equals(ruleDetail[3]))
			    		   {
			    			   double money = MoneyUtil.round(MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(product[i].getString("pc_id")),countryId,Double.parseDouble(ruleDetail[0])*discountRate), currency),2);
			    			   
			    			   product[i].add("unit_price", MoneyUtil.round(MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(product[i].getString("pc_id")),countryId,1.0), currency),2));
			    			   product[i].add("discount_policy"+j, money);
			    		   }
			    	   }
			       }
			      
			    	   for(int j=0;j<rule.length;j++)
				       {
				    	   String[] ruleDetail=rule[j];
				    	   if(ruleDetail[3]==null||ruleDetail[3]=="")
				    	   {
				    		   if(!list.contains(catalogName)){
					    		   double money = MoneyUtil.round(MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(product[i].getString("pc_id")),countryId,Double.parseDouble(ruleDetail[0])*discountRate), currency),2);
					    		   
					    		   product[i].add("unit_price", MoneyUtil.round(MoneyUtil.div(quoteMgr.getLowestProductPrice(StringUtil.getLong(product[i].getString("pc_id")),countryId,1.0), currency),2));
					    		   product[i].add("discount_policy"+j, money);
				    		   }
				    	   }
				       }
			    	   
			       
			       product[i].add("title",catalogName);
    			   removeDuplicate(catalogList, catalogName);
			}
			
			
			return product;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"createQuoteList",log);
		}
	}

	/**
	 * 根据发货快递、区域、货物重量，重量步长计算运费
	 * @param sc_id
	 * @param caId
	 * @param startWT
	 * @param endWT
	 * @param stepWT
	 * @return
	 * @throws Exception
	 */
	public List createTransportCost(long sc_id,long caId,float startWT,float endWT,float stepWT)
	throws Exception
	{
		float i=0f;
		ShippingInfoBean shippingInfoBean=null;
		List shippingInfos = null;
		double currency = systemConfig.getDoubleConfigValue("USD");
		try
		{
			DBRow detail = productMgr.getDetailCountryAreaByCaId(caId);	
			shippingInfos = new ArrayList();
          	for(i=startWT;i<=endWT;i+=stepWT)
			{
          		
				shippingInfoBean = expressMgr.getShippingFee(sc_id, i, detail.get("sig_country",0l), detail.get("sig_state",0l));
				if(shippingInfoBean!=null)
				{
					shippingInfoBean.setShippingFee(MoneyUtil.round(MoneyUtil.div(shippingInfoBean.getShippingFee(),currency),2));
					shippingInfos.add(shippingInfoBean);
				}
				
			}
			//如果for循环里的最终重量步长小于最终重量，那就需要将最终重量的运费计算一次
			if(i>endWT&&i-stepWT!=endWT)
			{
				shippingInfoBean = expressMgr.getShippingFee(sc_id, endWT, detail.get("sig_country",0l), detail.get("sig_state",0l));
				shippingInfos.add(shippingInfoBean);
			}
			
			return shippingInfos;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"createTransportCost",log);
		}
	}
	/**
	 * 去除重复的数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public List removeDuplicate(List list,String catalog)
	throws Exception
	{
		try
		{
		     if(!list.contains(catalog))
		     {
		    	 list.add(catalog);
		     }
			return list;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"removeDuplicate",log);
		}
	}
	
/**
 * 设置列名，组装报表
 * @param request
 * @throws Exception
 */
	public String createQuotedPDF(HttpServletRequest request) 
	throws Exception 
	{
		try{
			DBRow wholeSellDiscount = getWholesellDiscountByName();
			String[][] rule = quoteMgr.getWholeSellRule2(wholeSellDiscount.getString("discount_policy"));
			
			List<String> amounts = new ArrayList<String>();
			for(int j = 0;j<rule.length;j++)
	 		{
				String ruleDetail[] = rule[j];
				String title = "$"+ruleDetail[1]+"~$"+ruleDetail[2];
				if(!amounts.contains(title))
				{
					amounts.add(title);
				} 		
	 		}
			String[] columns = new String[3+amounts.size()];
			columns[0]="产品名称";
			columns[1]="单位重量";
			columns[2]="产品零售价";
			for(int i = 0 ;i<amounts.size();i++)
			{
				columns[i+3] = amounts.get(i).toString();
				
			}
			
			List list = this.processDataList(request,columns.length,amounts);		
			return  preview(request,columns, list); 
		}
		catch(Exception e)
		{
			throw new SystemException(e,"main",log);
		}
	}
	/**
	 * 处理Detail数据
	 * @param productData
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public DBRow processData(DBRow productData,List<String> list)
		throws Exception
	{
		try
		{
			DBRow wholeSellDiscount = getWholesellDiscountByName();
			String[][] rule = quoteMgr.getWholeSellRule2(wholeSellDiscount.getString("discount_policy"));			
			DBRow product = new DBRow();
			product.add("p_name", productData.getString("p_name"));
			product.add("weight", productData.get("weight", 0f)+"Kg");
			product.add("unit_price", "$"+productData.getString("unit_price"));
				for(int i = 0;i<rule.length;i++)
		 		{
					String ruleDetail[] = rule[i];
					String title = "$"+ruleDetail[1]+"~$"+ruleDetail[2];
					String amount = productData.get("discount_policy"+i,"0");
					if(!"0".equals(amount))
					{
						for(int k=0;k<list.size();k++)
						{
							if(title.equals(list.get(k).toString()))
							{
								String key = "discount_policy"+k;
								product.add(key, "$"+amount);
							}
						}
					}
		 		}
			product.add("title", productData.getString("title"));
			return product;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"processData",log);
		}
		
	}
	/**
	 * 处理Detail将要展示的数据
	 * @param request
	 * @param length
	 * @param catalogList
	 * @return
	 * @throws Exception
	 */
	private List processDataList(HttpServletRequest request,int length,List<String> catalogList)
	throws Exception
	{
		
		List QuotedList = null;
		Map quotedMap = new HashMap();
		List<String> catalogs = new ArrayList<String>();
		try{
			String cmd = StringUtil.getString(request, "cmd");
			//获取报表数据
			if("productLine".equals(cmd))
			{
				QuotedList= this.getQuoPriceByProductLine(request,true);
			}
			else if("productCatalog".equals(cmd))
			{
				QuotedList = this.getQuotedPriceByProductCatelog(request,true);
				
			}else if("productName".equals(cmd))
			{
				QuotedList = this.getQuoPriceByProductName(request);
			}
			
			quotedMap = (HashMap)QuotedList.get(0);
			catalogs = (List)QuotedList.get(1);
			
			List<String[]> list = new ArrayList<String[]>();
			
				for(String type:catalogs)
				{			
					DBRow[] productQuoteItems = (DBRow[])quotedMap.get(type);
					for(int j=0;j<productQuoteItems.length;j++)
					{
						String temp[] = new String[length+1];
						
						DBRow productData = processData(productQuoteItems[j],catalogList);
						for(int k = 0;k<length+1;k++)
						{
							if(k==0)
							{
								temp[0] = productData.getString("p_name");
							}
							else if(k==1)
							{
								temp[1] = productData.getString("weight", "0.0");
							}
							else if(k==2)
							{
								temp[2] = productData.getString("unit_price");
							}
							if(k>2&&k!=length)
							{
								temp[k] = productData.getString("discount_policy"+(k-3),"  ");
							}
							if(k==length)
							{
								temp[k] = productData.getString("title");
							}
							
						}
						list.add(temp);
					}
				}
					
			
					 
	
			 return list;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"processDataList",log);
		}
	}
	
	/**================================1
	 * PDF打印
	 */
	public  String preview(HttpServletRequest request,String headers[], List<Object[]> list) 
	throws Exception 
	{
		FileOutputStream fos = null;
		try
		{
			String quotationPDFTmpFolder = "quotation_pdf_tmp";//存放报表附件的临时文件夹
			String quotationPDFPath;//PDF文件完整路径
			
			//先生成报价单PDF附件文件到临时目录
			File tmpFolder = new File(Environment.getHome()+quotationPDFTmpFolder);
			if (!tmpFolder.exists())//不存在则创建
			{
				tmpFolder.mkdir();
			}
		    String date = DateUtil.FormatDatetime("yyyyMMddHHmmss");
			quotationPDFPath = Environment.getHome()+quotationPDFTmpFolder+"/VisionariProductQuote"+date+".pdf";
//			//system.out.println("_____:"+quotationPDFPath);
			//如果之前已经生成PDF，则先删除
			File pdfFile = new File(quotationPDFPath);
			if (pdfFile.exists())
			{
				pdfFile.delete();
			}
			// 处理columcHeaders
			String[] alias = preaseAliasColumnHeaders(headers);
			
			JasperReport jp = getJasperReport(request,headers, alias);
			Map<String, Object> parameters = new HashMap<String, Object>();
			JasperPrint jasperPrint = JasperFillManager
					.fillReport(jp, parameters, new JRBeanCollectionDataSource(
							getBaseList(alias, phrase(list))));
			
			byte[] bReport = JasperExportManager.exportReportToPdf(jasperPrint);
			fos = new FileOutputStream(quotationPDFPath,true); 
			fos.write(bReport);   
			fos.flush();
			return quotationPDFPath;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"preview",log);
		}
		finally
		{
			try
			{
				fos.close();
			}
			catch (Exception e)
			{
				throw new SystemException(e,"preview",log);
			}
			
		}
	}

	/**
	 * headers[]如果有中文就显示为乱码，利用别名代替
	 */
	private  String[] preaseAliasColumnHeaders(String headers[]) 
	throws Exception
	{
		try{
			int size = headers.length;
			String[] alias = new String[size];
			for (int i = 0; i < size; i++) {
				alias[i] = "column" + i;
			}// aliasColumn = "column"
			return alias;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"preaseAliasColumnHeaders",log);
		}
	}	
	
	/**========================================3
	 * 将各版块加入报表设计器进行编译
	 */
	private  JasperReport getJasperReport(HttpServletRequest request,String[] headers,
			String alias[]) throws Exception {
		JasperDesign design = prepareJasperReport();
		try{
			int reportWidth = 535;
//			design = setTitle(design);
			setColumnHeader(request,reportWidth,headers, design);
			setJRDesignGroup(alias,design);
			setDetail(reportWidth,alias, design);
			setColumnFooter(reportWidth,design);
			return JasperCompileManager.compileReport(design);
		}
		catch(JRException e)
		{
			throw new SystemException(e,"getJasperReport",log);
		}
	}
	
	/**
	 * 设置Detail数据
	 */
	private  List<String[]> phrase(List<Object[]> list) 
	throws Exception
	{
		try{
			List<String[]> temps = new ArrayList<String[]>();
			String[] s = null;
			for (Object[] obj : list) {
				s = new String[obj.length];
				for (int i = 0; i < obj.length; i++) {
					s[i] = obj[i].toString(); 
				}
				temps.add(s); 
			}
			return temps;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"phrase",log);
		}
	}
	
	/**
	 * 设置报表规格
	 */
	private  JasperDesign prepareJasperReport() 
	throws Exception
	{
		try
		{
			JasperDesign design = new JasperDesign(); 
			design.setName("statistics");  
			design.setPrintOrder(JRReport.PRINT_ORDER_VERTICAL); 
			design.setOrientation(JRReport.ORIENTATION_PORTRAIT); 
			design.setPageWidth(595); 
			design.setPageHeight(842); 
			design.setColumnWidth(535); 
			design.setColumnSpacing(0); 
			design.setLeftMargin(30); 
			design.setRightMargin(30); 
			design.setTopMargin(20); 
			design.setBottomMargin(20); 
			design.setWhenNoDataType(JRReport.WHEN_NO_DATA_TYPE_BLANK_PAGE); 
			design.setTitleNewPage(false); 
			design.setSummaryNewPage(false);
	
			return design;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"prepareJasperReport",log);
		}
	}
	
	/**
	 * 设置行头
	 * @param headers
	 * @param design
	 */
	private  void setColumnHeader(HttpServletRequest request,int reportWidth,String[] headers, JasperDesign design) 
	throws Exception
	{
		JRDesignBand columnHeader = new JRDesignBand();
		reportWidth=reportWidth-90;
		JRDesignStaticText titleText = new JRDesignStaticText();
		JRDesignStaticText titleText1 = new JRDesignStaticText();
		JRDesignStaticText caNameText = new JRDesignStaticText();
		JRDesignStaticText userNameText = new JRDesignStaticText();
		JRDesignStaticText caNameText1 = new JRDesignStaticText();
		JRDesignStaticText userNameText1 = new JRDesignStaticText();
		try{
			columnHeader.setHeight(71);
			String caName = StringUtil.getString(request, "ca_name");
			String userName = StringUtil.getString(request, "userName");
			
			//报价人 
			userNameText.setText("报   价  人:");
			userNameText.setWidth(30);
			userNameText.setHeight(9);
			userNameText.setFontName("Arial");		
			userNameText.setFontSize(6);
			userNameText.setX(reportWidth);
			userNameText.setY(36);
			userNameText.setPdfFontName("STSong-Light");
			userNameText.setPdfEmbedded(true);
			userNameText.setPdfEncoding("UniGB-UCS2-H");
			userNameText.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			userNameText.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);

			userNameText1.setText(userName);
			userNameText1.setWidth(60);
			userNameText1.setHeight(9);
			userNameText1.setFontName("Arial");		
			userNameText1.setFontSize(6);
			userNameText1.setX(reportWidth+30);
			userNameText1.setY(36);			
			userNameText1.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			userNameText1.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);
			//销售区域
			caNameText.setText("销售区域:");
			caNameText.setWidth(30);
			caNameText.setHeight(9);
			caNameText.setFontName("Arial");		
			caNameText.setFontSize(6);
			caNameText.setX(reportWidth);
			caNameText.setY(45);
			caNameText.setPdfFontName("STSong-Light");
			caNameText.setPdfEmbedded(true);
			caNameText.setPdfEncoding("UniGB-UCS2-H");
			caNameText.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			caNameText.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_TOP);
			
			caNameText1.setText(caName);
			caNameText1.setWidth(60);
			caNameText1.setHeight(9);
			caNameText1.setFontName("Arial");		
			caNameText1.setFontSize(6);
			caNameText1.setX(reportWidth+30);
			caNameText1.setY(45);
			caNameText1.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			caNameText1.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_TOP);
			//标题
			titleText.setWidth(30);
			titleText.setHeight(58);
			titleText.setFontName("Arial");
			titleText.setText("V");
			titleText.setFontSize(40);
			titleText.setTopPadding(12);
			titleText.setX(0);
			titleText.setY(0);
			titleText1.setText("isionari Quotation");
			titleText1.setWidth(300);
			titleText1.setHeight(58);
			titleText1.setFontName("Arial");
			titleText1.setFontSize(26);
			titleText1.setTopPadding(26);
			titleText1.setX(25);			
			columnHeader.addElement(0,titleText);
			columnHeader.addElement(1,titleText1);
			
			
			
			//列名，行头
			for (int i = 0; i < headers.length; i++) {
				// add column headers
				JRDesignRectangle rectangle = new JRDesignRectangle();
				JRDesignStaticText Text = new JRDesignStaticText();
				if(i==0)
				{
					rectangle.setHeight(13);
					rectangle.setWidth(90);
					rectangle.setX(i); 	
					rectangle.setY(58); 		
					rectangle.setBackcolor(Color.GRAY);
				}
				else
				{				
					rectangle.setHeight(13);
					rectangle.setWidth(reportWidth/(headers.length-1));
					rectangle.setX(reportWidth/(headers.length-1) * (i-1)+90); 		
					rectangle.setBackcolor(Color.GRAY);
					rectangle.setY(58); 
				}
				columnHeader.addElement(rectangle);
				
				if(i==0)
				{			
					Text.setText(headers[i]);
					Text.setFontSize(6);
					Text.setHeight(13);
					Text.setWidth(90);
					Text.setX(i); 
					Text.setY(58); 
					Text.setForecolor(Color.WHITE);
					Text.setFontName("Arial");
					Text.setPdfFontName("STSong-Light");
					Text.setPdfEmbedded(true);
					Text.setPdfEncoding("UniGB-UCS2-H");
					Text.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_CENTER);
					Text.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);
				}
				else
				{
					
					Text.setText(headers[i]);
					Text.setFontSize(6);
					Text.setHeight(13);
					Text.setFontName("Arial");
					Text.setForecolor(Color.WHITE);
					Text.setWidth(reportWidth/(headers.length-1));
					Text.setX(reportWidth/(headers.length-1) * (i-1)+90); 
					Text.setY(58); 
					Text.setPdfFontName("STSong-Light");
					Text.setPdfEmbedded(true);
					Text.setPdfEncoding("UniGB-UCS2-H");
					Text.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_CENTER);
					Text.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);
					
				}
				columnHeader.addElement(Text);
			}
			columnHeader.addElement(userNameText);
			columnHeader.addElement(caNameText);
			columnHeader.addElement(userNameText1);
			columnHeader.addElement(caNameText1);
			design.setColumnHeader(columnHeader);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"setColumnHeader",log);
		}
	}
	/**
	 * 设置分组
	 * @param type
	 * @param design
	 * @throws Exception
	 */
	private void setJRDesignGroup(String heads[],JasperDesign design)
	throws Exception
	{
		
		JRDesignGroup  group = new JRDesignGroup();
		JRDesignBand groupDetail = new JRDesignBand();
		JRDesignRectangle rectangle = new JRDesignRectangle();
		JRDesignTextField Text = new JRDesignTextField();
		try 
		{
			
			JRDesignField field = new JRDesignField();
			field.setName("column");
			field.setValueClass(String.class);
			design.addField(field);
			
			groupDetail.setHeight(13);
			
			rectangle.setHeight(13);
			rectangle.setWidth(90);
			rectangle.setX(0); 		
			rectangle.setBackcolor(Color.GRAY);
			
			JRDesignExpression expression = new JRDesignExpression();
			expression.setText("$F{column}");
			expression.setValueClass(String.class);
			Text.setExpression(expression);
			Text.setFontSize(6);
			Text.setForecolor(Color.WHITE);
			Text.setHeight(13);
			Text.setWidth(90);
			Text.setX(0); 
			Text.setPdfFontName("STSong-Light");
			Text.setPdfEmbedded(true);
			Text.setPdfEncoding("UniGB-UCS2-H");
			Text.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			Text.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);		
			
			groupDetail.addElement(rectangle);
			groupDetail.addElement(Text);
			
			group.setName("types");
			group.setExpression(expression);
			group.setGroupHeader(groupDetail);
			design.addGroup(group);
			
		} catch (JRException e) {
			
			throw  new SystemException(e,"setJRDesignGroup",log);
		}
		
	}
	/**
	 * 设置Detial 
	 */
	private  void setDetail(int reportWidth,String[] alias, JasperDesign design)
			throws Exception {
		
		reportWidth = reportWidth-90;
		// define fields
		JRDesignBand detail = new JRDesignBand();
		try{
			detail.setHeight(13);
			for (int i = 0; i < alias.length; i++) {
				// define fields
				
					JRDesignField field = new JRDesignField();
					field.setName(alias[i]);
					field.setValueClass(String.class);
					design.addField(field);
				
				// add text fields for displaying fields
				JRDesignTextField textField = new JRDesignTextField();
				
				if(i==0)
				{
					JRDesignExpression expression = new JRDesignExpression();
					expression.setText("$F{" + alias[i] + "}");
					expression.setValueClass(String.class);
					textField.setExpression(expression);
					textField.setHeight(13);
					textField.setFontName("Arial");
					textField.setFontSize(6);
					textField.setWidth(90);
					textField.setX(i);
					textField.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT); 
					textField.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);
				}else
				{
					JRDesignExpression expression = new JRDesignExpression();
					expression.setText("$F{" + alias[i] + "}");
					expression.setValueClass(String.class);
					textField.setExpression(expression);
					textField.setFontName("Arial");
					textField.setFontSize(6);
					textField.setHeight(13);
					textField.setWidth(reportWidth/(alias.length-1));
					textField.setX(reportWidth/(alias.length-1)*(i-1)+90);
					textField.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_CENTER);
					textField.setVerticalAlignment(JRBasePrintText.VERTICAL_ALIGN_MIDDLE);
				}
			    if(i==alias.length-1){
			    	this.setJRDesignTextFieldBorder(textField, true, true, true, true);
			    }else{
			    	this.setJRDesignTextFieldBorder(textField, true, true, false, true);
			    }
			  
				detail.addElement(textField);
			}
			design.setDetail(detail);
			}
		catch(Exception e)
		{
			throw new SystemException(e,"setDetail",log);
		}
	}
	
	private void setJRDesignTextFieldBorder(JRDesignTextField textField,boolean leftBorder,boolean topBorder,boolean rightBorder,boolean bottomBorder)
	throws Exception
	{
		try{
			if(leftBorder){
				textField.setLeftBorder(JRBaseLine.PEN_THIN);
			}else{
				textField.setLeftBorder(JRBaseLine.PEN_NONE);
			}
			
			if(topBorder){
				textField.setTopBorder(JRBaseLine.PEN_THIN);
			}else{
				textField.setTopBorder(JRBaseLine.PEN_NONE);
			}
			
			if(rightBorder){
				textField.setRightBorder(JRBaseLine.PEN_THIN);
			}else{
				textField.setRightBorder(JRBaseLine.PEN_NONE);
			}
			
			if(bottomBorder){
				textField.setBottomBorder(JRBaseLine.PEN_THIN);
			}else{
				textField.setBottomBorder(JRBaseLine.PEN_NONE);
			}
		}
		catch(Exception e)
		{
			throw  new SystemException(e,"setJRDesignTextFieldBorder",log);
		}
	}
	/**
	 * 设置分页
	 * @param design
	 * @throws Exception
	 */
	public void setColumnFooter(int reportWidth,JasperDesign design)
	throws Exception
	{
		JRDesignBand band = new JRDesignBand();
		JRDesignExpression expression = new JRDesignExpression();
		JRDesignExpression expression1 = new JRDesignExpression();
		JRDesignTextField Text = new JRDesignTextField();
		JRDesignTextField Text1 = new JRDesignTextField();
		try
		{
			band.setHeight(13);
			expression.setText("'第'+$V{PAGE_NUMBER}.toString()");
			expression.setValueClass(String.class);
			
			expression1.setText("'/'+$V{PAGE_NUMBER}.toString()+'页'");
			expression1.setValueClass(String.class);
			Text.setExpression(expression);
			Text.setFontName("Arial");
			Text.setFontSize(6);
			Text.setHeight(13);
			Text.setPdfFontName("STSong-Light");
			Text.setPdfEmbedded(true);
			Text.setPdfEncoding("UniGB-UCS2-H");
			Text.setX(reportWidth-40);
			Text.setWidth(20);
			Text.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_RIGHT);
			
			Text1.setExpression(expression1);
			Text1.setFontName("Arial");
			Text1.setFontSize(6);
			Text1.setHeight(13);
			Text1.setPdfFontName("STSong-Light");
			Text1.setPdfEmbedded(true);
			Text1.setPdfEncoding("UniGB-UCS2-H");
			Text1.setX(reportWidth-20);
			Text1.setWidth(20);
			Text1.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			Text1.setEvaluationTime(EvaluationTimeEnum.REPORT);
			//报表生成日期
			JRDesignStaticText staticText = new JRDesignStaticText();
			staticText.setText("报价日期："+DateUtil.NowStr());
			staticText.setX(0);
			staticText.setFontName("Arial");
			staticText.setFontSize(6);
			staticText.setHeight(13);
			staticText.setWidth(120);
			staticText.setPdfFontName("STSong-Light");
			staticText.setPdfEmbedded(true);
			staticText.setPdfEncoding("UniGB-UCS2-H");
			staticText.setTextAlignment(JRBasePrintText.HORIZONTAL_ALIGN_LEFT);
			band.addElement(staticText);
			band.addElement(Text);
			band.addElement(Text1);
			design.setColumnFooter(band);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"setColumnFooter",log);
		}
	}

	public void setJRDesignStaticTextBorder(JRDesignStaticText Text,boolean leftBorder,boolean topBorder,boolean rightBorder,boolean bottomBorder){
		if(leftBorder){
			Text.setLeftBorder(JRBaseLine.PEN_THIN);
		}else{
			Text.setLeftBorder(JRBaseLine.PEN_NONE);
		}
		
		if(topBorder){
			Text.setTopBorder(JRBaseLine.PEN_THIN);
		}else{
			Text.setTopBorder(JRBaseLine.PEN_NONE);
		}
		
		if(rightBorder){
			Text.setRightBorder(JRBaseLine.PEN_THIN);
		}else{
			Text.setRightBorder(JRBaseLine.PEN_NONE);
		}
		
		if(bottomBorder){
			Text.setBottomBorder(JRBaseLine.PEN_THIN);
		}else{
			Text.setBottomBorder(JRBaseLine.PEN_NONE);
		}
	}

	/**
	 * 利用反射机制，获得数据
	 */
	private  List<Object> getBaseList(String[] headers,
			List<String[]> list) throws Exception {
		List<Object> result = new ArrayList<Object>();
		int length = headers.length+1;
		DynaProperty[] dynaProps = new DynaProperty[length];
		for (int i = 0; i < length; i++) {
			if(i==length-1)
			{
				dynaProps[i] = new DynaProperty("column", String.class);
			}else
			{
				dynaProps[i] = new DynaProperty(headers[i], String.class);
			}
		}
		BasicDynaClass dynaClass = new BasicDynaClass("first",BasicDynaBean.class, dynaProps);
		for (Object[] obj : list) {
			DynaBean employee = dynaClass.newInstance();
			
			
				for (int i = 0; i < length; i++) {
					
					if(i==length-1)
					{
						employee.set("column", obj[length-1]);
					}
					else
					{
						employee.set(headers[i], obj[i]);
					}
				}
			result.add(employee);
		}		 
		return result;
	}
	
	public void downLoadPDF(HttpServletResponse response,String filedownload)
	throws Exception
	{
		
		InputStream input = null;
		OutputStream outPut = null;
		try
		{
			File file = new File(filedownload);
			String fileName = file.getName();
			input = new BufferedInputStream(new FileInputStream(filedownload));
			byte[] buffer = new byte[input.available()];
			input.read(buffer);
			
			response.reset();
			response.addHeader("Content-Disposition","attachment;filename=" + fileName);
			response.setContentType("application/pdf");
			
			outPut = new BufferedOutputStream(response.getOutputStream());
			outPut.write(buffer);
			outPut.flush();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"downLoadPDF",log);
		}
		finally
		{
			if(outPut != null)
			{
				outPut.close();
				outPut = null;
			}
			
			if(input!=null)
			{
				input.close();
				input = null;
			}
		}
	}
}
