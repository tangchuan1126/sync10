package com.cwc.app.api.zzz;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zzz.FloorApplyFundsCategoryMgrZZZ;
import com.cwc.app.iface.zzz.ApplyFundsCategoryMgrIfaceZZZ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public  class ApplyFundsCategoryMgrZZZ implements ApplyFundsCategoryMgrIfaceZZZ
{
	static Logger log = Logger.getLogger("ACTION");

	private FloorApplyFundsCategoryMgrZZZ floorApplyFundsCategoryMgrZZZ;
	
	public void setFloorApplyFundsCategoryMgrZZZ(
			FloorApplyFundsCategoryMgrZZZ floorApplyFundsCategoryMgrZZZ) 
	{
		this.floorApplyFundsCategoryMgrZZZ = floorApplyFundsCategoryMgrZZZ;
	}
	/**
	 * 增加资金申请类型
	 * @param row
	 * @throws Exception
	 */
	public void addApplyFundsCategory(HttpServletRequest request) 
	throws Exception 
	{
		try
		{
			long prentId=StringUtil.getLong(request, "parentId");
			String categoryName=StringUtil.getString(request,"categoryName");
			int categoryStatus=StringUtil.getInt(request, "categoryStatus",0);
			DBRow row=new DBRow();
			row.add("prent_id", prentId);
			row.add("category_name",categoryName);
			row.add("category_status",categoryStatus);
			floorApplyFundsCategoryMgrZZZ.addApplyMoneyCategory(row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyFundsCategory",log);
		}
	}

	/**
	 * 获取所有的资金申请类型
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyMoneyCategory()
	throws Exception
	{
		try
		{
			return (floorApplyFundsCategoryMgrZZZ.getAllApplyMoneyCategory());
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAllApplyMoneyCategory",log);
		}	
	}
	/**
	 * 根据ID获取所有的资金申请类型
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getApplyMoneyCategoryByCategoryId(HttpServletRequest request)
	throws Exception
	{
		try
		{
			long id=StringUtil.getLong(request, "id");
			DBRow row=new DBRow();
			row.add("category_id", id);
			return (floorApplyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(row));
		}
		catch(Exception e)
		{
			throw  new SystemException(e,"getApplyMoneyCategoryByCategoryId",log);
		}
	}
	/**
	 * 根据ID删除资金申请类型
	 * @param request
	 * @throws Exception
	 */
	public void deleteApplyFundsCategoryByCategoryId(HttpServletRequest request)
	throws Exception
	{
		try
		{
			long id=StringUtil.getLong(request, "id");
			DBRow row=new DBRow();
			row.add("category_id",id);
			floorApplyFundsCategoryMgrZZZ.delApplyFoundsCategoryByCategoryId(row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"deleteApplyFundsCategoryByCategoryId",log);
		}
	}
	/**
	 * 根据ID修改资金申请类型
	 * @param request
	 * @throws Exception
	 */
	public void updateApplyFundsCategoryByCategoryId(HttpServletRequest request)
	throws Exception
	{
		try
		{
			long id=StringUtil.getLong(request, "id");			
			String categoryName=StringUtil.getString(request, "categoryName");
			int categoryStatus=StringUtil.getInt(request, "categoryStatus");
			if(categoryStatus!=1)
			{
				categoryStatus=0;
			}
			
			DBRow row=new DBRow();			
			row.add("category_name", categoryName);
			row.add("category_status", categoryStatus);
			
			floorApplyFundsCategoryMgrZZZ.updateApplyFoundsCategoryByCategoryId(id,row);
		}
		catch(Exception e)
		{
			
			throw new SystemException(e,"updateApplyFundsCategoryByCategoryId",log);
		}
		
	}
	public DBRow[] getApplyFundsCategoryChildren(long id)
	throws Exception
	{
		try
		{
			DBRow row=new DBRow();
			row.add("prent_id",id);
			return (floorApplyFundsCategoryMgrZZZ.getAllApplyMoneyCategoryChildren(row));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyFundsCategoryChildren",log);
		}
	}
}
