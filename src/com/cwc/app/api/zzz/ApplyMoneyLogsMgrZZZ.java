package com.cwc.app.api.zzz;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyLogsMgrZZZ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ApplyMoneyLogsMgrZZZ implements ApplyMoneyLogsMgrIFaceZZZ{

	static Logger log = Logger.getLogger("ACTION");
	private FloorApplyMoneyLogsMgrZZZ floorApplyMoneyLogsMgrZZZ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private AdminMgrIFace adminMgr;
	private TransportMgrIFaceZJ transportMgrZJ;
	
	
	/**
	 * 增加日志记录
	 * @param applyId
	 * @param name
	 * @param context
	 * @throws Exception
	 */
	public void addApplyMoneyLogs(long applyId,long userName_id, String name,String context)
	throws Exception 
	{
		try
		{
			DBRow row = new DBRow();
			row.add("applyid", applyId);
			row.add("userName_id", userName_id);
			row.add("userName", name);
			row.add("context",context);
			row.add("createDate", DateUtil.NowStr());
			floorApplyMoneyLogsMgrZZZ.addApplyMoneyLogs(row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoneyLogs(long applyId,String name,String context)",log);
		}
	}
	/**
	 * 根据资金申请ID查询相关日志
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public  DBRow[] getApplyMoneyLogsByApplyId(long id,PageCtrl pc)
	throws Exception
	{
		try
		{			
			return floorApplyMoneyLogsMgrZZZ.getApplyMoneyLogsByApplyId(id, pc);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyMoneyLogsByApplyId(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 添加日志及任务跟进
	 */
	@Override
	public void addApplyMoneyLogAndSchedule(HttpServletRequest request) throws Exception
	{
		try 
		{
			long apply_money_id			= StringUtil.getLong(request, "apply_money_id");
			int apply_money_status		= StringUtil.getInt(request, "apply_money_status");
			int association_type_id		= StringUtil.getInt(request, "association_type_id");
			int categoryId				= StringUtil.getInt(request, "categoryId");
			long association_id			= StringUtil.getLong(request, "association_id");
		    String context				= StringUtil.getString(request, "context");
		    HttpSession session 		= request.getSession();
		    int moduleKeyId				= 0;
		    int processKeyId			= 0;
		    long association_schedule_id = 0;
		    boolean isAddScheduleFollow	= false;
		    boolean isFinish = false;
		    String period	= "";
		    //采购单定金
		    if(4 == association_type_id && 100009 == categoryId)
		    {
		    	moduleKeyId		= ModuleKey.APPLY_MONEY;
		    	processKeyId	= ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS;
		    	association_schedule_id = apply_money_id;
		    	if(FundStatusKey.FINISH_PAYMENT == apply_money_status){
				    	isFinish = true;
				}
		    	isAddScheduleFollow = true;
		    }
		    //交货单货款
		    else if(5 == association_type_id && 100001 == categoryId)
		    {
		    	moduleKeyId		= ModuleKey.APPLY_MONEY;
		    	processKeyId	= ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS;
		    	association_schedule_id = apply_money_id;
		    	if(FundStatusKey.FINISH_PAYMENT == apply_money_status){
				    	isFinish = true;
				}
		    	isAddScheduleFollow = true;
		    }
		    //交货单运费
		    else if(5 == association_type_id && 10015 == categoryId)
		    {
		    	moduleKeyId				= ModuleKey.APPLY_MONEY;
		    	processKeyId			= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
		    	association_schedule_id = apply_money_id;
		    	DBRow transportRow		= transportMgrZJ.getDetailTransportById(association_id);
		    	int stock_in_set		= null==transportRow?0:transportRow.get("stock_in_set", 0);
		    	isFinish				= TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH==stock_in_set?true:false;
		    	period					= "transport_stock_in_set_period";
		    	isAddScheduleFollow		= true;
		    	
		    }
		    //转运单运费
		    else if(6 == association_type_id && 10015 == categoryId)
		    {
		    	moduleKeyId				= ModuleKey.APPLY_MONEY;
		    	processKeyId			= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
		    	association_schedule_id = apply_money_id;
		    	DBRow transportRow		= transportMgrZJ.getDetailTransportById(association_id);
		    	int stock_in_set		= null==transportRow?0:transportRow.get("stock_in_set", 0);
		    	isFinish				= TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH==stock_in_set?true:false;
		    	period					= "transport_stock_in_set_period";
		    	isAddScheduleFollow		= true;
		    }
		    
		    AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
		    this.addApplyMoneyLogs(apply_money_id,adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), context);
		    
		    if(isAddScheduleFollow)
		    {
		    	String contentPre = adminLoggerBean.getEmploye_name()+"确认了资金单:"+apply_money_id
		    	+",关联:"+ new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(association_type_id+"")+"["+association_id+"],";
		    	scheduleMgrZr.addScheduleReplayExternal(association_schedule_id , moduleKeyId,
		    			processKeyId , contentPre+context , isFinish, request, period);
		    }
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addApplyMoneyLogAndSchedule(HttpServletRequest request)",log);
		}
	}
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	public void setFloorApplyMoneyLogsMgrZZZ(
			FloorApplyMoneyLogsMgrZZZ floorApplyMoneyLogsMgrZZZ)
	{
		this.floorApplyMoneyLogsMgrZZZ = floorApplyMoneyLogsMgrZZZ;
	}
}