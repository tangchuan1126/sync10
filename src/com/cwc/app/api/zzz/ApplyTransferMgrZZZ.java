package com.cwc.app.api.zzz;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.ll.FloorApplyMoneyLogsMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyMoneyMgrLL;
import com.cwc.app.floor.api.ll.FloorPurchaseMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zyj.FloorRepairOrderMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.floor.api.zzz.FloorApplyTransferMgrZZZ;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.app.key.ApplyFundsTypesAndProcessKey;
import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.RepairStockInSetKey;
import com.cwc.app.key.TransferAccountsKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.lucene.zr.ApplyTransferIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class ApplyTransferMgrZZZ implements ApplyTransferMgrIFace 
{
	private static final String followup_content = null;
	static Logger log = Logger.getLogger("ACTION");
	private FloorApplyTransferMgrZZZ applyTransferMgrZZZ;
	private FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ;
	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	private FloorApplyMoneyMgrLL floorApplyMoneyMgrLL;
	private FloorPurchaseMgrLL floorPurchaseMgrLL;
	private FloorTransportMgrLL floorTransportMgrLL;
	private SystemConfig systemConfig;
	private FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL;
	private TransportMgrZyjIFace transportMgrZyj;
	private FloorPurchaseMgr fpm;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
		private AccountMgrIfaceZr accountMgrZr ;
	private FloorRepairOrderMgrZyj floorRepairOrderMgrZyj;
	private AdminMgr adminMgr = new AdminMgr();
	
	

	/**
	 * 增加一条转账申请记录
	 * @param request
	 * @throws AdminIsExistException
	 * @throws Exception
	 */
	public DBRow addApplyTransfer(HttpServletRequest request)
	throws  Exception 
	{
		try
		{
			long apply_money_id=StringUtil.getLong(request,"apply_money_id");
			String payer=StringUtil.getString(request, "payer");
			//String paymentInformation=StrUtil.getString(request,"payment_information");	
			String receiver=StringUtil.getString(request, "receiver");
			String receiver_information=StringUtil.getString(request,"receiver_information");
			double amount=StringUtil.getDouble(request,"amount");
			String status="0";
			String remark=StringUtil.getString(request, "remark");
			String currency=StringUtil.getString(request, "currency");
			String lastTime=StringUtil.getString(request,"lastTime");
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String userName = adminLoggerBean.getEmploye_name();
			
			DBRow row=new DBRow();
			row.add("last_time",lastTime);
			row.add("apply_money_id", apply_money_id);
			row.add("payer", payer);
			//row.add("payment_information", paymentInformation);	
			row.add("receiver", receiver);
			if(receiver_information !=null && receiver_information.length()>0){
			row.add("receiver_information", receiver_information);	
			}
			row.add("amount", amount);
			row.add("status",status);
			row.add("create_time", DateUtil.NowStr());
			row.add("remark", remark);
			row.add("creater", userName);
			row.add("currency", currency);
			if(currency.equals("RMB"))
			{
				row.add("standard_money", amount);
			}
			else
			{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue("USD")));
			}
			
			long transferId=applyTransferMgrZZZ.addApplyTransfer(row);
			accountMgrZr.addAccount(transferId,request);
			//申请转账后创建转账索引,同时修改资金申请的索引（修改资金的状态不涉及到索引，不必非修改完后执行）
			this.editTransferIndex(transferId,"add");
			
			DBRow applyMoneyRow = floorApplyMoneyMgrLL.getRowById(apply_money_id+"");
			
			if(applyMoneyRow.get("status", 0) == FundStatusKey.NO_APPLY_TRANSFER)
			{
				applyMoneyRow.add("status", FundStatusKey.HAD_APPLY_TRANSFER);
				floorApplyMoneyMgrZZZ.updateApplyMoneyById(apply_money_id, applyMoneyRow);
			}
			String transferResponsiblePersonNames = StringUtil.getString(request, "transferResponsiblePersonNames");
			String transferResponsiblePersonIds   = StringUtil.getString(request, "transferResponsiblePersonIds");
			DBRow returnValue=new DBRow();
			returnValue.add("transferId", transferId);
			returnValue.add("adid",adminLoggerBean.getAdid());
		    String mail=StringUtil.getString(request,"mail");
		    String pageMessage=StringUtil.getString(request,"pageMessage");
		    String shortMessage=StringUtil.getString(request,"shortMessage");
			lastTime+=" 23:59:59";
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String startTime = dateformat.format(date);
			String title= "转账申请:"+transferId;
			String content="所属资金单:"+apply_money_id+",\n付款人:"+payer+",收款人:"+receiver+"\n收款人信息:"+receiver_information+",转账负责人:"+transferResponsiblePersonNames;
			boolean ma=false;
			boolean pm=false;
			boolean sm=false;
			if(mail.equals("true")){
				ma=true;
			}
			if(pageMessage.equals("true")){
				pm=true;
			}
			if(shortMessage.equals("true")){
				sm=true;
			}		
			int association_type_id=StringUtil.getInt(request,"association_type_id");
			int types = StringUtil.getInt(request, "types");
			//添加转账任务
			//采购单定金
			if(4 == association_type_id && 100009 == types)
			{
				scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content, 
						pm, ma, sm, request, true, ProcessKey.PURCHASE_EXPECT_TRANSFER_MONEY);
			}
			//交货单货款
			else if(5 == association_type_id && 100001 == types)
			{
				scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content,
						pm, ma, sm, request, true, ProcessKey.TRANSPORT_EXPECT_TRANSFER_MONEY);
			}
			//交货单运费
		    else if(5 == association_type_id && 10015 == types)
		    {
		    	scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content,
						pm, ma, sm, request, true, ProcessKey.TRANSPORT_STOCK_TRANSFER_MONEY);
		    }
			//转运单运费
		    else if(6 == association_type_id && 10015 == types)
		    {
		    	scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content,
						pm, ma, sm, request, true, ProcessKey.TRANSPORT_STOCK_TRANSFER_MONEY);
		    }
			//返修单运费
		    else if(7 == association_type_id && 10015 == types)
		    {
		    	scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content,
						pm, ma, sm, request, true, ProcessKey.REPAIR_STOCK_APPLY_FUNDS);
		    }
		    else
		    {
		    	ApplyFundsTypesAndProcessKey applyFundsTypesAndProcessKey = new ApplyFundsTypesAndProcessKey();
		    	int applyProcessKeyId = applyFundsTypesAndProcessKey.getTypesAndProcessKeyById(types);
		    	scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), transferResponsiblePersonIds, "",
						transferId, ModuleKey.APPLY_TRANSFER_MONEY, title, content,
						pm, ma, sm, request, true, applyProcessKeyId);
		    }
			
			return returnValue;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyTransfer(request)",log);
		}

	}
    public DBRow[] getApplyTransferLastTimeSort(PageCtrl pc)throws Exception{
    	try{
    		return this.applyTransferMgrZZZ.getApplyTransferLastTimeSort(pc);
    	}catch(Exception e){
    		throw new SystemException(e,"getApplyTransferLastTimeSort(pc)",log);
    	}
    }
	public DBRow[] getApplyTransferEndTimeSort(PageCtrl pc)throws Exception{
		try{
    		return this.applyTransferMgrZZZ.getApplyTransferEndTimeSort(pc);
    	}catch(Exception e){
    		throw new SystemException(e,"getApplyTransferEndTimeSort(pc)",log);
    	}
	}
	
	/**
	 * 更新转账申请的最迟转款时间
	 * @param request
	 * @throws Exception
	 */
	@Override
	public void updateTransferLastTime(HttpServletRequest request)throws Exception
	{
		try 
		{
			long transfer_id = StringUtil.getLong(request, "transfer_apply_id");
			String last_time = StringUtil.getString(request, "last_time");
			DBRow row		 = new DBRow();
			row.add("last_time", last_time+" 23:59:59");
			this.updateApplyTransferMoney(transfer_id, row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateTransferLastTime(request)",log);
		}
	}
	
	/**
	 * 更新转账申请
	 * @param transfer_id
	 * @param transferRow
	 * @throws Exception
	 */
	@Override
	public void updateApplyTransferMoney(long transfer_id, DBRow transferRow)throws Exception
	{
		try
		{
			applyTransferMgrZZZ.updateApplyTransferById(transfer_id, transferRow);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"updateApplyTransferMoney(transfer_id,transferRow)",log);
		}
	}
	/**
	 * 获取所有的转账申请记录
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyTransfer(PageCtrl pc) 
	throws Exception 
	{
		try
		{
			return (applyTransferMgrZZZ.getAllApplyTransfer(pc));
		}catch(Exception e)
		{
			throw new SystemException(e,"getAllApplyTransfer(pc)",log);
		}
		
	}

    /**
     * 根据资金申请的ID查询转账申请的记录
     * @param id
     * @param pc
     * @return
     * @throws Exception
     */
	public DBRow[] getApplyTransferByApplyMoneyId(long id, PageCtrl pc)
	throws Exception 
	{
		try
		{
			return (applyTransferMgrZZZ.getApplyTransferByApplyMoneyId(id, pc));
			
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyTransferById(id,pc)",log);
		}
		
	}	
	/**
	 * 根据ID修改转账申请和资金申请的状态
	 * @param id
	 * @throws AdminIsExistException
	 * @throws Exception
	 */
	public long updateApplyTransferById(long id,String path,double money,String payment_information, String receiver_account, AdminLoginBean adminLoggerBean,HttpServletRequest request)
	throws Exception
	{
		try
		{
			long userId = adminLoggerBean.getAdid();
			String userName=adminLoggerBean.getEmploye_name();
			Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String endTime = dateformat.format(date);
			//修改申请转账表状态，金额，凭证的文件名和上传凭证的日期
			DBRow row[]=null;
			row=applyTransferMgrZZZ.getApplyTransferById(id);						
			row[0].remove("status");
			row[0].remove("amount");
			row[0].add("amount",money);
			row[0].add("payment_information",payment_information);
			row[0].add("receiver_account",receiver_account);
			row[0].add("status", TransferAccountsKey.FINISH_TRANSFER);
			if(row[0].getString("currency").equals("RMB")) {
				row[0].add("standard_money",money);
			}else {
				row[0].add("standard_money",money*Double.parseDouble(systemConfig.getStringConfigValue(row[0].getString("currency"))));
			}
			row[0].add("operating_personnel", userName);
			row[0].add("voucher",path);
			row[0].add("voucher_createTime",DateUtil.NowStr());
			applyTransferMgrZZZ.updateApplyTransferById(id, row[0]);
			
			//修改转账,修改索引
			this.editTransferIndex(id,"update");
			
			DBRow[] applyMoneyRow=null;
			long uid=row[0].get("apply_money_id",0l);
			//修改资金申请表的状态和已转金额
			if(uid!=0l)
			{
				applyMoneyRow=floorApplyMoneyMgrZZZ.getApplyMoneyById(uid);
				applyMoneyRow[0].remove("status");
				double total=0d;
				double amount=applyMoneyRow[0].get("amount", 0d);
				double standard_money=applyMoneyRow[0].get("standard_money", 0d);
				if(row[0].getString("currency").equals("RMB"))
					total=applyMoneyRow[0].get("amount_transfer", 0d)+money;
				else
					total=applyMoneyRow[0].get("amount_transfer", 0d)+money*Double.parseDouble(systemConfig.getStringConfigValue(row[0].getString("currency")));
				applyMoneyRow[0].remove("amount_transfer");
				String content1 = "";
				if(Math.abs(total-standard_money) <= Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01)//已完全转款
				{
					applyMoneyRow[0].add("amount_transfer", total);
					int[] applyStatus	= {TransferAccountsKey.NOT_TRANSFER};
					int applyStatusLen	= applyTransferMgrZZZ.getApplyTransferArrayByApplyMoneyId(applyMoneyRow[0].get("apply_id", 0L), applyStatus).length;
					if(applyStatusLen > 0){
						applyMoneyRow[0].add("status", FundStatusKey.PART_PAYMENT);
						content1 = "部分转账";
					}else{
						DBRow zwbrow=new DBRow();
						zwbrow.add("end_time", endTime);
						zwbrow.add("operating_personnel", userName);
						floorApplyMoneyMgrZZZ.updateApplyMoneyById(uid, zwbrow);
						applyMoneyRow[0].add("status", FundStatusKey.FINISH_PAYMENT);
						content1 = "全部转账";
					}
					if(applyMoneyRow[0].get("types", 0l)==10014||applyMoneyRow[0].get("types", 0l)==100003)
					{
						applyMoneyMgrZZZ.updateAssetsState(applyMoneyRow[0].get("association_id", 0l), 5);
					}
					
				}
				
				if(total<standard_money&&Math.abs(total-standard_money) > Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01&&total>0d)//已部分转款
				{
					applyMoneyRow[0].add("amount_transfer", total);
					applyMoneyRow[0].add("status", FundStatusKey.PART_PAYMENT);
					content1 = "部分转账";
				}
				
				floorApplyMoneyMgrZZZ.updateApplyMoneyById(uid, applyMoneyRow[0]);
				//当定金+货款-采购单金额在一定的误差范围内，发票流程进中
				this.handlePurchaseInvoiceByApplyMoneyTotal(request, applyMoneyRow[0].get("association_id", 0L), applyMoneyRow[0].get("association_type_id", 0), applyMoneyRow[0].get("types", 0), applyMoneyRow[0].get("amount_transfer",0.0), userId, userName);
				//处理资金和转账的任务
				this.handleApplyAndTransferMoenySchedule(applyMoneyRow[0], id, row[0].get("amount", 0.0), row[0].getString("currency"), userId, userName, 1, request);
			}
			return id;
		}
		catch(Exception e)
		{
			throw  new SystemException(e,"updateApplyTransferById(id)",log);
		}
	}
	
	private void handleApplyAndTransferMoenySchedule(DBRow applyMoney, long transferId,double money, String currency, long adid, String adName, int upOrDe, HttpServletRequest request) throws Exception
	{
		try 
		{
			long applyId			= applyMoney.get("apply_id", 0L);
			long associateId		= applyMoney.get("association_id", 0L);
			int associateTypeId		= applyMoney.get("association_type_id", 0);
			int types				= applyMoney.get("types", 0);
			boolean isApplyFinish	= FundStatusKey.FINISH_PAYMENT == applyMoney.get("status", 0)?true:false;
			int applyProcessKeyId	= 0;
			int transferProcessKeyId= 0;
			String upOrDelStr		= 1==upOrDe?"转账完成":"被删除了";
			String content			= adName+"确认了转账单:"+transferId+upOrDelStr+",所属资金单:"+applyId
										+ ",关联"+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(associateTypeId+"")
										+ associateId;
			//采购单定金
			 if(FinanceApplyTypeKey.PURCHASE_ORDER == associateTypeId && 100009 == types)
		    {
				 applyProcessKeyId	= ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS;
				 transferProcessKeyId	= ProcessKey.PURCHASE_EXPECT_TRANSFER_MONEY;
				 DBRow dbrow=new DBRow();
				 dbrow.add("followup_type", 2);
				 dbrow.add("followup_type_sub", 0);
				 dbrow.add("purchase_id", associateId);
				 dbrow.add("followup_content",content);
				 dbrow.add("follower_id",adid);
				 dbrow.add("follower",adName);
				 dbrow.add("followup_date",DateUtil.NowStr());
				 fpm.addPurchasefollowuplogs(dbrow);
		    }
		    //交货单货款
		    else if(FinanceApplyTypeKey.DELIVERY_ORDER == associateTypeId && 100001 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS;
				transferProcessKeyId	= ProcessKey.TRANSPORT_EXPECT_TRANSFER_MONEY;
				floorTransportMgrLL.insertLogs(String.valueOf(associateId), content, adid, adName, 2);
		    }
		    //交货单运费
		    else if(FinanceApplyTypeKey.DELIVERY_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
				transferProcessKeyId= ProcessKey.TRANSPORT_STOCK_TRANSFER_MONEY;
		    	//由于运费可以申请多次，只能通过单据状态判断是否完成
//		    	if(this.isTransportFreightFinish(associateId,associateTypeId))
//		    	{
		    	boolean stockInSetIsFinish = false;
		    	DBRow transportRow = floorTransportMgrZJ.getDetailTransportById(associateId);
		    	if(null != transportRow && TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH == transportRow.get("stock_in_set", 0))
		    	{
		    		stockInSetIsFinish = true;
		    	}
		    	scheduleMgrZr.addScheduleReplayExternal(associateId, ModuleKey.TRANSPORT_ORDER, applyProcessKeyId, adName+"确认了交货单:"+associateId, stockInSetIsFinish, request, "");
//		    	}
		    }
		    //转运单运费
		    else if(FinanceApplyTypeKey.TRANSPORT_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
				transferProcessKeyId= ProcessKey.TRANSPORT_STOCK_TRANSFER_MONEY;
		    	//由于运费可以申请多次，只能通过单据状态判断是否完成
//		    	if(this.isTransportFreightFinish(associateId,associateTypeId))
//		    	{
		    	boolean stockInSetIsFinish = false;
		    	DBRow transportRow = floorTransportMgrZJ.getDetailTransportById(associateId);
		    	if(null != transportRow && TransportStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH == transportRow.get("stock_in_set", 0))
		    	{
		    		stockInSetIsFinish = true;
		    	}
		    	scheduleMgrZr.addScheduleReplayExternal(applyId, ModuleKey.TRANSPORT_ORDER, applyProcessKeyId, adName+"确认了转运单:"+associateId, stockInSetIsFinish, request, "");
//		    	}
		    }
			 //返修单运费
		    else if(FinanceApplyTypeKey.REPAIR_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.REPAIR_STOCK_APPLY_FUNDS;
				transferProcessKeyId= ProcessKey.REPAIR_STOCK_TRANSFER_MONEY;
		    	//由于运费可以申请多次，只能通过单据状态判断是否完成
		    	boolean stockInSetIsFinish = false;
		    	DBRow repairOrderRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(associateId);
		    	if(null != repairOrderRow && RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH == repairOrderRow.get("stock_in_set", 0))
		    	{
		    		stockInSetIsFinish = true;
		    	}
		    	scheduleMgrZr.addScheduleReplayExternal(applyId, ModuleKey.REPAIR_ORDER, applyProcessKeyId, adName+"确认了返修单:"+associateId, stockInSetIsFinish, request, "");
		    }
		    else
		    {
		    	ApplyFundsTypesAndProcessKey applyFundsTypesAndProcessKey = new ApplyFundsTypesAndProcessKey();
		    	applyProcessKeyId = applyFundsTypesAndProcessKey.getTypesAndProcessKeyById(types);
		    	transferProcessKeyId = applyProcessKeyId;
		    }
			//加任务日志
			String transferContent = "";
			//1代表转完，2代表删除申请转账
			if(1 == upOrDe)
			{
				transferContent = adName+"确认了转账单:"+transferId+"转账完成,金额:"+money+currency
				+ ",所属资金单:"+applyId+",关联"+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(associateTypeId+"")
				+associateId;
			}
			else if(2 == upOrDe)
			{
				transferContent = adName+"删除了转账单:"+transferId
								+ ",所属资金单:"+applyId+",关联"+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(associateTypeId+"")
								+ associateId;
			}
			//转账任务
			scheduleMgrZr.addScheduleReplayExternal(transferId, ModuleKey.APPLY_TRANSFER_MONEY, transferProcessKeyId, transferContent, true, request, "");
			String statusStr = new FundStatusKey().getFundStatusKeyNameById(applyMoney.get("status", 0)+"");
			if(statusStr.contains("</font>"))
			{
				int fontStaIndex = statusStr.indexOf(">");
				int fontEndIndex = statusStr.indexOf("</font>");
				statusStr = statusStr.substring(fontStaIndex+1, fontEndIndex);
			}
			//资金任务
			String applyMoneyContent = adName+"确认了资金单:"+applyId
			+ "关联"+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(associateTypeId+"")
			+ associateId+","+statusStr +","+transferContent;
			scheduleMgrZr.addScheduleReplayExternal(applyId, ModuleKey.APPLY_MONEY, applyProcessKeyId, applyMoneyContent, isApplyFinish, request, "");
		}
		catch (Exception e) {
			throw  new SystemException(e,"handleApplyMoenySchedule",log);
		}
	}
	
	private boolean isTransportFreightFinish(long associateId, int associateTypeId) throws Exception
	{
		try 
		{
			boolean isFinish = true;
			DBRow[] applyMoneys = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,associateId,associateTypeId);
	    	for (int i = 0; i < applyMoneys.length; i++) {
				if(TransferAccountsKey.NOT_TRANSFER == applyMoneys[i].get("status", 0))
				{
					isFinish = false;
					break;
				}
			}
	    	return isFinish;
		} catch (Exception e) {
			throw  new SystemException(e,"isTransportFreightFinish",log);
		}
	}
	
	/**
	 * 处理采购单发票状态
	 * @param request
	 * @param associateId
	 * @param associateType
	 * @param types
	 * @param amountTransfer
	 * @param adid
	 * @param adName
	 * @throws Exception
	 */
	private void handlePurchaseInvoiceByApplyMoneyTotal(HttpServletRequest request,long associateId,int associateType,int types,double amountTransfer,long adid,String adName) throws Exception
	{
		try
		{
			if(100009 == types && 4 == associateType){
				//如果此资金申请是采购单的定金，association_id就是采购单的Id
				double transfer_delivery_total	= 0.0;
				DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(associateId));
				for (int i = 0; i < deliveryOrderRows.length; i++) {
					DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[i].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
					for (int j = 0; j < applyMoneyDelivers.length; j++) {
						double amount_transfer_deliver = applyMoneyDelivers[0].get("amount_transfer", 0.0);
						transfer_delivery_total += amount_transfer_deliver;
					}
				}
				double purchase_sumprice = fpm.getPurchasePrice(associateId);
				if(Math.abs((amountTransfer+transfer_delivery_total-purchase_sumprice)) <= purchase_sumprice * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")) *0.01)
				{
					DBRow purchase = fpm.getPurchaseByPurchaseid(associateId);
					if(null != purchase)
					{
						if(InvoiceKey.INVOICE == purchase.get("invoice", 0))
						{
							DBRow purchaseRow = new DBRow();
							purchaseRow.add("invoice", InvoiceKey.INVOICING);
							fpm.updatePurchaseBasic(purchaseRow, associateId);
							DBRow dbrow=new DBRow();
							dbrow.add("followup_type", ProcessKey.INVOICE);
							dbrow.add("followup_type_sub", InvoiceKey.INVOICING);
							dbrow.add("purchase_id", associateId);
							dbrow.add("followup_content","[开票中]");
							dbrow.add("follower_id",adid);
							dbrow.add("follower",adName);
							dbrow.add("followup_date",DateUtil.NowStr());
							fpm.addPurchasefollowuplogs(dbrow);
							String contentInvoicing = adName+"确认了采购单："+associateId+"[发票中] ";
							scheduleMgrZr.addScheduleReplayExternal(associateId , ModuleKey.PURCHASE_ORDER ,
									ProcessKey.INVOICE , contentInvoicing , false, request, "purchase_invoice_period");
						}
					}
				}
			}
			if(100001 ==  types && 5 == associateType)
			{
				DBRow transport		= floorTransportMgrZJ.getDetailTransportById(associateId);
				long purchase_id	= transport.get("purchase_id", 0L);
				double purchase_deposit	= 0.0;
				DBRow[] applyMoneyPurhcase = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,purchase_id,FinanceApplyTypeKey.PURCHASE_ORDER);
				if(applyMoneyPurhcase.length > 0)
				{
					purchase_deposit = applyMoneyPurhcase[0].get("amount_transfer",0.0);
				}
				DBRow[] deliveryOrderRows = transportMgrZyj.getTransportRowsByPurchaseId(Long.valueOf(purchase_id));
				double transfer_delivery_total = 0.0;
				for (int i = 0; i < deliveryOrderRows.length; i++) {
					DBRow[] applyMoneyDelivers = applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,deliveryOrderRows[i].get("transport_id",0L),FinanceApplyTypeKey.DELIVERY_ORDER);
					if(applyMoneyDelivers.length > 0) {
						double amount_transfer_deliver = applyMoneyDelivers[0].get("amount_transfer", 0.0);
						transfer_delivery_total += amount_transfer_deliver;
					}
				}
				double purchase_sumprice = fpm.getPurchasePrice(purchase_id);
				if(Math.abs((purchase_deposit+transfer_delivery_total-purchase_sumprice)) <= purchase_sumprice * Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation")) *0.01)
				{
					DBRow purchase = fpm.getPurchaseByPurchaseid(purchase_id);
					if(null != purchase)
					{
						if(InvoiceKey.INVOICE == purchase.get("invoice", 0))
						{
							DBRow purchaseRow = new DBRow();
							purchaseRow.add("invoice", InvoiceKey.INVOICING);
							fpm.updatePurchaseBasic(purchaseRow, purchase_id);
							DBRow dbrow=new DBRow();
							dbrow.add("followup_type", ProcessKey.INVOICE);
							dbrow.add("followup_type_sub", InvoiceKey.INVOICING);
							dbrow.add("purchase_id", associateId);
							dbrow.add("followup_content","[开票中]");
							dbrow.add("follower_id",adid);
							dbrow.add("follower",adName);
							dbrow.add("followup_date",DateUtil.NowStr());
							fpm.addPurchasefollowuplogs(dbrow);
							String contentInvoicing = adName+"确认了采购单："+purchase_id+"[开票中]";
							scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
									 ProcessKey.INVOICE , contentInvoicing , false, request, "purchase_invoice_period");
						}
						
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw  new SystemException(e,"addScheduleReplayApplyMoney",log);
		}
	}
	
	/**
	 * 上传转账凭证
	 * @param request
	 * @throws AdminIsExistException
	 * @throws Exception
	 */
	public long uploadFile(HttpServletRequest request)
	throws Exception
	{
	    try
	    {
	    	//获取转账完成操作人
	    	AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String operateName = adminLoggerBean.getEmploye_name();
			long transferId=StringUtil.getLong(request,"transferId");
			DBRow row=new DBRow();
			row.add("operating_personnel",operateName);
			applyTransferMgrZZZ.updateApplyTransferById(transferId, row);
	    	String permitFile = "jpg,gif,bmp";
			
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));//文件名
			upload.setRootFolder("/upload/financial_management/");//文件保存路径
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			//得到资金申请的ID
			String payment_information=upload.getRequestRow().getString("payment_information");
			String receiver_account=upload.getRequestRow().getString("receiver_account");
			
			String transfer_ids = StringUtil.getString(upload.getRequestRow().getString("transfer_ids"));
			
			if(transfer_ids.equals("")) {
				long id = StringUtil.getLong(upload.getRequestRow().getString("applyTransferId"));
				//所转资金的金额
				double amount=StringUtil.getDouble(upload.getRequestRow().getString("money"));
				String fileName=upload.getFileName();
				updateApplyTransferById(id,fileName,amount,payment_information,receiver_account, adminMgr.getAdminLoginBean(StringUtil.getSession(request)),request);
				updateLogs(request,id);				
			}else {
				String[] transfer_id = transfer_ids.split(",");
				for(int i=0;i<transfer_id.length;i++) {
					long id = Long.parseLong(transfer_id[i]);
					//所转资金的金额
					double amount=StringUtil.getDouble(upload.getRequestRow().getString("money"+id));
					String fileName=upload.getFileName();
					updateApplyTransferById(id,fileName,amount,payment_information,receiver_account, adminMgr.getAdminLoginBean(StringUtil.getSession(request)),request);
					updateLogs(request,id);
				}
			}
			
			return (StringUtil.getLong(upload.getRequestRow().getString("applyMoneyId")));			
	    }
	    catch(Exception e)
	    {
	    	throw new SystemException(e,"uploadFile(requset)",log);
	    }
	}
	
	/**
	 * 确认已转账，上传凭证
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long confirmTransferUploadVoucher(HttpServletRequest request) throws Exception
	{
		try 
		{
			//获取转账完成操作人
	    	AdminMgr adminMgr		= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String operateName		= adminLoggerBean.getEmploye_name();
			long transferId			= StringUtil.getLong(request,"transferId");
			DBRow row				= new DBRow();
			row.add("operating_personnel",operateName);
			applyTransferMgrZZZ.updateApplyTransferById(transferId, row);
			
			//保存凭证文件
			String fileNames = StringUtil.getString(request, "file_names");
			String realFileNames = "";
			if(null!=fileNames && fileNames.trim().length()>0)
			{
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				realFileNames = handleApplyMoneyFile(sn, path, fileNames, transferId);
			}
			
			//得到资金申请的ID
			String payment_information	=StringUtil.getString(request, "payment_information");
			String receiver_account		=StringUtil.getString(request, "receiver_account");
			
			String transfer_ids = StringUtil.getString(request, "transfer_ids");
			
			if(transfer_ids.equals("")) {
				long id = StringUtil.getLong(request, "applyTransferId");
				//所转资金的金额
				double amount		=StringUtil.getDouble(request, "money");
				updateApplyTransferById(id,realFileNames,amount,payment_information,receiver_account, adminMgr.getAdminLoginBean(StringUtil.getSession(request)),request);
				updateLogs(request,id);				
			}else {
				String[] transfer_id = transfer_ids.split(",");
				for(int i=0;i<transfer_id.length;i++) {
					long id = Long.parseLong(transfer_id[i]);
					//所转资金的金额
					double amount		=StringUtil.getDouble(request, "money"+id);
					updateApplyTransferById(id,realFileNames,amount,payment_information,receiver_account, adminMgr.getAdminLoginBean(StringUtil.getSession(request)),request);
					updateLogs(request,id);
				}
			}
			
			return (StringUtil.getLong(request, "applyMoneyId"));	
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"confirmTransferUploadVoucher(requset)",log);
		}
	}
	
	public String handleApplyMoneyFile(String sn, String path, String fileNames, long apply_money_id) throws Exception 
	{
		try{
			//重新命名图片文件W_transfer_transferId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			String realFileNames = "";
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false );
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							StringBuffer realFileName = new StringBuffer(sn);
							if(0 != apply_money_id)
							{
								realFileName.append("_").append(apply_money_id);
							}
							realFileName.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							realFileNames = realFileName.toString();
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
//							this.insertApplyMoneyFile(apply_money_id, 1, realFileName.toString());
						}
					}
				}
			}
			return realFileNames;
		}catch (Exception e) {
			throw new SystemException(e, "handleApplyMoneyFile", log);
		}
	}
	
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	
	public void updateLogs(HttpServletRequest request,long id) throws Exception{
		DBRow row[]=null;
		row=applyTransferMgrZZZ.getApplyTransferById(id);						
		
		DBRow[] applyMoneyRow=null;
		long uid=row[0].get("apply_money_id",0l);
		if(uid!=0l)
		{
			applyMoneyRow=floorApplyMoneyMgrZZZ.getApplyMoneyById(uid);
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			floorApplyMoneyLogsMgrLL.insertLogs(applyMoneyRow[0].getString("apply_id"), "转账:转账单"+id+"完成转账", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());
			double total=0d;
//			double amount=applyMoneyRow[0].get("amount", 0d);		
			double standard_money = applyMoneyRow[0].get("standard_money", 0.0);
			total=applyMoneyRow[0].get("amount_transfer", 0d);
			
			String content1 = "";
			
			if(Math.abs(total-standard_money) <= Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01)//已完全转款
			{
				applyMoneyRow[0].add("amount_transfer", total);
				int[] applyStatus	= {TransferAccountsKey.NOT_TRANSFER};
				int applyStatusLen	= applyTransferMgrZZZ.getApplyTransferArrayByApplyMoneyId(applyMoneyRow[0].get("apply_id", 0L), applyStatus).length;
				if(applyStatusLen > 0){
					applyMoneyRow[0].add("status", FundStatusKey.PART_PAYMENT);
					content1 = "部分转账";
				}else{
					applyMoneyRow[0].add("status", FundStatusKey.FINISH_PAYMENT);
					content1 = "全部转账";
				}
				if(applyMoneyRow[0].get("types", 0l)==10014||applyMoneyRow[0].get("types", 0l)==100003)
				{
					applyMoneyMgrZZZ.updateAssetsState(applyMoneyRow[0].get("association_id", 0l), 5);
				}
			}
			if(total<standard_money&&Math.abs(total-standard_money) > Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01&&total>0d)//已部分转款
			{
				applyMoneyRow[0].add("amount_transfer", total);
				applyMoneyRow[0].add("status", FundStatusKey.PART_PAYMENT);
				content1 = "部分转账";
			}
			//记录日志
			int association_type_id = applyMoneyRow[0].get("association_type_id", 0);
			long association_id = applyMoneyRow[0].get("association_id",0);

			String content = "";
			long types = applyMoneyRow[0].get("types", 0);
			TDate tDate = new TDate();		
			if(types==100001||types==10015||types==10025||types==10026||types==10027) {
				if(association_type_id==4) {
					content += "采购:费用支付"+total;
					content += ","+content1;
					
					floorPurchaseMgrLL.insertLogs(Long.toString(association_id), content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 2);
				}
				else if(association_type_id==5) 
				{//交货单
					content += types==10025?"报关:费用支付"+total:"";
					content += types==10026?"清关:费用支付"+total:"";
					content += types==10027?"退税:费用收入"+total:"";
					content += types==10015?"运费:费用支付"+total:"";
					content += ","+content1;

				}
				else if(association_type_id==6) 
				{//转运单
					content += types==10025?"报关:资金支付"+total:"";
					content += types==10026?"清关:资金支付"+total:"";
					content += types==10027?"退税:资金收入"+total:"";
					content += types==10015?"运费:费用支付"+total:"";
					content += ","+content1;
					
					floorTransportMgrLL.insertLogs(Long.toString(association_id), content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 2);
				}
			}
		}
	}
	
	/**
	 * 根据转账申请的ID或资金申请的ID查询记录
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyTransferById(String id)
	throws Exception
	{
		try
		{
			//以”W“开头的是转账申请
			if("W".equals(id.substring(0, 1).toUpperCase()))
			{
				id=id.substring(1);
				return (applyTransferMgrZZZ.getApplyTransferById(Long.parseLong(id)));
			}
			else if("F".equals(id.substring(0, 1).toUpperCase()))//以”F“资金申请查询
			{
				id=id.substring(1);
				return (applyTransferMgrZZZ.getApplyTransferByApplyMoneyId(Long.parseLong(id), null));
			}
			else
			{				
				return (applyTransferMgrZZZ.getApplyTransferById(Long.parseLong(id)));
			}
		}
		catch(Exception e)
		{
			
			throw new SystemException(e,"getApplyTransferById(id)",log);
		}
		
	}
	/**
	 * 根据条件过滤转账申请
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyTransferByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception
    {
		try
		{	
			String startTime=StringUtil.getString(request, "st");	
	    	String endTime=StringUtil.getString(request,"en");	    	
	    	int status=StringUtil.getInt(request, "status");
	    	String selectSortTime=StringUtil.getString(request,"selectSortTime");
	    	if(selectSortTime==""){
	    		selectSortTime="create_time";
	    	}
			return applyTransferMgrZZZ.getApplyTransferByCondition(selectSortTime,startTime, endTime, status, pc);
		}
		catch(Exception e)
		{
			
			throw new SystemException(e,"getApplyTransferById(id)",log);
		}
    }
	/**
	 * 根据ID删除转账申请
	 * @param id
	 * @throws Exception
	 */
	public void deleteApplyTransferByTransferId(long id)
	throws Exception
	{
		try
		{
			
			applyTransferMgrZZZ.deleteApplyTransferById(id);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"deleteApplyTransferByTransferId",log);
		}
	}
	/**
    * 删除任务
    * @param apply_money_id
    * @throws Exception
    */
   private void deleteApplyTransferSchedule(long apply_money_id, HttpServletRequest request) throws Exception
   {
	   try 
	   {
			//资金任务
			int[] associateTypeArr = {ModuleKey.APPLY_TRANSFER_MONEY};
			DBRow[] transportSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(apply_money_id, associateTypeArr);
			for (int i = 0; i < transportSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(transportSchedules[i].get("schedule_id", 0L), request);
			}
	   } 
	   catch (Exception e) 
	   {
		   throw new SystemException(e,"deleteApplyMoneySchedule(apply_money_id)",log);
	   }
   }
	
	@Override
	public void deleteApplyTransferAndHandleApplyMoneyState(long applyId, long transportMoneyId,HttpServletRequest request)
    throws Exception{
		try {
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String operateName = adminLoggerBean.getEmploye_name();
			DBRow[] applyTransferMoney = applyTransferMgrZZZ.getApplyTransferById(transportMoneyId);
			applyTransferMgrZZZ.deleteApplyTransferById(transportMoneyId);
			this.deleteApplyTransferSchedule(transportMoneyId, request);
			int[] allStatus		= {};
			int allStatusLen	= applyTransferMgrZZZ.getApplyTransferArrayByApplyMoneyId(applyId, allStatus).length;
			int[] finishStatus	= {TransferAccountsKey.FINISH_TRANSFER};
			int finishStatusLen	= applyTransferMgrZZZ.getApplyTransferArrayByApplyMoneyId(applyId, finishStatus).length;
			DBRow[] applyMoneyRows = floorApplyMoneyMgrZZZ.getApplyMoneyById(applyId);
			DBRow applyMoney = new DBRow();
			boolean isFinish = false;
			if(allStatusLen > 0)
			{
				if(allStatusLen == finishStatusLen)
				{
					if(applyMoneyRows.length > 0){
						double standard_money  = applyMoneyRows[0].get("standard_money", 0d);
						double total_transfer  = applyMoneyRows[0].get("amount_transfer", 0d);
						if(Math.abs(total_transfer-standard_money) <= Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01)//已完全转款
						{
							applyMoney.add("status", FundStatusKey.FINISH_PAYMENT);
							isFinish = true;
							String userName=adminLoggerBean.getEmploye_name();
							Date date=(Date)Calendar.getInstance().getTime();
							SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String endTime = dateformat.format(date);
							DBRow zwbrow=new DBRow();
							zwbrow.add("end_time", endTime);
							zwbrow.add("operating_personnel", userName);
							floorApplyMoneyMgrZZZ.updateApplyMoneyById(applyId, zwbrow);
						}else if(total_transfer < standard_money &&(Math.abs(total_transfer-standard_money) > Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*standard_money*0.01)){
							if(applyMoneyRows[0].get("status", 0) != FundStatusKey.FINISH_PAYMENT){
								applyMoney.add("status", FundStatusKey.PART_PAYMENT);
							}
						}
					}
				}
				else
				{
					if(finishStatusLen > 0 && applyMoney.get("status", 0) != FundStatusKey.FINISH_PAYMENT){
						applyMoney.add("status", FundStatusKey.PART_PAYMENT);
					}else{
						applyMoney.add("status", FundStatusKey.HAD_APPLY_TRANSFER);
					}
				}
			}
			else
			{
				applyMoney.add("status", FundStatusKey.NO_APPLY_TRANSFER);
			}
			floorApplyMoneyMgrZZZ.updateApplyMoneyById(applyId, applyMoney);
			//如果采购定金+货款-采购金额在一定的误差范围内，发票置中
			this.handlePurchaseInvoiceByApplyMoneyTotal(request, applyMoney.get("association_id", 0L), applyMoney.get("association_type_id", 0), applyMoney.get("types", 0), applyMoney.get("amount_transfer",0.0), adminLoggerBean.getAdid(), operateName);
			//处理资金和转账的任务
			this.handleApplyAndTransferMoenySchedule(applyMoneyRows[0], transportMoneyId, applyTransferMoney[0].get("amount", 0.0), applyTransferMoney[0].getString("currency"),adminLoggerBean.getAdid(), operateName, 2, request);
		} catch (Exception e) {
			throw new SystemException(e,"deleteApplyTransferByTransferId",log);
		}
	}
	
	public DBRow[] getApplyTransferByKey(String search_key,int search_mode,PageCtrl pc)
		throws Exception 
	{
		try
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
//				search_key +="*";这个索引是IK分词器，不要最后+*
			}
			
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return ApplyTransferIndexMgr.getInstance().getSearchResults(search_key,search_mode,page_count,pc);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyTransferByKey",log);
		}
	}
	
	public void editTransferIndex(long apply_transfer_id,String type)
		throws Exception
	{
		DBRow applyTransfer = applyTransferMgrZZZ.getDetailApplyTransferById(apply_transfer_id);
		
		long apply_money_id = applyTransfer.get("apply_money_id",0l);
		String receiver = applyTransfer.getString("receiver");
		String receiver_information = applyTransfer.getString("receiver_information");
		String receiver_account = applyTransfer.getString("receiver_account");
		
		if(type.equals("add"))
		{
			ApplyTransferIndexMgr.getInstance().addIndex(apply_transfer_id, apply_money_id, receiver, receiver_information, receiver_account);
			//添加转账申请后，修资金申请索引
			applyMoneyMgrZZZ.editApplyMoneyIndex(apply_money_id, "update");
			
		}
		else if(type.equals("update"))
		{
			ApplyTransferIndexMgr.getInstance().updateIndex(apply_transfer_id, apply_money_id, receiver, receiver_information, receiver_account);
		}
	}
	
	
	public void setFloorApplyMoneyLogsMgrLL(
			FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL) {
		this.floorApplyMoneyLogsMgrLL = floorApplyMoneyLogsMgrLL;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorApplyMoneyMgrLL(FloorApplyMoneyMgrLL floorApplyMoneyMgrLL) {
		this.floorApplyMoneyMgrLL = floorApplyMoneyMgrLL;
	} 

	public void setFloorPurchaseMgrLL(FloorPurchaseMgrLL floorPurchaseMgrLL) {
		this.floorPurchaseMgrLL = floorPurchaseMgrLL;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}

	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) {
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}

	public void setFloorApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ) {
		this.floorApplyMoneyMgrZZZ = floorApplyMoneyMgrZZZ;
	}

	public void setApplyTransferMgrZZZ(FloorApplyTransferMgrZZZ applyTransferMgrZZZ) 
	{
		this.applyTransferMgrZZZ = applyTransferMgrZZZ;
	}

	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

	public void setFpm(FloorPurchaseMgr fpm) {
		this.fpm = fpm;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}
		public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}
	public void setFloorRepairOrderMgrZyj(
			FloorRepairOrderMgrZyj floorRepairOrderMgrZyj) {
		this.floorRepairOrderMgrZyj = floorRepairOrderMgrZyj;
	}

	
	
}
