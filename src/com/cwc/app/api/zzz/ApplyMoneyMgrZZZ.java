package com.cwc.app.api.zzz;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.api.zj.PurchaseMgr;
import com.cwc.app.api.zj.TransportMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.emailtemplate.velocity.financialmanagement.zzz.AgainApplyTransferEmailPageString;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.floor.api.ll.FloorApplyImagesMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyMoneyLogsMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.tjh.FloorAssetsMgr;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorApplyFundsMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyFundsCategoryMgrZZZ;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.key.ApplyFundsTypesAndProcessKey;
import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.TransferAccountsKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.lucene.zr.ApplyMoneyIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.key.GoodsApplyStatusKey;


public  class ApplyMoneyMgrZZZ implements ApplyMoneyMgrIFace {

	static Logger log = Logger.getLogger("ACTION");
	private FloorApplyMoneyMgrZZZ applyMoneyMgrZZZ;
	private FloorApplyFundsCategoryMgrZZZ floorApplyFundsCategoryMgrZZZ;
	private FloorAssetsMgr floorAssetsMgr;
	private AdminMgrIFace adminMgr;
	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	private SystemConfig systemConfig;
	private FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL;
	private TransportMgrZJ transportMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorPurchaseMgr fpm;
	private FloorTransportMgrLL floorTransportMgrLL;
	private FloorTransportMgrZr floorTransportMgrZr;
	private AccountMgrIfaceZr accountMgrZr ;
	private FloorApplyFundsMgrZyj floorApplyFundsMgrZyj;

	
	public DBRow[] getApplyMoneyLastTimeSort(PageCtrl pc)throws Exception{
		try{
			return this.applyMoneyMgrZZZ.getApplyMoneyLastTimeSort(pc);		
		}catch(Exception e){
			throw new SystemException(e,"getApplyMoneyLastTimeSort",log);
		}
	}
	public DBRow[] getApplyMoneyCreateTimeSort(PageCtrl pc)throws Exception{
		try{
			return this.applyMoneyMgrZZZ.getApplyMoneyCreateTimeSort(pc);		
		}catch(Exception e){
			throw new SystemException(e,"getApplyMoneyCreateTimeSort",log);
		}
	}
    /**
     * 增加资金申请单
     * @param request
     * @throws AdminIsExistException
     * @throws Exception
     */
	public DBRow addApplyMoney(String lastTime,String adid,long categoryId,long association_id,double amount,
			String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,
			int association_type_id,long center_account_type_id,long center_account_type1_id,long payee_type_id,
			long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean,HttpServletRequest request) 
	throws Exception 
	{
		try
		{
			double amount_transfer=0d;    
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid)).getString("account");
			DBRow row=new DBRow();
			row.add("types",categoryId);
			row.add("association_id", association_id);
			row.add("amount", amount);
			row.add("amount_transfer", amount_transfer);
			row.add("payee",payee);
			row.add("payment_information", paymentInformation);					
			row.add("create_time",DateUtil.NowStr());
			row.add("remark", remark);
			row.add("creater_id", adid);
			row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("association_type_id", association_type_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			row.add("last_time", lastTime);
			if(currency.equals("RMB"))
			{
				row.add("standard_money", amount);
			}
			else
			{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			
			DBRow returnValue=new DBRow();
			long apply_id = applyMoneyMgrZZZ.addApplyMoney(row);
			
			accountMgrZr.addAccount(apply_id, request);
			//申请资金创建索引
			this.editApplyMoneyIndex(apply_id, "add");
			returnValue.add("applyId", apply_id);
			returnValue.add("adid", adid);
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id), "添加资金申请:单号"+apply_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());
			return (returnValue);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoney",log);
		}
	}
	
	/**
	 * 添加资金申请、文件、任务
	 */
	public DBRow addApplyMoneyAndSchedule(String lastTime,String adid,long categoryId,long association_id,double amount,
			String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,
			int association_type_id,long center_account_type_id,long center_account_type1_id,long payee_type_id,
			long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean,
			String sn,String path,String fileNames,
			String adminUserIds,int isNeedMail,int isNeedMessage,int isNeedPage,
			HttpServletRequest request) throws Exception 
	{
		try
		{
			double amount_transfer=0d;    
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid)).getString("account");
			String userRealName = adminMgr.getDetailAdmin(Long.parseLong(adid)).getString("employe_name");
			DBRow row=new DBRow();
			row.add("types",categoryId);
			row.add("association_id", association_id);
			row.add("amount", amount);
			row.add("amount_transfer", amount_transfer);
			row.add("payee",payee);
			row.add("payment_information", paymentInformation);					
			row.add("create_time",DateUtil.NowStr());
			row.add("remark", remark);
			row.add("creater_id", adid);
			row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("association_type_id", association_type_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			row.add("last_time", lastTime);
			if(currency.equals("RMB"))
			{
				row.add("standard_money", amount);
			}
			else
			{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			
			DBRow returnValue=new DBRow();
			long apply_id = applyMoneyMgrZZZ.addApplyMoney(row);
			
			accountMgrZr.addAccount(apply_id, request);
			//申请资金创建索引
			this.editApplyMoneyIndex(apply_id, "add");
			returnValue.add("applyId", apply_id);
			returnValue.add("adid", adid);
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id), "添加资金申请:单号"+apply_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());
			//保存文件
			this.handleApplyMoneyFile(sn, path, fileNames, apply_id);
			//加任务
			if(adminUserIds != null && adminUserIds.trim().length() > 0  ){
			this.addApplyFundsScheduleOneProdure("", "", Long.parseLong(adid),
					adminUserIds, "", 
					apply_id, ModuleKey.APPLY_MONEY, "资金单:"+apply_id,
					userRealName+"申请了"+amount+currency,
					isNeedPage, isNeedMail, isNeedMessage, request, true,
					userRealName, "创建资金申请:"+apply_id,categoryId);
			}
			return (returnValue);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoney",log);
		}
	}
	
	/**
	 * 添加资金的任务
	 * @throws Exception
	 */
	public void addApplyFundsScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long apply_id, int moduleId,
			String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow,
			String employeeName, String logContent,long categoryId) throws Exception {
		
		try
		{
			//通过categoryId判断到底应该申请哪种类型的资金申请
			ApplyFundsTypesAndProcessKey applyFundsTypesAndProcessKey = new ApplyFundsTypesAndProcessKey();
			long processType = applyFundsTypesAndProcessKey.getTypesAndProcessKeyById(categoryId);
			ProcessKey processKey = new ProcessKey();
			String processKeyStr  = processKey.getStatusById((int)processType);
			title += " [" + processKeyStr +"]";
			content += " " + processKeyStr;
			
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			
			//添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(apply_id, moduleId, (int)processType);
			if(null == schedule){
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
						adid, executePersonIds, joinPersonIds,
						apply_id, moduleId, title, content,
						needPage, needMail, needMessage, request, isNow, (int)processType);
			}
			
		}catch (Exception e)
		{
			throw new SystemException(e,"addApplyFundsScheduleOneProdure",log);
		}
		
	}
	
	private void handleApplyMoneyFile(String sn, String path, String fileNames, long apply_money_id) throws Exception 
	{
		try{
			//重新命名图片文件F_applyMoney_applyId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false );
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(apply_money_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							DBRow row = new DBRow();
							row.add("association_id", apply_money_id);
							row.add("association_type", 1);
							row.add("path", realFileName.toString());
							floorApplyFundsMgrZyj.insertApplyMoneyFile(row);
						}
					}
				}
			}
 
		}catch (Exception e) {
			throw new SystemException(e, "handleApplyMoneyFile", log);
		}
	}
	
	/**
     * 增加资金申请单，获取页面传值
     * @param request
     * @throws AdminIsExistException
     * @throws Exception
     */
	public DBRow addApplyMoney(HttpServletRequest request) 
	throws Exception 
	{
		try
		{
			String adid						= StringUtil.getString(request, "adid");
			long categoryId					= StringUtil.getLong(request, "categoryId",0l);
			long association_id				= StringUtil.getLong(request, "associationId",0l);
			double amount					= StringUtil.getDouble(request, "amount");
			String payee					= StringUtil.getString(request, "payee");
			String paymentInfo				= StringUtil.getString(request, "paymentInfo");
			String remark					= StringUtil.getString(request, "remark");
			long center_account_id			= StringUtil.getLong(request, "center_account_id",01);
			long center_account_type_id		= StringUtil.getLong(request, "center_account_type_id",01);
			long center_account_type1_id	= StringUtil.getLong(request, "center_account_type1_id",01);
			long product_line_id			= StringUtil.getLong(request, "product_line_id",01);
			int association_type_id			= StringUtil.getInt(request, "association_type_id");
			long payee_type_id				= StringUtil.getLong(request,"payee_type_id",0);
			long payee_type1_id				= StringUtil.getLong(request,"payee_type1_id",0);
			long payee_id					= StringUtil.getLong(request,"payee_id",0);
			String currency					= StringUtil.getString(request, "currency");
			String lastTime					= StringUtil.getString(request, "last_time");
			String sn						= StringUtil.getString(request, "sn");
			String path						= StringUtil.getString(request, "path");
			String file_names				= StringUtil.getString(request, "file_names");
			String adminUserIds				= StringUtil.getString(request, "adminUserIds");
			int isNeedMail					= StringUtil.getInt(request, "isNeedMail");
			int isNeedMessage				= StringUtil.getInt(request, "isNeedMessage");
			int isNeedPage					= StringUtil.getInt(request, "isNeedPage");
			
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			return (this.addApplyMoneyAndSchedule(lastTime, adid, categoryId, association_id, amount, payee,
					paymentInfo, remark, center_account_id, product_line_id, association_type_id,
					center_account_type_id, center_account_type1_id, payee_type_id, payee_type1_id,
					payee_id, currency, adminLoggerBean, sn, path,file_names, 
					adminUserIds,isNeedMail,isNeedMessage,isNeedPage,request));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoney",log);
		}
	}
	//确认可以添加申请资产、样品资金
	public void affirmAddApplyMoney(HttpServletRequest request)
			throws Exception {
		{
			try {
				long association_id=StringUtil.getLong(request, "associationId",0l);
		 
				String remark = StringUtil.getString(request, "remark");

				double amount = StringUtil.getDouble(request, "amount");
				
				//接收接口参数
				String admin_id=StringUtil.getString(request,"adminUserIdsInvoice");
				String mail=StringUtil.getString(request,"mail");
				String pageMessage=StringUtil.getString(request,"pageMessage");
				String shortMessage=StringUtil.getString(request,"shortMessage");
				boolean ma=false;
				boolean pm=false;
				boolean sm=false;
				if(mail.equals("true")){
					ma=true;
				}
				if(pageMessage.equals("true")){
					pm=true;
				}
				if(shortMessage.equals("true")){
					sm=true;
				}			  
				String lastTime=StringUtil.getString(request,"last_time");
				lastTime+=" 23:59:59";
				Date date=(Date)Calendar.getInstance().getTime();
				SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String startTime = dateformat.format(date);
				
				String payee = StringUtil.getString(request, "payee");
				String paymentInfo=StringUtil.getString(request, "paymentInfo");
				long categoryId=StringUtil.getLong(request, "categoryId",0l);
				long center_account_id=StringUtil.getLong(request, "center_account_id",01);
				long center_account_type1_id=StringUtil.getLong(request, "center_account_type1_id",01);
				long product_line_id=StringUtil.getLong(request, "product_line_id",01);
				int association_type_id=StringUtil.getInt(request, "association_type_id");
				long payee_type_id = StringUtil.getLong(request,"payee_type_id",0);
				long payee_type1_id = StringUtil.getLong(request,"payee_type1_id",0);
				long payee_id = StringUtil.getLong(request,"payee_id",0);
				String currency=StringUtil.getString(request, "currency");
				String title,content;
				if(categoryId==10014){
					 title="申请样品资金："+amount+",关联样品单："+association_id;
					 content=title+",最迟转款时间："+lastTime+",收款人："+payee;
				}else{
					 title="申请固定资金："+amount+",关联固定资产单："+association_id;
					 content=title+",最迟转款时间："+lastTime+",收款人："+payee;
				}
					
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				
				DBRow applyTemp =this.addApplyMoney(lastTime,String.valueOf(adminLoggerBean.getAdid()), categoryId, association_id, amount, payee, paymentInfo, remark, center_account_id, product_line_id, association_type_id, FinanceApplyTypeKey.SAMPLE, center_account_type1_id, payee_type_id, payee_type1_id, payee_id, currency, adminLoggerBean,request);
			
				
				this.updateAssetsState(association_id,3);				
				//添加关联文件重新命名文件名称.将文件移动到新的文件夹中
				String fileNames = StringUtil.getString(request, "file_name");
				int association_type = StringUtil.getInt(request, "association_type");
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				
				String[] fileNameArray = null ;
				if(fileNames.trim().length() > 0){
					fileNameArray = fileNames.split(",");
				}
				if(fileNameArray != null && fileNameArray.length > 0 ){
					String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
					for(String name : fileNameArray){
						// 保存数据。移动文件
						DBRow row = new DBRow();
						StringBuffer realFileName = new StringBuffer(sn);
						realFileName.append("_").append(association_id).append("_");
						String suffix = getSuffix(name);
						realFileName.append(name.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfApplyMoneyImageByFileName(realFileName.toString());
						if(indexFileName != 0){
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						row.add("association_id", applyTemp.get("applyId", 0l));
						row.add("association_type", association_type);
						row.add("path", realFileName.toString());
						this.addUploadImage(row);
						String tempUrl = baseTempUrl+name;
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
						FileUtil.moveFile(tempUrl,url.toString());
					}	
				}
				//通知
				if(admin_id.trim().length() > 0 ){
					int process=0;
					if(categoryId==10014){
						process = ProcessKey.APPLY_FUNDS_SAMPLE_PROCUREMENT;
					}else{
						process = ProcessKey.APPLY_FUNDS_FIXED_ASSETS;
					}
					scheduleMgrZr.addScheduleByExternalArrangeNow(startTime, lastTime, adminLoggerBean.getAdid(), admin_id, "", applyTemp.get("applyId", 0l), ModuleKey.APPLY_MONEY, title, content, pm, ma, sm, request, true, process);
				}


			} catch(Exception e){
				throw new SystemException(e,"affirmAddApplyMoney",log);
			}
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	/**
	 * 保存上传图片信息
     * @param requests
     * @throws AdminIsExistException
     * @throws Exception
     */
	public void addUploadImage(DBRow row) throws Exception{
		try {
			applyMoneyMgrZZZ.addUploadImage(row);
			return;
		} catch (Exception e) {
			
			throw new SystemException(e,"addUploadImage",log);
		}
	}
	
	
	/**
	 * 更新资金申请
	 */
	public void updateApplyMoney(HttpServletRequest request)throws Exception 
	{
		try 
		{
			long apply_id=StringUtil.getLong(request, "apply_id",0l);
			String adid=StringUtil.getString(request, "adid");
			String adName = StringUtil.getString(request, "adName");
			long categoryId=StringUtil.getLong(request, "categoryId",0l);
			long old_category_id = StringUtil.getLong(request, "old_category_id");
			String association_id=StringUtil.getString(request, "associationId");
			String association_type_id=StringUtil.getString(request, "association_type_id");
			double amount=StringUtil.getDouble(request, "amount");
			String payee=StringUtil.getString(request, "payee");
			String paymentInfo=StringUtil.getString(request, "paymentInfo");
			String remark=StringUtil.getString(request, "remark");
			long center_account_id=StringUtil.getLong(request, "center_account_id",01);
			long center_account_type_id=StringUtil.getLong(request, "center_account_type_id",01);
			long center_account_type1_id=StringUtil.getLong(request, "center_account_type1_id",01);
			long product_line_id=StringUtil.getLong(request, "product_line_id",01);
			long payee_type_id = StringUtil.getLong(request,"payee_type_id",0);
			long payee_type1_id = StringUtil.getLong(request,"payee_type1_id",0);
			long payee_id = StringUtil.getLong(request,"payee_id",0);
			String currency=StringUtil.getString(request, "currency");
			int states = StringUtil.getInt(request, "states");
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.updateApplyMoney(apply_id,adid,categoryId, association_id, amount, payee, paymentInfo, remark, center_account_id,product_line_id,association_type_id, center_account_type_id, center_account_type1_id,payee_type_id,payee_type1_id,payee_id,currency,adminLoggerBean);
			//添加任务和日志
			this.handleApplyMoenySchedule(apply_id, Long.parseLong(association_id), Integer.parseInt(association_type_id),old_category_id, categoryId, states,Long.parseLong(adid), adName,amount,currency,"", request);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateApplyMoney(request)",log);
		}
	}
	
	/**
	 * 添加任务和日志
	 * @param applyId
	 * @param associateId
	 * @param associateTypeId
	 * @param types
	 * @param states
	 * @param adName
	 * @param request
	 * @throws Exception
	 */
	private void handleApplyMoenySchedule(long applyId, long associateId, int associateTypeId, long old_category_id, long types, int states, long adid, String adName,double amount,String currency,String content, HttpServletRequest request) throws Exception
	{
		try 
		{
			boolean isApplyFinish	= FundStatusKey.FINISH_PAYMENT == states?true:false;
			int applyProcessKeyId	= 0;
			boolean isUpdateSchedule = false;
			
			String applyMoneyContent = adName+"修改了资金单:"+applyId
										+ ",关联"+new FinanceApplyTypeKey().getFinanceApplyTypeKeyName(associateTypeId+"")
										+ associateId+",金额:"+amount+currency;
			if(!"".equals(content) && "".equals(currency))
			{
				applyMoneyContent = content;
			}
			
			if(old_category_id != types)
			{
				isUpdateSchedule = true;
			}
			//采购单定金
			 if(FinanceApplyTypeKey.PURCHASE_ORDER == associateTypeId && 100009 == types)
		    {
				 applyProcessKeyId	= ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS;
				 DBRow dbrow=new DBRow();
				 dbrow.add("followup_type", 2);
				 dbrow.add("followup_type_sub", 0);
				 dbrow.add("purchase_id", associateId);
				 dbrow.add("followup_content",applyMoneyContent);
				 dbrow.add("follower_id",adid);
				 dbrow.add("follower",adName);
				 dbrow.add("followup_date",DateUtil.NowStr());
				 fpm.addPurchasefollowuplogs(dbrow);
		    }
			 
		    //交货单货款
		    else if(FinanceApplyTypeKey.DELIVERY_ORDER == associateTypeId && 100001 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS;
				floorTransportMgrLL.insertLogs(String.valueOf(associateId), content, adid, adName, 2);
		    }
		    //交货单运费
		    else if(FinanceApplyTypeKey.DELIVERY_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
		    	
		    }
		    //转运单运费
		    else if(FinanceApplyTypeKey.TRANSPORT_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS;
		    }
			//返修单运费
		    else if(FinanceApplyTypeKey.REPAIR_ORDER == associateTypeId && 10015 == types)
		    {
		    	applyProcessKeyId	= ProcessKey.REPAIR_STOCK_APPLY_FUNDS;
		    }
		    else
		    {
		    	ApplyFundsTypesAndProcessKey applyFundsTypesAndProcessKey = new ApplyFundsTypesAndProcessKey();
		    	applyProcessKeyId = applyFundsTypesAndProcessKey.getTypesAndProcessKeyById(types);
		    }
			 if(isUpdateSchedule)
			 {
				//资金任务先更新类型
				 ApplyFundsTypesAndProcessKey applyFundsTypesAndProcessKey = new ApplyFundsTypesAndProcessKey();
				 int applyProcessKeyIdOld = applyFundsTypesAndProcessKey.getTypesAndProcessKeyById(old_category_id);
				 DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(applyId, ModuleKey.APPLY_MONEY, applyProcessKeyIdOld);
				 long scheduleId = schedule.get("schedule_id", 0L);
				 DBRow scheduleNow = new DBRow();
				 scheduleNow.add("associate_process", applyProcessKeyId);
				 scheduleMgrZr.udpateScheduleAndScheduleInfo(scheduleId, scheduleNow);
			 }
			 //资金任务跟进
			 scheduleMgrZr.addScheduleReplayExternal(applyId, ModuleKey.APPLY_MONEY, applyProcessKeyId, applyMoneyContent, isApplyFinish, request, "");
		}
		catch (Exception e) {
			throw  new SystemException(e,"handleApplyMoenySchedule",log);
		}
	}
	
	
	/**
     * @param request
     * @throws AdminIsExistException
     * @throws Exception
     */
	public void updateApplyMoney(long apply_id,String adid,long categoryId,String association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,String association_type_id, long center_account_type_id, long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean) 
	throws Exception 
	{
		try
		{
			SystemConfig systemConfig = new SystemConfig();   
			String userName = adminMgr.getDetailAdmin(Long.parseLong(adid)).getString("account");
			DBRow row=new DBRow();

			row.add("types",categoryId);
			row.add("association_id", association_id);
			row.add("association_type_id", association_type_id);
			row.add("amount", amount);
			row.add("payee",payee);
			if(paymentInformation !=null && paymentInformation.length()>0){
			row.add("payment_information", paymentInformation);					
			}
			//row.add("create_time",DateUtil.NowStr());
			row.add("remark", remark);
			//row.add("creater_id", adid);
			//row.add("creater", userName);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("product_line_id", product_line_id);
			row.add("payee_type_id", payee_type_id);
			row.add("payee_type1_id", payee_type1_id);
			row.add("payee_id", payee_id);
			row.add("currency", currency);
			if(currency.equals("RMB"))
				row.add("standard_money", amount);
			else
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			
			applyMoneyMgrZZZ.updateApplyMoneyById(apply_id,row);
			
			//修改资金申请修改索引
			this.editApplyMoneyIndex(apply_id, "update");
			
			floorApplyMoneyLogsMgrLL.insertLogs(Long.toString(apply_id), "修改资金申请:单号"+apply_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());
			return;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyMoney",log);
		}

	}
    /**
     * 获取所有的资金申请单     
     * @param pc
     * @return
     * @throws Exception
     */	
	public DBRow[] getAllApplyMoney(PageCtrl pc) 
	throws Exception 
	{
		try
		{
			return (updateApplyMoney(applyMoneyMgrZZZ.getApplyMoney(pc)));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAllApplyMoney",log);
		}
		
	}
	/**
	 * 根据申请资金表的ID进行筛选查询
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyById(String id) 
	throws Exception 
	{
		try
		{
			if("F".equals(id.substring(0, 1).toUpperCase()))
			{
				id=id.substring(1);
			}
			return (updateApplyMoney(applyMoneyMgrZZZ.getApplyMoneyById(Long.parseLong(id))));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyMoneyById",log);
		}
	}
	/**
	 * 根据条件过滤资金申请
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
    public DBRow[] getApplyMoneyByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception
    {
    	try
    	{
	    	String startTime=StringUtil.getString(request, "st");	
	    	String endTime=StringUtil.getString(request,"en");
	    	int types=StringUtil.getInt(request, "category");
	    	int status=StringUtil.getInt(request, "status");
	    	long productLineId=StringUtil.getLong(request, "productLine");
	    	long centerAccountId=StringUtil.getLong(request, "center_account_id");
	    	long association_type_id=StringUtil.getLong(request, "association_type_id");
	    	String selectSortTime=StringUtil.getString(request,"selectSortTime");
	    	if(selectSortTime==""){
	    		selectSortTime="create_time";
	    	}
	    	return (updateApplyMoney(applyMoneyMgrZZZ.getApplyMoneyByCondition(selectSortTime,startTime,endTime,types,status,productLineId,centerAccountId,association_type_id,pc)));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getApplyMoneyByCondition",log);
    	}
    }
    /**
     * 根据关联ID和类型查询申请资金
     * @param types
     * @param id
     * @return
     * @throws Exception
     */
    public DBRow getApplyMoneyByTypeAndAssociationId(int types,long id)
    throws Exception
    {
    	try
    	{    		
    		return (updateApplyMoney(applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationId(types, id)));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getApplyMoneyByTypeAndAssociationId",log);
    	}
    }
    /**
     * 取消该资金申请
     * @param type
     * @param id
     * @throws Exception
     */
    public void cancelApplyMoneyByTypeAndAssociationId(int type,long id)
    throws Exception
    {
    	try
    	{		
    			DBRow applyMoney = null;
    			applyMoney = applyMoneyMgrZZZ.getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel(type,id,FinanceApplyTypeKey.PURCHASE_ORDER);
    			if(applyMoney!=null)
    			{
		    		long applyId=applyMoney.get("apply_id", 0l);
		    		
		    		DBRow row=new DBRow();
		    		row.add("status",FundStatusKey.CANCEL);
		    		applyMoneyMgrZZZ.updateApplyMoneyById(applyId, row);
    			}
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"updateApplyMoneyByTypeAndAssociationId",log);
    	}
    }
    /**
     * 将查询结果里的资金类型替换成文字说明
     * @param row
     * @return
     * @throws Exception
     */
    public DBRow[] updateApplyMoney(DBRow[] row)
    throws Exception
    {
    	try
    	{
    		for(int i=0;i<row.length;i++)
    		{
    			DBRow data=new DBRow();
    			data.add("category_id", row[i].get("types", 0l));
    			data=floorApplyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(data);
    			row[i].add("categoryId", data==null?"":data.getString("category_id"));
    			row[i].add("categoryName", data==null?"":data.getString("category_name"));
    			if(data==null)
    				data = new DBRow();
    			else {
    				data.clear();
    			}
    			data.add("id", row[i].get("product_line_id", 0l));
    			data=floorApplyFundsCategoryMgrZZZ.getApplyMoneyProductLineById(data);
    			row[i].add("productLineName",  data==null?"":data.getString("name"));
    			if(data==null)
    				data = new DBRow();
    			else {
    				data.clear();
    			}
    			//data.add("id", row[i].get("center_account_id", 0l));
    			//data=floorApplyFundsCategoryMgrZZZ.getApplyMoneyAccountById(data);
    			//row[i].add("accountName",  data==null?"":data.getString("account_name"));
    		}
    		return row;	
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"updateApplyMoney",log);
    	}
    }
    /**
     * 对单条数据的资金申请类型转换
     * @param row
     * @return
     * @throws Exception
     */
    public DBRow updateApplyMoney(DBRow row)
    throws Exception
    {
    	try
    	{
    		
    			DBRow data=new DBRow();
    			data.add("category_id", row.get("types", 0l));
    			data=floorApplyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(data);
    			row.add("categoryName", data.getString("category_name"));
    		
    		return row;	
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"updateApplyMoney",log);
    	}
    }
    /**
     * 固定资产资金申请
     * @param request
     * @return
     * @throws Exception
     */
    public long addAssets(HttpServletRequest request) 
	throws  Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			String adid=StringUtil.getString(request, "adid");
			String a_name = StringUtil.getString(request,"a_name");
			String a_barcode = StringUtil.getString(request, "a_barcode");
			String unit_name = StringUtil.getString(request,"unit_name");
			double unit_price = StringUtil.getDouble(request,"unit_price");
//			String purchase_Date = StrUtil.getString(request,"purchase_date");
			String suppliers = StringUtil.getString(request,"suppliers");
			String consignee = StringUtil.getString(request,"consignee");
			int type = StringUtil.getInt(request,"type");
			int category_id = 0;
			int product_line_id = 0;
			if(type==1)
				category_id = StringUtil.getInt(request,"category");
			else {
				category_id = 100023;
			}
			product_line_id = StringUtil.getInt(request,"productLine");
			int center_account_id = StringUtil.getInt(request,"center_account_id");
			String location = StringUtil.getString(request, "location");
			String requisitioned_id = StringUtil.getString(request,"requisitioned_id");
			String requisitioned = StringUtil.getString(request,"requisitioned");
			int state = StringUtil.getInt(request,"state");
			int goodsstate = StringUtil.getInt(request,"goodsstate");
			int applystate = StringUtil.getInt(request,"applystate");
			String currency = StringUtil.getString(request,"currency");
			if(goodsstate==2){
				row.add("receive_time", DateUtil.NowStr());
			}
			
			row.add("a_name", a_name);
			row.add("a_barcode", a_barcode);
			row.add("unit_name", unit_name);
			row.add("unit_price", unit_price);
			row.add("purchase_date", DateUtil.NowStr());
			row.add("suppliers", suppliers);
			row.add("consignee", consignee);
			row.add("category_id", category_id);
			row.add("product_line_id", product_line_id);
			row.add("location_id", location);
			row.add("requisitioned_id", requisitioned_id);
			row.add("requisitioned", requisitioned);
			row.add("center_account_id", center_account_id);
			row.add("state", state);
			row.add("goodsstate", goodsstate);
			row.add("applystate", applystate);
			row.add("creater_id", adid);
			row.add("currency", currency);
			
			return floorAssetsMgr.addAssets(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"AssetsMgr.addAssets(request) error:",log);
		}
	}
    /**
     * 更改固定资产的状态为已申请
     * @param assetsId
     * @param state
     * @throws Exception
     */
    public void updateAssetsState(long assetsId,int state)
	throws Exception
	{
    	try
    	{
    		DBRow row = new DBRow();
    		row.add("applystate",state);
    		floorAssetsMgr.updateAssets(assetsId, row);
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"updateAssetsState(int state)",log);
    	}
    	
	}
    
    /**
     * 更改资金申请状态为已付款
     * @param request
     * @throws Exception
     */
   public  void updateApplyMoneyStatusByApplyId(HttpServletRequest request)
   throws Exception
   {
	   try
	   {
		   long id = StringUtil.getLong(request, "applyId");
		   DBRow row = new DBRow();
		   AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		   String operateName = adminLoggerBean.getEmploye_name();
		   row.add("operating_personnel", operateName);
		   row.add("end_time", DateUtil.NowStr());
		   row.add("status",2);
		   applyMoneyMgrZZZ.updateApplyMoney(id, row);
		   DBRow applyMoney[] = getApplyMoneyById(String.valueOf(id));
		   if(applyMoney[0].get("types", 0l)==10014||applyMoney[0].get("types", 0l)==100003)
		   {
			   updateAssetsState(applyMoney[0].get("association_id", 0l), 5);
		   }
		   //任务
		   scheduleMgrZr.addScheduleReplayExternal(id, ModuleKey.APPLY_MONEY, ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS, operateName+"确认了资金申请 "+id+"转账完成", true, request, "");
		   handleApplyMoenySchedule(id, applyMoney[0].get("association_id", 0L), applyMoney[0].get("association_type_id", 0), applyMoney[0].get("types", 0L),applyMoney[0].get("types", 0L), applyMoney[0].get("status", 0), adminLoggerBean.getAdid(), operateName,0.0,"",operateName+"确认了资金申请 "+id+"转账完成", request);
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"updateApplyMoneyStatusByApplyId",log);
	   }
   }
   /**
    * 通知财务转款
    * @param request
    * @throws Exception
    */
   public void updateRemarkByApplyId(HttpServletRequest request)
   throws Exception
   {
	   try
	   {
		   long applyId = StringUtil.getLong(request, "applyId");
		   long adid = StringUtil.getLong(request, "adid");
		   String newRemark = StringUtil.getString(request, "message");
		   String context = "通知财务:"+newRemark;
		   
		   applyMoneyLogsMgrZZZ.addApplyMoneyLogs(applyId, adminMgr.getAdminLoginBean(request.getSession()).getAdid(), adminMgr.getAdminLoginBean(request.getSession()).getEmploye_name(), context);
		   sendMail(applyId, adid, newRemark);
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"updateRemarkByApplyId",log);
	   }
   }
   /**
    * 通知财务的邮件发送
    * @param id
    * @param adid
    * @param context
    * @throws Exception
    */
   public void sendMail(long id,long adid,String context)
   throws Exception
   {
	   try
	   {
		  
			
			AgainApplyTransferEmailPageString applyMoneyEmailPageString = new AgainApplyTransferEmailPageString(String.valueOf(id));
			User user = new User(applyMoneyEmailPageString.getUserName(),applyMoneyEmailPageString.getPassWord(),applyMoneyEmailPageString.getHost(),applyMoneyEmailPageString.getPort(),applyMoneyEmailPageString.getUserName(),applyMoneyEmailPageString.getAuthor(),applyMoneyEmailPageString.getRePly());
			
			//获取当前登录用户的信息和角色信息
			DBRow sender=new DBRow();
			sender=adminMgr.getDetailAdmin(adid);
			sender.add("groupName", adminMgr.getDetailAdminGroup(sender.get("adgid", 0l)).getString("name"));
			sender.add("context", context);
		
			DBRow[] row=null;
			row = adminMgr.getAdminByAdgid(100020,null);	
			if(row!=null)
				for(int i=0;i<row.length;i++)
				{
					String email=row[i].getString("email");
					sender.add("receiver",row[i].getString("employe_name"));
					applyMoneyEmailPageString.setUserInfo(sender);
					if(checkMailAddress(email)){
						MailAddress mailAddress = new MailAddress(email.trim());
						MailBody mailBody = new MailBody(applyMoneyEmailPageString,true,null);
						Mail mail = new Mail(user);
						mail.setAddress(mailAddress);
						mail.setMailBody(mailBody);
						mail.send();
					}
				}
			
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"sendMail",log);
	   }
   }
   /**
    * 邮件地址校验
    * @param mail
    * @return
    */
   public boolean checkMailAddress(String mail)
	{
		boolean flag=false;
		////system.out.println(mail);
		if(mail!=null && !"".equals(mail.trim()))
		{
			if(mail.matches("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$"))
				return true;
		}
		
		return flag;
	}
   /**
    * 根据ID删除资金申请
    * @param id
    * @throws Exception
    */
   public void deleteApplyMoneyByApplyId(long id)
   throws Exception
   {
	   try
	   {
		   applyMoneyMgrZZZ.deleteApplyMoney(id);
		   
		   this.editApplyMoneyIndex(id,"del");
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"deleteApplyMoneyByApplyId(id)",log);
	   }
   }
   
   /**
    * 根据ID删除资金申请
    * @param id
    * @throws Exception
    */
   public void deleteApplyMoneyByApplyId(HttpServletRequest request)
   throws Exception
   {
	   try
	   {
		   long id = StringUtil.getLong(request, "applyId");
		   applyMoneyMgrZZZ.deleteApplyMoney(id);
		   //删除任务
		   this.deleteApplyMoneySchedule(id, request);
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"deleteApplyMoneyByApplyId(id)",log);
	   }
   }
   
   /**
    * 删除任务
    * @param apply_money_id
    * @throws Exception
    */
   private void deleteApplyMoneySchedule(long apply_money_id, HttpServletRequest request) throws Exception
   {
	   try 
	   {
			//资金任务
			int[] associateTypeArr = {ModuleKey.APPLY_MONEY};
			DBRow[] transportSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(apply_money_id, associateTypeArr);
			for (int i = 0; i < transportSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(transportSchedules[i].get("schedule_id", 0L), request);
			}
	   } 
	   catch (Exception e) 
	   {
		   throw new SystemException(e,"deleteApplyMoneySchedule(apply_money_id)",log);
	   }
   }
   
   /**
    * 根据输入信息进行模糊查询
    * @param info
    * @param pc
    * @return
    * @throws Exception
    */
   public DBRow[] getApplyMoneyByInfo(String info,PageCtrl pc)
   throws Exception
   {
	   try
	   {
		   DBRow applyMoney[] = applyMoneyMgrZZZ.getApplyMoneyByInfo(info, pc);
		   return updateApplyMoney(applyMoney);
	   }
	   catch(Exception e)
	   {
		   throw new SystemException(e,"getApplyMoneyByInfo",log);
	   }
   }
	@Override
	public DBRow[] getApplyMonyByIndexMgr(String search_key,int search_mode,PageCtrl pc) throws Exception {
		 try
		   {
			 if(!search_key.equals("")&&!search_key.equals("\""))
				{
					search_key = search_key.replaceAll("\"","");
					search_key = search_key.replaceAll("'","");
					search_key = search_key.replaceAll("\\*","");
//					search_key +="*";这个索引是IK分词器，不要最后+*
				}
			 
			 int page_count = systemConfig.getIntConfigValue("page_count");
			 
			 return ApplyMoneyIndexMgr.getInstance().getSearchResults(search_key,search_mode,page_count,pc);
		   }
		   catch(Exception e)
		   {
			   throw new SystemException(e,"getApplyMoneyByInfo",log);
		   }
	}
	
	public DBRow[] getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id)
		throws Exception
	{
		try 
		{
			return applyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(types, association_id, association_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getApplyMoneyByTypeAndAssociationIdAndAssociation",log);
		}
	}
	public DBRow getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id)
		throws Exception
	{
		try 
		{
			return applyMoneyMgrZZZ.getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel(types, association_id, association_type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel",log);
		}
	}
	
	public void editApplyMoneyIndex(long apply_money_id,String type)
		throws Exception
	{
		DBRow applyMoney = applyMoneyMgrZZZ.getDetailApplyMoneyById(apply_money_id);
		long association_id = applyMoney.get("association_id",0l);
		long creater_id = applyMoney.get("creater_id",0l);
		String payment_information = applyMoney.getString("payment_information");
		String payee = applyMoney.getString("payee");
		int association_type_id = applyMoney.get("association_type_id",0);
		
		if(type.equals("add"))
		{
			ApplyMoneyIndexMgr.getInstance().addIndex(apply_money_id, association_id, creater_id, payment_information, payee, association_type_id);
		}
		else if(type.equals("update"))
		{
			ApplyMoneyIndexMgr.getInstance().updateIndex(apply_money_id, association_id, creater_id, payment_information, payee, association_type_id);
		}
		else if(type.equals("del"))
		{
			ApplyMoneyIndexMgr.getInstance().deleteIndex(apply_money_id);
		}
		
		if(association_type_id==FinanceApplyTypeKey.DELIVERY_ORDER||association_type_id==FinanceApplyTypeKey.TRANSPORT_ORDER)
		{
			transportMgrZJ.editTransportIndex(association_id,"update");
		}
		else if(association_type_id==FinanceApplyTypeKey.PURCHASE_ORDER)
		{
			
		}
		
	}
	
	public void setFloorApplyMoneyLogsMgrLL(
			FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL) {
		this.floorApplyMoneyLogsMgrLL = floorApplyMoneyLogsMgrLL;
	}
	
	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	public void setFloorAssetsMgr(FloorAssetsMgr floorAssetsMgr) 
	{
		this.floorAssetsMgr = floorAssetsMgr;
	}
	public void setFloorApplyFundsCategoryMgrZZZ(
			FloorApplyFundsCategoryMgrZZZ floorApplyFundsCategoryMgrZZZ) 
	{
		this.floorApplyFundsCategoryMgrZZZ = floorApplyFundsCategoryMgrZZZ;
	}
	public void setApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ applyMoneyMgrZZZ) 
	{
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}
	public void setTransportMgrZJ(TransportMgrZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	public void setFpm(FloorPurchaseMgr fpm) {
		this.fpm = fpm;
	}
	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}
	public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}
	public void setFloorApplyFundsMgrZyj(FloorApplyFundsMgrZyj floorApplyFundsMgrZyj) {
		this.floorApplyFundsMgrZyj = floorApplyFundsMgrZyj;
	}
}
