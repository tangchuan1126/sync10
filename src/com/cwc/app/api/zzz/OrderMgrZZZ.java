package com.cwc.app.api.zzz;

import org.apache.log4j.Logger;

import com.cwc.app.api.Cart;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.iface.zzz.OrderMgrIfaceZZZ;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class OrderMgrZZZ implements OrderMgrIfaceZZZ{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorOrderMgr fom ;
	private FloorProductMgr fpm;
	public void setFom(FloorOrderMgr fom) {
		this.fom = fom;
	}
	public void setFpm(FloorProductMgr fpm) {
		this.fpm = fpm;
	}
	/**
	 * 获取USPS-P中，校验三个盒子的能否装下货物的商品数据
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailProductByOid(long oid)
	throws Exception
	{
		try
		{
			DBRow[] orderItems = fom.getPOrderItemsByOid(oid); 
			DBRow[] productData = new DBRow[orderItems.length];
			
			
			for(int i = 0;i<orderItems.length;i++)
			{
				productData[i]=new DBRow();
				productData[i].add("count", orderItems[i].getString("quantity"));
				productData[i].add("product_type", orderItems[i].getString("product_type"));
				
				long pc_id = Long.parseLong(orderItems[i].getString("pid"));
				
				if(productData[i].getString("product_type").equals(String.valueOf(ProductTypeKey.UNION_CUSTOM)))//判断商品类型是否为定制商品
				{
					pc_id = fpm.getDetailProductCustomByPcPcid(Long.parseLong(orderItems[i].getString("pid"))).get("orignal_pc_id",0l);
				}
				
				DBRow product = fpm.getDetailProductByPcid(pc_id);
				productData[i].add("length", product.getString("length"));
				productData[i].add("width", product.getString("width"));
				productData[i].add("heigth", product.getString("heigth"));
			}
			return productData;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getDetailProductByOid(oid)",log);
		}
	}	
	
	
}
