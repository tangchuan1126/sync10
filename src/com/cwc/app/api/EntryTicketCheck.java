package com.cwc.app.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class EntryTicketCheck implements Serializable {
	
	@XmlElement(name = "id")
	public String id;

	@XmlElement(name = "entryId")
	public String entryId;

	@XmlElement(name = "checkType")
	public CheckType checkType;

	@XmlElement(name = "checkAction")
	public CheckAction checkAction;

	@XmlElement(name = "driverName")
	public String driverName;

	@XmlElement(name = "driverLicense")
	public String driverLicense;

	@XmlElement(name = "mcDot")
	public String mcDot;

/*	@XmlElement(name = "carrierId")
	public String carrierId;*/

	@XmlElement(name = "photoId")
	public int photoId;
	
	@XmlElement(name = "photoIds")
	public List<Integer>photoIds;
	
	@XmlElement(name = "equipmentType")
	public EquipmentType equipmentType;

	@XmlElement(name = "tractor")
	public String tractor;

	@XmlElement(name = "trailers")
	public List<String> trailers;

	@XmlElement(name = "containerNOs")
	public List<String> containerNOs;

	@XmlElement(name = "containerOperation")
	public ContainerOperation containerOperation;

	@XmlElement(name = "notifyList")
	public List<String> notifyList;

	@XmlElement(name = "approveList")
	public List<String> approveList;

	@XmlElement(name = "note")
	public String note;

	@XmlElement(name = "createdBy")
	public String createdBy;

	@XmlElement(name = "updatedBy")
	public String updatedBy;
	
	@XmlElement(name = "checkInTime")
	public String checkInTime;
	
	/***************need to add fields*******************/
	@XmlElement(name = "warehouse")
	public String warehouse;
	
	@XmlElement(name = "companyName")
	public String companyName;
	
	@XmlElement(name = "customerName")
	public String customerName;
	
	@XmlElement(name = "seal")
	public String seal;
	
	@XmlElement(name = "carrierName")
	public String carrierName;
	
	@XmlElement(name = "spotName")
	public String spotName;
	
	@XmlElement(name = "dockName")
	public String dockName;
	
	@XmlElement(name = "actionType")
	public CheckinActionType actionType; //DELIVERY  PICK_UP BOTH NONE PATROL VISITOR SMALL_PARCEL
}
