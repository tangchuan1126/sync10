package com.cwc.app.api.ly;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zj.FloorCountryMgrZJ;
import com.cwc.app.iface.ly.CountryMgrIFaceLY;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class CountryMgrLY implements CountryMgrIFaceLY
{
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorCountryMgrZJ floorCountryMgrZJ;

	@Override
	public DBRow[] getAllCountry() throws Exception
	{
		try
		{
			return floorCountryMgrZJ.getAllCountryCode();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	 /**
     * 查询所有国家和省份
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年3月23日 下午6:22:56
     */
    public DBRow[] findCountryAndProvinces() throws Exception
    {
    	try
		{
			return floorCountryMgrZJ.findCountryAndProvinces();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CountryMgrLY findCountryAndProvinces",log);
		}
    }

	
	
	public void setFloorCountryMgrZJ(FloorCountryMgrZJ floorCountryMgrZJ)
	{
		this.floorCountryMgrZJ = floorCountryMgrZJ;
	}
	
}
