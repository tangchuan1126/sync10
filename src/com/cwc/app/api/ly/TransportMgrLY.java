package com.cwc.app.api.ly;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.ly.FloorTransportMgrLY;
import com.cwc.app.iface.ly.TransportMgrIFaceLY;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class TransportMgrLY implements TransportMgrIFaceLY
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrLY floorTransportMgrLY;
	
	private TransportMgrZyjIFace transportMgrZyj;
	
	@Override
	public DBRow[] fillterTransport(long send_psid, long receive_psid,
			PageCtrl pc, int status, int declaration, int clearance,
			int invoice, int drawback, int day, int stock_in_set,
			long create_account_id) throws Exception
	{
		try
		{
			return (floorTransportMgrLY.fillterTransport(send_psid,receive_psid, pc, status, declaration, clearance, invoice, drawback,day,stock_in_set,create_account_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	@Override
	public DBRow[] fillterTransportSelf(long self_psid,
			PageCtrl pc, int status, int declaration, int clearance,
			int invoice, int drawback, int day, int stock_in_set,
			long create_account_id) throws Exception 
	{
		try
		{
			return (floorTransportMgrLY.fillterTransportSelf(self_psid, pc, status, declaration, clearance, invoice, drawback,day,stock_in_set,create_account_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}


	@Override
	public DBRow[] getSelfDefaultTransport(long self_psid, PageCtrl pc,
			int status, int declaration, int clearance, int invoice,
			int drawback, int day, int stock_in_set, long create_account_id)
			throws Exception 
	{
		try
		{
			return (floorTransportMgrLY.getSelfDefaultTransport(self_psid, pc, status, declaration, clearance, invoice, drawback,day,stock_in_set,create_account_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 更新部分流程信息（运费，报关，清关，单证）
	 * @param request
	 * @throws Exception
	 */
	public void updateTransportProdures(HttpServletRequest request) throws Exception
	{
		try
		{
			//调用原先更新方法
			transportMgrZyj.updateTransportProdures(request);
		}
		 catch (Exception e)
		{
			throw new SystemException(e,"TransportMgrLY.updateTransportProdures",log);
		}
		
	}
	
	
	
	
	
	public void setFloorTransportMgrLY(FloorTransportMgrLY floorTransportMgrLY) 
	{
		this.floorTransportMgrLY = floorTransportMgrLY;
	}

	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj)
	{
		this.transportMgrZyj = transportMgrZyj;
	}
}
