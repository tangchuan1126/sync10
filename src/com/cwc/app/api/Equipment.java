package com.cwc.app.api;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;

import com.cwc.db.DBRow;

public class Equipment implements Serializable {
	@XmlElement(name = "id")
	public String id;

	@XmlElement(name = "equipmentNo")
	public String equipmentNo;

	@XmlElement(name = "equipmentId")
	public long equipmentId;

	@XmlElement(name = "equipmentType")
	public long equipmentType;

	@XmlElement(name = "checkInEntry")
	public String checkInEntry;

	@XmlElement(name = "checkOutEntry")
	public String checkOutEntry;

	@XmlElement(name = "checkInEntryId")
	public long checkInEntryId;

	@XmlElement(name = "type")
	public EquipmentType type;

	@XmlElement(name = "missingTimes")
	public Integer missingTimes;

	@XmlElement(name = "sealNo")
	public String sealNo;

	@XmlElement(name = "checkInSealNo")
	public String checkInSealNo;

	@XmlElement(name = "checkOutSealNo")
	public String checkOutSealNo;

	@XmlElement(name = "carrierId")
	public String carrierId;

	@XmlElement(name = "originLocationId")
	public String originLocationId;

	@XmlElement(name = "currentLocationId")
	public String currentLocationId;

	@XmlElement(name = "previousLocationId")
	public String previousLocationId;

	@XmlElement(name = "note")
	public String note;

	@XmlElement(name = "createdBy")
	public String createdBy;

	@XmlElement(name = "updatedBy")
	public String updatedBy;

	@XmlElement(name = "resource")
	public DBRow resource;
}
