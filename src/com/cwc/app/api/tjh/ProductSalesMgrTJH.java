package com.cwc.app.api.tjh;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.tjh.FloorOrderProcessMgrTJH;
import com.cwc.app.floor.api.tjh.FloorProductSalesMgrTJH;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.app.key.MapColorKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.db.zj.FloorProductSalesMgrZJ;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;

public class ProductSalesMgrTJH implements ProductSalesMgrIFaceTJH {
	
	private FloorProductSalesMgrTJH floorProductSalesMgr;
	private SystemConfig systemConfig;
	private FloorSupplierMgrTJH floorSupplierMgr;
	private FloorOrderProcessMgrTJH floorOrderProcessMgr;
	private FloorProductSalesMgrZJ floorProductSalesMgrZJ;
	private FloorProductMgr floorProductMgr;
	
	static Logger log = Logger.getLogger("ACTION");

	/**
	 * 查询某一段时间内所有的已发货的产品销售情况
	 */
	public DBRow[] getAllProductSales(PageCtrl pc) throws Exception 
	{
		
		try 
		{
			return (floorProductSalesMgr.getAllProductSales(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductSales(PageCtrl pc, String startDate,String endDate)",log);
		}
	}
	
	
	/**
	 * 统计某一时间段内订单已发货的产品销售情况
	 * @param startDate
	 * @param endDate
	 * @throws Exception 
	 */
	public long statsProductSalesByDelivery(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
				return (floorProductSalesMgrZJ.statsProdyctSalesByDelivey(start_date,end_date));
		}
		
		catch (Exception e) 
		{
			throw new SystemException(e,"statsProductSalesByDelivery(String startDate,String endDate)",log);
		}
		
		
	}

	/**
	 * 删除上一次的统计的某一段时间内已发货的产品销售信息
	 * @param end_date 
	 * @throws Exception 
	 */
	private void deleteLastProductSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			floorProductSalesMgr.deleteLastProductSales(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteLastProductSales(PageCtrl pc)",log);
		}
		
	}

	/**
	 * 查询所有国家的产品的销售额、利润、成本的分布情况
	 */
	public void statsAllCountryProductSales(long catalog_id, long p_line_id,long cid,String cmd, String p_name, String order_source,String monetary_unit,String start_date, String end_date)
			throws Exception 
	{
				try 
				{
					double usd = Double.valueOf(systemConfig.getStringConfigValue("USD"));
					// 创建根节点 
					Element root = new Element("map");
					Document Doc =new Document(root);    // 根节点添加到文档中； 

					/**
					 * 给map根节点设置属性
					 */
					if(monetary_unit.equals("USD"))
					{
						root.setAttribute("numberPrefix","$");   // 数字前缀[类似于$345.90]
					}
					else
					{
						root.setAttribute("numberPrefix","RMB ");   // 数字前缀[类似于$345.90]
					}
					root.setAttribute("baseFontSize","9");   //字体大小
					root.setAttribute("borderColor","005879");
					root.setAttribute("animation","0");
					root.setAttribute("fillColor","#FEA7F8");//FEA7F8
					root.setAttribute("showLabels","1");   //是否显示实体的labels
					root.setAttribute("includeValueInLabels","1");
					root.setAttribute("canvasBorderColor","FFFFFF");
					root.setAttribute("borderColor","FFFFFF");
					root.setAttribute("hoverColor","FFFFFF");
					//root.setAttribute("labelSepChar",": ");
					
					Element changeColor = new Element("colorRange");
					root.addContent(changeColor);

					double max = 0;
					double min = 0;
					List list = new ArrayList();
					long pid = 0;
					
					
					Element second_ele = new Element("data");
					root.addContent(second_ele);

					DBRow [] countries = floorSupplierMgr.getAllCountryCode();
					DecimalFormat df = new DecimalFormat("#.00");
					DBRow row = null;
					
					
					for (int i = 0; i < countries.length; i++) 
					{
						Element elements = new Element("entity");
						
						elements.setAttribute("id",countries[i].getString("fusion_id"));
						
						
						elements.setAttribute("displayValue",countries[i].getString("c_code"));
						elements.setAttribute("link","javascript:drillDown("+countries[i].getString("fusion_id")+",'"+cmd+"',"+catalog_id+",'"+p_line_id+"',"+pid+",'"+order_source+"','"+monetary_unit+"','"+start_date+"','"+end_date+"','"+cid+"')");

						String catalog_ids = "";
						
						DBRow product = floorProductMgr.getDetailProductByPname(p_name);
						
						if(product != null)   
						{
							pid = product.get("pc_id", 0l);
						}
						
						row = floorProductSalesMgrZJ.getAllCountryProductSalesOrProfitOrCost(countries[i].get("ccid", 0l),cmd,catalog_id,p_line_id,cid,pid,order_source,start_date, end_date);
						
						if(row != null)
						{
							double price = row.get("price", 0d);
							
							if(monetary_unit.equals("USD"))
							{
								
								price = price/usd;
							}
							
							String total_price = df.format(price);
							elements.setAttribute("value",total_price);
							 
							list.add(total_price);
						}
						else
						{
							elements.setAttribute("value","0");
						}
						
						second_ele.addContent(elements);
						
					}
					
					if(list.toArray().length != 0)
					{
						max = Double.parseDouble(list.get(0).toString());
						min = Double.parseDouble(list.get(0).toString());
								
						for (int j = 0; j < list.toArray().length; j++) 
						{
							if(Double.parseDouble(list.get(j).toString()) > max)
							{
							     max = Double.parseDouble(list.get(j).toString());
							}
							if(Double.parseDouble(list.get(j).toString()) < min)
							{
							     min = Double.parseDouble(list.get(j).toString());
							}
							
						}
						
					}
					double num = max/10;
					MapColorKey mapColorKey = new MapColorKey();
					
					for (int i = 1; i <= 10; i++)   //根据最大值和最小值分成十段不同的颜色
					{
						 DecimalFormat format = new DecimalFormat("#");
						 Element color = new Element("color");  
						 int forMin = 0;
						 int forMax = 0;
						 if(min<num*i)
						 {
							if(i==1)
							{
								forMin = (int)min;
								if(forMin<0)
								{
									forMin = forMin-1;
								}
							}
							else
							{
								forMin = (int)(num*(i-1));
							}
							
							if(i==10)
							{
								forMax = (int)(num*i+1);
							}
							else
							{
								forMax = (int)(num*i);
							}
							
						 }
						 else
						 {
							 if(i==10)
							 {
								forMin = (int)min;
								forMax = (int)max+1;
							 }
							 
						 }
						 
						color.setAttribute("minValue",""+format.format(forMin)+"");
						color.setAttribute("maxValue",""+format.format(forMax)+"");
						color.setAttribute("displayValue",(int)forMin+"-"+(int)forMax);
						color.setAttribute("color",mapColorKey.getMapColorKeyById(i));    //
						changeColor.addContent(color);
					}
					XMLOutputter XMLOut = new XMLOutputter();
					String path = Environment.getHome()+"administrator/product_sales/fusionMaps/mapsXml/";
					XMLOut.output(Doc,new FileOutputStream(path + "worlds.xml"));
				} 
				catch (Exception e) 
				{
					throw new SystemException(e,"statsAllCountryProductSales(long catalog_id, long p_line_id, String cmd)",log);
				}
		
	}
	
	/**
	 * 根据国家ID获取该国家区域的产品销售额/利润和成本的分布情况
	 */
	@SuppressWarnings("unchecked")
	public void getProvinceProductSalesRommByFusionId(String fusion_id,long catalog_id,String cmd, long p_line_id, long product_id, String order_source,String monetary_unit,String start_date, String end_date,long cid) 
		throws Exception 
	{
		try 
		{
			double usd = Double.parseDouble(systemConfig.getStringConfigValue("USD"));
			// 创建根节点 
			Element root = new Element("map");
			   
			Document Doc =new Document(root);    // 根节点添加到文档中； 

			/**
			 * 给map根节点设置属性
			 */
			if(monetary_unit.equals("USD"))
			{
				root.setAttribute("numberPrefix","$");
			}
			else
			{
				root.setAttribute("numberPrefix","RMB "); 
			}
			  // 数字前缀[类似于$345.90]
			root.setAttribute("baseFontSize","9");   //字体大小
			root.setAttribute("borderColor","005879");
			root.setAttribute("animation","0");
			root.setAttribute("fillColor","#FEA7F8");
			root.setAttribute("showLabels","1");   //是否显示实体的labels
			root.setAttribute("includeValueInLabels","1");
			root.setAttribute("canvasBorderColor","FFFFFF");
			root.setAttribute("borderColor","FFFFFF");
			root.setAttribute("hoverColor","FFFFFF");
			
			 
			Element changeColor = new Element("colorRange");
			root.addContent(changeColor);
			
			
			double max = 0;
			double min = 0;
			List list = new ArrayList();
			
			
			Element second_ele = new Element("data");
			root.addContent(second_ele);
			
			DBRow country = floorOrderProcessMgr.getCountryByfusion_id(fusion_id);
			if(country != null)
			{
				DBRow [] provinces = floorSupplierMgr.getProvinceByCountryId(country.get("ccid", 0l));
			    DecimalFormat df = new DecimalFormat("#.00");
				for (int i = 0; i < provinces.length; i++) 
				{
					Element elements = new Element("entity");
					
					elements.setAttribute("id",provinces[i].getString("fusion_id"));
					
					elements.setAttribute("displayValue", provinces[i].getString("p_code"));
					
					DBRow province_price = floorProductSalesMgrZJ.getProvinceSalesOrProfitOrCostByOrderSource(provinces[i].get("pro_id", 0l),cmd,product_id,catalog_id,p_line_id,order_source, start_date, end_date,cid);
					
					if(province_price != null)
					{
						double price = province_price.get("price", 0d);
						
						if(monetary_unit.equals("USD"))
						{
							
							price = price/usd;
						}
						String total_price = df.format(price); 
						elements.setAttribute("value",total_price);
						
						list.add(total_price);
					}
					else
					 {
						 elements.setAttribute("value","0");
					 }
	
					second_ele.addContent(elements);
				}
			}
			
			
			if(list.toArray().length != 0)
			{
				min = Double.parseDouble(list.get(0).toString());
				max = Double.parseDouble(list.get(0).toString());
				for (int k = 0; k < list.toArray().length; k++) 
				{
					if(max < Double.parseDouble(list.get(k).toString()))
					{
						max = Double.parseDouble(list.get(k).toString());
					}
					if(min > Double.parseDouble(list.get(k).toString()))
					{
						min = Double.parseDouble(list.get(k).toString());
					}
				}
			}
			
			
			
			double num = max/10;
			
			MapColorKey mapColorKey = new MapColorKey();
			
			for (int i = 1; i <= 10; i++)   //根据最大值和最小值分成十段不同的颜色
			{
				 DecimalFormat format = new DecimalFormat("#");
				 Element color = new Element("color");  
				 int forMin = 0;
				 int forMax = 0;
				 if(min<num*i)
				 {
					if(i==1)
					{
						forMin = (int)min;
						if(forMin<0)
						{
							forMin = forMin-1;
						}
					}
					else
					{
						forMin = (int)(num*(i-1));
					}
					
					if(i==10)
					{
						forMax = (int)(num*i+1);
					}
					else
					{
						forMax = (int)(num*i);
					}
					
				 }
				 
				 else
				 {
					 if(i==10)
					 {
						forMin = (int)min;
						forMax = (int)max+1;
					 }
				 }
				 
				color.setAttribute("minValue",""+format.format(forMin)+"");
				color.setAttribute("maxValue",""+format.format(forMax)+"");
				color.setAttribute("displayValue",(int)forMin+"-"+(int)forMax);
				color.setAttribute("color",mapColorKey.getMapColorKeyById(i));    //
				changeColor.addContent(color);
			}
			 
			XMLOutputter XMLOut = new XMLOutputter();
			String path = Environment.getHome()+"administrator/product_sales/fusionMaps/mapsXml/";
			XMLOut.output(Doc,new FileOutputStream(path + "province.xml"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProvinceProductSalesRommByFusionId(String fusion_id,long catalog_id,String cmd, long p_line_id)",log);
		}
	}
	

	/**
	 * 获取某一时间段内所有的产品销售的信息
	 */
	public DBRow [] getAllProductSalesByDate(String start_date, String end_date)
		throws Exception 
	{
		
		try 
		{
			return (floorProductSalesMgr.getAllProductSalesByDate(start_date,end_date));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductSalesByDate(String start_date, String end_date)",log);
		}
	}

	
	/**
	 * 根据不同的条件查询过滤产品销售的情况
	 */
	public DBRow[] getAllProductSalesByCondition(String input_st_date,String input_en_date, long catalog_id, long pro_line_id,String product_name, String order_source, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
			long product_id = 0;
			if(product!=null)
			{
				product_id = product.get("pc_id",0l);
			}
			
			return (floorProductSalesMgrZJ.fillterProductSalesZJ(product_id, catalog_id, pro_line_id, order_source,input_st_date,input_en_date, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductSalesByCondition",log);
		}
		
	}
	
	public DBRow[] getAllProductSalesByCondition(String input_st_date,String input_en_date, long catalog_id, long pro_line_id,String product_name, long ca_id,long ccid,long pro_id,long cid,int type,String order_source,String day_source,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
			long product_id = 0;
			if(product!=null)
			{
				product_id = product.get("pc_id",0l);
			}
			
			return (floorProductSalesMgrZJ.salesDeliveryAnalysis(input_st_date, input_en_date, catalog_id, pro_line_id, product_id, ca_id, ccid, pro_id, cid, type, order_source,day_source,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductSalesByCondition",log);
		}
		
	}
	
	/**
	 * 获得最后一条记录的发货时间
	 * @return
	 * @throws Exception
	 */
	public String productSalesLastTime()
		throws Exception
	{
		try 
		{
			String time;
			DBRow row = floorProductSalesMgrZJ.productSalesLastTime();
			if(row !=null)
			{
				time = row.getString("delivery_date").substring(0,10); 
			}
			else
			{
				time = DateUtil.FormatDatetime("yyyy-MM-dd");
			}
			
			return (time);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"ProductSalesLastTime",log);
		}
	}
	
	public String exportSalesDeliveryAnalysis(HttpServletRequest request)
		throws Exception
	{
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long pro_line_id = StringUtil.getLong(request,"pro_line_id");
		String product_name = StringUtil.getString(request,"product_name");
		String input_st_date = StringUtil.getString(request,"input_st_date");
		String input_en_date = StringUtil.getString(request,"input_en_date");
	
		long ccid = StringUtil.getLong(request,"ccid");
		long pro_id = StringUtil.getLong(request,"pro_id");
		long ca_id = StringUtil.getLong(request,"ca_id");
		int type = StringUtil.getInt(request,"type");//1代表根据选择地域求和统计，2代表根据选择地域的下一级地域分布统计
		
		String order_source = StringUtil.getString(request,"order_source");
		long cid = StringUtil.getLong(request,"cid",0l);//发货仓库ID
		String day_source = StringUtil.getString(request,"day_source");
		
		DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
		long pc_id = 0;
		if(product!=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		DBRow DateRow[]= floorOrderProcessMgr.getDate(input_st_date, input_en_date);
		DBRow[] rowsValue = floorProductSalesMgrZJ.salesDeliveryAnalysis(input_st_date, input_en_date, catalog_id, pro_line_id, pc_id, ca_id, ccid, pro_id, cid, type,order_source, day_source, null);
		
		if (rowsValue.length > 0) 
		{
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/order_process/ExportPlanAndStock.xlsx"));  
			XSSFSheet sheet= wb.getSheet("Sheet1");  
			
			XSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style.setLocked(false); //创建样式
			style.setWrapText(true);
			
			XSSFFont  font =wb.createFont();
			font.setFontName("Arial");   
			font.setFontHeightInPoints((short)10);   
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
			
			XSSFCellStyle styleTitle = wb.createCellStyle();
			styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleTitle.setLocked(false);  
			styleTitle.setWrapText(true);
			styleTitle.setFont(font);
			
			XSSFCellStyle stylelock = wb.createCellStyle();
			stylelock.setLocked(true); 
			stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
			//title赋值
			XSSFRow row = sheet.createRow(0);  
			
			
			row.createCell(0).setCellValue("所属产品线");
			row.getCell(0).setCellStyle(styleTitle);
			
			row.createCell(1).setCellValue("商品分类");
			row.getCell(1).setCellStyle(styleTitle);
			
			row.createCell(2).setCellValue("产品名称");
			row.getCell(2).setCellStyle(styleTitle);
			
			row.createCell(3).setCellValue("发货仓库");
			row.getCell(3).setCellStyle(styleTitle);
			
			row.createCell(4).setCellValue("来源");
			row.getCell(4).setCellStyle(styleTitle);
			
			row.createCell(5).setCellValue("销售区域");
			row.getCell(5).setCellStyle(styleTitle);
			
			int blankCell = 5;//时间显示从第几列开始
			if(ca_id>0)
			{
				blankCell++;
				row.createCell(blankCell).setCellValue("销售国家");
				row.getCell(blankCell).setCellStyle(styleTitle);
			}
			if(ccid>0)
			{
				blankCell++;
				row.createCell(blankCell).setCellValue("销售地区");
				row.getCell(blankCell).setCellStyle(styleTitle);
			}
				
			blankCell++;
			row.createCell(blankCell).setCellValue("总发货数");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("平均发货数");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("商品总成本");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("总运费");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("总重量");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("总销售额");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			row.createCell(blankCell).setCellValue("总利润");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			for (int i = 0; i < DateRow.length; i++)
			{
				
				row.createCell(i+blankCell).setCellValue(DateRow[i].getString("date"));
				row.getCell(i+blankCell).setCellStyle(styleTitle);	
			}
			//value赋值
			
			for (int i = 0; i < rowsValue.length; i++)
			{
				blankCell = 5;
				row = sheet.createRow(i+1); 
				DBRow rowTemp = rowsValue[i];
				
				row.createCell(0).setCellValue(rowTemp.getString("pl_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(rowTemp.getString("title"));
				row.getCell(1).setCellStyle(style);

				row.createCell(2).setCellValue(rowTemp.getString("p_name"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(cid==0?"ALL":rowTemp.getString("storage_title"));
				row.getCell(3).setCellStyle(style);
				
				row.createCell(4).setCellValue(order_source.equals("")?"ALL":rowTemp.getString("order_source"));
				row.getCell(4).setCellStyle(style);
				
				row.createCell(5).setCellValue(ca_id==0&&type==1?"ALL":rowTemp.getString("area_name"));
				row.getCell(5).setCellStyle(style);
				
				if(ca_id>0)
				{
					blankCell++;
					row.createCell(blankCell).setCellValue(ccid==0&&type==1?"ALL":rowTemp.getString("c_country"));
					row.getCell(blankCell).setCellStyle(style);
				}
				if(ccid>0)
				{
					blankCell++;
					row.createCell(blankCell).setCellValue(pro_id==0&&type==1?"ALL":rowTemp.getString("pro_name"));
					row.getCell(blankCell).setCellStyle(style);
				}
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_quantity",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				
				blankCell++;
				row.createCell(blankCell).setCellValue(MoneyUtil.round(rowTemp.get("sum_quantity",0d)/DateRow.length,2));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				 
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_product_cost",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_freight",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_weight",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_saleroom",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_profit",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				
				blankCell++;
				for (int j = 0; j < DateRow.length; j++)
				{
					row.createCell(j+blankCell).setCellValue(rowTemp.get("date_"+j,0d));
					row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(j+blankCell).setCellStyle(style);	
				}
			}
			String day = "";
			if(day_source.equals("sum_quantity"))
			{
				day = "每日发货";
			}
			if(day_source.equals("sum_saleroom"))
			{
				day = "每日销售额";
			}
			if(day_source.equals("sum_profit"))
			{
				day = "每日利润";
			}
			
			//写文件  
			String path = "upl_excel_tmp/销售发货分析"+"-"+day+"_"+input_st_date+"至"+input_en_date+".xlsx";
			OutputStream os= new FileOutputStream(Environment.getHome()+path);
			wb.write(os);  
			os.close();  
			return (path);
		}
		else
		{
			return "0";
		}
	}

	public void setFloorProductSalesMgr(FloorProductSalesMgrTJH floorProductSalesMgr) {
		this.floorProductSalesMgr = floorProductSalesMgr;
	}


	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}


	public void setFloorSupplierMgr(FloorSupplierMgrTJH floorSupplierMgr) {
		this.floorSupplierMgr = floorSupplierMgr;
	}


	public void setFloorOrderProcessMgr(FloorOrderProcessMgrTJH floorOrderProcessMgr) {
		this.floorOrderProcessMgr = floorOrderProcessMgr;
	}


	public void setFloorProductSalesMgrZJ(
			FloorProductSalesMgrZJ floorProductSalesMgrZJ) {
		this.floorProductSalesMgrZJ = floorProductSalesMgrZJ;
	}


	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
}
