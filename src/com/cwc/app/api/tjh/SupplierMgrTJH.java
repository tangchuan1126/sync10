package com.cwc.app.api.tjh;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.supplier.HaveUploadSupplierException;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class SupplierMgrTJH implements SupplierMgrIFaceTJH {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorSupplierMgrTJH floorSupplierMgr;

	/**
	 * 查询所有的供应商
	 * @throws Exception 
	 */
	public DBRow[] getAllSupplier(PageCtrl pc) 
		throws Exception 
	{
		
		try 
		{
			return (floorSupplierMgr.getAllSupplier(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllSupplier(pc)",log);
		}
	}
	
	/**
	 * 添加供应商的相关信息
	 * @throws Exception 
	 */
	public void addSupplier(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			String name = StringUtil.getString(request, "sup_name");
//			String nation = StrUtil.getString(request, "nation");
//			String province = StrUtil.getString(request, "province");
			
			long nation = StringUtil.getLong(request, "nation");
			long province = StringUtil.getLong(request, "province");
			String linkman = StringUtil.getString(request, "linkman");
			String address = StringUtil.getString(request, "address");
			String phone = StringUtil.getString(request, "phone");
			String legal_name = StringUtil.getString(request, "legal_name");
			String other_contacts = StringUtil.getString(request, "other_contacts");
			String bank_information = StringUtil.getString(request, "bank_information");
			
			row.add("sup_name", name);
			row.add("nation_id", nation);
			row.add("province_id", province);
			row.add("linkman", linkman);
			row.add("address", address);
			row.add("phone", phone);
			row.add("legal_name", legal_name);
			row.add("bank_information", bank_information);
			row.add("other_contacts", other_contacts);
			
			floorSupplierMgr.addSupplier(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addSupplier(request)",log);
		}
		
	}

	/**
	 * 删除供应商信息
	 * @throws Exception 
	 */
	public void delSupplier(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long s_id = StringUtil.getLong(request, "s_id");
			floorSupplierMgr.delSupplier(s_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delSupplier(request) ",log);
		}
		
	}
	
	/**
	 * 查看某一具体的供应商信息
	 */
	public DBRow getDetailSupplier(long sid) 
		throws Exception 
	{
		try 
		{
			return floorSupplierMgr.getDetailSupplier(sid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailSupplier(sid)",log);
		}
		
	}
	
	/**
	 * 修改某一供应商信息
	 * @throws Exception 
	 */
	public void modSupplier(HttpServletRequest request) 
		throws Exception 
	{
		
		try 
		{
			DBRow row = new DBRow();
			
			String name = StringUtil.getString(request, "sup_name");
			long nation = StringUtil.getLong(request, "nation");
			long province = StringUtil.getLong(request, "province");
			String linkman = StringUtil.getString(request, "linkman");
			String address = StringUtil.getString(request, "address");
			String phone = StringUtil.getString(request, "phone");
			String legal_name = StringUtil.getString(request, "legal_name");
			String other_contacts = StringUtil.getString(request, "other_contacts");
			String bank_information = StringUtil.getString(request, "bank_information");
			long sid = StringUtil.getLong(request, "sid");
			String password = StringUtil.getString(request, "password");
			if(!password.equals("") && password != null)
			{
				row.add("password", StringUtil.getMD5(password));
			}
			
			row.add("sup_name", name);
			row.add("nation_id", nation);
			row.add("province_id", province);
			row.add("linkman", linkman);
			row.add("address", address);
			row.add("phone", phone);
			row.add("legal_name", legal_name);
			row.add("bank_information", bank_information);
			row.add("other_contacts", other_contacts);
			
			
			floorSupplierMgr.modSupplier(row,sid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modSupplier(HttpServletRequest request)",log);		
		}
	}
	
	/**
	 * 上产供应商的资质文件
	 */
	public void uploadQualification(HttpServletRequest request)
			throws HaveUploadSupplierException,Exception 
	{
		 try 
		 {
			 	TUpload upload = new TUpload();
				
				String msg=new String();
				
				upload.setFileName("upload_supplier"+new Date().getTime());
				upload.setRootFolder("/administrator/supplier/upload_supplier/");
								
				upload.setPermitFile("xls,txt,rar,zip,jpg,doc,gif,docx");
				
				int size = request.getContentLength();
				int maxsize = 1048576;
				if(size > maxsize)
				{
					throw new HaveUploadSupplierException();
				}
				else
				{
					
					 	int flag = upload.upload(request);
					   
					   long id = Long.parseLong(upload.getRequestRow().getString("sid"));
					   
					   String path = upload.getFileName();
					   
					   DBRow row = new DBRow();
					   row.add("ap_name", path);
					   row.add("sup_id", id);
					   
					   if(flag == 1)
					   {
						   	msg = "上传出错";
							////system.out.println(msg);
					   }
					   else if(flag == 2)
					   {
						   msg = "上传的格式错误";
						   ////system.out.println(msg);
					   }
					   else
					   {
						   msg = upload.getFileName();
						   floorSupplierMgr.addAptitude(row);
					   }
				}
			   
		}
		 catch (HaveUploadSupplierException e) {
			throw e;
		}
		 catch (Exception e) 
		 {
			throw new SystemException(e,"uploadQualification(request)",log);
		}
		
	}

	/**
	 * 根据供应商的id查询该供应商的资质文件
	 */
	public DBRow[] getAptitudeBySupId(long id) 
		throws Exception 
	{
		
		try 
		{
			return floorSupplierMgr.getAptitudeBySupId(id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAptitudeBySupId(id)",log);
		}
	}
	
	/**
	 * 删除供应商的资质文件
	 * @throws Exception 
	 */
	public void delSupplierOfAptitude(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long ap_id = StringUtil.getLong(request, "ap_id");
			DBRow row = floorSupplierMgr.getDetailSupplierOfAptitude(ap_id);
			if (row != null) {
				FileUtil.delFile(Environment.getHome()+"administrator/supplier/upload_supplier/"+row.getString("ap_name"));
			}
			
			floorSupplierMgr.delSupplierOfAptitude(ap_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delSupplierOfAptitude(request)",log);
		}
		
	}

	
	/**
	 * 模糊查询供应商信息
	 */
	public DBRow [] getSearchSupplier(String key,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorSupplierMgr.getSearchSupplier(key,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchSupplier(request)",log);
		}
		
	}
	
	/**
	 * 获取全部国家名称
	 */
	public DBRow[] getAllCountryCode() 
		throws Exception 
	{
		try 
		{
			return (floorSupplierMgr.getAllCountryCode());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllCountryCode()",log);
		}
	}
	
	/**
	 * 根据国家id获取该国家下的省、州
	 * @throws Exception 
	 */
	public DBRow[] getProvinceByCountryId(long nation_id) 
		throws Exception 
	{
		try 
		{
			return (floorSupplierMgr.getProvinceByCountryId(nation_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProvinceByCountryId(long nation_id)",log);
		}
	}

	
	/**
	 * 获取国家具体信息
	 */
	public DBRow getDetailCountry(long nation_id) 
		throws Exception 
	{
		try 
		{
			return (floorSupplierMgr.getDetailCountry(nation_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailCountry(long nation_id) ",log);
		}
	}

	/**
	 * 根据id获取省、州的具体信息
	 */
	public DBRow getDetailProvinceById(long provinceId) 
		throws Exception 
	{
		return (floorSupplierMgr.getDetailProvinceById(provinceId));
	}

	
	public void setFloorSupplierMgr(FloorSupplierMgrTJH floorSupplierMgr) {
		this.floorSupplierMgr = floorSupplierMgr;
	}

	
}
