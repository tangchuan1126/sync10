package com.cwc.app.api.tjh;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.iface.tjh.ProductLineMgrIFaceTJH;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProductLineMgrTJH implements ProductLineMgrIFaceTJH {
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorProductLineMgrTJH floorProductLineMgr;
	
	private FloorCatalogMgr floorCatalogMgr;
	
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	/**
	 * 获取所有的产品线定义信息
	 */
	public DBRow[] getAllProductLine() 
		throws Exception 
	{
		try 
		{
			return (floorProductLineMgr.getAllProductLine());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllProductLine()",log);
		}
	}

	/**
	 * 根据产品线id获取该产品下的产品类别
	 */
	
	public DBRow[] getProductCatalogByProductLineId(long productLineId)
			throws Exception 
	{
		try 
		{
			return (floorProductLineMgr.getProductCatalogByProductLineId(productLineId));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCatalogByProductLineId(long productLineId)",log);
		}
	}
	
	public DBRow[] getProductCatalogByProductLineId(long productLineId,long parent_catalog_id) 
		throws Exception
	{
		try 
		{
			return (floorProductLineMgr.getProductCatalogByProductLineId(productLineId, parent_catalog_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCatalogByProductLineId(long productLineId,long parent_catalog_id)",log);
		}
	}
	
	/**
	 * 添加产品线[不能和产品重名]
	 * @param Stirng[name]
	 * @author subin
	 * */
	public DBRow addProductLine(HttpServletRequest request) throws Exception {
		
		try {
			
			String name = StringUtil.getString(request ,"name");
			
			DBRow resultRow = new DBRow();
			
			//产品分类和产品线都不可重名
			if(null == floorProductLineMgr.getProductLineByName(name) && 0 == floorCatalogMgr.getProductCatalogsByName(name).length ){
				AdminMgr am = new AdminMgr();
				AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true));
				DBRow params = new DBRow();
				params.add("name", name);
				params.add("creator", adminLoggerBean.getAdid());
				params.add("create_time", DateUtil.NowStr());
				floorProductLineMgr.addProductLine(params);
				
				resultRow.add("flag", "true");
			} else {
				
				resultRow.add("flag", "false");
			}
			
			return resultRow;
			
		} catch (Exception e) {
			throw new SystemException(e,"addProductLine(HttpServletRequest request)",log);
		}
	}

	/**
	 * 产品线关联产品分类
	 * @param [product_line_id,catalog_id]
	 * @author subin
	 */
	public void addProductCatalogById(HttpServletRequest request) throws Exception {
		
		try {
			
			long p_line_id = StringUtil.getLong(request, "product_line_id");
			String catalogs = StringUtil.getString(request, "catalog_id");
			
			DBRow param = new DBRow();
			param.add("product_line_id", p_line_id);
			
			if(catalogs != null && !catalogs.equals("")){
				
				String[] catalogArray = catalogs.split(",");
				
				DBRow[] lineRow = new DBRow[catalogArray.length];
				
				//查询分类对应的产品线
				for(int i=0;i<catalogArray.length;i++){
					
					lineRow[i] = floorCatalogMgr.getProductCatalogById(Long.valueOf(catalogArray[i]));
				}
				
				//清空关联的产品线
				floorProductLineMgr.emptyProductLineForProductCatalog(p_line_id);
				
				//更新关联的产品线
				for(String oneResult:catalogArray){
					
					floorProductLineMgr.updateProductCatalogByPId(Long.valueOf(oneResult), param);
				}
				
				//1.清除产品线与原title的关系,将现在的产品分类关联的title的总和关联到产品线
				for(DBRow oneResult:lineRow){
					
					this.updateCustomerTitleAndLine(oneResult.get("product_line_id",0L),oneResult.get("id",0L));
				}
				
			}else{
				
				//清空关联的产品线
				floorProductLineMgr.emptyProductLineForProductCatalog(p_line_id);
				
				//2.清空产品线与title的关系
				floorCatalogMgr.deleteTitleProductLineByline(p_line_id);
			}
			
		} catch (Exception e) {
			
			throw new SystemException(e,"addProductCatalogById(HttpServletRequest request)",log);
		}
	}
	
	/*************************************************************************************/
	
	private void updateCustomerTitleAndLine(long beforeLineId,long categoryId) throws Exception {
		
		//比较分类原产品线与现产品线是否一样
		DBRow category = floorCatalogMgr.getProductCatalogById(categoryId);
		
		if(category.get("product_line_id",0L) != beforeLineId){
			
			//原产品线与title的关系
			proprietaryMgrZyj.customerTitleAndLine(beforeLineId);
			
			proprietaryMgrZyj.customerTitleAndLine(category.get("product_line_id",0));
		}
	}
	
	
	/*************************************************************************************/
	/**
	 *1.影响此分类原来的产品线与title的关系
	 *2.影响此分类现在的产品线与title的关系
	 *
	 *@param long[原产品线ID,分类ID]
	 *@author subin
	*/
	@SuppressWarnings("unused")
	private void updateProductLineAndTitle(long beforeLineId,long categoryId) throws Exception {
		
		//比较分类原产品线与现产品线是否一样
		DBRow category = floorCatalogMgr.getProductCatalogById(categoryId);
		
		if(category.get("product_line_id",0L) != beforeLineId){
			
			//原产品线与title的关系
			this.beforeLineAndTitleRelation(beforeLineId);
			
			//现产品线与title的关系
			this.nowLineAndTitleRelation(categoryId);
		}
	}
	
	//原产品线与title的关系
	private void beforeLineAndTitleRelation(long beforeLineId) throws Exception{
		
		if(beforeLineId != 0){
			
			//查询产品线下产品分类与title的关系
			DBRow[] title1 = floorCatalogMgr.getTitleForLineCategoryRelation(beforeLineId);
			//查询产品线与title的关系
			DBRow[] title2 = floorCatalogMgr.getTitleForLineByLineId(beforeLineId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//删除不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					floorCatalogMgr.deleteTitleProductLine(beforeLineId,one.get("title",-1L));
				}
			}
		}
	}
	
	//现产品线与title的关系
	private void nowLineAndTitleRelation(long categoryId) throws Exception{
		
		//查询产品线
		DBRow category = floorCatalogMgr.getProductCategory(categoryId);
		
		//如果存在产品线
		if(category.get("product_line_id",0L) != 0){
			
			//查询此产品线的title
			DBRow[] title1 = floorCatalogMgr.getTitleForLine(categoryId);
			//查询此分类的title
			DBRow[] title2 = floorCatalogMgr.getTitleForCategory(categoryId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//添加不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title",-1L));
					row.put("tpl_product_line_id", category.get("product_line_id",-1L));
					
					floorCatalogMgr.insertTitleProductLine(row);
				}
			}
		}
	}
	
	/*************************************************************************************/

	/**
	 * 修改某一产品线下的产品类别
	 */
	public void modProductLine(long productLineId) 
		throws Exception 
	{
		
		try 
		{
			floorProductLineMgr.modProductLine(productLineId);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modProductLine(long productLineId)",log);
		}
		
	}
	
	/**
	 * 获取某一详细的产品线信息
	 */
	public DBRow getProductLineById(long id) 
		throws Exception 
	{
		try 
		{
			return (floorProductLineMgr.getProductLineById(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductLineById(long id)",log);
		}
	}

	/**
	 * 删除产品线信息
	 */
	public DBRow deleteProductLine(HttpServletRequest request) throws Exception {
		
		try {
			
			DBRow resultRow = new DBRow();
			long id = StringUtil.getLong(request, "id");
			DBRow rows [] = floorProductLineMgr.getProductCatalogByProductLineId(id);
			
			if(rows.length != 0) {
				
				for (int i = 0; i < rows.length; i++) {
					
					DBRow row = new DBRow();
					row.add("product_line_id", null);
					
					//删除产品线与分类的关系
					floorProductLineMgr.deleteProductCatalogByLineId(rows[i].get("id", 0l),row);
					//删除此产品线下分类与title的关系
					//floorProprietaryMgrZyj.deleteProprietaryProductCatagoryById(0L, 0L, rows[i].get("id", 0L));
				}
			}
			
			floorProductLineMgr.deleteProductLine(id);
			resultRow.add("flag", "true");
			//删除此产品线与title的关系
			floorProprietaryMgrZyj.deleteProprietaryProductLineById(0L, 0L, id);
			
			return resultRow;
			
		} catch (Exception e) {
			throw new SystemException(e,"deleteProductLine(HttpServletRequest request)",log);
		}
	}

	/**
	 * 修改某一产品线信息
	 */
	public void modProductLine(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request, "id");
			String name = StringUtil.getString(request, "name");
			DBRow rowByName = getProductLineByName(name);
			boolean name_available =true;
			if(rowByName==null){
				name_available = true;
			}else if(id==rowByName.get("id", 0)){
				name_available = true;
			}else{
				name_available = false;
			}
			
			if(name_available){
				DBRow row = new DBRow();
				row.add("name", name);
				floorProductLineMgr.updateProductLine(id,row);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modProductLine(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 从产品内清除分类
	 * @param request
	 * @throws Exception
	 */
	public void cleanProductCatalogForProductLine(HttpServletRequest request) throws Exception {
		
		try {
			
			long product_line_id = StringUtil.getLong(request,"product_line_id");
			
			long catalog_id = StringUtil.getLong(request,"catalog_id");
			
			DBRow[] category = floorCatalogMgr.getMaxProductCatalogByLineId(product_line_id);
			
			StringBuffer buff = new StringBuffer();
			
			for(DBRow one : category){
				
				if(catalog_id != one.get("id",0)){
					
					buff.append(","+one.get("id",0));
				}
			}
			
			String categoryId = buff.toString().length() > 0 ? buff.toString().substring(1) : buff.toString();
			
			this.addProductCatalogById(product_line_id, categoryId);
			
			/*DBRow[] catalogs = floorProductLineMgr.getSonProductCatalogByProductLiineId(product_line_id, catalog_id);
			
			for (int i = 0; i < catalogs.length; i++) {
				DBRow para = new DBRow();
				
				para.add("product_line_id",null);
				
				floorCatalogMgr.modProductCatalog(catalogs[i].get("id",0l),para);
			}*/
			
		} catch (Exception e) {
			throw new SystemException(e,"cleanProductCatalogForProductLine",log);
		}
	}
	
	private void addProductCatalogById(long p_line_id, String catalogs) throws Exception {
		
		try {
			
			DBRow param = new DBRow();
			param.add("product_line_id", p_line_id);
			
			if(catalogs != null && !catalogs.equals("")){
				
				String[] catalogArray = catalogs.split(",");
				
				DBRow[] lineRow = new DBRow[catalogArray.length];
				
				//查询分类对应的产品线
				for(int i=0;i<catalogArray.length;i++){
					
					lineRow[i] = floorCatalogMgr.getProductCatalogById(Long.valueOf(catalogArray[i]));
				}
				
				//清空关联的产品线
				floorProductLineMgr.emptyProductLineForProductCatalog(p_line_id);
				
				//更新关联的产品线
				for(String oneResult:catalogArray){
					
					floorProductLineMgr.updateProductCatalogByPId(Long.valueOf(oneResult), param);
				}
				
				//1.清除产品线与原title的关系,将现在的产品分类关联的title的总和关联到产品线
				for(DBRow oneResult:lineRow){
					
					this.updateCustomerTitleAndLine(oneResult.get("product_line_id",0L),oneResult.get("id",0L));
				}
				
			}else{
				
				//清空关联的产品线
				floorProductLineMgr.emptyProductLineForProductCatalog(p_line_id);
				
				//2.清空产品线与title的关系
				floorCatalogMgr.deleteTitleProductLineByline(p_line_id);
			}
			
		} catch (Exception e) {
			
			throw new SystemException(e,"addProductCatalogById(HttpServletRequest request)",log);
		}
	}

	//根据产品线名称获取产品线信息
	public DBRow getProductLineByName(String name) throws Exception{
		try {
			return (floorProductLineMgr.getProductLineByName(name));
		} catch (Exception e) {
			throw new SystemException(e,"getProductLineByName(name)",log);
		}
	}
	public void setFloorProductLineMgr(FloorProductLineMgrTJH floorProductLineMgr) {
		this.floorProductLineMgr = floorProductLineMgr;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setFloorProprietaryMgrZyj(
			FloorProprietaryMgrZyj floorProprietaryMgrZyj) {
		this.floorProprietaryMgrZyj = floorProprietaryMgrZyj;
	}
	
	/**
	 * 查询产品线下的产品分类级数
	 * @param request
	 * @throws Exception
	 */
	public DBRow findProductCategoryLevel(HttpServletRequest request) throws Exception {
		try {
			DBRow resultRow = new DBRow();
			long id = StringUtil.getLong(request, "idByTransfer");
			if(id>0) {
				String sql="select count(1) as countVal from pc_child_list pcl where pcl.pc_id in(select pc.id from product_catalog pc where pc.product_line_id="+id+") and pcl.levv=3;";
				DBRow row = getInfoBySql(sql);
				resultRow.add("flag", row.get("countVal", 0l));
			}else {
				resultRow.add("flag", 0l);
			}
			return resultRow;
		}catch (Exception e) {
			throw new SystemException(e,"findProductCategoryLevel",log);
		}
	}
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow getInfoBySql(String sql) throws Exception {
		try {
			DBRow infoRow = dbUtilAutoTran.selectSingle(sql);
			return(infoRow);
		}catch (Exception e) {
			log.error("CatalogMgr.getInfoBySql(sql) error:" + e);
			throw new Exception("getInfoBySql(sql) error:" + e);
		}
	}
}
