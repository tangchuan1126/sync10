package com.cwc.app.api.tjh;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.tjh.FlooStorageSortMgr;
import com.cwc.app.iface.tjh.StorageSortMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageSortMgr implements StorageSortMgrIFace {
	static Logger log = Logger.getLogger("ACTION");
	private FlooStorageSortMgr flooStorageSortMgr;

	
	public void addStorageSort(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String companyName = StringUtil.getString(request, "CompanyName");
			String addressLine1 = StringUtil.getString(request, "AddressLine1");
			String addressLine2 = StringUtil.getString(request, "AddressLine2");
			String addressLine3 = StringUtil.getString(request, "AddressLine3");
			String city = StringUtil.getString(request, "City");
			String divisionCode = StringUtil.getString(request, "DivisionCode");
			String postalCode = StringUtil.getString(request, "PostalCode");
			String countryCode = StringUtil.getString(request, "CountryCode");
			String countryName = StringUtil.getString(request, "CountryName");
			String phoneNumber = StringUtil.getString(request, "PhoneNumber");
			int psid = StringUtil.getInt(request, "ps_id");
			DBRow dbrow = new DBRow();
			dbrow.add("CompanyName", companyName);
			dbrow.add("AddressLine1", addressLine1);
			dbrow.add("AddressLine2", addressLine2);
			dbrow.add("AddressLine3", addressLine3);
			dbrow.add("City", city);
			dbrow.add("DivisionCode", divisionCode);
			dbrow.add("PostalCode", postalCode);
			dbrow.add("CountryCode", countryCode);
			dbrow.add("CountryName", countryName);
			dbrow.add("PhoneNumber", phoneNumber);
			dbrow.add("ps_id", psid);

			flooStorageSortMgr.addStorageSort(dbrow);

		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "addStorageSort", log);

		}
	}

	public DBRow[] getDetailStorageSortById(long id) 
		throws Exception 
	{
		try 
		{
			
			return (flooStorageSortMgr.getDetailStorageSortById(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getDetailStorageSortById", log);
		}

	}
	
	
	public void setFlooStorageSortMgr(FlooStorageSortMgr flooStorageSortMgr) {
		this.flooStorageSortMgr = flooStorageSortMgr;
	}


}
