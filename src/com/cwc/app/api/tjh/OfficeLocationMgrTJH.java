package com.cwc.app.api.tjh;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.assets.HaveDelOfficeLocationException;
import com.cwc.app.floor.api.slj.FloorOfficeAddressMgrSlj;
import com.cwc.app.floor.api.tjh.FloorOfficeLocationMgrTJH;
import com.cwc.app.iface.tjh.OfficeLocationMgrIFaceTJH;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class OfficeLocationMgrTJH implements OfficeLocationMgrIFaceTJH {
	
	private FloorOfficeLocationMgrTJH floorOfficeLocation;
	private FloorOfficeAddressMgrSlj floorOfficeAddressMgr;
	

	static Logger log = Logger.getLogger("ACTION");

	/**
	 * 获取办公地点的tree
	 */
	public DBRow[] getOfficeLocationTree() throws Exception {
		Tree tree = new Tree(ConfigBean.getStringValue("office_location"));
		DBRow dbrow [] = tree.getTree();
		return (dbrow);
	}
	



	
	/**
	 * 添加办公地点
	 */
//	public void addOfficeAddress(DBRow row) throws Exception {
//		try 
//		{
//			
//			floorOfficeLocation.addOfficeLocation(row);
//			
//			
//		} 
//		catch (Exception e) 
//		{
//			//throw new SystemException(e, "addOfficeAddress(row)", log);
//			throw new Exception("OfficeLocationMgrTJH.addOfficeAddress(DBRow row) error:" + e);
//		}
//		
//	}
//	
public DBRow addOfficeAddress(DBRow row,HttpServletRequest request) throws Exception{
		
		try {
			//获得创建人
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long created_byId = adminLoginBean.getAdid();
		
			////system.out.println("adid:"+created_byId);
			DBRow result = new DBRow();
			
			boolean validate = true;
			int parentid = row.get("parentid",0);
		//	//system.out.println("FU:"+parentid);
			//验证
			DBRow[] roleRow =floorOfficeLocation.getOfficeLocationNmae(row.getString("office_name"),parentid);
			
			
			if(roleRow != null&&roleRow.length>0){
				
				validate = false;
				result.add("error", "Office address already exists");
			}
			
			if(validate){
			 
			 if(parentid!=0){
				
				 DBRow officeAddress=  floorOfficeLocation.getDetailOfficeLocation(parentid);
			//	 //system.out.println(officeAddress.getString("house_number"));
				 row.add("house_number", officeAddress.getString("house_number")); 
				 
				 row.add("street", officeAddress.getString("street"));
				 row.add("city", officeAddress.getString("city")); 
				 row.add("zip_code", officeAddress.getString("zip_code"));
				 row.add("provinces", officeAddress.getString("provinces")); 
				 row.add("nation", officeAddress.getString("nation"));
				 row.add("contact", officeAddress.getString("contact")); 
				 row.add("phone", officeAddress.getString("phone"));
				 row.add("provinces_name", officeAddress.getString("provinces_name")); 
				 row.add("nation_name", officeAddress.getString("nation_name"));
			 }
			row.add("created_by",created_byId)	;
			row.add("creation_date",DateUtil.NowStr());
			floorOfficeLocation.addOfficeLocation(row);
				
				result.add("id",row.getString("id") );
				
			}
			
			result.add("success",validate);
				
			return result;
			
		}catch (Exception e){
			throw new Exception("OfficeLocationMgrTJH.addOfficeAddress(DBRow row) error:" + e);
		}
	}


	/**
	 * 添加办公地点
	 */
	public void addOfficeLocation(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			String location = StringUtil.getString(request, "office_name");
			long parentid = StringUtil.getLong(request, "parentid");
			
			DBRow [] subLocation = floorOfficeLocation.getDetailParentOfficeLocation(parentid);
			int sort;
			if(subLocation.length != 0)
			{
				sort = subLocation[subLocation.length-1].get("sort",0) + 1;
			}
			else
			{
				sort = 1;
			}
			
			row.add("office_name", location);
			row.add("parentid", parentid);
			row.add("sort", sort);
			
			floorOfficeLocation.addOfficeLocation(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "addOfficeLocation(request)", log);
		}
		
	}

	/**
	 * 获取办公地点的详细信息
	 */
	public DBRow getDetailOfficeLocation(long id)
			throws Exception 
	{
		try 
		{
			return (floorOfficeLocation.getDetailOfficeLocation(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailParentOfficeLocation(request)",log);
		}
	}
	
	/**
	 * 修改某一办公地点
	 * @return 
	 */
	public DBRow updateOfficeaddress(DBRow row,long id) throws Exception {

		try 
		{
			DBRow result = new DBRow();
			
			boolean validate = true;
			int parentid = row.get("parentid",0);
			////system.out.println("FU:"+parentid);
			//验证
			DBRow[] roleRow =floorOfficeLocation.getOfficeLocationNmae(row.getString("office_name"),parentid);
			
			if(roleRow != null&&roleRow.length>0){
				
				validate = false;
				result.add("error", "Office address already exists");
			}
			
			if(validate){
			 
		
			floorOfficeLocation.modOfficeLocation(id,row);
			//floorOfficeLocation.addOfficeLocation(row);
				
				result.add("id",row.getString("id") );
				
			}
			
			result.add("success",validate);
				
			return result;
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOfficeaddress(request)",log);
		}
		
	}

	
	/**
	 * 修改某一办公地点的详细信息
	 * @return 
	 */
	public DBRow updateOfficeaddressDetailed(DBRow row,long id) throws Exception {

		try 
		{
			DBRow result = new DBRow();
			floorOfficeLocation.modOfficeLocation(id,row);
			result.add("id",row.getString("id") );
			return result;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOfficeaddressDetailed(request)",log);
		}
		
		
		
	}
	/**
	 * 修改某一办公地点的详细信息
	 */
	public void modOfficeLocation(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			String location = StringUtil.getString(request, "office_name");
			long id = StringUtil.getLong(request, "id");
			
			row.add("office_name", location);
			
			floorOfficeLocation.modOfficeLocation(id,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modOfficeLocation(request)",log);
		}
		
	}

	/**
	 * 删除办公地点信息
	 */
	public void delOfficeLocation(HttpServletRequest request) 
		throws HaveDelOfficeLocationException,Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request, "id");
			DBRow [] rows = floorOfficeLocation.getDetailParentOfficeLocation(id);
			if(rows.length != 0)
			{
				throw new HaveDelOfficeLocationException();
			}
			else
			{
				floorOfficeLocation.delOfficeLocation(id);
			}
		} 
		catch (HaveDelOfficeLocationException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new Exception("delOfficeLocation(request) error:"+e);
		}
		
		
	}
	
	/**
	 * 办公地点删除判断
	 * 遍历节点下是否有子节点，是否有子节点绑定了帐号
	 * true可删除
	 * flase不可删除
	 */

	public boolean getNode(long id) throws Exception {
	
			boolean node = true;

			DBRow[] nodeRow = floorOfficeLocation.getDetailParentOfficeLocation(id); // 是否有子节点
			if (nodeRow != null && nodeRow.length > 0) { // 有子节点
				for (DBRow cnode : nodeRow) { // 循环子节点
					long cnodeRow = cnode.get("id", -1L);
					DBRow[] nodeAccount = floorOfficeLocation.getAccount(cnodeRow);
					if (nodeAccount != null && nodeAccount.length > 0) {
						node = false;
						 return false;
						 
					}
				
						if(!getNode(cnodeRow)){
							return false;
						}
						
			
				}
			} else {
				DBRow[] nodeAccount = floorOfficeLocation.getAccount(id);
				if (nodeAccount != null && nodeAccount.length > 0) {
					node = false;
					return false;
				}
				
			}

			return true;
		

	}
	
	
	/*
	 * 删除办公地址（验证办公地点是否绑定帐号）
	 * 
	 * 有没有子节点
	 * 有：子节点是否绑定帐号
	 * 无：节点本身是否绑定帐号
	 * 
	 * **/
	public DBRow delOfficeAddress(long id) throws Exception {

		try {

		
			DBRow result = new DBRow();

			boolean validate = getNode(id);;
			// 验证是否有子节点
			if(validate==true){
				floorOfficeLocation.delOfficeLocation(id); // 删除节点

				floorOfficeLocation.delOfficeLocationNode(id); // 删除子节点
				result.add("id",id);
			}else{
				result.put("error","Cann't delete, Users with this role.");
			}
			
		
			result.add("id", id);

			result.add("success", validate);

			return result;

		} catch (Exception e) {
			throw new Exception("RoleMgrSbb.delAddress(DBRow parameter) error:"
					+ e);
		}
	}


	//办公地点tree
	
		public Map<String, DBRow[]> getAllOfficeLocationTree() throws Exception
		{
			DBRow[] children = floorOfficeAddressMgr.getOfficeLocationList(-1,0);
			Map<String,DBRow[]> result = new HashMap<String,DBRow[]>();
			result.put("children",children);
			return result;

			
			
			
		/*	DBRow[] rows = floorOfficeLocation.getAllOfficeLocation();
			JSONArray office = DBRowUtils.dbRowArrayAsJSON(rows);
			for (int i = 0; i < office.length(); i++) 
			{
				JSONObject row = office.getJSONObject(i);
				DBRow[] children = floorOfficeLocation.getDetailParentOfficeLocation(row.getLong("id"));
				row.put("children", DBRowUtils.dbRowArrayAsJSON(children));
			}

			return office;*/
		}
	
	/**
	 * 查询所有的父类办公地点的信息
	 */
	public DBRow[] getParentOfficeLocation() 
		throws Exception 
	{
		
		try 
		{
			return (floorOfficeLocation.getParentOfficeLocation());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getParentOfficeLocation()",log);
		}
	}
	
	/**
	 * 根据父类id获取该父类下的所有子类信息
	 */
	public DBRow[] getChildOfficeLocation(long parentid) 
		throws Exception 
	{
		
		try 
		{
			return (floorOfficeLocation.getDetailParentOfficeLocation(parentid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getChildOfficeLocation(parentid)",log);
		}
	}
	

	/**
	 * 获取所有的办公地点的信息
	 */
	public DBRow[] getAllOfficeLocation() 
		throws Exception 
	{
		
		try 
		{
			return (floorOfficeLocation.getAllOfficeLocation());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllOfficeLocation()",log);
		}
	}
	/**
	 * 根据名字获取办公地点的信息
	 */
	public DBRow getLocationIdByName(String office_name) throws Exception {
		try 
		{
			return (floorOfficeLocation.getLocationIdByName(office_name));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getLocationIdByName(office_name)",log);
		}
	}	
	
	/*
	 * 
	 * 
	 */
	

	
	
	
	
	
	
	
	
	
	public void setFloorOfficeLocation(FloorOfficeLocationMgrTJH floorOfficeLocation) {
		this.floorOfficeLocation = floorOfficeLocation;
	}

	public FloorOfficeLocationMgrTJH getFloorOfficeLocation() {
		return floorOfficeLocation;
	}
	
	
	public FloorOfficeAddressMgrSlj getFloorOfficeAddressMgr() {
		return floorOfficeAddressMgr;
	}



	public void setFloorOfficeAddressMgr(
			FloorOfficeAddressMgrSlj floorOfficeAddressMgr) {
		this.floorOfficeAddressMgr = floorOfficeAddressMgr;
	}



















}
