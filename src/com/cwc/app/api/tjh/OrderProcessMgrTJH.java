package com.cwc.app.api.tjh;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.api.ProductMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.tjh.FloorOrderProcessMgrTJH;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.app.key.MapColorKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.db.zj.FloorOrderProcessMgrZJ;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;

import org.jdom.*;
import org.jdom.output.*;

import java.io.*;

import javax.servlet.http.HttpServletRequest;

public class OrderProcessMgrTJH implements OrderProcessMgrIFaceTJH{
	
	private FloorOrderProcessMgrTJH floorOrderProcessMgr;
	private ProductMgr productMgr;
	private FloorSupplierMgrTJH floorSupplierMgr;
	static Logger log = Logger.getLogger("ACTION");
	private FloorOrderProcessMgrZJ floorOrderProcessMgrZJ;
	private CatalogMgrIFace catalogMgr;
	private FloorProductMgr floorProductMgr;

	/**
	 * 获取所有的订单批处理
	 */
	public DBRow[] getAllOrderProcess(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getAllOrderProcess(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllOrderProcess()",log);
		}
	}
	
	/**
	 * 根据id获取商品的名称
	 */
	public DBRow getProductById(long id) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getProductNameById(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductById(long id)",log);
		}
	}

	/**
	 * 查询实际产品需求分布并保存数据
	 * @param endDate 
	 * @param startDate 
	 * @throws Exception
	 */
	public void statcProcuctNeedQuantity(PageCtrl pc,String start_date, long cid, String end_date) 
		throws Exception
	{
		try 
		{
				
				deleteProductSpread(start_date,end_date);    //删除传过来的某一时间的数据
				DBRow [] orders = floorOrderProcessMgr.getOrdersProduct(start_date,end_date,cid); 
				for (int i = 0; i < orders.length; i++) 
				{
					DBRow row = new DBRow();
					
					long prior_stoId = productMgr.getPriorDeliveryWarehouse(orders[i].get("ccid", 0l), orders[i].get("pro_id", 0l));  //根据国家和地区获取优先发货仓库
					row.add("prior_storage_id", prior_stoId);
					
					row.add("product_id", orders[i].get("p_pid", 0l));
					row.add("nation_id", orders[i].get("ccid", 0l));
					row.add("province_id", orders[i].get("pro_id", 0l));
					row.add("catalog_id", orders[i].get("catalog_id", 0l));
					row.add("total_quantity", orders[i].get("quantity", 0d));
					row.add("unit_price", orders[i].get("unit_price", 0d));
					row.add("stats_date", DateUtil.DatetimetoStr(new Date()));
					row.add("storage_id", orders[i].get("ps_id",0l));
					row.add("delivery_date", orders[i].getString("delivery_date"));
					
					floorOrderProcessMgr.statcProcuctNum(row);
					
				}
			statsTheoryStorageForProduct(start_date,end_date);
			
			floorOrderProcessMgrZJ.statcProcuctNeedQuantity(start_date, cid, end_date);
		}
		
		catch (Exception e) 
		{
			throw new SystemException(e,"statcProcuctSales()",log);
		}
	}
	
	/**
	 * 统计产品需求分布前删除前一次统计的数据
	 * @param end_date 
	 * @throws Exception
	 */
	public void deleteProductSpread(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
				floorOrderProcessMgr.deleteProductSpread(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteProductSpread()",log);
		}
	}
	
	/**
	 * 根据时间段统计产品需求分布
	 */
	public DBRow[] statsProductSpread(PageCtrl pc, String delivery_date,long cid, String end_date) 
		throws Exception 
	{
		try 
		{
			if(delivery_date != null)
			{
				statcProcuctNeedQuantity(pc,delivery_date,cid,end_date);
			}
			
			return (floorOrderProcessMgr.getAllOrderProcess(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"statsProductSpread(PageCtrl pc, String delivery_date,long cid)",log);
		}
	}

	/**
	 * 根据id查询商品类别的最顶层信息
	 */
	public DBRow getProductCategoryById(long id) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getProductCategoryById(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCategoryById(long id)",log);
		}
	}
	

	/**
	 * 在统计理论仓库发货需求前删除上一次统计的数据
	 * @param end_date 
	 * @throws Exception 
	 */
	public void delTheoryStorageOfProduct(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
				floorOrderProcessMgr.delSpreadTheoryProduct(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delTheoryStorageOfProduct(PageCtrl pc)",log);
		}
		
	}
	
	/**
	 * 根据国家和省份/州查询理论仓库后统计理论仓库需要的货物需求量并保存数据
	 * @param end_date 
	 * @param pc
	 * @throws Exception
	 */
	public void statsTheoryStorageForProduct(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
				
				delTheoryStorageOfProduct(start_date,end_date);
				DBRow [] rows = floorOrderProcessMgr.statsTheoryStorageForProduct(start_date,end_date);
				if(rows.length != 0)
				{
					for (int i = 0; i < rows.length; i++) 
					{
						DBRow row = new DBRow();
						row.add("product_id", rows[i].get("product_id", 0l));
						row.add("storage_id", rows[i].get("prior_storage_id", 0l));
						row.add("catalog_id", rows[i].get("catalog_id", 0l));
						row.add("quantity", rows[i].get("quantity", 0d));
						row.add("unit_price", rows[i].get("unit_price", 0d));
						row.add("deal_date", DateUtil.DatetimetoStr(new Date()));
						row.add("delivery_date", rows[i].getString("delivery_date"));
						
						floorOrderProcessMgr.addTheoryStorageQuantityByProduct(row);
					}
					
				}
				statsOriginalStoragesProduct(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"statsTheoryStorageForProduct(PageCtrl pc)",log);
		}
	}

	/**
	 * 统计代发后本来仓库的货物需求并保存数据
	 * @param end_date 
	 * @throws Exception
	 */
	private void statsOriginalStoragesProduct(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
				delStatsPlanStorageSalesBefore(start_date,end_date);
				DBRow [] rows =floorOrderProcessMgr.getAllTheoryStorageSales(start_date,end_date);
				
				DBRow row = new DBRow();
				for (int i = 0; i < rows.length; i++) 
				{
					DBRow trandspond = floorOrderProcessMgr.statsOriginalStorageSales(rows[i].get("storage_id", 0l), rows[i].get("product_id", 0l),rows[i].getString("delivery_date"));  //判断仓库间的产品是否需要代发
					if(trandspond != null)
					{
							long type_id = trandspond.get("modality_id", 0l);
							if(type_id == 1)          //1：代发形式为套装
							{
								row.add("product_id", trandspond.get("product_id", 0l));
								row.add("d_storage_id", trandspond.get("d_sid", 0l));
								row.add("prior_storage_id", trandspond.get("y_sid", 0l));
								row.add("quantity", trandspond.get("quantity", 0d));
								row.add("catalog_id", trandspond.get("catalog_id", 0l));
								row.add("unit_price", trandspond.get("unit_price", 0d));
								row.add("deal_date", DateUtil.DatetimetoStr(new Date()));
								row.add("delivery_date", trandspond.getString("delivery_date"));
								
								floorOrderProcessMgr.addOriginalStorageSales(row);
							}
							else if(type_id == 2)  //2：代发形式为散件
							{
								long set_product_id = trandspond.get("product_id", 0l);
								double set_quantity = trandspond.get("quantity", 0d);
								long category_id = 0;
								
								DBRow [] union_product_rows = productMgr.getProductsInSetBySetPid(set_product_id);  //判断商品是否是套装
								if(union_product_rows.length != 0)     //商品是套装
								{
									for (int j = 0; j < union_product_rows.length; j++)  //获取套装下的各商品
									{
										long pid = union_product_rows[j].get("pid", 0l);
										double quantity = union_product_rows[j].get("quantity", 0d);
										double quantity1 = union_product_rows[j].get("quantity", 0d);
										for (int k = 1; k < set_quantity; k++) 
										{
											quantity=quantity+quantity1;
										}
										DBRow product_catalog = productMgr.getDetailProductByPcid(pid);  //根据商品id获取套装代发后的各商品的类别
										if(product_catalog != null)
										{
											category_id = product_catalog.get("catalog_id", 0l);
										}
										
										row.add("product_id", pid);
										row.add("d_storage_id", trandspond.get("d_sid", 0l));
										row.add("prior_storage_id", trandspond.get("y_sid", 0l));
										row.add("quantity", quantity);
										
										row.add("catalog_id", category_id);
										row.add("unit_price", trandspond.get("unit_price", 0d));
										row.add("deal_date", DateUtil.DatetimetoStr(new Date()));
										row.add("delivery_date", trandspond.getString("delivery_date"));  
										
										floorOrderProcessMgr.addOriginalStorageSales(row);
									}
								}
								else
								{
									category_id = trandspond.get("catalog_id", 0l);
									
									row.add("product_id", set_product_id);
									row.add("d_storage_id", trandspond.get("d_sid", 0l));
									row.add("prior_storage_id", trandspond.get("y_sid", 0l));
									row.add("quantity", set_quantity);
									row.add("catalog_id", category_id);
									row.add("unit_price", trandspond.get("unit_price", 0d));
									row.add("deal_date", DateUtil.DatetimetoStr(new Date()));
									row.add("delivery_date", trandspond.getString("delivery_date"));
									
									floorOrderProcessMgr.addOriginalStorageSales(row);
								}
							}
					}
					else
					{
						
						row.add("product_id", rows[i].get("product_id", 0l));
						row.add("d_storage_id", rows[i].get("storage_id", 0l));
						row.add("prior_storage_id", rows[i].get("storage_id", 0l));
						row.add("quantity", rows[i].get("quantity", 0d));
						row.add("catalog_id", rows[i].get("catalog_id", 0l));
						row.add("unit_price", rows[i].get("unit_price", 0d));
						row.add("deal_date", DateUtil.DatetimetoStr(new Date()));
						row.add("delivery_date", rows[i].getString("delivery_date"));
						
						
						floorOrderProcessMgr.addOriginalStorageSales(row);
					}
			}
			
		statsPlanStorageProductSales(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"statsOriginalStoragesProduct()",log);
		}
		
	}

	/**
	 * 分页查询理论仓库货物的需求信息
	 */
	public DBRow[] getAllTheoryStroage(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getAllTheoryStorageForProduct(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllTheoryStroage(PageCtrl pc) ",log);
		}
	}
	
	/**
	 * 根据商品类别id获取父类信息
	 */
	public DBRow getProductCategory(long category_id) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getProductCategory(category_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCategory(long category_id)",log);
		}
	}
	
	/**
	 * 分页查询计划仓库的货物需信息
	 */
	public DBRow[] getAllPlanStorageSales(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getAllPlanStorageSales(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllPlanStorageSales(PageCtrl pc)",log);
		}
	}
	
	/**
	 * 统计代发后计划仓库的货物需求量前删除上一次统计的数据
	 * @param end_date 
	 * @param pc
	 * @throws Exception
	 */
	public void delStatsPlanStorageSalesBefore(String start_date, String end_date) 
		throws Exception
	{
		try
		{
			floorOrderProcessMgr.deleteOriginalPlanStorageSales(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delPlanStorageSales(PageCtrl pc)",log);
		}
	}
	
	
	/**
	 * 删除上一次计划仓库中统计的数据
	 * @param end_date 
	 * @throws Exception
	 */
	public void delPlanStorageSales(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
				floorOrderProcessMgr.deletePlanStorageSales(start_date,end_date);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delPlanStorageSales()",log);
		}
	}
	
	/**
	 * 根据仓库代发后的本来仓库所需的产品量统计计划仓库的产品需求并保持到数据库
	 * @param end_date 
	 * @throws Exception
	 */
	public void statsPlanStorageProductSales(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
				delPlanStorageSales(start_date,end_date);
				DBRow [] plan_storages = floorOrderProcessMgr.statsPlanStorageProductSales(start_date,end_date);
				DBRow params = new DBRow();
				if (plan_storages.length != 0) 
				{
					for (int i = 0; i < plan_storages.length; i++) 
					{
						params.add("plan_storage_id", plan_storages[i].get("d_storage_id", 0l));
						params.add("product_id", plan_storages[i].get("product_id", 0l));
						params.add("catalog_id", plan_storages[i].get("catalog_id", 0l));
						params.add("quantity", plan_storages[i].get("quantity", 0d));
						params.add("unit_price", plan_storages[i].get("unit_price", 0d));
						params.add("deal_date", DateUtil.DatetimetoStr(new Date()));
						params.add("delivery_date", plan_storages[i].getString("delivery_date"));
						
						
						floorOrderProcessMgr.addPlanStorageSales(params);
					}
				}
			}
			
		catch (Exception e) 
		{
			throw new SystemException(e,"statsPlanStorageProductSales()",log);
		}
	}
	
	/**
	 * 分页查询代发产品后的本来仓库的产品需求
	 */
	public DBRow[] getAllOriginalStorageSales(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getAllOriginalStorageSales(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllOriginalStorageSales(PageCtrl pc)",log);
		}
	}

	
	/**
	 * 查看全球各国家的货物需求总额分布
	 */
	public void statsProductSpreadByMaps(long catalog_id, long p_line_id, String p_name, String st_date, String end_date) 
		throws Exception 
	{
		try 
		{
			long start = System.currentTimeMillis();
			// 创建根节点 
			Element root = new Element("map");
			   
			Document Doc =new Document(root);    // 根节点添加到文档中； 
	
			/**
			 * 给map根节点设置属性
			 */
			root.setAttribute("numberPrefix","");   // 数字前缀[类似于$345.90]
			root.setAttribute("baseFontSize","9");   //字体大小
			root.setAttribute("borderColor","005879");
			root.setAttribute("animation","0");
			root.setAttribute("fillColor","#FEA7F8");
			root.setAttribute("showLabels","1");   //是否显示实体的labels
			root.setAttribute("includeValueInLabels","1");
			root.setAttribute("canvasBorderColor","FFFFFF");
			root.setAttribute("borderColor","FFFFFF");
			root.setAttribute("hoverColor","FFFFFF");
			//root.setAttribute("labelSepChar",": ");
			//root.setAttribute("formatNumberScale","1");   //配置是否将数字转化为K (千) and M (百万)。例如： 配置为1, 1043 将转化为1.04K (小数点后两位).
			
			 
			Element changeColor = new Element("colorRange");
			root.addContent(changeColor);
			  			
			List list = new ArrayList();
			long pid = 0;
			float max = 0;
			float min = 0;
	
			Element second_ele = new Element("data");
			root.addContent(second_ele);
			
			DBRow [] counties =floorSupplierMgr.getAllCountryCode();
			
			
			DBRow product = floorOrderProcessMgr.getProductIdByName(p_name);
	
			if(product != null)
			{
				pid = product.get("pc_id", 0l);
			}
			
		    DecimalFormat df = new DecimalFormat("#.0");

		    for (int i = 0; i < counties.length; i++) //国家循环
			{
				Element elements = new Element("entity");
				
				elements.setAttribute("id",counties[i].getString("fusion_id"));
				
				
				elements.setAttribute("displayValue",counties[i].getString("c_code"));
				elements.setAttribute("link","javascript:drillDown("+counties[i].getString("fusion_id")+","+catalog_id+",'"+p_line_id+"',"+pid+")");
				
				if(product != null)   
				{
					pid = product.get("pc_id", 0l);
				}
				
				DBRow country_count = floorOrderProcessMgrZJ.getCountryProductNeedNumZJ(counties[i].get("ccid", 0l),pid,catalog_id,p_line_id, st_date, end_date);
				
				if(country_count != null)
				{
					 elements.setAttribute("value",df.format(country_count.get("total_quantity", 0f)));
					 list.add(country_count.get("total_quantity", 0f));
				}
				else
				{
					elements.setAttribute("value","0");
				}
				
				second_ele.addContent(elements);		
			}
			
			
			if(list.toArray().length != 0)   //获取最大和最小值
			{
				min = Float.parseFloat(list.get(0).toString());
				max = Float.parseFloat(list.get(0).toString());
				for (int k = 0; k < list.toArray().length; k++) 
				{
					if(max < Float.parseFloat(list.get(k).toString()))
					{
						max = Float.parseFloat(list.get(k).toString());
					}
					if(min > Float.parseFloat(list.get(k).toString()))
					{
						min = Float.parseFloat(list.get(k).toString());
					}
				}
			}
			float num = max/10;
			
			MapColorKey mapColorKey = new MapColorKey();
			
			for (int i = 1; i <= 10; i++)   //根据最大值和最小值分成十段不同的颜色
			{
				 DecimalFormat format = new DecimalFormat("#");
				 Element color = new Element("color");  
				 int forMin = 0;
				 int forMax = 0;
				 if(min<num*i)
				 {
					if(i==1)
					{
						forMin = (int)min;
					}
					else
					{
						forMin = (int)(num*(i-1));
					}
					
					if(i==10)
					{
						forMax = (int)(num*i+1);
					}
					else
					{
						forMax = (int)(num*i);
					}
					
				 }
				 
				color.setAttribute("minValue",""+format.format(forMin)+"");
				color.setAttribute("maxValue",""+format.format(forMax)+"");
				color.setAttribute("displayValue",(int)forMin+"-"+(int)forMax);
				color.setAttribute("color",mapColorKey.getMapColorKeyById(i));    //
				changeColor.addContent(color);
			}
					
			XMLOutputter XMLOut = new XMLOutputter();
			String path = Environment.getHome()+"administrator/order_process/fusionMaps/mapsXml/";
	        XMLOut.output(Doc,new FileOutputStream(path + "worlds.xml"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"statsProductSpreadByMaps()",log);
		}
		
	}
	
	/**
	 * 根据fusion_id查询国家，创建该国家下的所有的区域产品分布的xml文件
	 */
	public void getProvinceSpreadbyMap(String fusion_id,long catalog_id, long p_line_id, long product_id, String start_date, String end_date) 
		throws Exception 
	{

		try 
		{
			// 创建根节点 
			Element root = new Element("map");
			   
			Document Doc =new Document(root);    // 根节点添加到文档中； 

			/**
			 * 给map根节点设置属性
			 */
			root.setAttribute("numberPrefix","");   // 数字前缀[类似于$345.90]
			root.setAttribute("baseFontSize","9");   //字体大小
			root.setAttribute("borderColor","005879");
			root.setAttribute("animation","0");
			root.setAttribute("fillColor","#FEA7F8");
			root.setAttribute("showLabels","1");   //是否显示实体的labels
			root.setAttribute("includeValueInLabels","1");
			root.setAttribute("canvasBorderColor","FFFFFF");
			root.setAttribute("borderColor","FFFFFF");
			root.setAttribute("hoverColor","FFFFFF");
			//root.setAttribute("labelSepChar",": ");
			//root.setAttribute("formatNumberScale","1");   //配置是否将数字转化为K (千) and M (百万)。例如： 配置为1, 1043 将转化为1.04K (小数点后两位).
			
			 
			Element changeColor = new Element("colorRange");
			root.addContent(changeColor);
			
			double max = 0;
			double min = 0;
			List list = new ArrayList();

			Element second_ele = new Element("data");
			root.addContent(second_ele);
			
			DBRow country = floorOrderProcessMgr.getCountryByfusion_id(fusion_id);
			if(country != null)
			{
				DBRow [] provinces = floorSupplierMgr.getProvinceByCountryId(country.get("ccid", 0l));
			    DecimalFormat df = new DecimalFormat("#.00");
				for (int i = 0; i < provinces.length; i++) 
				{
					Element elements = new Element("entity");
					
					elements.setAttribute("id",provinces[i].getString("fusion_id"));
					////system.out.println(provinces[i].getString("fusion_id"));
					
					elements.setAttribute("displayValue", provinces[i].getString("p_code"));
					
					DBRow province_count = floorOrderProcessMgrZJ.getAllProvinceSpreadProductNeedByLineIdZJ(provinces[i].get("pro_id", 0l),catalog_id,p_line_id, product_id, start_date, end_date);
					
					if(province_count != null)
					{
						elements.setAttribute("value",df.format(province_count.get("total_quantity", 0f)));
						
						list.add(province_count.get("total_quantity", 0f));
					}
					else
					 {
						 elements.setAttribute("value","0");
					 }
	
					second_ele.addContent(elements);
				}
			}
			
			
			if(list.toArray().length != 0)
			{
				min = Double.parseDouble(list.get(0).toString());
				max = Double.parseDouble(list.get(0).toString());
				for (int k = 0; k < list.toArray().length; k++) 
				{
					if(max < Double.parseDouble(list.get(k).toString()))
					{
						max = Double.parseDouble(list.get(k).toString());
					}
					if(min > Double.parseDouble(list.get(k).toString()))
					{
						min = Double.parseDouble(list.get(k).toString());
					}
				}
			}
			
			
			
			double num = max/10;
			
			MapColorKey mapColorKey = new MapColorKey();
			
			for (int i = 1; i <= 10; i++)   //根据最大值和最小值分成十段不同的颜色
			{
				 DecimalFormat format = new DecimalFormat("#");
				 Element color = new Element("color");  
				 int forMin = 0;
				 int forMax = 0;
				 if(min<num*i)
				 {
					if(i==1)
					{
						forMin = (int)min;
					}
					else
					{
						forMin = (int)(num*(i-1));
					}
					
					if(i==10)
					{
						forMax = (int)(num*i+1);
					}
					else
					{
						forMax = (int)(num*i);
					}
					
				 }
				 
				color.setAttribute("minValue",""+format.format(forMin)+"");
				color.setAttribute("maxValue",""+format.format(forMax)+"");
				color.setAttribute("displayValue",(int)forMin+"-"+(int)forMax);
				color.setAttribute("color",mapColorKey.getMapColorKeyById(i));    //
				changeColor.addContent(color);
			}
			
			XMLOutputter XMLOut = new XMLOutputter();
			String path = Environment.getHome()+"administrator/order_process/fusionMaps/mapsXml/";
			XMLOut.output(Doc,new FileOutputStream(path + "province.xml"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProvinceSpreadbyMap(String fusion_id)",log);
		}
	}

	/**
	 * 根据国家的fusionMap的id查询该国家的flash
	 */
	public DBRow getCountryMapFlashByFusionId(String fusion_id)
			throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getCountryByfusion_id(fusion_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getCountryMapFlashByFusionId(String fusion_id)",log);
		}
	}

	
	
	/**
	 * 查询全世界各国以及该国家的省份以及州的产品需求分布情况
	 */
	public DBRow[] getFusionMapsBySalesSpreadJSONAction(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String fusion_id = StringUtil.getString(request, "fusion_id");
			return (floorOrderProcessMgr.getFusionMapsBySalesSpreadJson(fusion_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getFusionMapsBySalesSpreadJSONAction(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 获取某一时间段内的所有的计划仓库的数据
	 */
	public DBRow[] getPlanStorageProductQuantity(String start_date,String end_date, long cid) 
		throws Exception 
	{
		try 
		{
			return (floorOrderProcessMgr.getPlanStorageSales(start_date,end_date,cid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPlanStorageProductQuantity(String start_date,String end_date, long cid)",log);
		}
	}

	/**
	 * 根据产品线、产品类别、或商品名称过滤查询某一时间段内实际仓库的货物需求量的情况
	 */
	public DBRow[] getActualStorageProductQuantity(String start_date,String end_date, long catalog_id, long pro_line_id,long ccid,long pro_id,String product_name,PageCtrl pc,String cmd) 
		throws Exception 
	{
		try 
		{
			DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
			long product_id = 0;
			if(product != null)
			{
				product_id = product.get("pc_id", 0l);
			}
			if(product_id != 0)
			{
				return (floorOrderProcessMgr.getActualOrPlanStorageProductQuantityByProductId(pc,start_date,end_date,product_id,cmd,ccid,pro_id));  //根据商品名称查询实际仓库的货物需求量
			}
			else if(pro_line_id!=0&&catalog_id==0)
			{
				return (floorOrderProcessMgr.getStorageProductQuantityByLineIds(pro_line_id,start_date,end_date,cmd,pc,ccid,pro_id));
			}
			else
			{
				return (floorOrderProcessMgr.getStoragesProductQuantityByCatalogIds(catalog_id,pro_line_id,start_date,end_date,pc,cmd,ccid,pro_id));
			}
			
//			else
//			{
//				return (floorOrderProcessMgr.getStoragesProductQuantityByDate(pc,start_date,end_date,cmd));
//			}
				
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getActualStorageProductQuantity(PageCtrl pc,String start_date,String end_date, long catalog_id, long pro_line_id,String product_name)",log);
		}
	}
	
	public long statcProcuctNeedQuantityZJ(String start, String end,long cid)
		throws Exception 
	{
			try 
			{
				long allCount = 0;
				
				allCount += floorOrderProcessMgrZJ.statcProcuctNeedQuantity(start, cid, end);//第一张表
				allCount += floorOrderProcessMgrZJ.statsTheoryStorageForProduct(start, end);//第二张表
				allCount += floorOrderProcessMgrZJ.statsOriginalStoragesProduct(start, end);//第三张表
				allCount += floorOrderProcessMgrZJ.statsPlanStorageProductSales(start, end);//第四张表
				
				return (allCount);
			}
			catch (Exception e) 
			{
				throw new SystemException(e,"statcProcuctNeedQuantityZJ",log);
			}
	}
	
	/**
	 * 返回最后一条记录的发货时间
	 * @return
	 * @throws Exception
	 */
	public String orderProcessLastTime()
		throws Exception
	{
		try 
		{
			DBRow row = floorOrderProcessMgrZJ.getLastTime();
			
			return (row.getString("delivery_date").substring(0,10));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"OrderProcessLastTime",log);
		}
	}
	
	/**
	 * 产品需求分析
	 * @param start_date
	 * @param end_date
	 * @param catalog_id
	 * @param pro_line_id
	 * @param product_name
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productDemandAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,String product_name,long ca_id,long ccid,long pro_id,int type,PageCtrl pc)
		throws Exception
	{
		DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
		long pc_id = 0;
		if(product!=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		return (floorOrderProcessMgr.productDemandAnalysisNew(start_date, end_date, catalog_id, pro_line_id, pc_id, ca_id, ccid, pro_id,"", type, pc));
//		return (floorOrderProcessMgr.productDemandAnalysis(start_date, end_date, catalog_id, pro_line_id, pc_id, ca_id, ccid, pro_id, type, pc));
	}
	
	/**
	 * 导出产品需求分析
	 */
	public String exportProductDemandAnalysis(HttpServletRequest request)
		throws Exception
	{
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long pro_line_id = StringUtil.getLong(request,"pro_line_id");
		String product_name = StringUtil.getString(request,"product_name");
		String input_st_date = StringUtil.getString(request,"input_st_date");
		String input_en_date = StringUtil.getString(request,"input_en_date");

		long ccid = StringUtil.getLong(request,"ccid");
		long pro_id = StringUtil.getLong(request,"pro_id");
		long ca_id = StringUtil.getLong(request,"ca_id");
		int type = StringUtil.getInt(request,"type");//1代表根据选择地域求和统计，2代表根据选择地域的下一级地域分布统计
		
		DBRow product = floorOrderProcessMgr.getProductIdByName(product_name);
		long pc_id = 0;
		if(product!=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		DBRow DateRow[]= floorOrderProcessMgr.getDate(input_st_date, input_en_date);
		DBRow[] rowsValue = floorOrderProcessMgr.productDemandAnalysisNew(input_st_date,input_en_date, catalog_id, pro_line_id, pc_id, ca_id, ccid, pro_id,"", type,null);
		
		if (rowsValue.length > 0) 
		{
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/order_process/ExportPlanAndStock.xlsx"));  
			XSSFSheet sheet= wb.getSheet("Sheet1");  
			
			XSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style.setLocked(false); //创建样式
			style.setWrapText(true);
			
			XSSFFont  font =wb.createFont();
			font.setFontName("Arial");   
			font.setFontHeightInPoints((short)10);   
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
			
			XSSFCellStyle styleTitle = wb.createCellStyle();
			styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleTitle.setLocked(false);  
			styleTitle.setWrapText(true);
			styleTitle.setFont(font);
			
			XSSFCellStyle stylelock = wb.createCellStyle();
			stylelock.setLocked(true); 
			stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
			//title赋值
			XSSFRow row = sheet.createRow(0);  
			
			row.createCell(0).setCellValue("所属产品线");
			row.getCell(0).setCellStyle(styleTitle);
			
			row.createCell(1).setCellValue("商品分类");
			row.getCell(1).setCellStyle(styleTitle);
			
			row.createCell(2).setCellValue("产品名称");
			row.getCell(2).setCellStyle(styleTitle);
			
			row.createCell(3).setCellValue("销售区域");
			row.getCell(3).setCellStyle(styleTitle);
			
			int blankCell = 3;//时间显示从第几列开始
			if(ca_id>0)
			{
				blankCell++;
				row.createCell(blankCell).setCellValue("销售国家");
				row.getCell(blankCell).setCellStyle(styleTitle);
			}
			if(ccid>0)
			{
				blankCell++;
				row.createCell(blankCell).setCellValue("销售地区");
				row.getCell(blankCell).setCellStyle(styleTitle);
			}
				
			blankCell++;
			row.createCell(blankCell).setCellValue("阶段总需求");
			row.getCell(blankCell).setCellStyle(styleTitle);
				
			blankCell++;
			row.createCell(blankCell).setCellValue("阶段平均需求");
			row.getCell(blankCell).setCellStyle(styleTitle);
			
			blankCell++;
			for (int i = 0; i < DateRow.length; i++)
			{
				row.createCell(i+blankCell).setCellValue(DateRow[i].getString("date"));
				row.getCell(i+blankCell).setCellStyle(styleTitle);	
			}
			//value赋值
			
			for (int i = 0; i < rowsValue.length; i++)
			{
				blankCell = 3;
				row = sheet.createRow(i+1); 
				DBRow rowTemp = rowsValue[i];
				

				row.createCell(0).setCellValue(rowTemp.getString("pl_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(rowTemp.getString("title"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(rowTemp.getString("p_name"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(ca_id==0&&type==1?"ALL":rowTemp.getString("area_name"));
				row.getCell(3).setCellStyle(style);
				
				if(ca_id>0)
				{
					blankCell++;
					row.createCell(blankCell).setCellValue(ccid==0&&type==1?"ALL":rowTemp.getString("c_country"));
					row.getCell(blankCell).setCellStyle(style);
				}
				if(ccid>0)
				{
					blankCell++;
					row.createCell(blankCell).setCellValue(pro_id==0&&type==1?"ALL":rowTemp.getString("pro_name"));
					row.getCell(blankCell).setCellStyle(style);
				}
				
				blankCell++;
				row.createCell(blankCell).setCellValue(rowTemp.get("sum_total_quantity",0d));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				
				blankCell++;
				row.createCell(blankCell).setCellValue(MoneyUtil.round(rowTemp.get("sum_total_quantity",0d)/DateRow.length,2));
				row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				row.getCell(blankCell).setCellStyle(style);
				
				
				blankCell++;
				for (int j = 0; j < DateRow.length; j++)
				{
					row.createCell(j+blankCell).setCellValue(rowTemp.get("date_"+j,0d));
					row.getCell(blankCell).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(j+blankCell).setCellStyle(style);	
				}
			}
			
			//写文件  
//			String path = "upl_excel_tmp/export_ProductDemandAnalysis_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
			String path = "upl_excel_tmp/产品需求"+input_st_date+"至"+input_en_date+".xlsx";
			OutputStream os= new FileOutputStream(Environment.getHome()+path);
			wb.write(os);  
			os.close();  
			return (path);
		}
		else
		{
			return "0";
		}
	}
	
	public void everyDayCrontabDemandAnalysisAndPlanAnalysis()
		throws Exception
	{
		DBRow[] sendStore = catalogMgr.getProductDevStorageCatalogTree();
		
		for (int i = 0; i < sendStore.length; i++) 
		{
			DBRow[] productStorage = productMgr.getAllProductStorageByPcid(0,sendStore[i].get("id",0l),0,null);
			
			TDate tDate = new TDate();
			tDate.addDay(-productStorage[i].get("sampling period",0));

			String st_date = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en_date = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();

			for (int j = 0; j < productStorage.length; j++) 
			{
				DBRow[] demandAnalysis = floorOrderProcessMgrZJ.getProductDemandAnalysisGroupByPs(productStorage[j].get("pc_id",0l), sendStore[i].get("id",0l), st_date, en_date);
				
				DBRow[] planAnalysis = floorOrderProcessMgrZJ.getProductPlanAnalysisGroupByPs(productStorage[j].get("pc_id",0l), sendStore[i].get("id",0l), st_date, en_date);
				
				DBRow sendAnalysis = floorOrderProcessMgrZJ.getProductSendAnalysisGroupByPs(productStorage[j].get("pc_id",0l), sendStore[i].get("id",0l), st_date, en_date);
				if(demandAnalysis.length==1||planAnalysis.length==1)
				{
					DBRow para = new DBRow();
					
					float storage_send_avgcount = 0;
					
					if(sendAnalysis!=null)
					{
						storage_send_avgcount = sendAnalysis.get("all_send_quantity",0f);
					}
					para.add("storage_send_avgcount",storage_send_avgcount);
					
					float storage_need_avgcount = 0;
					if(demandAnalysis.length==1)
					{
						storage_need_avgcount = demandAnalysis[0].get("demand_analysis_count",0f)/productStorage[i].get("sampling period",0);
						
						para.add("storage_need_avgcount",storage_need_avgcount);
					}
					else if(demandAnalysis.length>1)
					{
						throw new Exception("demandAnalsis length>1");
					}
					
					float storage_plan_avgcount = 0;
					if(planAnalysis.length==1)
					{
						storage_plan_avgcount = planAnalysis[0].get("plan_analysis_count",0f)/productStorage[i].get("sampling period",0);
						
						para.add("storage_plan_avgcount",storage_plan_avgcount);
					}
					else if(planAnalysis.length>1)
					{
						throw new Exception("planAnalysis length>1");
					}
					
					floorProductMgr.modProductStorage(productStorage[j].get("pid",0l),para);
				}
			}
		}
		
		DBRow[] products = floorProductMgr.getAllProducts(null);
		for (int j = 0; j < products.length; j++) 
		{
			DBRow[] productStorages = floorProductMgr.getProductStoragesByPcid(products[j].get("pc_id",0l));
			
			float plan_average_count = 0;
			float need_average_count = 0;
			for (int k = 0; k < productStorages.length; k++) 
			{
				plan_average_count += productStorages[k].get("storage_plan_avgcount",0f);
				need_average_count += productStorages[k].get("storage_need_avgcount",0f);	
				
				DBRow productPara = new DBRow();
				productPara.add("plan_average_count",plan_average_count);
				productPara.add("need_average_count",need_average_count);
				
				floorProductMgr.modProduct(products[j].get("pc_id",0l),productPara);
			}
		}
	}
	
	
	public void setFloorOrderProcessMgr(FloorOrderProcessMgrTJH floorOrderProcessMgr) {
		this.floorOrderProcessMgr = floorOrderProcessMgr;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorSupplierMgr(FloorSupplierMgrTJH floorSupplierMgr) {
		this.floorSupplierMgr = floorSupplierMgr;
	}

	public void setFloorOrderProcessMgrZJ(
			FloorOrderProcessMgrZJ floorOrderProcessMgrZJ) {
		this.floorOrderProcessMgrZJ = floorOrderProcessMgrZJ;
	}

	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
}
