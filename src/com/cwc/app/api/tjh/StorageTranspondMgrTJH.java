package com.cwc.app.api.tjh;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.tjh.FloorStorageTranspondMgrTJH;
import com.cwc.app.iface.tjh.StorageTranspondMgrIFaceTJH;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageTranspondMgrTJH implements StorageTranspondMgrIFaceTJH {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorStorageTranspondMgrTJH floorStorageTranspondMgr;

	/**
	 * 添加代发仓库信息
	 * @throws Exception 
	 */
	public void addStorageTranspond(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow rows = new DBRow();
			long y_sid = StringUtil.getLong(request, "y_sid");
			long catalog_id = StringUtil.getLong(request, "catalog_id");
			long d_sid = StringUtil.getLong(request, "d_sid");
			long type_id = StringUtil.getLong(request, "type_id");
			
			rows.add("y_sid", y_sid);
			rows.add("catalog_id", catalog_id);
			rows.add("d_sid", d_sid);
			rows.add("modality_id", type_id);
			
			floorStorageTranspondMgr.addStorageTranspond(rows);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e," addStorageTranspond(HttpServletRequest request) ",log);
		}
		
	}

	/**
	 * 分页查询仓库间商品代发信息
	 */
	public DBRow [] getAllStorageTranspond(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return (floorStorageTranspondMgr.getAllStorageTranspond(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllStorageTranspond(PageCtrl pc)",log);
		}
		
	}
	
	/**
	 * 查看仓库间商品代发的信息
	 */
	public DBRow getDetailStorageTrandspond(long id) 
		throws Exception 
	{
		try 
		{
			return (floorStorageTranspondMgr.getDetailStorageTrandspondById(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailStorageTrandspond(long id)",log);
		}
	}

	/**
	 * 修改某一具体的仓库间商品代发信息
	 * @throws Exception 
	 */
	public void modStorageTranspond(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow rows = new DBRow();
			
			long transpond_id = StringUtil.getLong(request,"transpondId");
			long prior_storage_id = StringUtil.getLong(request, "prior_sid");
			long category_id = StringUtil.getLong(request, "catalog_id");
			long d_storage_id = StringUtil.getLong(request, "d_sid");
			long type_id = StringUtil.getLong(request, "type_id");
			
			rows.add("y_sid", prior_storage_id);
			rows.add("catalog_id", category_id);
			rows.add("d_sid", d_storage_id);
			rows.add("modality_id", type_id);
			floorStorageTranspondMgr.modStorageTranspond(transpond_id,rows);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modStorageTranspond(HttpServletRequest request)",log);
		}
		
	}

	/**
	 * 删除仓库间商品的代发关系
	 * @throws Exception 
	 */
	public void delStorageTranspond(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request, "transpond_id");
			floorStorageTranspondMgr.delStorageTranspond(id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delStorageTranspond(HttpServletRequest request)",log);
		}
		
	}

	
	public void setFloorStorageTranspondMgr(
			FloorStorageTranspondMgrTJH floorStorageTranspondMgr) {
		this.floorStorageTranspondMgr = floorStorageTranspondMgr;
	}
}
