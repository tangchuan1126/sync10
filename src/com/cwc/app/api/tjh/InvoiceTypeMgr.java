package com.cwc.app.api.tjh;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.tjh.FloorInvoiceTypeMgr;
import com.cwc.app.iface.tjh.InvoiceTypeMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class InvoiceTypeMgr implements InvoiceTypeMgrIFace {
	static Logger log = Logger.getLogger("ACTION");
    private FloorInvoiceTypeMgr flooInvoiceTyoeMgr;
	
	/**
	 * ps_id  所属仓库ID
	 * 根据所属仓库id 获得该仓库下的数据
	 */
	public DBRow[] getDetailInvoiceById(long ps_id) 
		throws Exception 
	{
		try
		{
			return (flooInvoiceTyoeMgr.getDetailInvoiceInfo(ps_id));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getDetailInvoiceInfo",log);
		}
	}
	
	
	/**
	 * 查询出口发件人信息
	 */
	public DBRow getDetailInvoiceCompanyNameByDiId(long di_id) 
		throws Exception 
	{
		try
		{
			return (flooInvoiceTyoeMgr.getDetailInvoiceCompanyNameByDiId(di_id));
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getDetailInvoiceCompanyNameByDiId",log);
		}
	}


	/**
	 * 获取所有的发件人信息
	 */
	public DBRow[] getAllInvoice() 
		throws Exception 
	{
		try 
		{
			return (flooInvoiceTyoeMgr.getAllInvoice());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllInvoice()",log);
		}
	}
	
	/**
	 * 添加发票模板信息
	 * @throws Exception 
	 */
	public void addInvoiceInfo(HttpServletRequest request) 
		throws Exception 
	{
		
		try 
		{
			DBRow rows = new DBRow();
			String dog = StringUtil.getString(request, "dog");
			String rfe = StringUtil.getString(request, "rfe");
			String uv = StringUtil.getString(request, "uv");
			String tv = StringUtil.getString(request, "tv");
			long di_id = StringUtil.getLong(request, "di_id");
			String name = StringUtil.getString(request, "t_name");
			long ps_id = StringUtil.getLong(request, "ps_id");
			String hs_code = StringUtil.getString(request,"hs_code");
			String material = StringUtil.getString(request,"material");
			
			rows.add("dog", dog);
			rows.add("rfe", rfe);
			rows.add("uv", uv);
			rows.add("tv", tv);
			rows.add("di_id", di_id);
			rows.add("t_name", name);
			rows.add("ps_id", ps_id);
			rows.add("hs_code",hs_code);
			rows.add("material",material);
			
			flooInvoiceTyoeMgr.addInvoiceInfo(rows);
		} 
		catch (Exception e) 
		{
			
			throw new SystemException(e,"addInvoiceInfo(request) ",log);
		}
		
	}
	
	/**
	 * 根据id查看某一发票的详细信息
	 */
	public DBRow getDetailInvoiceTemplateById(long invoiceId) 
		throws Exception 
	{
		try 
		{
			return (flooInvoiceTyoeMgr.getDetailInvoiceTemplateById(invoiceId));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailInvoiceTemplateById(invoiceId) ",log);
		}
	}
	
	/**
	 * 修改发票模板的详细信息
	 * @throws Exception 
	 */
	public void modInvoiceTemplate(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			long invoice_id = StringUtil.getLong(request, "invoice_id");
			String dog = StringUtil.getString(request, "dog");
			String rfe = StringUtil.getString(request, "rfe");
			String uv = StringUtil.getString(request, "uv");
			String tv = StringUtil.getString(request, "tv");
			String name = StringUtil.getString(request, "t_name");
			long di_id = StringUtil.getLong(request, "di_id");
			long ps_id = StringUtil.getLong(request, "ps_id");
			int is_round_invoice = StringUtil.getInt(request,"is_round_invoice");
			String hs_code = StringUtil.getString(request,"hs_code");
			String material = StringUtil.getString(request,"material");
			
			row.add("dog", dog);
			row.add("rfe", rfe);
			row.add("uv", uv);
			row.add("tv", tv);
			row.add("t_name", name);
			row.add("di_id", di_id);
			row.add("ps_id", ps_id);
			row.add("is_round_invoice",is_round_invoice);
			row.add("hs_code",hs_code);
			row.add("material",material);
			
			flooInvoiceTyoeMgr.modInvoiceTemplate(invoice_id,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modInvoiceTemplate(request) ",log);
		}
		
	}
	
	/**
	 * 获取仓库信息
	 */
	public DBRow getStorageCategory(long id) 
		throws Exception 
	{
		try 
		{
			return (flooInvoiceTyoeMgr.getStorageCategory(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getStorageCategory(id)",log);
		}
	}

	
	public void setFlooInvoiceTyoeMgr(FloorInvoiceTypeMgr flooInvoiceTyoeMgr) {
		this.flooInvoiceTyoeMgr = flooInvoiceTyoeMgr;
	}

	
	
	
}
