package com.cwc.app.api.tjh;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.assets.HaveAssetsCategoryException;
import com.cwc.app.floor.api.tjh.FloorAssetsCategoryMgr;
import com.cwc.app.iface.tjh.AssetsCategoryMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class AssetsCategoryMgr implements AssetsCategoryMgrIFace{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorAssetsCategoryMgr floorAssetsCategroyMgr;

	/**
	 * 获取所有的资产类别信息
	 */
	public DBRow[] getAssetsCategoryTree()
		throws Exception
	{
		try 
		{
			DBRow dbrow [] = floorAssetsCategroyMgr.getAssetsCategory();
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsCategoryTree",log);
		}
	}
	
	public DBRow[] getAssetsProductLine(long productLineId)
		throws Exception
	{
		try 
		{
			DBRow dbrow [] = floorAssetsCategroyMgr.getAssetsProductLine(productLineId);
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsProductLine",log);
		}
	}
	public DBRow[] getAllAssetsCategoryTree() 
		throws Exception 
	{
		try 
		{
			Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
			DBRow dbrow [] = tree.getTree();
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllAssetsCategoryTree",log);
		}
	}

	
	/**
	 * 添加资产类别信息
	 * @throws Exception 
	 */
	public void addAssetsCategory(HttpServletRequest request)
		throws Exception 
	{
		
		try 
		{
			int parentId = StringUtil.getInt(request,"parentId");
			String chName = StringUtil.getString(request,"chName"); 
			String enName = StringUtil.getString(request,"enName");
			
			DBRow [] subCategory = floorAssetsCategroyMgr.getParentAssetsCategoryJSON(parentId);
			
			int sort;
			if(subCategory.length != 0)
			{
				sort = subCategory[subCategory.length-1].get("sort", 0)+1;
			}
			else
			{
				sort = 1;
			}
			
			DBRow dbrow = new DBRow();
			dbrow.add("parentId", parentId);
			dbrow.add("chName", chName);
			dbrow.add("enName", enName);
			dbrow.add("sort", sort);
			
			floorAssetsCategroyMgr.addAssetsCategory(dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addAssetsCategory",log);
		}
		
	}
	
	
	/**
	 * 删除资产类别相关信息
	 * @throws Exception 
	 */
	public void delAssetsCategory(HttpServletRequest request) 
		throws HaveAssetsCategoryException,Exception 
	{
		try 
		{
			long categoryId = StringUtil.getLong(request,"id");
			if(categoryId > 0)
			{
				DBRow [] dbrow = floorAssetsCategroyMgr.getParentAssetsCategoryJSON(categoryId);
				
				if(dbrow.length != 0)
				{
					throw new HaveAssetsCategoryException();
				}
				else
				{
					floorAssetsCategroyMgr.delAssetsCategory(categoryId);
				}
			}
		}
		catch (HaveAssetsCategoryException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delAssetsCategory",log);
		}
		
	}
	
	/**
	 * 根据id查询相关资产类别信息
	 * @throws Exception 
	 */
	public DBRow getDetailAssetsCategory(long id) 
		throws Exception 
	{
		try 
		{
			return (floorAssetsCategroyMgr.getDetailAssetsCategory(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailAssetsCategory",log);
		}
	}
	
	/**
	 * 修改某一具体的资产类别信息
	 * @throws Exception 
	 */
	public void modAssetsCategory(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			int id = StringUtil.getInt(request,"id");
			String chName = StringUtil.getString(request, "chName");
			String enName = StringUtil.getString(request,"enName");
			
			DBRow dbrow = new DBRow();
			dbrow.add("chName", chName);
			dbrow.add("enName",enName);
			
			floorAssetsCategroyMgr.modAssetsCategory(id,dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modAssetsCategory",log);
		}
	}
	
	/**
	 * 根据父类id查询该类下的所有子类信息
	 */
	public DBRow[] getAssetsCategoryChildren(long parentId) 
		throws Exception 
	{
		try 
		{
			return floorAssetsCategroyMgr.getParentAssetsCategoryJSON(parentId);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsCategoryChildren",log);
		}
	}

	/**
	 * 根据父类id查询该类下的所有子类信息
	 */
	public DBRow[] getAssetsCategoryChildren(long parentId,String type) 
		throws Exception 
	{
		try 
		{
			return floorAssetsCategroyMgr.getParentAssetsCategoryJSON(parentId,type);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsCategoryChildren",log);
		}
	}
	/**
	 * 获取parentid=0或为null的商品分类信息
	 */
	public DBRow[] getProductParentCatalog() 
		throws Exception 
	{
		try 
		{
			return (floorAssetsCategroyMgr.getProductParentCatalog());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductParentCatalog()",log);
		}
	}


	/**
	 * 根据父类id获取该类下所有商品分类子类信息
	 */
	public DBRow[] getProductCatalogByParentId(long parentid) 
		throws Exception 
	{
		try 
		{
			return (floorAssetsCategroyMgr.getProductCatalogByParentId(parentid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductCatalogByParentId(long parentid)",log);
		}
	}
	
	public void setFloorAssetsCategroyMgr(
			FloorAssetsCategoryMgr floorAssetsCategroyMgr) {
		this.floorAssetsCategroyMgr = floorAssetsCategroyMgr;
	}
}
