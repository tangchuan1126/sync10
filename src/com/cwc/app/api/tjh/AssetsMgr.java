package com.cwc.app.api.tjh;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.gzy.FloorAdminMgrGZY;
import com.cwc.app.floor.api.tjh.FloorAssetsCategoryMgr;
import com.cwc.app.floor.api.tjh.FloorAssetsMgr;
import com.cwc.app.floor.api.tjh.FloorOfficeLocationMgrTJH;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.iface.tjh.AssetsMgrIFace;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.ImportAssetsErrorKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class AssetsMgr implements AssetsMgrIFace{

	private FloorAssetsMgr floorAssetsMgr;
	private FloorAssetsCategoryMgr floorAssetsCategroyMgr;
	private FloorOfficeLocationMgrTJH floorOfficeLocationMgr;
	private FloorAdminMgrGZY floorAdminMgr;
	private FloorProductLineMgrTJH floorProductLineMgr;
	private FloorTransportMgrZr floorTransportMgrZr;
	static Logger log = Logger.getLogger("ACTION");
	
	/**
	 * 分页查询所有的资产信息
	 */
	public DBRow [] AssetsList(PageCtrl pc,String type) 
		throws Exception
	{
		try 
		{
			return (floorAssetsMgr.AssetsList(pc,type));
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"AssetsMgr.AssetsList(PageCtrl pc) error:",log);
		}
	}
	
	/**
	 * 添加资产信息
	 */
	public void addAssets(HttpServletRequest request) 
		throws  Exception 
	{
		
		try 
		{
			DBRow row = new DBRow();
			String a_name = StringUtil.getString(request,"a_name");
			String a_barcode = StringUtil.getString(request, "a_barcode");
			String unit_name = StringUtil.getString(request,"unit_name");
			double unit_price = StringUtil.getDouble(request,"unit_price");
			String standard = StringUtil.getString(request, "standard");
			//String purchase_Date = StrUtil.getString(request,"purchase_date");
			String suppliers = StringUtil.getString(request,"suppliers");
			int category_id = StringUtil.getInt(request,"category_id");
			String location = StringUtil.getString(request, "location");
			String requisitioned = StringUtil.getString(request,"requisitioned");
			int state = StringUtil.getInt(request,"state");
			int goodsstate = StringUtil.getInt(request,"goodsstate");
			String currency = StringUtil.getString(request,"currency");
			
			row.add("a_name", a_name);
			row.add("a_barcode", a_barcode);
			row.add("unit_name", unit_name);
			row.add("unit_price", unit_price);
			row.add("standard", standard);
			row.add("purchase_date",DateUtil.NowStr());
			row.add("suppliers", suppliers);
			row.add("category_id", category_id);
			row.add("location_id", location);
			row.add("requisitioned", requisitioned);
			row.add("state", state);
			row.add("goodsstate", goodsstate);
			row.add("currency", currency);
			floorAssetsMgr.addAssets(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"AssetsMgr.addAssets(request) error:",log);
		}
	}
	public DBRow[] getAccounts() throws Exception {
		return floorAssetsMgr.getAccounts();
	}
	public DBRow[] getAdminGroup() throws Exception {
		return floorAssetsMgr.getAdminGroup();
	}
	public DBRow[] getAdmin() throws Exception {
		return floorAssetsMgr.getAdmin();
	}
	
	/**
	 * 查看单个的资产详细信息
	 * @throws Exception 
	 */
	public DBRow getDetailAssets(long aid) 
		throws Exception 
	{
		try 
		{
			return (floorAssetsMgr.getDetailAssets(aid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailAssets(long aid)",log);
		}
	}
	
	//上传
	public void uploadImage(HttpServletRequest request) throws Exception{
			try {
				AdminMgr adminMgr = new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				
				//添加关联文件重新命名文件名称.将文件移动到新的文件夹中
				long id = StringUtil.getLong(request,"a_id");
				String fileNames = StringUtil.getString(request, "file_name");
				int file_with_type = StringUtil.getInt(request, "file_with_type");
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				StringBuffer fileNameSb = new StringBuffer(""); 
				
					if(fileNames.trim().length() > 0 ){
						String[] fileNameArray =  fileNames.split(",");
						
						if(fileNameArray != null && fileNameArray.length > 0){
							for(String tempFileName : fileNameArray){
								StringBuffer context = new StringBuffer();
								context.append(StringUtil.getString(request,"context"));
								String suffix = this.getSuffix(tempFileName);
								String tempUrl =  baseTempUrl+tempFileName;
								
								StringBuffer realFileName = new StringBuffer(sn).append("_").append(id)
								.append("_").append(tempFileName.replace("."+suffix, ""));
								int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
								if(indexFileName != 0){
									realFileName.append("_").append(indexFileName);
								}
								realFileName.append(".").append(suffix);
								 
								fileNameSb.append(",").append(realFileName);
								StringBuffer url = new StringBuffer();
								url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
								FileUtil.moveFile(tempUrl,url.toString());
								// file表上添加一条记录
								this.addAssetsReceiveImg(realFileName.toString(), id, file_with_type, 0, adminLoggerBean.getAdid(), DateUtil.NowStr());
							}
						}
					}
			} catch (Exception e) {
				throw new SystemException(e,"uploadImage",log);
			}
	}

	/**
	 * 修改某一具体的资产详细信息
	 * @throws Exception 
	 * @throws  
	 */
	public void modAssets(HttpServletRequest request) 
		throws  Exception 
	{
		try {
			long id = StringUtil.getLong(request,"a_id");
			String a_name = StringUtil.getString(request,"a_name");
			String a_barcode = StringUtil.getString(request, "a_barcode");
			String unit_name = StringUtil.getString(request,"unit_name");
			double unit_price = StringUtil.getDouble(request,"unit_price");
			String purchase_Date = StringUtil.getString(request,"purchase_date");
			String suppliers = StringUtil.getString(request,"suppliers");
			int category_id = StringUtil.getInt(request,"category_id");
			long product_line_id = StringUtil.getLong(request,"product_line_id");
			String location = StringUtil.getString(request, "location");
			int requisitioned_id = StringUtil.getInt(request,"requisitioned_id");
			String requisitioned = StringUtil.getString(request,"requisitioned");
			int state = StringUtil.getInt(request,"state");
			int center_account_id = StringUtil.getInt(request,"center_account_id");
			int goodsState = StringUtil.getInt(request,"goodsState");
			int applyState = StringUtil.getInt(request,"applyState");
			String handle_time=StringUtil.getString(request,"handle_time");
			double handle_price=StringUtil.getDouble(request,"handle_price");
			String responsi_person=StringUtil.getString(request,"responsi_person");
			String return_name=StringUtil.getString(request,"return_name");
			String consignee=StringUtil.getString(request,"consignee");
			String receive_time=StringUtil.getString(request,"receive_time");
			String currency=StringUtil.getString(request,"currency");
			int creater_id=StringUtil.getInt(request,"creater_id");
			String requisition_date=StringUtil.getString(request,"requisition_date");
			//保存上传图片
			this.uploadImage(request);
		
			DBRow row = new DBRow();
			row.add("a_name", a_name);
			row.add("a_barcode", a_barcode);
			row.add("unit_name", unit_name);
			row.add("unit_price", unit_price);
			if(!purchase_Date.equals(""))
			{
			row.add("purchase_date", purchase_Date);
			}
			row.add("suppliers", suppliers);
			row.add("category_id", category_id);
			row.add("product_line_id", product_line_id);
			row.add("location_id", location);
			row.add("requisitioned_id", requisitioned_id);
			row.add("requisitioned", requisitioned);
			row.add("state", state);
			if(!requisition_date.equals(""))
			{
				row.add("requisition_date", requisition_date);
			}
			row.add("center_account_id", center_account_id);
			row.add("goodsState", goodsState);
			row.add("applyState", applyState);
			if(!handle_time.equals(""))
			{
			row.add("handle_time", handle_time);
			}
			row.add("handle_price", handle_price);
			row.add("responsi_person", responsi_person);
			row.add("return_name", return_name);
			row.add("consignee", consignee);
			if(!receive_time.equals(""))
			{
				row.add("receive_time", receive_time);
			}
			row.add("currency", currency);
			row.add("creater_id", creater_id);
			
			floorAssetsMgr.updateAssets(id,row);
		} catch (Exception e) {
			throw new SystemException(e,"modAssets(request)",log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	/**
	 * 文件可以带多个的
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	private void addAssetsReceiveImg(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception {
		try{
			 
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addTransportCertificateFile(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addAssetsReceiveImg", log);
		}
	}
	
	/**
	 * 删除一条资产信息
	 * @throws Exception 
	 */
	public void delAssets(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long aid = StringUtil.getLong(request,"a_id");
			floorAssetsMgr.delAssets(aid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delAssets",log);
		}
		
	}
	
	/**
	 * 根据资产类别过滤资产信息
	 */
	public DBRow[] getAssetsByCategoryIdAndLocationId(int state,int goodsState,long locationId,long categoryId, PageCtrl pc,long productLineId, String type) 
		throws Exception 
	{
		
		try 
		{
			return floorAssetsMgr.getAssetsByCategoryIdAndLocationId(state,goodsState,locationId,categoryId,pc,productLineId,type);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsByCategoryId(categoryId,pc)",log);
		}
		
	}
	
	/**
	 * 根据查询字符查询相关的资产信息
	 */
	public DBRow[] getSearchAssets(String key, PageCtrl pc, String type) 
		throws Exception 
	{

		try 
		{
			return floorAssetsMgr.getSearchAssets(key,pc,type);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchAssets(key,pc)",log);
		}
	}
	
	/**
	 * 批量修改资产信息
	 */
	public void batchModAssets(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String aids [] = request.getParameterValues("batch_aid");
			DBRow rows = new DBRow();
			for(int i=0;i<aids.length;i++){
				String a_name = StringUtil.getString(request, "a_name_"+aids[i]);
				String a_code = StringUtil.getString(request, "a_code_"+aids[i]);
				String unit_name = StringUtil.getString(request, "unit_name_"+aids[i]);
				String standard = StringUtil.getString(request, "standard_"+aids[i]);
				double unit_price = StringUtil.getDouble(request, "unit_price_"+aids[i]);
				String location = StringUtil.getString(request, "location_"+aids[i]);
				
				rows.add("a_name", a_name);
				rows.add("a_barcode",a_code);
				rows.add("unit_name", unit_name);
				rows.add("unit_price", unit_price);
				rows.add("location_id", location);
				rows.add("standard", standard);
				
				
				floorAssetsMgr.batchModAssets(aids[i],rows);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"batchModAssets(request)",log);
		}
	}
	
	/**
	 * 导出资产信息到excel
	 */
	public String exportAssets(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			int location_id=StringUtil.getInt(request, "location_id");
			int product_line_id=StringUtil.getInt(request,"product_line_id");
			int categoryId=StringUtil.getInt(request,"categoryId",0);
			int state=StringUtil.getInt(request,"state");
			int goodsState=StringUtil.getInt(request,"goodsState");
			String type=StringUtil.getString(request,"type");
			
			
			DBRow [] exportAssets = floorAssetsMgr.exportAssets(location_id,product_line_id,categoryId,state,goodsState,type);
			long [] assets = new long[exportAssets.length];
			
			if (exportAssets.length > 0) 
			{
				POIFSFileSystem fs = null;   //获取模板
				fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/assets/ExportAssets.xls"));
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);
				HSSFRow row = sheet.getRow(0);
				HSSFCellStyle style = wb.createCellStyle();
				style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				style.setLocked(false);
				style.setWrapText(true);
				
				
				HSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); //创建一个锁定样式
				stylelock.setAlignment(HSSFCellStyle.ALIGN_LEFT);
				
				for (int i = 0; i < exportAssets.length; i++) 
				{
					row = sheet.createRow((int)i+1);
					sheet.setDefaultColumnWidth(26);
					DBRow asset = exportAssets[i];
					//设置excel列的值
					assets[i] = asset.get("aid", 0l);
					row.createCell(0).setCellValue(asset.get("aid",0l));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(asset.get("category_id", 0l));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(asset.getString("chName"));
					row.getCell(2).setCellStyle(style);
					
					row.createCell(3).setCellValue(asset.getString("a_name"));
					row.getCell(3).setCellStyle(style);
					
					row.createCell(4).setCellValue(asset.getString("a_barcode"));
					row.getCell(4).setCellStyle(style);
					
					row.createCell(5).setCellValue(asset.getString("unit_name"));
					row.getCell(5).setCellStyle(style);
					
//					row.createCell(6).setCellValue(asset.getString("standard"));
//					row.getCell(6).setCellStyle(style);
					
					row.createCell(6).setCellValue(asset.get("unit_price", 0.00));
					row.getCell(6).setCellStyle(style);
					
					row.createCell(7).setCellValue(asset.getString("currency"));
					row.getCell(7).setCellStyle(style);
					
					row.createCell(8).setCellValue(asset.getString("employe_name"));
				//	row.getCell(9).setCellStyle(style);
					
					row.createCell(9).setCellValue(asset.getString("purchase_date"));
					row.getCell(9).setCellStyle(style);
					if(asset.get("category_id", 0)==100023){
					row.createCell(10).setCellValue(asset.getString("name"));
					}
					else{
						row.createCell(10).setCellValue("");
						row.getCell(10).setCellStyle(style);
					}
					row.createCell(11).setCellValue(asset.getString("suppliers"));
					row.getCell(11).setCellStyle(style);
					
					row.createCell(12).setCellValue(asset.getString("office_name"));
					
					String state1 = "";
					if(asset.get("state",0) == 1)
					{
						state1 = "完好";
					}
					else if(asset.get("state",0) == 2)
					{
						state1 = "功能残损";
					}
					else if(asset.get("state",0) == 3)
					{
						state1 = "外观残损";
					}
					else if(asset.get("state",0) == 4)
					{
						state1 = "报废";
					}
					
					row.createCell(13).setCellValue(state1);
					row.getCell(13).setCellStyle(style);
					
					String state2 = "";
					if(asset.get("goodsState",0) == 1)
					{
						state2 = "未到货";
					}
					else if(asset.get("goodsState",0) == 2)
					{
						state2 = "已到货";
					}
					else if(asset.get("goodsState",0) == 3)
					{
						state2 = "已售出";
					}
					else if(asset.get("goodsState",0) == 4)
					{
						state2 = "已退货";
					}
					else if(asset.get("goodsState",0) == 5)
					{
						state2 = "已换货";
					}
					
					row.createCell(14).setCellValue(state2);
					row.getCell(14).setCellStyle(style);
					
					row.createCell(15).setCellValue(asset.getString("requisitioned"));
					row.getCell(15).setCellStyle(style);
					
					row.createCell(16).setCellValue(asset.getString("requisition_date"));
					row.getCell(16).setCellStyle(style);
					
					row.createCell(17).setCellValue(asset.getString("consignee"));
					row.getCell(17).setCellStyle(style);
					
					row.createCell(18).setCellValue(asset.getString("receive_time"));
					row.getCell(18).setCellStyle(style);
					if(asset.get("goodsState",0) == 3){
					row.createCell(19).setCellValue(asset.getString("responsi_person"));
					row.getCell(19).setCellStyle(style);
					
					row.createCell(20).setCellValue(asset.getString("handle_time"));
					row.getCell(20).setCellStyle(style);
					
					row.createCell(21).setCellValue(asset.get("handle_price", 0.00));
					row.getCell(21).setCellStyle(style);
					}else{
						row.createCell(19).setCellValue("");
						row.getCell(19).setCellStyle(style);
						
						row.createCell(20).setCellValue("");
						row.getCell(20).setCellStyle(style);
						
						row.createCell(21).setCellValue("");
						row.getCell(21).setCellStyle(style);
					}
					if(asset.get("goodsState",0) == 4){
					row.createCell(22).setCellValue(asset.getString("responsi_person"));
					row.getCell(22).setCellStyle(style);
					
					row.createCell(23).setCellValue(asset.getString("handle_time"));
					row.getCell(23).setCellStyle(style);
					
					row.createCell(24).setCellValue(asset.get("handle_price", 0.00));
					row.getCell(24).setCellStyle(style);
					}else{
						row.createCell(22).setCellValue("");
						row.getCell(22).setCellStyle(style);
						
						row.createCell(23).setCellValue("");
						row.getCell(23).setCellStyle(style);
						
						row.createCell(24).setCellValue("");
						row.getCell(24).setCellStyle(style);
						
					}
					if(asset.get("goodsState",0) == 5){
					row.createCell(25).setCellValue(asset.getString("return_name"));
					row.getCell(25).setCellStyle(style);
					
					row.createCell(26).setCellValue(asset.getString("handle_time"));
					row.getCell(26).setCellStyle(style);
					}else{
						row.createCell(25).setCellValue("");
						row.getCell(25).setCellStyle(style);
						
						row.createCell(26).setCellValue("");
						row.getCell(26).setCellStyle(style);
					}
					String applyState="";
					if(asset.get("applyState",0)==1){
						applyState="无需申请";
					}else if(asset.get("applyState",0)==2){
						applyState="未申请";
					}else if(asset.get("applyState",0)==3){
						applyState="已申请";
					}
					
					row.createCell(27).setCellValue(applyState);
					row.getCell(27).setCellStyle(style);
					
				}
				
				HSSFSheet sheet2 = wb.getSheetAt(1);
				HSSFRow row2 = sheet2.getRow(0);
				Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
				DBRow [] categories = tree.getTree();
				for (int j = 0; j < categories.length; j++) {
					row2 = sheet2.createRow((int)j+1);
					sheet2.setDefaultColumnWidth(26);
					DBRow assets_category = categories[j];
					
					row2.createCell(0).setCellValue(assets_category.get("id",0l));
					row2.getCell(0).setCellStyle(style);
					
					row2.createCell(1).setCellValue(assets_category.getString("chName"));
					row2.getCell(1).setCellStyle(style);
					
					row2.createCell(2).setCellValue(assets_category.getString("enName"));
					row2.getCell(2).setCellStyle(style);
				}
				
				HSSFSheet sheet3 = wb.getSheetAt(2);
				HSSFRow row3 = sheet3.getRow(0);
				Tree locationTree = new Tree(ConfigBean.getStringValue("office_location"));
				DBRow [] locations = locationTree.getTree();
				for (int i = 0; i < locations.length; i++) {
					row3 = sheet3.createRow((int)i+1);
					sheet3.setDefaultColumnWidth(26);
					DBRow location = locations[i];
					
					row3.createCell(0).setCellValue(location.get("id",0l));
					row3.getCell(0).setCellStyle(stylelock);
					
					row3.createCell(1).setCellValue(location.getString("office_name"));
					row3.getCell(1).setCellStyle(stylelock);
				}
				
				
				String path = "upl_excel_tmp/export_assets_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
				FileOutputStream fout = new FileOutputStream(Environment
						.getHome()
						+ path);
				wb.write(fout);
				fout.close();
				return ("../../"+path);
				
			}
			else
			{
				return "0";
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"exportAssets(category_id)",log);
		}
	}
	
	/**
	 * 导入excel文件
	 */
	public String importAssets(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String permitFile = "xls";
			
			TUpload upload = new TUpload();
			
			String msg=new String();
			
			upload.setFileName("import_assets"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
				
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
				////system.out.println(msg);
				throw new FileTypeException();//资产文件上传格式不对
			}
			else if (flag==1)
			{
				msg = "上传出错";
				////system.out.println(msg);
			}
			else
			{		  
				msg = upload.getFileName();
			}
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importAssets(request)",log);
		}
	}

	/**
	 * 上传资产excel
	 */
	public HashMap<String, DBRow[]> excelshow(String filename, String type,String asserttype)
		throws Exception
	{
		try 
		{
			String path = "";
			InputStream is;
			Workbook wrb;
			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
			if (type.equals("show")) 
			{
				path = Environment.getHome() + "upl_excel_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				
				DBRow[] assets = assetsData(wrb);
				DBRow[] errorAssets = checkAssets(assets,asserttype);//检查资产
				resultMap.put("errorAssets",errorAssets);
			}
			else if(type.equals("data"))
			{
				path = Environment.getHome() + "upl_excel_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				assetsData(wrb);
				
				DBRow[] assets = assetsData(wrb);//DBRow[]
				
				resultMap.put("assets",assets);
			}
			
			
			return resultMap;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelshow(filename,type)",log);
		}
	}
	
	/**
	 * 检查资产信息
	 * @param assets
	 * @return
	 * @throws Exception 
	 */
	private DBRow[] checkAssets(DBRow[] assets,String asserttype) 
		throws Exception 
	{
		try 
		{
			ArrayList<DBRow> errorAssets = new ArrayList<DBRow>();
			for (int i = 0; i < assets.length; i++) 
			{
				boolean checkResult = false;//检查检查结果，true表示有错，false表示没错
				StringBuffer errorMessage = new StringBuffer("");//错误信息
				DBRow rows = assets[i];
				if(rows.getString("a_name").trim().equals(""))   //检查资产名称是否为空
				{
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.A_NAME+",");
					
				}
				if(!rows.getString("a_name").matches(".+/.+")){//检查资产名称是否是合法的形式
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.MATCHNAME+",");
				}
				
				if(asserttype.equals("1")){//检查导入的数据是否是资产
					if(rows.get("category_id", 0)==100023){
						checkResult = true;
						errorMessage.append(ImportAssetsErrorKey.CATEGORYERROR+",");
					}
				}
				if(asserttype.equals("2")){//检查导入的数据是否是样品
					if(rows.get("category_id", 0)!=100023){
						checkResult = true;
						errorMessage.append(ImportAssetsErrorKey.SAMPLEERROR+",");
					}else{
						if(rows.get("product_line_id", 0)==0){
							checkResult = true;
							errorMessage.append(ImportAssetsErrorKey.PRODUCTLINENOTEXIST+",");
						}
					}
				}
				if(rows.getString("unit_price").trim().equals(""))//检查购买价格
				{
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.PRICENULL+",");
				}
				if(rows.get("location_id", 0)==0){//检测地址
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.LOCATIONERROR+",");
				}
				if(rows.get("creater_id", 0)==0){//检测申请人是否存在
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.CREATERNULL+",");
				}
//				if(rows.get("category_id", 0)==100023){ //若不是样品则不需要检测产品线
//					if(rows.get("product_line_id", 0)==0){
//						checkResult = true;
//						errorMessage.append(ImportAssetsErrorKey.PRODUCTLINENOTEXIST+",");
//					}
//				}
				if((!rows.getString("goodsState").equals("未到货")&&
						!rows.getString("goodsState").equals("已到货")&&!rows.getString("goodsState").equals("已售出")&&!rows.getString("goodsState").equals("已退货")&&!rows.getString("goodsState").equals("已换货")))
				{
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.GOODSSTATENULL+",");
				}
				if(!rows.getString("state").equals("完好")&&!rows.getString("state").equals("功能残损")&&!rows.getString("state").equals("外观残损")&&!rows.getString("state").equals("报废"))
					{
						checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.STATENULL+",");
					}
				if(!rows.getString("applyState").equals("无需申请")&&!rows.getString("applyState").equals("未申请")&&!rows.getString("applyState").equals("已申请"))
				{
					checkResult = true;
				errorMessage.append(ImportAssetsErrorKey.APPLYSTATEERROR+",");
				}
				else
				{
					try 
					{
					  	double price = Double.valueOf(rows.getString("unit_price"));
						if(!(price > 0))
						{
							checkResult = true;
							errorMessage.append(ImportAssetsErrorKey.A_PRICE+",");
						}
						
					} 
					catch (Exception e) 
					{
						checkResult = true;
						errorMessage.append(ImportAssetsErrorKey.A_PRICENERROR+",");
					}
				
				}
				
				try 
				{
					long category_id = Long.parseLong(rows.getString("category_id"));   //检查是否存在该资产类别
					
					DBRow assets_category = floorAssetsCategroyMgr.getDetailAssetsCategory(category_id);
					if(assets_category == null)
					{
						checkResult = true;
						errorMessage.append(ImportAssetsErrorKey.A_CATEGORY+",");
					}
				} 
				catch (NumberFormatException e) 
				{
					checkResult = true;
					errorMessage.append(ImportAssetsErrorKey.CATEGORYTYPEERROR+",");   
				}
				if (checkResult)//一条记录检查有问题
				{
					rows.add("errorMessage", errorMessage.toString());
					rows.add("errorAssetsRow",i+2);
					errorAssets.add(rows);
				}
			}
			return (errorAssets.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkAssets(rows)",log);
		}
		
	}

	/**
	 * 转化成DBRow类型
	 * @param wrb
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	private DBRow[] assetsData(Workbook wrb) 
		throws Exception 
	{
		try 
		{
			HashMap param = new HashMap();
			param.put(0, "aid");
			param.put(1, "category_id");
			param.put(2, "category_name");
			param.put(3, "a_name");
			param.put(4, "a_barcode");
			param.put(5, "unit_name");
//			param.put(6, "standard");
			param.put(6, "unit_price");
            param.put(7, "currency");
			param.put(8, "creater_id"); //申请人			
			param.put(9, "purchase_date");		
			param.put(10, "product_line_id"); //产品线
			param.put(11, "suppliers");
			param.put(12, "location_id");		
			param.put(13, "state");
			param.put(14, "goodsState");			
			param.put(15, "requisitioned");
			param.put(16, "requisition_date");//领用时间
			param.put(17, "consignee");//收货人
			param.put(18, "receive_time");//收货时间
			param.put(19, "responsi_person");//收款人
			param.put(20, "handle_time");//售出时间
			param.put(21, "handle_price");//售出价格
			param.put(22, "responsi_person");//退货负责人
			param.put(23, "handle_time");//退货时间
			param.put(24, "handle_price");//退款额
			param.put(25, "return_name");
			param.put(26, "handle_time");//换货时间
			param.put(27, "applyState");//资产状态
		
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			Sheet rs = wrb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
				DBRow drow;
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();

			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
				   	String content = rs.getCell(j,i).getContents().trim();
				   	if(j==10||j==7)
				   	{
				   		content = content.toUpperCase();
}
				   	if(j==12){	
				     		if(!content.equals("")&&content!=null){		   				   		
				     			drow = floorOfficeLocationMgr.getLocationIdByName(content);
				     			if(drow!=null){
				     			content = drow.getString("id");
				     			}else{
				     				content="0";
				     			}
				     		}else
				     			content = "0";
				   	}
				   	if(j==10){
				   		if(!content.equals("")&&content!=null){
				   			drow = floorProductLineMgr.getProductLineByName(content);
				   			if(drow!=null){
				   				content = drow.getString("id");
				   			}else{
				   				content="0";
				   			}
				   			
				   		}else
				   			content = "0";
				   	}
				   	if(j==8){
				   		if(!content.equals("")&&content!=null){
				   			drow = floorAdminMgr.getAdminByEmployeName(content);
				   			if(drow!=null){
				   				content = drow.getString("adid");
				   			}else{
				   				content="0";
				   			}
				   			
				   		}else
				   			content = "0";
				   	}
				   	if(j==9){
					   	if(content.equals("")||content==null){
					   			content ="";
						}
					   	}
					if(j==16){
					   	if(content.equals("")||content==null){
					   			content ="";
						}
					   	}
					if(j==18){
					   	if(content.equals("")||content==null){
					   			content ="";
						}
					   	}
				   	
				   	if(j==20){
				   	if(!content.equals("")&&content!=null){
				   		pro.add(param.get(j).toString(),content);
					}
				   	}
				   	
				 	if(j==23){
					   	if(!content.equals("")&&content!=null){
					   		pro.add(param.get(j).toString(),content);
						}
					   	}
				 	
				 	if(j==26){
					   	if(!content.equals("")&&content!=null){
					   		pro.add(param.get(j).toString(),content);
						}
					   	}
				 if(j==19){
					 if(!content.equals("")&&content!=null){
						 pro.add(param.get(j).toString(),content);
					 }
				 }
				 if(j==22){
					 if(!content.equals("")&&content!=null){
						 pro.add(param.get(j).toString(),content);
					 }
				 }
				 if(j==21){
					 if(!content.equals("")&&content!=null){
						 pro.add(param.get(j).toString(),content);
					 }
				 }
				 if(j==24){
					 if(!content.equals("")&&content!=null){
						 pro.add(param.get(j).toString(),content);
					 }
				 }
				 
				 if( rs.getCell(20,i).getContents().trim().equals("")&& rs.getCell(23,i).getContents().trim().equals("")&& rs.getCell(26,i).getContents().trim().toString().equals("")){
					 pro.add(param.get(26).toString(),""); 
					 
				 }
				 
				   if(j!=19&&j!=20&&j!=21&j!=22&&j!=23&&j!=24&&j!=26){
					   pro.add(param.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
				   }	
					 
			    
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  
			  return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"assetsData(wrb)",log);
		}
	}
	
	/**
	 * 上传的excel导出结果集
	 */
	public void uploadAssets(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String tempfilename = StringUtil.getString(request,"tempfilename");
			String type = StringUtil.getString(request,"type");
			HashMap<String,DBRow[]> excelDBRow = excelshow(tempfilename,"data",type);//excel导出结果集
			DBRow[] rows = excelDBRow.get("assets");
			importAssets(rows);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadAssets(request)",log);
		}	
	}

	/**
	 * 保存或修改上传的资产信息
	 * @param rows
	 * @throws Exception
	 */
	private void importAssets(DBRow[] rows) 
		throws Exception 
	{
		
		try {
			if(rows != null)
			{
				for (int i = 0; i < rows.length; i++) {
					DBRow assets = rows[i];
					assets.remove("category_name");
					if(assets.get("category_id",0)!=100023){
						assets.remove("product_line_id");
					}
					if(assets.getString("purchase_date").equals("")){
						assets.remove("purchase_date");
					}
					if(assets.getString("requisition_date").equals("")){
						assets.remove("requisition_date");
					}
					if(assets.getString("handle_time").equals("")){
						assets.remove("handle_time");
					}
					if(assets.getString("receive_time").equals("")){
						assets.remove("receive_time");
					}
					
					if(assets.getString("aid").equals(""))   //判断资产id是否存在，存在新增数据
					{
						assets.remove("aid");
						int state;
	                    int goodsstate;
	                    int applyState=0;
						if(assets.getString("state").equals("完好"))
						{
							state = 1;
						}
						else if(assets.getString("state").equals("功能残损"))
						{
							state = 2;
						}
						else if(assets.getString("state").equals("外观残损")){
							state = 3;
						}
						else if(assets.getString("state").equals("报废"))
						{
							state = 4;
						}else{
							state = 0;
						}
						
						if(assets.getString("goodsState").equals("未到货")){
							goodsstate = 1;
						}else if(assets.getString("goodsState").equals("已到货")){
							goodsstate = 2;
						}else if (assets.getString("goodsState").equals("已出售")||assets.getString("goodsState").equals("已售出")){
							goodsstate = 3;
						}else if(assets.getString("goodsState").equals("已退货")){
							goodsstate = 4;
						}else if(assets.getString("goodsState").equals("已换货")){
							goodsstate = 5;
						}else{
							goodsstate = 0;
						}							
						if(assets.getString("applyState").equals("无需申请")){
							applyState=1;
						}else if(assets.getString("applyState").equals("未申请")){
							applyState=2;
						}else if(assets.getString("applyState").equals("已申请")){
							applyState=3;
						}
						
						
						assets.add("state", state);
						assets.add("goodsState", goodsstate);
						assets.add("applyState", applyState);
						floorAssetsMgr.addAssets(assets);
					}
					else                                //修改上传的资产信息
					{
						long aid = Long.valueOf(assets.getString("aid"));
						int state1;
						int goodsState1;
						int applyState=0;
						if(assets.getString("state").equals("完好"))
						{
							state1 = 1;
						}
						else if(assets.getString("state").equals("功能残损"))
						{
							state1 = 2;
						}
						else if(assets.getString("state").equals("外观残损")){
							state1 = 3;
						}
						else if(assets.getString("state").equals("报废"))
						{
							state1 = 4;
						}else{
							state1 = 0;
						}
						
						if(assets.getString("goodsState").equals("未到货")){
							goodsState1 = 1;
						}else if(assets.getString("goodsState").equals("已到货")){
							goodsState1 = 2;
						}else if (assets.getString("goodsState").equals("已出售")||assets.getString("goodsState").equals("已售出")){
							goodsState1 = 3;
						}else if(assets.getString("goodsState").equals("已退货")){
							goodsState1 = 4;
						}else if(assets.getString("goodsState").equals("已换货")){
							goodsState1 = 5;
						}else{
							goodsState1 = 0;
						}
						if(assets.getString("applyState").equals("无需申请")){
							applyState=1;
						}else if(assets.getString("applyState").equals("未申请")){
							applyState=2;
						}else if(assets.getString("applyState").equals("已申请")){
							applyState=3;
						}
						assets.add("state", state1);
						assets.add("goodsState", goodsState1);
						assets.add("applyState", applyState);
						floorAssetsMgr.updateAssets(aid, assets);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"importAssets(rows)",log);
		}
	}

	public void setFloorAssetsMgr(FloorAssetsMgr floorAssetsMgr) {
		this.floorAssetsMgr = floorAssetsMgr;
	}

	public void setFloorAssetsCategroyMgr(
			FloorAssetsCategoryMgr floorAssetsCategroyMgr) {
		this.floorAssetsCategroyMgr = floorAssetsCategroyMgr;
	}

	public void setFloorOfficeLocationMgr(
			FloorOfficeLocationMgrTJH floorOfficeLocationMgr) {
		this.floorOfficeLocationMgr = floorOfficeLocationMgr;
	}

	public void setFloorAdminMgr(FloorAdminMgrGZY floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}

	public void setFloorProductLineMgr(FloorProductLineMgrTJH floorProductLineMgr) {
		this.floorProductLineMgr = floorProductLineMgr;
	}

	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}
	
	
}
