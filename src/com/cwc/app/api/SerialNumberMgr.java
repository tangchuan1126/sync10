package com.cwc.app.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.mgr.SerialNumberMgrAPI;
import com.cwc.app.exception.serialnumber.NumberIndexOfException;
import com.cwc.app.exception.serialnumber.SerialNumberAlreadyInStoreException;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class SerialNumberMgr implements SerialNumberMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private SerialNumberMgrAPI serialNumberMgrAPI;
	
	/**
	 * 打印后记录序列号（存入数据库）
	 */
	public void addSerialNumber(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String serial_number = StringUtil.getString(request,"serial_number");
			long supplier_id = StringUtil.getLong(request,"supplier_id");
			long pc_id = StringUtil.getLong(request,"pc_id");
			long purchase_id = StringUtil.getLong(request,"purchase_id");
			
			serialNumberMgrAPI.addSerialNumber(serial_number,supplier_id,pc_id,purchase_id);
		}
		catch(NumberIndexOfException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 生产序列号(只是生成，未存入到数据库)
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow madeSerialNumber(HttpServletRequest request)
		throws Exception
	{
		long supplier_id = StringUtil.getLong(request,"supplier_id");
		long pc_id = StringUtil.getLong(request,"pc_id");
		
		int needcount = StringUtil.getInt(request,"needcount");
		
		return serialNumberMgrAPI.madeSerialNumber(supplier_id, pc_id, needcount);
	}
	
	public DBRow getSerialNumberDetail(long serial_number) 
		throws Exception 
	{
		try 
		{
			return serialNumberMgrAPI.getSerialNumberDetail(serial_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void initSerialProduct()
		throws Exception
	{
		serialNumberMgrAPI.initSerialProduct();
	}
	
	/**
	 * 序列号入库
	 * @param serial_number
	 * @param pc_id
	 * @throws Exception
	 */
	public void putSerialProduct(String serial_number,long pc_id,long slc_id)
		throws Exception
	{
		try
		{
			serialNumberMgrAPI.putSerialProduct(serial_number, pc_id, slc_id);
		}
		catch (SerialNumberAlreadyInStoreException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"putSerialProduct",log);
		}
	}
	
	public void outSerialProduct(String serial_number,long pc_id)
		throws Exception
	{
		serialNumberMgrAPI.outSerialProduct(serial_number, pc_id);
	}
	
	public void addSerialProduct(long pc_id,String serial_number,long supplier_id)
		throws Exception
	{
		serialNumberMgrAPI.addSerialProduct(pc_id, serial_number, supplier_id);
	}
	
	public DBRow getDetailSerialProduct(String serial_number)
		throws Exception
	{
		try
		{
			return serialNumberMgrAPI.getDetailSerialProduct(serial_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailSerialProduct",log);
		}
	}
	
	public void serialProductPositioning(String serial_number,long slc_id)
		throws Exception
	{
		serialNumberMgrAPI.serialProductPositioning(serial_number, slc_id);
	}

	public void setAPI(SerialNumberMgrAPI serialNumberMgrAPI) {
		this.serialNumberMgrAPI = serialNumberMgrAPI;
	}

	public void setSerialNumberMgrAPI(SerialNumberMgrAPI serialNumberMgrAPI) {
		this.serialNumberMgrAPI = serialNumberMgrAPI;
	}
}
