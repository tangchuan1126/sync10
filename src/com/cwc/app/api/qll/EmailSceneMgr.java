package com.cwc.app.api.qll;

import javax.servlet.http.HttpServletRequest;
 





import org.apache.log4j.Logger;
 





import com.cwc.app.exception.email.HaveEmailSceneException;
import com.cwc.app.exception.email.HaveEmailTemplateException;
import com.cwc.app.floor.api.qll.FloorEmailSceneMgr;
import com.cwc.app.floor.api.qll.FloorEmailTemplateMgr;
import com.cwc.app.iface.qll.EmailSceneMgrIFace;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;
public class EmailSceneMgr implements EmailSceneMgrIFace {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorEmailSceneMgr floorEmailSceneMgr;
	private FloorEmailTemplateMgr floorEmailTemplateMgr;


	/**
	 * 增加邮件场景信息
	 * @param request
	 * @throws Exception
    */ 
	public void addScene(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			String parentid = StringUtil.getString(request,"parentid"); 
			String sname = StringUtil.getString(request,"sname");
			
			DBRow dbrow = new DBRow();
			dbrow.add("parentid", parentid);
			dbrow.add("sname", sname);
			
			floorEmailSceneMgr.addScene(dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addScene",log);
		}

	}
	
	/**
	 * 删除邮件场景信息
	 * @param request
	 * @throws Exception
    */ 
	public void delScene(HttpServletRequest request)
		throws HaveEmailSceneException,HaveEmailTemplateException,Exception
	{
		try 
		{
			long sid = StringUtil.getLong(request,"sid");
			
			if(sid > 0)
			{
				if(floorEmailSceneMgr.getParentScene(sid).length != 0)
				{
					throw new HaveEmailSceneException();
				}
				else if (floorEmailTemplateMgr.getTemplateById(sid, null, null).length != 0)
				{
					throw new HaveEmailTemplateException();
				}
				else
				{
					floorEmailSceneMgr.delScene(sid);
				}
			}
		}
		catch (HaveEmailTemplateException e)
		{
			throw e;
		}
		catch (HaveEmailSceneException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delScene()",log);
		}

	}
	
	/**
	 *  获得所有邮件树
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllSceneTree()
		throws Exception
	{
		try 
		{
			Tree tree = new Tree(ConfigBean.getStringValue("assets_category"));
			DBRow dbrow [] = tree.getTree();
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllAssetsCategoryTree",log);
		}
	}
	
	/**
	 * 获得具体一个邮件场景信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailScene(long id) 
		throws Exception
	{
		try 
		{
			return (floorEmailSceneMgr.getDetailScene(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailScene",log);
		}
	}

	/**
	 * 通过场景名称获得邮件场景
	 * @param sname
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailSceneByName(String sname) 
		throws Exception
	{
		try 
		{
			return (floorEmailSceneMgr.getDetailSceneByName(sname));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailScene",log);
		}
	}
	/**
	 * 获得邮件子类信息
	 * @param parentId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSceneChildren(long parentId)
		throws Exception
	{	 
		try 
		{
			return floorEmailSceneMgr.getParentScene(parentId);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSceneChildren",log);
		}
	}

	/**
	 * 获得邮件树
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSceneTree()
		throws Exception 
	{
		try 
		{
			DBRow dbrow [] = floorEmailSceneMgr.getScene();
			return (dbrow);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAssetsCategoryTree",log);
		}
	}

	/**
	 * 修改邮件场景
	 * @param request
	 * @throws Exception
	 */
	public void modScene(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long sid = StringUtil.getInt(request,"sid");
			String sname = StringUtil.getString(request,"sname");
			
			DBRow dbrow = new DBRow();
			dbrow.add("sname",sname);
			
			floorEmailSceneMgr.modScene(sid,dbrow);
			 
		 } 
		catch (Exception e) 
		{
			throw new SystemException(e,"modAssetsCategory",log);
		}

	}
	
	
	public void setFloorEmailTemplateMgr(FloorEmailTemplateMgr floorEmailTemplateMgr) {
		this.floorEmailTemplateMgr = floorEmailTemplateMgr;
	}
	
	public void setFloorEmailSceneMgr(FloorEmailSceneMgr floorEmailSceneMgr) {
		this.floorEmailSceneMgr = floorEmailSceneMgr;
		 
	}
 

}
