package com.cwc.app.api.qll;
import java.net.ConnectException;

import org.apache.log4j.Logger;

import com.cwc.app.beans.ebay.qll.CompleteSaleParaBeans;
import com.cwc.app.beans.ebay.qll.MessageToBidderParaBeans;
import com.cwc.app.beans.ebay.qll.MessageToPartnerParaBeans;
import com.cwc.app.floor.api.qll.FloorEbayMgr;
import com.cwc.app.iface.qll.EbayMgrIFace;
import com.cwc.app.jms.action.MessageToBidderCall;
import com.cwc.app.jms.action.MessageToPartenerCall;
import com.cwc.app.jms.action.UpTrackingNumberCall;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.ApiException;
import com.ebay.sdk.SdkException;
import com.ebay.sdk.call.AddMemberMessageAAQToPartnerCall;
import com.ebay.sdk.call.AddMemberMessagesAAQToBidderCall;
import com.ebay.sdk.call.CompleteSaleCall;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.soap.eBLBaseComponents.AddMemberMessagesAAQToBidderRequestContainerType;
import com.ebay.soap.eBLBaseComponents.MemberMessageType;
import com.ebay.soap.eBLBaseComponents.QuestionTypeCodeType;
import com.ebay.soap.eBLBaseComponents.ShipmentTrackingDetailsType;
import com.ebay.soap.eBLBaseComponents.ShipmentType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;

public class EbayMgr  implements EbayMgrIFace
{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorEbayMgr floorEbayMgr;
	
	public void setFloorEbayMgr(FloorEbayMgr floorEbayMgr)
	{
		this.floorEbayMgr = floorEbayMgr;
	}


	private String  token="";
	private String  url="";
	
	
	public String getToken()
	{
		return token;
	}
	public void setToken(String token) 
	{
		this.token = token;
	}
	
	 
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * 在调用Ebay API之前需要初始化的ApiContext公共方法
	 * @return
	 * @throws Exception 
	 */
	private ApiContext getApiContext(String sellerID,String itemID) throws Exception
	{
		ApiContext apiContext = new ApiContext(); 
    
	    ApiCredential cred = apiContext.getApiCredential(); 
	   
	    cred.seteBayToken(getSellerToken(sellerID,itemID));
		 
	    apiContext.setApiServerUrl(this.url);           
	     
	    apiContext.getApiLogging().setLogSOAPMessages(true); 
	
	    apiContext.setSite(SiteCodeType.UK);  
	   
	    return (apiContext);
			
	}
	
	/**
	 * 为UpTrackingNumber API 需要的ShipmentType 设置参数的公共方法
	 * @param TrackingNumber
	 * @param Carrier
	 * @return
	 */
	private ShipmentType getShipmentType(String TrackingNumber,String Carrier)
	{
		ShipmentType shipmentType = new ShipmentType(); //存储货信息包括运单号及运货厂商
        ShipmentTrackingDetailsType shpmnt = new ShipmentTrackingDetailsType(); 
        shpmnt.setShipmentTrackingNumber(TrackingNumber); 
        shpmnt.setShippingCarrierUsed(Carrier); 
        shipmentType.setShipmentTrackingDetails(new ShipmentTrackingDetailsType[]{shpmnt}); 
        return (shipmentType);
	}
	
	/**
	 * 为向已成交的买家通过ToPartner API发的Message设置参数
	 * @param bean
	 * @return
	 */
	private MemberMessageType setToPartnerMessage(MessageToPartnerParaBeans bean)
	{
		 MemberMessageType message = new MemberMessageType(); //声明一个消息并填充消息内容
         message.setBody(bean.getMessageBody());
         message.setQuestionType(QuestionTypeCodeType.GENERAL);
         message.setRecipientID(new String[]{bean.getRecipientID()});
          return (message);
	}
	  
	/**
	 * 为向竞拍的买家通过toBidder  API 设置发送的Message的参数
	 * @param bean
	 * @return
	 */
	private MemberMessageType setToBidderMessage(MessageToBidderParaBeans bean)
	{
		 MemberMessageType message = new MemberMessageType();//声明一个消息并填充内容
		 message.setBody(bean.getMessageBody());
         message.setQuestionType(QuestionTypeCodeType.GENERAL);
         message.setRecipientID(new String[]{bean.getRecipientID()});//
          return (message);
	}
	
	/**
	 * 通过AddMemberMessagesAAQToBidderCall API 向竞拍的买家发送Message
	 * @param bean 此接口只能向买家发送消息的最大数量为2，测试环境只支持Business和Motors Category
	 */
	public void MessageToBidderCall(MessageToBidderParaBeans bean)
		throws Exception
	{
		
         try 
         {	
        	 ApiContext apiContext=getApiContext(bean.getSellerID(),bean.getItemID());
             AddMemberMessagesAAQToBidderCall call = new AddMemberMessagesAAQToBidderCall(apiContext);//用于向竞拍者发送消息的Call
             AddMemberMessagesAAQToBidderRequestContainerType[] container = new AddMemberMessagesAAQToBidderRequestContainerType[1];//存放消息的容器
             container[0]=new AddMemberMessagesAAQToBidderRequestContainerType();
             container[0].setItemID(bean.getItemID());
             container[0].setMemberMessage(setToBidderMessage(bean));
             call.setAddMemberMessagesAAQToBidderRequestContainer(container);
  		     call.addMemberMessagesAAQToBidder();
         } 
         catch (ApiException e) 
         {
        	 throw e;
//        	  throw new SystemException(e,"MessageToBidderCall:ApiException",log);
   		 }
         catch (SdkException e)
   		 {
        	 throw e;
//   		 throw new SystemException(e,"MessageToBidderCall:SdkException",log);
   		 }
   		 catch (Exception e)
   		 {
   			throw new SystemException(e,"MessageToBidderCall:Exception",log);
   		 } 
	}
	
	 
	/**
	 * 向在交易以成功的买家发送消息（条件是必须有交易账号即transactionID）
	 * @param bean
	 */
	public void MessageToPartenerCall(MessageToPartnerParaBeans bean)
		throws Exception
	{
		
        try 
        {   
        	ApiContext apiContext=getApiContext(bean.getSellerID(),bean.getItemID());
		    AddMemberMessageAAQToPartnerCall call = new AddMemberMessageAAQToPartnerCall(apiContext);//向已成交买家发送消息的Call
		    call.setItemID(bean.getItemID()); 
		    MemberMessageType message=setToPartnerMessage(bean);
		    call.setMemberMessage(message);
        	call.addMemberMessageAAQToPartner();
			
        }
        catch (ApiException e)
        {
		 
        	throw e;
//			  throw new SystemException(e,"MessageToPartenerCall:ApiException",log);
		} 
        catch (SdkException e)
        {
			throw e; 
//        	throw new SystemException(e,"MessageToPartenerCall:ApiException",log);
		} 
        catch (Exception e) 
        {
			throw new SystemException(e,"MessageToPartenerCall:ApiException",log);
		} 
	}
	
	/**
	 * 向在交易以成功的买家发送消息(JMS)
	 * @param bean
	 */
	public void MessageToPartenerCallJMS(MessageToPartnerParaBeans bean)
	{
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new MessageToPartenerCall(bean));
	}
	 
	/**
	 * 实现上传TrackingNumber的具体方法completeSaleCall API 此方法在上传竞拍商品的tracking number时只能在竞拍时间结束之后才可以
	 * @param bean
	 * @throws Exception
	 */
	public void UpTrackingNumberCall(CompleteSaleParaBeans bean)
		throws Exception
	{
		
		try
		{	
			ApiContext apiContext = getApiContext(bean.getSellerID(),bean.getItemID());
			CompleteSaleCall call = new CompleteSaleCall(apiContext); //上传Tracking Nubmer 的CAll
			
			call.setShipped(true);
			call.setItemID(bean.getItemID());
			call.setTransactionID(bean.getTransactionID());
			call.setShipment(getShipmentType(bean.getTrackingNumber(),bean.getCarrier()));
			call.setSite(SiteCodeType.US);
			call.completeSale();
			
		} 
		catch (ApiException e) 
		{
			log.error("TransactionID:"+bean.getTransactionID());
			log.error("SellerID:"+bean.getSellerID());
			log.error("ItemID:"+bean.getItemID());
			throw e;
//			throw new SystemException(e,"UpTrackingNumberCall:ApiException",log);
		}
		catch (SdkException e)
		{
			throw e; 
//			throw new SystemException(e,"UpTrackingNumberCall:SdkException",log);
		} 
		catch (ConnectException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			 
			 throw new SystemException(e,"UpTrackingNumberCall:Exception",log);
		} 
		
	}
	
	/**
	 * 获取token 如果sellerID不为空就从数据库直接获取token否则再通过接口去查
	 * @param sellerID
	 * @param ItemNumber
	 * @return
	 * @throws Exception
	 */
	private String getSellerToken(String sellerID,String ItemNumber)
		throws Exception 
	{
		  	
			String userID = "";
			if(sellerID.equals("")||sellerID==null)
			{
				userID =  getSellerID(ItemNumber);
			}
			else
			{
				userID =sellerID;
				DBRow row  = floorEbayMgr.getUserToken(userID);//当传递的账号参数无效时根据itemID查询账号ID
		        if(row==null)
		        {
		        	userID =  getSellerID(ItemNumber);
		        }
			}
	        DBRow row  = floorEbayMgr.getUserToken(userID);
	        return row.getString("user_token");
	}
	

	/**
	 * 通过一个固定seller账号的token发送请求item获得sellerID
	 * @param ItemNumber
	 * @return
	 * @throws Exception 
	 * @throws SdkException 
	 * @throws ApiException 
	 * @throws Exception
	 */
	public String getSellerID(String ItemNumber) 
		throws Exception
	{
		try
		{
			ApiContext apiContext = new ApiContext(); 
	        
			ApiCredential cred = apiContext.getApiCredential(); 
			String token = floorEbayMgr.getFirstUserToken().getString("user_token");
	        cred.seteBayToken(token);
	        apiContext.setApiServerUrl(this.url);      //正式环境的时候也需要修改     
	        apiContext.getApiLogging().setLogSOAPMessages(false);//Ebay交易信息是否输出
	        apiContext.setSite(SiteCodeType.UK);
	          
	        GetItemCall call = new GetItemCall(apiContext);
	        call.setItemID(ItemNumber);
	        String[] filter  = new String []{"seller"};//对getItemResponse返回的结果过滤
	        call.setOutputSelector(filter);
	        
	        return call.getItem().getSeller().getUserID();
		}
		catch (ApiException e) 
		{
			//如果itemnumber不对，查询错误，则发挥空白sellid
			if (e.getMessage().indexOf("you are not the seller")>=0)
			{
				return("");
			}
			else
			{
				throw e;
			}
		}
		catch (SdkException e)
		{
			 throw e;
		} 
		catch (Exception e) 
		{
			 throw new SystemException(e,"getSellerID:Exception",log);
		} 
	}

	/**
	 * 上传TRacking Number的Jms接口
	 * @param bean
	 */
	public void UpTrackingNumberCallJMS(CompleteSaleParaBeans bean) 
	{
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new UpTrackingNumberCall(bean));
	}
	
	/**
	 * 向竞拍的买家的发送消息的Jms接口
	 * @param bean
	 */
	public void MessageToBidderCallJMS(MessageToBidderParaBeans bean) 
	{
		 
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new MessageToBidderCall(bean));
		
	}
	
 
 
	
}
