package com.cwc.app.api.qll;

import java.text.DateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.qll.FloorVertualTerminalMgrQLL;
import com.cwc.app.iface.qll.VertualTerminalMgrIFaceQLL;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class VertualTerminalMgrQLL implements VertualTerminalMgrIFaceQLL{

	private FloorVertualTerminalMgrQLL floorVertualTerminalMgrQLL;
	static Logger log = Logger.getLogger("ACTION");
	
	/**
	 * 获取paymentstatus结果
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getPaymentStatus(long oid)
		throws Exception
	{
		try
		{
			 return  floorVertualTerminalMgrQLL.getPaymentStatus(oid);
		}catch(Exception e)
		{
			throw new SystemException(e," getPaymentStatus(long oid)  ",log);
		}
	}
	/**
	 * 添加信用卡收款的记录
	 * @param request
	 * @throws Exception
	 */
	public void addVertualTerminal(HttpServletRequest request) 
		throws Exception 
	{
		try
		{
			Date date = new Date();
			DateFormat d1 = DateFormat.getDateTimeInstance();
		    String str1 = d1.format(date);
		    
			DBRow row = new DBRow();
			row.add("result_reasion", StringUtil.getString(request, "reasionQ").trim());
			row.add("result", StringUtil.getString(request, "resultQ").trim());
			row.add("user",StringUtil.getString(request, "userNameQ").trim());
			row.add("deal_time", str1);
			row.add("amount", StringUtil.getDouble(request, "amountQ"));
			row.add("item_amt",0.00 );
			row.add("tax_amt",0.00); 
			row.add("shipping_amt",0.00 );
			row.add("currency_code", StringUtil.getString(request, "currencyCodeQ").trim());
			String acct = StringUtil.getString(request, "acctQ").trim();
			acct="XXXXXXXX"+acct.substring(acct.length()-4, acct.length());
			row.add("acct", acct);
			row.add("expdate",StringUtil.getString(request, "expdateQ").trim() );
			row.add("cardType", StringUtil.getString(request, "cardTypeQ").trim());
			row.add("cvv2","-" );
			row.add("first_name",StringUtil.getString(request, "firstNameQ").trim() );
			row.add("last_name",StringUtil.getString(request, "lastNameQ").trim() );
			row.add("country_code", StringUtil.getString(request, "countryCodeQ").trim());
			row.add("state", StringUtil.getString(request, "stateQ").trim());
			row.add("city",StringUtil.getString(request, "cityQ").trim() );
			row.add("street",StringUtil.getString(request, "streetQ").trim() );
			row.add("zip", StringUtil.getString(request, "zipQ").trim());
			
			floorVertualTerminalMgrQLL.addVertualTerminal(row);
		}
		catch(Exception e )
		{
			throw new SystemException(e," addVertualTerminal(HttpServletRequest request)  ",log);
		}
	}

	/**
	 * 查询所有信用卡收款信息
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllVertualTerminal(PageCtrl pc) 
		throws Exception 
	{
		 try 
			{
			   return  floorVertualTerminalMgrQLL.getAllVertualTerminal(pc);
			} 
			catch (Exception e) 
			{
				throw new SystemException(e,"getAllVertualTerminal",log);
			}
	}
	
	public void setFloorVertualTerminalMgrQLL(
			FloorVertualTerminalMgrQLL floorVertualTerminalMgrQLL) {
		this.floorVertualTerminalMgrQLL = floorVertualTerminalMgrQLL;
	}
	@Override
	public DBRow[] getPaymentVertualTerminal(PageCtrl pc, String txnId,
			String state) throws Exception {
		try{	
			return floorVertualTerminalMgrQLL.getPayment(pc, txnId, state);
		}catch (Exception e) {
			throw new SystemException(e,"getPaymentVertualTerminal",log);
		}
	
	}

}
