package com.cwc.app.api.qll;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.qll.FloorTranInfoMgrQLL;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.iface.qll.TranInfoMgrIFaceQLL;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.jms.action.AddTranInfoActionJMS;
import com.cwc.app.key.PaymentMethodKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.web.WebRequest;
 
public class TranInfoMgrQLL implements TranInfoMgrIFaceQLL
{
	private FloorTranInfoMgrQLL floorTranInfoMgrQLL;
	static Logger log = Logger.getLogger("ACTION");
	private FloorOrderMgr floorOrderMgr;
	private FloorOrderMgrZr floorOrderMgrZr;
	
	private OrderMgrIfaceZR orderMgrZr;
	 
	public void setOrderMgrZr(OrderMgrIfaceZR orderMgrZr) {
		this.orderMgrZr = orderMgrZr;
	}

	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) 
	{
		this.floorOrderMgr = floorOrderMgr;
	}
	
	/**
	 * 检测信用卡付款三个地址
	 * @param txn_id
	 * @return
	 * @throws Exception
	 */
	public DBRow checkAddress(HttpServletRequest request) throws Exception
	{
		DBRow row = new DBRow();
		try
		{
			row = floorTranInfoMgrQLL.getTranInfo(StringUtil.getString(request,"txn_id"));
			if(row!=null)
			{
					String customerAddr = row.getString("cutomer_country").trim()+" "+row.getString("cutomer_city").trim()+" "+row.getString("cutomer_postcode").trim();
					String billingAddr = row.getString("billing_country").trim()+" "+row.getString("billing_city").trim()+" "+row.getString("billing_postcode").trim();
					String deliveryAddr = row.getString("delivery_country").trim()+" "+row.getString("delivery_city").trim()+" "+row.getString("delivery_postcode").trim();
					 
					if(billingAddr.equals("  ") && deliveryAddr.equals("  "))
					{
						row.add("rs", "different");
						row.add("note", "需要比较的国家、城市、邮编信息均为空！");
					}else 
					if(!deliveryAddr.toLowerCase().equals(billingAddr.toLowerCase()))	
					{
						row.add("customer", customerAddr+" "+row.getString("cutomer_street_address"));
						row.add("billing", billingAddr+" "+row.getString("billing_street_address"));
						row.add("delivery", deliveryAddr+" "+row.getString("delivery_street_address"));
						row.add("rs", "dif");
					}else
					{
						row.add("rs", "same");
					}
				}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTranInfo(String tranID)",log);
		}
		return row;
	}
	/**
	 * jMS添加商品交易信息
	 * @param tranID
	 */
	public void addTranInfoJMS(long oid)
	{
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new AddTranInfoActionJMS(oid));
	}
	
	/**
	 * 添加商品交易信息
	 * @param tranID
	 * @throws Exception
	 * 添加CIO by zhangRui
	 * 首先查询出这个订单关联的交易。。。这个时候只有一个交易和他关联 否则的话应该要报错
	 */
	public void  addTranInfo(long oid) 
		throws Exception
	{
		 
		
		try 
		{
			if (oid>0)
			{
				synchronized(String.valueOf(oid))
				{

					DBRow row = floorOrderMgr.getDetailPOrderByOid(oid);
					String txn_id =row.getString("txn_id");
					String url = row.getString("vvme_webservice_url");
			 
					String orderSource = row.getString("order_source");
					if(orderSource.equals(OrderMgr.ORDER_SOURCE_WEBSITE)) 
					{
						if(getTranInfo(txn_id)==null)
						{
							DBRow dbrow = new DBRow();
							dbrow.add("tran_id", txn_id);
							WebRequest wr = new WebRequest();
							 wr.setServer(url+"?txn_id="+txn_id);
							//wr.setServer(url+"?txn_id="+"0YD524029S042910N");
							 
							wr.commitPOST();
							
							int status = wr.getServerStatus();
							String sTotalString = wr.getServerResponse();
					 
							String rs = "";
							if(status==200)
							{
								//进一步检查数据
								if(sTotalString.contains("payment_type") && sTotalString.contains("currency") && !sTotalString.contains("error"))
								{
									String responseFirst =  sTotalString.substring(sTotalString.indexOf("payment_type"), sTotalString.indexOf("product_1_name"));
							        String responseSecond = sTotalString.substring(sTotalString.indexOf("product_1_name"), sTotalString.indexOf("</body>"));
							        dbrow.add("product", responseSecond); 
							        String tranInfo[] = responseFirst.split("<br/>");
							        String tranInfoParam[] = new String[]{"payment_type","currency","ot_subtotal","ot_shipping","ot_total","ip_address","cutomer_name","cutomer_company","cutomer_street_address","cutomer_suburb","cutomer_city","cutomer_postcode","cutomer_state","cutomer_country","cutomer_telephone","delivery_name","delivery_company","delivery_street_address","delivery_suburb","delivery_city","delivery_postcode","delivery_state","delivery_country","billing_name","billing_company","billing_street_address","billing_suburb","billing_city","billing_postcode","billing_state","billing_country","invoice_id","date_purchased","payment_method","cc_type","cc_owner","cc_number","cc_cvv","cc_expires"};
							        String tranInfoName[] =new String[tranInfoParam.length];
							        int tempStart;
							        String cutomer_telephone = "";
							        for (int i = 0; i < tranInfoParam.length; i++) 
							        {
							        	tranInfo[i]=StringUtil.replaceString(tranInfo[i], "\n", "");
							        	tranInfoName[i] = tranInfo[i].substring(0, tranInfo[i].indexOf(" ="));
							        	tempStart = tranInfo[i].indexOf("=")+2;
							        	tranInfo[i] = tranInfo[i].substring(tempStart, tranInfo[i].length());
							        	if(tranInfoName[i].equals("cutomer_telephone"))
							        	{
							        		cutomer_telephone=tranInfo[i];
							        	}
							        }
							        for (int i = 0; i < tranInfoParam.length; i++) 
							        {
										for (int j = i; j < tranInfoName.length; j++) 
										{
											if(tranInfoParam[i].equals(tranInfoName[j]))
							        		{
												 if( tranInfoName[j].equals("ot_subtotal") ||tranInfoName[j].equals("ot_shipping") ||tranInfoName[j].equals("ot_total") )//根据名称改 动态的获取
										        	{
										        		 int index=0 ;
										        		if(!tranInfo[j].equals(""))
										        		{
										        			String strTemp;
										        			for (int t = 0; t < tranInfo[j].length(); t++)//从整个参数中去掉货币符号部分保留数字
										        			{
										        				strTemp = tranInfo[j].substring(t,t+1);
										        				if(ifNum(strTemp))
										        				{
										        					index=t;
										        					break;
										        				}
										        			 }
											        		dbrow.add(tranInfoName[j], conversionFormat(tranInfo[j].substring(index, tranInfo[j].length())));//数据格式的转换
											        	  }else
											        	  {
											        		dbrow.add(tranInfoName[j], null);
											        	  }	
										        	 }else
										        	 {
										        		dbrow.add(tranInfoName[j], tranInfo[j]);
										        	 }
							        			break;
							        		}  
										}
									} 
							        
							        String[] items =  responseSecond.split("\n");
							        floorTranInfoMgrQLL.addTranInfo(dbrow);
							        DBRow rowTemp = new DBRow();
							        rowTemp.add("tel", cutomer_telephone);
							       
							        int start = responseSecond.indexOf("customer_commen");
							        String value = returnEquals( responseSecond.substring(start),false) ;
							        ////system.out.println(value);
							        rowTemp.add("note", value);
							        floorOrderMgr.modPOrder(oid, rowTemp);
							        //读取customer_comment字段 插入到Trade的note 字段
							        String customer_comment = responseSecond.substring(start+18);
							        
							        
							        
							       //更新Trade 的一些数据和插入CIO
							        
							       DBRow tradeRow =  floorOrderMgrZr.getTradeBillByTxnIdAndCreateOrder(txn_id);
							       if(tradeRow == null){
							    	   throw new RuntimeException("Trade Not Null");
							       }
							       ////system.out.println(customer_comment);
							       tradeRow.add("note", customer_comment);
							       tradeRow.add("cc_type", dbrow.getString("cc_type"));
							       // 如果是cc_type 不为null 那么就是Credit Card .其他情况不改变
							       if(dbrow.getString("cc_type").length() > 0 ){
							    	   tradeRow.add("payment_channel", PaymentMethodKey.CC);
							    	   StringBuffer payerEmail = new StringBuffer();
								       payerEmail.append(dbrow.getString("cc_type")).append("|").append(dbrow.getString("cc_owner")).append("|")
								       .append(dbrow.getString("cc_number")).append("|").append(dbrow.getString("cc_expires"));
								       tradeRow.add("payer_email", payerEmail.toString());
							       }
							       tradeRow.add("payment_method",dbrow.getString("payment_method"));
							       tradeRow.add("cc_owner", dbrow.getString("cc_owner"));
							       tradeRow.add("cc_number", dbrow.getString("cc_number"));
							       tradeRow.add("cc_cvv", dbrow.getString("cc_cvv"));
							       tradeRow.add("cc_expires", dbrow.getString("cc_expires"));
							       tradeRow.add("note", customer_comment);
							       tradeRow.add("ip_address", dbrow.getString("ip_address"));
							       tradeRow.add("mc_currency", dbrow.getString("currency"));
							      // tradeRow.add("mc_gross", dbrow.get("ot_subtotal",0.0d));
							       tradeRow.add("mc_shipping", dbrow.get("ot_shipping",0.0d));
							       //delivery
							       tradeRow.add("delivery_address_name", dbrow.getString("delivery_name"));
							       tradeRow.add("delivery_address_street", dbrow.getString("delivery_street_address"));
							       tradeRow.add("delivery_address_city", dbrow.getString("delivery_city"));
							       tradeRow.add("delivery_address_state", dbrow.getString("delivery_state"));
							       tradeRow.add("delivery_address_country", dbrow.getString("delivery_country"));     
							       tradeRow.add("delivery_address_zip", dbrow.getString("delivery_postcode"));
							       this.setCcidAndProId(tradeRow,"delivery_");
							       
							       //billing
							       tradeRow.add("billing_address_name", dbrow.getString("billing_name"));
							       tradeRow.add("billing_address_street", dbrow.getString("billing_street_address"));
							       tradeRow.add("billing_address_city", dbrow.getString("billing_city"));
							       tradeRow.add("billing_address_state", dbrow.getString("billing_state"));
							       tradeRow.add("billing_address_country", dbrow.getString("billing_country"));     
							       tradeRow.add("billing_address_zip", dbrow.getString("billing_postcode"));
							       this.setCcidAndProId(tradeRow,"billing_");
							       long tradeId = tradeRow.get("trade_id", 0l);
							       tradeRow.remove("trade_id");
							       floorOrderMgrZr.updateTradeByTradeId(tradeRow,tradeId);
							       
							      // 插入CIO  插入trade_item
							      // 先循环出有多少个Item
							      int count =0 ;
							      while(responseSecond.indexOf("product_"+(count+1)+"_name") != -1){ 
							    	  count++;
							      }
							     
							      if((count * 5 + 1) != items.length){
							    	  throw new RuntimeException("WebSite Item Error");
							      }
							      for(int index = 0 ; index < count ; index++){
							    	  //product_1_name
							    	  DBRow tradeItem = new DBRow();
							    	  tradeItem.add("txn_id", txn_id);
							    	  tradeItem.add("oid", oid);
							    	  tradeItem.add("trade_id", tradeId);
							    	  
							    	  //item_name,quantity,mc_gross
							    	  int j = (index+1) * 5;
							    	  StringBuffer itemName =  new StringBuffer();
							    	  for(int attr = index*5 ; attr < j ; attr++){
							    		 if(items[attr].contains("qty")){
							    			 double quantity = Double.parseDouble(returnEquals(items[attr],false));
							    			 tradeItem.add("quantity", quantity);
							    		 }else if(items[attr].contains("unit_price")){
							    			 tradeItem.add("mc_gross", Double.parseDouble(""+conversionFormat(returnEquals(items[attr],true)))); 
							    		 }else{
							    			 itemName.append(returnEquals(items[attr],false)).append("<br/>");
							    		 }
							    	  }
							    	  tradeItem.add("item_name", itemName.toString());
							    	  floorOrderMgrZr.insertTradeItem(tradeItem);
			 
							      }
								}
							}
							else
							{
								rs =status+": 请求WEBSITE服务器错误  ";
								throw new Exception(rs);
							}
						}
					}
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addTranInfo("+oid+")",log);
		}
	
	}
	
	private void setCcidAndProId(DBRow tradeRow, String type)throws Exception {
		try{
		boolean isBilling = type.indexOf("billing") != -1 ?true:false;
		String fix = isBilling?"billing_":"";
		
		DBRow result = orderMgrZr.setCcidAndProIdByStringValue(tradeRow.getString(type+"address_country"), tradeRow.getString(type+"address_zip"), tradeRow.getString(type+"address_state"),"");

		if(result != null){
			tradeRow.add(type+"ccid_id", result.get("ccid_id", 0));
			tradeRow.add(type+"pro_id", result.get("pro_id", -1));
			if(result.getString("address_zip").length() > 0){
				tradeRow.add(type+"address_zip", result.getString("address_zip"));
			}
			if(result.getString("address_state").length() > 0){
				tradeRow.add(type+"address_state", result.getString("address_state"));
			}
			
		}
			
		}catch (Exception e) {
			throw new SystemException(e,"setCcidAndProId("+tradeRow+")",log);
		}
	}
	// 获取 = 后面的值 如果有多个 = 号 那么返回的值 用|分割
	private   String  returnEquals(String str , boolean isDouble){
		String[] array = str.split("<br/>");
		 
		if(array.length < 1){
			return " ";
		}else{
			StringBuffer sb = new StringBuffer();
			for(int index  = 0 , count = array.length ; index < count ; index++ ){
				boolean isHasEquals = array[index].indexOf("=") != -1 ?true:false;
				if(isHasEquals){
					String[] keyValue = array[index].split("=");
					if(!isDouble){
						sb.append("|"+keyValue[1].trim());
					}else{
	 
						sb.append(keyValue[1].trim().replaceAll("[^0-9.]",""));
					}
				}
			}
			String value = sb.toString();
			if(!isDouble){
				return value.length()> 1 ? value.substring(1):"";
			}else{
				return value;
			}
		}
	}
	/**
	 * 传入的是金额部分当带有, 进行转换
	 * @param str
	 * @return
	 * @throws ParseException
	 */
	private double  conversionFormat(String str) 
		throws ParseException
	{
		 Number num=0;
		 if(str.contains(","))
		 {
			 DecimalFormatSymbols dfs = new DecimalFormatSymbols();  
		     dfs.setDecimalSeparator('.');  
		     dfs.setGroupingSeparator(',');  
		     dfs.setMonetaryDecimalSeparator('.');  
		     DecimalFormat df =null;
			 try
			 {
				 df = new DecimalFormat("###,###.##",dfs);
			 }
			 catch (Exception e)
			 {
				e.printStackTrace();
			 }  
			 num = df.parse(str);  	
		 }else
		 {
			 num = Double.parseDouble(str);
		 }
		
        return num.doubleValue();
	}
	 
	/**
	 * 判断是不是数字的方法
	 * @param str
	 * @return
	 */
	private boolean ifNum(String str)
	{
		boolean ifNum = (str).matches("[0-9]*") ; 
		return ifNum ;
	}
	 
	 /**
	  * 通过tranID 获得具体一条的商品交易信息
	  * @param tranID
	  * @return
	  * @throws Exception
	  */
	public DBRow getTranInfo(String tranID)
		throws Exception 
	{
		try 
			{
				DBRow row = floorTranInfoMgrQLL.getTranInfo(tranID);
				if(row!=null)
				{
					row.add("customer_comment", row.getString("product").substring(row.getString("product").indexOf("customer_comment")+18, row.getString("product").length()));
				}
				return row;	 
			} 
			catch (Exception e) 
			{
				throw new SystemException(e,"getTranInfo(String tranID)",log);
			}
	}
	
	/**
	 * 拆分product信息以表格显示
	 * @param tranID
	 * @return
	 * @throws Exception
	 */
	public String[] getProductInfo(String tranID) throws Exception
	{
		String result[]=null;
		try
		{
			DBRow rowPro = floorTranInfoMgrQLL.getTranInfo(tranID); 
			if(rowPro!=null)
			{
				String rs = rowPro.getString("product");
				if(rs!="" && !rs.equals(""))
				{
					String[] para = new String[]{"name","qty","attr","model","price"};
			        String tranInfo[] = rs.split("<br/>");
			        String tranInfoName[] =new String[tranInfo.length];
			        int tempStart;
			        String []tranInfoValue = new String [tranInfo.length*5];
			        //拆分数据
			        for (int i = 0; i < tranInfo.length; i++) 
			        {
			        	tranInfo[i]=tranInfo[i].replace("\n", "");
			        	tranInfoName[i] = tranInfo[i].substring(0, tranInfo[i].indexOf(" ="));
			        	tempStart = tranInfo[i].indexOf("=")+2;
			        	if(tempStart<tranInfo[i].length())
			        	{
			        		tranInfo[i] = tranInfo[i].substring(tempStart, tranInfo[i].length());
			        	}else
			        	{
			        		tranInfo[i]="";
			        	}
			        }   
			        //拆分数据
			        int t = 0 ;
			        int num = 0;
			        for (int i = 0; i < tranInfo.length; i++) 
			        {
			        		if(tranInfoName[i].contains(para[t]) )//如果当前name含有para 且下一name含有para  赋值t不变
			        		{
			        			if(tranInfoValue[num]!=null )
			        			{
			        				tranInfoValue[num]+=tranInfo[i];
			        			}else
			        			{
			        				tranInfoValue[num]=tranInfo[i];
			        			}
			        			if(i>=tranInfo.length-1 || !tranInfoName[i+1].contains(para[t]))//if(i<tranInfo.length-1 && tranInfoName[i+1].contains(para[t]))
		 	        			{
		 	        				t++; num++;
		 	        			}	 
			        		} else  
			        		////////////////此处修改原因 ：当name中缺少多个属性时 ；t每次增加之后判断是否到达最大值 5 如果到达 返回0
			        		if(!tranInfoName[i].contains(para[t]) && t<para.length && num<tranInfo.length)
			        			{
			        				if((t+1)<para.length && tranInfoName[i].contains(para[t+1]))
			        				{
			        					tranInfoValue[num]="";//如果name中少一个属性
					        			num++; t++;//赋值增加
					        			tranInfoValue[num]=tranInfo[i];
					        			if(i>=tranInfo.length-1 || !tranInfoName[i+1].contains(para[t]))
				 	        			{
					        				t++;  num++;
				 	        			}  
			        				}else if( (t+2)<para.length && tranInfoName[i].contains(para[t+2]))
			        				{
			        					tranInfoValue[num]=""; 
			        					num++; t++;  
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=tranInfo[i];
					        			if(i>=tranInfo.length-1 || !tranInfoName[i+1].contains(para[t]))
				 	        			{
					        				t++;  num++;
				 	        			} 
			        				}else if( (t+3)<para.length && tranInfoName[i].contains(para[t+3]))
			        				{
			        					tranInfoValue[num]=""; 
			        					num++; t++; 
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=tranInfo[i];
					        			if(i>=tranInfo.length-1 || !tranInfoName[i+1].contains(para[t]))
				 	        			{
					        				t++;  num++;
				 	        			} 
			        				}else if((t+4)<para.length && tranInfoName[i].contains(para[t+4]))
			        				{
			        					tranInfoValue[num]=""; 
			        					num++; t++;  
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=""; 
					        			num++; t++;  
					        			tranInfoValue[num]=tranInfo[i];
					        			if(i>=tranInfo.length-1 || !tranInfoName[i+1].contains(para[t]))
				 	        			{
					        				t++;  num++;
				 	        			} 
			        				}
			        			}
			        		if(t==para.length||t>para.length)
				        	{
				        		t=0;
				        	}
			        }
			        int len=0 ;
				 	 
				   	for(int n=0;n<tranInfoValue.length-5;n++)
				   	{
				   		if( tranInfoValue[n+1]==null && tranInfoValue[n+2]==null && tranInfoValue[n+3]==null && tranInfoValue[n+4]==null)
				   		{
				   			len = n ;//如果正好是5的倍数
				   			break;
				   		}
				   	}
				   	int number ;
				   	 if(len%5!=0)
				   	 {
				   		 number = (len/5)*5+5;
				   	 }else
				   	 {
				   		number = (len/5)*5;
				   	 }
				    result = new String[number];
				   	for(int l=0;l<result.length;l++)
				   	{
				   		result[l]=tranInfoValue[l];
				   	}
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductInfo(String tranID)",log);
		}
		return result;
	}
	
	/**
	 * 判断是否需要信用卡支付(返回IP为信用卡支付)	
	 * @param tranInfoMgrQLL
	 * @param txn_id
	 * @param source
	 * @param note
	 * @return
	 * @throws Exception
	 */
	public String getBuyerIp(String txn_id,String source,String note)
		throws Exception 
	{
		////system.out.println("getBuyerIp");
		if ( source.toLowerCase().equals(OrderMgr.ORDER_SOURCE_WEBSITE.toLowerCase())&&note.indexOf("|")>=0 )
		{
			String ip = note.split("\\|")[1];
			if ( ip.indexOf(":")>0 )
			{
				ip = ip.split(":")[0];
			}
			ip = ip.trim();
			
			if (ip.split("\\.").length==4)
			{
				return(ip.trim());
			}
			else
			{
				return("");
			}
		}
		
		
		if ( source.toLowerCase().equals(OrderMgr.ORDER_SOURCE_WEBSITE.toLowerCase())&&txn_id.equals("")==false )
		{
			//如果是ipAddress 的值为空的话那么在去tran_info中去查询。现在是为了兼容以前的
			//使用新方法校验一次
			 
				DBRow tranInfo = this.getTranInfo(txn_id);
				if (tranInfo!=null){
						if (tranInfo.getString("payment_type").equals("paypaldp")){
							//获取IP地址
							String ipa[] = tranInfo.getString("ip_address").split("-");
							if (ipa.length>1){
								return(ipa[0]);//信用卡支付
							} else{
								return(tranInfo.getString("ip_address"));//信用卡支付
							}
						} 
					}  
				}
	 
		else if (source.toLowerCase().equals(OrderMgr.ORDER_SOURCE_DIRECTPAY.toLowerCase())&&txn_id.equals("")==false )//升级前在测试
		{
			DBRow orderInfo = floorOrderMgr.getDetailPOrderByTxnid(txn_id);
			if (orderInfo!=null && orderInfo.getString("custom").indexOf("payment_type=paypaldp")>=0)
			{
				 return "192.168.2.1";//为了保持其他信用卡付款的ip地址校验功能的完整性，当不需要验证ip的情况下返回一个常量
			}    
		}
		 
		return("");
	}
	
	
	

	public void setFloorTranInfoMgrQLL(FloorTranInfoMgrQLL floorTranInfoMgrQLL) {
		this.floorTranInfoMgrQLL = floorTranInfoMgrQLL;
	}
	
	 
}
