package com.cwc.app.api.qll;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.email.SameEmailTemplateException;
import com.cwc.app.floor.api.qll.FloorEmailTemplateMgr;
import com.cwc.app.iface.qll.EmailTemplateIFace;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class EmailTemplateMgr implements EmailTemplateIFace {

	
	static Logger log = Logger.getLogger("ACTION");
	private FloorEmailTemplateMgr floorEmailTemplateMgr;
 
	/**
	 * 增加邮件模板
	 * @param request
	 * @throws Exception
	 */
	public void addTemplate(HttpServletRequest request) 
		throws SameEmailTemplateException,Exception 
	{
		try
		{
			String template_name = StringUtil.getString(request, "template_name");
			String smpt_server = StringUtil.getString(request, "smpt_server");
			String smpt_port = StringUtil.getString(request, "smpt_port");
			String send_id = StringUtil.getString(request, "send_id");
			String send_pwd = StringUtil.getString(request, "send_pwd");
			String send_name = StringUtil.getString(request, "send_name");
			String reply_id = StringUtil.getString(request, "reply_id");
			long scene_id = StringUtil.getLong(request, "scene_id");
			String receiver = StringUtil.getString(request, "receiver");
			
			DBRow row = new DBRow();
			row.add("template_name", template_name);
			row.add("smpt_server", smpt_server);
			row.add("smpt_port", smpt_port);
			row.add("send_id", send_id);
			row.add("send_pwd", send_pwd);
			row.add("send_name", send_name);
			row.add("reply_id", reply_id);
			row.add("scene_id", scene_id);
			row.add("receiver", receiver);

			if (floorEmailTemplateMgr.getDetailTemplateByNameBusines(template_name, receiver)!=null)
			{
				throw new SameEmailTemplateException();
			}
			
			floorEmailTemplateMgr.addTemplate(row);
		}
		catch(SameEmailTemplateException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addTemplate(HttpServletRequest request) error:",log);
		}
		
		
	}
	
	/**
	 * 删除邮件模板
	 * @param request
	 * @throws Exception
	 */
	public void delTemplate(HttpServletRequest request)
		throws Exception 
	{
		 
		try
		{
			long id = StringUtil.getLong(request, "template_id");
			floorEmailTemplateMgr.delTemplate(id);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"delTemplate(HttpServletRequest request) error:",log);
		}
		
		
		
	}

	/**
	 * 删除邮件模板
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTemplate(PageCtrl pc)
		throws Exception
	{
		try 
		{
			return (floorEmailTemplateMgr.getAllTemplate(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllTemplate1(PageCtrl pc) error:",log);
		}
	}

	/**
	 * 获取邮件模板一条详细数据
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTemplate(long tid)
		throws Exception 
	{
		 try 
		{
			return (floorEmailTemplateMgr.getDetailTemplate(tid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailTemplate(long tid)",log);
		}
	}
	
	public DBRow getDetailTemplateByName(String name)
		throws Exception 
	{
		 try 
		{
			return (floorEmailTemplateMgr.getDetailTemplateByName(name));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailTemplateByName(long tid)",log);
		}
	}

	/**
	 * 通过模板名称和收款帐号，获得模板详细信息
	 * @param name
	 * @param busines
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTemplateByNameBusines(String name,String busines) 
		throws Exception 
	{
		 try 
		{
			return (floorEmailTemplateMgr.getDetailTemplateByNameBusines(name,busines));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailTemplateByNameBusines(long tid)",log);
		}
	}
	
	/**
	 * 通过ID获得邮件模板
	 * @param id
	 * @param receiver
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTemplateById(long id,String receiver, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			return floorEmailTemplateMgr.getTemplateById(id, receiver, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getTemplateById(categoryId,pc)",log);
		}
	}

	/**
	 * 修改邮件模板
	 * @param request
	 * @throws Exception
	 */
	public void modTemplate(HttpServletRequest request)
		throws SameEmailTemplateException,Exception 
	{
		try
		{
			long id = Long.parseLong(StringUtil.getString(request, "template_id"));
			String template_name = StringUtil.getString(request, "template_name");
			String smpt_server = StringUtil.getString(request, "smpt_server");
			String smpt_port = StringUtil.getString(request, "smpt_port");
			String send_id = StringUtil.getString(request, "send_id");
			String send_pwd = StringUtil.getString(request, "send_pwd");
			String send_name = StringUtil.getString(request, "send_name");
			String reply_id = StringUtil.getString(request, "reply_id");
			String receiver = StringUtil.getString(request, "receiver");
			
			DBRow old = floorEmailTemplateMgr.getDetailTemplate(id);
			
			DBRow row = new DBRow();
			row.add("template_name", template_name);
			row.add("smpt_server", smpt_server);
			row.add("smpt_port", smpt_port);
			row.add("send_id", send_id);
			row.add("send_pwd", send_pwd);
			row.add("send_name", send_name);
			row.add("reply_id", reply_id);
			row.add("receiver", receiver);
			
			if ( !old.getString("template_name").equals(template_name)||!old.getString("receiver").equals(receiver) )
			{
				if (floorEmailTemplateMgr.getDetailTemplateByNameBusines(template_name, receiver)!=null)
				{
					throw new SameEmailTemplateException();
				}
			}
			
			floorEmailTemplateMgr.modTemplate(id,row);
		}
		catch(SameEmailTemplateException e)
		{
			throw e;
		} 
		catch(Exception e)
		{
			throw new SystemException(e,"modTemplate(HttpServletRequest request) error:",log);
		}
	}
	
	/**
	 * 修改场景
	 * @param request
	 * @throws Exception
	 */
	public void modScene(HttpServletRequest request)
		throws Exception 
	{
		 try
		{
			long template_id = StringUtil.getLong(request, "template_id");
			long scene_id = StringUtil.getLong(request, "scene_id");
			
			DBRow row = new DBRow();
			row.add("template_id", template_id);
			row.add("scene_id", scene_id);
			
			floorEmailTemplateMgr.modTemplate(template_id,row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"modScene(HttpServletRequest request) error:",log);
		}
	}
	
	/**
	 * 修改邮件模板内容
	 * @param request
	 * @throws Exception
	 */
	public void modVelocityTemplate(HttpServletRequest request)
		throws Exception 
	{
		 try
		{
			long tid = StringUtil.getLong(request, "tid");
			String email_template = StringUtil.getString(request, "email_template");
			
			DBRow row = new DBRow();
			row.add("email_template", email_template);
			
			floorEmailTemplateMgr.modTemplate(tid,row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"modVelocityTemplate(HttpServletRequest request) error:",log);
		}
	}
	
	/**
	 * 根据查询字符查询相关的资产信息
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchTemplate(String key, PageCtrl pc) 
		throws Exception 
	{

		try 
		{
			return floorEmailTemplateMgr.getSearchTemplate(key,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSearchTemplate(key,pc)",log);
		}
	}
	
	/**
	 *  获得邮件场景
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEmailScene()
		throws Exception
	{
		
			try 
			{
			 return floorEmailTemplateMgr.getEmailScene();
			} 
			catch (Exception e) 
			{
				throw new SystemException(e,"getEmailScene",log);
			}
	}
 
	/**
	 * 获取邮件模板文件名
	 * @return
	 */
	public String[]  getTemplateFileName()
	{
			
			 File file =  new  File(Environment.getHome()+"/administrator/email/email_template");    
    		 File[] array = file.listFiles();   
    		 int n=0;
    		 int num=0;
    		 for ( int  i= 0 ;i<array.length;i++)
	         {   
		            if (array[i].isFile())
		            {   
		            	  n++;
		            }
		            else  if (array[i].isDirectory())
		            {      
		            		num++;
		               	 File file1 =  new  File(array[i].getPath());    
			       			 File[] list = file1.listFiles();   
				          	 for ( int  j= 0 ;j<list.length;j++)
				            {   
				               if (list[j].isFile())
				               {   
				            	    n++;
				               }
			               	}   
			          	 
			          } 
	          } 
    		 String FileName[]= new String[n];
    		 String folderName[]=new String[num];
    		 int t=0;
    		 int len=0;
    	   for ( int  i= 0 ;i<array.length;i++)
	         {   
		            if (array[i].isFile())
		            {   
		            	FileName[i]=array[i].getName();
		                t++;
		            }
		            else  if (array[i].isDirectory())
		            {      
		            	 folderName[len]=array[i].getName();
		                 len++;
		               	 File file1 =  new  File(array[i].getPath());    
			       			 File[] list = file1.listFiles();   
				          	 for ( int  j= 0 ;j<list.length;j++)
				            {   
				               if (list[j].isFile())
				               {   
				            	    FileName[t]=list[j].getName();
				            	     t++;
				               }
			               	}   
			          	 
			          } 
	          }	        
	        return FileName;
	 }

	/**
	 * 获得邮件模板文件夹
	 * @return
	 * @throws Exception
	 */
	public String[][] getTemplateFolder()
		throws Exception
	{
		 File file =  new  File(Environment.getHome()+"/administrator/email/email_template");    
		 File[] array = file.listFiles();  
		 int num=0;
		 for ( int  i= 0 ;i<array.length;i++)
         {   
	             if (array[i].isDirectory())
	            {      
	            		num++;
	             } 
          } 
		 String folder[][]=new String[num][2];
		 int len=0;
		   for ( int  i= 0 ;i<array.length;i++)
	         {   
		             if (array[i].isDirectory())
		            {      
		            	 folder[len][0]=array[i].getName();
		            	 folder[len][1]=array[i].getPath();
		                 len++;
		               } 
	          }
		return folder;
	}
	
	/**
	 * 获得邮件模板
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTemplateFile(String path)
		throws Exception
	{
		 File file =  new  File(path);    
		 File[] array = file.listFiles();   
		
		 int n=0;
		 for ( int  i= 0 ;i<array.length;i++)
         {   
	            if (array[i].isFile())
	            {   
	            	  n++;
	            }
         }
		 DBRow fileName[]=new DBRow[n];
		 for ( int  i= 0 ;i<array.length;i++)
         {   
	            if (array[i].isFile())
	            {   
	            	fileName[i] = new DBRow();
	            	fileName[i].add("fileName",array[i].getName());
	            	 
	            }
         }
		 return fileName;
	}
	
 
	 
	public void setFloorEmailTemplateMgr(FloorEmailTemplateMgr floorEmailTemplateMgr) {
		this.floorEmailTemplateMgr = floorEmailTemplateMgr;
	}
	 
	
	  
	
	
	 
	   
}
