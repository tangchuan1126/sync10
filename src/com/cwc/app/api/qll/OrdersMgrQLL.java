package com.cwc.app.api.qll;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.qll.FloorOrdersMgrQLL;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.qll.OrdersMgrIFaceQLL;
import com.cwc.app.jms.action.AddressValidateActionJMS;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.fedex.AddressValidationWebServiceClient;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fedex.ship.stub.ClientDetail;
import com.fedex.ship.stub.WebAuthenticationCredential;
import com.fedex.ship.stub.WebAuthenticationDetail;

public class OrdersMgrQLL implements OrdersMgrIFaceQLL{
	 
	private OrderMgrIFace orderMgr;
	static Logger log = Logger.getLogger("ACTION");
	private FloorOrdersMgrQLL floorOrdersMgrQLL;
	 
	public void modAddressValidateStatus(long oid,String validateStatus)
		throws Exception
	{
		try
		{
			DBRow row=new DBRow();
			if(validateStatus.equals(AddressValidationWebServiceClient.INSUFFICIENT))
			{
				row.add("address_validate_status", AddressValidationWebServiceClient.INSUFFICIENT);
//				row.add("handle_status", HandStatusleKey.DOUBT_ADDRESS);//INSUFFICIENT提示验证失败  修改疑问地址状态
			}
			else
			{
				row.add("address_validate_status", validateStatus);
			}
			floorOrdersMgrQLL.modAddressValidateStatus(oid, row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"modAddressValidateStatus(long oid,String addressStatus) ",log);
		}
	}
	
	/**
	 * 验证fedex地址调用方法
	 * @param toAddress
	 * @param toZip
	 */
	public void addressValidateFedexJMS(long oid,String toAddress,String toZip)
	{
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new AddressValidateActionJMS(oid,toAddress,toZip));    
	}
	
	/**
	 * 通过activeMQ验证地址
	 * @param address
	 * @throws Throwable 
	 */
	public void addressValidateFedex(long oid,String toAddress,String toZip)
		throws Exception
	{
			if(toAddress.equals(""))
			{
				toAddress = "NULL";
			}
			if(toZip.equals(""))
			{
				toZip = "NULL";
			}
	        String residentialStatus = "";
	        AddressValidationWebServiceClient addressValidationWebServiceClient = new AddressValidationWebServiceClient();
	        try 
	        {
//				residentialStatus = addressValidationWebServiceClient.getResidentialStatus(toAddress, toZip);
			}
//	        catch(java.net.UnknownHostException e)
//	        {
//	        	//system.out.println("catch");
//	        }
	        catch (Exception e) 
	        {
				throw e;
			}        	
	        String validateStatus="";
	        if (residentialStatus.equals(AddressValidationWebServiceClient.INSUFFICIENT))
	        {
	        	validateStatus=AddressValidationWebServiceClient.INSUFFICIENT.toLowerCase();//INSUFFICIENT提示验证失败  修改疑问地址状态
	        	//同时发现地址问题，提交给客服处理
//	        	request.setAttribute("verify_address_note","自动验证地址失败");
//	        	request.setAttribute("oid", oid);
//	        	orderMgr.printVerifyAddress(request);
	        	
//	        	String note="自动验证地址失败";
//	        	HttpServletRequest request = new HttpServletrequest();
//	        	orderMgr.addPOrderNotePrivate(oid, note, 0, StrUtil.getSession(request), 0);
	        }
	        else
	        {
	        	validateStatus=residentialStatus.toLowerCase();//其他结果只原样提示
	        }
	        this.modAddressValidateStatus(oid,validateStatus);//同步验证结果到状态
		
	}
	
	private ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
         
        if (accountNumber == null) {
        	accountNumber = "128544119"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "102483532"; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "6ukdvDUaV0lc92jB"; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "IWBGFds3SiaEvzDl6Hn3JcZHM"; // Replace "XXX" with clients password
        }
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}
	
	

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setFloorOrdersMgrQLL(FloorOrdersMgrQLL floorOrdersMgrQLL) {
		this.floorOrdersMgrQLL = floorOrdersMgrQLL;
	}

	@Override
	public void examineRefundProcess(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reStartRefundProcess(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startRefundProcess(HttpServletRequest request, String manager)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	 

	 
	
}
