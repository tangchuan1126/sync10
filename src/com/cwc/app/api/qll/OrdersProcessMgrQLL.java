package com.cwc.app.api.qll;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.floor.api.qll.FloorOrdersProcessMgrQLL;
import com.cwc.app.iface.qll.OrdersProcessMgrIFaceQLL;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;

public class OrdersProcessMgrQLL implements OrdersProcessMgrIFaceQLL{

	private FloorOrdersProcessMgrQLL floorOrdersProcessMgrQLL;
	static Logger log = Logger.getLogger("ACTION");
	
	public String exportPlanAndStockNew(HttpServletRequest request)
		throws Exception
	{
		try 
		{
		 	long storage_id = StringUtil.getLong(request,"storage_id");
			String startDate = StringUtil.getString(request,"startDate");
			String endDate = StringUtil.getString(request,"endDate");
			String pid = StringUtil.getString(request,"search_product_nameQLL");
			long pcid = StringUtil.getLong(request,"category_id");
			String pro_line = StringUtil.getString(request,"pLineID");
			 
			DBRow rowsValue[]=null;
			DBRow DateRow[]= null;
		 	if(pcid!=0 && pro_line.equals("0") && pid.equals(""))//类别统计
		 	{
		 		DateRow= getDate(startDate,endDate);
		 		rowsValue = buildPlanDateExport(storage_id,pcid,startDate,endDate);
		 	}
		 	else
		 	if(pcid==0 && !pro_line.equals("0") && !pro_line.equals("") && pid.equals(""))//产品线统计
		 	{
		 		DateRow= getDate(startDate,endDate);
		 		rowsValue = buildPlanLineDateExport(storage_id,pro_line ,startDate,endDate);
		 	}
		 	else
		 	if(pcid==0 && pro_line.equals("0") && !pid.equals(""))//商品名称统计
		 	{
		 		DateRow= getDate(startDate,endDate);
		 		rowsValue = buildPidDateExport(pid,startDate,endDate,storage_id); 
		 	}   	
			if (rowsValue.length > 0) 
			{
				XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/order_process/ExportPlanAndStock.xlsx"));  
				XSSFSheet sheet= wb.getSheet("Sheet1");  
				
				XSSFCellStyle style = wb.createCellStyle();
				style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				style.setLocked(false); //创建样式
				style.setWrapText(true);
				
				XSSFFont  font =wb.createFont();
				font.setFontName("Arial");   
				font.setFontHeightInPoints((short)10);   
				font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
				
				XSSFCellStyle styleTitle = wb.createCellStyle();
				styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				styleTitle.setLocked(false);  
				styleTitle.setWrapText(true);
				styleTitle.setFont(font);
				
				XSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); 
				stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				//title赋值
				XSSFRow row = sheet.createRow(0);  
				row.createCell(0).setCellValue("产品名称");
				row.getCell(0).setCellStyle(styleTitle);
				for (int i = 0; i < DateRow.length; i++)
				{
						if(i==0)
						{
							row.createCell(1).setCellValue("当前库存");
							row.getCell(1).setCellStyle(styleTitle);
							row.createCell(2).setCellValue("总计");
							row.getCell(2).setCellStyle(styleTitle);
							row.createCell(3).setCellValue("平均值");
							row.getCell(3).setCellStyle(styleTitle);
						}
						else
						{
							row.createCell(i+3).setCellValue(DateRow[i].getString("date"));
							row.getCell(i+3).setCellStyle(styleTitle);	
						}
				}
				//value赋值
				for (int i = 0; i < rowsValue.length; i++)
				{
					row = sheet.createRow(i+1); 
					DBRow rowTemp = rowsValue[i];
					row.createCell(0).setCellValue(rowTemp.getString("product_title"));
					row.getCell(0).setCellStyle(styleTitle);
					double tempAmount=0.0;
					Pattern p1 = Pattern.compile("(-|[0-9])?[0-9]*(\\.?)[0-9]+"); // 非數字  
					for (int j = 0; j < DateRow.length; j++)
					{
						if(j==0)
						{
							row.createCell(1).setCellValue(rowTemp.getString("store_count"));
							row.getCell(1).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
							row.getCell(1).setCellStyle(style);
						}
						else
						{
							Matcher m = p1.matcher(rowTemp.getString("date_"+j));  
							if(m.matches())
							{
								tempAmount+=StringUtil.getDouble(rowTemp.getString("date_"+j));
							}
							if(j==DateRow.length-1)
							{
								row.createCell(2).setCellValue(tempAmount);
								row.getCell(2).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
								row.getCell(2).setCellStyle(style);	
								
								row.createCell(3).setCellValue((Math.round(tempAmount/DateRow.length*10))/10.0);
								row.getCell(3).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
								row.getCell(3).setCellStyle(style);
							}
								row.createCell(j+3).setCellValue(rowTemp.getString("date_"+j));
								row.getCell(j+3).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
								row.getCell(j+3).setCellStyle(style);	
						}
					}
				}
				
				//写文件  
				String path = "upl_excel_tmp/export_PlanAndStock_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
				OutputStream os= new FileOutputStream(Environment.getHome()+path);
				wb.write(os);  
				os.close();  
				return (path);
			}
			else
			{
				return "0";
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e," exportPlanAndStock(HttpServletRequest request) ",log);
		} 
	}
	 
	/**
	 * 生成通过产品条码统计的数据
	 * @param pidStr
	 * @param startDate
	 * @param endDate
	 * @param storageID
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPidDateExport(String pidStr,String startDate,String endDate,long storageID)
		throws Exception
	{
		DBRow[] row;
		long pid;
		try
		{
			if(floorOrdersProcessMgrQLL.getPid(pidStr)!=null );
			{
				pid = Long.parseLong(floorOrdersProcessMgrQLL.getPid(pidStr).getString("pc_id"));
				row = floorOrdersProcessMgrQLL.buildPidPlanAndStock(pid, storageID, startDate, endDate); 
			}
			return row;
		}
		catch(Exception e)
		{
			throw new SystemException(e," buildPidDateExport(String pidStr,String startDate,String endDate,long storageID)",log);
		}
	}
	/**
	 * 生成通过产品条码统计的数据
	 * @param pidStr
	 * @param startDate
	 * @param endDate
	 * @param storageID
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPidDate(String pidStr,String startDate,String endDate,long storageID,PageCtrl pc)
		throws Exception
	{
		DBRow[] row = null ;
		DBRow rowTemp = null;
		long pid;
		try
		{
			rowTemp = floorOrdersProcessMgrQLL.getPid(pidStr);//由名称转换为ID
			if( rowTemp != null )
			{
				pid = rowTemp.get("pc_id", 0l);//
				if( pid !=0)
				{
					row = floorOrdersProcessMgrQLL.buildPidPlanAndStock(pid, storageID, startDate, endDate); 
				}
			}  
			return row;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"buildPidDate(long pid,String startDate,String endDate,long storageID,PageCtrl pc)",log);
		}
	}
	
	/**
	 * 
	 * @param storage_id 仓库ID
	 * @param catalog_id 产品类别ID
	 * @param startDate 起始日期
	 * @param endDate 截止日期
	 * @param pc 分页参数
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPlanLineDateExport(long storage_id ,String catalog_id,String startDate,String endDate)
		throws Exception
	{
		try
		{
			DBRow[] rows = null;
			long catalog;
			if(catalog_id.contains("$")) 
			{
				long lineID = Long.parseLong(catalog_id.substring(0, catalog_id.indexOf("$")));
				DBRow rowIDlist = floorOrdersProcessMgrQLL.getLineStr(lineID);
				String listStr = rowIDlist.getString("idList"); 
				listStr = listStr.substring(listStr.indexOf("$")+2, listStr.length());
				rows = floorOrdersProcessMgrQLL.getPlanAndStockAllExport(listStr,storage_id,startDate,endDate);//in
			}else//类别
			{
				catalog = Long.parseLong(catalog_id);
				DBRow rowIDlist = floorOrdersProcessMgrQLL.getIdStr(catalog);
				String listStr = rowIDlist.getString("idList");   
				rows = floorOrdersProcessMgrQLL.getPlanAndStockAllExport(listStr,storage_id,startDate,endDate); 
			}
			return rows;
		}catch (Exception e)
		{
			throw new SystemException(e,"buildPlanLineDate(long storage_id ,String catalog_id,String startDate,String endDate,PageCtrl pc)",log);
		}
			 
	}
	
	
	/**
	 * 
	 * @param storage_id 仓库ID
	 * @param catalog_id 产品类别ID
	 * @param startDate 起始日期
	 * @param endDate 截止日期
	 * @param pc 分页参数
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPlanLineDate(long storage_id ,String catalog_id,String startDate,String endDate,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow[] rows = null;
			long catalog;
			if(catalog_id.contains("$")) 
			{
				long lineID = Long.parseLong(catalog_id.substring(0, catalog_id.indexOf("$")));
				DBRow rowIDlist = floorOrdersProcessMgrQLL.getLineStr(lineID);
				String listStr = rowIDlist.getString("idList");   
				listStr = listStr.substring(listStr.indexOf("$")+2, listStr.length());
				rows = floorOrdersProcessMgrQLL.buildPlanDateIN(listStr,storage_id,startDate,endDate,pc);//in
			}else//类别
			{
				catalog = Long.parseLong(catalog_id);
				DBRow rowIDlist = floorOrdersProcessMgrQLL.getIdStr(catalog);
				String listStr = rowIDlist.getString("idList");  
				rows = floorOrdersProcessMgrQLL.buildPlanDateIN(listStr,storage_id,startDate,endDate,pc); 
			}
			return rows;
		}catch (Exception e)
		{
			throw new SystemException(e,"buildPlanLineDate(long storage_id ,String catalog_id,String startDate,String endDate,PageCtrl pc)",log);
		}
	}
	/**
	 * 通过类别 日期 仓库 统计计划发货  若需要各子类逐一统计 循环此方法
	 * @param storage_id
	 * @param catalog_id
	 * @param startDate
	 * @param endDate
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPlanDateExport(long storage_id ,long catalog_id,String startDate,String endDate)
		throws Exception 
	{ 
		try
		{
			DBRow rowIDlist = floorOrdersProcessMgrQLL.getIdStr(catalog_id);
			String listStr = rowIDlist.getString("idList");   
			DBRow[] rows = floorOrdersProcessMgrQLL.getPlanAndStockAllExport(listStr,storage_id,startDate,endDate); 
			return  rows;
		}
		catch(Exception e )
		{
			throw new SystemException(e," buildPlanDateExport(long storage_id ,long catalog_id,String startDate,String endDate)",log);
		}
	}
	
	
	/**
	 * 通过类别 日期 仓库 统计计划发货  若需要各子类逐一统计 循环此方法
	 * @param storage_id
	 * @param catalog_id
	 * @param startDate
	 * @param endDate
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] buildPlanDate(long storage_id ,long catalog_id,String startDate,String endDate,PageCtrl pc)
		throws Exception 
	{ 
		try
		{
			DBRow rowIDlist = floorOrdersProcessMgrQLL.getIdStr(catalog_id);
			String listStr = rowIDlist.getString("idList");  
			DBRow[] row = floorOrdersProcessMgrQLL.buildPlanDateIN(listStr,storage_id,startDate,endDate,pc);//in
			return  row;
		}
		catch(Exception e )
		{
			throw new SystemException(e,"buildPlanDate(long storage_id ,long catalog_id,String startDate,String endDate,PageCtrl pc)",log);
		}
	}
	
	/**
	 * 通过起始和截止获得全部日期
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception
	 */
	public DBRow[]  getDate(String startDate,String endDate)
		throws Exception 
	{
		
		//缺少后面天大于 等于前面天的判断
		 long  DAY = 24L * 60L * 60L * 1000L;   
		 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
	     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
		 int num = Long.valueOf(n).intValue();
		 DBRow[] row = new DBRow[num+1];
		 Calendar calendarTemp = Calendar.getInstance();
		 calendarTemp.setTime(dateTemp);
	     int i= 0;
	     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
	     {
	            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
	            row[i] = new DBRow();
	            row[i].add("date",temp );
	            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
	            i++;
	     }
	     row[i] = new DBRow();
	     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
		 return row;
	}
	
	public void setFloorOrdersProcessMgrQLL(
			FloorOrdersProcessMgrQLL floorOrdersProcessMgrQLL) {
		this.floorOrdersProcessMgrQLL = floorOrdersProcessMgrQLL;
	}
}
