package com.cwc.app.api.qll;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.floor.api.qll.FloorProductMgrQLL;
import com.cwc.app.iface.qll.ProductMgrIFaceQLL;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFCell;
public class ProductMgrQLL implements ProductMgrIFaceQLL {

	private FloorProductMgrQLL floorProductMgrQLL;
	
	static Logger log = Logger.getLogger("ACTION");
	
	/**
	 * 获得产品的产品线
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllFatherCatalog(long cid)
		throws Exception 
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			
			return(tree.getAllFatherSQL(cid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllFatherCatalog",log);
		}
	}
	
	/**
	 * 获得默认预警产品信息
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDefultStockWarn(PageCtrl pc)
		throws Exception
	{
		DBRow[] rows=null;
		try
		{
			TDate date = new TDate();
			String endDate=date.formatDate("yyyy-MM-dd").toString();
			date.addDay(-30);
			String startDate=date.formatDate("yyyy-MM-dd").toString();
			rows = floorProductMgrQLL.getStockWarnDefult(pc, startDate, endDate);
			DBRow rowLine = new DBRow();
			String productLineNameTemp="";
			String productLineName="";
			
			for (int i = 0; i < rows.length; i++) 
			{
				DBRow[] rowTemp=this.getAllFatherCatalog(Long.parseLong(rows[i].getString("catalog_id")));
				productLineNameTemp=rowTemp[0].getString("PRODUCT_LINE_ID");
				if(!productLineNameTemp.equals(""))
				{
					rowLine=floorProductMgrQLL.getProductLineName(Long.parseLong(productLineNameTemp));
				}
				if(rowLine!=null)
				{
					productLineName=rowLine.getString("name");
					if(!productLineName.equals(""))
					{
						rows[i].add("p_line_name", productLineName);
					}
				}
			}
			
		}
		catch(Exception e)
		{
			throw new SystemException(e," getStockWarn(0 ",log);
		}
		
		return rows;
	}
	
	/**
	 * 统计产品的平均值及库存
	 * @param pc
	 * @param cmd
	 * @param cidf
	 * @param dayCountf
	 * @param categoryf
	 * @param coefficientf
	 * @param search_product_namef
	 * @param productLinef
	 * @param part_or_allf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockWarn(PageCtrl pc,String cmd,String cidf,int dayCountf,int categoryf,float coefficientf,String search_product_namef,String productLinef,String part_or_allf)
		throws Exception
	{
		DBRow[] rows =null;
		try
		{
			DBRow rowIDlist=null;
			String catalogIdStr="";
			DBRow rowLine = null;
			String productLineName="";
			String productLineNameTemp="";
			TDate date = new TDate();
			long searchKey=0;
			String endDate=date.formatDate("yyyy-MM-dd").toString();
			date.addDay(-dayCountf);
			String startDate=date.formatDate("yyyy-MM-dd").toString(); 
			 
			if(!search_product_namef.equals(""))//根据条码
			{
				DBRow rowTemp = floorProductMgrQLL.getPid(search_product_namef);//由名称转换为ID
				if( rowTemp != null )
				{
					long pc_id = rowTemp.get("pc_id", 0l);//
					if( pc_id !=0)
					{
						rows = floorProductMgrQLL.getStockWarnByName(pc,dayCountf,pc_id, startDate, endDate, cidf, coefficientf, part_or_allf);
					}
				}  
			}
			else if(categoryf!=0||!productLinef.equals("0"))
			{
				if(productLinef.contains("-p")) 
				{
					long lineID = Long.parseLong(productLinef.substring(0, productLinef.indexOf("-p")));
					rowIDlist = floorProductMgrQLL.getLineStr(lineID);
					catalogIdStr = rowIDlist.getString("idList");   
					rowLine=floorProductMgrQLL.getProductLineName(lineID);
					if(rowLine!=null)
					{
						productLineName=rowLine.getString("name");
					}
				}else
				{
					if(categoryf!=0&&productLinef.equals("0"))
					{
						searchKey=categoryf;
					}else
					{
						searchKey=Long.parseLong(productLinef);
					}
					rowIDlist = floorProductMgrQLL.getIdStr(searchKey);
					if(rowIDlist!=null)
					{
						catalogIdStr = rowIDlist.getString("idList"); 
					}
				}
				rows =floorProductMgrQLL.getStockWarnByCatalog(pc,dayCountf,catalogIdStr, startDate, endDate, cidf, coefficientf, part_or_allf);
			}
			else
			{
				rows=floorProductMgrQLL.getStockWarnByCid(pc,dayCountf,startDate, endDate, cidf, coefficientf, part_or_allf);
			}
			
			//获得产品的产品线
			if(!productLinef.contains("-p")&&productLineName.equals(""))
			{
				for (int i = 0; i < rows.length; i++) 
				{
					DBRow[] rowTemp=this.getAllFatherCatalog(Long.parseLong(rows[i].getString("catalog_id")));
					productLineNameTemp=rowTemp[0].getString("PRODUCT_LINE_ID");
					if(!productLineNameTemp.equals(""))
					{
						rowLine=floorProductMgrQLL.getProductLineName(Long.parseLong(productLineNameTemp));
					}
					if(rowLine!=null)
					{
						productLineName=rowLine.getString("name");
						if(!productLineName.equals(""))
						{
							rows[i].add("p_line_name", productLineName);
						}
					}
				}
			}else
			{
				for (int i = 0; i < rows.length; i++) 
				{
					 rows[i].add("p_line_name", productLineName);
				}
			}
		}
		catch(Exception e)
		{
			throw new SystemException(e," getStockWarn(0 ",log);
		}
		return rows;
	}
	
	/**
	 * 导出备货预警
	 */
	public String exportStockWarn(HttpServletRequest request)
		throws Exception
	{
		PageCtrl pc=null;
		try 
		{
			String cmd=StringUtil.getString(request,"cmd");
			int dayCountf=0;
			float coefficientf=0;
			DBRow DateRow[]= null;
			if(cmd.equals("1"))
			{
				DateRow=this.getDefultStockWarn(pc);
				coefficientf=1;
			}
			else
			{
				String cidf=StringUtil.getString(request,"cidf");
				dayCountf=StringUtil.getInt(request,"dayCountf");
				int categoryf=StringUtil.getInt(request,"categoryf");
				coefficientf=Float.parseFloat(StringUtil.getString(request,"coefficientf"));
				String search_product_namef=StringUtil.getString(request,"search_product_namef");
				String productLinef=StringUtil.getString(request,"productLinef");
				String part_or_allf=StringUtil.getString(request,"part_or_allf");
				DateRow=this.getStockWarn(pc,cmd, cidf, dayCountf, categoryf, coefficientf, search_product_namef, productLinef, part_or_allf);
			}
			
			if (DateRow.length > 0) 
			{
				XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product_require/ExportStockWarn.xlsx"));  
				XSSFSheet sheet= wb.getSheet("Sheet1");  
				
				XSSFCellStyle style = wb.createCellStyle();
				style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				style.setLocked(false); //创建样式
				style.setWrapText(true);
				
				XSSFFont  fontWarn =wb.createFont();
				fontWarn.setFontName("Arial");   
				fontWarn.setFontHeightInPoints((short)10);   
				fontWarn.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
				fontWarn.setColor(HSSFColor.BLUE.index);
				
				XSSFCellStyle styleWarn = wb.createCellStyle();
				styleWarn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				styleWarn.setLocked(false); //创建样式
				styleWarn.setWrapText(true);
				styleWarn.setFont(fontWarn);
				
				
				XSSFFont  font =wb.createFont();
				font.setFontName("Arial");   
				font.setFontHeightInPoints((short)10);   
				font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
				
				XSSFCellStyle styleTitle = wb.createCellStyle();
				styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				styleTitle.setLocked(false);  
				styleTitle.setWrapText(true);
				styleTitle.setFont(font);
				
				XSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); 
				stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
				
				XSSFFont  fontHead =wb.createFont();
				fontHead.setFontName("Arial");   
				fontHead.setFontHeightInPoints((short)13);   
				fontHead.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); 
				
				XSSFCellStyle styleHead = wb.createCellStyle();
				styleHead.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				styleHead.setLocked(false);  
				styleHead.setWrapText(true);
				styleHead.setFont(fontHead);
				 	
				//title赋值
				XSSFRow rowFirst = sheet.createRow(0);  
				TDate date = new TDate();
				String endDate=date.formatDate("yyyy-MM-dd").toString();
				date.addDay(-dayCountf);
				String startDate=date.formatDate("yyyy-MM-dd").toString(); 
				
				short hieght=400;
				rowFirst.createCell(0).setCellValue(startDate+"————"+endDate+"：备货预警商品信息");
				rowFirst.getCell(0).setCellStyle(styleHead);
				rowFirst.setHeight(hieght);
				
				
				XSSFRow row = sheet.createRow(1);  
				row.createCell(0).setCellValue("产品线");
				row.getCell(0).setCellStyle(styleTitle);
				row.createCell(1).setCellValue("产品类别");
				row.getCell(1).setCellStyle(styleTitle);
				row.createCell(2).setCellValue("产品名称");
				row.getCell(2).setCellStyle(styleTitle);
				row.createCell(3).setCellValue("仓库名称");
				row.getCell(3).setCellStyle(styleTitle);
				row.createCell(4).setCellValue("平均值");
				row.getCell(4).setCellStyle(styleTitle);
				row.createCell(5).setCellValue("当前库存");
				row.getCell(5).setCellStyle(styleTitle);
				row.createCell(6).setCellValue("可用天数");
				row.getCell(6).setCellStyle(styleTitle);
				row.createCell(7).setCellValue("备货天数");
				row.getCell(7).setCellStyle(styleTitle);
				row.createCell(8).setCellValue("备货期销量");
				row.getCell(8).setCellStyle(styleTitle);
				 
				//value赋值
				for (int i = 0; i < DateRow.length; i++)
				{
					row = sheet.createRow(i+2); 
					 
					row.createCell(0).setCellValue(DateRow[i].getString("p_line_name"));
					row.getCell(0).setCellStyle(style);	
					row.createCell(1).setCellValue(DateRow[i].getString("catalog_title"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(DateRow[i].getString("product_title"));
					row.getCell(2).setCellStyle(style);
					
					row.createCell(3).setCellValue(DateRow[i].getString("storage_title"));
					row.getCell(3).setCellStyle(style);
					
					row.createCell(4).setCellValue(DateRow[i].getString("avg_count"));
					row.getCell(4).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(4).setCellStyle(style);
					
					row.createCell(5).setCellValue(DateRow[i].getString("store_count"));
					row.getCell(5).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(5).setCellStyle(style);
					
					String dayCount="";
					if(DateRow[i].getString("day_count").equals(""))
					{
						dayCount="0.0";
					}else
					{
						dayCount=DateRow[i].getString("day_count");
					}
					row.getCell(6).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.createCell(6).setCellValue(dayCount);
					
					if(!DateRow[i].getString("store_count_alert").equals(""))
					{
						if(Double.parseDouble(dayCount)<=Double.parseDouble(DateRow[i].getString("store_count_alert")))
						{
							row.getCell(6).setCellStyle(styleWarn);
						}
						else
						{
							row.getCell(6).setCellStyle(style);
						}
					}
					else
					{
						row.getCell(6).setCellStyle(style);
					}
					
					row.createCell(7).setCellValue(DateRow[i].getString("store_count_alert"));
					row.getCell(7).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(7).setCellStyle(style);
					
					if(!DateRow[i].getString("avg_count").equals("")&&!DateRow[i].getString("store_count_alert").equals(""))
					{
						row.createCell(8).setCellValue(Double.parseDouble(DateRow[i].getString("store_count_alert"))*Double.parseDouble(DateRow[i].getString("avg_count"))*coefficientf);
					}
					else
					{
						row.createCell(8).setCellValue(0.0);
					}
					row.getCell(8).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					row.getCell(8).setCellStyle(style);
					
				}
				//写文件  
				String path = "upl_excel_tmp/export_StockAndWarn"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
				OutputStream os= new FileOutputStream(Environment.getHome()+path);
				wb.write(os);  
				os.close();  
				return (path);
			}
			else
			{
				return "0";
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e," exportPlanAndStock(HttpServletRequest request) ",log);
		} 
	}
	
	public DBRow[] preparationPlanAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,String product_name,long ps_id,int isAlert,float coefficient,PageCtrl pc)
		throws Exception
	{
		try 
		{
			long pc_id = 0;
			DBRow product = floorProductMgrQLL.getPid(product_name);
			if(product !=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			
			return (floorProductMgrQLL.preparationPlanAnalysisNew(start_date, end_date, catalog_id, pro_line_id, pc_id, ps_id, isAlert,coefficient,pc));
//			return (floorProductMgrQLL.preparationPlanAnalysis(start_date, end_date, catalog_id, pro_line_id, pc_id, ps_id, isAlert,coefficient,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"preparationPlanAnalysis",log);
		}
	}
	
	//导出备货计划分析
	public String exportPreparationPlanAnalysis(HttpServletRequest request)
		throws Exception
	{
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long pro_line_id = StringUtil.getLong(request,"pro_line_id");
		String product_name = StringUtil.getString(request,"product_name");
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		int isAlert = StringUtil.getInt(request,"isAlert");
		float coefficient = StringUtil.getFloat(request,"coefficient");
		
		String input_en_date = StringUtil.getString(request,"input_en_date");
		String input_st_date = StringUtil.getString(request,"input_st_date");

		long pc_id = 0;
		DBRow product = floorProductMgrQLL.getPid(product_name);
		if(product !=null)
		{
			pc_id = product.get("pc_id",0l);
		}
		
		DBRow DateRow[]= floorProductMgrQLL.getDate(input_st_date, input_en_date);
		DBRow[] rows = floorProductMgrQLL.preparationPlanAnalysisNew(input_st_date, input_en_date, catalog_id, pro_line_id, pc_id, ps_id, isAlert,coefficient,null);
		
		//excel文件
		XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/product_require/ExportStockWarn.xlsx"));  
		XSSFSheet sheet= wb.getSheet("Sheet1");  
		
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setLocked(false); //创建样式
		style.setWrapText(true);
		
		XSSFFont  fontWarn =wb.createFont();
		fontWarn.setFontName("Arial");   
		fontWarn.setFontHeightInPoints((short)10);   
		fontWarn.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		fontWarn.setColor(HSSFColor.BLUE.index);
		
		XSSFCellStyle styleWarn = wb.createCellStyle();
		styleWarn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleWarn.setLocked(false); //创建样式
		styleWarn.setWrapText(true);
		styleWarn.setFont(fontWarn);
		
		
		XSSFFont  font =wb.createFont();
		font.setFontName("Arial");   
		font.setFontHeightInPoints((short)10);   
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);    
		
		XSSFCellStyle styleTitle = wb.createCellStyle();
		styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleTitle.setLocked(false);  
		styleTitle.setWrapText(true);
		styleTitle.setFont(font);
		
		XSSFCellStyle stylelock = wb.createCellStyle();
		stylelock.setLocked(true); 
		stylelock.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		
		XSSFFont  fontHead =wb.createFont();
		fontHead.setFontName("Arial");   
		fontHead.setFontHeightInPoints((short)13);   
		fontHead.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); 
		
		XSSFCellStyle styleHead = wb.createCellStyle();
		styleHead.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHead.setLocked(false);  
		styleHead.setWrapText(true);
		styleHead.setFont(fontHead);
		 	
		//title赋值
		XSSFRow rowFirst = sheet.createRow(0);   
		
		short hieght=400;
		rowFirst.createCell(0).setCellValue(input_st_date+"————"+input_en_date+"：备货预警商品信息");
		rowFirst.getCell(0).setCellStyle(styleHead);
		rowFirst.setHeight(hieght);
		
		
		XSSFRow row = sheet.createRow(1);  
		row.createCell(0).setCellValue("产品线");
		row.getCell(0).setCellStyle(styleTitle);
		row.createCell(1).setCellValue("产品类别");
		row.getCell(1).setCellStyle(styleTitle);
		row.createCell(2).setCellValue("产品名称");
		row.getCell(2).setCellStyle(styleTitle);
		row.createCell(3).setCellValue("仓库名称");
		row.getCell(3).setCellStyle(styleTitle);
		row.createCell(4).setCellValue("平均值");
		row.getCell(4).setCellStyle(styleTitle);
		row.createCell(5).setCellValue("当前库存");
		row.getCell(5).setCellStyle(styleTitle);
		row.createCell(6).setCellValue("可用天数");
		row.getCell(6).setCellStyle(styleTitle);
		row.createCell(7).setCellValue("备货天数");
		row.getCell(7).setCellStyle(styleTitle);
		row.createCell(8).setCellValue("备货期销量");
		row.getCell(8).setCellStyle(styleTitle);
		
		String title = "";
		int blankCell = 8;
		blankCell++;
		for (int i = 0; i < DateRow.length; i++)
		{
			row.createCell(i+blankCell).setCellValue(DateRow[i].getString("date"));
			row.getCell(i+blankCell).setCellStyle(styleTitle);	
		}
		
		//赋值
		
		for (int i = 0; i < rows.length; i++)
		{
			blankCell = 8;
			row = sheet.createRow(i+2); 
			 
			row.createCell(0).setCellValue(rows[i].getString("product_line_title"));
			row.getCell(0).setCellStyle(style);	
			row.createCell(1).setCellValue(rows[i].getString("product_catalog_title"));
			row.getCell(1).setCellStyle(style);
			row.createCell(2).setCellValue(rows[i].getString("p_name"));
			row.getCell(2).setCellStyle(style);
			row.createCell(3).setCellValue(rows[i].getString("title"));
			row.getCell(3).setCellStyle(style);
			
			row.createCell(4).setCellValue(rows[i].get("avg_count",0d));
			row.getCell(4).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			row.getCell(4).setCellStyle(style);
			
			row.createCell(5).setCellValue(rows[i].get("store_count",0d));
			row.getCell(5).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			row.getCell(5).setCellStyle(style);
			double dayCount;
			if(rows[i].getString("day_count").equals(""))
			{
				dayCount=0;
			}else
			{
				dayCount=rows[i].get("day_count",0d);
			}
			row.createCell(6).setCellValue(dayCount);
			row.getCell(6).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			
			if(!rows[i].getString("store_count_alert").equals(""))
			{
				if(dayCount<=rows[i].get("store_count_alert",0d))
				{
					row.getCell(6).setCellStyle(styleWarn);
				}
				else
				{
					row.getCell(6).setCellStyle(style);
				}
			}
			else
			{
				row.getCell(6).setCellStyle(style);
			}
			
			row.createCell(7).setCellValue(rows[i].get("store_count_alert",0d));
			row.getCell(7).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			row.getCell(7).setCellStyle(style);
			
			if(!rows[i].getString("avg_count").equals("")&&!rows[i].getString("store_count_alert").equals(""))
			{
				row.createCell(8).setCellValue(Double.parseDouble(rows[i].getString("store_count_alert"))*Double.parseDouble(rows[i].getString("avg_count"))*coefficient);
			}
			else
			{
				row.createCell(8).setCellValue(0.0);
			}
			row.getCell(8).setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			row.getCell(8).setCellStyle(style);
			
			blankCell++;
			for (int j = 0; j < DateRow.length; j++)
			{
				row.createCell(j+blankCell).setCellValue(rows[i].get("date_"+j,0d));
				row.getCell(j+blankCell).setCellStyle(style);	
			}
			
			title = rows[i].getString("title");
		}
		//写文件  
		//String path = "upl_excel_tmp/export_StockAndWarn"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsx";
		String path = "upl_excel_tmp/"+title+"仓库备货预警"+input_st_date+"至"+input_en_date+".xlsx";
		OutputStream os= new FileOutputStream(Environment.getHome()+path);
		wb.write(os);  
		os.close();  
		return (path);
	}
	 
	
	public void setFloorProductMgrQLL(FloorProductMgrQLL floorProductMgrQLL) {
		this.floorProductMgrQLL = floorProductMgrQLL;
	}

}
