package com.cwc.app.api.qll;
import com.cwc.util.*;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.exception.paypal.SetExpressCheckOutFalException;
import com.cwc.app.floor.api.qll.FloorPaypalPaymentMgr;
import com.cwc.app.iface.qll.PaypalPaymentMgrIFace;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.paypal.sdk.core.nvp.NVPDecoder;
import com.paypal.sdk.core.nvp.NVPEncoder;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.NVPCallerServices;
public class PaypalPaymentMgr implements PaypalPaymentMgrIFace {
	
		private FloorPaypalPaymentMgr floorPaypalPaymentMgr;
		static Logger log = Logger.getLogger("ACTION");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();  
		java.text.DecimalFormat   df   = new DecimalFormat("###,###.00",dfs); 
		private String environment;
		 
		public void setEnvironment(String environment) {
			this.environment = environment;
		}
		
		/**
		 * 对pending状态的订单的进一步操作
		 * @param request
		 * @return
		 * @throws Exception
		 */
		public String ManagePendingTransactionStatus(HttpServletRequest request )
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller = null;
			String rs = "";
			try
			{
				DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=row.getString("account_name");
				String pwd=row.getString("account_pwd");
				String Signature=row.getString("account_signature");
			    caller =getPaypalCall(name,pwd,Signature);
			    
				encoder.add("VERSION", "72.0");
				encoder.add("METHOD","ManagePendingTransactionStatus");
				encoder.add("TRANSACTIONID",StringUtil.getString(request, "txn_id"));
				encoder.add("ACTION",StringUtil.getString(request, "action"));		
				String NVPRequest = encoder.encode();
				String NVPResponse =(String) caller.call(NVPRequest);
				decoder.decode(NVPResponse)  ;
				rs =  decoder.get("ACK")+","+decoder.get("L_SHORTMESSAGE0")+","+decoder.get("L_LONGMESSAGE0")+","+decoder.get("TRANSACTIONID");
			} catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return rs;
		}
		
		/**
		 * paypal虚拟终端执行收款方法
		 * @param request
		 * @throws Exception
		 */
		public String ExceuteVertualTerminal(HttpServletRequest request)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller = null;
			String rs = "";
			try
			{
				DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=row.getString("account_name");
				String pwd=row.getString("account_pwd");
				String Signature=row.getString("account_signature");
			    caller =getPaypalCall(name,pwd,Signature);
				encoder.add("VERSION", "72.0");
				encoder.add("METHOD","DoDirectPayment");
				encoder.add("PAYMENTACTION","Sale");
				encoder.add("AMT",StringUtil.getString(request, "amount").trim());
				encoder.add("CREDITCARDTYPE",StringUtil.getString(request, "cardType").trim());		
				encoder.add("ACCT",StringUtil.getString(request, "acct").trim());						
				encoder.add("EXPDATE",StringUtil.getString(request, "expdate").trim());
				encoder.add("CVV2",StringUtil.getString(request, "cvv2").trim());
				
				encoder.add("FIRSTNAME",StringUtil.getString(request, "firstName").trim());
				encoder.add("LASTNAME",StringUtil.getString(request, "lastName").trim());	
				encoder.add("SHIPTONAME",StringUtil.getString(request, "firstName").trim()+" "+StringUtil.getString(request, "lastName").trim());
				
				encoder.add("STREET",StringUtil.getString(request, "street").trim());
				encoder.add("SHIPTOSTREET",StringUtil.getString(request, "street").trim()); 
				
				encoder.add("CITY",StringUtil.getString(request, "city").trim());	
				encoder.add("SHIPTOCITY",StringUtil.getString(request, "city").trim()); 
				
				encoder.add("STATE",StringUtil.getString(request, "state").trim());	
				encoder.add("SHIPTOSTATE",StringUtil.getString(request, "state").trim());	 
				
				encoder.add("ZIP",StringUtil.getString(request, "zip").trim());	
				encoder.add("SHIPTOZIP",StringUtil.getString(request, "zip").trim()); 
				
				encoder.add("SHIPTOCOUNTRY",StringUtil.getString(request, "countryCode").trim()); 
				encoder.add("COUNTRYCODE",StringUtil.getString(request, "countryCode").trim());
				
				encoder.add("SHIPPINGAMT","0.00");
				encoder.add("TAXAMT", "0.00");
				encoder.add("ITEMAMT","0.00");
				encoder.add("CURRENCYCODE", StringUtil.getString(request, "currencyCode").trim());
				encoder.add("CUSTOM", "payment_type=paypaldp"+"|"+"payer_phone="+StringUtil.getString(request, "phone").trim());//标识是信用卡付款
				encoder.add("EMAIL",StringUtil.getString(request, "email").trim());//
				
				String NVPRequest = encoder.encode();
				String NVPResponse =(String) caller.call(NVPRequest);
				decoder.decode(NVPResponse);
				rs =  decoder.get("ACK")+","+decoder.get("L_SHORTMESSAGE0")+","+decoder.get("L_LONGMESSAGE0")+","+decoder.get("TRANSACTIONID");
				//log.info(NVPResponse);
			} 
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return rs;
		}

		/**
		 * 虚拟终端付款的方法   
		 * @param amount 付款总数（包含小数点后两位）
		 * @param cardType 信用卡类型
		 * @param acct   信用卡卡号
		 * @param expdate  信用卡到期的日期
		 * @param cvv2  信用卡安全的三位数字
		 * @param firstName  付款人名字
		 * @param lastName   付款人的姓氏
		 * @param street  收货地址
		 * @param city   城市
		 * @param state 州
		 * @param zip  邮编
		 * @param countryCode  国家代码
		 * @param  shipping,
		 * @param  taxRate ,税率
		 * @param  itemAmount	货物金额		
		 * @param note 注释
		 */
		public void virtualTerminal(double amount,String cardType,
				String acct,String expdate,String cvv2, String firstName,
				String lastName, String street, String city, String state, 
				String zip, String countryCode,String shipping,String taxRate ,double itemAmount,String currencyCode)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller = null;
			try
			{
				DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=row.getString("account_name");
				String pwd=row.getString("account_pwd");
				String Signature=row.getString("account_signature");
			    caller =getPaypalCall(name,pwd,Signature);
				 
				encoder.add("VERSION", "72.0");
				encoder.add("METHOD","DoDirectPayment");
				encoder.add("PAYMENTACTION","Sale");
				encoder.add("AMT",String.valueOf(amount));
				encoder.add("CREDITCARDTYPE",cardType.trim());		
				encoder.add("ACCT",acct.trim());						
				encoder.add("EXPDATE",expdate.trim());
				encoder.add("CVV2",cvv2.trim());
				encoder.add("FIRSTNAME",firstName.trim());
				encoder.add("LASTNAME",lastName.trim());										
				encoder.add("STREET",street.trim());
				encoder.add("CITY",city.trim());	
				encoder.add("STATE",state.trim());			
				encoder.add("ZIP",zip.trim());	
				encoder.add("COUNTRYCODE",countryCode.trim());
				encoder.add("SHIPPINGAMT", shipping);
				encoder.add("TAXAMT", taxRate);
				encoder.add("ITEMAMT",String.valueOf(itemAmount).trim());
				encoder.add("CURRENCYCODE", currencyCode.trim());
				String NVPRequest = encoder.encode();
				String NVPResponse =(String) caller.call(NVPRequest);
				decoder.decode(NVPResponse);
			} 
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
		}
		
		
		/**
		 * 退款的方法   
		 * @param refundType  全部付款Full ，部分退款Partial
		 * @param transactionId  交易ID
		 * @param amount 部分退款时的金额
		 * @param note 注释显示在买家及买家的交易中的信息可事实退款原因或备注
		 */
		public String rufundTransaction(String refundType,String transactionId,double amount,String note,String currencyCode)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller = null;
			String rs = "";
		  	try
		  	{
		  		DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=row.getString("account_name");
				String pwd=row.getString("account_pwd");
				String Signature=row.getString("account_signature");
				
			    caller =getPaypalCall(name,pwd,Signature);
				encoder.add("VERSION", "72.0");			
				encoder.add("METHOD","RefundTransaction");
				encoder.add("REFUNDTYPE",refundType); 
				encoder.add("TRANSACTIONID",transactionId);
				encoder.add("CURRENCYCODE",currencyCode);
				if((refundType != null) && (refundType.length() > 0) && (refundType.equalsIgnoreCase("Partial")))
				{
					encoder.add("AMT",String.valueOf(amount));					
				}
				encoder.add("NOTE",note);
				String NVPRequest = encoder.encode();
				String NVPResponse = (String) caller.call(NVPRequest);
				decoder.decode(NVPResponse);
				rs = decoder.get("ACK")+","+decoder.get("L_SHORTMESSAGE0")+","+decoder.get("L_LONGMESSAGE0");
			  	}catch (Exception ex) {
			  		
			  		ex.printStackTrace();
			  	}
			  	return rs;
		}
		
		/**
		 * 信用卡直接付款
		 * @param request
		 * @return
		 * @throws Exception
		 */
		public String doExpressCheckOut(HttpServletRequest request)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			
			try
			{
				DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=row.getString("account_name");
				String pwd=row.getString("account_pwd");
				String Signature=row.getString("account_signature");
				NVPCallerServices caller =getPaypalCall(name,pwd,Signature);
				 
				encoder.add("VERSION", "51.0");
				encoder.add("METHOD","DoExpressCheckoutPayment");
				encoder.add("TOKEN",StringUtil.getString(request,"token"));
				encoder.add("PAYERID",StringUtil.getString(request,"payerID"));
				encoder.add("AMT",StringUtil.getString(request,"amount"));
				encoder.add("PAYMENTACTION","sale");
//				encoder.add("CURRENCYCODE","USD");
				String NVPRequest = encoder.encode();
				String NVPResponse =caller.call(NVPRequest);
				decoder.decode(NVPResponse);
			} 
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			return decoder.get("ACK");
		}
		
		/**
		 * paypal付款的第二步 获取token及payerID
		 * @param token 参数为第一步获得的token
		 * @return
		 * @throws Exception
		 */
		public String getExpressCheckOut(String token) 
			throws Exception
		{
			
			DBRow row = floorPaypalPaymentMgr.getAccountInfo();
			String name=row.getString("account_name");
			String pwd=row.getString("account_pwd");
			String Signature=row.getString("account_signature");
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller=getPaypalCall(name,pwd,Signature);
			try
			{
				encoder.add("VERSION", "51.0");
				encoder.add("METHOD", "GetExpressCheckoutDetails");
	
				encoder.add("TOKEN", "");
				String NVPRequest = encoder.encode();
				String NVPResponse = caller.call(NVPRequest);
				decoder.decode(NVPResponse);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			return decoder.get("ACK")+","+decoder.get("TOKEN")+","+decoder.get("PAYERID");
		}
		
		/**
		 * paypal账号付款的第一步，返回token
		 * @param caller 传入初始化通信的需要的service 
		 * @param amount  所需要交易的金额
		 * @param paymentAction
		 * @param currencycode
		 * @return
		 * @throws Exception
		 */
		private String setExpressCheckOut(NVPCallerServices caller,double amount)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			encoder.add("VERSION", "51.0");			
			encoder.add("METHOD","SetExpressCheckout");
			encoder.add("RETURNURL","http://localhost:8080/paypal20/administrator/paypal/getExpressCheckOut.html");
			encoder.add("CANCELURL","http://localhost:8080/paypal20/administrator/admin/");	
			encoder.add("AMT",String.valueOf(amount));
			String NVPRequest= encoder.encode();
			String NVPResponse =caller.call(NVPRequest);
			decoder.decode(NVPResponse);
			return decoder.get("ACK")+","+decoder.get("TOKEN");
		}
	
		/**
		 * 执行获取token时的action方法
		 * @param request
		 * @return
		 * @throws SetExpressCheckOutFalException
		 * @throws Exception
		 */
		public String  doSetExpressCheckOut(HttpServletRequest request)
				throws SetExpressCheckOutFalException,Exception
		{
			DBRow row = floorPaypalPaymentMgr.getAccountInfo();
			String name=row.getString("account_name");
			String pwd=row.getString("account_pwd");
			String Signature=row.getString("account_signature");	 
			
			try
			{
				NVPCallerServices caller=getPaypalCall(name,pwd,Signature);
				String rs=setExpressCheckOut(caller,StringUtil.getDouble(request, "amount"));
				String []result = rs.split(",");
				if(result[0].equals("Success")||result[0]=="Success")
				{
					 return result[1];
				}
				else
				{
					throw new SetExpressCheckOutFalException();
				}
			}
			catch (SetExpressCheckOutFalException e) 
			{
				throw e;
			}
			catch (Exception e) 
			{
				throw new SystemException(e,"doSetExpressCheckOut",log);
			}
		}
		
		/**
		 * 初始化API Service 
		 * @param APIUserName 用户名
		 * @param APIPassWord 密码
		 * @param Signature  签名
		 * @return  返回一个服务对象
		 * @throws PayPalException
		 */
		private NVPCallerServices getPaypalCall(String APIUserName,String APIPassWord,String Signature) 
			throws PayPalException
		{
			NVPCallerServices caller = null;
			try
			{
				caller = new NVPCallerServices();
				APIProfile profile = ProfileFactory.createSignatureAPIProfile();
				profile.setAPIUsername(APIUserName);
				profile.setAPIPassword(APIPassWord);
				profile.setSignature(Signature);
				profile.setEnvironment(environment);
			  //profile.setEnvironment("SandBox");
				 
				caller.setAPIProfile(profile);
			}
			catch (Exception e)
			{
			 e.printStackTrace();
			}
			return caller;
		}
		
		/**
		 * 
		 * @param caller 调用paypalAPI 初始化的server
		 * @param paymentAction 付款类型：sale
		 * @param amount 付款总数（包含小数点后两位）
		 * @param cardType 信用卡类型
		 * @param acct   信用卡卡号
		 * @param expdate  信用卡到期的日期
		 * @param cvv2  信用卡安全的三位数字
		 * @param firstName  付款人名字
		 * @param lastName   付款人的姓氏
		 * @param street  收货地址
		 * @param city   城市
		 * @param state 州
		 * @param zip  邮编
		 * @param countryCode  国家代码
		 */
		private void DirectPayment(NVPCallerServices caller,String paymentAction,double amount,String cardType,
									String acct,String expdate,String cvv2, String firstName,
									String lastName, String street, String city, String state, 
									String zip, String countryCode)
			throws Exception
		{
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			
			try
			{
				encoder.add("VERSION", "51.0");
				encoder.add("METHOD","DoDirectPayment");
				encoder.add("PAYMENTACTION","sale");
				encoder.add("AMT",String.valueOf(amount));
				encoder.add("CREDITCARDTYPE",cardType);		
				encoder.add("ACCT",acct);						
				encoder.add("EXPDATE",expdate);
				encoder.add("CVV2",cvv2);
				encoder.add("FIRSTNAME",firstName);
				encoder.add("LASTNAME",lastName);										
				encoder.add("STREET",street);
				encoder.add("CITY",city);	
				encoder.add("STATE",state);			
				encoder.add("ZIP",zip);	
				encoder.add("COUNTRYCODE",countryCode);
				String NVPRequest = encoder.encode();
				String NVPResponse =(String) caller.call(NVPRequest);
				decoder.decode(NVPResponse);
				 
			} catch(Exception ex)
			{
				ex.printStackTrace();
			}
			 decoder.get("ACK");
		}
		
		/**
		 * Direct payment action的方法
		 * @param request  页面参数
		 * @throws Exception
		 */
		public void doDirectPayment(HttpServletRequest request) 
			throws Exception
		{
			//付款信息的参数
			String  paymentAction = "sale"; 
			double  amount = StringUtil.getDouble(request,"amount"); 
			String  cardType = StringUtil.getString(request,"cardType"); 
			String  acct = StringUtil.getString(request,"acct"); 
			String  expdate = StringUtil.getString(request,"expdate"); 
			String  cvv2 = StringUtil.getString(request,"cvv2"); 
			String  firstName = StringUtil.getString(request,"firstName"); 
			String  lastName = StringUtil.getString(request,"lastName"); 
			String  street = StringUtil.getString(request,"street"); 
			String  city = StringUtil.getString(request,"city"); 
			String  state = StringUtil.getString(request,"state"); 
			String  zip = StringUtil.getString(request,"zip"); 
			String  countryCode = StringUtil.getString(request,"countryCode"); 
			//初始化API的参数
			DBRow row = floorPaypalPaymentMgr.getAccountInfo();
			String name=row.getString("account_name");
			String pwd=row.getString("account_pwd");
			String Signature=row.getString("account_signature");	 
			
			NVPCallerServices caller=getPaypalCall(name,pwd,Signature);
			DirectPayment(caller,paymentAction,amount,cardType,acct,expdate,cvv2,firstName,lastName,street,city,state,zip,countryCode);
			
		}
		 
		public void setFloorPaypalPaymentMgr(FloorPaypalPaymentMgr floorPaypalPaymentMgr) {
			this.floorPaypalPaymentMgr = floorPaypalPaymentMgr;
		}

		/**
		 * 1.对于Ebay_Item 信用卡付款 商品的信息都必须传入进去 buyer_id通过custom字段custom字段中要包含:Ebay
		 */
		@Override
		public String ExceuteVertualTerminalByPayMentPage(
				HttpServletRequest request) throws Exception {
			String ip = request.getHeader("x-forwarded-for");
	        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	             ip = request.getHeader("Proxy-Client-IP");
	        }
	       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getHeader("WL-Proxy-Client-IP");
	        }
	        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	           ip = request.getRemoteAddr();
	        }
	        String ipString = ip+"-"+ip;
			NVPEncoder encoder = new NVPEncoder();
			NVPDecoder decoder = new NVPDecoder();
			NVPCallerServices caller = null;
			DBRow paymentInfo = new DBRow();
			paymentInfo.add("payment_source", "customer");
			paymentInfo.add("user", "customer");
			paymentInfo.add("deal_time",DateUtil.NowStr());
			paymentInfo.add("tax_amt",0.0d);
			paymentInfo.add("note", StringUtil.getString(request, "note"));
			String rs = "";
			try
			{
				DBRow row = floorPaypalPaymentMgr.getAccountInfo();
				String name=  row.getString("account_name");
				String pwd=  row.getString("account_pwd");
				String Signature= row.getString("account_signature");
			    caller =getPaypalCall(name,pwd,Signature);
				encoder.add("VERSION", "72.0");
				encoder.add("METHOD","DoDirectPayment");
				encoder.add("PAYMENTACTION","Sale");
				encoder.add("AMT",StringUtil.getString(request, "amount").trim());
				paymentInfo.add("amount", StringUtil.getString(request, "amount").trim());
				encoder.add("CURRENCYCODE", StringUtil.getString(request, "currencyCode"));
				paymentInfo.add("currency_code", StringUtil.getString(request, "currencyCode"));
				encoder.add("IPADDRESS", ip);
				paymentInfo.add("ip", ip);
				//encoder.add("CREDITCARDTYPE",StrUtil.getString(request, "cardType").trim());		
				encoder.add("ACCT",StringUtil.getString(request, "acct").trim());						
				paymentInfo.add("acct", StringUtil.getString(request, "acct").trim());
				encoder.add("EXPDATE",StringUtil.getString(request, "expdate").trim());
				paymentInfo.add("expdate", StringUtil.getString(request, "expdate").trim());
				encoder.add("CVV2",StringUtil.getString(request, "cvv2").trim());
				paymentInfo.add("cvv2", StringUtil.getString(request, "cvv2").trim());
				
				encoder.add("FIRSTNAME",StringUtil.getString(request, "firstName").trim());
				paymentInfo.add("first_name", StringUtil.getString(request, "firstName").trim());
				encoder.add("LASTNAME",StringUtil.getString(request, "lastName").trim());	
				paymentInfo.add("last_name", StringUtil.getString(request, "lastName").trim());
				encoder.add("SHIPTONAME",StringUtil.getString(request, "firstName").trim()+" "+StringUtil.getString(request, "lastName").trim());
				
				encoder.add("STREET",StringUtil.getString(request, "street").trim());
				paymentInfo.add("street", StringUtil.getString(request, "street").trim());
				encoder.add("SHIPTOSTREET",StringUtil.getString(request, "street").trim()); 
				
				encoder.add("CITY",StringUtil.getString(request, "city").trim());	
				paymentInfo.add("city", StringUtil.getString(request, "city").trim());
				encoder.add("SHIPTOCITY",StringUtil.getString(request, "city").trim()); 
				
				encoder.add("STATE",StringUtil.getString(request, "state").trim());	
				paymentInfo.add("state",StringUtil.getString(request, "state").trim());	
				encoder.add("SHIPTOSTATE",StringUtil.getString(request, "state").trim());	 
				
				encoder.add("ZIP",StringUtil.getString(request, "zip").trim());	
				paymentInfo.add("zip",StringUtil.getString(request, "zip").trim());	
				encoder.add("SHIPTOZIP",StringUtil.getString(request, "zip").trim()); 
				
				encoder.add("SHIPTOCOUNTRY",StringUtil.getString(request, "countryCode").trim()); 
				encoder.add("COUNTRYCODE",StringUtil.getString(request, "countryCode").trim());
				paymentInfo.add("country_code",StringUtil.getString(request, "countryCode").trim());	
				
				 
				String bill_id = StringUtil.getString(request, "bill_id");
				encoder.add("EMAIL",StringUtil.getString(request, "email").trim());//
				/**
				 * 添加  Ebay Item 
				 * payforitem 表示的是第三个标签页
				 * payforebayitem 表示的是为EbayItem
				 */
				String come_from = StringUtil.getString(request, "come_from");
				if(come_from.equals("payforebayitem") || come_from.equals("payforitem") ){
					int index = 1 ;
					double ITEMAMT = 0.0;
					String itemNumber = StringUtil.getString(request, "item_number_" + index);
					while(itemNumber != null && itemNumber.length() > 0){
						//分别读取amout quantity number  
						String itemName = StringUtil.getString(request, "item_name_"+index);
						String amount = StringUtil.getString(request, "amount_"+index);
						String quantity = StringUtil.getString(request, "quantity_"+index);
						double eachAmount = Double.parseDouble(amount)  * Double.parseDouble(quantity);
						encoder.add("L_NUMBER" + (index-1),itemNumber);
						encoder.add("L_NAME"+ (index-1), itemName);
						encoder.add("L_AMT"+ (index-1), getDoubleValueFromString(""+amount));
						encoder.add("L_QTY"+ (index-1),quantity);
						ITEMAMT += eachAmount;
						index++ ;
						itemNumber = StringUtil.getString(request, "item_number_" + index);
					}
					encoder.add("ITEMAMT", getDoubleValueFromString(""+ITEMAMT));
					paymentInfo.add("item_amt", ITEMAMT);
					encoder.add("SHIPDISCAMT", "0.0");
					encoder.add("SHIPPINGAMT", getDoubleValueFromString(StringUtil.getString(request, "shipping_1"))); 
					paymentInfo.add("shipping_amt", getDoubleValueFromString(StringUtil.getString(request, "shipping_1")));
					encoder.add("CUSTOM", "payment_type=paypaldp"+",ip="+ipString+",payer_phone="+StringUtil.getString(request, "phone").trim()+"," + StringUtil.getString(request, "custom"));//标识是信用卡付款
 				}else{
 					encoder.add("SHIPPINGAMT","0.00");
					paymentInfo.add("shipping_amt", 0.0d);
 					encoder.add("TAXAMT", "0.00");
 					encoder.add("ITEMAMT","0.00");
 					paymentInfo.add("item_amt", 0.0d);
					encoder.add("CUSTOM", "payment_type=paypaldp"+",ip="+ipString+",payer_phone="+StringUtil.getString(request, "phone").trim()+"," + "invoice:" + bill_id);//标识是信用卡付款
 				}
			 
 				String NVPRequest = encoder.encode();
				String NVPResponse =(String) caller.call(NVPRequest);
				decoder.decode(NVPResponse);
				rs =  decoder.get("ACK")+","+decoder.get("L_SHORTMESSAGE0")+","+decoder.get("L_LONGMESSAGE0")+","+decoder.get("TRANSACTIONID");
				String txn_id = decoder.get("TRANSACTIONID");
				paymentInfo.add("txn_id", txn_id);
				String result_reasion = decoder.get("L_LONGMESSAGE0");
				paymentInfo.add("result_reasion", result_reasion);
				paymentInfo.add("result", decoder.get("ACK"));
				log.info(NVPResponse);
			} 
 			catch(Exception ex){
				ex.printStackTrace();
			} 
			finally
			{
				try
				{
					floorPaypalPaymentMgr.addPaymentByCreadCardAndCustomerExecute(paymentInfo);
				}
				catch (Exception ex)
				{
					log.error("payment pay execute success but system error : result : " + rs + " encoder : " + encoder.toString());
				}
			}
			 
			return rs;
		}
		//格式化 数字 将所有的数字都转化为 保留两位小数。比如1,233.12 121.00
		private String getDoubleValueFromString(String value) throws Exception {
			try{
				return	df.format(Double.parseDouble(value));
			}catch (Exception e) {
				throw new SystemException(e,"getDoubleValueFromString",log);
			}
		}
		private void setDBRowValueFromEncoder(NVPEncoder encoder , NVPDecoder decoder , DBRow row) {
		 
		}
 


}
